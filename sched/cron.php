<?php                                                                                          
require __DIR__ . '/../config.inc.php';       
                                                                                               
/**********************************************************************                                                                                             
 * Raggruppamento e accodamento a file history                                                 
 **********************************************************************/                                                                                            
$sql = "INSERT INTO {$cfg_mod_Admin['file_app_log_history']}(LHUSER, LHDATE, LHCONT)           
   SELECT lguser, date(lgdend), count(*)                                                       
   from {$cfg_mod_Admin['file_app_log']}                                                       
   where date(lgdend) < current date                                                           
   AND ( date(lgdend) > (select max(lhdate) from {$cfg_mod_Admin['file_app_log_history']})     
          or (select max(lhdate) from {$cfg_mod_Admin['file_app_log_history']}) is null)       
   group by lguser, date(lgdend) order by date(lgdend), lguser";

$stmt = db2_prepare($conn, $sql);
if ($stmt === false) die("ERROR: prepare INSERT: " . db2_stmt_errormsg() . "\n\n{$sql}");
$result = db2_execute($stmt);
if ($result === false) die("ERROR: execute INSERT: " . db2_stmt_errormsg($stmt) . "\n\n{$sql}");

/***********************************************************************
 * Delete da file log
************************************************************************/
$sql = "DELETE FROM {$cfg_mod_Admin['file_app_log']} where date(lgdend) < current date - 3 DAYS";
$stmt = db2_prepare($conn, $sql);
if ($stmt === false) die("ERROR: prepare DELETE: " . db2_stmt_errormsg() . "\n\n{$sql}");
$result = db2_execute($stmt);
if ($result === false) die("ERROR: execute DELETE: " . db2_stmt_errormsg($stmt) . "\n\n{$sql}");

/*
 * Fine
*/
echo "\n\nFine.\n\n"
?>
                                                                                               