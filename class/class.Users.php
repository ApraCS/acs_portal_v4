<?php

class Users extends AbstractDb {
	
 	protected $table_id 	= "file_utenti"; 
	protected $table_k 		= "UTCUTE";
	protected $default_fields = array(
			'UTCOGE' 	=> '',
	        'UTCOG2' 	=> '',
	        'UTCOG3' 	=> '',
	        'UTCOG4' 	=> '',
	        'UTCOG5' 	=> ''
	);
	
	
	
	
public function find_all_by_ATTAV($op, $aspe = null, $row_ord = array()){
	global $conn, $cfg_mod_Admin, $id_ditta_default;

	$ret_ar = array();
	
	//dalla operazione (ATTAV) recupero quale utente aggiungere a quelli ammessi (rif ordine o cliente)
	$sql = "SELECT * FROM {$cfg_mod_Admin['file_tabelle']} TA_ATTAV	WHERE TADT=? AND TA_ATTAV.TATAID = 'ATTAV' AND TAKEY1=? ";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$rows = db2_execute($stmt, array($id_ditta_default, $op));
	$r_ATTAV = db2_fetch_assoc($stmt); 	
	
	$ar_add_users = array();
	switch ($r_ATTAV['TAFG01']){
		case "C": //ref. cliente
			$ar_add_users[] = trim($row_ord['TDSTOE']);
		break;
		case "E": //ENTRAMBI
			$ar_add_users[] = trim($row_ord['TDSTOE']);
			$ar_add_users[] = trim($row_ord['TDUSIO']);
			break;				
		default: //ref. ordine			
			$ar_add_users[] = trim($row_ord['TDUSIO']);			
	}
		
	$sql = "SELECT * FROM " . $this->get_table("Y") . "
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tabelle']} TA_ATTAV 
			 ON TA_ATTAV.TAKEY1 = UTCUTE AND TAKEY2 = ? AND TA_ATTAV.TATAID = 'USATT'
			WHERE TA_ATTAV.TADT = '{$id_ditta_default}' AND TA_ATTAV.TATAID IS NOT NULL OR UTCUTE IN(" . sql_t_IN($ar_add_users) .") 			 	
			ORDER BY UTDESC ";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$rows = db2_execute($stmt, array($op));
	$ret_ar["data"] = array();
		while ($row = db2_fetch_assoc($stmt)){
	
			//preparo i campi per le chiavi
			foreach(explode(',', $this->table_k) as $key)
				$row["k_{$key}"] = $row[$key];
				
			//se richiesto faccio delle operazioni per manipolare i dati
			$row = $this->after_get_row($row);
			$ret_ar["data"][] = array_map('trim',$row);
		}

		return $ret_ar["data"];
}



public function find_all_by_ATTAV_ar($op_ar, $aspe = null, $row_ord = array()){
	global $conn, $cfg_mod_Admin, $id_ditta_default;

	$ret_ar = array();


	$sql = "SELECT * FROM " . $this->get_table("Y") . "
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tabelle']} TA_USATT
				ON TA_USATT.TAKEY1 = UTCUTE AND TAKEY2 IN(" . sql_t_IN($op_ar) .") AND TA_USATT.TATAID = 'USATT'
			WHERE TA_ATTAV.TADT = '{$id_ditta_default}' AND TA_USATT.TATAID IS NOT NULL
			ORDER BY UTDESC ";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$rows = db2_execute($stmt, array());
	$ret_ar["data"] = array();
	while ($row = db2_fetch_assoc($stmt)){

	//preparo i campi per le chiavi
		foreach(explode(',', $this->table_k) as $key)
		$row["k_{$key}"] = $row[$key];

		//se richiesto faccio delle operazioni per manipolare i dati
		$row = $this->after_get_row($row);
		$ret_ar["data"][] = array_map('trim',$row);
	}

		return $ret_ar["data"];
}




public function get_by_cod($cod){
	global $conn;
	$sql = "SELECT * FROM " . $this->get_table("Y");
	$sql .= " WHERE UTCUTE = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($cod));
	$row = db2_fetch_assoc($stmt);
	$this->rec_data = $row;
}	
	
public static function out_Writer_Form_initComponent() {
 global $system_general_tipologia_utente;	
 $ret =  "
        this.addEvents('create');
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-user-16',
            frame: true,
            title: 'Utente',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_UTCUTE', xtype: 'hidden'},       
            {
                fieldLabel: 'Utente',
                name: 'UTCUTE',
                allowBlank: false
            }, {
                fieldLabel: 'Email',
                name: 'UTMAIL',
                allowBlank: false,
                vtype: 'email'        
            }, {
                fieldLabel: 'Denominazione',
                name: 'UTDESC',
                allowBlank: false
            }, {
							name: 'UTTPUT',
							xtype: 'combo',
							fieldLabel: 'Tipologia',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
						   	allowBlank: false,
 							forceSelection: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json($system_general_tipologia_utente, '') . " 	
								    ] 
							}						 
			}, {
							name: 'UTPRAC',
							xtype: 'combo',
							fieldLabel: 'Profilo',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
 							forceSelection: true,
						   	allowBlank: false,
                            store: {
                            autoLoad: true,
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
                                
					            url : '".ROOT_PATH."/module/base/acs_get_tab_TA.php?fn=get_data',
					            reader: {
					                type: 'json',
	                                method: 'POST',		
					                root: 'root',
					                totalProperty: 'totalCount'
					            },
                                actionMethods: {
    							          read: 'POST'
    							        },
                                extraParams: {
               		    		    		tab: 'PRAC'
               		    		       },               						        
				        		doRequest: personalizza_extraParams_to_jsonData
					        },       
								fields: ['id', 'text'],            	
			            }										
							/*store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('PRAC'), '') . "	
								    ] 
							}	*/				 
			}, {
                fieldLabel: 'Cod.Riservatezza',
                name: 'UTCOGE',
                allowBlank: true
            }, {
                fieldLabel: 'Cod.Ris. (2)',
                name: 'UTCOG2',
                allowBlank: true
            }, {
                fieldLabel: 'Cod.Ris (3)',
                name: 'UTCOG3',
                allowBlank: true
            }, {
                fieldLabel: 'Cod.Ris (4)',
                name: 'UTCOG4',
                allowBlank: true
            }, {
                fieldLabel: 'Cod.Ris (5)',
                name: 'UTCOG5',
                allowBlank: true
            }, {
                fieldLabel: 'Passwod',
                name: 'UTPSW',
                allowBlank: false
            }],
        dockedItems: [" . self::form_buttons(self::form_buttons_pre()) . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public static function out_Writer_Grid_initComponent_columns() {
	$ret = "
{
                text: 'Utente',
                width: 80,
                sortable: true,
                dataIndex: 'UTCUTE',
                filter: {type: 'string'}, filterable: true
            }, {
                header: 'Email',
                flex: 1,
                sortable: true,
                dataIndex: 'UTMAIL',
                field: {
                    type: 'textfield'
                },
               filter: {type: 'string'}, filterable: true
            }, {
                header: 'Denominazione',
                width: 100,
                sortable: true,
                dataIndex: 'UTDESC',
                field: {
                    type: 'textfield'
                },
                filter: {type: 'string'}, filterable: true
            }, {
                header: 'Profilo',
                width: 100,
                sortable: true,
                dataIndex: 'UTPRAC',
                field: {
                    type: 'textfield'
                },
                filter: {type: 'string'}, filterable: true
            }, {
                header: 'Tipologia',
                width: 100,
                sortable: true,
                dataIndex: 'UTTPUT',
                field: {
                    type: 'textfield'
                },
                filter: {type: 'string'}, filterable: true
            }, {
                header: 'Cod.Riserv.',
                width: 100,
                sortable: true,
                dataIndex: 'UTCOGE',
                field: {
                    type: 'textfield'
                },
                filter: {type: 'string'}, filterable: true
            }	
	";
 return $ret;	
}	
	
	
	
	
public static function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_UTCUTE',        
        useNull: false
    }, {
        name: 'UTCUTE',        
        useNull: false
    },  'UTDESC', 'UTMAIL', 'UTPSW', 'UTTPUT', 'UTPRAC', 'UTCOGE', 'UTCOG2', 'UTCOG3', 'UTCOG4', 'UTCOG5' ]
});

";

}	
	
	


static public function form_buttons_pre() {

	$cl= new ListinoTrasportatori();
	$cl_UsersAtt = new UsersAttivita();

	return "
			{
                    iconCls: 'icon-arrivi-16',
                    itemId: 'users-attivita',
                    text: 'To Do list',
                    disabled: false,
                    scope: this,
                    handler: function(){
						" . $cl_UsersAtt->out_Writer_Form($cl_UsersAtt->out_Writer_Form_initComponent(), "usersattivita") . "
						" . $cl_UsersAtt->out_Writer_Grid($cl_UsersAtt->out_Writer_Grid_initComponent_columns(), "usersattivita") . "
						" . $cl_UsersAtt->out_Writer_Model("usersattivita") . "
						" .	$cl_UsersAtt->out_Writer_Store("usersattivita", "") . "
						" .	$cl_UsersAtt->out_Writer_sotto_main("usersattivita", 0.4) . "
						" . $cl_UsersAtt->out_Writer_main("Elenco utenti disponibili in assegnazione attivit&agrave;",  "usersattivita") . "
						" . $cl_UsersAtt->out_Writer_window("Tabella attivit&agrave; di gestione ordine per utente") . "
                    }
                },
 ";
}

	
	
	
	
	
}

?>