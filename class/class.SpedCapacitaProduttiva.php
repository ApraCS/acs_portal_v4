<?php

class SpedCapacitaProduttiva extends AbstractDb {
 
 	protected $table_id 	= "file_capacita_produttiva"; 
	protected $table_k 		= "CPDATA,CPRIFE,CPAREA,CPTPCO";
	protected $tadt_f 		= "CPDT";
	protected $default_fields = array('CPCAPA' => 0);	
	
	
	public function get_table($per_select = false){
		global $cfg_mod_Spedizioni;
		$ret = $cfg_mod_Spedizioni[$this->table_id];
		if ($per_select == "Y")
			$ret .= " PRI " . $this->get_table_joins();
		return $ret;
	}	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Capacit&agrave; produttiva/spedizione',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_CPDATA', xtype: 'hidden'}, {name: 'k_CPRIFE', xtype: 'hidden'}, {name: 'k_CPAREA', xtype: 'hidden'}, {name: 'k_CPTPCO', xtype: 'hidden'}, 
            {
                fieldLabel: 'Anno (aaaa) o Data (aaaammgg)',
 				labelWidth: 205,
                name: 'CPDATA',
                allowBlank: false
            }, {
                fieldLabel: 'Giorno (1 .. 6)',
                name: 'CPRIFE',
                allowBlank: true
            }, {
							name: 'CPAREA',
							xtype: 'combo',
							fieldLabel: 'Area spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('ASPE', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
                fieldLabel: 'Reparto',
                name: 'CPTPCO',
                allowBlank: false
            }, {
                fieldLabel: 'Capacit&agrave;',
                name: 'CPCAPA',
                allowBlank: false
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Data / Anno',
                width: 90,
                sortable: true,
                dataIndex: 'CPDATA',
                allowBlank: false,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Rifer.',
                width: 80,
                sortable: true,
                dataIndex: 'CPRIFE',
                allowBlank: false,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'AREA',
                width: 80,
                sortable: true,
                dataIndex: 'CPAREA',
                allowBlank: false,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Reparto',
                flex: 80,
                sortable: true,
                dataIndex: 'CPTPCO',
                allowBlank: false,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Capacit&agrave;',
                width: 80,
                sortable: true,
                dataIndex: 'CPCAPA',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_CPDATA',        
        useNull: false
    }, {
        name: 'CPDATA',        
        useNull: false
    }, {
        name: 'k_CPRIFE',        
        useNull: false
    }, {
        name: 'CPRIFE',        
        useNull: false
    }, {
        name: 'k_CPAREA',        
        useNull: false
    }, {
        name: 'CPAREA',        
        useNull: false
    }, {
        name: 'k_CPTPCO',        
        useNull: false
    }, {
        name: 'CPTPCO',        
        useNull: false
    }, 'CPCAPA']
});
";

}	
	


public function get_tot_by_data($data, $reparto){
 global $cfg_mod_Spedizioni, $conn;
 global $id_ditta_default;
	 $sql_cp = "SELECT 0 as TIPO_REC, CPDATA, CPTPCO, CPRIFE, CPAREA, CPCAPA
		FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP
		INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL ON CAL.CSDT=CP.CPDT AND CAL.CSDTRG = CP.CPDATA AND CAL.CSCALE = '*CS' 
		WHERE CAL.CSDTRG = {$data} AND CP.CPTPCO = '{$reparto}' AND CP.CPDT='$id_ditta_default'
 
 			UNION

		SELECT 1 as TIPO_REC, CPDATA, CPTPCO, CPRIFE, CPAREA, CPCAPA
		FROM {$cfg_mod_Spedizioni['file_capacita_produttiva']} CP2		 
		INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL2 ON CAL2.CSDT=CP2.CPDT AND CAL2.CSAARG = CP2.CPDATA AND CAL2.CSGIOR = CP2.CPRIFE AND CAL2.CSCALE = '*CS'		
		WHERE CAL2.CSDTRG = {$data} AND CP2.CPTPCO = '{$reparto}' AND CP2.CPDT='$id_ditta_default'

		ORDER BY TIPO_REC DESC
 		";
 
 
 $stmt_cp = db2_prepare($conn, $sql_cp);
 echo db2_stmt_errormsg(); 
 $result = db2_execute($stmt_cp);
 
 //Carico tutto in un array (per ogni area di spedizione devo prendere solo il valore per day o per data)
 // avendo ordinato in DESC per tipo_record, considero prima la quantita' per giorno (generica)
 // che eventualmente viene sostiuita (per area) dalla quantita' per data (specifica)
 $per_area = array();
 while ($r = db2_fetch_assoc($stmt_cp)) {
 	$per_area[$r['CPAREA']] = $r['CPCAPA'];
 }
 
 //sommo la quantita' delle varie aree
 $ret = 0;
 foreach($per_area as $s)
 	$ret += $s;
 
 return $ret;	
}
	
public function get_tot_by_data_linea($data, $linea, $ordini_selezionati = null){
    
    global $cfg_mod_Spedizioni, $conn;
    global $id_ditta_default;
    
    $s = new Spedizioni(array('no_verify' => 'Y'));
    
    $sql_join_on_PL0 = "ON PLDT=TDDT AND PLTIDO=TDOTID AND PLINUM=TDOINU AND PLAADO=TDOADO AND digits(PLNRDO)=TDONDO";
    if ($cfg_mod_Spedizioni['disabilita_digits_on_PL0'] == 'Y')
        $sql_join_on_PL0 = "ON PLDT=TDDT AND PLTIDO=TDOTID AND PLINUM=TDOINU AND PLAADO=TDOADO AND PLNRDO=TDONDO";
    
      
        if(is_null($ordini_selezionati) || !is_array($ordini_selezionati['ar_tddocu'])){
            $sql_ordini_data = "AND TDDTEP = '{$data}'";
        }else{
            
                if($ordini_selezionati['IE'] == 'E') //escludi ordini selezionati
                    $sql_ordini_data = "AND TDDTEP = '{$data}' AND TDDOCU NOT IN (" . sql_t_IN($ordini_selezionati['ar_tddocu']) . ")";
                else   //aggiungi ordini selezionati
                    $sql_ordini_data = "AND (TDDTEP = '{$data}' OR TDDOCU IN (" . sql_t_IN($ordini_selezionati['ar_tddocu']) . "))";
             
        }
     
        
    $sql = "SELECT SUM(CASE WHEN PLTABB <> 'U' AND PLTABB <> 'F' THEN 1 ELSE 0 END) AS COLLI_T
            FROM {$cfg_mod_Spedizioni['file_colli']} PL
            INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
            {$sql_join_on_PL0}
            INNER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIPC
             ON PL.PLDT = TA_LIPC.TADT AND TA_LIPC.TAID = 'LIPC' AND TA_LIPC.TANR = PL.PLFAS10
            WHERE " . $s->get_where_std() . " AND TDSWSP='Y' AND TA_LIPC.TACINT = '{$linea}'
            {$sql_ordini_data}
    ";
            
           /* print_r($ordini_selezionati);
            print_r($sql);
            exit;*/
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg(); 
    $result = db2_execute($stmt);
    $r = db2_fetch_assoc($stmt);
    
    
    $r['capac'] = $this->get_tot_by_data($data, $linea);
    $diff_val = $r['COLLI_T'] - $r['capac'];
    if($r['capac']  != 0)
        $p_cap = $diff_val/$r['capac'] * 100;
        
    if($p_cap >= 5)
        $r['c_color'] = 'sfondo_rosso';
    else if($p_cap >= -5)
        $r['c_color'] = 'sfondo_giallo';
    else
        $r['c_color'] = 'sfondo_verde';
    
        $r['p_cap'] = n($p_cap, 1, 'Y');
        $r['data'] = $data;
        $r['linea'] = $linea;
        $diff_val = n($diff_val, 0, 'Y');
        $r['tooltip'] = "Capacit� {$r['capac']}, {$diff_val},  {$r['p_cap']}%";
        
     return $r;
     
            
    
}
	
}

?>