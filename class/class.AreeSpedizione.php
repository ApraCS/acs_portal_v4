<?php

class AreeSpedizione extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TADT,TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "ASPE";
	protected $tadt_f 		= "TADT";
	protected $con_YAML_PARAMETERS = 'Y';		
	
	public function load_rec_data_by_itin($itin_id){
		global $conn, $cfg_mod_Spedizioni;
	
		$sql = "SELECT TA_ASPE.* FROM " . $this->get_table('Y')
				.  " INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
				ON TA_ASPE.TATAID = 'ASPE' AND PRI.TAASPE = TA_ASPE.TAKEY1
				WHERE PRI.TATAID = 'ITIN' AND PRI.TAKEY1 = " . sql_t(trim($itin_id)) . "
				";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
	
		$this->rec_data = db2_fetch_assoc($stmt);
	}
	
	
	
	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Area spedizione',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TADT', xtype: 'hidden'}, {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},            
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden'
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 3
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }, {
                fieldLabel: 'Programmazione area dopo il (aaaammdd)',
                name: 'TARIF1',
                allowBlank: true,
 				maxLength: 8
            }, {
				name: 'TARIF2',
				xtype: 'combo',
				fieldLabel: 'Stabilimento',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,					     		
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('START', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}
					     		
			, {
			                fieldLabel: 'Protocollazione minima (aaaammdd)',
			                name: 'TAASPE',
			                allowBlank: true,
			 				maxLength: 8
			   }	,

                {
			                fieldLabel: 'Tipo carico',
			                name: 'TASITI',
			                allowBlank: true,
			 				maxLength: 3
			   }				     		
					     		
			, {
				xtype: 'fieldcontainer',
				defaultType: 'textfield',
				layout: { 	type: 'hbox' /*, pack: 'start', align: 'stretch'*/},
				fieldDefaults: {
	                anchor: '100%',
                	labelAlign: 'right'
            	},					     		
				items: [
					     		
						
			
						, {
			                fieldLabel: 'Tipologia Ordini',
			                name: 'TAFG01',
			                allowBlank: false,
			 				maxLength: 1,
					     	width: 130,
					     	allowBlank: true
			            }										     		

					   , {
			                fieldLabel: '',
			                name: 'TAFG02',
			                allowBlank: false,
			 				maxLength: 1,
					     	width: 30,
					     	allowBlank: true
			            }
					    , {
			                fieldLabel: '',
			                name: 'TAFG03',
			                allowBlank: false,
			 				maxLength: 1,
					     	width: 30,
					     	allowBlank: true
			            }					     		
					     		
				]
			}	     							     		
					     		
					     		
     		
					     		
					     		
			, {
                fieldLabel: 'Parametri',
                name: 'YAML_PARAMETERS',
                xtype: 'textareafield',
 		 		grow: true,
 				height: 200         
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TADT',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, 'TARIF1', 'TARIF2', 'TAMAIL', 'YAML_PARAMETERS', 'TAASPE', 'TAFG01', 'TAFG02', 'TAFG03', 'TASITI']
});
";

}	
	
	
	
}

?>