<?php

class Itinerari extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "ITIN";
	protected $tadt_f 		= "TADT";
	protected $con_YAML_PARAMETERS = 'Y';
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Itinerario',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},            
            {
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 10
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }, {
                fieldLabel: 'Sequenza',
                name: 'TASITI',
                allowBlank: true,
 				maxLength: 5
            }, {
                fieldLabel: 'e-mail',
                name: 'TAMAIL',
                allowBlank: true,
                vtype: 'email',
 				maxLength: 100
            }, {
				name: 'TAASPE',
				xtype: 'combo',
				fieldLabel: 'Area di spedizione',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: false,
				forceSelection: true, 																
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('ASPE', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
                fieldLabel: '% Magg. volume',
                name: 'TAPMIN',
				xtype: 'numberfield',					     		
                allowBlank: true
            }, {
				name: 'TARIF1',
				xtype: 'combo',
				fieldLabel: 'Itinerario abbinato',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
 				forceSelection: true,
			   	allowBlank: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('ITIN', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
                fieldLabel: 'Parametri',
                name: 'YAML_PARAMETERS',
                xtype: 'textareafield',
 		 		grow: true,
 				height: 200         
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Area',
                flex: 20,
                sortable: true,
                dataIndex: 'TAASPE',
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Sequenza',
                flex: 20,
                sortable: true,
                dataIndex: 'TASITI'
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, 'TAMAIL', 'TAASPE', 'TASITI', 'TAPMIN', 'TARIF1', 'YAML_PARAMETERS']
});
";

}	
	
	
	
}

?>