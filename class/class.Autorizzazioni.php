<?php

class Autorizzazioni extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "AUDE";
	
	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Autorizzazioni',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'}, 
            {
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, {name: 'TAVOLU', defaultValue: 0}, 
    {name:'TAPESO', defaultValue: 0}, {name: 'TABANC', defaultValue: 0}]
});
";

}	
	
	
	
}

?>