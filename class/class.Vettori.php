<?php

class Vettori extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TADT,TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "AUTR";
	protected $tadt_f 		= "TADT";
	protected $con_YAML_PARAMETERS = 'Y';		
	
	public function after_get_row($row){
		$s = new Spedizioni(array('no_verify' => 'Y'));
		//decodifico i vari valori
		$row['TACOGE_D'] = $s->decod_std('AVET', $row['TACOGE']);
		return $row;
	}	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Vettore',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TADT', xtype: 'hidden'}, {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'}
             , {
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 3
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }, {
                fieldLabel: 'Telefono',
                name: 'TATELE',
 				maxLength: 20
            }, {
                fieldLabel: 'email',
                name: 'TAMAIL',
                vtype: 'email',
 				maxLength: 100
            }, {
				name: 'TACOGE',
				xtype: 'combo',
				fieldLabel: 'Trasportatore',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true, 		
			   	allowBlank: true,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('AVET', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
				name: 'TATISP',
				xtype: 'combo',
				fieldLabel: 'Tipologia spedizione',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('TSPE', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
				name: 'TATITR',
				xtype: 'combo',
				fieldLabel: 'Tipologia trasporto',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('TTRA', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
                fieldLabel: 'Parametri',
                name: 'YAML_PARAMETERS',
                xtype: 'textareafield',
 		 		grow: true,
 				height: 200         
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }, {
                text: 'Telefono',
                flex: 30,
                sortable: true,
                dataIndex: 'TATELE'
            }, {
                text: 'Trasportatore',
                flex: 40,
                sortable: true,
                dataIndex: 'TACOGE_D'
            }, {
                text: 'TS',
                width: 40,
                sortable: true,
                dataIndex: 'TATISP'
            }, {
                text: 'TT',
                width: 40,
                sortable: true,
                dataIndex: 'TATITR'
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TADT',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, 'TATELE', 'TAMAIL', 'TACOGE', 'TACOGE_D', 'TATISP', 'TATITR', 'YAML_PARAMETERS']
});
";

}	
	
	
	
}

?>