<?php

class ListinoTrasportatori extends AbstractDb {
 
 	protected $table_id 	= "file_listino_trasportatori"; 
	protected $table_k 		= "LTDT,LTVETT,LTPROG";
	protected $table_gen_f  = "LTPROG";
	protected $table_gen_id = "NRLT";
	protected $tadt_f 		= "LTDT";	
	protected $default_fields = array(
			'LTCSPE' 	=> 0,
			'LTCOCO'	=> 0,
			'LTCSCA'	=> 0,
			'LTDTFV'	=> 0,
			'LTSESC'	=> 0, 
			'LTCOKM'	=> 0,
			'LTKMIN'	=> 0,
			'LTAMIN'	=> 0, 
			'LTAMAX'	=> 0, 
			'LTCUN1'	=> 0, 
			'LTQTA1'	=> 0, 
			'LTCUN2'	=> 0, 
			'LTQTA2'	=> 0, 
			'LTCUN3'	=> 0, 
			'LTQTA3'	=> 0, 
			'LTCUN4'	=> 0, 
			'LTQTA4'	=> 0, 
			'LTCUN5'	=> 0, 
			'LTQTA5'	=> 0,
			'LTPIMP'	=> 0			
			);

	
	

	public function load_rec_data_per_costo_sped($sped_id, $deposito_exchange = null){
		global $conn, $cfg_mod_Spedizioni;		
		$s = new Spedizioni;
		$spedizione = $s->get_spedizione($sped_id);		
		
		if (is_null($deposito_exchange)){
			
			//dal vettore prendo il trasportatore (a cui e' abbinato il listino)
			$tr = new Trasportatori;
			$tr->load_rec_data_by_vettore($spedizione['CSCVET']);
			$tr_id = $tr->rec_data['TAKEY1'];
			
			//dalla spedizione recupero il mezzo (perche' il listino potrebbe essere valido solo per una classe mezzo)
			$au = new Auto;
			$au->load_rec_data_by_k(array('TAKEY1' => $spedizione['CSCAUT']));
		} else {
			//per un determinato deposito_exchange
			$au = new Auto;
			$tr_id = $deposito_exchange;
		}	
		
		//recupero l'elenco dei listini validi per il trasportatore
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_listino_trasportatori']}
				 WHERE LTVETT = " . sql_t($tr_id) . "
				   AND LTDTIV <= {$spedizione['CSDTSP']} AND (LTDTFV >= {$spedizione['CSDTSP']} OR LTDTFV = 0)
				   AND LTAUTO IN ('', '{$au->rec_data['TARIF1']}')
				   AND LTITIN IN ('', '{$spedizione['CSCITI']}')				   
				 ORDER BY LTITIN DESC, LTAUTO DESC  			
		 		";
		
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	

		$this->rec_data = db2_fetch_assoc($stmt);				
		
	}



	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-address_book-16',
            frame: true,
            title: 'Listino trasportatore',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right',
                allowBlank: true
            },
            items: [
            {name: 'k_LTDT',   xtype: 'hidden'},
 			{name: 'k_LTVETT', xtype: 'hidden'},
 			{name: 'k_LTPROG', xtype: 'hidden'}, 		
            {
                name: 'LTVETT',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Descrizione',
                name: 'LTDESC',
                allowBlank: false,
 				maxLength: 100 		
            }, {
                fieldLabel: 'Valido dal',
                name: 'LTDTIV',
                allowBlank: false
            }, {
                fieldLabel: 'Valido al',
                name: 'LTDTFV'
            }, {
                fieldLabel: 'Province incluse',
                name: 'LTLPRO',
 				maxLength: 50 		
            }, {
                fieldLabel: 'Nazioni incluse',
                name: 'LTLNAZ',
 				maxLength: 50
            }, {
							name: 'LTITIN',
							xtype: 'combo',
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true, 																
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('ITIN', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
							name: 'LTAUTO',
							fieldLabel: 'Classe mezzo',
							maxLength: 10
						 
			}, {
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Costo per spedizione',
					defaultType: 'textfield',
			
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
					    labelAlign: 'right',								     		
						labelWidth: 45,
						width: 40
			        },
								     		
			        items: [{
		                name: 'LTCSPE',
						flex: 1
		            }, {
		                name: 'LTPIMP',
						fieldLabel: '%Imp.sped.',
						labelWidth: 65,
						flex: 3
		            }]
			}								     		
								     		
								     		
								     		
			, {
                fieldLabel: 'Costo per collo',
                name: 'LTCOCO'
            },

								     		

								     		
								     		
				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Per scarico',
					defaultType: 'textfield',
			
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
					    labelAlign: 'right',								     		
						labelWidth: 45,
						width: 40
			        },
								     		
			        items: [{
				                fieldLabel: 'Costo un',
				                name: 'LTCSCA',
								flex: 1
				            }, {
				                fieldLabel: 'Nr esclusi',
				                name: 'LTSESC',
								flex: 1
				            }]
			    },								     		
								     										     		
								     		
								     		
								     		
								     		
								     		


				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Per Km',
					defaultType: 'textfield',
			
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
					    labelAlign: 'right',								     		
						labelWidth: 45,
						width: 40
			        },
								     		
			        items: [{
		                 fieldLabel: 'Costo un',
        		         name: 'LTCOKM',
						 flex: 1
            		}, {
		                 fieldLabel: 'Km Minimi',
        		         name: 'LTKMIN',
						 flex: 1
            		}]
			    },								     		
								     		
								     		
								     		
								     		
								     		
				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Addebito',
					defaultType: 'textfield',
			
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
					    labelAlign: 'right',								     		
						labelWidth: 45,
						width: 40
			        },
								     		
			        items: [{
		                 fieldLabel: 'Min',
        		         name: 'LTAMIN',
						 flex: 1
            		}, {
		                 fieldLabel: 'Max',
        		         name: 'LTAMAX',
						 flex: 1
            		}]
			    },								     		
								     		
								     		
								     		
			{
			  xtype: 'fieldset',
			  title: 'Costo per volume',
			  defaultType: 'textfield',
			  layout: 'anchor',
              defaults: {
                 layout: '100%'
              },
								     		
								     		
			  items: [

				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Soglia',
			        labelWidth: 75,
					defaultType: 'textfield',
					bodyPadding: 10,
			
			        // The body area will contain three text fields, arranged
			        // horizontally, separated by draggable splitters.
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
						labelWidth: 65,
					    labelAlign: 'right',
						width: 20
			        },
								     		
			        items: [{
		                 fieldLabel: 'Da',
        		         name: 'LTQTA1',
						 flex: 1
            		}, {
		                 fieldLabel: 'costo m3',
        		         name: 'LTCUN1',
						 flex: 1
            		}]
			    },

				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Soglia',
			        labelWidth: 75,
					defaultType: 'textfield',
					bodyPadding: 10,
			
			        // The body area will contain three text fields, arranged
			        // horizontally, separated by draggable splitters.
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
						labelWidth: 65,
					    labelAlign: 'right',
						width: 20
			        },
								     		
			        items: [{
		                 fieldLabel: 'Da',
        		         name: 'LTQTA2',
						 flex: 1
            		}, {
		                 fieldLabel: 'costo m3',
        		         name: 'LTCUN2',
						 flex: 1
            		}]
			    },

				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Soglia',
			        labelWidth: 75,
					defaultType: 'textfield',
					bodyPadding: 10,
			
			        // The body area will contain three text fields, arranged
			        // horizontally, separated by draggable splitters.
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
						labelWidth: 65,
					    labelAlign: 'right',
						width: 20
			        },
								     		
			        items: [{
		                 fieldLabel: 'Da',
        		         name: 'LTQTA3',
						 flex: 1
            		}, {
		                 fieldLabel: 'costo m3',
        		         name: 'LTCUN3',
						 flex: 1
            		}]
			    },

				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Soglia',
			        labelWidth: 75,
					defaultType: 'textfield',
					bodyPadding: 10,
			
			        // The body area will contain three text fields, arranged
			        // horizontally, separated by draggable splitters.
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
						labelWidth: 65,
					    labelAlign: 'right',
						width: 20
			        },
								     		
			        items: [{
		                 fieldLabel: 'Da',
        		         name: 'LTQTA4',
						 flex: 1
            		}, {
		                 fieldLabel: 'costo m3',
        		         name: 'LTCUN4',
						 flex: 1
            		}]
			    },

				{
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Soglia',
			        labelWidth: 75,
					defaultType: 'textfield',
					bodyPadding: 10,
			
			        // The body area will contain three text fields, arranged
			        // horizontally, separated by draggable splitters.
			        layout: 'hbox',
								     		
					fieldDefaults: {
			            msgTarget: 'under',
			            //labelAlign: 'top',
						labelWidth: 65,
					    labelAlign: 'right',
						width: 20
			        },
								     		
			        items: [{
		                 fieldLabel: 'Da',
        		         name: 'LTQTA5',
						 flex: 1
            		}, {
		                 fieldLabel: 'costo m3',
        		         name: 'LTCUN5',
						 flex: 1
            		}]
			    }								     		
								     		
								     		
								     		
								     										     		
								     		
								     		
								     		
			  ]								     		
			  
			}					     		
								     		
								     		
								     		
			
								     		
		],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "{
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'LTDESC'                
            }, {
                text: 'Valido dal',
                sortable: true,
                dataIndex: 'LTDTIV',
                width: 70                
            }, {
                text: 'Valido al',
                sortable: true,
                dataIndex: 'LTDTFV',
                width: 70                                
            }, {
                text: 'Spedizione',
                sortable: true,
                dataIndex: 'LTCSPE',
                width: 70                                
            }, {
                text: 'Scarico',
                sortable: true,
                dataIndex: 'LTCSCA',
                width: 70                                
            }, {
                text: 'Esclusi',
                sortable: true,
                dataIndex: 'LTSESC',
                width: 70                                
            }, {
                text: 'Costo Km',
                sortable: true,
                dataIndex: 'LTCOKM',
                width: 70                                
            }, {
                text: 'Km min',
                sortable: true,
                dataIndex: 'LTKMIN',
                width: 100                                
            }, {
                text: 'Cl.Mezzo',
                sortable: true,
                dataIndex: 'LTAUTO',
                width: 90                                
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'k_LTPROG',
        type: 'int',
        useNull: true
    }, {
        name: 'k_LTVETT',
        useNull: true
    }, {
        name: 'k_LTDT',
        useNull: true
    }, {
        name: 'LTPROG',
        type: 'int',
        useNull: true
    }, 'LTDESC', 'LTDTIV', 'LTDTFV', 'LTLPRO', 'LTLNAZ', 'LTITIN', 'LTAUTO', 'LTCSPE', 'LTCOCO', 'LTCSCA', 'LTSESC', 'LTCOKM', 'LTKMIN'
     , 'LTAMIN', 'LTAMAX', 'LTCUN1', 'LTQTA1', 'LTCUN2', 'LTQTA2', 'LTCUN3', 'LTQTA3', 'LTCUN4', 'LTQTA4', 'LTCUN5', 'LTQTA5', 'LTPIMP'
	]
    

});
";

/*
 	//	{name: 'LTDTIV', type: 'date', convert: function(v, rec) { console.log(v); console.log(rec); return Ext.Date.format(v,'Ymd'); }},     
    //    {name: 'LTDTFV', type: 'date', convert: function(v, rec) { console.log(v); console.log(rec); return Ext.Date.format(v,'Ymd'); }}]
 */

}	
	
	
// TABELLE UTILIZZZATE DALLA CLASSE
	public function get_table($per_select = false){
		global $cfg_mod_Spedizioni;
		$ret = $cfg_mod_Spedizioni[$this->table_id];
		if ($per_select == "Y")
			$ret .= " PRI " . $this->get_table_joins();
		return $ret;
	}	
	
	public function get_table_NR(){
		global $cfg_mod_Spedizioni;
		return $cfg_mod_Spedizioni['file_numeratori'];
	}	
	
	
}

?>