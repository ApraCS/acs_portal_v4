<?php

class DeskAcqStabilimenti extends SpedStabilimenti {
	
	protected $con_YAML_PARAMETERS = 'Y';	
 

	

	public function form_buttons_2() {
	
		$cl= new ListinoTrasportatori();
		$cl_itinerari = new TrasportatoriItinerari();
	
		return "
		{
			xtype: 'toolbar',
			dock: 'bottom',
			ui: 'footer',
			items: ['->', {
				iconCls: 'icon-calendar-16',
				itemId: 'listino',
				text: 'Disponibilit&agrave;',
				disabled: false,
				scope: this,
				handler: function(){
					console.log(this.activeRecord);
					acs_show_win_std('Gestione disponibilit&agrave;', 'acs_panel_fo_presca_gestione.php?fn=get_open_form', {tipo_risorsa: 'STAB', codice: this.activeRecord.get('TAKEY1'), descr: this.activeRecord.get('TADESC')}, 800, 400, null, 'icon-calendar-16');
					return false;
				}
			}]
		}";
	}
	

	
	

	public function out_Writer_Form_initComponent() {
		$ret =  "
        this.addEvents('create');
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Sede inizio trasporto',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden'
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false
            }, {
                fieldLabel: 'Denominazione',
                name: 'TADESC',
                allowBlank: false
            }, {
                fieldLabel: 'Indirizzo',
                name: 'TAINDI',
 				maxLength: 60
            }, {
                fieldLabel: 'CAP',
                name: 'TACAP',
 				maxLength: 10
            }, {
                fieldLabel: " . j("Localit&agrave;") . ",
                name: 'TALOCA',
 				maxLength: 60
            }, {
                fieldLabel: 'Provincia',
                name: 'TAPROV',
                maxLength: 2
            }, {
                fieldLabel: 'Nazione',
                name: 'TANAZI',
                maxLength: 3
            }, {
                fieldLabel: 'Latitudine',
                name: 'TARIF1',
                allowBlank: true
            }, {
                fieldLabel: 'Longitudine',
                name: 'TARIF2',
                allowBlank: true
            }, {
                fieldLabel: 'Elenco porte<br>(separate da \",\")',
                name: 'TAMAIL',
                allowBlank: true,
 				maxLength: 100
            }, {
                fieldLabel: 'Parametri',
                name: 'YAML_PARAMETERS',
                xtype: 'textareafield',
 		 		grow: true,
 				height: 200         
            }],
            dockedItems: [" . self::form_buttons() . ", " . self::form_buttons_2() . "]
        });
        this.callParent();
";
		return $ret;
	}
	
	
	
	
	
	
	// TABELLE UTILIZZZATE DALLA CLASSE
	public function get_table($per_select = false){
		global $cfg_mod_DeskAcq;
		$ret = $cfg_mod_DeskAcq[$this->table_id];
		if ($per_select == "Y")
			$ret .= " PRI " . $this->get_table_joins();
		return $ret;
	}
	
	public function get_table_NR(){
		global $cfg_mod_DeskAcq;
		return $cfg_mod_DeskAcq['file_numeratori'];
	}
	
	public function get_table_TA(){
		global $cfg_mod_DeskAcq;
		return $cfg_mod_DeskAcq['file_tabelle'];
	}
	
}

?>