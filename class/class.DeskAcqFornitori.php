<?php

class DeskAcqFornitori extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TADT,TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "FORN";
	protected $tadt_f 		= "TADT";
	protected $default_fields = array('TAPMIN' 	=> 0);
	protected $con_YAML_PARAMETERS = 'Y';
	
	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Fornitori',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TADT', xtype: 'hidden'}, {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},            
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 20
            }, {
                fieldLabel: 'Denominazione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }, {
                fieldLabel: 'email',
                name: 'TAMAIL',
                vtype: 'email',
 				maxLength: 100
            }, {
				name: 'TAASPE',
				xtype: 'combo',
				fieldLabel: 'Gruppo fornitura',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " . acs_ar_to_select_json(find_TA_std('ITIN', null, 1, null, null, null, $this), '') . "
					    ]
				}
			}, {
                fieldLabel: 'Porta di default',
                name: 'TARIF1',
                allowBlank: true,
 				maxLength: 20
            }, {
                fieldLabel: 'Orario default',
                name: 'TARIF2',
                allowBlank: true,
 				maxLength: 20
            }, {
                fieldLabel: 'Sequenza prenotazione<br>(0 - fissa orario)',
				xtype: 'numberfield',
				allowDecimals: false, 		
                name: 'TAPMIN',
                allowBlank: true,
 				maxLength: 20
            }, {
                fieldLabel: 'Inibisci chiusura scaduti',
                name: 'TAFG01',
                xtype: 'checkboxfield',
                inputValue: 'Y'                
            }, {
                fieldLabel: 'Inibisci riprogramm. scaduti',
                name: 'TAFG02',
                xtype: 'checkboxfield',
                inputValue: 'Y'                
            }, {
                fieldLabel: 'Parametri',
                name: 'YAML_PARAMETERS',
                xtype: 'textareafield',
 		 		grow: true,
 				height: 200         
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Denominazione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }, {
                text: 'Porta',
                width: 50,
                sortable: true,
                dataIndex: 'TARIF1',
                allowBlank: true
            }, {
                text: 'Orario',
                width: 50,
                sortable: true,
                dataIndex: 'TARIF2',
                allowBlank: true,
				renderer: time_from_AS, align: 'right'
            }, {
                text: 'Seq.',
                width: 34,
                sortable: true,
                dataIndex: 'TAPMIN',
                allowBlank: true,
				renderer: floatRenderer0, align: 'right',
				
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TADT',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, 'TARIF1', 'TARIF2', 'TAPMIN', 'TAMAIL', 'YAML_PARAMETERS', 'TAASPE'
 	    , {name: 'TAFG01', type: 'string', convert: function(v, rec) { return (v === 'Y' || v === 'S' || v === true) ? 'Y' : 'N'; }}
 	    , {name: 'TAFG02', type: 'string', convert: function(v, rec) { return (v === 'Y' || v === 'S' || v === true) ? 'Y' : 'N'; }}
 	    , {name: 'TAFG03', type: 'string', convert: function(v, rec) { return (v === 'Y' || v === 'S' || v === true) ? 'Y' : 'N'; }} 	     	    
	]
});
";

}	
	
	

// TABELLE UTILIZZZATE DALLA CLASSE
public function get_table($per_select = false){
	global $cfg_mod_DeskAcq;
	$ret = $cfg_mod_DeskAcq[$this->table_id];
	if ($per_select == "Y")
		$ret .= " PRI " . $this->get_table_joins();
	return $ret;
}

public function get_table_NR(){
	global $cfg_mod_DeskAcq;
	return $cfg_mod_DeskAcq['file_numeratori'];
}

public function get_table_TA(){
	global $cfg_mod_DeskAcq;
	return $cfg_mod_DeskAcq['file_tabelle'];
}




	
}

?>