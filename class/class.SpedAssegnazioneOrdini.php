<?php

class SpedAssegnazioneOrdini {
	
    protected $ar_sql = array();
	protected $mod;
	protected $cfg_mod;
	
	//posso usarla? invece di scrivere $cfg_mod_Spedizioni['file_assegna_ord']
	//$this->cfg_mod['file_assegna_ord'], giusto??
	function __construct($mod = null) {
	    if ($mod === null){
	        //di default uso il mod Spedizioni
	        $mod = new Spedizioni(array('no_verify' => 'Y')); //TODO: VERIFCARE SE E' GIUSTO!!!!
	    }
	    
	    $this->mod	 = $mod;
	    $this->cfg_mod = $mod->get_cfg_mod();
	}
	
	
	
	public function get_el_entry_per_ordine($k_ordine, $solo_aperte = 'N'){
	    global $conn, $cfg_mod_Spedizioni, $cfg_mod_DeskArtV;

		$sql = "SELECT * FROM {$this->cfg_mod['file_assegna_ord']}
					WHERE ASDOCU = ? ";
		
		if ($solo_aperte == 'Y')
			$sql .= " AND ASFLRI <> 'Y'";
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($k_ordine));		
		
		return $stmt;
	}

	
	
	public function ha_entry_aperte_per_ordine($k_ordine){
	    global $conn, $cfg_mod_Spedizioni, $cfg_mod_DeskArt, $id_ditta_default;
	
		$sql = "SELECT count(*) AS T_ROW FROM {$this->cfg_mod['file_assegna_ord']}
				WHERE ASDOCU = ? AND ASFLRI <> 'Y'";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($k_ordine));
	
		$row = db2_fetch_assoc($stmt);
		
		if ($row['T_ROW'] > 0) return true;
		return false;
	}

	
	public function ha_entry_aperte_per_utente($utente){
	    global $conn, $cfg_mod_Spedizioni, $cfg_mod_DeskArt, $id_ditta_default;
	    
	    $sql = "SELECT count(*) AS T_ROW FROM {$this->cfg_mod['file_assegna_ord']}
	           WHERE ASUSAT = ? AND ASFLRI <> 'Y'";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, array($utente));
	    echo db2_stmt_errormsg($stmt);
	    $row = db2_fetch_assoc($stmt);
	    if ($row['T_ROW'] > 0) return true;
	    return false;
	}
	

	public function get_by_prog($prog){
	    global $conn, $cfg_mod_Spedizioni, $cfg_mod_DeskArt;
	
		$sql = "SELECT * FROM {$this->cfg_mod['file_assegna_ord']} WHERE ASIDPR = ?";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($prog));
	
		$row = db2_fetch_assoc($stmt);
		return $row;
	}	
	
	
	public function stato_entry_per_ordine_causale($k_ordine, $causale){
	    global $conn, $cfg_mod_Spedizioni, $cfg_mod_DeskArt;
	
		$sql = "SELECT ASFLRI, count(*) AS T_ROW FROM {$this->cfg_mod['file_assegna_ord']}
				WHERE ASDOCU = ? AND ASCAAS = ?
				GROUP BY ASFLRI
				";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($k_ordine, $causale));
		
		$ar = array();
		while ($row = db2_fetch_assoc($stmt)){
			$ar['TOT'] += $row['T_ROW'];
			if ($row['ASFLRI'] == 'Y') $ar['CLOSE'] += $row['T_ROW'];
			if ($row['ASFLRI'] != 'Y') $ar['OPEN']  += $row['T_ROW'];
		}
	
		
		if ($ar['TOT'] > 0){
			//ho almeno un messaggio
			if ($ar['OPEN'] == 0){
				//sono tutti chiusi
				return 1;
			} else {
				//alcuni sono aperti
				return 2;
			}
			
		} else
			return 0;
		

		return false;
	}	
	


	public function stato_tooltip_entry_per_ordine($k_ordine, $causale = null, $per_report = 'N', $filtra_gruppo = null, $mostra_causale_di_rilascio = 'N', $escludi_rilasciate = 'N'){
	    global $conn, $cfg_mod_Admin, $cfg_mod_Spedizioni, $cfg_mod_DeskArt, $id_ditta_default;
		
		$ret = array();
	
		$sql_where = "";
		$sql_where_p = array();
		
		if (strlen(trim($causale)) > 0 ){
			$sql_where .= " AND ASCASS=?";
			$sql_where_p[] = $causale;
		}
		
		if (strlen(trim($filtra_gruppo)) > 0 ){
			$sql_where .= " AND TA_ATTAV.TARIF1=?";
			$sql_where_p[] = $filtra_gruppo;
		}		
		
		if ($escludi_rilasciate == 'Y'){
			$sql_where .= " AND ASFLRI<>'Y'";
		}		
		
		$sql = "SELECT * FROM {$this->cfg_mod['file_assegna_ord']} AS0
				INNER JOIN {$cfg_mod_Admin['file_tabelle']} TA_ATTAV
			     ON TA_ATTAV.TADT = '{$id_ditta_default}' AND TA_ATTAV.TATAID='ATTAV' AND AS0.ASCAAS = TA_ATTAV.TAKEY1 
				WHERE ASDOCU = ? " . $sql_where . " ORDER BY ASDTAS, ASHMAS";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array_merge(
				array($k_ordine),
				$sql_where_p
				));
	
		$tooltip = array();
		$att_open = array();
		$att_distinct = array();
		while ($row = db2_fetch_assoc($stmt)){
			$ar['TOT']++;
			if ($row['ASFLRI'] == 'Y') $ar['CLOSE']++;
			if ($row['ASFLRI'] != 'Y') $ar['OPEN']++;
						
			if ($per_report != 'Y'){
				$tooltip[] = implode(" - " , array(
							print_date($row['ASDTAS']), 
							trim($row['ASUSAS']), 
							trim($row['TADESC']),
							"[" . trim($row['ASUSAT']) . "]",
							$this->out_flag_rilasciato($row['ASFLRI'], $mostra_causale_di_rilascio, $row['ASCARI']),
							trim($row['ASNORI']),
							$this->get_memo($row['ASIDPR'])
				));
			} else {
				$tooltip_out = "";
				$riga_bold = implode(" - " , array(
							print_date($row['ASDTAS']), 
							trim($row['ASUSAS']), 
							trim($row['TADESC']),
							"[" . trim($row['ASUSAT']) . "]",
							$this->out_flag_rilasciato($row['ASFLRI'], $mostra_causale_di_rilascio, $row['ASCARI'])));
				$riga_memo = $this->get_memo($row['ASIDPR']);
				$tooltip_out = "<b>" . $riga_bold . "</b>";
				if (strlen(trim($row['ASNORI'])) > 0)
					$tooltip_out .= "<br/>" . trim($row['ASNORI']);
				if (strlen(trim($riga_memo)) > 0)
					$tooltip_out .= "<br/>" . trim($riga_memo);

				$tooltip[] = $tooltip_out;
			}

			$att_distinct[trim($row['ASCAAS'])] = trim($row['ASCAAS']);			
			
			if ($row['ASFLRI'] != 'Y'){
				//array con elenco attivita' aperte
				$att_open[] = trim($row['TADESC']); 
			}
			
		}
	
	
		if ($ar['TOT'] > 0){
			//ho almeno un messaggio
			if ($ar['OPEN'] == 0){
				//sono tutti chiusi
				$ret['stato'] = 1;
			} else {
				//alcuni sono aperti
				$ret['stato'] = 2;
			}
				
		} else
			$ret['stato'] = 0;
	
		
		$ret['tooltip'] = implode("<br>", $tooltip);
		$ret['att_open'] = implode("<br>", $att_open);
		$ret['att_distinct'] = implode("<br>", $att_distinct);
	
		return $ret;
	}
	
	
	public function get_memo($id_prog, $blocco = 'ASMEM'){
		global $conn, $cfg_mod_Admin, $cfg_mod_Spedizioni, $id_ditta_default;
		$sql = "select NTMEMO FROM {$cfg_mod_Spedizioni['file_note']}
		 	WHERE NTDT = ? AND NTTPNO = '{$blocco}' AND INTEGER(NTKEY1) = ? AND NTSEQU=0";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($id_ditta_default, $id_prog));
		
		$tooltip = array();
		$att_open = array();
		$row = db2_fetch_assoc($stmt);

		return trim($row['NTMEMO']);
	}
	
	
	public function out_flag_rilasciato($flag, $mostra_causale_di_rilascio = 'N', $causale_di_rilascio = ''){
		if ($flag == 'Y'){
			if ($mostra_causale_di_rilascio == 'N')
			 return 'Rilasciato';
			else
			 return trim($causale_di_rilascio); 
		}
		return '';
	}
	
	
	public function crea($tipo_messaggio, $ar_par){
		global $auth;
			
		if ($ar_par['form_values']['f_notifica_assegnazione'] == 'Y'){
			$mail = prepare_new_mail();
			
			$s = new Spedizioni(array('no_verify' => 'Y'));
			$causale = $ar_par['form_values']['f_causale'];
			$causale_d = $s->decod_std('ATTAV', trim($causale));
				
			
			//come destinatario prendo l'utente assegnato
			$user_to = new Users();
			$user_to->get_by_cod($ar_par['form_values']['f_utente_assegnato']);
			$mail->addAddress(trim($user_to->rec_data['UTMAIL']));
			
			$ord_out = trim("{$ar_par['ordine']['TDOTPD']}_{$ar_par['ordine']['TDOADO']}_{$ar_par['ordine']['TDONDO']}");
			
			$mail->Subject = 'Supply Desk - ' . $causale_d . " [{$ord_out}]";
			
			//corpo email
			$body = '';
			$body = "
                <h2>Inserimento nuova attivit&agrave;</h2>
			    <table BORDER=0 WIDTH=\"100%\">
					<tr><th align='left' width='200'>Da:</th><td>" . $auth->out_name() . "</td></tr>					
					<tr><th align='left' width='200'>Attivit&agrave;:</th><td>{$causale_d}</td></tr>
					<tr><th align='left' width='200'>Riferimento:</th><td>{$ar_par['form_values']['f_note']}</td></tr>
					<tr><th align='left' width='200'>Ordine cliente:</th><td>{$ord_out}</td></tr>		
					<tr><th align='left' width='200'>Cliente/Riferimento:</th><td>" . trim($ar_par['ordine']['TDDCON']) . " / " . trim($ar_par['ordine']['TDVSRF']) . "</td></tr>
				    <tr><th align='left' width='200'>Memo:</th><td>{$ar_par['form_values']['f_memo']}</td></tr>
                 </table>
			 
			";
			
			$mail->Body = $body;
			$mail->send();				
		}

		
		switch ($tipo_messaggio){
			default:
				$prog = $this->crea_std($ar_par);
			break;			
		}
		
	//se devo chiudere la segnalazione di partenza (provengo da un avanzamento)
		if (strlen($ar_par['as_selected']->prog) > 0){
		    
			$this->avanza($ar_par['form_values']['f_entry_prog_causale'], array(
					'prog' 			=> $ar_par['as_selected']->prog,
					'prog_coll'		=> $prog,
					'form_values' => $ar_par['form_values']
			   
			));			
		}
		
	}
	
	
	public function avanza($causale_rilascio, $ar_par, $nrec = 0){		
	    global $auth, $conn, $cfg_mod_Spedizioni;
	    
		//In base ai parametri in RILAV verifico se scrivere il file RI0
		$row = $this->get_by_prog($ar_par['prog']);
		
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$modulo = $this->mod;
		$row_rilav = $modulo->get_TA_std('RILAV', trim($row['ASCAAS']), $causale_rilascio);
		
		@include '../../personal/ao_avanza.php';
		

		//se richiesto scrivo RI0 CON IL MESSAGGIO PERSONALIZZATO (e non rilascio il messaggio)
		if (strlen(trim($row_rilav['TATELE'])) > 0){
		    $sh = new SpedHistory($this->mod);
			$sh->crea(
					'ril_entry_PERS',
					array(
							"prog" 			=> $ar_par['prog'],
							"k_ordine"		=> $row['ASDOCU'],
							"op"			=> trim($row['ASCAAS']),
							"ex"			=> trim($row['ASCARI']),
					        "RIAUTO"        => trim($causale_rilascio),
							"messaggio"		=> trim($row_rilav['TATELE']),
					)
					);
		}
		
		
		//in base a rilav verifico se eseguire anche una call
		if (strlen(trim($row_rilav['TACOGE'])) > 0){
			global $tkObj, $libreria_predefinita, $libreria_predefinita_EXE;
				
			$oe = $s->k_ordine_td_decode_xx($row['ASDOCU']);
				
			//costruzione del parametro
			$cl_p = str_repeat(" ", 246);
			$cl_p .= sprintf("%-2s", trim($oe['TDDT']));
			$cl_p .= sprintf("%-2s", trim($oe['TDOTID']));
			$cl_p .= sprintf("%-3s", trim($oe['TDOINU']));
			$cl_p .= sprintf("%-4s", trim($oe['TDOADO']));
			$cl_p .= sprintf("%-6s", trim($oe['TDONDO']));
				
			//attivita + causale di rilascio
			$cl_p .= sprintf("%-10s", trim($row_rilav['TAKEY1']));
			$cl_p .= sprintf("%-3s",  trim($row_rilav['TAKEY2']));
			
			$cl_p .= sprintf("%-3s",  '');		//funzione history
			$cl_p .= sprintf("%-10s",  '');		//prgramma di aggiornamento
			
			$cl_p .= sprintf("%-1s",  ''); //flag OUTPUT Y=ELABORATO
			
			$cl_p .= sprintf("%06d",  $nrec); //serve per la call da visualizza righe
			
			$cl_p .= sprintf("%-50s",  $row['ASDOCU']); //TDDOCU
			
			//Per VERGAR: passo anche i riferimenti per l'ordine origine da assegnare
			$cl_p .= sprintf("%-2s", ''); //TDOTID
			$cl_p .= sprintf("%-3s", ''); //TDOINU
			$cl_p .= sprintf("%04d", $ar_par['form_values']['f_ord_orig_anno']);
			$cl_p .= sprintf("%06d", $ar_par['form_values']['f_ord_orig_numero']);
			
			$cl_p .= sprintf("%-10s", trim($auth->get_user()));	//utente
			
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
				
			$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
			$call_return = $tkObj->PgmCall(trim($row_rilav['TACOGE']), $libreria_predefinita_EXE, $cl_in, null, null);
		
			$lk_area = $call_return['io_param']['LK-AREA'];
			if (substr($lk_area, 289, 1) != 'Y') {
				$ret = array();
				$ret['success'] = false;
				$ret['message'] = 'Attenzione. Aggiornamento non riuscito. Richiedere assistenza';
				echo acs_je($ret);
				return;
			}
		}		
		
		
		if ($nrec > 0) return; //sono da visualizza righe, non chiudo l'intera riga di AS0
		
		//Gestisco il flag di rilasciato solo se NON ho una call da chiamare
		if (strlen(trim($row_rilav['TACOGE'])) == 0 && strlen(trim($row_rilav['TATELE'])) == 0
		    && trim($row_rilav['TASTAL']) != 'A'){ 
		    $sql = "UPDATE {$this->cfg_mod['file_assegna_ord']}
					SET ASCARI = ?, ASNORI = ?, ASDTRI = ?, ASHMRI = ?, ASIDAC = ?, ASFLRI = ?
					WHERE ASIDPR = ?";			
			if (!isset($ar_par['prog_coll'])) $ar_par['prog_coll'] = 0;
			
			
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt, array($causale_rilascio, $ar_par['form_values']['f_entry_prog_note'] . '',
						oggi_AS_date(), oggi_AS_time(), $ar_par['prog_coll'],'Y',
						$ar_par['prog']));
			if (!$result) echo db2_stmt_errormsg($stmt);
		} //update flag rilascio


		//se richiesto scrivo RI0
		if ($row_rilav['TAFG01'] == 'Y'){
		    $sh = new SpedHistory($this->mod);
			$sh->crea(
					'ril_entry',
					array(
							"prog" 			=> $ar_par['prog'],
							"k_ordine"		=> $row['ASDOCU'],
							"op"			=> trim($row['ASCAAS']),
							"ex"			=> trim($row['ASCARI'])
					)
			);			
		}
		
				
		if($ar_par['form_values']['f_ril'] == 'R')
		    $this->write_email($row_rilav, $row, $ar_par);
		    
		
		@include '../../personal/ao_avanza_after.php';
	
	}	
	
	public function write_email($row_rilav, $row, $ar_par){
	    global $auth, $conn;
	    
	    $mail = prepare_new_mail();
	    
	    
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	   
	    //come destinatario prendo l'utente assegnato
	    $user_to = new Users();
	    $user_to->get_by_cod(trim($row['ASUSAS']));
	    $mail->addAddress(trim($user_to->rec_data['UTMAIL'])); 

	    
	    $ord_out = trim("{$ar_par['ordine']['TDOTPD']}_{$ar_par['ordine']['TDOADO']}_{$ar_par['ordine']['TDONDO']}");
	    $causale_d = trim($row_rilav['TADESC']);
	    $mail->Subject = 'Supply Desk - ' . $causale_d . " [{$ord_out}]";
	    
	    //corpo email
	    
	   
	    $desc_attav = $s->decod_std('ATTAV', trim($row['ASCAAS']));
	    $memo_attav = $this->get_memo($row['ASIDPR']);
	
	    $body = " <h2>Chiusura attivit&agrave;</h2>

                  <table BORDER=0 WIDTH=\"100%\">
					<tr><th align='left' width='200'>Da:</th><td>" . $auth->out_name() . "</td></tr>
					<tr><th align='left' width='200'>Causale di rilascio: </th><td>{$causale_d}</td></tr>
					<tr><th align='left' width='200'>Note: </th><td>{$ar_par['form_values']['f_entry_prog_note']}</td></tr>	
                    <tr><td colspan=2>___________________________________________________</td></tr>		    
			
					<tr><th align='left' width='200'>Da:</th><td>" . $auth->out_name() . "</td></tr>
					<tr><th align='left' width='200'>Attivit&agrave;:</th><td>{$desc_attav}</td></tr>
					<tr><th align='left' width='200'>Riferimento: </th><td>{$row['ASNOTE']}</td></tr>
					<tr><th align='left' width='200'>Ordine cliente:</th><td>{$ord_out}</td></tr>
					<tr><th align='left' width='200'>Cliente/Riferimento:</th><td>" . trim($ar_par['ordine']['TDDCON']) . " / " . trim($ar_par['ordine']['TDVSRF']) . "</td></tr>
					<tr><th align='left' width='200'>Memo:</th><td>{$memo_attav}</td></tr>
					</table>
					
					";

	    $mail->Body = $body;
	    $mail->send();
	    
	}
	
	
	public function crea_std($ar_par){
	    
		global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
			
		$this->ar_sql['ASCAAS'] = $ar_par['form_values']['f_causale'];
		$this->ar_sql['ASNOTE'] = $ar_par['form_values']['f_note'] . '';
		$this->ar_sql['ASUSAT'] = $ar_par['form_values']['f_utente_assegnato'];
		if ($ar_par['form_values']['f_notifica_rilascio'] == 'Y') $this->ar_sql['ASFLNR'] = "R";		
		$this->ar_sql['ASDTVI'] = $ar_par['form_values']['f_attiva_dal'];		
		$this->ar_sql['ASDTSC'] = (int)$ar_par['form_values']['f_scadenza'];
		
		if (isset($ar_par['form_values']['f_progetto']))
		    $this->ar_sql['ASPROG'] = $ar_par['form_values']['f_progetto'];
	    if (isset($ar_par['form_values']['f_prog_coll']))
	        $this->ar_sql['ASIDAC'] = $ar_par['form_values']['f_prog_coll'];
	    
	    if (isset($ar_par['form_values']['f_ASORD1'])) $this->ar_sql['ASORD1'] = $ar_par['form_values']['f_ASORD1'];
	    if (isset($ar_par['form_values']['f_ASORD2'])) $this->ar_sql['ASORD2'] = $ar_par['form_values']['f_ASORD2'];
	    if (isset($ar_par['form_values']['f_ASORD3'])) $this->ar_sql['ASORD3'] = $ar_par['form_values']['f_ASORD3'];
	    if (isset($ar_par['form_values']['f_ASORD4'])) $this->ar_sql['ASORD4'] = $ar_par['form_values']['f_ASORD4'];
	    if (isset($ar_par['form_values']['f_ASORD5'])) $this->ar_sql['ASORD5'] = $ar_par['form_values']['f_ASORD5'];
	        
		    
		$this->imposta_campi_base();
		
		if (isset($ar_par['k_ordine']))
			$this->imposta_campi_ordine($ar_par['k_ordine']);
		else			
			$this->imposta_campi_ordine($ar_par['ordine']['TDDOCU']);
		
		if (strlen(trim($this->ar_sql['ASUSAT'])) == 0)
			$this->ar_sql['ASUSAT'] = 'SV2_GPS';
		
		
		//in base a ATTAV verifico se deve essere creata gia' rilasciata
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$row_attav = $s->get_TA_std('ATTAV', trim($this->ar_sql['ASCAAS']));
		if ($row_attav['TAFG02'] == 'R'){
			$this->ar_sql['ASFLRI'] = 'Y';
			$this->ar_sql['ASCARI'] = $row_attav['TARIF2'];
			if (strlen(trim($this->ar_sql['ASUSAT'])) == 0)
				$this->ar_sql['ASUSAT'] = $this->ar_sql['ASUSAS'];
			if (strlen(trim($this->ar_sql['ASDTVI'])) == 0)
				$this->ar_sql['ASDTVI'] = $this->ar_sql['ASDTAS'];			
			$this->ar_sql['ASDTRI'] = $this->ar_sql['ASDTAS'];
			$this->ar_sql['ASHMRI'] = $this->ar_sql['ASHMAS'];
		}				
		
		$sql = $this->get_insert_sql();
		
		if ($this->scrivi_messaggio($sql)){
		    
		    if(isset($ar_par['blocco'])){
		        $blocco = $ar_par['blocco'];
		    }else{
		        $blocco = 'ASMEM';
		    }
			
			//salvo eventuali note memo
			if (strlen(trim($ar_par['form_values']['f_memo']))){
				$sqlMemo = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(NTDT, NTMEMO, NTKEY1, NTSEQU, NTTPNO)
						VALUES(?, ?, ?, ?, ?)";				
				
				$stmtMemo = db2_prepare($conn, $sqlMemo);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmtMemo, array($id_ditta_default, $ar_par['form_values']['f_memo'], $this->ar_sql['ASIDPR'], 0, $blocco));
				
			}
			
			return $this->ar_sql['ASIDPR'];			
		}
		else 
			return false;
	}

	
	
	
	protected function scrivi_messaggio($sql){
		global $conn;
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		echo db2_stmt_errormsg();		
		return $result;		
	}
	
	protected function imposta_campi_base(){
		global $auth;
		$this->ar_sql['ASUSAS'] = $auth->get_user();
		$this->ar_sql['ASDTAS'] = oggi_AS_date();
		$this->ar_sql['ASHMAS'] = oggi_AS_time();
		
		//numeratore
		$modulo = $this->mod;
		$this->ar_sql['ASIDPR'] = $modulo->next_num('IDATT');
	}
	
	protected function get_insert_sql(){
	    global $cfg_mod_Spedizioni, $cfg_mod_DeskArt;
		
	    $sql = "INSERT INTO {$this->cfg_mod['file_assegna_ord']} (" . $this->get_el_field_name() . ")
				VALUES (" . $this->get_el_field_values() . ") 
				";
	
		return $sql;
	}
	
	protected function get_el_field_name(){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $kf;

		return implode(', ', $ar);
	}
	
	protected function get_el_field_values(){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $v;
	
		return implode(', ', array_map('sql_t_trim', $ar));
	}	
	
	protected function imposta_campi_ordine($k_ordine){		
		$this->ar_sql['ASDOCU'] 	= $k_ordine;		
	}
	
	protected function imposta_campi_carico($k_carico){
		if (strlen($k_carico) == 0) return;
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$oe = $s->k_carico_td_decode($k_carico);
	
		$this->ar_sql['RITPCA'] = $oe['TDTPCA'];
		$this->ar_sql['RIAACA'] = $oe['TDAACA'];
		$this->ar_sql['RINRCA'] = $oe['TDNRCA'];
	
	}	
	
}

?>