<?php

class SpedHistory {
	
	protected $ar_sql = array();
	protected $mod;
	protected $cfg_mod;
		
	function __construct($mod = null) {
	  if ($mod === null){	  
	  	//di default uso il mod Spedizioni
	  	$mod = new Spedizioni(array('no_verify' => 'Y')); //TODO: VERIFCARE SE E' GIUSTO!!!!
	  }
	
	  $this->mod	 = $mod;
	  $this->cfg_mod = $mod->get_cfg_mod();
	}	
	
	
	public function crea($tipo_messaggio, $ar_par){
		
		switch ($tipo_messaggio){
			case 'assegna_spedizione':
				$this->crea_history_assegna_spedizione($ar_par);
			break;
			case 'modifica_spedizione':
				$this->crea_history_modifica_spedizione($ar_par);
			break;
			case 'assegna_indice_rottura':
				$this->crea_history_assegna_indice_rottura($ar_par);
				break;
			case 'END_assegna_indice_rottura':
				$this->crea_history_END_assegna_indice_rottura($ar_par);
				break;
			case 'provvigioni_aggiorna_perc_importo_agente':
				$this->crea_history_provvigioni_aggiorna_perc_importo_agente($ar_par);
				break;
			case 'provvigioni_azzera_perc_importo_agente':
				$this->crea_history_provvigioni_azzera_perc_importo_agente($ar_par);
				break;				
			case 'provvigioni_nuova_perc_importo_agente':
				$this->crea_history_provvigioni_nuova_perc_importo_agente($ar_par);
				break;				
			case 'provvigioni_aggiorna_agente':
				$this->crea_history_provvigioni_aggiorna_agente($ar_par);
				break;
			case 'provvigioni_aggiorna_perc_provv_su_riga':
				$this->crea_history_provvigioni_aggiorna_perc_provv_su_riga($ar_par);
				break;
			case 'provvigioni_aggiorna_perc_provv_azzera_su_riga':
				$this->crea_history_provvigioni_aggiorna_perc_provv_azzera_su_riga($ar_par);
				break;					
			case 'provvigioni_inserisci_maggiorazione_diminuzione_per_agente':
				$this->crea_history_provvigioni_inserisci_maggiorazione_diminuzione_per_agente($ar_par);
				break;
				
				
			case 'assegna_spedizione_desk_acq':
				$this->crea_history_assegna_spedizione_desk_acq($ar_par);
				break;			
			case 'sincronizza_spedizione':
				$this->crea_history_sincronizza_spedizione($ar_par);
				break;		
			case 'sincronizza_ordine':
				$this->crea_history_sincronizza_ordine($ar_par);
				break;
			case 'assegna_scarico':
				$this->crea_history_assegna_scarico($ar_par);
			break;			
			case 'assegna_destinazione_finale':
				$this->crea_history_assegna_destinazione_finale($ar_par);
				break;
			case 'assegna_destinazione_sosta_tecnica':
				$this->crea_history_assegna_destinazione_sosta_tecnica($ar_par);
				break;							
			case 'assegna_carico':
				$this->crea_history_assegna_carico($ar_par);
			break;			
			case 'upd_data_confermata':
				$this->crea_history_upd_data_confermata($ar_par);
				break;			
			case 'assegna_seq_carico':
				$this->crea_history_assegna_seq_carico($ar_par);
				break;			
			case 'upd_commento_ordine':
				$this->crea_history_upd_commento_ordine($ar_par);
				break;				
			case 'variazione_destinazione':
				$this->crea_history_variazione_destinazione($ar_par);
				break;
			case 'variazione_destinazione_sosta_tecnica':
				$this->crea_history_variazione_destinazione_sosta_tecnica($ar_par);
				break;				
			case 'ril_entry':
				$this->crea_history_ril_entry($ar_par);
				break;
			case 'ril_entry_PERS':
				$this->crea_history_ril_entry_PERS($ar_par);
				break;
			case 'stampa_doc_ordine':
				$this->crea_history_stampa_doc_ordine($ar_par);
				break;				
			case 'forza_invio_conferma':
				$this->crea_history_forza_invio_conferma($ar_par);
				break;

			case 'forza_chiusura_ordine':
				$this->crea_history_forza_chiusura_ordine($ar_par);
				break;
			case 'dora_gen_ord_mto':
				$this->crea_history_dora_gen_ord_mto($ar_par);
				break;
			case 'dora_gen_ord_mpr':	//promo
				$this->crea_history_dora_gen_ord_mpr($ar_par);
				break;
			case 'dora_gen_ord_mts':	//promo
				$this->crea_history_dora_gen_ord_mts($ar_par);
				break;
			case 'aggiorna_preferenza':	//promo
				$this->crea_history_aggiorna_preferenza($ar_par);
				break;
			case 'blocca_ordine':	//promo
				$this->crea_history_blocca_ordine($ar_par);
				break;			
			case 'conferma_fornitura':	//promo
				$this->crea_history_conferma_fornitura($ar_par);
				break;				
			case 'annulla_fornitura':	//promo
				$this->crea_history_annulla_fornitura($ar_par);
				break;
			case 'conferma_ord_reso':	//promo
				$this->crea_history_conferma_ord_reso($ar_par);
				break;
			case 'aggiungi_articoli':	//promo
				return $this->crea_history_aggiungi_articoli($ar_par);
				break;
			
			case 'modifica_articoli':	//promo
				$this->crea_history_modifica_articoli($ar_par);
				break;
			case 'cancella_articoli':	//promo
				$this->crea_history_cancella_articoli($ar_par);
				break;
			case 'duplica_preventivo':	//promo
				return $this->crea_history_duplica_preventivo($ar_par);
				break;
			case 'preventivo_to_ordine':	//promo
				return $this->crea_history_preventivo_to_ordine($ar_par);
				break;
			case 'crea_ordine_assistenza':	//promo
				return $this->crea_history_crea_ordine_assistenza($ar_par);
				break;
			case 'registra_anticipo':	//promo
				return $this->crea_history_registra_anticipo($ar_par);
				break;
			case 'restituzione_caparra':	//promo
			    return $this->crea_history_restituzione_caparra($ar_par);
			    break;
			case 'genera_fattura':	//promo
				return $this->crea_history_genera_fattura($ar_par);
				break;
			case 'abbina_fattura':	//promo
				return $this->crea_history_abbina_fattura($ar_par);
				break;

			case 'attiva_spunta':	//fatture entrata acq
				return $this->crea_history_attiva_spunta($ar_par);
				break;
			case 'annulla_spunta':	//fatture entrata acq
				return $this->crea_history_annulla_spunta($ar_par);
				break;
			case 'attiva_anomalia':	//fatture entrata acq
				return $this->crea_history_attiva_anomalia($ar_par);
				break;
			case 'annulla_anomalia': //fatture entrata acq
				return $this->crea_history_annulla_anomalia($ar_par);
				break;
			case 'modifica_ord_mto':	//promo
				return $this->crea_history_modifica_ord_mto($ar_par);
				break;
			case 'modifica_ord_mag_mto':	//promo
				return $this->crea_history_modifica_ord_mag_mto($ar_par);
				break;
			case 'genera_ddt':	//promo
				return $this->crea_history_genera_ddt($ar_par);
				break;
			case 'salva_stato':	//promo
				return $this->crea_history_salva_stato($ar_par);
				break;
			case 'ripristino_fornitura':	//promo
			    return $this->crea_history_ripristino_fornitura($ar_par);
			    break;
			case 'riapplica_listino':	// riapplica listino su ddt
				return $this->crea_history_riapplica_listino($ar_par);
				break;
			case 'aggiorna_listino':	// riapplica listino su ddt
				return $this->crea_history_aggiorna_listino($ar_par);
				break;
			case 'mod_rif_ddt':	// riapplica listino su ddt
				return $this->crea_history_mod_rif_ddt($ar_par);
				break;
			case 'ddt_rimuovi_da_fattura':	// riapplica listino su ddt
				return $this->crea_history_ddt_rimuovi_da_fattura($ar_par);
				break;
			case 'mod_importo_spese_fatt':	//mod importo spese fatture
				return $this->crea_history_mod_importo_spese_fatt($ar_par);
				break;
			case 'mod_pagam_fatt':	//mod importo spese fatture
				return $this->crea_history_mod_pagam_fatt($ar_par);
				break;
			case 'cambia_stato':	//promo
				return $this->crea_history_cambia_stato($ar_par);
				break;
			case 'genera_fatt_acc':	//promo
				return $this->crea_history_genera_fatt_acc($ar_par);
				break;
			case 'genera_nota_credito':	//promo
				return $this->crea_history_genera_nota_credito($ar_par);
				break;
			case 'stampa_fatt_acq':	//promo
				return $this->crea_history_stampa_fatt_acq($ar_par);
				break;
			case 'stampa_fatt_ven':	//promo
				return $this->crea_history_stampa_fatt_ven($ar_par);
				break;
			case 'pers':	//promo
				return $this->crea_history_pers($ar_par);
				break;				
		}
		
	}
	
	

	public function rendi_attiva_sessione($use_session_history, $clear_session = 'Y'){
		global $conn, $appLog, $auth;
		
		$use_session_history = sprintf("%-15s", $use_session_history);
		$len_from = strlen($use_session_history) + 2;
		
		
		if ($clear_session == 'N') { //non rimuovo la sessione da RIRGES
			$sql = "UPDATE {$this->cfg_mod['file_richieste']}
					SET RIESIT = 'A'
					WHERE SUBSTR(RIRGES, 1, " . strlen($use_session_history) . ") = ?";			
		} else { 	//rimuovo la session da RIRGES	
			$sql = "UPDATE {$this->cfg_mod['file_richieste']} 
					SET RIRGES = SUBSTR(RIRGES, " . $len_from . ", 100), RIESIT = 'A'
					WHERE SUBSTR(RIRGES, 1, " . strlen($use_session_history) . ") = ?";
		}

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		if (!$stmt){
			$al = new AppLog();
			$al->write_log("prepare " . $sql);
		}		
		$result = db2_execute($stmt, array($use_session_history));
		
		//scrivo messaggio in A per attivare trigger
		$this->ar_sql['RIRGES'] = "START|{$use_session_history}";
		$this->ar_sql['RIESIT'] = '';
		$this->ar_sql['RIUSRI'] = $auth->get_user();
		$this->ar_sql['RIDTRI'] = oggi_AS_date();
		$this->ar_sql['RIHMRI'] = oggi_AS_time();		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	

	private function crea_history_aggiorna_preferenza($ar_par){
		$this->ar_sql['RIRGES'] = 'AGG_PREF';
		$this->ar_sql['RIFG01'] = $ar_par['pref'];
		$this->ar_sql['RINOTR'] = trim($ar_par['ex']);
	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_stampa_fatt_acq($ar_par){
		$this->ar_sql['RIRGES'] = 'PRT_FAT_ACQ';
	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_stampa_fatt_ven($ar_par){
		$this->ar_sql['RIRGES'] = 'PRT_FAT_VEN';
		$this->ar_sql['RIFG01'] = $ar_par['verbale'];
		$this->ar_sql['RIFG02'] = $ar_par['scheda'];
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_fattura']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_cancella_articoli($ar_par){
	    $this->ar_sql['RIRGES'] = 'DEL_RIGA_VEN';
	    $this->ar_sql['RIDTEP'] = $ar_par['nrec'];
	    $this->ar_sql['RIART'] = $ar_par['codice'];
	    $this->ar_sql['RIDART'] = $ar_par['articolo'];
	   
	    
	    $this->imposta_campi_base();
	    $this->imposta_campi_ordine($ar_par['k_ordine']);
	    $sql = $this->get_insert_sql();
	    $this->scrivi_messaggio($sql);
	}
	
	private function crea_history_aggiungi_articoli($ar_par){
		$this->ar_sql['RIRGES'] = 'INS_RIGA_VEN';
		$this->ar_sql['RIART'] = $ar_par['codice'];
		$this->ar_sql['RIQTA'] = $ar_par['quant'];
		$this->ar_sql['RIDART'] = $ar_par['articolo'];
		$this->ar_sql['RIIMPO'] = $ar_par['prezzo'];
		$this->ar_sql['RIAZDV'] = $ar_par['config_par']; //recupera progressivo
		$this->ar_sql['RINOTE'] = $ar_par['idsc'];
	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
		
		//Ritorno riga (potrei avere nelle note le info di non inserimento
		global $conn;
		$ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		return $r;
		
	}
	
	
	private function crea_history_modifica_articoli($ar_par){
		$this->ar_sql['RIRGES'] = 'MOD_RIGA_VEN';
		$this->ar_sql['RIART'] = $ar_par['codice'];
		$this->ar_sql['RIQTA'] = $ar_par['quant'];
		$this->ar_sql['RIDART'] = $ar_par['articolo'];
		$this->ar_sql['RIIMPO'] = $ar_par['prezzo'];
		$this->ar_sql['RIDTEP'] = $ar_par['nrec'];
		$this->ar_sql['RIDTVA'] = $ar_par['riga'];
		$this->ar_sql['RITPCA'] = $ar_par['stato'];
	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_salva_stato($ar_par){
		$this->ar_sql['RIRGES'] = 'STA_ORD_MTO';
		$this->ar_sql['RISTNW'] = $ar_par['stato'];
		$this->ar_sql['RICLIE'] = $ar_par['RDPMTO'];
	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_ripristino_fornitura($ar_par){
	    $this->ar_sql['RIRGES'] = 'ANN_ST_MAG';
	    $this->ar_sql['RICLIE'] = $ar_par['RDPMTO'];
	    
	    $this->imposta_campi_base();
	    $this->imposta_campi_ordine($ar_par['k_ordine']);
	    $sql = $this->get_insert_sql('Y');
	    $this->scrivi_messaggio($sql);
	}

	private function crea_history_riapplica_listino($ar_par){
	    $this->ar_sql['RIRGES'] = 'ASS_LIST';	
		$this->ar_sql['RITPCA'] = $ar_par['listino'];
		$this->ar_sql['RIAZCA'] = $ar_par['sconti'];
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_aggiorna_listino($ar_par){  
		$this->ar_sql['RIRGES'] = 'ASS_SCO_RIG';
		if (isset($ar_par['sc1']))
			$this->ar_sql['RISC1']   = (float)$ar_par['sc1'];
		if (isset($ar_par['sc2']))
			$this->ar_sql['RISC2']   = (float)$ar_par['sc2'];
		if (isset($ar_par['sc3']))
			$this->ar_sql['RISC3']   = (float)$ar_par['sc3'];
		if (isset($ar_par['sc4']))
			$this->ar_sql['RISC4']   = (float)$ar_par['sc4'];
		if (isset($ar_par['magg']))
			$this->ar_sql['RIMAGG']   = (float)$ar_par['magg'];
	    if (isset($ar_par['elab1']))
		    $this->ar_sql['RIFG01']   = $ar_par['elab1'];
	    if (isset($ar_par['elab2']))
	        $this->ar_sql['RIFG02']   = $ar_par['elab2'];
        if (isset($ar_par['elab3']))
            $this->ar_sql['RIFG03']   = $ar_par['elab3'];
        if (isset($ar_par['elab4']))
            $this->ar_sql['RIFG04']   = $ar_par['elab4'];
        if (isset($ar_par['elab5']))
            $this->ar_sql['RIFG05']   = $ar_par['elab5'];
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
	}
	

	private function crea_history_mod_rif_ddt($ar_par){
		$this->ar_sql['RIRGES'] = 'MOD_RIF_DDT';
		$this->ar_sql['RIDTEP'] = (int)$ar_par['data_reg'];
		$this->ar_sql['RIDTRF'] = (int)$ar_par['data_rif'];
		$a_n = explode("-", $ar_par['num_rif']);
		if (count($a_n) == 2){
			$this->ar_sql['RIANRF'] = (int)$a_n[0];
			$this->ar_sql['RINRRF'] = (int)$a_n[1];
		}
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_ddt_rimuovi_da_fattura($ar_par){
	    $this->ar_sql['RIRGES'] = 'ANN_FAT_ACQM';  //ANN_FAT_ACQ
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$this->imposta_campi_base($ar_par);
		
		if(strlen($ar_par['k_ordine'])>0){
		    $this->imposta_campi_ordine($ar_par['k_ordine']);
		}
		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}

	private function crea_history_mod_importo_spese_fatt($ar_par){
		$this->ar_sql['RIRGES'] = 'MOD_IMP_FAT';
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->ar_sql['RIIMPO'] = $ar_par['importo'];
		$this->ar_sql['RICITI'] = $ar_par['tipologia'];
		$this->ar_sql['RIDTRF'] = (int)$ar_par['data_rif'];
		$this->ar_sql['RIVETT'] = $ar_par['ass_iva'];
		$this->ar_sql['RIDTEP'] = (int)$ar_par['ns_data'];
		$this->ar_sql['RIDART'] = $ar_par['ns_rif'];
		$this->ar_sql['RIIVRI'] = $ar_par['assog_spese'];
		
		//deve arrivare anno-numero
		$a_n = explode("-", $ar_par['num_rif']);
		if (count($a_n) == 2){
			$this->ar_sql['RIANRF'] = (int)$a_n[0];
			$this->ar_sql['RINRRF'] = (int)$a_n[1];
		}
			
		
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_mod_pagam_fatt($ar_par){
		$this->ar_sql['RIRGES'] = 'MOD_PAG_FAT';
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->ar_sql['RICITI'] = $ar_par['pagam'];
		$this->ar_sql['RIIVRI'] = $ar_par['rata_iva'];
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_cambia_stato($ar_par){
		$this->ar_sql['RIRGES'] = 'ASS_STDOC';
		$this->ar_sql['RISTNW'] = $ar_par['stato'];
		if($ar_par['forza'] == 'F')
		  $this->ar_sql['RIFG01'] = $ar_par['forza'];
		
		if (isset($ar_par['data_prevista']))
		   $this->ar_sql['RIDTEP']   = (int)$ar_par['data_prevista'];
			
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
		
		global $conn;
		//Ritorno RI generato SOLO IN BASE A RITIME
		$ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		return $r;
	}
	
	
	private function crea_history_conferma_fornitura($ar_par){
		$this->ar_sql['RIRGES'] = 'GEST_ORD_FORN';
		$this->ar_sql['RICLIE'] = $ar_par['num_prog'];
		$this->ar_sql['RIART'] = $ar_par['articolo'];
		$this->ar_sql['RIDEST'] = $ar_par['tipo_op'];
		$this->ar_sql['RIDT'] = $ar_par['ditta'];
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
	
		$this->imposta_campi_base($ar_par);
		
		if(strlen($ar_par['k_ordine'])>0){
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		}
		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	

	
	private function crea_history_conferma_ord_reso($ar_par){
		$this->ar_sql['RIRGES'] = 'GEST_ORD_RESO';
		$this->ar_sql['RIPROG'] = $ar_par['num_prog'];
		$this->ar_sql['RIDTVA'] = (int)$ar_par['riga_comp'];
		$this->ar_sql['RIDEST'] = $ar_par['tipo_op'];
		$this->ar_sql['RIDART'] = $ar_par['commento'];
		$this->ar_sql['RINRCA'] = (int)$ar_par['nrec_orig'];
		$this->ar_sql['RITPCA'] = trim($ar_par['causale_nc']);
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
	
		$this->imposta_campi_base($ar_par);
	
		if(strlen($ar_par['k_ordine'])>0){
			$this->imposta_campi_ordine($ar_par['k_ordine']);
		}
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	
	private function crea_history_blocca_ordine($ar_par){
		$this->ar_sql['RIRGES'] = 'BLC_ORD';	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->ar_sql['RINOTE'] = $ar_par['f_note'];
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_ril_entry($ar_par){
		$this->ar_sql['RIRGES'] = 'RIL_ENTRY';
		$this->ar_sql['RIIDPA'] = $ar_par['prog'];
	    $this->ar_sql['RINOTR'] = $ar_par['op'] . ',' . trim($ar_par['ex']);
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}

	
	//rivacold
	private function crea_history_pers($ar_par){
	    global $conn;
		$this->ar_sql = $ar_par['vals'];
		$this->ar_sql['RIRGES'] = $ar_par['messaggio'];
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->imposta_campi_carico($ar_par['k_carico']);
		if (isset($ar_par['end_session_history']))
			$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$sql = $this->get_insert_sql('Y');
		$this->scrivi_messaggio($sql);
		
		//Ritorno RI generato SOLO IN BASE A RITIME
        		/* Vecchia versione ( (2018-11-23) )
        		$ar_sql_s = $this->ar_sql;
        		unset($ar_sql_s['RIESIT']);
        		*/
		$ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		//ToDO: generalizzare
		if ($r)
		    $r['RINOTE'] = utf8_encode($r['RINOTE']);
		return $r;
	}

	
	
	private function crea_history_ril_entry_PERS($ar_par){
		$this->ar_sql['RIRGES'] = $ar_par['messaggio'];
		$this->ar_sql['RIIDPA'] = $ar_par['prog'];
		$this->ar_sql['RINOTR'] = $ar_par['op'] . ',' . trim($ar_par['ex']);
		
		if (!isset($ar_par['RIAUTO'])) $ar_par['RIAUTO'] = '';
		if (strlen($ar_par['RIAUTO']) > 10)
		  $this->ar_sql['RIAUTO'] = substr($ar_par['RIAUTO'], 0, 10);
		else
		  $this->ar_sql['RIAUTO'] = $ar_par['RIAUTO'];
		
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_stampa_doc_ordine($ar_par){
		$this->ar_sql['RIRGES'] = 'DOC_ORDASS';
		$this->ar_sql['RIIDPA'] = $ar_par['prog'];
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->imposta_campi_base($ar_par);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_forza_invio_conferma($ar_par){
		$this->ar_sql['RIRGES'] = 'CONFOR';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	
	
	
	private function crea_history_forza_chiusura_ordine($ar_par){
		$this->ar_sql['RIRGES'] = 'FORZAC';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->ar_sql['RIART'] 	= $ar_par['k_articolo'];
		$this->ar_sql['RIDTVA'] = $ar_par['da_data'];				
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	

	private function crea_history_sincronizza_spedizione($ar_par){
		$this->ar_sql['RIRGES'] = 'REFRESH';
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_sincronizza_ordine($ar_par){
		$this->ar_sql['RIRGES'] = 'REFRESH';
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_dora_gen_ord_mto($ar_par){
		$this->ar_sql['RIRGES'] = 'GEN_ORD_MTO';
		$this->imposta_campi_base();
		$this->ar_sql['RIDT'] 	= $ar_par['ditta'];		
		$this->ar_sql['RICVES'] = $ar_par['fornitore'];
		$this->ar_sql['RIDTEP'] = $ar_par['data_cons'];
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}

	private function crea_history_dora_gen_ord_mpr($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'GEN_ORD_MPR';
		$this->imposta_campi_base();
		$this->ar_sql['RIDT'] 	= $ar_par['ditta'];
		$this->ar_sql['RICVES'] = $ar_par['fornitore'];
		$this->ar_sql['RIDTEP'] = $ar_par['data_cons'];
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	
	
	
	private function crea_history_dora_gen_ord_mts($ar_par){ //promo
		if(strlen($ar_par['msg']) > 0)
			$this->ar_sql['RIRGES'] = $ar_par['msg'];
		else
			$this->ar_sql['RIRGES'] = 'GEN_ORD_MTS';
		$this->imposta_campi_base();
		$this->ar_sql['RIDT'] 	= $ar_par['ditta'];
		$this->ar_sql['RICVES'] = $ar_par['fornitore'];
		$this->ar_sql['RITPCA'] = $ar_par['tipologia'];
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	private function crea_history_genera_fatt_acc($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'GEN_FAT_ACC';  
		$this->ar_sql['RIDTEP'] = (int)$ar_par['data'];
		$this->ar_sql['RIIVES'] = $ar_par['flag_parz'];
		$this->ar_sql['RIFG02'] = $ar_par['fatt_corr'];
		$this->ar_sql['RICITI'] = $ar_par['tipo_paga'];
		$this->ar_sql['RICVES'] = $ar_par['trasp'];
		$this->ar_sql['RIIMPO'] = (int)$ar_par['imp_trasp_cli'];
		$this->ar_sql['RIQTA']  = (int)$ar_par['imp_trasp_ns'];
		$this->ar_sql['RIDEST'] = $ar_par['causale'];
		$this->ar_sql['RIDART'] = $ar_par['text_nota'];
		$this->ar_sql['RIART']  = $ar_par['nota'];
		$this->ar_sql['RINRCA'] = (int)$ar_par['nrec'];
		$this->ar_sql['RIVETT'] = $ar_par['tipologia'];  
		$this->ar_sql['RICAGE'] = $ar_par['a_mezzo'];
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];		
		
	    $this->imposta_campi_base($ar_par);
	    $this->imposta_campi_ordine($ar_par['k_ordine']);
		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
		
		//Ritorno RI generato SOLO IN BASE A RITIME
		global $conn;
		$ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		return $r;	
	}
	
	private function crea_history_genera_nota_credito($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'GEN_ACC_ATT';
		$this->ar_sql['RIDTEP'] = (int)$ar_par['data_reg'];
		$this->ar_sql['RIDTVA'] = (int)$ar_par['data_evas'];
		$this->ar_sql['RIIMPO'] = (float)$ar_par['importo'];

		if(strlen($ar_par['end_session_history'])>0){
			$this->ar_sql['RIDART'] = $ar_par['rif'];
		}else{
			$this->ar_sql['RIDART'] = $ar_par['nota'];
		}
		
		$this->ar_sql['RIART']  = $ar_par['ast'];

		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	    //Ritorno RI generato SOLO IN BASE A RITIME
		global $conn;
		$ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
	    
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		//ToDO: generalizzare
		if ($r)
		    $r['RINOTE'] = utf8_encode($r['RINOTE']);
		return $r;
	   
	}
	
	private function crea_history_genera_fattura($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'GEN_FAT_ACQ';   //'GEN_FAT_ACQ';
	    $this->ar_sql['RIDTRF'] = (int)$ar_par['data_rif'];
	    $this->ar_sql['RIDTEP'] = (int)$ar_par['nostra_data'];
		
		//deve arrivare anno-numero
		$a_n = explode("-", $ar_par['num_rif']);
		if (count($a_n) == 2){
			$this->ar_sql['RIANRF'] = (int)$a_n[0];
			$this->ar_sql['RINRRF'] = (int)$a_n[1];
		}
		
		
		$this->ar_sql['RIDT'] = $ar_par['ditta'];
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$this->imposta_campi_base($ar_par);
		
		if(strlen($ar_par['k_ordine_bolla'])>0){
			$this->imposta_campi_ordine($ar_par['k_ordine_bolla']);
		}
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
		
		global $conn;
		$ar_sql_s = $this->ar_sql;
		unset($ar_sql_s['RIESIT']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		return $r;
		
	}
	
	private function crea_history_genera_ddt($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'GEN_DDT_ACQ'; 
		$this->ar_sql['RICLIE'] = $ar_par['k_prog'];
		$this->ar_sql['RIDTEP'] = (int)$ar_par['data_rif'];
		$this->ar_sql['RINRDO'] = (int)$ar_par['num_rif'];
		$this->ar_sql['RIDT'] = $ar_par['ditta'];
		if(strlen($ar_par['articolo']) > 0)
		    $this->ar_sql['RIART'] = $ar_par['articolo'];
	    if(strlen($ar_par['rddes2']) > 0)
	        $this->ar_sql['RIDART'] = $ar_par['rddes2'];
		if(strlen($ar_par['data_reg']) > 0){
		    $this->ar_sql['RIDTVA'] = (int)$ar_par['data_reg'];
		}
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$this->imposta_campi_base($ar_par);
	
		if(strlen($ar_par['k_ordine'])>0){
			$this->imposta_campi_ordine($ar_par['k_ordine']);
		}
		
		$sql = $this->get_insert_sql('Y');
		
		$this->scrivi_messaggio($sql);
		
	
	}
	
	private function crea_history_abbina_fattura($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'ABB_FAT_ACQ';   //'GEN_FAT_ACQ';
		$this->ar_sql['RIDTRF'] = (int)$ar_par['data_rif'];
		$this->ar_sql['RINRRF'] = (int)$ar_par['num_rif'];
		$this->ar_sql['RIDT']   = $ar_par['ditta'];
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$this->imposta_campi_base($ar_par);
	
		if(strlen($ar_par['k_ordine_bolla'])>0){
			$this->imposta_campi_ordine($ar_par['k_ordine_bolla']);
		}
		
		if(strlen($ar_par['k_ordine_fattura'])>0){
			$this->imposta_campi_ordine($ar_par['k_ordine_fattura']);
		}
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	
	}
	
	private function crea_history_attiva_spunta($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'ATT_CONTROLLATO';  
	    $this->imposta_campi_base($ar_par);
	    $this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);	
	}
	
	private function crea_history_annulla_spunta($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'ANN_CONTROLLATO';
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);	
	}
	
	
	private function crea_history_attiva_anomalia($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'ATT_ANOMALIA';
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	
	}
	
	private function crea_history_annulla_anomalia($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'ANN_ANOMALIA';
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	
	}
	
	
	private function crea_history_modifica_ord_mto($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'MOD_ORD_MTO';
		$this->imposta_campi_base($ar_par);
		if (isset($ar_par['prezzo']))
			$this->ar_sql['RIIMPO']   = $ar_par['prezzo'];
		if (isset($ar_par['nrec']))
			$this->ar_sql['RINRCA']  = $ar_par['nrec'];
		if (isset($ar_par['check']))
		    $this->ar_sql['RIFG01']  = $ar_par['check'];
		$this->ar_sql['RINOTR']  = $ar_par['end_session_history'];
		if(strlen($ar_par['k_ordine'])>0)
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	
	}
	
	private function crea_history_modifica_ord_mag_mto($ar_par){ //promo
		$this->ar_sql['RIRGES'] = 'ASS_PRZ_MTO';
		$this->imposta_campi_base($ar_par);
		if (isset($ar_par['prezzo']))
			$this->ar_sql['RIIMPO']   = $ar_par['prezzo'];
		if (isset($ar_par['nrec']))
			$this->ar_sql['RINRCA']  = $ar_par['nrec'];
		$this->ar_sql['RINOTR']  = $ar_par['end_session_history'];
		if(strlen($ar_par['k_ordine'])>0)
			$this->imposta_campi_ordine($ar_par['k_ordine']);
			$sql = $this->get_insert_sql();
			$this->scrivi_messaggio($sql);
	
	}

	
	private function crea_history_modifica_spedizione($ar_par){
		global $id_ditta_default;
		$this->ar_sql['RIRGES'] = 'UPD_SPED';
		$this->imposta_campi_base();

		if (isset($ar_par['ditta']))
			$ditta = $ar_par['ditta'];
		else
			$ditta = $id_ditta_default;
		
		$this->ar_sql['RIDT']   = $ditta;		
		$this->ar_sql['RINSPE'] = $ar_par['sped_id'];		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
		
	}
	
	private function crea_history_assegna_spedizione($ar_par){		
		$this->ar_sql['RIRGES'] = 'ASS_SPED';
		$this->imposta_campi_base($ar_par);
		
		$this->imposta_campi_ordine($ar_par['k_ordine']);

		if (isset($ar_par['k_carico']) && strlen($ar_par['k_carico']) > 0)
			$this->imposta_campi_carico($ar_par['k_carico']);
		
		if (isset($ar_par['seca']) && strlen($ar_par['seca']) > 0)		
			$this->ar_sql['RISECA'] = $ar_par['seca'];		
		
		if (isset($ar_par['nuovo_stato_ordine']) && strlen($ar_par['nuovo_stato_ordine']) > 0)
			$this->ar_sql['RISTNW'] = $ar_par['nuovo_stato_ordine'];		
		
		$this->ar_sql['RINSPE'] = $ar_par['sped']['CSPROG'];		
		$this->ar_sql['RIDTEP'] = $ar_par['sped']['CSDTSP'];				
		$this->ar_sql['RIVETT'] = $ar_par['sped']['CSCVET'];
		$this->ar_sql['RIAUTO'] = $ar_par['sped']['CSCAUT'];		
		$this->ar_sql['RICOSC'] = $ar_par['sped']['CSCCON'];		
		$this->ar_sql['RICITI'] = $ar_par['sped']['CSCITI'];

		$this->ar_sql['RIIVES'] = $ar_par['RIIVES'];
		$this->ar_sql['RICVES'] = $ar_par['RICVES'];		
		
		$this->ar_sql['RIFG01'] = $ar_par['RIFG01'];		
		
		$this->ar_sql['RINOTR'] = $ar_par['op'] . ' ' . trim($ar_par['ex']);		
		
		if ($ar_par['registra_data_confermata'] == 'Y')
			$this->imposta_campi_data_conf($ar_par['data_confermata']);
		
		$sql = $this->get_insert_sql();
		global $appLog;
		$appLog->add_bp("RI " . trim($this->ar_sql['RINRDO']));
		$this->scrivi_messaggio($sql);
		$appLog->add_bp('-RI');		
	}
	
	
	
	private function crea_history_assegna_indice_rottura($ar_par){
		$this->ar_sql['RIRGES'] = 'ASS_INRO';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);

		$this->ar_sql['RINOTR'] = $ar_par['op'] . ' ' . trim($ar_par['ex']);
	
		$sql = $this->get_insert_sql();
		global $appLog;
		$appLog->add_bp("RI " . trim($this->ar_sql['RINRDO']));
		$this->scrivi_messaggio($sql);
		$appLog->add_bp('-RI');
	}
	
	
        private function crea_history_END_assegna_indice_rottura($ar_par){
		$this->ar_sql['RIRGES'] = 'END_INRO';
		$this->imposta_campi_base();
		$this->ar_sql['RINOTR'] = $ar_par['op'] . ' ' . trim($ar_par['ex']);
		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
        }


	
	private function crea_history_provvigioni_aggiorna_perc_importo_agente($ar_par){
		$this->ar_sql['RIRGES'] = 'AGG_PRAG';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['RICLIE'] = $ar_par['k_cliente'];
		$this->ar_sql['RICAGE'] = $ar_par['k_agente'];
		$this->ar_sql['RIPPAN'] = $ar_par['newPercProvv'];
		$this->ar_sql['RIPPAO'] = $ar_par['oldPercProvv'];
		$this->ar_sql['RIIMPO'] = $ar_par['newImpoProvv'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_provvigioni_azzera_perc_importo_agente($ar_par){
		$this->ar_sql['RIRGES'] = 'AZZ_PRAG';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['RICLIE'] = $ar_par['k_cliente'];
		$this->ar_sql['RICAGE'] = $ar_par['k_agente'];
		$this->ar_sql['RIPPAN'] = $ar_par['newPercProvv'];
		$this->ar_sql['RIPPAO'] = $ar_par['oldPercProvv'];
		$this->ar_sql['RIIMPO'] = $ar_par['newImpoProvv'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	
	private function crea_history_provvigioni_aggiorna_agente($ar_par){
		$this->ar_sql['RIRGES'] = 'AGG_AGEN';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['RICLIE'] = $ar_par['k_cliente'];
		$this->ar_sql['RICAGE'] = $ar_par['k_agente'];
		$this->ar_sql['RIVETT'] = $ar_par['oldAgente'];

	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	
	private function crea_history_provvigioni_nuova_perc_importo_agente($ar_par){
		$this->ar_sql['RIRGES'] = 'INS_PRAG';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['RICLIE'] = $ar_par['k_cliente'];
		$this->ar_sql['RICAGE'] = $ar_par['k_agente'];
		$this->ar_sql['RIPPAN'] = $ar_par['newPercProvv'];
		$this->ar_sql['RIIMPO'] = $ar_par['newImpoProvv'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	

	
	private function crea_history_provvigioni_aggiorna_perc_provv_su_riga($ar_par){
		$this->ar_sql['RIRGES'] = 'AGG_PRAG_RIGADOC';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		
		$this->ar_sql['RINRCA'] = $ar_par['nrec'];
		$this->ar_sql['RIPPAN'] = $ar_par['newPercProvv'];
		$this->ar_sql['RIPPAO'] = $ar_par['oldPercProvv'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	private function crea_history_provvigioni_aggiorna_perc_provv_azzera_su_riga($ar_par){
		$this->ar_sql['RIRGES'] = 'AZZ_PRAG_RIGADOC';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['RINRCA'] = $ar_par['nrec'];
		$this->ar_sql['RIPPAN'] = 0;
		$this->ar_sql['RIPPAO'] = $ar_par['oldPercProvv'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	private function crea_history_provvigioni_inserisci_maggiorazione_diminuzione_per_agente($ar_par){
		$this->ar_sql['RIRGES'] = 'INS_IMP_AGE';
		$this->imposta_campi_base();
		
		$this->ar_sql['RICLIE'] = $ar_par['k_agenzia'];
		$this->ar_sql['RICAGE'] = $ar_par['k_agente'];
		$this->ar_sql['RIDTEP'] = $ar_par['data'];
		$this->ar_sql['RIPPAN'] = $ar_par['perc'];
		$this->ar_sql['RIIMPO'] = $ar_par['importo'];
		$this->ar_sql['RINOTE'] = $ar_par['note'];
		$this->ar_sql['RITPCA'] = $ar_par['causale'];
		
		$this->ar_sql['RIDT'] 	= $ar_par['ditta'];
		$this->ar_sql['RIAADO'] = $ar_par['anno'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	
	
	

	private function crea_history_assegna_spedizione_desk_acq($ar_par){
		$this->ar_sql['RIRGES'] = 'ASS_CONS';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->ar_sql['RIART'] 	= $ar_par['k_articolo'];		
	
		$this->ar_sql['RINSPE'] = $ar_par['sped']['CSPROG'];
		$this->ar_sql['RIDTEP'] = $ar_par['sped']['CSDTSP']; //nuova data programmata
		$this->ar_sql['RIDTVA'] = $ar_par['da_data'];
	
		$this->ar_sql['RINOTR'] = $ar_par['op'] . ' ' . trim($ar_par['ex']);
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	
	
	
	private function crea_history_upd_data_confermata($ar_par){		
		$this->ar_sql['RIRGES'] = 'CONF_DATA';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);

		$this->imposta_campi_data_conf($ar_par['data_confermata']);
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	
	
	
	private function crea_history_variazione_destinazione($ar_par){	
		$this->ar_sql['RIRGES'] = 'VAR_DEST';
		$this->imposta_campi_base();
		$this->ar_sql['RIDT'] 	= $ar_par['p']['dt'];
		$this->ar_sql['RICLIE']	= $ar_par['p']['cod_cli'];			
		$this->ar_sql['RIDEST']	= $ar_par['p']['cod_des'];		
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	
	

	private function crea_history_variazione_destinazione_sosta_tecnica($ar_par){
		$this->ar_sql['RIRGES'] = 'VAR_DESTS';
		$this->imposta_campi_base();
		$this->ar_sql['RIDT'] 	= $ar_par['p']['dt'];
		$this->ar_sql['RICLIE']	= $ar_par['p']['cod_cli'];
		$this->ar_sql['RIDEST']	= $ar_par['p']['cod_des'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	private function crea_history_assegna_seq_carico($ar_par){	
		$this->ar_sql['RIRGES'] = 'ASS_SEQ';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->ar_sql['RISECA'] = $ar_par['seq'];
		
		if (isset($ar_par['k_carico']) && strlen($ar_par['k_carico']) > 0)
			$this->imposta_campi_carico($ar_par['k_carico']);
		
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	

	
	private function crea_history_assegna_scarico($ar_par){
		$this->ar_sql['RIRGES'] = 'ASS_SCAR';	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		
		$this->ar_sql['RIIVES'] = $ar_par['RIIVES'];		
		$this->ar_sql['RICVES'] = $ar_par['RICVES'];
		$this->ar_sql['RIDEST'] = $ar_par['RIDEST'];
		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	
	
	
	private function crea_history_assegna_destinazione_finale($ar_par){
		$this->ar_sql['RIRGES'] = 'ASS_DEST';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['RIDEST'] = $ar_par['cod_dest'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}

	
	private function crea_history_assegna_destinazione_sosta_tecnica($ar_par){
		$this->ar_sql['RIRGES'] = 'ASS_SOST';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['RIDEST'] = $ar_par['cod_dest'];
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	

	
	private function crea_history_assegna_carico($ar_par){
		$this->ar_sql['RIRGES'] = 'ASS_CAR';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$this->imposta_campi_carico($ar_par['k_carico']);
		$this->ar_sql['RISECA'] 	= $ar_par['seca'];			
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}

	
	private function crea_history_upd_commento_ordine($ar_par){
		$this->ar_sql['RIRGES'] = 'UPD_COM';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}	
	
	private function crea_history_registra_anticipo($ar_par){
	    global $conn;
		$this->ar_sql['RIRGES'] = 'GEN_FAT_ANT';
		$this->ar_sql['RIDTEP'] = (int)$ar_par['data'];
		$this->ar_sql['RIIMPO'] = (float)$ar_par['importo'];
		$this->ar_sql['RICITI'] = $ar_par['tipo_paga'];
		$this->ar_sql['RIIVES'] = $ar_par['flag_saldo'];
		$this->ar_sql['RIDART'] = $ar_par['text_nota'];
		$this->ar_sql['RIART']  = $ar_par['nota'];
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
		
		//Ritorno RI generato SOLO IN BASE A RITIME
		$ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		
		//ToDO: generalizzare
		if ($r)
		    $r['RINOTE'] = utf8_encode($r['RINOTE']);
		
		return $r;
	}
	
	private function crea_history_restituzione_caparra($ar_par){
	    global $conn;
	    $this->ar_sql['RIRGES'] = 'GEN_REST_CAP';
	    $this->ar_sql['RIDTEP'] = (int)$ar_par['data'];
	    $this->ar_sql['RIIMPO'] = (float)$ar_par['importo'];
	    $this->ar_sql['RICITI'] = $ar_par['tipo_paga'];
	    $this->ar_sql['RIIVES'] = '';
	    $this->ar_sql['RIDART'] = $ar_par['text_nota'];
	    $this->ar_sql['RIART']  = $ar_par['nota'];
	    $this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
	    
	    $this->imposta_campi_base($ar_par);
	    $this->imposta_campi_ordine($ar_par['k_ordine']);
	    
	    $sql = $this->get_insert_sql();
	    $this->scrivi_messaggio($sql);
	    
	    //Ritorno RI generato SOLO IN BASE A RITIME
	    $ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
	    $sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, $ar_sql_s);
	    echo db2_stmt_errormsg($stmt);
	    $r = db2_fetch_assoc($stmt);
	    
	    //ToDO: generalizzare
	    if ($r)
	        $r['RINOTE'] = utf8_encode($r['RINOTE']);
	        
	        return $r;
	}
	

	private function crea_history_duplica_preventivo($ar_par){
		$this->ar_sql['RIRGES'] = 'PREV_TO_PREV';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
		
		//ritorno preventivo generato XXX
		global $conn;
		$ar_sql_s = $this->ar_sql;
		unset($ar_sql_s['RIESIT']); 
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		return $r['RIAUTO'];
	}
	
	private function crea_history_preventivo_to_ordine($ar_par){
		$this->ar_sql['RIRGES'] = 'PREV_TO_ORD';
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
		
		//ritorno preventivo generato XXX
		global $conn;
		$ar_sql_s = $this->ar_sql;
		unset($ar_sql_s['RIESIT']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		return $r['RIAUTO'];
		
	}
	
	private function crea_history_crea_ordine_assistenza($ar_par){
		$this->ar_sql['RIRGES'] = 'GEN_ORD_ASS';
		$this->ar_sql['RINRCA'] = (int)$ar_par['nrec'];
		$this->ar_sql['RICITI'] = $ar_par['causale'];
		$this->ar_sql['RIVETT'] = $ar_par['prior'];
		if(trim($ar_par['articolo']) != "")
		    $this->ar_sql['RIART'] = $ar_par['articolo'];
	    if($ar_par['quant'] > 0)
	        $this->ar_sql['RIQTA'] = $ar_par['quant'];
       if($ar_par['prezzo'] > 0)
            $this->ar_sql['RIIMPO'] = $ar_par['prezzo'];
		$this->ar_sql['RINOTR'] = $ar_par['end_session_history'];
		$this->imposta_campi_base($ar_par);
		$this->imposta_campi_ordine($ar_par['k_ordine']);
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	
		//ritorno preventivo generato
		global $conn;
		$ar_sql_s = array('RITIME' => $this->ar_sql['RITIME']);
		$sql = "SELECT * FROM {$this->cfg_mod['file_richieste']} WHERE " . create_name_field_by_ar_SELECT($ar_sql_s);
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql_s);
		echo db2_stmt_errormsg($stmt);
		$r = db2_fetch_assoc($stmt);
		return $r;
	}
	
	
	
	
	
	
	private function scrivi_messaggio($sql){
		global $conn;

		global $appLog;		
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		if (!$stmt){
			$al = new AppLog();
			$al->write_log("prepare " . $sql);
		}
		
		$result = db2_execute($stmt);
		echo db2_stmt_errormsg($stmt);	
		
		if (!$result){		
			$al = new AppLog();
			$al->write_log("execute" . $sql);
		}
		
		

		//se richiesto lancio il programma (dove non e' stato possibile installare il trigger)
		if (isset($this->cfg_mod['call_odbc_after_history']) && strlen($this->cfg_mod['call_odbc_after_history'])>0){
			$appLog->add_bp("/call");
			$qry1	 =	"{$this->cfg_mod['call_odbc_after_history']}"; //es: CALL SV2EXE.UR21H1C
			$stmt1   = 	odbc_prepare($conn, $qry1);
			$result1 = 	odbc_execute($stmt1);				
		}
		
	}
	
	
	
	
	
	private function imposta_campi_base($ar_par = array()){
		global $auth, $id_ditta_default;
		
		//verifico se esiste la configurazione personalizzata per questo messaggio
		$conf_msg = $this->get_configurazione_messaggio($this->mod->get_cod_mod(), $this->ar_sql['RIRGES']);

		if (!$conf_msg){
			//altrimenti prendo la configurazione di default del modulo
			$m = new Modules();
			$m->load_rec_data_by_k(array("TAKEY1" => $this->mod->get_cod_mod())); //TODO: parametrizzare in qualche maniera
			$this->ar_sql['RIESIT'] = $m->rec_data['TAFG01'];								
		} else {
			$this->ar_sql['RIESIT'] = $conf_msg['TAFG01'];			
		}

		$this->ar_sql['RIUSRI'] = $auth->get_user();
		$this->ar_sql['RIDTRI'] = oggi_AS_date();
		$this->ar_sql['RIHMRI'] = oggi_AS_time();
		
		if (isset($ar_par['RITIME']))
		    $this->ar_sql['RITIME'] = $ar_par['RITIME'];
		
		if (strlen($this->ar_sql['RIDT']) == 0)
			$this->ar_sql['RIDT']   = $id_ditta_default;
		
		if (strlen($ar_par['use_session_history']) > 0){
			$ar_par['use_session_history'] = sprintf("%-15s", $ar_par['use_session_history']);
			$this->ar_sql['RIRGES'] = implode("|", array($ar_par['use_session_history'], $this->ar_sql['RIRGES']));
			$this->ar_sql['RIESIT'] = 'S';
		}
	}
	
	
	private function get_configurazione_messaggio($modulo, $msg){
		global $conn, $cfg_mod_Spedizioni;
		
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} 
				WHERE TATAID = ? AND TAKEY1 = ? AND TAKEY2 = ?";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array('MMSG', $modulo, $msg));
		return db2_fetch_assoc($stmt);
	}
	
	
	private function get_insert_sql($disabled_trim = 'N'){
	    if (strlen($this->ar_sql['RITIME']) == 0)
		  $this->ar_sql['RITIME'] = microtime(true);		
		
		$sql = "INSERT INTO {$this->cfg_mod['file_richieste']} (" . $this->get_el_field_name() . ")
				VALUES (" . $this->get_el_field_values($disabled_trim) . ") 
				";
		
      
		
		return $sql;
	}
	
	private function get_el_field_name(){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $kf;

		return implode(', ', $ar);
	}
	
	private function get_el_field_values($disabled_trim = 'N'){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $v;
		
		if($disabled_trim == 'Y')
			return implode(', ', array_map('sql_t', $ar));
		else	
			return implode(', ', array_map('sql_t_trim', $ar));
	}	
	
	private function imposta_campi_ordine($k_ordine){
		if (strlen($k_ordine) == 0) return;		
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$oe = $s->k_ordine_td_decode($k_ordine);
		
		$this->ar_sql['RIDT'] 	= $oe['TDDT'];
		$this->ar_sql['RITIDO'] = $oe['TDOTID'];
		$this->ar_sql['RIINUM'] = $oe['TDOINU'];
		$this->ar_sql['RIAADO'] = $oe['TDOADO'];								
		$this->ar_sql['RINRDO'] = $oe['TDONDO'];
		
		global $cfg_mod_Spedizioni;
		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			$this->ar_sql['RIPROG'] = $oe['TDPROG'];
		}
		
						
	}
	
	
	
	private function imposta_campi_carico($k_carico){
		if (strlen($k_carico) == 0) return;
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$oe = $s->k_carico_td_decode($k_carico);
	
		if ($oe['TDNRCA'] > 0){
			$this->ar_sql['RITPCA'] = $oe['TDTPCA'];
			$this->ar_sql['RIAACA'] = $oe['TDAACA'];
			$this->ar_sql['RINRCA'] = $oe['TDNRCA'];
		} else {
			$this->ar_sql['RIAZCA'] = 'Y';
		}
	
	}	
	
	
	private function imposta_campi_data_conf($data_conf){
		if ($data_conf > 0){
			$this->ar_sql['RIDTVA'] = $data_conf;
		} else {
			$this->ar_sql['RIAZDV'] = 'Y';
		}
	
	}	
	
	
}

?>