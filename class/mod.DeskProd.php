<?php

/**********************************************
 * DESKTOP Programma di Produzione
 **********************************************/


class DeskProd {
	
	private $mod_cod = "DESK_PROD";
	private $mod_dir = "desk_prod";
	
	
	function __construct($parameters = array()) {
		global $auth;
		
		
		if (isset($parameters['no_verify']) && $parameters['no_verify'] == 'Y'){
		    return true;
		}	
	
		$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
		
		if (is_null($mod_permitted) || !$mod_permitted)
		    //se mi ha passato un secondo modulo testo i permessi
	        if (isset($parameters['abilita_su_modulo']))
	             $mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
		        
        if (is_null($mod_permitted) || !$mod_permitted)
            //se mi ha passato un secondo modulo testo i permessi
            if (isset($parameters['abilita_su_modulo2']))
                $mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo2']);
		                
		                
        if (is_null($mod_permitted) || !$mod_permitted){
            global $cod_mod_provenienza;
            
           
            //verifico permessi su cod_mod_provenienza
            if (isset($cod_mod_provenienza))
                $mod_permitted = $auth->verify_module_permission($cod_mod_provenienza);
        }
		                
        
        if (is_null($mod_permitted) || !$mod_permitted){
            die("\nNon hai i permessi ({$this->mod_cod})!!");
        }
	}	
	

	public function get_cod_mod(){
		return $this->mod_cod;
	}	
	
	public function get_cfg_mod(){
		global $cfg_mod_DeskProd;
		return $cfg_mod_DeskProd;
	}	
	

	public function get_mod_parameters(){
		$t = new Modules();
		$t->load_rec_data_by_k(array('TAKEY1' => $this->get_cod_mod()));
		return json_decode($t->rec_data['TAMAIL']);
	}	
	
	
	
	function next_num($cod){
	    global $conn, $id_ditta_default;
	    
	    $cfg_mod = $this->get_cfg_mod();
	    	    
	    $sql = "SELECT * FROM {$cfg_mod['file_numeratori']} WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    
	    if ($row = db2_fetch_assoc($stmt)){
	        $n = $row['NRPROG'] +1;
	        $sql = "UPDATE {$cfg_mod['file_numeratori']} SET NRPROG=$n WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	    } else {
	        $n = 1;
	        $sql = "INSERT INTO {$cfg_mod['file_numeratori']}(NRDT, NRCOD, NRPROG)  VALUES(" . sql_t($id_ditta_default) . ", '$cod', $n)";
	        
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	    }
	    
	    return $n;
	}
	
	public function fascetta_function(){
		global $auth, $main_module;
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	
		echo "<div class='fascetta-toolbar'>";
		echo "<img id='bt-page-refresh' src=" . img_path("icone/48x48/button_grey_repeat.png") . " height=30>";
		echo "<img id='bt-toolbar' src=" . img_path("icone/48x48/archive.png") . " height=30>";
		echo "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
		echo "<img id='bt-avanzamento_produzione' src=" . img_path("icone/48x48/fabbrica.png") . " height=30>";
		
		//indici/display kiosk
		echo "<img id='bt-indici' src=" . img_path("icone/48x48/bookmark.png") . " height=30>";
		echo "<img id='bt-kiosk'  src=" . img_path("icone/48x48/monitor.png") . " height=30>";
		
		//Ticket manutenzione macchine (calendario orari attivita + dashboard)
		echo "<img id='bt-ticket-cal'  src=" . img_path("icone/48x48/tools.png") . " height=30>";
		
		//Ticket manuntenzioni (MApp/Sud2)
		echo "<img id='bt-ticket-man'  src=" . img_path("icone/48x48/bluetooth_blue.png") . " height=30>";
		
		echo "<img id='add-promemoria' src=" . img_path("icone/48x48/clock.png") . " height=30>";
		echo "<img id='bt-call-pgm' src=" . img_path("icone/48x48/button_blue_play.png") . " height=30>";
		
		
		
		//orario aggiornamento dati
		echo "<div id='stato-aggiornamento'>[<img id='bt-stato-aggiornamento' class='st-aggiornamento' src=" . img_path("icone/48x48/rss_blue.png") . " height=20><span id='bt-stato-aggiornamento-esito'></span>]</div>";
		echo "</div>";
		
	
	}	
	
	
	public function get_stato_aggiornamento(){
	    global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
	    
	    $ret = array();
	    $ret['success'] = true;
	    
	    $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT=? AND TATAID=?";
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt, array($id_ditta_default, 'IELA'));
	    $row = db2_fetch_assoc($stmt);
	    
	    if ($row != false){
	        if ($row['TAFG01'] == 'F'){
	            $image_name = "icone/48x48/rss_blue.png";
	            $class_name = 'st-aggiornamento-ok';
	            $data_ora = print_date($row['TADTGE']) . " " . print_ora($row['TAORGE']);
	            $img_str = "";
	        } else {
	            $image_name = "under-construction_q_48.png";
	            $class_name = 'st-aggiornamento-warning';
	            $data_ora = print_ora($row['TAORGE']);
	            $img_str = "<img class='st-aggiornamento' src=" . img_path($image_name) . " height=20>";
	        }
	        
	        
	        $ret['html'] = "{$img_str}<span class='st-aggiornamento {$class_name}'>" . $data_ora . "</span>";
	    }
	    else $ret['html'] = '-';
	    
	    return $ret;
	}
	
	public function get_orari_aggiornamento(){
	    global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
	    
	    
	    $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT=? AND TATAID=?";
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt, array($id_ditta_default, 'IELA'));
	    $row = db2_fetch_assoc($stmt);
	    
	    if ($row != false){
	        return $row['TADESC'];
	    }
	    else return '-';
	}
	
	
	function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null, $order_by_seq = 'N', $mostra_codice = 'N'){
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	    return $s->find_TA_std($tab, $k1, $is_get, $esporta_esteso, $k2, $aspe, $gruppo, $order_by_seq, $mostra_codice);
	}
	
	function get_TA_std($tab, $k1, $k2 = null, $no_ditta = 'N'){
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	    return $s->get_TA_std($tab, $k1, $k2, $no_ditta);
	}
	    
		 	
} //class

?>