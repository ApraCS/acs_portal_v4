<?php

class DeskAcqItinerari extends Itinerari {
 

	// TABELLE UTILIZZZATE DALLA CLASSE
	public function get_table($per_select = false){
		global $cfg_mod_DeskAcq;
		$ret = $cfg_mod_DeskAcq[$this->table_id];
		if ($per_select == "Y")
			$ret .= " PRI " . $this->get_table_joins();
		return $ret;
	}
	
	public function get_table_NR(){
		global $cfg_mod_DeskAcq;
		return $cfg_mod_DeskAcq['file_numeratori'];
	}
	
	public function get_table_TA(){
		global $cfg_mod_DeskAcq;
		return $cfg_mod_DeskAcq['file_tabelle'];
	}	
	
}

?>