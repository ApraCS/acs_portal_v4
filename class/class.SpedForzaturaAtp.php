<?php

class SpedForzaturaAtp extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TADT,TATAID,TAKEY1,TAKEY2";	
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "FDATP";
	protected $tadt_f 		= "TADT";
	

	public function after_get_row($row){
		$s = new Spedizioni();
		//decodifico i vari valori
		$row['TAKEY2_D'] = $s->decod_std('ITIN', $row['TAKEY2']);
		return $row;
	}
	

	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-blog_add-16',
            frame: true,
            title: 'Forzatura data ATP',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
 			{name: 'k_TADT',   xtype: 'hidden'},
            {name: 'k_TATAID', xtype: 'hidden'}, 
            {name: 'k_TAKEY1', xtype: 'hidden'},
 		    {name: 'k_TAKEY2', xtype: 'hidden'},
 			{
                fieldLabel: 'Ditta',
                name: 'TADT',
                xtype: 'hidden'
            }, {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden'
            }, {
                fieldLabel: 'Data',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 8,
 				minLength: 8
            }, {
				name: 'TAKEY2',
				xtype: 'combo',
				fieldLabel: 'Itinerario',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
 				forceSelection: true,
			   	allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('ITIN', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
                fieldLabel: 'Nuova data',
                name: 'TARIF1',
                allowBlank: false,
 				maxLength: 8,
 				minLength: 8
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Data',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false,
				filterable: true
            }, {
                text: 'Itinerario',
                flex: 80,
                sortable: true,
                dataIndex: 'TAKEY2_D',
                allowBlank: false,
				filterable: true
            }, {
                text: 'Nuova data',
                width: 100,
                sortable: true,
                dataIndex: 'TARIF1',
                allowBlank: true,
				filterable: true
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'k_TAKEY2',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TADT',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TAKEY2',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, {
        name: 'TARIF1',        
        useNull: false
    }, {
        name: 'TARIF2',        
        useNull: false
    }, 'TAASPE', 'TAMAIL', 'TAINDI', 'TACAP', 'TALOCA', 'TAPROV', 'TANAZI', 'YAML_PARAMETERS', 'TAKEY2_D']
});
";

}	
	
	
	
}

?>