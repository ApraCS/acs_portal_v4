<?php

class AbstractDb {
	
	public $rec_data = array();
	
	
	public function find_all(){
		$rows = $this->get_stmt_rows();
		while ($row = db2_fetch_assoc($rows)){
		
			//preparo i campi per le chiavi	
		    foreach(explode(',', $this->table_k) as $key)
		        $row["k_{$key}"] = $row[$key];
			
			//se richiesto faccio delle operazioni per manipolare i dati
			$row = $this->after_get_row($row);
			$ret_ar["data"][] = array_map('trim',$row);
		}
	 return $ret_ar["data"];				 
	}
		
	
	public function load_rec_data_by_k($ar_field){
		global $conn;

		if ($this->taid_f != null)
			$ar_field[$this->taid_f] = $this->taid_k;
		
		//inizializzo ditta
		if (isset($this->tadt_f) && !isset($ar_field[$this->tadt_f])){
			global $id_ditta_default;
			$ar_field[$this->tadt_f] = $id_ditta_default;
		}		
		
		$object = json_decode(acs_je($ar_field), FALSE);		
	
		$sql = "SELECT * FROM " . $this->get_table("Y");
		$sql .= " WHERE " . $this->get_WHERE_KEY_sql_jd($object, $this->table_k, "N");
	
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		$row = db2_fetch_assoc($stmt);
		$this->rec_data = $row;				 
	}
	

	public function get_table_k(){
		return $this->table_k;
	}
	
	
	public function view_jd($p){		
		$ret_ar = array();
		$ret_ar["success"] = true;
		
		$rows = $this->get_stmt_rows($p);
		while ($row = db2_fetch_assoc($rows)){
		
			//preparo i campi per le chiavi	
		    foreach(explode(',', $this->table_k) as $key)
		        $row["k_{$key}"] = $row[$key];
			
			//se richiesto faccio delle operazioni per manipolare i dati
			$row = $this->after_get_row($row);
			
			if ($this->con_YAML_PARAMETERS == 'Y')
				$row['YAML_PARAMETERS'] = $this->get_YAML_PARAMETERS($row);
				
			
			$ret_ar["data"][] = array_map('trim_utf8',$row);
		}	
						
		echo acs_je($ret_ar);
	}
	

	public function after_get_row($row){
		return $row;
	}

	
	public function create_jd(){			
		global $conn;		
		$ar_field = acs_m_params_json_decode()->data;

		if ($this->taid_f != null)
			$ar_field->{$this->taid_f} = $this->taid_k;
		
		//filtri con extraParams in ingresso di tipo "ep_"
		foreach ($_REQUEST as $kv=>$v){
			if (substr($kv, 0, 3) == 'ep_'){
				$campo =  substr($kv, 3, 100);
				$ar_field->{$campo} = $v;
			}
		}		

		//inizializzo ditta		
		if (isset($this->tadt_f)){
			global $id_ditta_default;
			$ar_field->{$this->tadt_f} = $id_ditta_default;
		}			
			
		//se esiste creo il valore per la chiave autoincrementata
		if (isset($this->table_gen_f)){
			$ar_field->{$this->table_gen_f} = next_num('NRLT', $this->get_table_NR());
		}	

		//imposto a 0 gli eventuali campi
		if (isset($this->default_fields))
		foreach($this->default_fields as $kf=>$f){
			if (strlen($ar_field->{$kf}) == 0) $ar_field->{$kf} = $f;
		}
			
		//eseguo inserimento
		$sql = "INSERT INTO " . $this->get_table() . "(" . $this->get_INSERT_KEY_name($ar_field, $this->table_k) . ")";		
		$sql .= " VALUES (" . $this->get_INSERT_KEY_values($ar_field, $this->table_k) . ")";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		echo db2_stmt_errormsg($stmt);		
		
		//eseguo aggiornamento per aggiornare i dati passati
		$this->update_jd('Y', $ar_field);						
	}
		
	
	public function destroy_jd(){			
		global $conn;		
		$ar_field = acs_m_params_json_decode()->data;
		
			//filtri con extraParams in ingresso di tio "ep_"
			foreach ($_REQUEST as $kv=>$v){
				if (substr($kv, 0, 3) == 'ep_'){
					$campo =  substr($kv, 3, 100);
					$k_campo = "k_{$campo}";
					$ar_field->{$campo} = $v;
					$ar_field->{$k_campo} = $v;
				}
			}		
		
			//eseguo inserimento
			$sql = "DELETE FROM " . $this->get_table();		
			$sql .= " WHERE ". $this->get_WHERE_KEY_sql_jd($ar_field, $this->table_k);

			if ($this->con_YAML_PARAMETERS == 'Y')
				$this->gestione_YAML_PARAMETERS($ar_field, 'DELETE');			
			
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);						
	}	
	

	public function before_create_jd($ar_field){
		return $ar_field;
	}	
	public function before_update_jd($ar_field){
		return $ar_field;
	}
	public function before_update_jd_fix_date($ar_field){
		return $ar_field;
	}
	
	public function update_jd($from_insert = false, $ar_field = null){			
		global $conn;
		
 		if ($from_insert!='Y')					
			$ar_field = acs_m_params_json_decode()->data;

		if ($this->taid_f != null)
			$ar_field->{$this->taid_f} = $this->taid_k;

		//filtri con extraParams in ingresso di tio "ep_"
		foreach ($_REQUEST as $kv=>$v){
			if (substr($kv, 0, 3) == 'ep_'){
				$campo =  substr($kv, 3, 100);
				$k_campo = "k_{$campo}";
				$ar_field->{$campo} = $v;
				$ar_field->{$k_campo} = $v;				
			}
		}
		
 		if ($from_insert=='Y'){ 			
			//copio il valore delle chiavi
			$ar_k_chiave = explode(',', $this->table_k);
			
	    	foreach($ar_k_chiave as $key) {
	    		$ar_field->{to_field_k($key)} = $ar_field->$key;
			}
		}
				
		//imposto a 0 gli eventuali campi
		if (isset($this->default_fields))
		foreach($this->default_fields as $kf=>$f){
			if (strlen($ar_field->{$kf}) == 0) $ar_field->{$kf} = $f;
		}
		
		
		if ($from_insert=='Y')
			$ar_field = $this->before_create_jd($ar_field);
		else //da save: devo correggere le date
			$ar_field = $this->before_update_jd_fix_date($ar_field);
		
		$ar_field = $this->before_update_jd($ar_field);		
		
		$ret_ar = array();
		
		//eseguo aggiornamento
		$sql = "UPDATE " . $this->get_table();
		$sql .= $this->get_SET_sql_jd($ar_field);
		$sql .= " WHERE " . $this->get_WHERE_KEY_sql_jd($ar_field, $this->table_k);		
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();				
		$result = db2_execute($stmt);
		if (!$result) {
			
			print_r($sql);
			
	        $ret_ar["success"] = $result;
	        $ret_ar['message'] = db2_stmt_errormsg($stmt);
	        echo acs_je($ret_ar);
	    }
		
		//Se presenti aggiorno i parametri YAML_PARAMETERS
		if (isset($ar_field->YAML_PARAMETERS))
			$this->gestione_YAML_PARAMETERS($ar_field, 'UPDATE');
		
		$ret_ar["success"] = $result;				
		
		echo acs_je($ret_ar);
	}	
	

	public function get_YAML_PARAMETERS($ar_field){
		global $conn, $cfg_mod_Admin;
		$class_name = get_class($this);
		$record_k   = $this->get_KEY((object)$ar_field, $this->table_k);
		$sql_s = "SELECT DFMEMO FROM {$cfg_mod_Admin['file_memorizzazioni']} WHERE DFMODU = 'YAML_P' AND DFFUNC=? AND DFNAME=?";
		
		$stmt_s = db2_prepare($conn, $sql_s);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt_s, array($class_name, $record_k));
		echo db2_stmt_errormsg($stmt_s);

		$row = db2_fetch_assoc($stmt_s);
		if ($row == false) return null;

		$ret = trim($row['DFMEMO']);		
		return $ret;
	}
	
	public function parse_YAML_PARAMETERS($ar_field){
		$p_text = trim($this->get_YAML_PARAMETERS($ar_field));
		if ($p_text === null) return null;

		//se serve aggiungo le parentesi graffe (inizio - fine)
		if (substr($p_text, 0, 1) != '{')
			$p_text = '{' . $p_text . '}';		
		
		$ret = (array)json_decode($p_text);
		return $ret;
	}
	 
	
	
	protected function gestione_YAML_PARAMETERS($ar_field, $op){
		global $conn, $cfg_mod_Admin;
		$class_name = get_class($this);
		$record_k   = $this->get_KEY($ar_field, $this->table_k);
		
		switch ($op){
			case "CREATE":
			case "UPDATE":
				
				if (strlen(trim($ar_field->YAML_PARAMETERS)) == 0){
					$sql_s = "DELETE FROM {$cfg_mod_Admin['file_memorizzazioni']} WHERE DFMODU = 'YAML_P' AND DFFUNC=? AND DFNAME=?";
					$stmt_s = db2_prepare($conn, $sql_s);
					$result = db2_execute($stmt_s, array($class_name, $record_k));
					break;										
				}
				
				$sql_s = "SELECT DFID FROM {$cfg_mod_Admin['file_memorizzazioni']}
						WHERE DFMODU = 'YAML_P' AND DFFUNC=? AND DFNAME=?";
				$stmt_s = db2_prepare($conn, $sql_s);
				$result = db2_execute($stmt_s, array($class_name, $record_k));
				$row = db2_fetch_assoc($stmt_s);
				if ($row == false){
					$sql = "INSERT INTO {$cfg_mod_Admin['file_memorizzazioni']}(DFMODU, DFFUNC, DFNAME)
								VALUES('YAML_P', ?, ?)";
					$stmt = db2_prepare($conn, $sql);
					$result = db2_execute($stmt, array($class_name, $record_k));

					//recupero DFID creato
					$result = db2_execute($stmt_s, array($class_name, $record_k));
					$row = db2_fetch_assoc($stmt_s); 					
				}
				$dfid = $row['DFID'];
				
				$sql = "UPDATE {$cfg_mod_Admin['file_memorizzazioni']} SET DFMEMO = ? WHERE DFID = ?";
				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt, array($ar_field->YAML_PARAMETERS, $dfid));				
				
			break;
			case "DELETE":
				$sql_s = "DELETE FROM {$cfg_mod_Admin['file_memorizzazioni']} WHERE DFMODU = 'YAML_P' AND DFFUNC=? AND DFNAME=?";
				$stmt_s = db2_prepare($conn, $sql_s);
				$result = db2_execute($stmt_s, array($class_name, $record_k));
				break;							
		} //switch
		
	}


	public function implode_key($glue = "", $pieces = array())
	{
		$keys = array_keys($pieces);
		return implode($glue, $keys);
	}
	


	protected function get_stmt_rows($p = array()){
		global $conn;
		global $_REQUEST;
		global $id_ditta_default;
		
		$sql = "SELECT * FROM " . $this->get_table("Y");
		$sql .= " WHERE 1=1 ";
		
		if ($this->taid_f != null)		
		 $sql .= " AND " . $this->taid_WHERE();
		
		 if ($this->tadt_f != null)
		 	$sql .= " AND {$this->tadt_f} = '{$id_ditta_default}'";
		 

		//filtri con extraParams in ingresso di tio "ep_"
		foreach ($_REQUEST as $kv=>$v){
			if (substr($kv, 0, 3) == 'ep_'){
				$sql .= " AND " . substr($kv, 3, 100) . " = " . sql_t($v);
			}
		}
		

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		return $stmt;				
	}




	protected function taid_WHERE(){
		return " PRI.{$this->taid_f} = " . sql_t($this->taid_k);		
	}
	

	
	
	public function get_table($per_select = false){
		global $cfg_mod_Admin;
		$ret = $cfg_mod_Admin[$this->table_id];
		if ($per_select == "Y")
			$ret .= " PRI " . $this->get_table_joins();
		return $ret;
	}
	
	public function get_table_NR(){
		global $cfg_mod_Admin;
		return $cfg_mod_Admin['file_numeratori'];		
	}	
	
	public function get_table_TA(){
		global $cfg_mod_Admin;
		return $cfg_mod_Admin['file_tabelle'];
	}
	
	
	public function get_table_joins(){
		return "";
	}
		
	
	
	
	protected function get_SET_sql_jd($ar_field, $table_k = null, $upd_data_ora = 'Y'){
		$cols = array();
		if (is_null($table_k)) $table_k = $this->table_k;			
		$ar_k_chiave = array_map('to_field_k', explode(',', $table_k));
		$ar_calcolati = explode(',', $this->table_calcolati);
	
	    foreach($ar_field as $key=>$val) {
	        if ($key != 'id' && !in_array($key, $ar_k_chiave) && !in_array($key, $ar_calcolati) && substr($key, -2) != '_D' && $key != 'YAML_PARAMETERS') {
	        	//salto le chiavi
				//$cols[] = "$key = '$val'";
	        	$cols[] = "$key = " . sql_t($val);
			}
	    }
	    
	    //se sono nelle tabelle aggiorno utente/data/ora generazione
	    if ($this->table_id == "file_tabelle" && $upd_data_ora == 'Y'){
	    	global $auth;
	    	$cols[] = 'TAUSGE = ' . sql_t(substr($auth->get_user(), 0, 8));
	    	$cols[] = 'TADTGE = ' . oggi_AS_date();
	    	$cols[] = 'TAORGE = ' . oggi_AS_time();
	    }
	     
	    
	    
	    $sql = " SET " . implode(', ', $cols);

	    return($sql);		
	}

	
	

	protected function get_KEY($ar_field, $key_list){
		$cols = array();
		$ar_key = explode(',', $key_list);
		foreach($ar_key as $key) {
			$cols[] = trim($ar_field->$key); 
		}
	 return implode("|", $cols);
	}
	
	//COSTRUISCO IL CODICE WHERE IN BASE ALLA CHIAVE
	protected function get_WHERE_KEY_sql_jd($ar_field, $key_list, $to_field_k = "Y"){
		$cols = array();
		$ar_key = explode(',', $key_list);
		
	    foreach($ar_key as $key) {

	    	if ($to_field_k == "Y")
				$m_key = to_field_k($key);
			else
				$m_key = $key;			
			
	        if ($key != 'id') $cols[] = "{$key} =  " . sql_t($ar_field->$m_key);
	    }
	    $sql = implode(' AND ', $cols);
	
	    return($sql);		
	}	
	

	//COSTRUISCO IL CODICE INSERT in base alla chiave
	protected function get_INSERT_KEY_name($ar_field, $key_list, $upd_data_ora = 'Y'){
		$cols = array();
		$ar_key = explode(',', $key_list);	
		$ar_k_chiave = array_map('to_field_k', explode(',', $this->table_k));
		$ar_calcolati = array_map('to_field_k', explode(',', $this->table_calcolati));
		
	    foreach($ar_key as $key) {
	        if ($key != 'id' && !in_array($key, $ar_k_chiave) && !in_array($key, $ar_calcolati)) $cols[] = "{$key}";
	    }

	    //se sono nelle tabelle inserisco utente/data/ora generazione
	    if ($this->table_id == "file_tabelle" && $upd_data_ora == 'Y'){
	    	$cols[] = 'TAUSGE';
	    	$cols[] = 'TADTGE';
	    	$cols[] = 'TAORGE';
 	    }
	     
	    
	    $sql = implode(',', $cols);
	
	    
	    return($sql);		
	}	
	
	
	protected function get_INSERT_KEY_values($ar_field, $key_list, $upd_data_ora = 'Y'){
		$cols = array();
		$ar_key = explode(',', $key_list);
		$ar_k_chiave = array_map('to_field_k', explode(',', $this->table_k));
		$ar_calcolati = array_map('to_field_k', explode(',', $this->table_calcolati));				
	
	    foreach($ar_key as $key) {			
	        if ($key != 'id' && !in_array($key, $ar_k_chiave) && !in_array($key, $ar_calcolati)) $cols[] = sql_t($ar_field->$key);
	    }
	    
	    //se sono nelle tabelle inserisco utente/data/ora generazione	    
	    if ($this->table_id == "file_tabelle" && $upd_data_ora == 'Y'){
	    	global $auth;
	    	$cols[] = sql_t(substr($auth->get_user(), 0, 8));
	    	$cols[] = oggi_AS_date();
	    	$cols[] = oggi_AS_time();
	    }
	     
	    
	    $sql = implode(', ', $cols);

	    return($sql);		
	}	

	


	
	
	
/*
 * JSON PER GESTIONE TABELLA
 */	
	
	
	
public static function out_Writer_Grid($columns, $class_name, $cls = '') {
return "
Ext.define('Writer.Grid.{$class_name}', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.writergrid_{$class_name}',

    requires: [
        'Ext.grid.plugin.CellEditing',
        'Ext.form.field.Text',
        'Ext.toolbar.TextItem'
    ],

    
	features: [{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true   // defaults to false (remote filtering)	
		}],
		
    
    
    initComponent: function(){

        this.editing = Ext.create('Ext.grid.plugin.CellEditing');

        Ext.apply(this, {
            iconCls: 'icon-grid',
            frame: true,
            cls: " . j($cls) . ",
            // plugins: [this.editing], //togliere commento per abilitare modifica direttamente dalla grid
           
            columns: [" . $columns . "]
            
           //Gestire eventualmente parametricamente 		
           /*
			, 	viewConfig: {
	 	
		        	getRowClass: function(record, index) {		
						v = record.get('liv');
			        	v = v + ' flight ';
			            return v;
		         	}   
		    }            		
            */		
            		
        });
        this.callParent();
        this.getSelectionModel().on('selectionchange', this.onSelectChange, this);
    },
    
    onSelectChange: function(selModel, selections){
      //  this.down('#delete').setDisabled(selections.length === 0);
    },

    onSync: function(){
        this.store.sync();
    },

    onDeleteClick: function(){
        var selection = this.getView().getSelectionModel().getSelection()[0];
        if (selection) {
            this.store.remove(selection);
        }
    }
    
});

";	
}
		
	
public static function out_Writer_Form($initCompoonentContent, $class_name){
	
return  "
Ext.define('Writer.Form.{$class_name}', {
    extend: 'Ext.form.Panel',
    alias: 'widget.writerform_{$class_name}',

    requires: ['Ext.form.field.Text'],

    initComponent: function(){
		" . $initCompoonentContent . "
    },

    setActiveRecord: function(record){
        this.activeRecord = record;
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },

    onSave: function(){
        var active = this.activeRecord,
            form = this.getForm();

        if (!active) {
            return;
        }
        if (form.isValid()) {
            form.updateRecord(active);
            this.onReset();
        }
    },

    onCreate: function(){
        var form = this.getForm();

        if (form.isValid()) {
            this.fireEvent('create', this, form.getValues());
            form.reset();				

			//refresh grid
            //PER ORA RIMUOVO IL REFRESH. MA MODIFICARE PER ESSERE SICURI DELL INSERIMENTO AVVENUTO SU DB
            //store_{$class_name}.load();
        }

    },

    onReset: function(){
        this.setActiveRecord(null);
        this.getForm().reset();
    },
	
    onDelete: function(){
        var active = this.activeRecord,
            form = this.getForm();
		
		active.store.remove(active);
		
        //form.reset();			
    }	
	
});
";
}	
	

public static function out_Writer_sotto_main($class_name, $flex=1) {
	return "
						    var sotto_main = Ext.create('Ext.container.Container', {
						        padding: '0 0 0 0',
						        flex: {$flex},
						        layout: {
						            type: 'vbox',
						            align: 'stretch'
						        },
						        items: [{
						            itemId: 'form_{$class_name}',
						            flex: 1,            
						            xtype: " . j("writerform_{$class_name}") . ",
						            height: 150,
						            margins: '0 0 0 0',
						            listeners: {
						                create: function(form, data){
						                	//richiama l'insert del record
						                    store_{$class_name}.insert(0, data);
						                    // refresh spostato in fireEvent (in coda)
						                    //store_{$class_name}.load();
						                }
						            }
						        }]
						    });

	";
	
}	
		
	
public static function out_Writer_Store($class_name, $extra_params = '', $to_self = 'N', $add_personalizzazione = 'N'){

	if ($to_self == 'Y')
	 $path_to = $_SERVER['PHP_SELF'];
	else
	 $path_to = ROOT_PATH . "module/admin/app.php";
				
	$add_personalizzazione_txt = "";
	if ($add_personalizzazione == 'Y')
		$add_personalizzazione_txt = "
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				
 				actionMethods: {read: 'POST'},				
				doRequest: personalizza_extraParams_to_jsonData,
				";		
	
	return "

    var store_{$class_name} = Ext.create('Ext.data.Store', {
        model: 'Writer.Model.{$class_name}',
        autoLoad: true,
        autoSync: true,
        proxy: {
            type: 'ajax',
			 extraParams: {
	           	" . $extra_params .  "
	        },
			
	        {$add_personalizzazione_txt}
	           			
            api: {
                read: '" . $path_to . "?obj=" . $class_name . "&fn=view_jd',
                create: '" . $path_to . "?obj=" . $class_name . "&fn=create_jd',
                update: '" . $path_to . "?obj=" . $class_name . "&fn=update_jd',
                destroy: '" . $path_to . "?obj=" . $class_name . "&fn=destroy_jd'
            },
            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data',
                messageProperty: 'message'
            },
            writer: {
                type: 'json',
                writeAllFields: true,
                root: 'data'
            },
            listeners: {
                exception: function(proxy, response, operation){
                    Ext.MessageBox.show({
                        title: 'REMOTE EXCEPTION',
                        msg: operation.getError(),
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
                    store_{$class_name}.load();
                }
            }
        },
        listeners: {
            write: function(proxy, operation){
                if (operation.action == 'destroy') {
                    sotto_main.child('form').setActiveRecord(null);
                }
    
            }
        }
    });

	";
}	
		
	
public static function out_Writer_main($title, $class_name, $add_grid_selectionchange = "", $flex=1, $preventHeader='false', $tbar = null, $celldblclick = "", $addmenu="") {
	if (isset($tbar))
		$tbar = "tbar: {$tbar},";
	
	return "

						    var main = Ext.create('Ext.container.Container', {
						        padding: '0 0 0 0',
						        layout: {
						            type: 'hbox',
						            align: 'stretch'
						        },
						        items: [{
						            itemId: 'grid_{$class_name}',
						            xtype: 'writergrid_{$class_name}',
						            title: " . j($title) . ",
						            preventHeader: " . $preventHeader . ", //non mostra il titolo
						            
						            /*
									tbar: new Ext.Toolbar({
									            items:['<b>Ecommerce - Anagrafica articoli</b>', '->',
									           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){}}
									       		, {iconCls: 'tbar-x-tool x-tool-close', handler: function(event, toolEl, panel){}}
									         ]            
									}),
									*/ 						            
						            
									{$tbar}
									
						            flex: {$flex},
						            store: store_{$class_name},
						            listeners: {
						                selectionchange: function(selModel, selected) {                	
						                
						                	//pulisco eventuali filtri
						                	sotto_main.child('form').getForm().reset();
						                
						                	//ricarico i dati della form
						                    sotto_main.child('form').setActiveRecord(selected[0] || null);

											{$add_grid_selectionchange}

						                }, 
						                

										  celldblclick: {								
											  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
											  	{$celldblclick}
											  }
										}	
										
										
										, itemcontextmenu : function(grid, rec, node, index, event) {
											event.stopEvent();				
						
						                	var voci_menu = [];
						                	
						                	{$addmenu}
						                	
											var menu = new Ext.menu.Menu({
								            	items: voci_menu
											}).showAt(event.xy);
											return false;    						                	
						                	
						                }	
										
						                
						                
						                
						            }
						        }, 
						        sotto_main]
						    });

	
	";
}	
		
	
	
public static function out_Writer_window($title, $help_codice = null) {
	
	if (!is_null($help_codice))
		$tools_cod = ", tools: [
			{
				type:'help',
				tooltip: 'Help',
				handler: function(event, toolEl, panel){
					show_win_help(" . j($help_codice) . ");
				}
			}
		]";
	else $tools_cod = '';		
	
	return "
							var m_win = new Ext.Window({
										  width: 600
										, height: 500
										, minWidth: 300
										, minHeight: 300
										, plain: true
										, title:  " . j($title) . "
										, layout: 'fit'
										, border: true
										, closable: true
										, maximizable: true
										, items: main
										" . $tools_cod . "		
									});	
							m_win.show();
							m_win.maximize(); 

	
	";
}	
	
	
public static function form_buttons($b_pre = '') {
 return "
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {$b_pre}{
                    iconCls: 'icon-save',
                    itemId: 'save',
                    text: 'Salva',
                    disabled: true,
                    scope: this,
                    handler: this.onSave
                }, {
                    iconCls: 'icon-user-add',
                    itemId: 'create',                    
                    text: 'Crea',
                    scope: this,
                    handler: this.onCreate
                }, {
                    iconCls: 'icon-reset',
                    text: 'Reset',
                    scope: this,
                    handler: this.onReset
                }, {
                    iconCls: 'icon-delete',
                    text: 'Elimina',
                    scope: this,
                    handler: this.onDelete
                }]
            } 
 ";	
}	




	public function decodifica_data($calendar_datatime){
		$data_ora = explode("T", $calendar_datatime);
		$data_t = $data_ora[0] . '';
		$data_ext = explode("-", $data_t);
	
		return implode("", $data_ext);
	}

		
}

?>