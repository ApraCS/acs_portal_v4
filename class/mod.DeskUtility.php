<?php

/**********************************************
 * DESKTOP UTILITY
 **********************************************/


class DeskUtility {
	
	private $mod_cod = "DESK_UTIL";
	private $mod_dir = "desk_utility";
	
	
	function __construct($parameters = array()) {
		global $auth;
		
		
		if (isset($parameters['no_verify']) && $parameters['no_verify'] == 'Y'){
		    return true;
		}	
	
		$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
		
		if (is_null($mod_permitted) || !$mod_permitted)
		    //se mi ha passato un secondo modulo testo i permessi
	        if (isset($parameters['abilita_su_modulo']))
	             $mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
		        
        if (is_null($mod_permitted) || !$mod_permitted)
            //se mi ha passato un secondo modulo testo i permessi
            if (isset($parameters['abilita_su_modulo2']))
                $mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo2']);
		                
		                
        if (is_null($mod_permitted) || !$mod_permitted){
            global $cod_mod_provenienza;
            
           
            //verifico permessi su cod_mod_provenienza
            if (isset($cod_mod_provenienza))
                $mod_permitted = $auth->verify_module_permission($cod_mod_provenienza);
        }
		                
        
        if (is_null($mod_permitted) || !$mod_permitted){
            die("\nNon hai i permessi ({$this->mod_cod})!!");
        }
	}	
	

	public function get_cod_mod(){
		return $this->mod_cod;
	}	
	
	public function get_cfg_mod(){
		global $cfg_mod_DeskUtility;
		return $cfg_mod_DeskUtility;
	}	
	

	public function get_mod_parameters(){
		$t = new Modules();
		$t->load_rec_data_by_k(array('TAKEY1' => $this->get_cod_mod()));
		return json_decode($t->rec_data['TAMAIL']);
	}	
	
	
	public function fascetta_function(){
		global $auth, $main_module;
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	
		echo "<div class='fascetta-toolbar'>";
		//	echo "<img id='bt-ecommerce-gestart' src=" . img_path("icone/48x48/shopping_bag.png") . " height=30>";
		    echo "<img id='bt-toolbar' src=" . img_path("icone/48x48/archive.png") . " height=30>";
		    echo "<img id='bt-anag-art' src=" . img_path("icone/48x48/barcode.png") . " height=30>";	
			echo "<img id='bt-indici' src=" . img_path("icone/48x48/bookmark.png") . " height=30>";	
		//	echo "<img id='bt-anag-forn' src=" . img_path("icone/48x48/clienti.png") . " height=30>";
			echo "<img id='bt-setup-scorte' src=" . img_path("icone/48x48/box.png") . " height=30>";
			echo "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
			echo "<img id='bt-import' src=" . img_path("icone/48x48/button_black_rec.png") . " height=30>";
			echo "<img id='bt-regole-riordino' src=" . img_path("icone/48x48/shopping_setup.png") . " height=30>";
			echo "<img id='bt-anag-forn' src=" . img_path("icone/48x48/clienti.png") . " height=30>";
			echo "<img id='bt-call-pgm' src=" . img_path("icone/48x48/button_blue_play.png") . " height=30>";
			echo "</div>";
		
	
	}	

	
	
	
	function find_sys_TA($tab){
		global $conn, $id_ditta_default;
		$ar = array();
		$cfg_mod = $this->get_cfg_mod();
		

		//in join prendo il mezzo, per recuperare la targa (da impostare in automatico nella spedizione)
		$sql = "SELECT * FROM {$cfg_mod['file_tab_sys']} TA WHERE TADT=? AND TAID = ?
                AND TALINV = ''
                ORDER BY TADESC";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt, array($id_ditta_default, $tab));
	
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
				
			$ret[] = array(
						"id" 		=> $row['TANR'],
						"text" 		=> trim($row['TADESC']) . " [" . trim($row['TANR']) . "]"
			) ;
				
		}
	
		return $ret;
	}
	
	function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null, $order_by_seq = 'N', $mostra_codice = 'N'){
		global $conn;
		global $cfg_mod_DeskUtility, $id_ditta_default;
		$ar = array();
		$sql = "SELECT *
		FROM {$cfg_mod_DeskUtility['file_tabelle']}
		WHERE TADT='$id_ditta_default' AND TATAID = '$tab'";
	
		if (isset($k1))
			$sql .= " AND TAKEY1 = '$k1' ";
	
		if (isset($k2))
			$sql .= " AND TAKEY2 = '$k2' ";

		if (isset($aspe))
			$sql .= " AND TAASPE = '$aspe' ";
	
		if (isset($gruppo))
			$sql .= " AND TARIF1 = '$gruppo' ";
	
	
		if ($order_by_seq == 'Y')
			$sql .= "  ORDER BY TASITI, UPPER(TADESC)";
		else if ($order_by_seq == 'P')
			$sql .= "  ORDER BY TAPESO, UPPER(TADESC)";
		else
			$sql .= "  ORDER BY UPPER(TADESC)";

			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);

			$ret = array();
			while ($row = db2_fetch_assoc($stmt)) {
				if (is_null($k1) || $is_get == 'Y') $m_id = trim($row['TAKEY1']);
				else $m_id = $row['TAKEY2'];
					
				$r = array();
					
				$r['id'] 		= $m_id;
				
				if ($mostra_codice == 'Y')
				    $r['text'] = "[" . $m_id. "] " . $row['TADESC'];
				else
				    $r['text'] = acs_u8e(trim($row['TADESC']));
				
					
				if ($esporta_esteso == 'Y'){
					$r['TAKEY1'] = $row['TAKEY1'];
					$r['TAKEY2'] = $row['TAKEY2'];
					$r['TAKEY3'] = $row['TAKEY3'];
					$r['TAKEY4'] = $row['TAKEY4'];
					$r['TAKEY5'] = $row['TAKEY5'];
					$r['TARIF2'] = $row['TARIF2'];
					$r['TAFG01'] = $row['TAFG01'];
					$r['TAFG02'] = $row['TAFG02'];
				}

				//		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
				$ret[] = $r;
			}
				
			return $ret;
	}
	
	
	function get_TA_std($tab, $k1, $k2 = null){
		global $conn;
		global $cfg_mod_DeskUtility;
		$ar = array();
		$sql = "SELECT * FROM {$cfg_mod_DeskUtility['file_tabelle_s']} WHERE TATAID = '$tab' ";
	
		if (isset($k1))	$sql .= " AND TAKEY1 = '$k1' ";
		if (isset($k2))	$sql .= " AND TAKEY2 = '$k2' ";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
	
		$row = db2_fetch_assoc($stmt);
		if (!$row) $row = array();
	
		return $row;
	}
	
	
	
	
	
	
	function ta_sys_decod_std($dt, $tab, $cod){
		global $conn, $ar_decod_std;
		$cfg_mod = $this->get_cfg_mod();
		 
		$tab = trim($tab);
		$cod = trim($cod);
	
		if (!isset($ar_decod_std[$tab])){
	
			$sql = "SELECT *
				FROM {$cfg_mod['file_tab_sys']}
				WHERE TADT = '$dt' AND TAID = '$tab' AND TANR = '$cod'
			";
				
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
	
			$row = db2_fetch_assoc($stmt);
			return $row['TADESC'];
				
		}
	
	}
	
	
  public function setup_scorte_save_divisione($divisione){
  	$_SESSION['setup_scorte']['divisione'] = $divisione;
  }

  public function setup_scorte_get_divisione(){
  	return $_SESSION['setup_scorte']['divisione'];
  }
  
  public function get_recap_articoli($ar_art, $bl, $tpno = null){
      global $conn;
      global $cfg_mod_DeskUtility, $id_ditta_default;
      
      $bl_yes = 0;
      $bl_no = 0;
      foreach($ar_art as $v){
          
          if(isset($tpno) && $tpno != '')
              $where .= " AND RLTPNO = '{$tpno}'";
          else
              $where .= " AND RLTPNO = 'AR'";
                  
          $sql = "SELECT COUNT(*) AS C_ROW
                  FROM {$cfg_mod_DeskUtility['file_note_anag']}
                  WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$v}'
                  AND RLRIFE2 = '{$bl}' {$where}";
          
          
          
          $stmt = db2_prepare($conn, $sql);
          $result = db2_execute($stmt);
          $row = db2_fetch_assoc($stmt);
          if($row['C_ROW'] > 0)
              $bl_yes++;
          else
              $bl_no++;
          
      }
      
      $text = "Articoli in cui � presente: <b>{$bl_yes}</b>. Articoli in cui non � presente : <b>{$bl_no}</b>";
      
         
     return $text;
  }
  
  
  public function get_commento_articolo($art, $bl, $tpno = null){
      global $conn;
      global $cfg_mod_DeskUtility, $id_ditta_default;
      
      if (is_null($art) || strlen(trim($art)) == 0)
          return array();
      
          if(isset($tpno) && $tpno != '')
              $where = " AND RLTPNO = '{$tpno}'";
          else
              $where = " AND RLTPNO = 'AR'";

          $sql = "SELECT *
                    FROM {$cfg_mod_DeskUtility['file_note_anag']}
                    WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$art}'
                    AND RLRIFE2 = '{$bl}' {$where}
                    ORDER BY RLRIGA";
               
          $stmt = db2_prepare($conn, $sql);
          $result = db2_execute($stmt);
          
          $ret = array();
          $n_riga = 0;
          while ($row = db2_fetch_assoc($stmt)) {
              
              //devo rispettare le righe vuote (in base a RLRIGA)
              for ($i = $n_riga + 1; $i < $row['RLRIGA']; $i++)
                  $ret[] = '';
              
              $ret[] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
              $ret['data'] = $row['RLDTGE'];
              $ret['user'] = $row['RLUSGE'];
              
              $n_riga = $row['RLRIGA'];
          }
          
          return $ret;
  }
  
   
  public function has_commento_articolo($art, $bl = null, $tpno = null){
      global $conn;
      global $cfg_mod_DeskUtility, $id_ditta_default;

      if (is_null($art) || strlen(trim($art)) == 0)
          return false;

      $art = trim($art);
      $where = "RLDT='{$id_ditta_default}'";

      if(isset($tpno) && $tpno != '')
        $where .= " AND RLTPNO = '{$tpno}'";
      else
        $where .= " AND RLTPNO = 'AR'";
        
      $where .=  "AND RLRIFE1 = '{$art}'";
        
      if(isset($bl) && $bl != '')
        $where .= " AND RLRIFE2 = '{$bl}'";
             
      $sql = "SELECT count(*) AS NR FROM {$cfg_mod_DeskUtility['file_note_anag']} WHERE {$where}";

      $stmt = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      $result = db2_execute($stmt);
      echo db2_stmt_errormsg($stmt);
      $row = db2_fetch_assoc($stmt);
              
      if ($row['NR'] > 0) return TRUE;
      else return FALSE;
  }
  
  
  function get_rows_ordine_by_num_dtep($ord, $dtep, $ar_blocchi = null, $nrec = null){
      global $conn;
      global $cfg_mod_DeskAcq, $cfg_mod_Spedizioni, $cfg_mod_Admin;
      global $backend_ERP;
      
      $oe = $this->k_ordine_td_decode_xx($ord);
      $ar = array();
      
      switch ($backend_ERP){
          
          case 'GL':
              $sql = "SELECT
              RD.MECAR0 AS RDART,
              MADES0    AS RDDART,
              MEUNM0    AS RDUM,
              MERID0    AS RDRIGA,
              MEQTA0    AS RDQTA, MEQTS0 AS RDQTE, MERID0 AS RDRIGA
              FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
              LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} ART
              ON RD.MECAR0 = ART.MACAR0
              LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_art_des_aggiuntive']} ART_DES_AGG
              ON RD.MECAD0 = ART_DES_AGG.MHCAD0
              AND RD.METND0 = ART_DES_AGG.MHTND0
              AND RD.MEAND0 = ART_DES_AGG.MHAND0
              AND RD.MENUD0 = ART_DES_AGG.MHNUD0
              AND RD.MERID0 = ART_DES_AGG.MHRID0
              WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
              
              break;
              
          default: //SV2
              
              $sql = "SELECT RRN(RD) AS RRN, RD.*, RT.*, ANAG_ART.ARSOSP, ANAG_ART.ARESAU, ANAG_ART.ARFOR1
              FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
              LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
              ON RD.RDDT = ANAG_ART.ARDT AND RD.RDART = ANAG_ART.ARART
              LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
              ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
              WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
              AND RD.RDTISR = '' AND RD.RDSRIG = 0";
              
              if (isset($nrec))
                  $sql .= " AND RDNREC={$nrec}";
                  
      } //switch $backend_ERP
      
      
      $stmt = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      
      $result = db2_execute($stmt, array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']));
      echo db2_stmt_errormsg($stmt);
      return $stmt;
  }
  
  function k_ordine_td_decode($ordine){
      $ar = explode("_", $ordine);
      $r = array();
      $r['TDDT'] 		= $ar[0];
      $r['TDOTID'] 	= $ar[1];
      $r['TDOINU'] 	= $ar[2];
      $r['TDOADO'] 	= $ar[3];
      $r['TDONDO'] 	= $ar[4];
      $r['TDPROG'] 	= $ar[5];
      
      return $r;
  }
  
  function k_ordine_td_decode_xx($ordine){
      
      $r = $this->k_ordine_td_decode($ordine);
      
      /* GESTIONE MULTI DITTA */
      $exp_ord = explode("-", $r['TDONDO']);
      if (sizeof($exp_ord) == 2){ //sono nel formato NUMERO-DITTA
          $r['TDONDO'] 	= $exp_ord[0];
          $r['TDDT']		= $exp_ord[1];
      }
      
      return $r;
  }	
  

		 	
		 	
} //class

?>