<?php

class SpedAutorizzazioniDeroghe {
	
	private $ar_sql = array();
	
	public function crea($tipo_messaggio, $ar_par){
		
		switch ($tipo_messaggio){
			case 'assegna_spedizione':
				$this->crea_deroga_assegna_spedizione($ar_par);
			break;
			case 'RIPRO':
				$this->crea_RIPRO($ar_par);
			break;			
		}
		
	}
	
	
	
	
	
	public function ha_deroghe_by_k_ordine($k_ordine){
		global $conn, $cfg_mod_Spedizioni;
		
		$sql = "SELECT count(*) AS T_ROW FROM {$cfg_mod_Spedizioni['file_deroghe']}	WHERE ADTP='' AND ADRGES = ?";
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($k_ordine));

		$row = db2_fetch_assoc($stmt);
		
		if ($row['T_ROW'] > 0) return 1;
		return 0; 				
	}

	public function ha_RIPRO_by_k_ordine($k_ordine){
		global $conn, $cfg_mod_Spedizioni;
	
		$sql = "SELECT count(*) AS T_ROW FROM {$cfg_mod_Spedizioni['file_deroghe']}	WHERE ADTP='R' AND ADRGES = ?";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($k_ordine));
	
		$row = db2_fetch_assoc($stmt);
	
		if ($row['T_ROW'] > 0) return 1;
		return 0;
	}	
	

	public function array_deroghe_by_k_ordine($k_ordine){
		global $conn, $cfg_mod_Spedizioni;
	
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_deroghe']} WHERE ADTP = '' AND ADRGES = ?";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($k_ordine));
	
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)){
			$ret[] = $row; 			
		}
	
		return $ret;
	}	
	
	
	public function tooltip_RIPRO_by_k_ordine($k_ordine){
		$ar_tooltip_ripro = array();		
		$array_ripro = $this->array_RIPRO_by_k_ordine($k_ordine);
		$s = new Spedizioni();
		foreach ($array_ripro as $r_ripro){
			$ar_tooltip_ripro[] = "> Riprogramm. dal " . print_date($r_ripro['ADDTDE']) . " [il " . print_date($r_ripro['ADDTRI']) . " " . print_ora($r_ripro['ADHMRI']) . " - " . trim($r_ripro['ADUSRI']) . "] " .
			" - " . trim($s->decod_std('RIPRO', $r_ripro['ADAUTOR'])) . ": " .  acs_u8e(trim($r_ripro['ADCOMM'])). " - ATP: ".print_date($r_ripro['ADDATP']);
		}
		return $ar_tooltip_ripro;
	}	
	
	
	public function array_RIPRO_by_k_ordine($k_ordine){
		global $conn, $cfg_mod_Spedizioni;
	
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_deroghe']} WHERE ADTP = 'R' AND ADRGES = ?";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($k_ordine));
	
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)){
			$ret[] = $row;
		}
	
		return $ret;
	}	


	private function crea_deroga_assegna_spedizione($ar_par){
		global $auth;
				
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);

		$this->ar_sql['ADDTRI'] = oggi_AS_date(); 
		$this->ar_sql['ADHMRI'] = oggi_AS_time();		
		$this->ar_sql['ADUSRI'] = $auth->get_user();
		
		$this->ar_sql['ADAUTOR'] = $ar_par['autorizzazione'];
		$this->ar_sql['ADDTDE'] = $ar_par['data'];
		$this->ar_sql['ADCOMM'] = $ar_par['commento'];		

		$m_ar_prog = print_date($ar_par['data_prog']);
		$m_ar_disp = print_date($ar_par['data_disp']);		
		$this->ar_sql['ADNOTE'] = "Evasione programmata {$m_ar_prog} , disponibilit&agrave; prevista: {$m_ar_disp}";
			
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}


	private function crea_RIPRO($ar_par){
		global $auth;
	
		$this->imposta_campi_base();
		$this->imposta_campi_ordine($ar_par['k_ordine']);
	
		$this->ar_sql['ADTP'] 	= 'R';		
		$this->ar_sql['ADDTRI'] = oggi_AS_date();
		$this->ar_sql['ADHMRI'] = oggi_AS_time();		
		$this->ar_sql['ADUSRI'] = $auth->get_user();
		
		$this->ar_sql['ADAUTOR'] = $ar_par['causale'];		
		$this->ar_sql['ADCOMM']  = $ar_par['descrizione'];
		$this->ar_sql['ADNOTE']  = $ar_par['segnalazioni'];
		$this->ar_sql['ADDATP']  = sql_f($ar_par['data_atp']);
		$this->ar_sql['ADFG01']  = $ar_par['blocco'];
		
		//dall'ordine recupero la spedizione e il carico e altri dati da memorizzare
		$s = new Spedizioni();		
 		$ord = $s->get_ordine_by_k_docu($ar_par['k_ordine']);
 		
	 		//memorizza la data di programmazione da cui ho riprogrammato l'ordine
	 		$this->ar_sql['ADDTDE']  = $ord['TDDTEP']; 			 		
	 		$this->imposta_campi_sped_carico($ord);
	 		
	 		$this->ar_sql['ADSECA'] = $ord['TDSECA'];
	 		$this->ar_sql['ADRFCA'] = $ord['TDRFCA'];
	 		$this->ar_sql['ADTPLO'] = $ord['TDTPLO'];
	 		$this->ar_sql['ADAALO'] = $ord['TDAALO'];
	 		$this->ar_sql['ADNRLO'] = $ord['TDNRLO'];
	 		$this->ar_sql['ADSELO'] = $ord['TDSELO'];
	 		$this->ar_sql['ADRFLO'] = $ord['TDRFLO'];
	 		$this->ar_sql['ADDTOR'] = $ord['TDDTOR'];
	 		$this->ar_sql['ADINUM'] = $ord['TDOINU'];
	 		$this->ar_sql['ADAADO'] = $ord['TDOADO'];
	 		$this->ar_sql['ADNRDO'] = $ord['TDONDO'];
	 		$this->ar_sql['ADTPDO'] = $ord['TDOTPD'];
	 		$this->ar_sql['ADSTAT'] = $ord['TDSTAT'];
	 		$this->ar_sql['ADCCON'] = $ord['TDCCON'];
	 		$this->ar_sql['ADDCON'] = $ord['TDDCON'];
	 		$this->ar_sql['ADDTRG'] = $ord['TDODRE'];
	 		$this->ar_sql['ADDTEP'] = $ord['TDDTEP'];
	 		$this->ar_sql['ADDTVA'] = $ord['TDDTCF'];
	 		$this->ar_sql['ADCITI'] = $ord['TDCITI'];
	 		$this->ar_sql['ADCDIV'] = $ord['TDCDIV'];
	 		$this->ar_sql['ADTOCO'] = $ord['TDTOCO'];
	 		$this->ar_sql['ADVOLU'] = $ord['TDVOLU'];
	 		$this->ar_sql['ADTOTD'] = $ord['TDTOTD'];
	 		$this->ar_sql['ADTIMP'] = $ord['TDTIMP'];
 		 					
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	
	private function scrivi_messaggio($sql){
		global $conn;

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);		
	}
	
	private function imposta_campi_base(){		
	}
	
	private function get_insert_sql(){
		global $cfg_mod_Spedizioni;
		
		$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_deroghe']} (" . $this->get_el_field_name() . ")
				VALUES (" . $this->get_el_field_values() . ") 
				";
		
		return $sql;
	}
	
	private function imposta_campi_sped_carico($oe){
		$this->ar_sql['ADTPCA'] = $oe['TDTPCA'];
		$this->ar_sql['ADAACA'] = $oe['TDAACA'];
		$this->ar_sql['ADNRCA'] = $oe['TDNRCA'];
		$this->ar_sql['ADNSPE'] = $oe['TDNBOC'];			
	}	
	
	
	private function get_el_field_name(){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $kf;

		return implode(', ', $ar);
	}
	
	private function get_el_field_values(){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $v;
	
		return implode(', ', array_map('sql_t_trim', $ar));
	}	
	
	private function imposta_campi_ordine($k_ordine){
		$this->ar_sql['ADTIDO'] = 'VO';		
		$this->ar_sql['ADRGES'] = $k_ordine;
	}
	
	
}

?>