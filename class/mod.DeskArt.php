<?php

class DeskArt {
    
    private $mod_cod = "DESK_ART";
    private $mod_dir = "desk_art";
    
    
    function __construct($parameters = array()) {
        global $auth;
        
        if (isset($parameters['no_verify']) && $parameters['no_verify'] == 'Y'){
            return true;
        }
        
        $mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
        
        if (is_null($mod_permitted) || !$mod_permitted){
            
            //se mi ha passato un secondo modulo testo i permessi
            if (isset($parameters['abilita_su_modulo'])){
                $mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
                if (is_null($mod_permitted) || !$mod_permitted){
                    die("\nNon hai i permessi!!");
                }
            } else
                die("\nNon hai i permessi!!!");
        }
    }
    
    
    public function get_cod_mod(){
        return $this->mod_cod;
    }
    
    public function get_cfg_mod(){
        global $cfg_mod_DeskArt;
        return $cfg_mod_DeskArt;
    }	
    
    public function get_table_TA(){
        global $cfg_mod_DeskArt;
        return $cfg_mod_DeskArt['file_tabelle'];
    }
    
    
    
    function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null, $order_by_seq = 'N', $mostra_codice = 'N', $add_where = ''){
        global $conn;
        global $cfg_mod_DeskArt, $id_ditta_default;
        $ar = array();
        
        $sql = "SELECT *
        FROM {$cfg_mod_DeskArt['file_tabelle']}
        WHERE TADT='{$id_ditta_default}' 
        AND TATAID = '{$tab}' {$add_where} ";
     
        if (isset($k1))
            $sql .= " AND TAKEY1 = '$k1' ";
            
        if (isset($k2))
            $sql .= " AND TAKEY2 = '$k2' ";
                
        if (isset($aspe))
            $sql .= " AND TAASPE = '$aspe' ";
                    
       if (isset($gruppo)){
            if (is_string($gruppo))	$gruppo = trim($gruppo);
            if ($gruppo == '')
                $sql .= " AND TARIF1 = '$gruppo' ";
            else
                $sql .= sql_where_by_combo_value('TARIF1', $gruppo);
        } 	
                        
                        
        if ($order_by_seq == 'Y')
            $sql .= "  ORDER BY TASITI, UPPER(TADESC)";
        else if ($order_by_seq == 'P')
            $sql .= "  ORDER BY TAPESO, UPPER(TADESC)";
        else
            $sql .= "  ORDER BY TAKEY1, UPPER(TADESC)";
                                    
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);
                      
        $ret = array();
        while ($row = db2_fetch_assoc($stmt)) {
            if (is_null($k1) || $is_get == 'Y') $m_id = trim($row['TAKEY1']);
            else $m_id = trim($row['TAKEY2']);
            
            $r = array();
            
            $r['id'] 		= $m_id;
            
            if ($mostra_codice == 'Y')
                $r['text'] = "[" . $m_id. "] " . trim($row['TADESC']);
            else
                $r['text'] = acs_u8e(trim($row['TADESC']));
                                                
                                                
            if ($esporta_esteso == 'Y'){
                $r['TAKEY1'] = $row['TAKEY1'];
                $r['TAKEY2'] = $row['TAKEY2'];
                $r['TAKEY3'] = $row['TAKEY3'];
                $r['TAKEY4'] = $row['TAKEY4'];
                $r['TAKEY5'] = $row['TAKEY5'];
                $r['TARIF2'] = $row['TARIF2'];
                $r['TAFG01'] = $row['TAFG01'];
                $r['TAFG02'] = $row['TAFG02'];
                $r['TAASPE'] = $row['TAASPE'];
                $r['TAMAIL'] = $row['TAMAIL'];
            }
            
                                                //		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
            $ret[] = $r;
            }
                                    
        return $ret;
    }
    
    function next_num($cod){
        global $conn, $id_ditta_default;
        global $cfg_mod_DeskArt;
        
        
        $sql = "SELECT * FROM {$cfg_mod_DeskArt['file_numeratori']} WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        if ($row = db2_fetch_assoc($stmt)){
            $n = $row['NRPROG'] +1;
            $sql = "UPDATE {$cfg_mod_DeskArt['file_numeratori']} SET NRPROG=$n WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);
        } else {
            $n = 1;
            $sql = "INSERT INTO {$cfg_mod_DeskArt['file_numeratori']}(NRDT, NRCOD, NRPROG)  VALUES(" . sql_t($id_ditta_default) . ", '$cod', $n)";
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);
        }
        
        return $n;
    }
    
    function exe_avanzamento_segnalazione_arrivi($r){
        global $conn;
        global $cfg_mod_DeskArt;
        
        $ret = array();
        $ret['success'] = true;
       
        
        $list_selected_id = json_decode($r['list_selected_id']);

        //per ogni riga selezionata
        foreach($list_selected_id as $krow_selected => $row_selected){
            
            //$row_selected = $list_selected_id[0];
            
            //recupero causale/utente/ordine da usare come chiave per AS0
            $m_k_exp = explode("|", $row_selected->id);
         
            //aggiorno la riga in WPI2AS0
            $na = new SpedAssegnazioneArticoli($this);
            $na->avanza($r['f_entry_prog_causale'], array(
                'prog' => $row_selected->prog,
                'form_values' => $r,
                'articolo' => trim($row_selected->articolo)
            ));
            
        }
        
        return acs_je($ret);
    }
    
    
    
    function get_TA_std($tab, $k1, $k2 = null){
        global $conn;
        global $cfg_mod_DeskArt, $id_ditta_default;
        $ar = array();
        $sql = "SELECT * FROM {$cfg_mod_DeskArt['file_tabelle']} WHERE TADT='{$id_ditta_default}' AND TATAID = '{$tab}' ";
        
        if (isset($k1))	$sql .= " AND TAKEY1 = " . sql_t($k1);
        if (isset($k2))	$sql .= " AND TAKEY2 = " . sql_t($k2);
                
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        $row = db2_fetch_assoc($stmt);
        if (!$row) $row = array();
        
        return $row;
    }
    
    
    public function has_nota_progetto($prog){
        global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
        
        $sql = "SELECT COUNT(*) AS C_ROW
                FROM {$cfg_mod_Spedizioni['file_note']}
                WHERE NTDT = '{$id_ditta_default}' AND NTTPNO = 'ASPRS'
                AND NTKEY1 = ?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($prog));
        $row = db2_fetch_assoc($stmt);
        
        return $row['C_ROW'];
        
    }
    
    public function get_note_progetto($prog){
        global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
        $sql = "SELECT RRN(NT) AS RRN, NTMEMO
                FROM {$cfg_mod_Spedizioni['file_note']} NT
                WHERE NTDT = '{$id_ditta_default}' AND NTSEQU=0
                AND NTTPNO = 'ASPRS' AND NTKEY1 = ?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($prog));
        $row = db2_fetch_assoc($stmt);
        
        return $row;
    }
    
    public function has_nota_indici($chiave, $tpno = "CDIND"){
        global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
   
        $sql = "SELECT COUNT(*) AS C_ROW
                FROM {$cfg_mod_Spedizioni['file_note']}
                WHERE NTDT = '{$id_ditta_default}' AND NTTPNO = '{$tpno}'
                AND NTKEY1 = ?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($chiave));
        $row = db2_fetch_assoc($stmt);
        
        return $row['C_ROW'];
        
    }
    
    public function get_note_indici($chiave, $tpno = "CDIND"){
        global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
        $sql = "SELECT RRN(NT) AS RRN, NTMEMO 
                FROM {$cfg_mod_Spedizioni['file_note']} NT
                WHERE NTDT = '{$id_ditta_default}' AND NTSEQU=0 
                AND NTTPNO = '{$tpno}' AND NTKEY1 = ?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($chiave));
        $row = db2_fetch_assoc($stmt);
        
        return $row;
    }
    
    public function has_scheda_articolo($art, $scheda){
        global $conn, $cfg_mod_DeskArt, $id_ditta_default;
        
        $sql = "SELECT count(*) AS NR
                FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                WHERE TADT = '{$id_ditta_default}' AND TAID = '{$scheda}' 
                AND TACOR1 = '{$art}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if ($row['NR'] > 0) return TRUE;
        else return FALSE;
        
        
    }
    
    
    /*--------------------------------------------------------------------------------------------------------------*/
    public function get_row_art($cod_art){
    /*--------------------------------------------------------------------------------------------------------------*/
        global $conn, $id_ditta_default, $cfg_mod_DeskArt, $cfg_mod_DeskUtility;
        $sql = "SELECT * FROM {$cfg_mod_DeskArt['file_anag_art_sst']} AR 
                WHERE ARDT = '{$id_ditta_default}' AND ARART = ? ";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($cod_art));
        $row = db2_fetch_assoc($stmt);
        
        $row = array_map('rtrim', $row);            
        return $row;        
    }
    
    
    
    
    
    
}