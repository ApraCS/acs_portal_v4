<?php

/**********************************************
 * DESKTOP UTILITY
 **********************************************/

class DeskStats {
	
	private $mod_cod = "DESK_STATS";
	private $mod_dir = "desk_stats";
	
	
	function __construct($parameters = array()) {
		global $auth;
		$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
	
		if (is_null($mod_permitted) || !$mod_permitted){
				
			//se mi ha passato un secondo modulo testo i permessi
			if (isset($parameters['abilita_su_modulo'])){
				$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
				if (is_null($mod_permitted) || !$mod_permitted){
					die("\nNon hai i permessi!!");
				}
			} else
				die("\nNon hai i permessi!!!");
		}
	}	
	

	public function get_cod_mod(){
		return $this->mod_cod;
	}	
	
	public function get_cfg_mod(){
		global $cfg_mod_DeskStats;
		return $cfg_mod_DeskStats;
	}	
	

	public function get_mod_parameters(){
		$t = new Modules();
		$t->load_rec_data_by_k(array('TAKEY1' => $this->get_cod_mod()));
		return json_decode($t->rec_data['TAMAIL']);
	}	
	
	
	public function fascetta_function(){
		global $auth, $main_module;
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	
		echo "<div class='fascetta-toolbar'>";
			echo "<img id='bt-stat-direz' src=" . img_path("icone/48x48/chart_bar.png") . " height=30>";
			echo "<img id='bt-docum' src=" . img_path("icone/48x48/blog_compose.png") . " height=30>";
		echo "</div>";
	
	}	

	

	function find_sys_TA($tab){
		global $conn;
		$ar = array();
		$cfg_mod = $this->get_cfg_mod();
	
	
		//in join prendo il mezzo, per recuperare la targa (da impostare in automatico nella spedizione)
		$sql = "SELECT * FROM {$cfg_mod['file_tab_sys']} TA WHERE TAID = ?";
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($tab));
	
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
	
			$ret[] = array(
					"id" 		=> $row['TANR'],
					"text" 		=> trim($row['TADESC']) . "[" . trim($row['TANR']) . "]"
			) ;
	
		}
	
	
		return $ret;
	}
	
	
	
	
		 	
		 	
} //class

?>