<?php

class F8{
	
	public function write_json_buttons($mod, $func, $tbspacer=0){
		$b_memorizza = "
			{   xtype : 'button',
				text: 'Memorizza',
				iconCls: 'icon-F8-m-32',
				scale: 'large',
				tooltip: 'Memorizza parametri correnti',				
				handler: function() {
	            	var form = this.up('form').getForm();
					var form_values = form.getValues();
 
                    //gestione eventuali grid di cui salvare lo store
                    var grids = this.up('form').query('[grid_save_in_F8]');
		        	Ext.each(grids, function(gr) {
                        var grid_values = gr.get_f8_data();
                        form_values[gr.grid_save_in_F8] = grid_values;
		        	});
				
					ar_parameters = " . acs_je(array("op" => "M", "mod" => $mod, "func" => $func)) . ";				
					ar_parameters['memo'] = form_values;

					//apro la form per scegliere il nome della memorizzazione
					acs_show_win_std('Memorizza parametri correnti', " . acs_url('module/base/f8.php?fn=get_json_m') . ", ar_parameters, 500, 460, null, 'icon-F8-m-16');				
				}
		    }
		";
		
		$b_richiama = "
			{
                xtype : 'button',
				text: 'Richiama',
				iconCls: 'icon-F8-r-32',
				scale: 'large',
				tooltip: 'Richiama parametri preimpostati',
                listeners: {
	         		'afterrender': function(comp){ 
                        var form_origine = comp.up('form');
                        ar_parameters = " . acs_je(array("op" => "M", "mod" => $mod, "func" => $func)) . ";
                       	Ext.Ajax.request({
    				   		url:" . acs_url('module/base/f8.php?fn=get_data') . ",
    				   		method: 'POST',
    	        			jsonData: {
    	        				ar_parameters: ar_parameters
    	        				},
    				   		success: function(response, opts) {
    				      		var src = Ext.decode(response.responseText);
                                var data = src.data[0].DFMEMO;

                                memorizzazione = Ext.decode(data.replace(/\\\"/g, '\"'));
                                p_ar = new Array();
                        	  	 Ext.Object.each(memorizzazione, function(key, value) {
                        	  	 	p_ar[key] = value;
                        	  	 });
                        
                        	  	//per tutti gli input della form
                        	  	form_origine.getForm().getFields().items.forEach(function(n) {
                        	  	  if (n.xtype == 'datefield'){
                        	  	    if (!Ext.isEmpty(p_ar[n.name]) && p_ar[n.name].length > 0)
                        	  	  		n.setValue(acs_date_from_AS(p_ar[n.name], 'd/m/Y'));
                        	  	  	else
                        	  	  		n.setValue(null);	
                                  }else if(n.xtype == 'timefield') {
                              	    if (!Ext.isEmpty(p_ar[n.name]) && p_ar[n.name].length > 0)
                              	  		n.setValue(time_from_AS(p_ar[n.name], 'H:i'));
                              	  	else
                              	  		n.setValue(null);
                              	  } else if (n.xtype == 'hidden') {
                        	  		//non ricompilo i campi hidden
                        	  	  } else if (n.xtype == 'displayfield') {
                        	  		//non ricompilo i campi displayfield
                        	  	  } else {
                        	  		n.setValue(p_ar[n.name]);
                        	  	  }	
                        	  	});
                
    					   	}
    					});

                }},
				handler: function() {
	            	var form = this.up('form').getForm();				
					ar_parameters = " . acs_je(array("op" => "M", "mod" => $mod, "func" => $func)) . ";
					ar_parameters['form_id'] = this.up('form').id;											

					//apro la form per la scelta della memorizzazione
					acs_show_win_std('Richiama parametri preimpostati', " . acs_url('module/base/f8.php?fn=get_json_r') . ", ar_parameters, 500, 360, null, 'icon-F8-r-16');							
				}
		    }
		";		
		
	 $ar_b = array();
	 
	 //memorizza visibile solo se profilo puo' modificare parametri
	 global $auth;
	 
	 global $cod_mod_provenienza;
	 if (isset($cod_mod_provenienza)){
	 	$js_parameters = $auth->get_module_parameters($cod_mod_provenienza);
	 } else {
	 	$js_parameters = $auth->get_module_parameters($mod);
	 } 
	 
	 if ($js_parameters->p_mod_param == 'Y')	 
	 	$ar_b[] = $b_memorizza;	  

	 $ar_b[] = $b_richiama;

	 //per mettere i bottoni di F8 a sinistra e il resto a destro (
	 // la form deve avere 	buttonAlign:'center'
	 if ($tbspacer > 0)
	 	$ar_b[] = "{ xtype: 'tbseparator', margin: '0 {$tbspacer} 0 {$tbspacer}' }";
	 else
	 	$ar_b[] = "{ xtype: 'tbfill'}";	 	 
		
	 return implode(",", $ar_b) . ",";	  
	}
	
	
	public function get_memorizzazioni_data($params){
		global $conn, $cfg_mod_Admin, $id_ditta_default;
		
		if(isset($params->name))
		    $where = " AND DFNAME = '{$params->name}'";
		
		//recupero elenco memorizzazioni
		$sql = "SELECT * FROM {$cfg_mod_Admin['file_memorizzazioni']} WHERE DFDT=? AND DFMODU=? AND DFFUNC=? {$where} ORDER BY UPPER(DFNAME)";
	
		$stmt = db2_prepare($conn, $sql);
		$p_sql = array();
		$p_sql[] = $id_ditta_default;
		$p_sql[] = $params->mod;
		$p_sql[] = $params->func;
		$result = db2_execute($stmt, $p_sql);
		
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)){
			$row['DFNAME'] = trim(acs_u8e($row['DFNAME']));
			$ret[] = $row;
		}		
		return $ret;		
	}
	
	
	public function get_memorizzazione_data_by_id($id, $params){
		global $conn, $cfg_mod_Admin, $id_ditta_default;
	
		//recupero elenco memorizzazioni
		$sql = "SELECT * FROM {$cfg_mod_Admin['file_memorizzazioni']} WHERE DFID = ? AND DFDT=? AND DFMODU=? AND DFFUNC=?";
		$stmt = db2_prepare($conn, $sql);
		$p_sql = array();
		$p_sql[] = $id;
		$p_sql[] = $id_ditta_default;
		$p_sql[] = $params->mod;
		$p_sql[] = $params->func;		
		$result = db2_execute($stmt, $p_sql);
	
		$row = db2_fetch_assoc($stmt);
		return $row;
	}	

	public function delete($params){
		global $conn, $cfg_mod_Admin, $id_ditta_default;
		$sql = "DELETE FROM {$cfg_mod_Admin['file_memorizzazioni']} WHERE DFID=?";
		$stmt = db2_prepare($conn, $sql);
		$p_sql = array();
		$p_sql[] = $params->DFID;		
		$result = db2_execute($stmt, $p_sql);						
	}
	
	
	
	public function save($params){
		global $conn, $cfg_mod_Admin, $id_ditta_default;
		
		//verifico se gia' esiste
		$sql = "SELECT * FROM {$cfg_mod_Admin['file_memorizzazioni']} WHERE DFDT=? AND DFMODU=? AND DFFUNC=? AND DFNAME=?";
		$stmt = db2_prepare($conn, $sql);
		$p_sql = array();
		$p_sql[] = $id_ditta_default;
		$p_sql[] = $params['mod'];
		$p_sql[] = $params['func'];
		$p_sql[] = trim($params['name']);		
		$result = db2_execute($stmt, $p_sql);
		$row = db2_fetch_assoc($stmt);
		
		//converto eventuali apici
		$params['memo'] = strtr($params['memo'], array( '\\"' => '"'));
		
		if ($row == false){ //NUOVO INSERIMENTO
		   $sql = "INSERT INTO {$cfg_mod_Admin['file_memorizzazioni']}(DFDT, DFMODU, DFFUNC, DFNAME, DFMEMO) VALUES (?,?,?,?,?)";
		   $p_sql = array();
		   $p_sql[] = $id_ditta_default;
		   $p_sql[] = $params['mod'];	   	
		   $p_sql[] = $params['func'];	   
		   $p_sql[] = trim($params['name']);	   
		   $p_sql[] = $params['memo'];
		} else { //UPDATE
			$sql = "UPDATE {$cfg_mod_Admin['file_memorizzazioni']} SET DFMEMO=? WHERE DFDT=? AND DFMODU=? AND DFFUNC=? AND DFNAME=?";
			$p_sql = array();
			$p_sql[] = $params['memo'];			
			$p_sql[] = $id_ditta_default;
			$p_sql[] = $params['mod'];
			$p_sql[] = $params['func'];
			$p_sql[] = trim($params['name']);						
		}

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, $p_sql);	   
	}
	
} //class

?>