<?php

class Spedizioni {
	
	private $mod_cod = "DESK_VEND";
	private $mod_dir = "desk_vend";
	
	function __construct($parameters = array()) {
		global $auth;
		
		if (isset($parameters['no_verify']) && $parameters['no_verify'] == 'Y'){
			return true;
		}		
		
		$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
	
		if (is_null($mod_permitted) || !$mod_permitted)		
			//se mi ha passato un secondo modulo testo i permessi
			if (isset($parameters['abilita_su_modulo']))
				$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
			
		if (is_null($mod_permitted) || !$mod_permitted)
			//se mi ha passato un secondo modulo testo i permessi
			if (isset($parameters['abilita_su_modulo2']))
				$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo2']);
				

		if (is_null($mod_permitted) || !$mod_permitted){
			global $cod_mod_provenienza;
			//verifico permessi su cod_mod_provenienza
			if (isset($cod_mod_provenienza))
				$mod_permitted = $auth->verify_module_permission($cod_mod_provenienza);
		}	
				
				
		if (is_null($mod_permitted) || !$mod_permitted){
			die("\nNon hai i permessi ({$this->mod_cod})!!");
		}
					
	  return true;	
	}
	
	
	
	public function get_cod_mod(){
		return $this->mod_cod;
	}
	
	
	public function get_cfg_mod(){
		global $cfg_mod_Spedizioni;
		return $cfg_mod_Spedizioni;
	}	
	
	
	public function get_mod_parameters(){
		$t = new Modules();
		$t->load_rec_data_by_k(array('TAKEY1' => $this->get_cod_mod()));
		return json_decode($t->rec_data['TAMAIL']);			
	}
		
	public function get_table_TA(){
	    global $cfg_mod_Spedizioni;
	    return $cfg_mod_Spedizioni['file_tabelle'];
	}
	
	public function add_riservatezza($campo_tddocu = 'TD.TDDOCU'){
		global $auth, $cfg_mod_Spedizioni;

		//se sono TRASPORTATORE filtro in base agli ordini in cui sono in TDVET1 (trasportatore)
		if ($auth->get_tipologia() == 'TRASP'){
		      if (strlen($auth->get_cod_riservatezza()) > 0){
		          $ret = "";
		          $ret .= "
                        INNER JOIN (
                            SELECT TDDOCU TRASP_RIS_TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']}
                            WHERE TDVET1 = " . sql_t($auth->get_cod_riservatezza()) . "
                        ) TRASP_RIS ON {$campo_tddocu} = TRASP_RIS_TDDOCU
                    ";		          
		          return " " . $ret . " ";
		      } else {
		          die("Errore di configurazione account: Trasportatore senza codice riservatezza");
		      }		          
		} // if TRASP
		

		//se sono CLIENTE filtro in base agli ordini in cui sono in TDCCON
		if ($auth->get_tipologia() == 'CLI'){
		    if (strlen($auth->get_cod_riservatezza()) > 0){
		        
		        $cod_ris_where = "(";
		        $cod_ris_where .= "TDCCON = " . sql_t($auth->get_cod_riservatezza());
		        for ($i=2; $i<=5; $i++){
		            $fnr = "get_cod_riservatezza{$i}";
		            $nc = $auth->$fnr();
		            if (strlen($nc) > 0)
		                $cod_ris_where .= " OR TDCCON = " . sql_t($nc);
		        }
		        $cod_ris_where .= ") ";
		        
		        $ret = "";
		        $ret .= "
		        INNER JOIN (
		        SELECT TDDOCU CLI_RIS_TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']}
		        WHERE {$cod_ris_where}
		        ) TRASP_RIS ON {$campo_tddocu} = CLI_RIS_TDDOCU
		        ";
		        
		        return " " . $ret . " ";
		    } else {
		        die("Errore di configurazione account: Cliente senza codice riservatezza");
		    }
		} // if CLIENTE
		
		if (strlen($auth->get_cod_riservatezza($campo_tddocu = 'TD.TDDOCU')) > 0){
			$ret = "";
			$ret .= " INNER JOIN {$cfg_mod_Spedizioni['file_riservatezza']} ";
			$ret .= " ON {$campo_tddocu}=PGDOCU AND PGTRIS='ORDCLI' AND PGPGES='{$auth->get_tipologia()}' AND PGCPGE='{$auth->get_cod_riservatezza()}'";
			return " " . $ret . " "; 
		}
		
		return '';
	}
	
	

	public function get_where_std($tipo = '', $filtra_per_account = false){
		global $id_ditta_default;
		$ret = '';
		switch ($tipo){
			case 'search':
				$ret .= " (TDSWPP='Y' OR TDFDOE='Y') AND TDDT='$id_ditta_default' ";				
			    break;
			case 'with_history':
			    $ret .= " TDSWPP IN('Y', 'S') AND TDDT='$id_ditta_default' ";
			    break;
			case 'none':
				$ret .= " 1=1 ";
				break;			
			default:
				$ret .= " TDSWPP='Y' AND TDDT='$id_ditta_default' ";				
		}
		
	  	if ($filtra_per_account != false){
	  		$ret .= " " . $this->get_where_std_account($tipo, $filtra_per_account) . " ";
	  	}
	  	
	  	global $cod_mod_provenienza;
	
	  	
	  	if ($cod_mod_provenienza == 'DESK_PVEN'){
	  		
	  		//eventuale filtro per stabilimento
	  		$dpv = new DeskPVen(array('no_verify' => 'Y'));
	  		$stab_assegnati = $dpv->get_stabilimenti_assegnati();
	  		if (count($stab_assegnati) > 0)
	  			$ret .= " AND TDSTAB IN (" . sql_t_IN($stab_assegnati) . ")";
	  	}
		
	  return $ret;		
	}
	

	public function get_where_std_account($tipo = '', $filtra_per_account = array()){
		global $auth;
		$js_parameters = $auth->get_module_parameters($this->get_cod_mod());
		
		$filtra_su = explode(",", $filtra_per_account['filtra_su']);
		
		$ret  = '';
		

			//filtro per area di spedizione
			if (isset($js_parameters->area_sped)){
				if (in_array('SP', $filtra_su))
					$ret .= " AND TA_ITIN.TAASPE IN (" . sql_t_IN($js_parameters->area_sped) . ") ";
				elseif (in_array('TD', $filtra_su))
					$ret .= " AND TDASPE IN (" . sql_t_IN($js_parameters->area_sped) . ") ";				
			}
			
			//filtro in base all'utente (entry + ALLUSER)
			if ($js_parameters->entry_flt == 1){
				if (in_array('AS', $filtra_su)){
					$ret .= " AND ( 
							ASUSAT = " . sql_t_trim($auth->get_user()) . " OR
						    UPPER(ASUSAT) = '[ALLUSER]'
							) ";					
				}				
			}			
			
			//filtro in base all'utente (solo entry)
			if ($js_parameters->entry_flt == 2){
				if (in_array('AS', $filtra_su)){
					$ret .= " AND (
							ASUSAT = " . sql_t_trim($auth->get_user()) . "
							) ";
				}
			}
		
		return $ret;
	}
	
	
	
	public function out_table($ars, $da_data, $n_giorni, $field_dett){
	 // OUT LIV1	
		foreach ($ars as $k1 => $v1){
		 echo"
		 <tr class=tier1 id={$k1} class=\"a\">
		  <td>
		  	<div class=\"tier1\">
		  		<a style=\"background-image: url(images/folder-open.gif);\" href=\"#\" onclick=\"toggleRows(this); return false;\" class=\"folder\"></a>
		  		{$v1["txt"]}
		  	</div></td>
		 " . $this->out_table_cell($v1["dta"], $da_data, $n_giorni, $field_dett) . "
		 </tr>";		
		 
		 foreach ($v1["val"] as $k2=>$v2){
		 echo "		 	
			 <tr class=tier2 style=\"display: none;\" id=\"{$k1}-{$k2}\" class=\"a\">
			  <td><div class=\"tier2\"><a style=\"background-image: url(images/folder-open.gif);\" href=\"\" onclick=\"toggleRows(this); return false;\" class=\"folder\"></a>
			  {$v2["txt"]}</div></td>
			  " . $this->out_table_cell($v2["dta"], $da_data, $n_giorni, $field_dett) . "
			 </tr>";
			 
			//liv3
			foreach ($v2["val"] as $k3=>$v3){
			 echo "
			 <tr class=tier3 style=\"display: none;\" id=\"{$k1}-{$k2}-{$k3}\" class=\"a\">
			  <td><div class=\"tier3\"><a href=\"#\" class=\"doc\"></a>
			  {$v3["txt"]}</div></td>
			  " . $this->out_table_cell($v3["dta"], $da_data, $n_giorni, $field_dett) . "
			 </tr>";				
			}	 
			 
		 } //liv2
		 
		}
	}
	
	
	public function out_albero_itinerari($ars, $da_data, $n_giorni, $field_dett){
	  $r = "";
	  $r .= "<table width=\"100%\"><tbody>";		
	 // OUT LIV1	
		foreach ($ars as $k1 => $v1){
		 $r .= "
		 <tr class=tier1 id={$k1} class=\"a\">
		  <td>
		  	<div class=\"tier1\">
		  		<a style=\"background-image: url(images/folder-open.gif);\" href=\"#\" onclick=\"toggleRows(this); return false;\" class=\"folder\"></a>
		  		{$v1["txt"]}
		  	</div></td>
		 </tr>";		
		 
		 foreach ($v1["val"] as $k2=>$v2){
		  $r .= "		 	
			 <tr class=tier2 style=\"display: none;\" id=\"{$k1}-{$k2}\" class=\"a\">
			  <td><div class=\"tier2\"><a style=\"background-image: url(images/folder-open.gif);\" href=\"\" onclick=\"toggleRows(this); return false;\" class=\"folder\"></a>
			  {$v2["txt"]}</div></td>
			 </tr>";
			 
			//liv3
			foreach ($v2["val"] as $k3=>$v3){
			 $r .= "
			 <tr class=tier3 style=\"display: none;\" id=\"{$k1}-{$k2}-{$k3}\" class=\"a\">
			  <td><div class=\"tier3\">
			  	<a k1={$v1["cod"]} k2={$v2["cod"]} k3={$v3["cod"]} href=\"#\" class=\"doc ajax-flt\"></a>
			  {$v3["txt"]}</div></td>
			 </tr>";				
			}	 
			 
		 } //liv2
		 
		}
	  $r .= "</tbody></table>";
	  return $r;		
	}	
	
	
	
	
	
	
/* *********************************************************************************************/
/* CALENDARIO */
/* *********************************************************************************************/
	
	public function get_elenco_itinerari($da_data , $n_giorni=14, $root_id="", $field_dett = null, $field_data = 'TDDTEP', $ar_filtri = null, $escludi_riservatezza = 'N'){		
		global $conn;
		global $cfg_mod_Spedizioni;
		$da_data_as_txt = $da_data->format('Y-m-d');
		$da_data_as = $da_data->format('Ymd');
		$a_data_as = date('Ymd', strtotime($da_data_as_txt . " +{$n_giorni} days"));
		
		
		//Se lavoriamo con la TDDTSP allora la spedizione di riferimento e' la TDNBOF e non la TDNBOC
		if ($field_data == 'TDDTSP')
		    $td_sped_field = 'TDNBOF';
		else
		    $td_sped_field = 'TDNBOC';
		
		
		//se voglio forzare a non fare filtri su utente (es. area_sped). Usato nel calcolo dei totali del flight
		$ar_filtro_account = array('filtra_su' => 'SP,TD');
		if ($escludi_riservatezza == 'Y') {
			$ar_filtro_account = false;
		}		
		
		//in base al parametro di config decido se il numero ordini e' definito da TDFN10 o e' un semplice conteggio
		$cfg_mod = $this->get_cfg_mod();
		if ($cfg_mod['sum_su_TDFN10'] == 'N')
			$f_T_N_ORD = 'count(*)';
		else
			$f_T_N_ORD = 'sum(TDFN10)';
		

		$f_select_f = "	TDDT, TAASPE, 
						TDCITI, TASITI,						
						TD.{$field_data}, TDSWSP";
		$f_select_sum = ", sum(TDVOLU) as T_TDVOLU,  sum(TDTOCO) as T_TDTOCO, sum(TDBANC) as T_TDBANC, SUM(TDTIMP/1000) AS T_TDTIMP, 
						   sum(TDFN01) as T_CON_CARICO, sum(TDFN02) as T_cli_bloc, sum(TDFN03) as T_ord_bloc,
						   sum(TDFN05) as T_art_mancanti, sum(TDFN04) as T_da_prog, {$f_T_N_ORD} as T_N_ORD,
						   sum(TDFN06) as T_data_confermata, sum(TDFN07 + TDFN08) as T_data_tassativa,
						   sum(TDFN11) as T_ORD_EVASI, /* MAX(TDNBOC) AS MAX_TDNBOC,*/
						   count(*) as T_ROW ";
		if ($field_dett == 'CP') //colli produzione
			$f_select_sum .= " ,sum(RD.RDQTA) AS T_CP";		
		
		$f_select_liv3 = ", {$td_sped_field}, TDCCON, TDDCON, CSDTSP, CSHMPG, CSTISP, CSTITR, CSVOLD, CSBAND, CSPESO ";
		
		if ($root_id == "root")
		{
		 $pre_id = "";
		 $liv = 1;
		 $sql_field = $f_select_f . $f_select_sum;
		 $sql_group_by = " GROUP BY " . $f_select_f;
		 $sql_order = "";
		 $sql_where = "";
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$pre_id = $root_id;
		 	$sottostringhe = explode("|", $root_id);
			$liv1 = $sottostringhe[2]; //AREA
			$liv2 = $sottostringhe[4]; //ITINERARIO
			$liv = 3;
		 	$sql_field = $f_select_f . $f_select_sum. $f_select_liv3;
		 	$sql_group_by = " GROUP BY " . $f_select_f . $f_select_liv3;
		 	$sql_order = ", {$field_data}, CSDTSP, CSHMPG, TDDCON";
			$sql_where = " AND TAASPE = '{$liv1}' AND TDCITI = '{$liv2}' ";									
		 }


		if (is_null($ar_filtri)) 
			$filtri_plan = $this->get_plan_filtro_ordini();
		else 
			$filtri_plan = $ar_filtri;
		
		if ($filtri_plan['f_carico_assegnato'] == 'Y')
			$sql_where .= " AND TDFN01 = 1 ";
		if (strlen($filtri_plan['f_divisione']) > 0)
			$sql_where .= " AND TDCDIV = " . sql_t($filtri_plan['f_divisione']);
		if (strlen($filtri_plan['f_mercato']) > 0)
			$sql_where .= " AND TDCAVE = " . sql_t($filtri_plan['f_mercato']);
				
		
		//se sono in flight (data = TDDTSP) devo escludere le spedizioni con stato DP (Da programmare) prendendo comunque gli evasi
		$sql_where_NO_DP = '';
		if ($field_data == 'TDDTSP')
			$sql_where_NO_DP .= " AND ( SP.CSSTSP <> 'DP' OR TDFN11 = 1 )"; 
		$sql_where_DP = '';
		if ($field_data == 'TDDTSP')
			$sql_where_DP .= " AND ( SP.CSSTSP = 'DP' AND TDFN11 = 0 )";		
		
		
		$sql = "SELECT '' AS DP_NO_DP, {$sql_field}
				FROM {$cfg_mod_Spedizioni['file_testate']} TD" . $this->add_riservatezza();
		
		
		if ($field_dett == 'CP') //colli produzione		
			$sql .= "
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO				
					AND RDTPNO='COLLI' AND RDRIGA=0 AND RDRIFE = 'PRODUZIONE'				
				";
		
		$sql .= "
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND {$td_sped_field} = SP.CSPROG				    
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI				    
				WHERE " . $this->get_where_std(null, $ar_filtro_account) . " 
				 AND TDSWSP='Y' AND TD.{$field_data} >= {$da_data_as} AND TD.{$field_data} <{$a_data_as} {$sql_where} {$sql_where_NO_DP}
				{$sql_group_by}
				";
		if ($liv==1){		
			$sql .= " UNION		
				SELECT '' AS DP_NO_DP, {$sql_field} 
				FROM {$cfg_mod_Spedizioni['file_testate']} TD " .  $this->add_riservatezza() . "
				";
		
		if ($field_dett == 'CP') //colli produzione
			$sql .= "		
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO				
					AND RDTPNO='COLLI' AND RDRIGA=0 AND RDRIFE = 'PRODUZIONE'				
				";
				
			$sql .= "
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI				
				WHERE " . $this->get_where_std(null, $ar_filtro_account) . " AND TDSWSP='N' {$sql_where} /* ORD. NON PROGRAMMABILI */
				{$sql_group_by}
				";
		} //if liv==1	

		//se sono in flight (data = TDDTSP) aggiungo i DP (SPEDIZIONI DA PROGRAMMARE, SENZA CONSIDERARE LA DATA)		
		if ($field_data == 'TDDTSP'){
			$sql .= " UNION
					SELECT 'DP' AS DP_NO_DP, {$sql_field}
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				";
				if ($field_dett == 'CP') //colli produzione
				$sql .= "
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
				AND RDTPNO='COLLI' AND RDRIGA=0 AND RDRIFE = 'PRODUZIONE'
				";
				
				$sql .= "
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND {$td_sped_field} = SP.CSPROG
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
				WHERE " . $this->get_where_std(null, $ar_filtro_account) . "
				AND TDSWSP='Y' {$sql_where} {$sql_where_DP}
				{$sql_group_by}
			";
				
		}			
		
								
		$sql .= "ORDER BY DP_NO_DP, TAASPE, TASITI, TDDT " . $sql_order;
		
//echo $sql; exit;		
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();				
		$result = db2_execute($stmt);
		
		if (!$result)
			echo db2_stmt_errormsg($stmt);
		
		return $stmt;
	}	


	public function crea_array_valori($its, $field_dett, $root_id, $field_data = 'TDDTEP'){
	    
	    //Se lavoriamo con la TDDTSP allora la spedizione di riferimento e' la TDNBOF e non la TDNBOC
	    if ($field_data == 'TDDTSP')
	      $td_sped_field = 'TDNBOF';
	    else
	      $td_sped_field = 'TDNBOC';	    

		$ar = array();
		$ar_tot = array();		
		$liv1_c = $liv2_c = $liv3_c = 0;
		$liv1_v = $liv2_v = $liv3_v = "--------------";
		
		if ($root_id == "root")
		{
		 $pre_id = "";
		 $liv = 1;
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$pre_id = $root_id;
		 	$sottostringhe = explode("|", $root_id);
			$liv1 = $sottostringhe[2]; //AREA
			$liv2 = $sottostringhe[4]; //ITINERARIO
			$liv = 3;
		 }		

		while ($row = db2_fetch_assoc($its)) {
			
			$row['TAASPE'] = trim($row['TAASPE']);
			$row['TDCITI'] = trim($row['TDCITI']);
			
			
			if ($row['TAASPE'] != $liv1_v){
				$liv1_c += 1;
				$liv2_c = $liv3_c = 0;
				$liv2_v = $liv3_v = "-----------";
				$liv1_v = $row['TAASPE'];
				if (is_null($ar[$liv1_v]))
					$ar[$liv1_v] = array("cod"=>$liv1_v, "txt"=> $this->decod_std('ASPE', $row['TAASPE']), "dta"=>array(), "val"=>array()); 
			}
			if ($row['TDCITI'] != $liv2_v){
				$liv2_c += 1;
				$liv3_c = 0; $liv3_v = "-----------";								
				$liv2_v = $row['TDCITI'];
				if (is_null($ar[$liv1_v]["val"][$liv2_v]))
					$ar[$liv1_v]["val"][$liv2_v] = array("cod"=>$liv2_v, "txt"=> $this->decod_std('ITIN', $row['TDCITI']), "dta"=>array(), "val"=>array());
			}
			
			
			if ($liv==3 && $row[$td_sped_field] != $liv3_v){
				$liv3_c += 1;								
				$liv3_v = $row[$td_sped_field];
				
				//TODO: non ho in linea tutti i dati della spedizione? Serve recuperarla?
				$sped = $this->get_spedizione($row[$td_sped_field]);
				
				$m_txt = $this->decod_std('AUTR', $sped['CSCVET']) . " " .	$this->decod_std('AUTO', $sped['CSCAUT']) . 
						" [" . print_date($sped['CSDTSP']) . " " . print_ora($sped['CSHMPG']) . "]" .
						" " . $row['CSTISP']  . " " . $row['CSTITR'] . " - {$sped['CSPROG']}";
				
				//targa e porta
				if (strlen(trim($sped['CSTARG']) ) > 0)
					$m_txt .= " " . trim($sped['CSTARG']);
 
				if (strlen(trim($sped['CSPORT']) ) > 0)
					$m_txt .= " Porta: " . trim($sped['CSPORT']);				
				
				if (is_null($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]))				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v] = array("cod"=>$liv3_v,
						"txt"		=> $m_txt,
					    "CSFG04"    => $sped['CSFG04'],
						"dta"		=> array()														
				);

					
			}			
						
			if ($liv==3 && $row['TDCCON'] != $liv3_v){
				$liv4_c += 1;								
				$liv4_v = $row['TDCCON'];
				if (is_null($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]))				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v] = array("cod"=>$liv4_v,
						"txt"=> $row['TDDCON'],
						"dta"=>array()); 
			}			

		if (trim($row['TDSWSP']) == 'Y' && trim($row['DP_NO_DP']) == '') {

			//sommo il volume o il peso o i colli .... nella cella della data
			if (!isset($ar[$liv1_v]["dta"][$row[$field_data]]))
				$ar[$liv1_v]["dta"][$row["{$field_data}"]] = array("VOL"=>0, "COL"=>0, "ORD"=>0);
			if (!isset($ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]))
				$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]] = array("VOL"=>0, "COL"=>0, "ORD"=>0);	
			if ($liv==3 && !isset($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]))
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]] = array("VOL"=>0, "COL"=>0, "ORD"=>0);
			if ($liv==3 && !isset($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]))
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]] = array("VOL"=>0, "COL"=>0, "ORD"=>0);			

			$ar[$liv1_v]["dta"][$row[$field_data]]['VOL'] += $row['T_TDVOLU'];  
			$ar[$liv1_v]["dta"][$row[$field_data]]['COL'] += $row['T_TDTOCO'];
			$ar[$liv1_v]["dta"][$row[$field_data]]['PAL'] += $row['T_TDBANC'];			
			$ar[$liv1_v]["dta"][$row[$field_data]]['IMP'] += $row['T_TDTIMP'];
			$ar[$liv1_v]["dta"][$row[$field_data]]['ORD'] += $row['T_N_ORD'];
			$ar[$liv1_v]["dta"][$row[$field_data]]['ROW'] += $row['T_ROW'];			
			$ar[$liv1_v]["dta"][$row[$field_data]]['CP'] += $row['T_CP'];			
			$ar[$liv1_v]["dta"][$row[$field_data]]['ORD_CON_CAR'] += $row['T_CON_CARICO'];			
			$ar[$liv1_v]["dta"][$row[$field_data]]['ORD_EVASI']   += $row['T_ORD_EVASI'];			
			$ar[$liv1_v]["dta"][$row[$field_data]]['ORD_DATA_CONF'] += $row['T_DATA_CONFERMATA'];			
			$ar[$liv1_v]["dta"][$row[$field_data]]['ORD_DATA_TASS'] += $row['T_DATA_TASSATIVA'];			
						
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['VOL'] += $row['T_TDVOLU'];
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['COL'] += $row['T_TDTOCO'];
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['PAL'] += $row['T_TDBANC'];			
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['IMP'] += $row['T_TDTIMP'];
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['ORD'] += $row['T_N_ORD'];
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['ROW'] += $row['T_ROW'];
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['CP']  += $row['T_CP'];						
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['ORD_CON_CAR'] += $row['T_CON_CARICO'];			
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['ORD_EVASI'] += $row['T_ORD_EVASI'];			
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['ORD_DATA_CONF'] += $row['T_DATA_CONFERMATA'];
			$ar[$liv1_v]["val"][$liv2_v]["dta"][$row[$field_data]]['ORD_DATA_TASS'] += $row['T_DATA_TASSATIVA'];			
						
			if ($liv==3)
			{								
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['VOL'] += $row['T_TDVOLU'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['COL'] += $row['T_TDTOCO'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['PAL'] += $row['T_TDBANC'];			
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['IMP'] += $row['T_TDTIMP'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['ORD'] += $row['T_N_ORD'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['ROW'] += $row['T_ROW'];				
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['CP']  += $row['T_CP'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['ORD_CON_CAR'] += $row['T_CON_CARICO'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['ORD_EVASI']   += $row['T_ORD_EVASI'];				
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['ORD_DATA_CONF'] += $row['T_DATA_CONFERMATA'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['ORD_DATA_TASS'] += $row['T_DATA_TASSATIVA'];

				//disponibilita' spedizione
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['D_SPED_VOL'] = $row['CSVOLD'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['D_SPED_PES'] = $row['CSPESO'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["dta"][$row[$field_data]]['D_SPED_PAL'] = $row['CSBAND'];
				

				//LIV4				
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['VOL'] += $row['T_TDVOLU'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['COL'] += $row['T_TDTOCO'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['PAL'] += $row['T_TDBANC'];			
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['IMP'] += $row['T_TDTIMP'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['ORD'] += $row['T_N_ORD'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['ROW'] += $row['T_ROW'];				
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['CP']  += $row['T_CP'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['ORD_CON_CAR'] += $row['T_CON_CARICO'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['ORD_EVASI'] += $row['T_ORD_EVASI'];				
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['ORD_DATA_CONF'] += $row['T_DATA_CONFERMATA'];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["dta"][$row[$field_data]]['ORD_DATA_TASS'] += $row['T_DATA_TASSATIVA'];
				
			}			
			
			

			if ($row['T_CLI_BLOC'] > 0)
			{
				$ar[$liv1_v]["fl_bloc"] 	= 1;				
				$ar[$liv1_v]["val"][$liv2_v]["fl_bloc"] 	= 1;
				if ($liv==3){				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_bloc"] 	= 1;
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["fl_bloc"] 	= 1;					
				}				
			}
			if ($row['T_ORD_BLOC'] > 0)
			{
				$ar[$liv1_v]["fl_ord_bloc"] 	= 1;				
				$ar[$liv1_v]["val"][$liv2_v]["fl_ord_bloc"] 	= 1;
				if ($liv==3){				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_ord_bloc"] 	= 1;
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["fl_ord_bloc"] 	= 1;
				}				
			}			
			if ($row['T_ART_MANCANTI'] > 0)
			{
				$ar[$liv1_v]["fl_art_mancanti"] 	= 1;				
				$ar[$liv1_v]["val"][$liv2_v]["fl_art_mancanti"] 	= 1;
				if ($liv==3){				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_mancanti"] 	= 1;
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["fl_art_mancanti"] 	= 1;
				}				
			}
			
			//totali
			if (!isset($ar_tot[$row[$field_data]]))
				$ar_tot[$row[$field_data]] = array("VOL"=>0, "COL"=>0, "ORD"=>0);
			$ar_tot[$row[$field_data]]["VOL"] += $row['T_TDVOLU'];
			$ar_tot[$row[$field_data]]["COL"] += $row['T_TDTOCO'];
			$ar_tot[$row[$field_data]]["PAL"] += $row['T_TDBANC'];
			$ar_tot[$row[$field_data]]["IMP"] += $row['T_TDTIMP'];
			$ar_tot[$row[$field_data]]["ORD"] += $row['T_N_ORD'];			
			$ar_tot[$row[$field_data]]["ROW"] += $row['T_ROW'];			
						
		} //if tdswsp==Y
		
			//ordini da programmare (con tdswsp!=Y)
			if ($row['T_DA_PROG'] > 0 && trim($row['DP_NO_DP']) == '')
			{
				$ar[$liv1_v]["fl_da_prog"] 	= 1;				
				$ar[$liv1_v]["val"][$liv2_v]["fl_da_prog"] 	= 1;
				if ($liv==3){				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_da_prog"] 	= 1;
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["fl_da_prog"] 	= 1;
				}				
			}
			
			//Spedizioni da programmare (flight)
			if (trim($row['DP_NO_DP']) == 'DP'){
				
				//totale
				$ar_tot["DP"]["VOL"] += $row[$this->get_tot_field_by_field_dett("VOL")];				
				$ar_tot["DP"]["COL"] += $row[$this->get_tot_field_by_field_dett("COL")];
				$ar_tot["DP"]["PAL"] += $row[$this->get_tot_field_by_field_dett("PAL")];
				$ar_tot["DP"]["IMP"] += $row[$this->get_tot_field_by_field_dett("IMP")];
				$ar_tot["DP"]["ORD"] += $row[$this->get_tot_field_by_field_dett("ORD")];
				$ar_tot["DP"]["ROW"] += $row[$this->get_tot_field_by_field_dett("ROW")];
				
				$ar[$liv1_v]["DP"][$field_dett] 	+= $row[$this->get_tot_field_by_field_dett($field_dett)];
				$ar[$liv1_v]["val"][$liv2_v]["DP"][$field_dett] 	+= $row[$this->get_tot_field_by_field_dett($field_dett)];
				if ($liv==3){
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["DP"][$field_dett] 	+= $row[$this->get_tot_field_by_field_dett($field_dett)];
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["val"][$liv4_v]["DP"][$field_dett] 	+= $row[$this->get_tot_field_by_field_dett($field_dett)];
				}				
			}
					


		} //per ogni riga
		
	 return array("det"=>$ar, "tot"=>$ar_tot);		
	}
	
	



	public function crea_alberatura_calendario_json($ar, $field_dett, $id, $da_data, $n_giorni, $ar_righe_totali = null, $field_data = 'TDDTEP'){
		
		$giorni_festivi 	= $this->get_giorni_festivi($da_data, $n_giorni);
		$giorni_stadio      = $this->get_giorni_stadio($da_data, $n_giorni);
		$dispon_itinerari 	= $this->get_disponibilita_itinerari($field_data, $da_data, $n_giorni, true);
	
		if ($id == "root")
		{
		 $pre_id = "";
		 $liv = 1;			 
		 $ar1 = $ar["det"];
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$pre_id = $id;
		 	$sottostringhe = explode("|", $id);
			$liv1 = $sottostringhe[2];
			$liv2 = $sottostringhe[4];
			$liv = 3;
			$ar1 = $ar["det"][$liv1]["val"][$liv2]["val"];			
		 }	
		
		
		$txt_columns = "";
		$txt_stadio = "";
		$txt_header_qtip_h = "";
		$txt_header_qtip_t = "";				
		
		$da_data_txt = $da_data;

 		$oldLocale = setlocale(LC_TIME, 'it_IT');
		for ($i=1; $i<=14; $i++)
		{			
			$d = strtotime($da_data_txt);
			$txt_color_day = "<br><hr>";			
			$txt_columns .= "\"c{$i}\": \"". ucfirst(strftime("%a", $d)) . "<br>" . strftime("%d/%m", $d) . "\"";
			$txt_stadio .= "\"c{$i}\": \"". $giorni_stadio[date('Ymd', $d)] . "\"";			
			$txt_header_qtip_h .= "\"c{$i}\": ". j("Settimana " . strftime("%V", $d));			
			$txt_header_qtip_t .= "\"c{$i}\": " . j("[". $this->decod_stadio($giorni_stadio[date('Ymd', $d)]) . "]"
													. "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
													. "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
												);			
			if ($i<14){
				$txt_columns .= ", ";
				$txt_stadio .= ", ";
				$txt_header_qtip_h .= ", ";
				$txt_header_qtip_t .= ", ";
			}
			$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " +1 days"));			
		}
		setlocale(LC_TIME, $oldLocale);		
		
		
		
		$ret = '{"text":".", "columns": {' . $txt_columns . '}, "stadio": {' . $txt_stadio . '}, "header_qtip_h": {' . $txt_header_qtip_h . '}, "header_qtip_t": {' . $txt_header_qtip_t . '},
		              "toggle_fl_solo_con_carico_assegnato":'  . $this->ha_plan_filtro_ordini() . ' ,  
		              "field_dett": "'  . $field_dett . '" ,
		              "children": [';
		
		if ($id == "root"){
			//come prime righe esporto i totali

			
			global $mod_js_parameters;
			if (!isset($ar_righe_totali)){
				$ar_righe_totali = explode('|', $mod_js_parameters->tot_plan);
			}	
			
			
				$ret .= " {";
				$ret .= "id: 'tot_VOL',task: 'Totale Volume',liv: 'liv_totale',iconCls: 'task-folder',DP_val: " . j($ar["tot"]['DP']['VOL']) . ",leaf: true,"
			         . $this->json_out_ar_date(0, null, $ar["tot"], $da_data, $n_giorni, "VOL", $giorni_festivi, $dispon_itinerari) .
			         ""; 
				$ret .=" },";
				
				
				$ret .= " {";
				$ret .= "id: 'tot_COL',task: 'Totale Colli',liv: 'liv_totale',iconCls: 'task-folder',DP_val: " . j($ar["tot"]['DP']['COL']) . ",leaf: true,"
			         . $this->json_out_ar_date(0, null, $ar["tot"], $da_data, $n_giorni, "COL", $giorni_festivi, $dispon_itinerari); 
				$ret .=" },";				
				
				
			global $mod_js_parameters;
			if ($mod_js_parameters->v_pallet == 1){
				$ret .= " {";
				$ret .= "id: 'tot_PAL',task: 'Totale Pallet',liv: 'liv_totale',iconCls: 'task-folder',DP_val: " . j($ar["tot"]['DP']['PAL']) . ",leaf: true,"
			         . $this->json_out_ar_date(0, null, $ar["tot"], $da_data, $n_giorni, "PAL", $giorni_festivi, $dispon_itinerari); 
				$ret .=" },";
			}	
								
				
				
				$ret .= " {";
				$ret .= "id: 'tot_ORD'," .
			        "task: 'Totale Ordini'," .
			        "liv: 'liv_totale'," . 
			        "iconCls: 'task-folder'," .
			        "DP_val: " . j($ar["tot"]['DP']['ORD']) . "," .						
			        "leaf: true,"
			         . $this->json_out_ar_date(0, null, $ar["tot"], $da_data, $n_giorni, "ORD", $giorni_festivi, $dispon_itinerari); 
				$ret .=" },";
				

				if (in_array("IMP", $ar_righe_totali)) {				
					$ret .= " {";
					$ret .= "id: 'tot_IMP'," .
				        "task: 'Totale Importi (in migliaia)'," .
				        "liv: 'liv_totale'," .
				        "iconCls: 'task-folder'," .
			        	"DP_val: " . j($ar["tot"]['DP']['IMP']) . "," .							
				        "leaf: true,"
							. $this->json_out_ar_date(0, null, $ar["tot"], $da_data, $n_giorni, "IMP", $giorni_festivi, $dispon_itinerari);
					$ret .=" },";
				}				

				
				if (in_array("CP", $ar_righe_totali)) {					
					//costruisco l'array con i valori (per data)
					$ar_tot_CP = $this->get_ar_tot_CP($da_data, $n_giorni);
					
					$ret .= " {";
					$ret .= "id: 'tot_CP',task: 'Totale Colli produzione',liv: 'liv_totale',iconCls: 'task-folder',leaf: true,"
							. $this->json_out_ar_date(0, null, $ar_tot_CP, $da_data, $n_giorni, "CP", $giorni_festivi, $dispon_itinerari);
					$ret .=" },";
				}

				
				if (in_array("PR", $ar_righe_totali)) {
					//costruisco l'array con i valori (per data) -  CAPACITA' PRODUTTIVA
					$ar_tot_PR = $this->get_ar_tot_PR($da_data, $n_giorni);
					$style_cella = array();
					$qtip_cella  = array();
					foreach($ar_tot_PR as $kcpd => $cpd){
						
						if ($ar_tot_CP[$kcpd]['CP'] > 0 && $cpd['PR'] > 0)
						{
							$m_cp = $cpd['PR'];
							$m_colli = $ar_tot_CP[$kcpd]['CP'];
							$m_dif = $m_colli - $m_cp;
							$m_dif_perc = ($m_colli - $m_cp) / $m_cp * 100;
								
							$qtip_cella[$kcpd] = n($m_dif, 0, 'Y');							
							
							//Se e' stato impostato, mostro il valore "Esubero limite capacita' produttiva" (comprensivo straordinari, ...)
							global $mod_js_parameters;
							if ((int)$mod_js_parameters->esub_CP > 0){
								$esub_limite = (int)($m_cp + (int)$mod_js_parameters->esub_CP);
								
								//se e' stato impostato un aumento percentuale per WATER
								if ((int)$giorni_stadio[$kcpd] == 3) //Solo per water
									$esub_limite += $esub_limite*(int)$mod_js_parameters->ep_WT/100;
									
								$qtip_cella[$kcpd] .= "<br\>(Esubero limite: " . (int)($esub_limite) . ")";
							}
							
							if ($m_dif_perc >= 1)
								$style_cella[$kcpd] = "color: #f9827f; font-weight: bold;"; //rosso
							elseif ($m_dif_perc >= -1)
								$style_cella[$kcpd] = "font-weight: bold;"; //giallo su giallo non si vede (lascio in nero)
							else $style_cella[$kcpd] = "color: #465920; font-weight: bold;"; //verde							

						}
					}
					
						
					$ret .= " {";
					$ret .= "id: 'tot_PR',task: 'Totale Colli produzione programmabili',liv: 'liv_totale'," .
					        "iconCls: 'task-folder',leaf: true,"
							. $this->json_out_ar_date(0, null, $ar_tot_PR, $da_data, $n_giorni, "PR", $giorni_festivi, $dispon_itinerari, $style_cella, $qtip_cella);
					$ret .=" },";
				}

				
				
				if (in_array("PS", $ar_righe_totali)) {
					//costruisco l'array con i valori (per data) -  CAPACITA' SPEDIZIONE
					$ar_tot_PS = $this->get_ar_tot_PS($da_data, $n_giorni);
					$style_cella = array();
					$qtip_cella  = array();
					
					
					foreach($ar_tot_PS as $kcpd => $cpd){
						if ($ar["tot"][$kcpd]['COL'] > 0 && $cpd['PS'] > 0)
						{
							$m_cp = $cpd['PS'];
							$m_colli = $ar["tot"][$kcpd]['COL'];
							$m_dif = $m_colli - $m_cp;
							$m_dif_perc = ($m_colli - $m_cp) / $m_cp * 100;
				
							$qtip_cella[$kcpd] = n($m_dif, 0, 'Y');
								
							if ($m_dif_perc >= 1)
								$style_cella[$kcpd] = "color: #f9827f; font-weight: bold;"; //rosso
							elseif ($m_dif_perc >= -1)
								$style_cella[$kcpd] = "font-weight: bold;"; //giallo su giallo non si vede (lascio in nero)
							else $style_cella[$kcpd] = "color: #465920; font-weight: bold;"; //verde
				
						}
					}
						
				
					$ret .= " {";
					$ret .= "id: 'tot_PR',task: 'Totale Colli programmabili in spedizione',liv: 'liv_totale',"
					        . "iconCls: 'task-folder',leaf: true,"
							. $this->json_out_ar_date(0, null, $ar_tot_PS, $da_data, $n_giorni, "PS", $giorni_festivi, $dispon_itinerari, $style_cella, $qtip_cella);
					$ret .=" },";
				}				
				
				
				
				
				
				
		}
		

		if ($liv==3) { //LIVELLO SPEDIZIONE
			$icon_class = "iconSpedizione";


						
			/*if ($field_data == 'TDDTSP') //flight
			  $icon_class = "iconPartenze";
			else if ($field_data == 'TDDTEP') //plan
			  $icon_class = "iconFabbrica";*/			  			


		}
		else {
		  if ($field_data == 'TDDTSP') //flight 			
			$icon_class = "iconPartenze";
		  else if ($field_data == 'TDDTEP') //plan
		  	$icon_class = "iconFabbrica";
		  else			
		  	$icon_class = "task-folder";
		}		
				
			foreach ($ar1 as $r){
			    


			    if ($r["CSFG04"] == 'F')
			        $my_icon_class = "iconPartenze";


			    else
			        $my_icon_class = $icon_class;
			        
			    
				$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];			
				$ret .= " {";
				$ret .= "id: \"{$m_id}\",task:" . j(trim($r["txt"])) .",flag1: '{$r["FLAG1"]}',liv: \"liv_{$liv}\"," 
			        . "iconCls:'$my_icon_class',expanded: false,fl_bloc: '{$r["fl_bloc"]}',fl_ord_bloc: '{$r["fl_ord_bloc"]}',"
			        . "fl_art_mancanti: '{$r["fl_art_mancanti"]}',fl_da_prog: '{$r["fl_da_prog"]}',DP_val: " . j($r['DP'][$field_dett]) . ","
			        . $this->json_out_ar_date($liv, null, $r["dta"], $da_data, $n_giorni, $field_dett, $giorni_festivi, $dispon_itinerari) . 
			        ""
			         . $this->nodo_children_calendario($m_id, $liv +1, $r["val"], $da_data, $n_giorni, $field_dett, $giorni_festivi, $dispon_itinerari) . 
			        "
       			";			        				
				$ret .=" },"; 		
			}
			
						
		$ret .= ']}';
		return $ret;		
	}

	
	//TOTALE COLLI IN PRODUZIONE	
	private function get_ar_tot_CP($da_data_txt, $n_giorni, $ordini_da_escludere = null, $ordini_da_includere = null, $includi_HOLD = "N"){
		global $cfg_mod_Spedizioni, $conn;		
		$ret = array();

		//DRY: da print_riepilogo_programmate
		$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_righe']}
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
				";

		//se filtro per data, vado in JOIN con le spedizioni (praticamente escludo gli HOLD)
		if (!is_null($da_data_txt))
			$sql_FROM .= "
					INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
					    ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
					 ";
					    
		$sql_FROM .= "		    					    
					INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL ON
						TDDT = CAL.CSDT AND TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
		
							   ";
	   	$sql_WHERE = " WHERE " . $this->get_where_std() . "
							AND RDTPNO='COLLI' AND RDRIGA=0 AND RDRIFE = 'PRODUZIONE' ";
	   	
	   	if ($includi_HOLD == "Y")
	   		$sql_WHERE .= " AND 1=1 ";
	   	else
		   	$sql_WHERE .= " AND TDSWSP='Y' ";

	   	//filtro per data
	   	if (!is_null($da_data_txt)){
	   	
	   		$da_data 	= new DateTime($da_data_txt);
	   		$d = date("Ymd", strtotime($da_data_txt));
	   			
	   		$date_sql_ar = array();
	   		for ($i = 1; $i<= $n_giorni; $i++){
	   			$m_d = $i-1;
	   			$dd = date('Ymd', strtotime($da_data_txt . " +{$m_d} days"));
	   				
	   			$date_sql_ar[] = $dd;
	   		}
	   	 $sql_WHERE .= " AND CAL.CSDTRG IN (" . implode(', ', $date_sql_ar) . ") ";	   		
	   	}

	   	
	   	//escludo eventuali ordini
	   	if (!is_null($ordini_da_escludere) && count($ordini_da_escludere) > 0)
	   		$sql_WHERE .= " AND (TDONDO IS NULL OR TDONDO NOT IN (" . sql_t_IN($ordini_da_escludere) . ") )" ;

	   	//includo solo eventuali ordini
	   	if (!is_null($ordini_da_includere) && count($ordini_da_includere) > 0)
	   		$sql_WHERE .= " AND (TDONDO IN (" . sql_t_IN($ordini_da_includere) . ") )" ;
	   	
	   	
	   	$sql = "SELECT CAL.CSDTRG, SUM(RDQTA) AS S_QTA " . $sql_FROM . $sql_WHERE . " GROUP BY CAL.CSDTRG";

	   	$stmt = db2_prepare($conn, $sql);
	   	echo db2_stmt_errormsg();
	   	$result = db2_execute($stmt);
	   	 
	   	while ($r = db2_fetch_assoc($stmt)) {
	   		if (!is_null($da_data_txt))	   		
	   			$ret[$r['CSDTRG']]['CP'] = $r['S_QTA'];
	   		else
	   			$ret['TOT']['CP'] = (int)$ret['TOT']['CP'] + (int)$r['S_QTA'];	   			
	   	}

		return $ret;		
	}

	
	
	private function get_clor($ordini_da_includere = null){
		global $cfg_mod_Spedizioni, $conn;
		$ret = array();
	
		//DRY: da print_riepilogo_programmate
		$sql_FROM = "FROM {$cfg_mod_Spedizioni['file_testate']} TD";
		$sql_WHERE = " WHERE " . $this->get_where_std();
	
		//includo solo eventuali ordini
		//TODO: usare tddocu
		if (!is_null($ordini_da_includere) && count($ordini_da_includere) > 0)
		$sql_WHERE .= " AND (TDONDO IN (" . sql_t_IN($ordini_da_includere) . ") )" ;
	 
		$sql = "SELECT TDCLOR, COUNT(*) AS T_COUNT " . $sql_FROM . $sql_WHERE . " GROUP BY TDCLOR";
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
	
		while ($r = db2_fetch_assoc($stmt)) {
			$ret[$r['TDCLOR']] = (int)$r['T_COUNT'];
		}
	
	return $ret;
	}
	
	

	//TOTALE COLLI PRODUZIONE PROGRAMMABILI
	private function get_ar_tot_PR($da_data_txt, $n_giorni, $max_esubero = 'N'){
		global $cfg_mod_Spedizioni, $conn;
		$ret = array();
		$da_data 	= new DateTime($da_data_txt);
		$d = date("Ymd", strtotime($da_data_txt));
	
		$date_sql_ar = array();
		for ($i = 1; $i<= $n_giorni; $i++){
			$m_d = $i-1;
			$dd = date('Ymd', strtotime($da_data_txt . " +{$m_d} days"));
	
			$date_sql_ar[] = $dd;
		}
	
		$cp = new SpedCapacitaProduttiva;
		foreach($date_sql_ar as $data){
			$ret[$data]['PR'] = $cp->get_tot_by_data($data, 'PRODUZIONE');
			
			if ($max_esubero == 'Y'){
				$mod_js_parameters = $this->get_mod_parameters();
				if ((int)$mod_js_parameters->esub_CP != 0)
					$ret[$data]['PR'] += (int)$mod_js_parameters->esub_CP;
			}
			
			if ($ret[$data]['PR'] == 0) $ret[$data]['PR'] = null; 
		}
	 
		return $ret;
	}
	

	
	//TOTALE COLLI SPEDIZIONE PROGRAMMABILI
	public function get_ar_tot_PS($da_data_txt, $n_giorni, $max_esubero = 'N'){
		global $cfg_mod_Spedizioni, $conn;
		$ret = array();
		$da_data 	= new DateTime($da_data_txt);
		$d = date("Ymd", strtotime($da_data_txt));
	
		$date_sql_ar = array();
		for ($i = 1; $i<= $n_giorni; $i++){
			$m_d = $i-1;
			$dd = date('Ymd', strtotime($da_data_txt . " +{$m_d} days"));
	
			$date_sql_ar[] = $dd;
		}
	
		$cp = new SpedCapacitaProduttiva;
		foreach($date_sql_ar as $data){
			$ret[$data]['PS'] = $cp->get_tot_by_data($data, 'SPEDIZIONE');
				
			if ($ret[$data]['PS'] == 0) $ret[$data]['PS'] = null;
		}
	
		return $ret;
	}
		
	
	
	
	
	
	private function json_out_ar_date($liv, $itin, $ar, $da_data_txt, $n_giorni, $field_dett, $giorni_festivi = array(), $dispon_itinerari = array(), $style_cella = array(), $qtip_cella = array()){
		$ret = "";
		$da_data 	= new DateTime($da_data_txt);		
		$d = date("Ymd", strtotime($da_data_txt));
		
		$ret .= "da_data: " . $d . ",";
		
		for ($i = 1; $i<= $n_giorni; $i++){						
			$m_d = $i-1;
			$dd = date('Ymd', strtotime($da_data_txt . " +{$m_d} days"));
			
			$n_dec = 0;
			if ($field_dett == "PAL") $n_dec = 1;
				
			//floatval: rimuove gli 0 non significativi da decimali
			
			if (strlen($ar[$dd][$field_dett]) > 0)					
				$ret .= "d_{$i}: \"" . n_auto($ar[$dd][$field_dett],$n_dec,',','.') . "\",";
			if (isSet($giorni_festivi[$dd]))						
				$ret .= "d_{$i}_f: \"" . $giorni_festivi[$dd] . "\",";
			
			if (strlen(trim($qtip_cella[$dd])) > 0)
				$ret .= "d_{$i}_qtip: \"" . $qtip_cella[$dd] . "\",";
			

			if ( ($field_dett == 'PR' || $field_dett == 'PS') && strlen($ar[$dd][$field_dett]) > 0) //caso colli da prog (cap. produttiva o spedizione)					
				$ret .= "d_{$i}_t: \"" . $style_cella[$dd] . "\",";
			else if (strlen($ar[$dd][$field_dett]) > 0)
				$ret .= "d_{$i}_t: \"" . $this->out_tipo_carico($ar[$dd]) . "\",";

			//SOLO PER VOLUME
			if ($field_dett == 'VOL'){
			  //dispon. per itinerario			
				$ret .= "d_{$i}_d: \"" . n_auto($dispon_itinerari["{$dd}"]["{$itin}"][$field_dett], 1) . "\",";
			
  			  //dispon. per spedizione			
			  if (strlen($ar[$dd][$field_dett]) > 0 && $liv==3 && strlen($ar[$dd]["D_SPED_{$field_dett}"]) > 0)
				$ret .= "d_{$i}_d: \"" . n_auto($ar[$dd]["D_SPED_{$field_dett}"], 0) . "\",";
			}

		}			
	  return $ret;		
	}

	
	//coloro le celle in base ai carichi assegnati
	// 0 - nessun carico assegnato
	// 1 - alcuni carichi assegnati (grigio)
	// 2 - tutti i carichi assegnati (nero)
	// 3 - con date confermate
	// 4 - con consegna tassative
	private function out_tipo_carico($r){		

		if ($r['ORD_EVASI'] > 0) {
			if ($r['ORD_EVASI'] == $r['ROW']) return 6; //verde scuro -- tutto evaso
		 
			//2014-02-25: disabilito visualizzazione verde chiaro
			//return 5; //verde chiavo - solo alcuni evasi	
		}

		if ($r['ORD_CON_CAR'] == 0) {			
		  if ($r['ORD_DATA_CONF'] > 0)  return 3; 	
		  if ($r['ORD_DATA_TASS'] > 0)  return 4;		  
		 return 0;			
		}

		if ($r['ORD_CON_CAR'] == $r['ROW'])
		 return 2;
		return 1;
				
	}



    private function nodo_children_calendario($id_partenza, $liv, $ar, $da_data, $n_giorni, $field_dett, $giorni_festivi = array(), $dispon_itinerari = array()){
    	if ($liv > 4){
			return "leaf: true";
		}
				
				
    	$ret = "children: [ ";
    	foreach ($ar as $kr => $r){
    		
				if ($liv == 2) //livello itinerario
					$itin = $r["cod"];
				else $itin = null;				
			
				$m_id = $id_partenza . "|LIV{$liv}|" . $r["cod"];    		
				$ret .= " {";
				$ret .= "id: \"{$m_id}\", task:" . j(trim($r["txt"])) .", flag1: '{$r["FLAG1"]}', liv: \"liv_{$liv}\","			        
			        . "iconCls:'$icon_class',expanded: false,fl_bloc: '{$r["fl_bloc"]}',fl_ord_bloc: '{$r["fl_ord_bloc"]}',"
			        . "fl_art_mancanti: '{$r["fl_art_mancanti"]}',fl_da_prog: '{$r["fl_da_prog"]}',DP_val: " . j($r['DP'][$field_dett]) . ","
			         . $this->json_out_ar_date($liv, trim($itin), $r["dta"], $da_data, $n_giorni, $field_dett, $giorni_festivi, $dispon_itinerari);
				
				if ($liv == 4)
				  $ret .= "leaf: true";
							        				
				$ret .=" },";
    	}
		$ret .= " ]";
	 return $ret;	
    }

	
	
	
	
	
	
	
	
	
	














	public function crea_alberatura_json($its, $field_dett, $id){
	
		$liv1_c = $liv2_c = $liv3_c = 0;
		$liv1_v = $liv2_v = $liv3_v = "--------------";
		
		
		if ($id == "root")
			$liv = 1;
		else
			$liv = 3;

		$oldLocale = setlocale(LC_TIME, 'it_IT');
		while ($row = db2_fetch_assoc($its)) {
			
			$row['C_LIV3'] = $row['CSDTSP'] . "-" . $row['CSPROG'] . "-" . trim($row['CSCVET']) . "-" . trim($row['CSCAUT']) . "-" . trim($row['CSCCON']);
			
						
			if ($row['C_LIV1'] != $liv1_v){
				$liv1_c += 1;
				$liv2_c = $liv3_c = 0;
				$liv2_v = $liv3_v = "-----------";
				$liv1_v = $row['C_LIV1'];
				if (!isset($ar[$liv1_v]))
					$ar[$liv1_v] = array("cod" => $row['C_LIV1'], "task"=> $this->decod_std('ASPE', $row['C_LIV1']), "iconCls"=>"task-folder", "children"=>array()); 
			}
			if ($row['C_LIV2'] != $liv2_v){
				$liv2_c += 1;
				$liv3_c = 0; $liv3_v = "-----------";								
				$liv2_v = $row['C_LIV2'];
				
				$row['D_LIV2'] = $this->decod_std('ITIN', trim($row['C_LIV2']));				
				
				if (!isset($ar[$liv1_v]["children"][$liv2_v]))				
					$ar[$liv1_v]["children"][$liv2_v] = array("cod"=>$row['C_LIV2'], "task"=>$row['D_LIV2'], "children"=>array()); 
			}			
			if ($liv==3 && $row['C_LIV3'] != $liv3_v){
				$liv3_c += 1;								
				$liv3_v = $row['C_LIV3'];
				$m_txt = "";
				if (strlen($row['CSDTSP']) == 8){
					$dd = date('D d/m', strtotime($row['CSDTSP']));
					$dd = ucfirst(strftime("%a %d/%m", strtotime($row['CSDTSP'])));
					$dd .= " " . print_ora($row['CSHMPG']);					 
					$m_txt = $dd;
				}
				
				if (!isset($ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]))
				{ 
					$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v] = array("cod"=>$row['C_LIV3'], "task"=>trim($m_txt));
					$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['dd'] = $dd;
					$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['txt'] = $this->decod_std('AUTR', trim($row['CSCVET'])) . " " . $this->decod_std('AUTO', trim($row['CSCAUT'])) . " " . $this->decod_std('COSC', trim($row['CSCCON']));					
				}
				
				$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['assegnato'] += $row['TOT_F'];
				$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['disponibile'] += $row['DISP_F'];
			}			
									
		}
		setlocale(LC_TIME, $oldLocale);	


	
	
		if ($id == "root")
		{
		 $pre_id = "";			 
		 $ar = $ar;
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$pre_id = $id;
		 	$sottostringhe = explode("|", $id);
			$liv1 = $sottostringhe[2];
			$liv2 = $sottostringhe[4];
			$ar = $ar[$liv1]["children"][$liv2]["children"];
			//$ar = array(); //riprendo tutte le spedizioni dalla tabella spedizioni //TODO: essere sicuri che ci siano!!			
		 }	


		
		switch($this->imposta_field_dett()){
			case "VOL": $tipo_sum = "V: "; break;
			case "ORD": $tipo_sum = "O: ";  break;
			case "COL": $tipo_sum = "C: "; break;
			case "PAL": $tipo_sum = "P: "; break;
			case "IMP": $tipo_sum = "I: "; break;			
			default: $tipo_sum = ""; break;
		}
		
		if ($liv==3)
			$icon_class = "iconSpedizione";
		else
			$icon_class = "task-folder";		
		
		$ret = '{"text":".","children": [';
		
			foreach ($ar as $r){
				
				$m_txt = $r['task'];
				
				if ($liv==3){
					$m_txt = $r['dd'];
					//$m_txt .=  " (". number_format($r['assegnato'], 0) . "/" . $r['disponibile'] . ") ";
					$m_txt .=  " (". $tipo_sum .  number_format($r['assegnato'], 0)  . "/" . number_format($r['disponibile'], 0) . ") ";
					$m_txt .= $r['txt'];
				} 
				
				$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];			
				$ret .= " {";
				$ret .=
					"id: \"{$m_id}\"," .
					"liv: \"liv_{$liv}\"," .
			        "task: " . j($m_txt) . "," .
			        "iconCls:'$icon_class'," .
			        "expanded: false,"
			         . $this->nodo_children($m_id, $liv +1, $r["children"]);			        				
				$ret .=" },"; 		
			}
			
						
		$ret .= ']}';
		return $ret;		
	}






	public function crea_alberatura_json_per_data($its, $field_dett, $id, $separa_per_settimana = 'N'){
	
		$liv1_c = $liv2_c = $liv3_c = 0;
		$liv1_v = $liv2_v = $liv3_v = "--------------";	

		if ($id == "root")
			$liv = 1;
		else
			$liv = 3;
		
		$oldLocale = setlocale(LC_TIME, 'it_IT');		
		$c_row = 0;
		while ($row = db2_fetch_assoc($its)) {
			$c_row++;
			$row['C_LIV1'] = $row['CSAARG'] . $row['CSNRSE'];
			$row['D_LIV1'] = "Settimana " . $row['CSNRSE'] . " / " . $row['CSAARG'];
			
			$row['C_LIV2'] = $row['CSDTSP'];
			$row['D_LIV2'] = $row['CSDTSP'];			
			$row['C_LIV3'] = $row['CSCITI'] . "-" . $row['CSCVET'];
			$row['D_LIV3'] = trim($row['D_ITIN']);			
			$row['D_LIV3_txt'] = $this->decod_std('AUTR', trim($row['CSCVET'])) . " " . $this->decod_std('AUTO', trim($row['CSCAUT'])) . " " . $this->decod_std('COSC', trim($row['CSCCON']));
			
			if ($row['C_LIV1'] != $liv1_v){
				$liv1_c += 1;
				$liv2_c = $liv3_c = 0;
				$liv2_v = $liv3_v = "-----------";
				$liv1_v = $row['C_LIV1'];
				$ar[$liv1_v] = array("cod" => $row['C_LIV1'], "task"=>$row['D_LIV1'], "iconCls"=>"task-folder", "children"=>array()); 
			}
			if ($row['C_LIV2'] != $liv2_v){
				$liv2_c += 1;
				$liv3_c = 0; $liv3_v = "-----------";								
				$liv2_v = $row['C_LIV2'];
				$m_txt = "";
				$m_week = "";
				if (strlen($row['C_LIV2']) == 8){

					$dd = date('D d/m', strtotime($row['C_LIV2']));
					$dd = ucfirst(strftime("%a %d/%m", strtotime($row['C_LIV2']))); 
					$m_txt = $dd;
					$m_week = strftime("%V / %G", strtotime($row['C_LIV2']));
				}				
				$ar[$liv1_v]["children"][$liv2_v] = array("cod"=>$row['C_LIV2'], "task"=>$m_txt, "week"=>$m_week, "children"=>array()); 
			}			
			if ($liv==3 && $row['C_LIV3'] != $liv3_v){
				$liv3_c += 1;								
				$liv3_v = $row['C_LIV3']; 
				$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v] = array("cod"=>$row['C_LIV3'], "task"=>trim($row['D_LIV3']));
				$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['txt'] = trim($row['D_LIV3_txt']);
				
				$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['assegnato'] += $row['TOT_F'];
				$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['disponibile'] += $row['DISP_F'];				
			}			
									
		}
		
		global $appLog;
		$appLog->add_bp("while row ({$c_row}) - END");		
		
		setlocale(LC_TIME, $oldLocale);		
	
		if ($id == "root")
		{
		 $pre_id = "";
		 $liv = 1;			 
		 $ar = $ar;
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$pre_id = $id;
		 	$sottostringhe = explode("|", $id);
			$liv1 = $sottostringhe[2];
			$liv2 = $sottostringhe[4];
			$liv = 3;
			$ar = $ar[$liv1]["children"][$liv2]["children"];
		 }	
		
		switch($this->imposta_field_dett()){
			case "VOL": $tipo_sum = "V: "; break;
			case "ORD": $tipo_sum = "O: ";  break;
			case "COL": $tipo_sum = "C: "; break;
			case "PAL": $tipo_sum = "P: "; break;
			default: $tipo_sum = ""; break;
		}		
		

		if ($liv==3)
			$icon_class = "iconSpedizione";
		else
			$icon_class = "task-folder";		
		
		
		
		$ret = '{"text":".","children": [';
		
			foreach ($ar as $r){
				$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];
								
				$m_txt = $r['task'];
				if ($liv==3){					
					//$m_txt .=  " (". number_format($r['assegnato'], 0) . "/" . $r['disponibile'] . ") ";
					$m_txt .=  " (". $tipo_sum .  number_format($r['assegnato'], 0) . "/" . number_format($r['disponibile'], 0) . ") ";
					$m_txt .= $r['txt'];
				}				
				
							
				$ret .= " {";
				$ret .= "id: \"{$m_id}\"," .
			        "task: " . j($m_txt) . "," .
			        "iconCls:'$icon_class'," .
			        "expanded: false,"
			         .  $this->nodo_children($m_id, $liv +1, $r["children"], $separa_per_settimana);			        				
				$ret .=" },"; 		
			}
			
						
		$ret .= ']}';
		return $ret;		
	}





    private function nodo_children($id_partenza, $liv, $ar, $separa_per_settimana = "N"){
    	if ($liv > 3){
			return "leaf: true";
		}
		
    	$ret = "children: [ ";
		
		$cur_week = -9999999;
    	foreach ($ar as $kr => $r){
				$m_id = $id_partenza . "|LIV{$liv}|" . $r["cod"];
			    		
				$ret .= " {";
				$ret .="id: \"{$m_id}\"," .
					"liv: \"liv_{$liv}\"," .					
			        "task: " . j($r["task"]) . "," .
			        "iconCls:'task-folder'," .
			        "expanded: false";			        				
				$ret .=" },";
    	}
		$ret .= " ]";
	 return $ret;	
    }
	
    







/* *********************************************************************************************/
/* ALBERATURA PER ELENCO ORDINI - SEARCH */
/* *********************************************************************************************/

    public function crea_array_valori_el_ordini_search($its, $tipo_elenco = "", $nr_ord_sel = "N", $f_subtotale_prov_d = false){		
        global $auth, $cfg_mod_Spedizioni, $cfg_info_columns_properties, $info_importo_field, $conn, $id_ditta_default;
		$js_parameters = $auth->get_module_parameters($this->get_cod_mod());
		
		if (isset($info_importo_field)==false || strlen($info_importo_field) == 0)
			$info_importo_field = 'TDTIMP';
		
		$ar = array();
		$ar_tot = array();		
		$liv1_c = $liv2_c = $liv3_c = 0;
		$liv1_v = $liv2_v = $liv3_v = "--------------";
				
		while ($row = db2_fetch_assoc($its)) {
			
			if ($tipo_elenco == 'HOLD')	$row['TDDTEP'] = 0;
			
			
			if ($f_subtotale_prov_d == 'Y'){ //stacco anche per provincia
			 $m_liv1 = implode("___", array($row['TDASPE'], $row['TDCITI'], $row['TDPROD']));
			} else {
			  $m_liv1 = implode("___", array($row['TDASPE'], $row['TDCITI']));
			}
			$m_liv2 = implode("___", array($row['TDCCON'], $row['TDCDES'], $row['TDTDES']));

			$m_stacco_liv0_c = 'TOTALE';
			$m_stacco_liv0_d = 'TOTALE';			
			
			if ($m_liv1 != $liv1_v){
				$liv1_c += 1;
				$liv2_c = $liv3_c = 0;
				$liv2_v = $liv3_v = "-----------";
				$liv1_v = $m_liv1;

				$liv1_vt = $this->decod_std('ITIN', $row['TDCITI']);		
				
				if ($f_subtotale_prov_d == 'Y')
				    $liv1_vt .= " [" . trim($row['TDPROD']) . "]";
				
				if (is_null($ar[$liv1_v]))
					$ar[$liv1_v] = array("cod"=>$liv1_v, "txt"=> $liv1_vt, "dta"=>array(), "val"=>array());
				
				$ar[$liv1_v]["stacco_liv0_c"]  = $m_stacco_liv0_c;
				$ar[$liv1_v]["stacco_liv0_d"]  = $m_stacco_liv0_d;				
				
				//$ar[$liv1_v]["id_spedizione"] = $row['TDNBOC'];
				//$ar[$liv1_v]["n_carico"] 	  = $row['TDNRCA'];
								
			}
			if ($m_liv2  != $liv2_v){
				$liv2_c += 1;
				$liv3_c = 0; $liv3_v = "-----------";								
				$liv2_v = $m_liv2;
				if (is_null($ar[$liv1_v]["val"][$liv2_v]))
					$ar[$liv1_v]["val"][$liv2_v] = array("cod"=>$liv2_v, "txt"=> trim($row['TDDCON']), "dta"=>array(), "val"=>array());
												
				$ar[$liv1_v]["val"][$liv2_v]['k_cli_des'] = implode("_", array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));				
				
				$ar[$liv1_v]["val"][$liv2_v]["riferimento"]	= $this->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
				
				//se ha note cliente o destinazione
				    if ($this->ha_note_cliente($row['TDDT'], $row['TDCCON']) ||
				        $this->ha_note_cliente_dest($row['TDDT'], $row['TDCCON'], $row['TDCDES'])
				        )
				        $ar[$liv1_v]["val"][$liv2_v]["priorita"] = '<span><a href="javascript:show_win_note_cliente(\'' . $row['TDDT'] . '\', \'' . $row['TDCCON'] . '\', \'' . $row['TDCDES'] . '\')";><img class="cell-img" src=' . img_path("icone/16x16/blog_compose.png") . '></a></span>';
				
		        //icona mezzi inclusi esclusi
		        if ($this->ha_mezzi($row['TDCCON'], $row['TDCDES'])){
		            $ar[$liv1_v]["val"][$liv2_v]["cons_rich"] = '<img class="cell-img" src=' . img_path("icone/16x16/spedizione.png") . '>';
		            $ar_tooltip_mezzi = $this->get_tooltip_mezzi($row['TDCCON'], $row['TDCDES']);
		            $ar[$liv1_v]["val"][$liv2_v]["tooltip_mezzi"] = $ar_tooltip_mezzi['tooltip'];
		        }
				
				//se scarico intermedio o sosta tecnica
				if ($row['TDTDES'] == '1' || $row['TDTDES'] == '2' || $row['TDTDES'] == 'T'){
					$ar[$liv1_v]["val"][$liv2_v]["scarico_intermedio"] = 1;
					if ($row['TDFN18'] == '1') //coincide con quello indicato in anagrafica cliente
 						$ar[$liv1_v]["val"][$liv2_v]["riferimento"] = '<img class="cell-img" src=' . img_path("icone/16x16/exchange_black.png") . '>&nbsp;&nbsp;' . $ar[$liv1_v]["val"][$liv2_v]["riferimento"];
					else
						$ar[$liv1_v]["val"][$liv2_v]["riferimento"] = '<img class="cell-img" src=' . img_path("icone/16x16/exchange.png") . '>&nbsp;&nbsp;' . $ar[$liv1_v]["val"][$liv2_v]["riferimento"];					
				}					
								
				//icona ordine di reso
				if ($this->ha_con_ordini_reso($row['TDDT'], $row['TDCCON'], $row['TDCDES']))
					$ar[$liv1_v]["val"][$liv2_v]["tipo"] = '<img class="cell-img" src=' . img_path("icone/16x16/recycle_bin.png") . '>';
					
				$ar[$liv1_v]["val"][$liv2_v]["gmap_ind"]	= implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));
				$ar[$liv1_v]["val"][$liv2_v]["seq_carico"]	= $row['TDSECA'];	
				$ar[$liv1_v]["val"][$liv2_v]["cliente"]		= $row['TDDCON'];
				
				 
			}			
			
			$documento_cambiato = false;
			if ($row['TDDOCU'] != $liv3_v){
				$liv3_c += 1;								
				$liv3_v = "{$row['TDDOCU']}";
				$liv3_txt = trim($row['TDOADO']) . "_" . trim($row['TDONDO']) . " " . trim($row['TDMODI']);
				
				if ($liv3_txt != $old_liv3_txt) {
					$documento_cambiato = true;
					$old_liv3_txt = $liv3_txt;
				}				
				
				if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
				    if(trim($row['TDFG08']) == 'R')
					   $liv3_txt .= " Riga {$row['TDPROG']}";
				}
				

				//ordine bloccato autorizzato (divieto green)
				if ($row["TDBLOC"] == 'A')
					$liv3_txt .= '&nbsp;<img class="cell-img" style="margin-top: 2px;" src=' . img_path("icone/16x16/divieto_green.png") . '>';					
				
				//icona ordine abbinato
				if (trim($row['TDABBI']) > 0)
				  $liv3_txt .= '&nbsp;&nbsp;&nbsp;<img class="cell-img" src=' . img_path("icone/16x16/link.png") . '>';
				  
				  $ha_commenti = $this->has_commento_ordine_by_k_ordine($this->k_ordine_td($row));
				if ($ha_commenti == FALSE)
					$img_com_name = "icone/16x16/comment_light.png";
				else
					$img_com_name = "icone/16x16/comment_edit_yellow.png";
				
				
				//icona commento ordine
				$liv3_txt .= '&nbsp;<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine(\'' . $row['TDDOCU'] . '\', \'' . $liv1_v . '\', \'' . $liv2_v . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
				
				if (trim($row['TDFN20']) == 1)
				    $liv3_txt .= '<span style="display: inline; float: right;"><img class="cell-img" src=' . img_path("icone/16x16/currency_black_pound.png") . '></span>';
				
				//icona entry (per attivita' in corso sull'ordine)
				$sa = new SpedAssegnazioneOrdini(); 
				if ($sa->ha_entry_aperte_per_ordine($row['TDDOCU'], 'Y')){
					$img_entry_name = "icone/16x16/arrivi_gray.png";				
					$liv3_txt .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><a href="javascript:show_win_entry_ordine(\'' . $row['TDDOCU'] . '\', \'' . $liv1_v . '\', \'' . $liv2_v . '\')";><img class="cell-img" src=' . img_path($img_entry_name) . '></a> </span>';
				}					

				//icona riprogrammazione (se e' stato riprogrammato)
				$sr = new SpedAutorizzazioniDeroghe();
				if ($sr->ha_RIPRO_by_k_ordine($row['TDDOCU'])){
					$img_entry_name = "icone/16x16/button_black_repeat_dx.png";
					$liv3_txt .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';
				}
				
				
				if (is_null($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]))
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v] = array("cod"=>$liv3_v,
						"txt"=> $liv3_txt,
						"dta"=>array()); 
				
				if ($sr->ha_RIPRO_by_k_ordine($row['TDDOCU'])){
					$ar_tooltip_ripro = $sr->tooltip_RIPRO_by_k_ordine($row['TDDOCU']);						
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tooltip_ripro"] = "<B>RIPROGRAMMAZIONI:</B><br>" . implode("<br>", $ar_tooltip_ripro);
				}
				
				
				//icona: ha deroghe?
				if ($sr->ha_deroghe_by_k_ordine($row['TDDOCU'])){
					$img_entry_name = "icone/48x48/unlock.png";
						
					$ar_deroghe = array();
					$array_deroghe = $sr->array_deroghe_by_k_ordine($row['TDDOCU']);
					foreach ($array_deroghe as $r_deroga){
						$ar_deroghe[] = "> [" . trim($r_deroga['ADUSRI']) . "] " . print_date($r_deroga['ADDTDE']) . " - " . trim($this->decod_std('AUDE', $r_deroga['ADAUTOR'])) . ": " .  acs_u8e(trim($r_deroga['ADCOMM']));
					}
					$tooltip_ha_deroghe = implode("<br>", $ar_deroghe);
					
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['txt'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img width=16 class="cell-img" src=' . img_path($img_entry_name) . '></span>';					
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['tooltip_ripro'] .= "<br><br><B>DEROGHE:</B><br>" . $tooltip_ha_deroghe;
				}
				
				
				if(trim($row['TDFU03']) != ''){
				    $img_entry_name = "icone/16x16/telephone_blue.png";
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['txt'] .= '<span style="display: inline; float: right; padding-right: 5px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['tooltip_ripro'] .= "<br>[".trim($row['TDFU03'])."]";
				}
				
			}			
		
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_carico"] 	= $this->k_carico_td($row);	
			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDSWPP"] 	= $row['TDSWPP'];
			
			if ($documento_cambiato == true)
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] =  trim($row['TDVSRF']);
			
			if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
				if ($documento_cambiato == true) {
					if (strlen(trim($row['TDVSRF'])) == 0) //se rif e' vuoto per evidenziare il cambio documento ripeto anno-numero
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] = trim($row['TDOADO']) . "_" . trim($row['TDONDO']) . " " . trim($row['TDMODI']);						
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] .= "<BR>";
				}
				if(strlen(trim($row['RDART'])) > 0){
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] .= "<b>" . " [" . trim(acs_u8e($row['RDART'])) . "] " . trim(acs_u8e($row['RDDES1'])) . "</b>";
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["articolo"] = trim(acs_u8e($row['RDART']));
				    }
			}
			
			if ($cfg_info_columns_properties['info_show_vettore'] == 'Y'){
			    
			    if($row['VETTORE'] != '')
			     $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["vettore"] = trim($row['VETTORE']);
			    else
			     $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["vettore"] = trim($row['TRASPORTATORE']);
		    }
				
		    if ($cfg_info_columns_properties['info_show_architetto'] == 'Y'){
		        $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["architetto"] = trim($row['ARCHITETTO']);
		    }
			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["nr"] 			=  $row['TDONDO'];
			//$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_ordine"]	=  $this->k_ordine_td($row);			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_ordine"]	=  trim($row['TDDOCU']);
			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["sped_id"]	  	= $row['TDNBOC'];
			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_cli_des"]	=  trim($ar[$liv1_v]["val"][$liv2_v]['k_cli_des']);			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cod_iti"]		=  trim($row['TDCITI']);
						
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tipo"] 		=  trim($row['TDOTPD']);			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["qtip_tipo"] 	=  trim($row['TDDOTD']);			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["raggr"] 		=  trim($row['TDCLOR']);	
			if(trim($row['TDSWPP']) == 'T')
			    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["raggr"] 	=  trim($row['TDSWPP']);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["priorita"]	=  trim($row['TDOPRI']);			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["qtip_pri"]	=  trim($row['TDDPRI']);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"]		=  trim($this->get_tp_pri(trim($row['TDOPRI']), $row));			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_reg"]	=  $row['TDODRE'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_rich"]	=  $row['TDODER'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_prog"]	=  $row['TDDTEP'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tdfg06"]	=  trim($row['TDFG06']);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["proposte"]	= trim($row['TDFG07']);
						
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDFN11"]		=  $row['TDFN11']; //evaso			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDFN19"]		=  $row['TDFN19']; //evaso parziale
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_cap"]		=  $row['TP_CAP']; //tipo pagamento garanzia (TAFG01)
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDSWSP"]		=  $row['TDSWSP']; //non programmabile
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDBLOC"]		=  $row['TDBLOC'];

			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["n_carico"] 	= $row['TDNRCA'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["gg_rit"] 	  	= $row['TDGGRI'];			
			
			$stato_pagamento_ordine = $this->get_stato_pagamento_ordine($row['TDDOCU'], $row['TDFG06']);
			$icona = $this->gest_image_pagamento($stato_pagamento_ordine);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["i_paga"] 	= $icona;
/*			
			if ($tipo_elenco == 'HOLD')
				//data disponibilita'
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTDS'];
			else			
				//data confermata
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTVA'];
*/				
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTVA'];			
				
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_cons_conf"]	=  $row['TDFN06'];

			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_da_prog"]	=  $row['TDFN04'];			
							
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_evaso"]	=  $row['TDFN11'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["stato"]		=  $row['TDSTAT'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["qtip_stato"]  =  $row['TDDSST'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["volume"] 		+= $row['TDVOLU'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["peso"] 		+= $row['TDPLOR'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["importo"]		+= $row[$info_importo_field];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["pallet"]		+= $row['TDBANC'];
			
			
			$m_caparra = $this->get_caparra_ordine($row['TDDOCU']);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["caparra"]	= number_format($m_caparra, 0, ',', '');
			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli"] 		+= $row['TDTOCO'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli_disp"] 	+= $row['TDCOPR'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli_sped"] 	+= $row['TDCOSP'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_disp"] 	= $row['TDDTDS'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_conf_ord"] = $row['TDDTCF'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_sped"] 	= $row['TDDTSP'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["stato_sped"] 	= trim($row['CSSTSP']);
			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["nr_fatt"] 	= $row['TDNRFA'];
			//pulisco 'DP' se ordine evaso
			if ($row['TDFN11'] == 0 && $row['CSSTSP'] == 'DP')
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["stato_sped"]  = '';

		

			//icona bandiera_gray (flag speciale)
			if (strlen(trim($row['TDCODO'])) > 0){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["seq_carico"]	= "S"; //attivo bandiera_grey
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tooltip_seq_carico"]	= $row['TDDCDO']; //tooltip
			}
			
			//icona sticker_black - indice rottura
			if (strlen(trim($row['TDIRLO'])) > 0){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["seq_carico"]	.= "I"; //indice rottura
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tooltip_seq_carico"]	.= '<br/>Indice rottura: ' . trim($row['TDIRLO']); //tooltip
			}
			
			if($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["seq_carico"] == '' && in_array(trim($row['TDOWEB']), array('Y', 'C')))
			    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["seq_carico"] = trim($row['TDOWEB']);
			
			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["contract"] = $row['TDCOST']; //contratto
			
			//data consegna cliente (la riporto sulla spedizione)			
			if ($row['TDDTCP'] >= $row['TDDTEP'])
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_cons_cli"] = $row['TDDTCP'];

			//se impostata visualizza la data di scarico al cliente (calcolata in base alle sequenza di scarico sulla spedizione)
			if ($row['TDDTIS'] > 0)
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_cons_cli"] = $row['TDDTIS'];
			
			
			
			//cliente bloccato (basta che sia indicato in almeno un ordine) 
			if ($row['TDCLBL'] == 'Y' || $row['TDCLBL'] == 'E' || $row['TDCLBL'] == 'P')
			{
				$ar[$liv1_v]["val"][$liv2_v]["fl_cli_bloc"] = 1;
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_cli_bloc"] = 1;				
			}			
			
			
			//blocco amministrativo (TDBLEV) e/o commerciale (TDBLOC)
			//fl_bloc:
			// 4 - amministrativo e commerciale
			// 3 - amministrativo
			// 2 - commerciale
			// 1 - sbloccato
			
			$m_tipo_blocco = null;
			if ($row['TDBLEV'] == 'Y' || $row['TDBLOC'] == 'Y')
			{
				if ($row['TDBLEV'] == 'Y' && $row['TDBLOC'] == 'Y')	$m_tipo_blocco = 4;
				else {
					if ($row['TDBLEV'] == 'Y') $m_tipo_blocco = 3;
					else if ($row['TDBLOC'] == 'Y') $m_tipo_blocco = 2;
				}

				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_bloc"] 	= $m_tipo_blocco;				
				
				//riporto a livello superiore
				$ar[$liv1_v]["fl_bloc"] 				= $this->get_fl_bloc($ar[$liv1_v]["fl_bloc"], $m_tipo_blocco);				
				$ar[$liv1_v]["val"][$liv2_v]["fl_bloc"] = $this->get_fl_bloc($ar[$liv1_v]["val"][$liv2_v]["fl_bloc"], $m_tipo_blocco);				
			}
			
			
			if (($row['TDBLEV'] != 'Y' && $row['TDBLOC'] != 'Y') && ($row['TDBLEV'] == 'S' || $row['TDBLOC'] == 'S')) //sbloccato
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_bloc"] 	= 1;
			
				
			
			if (trim($row['TDFG01']) == 'A' || trim($row['TDFG01']) == 'R') //indice modifica (icona + su new)
			{
				$ar[$liv1_v]["ind_modif"] = trim($row['TDFG01']);				
				$ar[$liv1_v]["val"][$liv2_v]["ind_modif"] 	= trim($row['TDFG01']);
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["ind_modif"]	=  trim($row['TDFG01']);								
			}
			
			
			//Icona NEWS - INFO 
			//non riporto l'icona nei livelli superiori
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"] = $this->get_fl_new($row);
			
			if(trim($row['TDOWEB']) == 'C')
			    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"] = 'C';
			
			//passo a livello superiore il tipo priorita
			$ar[$liv1_v]["tp_pri"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"], $ar[$liv1_v]["tp_pri"]);
			$ar[$liv1_v]["val"][$liv2_v]["tp_pri"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"], $ar[$liv1_v]["val"][$liv2_v]["tp_pri"]);

			
			/* ***** ICONA ARTICOLI MANCANTI (INFO_BLUE) ****************/			
			$tmp_fl_art_manc = $this->get_fl_art_manc($row);
			$ar[$liv1_v]["fl_art_manc"] = max($ar[$liv1_v]["fl_art_manc"], $tmp_fl_art_manc);
			$ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"] = max($ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"], $tmp_fl_art_manc);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"], $tmp_fl_art_manc);
			
	
			

			
			/* ***** ICONA ARTICOLI DA PROGRAMMARE (INFO_BLACK) ****************/
			if ($row['TDFMTS'] == 'Y')			
			{
				if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"				
					$ar[$liv1_v]["art_da_prog"] 	= 1;				
					$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] 	= 1;				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] 	= 1;
				}
			}
			if ($row['TDFN15'] == 1){
				if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
					$ar[$liv1_v]["art_da_prog"] 	= 2;
					//a livello superiore ha priorita info_black (1)
					if ($ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] != 1)
						$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] 	= 2;
					if ($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] != 1)
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] 	= 2;						
				}								
			}

			
			//background non allineato con order entry (come caso 1),
			//pero' in questo ordine non ci sono articoli che si stanno controllando in bg
			if ($row['TDFN15'] == 2){
				if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
					$ar[$liv1_v]["art_da_prog"] 	= 3;
					//a livello superiore
					if ((int)$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] == 0 || (int)$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] > 2)
						$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] 	= 3;
					if ((int)$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] == 0 || (int)$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] > 2)
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] 	= 3;
				}
			}
							
		
						

			
			
			
			//somme parziali
			$ar[$liv1_v]["val"][$liv2_v]["colli"] 		+= $row['TDTOCO'];
			$ar[$liv1_v]["val"][$liv2_v]["pallet"] 		+= $row['TDBANC'];			
			$ar[$liv1_v]["val"][$liv2_v]["colli_disp"] 	+= $row['TDCOPR'];						
			$ar[$liv1_v]["val"][$liv2_v]["colli_sped"] 	+= $row['TDCOSP'];			
			$ar[$liv1_v]["val"][$liv2_v]["volume"] 		+= $row['TDVOLU'];									
			$ar[$liv1_v]["val"][$liv2_v]["peso"] 		+= $row['TDPLOR'];			
			$ar[$liv1_v]["val"][$liv2_v]["importo"] 	+= $row[$info_importo_field];			
			$ar[$liv1_v]["val"][$liv2_v]["fl_evaso"] 	+= $row['TDFN11'];			
					
			$ar[$liv1_v]["colli"] 		+= $row['TDTOCO'];			
			$ar[$liv1_v]["pallet"] 		+= $row['TDBANC'];			
			$ar[$liv1_v]["colli_disp"]	+= $row['TDCOPR'];			
			$ar[$liv1_v]["colli_sped"]	+= $row['TDCOSP'];			
			$ar[$liv1_v]["volume"] 		+= $row['TDVOLU'];			
			$ar[$liv1_v]["peso"] 		+= $row['TDPLOR'];			
			$ar[$liv1_v]["importo"] 	+= $row[$info_importo_field];
			$ar[$liv1_v]["fl_evaso"] 	+= $row['TDFN11'];			
			
			//totali per raggruppamento liv0
			if (is_null($ar_tot["LIV0"][$m_stacco_liv0_c]))
				$ar_tot["LIV0"][$m_stacco_liv0_c] = array();
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli"] 		+= $row['TDTOCO'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["pallet"] 	+= $row['TDBANC'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli_disp"]	+= $row['TDCOPR'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli_sped"]	+= $row['TDCOSP'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["volume"] 	+= $row['TDVOLU'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["peso"] 		+= $row['TDPLOR'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["importo"] 	+= $row[$info_importo_field];
			
			//per colorare la priorita' nel primo/secondo livello
			$ar[$liv1_v]["val"][$liv2_v]["T_ROW"] 		+= 1;
			$ar[$liv1_v]["val"][$liv2_v]["T_TDFN06"] 	+= $row['TDFN06'];
			$ar[$liv1_v]["val"][$liv2_v]["T_TDFN11"] 	+= $row['TDFN11'];			
			if (($row['TDFN07']==1 || $row['TDFN08']==1) && $row['TDFN06']==0 )			
				$ar[$liv1_v]["val"][$liv2_v]["T_TDFN07_TDFN08"] 	+= 1;

			$ar[$liv1_v]["T_ROW"] 		+= 1;						
			$ar[$liv1_v]["T_TDFN06"] 	+= $row['TDFN06'];
			$ar[$liv1_v]["T_TDFN11"] 	+= $row['TDFN11'];			
			if (($row['TDFN07']==1 || $row['TDFN08']==1) && $row['TDFN06']==0 )			
				$ar[$liv1_v]["T_TDFN07_TDFN08"] 	+= 1;
			
			$ar[$liv1_v]["val"][$liv2_v]["data_disp"] 	= max($ar[$liv1_v]["val"][$liv2_v]["data_disp"], $row['TDDTDS']);			

			
			if($nr_ord_sel == "Y"){
			    
			   
			    $k_ordine = $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cod"];
			    $tipo = $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['tipo'];
			    $ar_docu = explode("_", $k_ordine);
			    $anno = $ar_docu[3];
			    $num = trim($ar_docu[4]);
			    $proforma = "{$anno}.{$num}_{$tipo}";
			    
			    $sql_c = " SELECT *
                         FROM {$cfg_mod_Spedizioni['file_testate']} TD
			             WHERE TDDT = '{$id_ditta_default}' AND TDPROF = '{$proforma}'";
			 
			    $stmt_c = db2_prepare($conn, $sql_c);
			    echo db2_stmt_errormsg();
			    $result = db2_execute($stmt_c);
			    while($row_c = db2_fetch_assoc($stmt_c)){
			        
			         $nr = array();
			         $nr['cod']	= trim($row_c['TDDOCU']);
			         $nr['txt']	= "-> (".trim($row_c['TDOADO']) . "_" . trim($row_c['TDONDO']) . " " . trim($row_c['TDMODI']).")";
			         $nr['tipo']	= trim($row_c['TDOTPD']);
			         $nr['stato']	= trim($row_c['TDSTAT']);
			         $nr['data_reg']	= trim($row_c['TDODRE']);
			         $nr["volume"] 	= '';
			         $nr["importo"] = '';
			     
			         $liv3_v = trim($row_c['TDDOCU']);
			         $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v] = $nr;
			        	         
			    }
			    
			    if(trim($row['TDPROF']) != ''){
			        
			        $anno_p = substr($row['TDPROF'], 0, 4);
			        $numero = substr($row['TDPROF'], 5, 6);
			        $tipo_p = substr($row['TDPROF'], 12, 2);
			        
			        $sql_p = " SELECT *
			        FROM {$cfg_mod_Spedizioni['file_testate']} TD
			        WHERE TDDT = '{$id_ditta_default}' AND
			        TDOADO = '{$anno_p}' AND TDONDO = '{$numero}' AND TDOTPD = '{$tipo_p}'";
			        
			        
			        $stmt_p = db2_prepare($conn, $sql_p);
			        echo db2_stmt_errormsg();
			        $result = db2_execute($stmt_p);
			        $row_p = db2_fetch_assoc($stmt_p);
			        
			        $pr = array();
			        $pr['cod']	= trim($row_p['TDDOCU']);
			        $pr['txt']	= "<- [".trim($row_p['TDOADO']) . "_" . trim($row_p['TDONDO']) . " " . trim($row_p['TDMODI'])."]";
			        $pr['tipo']	= trim($row_p['TDOTPD']);
			        $pr['stato']	= trim($row_p['TDSTAT']);
			        $pr['data_reg']	= trim($row_p['TDODRE']);
			        
			        
			        $liv3_v = trim($row_p['TDDOCU']);
			        $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v] = $pr;
			        
			    }
			    
		
			  }
			

		}  //fine while per ogni ordine

	 return array("det"=>$ar, "tot"=>$ar_tot);		
	}





















/* *********************************************************************************************/
/* ALBERATURA PER ELENCO ORDINI */
/* *********************************************************************************************/

	public function crea_array_valori_el_ordini($its, $tipo_elenco = "", $ordina_per_tipo_ord = "N", $field_data = 'TDDTEP'){
		global $auth, $cfg_mod_Spedizioni;
		$js_parameters = $auth->get_module_parameters($this->get_cod_mod());
		
		
		//Se lavoriamo con la TDDTSP allora la spedizione di riferimento e' la TDNBOF e non la TDNBOC
		if ($field_data == 'TDDTSP')
		    $td_sped_field = 'TDNBOF';
		else
		    $td_sped_field = 'TDNBOC';
		        
		
		
		$ar = array();
		$ar_tot = array( $td_sped_field => array() );		
		$liv1_c = $liv2_c = $liv3_c = 0;
		$liv1_v = $liv2_v = $liv3_v = "--------------";
				
		while ($row = db2_fetch_assoc($its)) {
			
		    if ($tipo_elenco == 'HOLD'){
		    	$row['hold_TDDTEP'] = $row['TDDTEP'];
		    	$row['TDDTEP'] = 0;
		    }
		
			if ($ordina_per_tipo_ord == 'Y'){
				$m_stacco_liv0_c = implode("___", array($row['TDCLOR'], $row['TDOTPD']));
				$m_stacco_liv0_d = $row['TDDOTD'];
			}
			else {
				$m_stacco_liv0_c = $row[$td_sped_field];
				$m_stacco_liv0_d = $row[$td_sped_field];				
			}
			
			
			if ($row['TDDTEP'] . "___" . $m_stacco_liv0_c . "___" . $row['TDTPCA'] . "___" . $row['TDAACA'] . "___" .  $row['TDNRCA'] != $liv1_v){
				$liv1_c += 1;
				$liv2_c = $liv3_c = 0;
				$liv2_v = $liv3_v = "-----------";
				$liv1_v = $row['TDDTEP'] . "___" . $m_stacco_liv0_c . "___" . $row['TDTPCA'] . "___" . $row['TDAACA'] . "___" .  $row['TDNRCA'];
				
				$d_carico = strtotime($row['TDDTEP']);			
				
				if ((int)$row['TDNRCA'] == 0 && $tipo_elenco == "HOLD")
					$liv1_vt = "Carico non associato";
				else
					//$liv1_vt = "Carico " . trim($row['TDAACA']) . "_" . trim($row['TDTPCA']) . "_" .  trim($row['TDNRCA']) . " del " . strftime("%d/%m", $d_carico);
					$liv1_vt = "Carico " . trim($row['TDAACA']) . "_" . trim($row['TDTPCA']) . "_" .  trim($row['TDNRCA']);
				
				//icona stato pagamento carico
				$path_icona_stato_pagamento_carico = $this->get_icona_stato_pagamento_carico($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA'], $row['PSFG01']);
				if ($path_icona_stato_pagamento_carico)
				   $liv1_vt .= '<span style="display: inline; float: right;"><img width=16 class="cell-img" src=' . img_path($path_icona_stato_pagamento_carico) . '></span>';
				
				if (is_null($ar[$liv1_v]))
					$ar[$liv1_v] = array("cod"=>$liv1_v, "txt"=> $liv1_vt, "PSSTAT"=>$row['PSSTAT'], "dta"=>array(), "val"=>array());
				//$ar[$liv1_v]["riferimento"] = $row['TDDVET'] . $this->decod_std('AUTO', $row['TDAUTO']) . " " . $row['TDDCOS'];				 
				$ar[$liv1_v]["id_spedizione"] = $row[$td_sped_field];
				$ar[$liv1_v]["n_carico"] 	  = $row['TDNRCA'];
				$ar[$liv1_v]["k_carico"]	  = $this->k_carico_td($row);
				
				//if (strlen(trim($row['PSPPAC'])) > 0 && trim($row['PSPPAC']) != 'C' )
				//	$ar[$liv1_v]["tooltip_acc_paga"] = (float)$row['PSPPAC'];
				$ar[$liv1_v]["tooltip_acc_paga"] = $this->get_tooltip_acc_paga($row);

				if ($ordina_per_tipo_ord != 'Y')
					$ar[$liv1_v]["sped_id"]	  	  = $row[$td_sped_field];
				
				$ar[$liv1_v]["stacco_liv0_c"]  = $m_stacco_liv0_c;
				$ar[$liv1_v]["stacco_liv0_d"]  = $m_stacco_liv0_d;				
				
				//dal primo ordine carico la data della spedizione (perche' non e' detta che ancora sia impostata in CSDTPG)
				$ar[$liv1_v]["CSDTPG_ORD"]  = $row['TDDTSP'];				
				
				//prendere la descrizione dalla tabella dei carichi				
				$m_carico = $this->get_carico_td($row);				
				$ar[$liv1_v]["riferimento"]   = $m_carico['PSDESC']; 
			}
			
			//TODO: anche per ditta?
			if (($row['TDSECA'] . "___" . $row['TDCCON'] . "___" . $row['TDCDES'])  != $liv2_v){
				$liv2_c += 1;
				$liv3_c = 0; $liv3_v = "-----------";								
				$liv2_v = $row['TDSECA'] . "___" . $row['TDCCON'] . "___" . $row['TDCDES'];
				if (is_null($ar[$liv1_v]["val"][$liv2_v]))
					$ar[$liv1_v]["val"][$liv2_v] = array("cod"=>$liv2_v, "txt"=> trim($row['TDDCON']), "dta"=>array(), "val"=>array());
				
				$ar[$liv1_v]["val"][$liv2_v]['k_cli_des'] = implode("_", array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));
				
				$ar[$liv1_v]["val"][$liv2_v]["riferimento"]	= $this->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
				
				//icona mezzi inclusi esclusi
				if ($this->ha_mezzi($row['TDCCON'], $row['TDCDES'])){
				    $spedizione = $this->get_spedizione($row[$td_sped_field]);
				    $ar[$liv1_v]["val"][$liv2_v]["cons_rich"] = '<span><a href="javascript:show_win_note_cliente(\'' . $row['TDDT'] . '\', \'' . $row['TDCCON'] . '\', \'' . $row['TDCDES'] . '\')";><img class="cell-img" src=' . img_path("icone/16x16/spedizione.png") . '></a></span>';
				    $ar_tooltip_mezzi = $this->get_tooltip_mezzi($row['TDCCON'], $row['TDCDES'], trim($spedizione['CSCAUT']));
				    $ar[$liv1_v]["val"][$liv2_v]["tooltip_mezzi"] = $ar_tooltip_mezzi['tooltip'];
				    $ar[$liv1_v]["val"][$liv2_v]["s_mezzi_red"] = $ar_tooltip_mezzi['segnala_mezzo'];
				}
				    //se ha note cliente o destinazione
				    if ($this->ha_note_cliente($row['TDDT'], $row['TDCCON']) ||
				        $this->ha_note_cliente_dest($row['TDDT'], $row['TDCCON'], $row['TDCDES']) ||
				        $this->ha_note_cliente($row['TDDT'], implode('_', array($row['TDDT'], trim($row['TDCCON']), trim($row['TDCDES']), $row['CSPROG'])), '')
				        )
				        $ar[$liv1_v]["val"][$liv2_v]["priorita"] = '<span><a href="javascript:show_win_note_cliente(\'' . $row['TDDT'] . '\', \'' . $row['TDCCON'] . '\', \'' . $row['TDCDES'] . '\', \'' . $row[$td_sped_field] . '\')";><img class="cell-img" src=' . img_path("icone/16x16/blog_compose.png") . '></a></span>';									
				
			
				//se scarico intermedio o sosta tecnica
				if ($row['TDTDES'] == '1' || $row['TDTDES'] == '2' || $row['TDTDES'] == 'T'){
					$ar[$liv1_v]["val"][$liv2_v]["scarico_intermedio"] = 1;

					if ($row['TDFN18'] == '1') //coincide con quello indicato in anagrafica cliente
						$ar[$liv1_v]["val"][$liv2_v]["riferimento"] = '<img class="cell-img" src=' . img_path("icone/16x16/exchange_black.png") . '>&nbsp;&nbsp;' . $ar[$liv1_v]["val"][$liv2_v]["riferimento"];
					else
						$ar[$liv1_v]["val"][$liv2_v]["riferimento"] = '<img class="cell-img" src=' . img_path("icone/16x16/exchange.png") . '>&nbsp;&nbsp;' . $ar[$liv1_v]["val"][$liv2_v]["riferimento"];										
				}	 
				
				//icona ordine di reso
				if ($this->ha_con_ordini_reso($row['TDDT'], $row['TDCCON'], $row['TDCDES']))
					$ar[$liv1_v]["val"][$liv2_v]["tipo"] = '<img class="cell-img" src=' . img_path("icone/16x16/recycle_bin.png") . '>';
				
				
				$ar[$liv1_v]["val"][$liv2_v]["seq_carico"]	= $row['TDSECA'];

				if (strlen(trim($row['TDSECA'])) > 0){
					//nel tooltip metto la massima data di scarico				
					$ar[$liv1_v]["val"][$liv2_v]["tooltip_seq_carico"] = max((int)$ar[$liv1_v]["val"][$liv2_v]["tooltip_seq_carico"], $row['TDDTIS']);
					
					//se la data di scarico non coincide non quella della spedizione mostro il dirty sulla cella
					if ($ar[$liv1_v]["val"][$liv2_v]["tooltip_seq_carico"] > 0 && $row['TDDTIS'] != $row['TDDTCP'])
						$ar[$liv1_v]["val"][$liv2_v]["dirty_seq_carico"] = 1;
				}
				
				
				$ar[$liv1_v]["val"][$liv2_v]["cliente"]		= $row['TDDCON'];
				$ar[$liv1_v]["val"][$liv2_v]["gmap_ind"]	= implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));				
				 
			}			
			
			$documento_cambiato = false;
			if ($row['TDDOCU'] != $liv3_v){
				$liv3_c += 1;								
				$liv3_v = "{$row['TDDOCU']}";
				$liv3_txt = trim($row['TDOADO']) . "_" . trim($row['TDONDO']) . " " . trim($row['TDMODI']);
				
				if ($liv3_txt != $old_liv3_txt) {
					$documento_cambiato = true;
					$old_liv3_txt = $liv3_txt;
				}				
				
				if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
				    if(trim($row['TDFG08']) == 'R')
					   $liv3_txt .= " Riga {$row['TDPROG']}";
				}
				

				//ordine bloccato autorizzato (divieto green)
				if ($row["TDBLOC"] == 'A')
					$liv3_txt .= '&nbsp;<img class="cell-img" style="margin-top: 2px;" src=' . img_path("icone/16x16/divieto_green.png") . '>';
				
				
				//icona ordine abbinato
				if (trim($row['TDABBI']) > 0)
				  $liv3_txt .= '&nbsp;&nbsp;&nbsp;<img class="cell-img" src=' . img_path("icone/16x16/link.png") . '>';

			
				$ha_commenti = $this->has_commento_ordine_by_k_ordine($this->k_ordine_td($row)); 
				if ($ha_commenti == FALSE)
					$img_com_name = "icone/16x16/comment_light.png";
				else
					$img_com_name = "icone/16x16/comment_edit_yellow.png";
				
				//icona commento ordine
				  $liv3_txt .= '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine(\'' . $row['TDDOCU'] . '\', \'' . $liv1_v . '\', \'' . $liv2_v . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';

				  
				//icona entry (per attivita' in corso sull'ordine)
				$sa = new SpedAssegnazioneOrdini(); 
				if ($sa->ha_entry_aperte_per_ordine($row['TDDOCU'])){
					$img_entry_name = "icone/16x16/arrivi_gray.png";				
					$liv3_txt .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><a href="javascript:show_win_entry_ordine(\'' . $row['TDDOCU'] . '\', \'' . $liv1_v . '\', \'' . $liv2_v . '\')";><img class="cell-img" src=' . img_path($img_entry_name) . '></a> </span>';
				}				  
				
				
				//icona riprogrammazione (se e' stato riprogrammato)
				$sr = new SpedAutorizzazioniDeroghe();
				if ($sr->ha_RIPRO_by_k_ordine($row['TDDOCU'])){
					$img_entry_name = "icone/16x16/button_black_repeat_dx.png";
					$liv3_txt .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';					
				}				
				
				if (is_null($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]))
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v] = array("cod"=>$liv3_v,
						"txt"=> $liv3_txt,
						"dta"=>array());
				
				if ($sr->ha_RIPRO_by_k_ordine($row['TDDOCU'])){
					$ar_tooltip_ripro = $sr->tooltip_RIPRO_by_k_ordine($row['TDDOCU']);
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tooltip_ripro"] =  "<B>RIPROGRAMMAZIONI:</B><br>" . implode("<br>", $ar_tooltip_ripro);					
				}
				
				
				//icona: ha deroghe?
				if ($sr->ha_deroghe_by_k_ordine($row['TDDOCU'])){
					$img_entry_name = "icone/48x48/unlock.png";
				
					$ar_deroghe = array();
					$array_deroghe = $sr->array_deroghe_by_k_ordine($row['TDDOCU']);
					foreach ($array_deroghe as $r_deroga){
						$ar_deroghe[] = "> [" . trim($r_deroga['ADUSRI']) . "] " . print_date($r_deroga['ADDTDE']) . " - " . trim($this->decod_std('AUDE', $r_deroga['ADAUTOR'])) . ": " .  acs_u8e(trim($r_deroga['ADCOMM']));
					}
					$tooltip_ha_deroghe = implode("<br>", $ar_deroghe);
						
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['txt'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img width=16 class="cell-img" src=' . img_path($img_entry_name) . '></span>';
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['tooltip_ripro'] .= "<br><br><B>DEROGHE:</B><br>" . $tooltip_ha_deroghe;
				}
				
				if(trim($row['TDFU03']) != ''){
				    $img_entry_name = "icone/16x16/telephone_blue.png";
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['txt'] .= '<span style="display: inline; float: right; padding-right: 5px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]['tooltip_ripro'] .= "<br><br>[".trim($row['TDFU03'])."]";
				}
				
			}			
		
			
			$ar[$liv1_v]["val"][$liv2_v]["k_carico"] = $ar[$liv1_v]["k_carico"]; 
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_carico"] = $ar[$liv1_v]["k_carico"];
						
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDSWPP"] 	= $row['TDSWPP'];
			

			if ($ordina_per_tipo_ord != 'Y'){
				$ar[$liv1_v]["val"][$liv2_v]["sped_id"] = $ar[$liv1_v]["sped_id"];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["sped_id"] = $ar[$liv1_v]["sped_id"];
			}			
			
			if ($documento_cambiato == true)
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] =  trim($row['TDVSRF']);
			
			if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
				if ($documento_cambiato == true) {
					if (strlen(trim($row['TDVSRF'])) == 0) //se rif e' vuoto per evidenziare il cambio documento ripeto anno-numero
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] = trim($row['TDOADO']) . "_" . trim($row['TDONDO']) . " " . trim($row['TDMODI']);
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] .= "<BR>";
				}
				if(strlen(trim($row['RDART'])) > 0){
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] .= "<b>" . " [" . trim(acs_u8e($row['RDART'])) . "] " . trim(acs_u8e($row['RDDES1'])) . "</b>";
				    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["articolo"] = trim(acs_u8e($row['RDART']));
				}
			
			}

			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["nr"] 			=  $row['TDONDO'];
			//$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_ordine"]	=  $this->k_ordine_td($row);			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_ordine"]	=  $row['TDDOCU'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_cli_des"]	=  $ar[$liv1_v]["val"][$liv2_v]['k_cli_des'];						
						
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tipo"] 		=  $row['TDOTPD'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["raggr"] 		=  $row['TDCLOR'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["priorita"]	=  $row['TDOPRI'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"]		=  $this->get_tp_pri(trim($row['TDOPRI']), $row);			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_reg"]	=  $row['TDODRE'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_rich"]	=  $row['TDODER'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["gg_rit"] 	  	=  $row['TDGGRI'];
			//if ($tipo_elenco == 'HOLD')
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["proposte"] 	= trim($row['TDFG07']);
		
			
			if ($tipo_elenco == 'HOLD'){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_prog"]	=  $row['hold_TDDTEP'];
			} else {
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_prog"]	=  $row['TDDTEP'];				
			}

			//icona bandiera_gray (flag speciale) - contrassegno
			if (strlen(trim($row['TDCODO'])) > 0){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["seq_carico"]	= "S"; //attivo bandiera_grey
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tooltip_seq_carico"]	= $row['TDDCDO']; //tooltip				
			}

			//icona sticker_black - indice rottura
			if (strlen(trim($row['TDIRLO'])) > 0){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["seq_carico"]	.= "I"; //indice rottura
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tooltip_seq_carico"]	.= '<br/>Indice rottura: ' . trim($row['TDIRLO']); //tooltip
			}
				
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["contract"]	= $row['TDCOST']; //indice rottura
			
			//a livello di cliente riporto la MAX data disponibilita
			$ar[$liv1_v]["val"][$liv2_v]["data_disp"] 	= max($ar[$liv1_v]["val"][$liv2_v]["data_disp"], $row['TDDTDS']);

						
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDFN11"]		=  $row['TDFN11']; //evaso
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDFN19"]		=  $row['TDFN19']; //evaso parziale
		    $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDSWSP"]		=  $row['TDSWSP']; //non programmabile
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDBLOC"]		=  $row['TDBLOC'];									
			
/*			
			if ($tipo_elenco == 'HOLD')
				//data disponibilita'
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTDS'];
			else			
				//data confermata
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTVA'];
*/				
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTVA'];			
				
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_cons_conf"]	=  $row['TDFN06'];				
			 				
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_evaso"]	=  $row['TDFN11'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["stato"]		=  $row['TDSTAT'];						
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["volume"] 		+= $row['TDVOLU'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["peso"] 		+= $row['TDPLOR'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["importo"]		+= $row['TDTIMP'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["pallet"]		+= $row['TDBANC'];			

			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["c_prod"] 		+= $row['COLLI_PROD'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli"] 		+= $row['TDTOCO'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli_disp"] 	+= $row['TDCOPR'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli_sped"] 	+= $row['TDCOSP'];			
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_disp"] 		= $row['TDDTDS'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_conf_ord"] 	= $row['TDDTCF'];						

			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_cons_cli"] = $row['TDDTCP'];			

			//a livello di cliente riporto la MAX data disponibilita
			$ar[$liv1_v]["val"][$liv2_v]["data_disp"] 	= max($ar[$liv1_v]["val"][$liv2_v]["data_disp"], $row['TDDTDS']);

			//segnalo di rosa se data_disp > data prog
			if ($row['TDDTDS'] > $row['TDDTEP'] && $row['TDDTEP'] > 0){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_data_disp"] = 1; //segnalo di rosa
				$ar[$liv1_v]["val"][$liv2_v]["fl_data_disp"] = 1; //segnalo di rosa anche sul cliente				
			}			
			
			
			//flag
			
			//cliente bloccato (basta che sia indicato in almeno un ordine)
			if ($row['TDCLBL'] == 'Y' || $row['TDCLBL'] == 'E' || $row['TDCLBL'] == 'P')
			{
				$ar[$liv1_v]["val"][$liv2_v]["fl_cli_bloc"] = 1;
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_cli_bloc"] = 1;
			}
				
			
			
			//blocco amministrativo (TDBLEV) e/o commerciale (TDBLOC)
			//fl_bloc:
			// 4 - amministrativo e commerciale
			// 3 - amministrativo
			// 2 - commerciale
			// 1 - sbloccato
			
			$m_tipo_blocco = null;
			if ($row['TDBLEV'] == 'Y' || $row['TDBLOC'] == 'Y')
			{
				if ($row['TDBLEV'] == 'Y' && $row['TDBLOC'] == 'Y')	$m_tipo_blocco = 4;
				else {
					if ($row['TDBLEV'] == 'Y') $m_tipo_blocco = 3;
					else if ($row['TDBLOC'] == 'Y') $m_tipo_blocco = 2;
				}

				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_bloc"] 	= $m_tipo_blocco;				
				
				//riporto a livello superiore
				$ar[$liv1_v]["fl_bloc"] 				= $this->get_fl_bloc($ar[$liv1_v]["fl_bloc"], $m_tipo_blocco);				
				$ar[$liv1_v]["val"][$liv2_v]["fl_bloc"] = $this->get_fl_bloc($ar[$liv1_v]["val"][$liv2_v]["fl_bloc"], $m_tipo_blocco);				

			}
			
			
			if (($row['TDBLEV'] != 'Y' && $row['TDBLOC'] != 'Y') && ($row['TDBLEV'] == 'S' || $row['TDBLOC'] == 'S')) //sbloccato
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_bloc"] 	= 1;

 
			

			if ($tipo_elenco == 'HOLD')
			{
				$ar[$liv1_v]["fl_da_prog"] 	= 1;
				$ar[$liv1_v]["val"][$liv2_v]["fl_da_prog"] 	= 1;
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_da_prog"] 	= 1;
			}			
			
			
			if (trim($row['TDFG01']) == 'A' || trim($row['TDFG01']) == 'R') //indice modifica (icona + su new)
			{
				$ar[$liv1_v]["ind_modif"] = trim($row['TDFG01']);				
				$ar[$liv1_v]["val"][$liv2_v]["ind_modif"] 	= trim($row['TDFG01']);
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["ind_modif"]	=  trim($row['TDFG01']);								
			}
			
			
			
			$stato_pagamento_ordine = $this->get_stato_pagamento_ordine($row['TDDOCU'], $row['TDFG06']);
			$icona = $this->gest_image_pagamento($stato_pagamento_ordine);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["i_paga"] 	= $icona;
			
			//Icona NEWS - DAY
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"] = $this->get_fl_new($row);
			
				//passo a livello superiore
				//se sono il primo e sono completo imposto il livello superiore a parziale
				$ar[$liv1_v]["fl_new_cont"] = (int)$ar[$liv1_v]["fl_new_cont"] + 1;
				if ($ar[$liv1_v]["fl_new_cont"] > 1 &&						//se ordine C, ma liv !=C lo imposto in P
						$ar[$liv1_v]["fl_new"] < 10 &&
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"] == 10)
					$ar[$liv1_v]["fl_new"] = 11;
					if ($ar[$liv1_v]["fl_new"] == 10 &&							//se liv C, ma ordine <> C, imposto in P
							$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"] != '10')
						$ar[$liv1_v]["fl_new"] = 11;
				$ar[$liv1_v]["fl_new"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"], $ar[$liv1_v]["fl_new"]);
				
				$ar[$liv1_v]["val"][$liv2_v]["fl_new_cont"] = (int)$ar[$liv1_v]["val"][$liv2_v]["fl_new_cont"] +1;
				if ($ar[$liv1_v]["val"][$liv2_v]["fl_new_cont"] > 1 &&						//se ordine C, ma liv !=C lo imposto in P
						$ar[$liv1_v]["val"][$liv2_v]["fl_new"] < 10 &&
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"] == 10)
					$ar[$liv1_v]["val"][$liv2_v]["fl_new"] = 11;
				if ($ar[$liv1_v]["val"][$liv2_v]["fl_new"] == 10 &&							//se liv C, ma ordine <> C, imposto in P
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"] != '10')
					$ar[$liv1_v]["val"][$liv2_v]["fl_new"] = 11;
				$ar[$liv1_v]["val"][$liv2_v]["fl_new"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"], $ar[$liv1_v]["val"][$liv2_v]["fl_new"]);
				

			//passo a livello superiore il tipo priorita
			$ar[$liv1_v]["tp_pri"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"], $ar[$liv1_v]["tp_pri"]);
			$ar[$liv1_v]["val"][$liv2_v]["tp_pri"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"], $ar[$liv1_v]["val"][$liv2_v]["tp_pri"]); 
			
			
			
			$tmp_fl_art_manc = $this->get_fl_art_manc($row);
			$ar[$liv1_v]["fl_art_manc"] = max($ar[$liv1_v]["fl_art_manc"], $tmp_fl_art_manc);
			$ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"] = max($ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"], $tmp_fl_art_manc);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"], $tmp_fl_art_manc);
			
				
				
			

			if ($row['TDFMTS'] == 'Y')			
			{
				if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"				
					$ar[$liv1_v]["art_da_prog"] 	= 1;				
					$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] 	= 1;				
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] 	= 1;
				}
			}			
			if ($row['TDFN15'] == 1){
				if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
					$ar[$liv1_v]["art_da_prog"] 	= 2;
					//a livello superiore ha priorita info_black (1)
					if ($ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] != 1)
						$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] 	= 2;
					if ($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] != 1)
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] 	= 2;
				}
			}			
			
			//background non allineato con order entry (come caso 1),
			//pero' in questo ordine non ci sono articoli che si stanno controllando in bg
			if ($row['TDFN15'] == 2){
				if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
					$ar[$liv1_v]["art_da_prog"] 	= 3;
					//a livello superiore
					if ((int)$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] == 0 || (int)$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] > 2)
						$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] 	= 3;
					if ((int)$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] == 0 || (int)$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] > 2)
						$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] 	= 3;
				}
			}
				
			
			

			//somme parziali
			$ar[$liv1_v]["val"][$liv2_v]["colli"] 		+= $row['TDTOCO'];
			$ar[$liv1_v]["val"][$liv2_v]["c_prod"] 		+= $row['COLLI_PROD'];
			$ar[$liv1_v]["val"][$liv2_v]["pallet"] 		+= $row['TDBANC'];			
			$ar[$liv1_v]["val"][$liv2_v]["colli_disp"] 	+= $row['TDCOPR'];						
			$ar[$liv1_v]["val"][$liv2_v]["colli_sped"] 	+= $row['TDCOSP'];			
			$ar[$liv1_v]["val"][$liv2_v]["volume"] 		+= $row['TDVOLU'];									
			$ar[$liv1_v]["val"][$liv2_v]["peso"] 		+= $row['TDPLOR'];			
			$ar[$liv1_v]["val"][$liv2_v]["importo"] 	+= $row['TDTIMP'];
			$ar[$liv1_v]["val"][$liv2_v]["fl_evaso"] 	+= $row['TDFN11'];						
					
			$ar[$liv1_v]["colli"] 		+= $row['TDTOCO'];	
			$ar[$liv1_v]["c_prod"] 		+= $row['COLLI_PROD'];	
			$ar[$liv1_v]["pallet"] 		+= $row['TDBANC'];			
			$ar[$liv1_v]["colli_disp"]	+= $row['TDCOPR'];			
			$ar[$liv1_v]["colli_sped"]	+= $row['TDCOSP'];			
			$ar[$liv1_v]["volume"] 		+= $row['TDVOLU'];			
			$ar[$liv1_v]["peso"] 		+= $row['TDPLOR'];			
			$ar[$liv1_v]["importo"] 	+= $row['TDTIMP'];
			$ar[$liv1_v]["fl_evaso"] 	+= $row['TDFN11'];			
			
			//totali per raggruppamento liv0
			if (is_null($ar_tot["LIV0"][$m_stacco_liv0_c]))
				$ar_tot["LIV0"][$m_stacco_liv0_c] = array();
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli"] 		+= $row['TDTOCO'];	
			$ar_tot["LIV0"][$m_stacco_liv0_c]["c_prod"]     += $row['COLLI_PROD'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["pallet"] 	+= $row['TDBANC'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli_disp"]	+= $row['TDCOPR'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli_sped"]	+= $row['TDCOSP'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["volume"] 	+= $row['TDVOLU'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["peso"] 		+= $row['TDPLOR'];			
			$ar_tot["LIV0"][$m_stacco_liv0_c]["importo"] 	+= $row['TDTIMP'];
			
			//data consegna cliente (la riporto sulla spedizione)			
			if ($row['TDDTCP'] >= $row['TDDTEP']) 
			  $ar_tot["LIV0"][$m_stacco_liv0_c]["data_cons_cli"] = $row['TDDTCP'];				
			 
			
			//per colorare la priorita' nel primo/secondo livello
			$ar[$liv1_v]["val"][$liv2_v]["T_ROW"] 		+= 1;
			$ar[$liv1_v]["val"][$liv2_v]["T_TDFN06"] 	+= $row['TDFN06'];
			$ar[$liv1_v]["val"][$liv2_v]["T_TDFN11"] 	+= $row['TDFN11'];			
			if (($row['TDFN07']==1 || $row['TDFN08']==1) && $row['TDFN06']==0 )			
				$ar[$liv1_v]["val"][$liv2_v]["T_TDFN07_TDFN08"] 	+= 1;

			$ar[$liv1_v]["T_ROW"] 		+= 1;						
			$ar[$liv1_v]["T_TDFN06"] 	+= $row['TDFN06'];
			$ar[$liv1_v]["T_TDFN11"] 	+= $row['TDFN11'];			
			if (($row['TDFN07']==1 || $row['TDFN08']==1) && $row['TDFN06']==0 )			
				$ar[$liv1_v]["T_TDFN07_TDFN08"] 	+= 1;				
		}
		
	 return array("det"=>$ar, "tot"=>$ar_tot);		
	}


	
	
	


	public function calc_fl_bloc($row) {
		//FLAG BLOCCO
		// 4 - amministrativo e commerciale
		// 3 - amministrativo
		// 2 - commerciale
		// 1 - sbloccato
		$m_tipo_blocco = null;
		if ($row['TDBLEV'] == 'Y' || $row['TDBLOC'] == 'Y')
		{
			if ($row['TDBLEV'] == 'Y' && $row['TDBLOC'] == 'Y')	$m_tipo_blocco = 4;
			else {
				if ($row['TDBLEV'] == 'Y') $m_tipo_blocco = 3;
				else if ($row['TDBLOC'] == 'Y') $m_tipo_blocco = 2;
			}
		}
			
		if (($row['TDBLEV'] != 'Y' && $row['TDBLOC'] != 'Y') && ($row['TDBLEV'] == 'S' || $row['TDBLOC'] == 'S')) //sbloccato
			$m_tipo_blocco 	= 1;
	
		return $m_tipo_blocco;
	}
	
	
	
	public function get_fl_new($row){
		
		/*if (record.get('fl_new')==1) 	return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';
		if (record.get('fl_new')==2) 	return '<img src=<?php echo img_path("icone/48x48/mano_ko.png") ?> width=18>';
		if (record.get('fl_new')==3) 	return '<img src=<?php echo img_path("icone/48x48/timer_blue.png") ?> width=18>';
		if (record.get('fl_new')==4) 	return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=18>';
		*/		
			
		if (oggi_AS_date() <= $row['TDDTNEW']){
			return  1;
		}
		
		if ($row['TDFN14'] == 1){
			return 2;
		}
		
		if ( to_AS_date_time($row['TDDTAR'], $row['TDHMAR']) > now_AS_date_time() ){ //attesa maturazione confermata
			return 3;
		}
		
		if ($row['TDFN13'] == 1){
			return 4;
		}
		
		//scarico delivery control
		if ( $row['TDTCNS'] > 0) 	//ho anomalie -> bandiera rossa
			return 12;
		if ( $row['TDFLSV'] == 'P')	//scarico parziale (o perche' parziale, o perche' ha nomalie) -> bandiera grigia
			return 11;
		if ( $row['TDFLSV'] == 'C')	//scarico completo -> bandiera scacchi
			return 10;
		
	}
		
	
	
	
	
	
	public function get_fl_bloc($val_iniziale, $nuovo_valore){
		switch($val_iniziale){
			case 4: return 4;
			case 3: 
				if ($nuovo_valore == 2) return 4; else return 3;
			case 2:
				if ($nuovo_valore == 4) return 4;
				if ($nuovo_valore == 3) return 4;
				return 2;
			default:
				return $nuovo_valore;						
		}
		
	}
	
	public function get_tp_pri($pri, $row){

	  if ($row['TDOPUN'] == 'N') //ordine puntuale
	  	 return 6;
		
	  if ($row['TDFN07'] == 1 || $row['TDFN08'] == 1)	
		 return 4;
	  
	  return 0;	 		 
	}

	public function scrivi_rif_destinazione($loc, $naz, $prov, $naz_des)
	{
		if ($naz=="ITA" || substr($naz, 0,2)=="IT" || strlen(trim($naz))==0)
			return trim($loc) . " ($prov)";
		else
			return trim($loc) . " ($naz_des)";
	}



	public function crea_alberatura_el_ordini_json($ar, $id, $cod_itin, $titolo_panel, $tbar_title, $tipo_elenco = "", $ordina_per_tipo_ord = "N", $data = 0){
		$ar_tot_sped = $ar["tot"]["LIV0"];
		
		if ($id == "root" || $id=='')
		{
		 $pre_id = "";
		 $liv = 1;			 
		 $ar = $ar["det"];
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$pre_id = $id;
		 	$sottostringhe = explode("|", $id);
			$liv1 = $sottostringhe[2];
			$liv2 = $sottostringhe[4];
			
			if (strlen($liv2) == ""){
				$liv2 = null;
				$refresh_liv1 = "Y";
				$ar = $ar["det"][$liv1]["val"];
				$liv = 2;								
			}
			else {
				
				$liv = 3;
				$ar = $ar["det"][$liv1]["val"][$liv2]["val"];						
			}
			
		 }	
		
		
		$inline_id_liv0 = -99999999;
		
		
		$ret = '{"text":".",
		 		 "titolo_panel": "' . $titolo_panel . '",
		 		 "tbar_title": ' . j($tbar_title) . ',
		 		 "tipo_elenco": "' . $tipo_elenco . '",		 		 
		 		 "cod_iti": "' . $cod_itin . '",
		 		 "data": "' . $data . '",		 		 		
				 "itinerario": ' . j($this->decod_std('ITIN', $cod_itin)) . ',
				 "children": [';
		
		
			$cont_row = 0;
			foreach ($ar as $r){
								
				//se cambia, aggiungo la riga (foglia) con id spedizione
				
				if ($refresh_liv1 != "Y" && $liv == 1 && $inline_id_spedizione!=$r['stacco_liv0_c']){
					
					//decodifico dati spedizione
					if ($ordina_per_tipo_ord == 'Y'){
						$spedizione = array();
						
						$m_txt = $r['stacco_liv0_d'];
						$m_rif = '';
					}
					else {
						$spedizione = $this->get_spedizione($r['id_spedizione']);
						
						if ($r['id_spedizione'] > 0) {
							$m_txt = '';					
							$m_txt .= "Spedizione #" . $r['id_spedizione'] . " <span style=\"font-weight: normal;\"> - " . $spedizione['CSTISP'] . " - " . $spedizione['CSTITR'] . "</span>";
							
							// DAY -> mostro flight e data spedizione
							// negli altri mostro fabbrica e data evasione programmata
							if ($tipo_elenco == 'DAY'){
								$m_txt .= "&nbsp;<img class=\"cell-img\" src=" . img_path("icone/16x16/partenze.png") . ">";
								$m_txt .= "&nbsp;<span style=\"font-weight: normal;\">" . strftime("%d/%m", strtotime($r['CSDTPG_ORD'])) . "</span>";
							} elseif ($tipo_elenco == 'GATE'){
							    $m_txt .= "&nbsp;<img class=\"cell-img\" src=" . img_path("icone/16x16/fabbrica.png") . ">";
							    if ($spedizione['CSFG04'] != 'F')
							     $m_txt .= "&nbsp;<span style=\"font-weight: normal;\">" . strftime("%d/%m", strtotime($spedizione['CSDTSP'])) . "</span>";
							} else {
								$m_txt .= "&nbsp;<img class=\"cell-img\" src=" . img_path("icone/16x16/fabbrica.png") . ">";
								$m_txt .= "&nbsp;<span style=\"font-weight: normal;\">" . strftime("%d/%m", strtotime($spedizione['CSDTSP'])) . "</span>";								
							}

							$consegna_txt = '';							
							if ($ordina_per_tipo_ord != 'Y'){

								//se c'� inserisco la data di consegna
								if ($ar_tot_sped[$r['stacco_liv0_c']]['data_cons_cli'] > 0)						
								 $consegna_txt = "<p class=\"sottoriga-liv_0\">(Consegna entro il " . print_date($ar_tot_sped[$r['stacco_liv0_c']]['data_cons_cli']) . ")</p>";

								//se c'e ci sovrascrivo la data di scarico dal cliente
								if ($spedizione['CSDTSC'] > 0)
								 $consegna_txt = "<p class=\"sottoriga-liv_0\">(Scarico il " . print_date($spedizione['CSDTSC']) . " " . print_ora($spedizione['CSHMSC']) . ")</p>";
								
							}
	
							//riga nota spedizione
							$m_txt .= "<p class=\"orario-liv_0\">";
							
								//se impstata aggiungo l'ora
								if ($spedizione['CSHMPG'] > 0 || strlen(trim($spedizione['CSPORT'])) > 0){
									
									 $m_txt .= "[" . trim(print_ora($spedizione['CSHMPG']) . ' ' . trim($spedizione['CSPORT'])). "]";
								}
								
								//se impstata aggiungo la descrizione della spedizione
								if (strlen(trim($spedizione['CSDESC'])) > 0) $m_txt .= " " . trim($spedizione['CSDESC']);

								//se presenta aggiungo la targa
								if (strlen(trim($spedizione['CSTARG'])) > 0) $m_txt .= " {$spedizione['CSTARG']}";
								
							
							$m_txt .= "</p>";
							
							$m_rif = $this->decod_std('AUTR', $spedizione['CSCVET']) . " " . 
								$this->decod_std('AUTO', $spedizione['CSCAUT']) . " " . $this->decod_std('COSC', $spedizione['CSCCON']);
							
/*	2014-11-06 La porta e' stata inserita dopo l'ora di spedizione						
							//aggiunto PORTA
							if (strlen(trim($spedizione['CSPORT'])) > 0){
								$m_rif .= "<br><p class=\"sottoriga-liv_0\">";
								
								if (strlen(trim($spedizione['CSPORT'])) > 0)
									$m_rif .= " Porta: {$spedizione['CSPORT']}";
								$m_rif .= "</p>";								
							}
*/								
							
							//aggiungo il localita/riferimento l'indicazione della data di consegna
							$m_rif .= $consegna_txt;							
							
						}
						else //spedizione non definita
						{
							$m_txt = "Programmazione non definita";
							$m_rif = "";
						}						
						
					}



					//default


					$icon_class = 'iconSpedizione';
					/*
					if ($tipo_elenco == 'GATE')
					   $icon_class = 'iconPartenze';
					else
					    $icon_class = 'iconFabbrica';
					*/    					    
					
					
					if ($spedizione['CSFG04'] == 'F')  
					    $my_icon_class = 'iconPartenze';
					else 
					    $my_icon_class = $icon_class;
					
					
					
					$ret .= " {";
					$ret .="
				        task: " . j($m_txt) . ",
						id: \"|SPED|{$r['stacco_liv0_c']}\",
						sped_id: " . j($r['stacco_liv0_c']) . ", 				        
				        liv: \"liv_0\",
				        iconCls: \"{$my_icon_class}\",				        
						riferimento: " . j(trim($m_rif)) . "," . "
						gmap_ind: " . j($r['gmap_ind']) . "," . "		        
	  					volume: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['volume'], 0,',','.') . "\"" . "," . "				         
	  					peso: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['peso'], 0,',','.') . "\"" . "," . "
	  					pallet: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['pallet'], 1,',','.') . "\"" . "," . "
	  					colli: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['colli'], 0,',','.') . "\"" . "," . "	 
                        c_prod: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['c_prod'], 0,',','.') . "\"" . "," . "	  					 					
	  					importo: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['importo'], 2,',','.') . "\"" . "," . "	  					
	  											
	  					volume_disp: \"" . number_format($spedizione["CSVOLD"], 0,',','.') . "\"" . "," . "				         
	  					peso_disp: \"" . number_format($spedizione["CSPESO"], 0,',','.') . "\"" . "," . "
	  					pallet_disp: \"" . number_format($spedizione["CSBAND"], 0,',','.') . "\"" . "," . "	  						  					
				        leaf: true 
	       			";			        				
					$ret .=" },";							
									
					$inline_id_spedizione = $r['stacco_liv0_c'];	
				}
				
				
				
				//PAGINAZIONE
				$cont_row++;
				$per_page = 100;				
				
				if ($liv == 3){				
					$p_page = (int)$_REQUEST['p_page'];
					if ($p_page == 0) $p_page = 1;
					if ($cont_row <= (($p_page-1) * $per_page)) continue;
				}	
				
				
				$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];			
				$ret .= " {";
				$ret .="
					id: \"{$m_id}\",
			        task: " . j($r["txt"]) . ",
			        liv: \"liv_{$liv}\", 
			        iconCls: " . j($this->get_iconCls($liv, $r)) . ",
			        riferimento: " . j(trim($r["riferimento"])) . "," . "
			        expanded: true,
			        " . $this->nodo_dett_el_ordini($r, $liv) 
			         . $this->nodo_children_el_ordini($m_id, $liv +1, $r["val"]) . 
			        " 
       			";
				
				//if ($liv==1)					
				 //// lo sposto in nodo_dett_el_ordini??? $ret .= ", n_carico: \"" . $r['n_carico'] . "\"";  
							        				
				$ret .=" },";

				
				
				
				
				//PAGINAZIONE

				if ($liv == 3 
					&& $cont_row == ($p_page * $per_page) 	//raggiunto unltimo record della pagina 
					&& count($ar) > $cont_row + 50			//ho altri record da mostrare (almeno 50)
					){
					//aggiunto " -> Carica successivi ";
					$nr_mancanti = count($ar) - $cont_row;
					
					$pagina = $p_page + 1;
					
					$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"] . "|{$pagina}";
					$ret .= " {";
					$ret .="
					id: \"{$m_id}\",
					prog: {$pagina},
					task: " . j("Carica successivi... [{$nr_mancanti}]") . ",
					liv: \"liv_{$liv}_SUCC\",
					iconCls: " . j($this->get_iconCls($liv, $r)) . ",
					expanded: true,
					leaf: true
					}";					
					break;
				}
				
				
			} //foreach
			
		$ret .= ']}';
		return $ret;		
	}




	public function crea_alberatura_el_ordini_json_search($ar, $id, $cod_itin, $titolo_panel, $tbar_title, $tipo_elenco = ""){
		$ar_tot_sped = $ar["tot"]["LIV0"];
		$liv1_expanded = 'true';			
		if ($id == "root" || $id=='')
		{
		 $pre_id = "";
		 $liv = 1;			 
		 $ar = $ar["det"];
		 
		 //se ho troppe righe a liv1 (itinerari), non li mostro aperti
		 if (count($ar) > 100)
		 	 $liv1_expanded = 'false';
		 else
			 $liv1_expanded = 'true';
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$pre_id = $id;
		 	$sottostringhe = explode("|", $id);
			$liv1 = $sottostringhe[2];
			$liv2 = $sottostringhe[4];
			
			if (strlen($liv2) == ""){
				$liv2 = null;
				$refresh_liv1 = "Y";
				$ar = $ar["det"][$liv1]["val"];
				$liv = 2;								
			}
			else {
				$liv = 3;
				$ar = $ar["det"][$liv1]["val"][$liv2]["val"];						
			}
			
		 }	
		
		
		$inline_liv0 = -99999999;
		
		
		$ret = '{"text":".",
		 		 "titolo_panel": "' . $titolo_panel . '",
				 "tbar_title": ' . j($tbar_title) . ',		 		 		
		 		 "tipo_elenco": "' . $tipo_elenco . '",		 		 
		 		 "cod_iti": "' . $cod_itin . '",
				 "itinerario": ' . j($this->decod_std('ITIN', $cod_itin)) . ',
				 "children": [';
		
			foreach ($ar as $r){
				
				//se cambia, aggiungo la riga (foglia) con id spedizione
				
				
				if ($refresh_liv1 != "Y" && $liv == 1 && $inline_liv0!=$r['stacco_liv0_c']){
					
					//decodifico dati spedizione
					
					$m_txt = $r['stacco_liv0_d'];
					$m_rif = '';
					
					$ret .= " {";
					$ret .="
				        task: " . j($m_txt) . ",
						id: \"|TOT|{$r['stacco_liv0_c']}\",				        
				        liv: \"liv_0\",
				        iconCls: \"iconSpedizione\",				        
	  					
	  					volume: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['volume'], 0,',','.') . "\"" . "," . "				         
	  					peso: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['peso'], 0,',','.') . "\"" . "," . "
	  					pallet: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['pallet'], 1,',','.') . "\"" . "," . "
	  					colli: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['colli'], 0,',','.') . "\"" . "," . "	  					
	  					importo: \"" . number_format($ar_tot_sped[$r['stacco_liv0_c']]['importo'], 2,',','.') . "\"" . "," . "	  					
	  						  						  					
				        leaf: true 
	       			";			        				
					$ret .=" },";							
									
					$inline_liv0 = $r['stacco_liv0_c'];	
				}
				
				
				$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];			
				$ret .= " {";
				$ret .="
					id: \"{$m_id}\",
			        task: " . j($r["txt"]) . ",
			        liv: \"liv_{$liv}\", 
			        expanded: {$liv1_expanded}, 
			        " . $this->nodo_dett_el_ordini($r, $liv) 
			         . $this->nodo_children_el_ordini($m_id, $liv +1, $r["val"], $liv1_expanded, count($ar)) . 
			        " 
       			";
				
				//if ($liv==1)
				 //// lo sposto in nodo_dett_el_ordini??? $ret .= ",n_carico: \"" . $r['n_carico'] . "\"";  
							        				
				$ret .=" },"; 		
			}
			
						
		$ret .= ']}';
		return $ret;		
	}





	public function get_iconCls($liv, $r){

		if ($liv == 2 && $r['fl_cli_bloc']==1)
		  return "iconBlocco";
		elseif ($liv == 3 && $r["TDFN11"]==1) //evaso
		  return "iconEvaso";
		elseif ($liv == 3 && $r["TDFN19"]==1) //evaso parziale
		  return "iconEvasoParz";
		elseif ($liv == 3 && $r["TDSWSP"]=='N') //non programmabile (o su TDFN04)
			return "iconNonProgrammabile";				
		elseif ($liv == 3 && ($r["fl_cons_conf"]!=0 || $r["TDFN06"] == 1)) //consegna confermata (spunta rossa)
		  return "iconConf";
		
/*		-> il divieto e' stato spostato subito dopo il num ordine
		elseif ($liv == 3 && $r["TDBLOC"] == 'A') //ordine bloccato autorizzato
		  return "icon-divieto_green-16";
*/		  
				
		elseif ($liv == 1 && trim($r['PSSTAT']) != '')
			return "carico-folder-" . trim($r['PSSTAT']);		
		elseif ( ($liv == 1 || $liv == 2) && $r['T_ROW'] == $r['T_TDFN11'])
			return "iconEvaso";		
		elseif ( ($liv == 1 || $liv == 2) && $r['T_ROW'] == $r['T_TDFN06'])
			return "iconConf";				
		else 			
		  return "task-folder";
	}


	private function nodo_dett_el_ordini($r, $liv){
	    
	   global $cfg_info_columns_properties;
	   $ret = "";

	   
	   $ret .= "iconCls: " . j($this->get_iconCls($liv, $r)) . ",";
	   $ret .= "tipo: " . j($r["tipo"]) . ",";
	   $ret .= "qtip_tipo: " . j($r["qtip_tipo"]) . ",";
	   $ret .= "tp_pri: \"{$r["tp_pri"]}\"" . ",";	 
	   $ret .= "qtip_pri: \"{$r["qtip_pri"]}\"" . ",";	 
	   $ret .= "fl_evaso: \"{$r["fl_evaso"]}\"" . ",";	   
	   $ret .= "fl_new: \"{$r["fl_new"]}\"" . ",";
	   $ret .= "tooltip_acc_paga: \"{$r["tooltip_acc_paga"]}\"" . ",";
	   
	  if ($liv==3){ //ORDINE	 
	      	      
		$ret .= "raggr: \"{$r["raggr"]}\"" . ",";
		$ret .= "tooltip_ripro: " . j($r["tooltip_ripro"]) . ",";				  
	  	$ret .= "nr: \"{$r["nr"]}\"" . ",";
	  	$ret .= "TDSWPP: " . j($r['TDSWPP']) . ",";
	  	$ret .= "k_ordine: \"{$r["k_ordine"]}\"" . ",";
	  	$ret .= "k_cli_des: \"{$r["k_cli_des"]}\"" . ",";	  	
	  	$ret .= "cod_iti: \"{$r["cod_iti"]}\"" . ",";	  							
		$ret .= "data_reg: \"{$r["data_reg"]}\"" . ",";
		$ret .= "cons_rich: \"{$r["cons_rich"]}\"" . ",";
		$ret .= "cons_prog: \"{$r["cons_prog"]}\"" . ",";
		$ret .= "stato_sped: \"{$r["stato_sped"]}\"" . ",";	
		$ret .= "tdfg06: \"{$r["tdfg06"]}\"" . ",";
		$ret .= "proposte: \"{$r["proposte"]}\"" . ",";
		$ret .= "caparra: \"{$r["caparra"]}\"" . ",";
		$ret .= "TDFN19: \"{$r["TDFN19"]}\"" . ",";
		$ret .= "tp_cap: \"{$r["tp_cap"]}\"" . ",";
		$ret .= "nr_fatt: \"{$r["nr_fatt"]}\"" . ",";
		$ret .= "contract: " . j(trim($r['contract'])) . ",";

		//in base alla data programmato scrivo lo stadio per abilitare o meno le modifiche
		//TODO: ottimizzare (ogni volta rileggo tutti i giorni stadio
		$giorni_stadio = $this->get_giorni_stadio(0, 0);
		$ret .= "cons_prog_stadio: \"{$giorni_stadio["{$r["cons_prog"]}"]}\"" . ",";		
		
		
		$ret .= "cons_conf: \"{$r["cons_conf"]}\"" . ",";
		$ret .= "data_cons_cli: \"{$r["data_cons_cli"]}\"" . ",";												
		$ret .= "stato: \"{$r["stato"]}\"" . ",";		
		$ret .= "qtip_stato: \"{$r["qtip_stato"]}\"" . ",";	
		$ret .= "gg_rit: \"{$r["gg_rit"]}\"" . ",";		
		
		$ret .= "i_paga: \"{$r["i_paga"]}\"" . ",";
	    }
	   
	    if ($liv==3 || $liv==2){ //ORDINE o CLIENTE	    
	    	$ret .= "data_disp: \"{$r["data_disp"]}\"" . ",";
	    	$ret .= "data_sped: \"{$r["data_sped"]}\"" . ",";	    	
	    	$ret .= "data_conf_ord: \"{$r["data_conf_ord"]}\"" . ",";	    	
	    	$ret .= "fl_data_disp: \"{$r["fl_data_disp"]}\"" . ",";
	    	$ret .= "seq_carico: \"{$r["seq_carico"]}\"" . ",";	
	    	$ret .= "contract: \"{$r["contract"]}\"" . ",";	
	    	$ret .= "tooltip_seq_carico: " . j($r["tooltip_seq_carico"]) . ",";
	    	$ret .= "dirty_seq_carico: " . j($r["dirty_seq_carico"]) . ",";
	    	$ret .= "priorita: " . j($r["priorita"]) .  ",";
	    	$ret .= "fl_cli_bloc: ". j($r['fl_cli_bloc']) . ",";
	    }	    
	  
	  
	  if ($liv==2){
	  	$ret .= "k_cli_des: \"{$r["k_cli_des"]}\"" . ",";	  	
		$ret .= "cliente: " . j(trim($r["cliente"])) . ",";
		$ret .= "gmap_ind: " . j($r['gmap_ind']) . ",";						
		$ret .= "scarico_intermedio: " . j($r['scarico_intermedio']) . ",";	
		$ret .= "cons_rich: " . j($r['cons_rich']) . ",";	
		$ret .= "tooltip_mezzi: " . j($r['tooltip_mezzi']) . ",";
		$ret .= "s_mezzi_red: " . j($r['s_mezzi_red']) . ",";
	    }	  
	  
	    $ret .= "k_carico: \"{$r["k_carico"]}\"" . ",";	    
	    $ret .= "n_carico: \"{$r["n_carico"]}\"" . ",";	    	    
	    $ret .= "sped_id: \"{$r["sped_id"]}\"" . ",";	    
		$ret .= "riferimento: " . trim(j($r["riferimento"])) . ",";	 
		$ret .= "articolo: " . trim(j($r["articolo"])) . ",";	 
   		$ret .= "colli: \"{$r["colli"]}\"" . ",";
   		$ret .= "c_prod: \"{$r["c_prod"]}\"" . ",";
   		$ret .= "pallet: \"" . number_format($r["pallet"], 1, ',', '.') ."\"" . ",";		
   		$ret .= "colli_disp: \"{$r["colli_disp"]}\"" . ",";
   		$ret .= "colli_sped: \"{$r["colli_sped"]}\"" . ",";	
   		if($cfg_info_columns_properties["info_show_vettore"] == 'Y')
   		   $ret .= "vettore:" . trim(j($r["vettore"])) . ",";	
   		if($cfg_info_columns_properties["info_show_architetto"] == 'Y')
   		   $ret .= "architetto:" . trim(j($r["architetto"])) . ",";	
	  	$ret .= "volume: \"" . number_format((float)$r["volume"], 1,',','.') . "\"" . ",";
	  	$ret .= "peso: \"" . number_format($r["peso"], 1,',','.') . "\"" . ",";
	  	$ret .= "importo: \"" . number_format((float)$r["importo"], 2,',','.') . "\"" . ",";				
	  	$ret .= "fl_bloc: \"{$r["fl_bloc"]}\"" . ",";			  		
	  	$ret .= "fl_art_manc: \"{$r["fl_art_manc"]}\"" . ",";
	  	$ret .= "art_da_prog: \"{$r["art_da_prog"]}\"" . ",";	  	
	  	$ret .= "fl_da_prog: \"{$r["fl_da_prog"]}\"" . ",";		
	  	$ret .= "ind_modif: \"{$r["ind_modif"]}\"" . ",";				
	  	
	
	  return $ret;
	}


    private function nodo_children_el_ordini($id_partenza, $liv, $ar, $liv1_expanded=null, $tot_liv0=0){
    	if ($liv > 3){
			return "leaf: true";
		}

		
    	$ret = "children: [ ";
    	foreach ($ar as $kr => $r){
    		
    		//se ho meno di 100 clienti (per questo itinerario), esplodo gia'
    		//if($liv1_expanded== 'true' && count($ar) < 100)
    			
    		//se un solo itinerario, un solo cliente... allora espando subito gli ordini
    		if($liv1_expanded== 'true' && $tot_liv0 ==1 && count($ar)==1)
    			$l_expanded = 'true';
    		else
    			$l_expanded = 'false';
    		
    		
				$m_id = $id_partenza . "|LIV{$liv}|" . $r["cod"];    		
				$ret .= " {";
				if ($liv == 3) $ret .= "leaf: true,";
				$ret .="
					id: \"" . acs_u8e($m_id) . "\",
			        task: " . j($r["txt"]) . ",			        
			        liv: \"liv_{$liv}\",			        
			        iconCls: " . j($this->get_iconCls($liv, $r)) . ",
			        expanded: {$l_expanded},
			        " . $this->nodo_dett_el_ordini($r, $liv)			        			        
       			;

				//precarico gli ordini (se no ho meno di 5) se ho meno di 5 clienti				
				if ($liv1_expanded== 'true' && count($r["val"]) < 5 && count($ar) < 5)
					$ret .= $this->nodo_children_el_ordini($m_id, $liv +1, $r["val"]);					
				
				$ret .=" },";
    	}
		$ret .= " ]";
	 return $ret;	
    }





























	public function crea_alberatura($its){
		$ar = array();
		$ar_tot = array();		
		$liv1_c = $liv2_c = $liv3_c = 0;
		$liv1_v = $liv2_v = $liv3_v = "--------------";		
		while ($row = db2_fetch_assoc($its)) {
			if ($row['C_LIV1'] != $liv1_v){
				$liv1_c += 1;
				$liv2_c = $liv3_c = 0;
				$liv2_v = $liv3_v = "-----------";
				$liv1_v = $row['C_LIV1'];
				$ar[$liv1_c] = array("cod"=>$liv1_v, "txt"=> $row['D_LIV1'], "dta"=>array(), "val"=>array()); 
			}
			if ($row['C_LIV2'] != $liv2_v){
				$liv2_c += 1;
				$liv3_c = 0; $liv3_v = "-----------";								
				$liv2_v = $row['C_LIV2'];
				$ar[$liv1_c]["val"][$liv2_c] = array("cod"=>$liv2_v, "txt"=> $row['D_LIV2'], "dta"=>array(), "val"=>array()); 
			}			
			if ($row['C_LIV3'] != $liv3_v){
				$liv3_c += 1;								
				$liv3_v = $row['C_LIV3'];
				$ar[$liv1_c]["val"][$liv2_c]["val"][$liv3_c] = array("cod"=>$liv3_v,
						"txt"=> $row['D_LIV3'],
						"dta"=>array()); 
			}			
			
			//sommo il volume o il peso o i colli .... nella cella della data
			if (!isset($ar[$liv1_c]["dta"][$row['TPDTSPP']]))
				$ar[$liv1_c]["dta"][$row['TPDTSPP']] = array("VOL"=>0, "COL"=>0, "ORD"=>0);
			if (!isset($ar[$liv1_c]["val"][$liv2_c]["dta"][$row['TPDTSPP']]))
				$ar[$liv1_c]["val"][$liv2_c]["dta"][$row['TPDTSPP']] = array("VOL"=>0, "COL"=>0, "ORD"=>0);	
			if (!isset($ar[$liv1_c]["val"][$liv2_c]["val"][$liv3_c]["dta"][$row['TPDTSPP']]))
				$ar[$liv1_c]["val"][$liv2_c]["val"][$liv3_c]["dta"][$row['TPDTSPP']] = array("VOL"=>0, "COL"=>0, "ORD"=>0);
			
			$ar[$liv1_c]["dta"][$row['TPDTSPP']]['VOL'] += $row['TPVOLO'];  
			$ar[$liv1_c]["dta"][$row['TPDTSPP']]['COL'] += $row['TPCOLO'];
			$ar[$liv1_c]["dta"][$row['TPDTSPP']]['ORD'] += 1;
			$ar[$liv1_c]["val"][$liv2_c]["dta"][$row['TPDTSPP']]['VOL'] += $row['TPVOLO'];
			$ar[$liv1_c]["val"][$liv2_c]["dta"][$row['TPDTSPP']]['COL'] += $row['TPCOLO'];
			$ar[$liv1_c]["val"][$liv2_c]["dta"][$row['TPDTSPP']]['ORD'] += 1;
			$ar[$liv1_c]["val"][$liv2_c]["val"][$liv3_c]["dta"][$row['TPDTSPP']]['VOL'] += $row['TPVOLO'];
			$ar[$liv1_c]["val"][$liv2_c]["val"][$liv3_c]["dta"][$row['TPDTSPP']]['COL'] += $row['TPCOLO'];
			$ar[$liv1_c]["val"][$liv2_c]["val"][$liv3_c]["dta"][$row['TPDTSPP']]['ORD'] += 1;
			
			//totali
			if (!isset($ar_tot[$row['TPDTSPP']]))
				$ar_tot[$row['TPDTSPP']] = array("VOL"=>0, "COL"=>0, "ORD"=>0);
			$ar_tot[$row['TPDTSPP']]["VOL"] += $row['TPVOLO'];
			$ar_tot[$row['TPDTSPP']]["COL"] += $row['TPCOLO'];
			$ar_tot[$row['TPDTSPP']]["ORD"] += 1;
		}
	 return array("det"=>$ar, "tot"=>$ar_tot);		
	}


	
	
	public function out_table_cell($dta, $da_data, $n_giorni, $field_dett){
		$r = "";
		$d = $da_data->format('Ymd');
		for ($i=1; $i<=$n_giorni; $i++){
			if(isset($dta[$d])) $c = $dta[$d][$field_dett]; else $c = "&nbsp;";
			$r .= "<th class=n>" . $c . "</th>";
			$d = date('Ymd', strtotime($d . " +1 days"));			
		}		
     return $r;  		
	}	
	
	public function print_table_titoli($da_data, $n_giorni, $ar_tot){
		echo "<tr>
				<th style=\"width: 200px;\">
				PROGRAMMA SPEDIZIONI
				<div class=\"n f-n\">
				 <a href=\"index.php?modulo=spedizioni&field_dett=VOL\">Volume</a><br>
				 <a href=\"index.php?modulo=spedizioni&field_dett=COL\">Colli</a><br>				
				 <a href=\"index.php?modulo=spedizioni&field_dett=ORD\">Ordini</a>
				</div>				
			  </th>";
					
		$d = $da_data->format('Y-m-d');		
		for ($i=1; $i<=$n_giorni; $i++){
			$d_txt = date('m-d', strtotime($d));
			$d_as = date('Ymd', strtotime($d));
			echo "<th>
				{$d_txt}
				<div class=\"n f-n\">
				 {$ar_tot[$d_as]['VOL']}<br>
				 {$ar_tot[$d_as]['COL']}<br>				
				 {$ar_tot[$d_as]['ORD']}
				</div>				
				</th>";
			$d = date('Y-m-d', strtotime($d . " +1 days"));
		}		  
		echo "</tr>";						
	} 
	
	
	
	public function get_elenco_raggruppato_itinerari($id){		
		global $conn, $id_ditta_default;
		global $cfg_mod_Spedizioni;
		
		$oggi = oggi_AS_date();
		$next_3_months = date('Ymd', strtotime('+3 month'));
		
		switch($this->imposta_field_dett()){
			case "VOL": $sum_field = ", SUM(TDVOLU) AS TOT_F, MAX(CSVOLD) AS DISP_F "; break;
			case "ORD": $sum_field = ", COUNT(*)    AS TOT_F, COUNT(*) AS DISP_F ";  break;
			case "COL": $sum_field = ", SUM(TDTOCO) AS TOT_F, SUM(TDTOCO) AS DISP_F "; break;
			case "PAL": $sum_field = ", SUM(TDBANC) AS TOT_F, MAX(CSBAND) AS DISP_F "; break;
			default: $val = 0; break;
		}	
 
 
 		if ($id == "root")
		{
			$sql_where_flt = " 1=1 ";
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$sottostringhe = explode("|", $id);		 	
			$liv1 = $sottostringhe[2];
			$liv2 = $sottostringhe[4];
			$sql_where_flt = " ( /*TDASPE='$liv1' AND*/ CSCITI='$liv2' ) ";
		 }	
 
 
				
		$sql = "SELECT TRIM(TA_ITIN.TAASPE) as C_LIV1, TRIM(TA_ITIN.TAASPE) as D_LIV1,
				TRIM(CSCITI) as C_LIV2, TRIM(CSCITI) as D_LIV2,
				CSDTSP, CSCVET, CSCAUT, CSCCON, CSPROG, CSHMPG, CSSTSP $sum_field				 
				FROM {$cfg_mod_Spedizioni['file_calendario']} SP
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
				  ON TDDT = SP.CSDT AND SP.CSPROG = TD.TDNBOC AND " . $this->get_where_std() . " AND TDSWSP = 'Y'
				" . $this->add_riservatezza() . "  
			 	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
			 	  ON TA_ITIN.TATAID='ITIN' AND TA_ITIN.TADT = SP.CSDT AND TA_ITIN.TAKEY1 = SP.CSCITI
				WHERE SP.CSDT = '{$id_ditta_default}' AND SP.CSCALE = '*SPR' AND $sql_where_flt AND " . $this->get_where_std('none', array('filtra_su' => 'SP')) . " 
				  AND (
				   	   (SP.CSSTSP <> 'PI' AND SP.CSDTSP >= {$oggi})
				  	OR ( (SP.CSSTSP = 'PI' OR SP.CSSTSP = '') AND SP.CSDTSP >= {$oggi} AND SP.CSDTSP <= {$next_3_months})				   	    
				  	OR (TD.TDONDO <> '')
				  )
				GROUP BY TAASPE, CSCITI, CSDTSP, CSCVET, CSCAUT, CSCCON, CSPROG, CSHMPG, CSSTSP, TA_ITIN.TASITI
				HAVING MAX(TD.TDONDO) IS NOT NULL OR CSSTSP NOT IN('GA', 'AN') 								
				ORDER BY TAASPE, TA_ITIN.TASITI, SP.CSDTSP, CSHMPG, CSPROG
				";
				/* FETCH FIRST 500 ROWS ONLY */
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		return $stmt;
	}	

	public function get_elenco_raggruppato_itinerari_per_data($id){		
		global $conn, $id_ditta_default;
		global $cfg_mod_Spedizioni;
		
		$oggi = oggi_AS_date();
		$next_3_months = date('Ymd', strtotime('+3 month'));
		
		switch($this->imposta_field_dett()){
			case "VOL": $sum_field = ", SUM(TDVOLU) AS TOT_F, MAX(SP.CSVOLD) AS DISP_F "; break;
			case "ORD": $sum_field = ", COUNT(*)    AS TOT_F, COUNT(*) AS DISP_F ";  break;
			case "COL": $sum_field = ", SUM(TDTOCO) AS TOT_F, SUM(TD.TDTOCO) AS DISP_F "; break;
			case "PAL": $sum_field = ", SUM(TDBANC) AS TOT_F, MAX(SP.CSBAND) AS DISP_F "; break;			
			default: $val = 0; break;
		}	
 
 
 		if ($id == "root")
		{
			$sql_where_flt = " 1=1 ";
		}
		else
		 {
		 	//trovo liv1 e liv2 selezionati
		 	$sottostringhe = explode("|", $id);		 	
			$liv1 = $sottostringhe[2]; //anno-settimana
			$liv2 = $sottostringhe[4];
			$sql_where_flt = " ( TDDTEP='$liv2' ) ";
		 }	
				
				
		$sql = "SELECT CAL.CSAARG, CAL.CSNRSE
					 , SP.CSCITI, TA_ITIN.TADESC AS D_ITIN
					 , SP.CSDTSP, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSPROG, SP.CSSTSP	 
					  $sum_field 				 
				FROM {$cfg_mod_Spedizioni['file_calendario']} SP
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
			  		ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG
					AND  " . $this->get_where_std() . " AND TDSWSP = 'Y' AND $sql_where_flt
				" . $this->add_riservatezza() . "							
			 	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
			 	  ON TA_ITIN.TADT = SP.CSDT AND TA_ITIN.TATAID='ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI				 	  				 	  						   						
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
			  		ON CAL.CSDT = SP.CSDT AND CAL.CSCALE = '*CS' AND CAL.CSDTRG = SP.CSDTSP				  						
				WHERE SP.CSDT = '{$id_ditta_default}' AND SP.CSCALE = '*SPR' AND SP.CSDTRG = 0 
				  AND (
				   	   (SP.CSSTSP <> 'PI' AND SP.CSDTSP >= {$oggi})
				  	OR ( (SP.CSSTSP = 'PI' OR SP.CSSTSP = '') AND SP.CSDTSP >= {$oggi} AND SP.CSDTSP <= {$next_3_months})				   	    
				  	OR (TD.TDONDO <> '')
				    )
				  AND " . $this->get_where_std('none', array('filtra_su' => 'SP')) . "
				GROUP BY CAL.CSAARG, CAL.CSNRSE	 
				  		, SP.CSCITI, TA_ITIN.TADESC 
				  		, SP.CSDTSP, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSPROG, SP.CSSTSP	
				HAVING MAX(TD.TDONDO) IS NOT NULL OR SP.CSSTSP NOT IN('GA', 'AN')				
				ORDER BY CAL.CSAARG, CAL.CSNRSE, SP.CSDTSP, SP.CSCVET";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		return $stmt;
	}	
	


	
	
	public function get_elenco_ordini_create_filtro($form_values){
		
		$filtro = array();
		$filtro["cliente"] = $form_values['f_cliente_cod'];
		
		if (strlen($form_values['f_destinazione_cod']) > 0)
			$filtro["destinazione"] = $form_values['f_destinazione_cod'];
		
		$filtro["riferimento"] = $form_values['f_riferimento'];
		
		$filtro["agente"] = $form_values['f_agente'];
	
		
		//data programmata
		if (strlen($form_values['f_data_dal']) > 0)
			$filtro["data_dal"] = date('Ymd', strtotime($form_values['f_data_dal']));
		if (strlen($form_values['f_data_al']) > 0)
			$filtro["data_al"] = date('Ymd', strtotime($form_values['f_data_al']));
		
		//data spedizione
		if (strlen($form_values['f_data_sped_dal']) > 0)
			$filtro["data_sped_dal"] = date('Ymd', strtotime($form_values['f_data_sped_dal']));
		if (strlen($form_values['f_data_sped_al']) > 0)
			$filtro["data_sped_al"] = date('Ymd', strtotime($form_values['f_data_sped_al']));
		
		
		//data rilascio
		if (strlen($form_values['f_data_ril_dal']) > 0)
			$filtro["data_ril_dal"] = date('Ymd', strtotime($form_values['f_data_ril_dal']));
		if (strlen($form_values['f_data_ril_al']) > 0)
			$filtro["data_ril_al"] = date('Ymd', strtotime($form_values['f_data_ril_al']));
		
		
		//data ricezione (dal, al)
		if (strlen($form_values['f_data_ricezione_dal']) > 0)
			$filtro["data_ricezione_dal"] = date('Ymd', strtotime($form_values['f_data_ricezione_dal']));
		if (strlen($form_values['f_data_ricezione_al']) > 0)
			$filtro["data_ricezione_al"] = date('Ymd', strtotime($form_values['f_data_ricezione_al']));
		
		//data conferma (dal, al)
		if (strlen($form_values['f_data_conferma_dal']) > 0)
			$filtro["data_conferma_dal"] = date('Ymd', strtotime($form_values['f_data_conferma_dal']));
		if (strlen($form_values['f_data_conferma_al']) > 0)
			$filtro["data_conferma_al"] = date('Ymd', strtotime($form_values['f_data_conferma_al']));
		
		//data scarico (dal, al)
		if (strlen($form_values['f_data_scarico_dal']) > 0)
			$filtro["data_scarico_dal"] = date('Ymd', strtotime($form_values['f_data_scarico_dal']));
		if (strlen($form_values['f_data_scarico_al']) > 0)
			$filtro["data_scarico_al"] = date('Ymd', strtotime($form_values['f_data_scarico_al']));
		
		//data disponibilita (dal, al)
		if (strlen($form_values['f_data_disp_dal']) > 0)
			$filtro["data_disp_dal"] = date('Ymd', strtotime($form_values['f_data_disp_dal']));
		if (strlen($form_values['f_data_disp_al']) > 0)
			$filtro["data_disp_al"] = date('Ymd', strtotime($form_values['f_data_disp_al']));
		
		//data evasione richiesta (dal, al)
		if (strlen($form_values['f_data_ev_rich_dal']) > 0)
			$filtro["data_ev_rich_dal"] = date('Ymd', strtotime($form_values['f_data_ev_rich_dal']));
		if (strlen($form_values['f_data_ev_rich_al']) > 0)
			$filtro["data_ev_rich_al"] = date('Ymd', strtotime($form_values['f_data_ev_rich_al']));
						
		
		
		if (strlen($form_values['f_data_ricezione']) > 0)
			$filtro["data_ricezione"] = date('Ymd', strtotime($form_values['f_data_ricezione']));
		if (strlen($form_values['f_data_conferma']) > 0)
			$filtro["data_conferma"] = date('Ymd', strtotime($form_values['f_data_conferma']));
		
		$filtro['carico_assegnato'] = $form_values['f_carico_assegnato'];
		$filtro['lotto_assegnato'] 	= $form_values['f_lotto_assegnato'];
		$filtro['proforma_assegnato'] 	= $form_values['f_proforma_assegnato'];
		
		$filtro['indice_rottura_assegnato'] 	= $form_values['indice_rottura_assegnato'];		
		$filtro['indice_rottura'] 	= $form_values['f_indice_rottura'];
		$filtro['num_sped'] 		= $form_values['f_num_sped'];
		$filtro['conf'] 		    = $form_values['f_conf'];
		
		$filtro['collo_disp'] 		= $form_values['f_collo_disp'];
		$filtro['collo_sped'] 		= $form_values['f_collo_sped'];
		
		$filtro['anomalie_evasione']= $form_values['f_anomalie_evasione'];
		$filtro['ordini_evasi'] 	= $form_values['f_ordini_evasi'];
		$filtro['area']				= $form_values['f_area'];
		$filtro['stabilimento']		= $form_values['f_stabilimento'];
		$filtro['divisione'] 		= $form_values['f_divisione'];
		$filtro['modello'] 			= $form_values['f_modello'];
		$filtro['referente_cliente']= $form_values['f_referente_cliente'];
		$filtro['referente_ordine']	= $form_values['f_referente_ordine'];
		$filtro['num_ordine'] 		= $form_values['f_num_ordine'];
		$filtro['num_carico'] 		= $form_values['f_num_carico'];
		$filtro['num_lotto'] 		= $form_values['f_num_lotto'];
		$filtro['num_proforma'] 	= $form_values['f_num_proforma'];
		$filtro['itinerario'] 		= $form_values['f_itinerario'];
		$filtro['tipologia_ordine'] = $form_values['f_tipologia_ordine'];
		$filtro['stato_ordine'] 	= $form_values['f_stato_ordine'];
		$filtro['priorita'] 		= $form_values['f_priorita'];
		$filtro['solo_bloccati']	= $form_values['f_solo_bloccati'];
		$filtro['confermati']		= $form_values['f_confermati'];
		$filtro['hold']				= $form_values['f_hold'];
		$filtro['vettore']		    = $form_values['f_vettore'];
		$filtro['piva']     		= $form_values['f_piva'];
		
		
		$filtro['num_contract'] 	= $form_values['f_num_contract'];
		$filtro['mercato'] 			= $form_values['f_mercato'];
		$filtro['forniture_MTO'] 	= $form_values['f_forniture_MTO'];
		$filtro['articolo'] 		= $form_values['f_articolo'];
		$filtro['tipo_ordine']		= $form_values['f_tipo_ordine'];
		$filtro['stadio']		    = $form_values['f_stadio'];
		$filtro['ava_paga']		    = $form_values['f_ava_paga'];
		
		$filtro['prov_d']           = $form_values['f_prov_d'];
		$filtro['loc_d']            = $form_values['f_loc_d'];
		
		
		return $filtro;
	}
	
	
	
	
	

	public function get_elenco_ordini($filtro, $ordina_per_tipo_ord = 'N', $tipo = '', $field_data = 'TDDTEP', $join_articoli = false, $separa_hold = 'N', $forza_campi_select = null, $forza_campi_group_by = null, $forza_campi_order_by = null){		
		global $conn;
		global $cfg_mod_Spedizioni, $cfg_info_columns_properties;
		
		
		$where = "";
		
		//Se lavoriamo con la TDDTSP allora la spedizione di riferimento e' la TDNBOF e non la TDNBOC
		if ($field_data == 'TDDTSP')
		    $td_sped_field = 'TDNBOF';
		else
		    $td_sped_field = 'TDNBOC';
		
		$where .= sql_where_by_combo_value("TD.{$td_sped_field}", $filtro['ar_spedizioni']);
		
		if (isset($filtro["area"]) && strlen($filtro["area"]) > 0)			
			$where .= " AND TDASPE='{$filtro["area"]}'";

		
		//gestione multiselect (valori separati da ,)
		if (isset($filtro["itinerario"]) && is_array($filtro["itinerario"])){
			$ar_itinerario = $filtro["itinerario"];
			if (count($ar_itinerario) == 1)
				$where .= " AND TDCITI='{$filtro["itinerario"][0]}'";
			else if (count($ar_itinerario) > 1)
				$where .= " AND TDCITI IN (" . sql_t_IN($ar_itinerario) . ") ";			
		}		
		elseif (isset($filtro["itinerario"]) && strlen($filtro["itinerario"]) > 0){
			$ar_itinerario = explode(",", $filtro["itinerario"]);
			if (count($ar_itinerario) == 1)
				$where .= " AND TDCITI='{$filtro["itinerario"]}'";
			else
				$where .= " AND TDCITI IN (" . sql_t_IN($ar_itinerario) . ") ";
		}

		/*if (isset($filtro["vettore"]))
			$where .= " AND CSCVET='{$filtro["vettore"]}'";		*/
		if (isset($filtro["data_spedizione"]))
		{
			if (isset($filtro["a_data_spedizione"]))			
				$where .= " AND {$field_data}>={$filtro["data_spedizione"]} AND {$field_data}<={$filtro["a_data_spedizione"]} ";
			else
				$where .= " AND {$field_data}={$filtro["data_spedizione"]} ";	
		}		

		if (isset($filtro["cliente"]) && strlen($filtro["cliente"]) > 0){
			if ($cfg_mod_Spedizioni['disabilita_sprintf_codice_cliente'] == 'Y')
				$where .= " AND TDCCON=" . sql_t($filtro["cliente"]);
			else
				$where .= " AND TDCCON=" . sql_t(sprintf("%09s", $filtro["cliente"]));
		}	
		
		if (isset($filtro["destinazione"]) && strlen($filtro["destinazione"]) > 0)
			$where .= " AND TDCDES =" . sql_t($filtro["destinazione"]);		
			
		if (isset($filtro["TDSWSP"]))
			$where .= " AND TDSWSP='{$filtro["TDSWSP"]}'";			
		
		if (isset($filtro["riferimento"]) && strlen($filtro["riferimento"]) > 0)
			$where .= " AND UPPER(TDVSRF) LIKE '%" . strtoupper($filtro["riferimento"]) . "%'";		
		
		if (isset($filtro["agente"])  && strlen($filtro["agente"]) > 0 )
				$where .= " AND TDCAG1 ='{$filtro["agente"]}'";

		//data programmata
		if (isset($filtro["data"]) && strlen($filtro["data"]) > 0)
			$where .= " AND {$field_data}={$filtro["data"]}";				

		//data programmata (dal - al)
		if (isset($filtro["data_dal"]) && strlen($filtro["data_dal"]) > 0)
			$where .= " AND {$field_data} >= {$filtro["data_dal"]}";
		if (isset($filtro["data_al"]) && strlen($filtro["data_al"]) > 0)
				$where .= " AND {$field_data} <= {$filtro["data_al"]}";		
		
		//data spedizione (dal - al)
		if (isset($filtro["data_sped_dal"]) && strlen($filtro["data_sped_dal"]) > 0)
			$where .= " AND TDDTSP >= {$filtro["data_sped_dal"]}";
		if (isset($filtro["data_sped_al"]) && strlen($filtro["data_sped_al"]) > 0)
			$where .= " AND TDDTSP <= {$filtro["data_sped_al"]}";		
		
		//data rilascio (dal - al)
		if (isset($filtro["data_ril_dal"]) && strlen($filtro["data_ril_dal"]) > 0)
			$where .= " AND TDDTRP >= {$filtro["data_ril_dal"]}";
		if (isset($filtro["data_ril_al"]) && strlen($filtro["data_ril_al"]) > 0)
			$where .= " AND TDDTRP <= {$filtro["data_ril_al"]}";
		
		//data ricezione (dal - al)
		if (isset($filtro["data_ricezione_dal"]) && strlen($filtro["data_ricezione_dal"]) > 0)
			$where .= " AND TDODRE >= {$filtro["data_ricezione_dal"]}";
		if (isset($filtro["data_ricezione_al"]) && strlen($filtro["data_ricezione_al"]) > 0)
			$where .= " AND TDODRE <= {$filtro["data_ricezione_al"]}";
		
		//data conferma (dal - al)
		if (isset($filtro["data_conferma_dal"]) && strlen($filtro["data_conferma_dal"]) > 0)
			$where .= " AND TDDTCF >= {$filtro["data_conferma_dal"]}";
		if (isset($filtro["data_conferma_al"]) && strlen($filtro["data_conferma_al"]) > 0)
			$where .= " AND TDDTCF <= {$filtro["data_conferma_al"]}";

		//data scarico (dal - al)
		if (isset($filtro["data_scarico_dal"]) && strlen($filtro["data_scarico_dal"]) > 0)
			$where .= " AND SP.CSDTSC >= {$filtro["data_scarico_dal"]}";
		if (isset($filtro["data_scarico_al"]) && strlen($filtro["data_scarico_al"]) > 0)
			$where .= " AND SP.CSDTSC <= {$filtro["data_scarico_al"]}";		
		
		//data disponibilita' (dal - al)
		if (isset($filtro["data_disp_dal"]) && strlen($filtro["data_disp_dal"]) > 0)
			$where .= " AND TDDTDS >= {$filtro["data_disp_dal"]}";
		if (isset($filtro["data_disp_al"]) && strlen($filtro["data_disp_al"]) > 0)
			$where .= " AND TDDTDS <= {$filtro["data_disp_al"]}";		
		
		//data evasione richiesta (dal - al)
		if (isset($filtro["data_ev_rich_dal"]) && strlen($filtro["data_ev_rich_dal"]) > 0)
			$where .= " AND TDODER >= {$filtro["data_ev_rich_dal"]}";
		if (isset($filtro["data_ev_rich_al"]) && strlen($filtro["data_ev_rich_al"]) > 0)
			$where .= " AND TDODER <= {$filtro["data_ev_rich_al"]}";
		
		if (isset($filtro["data_ricezione"]) && strlen($filtro["data_ricezione"]) > 0)
			$where .= " AND TDODRE={$filtro["data_ricezione"]}";

		if (isset($filtro["data_conferma"]) && strlen($filtro["data_conferma"]) > 0)
			$where .= " AND TDDTCF={$filtro["data_conferma"]}";
		
		
		if (isset($filtro["num_ordine"])  && strlen($filtro["num_ordine"]) > 0 )
			$where .= " AND TDONDO='{$filtro["num_ordine"]}'";		
		
		if (isset($filtro["num_carico"])  && strlen($filtro["num_carico"]) > 0 )
			$where .= " AND TDNRCA ='{$filtro["num_carico"]}'";		
		if (isset($filtro["num_lotto"])  && strlen($filtro["num_lotto"]) > 0 )
			$where .= " AND TDNRLO ='{$filtro["num_lotto"]}'";		
		if (isset($filtro["num_proforma"])  && strlen($filtro["num_proforma"]) > 0 )
			$where .= " AND TDPROF LIKE '%{$filtro["num_proforma"]}%'";
		
		if (isset($filtro["num_sped"])  && strlen($filtro["num_sped"]) > 0 ){
		    if ($field_data == 'TDDTSP')
		        $where .= " AND {$td_sped_field} = {$filtro['num_sped']} ";
	        else
	            $where .= " AND (TDNBOC = {$filtro['num_sped']} OR TDNBOF = {$filtro['num_sped']})";
		}
		

		if (isset($filtro["seca"])  && strlen($filtro["seca"]) > 0 )
			$where .= " AND TDSECA ='{$filtro["seca"]}'";
		
	
		if (isset($filtro["vettore"])  && strlen($filtro["vettore"]) > 0 )
		    $where .= " AND TDVETT ='{$filtro["vettore"]}'";
		
	    if (isset($filtro["tipo_ordine"])  && strlen($filtro["tipo_ordine"]) > 0 )
	        $where .= " AND TDOTPD ='{$filtro["tipo_ordine"]}'";
	  
        if (isset($filtro["piva"])  && strlen($filtro["piva"]) > 0 ){
            
            //solo clienti con partita iva
            if ($filtro["piva"] == "Y"){
                $where.= " AND CFPIVA <> '' ";
            }
           
            if ($filtro["piva"] == "N"){
                $where.= " AND CFPIVA = '' ";
            }
        }
		
		
		if ($filtro['carico_assegnato'] == "Y")
			$where .= " AND TDNRCA > 0 ";		
		if ($filtro['carico_assegnato'] == "N")
			$where .= " AND TDNRCA = 0 ";		
		if ($filtro['lotto_assegnato'] == "Y")
			$where .= " AND TDNRLO > 0 ";		
		if ($filtro['anomalie_evasione'] == "Y")
			$where .= " AND TDOPUN = 'N' ";		
		if ($filtro['lotto_assegnato'] == "N")
			$where .= " AND TDNRLO = 0 ";

		if ($filtro['proforma_assegnato'] == "Y")
			$where .= " AND TDPROF <> '' ";
		if ($filtro['proforma_assegnato'] == "N")
			$where .= " AND TDPROF = '' ";		
		

		if (isset($filtro["indice_rottura"])  && strlen($filtro["indice_rottura"]) > 0 )
			$where .= " AND TDIRLO LIKE '%{$filtro["indice_rottura"]}%'";		
		if ($filtro['indice_rottura_assegnato'] == "Y")
			$where .= " AND TDIRLO <> '' ";
		if ($filtro['indice_rottura_assegnato'] == "N")
			$where .= " AND TDIRLO = '' ";
		
		if (isset($filtro["conf"])  && strlen($filtro["conf"]) > 0 )
		    $where .= " AND TDISON = '".trim($filtro["conf"])."'";
		
		
		if ($filtro['collo_disp'] == "Y")
			$where .= " AND TDCOPR > 0 ";
		if ($filtro['collo_disp'] == "N")
			$where .= " AND TDCOPR = 0 ";		
		
		if ($filtro['collo_sped'] == "Y")
			$where .= " AND TDCOSP > 0 ";
		if ($filtro['collo_sped'] == "N")
			$where .= " AND TDCOSP = 0 ";		
		
		//solo ordini evasi	
		if ($filtro['ordini_evasi'] == "Y"){
			$where .= " AND TDSWPP = 'Y' AND TDFN11 = 1 ";
		}		
		//solo ordini da evadere
		if ($filtro['ordini_evasi'] == "N"){
			$where .= " AND TDSWPP = 'Y' AND TDFN11 = 0 ";
		}		

/*		
		if (isset($filtro["divisione"]) && strlen($filtro["divisione"]) > 0)
			$where .= " AND TDCDIV='{$filtro["divisione"]}'";
*/
		//gestione multiselect (valori separati da ,) o array
		if (isset($filtro["divisione"]) && is_array($filtro["divisione"])){
			$ar_divisione = $filtro["divisione"];
			if (count($ar_divisione) == 1)
				$where .= " AND TDCDIV='{$filtro["divisione"][0]}'";
			else if (count($ar_divisione) > 1)
				$where .= " AND TDCDIV IN (" . sql_t_IN($ar_divisione) . ") ";
		}
		else if (isset($filtro["divisione"]) && strlen(trim($filtro["divisione"])) > 0){
			$ar_divisione = explode(",", $filtro["divisione"]);
			if (count($ar_divisione) == 1)
				$where .= " AND TDCDIV='{$filtro["divisione"]}'";
			else
				$where .= " AND TDCDIV IN (" . sql_t_IN($ar_divisione) . ") ";
		}					
				
		
		//gestione multiselect (valori separati da ,) o array
		if (isset($filtro["stabilimento"]) && is_array($filtro["stabilimento"])){
		    $ar_stab = $filtro["stabilimento"];
		    if (count($ar_stab) == 1)
		        $where .= " AND TDSTAB='{$filtro["stabilimento"][0]}'";
		    else if (count($ar_stab) > 1)
		        $where .= " AND TDSTAB IN (" . sql_t_IN($ar_stab) . ") ";
		}
		else if (isset($filtro["stabilimento"]) && strlen(trim($filtro["stabilimento"])) > 0){
		    $ar_stab = explode(",", $filtro["stabilimento"]);
		    if (count($ar_stab) == 1)
		        $where .= " AND TDSTAB='{$filtro["stabilimento"]}'";
		    else
		        $where .= " AND TDSTAB IN (" . sql_t_IN($ar_stab) . ") ";
		}

		//gestione multiselect (valori separati da ,) o array
		if (isset($filtro["tipologia_ordine"]) && is_array($filtro["tipologia_ordine"])){
			$ar_tipologia_ordine = $filtro["tipologia_ordine"];
			if (count($ar_tipologia_ordine) == 1)
				$where .= " AND TDCLOR='{$filtro["tipologia_ordine"][0]}'";
			else if (count($ar_tipologia_ordine) > 1)
				$where .= " AND TDCLOR IN (" . sql_t_IN($ar_tipologia_ordine) . ") ";
		}		
		else if (isset($filtro["tipologia_ordine"]) && strlen($filtro["tipologia_ordine"]) > 0){
			$ar_tipologia_ordine = explode(",", $filtro["tipologia_ordine"]);
			if (count($ar_tipologia_ordine) == 1)
				$where .= " AND TDCLOR='{$filtro["tipologia_ordine"]}'";
			else
				$where .= " AND TDCLOR IN (" . sql_t_IN($ar_tipologia_ordine) . ") ";
		}
		
		if (isset($filtro["modello"]) && strlen($filtro["modello"]) > 0)
			$where .= " AND TDCVN1='{$filtro["modello"]}'";
		
		if (isset($filtro["referente_cliente"]) && strlen($filtro["referente_cliente"]) > 0)
			$where .= " AND TDCOCL='{$filtro["referente_cliente"]}'";
		if (isset($filtro["referente_ordine"]) && strlen($filtro["referente_ordine"]) > 0)
			$where .= " AND TDCORE='{$filtro["referente_ordine"]}'";
		
		
		//gestione multiselect (valori separati da ,) o array
		if (isset($filtro["stato_ordine"]) && is_array($filtro["stato_ordine"])){
			$ar_stato_ordine = $filtro["stato_ordine"];
			if (count($ar_stato_ordine) == 1)
				$where .= " AND TDSTAT='{$filtro["stato_ordine"][0]}'";
			else if (count($ar_stato_ordine) > 1)
				$where .= " AND TDSTAT IN (" . sql_t_IN($ar_stato_ordine) . ") ";
		}		
		else if (isset($filtro["stato_ordine"]) && strlen(trim($filtro["stato_ordine"])) > 0){
			$ar_stato_ordine = explode(",", $filtro["stato_ordine"]);
			if (count($ar_stato_ordine) == 1)
				$where .= " AND TDSTAT='{$filtro["stato_ordine"]}'";
			else
				$where .= " AND TDSTAT IN (" . sql_t_IN($ar_stato_ordine) . ") ";
		}
		
		if (isset($filtro["priorita"]) && strlen($filtro["priorita"]) > 0)
			$where .= " AND TDOPRI='{$filtro["priorita"]}'";		
		
		if ($filtro["solo_bloccati"] == 1) //solo bloccati
			$where .= " AND (TDBLEV='Y' OR TDBLOC = 'Y') ";		
		if ($filtro["solo_bloccati"] == 'N') //bloccati esclusi
			$where .= " AND (TDBLEV <> 'Y' AND TDBLOC <> 'Y') ";		
		
		
		if ($filtro["confermati"] == 'C') //solo confermati (da Info)
			$where .= " AND TDFN06 = 1 ";		
		if ($filtro["confermati"] == 'N') //solo NON confermati (da Info)
			$where .= " AND TDFN06 = 0 ";		
		
		if ($filtro["hold"] == 'H')//solo Hold
			$where .= " AND TDSWSP = 'N' ";
		if ($filtro["hold"] == 'P') //solo programmabili (NON HOLD)
			$where .= " AND TDSWSP = 'Y' ";		
		
		if ($filtro["solo_sped_DP"] == 'Y') //solo spedizioni con stato DP (da programmare)		
			$where .= " AND ( SP.CSSTSP = 'DP' AND TDFN11 = 0 )";
		else {
			if ($field_data == 'TDDTSP' && $tipo != 'search' && $filtro["escludi_filtro_DP"] != 'Y') //di base in flight escludo le DP
				$where .= " AND ( SP.CSSTSP <> 'DP' OR TDFN11 = 1 )";
		}

		
		if (isset($filtro["num_contract"])  && strlen($filtro["num_contract"]) > 0 )
			$where .= " AND TDCOST like '%{$filtro["num_contract"]}%'";
		if (isset($filtro["mercato"]) && strlen($filtro["mercato"]) > 0)
			$where .= " AND TDCAVE='{$filtro["mercato"]}'";
				
	    //gestione multiselect (valori separati da ,) o array
	    if (isset($filtro["ava_paga"]) && is_array($filtro["ava_paga"])){
	        $ar_ava_paga = $filtro["ava_paga"];
	        if (count($ar_ava_paga) == 1)
	            $where .= " AND TDAUX1='{$filtro["ava_paga"][0]}'";
            else if (count($ar_ava_paga) > 1)
                $where .= " AND TDAUX1 IN (" . sql_t_IN($ar_ava_paga) . ") ";
		    }
	    else if (isset($filtro["ava_paga"]) && strlen(trim($filtro["ava_paga"])) > 0){
	        $ar_ava_paga = explode(",", $filtro["ava_paga"]);
	        if (count($ar_ava_paga) == 1)
	            $where .= " AND TDAUX1='{$filtro["ava_paga"]}'";
            else
                $where .= " AND TDAUX1 IN (" . sql_t_IN($ar_ava_paga) . ") ";
	    }
	    
	    
	    //gestione multiselect (valori separati da ,) o array
	    if (isset($filtro["prov_d"]) && is_array($filtro["prov_d"])){
	        $where .= sql_where_by_combo_value("TD.TDPROD", $filtro['prov_d']);
	    }
	    else if (isset($filtro["prov_d"]) && strlen(trim($filtro["prov_d"])) > 0){
	        $ar_prov_d = explode(",", $filtro["prov_d"]);
	        $where .= sql_where_by_combo_value("TD.TDPROD", $ar_prov_d);
	    }
	    
	    //gestione multiselect (valori separati da ,) o array
	    if (isset($filtro["loc_d"]) && is_array($filtro["loc_d"])){
	        $where .= sql_where_by_combo_value("TD.TDDLOC", $filtro['loc_d']);
	    }
	    else if (isset($filtro["loc_d"]) && strlen(trim($filtro["loc_d"])) > 0){
	        $ar_loc_d = explode(",", $filtro["loc_d"]);
	        $where .= sql_where_by_combo_value("TD.TDDLOC", $ar_loc_d);
	    }
	    
	    $ar_forniture_MTO = array();
	    //gestione multiselect (valori separati da ,) o array
	    if (isset($filtro["forniture_MTO"]) && is_array($filtro["forniture_MTO"])){
	        $ar_forniture_MTO = $filtro["forniture_MTO"];
	    }
	    else if (isset($filtro["forniture_MTO"]) && strlen(trim($filtro["forniture_MTO"])) > 0){
	        $ar_forniture_MTO = explode(",", $filtro["forniture_MTO"]);
	    }
	    
	    if (isset($filtro["forniture_MTO"]) && count($ar_forniture_MTO) > 0){
    	    $where_mto = array();
    	    foreach($ar_forniture_MTO as $v){
    	        if($v == 'OE')   //INFO GRIGIA, ORDINI DA EMETTERE
    	            $where_mto[] = "(TDFMTO = 'Y' AND TDMTOO NOT IN ('Y', 'O'))";
                if($v == 'OEA')  //CARRELLO BLU, ORDINE EMESSI/APERTI
                    $where_mto[] = "(TDFMTO = 'Y' AND TDMTOO = 'O')";
                if($v == 'OEE')  //CARRELLO VERDE, ORDINI EMESSI/EVASI
                    $where_mto[] = "(TDFMTO = '' AND TDMTOO IN ('Y', 'O'))";
                if($v == 'SOM')  //NESSUN ICONA, SENZA ORDINI MTO
                    $where_mto[] = "(TDOMTO = 'N')";
	    }
	    
	    $where_forniture_MTO = implode('OR', $where_mto);
	    $where .= " AND ({$where_forniture_MTO}) ";
	    }
		$campi_select = "TD.*, SP.*, PS.*";
		
		if ($ordina_per_tipo_ord == 'Y')
		 $m_ord_f = "TDCLOR, TDOTPD, {$field_data}";
		else {
		 if (isset($filtro["TDSWSP"]) && $filtro["TDSWSP"] == 'N')		 	
		 	$m_ord_f = $td_sped_field; //hold: non ha senso filtrare per data o per spedizione
		 else
		 	$m_ord_f = "{$field_data}, {$td_sped_field}";
		}
		
		if ($separa_hold == 'Y')
		    $m_ord_f = "TDSWSP DESC, {$m_ord_f}";
		
		
		$join_search .= " LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				  				ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI ";
		if ($tipo=='search'){
			$m_ord_f = " TA_ITIN.TAASPE, TA_ITIN.TASITI, " . $m_ord_f; //in search, ordini prima per area sped/seq itinerario
		}
		
		
		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			$join_search .= "			
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO AND TDPROG=RDPROG
				AND RDTPNO='COLLI' /*AND RDRIGA=0*/ AND RDRIFE = 'PRODUZIONE'
                ";				
			$campi_select .= ", RD.*";
			
			if(strlen($filtro['articolo']) > 0){
			    $where .= " AND RDART = '{$filtro['articolo']}'";
			}		
			
		}
		
		if($tipo == 'DAY'){
		    
		    $campi_select .= ", RDQTA AS COLLI_PROD ";
		    $join_cprod = " LEFT OUTER JOIN (
		    SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA
		    FROM {$cfg_mod_Spedizioni['file_righe']} RDO
		    WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE = 'PRODUZIONE'
		    GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
		    ) RD
		    ON TD.TDDT=RD.RDDT AND TD.TDOTID=RD.RDOTID AND TD.TDOINU=RD.RDOINU AND TD.TDOADO=RD.RDOADO AND TD.TDONDO=RD.RDONDO";
		}
		
		
		if ($cfg_info_columns_properties['info_show_vettore'] == 'Y'){
		    $join_search .= "
		    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AUTR
		    ON TD.TDDT = TA_AUTR.TADT AND TD.TDVETT = TA_AUTR.TAKEY1 AND TA_AUTR.TATAID = 'AUTR'
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AVET
		    ON TD.TDDT = TA_AVET.TADT AND TD.TDVET1 = TA_AVET.TAKEY1 AND TA_AVET.TATAID = 'AVET'
		    ";
		   $campi_select .= ", TA_AUTR.TADESC AS VETTORE, TA_AVET.TADESC AS TRASPORTATORE";
		}
		
		if ($cfg_info_columns_properties['info_show_architetto'] == 'Y'){
		    $join_search .= "
		     LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ARCHI
		    ON TD.TDDT = TA_ARCHI.TADT AND TD.TDCAG2 = TA_ARCHI.TAKEY1 AND TA_ARCHI.TATAID = 'ARCHI'
		    ";
		    $campi_select .= ", TA_ARCHI.TADESC AS ARCHITETTO";
		}
		
		$join_art = '';
		if($join_articoli == true){
			
			$join_art.= "
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
						ON TD.TDDT=RD.RDDT AND TD.TDOTID = RD.RDTIDO AND TD.TDOINU = RD.RDINUM AND RD.RDAADO = TD.TDOADO AND TD.TDONDO = RD.RDNRDO
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest_valuta']} RT
					    ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'";
			}
			
			
			global $backend_ERP;
			if ($backend_ERP == 'SV2'){
			    $join_an_cli_gest = "
            	            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
            	               ON CF.CFDT = TD.TDDT AND digits(CF.CFCD) = TD.TDCCON";
			} else {
			    $join_an_cli_gest = "";
			}
			
			//gestione multiselect (valori separati da ,) o array
			if(isset($filtro["stadio"])){
			   
			    $join_cal = " LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
			                 ON TD.TDDT = CAL.CSDT AND TD.TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'";
			    
			    if (is_array($filtro["stadio"])){
			        $ar_stadio = $filtro["stadio"];
			        if (count($ar_stadio) == 1)
			            $where .= " AND CAL.CSFG01 ='{$filtro["stadio"][0]}'";
			            else if (count($ar_stato_ordine) > 1)
			                $where .= " AND CAL.CSFG01 IN (" . sql_t_IN($ar_stadio) . ") ";
			    }
			    else if (strlen(trim($filtro["stadio"])) > 0){
			        $ar_stadio = explode(",", $filtro["stadio"]);
			        if (count($ar_stadio) == 1)
			            $where .= " AND CAL.CSFG01 ='{$filtro["stadio"]}'";
			            else
			                $where .= " AND CAL.CSFG01 IN (" . sql_t_IN($ar_stadio) . ") ";
			    }
			
			}
			
			$join_tpcap = "
			     LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TPCAP
			     ON TD.TDDT = TA_TPCAP.TADT AND TD.TDCARP = TA_TPCAP.TAKEY1 AND TA_TPCAP.TATAID = 'TPCAP'
		
			";
			$campi_select .= ", TA_TPCAP.TAFG01 AS TP_CAP";
					
			$campi_order_by = "{$m_ord_f}, PSFG02, TDAACA, TDNRCA, TDSECA, TDDCON, TDCCON, TDCDES, TDSCOR, TDONDO";
			
			if (!is_null($forza_campi_select))   $campi_select = $forza_campi_select;
			if (!is_null($forza_campi_order_by)) $campi_order_by = $forza_campi_order_by;
			if (!is_null($forza_campi_group_by)) $campi_group_by = " GROUP BY {$forza_campi_group_by} ";
			
			$sql = "SELECT $campi_select		 
				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $this->add_riservatezza() . "
				 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				   		ON SP.CSCALE = '*SPR' AND SP.CSDT=TDDT AND {$td_sped_field} = SP.CSPROG 
				 " . $join_search . "  		
				 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS
				   ON PSTAID = 'PRGS' 
				   AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA	
				  " . $join_art  
				    . $join_an_cli_gest
				    . $join_cal
				    .$join_tpcap
				    .$join_cprod.
				" WHERE (" . $this->get_where_std($tipo) . "  $where) 
                  {$campi_group_by}
				  ORDER BY {$campi_order_by} 
				";
			
                  
                  
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt);
		return $stmt;
	}		
	
	
	
	
	
	

	
	
	
	
	
	

	
	
	
	
	
	
	
	
	public function ajax_flt_ord(){
		$filtro = array();
		$filtro["itinerario"] = $_REQUEST["k2"];
		$filtro["data_spedizione"] = substr($_REQUEST["k3"], 0, 8);
		$filtro["vettore"] = substr($_REQUEST["k3"], 9, 3);
		$ords = $this->get_elenco_ordini($filtro);
		echo $this->print_table_elenco_ordini($ords);
	}
	
	
	public function print_table_elenco_ordini($ords){
		$r = "";
		$r .= "<table class=\"tw tab\"><tbody>
				<tr>
				 <th>Cliente</th>
				 <th>Data</th>
				 <th class=n>Volume</th>
				 <th class=n>Colli</th>				 				 
				</tr>
				";
		
		while ($row = db2_fetch_assoc($ords)) {
			$r .= "<tr class=ev>";
			 $r .= "<td>" . $row["TPRGSC"] . "</td>";
			 $r .= "<td>" . $row["TPDTSPP"] . "</td>";			 
			 $r .= "<td class=n>" . $row["TPVOLO"] . "</td>";			 
			 $r .= "<td class=n>" . $row["TPCOLO"] . "</td>";
			$r .= "</tr>";			
		}		
			
		$r .= "</tbody></table>";		
	 return $r;
	}
	
	
	public function fascetta_function(){
		global $auth, $main_module;
		
		if(isset($_REQUEST['menu_type']))
		  $menu_type = $_REQUEST['menu_type'];
		else
		  $menu_type = 'PRO_MON';
		
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
		$cfg_mod = $main_module->get_cod_mod();
		
		$list_user_function = $auth->list_module_function_permission($main_module->get_cod_mod());
		
		if (!is_null($list_user_function) && !empty($list_user_function))
		    $verify_user_list_function = true;
		else
		    $verify_user_list_function = false;
		        
        //se il profilo ha solo "interrogazione" (disabilita_menu).... forzo la verifica dei permessi
        if ($js_parameters->p_solo_inter == 'Y')
            $verify_user_list_function = true;
		            
		$ar_btn = array();
            
		echo "";
		echo "<div class='fascetta-toolbar'>";
			$ar_btn[] = "<img id='bt-page-refresh' src=" . img_path("icone/48x48/button_grey_repeat.png") . " height=30>";			
	   
	   if ($menu_type == 'PRO_MON')
	   if (isset($list_user_function['SETUP']) || 
	       (!$verify_user_list_function && $js_parameters->p_mod_param == 'Y'))
	       $ar_btn[] = "<img id='bt-toolbar' src=" . img_path("icone/48x48/archive.png") . " height=30>";
	           
       if ($menu_type == 'ORD_VEN')
       if (isset($list_user_function['SETUP']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-toolbar_ov' src=" . img_path("icone/48x48/archive.png") . " height=30>";
	       
       if (isset($list_user_function['TODO']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['PLAN']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-partenze' src=" . img_path("icone/48x48/fabbrica.png") . " height=30>";
       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['FLIGHT']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-flights' src=" . img_path("icone/48x48/partenze.png") . " height=30>";
       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['GRID']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-avanzamento_produzione' src=" . img_path("icone/48x48/bandiera_scacchi.png") . " height=30>";
                       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['BOOKING']) || !$verify_user_list_function) {
           $ar_btn[] = "<div class='div-notifica'>
                            <img id='bt-booking' src=" . img_path("icone/48x48/calendar.png") . " height=30 class=notifica>
                            <span style='visibility: hidden;' class='bt-notifica' id='bt-booking-notifica'></span>
                        </div>";
       }
       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['ALERT']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-report-art-mancanti-grid' src=" . img_path("icone/48x48/warning_black.png") . " height=30>";
           
       if ($menu_type == 'ORD_VEN')
       if (isset($list_user_function['OVERFLOW']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-espo' src=" . img_path("icone/48x48/power_black_blue.png") . " height=30>";
               
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['REPORT']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-print' src=" . img_path("icone/48x48/print.png") . " height=30>";
       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['RATES']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-manutenzione-costi' src=" . img_path("icone/48x48/currency_black_euro.png") . " height=30>";
                       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['UPLOAD']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-prenotazione_carichi' src=" . img_path("icone/48x48/inbox.png") . " height=30>";
           
       if (isset($list_user_function['WEEK']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-agenda_settimanale_spedizioni' src=" . img_path("icone/48x48/wallet.png") . " height=30>";
                   
       if ($menu_type == 'ORD_VEN')
       if (isset($list_user_function['HEADING']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-protocollazione' src=" . img_path("icone/48x48/blog_add.png") . " height=30>";
                   
       //in Turi (es: Roberto) l'utente ha il modulo di default e quindi non vedrebbe questa icona.... 
       if ($menu_type == 'ORD_VEN' || (isset($cfg_mod['abilita_OENTRY_IN_PRO_MON']) && $cfg_mod['abilita_OENTRY_IN_PRO_MON'] == 'Y'))
       if (isset($list_user_function['OENTRY']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-order-entry' src=" . img_path("icone/48x48/design.png") . " height=30>";
       
       if (isset($list_user_function['EXPO']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-expo' src=" . img_path("icone/48x48/monitor.png") . " height=30>";
                               
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['CHARTS']) ||
           (!$verify_user_list_function && $js_parameters->p_acc_stat == 'Y'))
           $ar_btn[] = "<img id='bt-grafici' src=" . img_path("icone/48x48/grafici.png") . " height=30>";
       
       if ($menu_type == 'PRO_MON')
       if (isset($list_user_function['MEMO']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='add-promemoria' src=" . img_path("icone/48x48/clock.png") . " height=30>";
       
      // if ($menu_type == 'PRO_MON')
       if ($menu_type == 'ORD_VEN')
       if (isset($list_user_function['OPM']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-marketing' src=" . img_path("icone/48x48/gift.png") . " height=30>";
       
       if ($menu_type == 'ORD_VEN')
       if (isset($list_user_function['OVM']) || !$verify_user_list_function)
           $ar_btn[] = "<img id='bt-gift' src=" . img_path("icone/48x48/shopping_cart.png") . " height=30>";
       
       if (isset($list_user_function['PLAY']) || !$verify_user_list_function)
            $ar_btn[] = "<img id='bt-call-pgm' src=" . img_path("icone/48x48/button_blue_play.png") . " height=30>";
           
       echo implode('', $ar_btn);           
           
       //orario aggiornamento dati
       echo "<div id='stato-aggiornamento'>[<img id='bt-stato-aggiornamento' class='st-aggiornamento' src=" . img_path("icone/48x48/rss_blue.png") . " height=20><span id='bt-stato-aggiornamento-esito'></span>]</div>";
			
	  echo "</div>";		

	}
	
	
	
	public function imposta_field_dett(){		
	 	if (!isSet($_SESSION[$this->get_cod_mod]['calendar_field'])){
	 		
	 		$cfg_mod = $this->get_cfg_mod();
	 		if (isset($cfg_mod['default_calendar_field']))
	 			$_SESSION[$this->get_cod_mod]['calendar_field'] = $cfg_mod['default_calendar_field'];
	 		else
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "VOL";
		}				
		return $_SESSION[$this->get_cod_mod]['calendar_field'];
	}
	
	
	
	public function set_calendar_field_dett($set_field) {
		switch ($set_field){
			case "tot_VOL":	
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "VOL";
				break;
			case "tot_COL":		
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "COL";
				break;							
			case "tot_ORD":		
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "ORD";
				break;							
			case "tot_PAL":		
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "PAL";
				break;
			case "tot_CP":
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "CP";
				break;																										
		}		
	}	
	

	
	
	public function get_tot_field_by_field_dett($field_dett) {
		switch ($field_dett){
			case "VOL": return "T_TDVOLU";
			case "COL": return "T_TDTOCO";
			case "ORD": return "AAAAA";
			case "PAL": return "T_TDBANC";
			case "IMP": return "T_TDTIMP";
			case "CP":  return "BBBBB";
		}
	}
	
	
	
  public function get_initial_calendar_date(){

  	if (!isSet($_SESSION[$this->get_cod_mod]['calendar_inizial_date'])){
		$_SESSION[$this->get_cod_mod]['calendar_inizial_date'] = date('Y-m-d');				
	} 
	return $_SESSION[$this->get_cod_mod]['calendar_inizial_date'];
  }
  
 
 public function set_initial_calendar_date($move) {
 	
	$da_data_txt = $this->get_initial_calendar_date();
	 
	switch ($_REQUEST['move']){
		case "next_1d":
			$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " +1 days"));		
		break;
		case "next_1w":
			$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " +7 days"));		
		break;		
		case "prev_1d":
			$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " -1 days"));			
		break;
		case "prev_1w":
			$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " -7 days"));		
		break;		
	}  
	$_SESSION[$this->get_cod_mod]['calendar_inizial_date'] = $da_data_txt;
 } 
  

 
  public function toggle_fl_solo_con_carico_assegnato(){
  	if (!isSet($_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'])){
		$_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] = 0;				
	} 
	
	$_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] = ($_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] == 0) ? 1 : 0;	
  }
  public function get_fl_solo_con_carico_assegnato(){
  	if (!isSet($_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'])){
		$_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] = 0;				
	}  	
  	return $_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'];
  }

  
  
  public function get_plan_filtro_ordini(){
  	if (!isSet($_SESSION[$this->get_cod_mod]['plan_filtro_ordini'])){
  		$_SESSION[$this->get_cod_mod]['plan_filtro_ordini'] = array();
  	}
  	return $_SESSION[$this->get_cod_mod]['plan_filtro_ordini'];
  }  
  public function set_plan_filtro_ordini($filtri){
  	if (!isSet($_SESSION[$this->get_cod_mod]['plan_filtro_ordini'])){
  		$_SESSION[$this->get_cod_mod]['plan_filtro_ordini'] = array();
  	}
  	$_SESSION[$this->get_cod_mod]['plan_filtro_ordini'] = $filtri;
  }
  public function ha_plan_filtro_ordini(){
  	$filtri_plan = $this->get_plan_filtro_ordini();
  	foreach ($filtri_plan as $kf => $f){
  		if (strlen(trim($f)) > 0) return 1; //ho applicato un filtro
  	}
  	return 0; //non ho applicato filtri
  }    
  
  
  
  
  	
	
  public function get_ordine_by_num($num){
  		global $conn;
		global $cfg_mod_Spedizioni;  		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_testate']}
				WHERE TDONDO = $num";	
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		return db2_fetch_assoc($stmt);
  }	
	
  
  
  public function get_ordine_gest_by_k_docu($k_docu){
      global $conn, $cfg_mod_Spedizioni;
      $oe = $this->k_ordine_td_decode_xx($k_docu);
      $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD0
                WHERE TDDT = ? AND TDTIDO = ? AND TDINUM = ? AND TDAADO = ? AND TDNRDO = ?";      
      
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt, $oe);
      return db2_fetch_assoc($stmt);
  }
  
  public function get_ordine_by_k_docu($k_docu){
  	global $conn;
  	global $cfg_mod_Spedizioni;

  	//non piu' usato il ? per anomalia in protocollazione ALA
  	$sql = "SELECT *
  			FROM {$cfg_mod_Spedizioni['file_testate']}
  			WHERE TDDOCU = '{$k_docu}' AND " . $this->get_where_std();


  			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt);
  			return db2_fetch_assoc($stmt);
  }
    
  
  public function get_ordine_by_k_ordine($k_ord){
  		global $conn, $cfg_mod_Spedizioni;  		
		
		$oe = $this->k_ordine_td_decode($k_ord);
		
		$oe['TDONDO'] = sprintf("%06s", $oe['TDONDO']);
		
		//TODO: $k_ord e' tddocu perche' non filtro direttamente sul campo TDDOCU? 
		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_testate']}
				WHERE TDDT=" . sql_t($oe['TDDT']) . " AND TDOTID=" . sql_t($oe['TDOTID'])  . " 
				  AND TDOINU=" . sql_t($oe['TDOINU']) . " AND TDOADO=" . sql_t($oe['TDOADO'])  . " AND TDONDO=" . sql_t($oe['TDONDO'])  . " ";

		
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		return db2_fetch_assoc($stmt);
  }	

      
  public function get_cronologia_ordine($k_ord){
  		global $conn, $cfg_mod_Spedizioni, $cfg_mod_Admin;  	
		
  		$oe = $this->k_ordine_td_decode_xx($k_ord);
  		$sql = "SELECT *
  				FROM {$cfg_mod_Spedizioni['file_cronologia_ordine']}";
  		
  		global $backend_ERP;  		
  		if ($backend_ERP != 'GL')
  			$sql .= "
				LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']}
				  ON PQDT = TADT AND TAID = 'QUPR' AND PQFUNZ = TANR
			";
  		
  		if ($cfg_mod_Spedizioni["gestione_per_riga"] == 'Y'){
  			$exp_ord = explode("_", $k_ord);
  			$add_flt_riga = " AND PQNRRG= ?";
  		} else {
  			$add_flt_riga = "";
  		}
  		
  		
  		
		$sql .= "		  
  				WHERE PQDT= ? AND PQTIDO = ?
  				AND PQINUM = ? AND PQAADO = ? AND PQNRDO = ? {$add_flt_riga}
  				ORDER BY PQDTGE, PQHMGE ";

  				$stmt = db2_prepare($conn, $sql);
  				echo db2_stmt_errormsg();
  				$result = db2_execute($stmt, $oe);
  				//$result =acs_execute($stmt, array());
  				if (!$result){
  					echo "bbbbbbbbb";
  					echo db2_stmt_error($stmt);
  					echo db2_stmt_errormsg($stmt);
  				}	  	
  	
  		return $stmt;			
  }
  
  public function has_commento_ordine_by_k_ordine_gest($k_ord, $bl = null, $tpno = null){
      global $conn,$id_ditta_default;
      global $cfg_mod_DeskAcq;
      
      global $backend_ERP;
      if ($backend_ERP == 'GL') return false;
      
      $where = "";
      if (is_null($k_ord) || strlen(trim($k_ord)) == 0){
          return false;
      }else{
          $row_ord  = $this->get_ordine_by_k_ordine($k_ord);
          $k_ord = trim($row_ord['TDOTID']).trim($row_ord['TDOINU']).trim($row_ord['TDOADO']).trim($row_ord['TDONDO']);
      } 
          
      if(isset($bl) && $bl != '')
          $where .= " AND RLRIFE2 = '{$bl}'";
      else
          $where .= "";
                  
                  
      if(isset($tpno) && $tpno != '')
          $where .= " AND RLTPNO = '{$tpno}'";
      else
          $where .= " AND RLTPNO = 'TD'";
                          
                          
      $sql = "SELECT count(*) AS NR
      FROM {$cfg_mod_DeskAcq['file_note_anag']}
      WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$k_ord}'
      {$where}";
      
      $stmt = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      $result = db2_execute($stmt);
      echo db2_stmt_errormsg($stmt);
      $row = db2_fetch_assoc($stmt);
      
      if ($row['NR'] > 0) return TRUE;
      else return FALSE;
                          
  }
  
  
  
  public function has_commento_ordine_by_k_ordine($k_ord, $bl = null){
  		global $conn;
		global $cfg_mod_Spedizioni;
		
		if (is_null($bl))
			$bl = $cfg_mod_Spedizioni['bl_commenti_ordine'];

		
		$oe = $this->k_ordine_td_decode($k_ord);
		$sql = "SELECT count(*) AS NR 
				FROM {$cfg_mod_Spedizioni['file_righe']}
				WHERE RDDT=" . sql_t($oe['TDDT']) . " AND RDOTID=" . sql_t($oe['TDOTID'])  . " 
				  AND RDOINU=" . sql_t($oe['TDOINU']) . " AND RDOADO=" . sql_t($oe['TDOADO'])  . " AND RDONDO=" . sql_t($oe['TDONDO'])  . "
				  AND RDTPNO=" . sql_t($bl) . "	  
			";
				  	
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		$row = db2_fetch_assoc($stmt);
		
		if ($row['NR'] > 0) return TRUE;
		else return FALSE;	
  }  
  
  public function get_commento_ordine_by_k_ordine_gest($k_ord, $bl, $tpno = null, $riga = null){
      global $conn, $id_ditta_default;
      global $cfg_mod_DeskAcq;
      
      if (is_null($k_ord) || strlen(trim($k_ord)) == 0){
          return array();
      }else{
          $row_ord  = $this->get_ordine_by_k_ordine($k_ord);
          $k_ord = trim($row_ord['TDOTID']).trim($row_ord['TDOINU']).trim($row_ord['TDOADO']).trim($row_ord['TDONDO']);
      }   
      
      if(isset($tpno) && $tpno != '')
          $where = " AND RLTPNO = '{$tpno}'";
      else
          $where = " AND RLTPNO = 'TD'";
                  
      if(isset($riga) && strlen($riga) > 0)
          $where .= " AND RLRIGA = {$riga}";
                  
      $sql = "SELECT RRN(RL) AS RRN, RL.*
              FROM {$cfg_mod_DeskAcq['file_note_anag']} RL
              WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$k_ord}'
              AND RLRIFE2 = '{$bl}' {$where}
              ORDER BY RLRIGA";
                      
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt);
      
      $ret = array('testi' =>array());
      while ($row = db2_fetch_assoc($stmt)) {
          $ret['testi'][] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
          
          
          if ((int)$ret['data_user']['min_data_ge'] > 0){
              if ($row['RLDTGE'] < $ret['min_data_ge']){
                  $ret['data_user']['min_data_ge'] = $row['RLDTGE'];
                  $ret['data_user']['min_user_ge'] = trim($row['RLUSGE']);
              }
          }
          else {
              $ret['data_user']['min_data_ge'] = $row['RLDTGE'];
              $ret['data_user']['min_user_ge'] = trim($row['RLUSGE']);
          }
          
          if ((int)$ret['data_user']['max_data_um'] > 0){
              if ($row['RLDTUM'] > $ret['data_user']['max_data_um']){
                  $ret['data_user']['max_data_um'] = $row['RLDTUM'];
                  $ret['data_user']['max_user_um'] = trim($row['RLUSUM']);
              }
          }
          else {
              $ret['data_user']['max_data_um'] = $row['RLDTUM'];
              $ret['data_user']['max_user_um'] = trim($row['RLUSUM']);
          }
      }
      
      return $ret;
  }
  
  public function get_riga_commento_ordine_by_k_ordine_gest($k_ord, $bl, $tpno = null){
      global $conn;
      global $cfg_mod_DeskAcq, $id_ditta_default;
      
      if (is_null($k_ord) || strlen(trim($k_ord)) == 0){
          return array();
      }else{
          $row_ord  = $this->get_ordine_by_k_ordine($k_ord);
          $k_ord = trim($row_ord['TDOTID']).trim($row_ord['TDOINU']).trim($row_ord['TDOADO']).trim($row_ord['TDONDO']);
      }
      
      if(isset($tpno) && $tpno != '')
          $where = " AND RLTPNO = '{$tpno}'";
      else
          $where = " AND RLTPNO = 'TD'";
                  
      $sql = "SELECT RRN(RL) AS RRN, RL.*
              FROM {$cfg_mod_DeskAcq['file_note_anag']} RL
              WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$k_ord}'
              AND RLRIFE2 = '{$bl}' {$where}
              ORDER BY RLRIGA";
                  
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt);
      
      $ret = array();
      while ($row = db2_fetch_assoc($stmt)) {
          $nr = array();
          $nr['text'] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
          
          $nr['rrn'] = trim($row['RRN']);
          
          $ret[$row['RLRIGA']] = $nr;
      }
      
      return $ret;
  }
  
  
  public function get_commento_ordine_by_k_ordine($k_ord, $bl = null, $solo_text = false){
  		global $conn;
		global $cfg_mod_Spedizioni;
		
		if (is_null($bl))
			$bl = $cfg_mod_Spedizioni['bl_commenti_ordine'];
		
		$oe = $this->k_ordine_td_decode($k_ord);
		
		$sql = "SELECT RRN(RD) AS RRN, RD.*
				FROM {$cfg_mod_Spedizioni['file_righe']} RD
				WHERE RDDT=" . sql_t($oe['TDDT']) . " AND RDOTID=" . sql_t($oe['TDOTID'])  . " 
				  AND RDOINU=" . sql_t($oe['TDOINU']) . " AND RDOADO=" . sql_t($oe['TDOADO'])  . " AND RDONDO=" . sql_t($oe['TDONDO'])  . "
				  AND RDTPNO=" . sql_t($bl);

		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			$sql .= " AND (RDPROG=" . sql_t($oe['TDPROG']) . " OR RDPROG=' ')"; 
		}		
		
		$sql .= "ORDER BY RDRINO";
			  	
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
			//$ret[] = $row['RDDES1'];
			$nr = array();
			$nr['text'] = $row['RDDES1'];
			$nr['rrn'] = trim($row['RRN']);
			$nr['user_ge'] = trim($row['RDUSGE']);
			$nr['data_ge'] = trim($row['RDDTGE']);
			
			$nr['user_um'] = trim($row['RDRIFE']);
			$nr['data_um'] = trim($row['RDDTEP']);
			
			$ret[$row['RDRINO']] = $nr;
			
		}
		
		//ritorno il solo array con il campo nota
		if ($solo_text == true){
		    $ar_simple = array();
		    foreach ($ret as $c => $t){		        
		        $ar_simple = $t['text'];
		        return $ar_simple;
		    }
		}
		
		//altrimenti ritorno struttura completa delle varie note
		return $ret;	
  }
  
  public function get_commento_by_cli($cliente, $bl = null){
      global $conn,$id_ditta_default;
      global $cfg_mod_Spedizioni, $cfg_mod_DeskPVen;
      
      if (is_null($bl))
          $bl = $cfg_mod_DeskPVen['pv_commenti_delivery'];

          
          $sql = "SELECT *
          FROM {$cfg_mod_Spedizioni['file_righe']} RD
          LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
          ON RD.RDDT = TD.TDDT AND RD.RDOTID = TD.TDOTID AND RD.RDOINU = TD.TDOINU AND RD.RDOADO = TD.TDOADO AND RD.RDONDO = TD.TDONDO
          WHERE RDDT= '{$id_ditta_default}' AND TDCCON = '{$cliente}' AND RDTPNO=" . sql_t($bl) . "";
           
          $sql .= "ORDER BY RDRINO";
          
          $stmt = db2_prepare($conn, $sql);
          $result = db2_execute($stmt);
          
          $ret = array();
          while ($row = db2_fetch_assoc($stmt)) {
              $ret[] = $row['RDDES1'];
          }
          
          return $ret;
  }


  public function exe_upd_commento_ordine($p){
      global $cfg_mod_Spedizioni, $auth, $conn, $id_ditta_default, $is_linux;
		
		$oe = $this->k_ordine_td_decode($p['k_ordine']);
		$bl = $cfg_mod_Spedizioni['bl_commenti_ordine'];
		
		foreach($p as $k => $v){
		    if (substr($k, 0, 7) == 'f_text_'){
		        $ar_value = explode('_', $k);
		        $riga = $ar_value[2];
		        $rrn = $ar_value[3];
		        
		        $name = "f_h_text_{$riga}";
		        $old_value = trim($p["{$name}"]);
		        if ($is_linux == 'Y')
		            $old_value = strtr($old_value, array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	            if ($is_linux == 'Y')
	                $v = strtr($v, array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
		        
		        $ar_ins = array();
		        
	            $ar_ins['RDDES1'] 	= $v;
	            $ar_ins['RDDTEP'] 	= oggi_AS_date();     //data ultima modifica
	            $ar_ins['RDRIFE'] 	= $auth->get_user();  //utente ultima modifica
	            
            if($rrn != ""){
                
                if($old_value != trim($v)){
                    $sql = "UPDATE {$cfg_mod_Spedizioni['file_righe']} RD
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE RRN(RD) = '{$rrn}'";
                    
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt, $ar_ins);
                    echo db2_stmt_errormsg($stmt);
                    
                }
                
            }else{
		                
                if(trim($v) != ""){
                    
                    $ar_ins['RDDTGE'] 	= oggi_AS_date();
                    $ar_ins['RDORGE'] 	= oggi_AS_time();
                    $ar_ins['RDUSGE'] 	= SV2_db_user($auth->get_user());
                    $ar_ins['RDDT'] 	= $id_ditta_default;
                    $ar_ins['RDOTID'] 	= trim($oe['TDOTID']);
                    $ar_ins['RDOINU'] 	= trim($oe['TDOINU']);
                    $ar_ins['RDOADO'] 	= trim($oe['TDOADO']);
                    $ar_ins['RDONDO'] 	= trim($oe['TDONDO']);
                    $ar_ins['RDTPNO'] 	= $bl;
                    $ar_ins['RDRINO'] 	= $riga;
                    
                    $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_righe']}(" . create_name_field_by_ar($ar_ins) . ")
	                    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                    
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt, $ar_ins);
                    echo db2_stmt_errormsg();
                    
                    
                }
	          }
		            
		    }
		    
		}
		  	
		
		$sh = new SpedHistory();
		$sh->crea(
				'upd_commento_ordine',
				array(
						"k_ordine" 	=> $p['k_ordine']
				)
		);		
		
  }



	
 function get_giorni_festivi($da_data, $n_giorni){
 		global $conn;
		global $cfg_mod_Spedizioni;
		global $global_tmp_giorni_festivi;
		global $id_ditta_default;
		
		if (isset($global_tmp_giorni_festivi))
			return $global_tmp_giorni_festivi;
		
		$ar = array();  		
		$sql = "SELECT CSDTRG, CSTPGG  
				FROM {$cfg_mod_Spedizioni['file_calendario']}
				WHERE CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
				AND CSPROG=0 AND CSDT = '$id_ditta_default'
				ORDER BY CSDTRG
				";		
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
 	
		while ($row = db2_fetch_assoc($stmt)) {
			$d = $row["CSDTRG"];
			$ar["{$d}"] = $row["CSTPGG"];
		}
		
		$global_tmp_giorni_festivi = $ar;		
	return $ar;			
 }

 function get_giorni_stadio($da_data, $n_giorni){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	global $id_ditta_default;
 	$ar = array();
 	$sql = "SELECT CSDTRG, CSFG01 
 			FROM {$cfg_mod_Spedizioni['file_calendario']}
 			WHERE CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
 			AND CSPROG=0 AND CSDT='$id_ditta_default'
 			";
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt);
 
 			while ($row = db2_fetch_assoc($stmt)) {
 			$d = $row["CSDTRG"];
			$ar["{$d}"] = $row["CSFG01"];
		}
 		return $ar;
 } 
	
	
 function get_disponibilita_itinerari($field_data, $da_data, $n_giorni, $solo_con_ordini = false){
 		global $conn;
		global $cfg_mod_Spedizioni;
		$ar = array();  

		 
		if ($field_data == 'TDDTEP'){
			$field_data = 'CSDTSP';
		
			$sql = "SELECT {$field_data}, CSCITI, SUM(CSVOLD) AS SUM_CSVOLD, SUM(CSBAND) AS SUM_CSBAND 
					FROM {$cfg_mod_Spedizioni['file_calendario']}
					WHERE 1 = 1 ";
		}

		//TODO: per recuperare le spedizioni da considerare per data  
		// legarle agli ordini raggruppando per TDDTSP, TDCITI XXXX
		if ($field_data == 'TDDTSP'){
			$field_data = 'TDDTSP';
			$sql = "SELECT {$field_data}, CSCITI, SUM(CSVOLD) AS SUM_CSVOLD, SUM(CSBAND) AS SUM_CSBAND
					FROM (
						SELECT CSPROG, CSCALE, max({$field_data}) as {$field_data}, CSCITI, CSVOLD, CSBAND
							FROM {$cfg_mod_Spedizioni['file_calendario']} SP
							INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
							  ON TDNBOC = CSPROG AND " . $this->get_where_std() . "
							WHERE SP.CSSTSP <> 'DP'
							GROUP BY CSPROG, CSCALE, CSCITI, CSVOLD, CSBAND
						) AS AA	  
					WHERE 1 = 1 ";			
		}
				
		if ($solo_con_ordini){
			$sql .= " AND CSPROG IN (SELECT DISTINCT TDNBOC FROM {$cfg_mod_Spedizioni['file_testate']}) ";
		}				
				
		$sql .= " AND CSCALE = '*SPR'
				  GROUP BY {$field_data}, CSCITI
				";	
				
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt);	
 	
		while ($row = db2_fetch_assoc($stmt)) {
			$d = $row[$field_data];
			$i = trim($row["CSCITI"]);			
			if (!isset($ar["{$d}"]))
				$ar["{$d}"] = array();
			if (!isset($ar["{$d}"]["{$i}"]))
				$ar["{$d}"]["{$i}"] = array(
					"VOL__" => number_format($row['SUM_CSVOLD'],0,',','.'),
					"PAL__" => number_format($row['SUM_CSBAND'],1,',','.'),
						"VOL" => $row['SUM_CSVOLD'],
						"PAL" => $row['SUM_CSBAND']											
				);			
		}
		
	return $ar;			
 }	
	

	
 
 
 
 
	
 function get_spedizioni_carichi($itin = null, $da_oggi = null, $escludi_ordini = null){
 		global $conn;
		global $cfg_mod_Spedizioni;
		$ar = array();
		
		$g_field = "CSPROG, CSCVET, CSCAUT, CSCCON, CSDESC, CSDTSP, CSVOLD, CSBAND, CSPESO, TDTPCA, TDAACA, TDNRCA ";
		$s_field = " SUM(TDVOLU) AS ASS_VOL, SUM(TDPLOR) AS ASS_PES, SUM(TDBANC) AS ASS_PAL ";
		$o_field = " CSDTSP, CSCVET, CSCAUT, CSCCON, TDTPCA, TDAACA, TDNRCA ";
		  		
		$sql = "SELECT $g_field , $s_field 
				FROM {$cfg_mod_Spedizioni['file_calendario']} 
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} ON TDNBOC = CSPROG AND " . $this->get_where_std() . "  
				WHERE CSCALE = '*SPR'
				";		
				
		if ($da_oggi == "Y")
			$sql .= " AND CSDTSP >= " . oggi_AS_date();				
				
		if (!is_null($itin))
			$sql .= " AND CSCITI = " . sql_t($itin);
		
		$sql .= " GROUP BY $g_field";
		$sql .= " ORDER BY $o_field";		
		
		$stmt = db2_prepare($conn, $sql);	
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);	
		return $stmt;	
 }	


 function get_spedizioni_per_assegnazione($itin = null, $da_oggi = null, $ordini_da_escludere = null, $scadute_da_data = null, $solo_data = null){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 
 	$g_field = "CSPROG, CSCVET, CSCAUT, CSCCON, CSDESC, CSDTSP, CSDTPG, CSVOLD, CSBAND, CSPESO, CSSTSP, CSTARG, CSNSPC ";
 	$s_field = " SUM(TDVOLU) AS ASS_VOL, SUM(TDPLOR) AS ASS_PES, SUM(TDBANC) AS ASS_PAL, MAX(TDONDO) AS MAX_TDONDO, MAX(TDDTSP) AS MAX_TDDTSP ";
 	$o_field = " CSDTSP, CSCVET, CSCAUT, CSCCON ";
 	
 	$sql = "SELECT $g_field , $s_field
 			FROM {$cfg_mod_Spedizioni['file_calendario']}
 			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} ON TDNBOC = CSPROG AND " . $this->get_where_std() . "
 			WHERE CSCALE = '*SPR'";
 	


 	$sql .= " AND CSFG04<>'F' ";

 	if(strlen($solo_data) > 0){
 	    $sql .= " AND CSDTSP = {$solo_data}";
 	}else{
 	    if ($da_oggi == "Y")
 	        $sql .= " AND CSDTSP >= " . oggi_AS_date();
 	        
 	        if ($da_oggi == "SC"){ //solo scadute
 	            $sql .= " AND CSDTSP < " . oggi_AS_date();
            if ($scadute_da_data != null){
                $sql .= " AND CSDTSP >= " . $scadute_da_data;
            }
        }
 	    
 	    
 	}



		
 
 		if (!is_null($itin))
 			$sql .= " AND CSCITI = " . sql_t($itin);

 		//TODO: DOVRO' USARE LA CHIAVE DELL'ORDINE
 		if (!is_null($ordini_da_escludere) && count($ordini_da_escludere) > 0)
 			$sql .= " AND (TDDOCU IS NULL OR TDDOCU NOT IN (" . sql_t_IN($ordini_da_escludere) . ") )" ; 		

		$sql .= " GROUP BY $g_field";
		
		$sql .= " HAVING COUNT(TDONDO) > 0 OR CSSTSP NOT IN('GA', 'AN') ";
 	$sql .= " ORDER BY $o_field";

  	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	return $stmt;
 }
 
 
 
	
 function get_spedizioni($itin = null){
 		global $conn;
		global $cfg_mod_Spedizioni;
		global $id_ditta_default;
		$ar = array();  		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_calendario']}
				WHERE CSCALE = '*SPR'
				AND CSDT = '$id_ditta_default'
				";		
				
		if (!is_null($itin))
			$sql .= " AND CSCITI = " . sql_t($itin);
			
		$sql .= " ORDER BY CSCITI, CSDTSP, CSHMPG";
				
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
		return $stmt;	
 }	
		
	
		
	
 function get_spedizione($id_spedizione){
 		global $conn;
		global $cfg_mod_Spedizioni;
		global $id_ditta_default;
		$ar = array();  		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_calendario']}
				WHERE CSCALE = '*SPR'
				AND CSPROG = '$id_spedizione'
				AND CSDT = '$id_ditta_default'
				";		
				
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
 	
		$row = db2_fetch_assoc($stmt);
		return $row;	
 }	
		

 function get_carico($tp, $aa, $nr, $dt = null){
	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
	if (is_null($dt)) $dt = $id_ditta_default;  		
	$sql = "SELECT * 
			FROM {$cfg_mod_Spedizioni['file_carichi']}
			WHERE PSTAID = 'PRGS'
			AND PSTPCA = '$tp' AND PSAACA = $aa AND PSNRCA = $nr AND PSDT = '$dt'
			";	
	
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);	

	$row = db2_fetch_assoc($stmt);
	return $row;	
 }
 
 
 function get_carico_td($row){
 	$c = $this->get_carico($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA'], $row['TDDT']);
	return $c;
 }
		


 function decod_mc_by_sped_row($row, $separatore1 = " "){
 	$ret = $this->decod_std('AUTO', trim($row['CSCAUT']))
 	. $separatore1 . $this->decod_std('COSC', trim($row['CSCCON']));
 	return $ret;
 }
 
 function decod_vmc_by_sped_row($row, $separatore1 = " ", $separatore2 = " "){
 	$ret = $this->decod_std('AUTR', trim($row['CSCVET'])) 
 			. $separatore1 . $this->decod_std('AUTO', trim($row['CSCAUT'])) 
 			. $separatore2 . $this->decod_std('COSC', trim($row['CSCCON']));		
	return $ret;		
 }

 function decod_trasportatore_by_sped_row($row){
 	$t = new Trasportatori;
 	
 	$t->load_rec_data_by_vettore(trim($row['CSCVET']));

 	return $t->rec_data['TADESC'];
 } 
 

 function get_volume_evaso_by_sped($sped_id){
  global $conn, $cfg_mod_Spedizioni;
  $sql = "
  	SELECT SUM(TDVOLU) AS S_VOL_EVASO FROM {$cfg_mod_Spedizioni['file_testate']}
  		WHERE " . $this->get_where_std() . "
  		  AND TDFN11 = 1 /* SOLO ORDINI EVASI*/
  		  AND TDNBOC = {$sped_id}
  	 ";
  	 
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);	
		
	$row = db2_fetch_assoc($stmt);
	
	if (is_null($row['S_VOL_EVASO'])) $row['S_VOL_EVASO'] = 0;
	return $row['S_VOL_EVASO'];  	 
 }


 function get_volume_by_sped($sped_id){
 	global $conn, $cfg_mod_Spedizioni;
 	$sql = "
 	SELECT SUM(TDVOLU) AS S_VOL_EVASO FROM {$cfg_mod_Spedizioni['file_testate']}
 	WHERE " . $this->get_where_std() . "
 	/* AND TDFN11 = 1 ANCHE CON GLI ORDINI NON EVASI */
 	AND TDNBOC = {$sped_id}
 	";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 
 	$row = db2_fetch_assoc($stmt);
 	return $row['S_VOL_EVASO'];
 } 
 
 
 
 function get_first_TDCCON_by_sped($sped_id){
 	global $conn, $cfg_mod_Spedizioni;
 	$sql = "SELECT MIN(TDCCON) AS TDCCON FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . "	AND TDNBOC = {$sped_id}";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt); 
 	$row = db2_fetch_assoc($stmt);
 	return $row['TDCCON'];
 }
 
 function get_first_TDCCON_by_TDNBOF($sped_id){
     global $conn, $cfg_mod_Spedizioni;
     $sql = "SELECT MIN(TDCCON) AS TDCCON FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . "	AND TDNBOF = {$sped_id}";
     
     $stmt = db2_prepare($conn, $sql);
     $result = db2_execute($stmt);
     $row = db2_fetch_assoc($stmt);
     return $row['TDCCON'];
 }
 

 function get_totale_by_sped($sped_id, $field_return, $solo_evasi = 'N'){
 	global $conn, $cfg_mod_Spedizioni;
 	$sql = "SELECT
 				 SUM(TDVOLU) AS S_VOLUME, SUM(TDTOCO) AS S_COLLI, SUM(TDCOSP) AS S_COLLI_SPEDITI, SUM(TDCOPR) AS S_COLLI_PRODOTTI, SUM(TDTIMP) AS S_IMPORTO				 
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() ;
 	if ($solo_evasi == 'Y')
 		$sql .= " AND TDFN11 = 1 ";
 	
 	$sql .= " AND TDNBOC = {$sped_id}";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 
 	$row = db2_fetch_assoc($stmt);
 	return $row[$field_return];
 }
 
 function get_totale_by_TDNBOF($sped_id, $field_return, $solo_evasi = 'N'){
     global $conn, $cfg_mod_Spedizioni;
     $sql = "SELECT
 				 SUM(TDVOLU) AS S_VOLUME, SUM(TDTOCO) AS S_COLLI, SUM(TDCOSP) AS S_COLLI_SPEDITI, SUM(TDCOPR) AS S_COLLI_PRODOTTI, SUM(TDTIMP) AS S_IMPORTO
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() ;
     if ($solo_evasi == 'Y')
         $sql .= " AND TDFN11 = 1 ";
         
         $sql .= " AND TDNBOF = {$sped_id}";
         
         $stmt = db2_prepare($conn, $sql);
         $result = db2_execute($stmt);
         
         $row = db2_fetch_assoc($stmt);
         return $row[$field_return];
 }
 
 
 function get_totale_by_data($field_data, $data, $field_return, $solo_evasi = 'N', $escludi_sped_id = 0){
 	global $conn, $cfg_mod_Spedizioni;
 	
 	if ($field_data == 'TDDTSP'){
 	    $td_sped_field = 'TDNBOF';
 	} else {
 	    $td_sped_field = 'TDNBOC';
 	}
 	
 	$sql = "SELECT
 				SUM(TDVOLU) AS S_VOLUME, SUM(TDTOCO) AS S_COLLI, SUM(TDCOSP) AS S_COLLI_SPEDITI, SUM(TDTIMP) AS S_IMPORTO
 				FROM {$cfg_mod_Spedizioni['file_testate']} TD
 				 INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
					    ON TDDT = SP.CSDT AND {$td_sped_field} = SP.CSPROG AND SP.CSCALE = '*SPR' 	
 				WHERE " . $this->get_where_std() . " AND TDSWSP='Y' AND {$field_data} = {$data}";
 	
 	if ($field_data == 'TDDTSP') //per spedizioni, non considero quelle da programmare
 		$sql .= " AND ( SP.CSSTSP <> 'DP' OR TDFN11 = 1 ) "; 	
 	
  	if ($solo_evasi == 'Y') $sql .= " AND TDFN11 = 1 ";
  	if ($escludi_sped_id > 0) $sql .= " AND {$td_sped_field} <> {$escludi_sped_id}";
  	
  	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt);
 
  	$row = db2_fetch_assoc($stmt);
  	
  	return $row[$field_return];
 }
 

 function get_nr_scarichi_by_sped($sped_id, $deposito = null, $escludi_scarichi_da_sped_id = null){
  global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
  
  if (is_null($deposito))
      $distinct_f = "CONCAT(TDIDES, CONCAT(TDDLOC, CONCAT(TDDCAP, TDNAZD)))";
  else 
      $distinct_f = "CONCAT(TDINDI, CONCAT(TDLOCA, CONCAT(TDCAP, TDNAZI)))";  	
    	
  $sql = "SELECT COUNT(DISTINCT({$distinct_f})) AS C_CLIENTI_DEST";
  	
  $sql .= " 
		FROM {$cfg_mod_Spedizioni['file_testate']} 	
		WHERE " . $this->get_where_std() . " AND TDNBOC = " . $sped_id . " 
  	 ";
  
  	if (!is_null($deposito)){
  		$sql .= " AND TDTDES IN ('1', '2') AND (CASE WHEN TDTDES = '1' THEN TDVET1 WHEN TDTDES = '2' THEN TDVET2 END = '{$deposito['TRASPORTATORE']}')";
  	}
  	
  	
  	if (!is_null($escludi_scarichi_da_sped_id)){
  	    if (!is_null($deposito))
  	        $sql_dep = " AND TD2.TDTDES IN ('1', '2') AND (CASE WHEN TD2.TDTDES = '1' THEN TD2.TDVET1 WHEN TD2.TDTDES = '2' THEN TD2.TDVET2 END = '{$deposito['TRASPORTATORE']}')";
  	    else
  	        $sql_dep = '';
  	    
  	    $sql .= " AND {$distinct_f} NOT IN (
  	             SELECT {$distinct_f} FROM {$cfg_mod_Spedizioni['file_testate']} TD2
                    WHERE TD2.TDDT = '{$id_ditta_default}' AND TD2.TDNBOC={$escludi_scarichi_da_sped_id}
                    {$sql_dep}
                )";

  	}
  	
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);	
		
	$row = db2_fetch_assoc($stmt);
	return $row['C_CLIENTI_DEST'];  	 
 }





 
 

 function get_el_carichi_by_sped($sped_id, $per_esteso = 'N', $crea_default_0 = 'N', $add_area_spedizione = 'N', $stabilimento=null, $mostra_img_pagamento = 'N', $return_array='N', $mostra_seq_in_sped = 'N'){
  global $conn, $cfg_mod_Spedizioni;
  $sql = "
  	SELECT DISTINCT TDAACA, TDTPCA, TDNRCA, PSFG01, PSFG02 FROM {$cfg_mod_Spedizioni['file_testate']}
	  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS 
	  ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
  		WHERE " . $this->get_where_std() . "
  		  AND TDNBOC = {$sped_id}
  	 ";
  
  	if (isset($stabilimento))
  		$sql .= " AND TDSTAB = " . sql_t($stabilimento);
  
 	$sql .= " ORDER BY PSFG02, TDTPCA, TDAACA, TDNRCA"; 

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);	
	
	
	$ret = array();	
	$esiste_0 = false;
	while ($row = db2_fetch_assoc($stmt)){
		
		$out_carico = "";
		
		if ($row['TDNRCA'] == 0) $esiste_0 = true;
		
		if ($per_esteso == 'Y')
			$out_carico = $this->k_carico_td_encode($row);
		else if ($per_esteso == 'AN') //anno/numero
			$out_carico = implode("_", array(trim($row['TDAACA']), $row['TDNRCA']));					
		else if ($per_esteso == 'TN') //tipo/numero
			$out_carico = implode("_", array(trim($row['TDTPCA']), $row['TDNRCA']));
		else
			$out_carico = $row['TDNRCA'];
		
		if($mostra_img_pagamento == 'Y'){
			//icona stato pagamento carico
		    $path_icona_stato_pagamento_carico = $this->get_icona_stato_pagamento_carico($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA'], $row['PSFG01']);
		    if ($path_icona_stato_pagamento_carico)
		        $out_carico .= '<span style="display: inline;"><img width=16 class="cell-img" src=' . img_path($path_icona_stato_pagamento_carico) . '></span>';			
		}
			
		if($mostra_seq_in_sped == 'Y'){
		    if (trim($row['PSFG02']) > 0){
		        $out_carico .= " (" . trim($row['PSFG02']) . ")";
		    }
		}
		
		$ret[] = $out_carico;	
	}

	if ($crea_default_0 == 'Y' && $esiste_0 == false){
		if ($per_esteso == 'Y')
			$ret[] = '0_ _0'; 
		else
			$ret[] = 0;
	}
	
	
	$str_ret = '';	
	if ($add_area_spedizione == 'Y'){
		$sped = $this->get_spedizione($sped_id);
		$aspe = new AreeSpedizione();
		$aspe->load_rec_data_by_itin($sped['CSCITI']);		
		$str_ret = "[" . trim($aspe->rec_data['TAKEY1']) . "] ";
	} 	
	
	if ($return_array == 'Y')
		return $ret;
		
	return $str_ret . implode(', ', $ret);  	 
 }
 
 function get_el_carichi_by_TDNBOF_tooltip($sped_id, $stabilimento=null){
     global $conn, $cfg_mod_Spedizioni;
     
     $sql = "SELECT DISTINCT TDAACA, TDTPCA, TDNRCA, PSSTAT, PSNOTE, PSCOEX
     FROM {$cfg_mod_Spedizioni['file_testate']}
     LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS
     ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
     WHERE " . $this->get_where_std() . "
     AND TDNBOF = {$sped_id}";
     
     if (isset($stabilimento))
         $sql .= " AND TDSTAB = " . sql_t($stabilimento);
         
         $sql .= " ORDER BY TDTPCA, TDAACA, TDNRCA";
         
         $stmt = db2_prepare($conn, $sql);
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt);
         
         $ret = array();
         while ($row = db2_fetch_assoc($stmt)){
             $out_carico = "";
             
             if ($mostra_img_stato == 'Y') {
                 // icona stato carico
                 if (trim($row['PSSTAT']) != ''){
                     switch(trim($row['PSSTAT'])){
                         case 'SP':
                             $img = 'icone/16x16/carico_spedito.png';
                             break;
                         case 'CO':
                             $img = 'icone/16x16/carico_controllato.png';
                             break;
                         case 'LT' :
                             $img = 'icone/16x16/button_black_play.png';
                             break;
                     }
                     
                     $out_carico .= '<span style="display: inline;"><img width=16 class="cell-img" src=' . img_path($img) . '></span>';
                 }
                 
             }
             
             $out_carico .= implode("_", array(trim($row['TDTPCA']),$row['TDNRCA']));
             $stato = trim($row['PSSTAT']);
             $row_stcar = find_TA_std('STCAR', $stato);
             $d_stato = trim($row_stcar[0]['text']);
             if($stato != '')
                 $out_carico .= " " .$d_stato;
                 if(trim($row['PSCOEX']) != '')
                     $out_carico .= ", " .trim($row['PSCOEX']);
                     if(trim($row['PSNOTE']) != '')
                         $out_carico .= ", " .trim($row['PSNOTE']);
                         
                         $ret[] = $out_carico;
         }
         
         return implode('<br>', $ret);
         
         
 }
 
 function get_el_carichi_by_TDNBOF($sped_id, $per_esteso = 'N', $crea_default_0 = 'N', $add_area_spedizione = 'N', $stabilimento=null, $mostra_img_pagamento = 'N', $return_array='N', $mostra_seq_in_sped = 'N', $mostra_img_stato = 'N', $ha_note = 'N', $no_zero = 'N'){
     global $conn, $cfg_mod_Spedizioni;
     $sql = "SELECT DISTINCT TDAACA, TDTPCA, TDNRCA, PSFG01, PSFG02, PSSTAT, PSNOTE
              FROM {$cfg_mod_Spedizioni['file_testate']}
	           LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS
	           ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
  		        WHERE " . $this->get_where_std() . "
  		        AND TDNBOF = {$sped_id}";
     
     if (isset($stabilimento))
         $sql .= " AND TDSTAB = " . sql_t($stabilimento);
         
         $sql .= " ORDER BY PSFG02, TDTPCA, TDAACA, TDNRCA";
         
         $stmt = db2_prepare($conn, $sql);
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt);
         
         $ret = array();
         $esiste_0 = false;
         while ($row = db2_fetch_assoc($stmt)){
            $out_carico = "";
            
            
            if ($mostra_img_stato == 'Y') {
                // icona stato carico
                if (trim($row['PSSTAT']) != ''){
                    switch(trim($row['PSSTAT'])){
                        case 'SP':
                            $img = 'icone/16x16/carico_spedito.png';
                            break;
                        case 'CO':
                            $img = 'icone/16x16/carico_controllato.png';
                            break;
                        case 'LT' :
                            $img = 'icone/16x16/button_black_play.png';
                            break;
                    }
                    
                    $out_carico .= '<span style="display: inline;"><img width=16 class="cell-img" src=' . img_path($img) . '></span>';
                }
                
            }
            

            if ($row['TDNRCA'] == 0)
                $esiste_0 = true;

            if ($per_esteso == 'Y')
                $out_carico .= $this->k_carico_td_encode($row);
            else if ($per_esteso == 'AN') // anno/numero
                $out_carico .= implode("_", array(
                    trim($row['TDAACA']),
                    $row['TDNRCA']
                ));
            else if ($per_esteso == 'TN') // tipo/numero
                $out_carico .= implode("_", array(
                    trim($row['TDTPCA']),
                    $row['TDNRCA']
                ));
            else
                $out_carico .= $row['TDNRCA'];
            
            if($ha_note == 'Y' && trim($row['PSNOTE']) != ''){
                $out_carico = "<b>{$out_carico}</b>";
            }

            if ($mostra_img_pagamento == 'Y') {
                // icona stato pagamento carico
                $path_icona_stato_pagamento_carico = $this->get_icona_stato_pagamento_carico($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA'], $row['PSFG01']);
                if ($path_icona_stato_pagamento_carico)
                    $out_carico .= '<span style="display: inline;"><img width=16 class="cell-img" src=' . img_path($path_icona_stato_pagamento_carico) . '></span>';
            }
            
            
       

            if ($mostra_seq_in_sped == 'Y') {
                if (trim($row['PSFG02']) > 0) {
                    $out_carico .= " (" . trim($row['PSFG02']) . ")";
                }
            }
            
            if($no_zero == 'Y'){
                if($row['TDNRCA'] > 0)
                     $ret[] = $out_carico;
            }else{
                $ret[] = $out_carico;
            }

            
         }
         
         if ($crea_default_0 == 'Y' && $esiste_0 == false){
             if ($per_esteso == 'Y')
                 $ret[] = '0_ _0';
                 else
                     $ret[] = 0;
         }
         
         
         $str_ret = '';
         if ($add_area_spedizione == 'Y'){
             $sped = $this->get_spedizione($sped_id);
             $aspe = new AreeSpedizione();
             $aspe->load_rec_data_by_itin($sped['CSCITI']);
             $str_ret = "[" . trim($aspe->rec_data['TAKEY1']) . "] ";
         }
         
         if ($return_array == 'Y')
             return $ret;
             
             return $str_ret . implode(', ', $ret);
 }
 
 
 
 
 
 


 

 function get_el_ddt_by_sped($sped_id){
 	global $conn, $cfg_mod_Spedizioni;
 	$sql = "SELECT DISTINCT TDTIDE, TDINDE, TDAADE, TDNRDE, TDTPDE, TDDTRE FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . " AND TDNBOC = {$sped_id}
 			ORDER BY TDDTRE, TDTPDE, TDNRDE";
 		
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 
 	$ret = array();
 	while ($row = db2_fetch_assoc($stmt)){
 		$ret[] = $row;
 	}
 	
   return $ret;	
 }
 

 function get_el_divisioni_by_sped($sped_id){
 	global $conn, $cfg_mod_Spedizioni;
 	$sql = "SELECT TDCDIV, TDDDIV, sum(TDBANC) as T_TDBANC, sum(TDVOLU) as T_TDVOLU
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . " AND TDNBOC = {$sped_id}
 			GROUP BY TDCDIV, TDDDIV";
 		
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 
 	$ret = array();
 	while ($row = db2_fetch_assoc($stmt)){
 		$ret[] = $row;
 	}
 
 	return $ret;
 	}
 
 

 function get_nr_scarichi_dep_by_sped($sped_id, $per_esteso = 'N', $crea_default_0 = 'N'){
 	global $conn, $cfg_mod_Spedizioni;
 	$sql = "
 			SELECT COUNT(DISTINCT(CONCAT(TDINDI, CONCAT(TDLOCA, CONCAT(TDCAP, TDNAZI))))) AS C_CLIENTI_DEP	 
 	 		FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . "
 			AND TDNBOC = ? AND TDTDES IN ('1', '2')
 		";
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($sped_id));
 	$row = db2_fetch_assoc($stmt);

 	return $row['C_CLIENTI_DEP'];
 	}
 
 	function get_nr_ordini_completi_by_sped($sped_id){
 		global $conn, $cfg_mod_Spedizioni;
 		$sql = "
 		SELECT COUNT(*) AS C_ORDINI_COMPLETI
 		FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . "
 	 			AND TDNBOC = ? AND TDCLOR IN ('O', 'M', 'P')
 		";
 	
 		$stmt = db2_prepare($conn, $sql);
 	 	$result = db2_execute($stmt, array($sped_id));
 	 	$row = db2_fetch_assoc($stmt);
 	
 	 	return $row['C_ORDINI_COMPLETI'];
 	} 	
 	
 
 	function get_trasportatori_exchange_by_sped($sped_id){
 		global $conn, $cfg_mod_Spedizioni;
 		$sql = "
 		SELECT CASE WHEN TDTDES = '1' THEN TDVET1 WHEN TDTDES = '2' THEN TDVET2 END AS TRASPORTATORE
				, SUM(TDTOCO) AS S_COLLI, SUM(TDVOLU) AS S_VOLUME, SUM(TDBANC) AS S_PALLET
				, SUM(TDPLOR) AS S_PESOL, SUM(TDPNET) AS S_PESON, SUM(TDTIMP) AS S_IMPORTO
				, SUM(TDCOSP) AS S_COLLI_SPUNTATI
				, COUNT(DISTINCT(CONCAT(TDINDI, CONCAT(TDLOCA, CONCAT(TDCAP, TDNAZI))))) AS C_CLIENTI_DEP 		
 		FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . "
 	 			AND TDNBOC = ? AND TDTDES IN ('1', '2')
 		GROUP BY CASE WHEN TDTDES = '1' THEN TDVET1 WHEN TDTDES = '2' THEN TDVET2 END 			
 					
 		";
 	
 		$stmt = db2_prepare($conn, $sql);
 	 	$result =  db2_execute($stmt, array($sped_id));
 	 	return $stmt;
 	} 	

 	function get_volume_evaso_by_sped_trasportatore_exchange($sped_id, $trasportatore){
 		global $conn, $cfg_mod_Spedizioni;
 		$sql = "
 		SELECT SUM(TDVOLU) AS S_VOL_EVASO FROM {$cfg_mod_Spedizioni['file_testate']}
 		WHERE " . $this->get_where_std() . "
 		AND TDFN11 = 1 /* SOLO ORDINI EVASI*/
 		AND TDNBOC = {$sped_id}
 		AND CASE WHEN TDTDES = '1' THEN TDVET1 WHEN TDTDES = '2' THEN TDVET2 END = '{$trasportatore}'
 		";
 	
 		$stmt = db2_prepare($conn, $sql);
 		$result = db2_execute($stmt);
 	
 		$row = db2_fetch_assoc($stmt);

 		if (is_null($row['S_VOL_EVASO'])) $row['S_VOL_EVASO'] = 0; 		
 		return $row['S_VOL_EVASO'];
 	}
 	
 	
 	
 	
 //passo un insieme di CSPROG di cui calcolare (e salvare!!!) il costo	
 function exe_ricalcolo_costo_globale($p){
 	global $conn, $cfg_mod_Spedizioni;
 	global $id_ditta_default;
 	
 	$ar_sped_id = $p->ar_sped_id;
		
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}";
	$sql .= " SET CSCSTR = ?, CSCSTD = ?";
	$sql .= " WHERE CSCALE = '*SPR' AND CSPROG = ? AND CSDT='$id_ditta_default'";
	$stmt = db2_prepare($conn, $sql);		
			
 	foreach($ar_sped_id as $sped_id){
 		$r = $this->exe_ricalcolo_costo_sped($p, $sped_id);
 		$ar_r = (array)json_decode($r);

 		$result = db2_execute($stmt, array((float)$ar_r['costo'], (float)$ar_r['tot_costo_deposito'], $sped_id)); 		 	
 	}
 	
 	$ret = array();
 	$ret['success'] = true; 	
 	echo acs_je($ret);
 }  	
 	
 	
 function exe_ricalcolo_costo_sped($p, $sped_id = null){
 	global $conn, $cfg_mod_Spedizioni;

	$ret = array();
	$ret['success'] = true;
	$ret['costo']	= 0;
	$ret['log_calcolo'] = '';	

	//spedizione
	if (is_null($sped_id))
		$sped_id = $_REQUEST['sped_id'];
	
	$spedizione = $this->get_spedizione($sped_id);
	
	//VETTORE PRINCIPALE	
		//ricerco il listino trasportatore valido in base alla spedizione
		$lt = new ListinoTrasportatori();
		$lt->load_rec_data_per_costo_sped($sped_id);

		$ret['log_calcolo'] .= "\n------------------";
		$ret['log_calcolo'] .= "\nCosto trasporto";		
		$ret = $this->exe_ricalcolo_costo_sped_parziale($sped_id, $lt, $ret);
		
		$ret['tot_costo_deposito'] = 0;		
	
	//DEPOSITI EXCHANGE
		$stmt_exch = $this->get_trasportatori_exchange_by_sped($sped_id);
		while ($row_exch = db2_fetch_assoc($stmt_exch)){
			$t = new Trasportatori();
			$t->load_rec_data_by_k(array('TAKEY1' => $row_exch['TRASPORTATORE']));
			
			$ret['log_calcolo'] .= "\n------------------";
			$ret['log_calcolo'] .= "\nCosto deposito: \n" . $t->rec_data['TADESC'];			
			$lt = new ListinoTrasportatori();
			$lt->load_rec_data_per_costo_sped($sped_id, $row_exch['TRASPORTATORE']);			
			$ret = $this->exe_ricalcolo_costo_sped_parziale($sped_id, $lt, $ret, $row_exch);
			$ret['tot_costo_deposito'] += $ret['costo_dep'];
		}
	
	return acs_je($ret);	 	
 }

 
 
 
 
 public function exe_ricalcolo_costo_sped_parziale($sped_id, $lt, $ret, $deposito = null){
 	if (is_null($deposito))
 		$f_costo = 'costo'; 	
 	else {
 		$f_costo = 'costo_dep';
 		$ret[$f_costo] = 0;
 	}
 	
 	if ($lt->rec_data['LTPROG'] > 0){
 		$ret[$f_costo] .= "\nTrovato record di listino valido (" . $lt->rec_data['LTPROG'] . ")";
 	
 		//costo spedizione
 		$ret[$f_costo] 		+= $lt->rec_data['LTCSPE'];
 		$ret['log_calcolo'] .= "\nCosto spedizione: " . $lt->rec_data['LTCSPE'];
 		
 		//costo percentuale per spedizione
 		if (is_null($deposito))
 			$totale_importo = $this->get_totale_by_sped($sped_id, 'S_IMPORTO');
 		else 
 			$totale_importo = $deposito['S_IMPORTO'];
 		
 		if ($lt->rec_data['LTPIMP'] > 0){
 			$costo_sped_percentuale = $lt->rec_data['LTPIMP'] * $totale_importo / 100 ; 
 			$ret[$f_costo] 		+= $costo_sped_percentuale; 			
 			$ret['log_calcolo'] .= "\n(perc.sped:: {$lt->rec_data['LTPIMP']} * {$totale_importo} = {$costo_sped_percentuale}"; 			
 		}
 		
 	
 		if (is_null($deposito)) { 		
	 		//costo per Km
	 		$m_km = $spedizione['CSKMTR'];
	 		if (isset($_REQUEST['km'])) $m_km = $_REQUEST['km']; //prendo quelli dalla form
 	
	 		//applico i km minimi
	 		$ret['log_calcolo'] .= "\nKm minimi: {$lt->rec_data['LTKMIN']}";
	 		$m_km = max($m_km, $lt->rec_data['LTKMIN']);
	 			
	 		$ret[$f_costo] 		+= ($lt->rec_data['LTCOKM'] * $m_km );
	 		$ret['log_calcolo'] .= "\nCosto al Km: {$lt->rec_data['LTCOKM']} x {$m_km}";
 		}
	 		
 		
 		//costo per scarichi (meno scarichi esclusi)
 		if (is_null($deposito))
 			$n_scarichi = $this->get_nr_scarichi_by_sped($sped_id, $deposito);
 		else 
 			$n_scarichi = $deposito['C_CLIENTI_DEP'];
 			
 			$sc_esclusi = $lt->rec_data['LTSESC'];
 				$sc_fatturare = max($n_scarichi - $sc_esclusi, 0);
 				$ret[$f_costo] 		+= ( $sc_fatturare  * $lt->rec_data['LTCSCA'] );
 				$ret['log_calcolo'] .= "\nCosto x scarico ({$n_scarichi} - {$sc_esclusi}): {$sc_fatturare} x {$lt->rec_data['LTCSCA']}";
 	
 				//costo per collo
 				if ($lt->rec_data['LTCOCO'] > 0){
 					
 					if (is_null($deposito))
 						$colli_spediti = $this->get_totale_by_sped($sped_id, 'S_COLLI_SPEDITI');
 					else
 						$colli_spediti = $deposito['S_COLLI_SPUNTATI'];
 					
 					if ($colli_spediti == 0){ //riparto dai colli programmati
 						if (is_null($deposito))
 							$colli_spediti = $this->get_totale_by_sped($sped_id, 'S_COLLI');
 						else
 							$colli_spediti = $deposito['S_COLLI'];
 					}
 					
				$costo_colli	= $colli_spediti * $lt->rec_data['LTCOCO'];
				$ret[$f_costo] += $costo_colli;
 				$ret['log_calcolo'] .= "\nCosto per collo: {$lt->rec_data['LTCOCO']} x {$colli_spediti} = {$costo_colli}";
 				}
 	
 				//costo per volume
 				if (is_null($deposito)){
	 				$v_totale = $this->get_volume_evaso_by_sped($sped_id);
	 				if ($v_totale == 0) //se il volume evaso e' 0 prendo quello totale
	 					$v_totale = $this->get_volume_by_sped($sped_id);
 				} else {
 					$v_totale = $this->get_volume_evaso_by_sped_trasportatore_exchange($sped_id, $deposito['TRASPORTATORE']);
 					if ($v_totale == 0) //se il volume evaso e' 0 prendo quello totale
 						$v_totale = $deposito['S_VOLUME']; 					
 				}
 	
 				//trovo la fascia del volume (in base alla quantit�)
 				if (      $lt->rec_data['LTQTA5'] <= $v_totale && $lt->rec_data['LTQTA5'] != 0){
 						$prz_m3 = $lt->rec_data['LTCUN5'];
				} elseif ($lt->rec_data['LTQTA4'] <= $v_totale&& $lt->rec_data['LTQTA4'] != 0){
 					$prz_m3 = $lt->rec_data['LTCUN4'];
 				} elseif ($lt->rec_data['LTQTA3'] <= $v_totale&& $lt->rec_data['LTQTA3'] != 0){
 						$prz_m3 = $lt->rec_data['LTCUN3'];
 				} elseif ($lt->rec_data['LTQTA2'] <= $v_totale&& $lt->rec_data['LTQTA2'] != 0){
 						$prz_m3 = $lt->rec_data['LTCUN2'];
 				} elseif ($lt->rec_data['LTQTA1'] <= $v_totale&& $lt->rec_data['LTQTA1'] != 0){
 						$prz_m3 = $lt->rec_data['LTCUN1'];
 				} else $prz_m3 = 0;
 	
 	
 				$costo_volume = $prz_m3  * $v_totale ;
 				$ret['log_calcolo'] .= "\nCosto x volume: ({$prz_m3} x {$v_totale}) = {$costo_volume}";
 	
 				$ret[$f_costo] 		+= $costo_volume;
 				$ret['log_calcolo'] .= "\nCosto totale: " . $ret[$f_costo];
 	
 				//e' compreso tra l'addebito minimo e massimo
 				if ($ret[$f_costo] < $lt->rec_data['LTAMIN'] && $lt->rec_data['LTAMIN'] != 0){
					$ret['log_calcolo'] .= "\nAddebito il valore minimo (" . $lt->rec_data['LTAMIN'] . ")";
					$ret[$f_costo] = $lt->rec_data['LTAMIN'];
		 				}
				if ($ret[$f_costo] > $lt->rec_data['LTAMAX'] && $lt->rec_data['LTAMAX'] != 0){
					$ret['log_calcolo'] .= "\nAddebito il valore massimo (" . $lt->rec_data['LTAMAX'] . ")";
					$ret[$f_costo] = $lt->rec_data['LTAMAX'];
 				}
 	
	} else {
 				$ret['log_calcolo'] .= "\nNessun listino valido";
	}
 	
  return $ret;	
 }
 
 
 
 

 
 

 function decod_std($tab, $cod){
 		global $conn, $cfg_mod_Spedizioni, $ar_decod_std, $id_ditta_default;
		$tab = trim($tab);
		$cod = trim($cod);

		if (!isset($ar_decod_std[$tab])){
				
			$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_tabelle']}
				WHERE TADT='$id_ditta_default' AND TATAID = '$tab'	/* AND TAKEY1 = '$cod' */
				";
					
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);	
				
			while ($row = db2_fetch_assoc($stmt)) {
				$r_cod = trim($row['TAKEY1']);
				$ar_decod_std[$tab][$r_cod] = trim($row['TADESC']);
			}
			
		}

		
		if (isset($ar_decod_std[$tab][$cod]))			
			return $ar_decod_std[$tab][$cod];
		else
			return "$cod";
 }	

	

 function find_TA_ITVE($tab, $k1 = null, $valore_attuale = null){
 		global $conn;
		global $cfg_mod_Spedizioni, $id_ditta_default;
		$ar = array();
		
		//in join prendo il mezzo, per recuperare la targa (da impostare in automatico nella spedizione)  		
		$sql = "SELECT TA_ITVE.*, TA_AUTO.TARIF2 AS TARGA
				 FROM {$cfg_mod_Spedizioni['file_tabelle']} TA_ITVE
				  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_AUTO
				    ON TA_ITVE.TADT = TA_AUTO.TADT AND TA_AUTO.TATAID = 'AUTO' AND TA_ITVE.TAKEY3 = TA_AUTO.TAKEY1
				 WHERE TA_ITVE.TADT='$id_ditta_default' AND TA_ITVE.TATAID = '$tab' ";
		
		if (isset($k1)) $sql .= " AND TA_ITVE.TAKEY1 IN('$k1', '*ALL') ";
				
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
		
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
		 if (isset($k1))
		 	$m_itin_id = $k1;
		 else 
			$m_itin_id = $row['TAKEY1'];
		 				
		 $ret[] = array("id" 		=> implode("|", array(trim($m_itin_id), trim($row['TAKEY2']), trim($row['TAKEY3']), trim($row['TAKEY4']))),
		 				"TAVOLU"	=> $row['TAVOLU'],
		 				"TAPESO"	=> $row['TAPESO'],
		 				"TABANC"	=> $row['TABANC'],
		 				"TATISP"	=> trim($row['TATISP']),
		 				"TATITR"	=> trim($row['TATITR']),
		 				"TARGA"		=> trim($row['TARGA']),		 		
		 		        "text" => acs_u8e(implode(" ", array(  trim($this->decod_std('AUTR', $row['TAKEY2']))
		 		        							 , trim($this->decod_std('AUTO', $row['TAKEY3']))
		 		        							 , trim($this->decod_std('COSC', $row['TAKEY4']))))) ) ;
		 
		}

		//se non e' presente, aggiungo il valore di partenza
		$valore_attuale_presente = false;
		foreach ($ret as $subarray){
			if ($subarray['id'] == $valore_attuale)
				$valore_attuale_presente = true;
		}		

		if (!$valore_attuale_presente){
			
			$vmc_exp = explode("|", $valore_attuale);
			if (strlen($vmc_exp[1]) > 0){
			
				$des_vmc = $this->des_vmc_sp(array(
						'CSCVET' => $vmc_exp[1], 
						'CSCAUT' => $vmc_exp[2],
						'CSCCON' => $vmc_exp[3]												
				));
				
				
				$ret[] = array(	"id"			=> $valore_attuale,
								"TAVOLU"		=> 0,
								"TAPESO"		=> 0,
								"TABANC"		=> 0,
								"TATISP"		=> '',
								"TATITR"		=> '',						
								"text"			=> "(?) {$des_vmc}");
			}
		}
		
	return $ret;	
 }	
 
 

 


 function get_TA_std($tab, $k1, $k2 = null, $no_ditta = 'N'){
 	global $conn;
 	global $cfg_mod_Spedizioni, $id_ditta_default;
 	$ar = array();
 	
 	if($no_ditta == 'N')
 	    $where_tadt = " AND TADT ='{$id_ditta_default}'";
 	else 
 	    $where_tadt = "";
 	  
 	
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} 
            WHERE 1 = 1 {$where_tadt} AND TATAID = '$tab' ";

 	
 	if (isset($k1))	$sql .= " AND TAKEY1 = " . sql_t($k1);
 	if (isset($k2))	$sql .= " AND TAKEY2 = " . sql_t($k2); 
 	
 	 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);

 	$row = db2_fetch_assoc($stmt);
 	if (!$row) $row = array();
 	
 	
 	
 	return $row;
 }

 function get_TA_std_by_des($tab, $des = null, $k1 = null, $k2 = null){
 	global $conn;
 	global $cfg_mod_Spedizioni, $id_ditta_default;
 	$ar = array();
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TATAID = '{$tab}' ";
 
 	if (isset($des))	$sql .= " AND TADESC = " . sql_t($des);
 	if (isset($k1))	$sql .= " AND TAKEY1 = '$k1' ";
 	if (isset($k2))	$sql .= " AND TAKEY2 = '$k2' ";
 	
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 
 	$row = db2_fetch_assoc($stmt);
 	if (!$row) $row = array();
 
 	return $row;
 }
 
 

 function get_elenco_porte_itinerario($itin_id){
 	global $conn, $cfg_mod_Spedizioni;
 	$ret = array(); 	
 	
 	//dall'itinerario recupero l'area di spedizione -> e lo stabilimento
 	$aspe = new AreeSpedizione();
 	$aspe->load_rec_data_by_itin($itin_id);
 	
 	if (isset($aspe->rec_data['TARIF2']) && !is_null($aspe->rec_data['TARIF2']) && strlen(trim($aspe->rec_data['TARIF2'])) > 0){
 		$stab = new SpedStabilimenti();
 		$stab->load_rec_data_by_k(array('TAKEY1' => $aspe->rec_data['TARIF2']));
 		
 	} else { 		
 		return $ret;
 	}
 	
 	$el_porte = explode(",", $stab->rec_data['TAMAIL']);
 	foreach ($el_porte as $porta){
 		$ret[] = array("id" => trim($porta), "text" => trim($porta));
 	}
 	return $ret;
 }
 

 function get_elenco_porte_stabilimento($cod){
 	global $conn, $cfg_mod_Spedizioni;
 
 	$stab = new SpedStabilimenti();
 	$stab->load_rec_data_by_k(array('TAKEY1' => $cod));
 
 	$el_porte = explode(",", $stab->rec_data['TAMAIL']);
 	$ret = array();
 	foreach ($el_porte as $porta){
 		$ret[] = array("id" => trim($porta), "text" => trim($porta));
 	}
 	return $ret;
 }
 
 
 
 
 function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null, $order_by_seq = 'N', $mostra_codice = 'N', $k3 = null){
 		global $conn;
		global $cfg_mod_Spedizioni, $id_ditta_default;
		$ar = array();  		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_tabelle']}
				WHERE TADT='{$id_ditta_default}' AND TATAID = '$tab'";
	
		if (isset($k1))		
				$sql .= " AND TAKEY1 = '$k1' ";
		
		if (isset($k2))
			$sql .= " AND TAKEY2 = '$k2' ";
		
		if (isset($k3))
		    $sql .= " AND TAKEY3 = '$k3' ";
		
		if (isset($aspe))
			$sql .= " AND TAASPE = '$aspe' ";		
		
		if (isset($gruppo))
			$sql .= " AND TARIF1 = '$gruppo' ";		
		

		if ($order_by_seq == 'Y')
			$sql .= "  ORDER BY TASITI, UPPER(TADESC)";
		else if ($order_by_seq == 'P')
			$sql .= "  ORDER BY TAPESO, UPPER(TADESC)";
		else
			$sql .= "  ORDER BY UPPER(TADESC)";	

		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
		
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
		 if (is_null($k1) || $is_get == 'Y') $m_id = trim($row['TAKEY1']);
		 else $m_id = $row['TAKEY2'];
		 
		 $r = array();
		 
		 $r['id'] 		= $m_id;
		 
		 if ($mostra_codice == 'Y')
		     $r['text'] = "[" . trim($m_id). "] " . $row['TADESC'];
		 else
		     $r['text'] = acs_u8e(trim($row['TADESC']));
		 
		 if ($esporta_esteso == 'Y'){
		 	$r['TAKEY1'] = $row['TAKEY1'];
		 	$r['TAKEY2'] = $row['TAKEY2'];
		 	$r['TAKEY3'] = $row['TAKEY3'];
		 	$r['TAKEY4'] = $row['TAKEY4'];
		 	$r['TAKEY5'] = $row['TAKEY5'];
		 	$r['TARIF2'] = $row['TARIF2'];
		 	$r['TAFG01'] = $row['TAFG01'];
		 	$r['TAFG02'] = $row['TAFG02'];
		 	$r['TAFG03'] = $row['TAFG03'];
		 	$r['TAFG04'] = $row['TAFG04'];
		 	$r['TAMAIL'] = trim($row['TAMAIL']);
		 	$r['TANAZI'] = trim($row['TANAZI']);
		 }
		 	
//		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
		 $ret[] = $r;	
		}		
			
	return $ret;	
 }	


 
 
 
 
 //NON FILTRO PER DITTA???
 function find_TA_std_comuni($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT *
 				FROM {$cfg_mod_Spedizioni['file_tabelle']}
 				WHERE TATAID = '$tab'";
 
 	if (isset($k1))
 		$sql .= " AND TAKEY1 = '$k1' ";
 
 		if (isset($k2))
 		$sql .= " AND TAKEY2 = '$k2' ";
 
 		if (isset($aspe))
 		$sql .= " AND TAASPE = '$aspe' ";
 
 		if (isset($gruppo))
 	$sql .= " AND TARIF1 = '$gruppo' ";
 
 
 	$sql .= "  ORDER BY UPPER(TADESC)";
 
		$stmt = db2_prepare($conn, $sql);
 		$result = db2_execute($stmt);
 
 	$ret = array();
 	while ($row = db2_fetch_assoc($stmt)) {
 	if (is_null($k1) || $is_get == 'Y') $m_id = $row['TAKEY1'];
 	else $m_id = $row['TAKEY2'];
		
 		 $r = array();
 		 	
 		 $r['id'] 		= $m_id;
 		 $r['text']		= acs_u8e(trim($row['TADESC']));
 		 			
 		 if ($esporta_esteso == 'Y'){
 		 		$r['TAKEY1'] = $row['TAKEY1'];
 		 		$r['TAKEY2'] = $row['TAKEY2'];
 		 		$r['TAKEY3'] = $row['TAKEY3'];
 		 		$r['TAKEY4'] = $row['TAKEY4'];
 		 		$r['TAKEY5'] = $row['TAKEY5'];
 		 		$r['TARIF2'] = $row['TARIF2'];
 		 		$r['TAFG01'] = $row['TAFG01'];
 		 		$r['TAFG02'] = $row['TAFG02'];
 		 		
 		 		$r['COD_PRO'] = trim($row['TAKEY2']);
 		 		$r['COD_REG'] = trim($row['TAKEY3']);
 		 		$r['COD_NAZ'] = trim($row['TAKEY4']);
 		 		 
 		 		$r['DES_PRO'] = trim($this->decod_std('ANPRO', $r['COD_PRO'])); 
 		 		$r['DES_REG'] = trim($this->decod_std('ANREG', $r['COD_REG']));
 		 		$r['DES_NAZ'] = trim($this->decod_std('ANNAZ', $r['COD_NAZ']));
 		 }
 
 		 //		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
 		 $ret[] = $r;
 	}
 		
 	return $ret;
 	}
 
 
 
 
 

 
 function options_select_std($field_cod, $field_des, $mostra_codice = 'N', $to_trim = null, $solo_sped_id = 0){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ret = array(); 	

 	if ($solo_sped_id > 0)
 		$sql_where = " AND TDNBOC = {$solo_sped_id}"; 	
 	
 	$sql = "SELECT {$field_cod} AS COD, {$field_des} AS DES
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD  " .  $this->add_riservatezza() . "
			WHERE " . $this->get_where_std() . " {$sql_where}
			GROUP BY {$field_cod}, {$field_des} ORDER BY {$field_des}";


 	
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg();
 	$result = db2_execute($stmt);
 	
 	while ($row = db2_fetch_assoc($stmt)) {
 		$cod = $row['COD'];
 		$des = $row['DES'];
 		
 		if ($to_trim == 'A'){
 			$cod = trim($cod);
 			$des = trim($des);
 		}
 		if ($to_trim == 'C') $cod = trim($cod);
 		if ($to_trim == 'D') $des = trim($des); 		
 		
 		if ($mostra_codice == 'Y' || (trim($cod) != '' && trim($des) == ''))
 			$des = "[{$cod}] {$des}";  		
 		
 		$ret[] = array( "id" => $row['COD'], "text"	=> $des );
 	}
 	return $ret;
 }
 
 
 
 function options_select_referente_cliente($mostra_codice = 'N'){
 	return $this->options_select_std('TDCOCL', 'TDDOCL', $mostra_codice);
 }
  
 function options_select_referente_ordine($mostra_codice = 'N'){
 	return $this->options_select_std('TDCORE', 'TDDORE', $mostra_codice);
 }
 
 function options_select_divisioni($mostra_codice = 'N'){
 	return $this->options_select_std('TDCDIV', 'TDDDIV', $mostra_codice);
 }
 
  
 function options_select_tipi_ordine($mostra_codice = 'N', $solo_sped_id = 0){
 	return $this->options_select_std('TDOTPD', 'TDDOTD', $mostra_codice, null, $solo_sped_id);
 }
 
 
 
 
 function decod_divisione_td($ditta, $codice, $mostra_codice = 'N'){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 
 	$sql = "SELECT TDCDIV, TDDDIV FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . " AND TDDT=? AND TDCDIV=?
 			GROUP BY TDCDIV, TDDDIV";
 
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt, array($ditta, $codice));
 
 			$ret = array();
 			$row = db2_fetch_assoc($stmt);
 			
 			if ($row){
 				if ($mostra_codice == 'Y')
 					$text = "[" . $row['TDCDIV'] . "] " . $row['TDDDIV'];
 				else $text = $row['TDDDIV'];
 			} else {
 				$text = " - " . $codice . " - ";
 			} 			
 			return $text;
 } 
 
 

 
 
 
 function get_el_clienti($p){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();

 	if ($cfg_mod_Spedizioni['an_cliente_per_ditta_origine'] == 'Y')
 		$per_ditta_orig = ', TDDTOR';
 	else 
 		$per_ditta_orig = '';
 	
 	
 	$sql = "SELECT TDCCON, TDDCON,
 			TDDLOC, TDNAZD, TDPROD, TDDNAD {$per_ditta_orig}
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE UPPER(TDDCON) LIKE '%" . strtoupper($p['query']) . "%'
 			GROUP BY TDCCON, TDDCON, TDDLOC, TDNAZD, TDPROD, TDDNAD {$per_ditta_orig}";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 
 	$ret = array();
 	while ($row = db2_fetch_assoc($stmt)) {
 	
 		$out_loc = $this->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
 		
 		if (strlen(trim($row['TDDTOR'])) > 0)
 			$ditta_orig = "Ditta {$row['TDDTOR']}"; 
 		else 
 			$ditta_orig = '';
 		
	 	$ret[] = array( "cod" 		=> $row['TDCCON'],
	 					"descr" 	=> trim(acs_u8e($row['TDDCON'])),
	 					"out_loc"	=> $out_loc,
	 					"ditta_orig"=> $ditta_orig,
	 	);
 	}
 	return $ret;
 } 
 
 
 function get_el_clienti_anag($p){
 	global $auth, $conn, $id_ditta_default;
 	global $cfg_mod_Spedizioni, $backend_ERP;
 	$ar = array();
 	$ret = array();
 	
 	if (strlen($auth->get_cod_riservatezza()) > 0){
		//add_riservatezza
		$join_riservatezza = "
				INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
				 ON CFDT = TD.TDDT AND DIGITS(CFCD) = TD.TDCCON
		" . $this->add_riservatezza();
	} else {
		$join_riservatezza = '';
	}

	if ($backend_ERP == 'GL'){
		/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
		$sql = "SELECT distinct BACLF0, BARAG0, BAIND0, BALOC0, BAPRO0, BANAZ0, BATEL0, BAFAX0
			FROM {$cfg_mod_Spedizioni['file_anag_cli']} {$join_riservatezza}
			WHERE (UPPER(
				REPLACE(REPLACE(BARAG0, '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
				OR BACLF0 LIKE '%".$p['query']."%') 
                AND BATCF0 = '1'
				AND BAQLA0 <> '98'
 			    order by BARAG0
 				";
	
  			$stmt = db2_prepare($conn, $sql);
		  	echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			
			while ($row = db2_fetch_assoc($stmt)) {
			    if (trim($row['BANAZ0']) == 'ITA' || trim($row['BANAZ0']) == 'IT' || trim($row['BANAZ0']) == ''){
					$out_loc = acs_u8e(trim($row['BALOC0']) . " (" . trim($row['BAPRO0']) . ")");
				} else {
					$des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
					$out_loc = acs_u8e(trim($row['BALOC0']) . ", " . trim($des_naz['text']));
				}
			
				$ret[] = array( "cod" 		=> $row['BACLF0'],
						"descr" 	=> acs_u8e(trim($row['BARAG0'])),
						"out_loc"	=> $out_loc,
						"out_ind"	=> acs_u8e(trim($row['BAIND0'])),
						"out_ema"	=> "",
						"out_tel"	=> acs_u8e(trim($row['BATEL0']) ." / ".trim($row['BAFAX0']) )
				);
			}			
		
				
	} else {
	    
	     $m_params = acs_m_params_json_decode();
	    	   
	     if (strlen(trim($m_params->form_values->f_cod_fi)) > 0)
	         $sql_where .= " AND CFCDFI LIKE '%{$m_params->form_values->f_cod_fi}%'";
	             
         if (strlen(trim($m_params->form_values->f_tel)) > 0)
             $sql_where .= " AND CFTEL LIKE '%{$m_params->form_values->f_tel}%'";
         
         if (strlen($m_params->form_values->f_email) > 0)
             $sql_where .= " AND GC.GCMAIL LIKE '%{$m_params->form_values->f_email}%'";
         
         if (strlen(trim($m_params->form_values->f_sel)) > 0)
             $sql_where .= " AND CFSEL1 = '{$m_params->form_values->f_sel}'";
	    
	    
 		/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */ 	
 			$sql = "SELECT distinct CFCD, CFTEL, CFCDFI, CFRGS1, CFRGS2, CFIND1, CFIND2, CFLOC1, CFPROV, CFNAZ, GC.GCMAIL 
 				FROM {$cfg_mod_Spedizioni['file_anag_cli']} {$join_riservatezza}
 				LEFT OUTER JOIN {$cfg_mod_Spedizioni['ins_new_anag']['file_appoggio']} GC
                    ON CFDT = GC.GCDT AND DIGITS(CFCD) = GC.GCCDCF AND GC.GCTPAN='CLI'
 				WHERE (UPPER(
 					REPLACE(REPLACE(CONCAT(CFRGS1, CFRGS2), '.', ''), ' ', '')
 					) " . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
 				
                OR CFCD " . sql_t_compare($p['query'], 'LIKE') .") 
                AND CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = 'C' AND CFFLG3 = ''  {$sql_where}
 			    order by CFRGS1, CFRGS2
 				";
 			
 			 			
  			$stmt = db2_prepare($conn, $sql);
  			echo db2_stmt_errormsg();  			
  			$result = db2_execute($stmt);

  			while ($row = db2_fetch_assoc($stmt)) {
  				if (trim($row['CFNAZ']) == 'ITA' || trim($row['CFNAZ']) == ''){
  					$out_loc = acs_u8e(trim($row['CFLOC1']) . " (" . trim($row['CFPROV']) . ")");
  				} else {
  					$des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
  					$out_loc = acs_u8e(trim($row['CFLOC1']) . ", " . trim($des_naz['text']));
  				}
  				
  				
  				$descr = trim($row['CFRGS1']) . " " . trim($row['CFRGS2']);
  				if ($p['mostra_codice'] == 'Y')
  					$descr .= " " . "[" . $row['CFCD'] . "]";
  				
  				$ret[] = array( "cod" 		=> $row['CFCD'],
  								"descr" 	=> acs_u8e($descr),
  								"out_loc"	=> $out_loc,
  								"out_ind"	=> acs_u8e(trim($row['CFIND1']) . " " . trim($row['CFIND2'])),
  				                "tel" 		=> $row['CFTEL'],
  				                "cod_fi" 	=> $row['CFCDFI']
  				    
  				);
  			}
	}		
  			
  	return $ret;
 } 
 
 
 function get_el_clienti_des_anag($p, $tab='VUDE'){
 	global $conn, $id_ditta_default;
 	global $cfg_mod_Spedizioni, $backend_ERP;
 	$ar = array();
 	$ret = array();
 	
 	if (strlen($p['cliente_cod']) == 0) return $ret;
 
 	
 	if ($backend_ERP == 'GL'){
 	    
 	    $sql_where = "";
 	    $today = oggi_AS_date();
 	    
 	    if(isset($p['gest_des']) && $p['gest_des'] == 'Y'){
 	       $sql_where .= ""; 
 	    }else{
 	       $sql_where .= " AND BFLI10  NOT LIKE  (CASE WHEN BFDTX0  <> '{$today}' THEN  'RI' ELSE  '' END)"; 
 	    }
 	    
 	    
 	    if (isset($cfg_mod_Spedizioni['file_anag_cli_des_escludi'])){
 	        $sql_where .= " AND BFTIN0 NOT IN (" . sql_t_IN($cfg_mod_Spedizioni['file_anag_cli_des_escludi']) . ")";
 	    }
 		
 		$sql = "SELECT *
 				FROM {$cfg_mod_Spedizioni['file_anag_cli_des']}
 				WHERE UPPER(BFDES0) LIKE '%" . strtoupper($p['query']) . "%'
 				AND BFTCF0=1 AND BFCLF0 = " . sql_t($p['cliente_cod']) . "
 				{$sql_where}";
		
 		$stmt = db2_prepare($conn, $sql);
 		echo db2_stmt_errormsg();
 		$result = db2_execute($stmt);
 		
 		while ($row = db2_fetch_assoc($stmt)) {
 			$c_naz = trim($row['BFNAZ0']);
 			if (trim($c_naz) == '' || trim($c_naz) == 'IT' || trim($c_naz) == 'ITA')
 				$out_loc = trim(acs_u8e($row['BFLOC0']));
 			else {
 				$des_naz = find_TA_sys('BNAZ', trim($row['BFNAZ0']));
 				$des_naz = trim($row['BFNAZ0']);
 				$out_loc = trim(acs_u8e($row['BFLOC0'])) . " (" . $des_naz . ")";
 			}
 				
 			$ret[] = array( "cod" 		=> $row['BFTIN0'],
 					"cod_cli"	=> $row['BFCLF0'],
 					"dt"		=> $id_ditta_default,
 			    
 			        "riser" => acs_u8e(trim($row['BFLI10'])),
 		
 					"descr" 	=> acs_u8e(
 							trim($row['BFRAG0'])
 					),
 					"denom"  => acs_u8e(trim($row['BFRAG0'])),
 					"out_loc"=> $out_loc,
 		
 					//Decodifico i vari campi indirizzo
 					'IND_D' => acs_u8e(trim($row['BFIND0'])),
 					'LOC_D' => acs_u8e(trim($row['BFLOC0'])),
 					'CAP_D' => acs_u8e(trim($row['BFCAP0'])),
 					'PRO_D' => acs_u8e(trim($row['BFPRO0'])),
 					'NAZ_D' => acs_u8e(trim($row['BFNAZ0'])),
 					'TEL_D' => acs_u8e(trim($row['BFTEL0'])),
 		
 			);
 		} 		
 		
 	} else {
 	
 		$sql = "SELECT TA.*, SUBSTRING(V2.TADES2, 27, 3) AS D_ASS
 				FROM {$cfg_mod_Spedizioni['file_anag_cli_des']} TA
 				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli_des']} V2
                ON TA.TADT = V2.TADT AND TA.TANR = V2.TANR AND TA.TACOR1 = V2.TACOR1
                   AND V2.TAID = 'VUD2'
                WHERE UPPER(TA.TAREST) LIKE '%" . strtoupper($p['query']) . "%'
                AND TA.TACOR1 LIKE '%" . strtoupper($p['query']) . "%'
 				AND TA.TADT = " . sql_t($id_ditta_default) . " AND TA.TAID=" . sql_t($tab) . " AND TA.TACOR1 = " . sql_t(sprintf("%09s", $p['cliente_cod'])) . " 
 				AND TA.TATP <>'S' /*AND TANR <> '=AG'*/
 				";
 		
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
   			$result = db2_execute($stmt);
   			echo db2_stmt_errormsg();
 
   			while ($row = db2_fetch_assoc($stmt)) {
   				$c_naz = substr($row['TAREST'], 193,   2);
   				if (trim($c_naz) == '' || trim($c_naz) == 'IT' || trim($c_naz) == 'ITA')
   					$out_loc = trim(acs_u8e(substr($row['TAREST'],  90,  60))) . " (" . substr($row['TAREST'], 160,   2) . ")";
   				else {
   					$des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
   					$out_loc = trim(acs_u8e(substr($row['TAREST'],  90,  60))) . " (" . $des_naz . ")";   					
   				}
   				
   				$m_cod = $row['TANR'];
   				if ($p['trim_cod'] == 'Y')
   					$m_cod = trim($m_cod);
   				
   				$ret[] = array( "cod" 		=> acs_u8e($m_cod),
   							"cod_cli"	=> $row['TACOR1'],
   							"dt"		=> $row['TADT'],
   							
   					
   							"descr" 	=> acs_u8e(
   										trim(substr($row['TAREST'], 90, 30)) . " " . 
   										trim(substr($row['TAREST'], 150, 10)) . " " .
   										trim(substr($row['TAREST'], 160, 2)) . " "    									
   									),
   							"denom"  => acs_u8e(trim($row['TADESC'] . $row['TADES2'] )),
   							"out_loc"=> $out_loc,
 					   					
   							//Decodifico i vari campi indirizzo
   							'IND_D'  => acs_u8e(substr($row['TAREST'],  30,  60)),
   							'LOC_D'  => acs_u8e(substr($row['TAREST'],  90,  60)),
   							'CAP_D'  => substr($row['TAREST'], 150, 10),
   							'PRO_D'  => substr($row['TAREST'], 160, 2),
   				            't_indi' => substr($row['TAREST'], 162, 1),
   							'des_s'  => substr($row['TAREST'], 172, 1), 
   				            'd_ass'  => trim($row['D_ASS']),
   				            'NAZ_D'  => substr($row['TAREST'], 193, 2), 
   							'TEL_D'  => substr($row['TAREST'], 173, 20)
   					
   			 );
   		}
 	}
 	
 	//print_r($ret);
 	
   	return $ret;
 } 
 
 
 function decod_cliente($cod, $tipo = 'C'){
     
     global $id_ditta_default, $conn, $cfg_mod_Gest;
     
     $sql = "SELECT * FROM {$cfg_mod_Gest['file_anag_cli']}
     WHERE CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = '{$tipo}' AND CFCD = ?";
     
     $stmt = db2_prepare($conn, $sql);
     $result = db2_execute($stmt, array($cod));
     $row = db2_fetch_assoc($stmt);
     
     return $row['CFRGS1'];
 }

 function get_anagrafica_from_gc($tipo_anagrafica, $codice){
     global $conn, $id_ditta_default;
     global $cfg_mod_Spedizioni;
     
     $ar_where = array($id_ditta_default, $tipo_anagrafica, $codice);
     
     $sql = "SELECT * FROM {$cfg_mod_Spedizioni['ins_new_anag']['file_appoggio']}
				 	WHERE GCDT = ? AND GCTPAN = ? AND GCCDCF = ?";
     $stmt = db2_prepare($conn, $sql);
     $result = db2_execute($stmt, $ar_where);     
     $row = db2_fetch_assoc($stmt);
     return $row;
 }
 
 
 
 function get_cliente_des_anag($dt, $cod_cli, $cod_des, $taid = 'VUDE'){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$ret = array();
 
 
 		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_anag_cli_des']}
				 	WHERE TADT = ? AND TAID='{$taid}' AND TACOR1 = ?
  					AND TATP <>'S' AND TANR = ?
 				";
 
  		$stmt = db2_prepare($conn, $sql);
  		$result = db2_execute($stmt, array($dt, sprintf("%09s", $cod_cli), $cod_des));
 
  		$row = db2_fetch_assoc($stmt);
  		
  		//Decodifico i vari campi indirizzo  		
  		$row['IND_D'] = substr($row['TAREST'],  30,  60);
  		$row['LOC_D'] = substr($row['TAREST'],  90,  30);
  		$row['CAP_D'] = substr($row['TAREST'], 150,  10);
  		$row['PRO_D'] = substr($row['TAREST'], 160,   2);  		
  				
  		return $row;
	}
  

	
	
	function get_el_ordini_by_day_list_select($list_selected_id){
		//recupero l'elenco degli ordini interessati
		$ar_ord = array();
		$el_ord = array();
		
		foreach($list_selected_id as $obj_row){
		
			if ($obj_row->liv == 'liv_3') { //singolo ordine
				$oer = $obj_row->k_ordine;
				$oe = explode('_', $oer);
		
				if (!in_array($obj_row->k_ordine, $el_ord)){
					$el_ord[] = $obj_row->k_ordine;
					$ar_ord[] = $oe[4];
				}
			}  //a livello ordine
		
		
			if ($obj_row->liv == 'liv_2') { //a livello di cliente
				//recupero gli ordini del livello selezionato
				$stmt = $this->get_stmt_ordini_by_liv2_plan($obj_row);
		
				while ( $row = db2_fetch_assoc($stmt) ){
					$k_ord = $row['TDDOCU'];
					//per ogni ordine
					if (!in_array($k_ord, $el_ord)){
						$el_ord[] = $k_ord;
						$ar_ord[] = $row['TDONDO'];
					}
				}
			} //a livello cliente
				
				
			if ($obj_row->liv == 'liv_1') { //a livello di carico
				//recupero gli ordini del livello selezionato
				$stmt = $this->get_stmt_ordini_by_liv1_plan($obj_row);
				$properties['k_carico_provenienza'] = $obj_row->k_carico;
					
				while ( $row = db2_fetch_assoc($stmt) ){
					$k_ord = $this->k_ordine_td($row);
					//per ogni ordine
					if (!in_array($k_ord, $el_ord)){
						$el_ord[] = $k_ord;
						$ar_ord[] = $row['TDONDO'];
					}
				}
			} //a livello carico
		}
	  return $el_ord;			
	} 
	
	
	
	
	
	
	
	
	
 

 function get_el_spedizioni($r, $return_el_ord = 'N', $solo_data = null){
 		global $conn;
		global $cfg_mod_Spedizioni;
		
		$properties = array();

		//bug parametri linux
		global $is_linux;
		if ($is_linux == 'Y')
			$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));

				
		$list_selected_id = json_decode($r['list_selected_id']);
		
		//recupero l'elenco degli ordini interessati
		$ar_ord = array();		//TODO: dovra' essere sostituito con $el_ord (anche nelle sotto funzioni)
		$el_ord = array();
		
		foreach($list_selected_id as $obj_row){

			if ($obj_row->liv == 'liv_3') { //singolo ordine
				$oer = $obj_row->k_ordine;
				$oe = explode('_', $oer);
				
				if (!in_array($obj_row->k_ordine, $el_ord)){
					$el_ord[] = $obj_row->k_ordine;
					$ar_ord[] = $oe[4];					
				}
			}  //a livello ordine
				

			if ($obj_row->liv == 'liv_2') { //a livello di cliente				
				//recupero gli ordini del livello selezionato
				$stmt = $this->get_stmt_ordini_by_liv2_plan($obj_row);
				
				while ( $row = db2_fetch_assoc($stmt) ){
					$k_ord = $this->k_ordine_td($row);
					//per ogni ordine
					if (!in_array($k_ord, $el_ord)){
						$el_ord[] = $row['TDDOCU']; //////$k_ord;
						$ar_ord[] = $row['TDONDO'];
					}					
				}								
			} //a livello cliente
			
			
			if ($obj_row->liv == 'liv_1') { //a livello di carico
				//recupero gli ordini del livello selezionato
				$stmt = $this->get_stmt_ordini_by_liv1_plan($obj_row);
				$properties['k_carico_provenienza'] = $obj_row->k_carico;				
			
				while ( $row = db2_fetch_assoc($stmt) ){
					$k_ord = $this->k_ordine_td($row);
					//per ogni ordine
					if (!in_array($k_ord, $el_ord)){
						$el_ord[] = $row['TDDOCU']; ////$k_ord;
						$ar_ord[] = $row['TDONDO'];
					}
				}
			} //a livello carico			
			
			
		}
		
		if ($return_el_ord == 'Y')
			return $el_ord;
		
		if (count($ar_ord) > 0)
			$sql_where_tdondo .= " AND TDONDO IN (" . sql_t_IN($ar_ord) . ")";
		else $sql_where_tdondo = " AND 1=2"; //non devo selezionare nessun ordine
		
		if (count($ar_ord) > 0)
			$sql_where_tddocu .= " AND TDDOCU IN (" . sql_t_IN($el_ord) . ")";
		else $sql_where_tddocu = " AND 1=2"; //non devo selezionare nessun ordine		
		
		
		//in base alle'area di spedizione (se indicata) imposto la minima data disponibile di assegnazione
		$itin_attuale_cod = $r['cod_iti'];
		$itin = $this->get_TA_std('ITIN', $itin_attuale_cod);
		$area = $this->get_TA_std('ASPE', $itin['TAASPE']);
		if ((int)$area['TARIF1'] > 0){
			if (((int)$area['TARIF1'] > 100)) 	
				//prendo direttamente la data inserita
				$properties['area_min_dispo'] = (int)$area['TARIF1'];
			else {
				//se ho indicato un numero < 100 (es: 15) considero questi come i giorni lavorivi (no Sab e Dom) a partire da oggi
				$properties['area_min_dispo'] = $this->aggiungi_gg_lavorativi(0, (int)$area['TARIF1'], '', true);
			}
		}
		else
			$properties['area_min_dispo'] = 0;
		
		
		//se l'area di spedizione ha il parametro prg_dopo_il <ELENCO_TIPI_ORDINE>
		// allora faccio il controllo su area_min_dispo solo se gli stati degli ordini appartengono a quelli indicati
		$aspe = new AreeSpedizione();
		$aspe->load_rec_data_by_itin($itin_attuale_cod);
		$aspe_parameters = $aspe->parse_YAML_PARAMETERS($aspe->rec_data);

		if (isset($aspe_parameters['prg_dopo_il'])){
			$ar_considera_solo_tipologia_ordini  = explode(",", $aspe_parameters['prg_dopo_il'] );
			$sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_testate']} 
					WHERE 1=1 {$sql_where_tddocu} 
					AND TDCLOR IN (" . sql_t_IN($ar_considera_solo_tipologia_ordini) . ")";
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);			
			$row = db2_fetch_assoc($stmt);
			if ($row['C_ROW'] == 0) //se non ho ordini delle tipologie indicate allora non devo considerare area_min_dispo (azzero)
				$properties['area_min_dispo'] = 0;
		}		

		
		
		
		
		//calcolo la data disponibilita' piu' alta
		$sql = "SELECT max(TDDTDS) as MAX_DISPO,
					   SUM(TDVOLU) as SUM_VOL,
					   SUM(TDPLOR) as SUM_PES,
					   SUM(TDBANC) as SUM_PAL,
					   MIN(TDDTEP) AS TDDTEP,
					   MAX(TDSTAT) AS TDSTAT,
					   sum(TDFN02) as T_cli_bloc, sum(TDFN03) as T_ord_bloc 
				 FROM {$cfg_mod_Spedizioni['file_testate']}
				 WHERE 1=1 {$sql_where_tddocu} ";
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		$row = db2_fetch_assoc($stmt);

		$oggi_AS = oggi_AS_date();
		$max_dispo = max($row['MAX_DISPO'], $oggi_AS);		
		$properties['max_dispo'] = $max_dispo;
		$properties['sum_vol'] = $row['SUM_VOL'];
		$properties['sum_pes'] = $row['SUM_PES'];
		$properties['sum_pal'] = $row['SUM_PAL'];
		$properties['el_ordini'] = $el_ord;
		$properties['num_ordini'] = count($ar_ord);
		$properties['TDDTEP'] = $row['TDDTEP'];
		$properties['T_cli_bloc'] = (int)$row['T_CLI_BLOC'];
		$properties['T_ord_bloc'] = (int)$row['T_ORD_BLOC'];
		
		//se tutti gli ordini sono di un singolo stato, impostato nella TATAID='CAR0S'
		// allora passo i posssibili stato da assegnare abbinando carico 0
		
		//ho un unico stato?
		$sql = "SELECT TDSTAT
			FROM {$cfg_mod_Spedizioni['file_testate']}
			WHERE 1=1 {$sql_where_tddocu}
			GROUP BY TDSTAT";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);		
		
		$n_stati = $n_CAR0S = 0;
		while($row = db2_fetch_assoc($stmt)){
			$ar_stati[] = $row;
			$n_stati++;						

				$m_tdstat = $row['TDSTAT'];
				$el_array = find_TA_std('CAR0S', $m_tdstat, 5);
					
				foreach($el_array as $nuovo_stao){
					$n_CAR0S ++;
			
					$nv = array();
					$nv[] = trim($nuovo_stao['id']);
					$nv[] = acs_u8e(trim($nuovo_stao['text']));
					$nv_to_pro[] = $nv;

				}
			
		}		
		
		//ho un unico stato, e ho trovato CAR0S, lo aggiungo alle properties
		$properties['NUOVO_STATO_DISTINCT'] = $n_stati;		
		$properties['NUOVO_STATO_CAR0S'] = $n_CAR0S;
		if ($n_stati == 1 && $n_CAR0S > 0)
			$properties['AR_NUOVO_STATO']= $nv_to_pro;
		


		
		//calcolo i colli programmati (in base agli ordini selezionati)
		$colli_programmati_by_selezionati = $this->get_ar_tot_CP(null, 0, null, $ar_ord, "Y");
		
		if (count($colli_programmati_by_selezionati) > 0)
			$properties['sum_colli_PRODUZIONE'] = $colli_programmati_by_selezionati['TOT']['CP'];
		else  
			$properties['sum_colli_PRODUZIONE'] = 0;
		
		//calcolo se ci sono ordini con data valorizzata (TDDTVA) o con MTO emessi (TDMTOO = 'Y'')
		//senza una causale di riprogrammazione (file WPI0AD0, ADTP = 'R').
		//Se ci sono degli ordini che soddisfano questa richiesta -> impediscono l'assegnazione in altre date
		
		//parametri del modulo (non legati al profilo)
		$mod_js_parameters = $this->get_mod_parameters();
		
		$sql_where_obbl_ripro = '1=2 ';
		if (strpos($mod_js_parameters->obbl_ripro, 'C') !== false) //per ordini confermati
			$sql_where_obbl_ripro .= " OR TDDTVA > 0";
		if (strpos($mod_js_parameters->obbl_ripro, 'E') !== false) //per ordini con MTO emessi
		    $sql_where_obbl_ripro .= " OR TDMTOO IN ('Y', 'O')";  //OR TDMTOO = 'Y'
		if (strpos($mod_js_parameters->obbl_ripro, 'I') !== false) //ICE (solo per NON HOLD)
			$sql_where_obbl_ripro .= " OR (CSFG01 = '1' AND TDSWSP='Y')";				
		if (strpos($mod_js_parameters->obbl_ripro, 'T') !== false) //test: tutti
			$sql_where_obbl_ripro .= " OR 1=1";		
				
		$sql = "SELECT COUNT(*) AS C_ROW
				FROM {$cfg_mod_Spedizioni['file_testate']} TD
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_deroghe']} AD
					ON ADTP = 'R' AND TD.TDDOCU = AD.ADRGES AND TD.TDDTEP = AD.ADDTDE
				INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL ON
						TDDT = CAL.CSDT AND TD.TDDTEP = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_Spedizioni['tipo_calendario']}'
									
				WHERE 1=1 {$sql_where_tddocu} AND ( ({$sql_where_obbl_ripro}) AND AD.ADRGES IS NULL)";

		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);		
		$row = db2_fetch_assoc($stmt);
		$properties['ordini_con_obbligo_causale_riprogrammazione'] = $row['C_ROW'];
		
		//ho un unico cliente/destinazione?		 
		$sql = "SELECT TDCCON, TDCDES
				FROM {$cfg_mod_Spedizioni['file_testate']}
				WHERE 1=1 {$sql_where_tddocu} 
				GROUP BY TDCCON, TDCDES";		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);

		$ar_cli = array();
		$n_cli = 0;
		while($row = db2_fetch_assoc($stmt)){
			$ar_cli[] = $row;
			$n_cli++;
		}

		if ($n_cli == 1)
			$cli = $ar_cli[0];
		
		
		//provengo da un'unica spedizione o data?
		//se si rilasso i vincoli sulla spedizione stessa (nessun controllo) o sulla data (no controlli di data)
		$sql = "SELECT  COUNT(DISTINCT(TDNBOC)) AS C_TDNBOC, MAX(TDNBOC) AS TDNBOC,
						COUNT(DISTINCT(TDDTEP)) AS C_TDDTEP, MAX(TDDTEP) AS TDDTEP
					 FROM {$cfg_mod_Spedizioni['file_testate']} WHERE 1=1 {$sql_where_tddocu}";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		$row = db2_fetch_assoc($stmt);
		
		if ($row['C_TDNBOC'] == 1)
			$rilassa_vincoli_su_sped = $row['TDNBOC'];
		if ($row['C_TDDTEP'] == 1)
			$rilassa_vincoli_su_data = $row['TDDTEP'];


		
		
		if ($r['solo_scadute'] == 'Y'){
			$r['da_oggi'] = 'SC';
		}
		
		$stmt = $this->get_spedizioni_per_assegnazione($r['cod_iti'], $r['da_oggi'], $el_ord, $r['scadute_da_data'], $r['solo_data']);
		
		$ar = array();
		$giorni_stadio      = $this->get_giorni_stadio(0,0);		

		
		//sono in "collega spedizione"
		if (strlen($r['scelta_sped_collegata_per_sped_id']) > 0){
			$sped_provenienza = $this->get_spedizione($r['scelta_sped_collegata_per_sped_id']);
		}
		
		
		//per ogni spedizione
		$cont_out_sped = 0;
		
		while ($row = db2_fetch_assoc($stmt)) {
		
			//in assegna spedizioe (non collega spedizione)
			if (strlen($r['scelta_sped_collegata_per_sped_id']) == 0){
				
				//provengo da questa spedizione.... non devo fare controlli (server per azzera carico)
				if ($row['CSPROG'] == $rilassa_vincoli_su_sped){
					$row['STATUS'] = 'FORCE_OK';
					$row['STATUS_MESSAGE'] = "Stessa spedizione di partenza";
				}
				

				if ($cfg_mod_Spedizioni['disabilita_tutti_vincoli_su_assegna_sped'] == 'Y'){
					$row['STATUS'] = 'FORCE_OK';
					$row['STATUS_MESSAGE'] = "No check from parameters";
				}
				
				
				
			}
			
			
			//sono in "collega spedizione"
			if (strlen($r['scelta_sped_collegata_per_sped_id']) > 0 && $r['solo_raffronto'] != 'Y'){
				//vincoli su ammissibilita' in collega spedizione
				
				//GESTISCO 'STATUS' E 'STATUS_MESSAGE'
				//PER SEGNALARE DOVE NON E' AMMESSA LA SELEZIONE DELLA SPEDIZIONE COLLEGATA
				
				//- Stessa spedizione
				if ($row['CSPROG'] == $sped_provenienza['CSPROG']){
					$row['STATUS'] = 'ERR';
					$row['STATUS_MESSAGE'] = "Le due spedizoni coincidono";
				}
				
				
				//- Spedizione selezionata non gia' collegata ad altra spedizione
				if ($row['CSNSPC'] > 0 && $row['CSNSPC'] != $sped_provenienza['CSPROG']){
					$row['STATUS'] = 'ERR';
					$row['STATUS_MESSAGE'] = "Spedizione gi&agrave; collegata ad altra spedizione";					
				}
				
				//- devono avere stessa data di spedizione (CSDTPG) o se non impostata devono avere stessa data evasione
				if ($row['CSDTPG'] != $sped_provenienza['CSDTPG']){
					$row['STATUS'] = 'ERR';
					$row['STATUS_MESSAGE'] = "Le spedizioni devono avere stessa data di spedizione ({$row['CSDTPG']}, {$sped_provenienza['CSDTPG']})";
				}
				
				
				//- (senza data di spedizione -> verifico evasione programmata)
				if ($row['CSDTPG'] == 0 && $sped_provenienza['CSDTPG'] == 0 && $row['CSDTSP'] != $sped_provenienza['CSDTSP']){
					$row['STATUS'] = 'ERR';
					$row['STATUS_MESSAGE'] = "Sulle spedizioni non e&grave; stata impostata la data di spedizione, e le date di evasione programmata non coincidono";
				}
				
				
				//- devono avere stesso vettore
				if ($row['CSCVET'] != $sped_provenienza['CSCVET']){
					$row['STATUS'] = 'ERR';
					$row['STATUS_MESSAGE'] = "Le spedizioni devono avere stesso vettore";
				}
				
			}			
		  	
			
		 if (strlen(trim($row['CSTARG'])) > 0)
		  $accoda_targa = " (T: " . trim($row['CSTARG']) . ")";
		 else
		  $accoda_targa = '';	
			
		 //per gestione colli produzione max esubero programmabili
			 //stadio		 
			 $dati_colli_per_data_stadio = $giorni_stadio[$row['CSDTSP']];
			 //max esubero
			 $colli_programmabili_data = $this->get_ar_tot_PR(date('Y-m-d', strtotime($row['CSDTSP'] . " +0 days")), 1, 'Y');
			 if ((int)$colli_programmabili_data[$row['CSDTSP']]["PR"] > 0)
			 	$dati_colli_per_data_max_esub = $colli_programmabili_data[$row['CSDTSP']]["PR"];
			 else
			 	$dati_colli_per_data_max_esub = 0;
			 //colli in produzione (escluso ordini in spostamento)
			 $ar_tot_CP = $this->get_ar_tot_CP(date('Y-m-d', strtotime($row['CSDTSP'] . " +0 days")), 1, $ar_ord);
			 if ((int)$ar_tot_CP[$row['CSDTSP']]["CP"] > 0)
			 	$dati_colli_per_data_in_prod = $ar_tot_CP[$row['CSDTSP']]["CP"];
			 else
			 	$dati_colli_per_data_in_prod = 0;
			 			 
		 	//sono tutte assistenze? (nel caso non devo fare nessun controllo su SLUSH)
		 	$ar_clor = $this->get_clor($ar_ord);
		 	$solo_assistenze = 'Y';
		 	foreach($ar_clor as $kclor=>$clor)
		 		if ($kclor != 'A') $solo_assistenze = 'N';
					 		 
			 			 
			 
		 $dati_colli_per_data = implode("|", array( $dati_colli_per_data_stadio, 
		 											$dati_colli_per_data_max_esub, 
		 											$dati_colli_per_data_in_prod,
		 											$solo_assistenze
		 ));
 
		 
		 $ar[] = array( "CSPROG" 	=> $row['CSPROG'],
		 				"DATA"		=> $row['CSDTSP'],
		 				"DATA_PROG"	=> print_date($row['CSDTPG']),		 				
		 				"ORA_PROG"	=> print_ora($row['CSHMPG']),
		 				"CSDESC"	=> acs_u8e($row['CSDESC']),
		 		        "VETTORE" 	=> $vet,
		 		        "MEZZO"		=> $mez,
						"CASSA"		=> $cas,
						"vmc"		=> acs_u8e($this->des_vmc_sp($row)) . $accoda_targa,
						"carico"	=> $this->get_el_carichi_by_sped($row['CSPROG'], 'Y', 'Y'),
		 				"carico_d"	=> $this->get_el_carichi_by_sped($row['CSPROG'], 'N', 'N'), //per la colonna  
						"VOL_D"		=> n($row['CSVOLD'],1),
						"VOL_A"		=> n($row['ASS_VOL'],1),
						"PES_D"		=> n($row['CSPESO'],1),
						"PES_A"		=> n($row['ASS_PES'],1),								 		        
						"PAL_D"		=> n($row['CSBAND'],0),
						"PAL_A"		=> n($row['ASS_PAL'],0),

		 				"VOL_CARICABILE" => $row['CSVOLD'] - $row['ASS_VOL'], 
		 				"PES_CARICABILE" => $row['CSPESO'] - $row['ASS_PES'],
		 				"PAL_CARICABILE" => $row['CSBAND'] - $row['ASS_PAL'],
		 		
				 		"VOL_DA_CARICARE" => $properties['sum_vol'],
				 		"PES_DA_CARICARE" => $properties['sum_pes'],
				 		"PAL_DA_CARICARE" => $properties['sum_pal'],		 		

		 				"ESISTE_CLIENTE" => $this->verifica_cliente_in_sped($cli['TDCCON'], $cli['TDCDES'], $row['CSPROG']),
		 		
		 				//per controllo su max esubero colli produzione		 				
		 				"DATI_COLLI_PER_DATA" => $dati_colli_per_data,
		 		
		 				//per validita
		 				"STATUS" 		=> $row['STATUS'],
		 				"STATUS_MESSAGE"	=> $row['STATUS_MESSAGE']
		 		         );

		 //limite per evitare supero tempo max (Marinelli)
		 if ((int)$cfg_mod_Spedizioni['nr_max_in_el_spedizioni'] > 0)
		 	$nr_max_in_el_spedizioni = (int)$cfg_mod_Spedizioni['nr_max_in_el_spedizioni'];		 	
		 else 
		 	$nr_max_in_el_spedizioni = 200;
		 
		 if (++$cont_out_sped >= $nr_max_in_el_spedizioni)
		 	break; 
		 
		} //per ogni spedizione
		
		$ret = 	"{\"root\": " . acs_je($ar) . ",
				  \"properties\": " . acs_je($properties) . "  
 				}";		
	return $ret;	
 }	



 function get_el_scarichi_intermedi($r){
 	global $conn, $id_ditta_default;
 	global $cfg_mod_Spedizioni;
 
 	$properties = array();
 
 	$list_selected_id = json_decode($r['list_selected_id']);
 
 	//calcolo la data disponibilita' piu' alta
 	$sql = "SELECT *
		 	FROM {$cfg_mod_Spedizioni['file_tabelle']}
		 	WHERE TADT='{$id_ditta_default}' AND TATAID = 'TRIT' AND TAKEY2 = ?";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($r['cod_iti']));
 
 		$ar = array();
 		while ($row = db2_fetch_assoc($stmt)) {
 			
	 		$ar[] = array(
	 			"COD_TRASPORTATORE"	=> trim($row['TAKEY1']),	
		 		"TRASPORTATORE"		=> acs_u8e($this->decod_std('AVET', $row['TAKEY1'])),
	 			);
 				 							
		}
 
 		$ret = 	"{\"root\": " . acs_je($ar) . ",
 				\"properties\": " . acs_je($properties) . "
 		}";
 		return $ret;
 }
 

 
 
 
 function get_el_destinazioni_finali($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$properties = array();
 	
 	//bug parametri linux
 	global $is_linux;
 	if ($is_linux == 'Y')
 		$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\')); 	
 
 	$list_selected_id = json_decode($r['list_selected_id']);
 	
 	//recupero il codice cliente
 	$k_cli_des = $list_selected_id[0]->k_cli_des;
 	$k_cod_cli_exp = explode("_", $k_cli_des);
 	$cod_cli = $k_cod_cli_exp[1]; 
 	
  	$ar_dest = $this->get_el_clienti_des_anag(array("cliente_cod" => $cod_cli));
 	
 		$ret = 	"{\"root\": " . acs_je($ar_dest) . ",
 				\"properties\": " . acs_je($properties) . "
 		}";
  		return $ret;
 }
 
 
 
 
 

 public function verifica_cliente_in_sped($cli, $des, $sped_id){
 	global $conn;
 	global $cfg_mod_Spedizioni; 	
 	
 	$sql = "SELECT count(*) AS TOT_R
		 	FROM {$cfg_mod_Spedizioni['file_testate']}
		 	WHERE TDNBOC = {$sped_id} AND TDCCON = " . sql_t($cli) . " 
		 	  AND TDCDES = " . sql_t($des);

 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	$row = db2_fetch_assoc($stmt); 	
 	return $row['TOT_R'];
 }
 
 

 function crt_spedizione($p){ 	
 		global $conn, $id_ditta_default;
		global $cfg_mod_Spedizioni;
		
		$ar_itvet = explode('|', $p['f_itvet']);
		
		//Se non mi viene passata la data spedizione
		if (!strlen($p['f_data_spedizione']) > 0) 	$p['f_data_spedizione'] = 0;		
		
		$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_calendario']}(CSDT, CSCALE, CSDTRG, CSPROG, 
						CSCITI, CSCVET, CSCAUT, CSCCON, CSTISP, CSDTSP, CSHMPG, CSSTSP, CSDTPG) VALUES (";
					
		$id_sped = $this->next_num('SPE');
					
		$sql .= implode(", ", array(
								sql_t($id_ditta_default), sql_t("*SPR"), 0, $id_sped,
								sql_t($p['itin_id']), sql_t($ar_itvet[1]),sql_t($ar_itvet[2]),
								sql_t("-"), sql_t($p['f_tipologia']) , $p['f_data'], sql_f($p['f_ora']), sql_t('PI'),
								$p['f_data_spedizione']));
	    $sql .= ")";
									
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		$result = $id_sped;				
	return $result; 		
 }

 function upd_spedizione($p){ 	
 		global $conn, $appLog;
		global $cfg_mod_Spedizioni;
		
		$ar_itvet = explode('|', $p['f_itvet']);
		
		//Se non mi viene passata la data
		if (!isset($p['f_data'])) 	$p['f_data'] = 'CSDTSP';  //puo' non essere passata e non devo modificarla		
		
		//Se non mi viene passata la data/ora inizio carico
		if (!isset($p['f_data_inizio_carico']) || strlen($p['f_data_inizio_carico']) == 0 ) $p['f_data_inizio_carico'] = '0';
		if (!isset($p['f_ora_inizio_carico']) || strlen($p['f_ora_inizio_carico']) == 0 )  	$p['f_ora_inizio_carico']  = '0';		
		
		//Se non mi viene passata la data/ora inizio scarico
		if (!isset($p['f_data_inizio_scarico']) || strlen($p['f_data_inizio_scarico']) == 0 ) $p['f_data_inizio_scarico'] = '0';
		if (!isset($p['f_ora_inizio_scarico']) || strlen($p['f_ora_inizio_scarico']) == 0 )  	$p['f_ora_inizio_scarico']  = '0';		
		
		//Se non mi viene passata la data/ora spedizione
		if (!strlen($p['f_data_spedizione']) > 0) 	$p['f_data_spedizione'] = '0';		
		if (!isset($p['f_ora']))  	$p['f_ora']  = '0';		
		
		if (!isset($p['f_stato']))  
			$p['f_stato']  = 'CSSTSP';
		else
			$p['f_stato'] = sql_t($p['f_stato']);
		

		
		if (!isset($p['f_seq_gg_scarico_1'])) $p['f_seq_gg_scarico_1']  = 'CSSEQ1'; else $p['f_seq_gg_scarico_1'] = sql_t($p['f_seq_gg_scarico_1']);
		if (!isset($p['f_seq_gg_scarico_2'])) $p['f_seq_gg_scarico_2']  = 'CSSEQ1'; else $p['f_seq_gg_scarico_2'] = sql_t($p['f_seq_gg_scarico_2']);
		if (!isset($p['f_seq_gg_scarico_3'])) $p['f_seq_gg_scarico_3']  = 'CSSEQ1'; else $p['f_seq_gg_scarico_3'] = sql_t($p['f_seq_gg_scarico_3']);
		if (!isset($p['f_seq_gg_scarico_4'])) $p['f_seq_gg_scarico_4']  = 'CSSEQ1'; else $p['f_seq_gg_scarico_4'] = sql_t($p['f_seq_gg_scarico_4']);
		if (!isset($p['f_seq_gg_scarico_5'])) $p['f_seq_gg_scarico_5']  = 'CSSEQ1'; else $p['f_seq_gg_scarico_5'] = sql_t($p['f_seq_gg_scarico_5']);
		
		if (!isset($p['f_cointainer'])) $p['f_cointainer']  = 'CSNCON'; else $p['f_cointainer'] = sql_t($p['f_cointainer']);
		if (!isset($p['f_sigillo'])) $p['f_sigillo']  = 'CSNSIG'; else $p['f_sigillo'] = sql_t($p['f_sigillo']);
		
		
		
		$sped = $this->get_spedizione($p['sped_id']);		
		
		//2014-12-04
		//Se viene modificata la data di spedizione devo prima verificare il limite dei colli (SPEDIZIONE in capacita' produttiva)
		if ($p['f_data_spedizione'] > 0 && $sped['CSDTPG'] != $p['f_data_spedizione']){
			
			$colli_in_sped = (int)$this->get_totale_by_sped($p['sped_id'], 'S_COLLI');
			$tot_colli_data_dest = (int)$this->get_totale_by_data("TDDTSP", $p['f_data_spedizione'], 'S_COLLI', 'N', $p['sped_id']);			

			//recupero il totale spedibile per giorno
			$max_SPEDIZIONE_ar = $this->get_ar_tot_PS($p['f_data_spedizione'], 1);
			$max_SPEDIZIONE = (int)$max_SPEDIZIONE_ar[$p['f_data_spedizione']]['PS'];

			if ($colli_in_sped + $tot_colli_data_dest > $max_SPEDIZIONE && $max_SPEDIZIONE > 0){
				$ret['success'] = false;
				$ret['error_log'] = "Superato il limite di capacit&agrave; di spedizione ( {$max_SPEDIZIONE} )<br/>nella data selezionata";
				$ret['error_log'] .= " <br/>( colli in sped: {$colli_in_sped}, tot_colli_data_dest: {$tot_colli_data_dest} )";
				return $ret;				
			}

		} 
		
		//2014-12-04
		//Se viene modificata la data di spedizione devo verificare che soddisfi le regole di validita'		
		if ($p['f_data_spedizione'] > 0 && $sped['CSDTPG'] != $p['f_data_spedizione']){
			
			//recupero l'area di spedizione da cui prendere i parametri
			$aspe = new AreeSpedizione();
			$aspe->load_rec_data_by_itin($sped['CSCITI']);
			$aspe_parameters = $aspe->parse_YAML_PARAMETERS($aspe->rec_data);
			$min_gg_params = explode(",", $aspe_parameters['data_sped_min_gg']);

			//il parametro deve essere del tipo 1,1600,1
			if (count($min_gg_params) != 3){
				//non ho i valori impostati
				$gg_dopo_dtep = 1;
				$orario_limite = 235900;
				$gg_se_carico_non_interamente_prodotto = 0;
			} else {
				$gg_dopo_dtep = $min_gg_params[0];
				$orario_limite = $min_gg_params[1];
				$gg_se_carico_non_interamente_prodotto = $min_gg_params[2];				
			}

			
			$min_date = max(date('Ymd', strtotime($sped['CSDTSP'] . " +0 days")),
							date('Ymd', strtotime(oggi_AS_date() . " +0 days")));

			//aggiungo i giorni lavorativi necessari dopo la dtep
			$min_date = $this->aggiungi_gg_lavorativi($min_date, $gg_dopo_dtep, $sped['CSDT']);
			
			//solo se sto lavorando in evasione programmata <= oggi
			if ($sped['CSDTSP'] <= oggi_AS_date()){			
				//se ho passato l'orario limite aggiungo anche un altro giorno
				$ora = oggi_AS_time();
				
				if ($ora > $orario_limite)
					$min_date = $this->aggiungi_gg_lavorativi($min_date, 1, $sped['CSDT']);
						
				//se il carico non e' interamente prodotto aggiungo anche altri gg
				if ($gg_se_carico_non_interamente_prodotto > 0){
					$tot_colli = $this->get_totale_by_sped($p['sped_id'], 'S_COLLI');
					$colli_prodotti = $this->get_totale_by_sped($p['sped_id'], 'S_COLLI_PRODOTTI');
					
					if ($colli_prodotti < $tot_colli)
						$min_date = $this->aggiungi_gg_lavorativi($min_date, $gg_se_carico_non_interamente_prodotto, $sped['CSDT']);
					
				}
			}	
			
			$min_date_by_dtep = date('Ymd', strtotime($sped['CSDTSP'] . " +{$gg_dopo_dtep} days")); 
			
			$data_minima_calcolata = max($min_date_by_dtep, $min_date);
			
			
			//al momento non eseguo controllo su spedizioni FLIGHT (TDNBOF)
			if ($sped['CSFG04'] != 'F'){
    			if ($p['f_data_spedizione'] < $data_minima_calcolata){
    				$ret['success'] = false;
    				$ret['error_log'] = "Data di spedizione minima assegnabile: " . print_date($data_minima_calcolata);
    				return $ret;				
    			}
			}
				
		}
		
		
		global $auth;
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}
						SET 
							CSUSGE = " . sql_t($auth->get_user()) . "
							, CSDTGE = " . oggi_AS_date() . "
							, CSORGE = " . oggi_AS_time() . "				 
									
							, CSCVET = " . sql_t($ar_itvet[1]) . " 
							, CSCAUT = " . sql_t($ar_itvet[2]) . "
							, CSCCON = " . sql_t($ar_itvet[3]) . "
							, CSSTSP = " . $p['f_stato'] . "			 
							, CSTISP = " . sql_t($p['f_tipologia']) . " 
							, CSDTSP = " . $p['f_data'] . " 
							, CSHMPG = " . $p['f_ora'] . "
							, CSDTIC = " . $p['f_data_inizio_carico'] . " 
							, CSHMIC = " . $p['f_ora_inizio_carico'] . "									
							, CSDTSC = " . $p['f_data_inizio_scarico'] . " 
							, CSHMSC = " . $p['f_ora_inizio_scarico'] . "									
							, CSDTPG = " . $p['f_data_spedizione'] . "									
							, CSBAND = " . sql_f($p['f_pallet_disp']) . "							
							, CSVOLD = " . sql_f($p['f_volume_disp']) . "
							, CSPESO = " . sql_f($p['f_peso_disp']) . "														
							, CSTITR = " . sql_t($p['f_trasporto']) . "
							, CSGGTR = " . sql_f($p['f_gg_trasporto']) . "
							, CSKMTR = " . sql_f($p['f_km_trasporto']) . "
							, CSCSTR = " . sql_f($p['f_costo_trasporto']) . "																					 						
							, CSCSTD = " . sql_f($p['f_costo_deposito']) . "
							, CSTARG = " . sql_t($p['f_targa']) . "							
							, CSPORT = " . sql_t($p['f_porta']) . "							
							, CSDESC = " . sql_t($p['f_descr']) . "		

							, CSSEQ1 = " . $p['f_seq_gg_scarico_1'] . "
							, CSSEQ2 = " . $p['f_seq_gg_scarico_2'] . "
							, CSSEQ3 = " . $p['f_seq_gg_scarico_3'] . "
							, CSSEQ4 = " . $p['f_seq_gg_scarico_4'] . "
							, CSSEQ5 = " . $p['f_seq_gg_scarico_5'] . "																											
							
	                        , CSNCON = " . $p['f_cointainer'] . "
							, CSNSIG = " . $p['f_sigillo'] . "	
						WHERE CSPROG = {$p['sped_id']}
				";					
										
		// la spedizione collegata viene selezionata/resettata solo attraverso le specifiche funzioni
		//	, CSNSPC = " . sql_f($p['f_sped_coll']) . "
		
		$stmt = db2_prepare($conn, $sql);
		if (!$stmt) echo "errore: sql: {$sql}";		
		$result = db2_execute($stmt);	

		//scrivo su RI
		$sh = new SpedHistory();
		$sh->crea(
				'modifica_spedizione',
				array(
						"sped_id"		=> $p['sped_id']
				)
		);

		$sped = $this->get_spedizione($p['sped_id']);
		$appLog->add_bp('UPDATE sped - END');	
		
		if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y')
			$use_session_history = microtime(true);
		else
			$use_session_history = '';
		
		//Recupero l'elenco degli ordini e aggiorno i dati in base alla spedizione
	    if ($sped['CSFG04'] == 'F')
	      $td_sped_field = 'TDNBOF';
	    else
	      $td_sped_field = 'TDNBOC';
	        
	        
		$sql = "SELECT TDDOCU 
                FROM {$cfg_mod_Spedizioni['file_testate']} 
                WHERE " . $this->get_where_std() . " AND {$td_sped_field} = ?";
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($p['sped_id']));

		while ($row = db2_fetch_assoc($stmt)) {
		    
		    //per le spedizioni flight eseguo solo il refresh dell'ordine
		    if ($sped['CSFG04'] == 'F') {		    		    
    		    //HISTORY
    		    $sh = new SpedHistory();
    		    $sh->crea(
    		        'sincronizza_ordine',
    		        array(
    		            "use_session_history" => $use_session_history,
    		            "k_ordine" 	=> $row['TDDOCU'])
    		    );
		    } else {
		    //spedizioni standard (no flight)    
		        $this->exe_allinea_dati_spedizione_su_testata($sped, $row['TDDOCU']);
		        $ord = $this->get_ordine_by_k_docu($row['TDDOCU']);
		        
		        $new_RICVES = $this->get_new_RICVES($ord);
		        
		        //HISTORY
		        $sh = new SpedHistory();
		        $sh->crea(
		            'assegna_spedizione',
		            array(
		                'use_session_history' => $use_session_history,
		                "k_ordine" 	=> $row['TDDOCU'],
		                "sped"		=> $sped,
		                "op"		=> 'UPD',
		                "ex"		=> "(DTPG: " . $p['f_data_spedizione'] . ")",
		                
		                "k_carico"	=> $this->k_carico_td_encode($ord),
		                
		                "seca"		=> $ord['TDSECA'],
		                "RIIVES"	=> $ord['TDTDES'],
		                "RICVES"	=> $new_RICVES
		                
		            )
		        );    		            
    		}
		} //while
		
		//se sto usando	use_session_history
		if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y'){
			$sh = new SpedHistory();
			$sh->rendi_attiva_sessione($use_session_history);
		}	
		
	  $appLog->add_bp('RI ordini - END');

	return $result; 		
 }


 
 function aggiungi_gg_lavorativi($da_data = 0, $add_gg = 1, $dt = '', $escludi_Sab_Dom = false){
 	global $id_ditta_default;
 	if ($da_data == 0) $da_data = oggi_AS_date();
 	if ($dt == '') $dt = $id_ditta_default; 

 	$gg_calendario = $this->get_giorni_festivi($da_data, 10000);

 	for ($i = 1; $i <= $add_gg; $i++){
 		$lavorativo = false;
 		while ($lavorativo == false){
 			$da_data = date('Ymd', strtotime($da_data . " +1 days"));
 			$d_o_w   = date('w', strtotime($da_data . " +0 days")); //giorno della settimana
 			if (
 					$gg_calendario["{$da_data}"] == 'L' &&
 					($escludi_Sab_Dom == false || ($d_o_w >=1 && $d_o_w <=5)) 
 				)
 				$lavorativo = true;
 		}

 	}
  return $da_data;	
 } 
 
 
 function exe_sincronizza_spedizione($p){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	
 	$sped = $this->get_spedizione($p->sped_id);
 
 	//Recupero l'elenco degli ordini e aggiorno i dati in base alla spedizione
 	$sql = "SELECT TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']} 
            WHERE " . $this->get_where_std() . " AND (TDNBOC = ? OR TDNBOF = ?)";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($p->sped_id, $p->sped_id));
 	
 	if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y')
 		$use_session_history = microtime(true);
 	else
 		$use_session_history = ''; 	
 
 	while ($row = db2_fetch_assoc($stmt)) {

 			$ord = $this->get_ordine_by_k_docu($row['TDDOCU']);
 
 		//HISTORY
 		$sh = new SpedHistory();
 		$sh->crea(
 				'sincronizza_spedizione',
		 		array(
		 			"use_session_history" => $use_session_history,
 					"k_ordine" 	=> $row['TDDOCU'],
 					"sped"		=> $sped 								
					)
 		);
 	}
 
 	//se sto usando	use_session_history
 	if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y'){
 		$sh = new SpedHistory();
 		$sh->rendi_attiva_sessione($use_session_history);
 	} 	
 
 	return $result;
 }
 
 

 function exe_sincronizza_ordine($p){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$ord = $p->k_ordine;
 	

 	//Recupero l'elenco degli ordini e aggiorno i dati in base alla spedizione
 	$sql = "SELECT TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']} WHERE TDDOCU= '{$ord}' AND " . $this->get_where_std() ;
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 
 	if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y')
 		$use_session_history = microtime(true);
	else
		$use_session_history = '';
	
		
 
 			while ($row = db2_fetch_assoc($stmt)) {
 			
               
               //HISTORY
 				$sh = new SpedHistory();
 				$sh->crea(
 						'sincronizza_ordine',
 						array(
 								"use_session_history" => $use_session_history,
 								"k_ordine" 	=> $ord
 								
 						)
 						);
 			}
 
 			//se sto usando	use_session_history
 			if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y'){
 				$sh = new SpedHistory();
 				$sh->rendi_attiva_sessione($use_session_history);
 			}
 
 			return $result;
 }
 
 
 function exe_sincronizza_hold($p){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	
 	$cod_iti 	= $p->cod_iti;
 	$k_cli_des 	= $p->k_cli_des;
 	$k_cli_des_exp = explode("_", $k_cli_des);
 
 	//Recupero l'elenco degli ordini
 	$sql = "SELECT TDDOCU FROM {$cfg_mod_Spedizioni['file_testate']} WHERE TDSWSP = 'N' 
 			AND TDCITI = ? AND TDDT = ? AND TDCCON = ? AND TDCDES = ?";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array(trim($cod_iti), trim($k_cli_des_exp[0]), trim($k_cli_des_exp[1]), trim($k_cli_des_exp[2])));
 	
 	while ($row = db2_fetch_assoc($stmt)) {

 		$ord = $this->get_ordine_by_k_docu($row['TDDOCU']);
 
 		//HISTORY
 		$sh = new SpedHistory();
 		$sh->crea(
 				'sincronizza_spedizione',
 				array(
 						"k_ordine" 	=> $row['TDDOCU'],
 						"sped"		=> $sped
 				)
 		);
 	}
 
 
 	return $result;
 }
  
 

 	
 	
 	
 	function exe_forza_invio_conferma_ordine($p){
 		global $conn;
 		global $cfg_mod_Spedizioni;
 	
 	
 			//HISTORY
 			$sh = new SpedHistory();
 			$sh->crea(
 					'forza_invio_conferma',
 					array(
 							"k_ordine" 	=> $p->list_selected_id,
 							"sped"		=> $sped
 					)
 			);

 		$ret['success'] = true; 	
 	
 		return acs_je($ret);
 	}
 	
 	
 	
 	
 
 
 
 
 
 function get_new_RICVES($ord){
 	if ($ord['TDTDES'] == '1') return $ord['TDVET1'];
 	if ($ord['TDTDES'] == '2') return $ord['TDVET2'];
 	return ''; 	
 }
 

 function upd_carico($p){ 	
 		global $conn;
		global $cfg_mod_Spedizioni;
		
		$this->exe_upd_commento_carico($p, 'CAR');
		
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_carichi']}
						SET PSDESC = ? 
						  , PSSTAT = " . sql_t($p['f_stato']) . "
						  , PSFG01 = " . sql_t($p['f_paga']) . "
						  , PSFG02 = " . sql_t($p['f_seq_in_sped']) . "
						  , PSNOTE = ?						  
						  , PSCOEX = " . sql_t($p['f_cod_est']) . "		
						  , PSPPAC = " . sql_f($p['f_perc_pag']) . "	
						WHERE PSDT = '{$p['dt']}'
						  AND PSTPCA = '{$p['tp_ca']}'
						  AND PSAACA = {$p['aa_ca']}
						  AND PSNRCA = {$p['nr_ca']}						  						  
				";
															
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt, array(utf8_decode($p['f_descrizione']), utf8_decode($p['f_note'])));				
	
	return $result; 		
 }
 
 public function exe_upd_commento_carico($p, $bl){
     global $conn;
     global $cfg_mod_Spedizioni;
 
     //elimino il vecchio commento
     $sql = "DELETE
     FROM {$cfg_mod_Spedizioni['file_righe']}
     WHERE RDDT=" . sql_t($p['dt']) . " 
				  AND RDOADO=" . sql_t($p['aa_ca'])  . " AND RDONDO=" . sql_t($p['nr_ca'])  . "
				  AND RDTPNO=" . sql_t($bl) . "
			";
     
     $stmt = db2_prepare($conn, $sql);
     $result = db2_execute($stmt);
     
     //divido il testo in blocchi da 80 caratteri e creo i record
     //$str = str_split($p['f_text'], 80);
     $str = array();
     if (strlen($p["f_text_0"]) > 0) $str[] = $p["f_text_0"];
     if (strlen($p["f_text_1"]) > 0) $str[] = $p["f_text_1"];
     if (strlen($p["f_text_2"]) > 0) $str[] = $p["f_text_2"];
     if (strlen($p["f_text_3"]) > 0) $str[] = $p["f_text_3"];
     if (strlen($p["f_text_4"]) > 0) $str[] = $p["f_text_4"];
     if (strlen($p["f_text_5"]) > 0) $str[] = $p["f_text_5"];
   
     $cont_riga = 0;
     $ar_values = array();
     $ar_values[0] = sql_t($p['dt']);
     $ar_values[1] = sql_t($p['aa_ca']);
     $ar_values[2] = sql_t($p['nr_ca']);
     $ar_values[3] = sql_t($bl);
     
     $c=0;
     foreach($str as $t){
         $ar_values[4] = ++$c;
         $ar_values[5] = sql_t($t);
         $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_righe']}(RDDT, RDOADO, RDONDO, RDTPNO, RDRINO, RDDES1)
         VALUES(" . implode(', ', $ar_values) . ")";
         $stmt = db2_prepare($conn, $sql);
         $result = db2_execute($stmt);
     }
     

 }
 
 public function get_commento_carico($p, $bl){
     global $conn;
     global $cfg_mod_Spedizioni;
    
         
         $sql = "SELECT *
         FROM {$cfg_mod_Spedizioni['file_righe']}
         WHERE RDDT=" . sql_t($p['PSDT']). " AND RDOADO=" . sql_t($p['PSAACA'])  . " AND RDONDO=" . sql_t($p['PSNRCA'])  . "
				  AND RDTPNO=" . sql_t($bl);
         
         $sql .= "ORDER BY RDRINO";
       
         $stmt = db2_prepare($conn, $sql);
         $result = db2_execute($stmt);
         
         $ret = array();
         while ($row = db2_fetch_assoc($stmt)) {
             $ret[] = $row['RDDES1'];
         }
         
         return $ret;
 }


function exe_allinea_dati_spedizione_su_testata($sped, $tddocu){ 	
 		global $conn;
		global $cfg_mod_Spedizioni;
		
		
		if ($sped['CSFG04'] == 'F') {
		    //non aggiorno niente se e' una spedizione 'FLIGHT'
		    return true;
		}
		
    		//Se  ho indicato la data di spedizione nella spedizione
    		if (!$sped['CSDTPG'] == 0) {
    				$new_TDDTSP = $sped['CSDTPG'];
    				$new_TDDTCP = $sped['CSDTPG'];				
    		} else { //lascio i valori invariati
    			$new_TDDTSP = 'TDDTSP';
    			$new_TDDTCP = 'TDDTCP';			
    		}
		
			$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET 
						/*, TDSWSP = 'Y' ??????? */
						  TDDTEP = " . $sped['CSDTSP'] . "			
						, TDCITI = " . sql_t(trim($sped['CSCITI'])) . "						
						, TDVETT = " . sql_t(trim($sped['CSCVET'])) . "
						, TDAUTO = " . sql_t(trim($sped['CSCAUT'])) . "						
						, TDCOSC = " . sql_t(trim($sped['CSCCON'])) . "
						, TDDTSP = " . $new_TDDTSP . "
						, TDDTCP = " . $new_TDDTCP . "								
					WHERE " . $this->get_where_std() . " AND TDDOCU = " . sql_t($tddocu) . "
			";
			
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);			

	return $result; 		
 }

 

 function exe_modifica_dati_destinazione($p){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	
 			if (!isset($p['taid']) || strlen($p['taid']) == 0)
 				$p['taid'] = 'VUDE';
 	
 			//recupero record da modificare
 			$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_anag_cli_des']}  						
 						WHERE TAID = " . sql_t($p['taid']) . " AND TADT = ? AND TACOR1 = ? AND TANR = ?";
 	
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt, array($p['dt'], $p['cod_cli'], $p['cod_des'])); 			
 			$row_dest = db2_fetch_assoc($stmt);
 			
 			//tarest:
 			//   1 ->  30 --- (IGNORARE)
 			//  31 ->  90 --- ind1 + ind2
 			//  91 -> 150 --- loc1 + loc2
 			// 151 -> 160 --- cap
 			// 161 -> 162 --- prov
 			// 163 -> 173 --- (IGNORARE)
 			// 174 -> 193 --- telefono
 			// 194 -> 248 --- (IGNORARE)

 			//Costruisco i valori per i parametri
 			$upd_p = array();
 			$upd_p['TAUSUM'] = substr($auth->get_user(), 0, 8);
 			$upd_p['TADTUM'] = oggi_AS_date();
 			$upd_p['TADESC'] = sprintf("%-30s", substr($p['denom1'],  0,  30));
 			$upd_p['TADES2'] = sprintf("%-30s", substr($p['denom2'],   0,  30));
 			
 			//TAREST
 			$upd_p['TAREST'] =   substr($row_dest['TAREST'], 0, 30) 
 							 . sprintf("%-30s", $p['ind1']) . sprintf("%-30s", $p['ind2'])
 							 . sprintf("%-30s", $p['loc1']) . sprintf("%-30s", $p['loc2'])
 							 . sprintf("%-10s", $p['cap'])
 							 . sprintf("%-2s",  $p['pro'])
 							 . substr($row_dest['TAREST'], 162, 11)
 							 . sprintf("%-20s",  $p['tel'])
 							 . substr($row_dest['TAREST'], 193);
 	
		 	$sql = "UPDATE {$cfg_mod_Spedizioni['file_anag_cli_des']} SET
		 				  TADESC = ?, TADES2 = ?, TAREST = ?
		 			WHERE TAID = " . sql_t($p['taid']) . " AND TADT = ? AND TACOR1 = ? AND TANR = ?";
		 	
		 	
		 	$stmt = db2_prepare($conn, $sql);
		 	$result = db2_execute($stmt, array($upd_p['TADESC'], $upd_p['TADES2'], utf8_decode($upd_p['TAREST']),
		 									   $p['dt'], $p['cod_cli'], $p['cod_des']));
		 	

 	if ($p['taid'] == 'VUDS'){
 		//REGISTRO VARIAZIONE sosta tecnica
 		$sh = new SpedHistory();
 		$sh->crea(
 				'variazione_destinazione_sosta_tecnica',
 				array("p" 	=> $p)
 		);
 		
 		//TODO: devo modificare le testate?
 	}
		 	
		 	
		 	
if ($p['taid'] == 'VUDE'){		 	
 			//REGISTRO VARIAZIONE
 			$sh = new SpedHistory();
 			$sh->crea(
 					'variazione_destinazione',
 					array("p" 	=> $p)
 			);
		 	
 	
 	
		 	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
 						  TDINDI = ?
 						, TDCAP  = ?
						, TDLOCA = ?
 						, TDPROV = ?
 						WHERE TDDT = ? AND TDCCON = ? AND TDCDES = ?";
		 	
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt, array($p['ind1'] . $p['ind2'] , $p['cap'], $p['loc1'] . $p['loc2'], $p['pro'],
 												$p['dt'], $p['cod_cli'], $p['cod_des']));
 			
 			//aggiorno anche la destinazione dove TDTDES = 'D'
 			$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
 						  TDDDES = ?
 						, TDIDES = ?
 						, TDDCAP = ?
 						, TDDLOC = ?
 						, TDPROD = ?
 					WHERE TDDT = ? AND TDCCON = ? AND TDCDES = ? AND TDTDES = 'D'";
 			
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt, array( $p['denom1'] . $p['denom2'], $p['ind1'] . $p['ind2'], $p['cap'], $p['loc1'] . $p['loc2'], $p['pro'],
 												$p['dt'], $p['cod_cli'], $p['cod_des']));

 			if (!$result)
 				echo db2_stmt_errormsg();
} 			
 			

        //NUOVA VERSIONE ATTRAVERSO RI
        $sh = new SpedHistory();
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'EXP_ANAG_CLI',
                "vals" => array(
                    "RICLIE" => $p['cod_cli']
                )
            )
        );


 	$ret = array("success" => true);		
 	return acs_je($ret);
 } 
 

 
 
 
 
 
 function exe_crea_dati_destinazione($p){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	if (!isset($p['taid']) || strlen($p['taid']) == 0)
 		$p['taid'] = 'VUDE';

 	$row_dest = array();
 	$row_dest['TAREST'] = str_repeat(" ", 248);
 
 	//tarest:
 	//   1 ->  30 --- (IGNORARE)
 	//  31 ->  90 --- ind1 + ind2
 	//  91 -> 150 --- loc1 + loc2
 	// 151 -> 160 --- cap
 	// 161 -> 162 --- prov
 	// 163 -> 173 --- (IGNORARE)
 	// 174 -> 193 --- telefono
 	// 194 -> 248 --- (IGNORARE)
 
 	//Costruisco i valori per i parametri
 	$upd_p = array();
 	$upd_p['TADESC'] = sprintf("%-30s", substr($p['denom1'],  0,  30));
 	$upd_p['TADES2'] = sprintf("%-30s", substr($p['denom2'],   0,  30));
 
 	//TAREST
 	$upd_p['TAREST'] =   substr($row_dest['TAREST'], 0, 30)
 	. sprintf("%-30s", $p['ind1']) . sprintf("%-30s", $p['ind2'])
 	. sprintf("%-30s", $p['loc1']) . sprintf("%-30s", $p['loc2'])
 	. sprintf("%-10s", $p['cap'])
 	. sprintf("%-2s",  $p['pro'])
 	. substr($row_dest['TAREST'], 162, 11)
 	. sprintf("%-20s",  $p['tel'])
 	. substr($row_dest['TAREST'], 193);
 
 	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_cli_des']}(TADT,TAID,TACOR1,TANR,TADESC,TADES2,TAREST)
 			VALUES(?,?,?,?,?,?,?)";
 			//TADESC = ?, TADES2 = ?, TAREST = ?
 			//WHERE TAID = " . sql_t($p['taid']) . " AND TADT = ? AND TACOR1 = ? AND TANR = ?";
 
 
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg(); 	
 	$result = db2_execute($stmt, array(
 			$p['dt'], $p['taid'], $p['cod_cli'], $p['cod_des'],
 			$upd_p['TADESC'], $upd_p['TADES2'], utf8_decode($upd_p['TAREST']),
 			));
 	
 	echo db2_stmt_errormsg($stmt); 
 	$ret = array("success" => true);
 	return acs_je($ret);
 }
 
 
 
 
 function get_stati_non_ammessi($tab, $takey2){
     
     $ta_std = find_TA_std($tab, null, 1, $takey2);
     
     $ret = array();
     foreach($ta_std as $v){
         $ret[] = trim($v['id']);
         
     }
     
     return $ret;
     
     
 }
 



function exe_assegna_spedizione($r){ 	
        global $conn, $cfg_mod_Spedizioni;
        
        $to_sped_id = $r->to_sped_id;
        $sped = $this->get_spedizione($to_sped_id);
        
        //recupero (dall'itinerario attuale) l'area di spedizione attuale
        $itin_attuale = $this->get_TA_std('ITIN', $sped['CSCITI']);
        $area_attuale_cod = trim($itin_attuale['TAASPE']);
        
   		
 		//controllo stati ammessi
 		$ret = array();
 		$car_exp = explode("_", $r->to_carico);
 		$tp_ca = $car_exp[1];
 		$aa_ca = $car_exp[0];
 		$nr_ca = $car_exp[2];
 		if($nr_ca != 0){
 		    $car= $this->get_carico($tp_ca, $aa_ca, $nr_ca);
      		$stato_carico = $car['PSSTAT'];
      		
      		$ar_stati_carico_na = $this->get_stati_non_ammessi('STCNA', $area_attuale_cod);
      		if(in_array($stato_carico, $ar_stati_carico_na)){
      		    
      		    $list = array();
      		    foreach($ar_stati_carico_na as $k => $v){
      		        $list[] = $v;
      		    }
      		    
      		    $stati_list = implode(', ', $list);
      		    
      		    $ret['success'] = false;
      		    $ret['message'] = "Stato carico non ammesso (non ammessi gli stati {$stati_list})";
      		    return $ret;
      		}
 		}
		
		
		$list_selected_id = $r->list_selected_id;

		$ar_carico = $this->k_carico_td_decode($r->to_carico);

		if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y')
			$use_session_history = microtime(true);
		else
			$use_session_history = '';
		
		
		foreach ($list_selected_id as $k => $k_ord){
			$ord = $this->get_ordine_by_k_ordine($k_ord);
			
			if ((int)$ar_carico['TDNRCA'] > 0)
				$tdfn01 = 1;
			else
				$tdfn01 = 0;
	
			if (trim($ord['TDSWSP']) == 'N' || $r->richiesta_confermaData=='Y') { //da hold ... confermo subito la data
				$new_TDDTVA = $sped['CSDTSP'];
				$new_TDFN06 = 1;
				$registra_data_confermata = 'Y';				
			} else {
				//altrimenti lascio quella precedentemente confermata dall'operatore
				$new_TDDTVA = $ord['TDDTVA'];
				$new_TDFN06 = $ord['TDFN06'];	
				$registra_data_confermata = 'N';
			}

			//verifico se abbinare la sequenza e/o deposito (in base a sped/carico/cliente/dest)
			$ar_upd = $this->verifica_abbinamento_nel_carico($ord, $to_sped_id, $ar_carico);

			$sql_set = "";
			//gestione data di spedizione
			if ($sped['CSDTPG'] > 0){
				$sql_set .= " , TDDTSP = {$sped['CSDTPG']}";
				$sql_set .= " , TDDTCP = {$sped['CSDTPG']}";				
			}
			
			
			//Mantieni allineato TDNBOC|TDNBOF (o riallinea se azzero carico)
			if ($ord['TDNBOC'] == $ord['TDNBOF'] || $tdfn01 == 0)
			    $sql_allinea_TDNBOF = ", TDNBOF = {$to_sped_id}";
			else 			    
			    $sql_allinea_TDNBOF = "";
			    
		    if ($tdfn01 == 1) {
                $esegui_sincronizza = false;
			    //assegno ad un carico:
			    //il TDNBOF lo prendo dal primo ordine della stessa spedizione/carico (andranno via di sicuro insieme) 			    
			    $sql_spe_ca = "SELECT TDNBOF FROM  {$cfg_mod_Spedizioni['file_testate']}
						 WHERE " . $this->get_where_std() . "
						 AND TDNBOC = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ?";
			    
			    $stmt_spe_ca = db2_prepare($conn, $sql_spe_ca);
			    $result = db2_execute($stmt_spe_ca, array($to_sped_id,
			        $ar_carico['TDTPCA'], $ar_carico['TDAACA'], $ar_carico['TDNRCA']));
			    
			    $row_spe_ca = db2_fetch_assoc($stmt_spe_ca);

			    if ($row_spe_ca) { //Se trovato associo TDNBOF
			        $sql_allinea_TDNBOF = ", TDNBOF = {$row_spe_ca['TDNBOF']}";
			        $esegui_sincronizza = true;
			    }
		    }			
			
			$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET 
						  TDNBOC = " . $to_sped_id . "
						  {$sql_allinea_TDNBOF}  
						, TDSWSP = 'Y' /* TODO: e' giusto?? */
						, TDTPCA = " . sql_t($ar_carico['TDTPCA']) . "
						, TDAACA = " . $ar_carico['TDAACA'] . "
						, TDNRCA = " . $ar_carico['TDNRCA'] . "
						, TDFN01 = $tdfn01
						, TDDTVA = $new_TDDTVA
						, TDFN06 = $new_TDFN06
						
						, TDSECA = ?, TDTDES = ?, TDVET1 = ?, TDVET2 = ?, TDDDES = ?, TDIDES = ?, TDDCAP = ?, TDDLOC = ?, TDPROD = ?						

						, TDDTEP = " . $sped['CSDTSP'] . "			
						, TDCITI = " . sql_t(trim($sped['CSCITI'])) . "						
						, TDVETT = " . sql_t(trim($sped['CSCVET'])) . "
						, TDAUTO = " . sql_t(trim($sped['CSCAUT'])) . "						
						, TDCOSC = " . sql_t(trim($sped['CSCCON'])) .  $sql_set . "						
						
					WHERE " . $this->get_where_by_k_ordine($k_ord) . "
			";

			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt, array($ar_upd['TDSECA'], $ar_upd['TDTDES'], $ar_upd['TDVET1']
				,$ar_upd['TDVET2'], $ar_upd['TDDDES'], $ar_upd['TDIDES']
				,$ar_upd['TDDCAP'], $ar_upd['TDDLOC'], $ar_upd['TDPROD']));
			
			
			if (strlen(trim($r->f_nuovo_stato_ordine)) > 0){
				$nuovo_stato_ordine_ar = explode("|", $r->f_nuovo_stato_ordine . '');
				$nuovo_stato_ordine = trim($nuovo_stato_ordine_ar[1]);
			} else $nuovo_stato_ordine = '';
			
			$sh = new SpedHistory();
			$sh->crea(
					'assegna_spedizione',					
					array(
							'use_session_history' => $use_session_history,							
							"k_ordine" 	=> $k_ord,
							"data"		=> $sped['CSDTSP'],
							"k_carico"	=> $r->to_carico,
							"nuovo_stato_ordine" => $nuovo_stato_ordine,
							
							"seca"		=> $ar_upd['TDSECA'],
							"RIIVES"	=> $ar_upd['RIIVES'],
							"RICVES"	=> $ar_upd['RICVES'],
																					 
							"sped"		=> $sped,
							"registra_data_confermata" => $registra_data_confermata,
							"data_confermata" => $sped['CSDTSP'],
							"op"		=> "ASS",
							"ex"		=> "(#{$ord['TDNBOC']},C:{$ord['TDNRCA']},Sw:{$ord['TDSWSP']})" 
					)
			);
			
			if ($esegui_sincronizza == true){
			    $sh = new SpedHistory();
			    $sh->crea(
			        'sincronizza_ordine',
			        array(
			            "use_session_history" => $use_session_history,
			            "k_ordine" 	=> $k_ord)
			        );
			}
				
				
				
			if ($r->f_data > 0){
				//memorizzo la autorizzazione deroga
				$sh = new SpedAutorizzazioniDeroghe();
				$sh->crea(
						'assegna_spedizione',
						array(
								"k_ordine" 		=> $k_ord,
								"data"			=> $r->f_data,
								"commento"		=> $r->f_commento,
								"autorizzazione"=> $r->f_autorizzazione,
								"data_prog"		=> $r->f_data_programmata,
								"data_disp"		=> $r->f_data_disponibilita
						)
				);			
			}
			
			
		}

		
	//se sto usando	use_session_history
	if ($cfg_mod_Spedizioni['force_use_session_history'] == 'Y'){	
		$sh = new SpedHistory();
		$sh->rendi_attiva_sessione($use_session_history);
	}
	
	
	$ret['success'] = true;
	return $ret; 		
 }


 
 
	//verifico se abbinare la sequenza e/o deposito (in base a sped/carico/cliente/dest) 
 	function verifica_abbinamento_nel_carico($ord, $to_sped_id, $ar_carico){
 			global $conn, $cfg_mod_Spedizioni;
			$ar_upd = array();						
			
			$sql_seca = "SELECT * FROM  {$cfg_mod_Spedizioni['file_testate']}
						 WHERE " . $this->get_where_std() . "
						 AND TDNBOC = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ?
						 AND TDCCON = ? AND TDCDES = ? AND TDDOCU <> ?
					";
			
			$stmt_seca = db2_prepare($conn, $sql_seca);
			$result = db2_execute($stmt_seca, array($to_sped_id,
								$ar_carico['TDTPCA'], $ar_carico['TDAACA'], $ar_carico['TDNRCA'],
								$ord['TDCCON'], $ord['TDCDES'], $ord['TDDOCU']));
									
			$row_seca = db2_fetch_assoc($stmt_seca);
			$new_seca = '';

			if ($row_seca != false){				
				$ar_upd['TDSECA'] = $row_seca['TDSECA'];

				$ar_upd['TDTDES'] = $row_seca['TDTDES'];
				
				$ar_upd['TDVET1'] = $row_seca['TDVET1'];					
				$ar_upd['TDVET2'] = $row_seca['TDVET2']; 

				$ar_upd['TDDDES'] = $row_seca['TDDDES'];
				$ar_upd['TDIDES'] = $row_seca['TDIDES'];
				$ar_upd['TDDCAP'] = $row_seca['TDDCAP'];
				$ar_upd['TDDLOC'] = $row_seca['TDDLOC'];
				$ar_upd['TDPROD'] = $row_seca['TDPROD'];

				$ar_upd['RIIVES'] = $row_seca['TDTDES'];

				if ($row_seca['TDTDES'] == '1')		
					$ar_upd['RICVES'] = $row_seca['TDVET1'];
				elseif ($row_seca['TDTDES'] == '2')
					$ar_upd['RICVES'] = $row_seca['TDVET2'];
				else $ar_upd['RICVES'] = '';
					 
			} else {				
				//ordine non abbinale per sequenza
				
				//se non cambio il carico (es: provengo da Assegna Spedizione su riga carico,  mantengo la sequenza attuale)
				if ($ar_carico['TDNRCA'] > 0 && $this->k_carico_td_encode($ar_carico) == $this->k_carico_td_encode($ord))
					$ar_upd['TDSECA'] = $ord['TDSECA']; //TDSECA mantengo attuale
				else				
					$ar_upd['TDSECA'] = ''; //TDSECA azzero sempre la sequenza
				
				$ar_upd['TDTDES'] = $ord['TDTDES'];			
				$ar_upd['TDVET1'] = $ord['TDVET1'];
				$ar_upd['TDVET2'] = $ord['TDVET2'];					
				$ar_upd['TDDDES'] = $ord['TDDDES'];
				$ar_upd['TDIDES'] = $ord['TDIDES'];
				$ar_upd['TDDCAP'] = $ord['TDDCAP'];
				$ar_upd['TDDLOC'] = $ord['TDDLOC'];
				$ar_upd['TDPROD'] = $ord['TDPROD'];			

				$ar_upd['RIIVES'] = $ord['TDTDES'];
				if ($ord['TDTDES'] == '1')				
					$ar_upd['RICVES'] = $ord['TDVET1'];
				elseif ($ord['TDTDES'] == '2')
					$ar_upd['RICVES'] = $ord['TDVET2'];
				else $ar_upd['RICVES'] = '';
			}
	return $ar_upd;		 	
 }
 
 
 
 

 
 function exe_crea_segnalazione_arrivi($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;

 	$ret = array();
 	$ret['success'] = true;
 	
 	//bug parametri linux
 	global $is_linux;
 	if ($is_linux == 'Y')
 		$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\')); 	

 	//memorizzo ultimo utente assegnato per tipo di operazione
 	$_SESSION['ultimo_utente_assegnato_' . trim($r['f_causale'])] = $r['f_utente_assegnato'];
 	
 	
 	$ar_ordini = array();
 	$list_selected_id = json_decode($r['list_selected_id']);
 	
 	$ar_ord = array();
 	foreach ($list_selected_id as $ar_selected_AS){

 		//recupero l'ordine selezionato
 		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']}
 				WHERE " . $this->get_where_std() . " AND TDDOCU = " . sql_t($ar_selected_AS->k_ordine) . " ";
 		
 		$stmt = db2_prepare($conn, $sql);
 		$result = db2_execute($stmt);
 		
 		
 		while ($row = db2_fetch_assoc($stmt)) {
 		//creo la riga in WPI0AS0
 			$na = new SpedAssegnazioneOrdini();
 			$na->crea('POSTM', array(
 			'ordine' => $row,
 			'as_selected' => $ar_selected_AS,
 			'form_values' => $r
 			));
 		}
 		
 	}
 	
 	
 	/*
 	//recupero l'elenco degli ordini selezionati 	
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} 
 			WHERE " . $this->get_where_std() . " AND TDDOCU IN (" . sql_t_IN($ar_ord) . ") ";
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	 	
 	while ($row = db2_fetch_assoc($stmt)) {
 		//creo la riga in WPI0AS0 		
 		$na = new SpedAssegnazioneOrdini();
 		$na->crea('POSTM', array(
 				'ordine' => $row,
 				'form_values' => $r
 		)); 	 	
 	}
 	*/
 	
 	 	
 	return acs_je($ret);
 }
 
 

 
 
 function exe_crea_segnalazione_arrivi_json($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$ret = array();
 	$ret['success'] = true;
 
 	$ar_ordini = array();
 	$list_selected_id = $r->list_selected_id;
 
 	$ar_ord = array();
 	foreach ($list_selected_id as $ar_selected_AS){
 
 	  if ($r->delete_if_exists == 'Y') {
 	  	//utilizzato nelle check list (SBLOC)
 	  	//Se gia' esiste la causale per l'ordine (rilasciata) la rimuovo
 	  	$sql = "SELECT COUNT(*) AS R_COUNT FROM {$cfg_mod_Spedizioni['file_assegna_ord']}
 	  					WHERE ASDOCU = " . sql_t($ar_selected_AS->k_ordine) . "
 	  				      AND ASCAAS = " . sql_t($r->f_causale) . "
 	  				      AND ASFLRI = 'Y'	";
 	  	
 	  	$stmt = db2_prepare($conn, $sql);
 	  	$result = db2_execute($stmt);
 	  	
 	  	$row = db2_fetch_assoc($stmt);
 	  	if ($row['R_COUNT'] > 0) {
 	  		

 	  		$sql = "DELETE FROM {$cfg_mod_Spedizioni['file_assegna_ord']}
 	  					  WHERE ASDOCU = " . sql_t($ar_selected_AS->k_ordine) . "
 	  				      AND ASCAAS = " . sql_t($r->f_causale) . "
 	  				      AND ASFLRI = 'Y' ";
 	  		$stmt = db2_prepare($conn, $sql);
 	  		$result = db2_execute($stmt); 	  		
 	  		
 	  		$ret['message'] = "Check list gia' presente. Elimino."; 	  			  	 
 	  		continue;
 	  	}
 	  }
 	  		
 		//recupero l'ordine selezionato
 		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']}
 				WHERE " . $this->get_where_std() . " AND TDDOCU = " . sql_t($ar_selected_AS->k_ordine) . " ";
 			
 		$stmt = db2_prepare($conn, $sql);
 		$result = db2_execute($stmt);
 			
 		while ($row = db2_fetch_assoc($stmt)) {
 			//creo la riga in WPI0AS0
 			$na = new SpedAssegnazioneOrdini();
 			$na->crea('POSTM', array(
 					'ordine' => $row,
 					'as_selected' => $ar_selected_AS,
 					'form_values' => (array)$r
 			));
 		}
 		
 			
 	}
 
 	return acs_je($ret);
 }
 
 
 
 
 
 
 function exe_avanzamento_segnalazione_arrivi($r){
 	global $conn;
 	global $cfg_mod_Spedizioni, $id_ditta_default, $is_linux;
 
 	$ret = array();
 	
 	if ($is_linux == 'Y')
 	    $r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
 	
 	$list_selected_id = json_decode($r['list_selected_id']);
  	
 	//per ogni riga selezionata
 	foreach($list_selected_id as $krow_selected => $row_selected){
 	
	 	//$row_selected = $list_selected_id[0];
	 	
	 	//recupero causale/utente/ordine da usare come chiave per AS0
 	    if(isset($row_selected->id)){
	 	     $m_k_exp = explode("|", $row_selected->id);
	 	     $tddocu = $m_k_exp[2];
 	    } else{
 	        
 	        $tddocu = $row_selected->k_ordine;
 	    }
	 	
	 	//recupero l'ordine selezionato
	 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']}
	 	WHERE TDDT = '{$id_ditta_default}' AND TDDOCU = '{$tddocu}'";
	 	
	 	
	 	$stmt = db2_prepare($conn, $sql);
	 	$result = db2_execute($stmt);
	 	$row = db2_fetch_assoc($stmt);
	 	
		 	//aggiorno la riga in WPI0AS0
	 		$na = new SpedAssegnazioneOrdini();
	 		$na->avanza($r['f_entry_prog_causale'], array(
	 				'prog' => $row_selected->prog,
	 		        'ordine' => $row,
	 				'form_values' => $r
	 		));
	
 	}		
 
 	$ret['success'] = true;
 	return acs_je($ret);
 }
 
 
 function exe_avanzamento_segnalazione_arrivi_json($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$ret = array();
 	$ret['success'] = true;
 
 	$list_selected_id = $r->list_selected_id;
 
 	//per ogni riga selezionata
 	foreach($list_selected_id as $krow_selected => $row_selected){
 
 		//$row_selected = $list_selected_id[0];
 		 
 		//recupero causale/utente/ordine da usare come chiave per AS0
 		$m_k_exp = explode("|", $row_selected->id);
 
 		//aggiorno la riga in WPI0AS0
 		$na = new SpedAssegnazioneOrdini();
 		$na->avanza($r->f_entry_prog_causale, array(
 				'prog' => $row_selected->prog,
 				'form_values' => (array)$r
 		));
 
 	}
 
 	return acs_je($ret);
 }
 
 
 

 function exe_create_RIPRO($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$ret = array();
 	$ret['success'] = true;
 
 	$list_selected_id = json_decode($r['list_selected_id']);

 	foreach ($list_selected_id as $selected_id){
 		
 		if (trim($selected_id != '')) {
 			
	 		//creo la riga nel file delle deroghe
			$sh = new SpedAutorizzazioniDeroghe();
			$sh->crea(
					'RIPRO',
					array(
							"k_ordine" 		=> $selected_id,
							"causale"		=> $r['f_causale'],
							"descrizione"	=> $r['f_desc'],
							"segnalazioni"	=> $r['f_segn'],
							"data_atp"		=> $r['f_data'],
							"blocco"		=> $r['f_bloc']
					)
			);
 		}	
 		
 	}
 
 	return acs_je($ret);
 }
 
 
 
 
 
 
 
 

 function exe_assegna_scarico_intermedio($r){
 	global $conn;
 	global $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$list_selected_id = json_decode($r->list_selected_id);
 	
 	$to_trasportatore = $r->to_trasportatore;

 	if ($to_trasportatore != 'RIPRISTINO'){ 	
	 	//recupero i dati del trasportatore
	 	$trasportatore = new Trasportatori();
	 	$trasportatore->load_rec_data_by_k(array('TAKEY1' => $to_trasportatore));
	 	$to_trasportatore_RI = $to_trasportatore;
	 	
	 	//se ho il codice destinazione, devo recuperare le info dalla VUDE	 	
	 	if (isset($r->to_trasportatore_cod_des) && strlen(trim($r->to_trasportatore_cod_des)) > 0){
	 	    $sql = "SELECT *
                    FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA_VUDE
     	            WHERE TA_VUDE.TADT = '{$id_ditta_default}' AND TA_VUDE.TAID = 'VUDE'
                    AND TA_VUDE.TACOR1 = ? AND TA_VUDE.TANR = ? ";
	 	    $stmtVude = db2_prepare($conn, $sql);
	 	    echo db2_stmt_errormsg();
	 	    $result = db2_execute($stmtVude, array($r->to_trasportatore, $r->to_trasportatore_cod_des));
	 	    echo db2_stmt_errormsg($stmtVude);
	 	    $rowVude = db2_fetch_assoc($stmtVude);
	 	}
	 	
	 	
 	} 	else {
 		$to_trasportatore_RI = '';
 	}
 
 	//ho selezionato un insieme di liv2 del plan
 	foreach ($list_selected_id as $sel){

 		$stmt = $this->get_stmt_ordini_by_liv2_plan($sel);

 		//per ogni ordine
 		while ( $row = db2_fetch_assoc($stmt) ){
 			$ar_p = array();
 			$ar_p['TDTDES'] = $row['TDTDES'];
 			$ar_p['TDVET1'] = $row['TDVET1'];
 			$ar_p['TDVET2'] = $row['TDVET2']; 			 			

 			$k_ord = $this->k_ordine_td($row);
 			
 			
 			if ($to_trasportatore != 'RIPRISTINO'){ 
 			    
	 			switch (trim($row['TDTDES'])){
	 				case '1':
						$ar_p['TDVET1'] = trim($to_trasportatore);					
	 					break;
	 				case '2':
	 					$ar_p['TDVET2'] = trim($to_trasportatore); 					
	 					break; 					
	 				default:
	 					if (strlen(trim($row['TDVET1'])) == 0){
	 						$ar_p['TDTDES'] = '1';
	 						$ar_p['TDVET1'] = trim($to_trasportatore); 						
	 					}	else {
	 						$ar_p['TDTDES'] = '2';
	 						$ar_p['TDVET2'] = trim($to_trasportatore); 						
	 					} 					
	 			}
	 			
	 			$ar_p['TDDDES'] = trim($trasportatore->rec_data['TADESC']);
	 			
	 			if (isset($r->to_trasportatore_cod_des) && strlen(trim($r->to_trasportatore_cod_des)) > 0){
	 			    //da vude
	 			    $ar_p['TDAUX3']	= trim($r->to_trasportatore_cod_des);
	 			    $ar_p['TDIDES']	= acs_u8e(substr($rowVude['TAREST'],  30,  60));
	 			    $ar_p['TDDLOC']	= acs_u8e(substr($rowVude['TAREST'],  90,  60));
	 			    $ar_p['TDPROD']	= substr($rowVude['TAREST'], 160, 2);
	 			    $ar_p['TDDCAP']	= substr($rowVude['TAREST'], 150, 10);
	 			    
	 			} else {
	 			   //da anagrafica vettore
	 			   $ar_p['TDAUX3']  = '';
	 			   $ar_p['TDIDES']	= trim($trasportatore->rec_data['TAINDI']);
	 			   $ar_p['TDDCAP']	= trim($trasportatore->rec_data['TACAP']); 			
	 			   $ar_p['TDDLOC']	= trim($trasportatore->rec_data['TALOCA']); 			
	 			   $ar_p['TDPROD']	= trim($trasportatore->rec_data['TAPROV']);
	 			}
 			} else {
 			    
 				//RIPRISTINO DESTINAZIONE CLIENTE
 				
 				//pulisco il vecchio trasportatore
 				if (trim($row['TDTDES']) == '1') $ar_p['TDVET1'] = '';
 				if (trim($row['TDTDES']) == '2') $ar_p['TDVET2'] = '';
 				
 				//ripristino il flag TDTDES
 				if (trim($row['TDCDES']) == '')
 					$ar_p['TDTDES'] = 'C';
 				else 
 					$ar_p['TDTDES'] = 'D';
 				
 				//ripristino la destinazione in base a qualla del cliente
 				$ar_p['TDAUX3'] = '';
 				$ar_p['TDDDES'] = $row['TDDCON'];
 				$ar_p['TDIDES']	= $row['TDINDI'];
 				$ar_p['TDDCAP']	= $row['TDCAP'];
 				$ar_p['TDDLOC']	= $row['TDLOCA'];
 				$ar_p['TDPROD']	= $row['TDPROV'];	
 				
 			}
 			
			//eseguo aggiornamento
			$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']} 
						SET TDTDES = ?,
						    TDVET1 = ?,
						    TDVET2 = ?,
						    TDDDES = ?,
						    TDIDES = ?,
						    TDDCAP = ?,
						    TDDLOC = ?,
						    TDPROD = ?,
                            TDAUX3 = ?
						WHERE " . $this->get_where_by_ordine_td($row);

			$stmt_upd = db2_prepare($conn, $sql_upd);

			
			  $result_upd = db2_execute($stmt_upd, array(
					$ar_p['TDTDES'],
					$ar_p['TDVET1'],
					$ar_p['TDVET2'],
					$ar_p['TDDDES'],
					$ar_p['TDIDES'],
					$ar_p['TDDCAP'],
					$ar_p['TDDLOC'],
					$ar_p['TDPROD'],
			        $ar_p['TDAUX3']
				));

			  //history richiesta
			  $sh = new SpedHistory();
			  $sh->crea(
			  		'assegna_scarico',
			  		array(
			  				"k_ordine" 	=> $k_ord,
			  				"RIIVES"	=> $ar_p['TDTDES'],
			  				"RICVES"	=> trim($to_trasportatore_RI),
			  		        "RIDEST"    => trim($r->to_trasportatore_cod_des)
			  		)
			  );			  

			
 		} //per ogni ordine
 		
 	} //foreach list_selected_id
 
 	$ret = array();
 	$ret['success'] = true;
 	return acs_je($ret);
 }
 

 
 
 
 
 
 
 
 
 
 function exe_assegna_destinazione_sosta_tecnica($r){
 	
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$list_selected_id = json_decode($r->list_selected_id);
 
 	$to_destinazione = $r->to_destinazione;
 
 	if ($to_destinazione != 'RIPRISTINO'){
 		//recuperare i dati della destinazione selezionata
 		$to_destinazione_RI = $to_destinazione;
 		
 	} 	else {
 		$to_destinazione_RI = '';
 	}
 
 	//ho selezionato un insieme di liv2 del plan
 	foreach ($list_selected_id as $sel){
 		
 		//recupero i dati del cliente
 		$k_cli_des = $sel->k_cli_des;
 		$k_cli_des_exp = explode("_", $k_cli_des);
 
 		$stmt = $this->get_stmt_ordini_by_liv2_plan($sel);
 
 		//per ogni ordine
 		while ( $row = db2_fetch_assoc($stmt) ){
 			$ar_p = array();
 			$ar_p['TDTDES'] = $row['TDTDES'];
 			$ar_p['TDVET1'] = $row['TDVET1'];
 			$ar_p['TDVET2'] = $row['TDVET2'];
 
 			$k_ord = $this->k_ordine_td($row);
 
 
 			if ($to_destinazione != 'RIPRISTINO'){ 				
				$ar_p['TDTDES'] = 'T';
 				$ar_p['TDCDEL'] = trim($to_destinazione);
 				if (trim($row['TDTDES']) == '1') $ar_p['TDVET1'] = '';
 				if (trim($row['TDTDES']) == '2') $ar_p['TDVET2'] = '';
 				
 				$destSostaTecnica = new SpedDestinazioneSostaTecnica(); 				
 				$row_cli_des_anag = $destSostaTecnica->get_cliente_des_anag($k_cli_des_exp[0], $k_cli_des_exp[1], $to_destinazione);
 					
 				$ar_p['TDDDES'] = $row_cli_des_anag['TADESC'];
 				$ar_p['TDIDES']	= $row_cli_des_anag['TAINDI'];
 				$ar_p['TDDCAP']	= $row_cli_des_anag['TACAP'];
 				$ar_p['TDDLOC']	= $row_cli_des_anag['TALOCA'];
 				$ar_p['TDPROD']	= $row_cli_des_anag['TAPROV'];
 				$ar_p['TDNAZD']	= $row_cli_des_anag['TANAZI'];
				$ar_p['TDDNAD']	= $this->decod_std('NAZIO', $ar_p['TDNAZD']); 

 			} else {
 				//RIPRISTINO DESTINAZIONE CLIENTE
 					
 				//pulisco il vecchio trasportatore
 				$ar_p['TDCDEL'] = '';
 				
 					
 				//ripristino il flag TDTDES
 				if (trim($row['TDCDES']) == '')
 					$ar_p['TDTDES'] = 'C';
 				else
 					$ar_p['TDTDES'] = 'D';
 					
 				//ripristino la destinazione in base a qualla del cliente
 				$ar_p['TDDDES'] = $row['TDDCON'];
 				$ar_p['TDIDES']	= $row['TDINDI'];
 				$ar_p['TDDCAP']	= $row['TDCAP'];
 				$ar_p['TDDLOC']	= $row['TDLOCA'];
 				$ar_p['TDPROD']	= $row['TDPROV'];
 				$ar_p['TDNAZD']	= $row['TDNAZI'];
 				$ar_p['TDDNAD'] = $row['TDDNAZ'];
 					
 			}
 
 			//eseguo aggiornamento
 			$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
 				SET TDTDES = ?,
 				TDVET1 = ?,
 				TDVET2 = ?,
 				TDDDES = ?,
 				TDIDES = ?,
 				TDDCAP = ?,
 				TDDLOC = ?,
 				TDPROD = ?,
 				TDCDEL = ?,
 				TDNAZI = ?,
 				TDDNAD = ?
 				WHERE " . $this->get_where_std() . " AND TDDOCU = ?";
 
 			$stmt_upd = db2_prepare($conn, $sql_upd);
 			echo db2_stmt_errormsg();
 				
 			$result_upd = db2_execute($stmt_upd, array(
 					$ar_p['TDTDES'],
 					$ar_p['TDVET1'],
 					$ar_p['TDVET2'],
 					$ar_p['TDDDES'],
 					$ar_p['TDIDES'],
 					$ar_p['TDDCAP'],
 					$ar_p['TDDLOC'],
 					$ar_p['TDPROD'],
 					$ar_p['TDCDEL'],
 					$ar_p['TDNAZD'],
 					$ar_p['TDDNAD'],
 					$row['TDDOCU']
 			));
 			echo db2_stmt_errormsg($stmt_upd);
 			
 			
 			//history richiesta
 			$sh = new SpedHistory();
 			$sh->crea(
 					'assegna_destinazione_sosta_tecnica',
 					array(
 							"k_ordine" 	=> $k_ord,
 							"cod_dest"	=> $to_destinazione_RI
 					)
 			);

 
 				
 		} //per ogni ordine
 			
 	} //foreach list_selected_id
 
 	$ret = array();
 	$ret['success'] = true;
 	return acs_je($ret);
 }
 
 
 
 
 

 
 
 
 function exe_assegna_destinazione_finale($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$list_selected_id = json_decode($r->list_selected_id);
 	
 	$to_destinazione = $r->to_destinazione;
 
 	//ho selezionato un insieme di liv2 del plan
 	foreach ($list_selected_id as $sel){
 		
 		//recupero i dati del cliente
 		$k_cli_des = $sel->k_cli_des;
 		$k_cli_des_exp = explode("_", $k_cli_des);
 		
 		//recupero i dati della destinazione selezionata
 		$row_cli_des_anag = $this->get_cliente_des_anag($k_cli_des_exp[0], $k_cli_des_exp[1], $to_destinazione);
 
 		if ($sel->liv == 'liv_2') 	//per cliente	
 			$stmt = $this->get_stmt_ordini_by_liv2_plan($sel);
 		elseif ($sel->liv == 'liv_3') //per ordine
 			$stmt = $this->get_stmt_ordini_by_liv3_plan($sel);
 		elseif ($r->tipo == 'PROGETTA') //vengo da progetta spedizioni
 			$stmt = $this->get_stmt_ordini_by_progetta($sel); 		

 
 		//per ogni ordine
 		while ( $row = db2_fetch_assoc($stmt) ){
 			$ar_p = array();
 
 			$k_ord = $row['TDDOCU'];

 			$ar_p['TDTDES'] = 'D';
 			$ar_p['TDCDES'] = $to_destinazione; 			
 					
			//ripristino la destinazione in base a qualla del cliente
			$ar_p['TDDDES'] = $row_cli_des_anag['TADESC'];
			$ar_p['TDIDES']	= $row_cli_des_anag['IND_D'];
			$ar_p['TDDCAP']	= $row_cli_des_anag['CAP_D'];
			$ar_p['TDDLOC']	= $row_cli_des_anag['LOC_D'];
			$ar_p['TDPROD']	= $row_cli_des_anag['PRO_D'];
 
 			//eseguo aggiornamento
 			$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
			 			SET TDTDES = ?, TDCDES = ?,
			 			
			 			TDINDI = ?,
			 			TDCAP  = ?,
			 			TDLOCA = ?,
			 			TDPROV = ?,
			 						 			
			 			TDDDES = ?,
			 			TDIDES = ?,
			 			TDDCAP = ?,
			 			TDDLOC = ?,
			 			TDPROD = ?
			 			WHERE TDDOCU = ?";
 
 			$stmt_upd = db2_prepare($conn, $sql_upd);
 
 				
 			$result_upd = db2_execute($stmt_upd, array(
			 			$ar_p['TDTDES'], $ar_p['TDCDES'], 

	 					$ar_p['TDIDES'],
	 					$ar_p['TDDCAP'],
	 					$ar_p['TDDLOC'],
	 					$ar_p['TDPROD'],
 					 					
			 			$ar_p['TDDDES'],
			 			$ar_p['TDIDES'],
			 			$ar_p['TDDCAP'],
			 			$ar_p['TDDLOC'],
			 			$ar_p['TDPROD'],
 					
			 			$k_ord
 			));
 
 			//history richiesta
 			$sh = new SpedHistory();
 			  $sh->crea(
 			  'assegna_destinazione_finale',
 			  array(
 			  "k_ordine" 	=> $k_ord,
 			  "cod_dest"	=> $to_destinazione
 			  )
 			);
 			
		
 		} //per ogni ordine
 			
 		} //foreach list_selected_id
 
 		$ret = array();
 		$ret['success'] = true;
 		return acs_je($ret);
 }
 
 
 
 
 
 
 
 
 
 
 
//da day, per cli/des 
function get_stmt_ordini_by_liv2_plan($sel) {
	global $conn,  $cfg_mod_Spedizioni;

	$sped_id 	= $sel->sped_id;
	$k_carico 	= $sel->k_carico;
	$ar_carico  = $this->k_carico_td_decode($k_carico);
	$ar_cli_des = explode("_", $sel->k_cli_des);
	
	$ar_id = explode("|", $sel->id);
	$liv1_exp = explode("___", $ar_id[2]);

	$m_tddtep = $liv1_exp[0];
	$m_tdciti = $liv1_exp[1];
	
	//ricerco gli ordini da modificare
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']}
	WHERE " . $this->get_where_std() . " AND TDNBOC = ? AND TDAACA = ? AND TDTPCA = ? AND TDNRCA = ?
	 				  AND TDDT = ? AND TDCCON = ? AND TDCDES = ?  AND TDDTEP = ?  /* AND TDCITI = ? */
	 			    ";
	 		$stmt = db2_prepare($conn, $sql);
	 		$result = db2_execute($stmt, array($sped_id, $ar_carico['TDAACA'], $ar_carico['TDTPCA'], $ar_carico['TDNRCA'],
	 				$ar_cli_des[0], $ar_cli_des[1], $ar_cli_des[2], $m_tddtep
	 		));
	return $stmt; 		
}

//da day, per carico
function get_stmt_ordini_by_liv1_plan($sel) {
	global $conn,  $cfg_mod_Spedizioni;

	$sped_id 	= $sel->sped_id;
	$k_carico 	= $sel->k_carico;
	$ar_carico  = $this->k_carico_td_decode($k_carico);

	$ar_id = explode("|", $sel->id);
	$liv1_exp = explode("___", $ar_id[2]);

	$m_tddtep = $liv1_exp[0];
	$m_tdciti = $liv1_exp[1];

	//ricerco gli ordini da modificare
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']}
	WHERE " . $this->get_where_std() . " AND TDNBOC = ? AND TDAACA = ? AND TDTPCA = ? AND TDNRCA = ?
	 				  AND TDDTEP = ?  /* AND TDCITI = ? */
	 			    ";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($sped_id, $ar_carico['TDAACA'], $ar_carico['TDTPCA'], $ar_carico['TDNRCA'],
			$m_tddtep
	));
	return $stmt;
}



function get_stmt_ordini_by_progetta($sel) {
	global $conn,  $cfg_mod_Spedizioni;
	
	$k_sped_carico_ar = explode("|", $sel->k_sped_carico);
	$sped_id 	= $k_sped_carico_ar[0];
	$k_carico 	= $k_sped_carico_ar[1];
	$ar_carico  = $this->k_carico_td_decode($k_carico);
	$ar_cli_des = explode("_", $sel->k_cli_des);

	$ar_id = explode("|", $sel->id);
	$liv1_exp = explode("___", $ar_id[2]);

	$m_tddtep = $liv1_exp[0];
	$m_tdciti = $liv1_exp[1];

	//ricerco gli ordini da modificare
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']}
	WHERE " . $this->get_where_std() . " AND TDNBOC = ? AND TDAACA = ? AND TDTPCA = ? AND TDNRCA = ?
	 				  AND TDDT = ? AND TDCCON = ? AND TDCDES = ?  /* AND TDDTEP = ?  AND TDCITI = ? */
	 			    ";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($sped_id, $ar_carico['TDAACA'], $ar_carico['TDTPCA'], $ar_carico['TDNRCA'],
			$ar_cli_des[0], $ar_cli_des[1], $ar_cli_des[2] //, $m_tddtep
	));
	return $stmt;
}

 
 
function get_stmt_ordini_by_liv3_plan($sel) {
	global $conn,  $cfg_mod_Spedizioni;

	$k_ordine 	= $sel->k_ordine;

	//ricerco gli ordini da modificare
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']}
				WHERE " . $this->get_where_std() . " AND TDDOCU = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($k_ordine));
	return $stmt;
}

 
 
 
 
function crea_carico($dt, $tp, $aa, $nr, $desc, $stat){
 		global $conn;
		global $cfg_mod_Spedizioni;
		
		//verifico che il carico non esista
		$sql = "SELECT count(*) AS COUNT_T FROM {$cfg_mod_Spedizioni['file_carichi']}
				WHERE PSDT = " . sql_t($dt) . "
				  AND PSTAID = " . sql_t("PRGS") . "
				  AND PSTPCA = " . sql_t($tp) . "				  		
				  AND PSAACA = " . sql_t($aa) . "				  		
				  AND PSNRCA = " . sql_t($nr) . "				  		
		 ";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);		
		$row = db2_fetch_assoc($stmt); 
		
		if ($row['COUNT_T'] > 0 ) //gia' esiste
			return false;		
		
		$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_carichi']}(PSDT, PSTAID, PSTPCA, PSAACA, PSNRCA, PSDESC, PSSTAT)
				VALUES('$dt', 'PRGS', '$tp', $aa, $nr, '$desc', '$stat')";		
	
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		return $result;						
}



function exe_assegna_progressivo_carico($r){
	global $cfg_mod_Spedizioni;	
	$chiave_carico = $r->f_chiave;
	$tipo_carico = $r->f_tipo_carico;

	if (strlen(trim($r->f_carico)) == 0)
	    $nr_carico = $this->next_num('CAR' . $tipo_carico. $chiave_carico);
	else
		$nr_carico = $r->f_carico;
	
	if ($cfg_mod_Spedizioni['carico_per_data'] == 'Y'){
		//restituisco il carico nel formato mmggnr
		$nr_carico = substr($chiave_carico, 4, 4) . sprintf("%02s", $nr_carico);
	}	
	
	$ret = array();
	$ret['success'] = true;
	$ret['nr'] = $nr_carico;

  return acs_je($ret);	
}



function exe_assegna_carico($r){
	
 		global $conn, $id_ditta_default;
		global $cfg_mod_Spedizioni;
		
		$to_sped_id = $r["sped_id"];
		$list_selected_id = $r["list_selected_id"];
		
		//carico
		
		if(isset($cfg_mod_Spedizioni['tipo_carico']))		
			$tp_carico = $cfg_mod_Spedizioni['tipo_carico'];
		elseif(isset($r['f_tipo_carico']) && strlen($r['f_tipo_carico']) > 0)
		    $tp_carico = $r['f_tipo_carico'];
		else
			$tp_carico = 'CA';
		
		$dt_carico = $id_ditta_default;		
		$aa_carico = $r['f_anno'];		
		$chiave_carico = $r['f_chiave'];		
		
		$f_descrizione = $r['f_descrizione'];
		$f_stato 	   = $r['f_stato'];

		$tdfn01 = 1;
		
		if (trim($r['f_carico']) == '')
		{
			//recupero il numero di carico dalla tab. numeratori
			$nr_carico = $this->next_num('CAR' . $chiave_carico);			
		} 
		else {
			//numero carico impostato automaticamente
			$nr_carico = $r['f_carico'];
		}
		
		//creo il nuovo carico
		//TODO: sentire con Giorgio su come gestire la modifica del carico (cambia anche dstinazione,
		// che controlli devo fare? etc...)
		$result = $this->crea_carico($dt_carico, $tp_carico, $aa_carico, $nr_carico, $f_descrizione, $f_stato);
		
		if ($result == false){ //non ho potuto creare il carico
			$ret = array();
			$ret["success"] = false;
			$ret["error_log"] = "Carico gia' esitente.";
			
			//verifico se il carico ha ordini abbinati
			$num_ord_in_carico = $this->get_num_ord_by_car($dt_carico, $tp_carico, $aa_carico, $nr_carico);
			$ret["num_ordini_in_carico"] = $num_ord_in_carico;
			
			//se il carico non ha ordini abbinati, e mi ha chiesto di forzare l'oerazione, proseguo
			if ($num_ord_in_carico == 0 && $r['forzaCarico'] == $r['f_carico']){
				//procedo assegnando gli ordini al carico selezionato (e forzato)
			}
			else 
				return acs_je($ret);
		}
		
		
		//Se ho un unico cliente|destinazione applico direttamente sequenza 01
		$applica_seq = '';
		if ($r['by_sped_car'] == 'Y'){			// per sped_carico (da Progettazione)
			$sottostringhe = explode("|", $list_selected_id[0]);			
			$sped 			= $sottostringhe[0];
			$carico_txt 	= $sottostringhe[1];
			$carico_ar		= $this->k_carico_td_decode($carico_txt);
			
			//recupero l'elenco degli ordini da modificare
			$sql = "SELECT TDCCON, TDCDES FROM {$cfg_mod_Spedizioni['file_testate']} TD
							WHERE " . $this->get_where_std() . "
							AND TDDTEP = " . $r['dtep'] . "
							AND TDNBOC = " . $sped . "
							AND TDTPCA = " . sql_t($carico_ar['TDTPCA']) . "
							AND TDAACA = " . $carico_ar['TDAACA'] . "
							AND TDNRCA = " . $carico_ar['TDNRCA'] . "
							GROUP BY TDCCON, TDCDES
							";

			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
			$tot_cli_des = 0;
			while ($row_test = db2_fetch_assoc($stmt))
				$tot_cli_des++;
			
			
			if ($tot_cli_des == 1) //ho un unico cliente/destinazione
				$applica_seq = '01';
				
			
		} else if ($r['by_sped_car'] == 'by_ORD') { //ricevo direttamente l'elenco degli ordini		
			
		} else { 								//per cliente/sequenza/.. da DAY
			$cli_des_ar = array();
			foreach ($list_selected_id as $k1){
				$sottostringhe = explode("|", $k1);				
				$liv1 = $sottostringhe[2];
				$liv2 = $sottostringhe[4];
					
				$s1 = explode("___", $liv1);
				$s2 = explode("___", $liv2);	

				//$s2[1] TDDCON
				//$s2[2] TDCDES				
				$cli_des_ar[implode("_", array($s2[1], $s2[2]))] += 1;
			}			

			if (count($cli_des_ar) == 1) //ho un unico cliente/destinazione
				$applica_seq = '01';
			
		}		
		
		
		
		foreach ($list_selected_id as $k1){

			//nelle testate relative aggiorno il carico						
			$sottostringhe = explode("|", $k1);
			
			if ($r['by_sped_car'] == 'Y'){			// per sped_carico (da Progettazione)
				$sped 			= $sottostringhe[0];
				$carico_txt 	= $sottostringhe[1];
				$carico_ar		= $this->k_carico_td_decode($carico_txt);
				
				//recupero l'elenco degli ordini da modificare
				$sql = "SELECT TDDOCU, TDSECA, TDVOLU FROM {$cfg_mod_Spedizioni['file_testate']} TD
							WHERE " . $this->get_where_std() . "
							AND TDDTEP = " . $r['dtep'] . "
							AND TDNBOC = " . $sped . "
							AND TDTPCA = " . sql_t($carico_ar['TDTPCA']) . "
							AND TDAACA = " . $carico_ar['TDAACA'] . "
							AND TDNRCA = " . $carico_ar['TDNRCA'] . "
						    ORDER BY TDSECA, TDSCOR, TDONDO
					";				
				
			} else if ($r['by_sped_car'] == 'by_ORD') { //ricevo direttamente l'elenco degli ordini
				//recupero l'elenco degli ordini da modificare
				$sql = "SELECT TDDOCU, TDSECA FROM {$cfg_mod_Spedizioni['file_testate']} TD
						WHERE " . $this->get_where_std() . "
							AND TDDOCU = " . sql_t($k1) . "
					";
				
				
			} else { 								//per cliente/sequenza/.. da DAY
				$liv1 = $sottostringhe[2];
				$liv2 = $sottostringhe[4];
					
				$s1 = explode("___", $liv1);
				$s2 = explode("___", $liv2);
				
					
				//recupero l'elenco degli ordini da modificare
				$sql = "SELECT TDDOCU, TDSECA FROM {$cfg_mod_Spedizioni['file_testate']} TD
							WHERE " . $this->get_where_std() . "
							AND TDDTEP = " . $s1[0] . "
							AND TDNBOC = " . $s1[1] . "
							AND TDTPCA = " . sql_t($s1[2]) . "
							AND TDAACA = " . $s1[3] . "
							AND TDNRCA = " . $s1[4] . "
							AND TDSECA = " . sql_t($s2[0]) . "
							AND TDCCON = " . sql_t($s2[1]) . "
							AND TDCDES = " . sql_t($s2[2]) . "
					";
			}
			
			
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
			
			
			$funzione_riversa_su_carico_spedizione = 'N';
			/* se vuole split carico */
			if (
					trim($r['f_seq_inizio_altro_carico']) != '' ||
					trim($r['f_volume_limite_primo_carico']) != ''
				){
				$funzione_riversa_su_carico_spedizione = 'Y';
				
				//creo carico successivo
				$chiave_carico = $r['f_chiave'];
				$nr_carico2 = $this->next_num('CAR' . $chiave_carico);
				if ($cfg_mod_Spedizioni['carico_per_data'] == 'Y'){
					//restituisco il carico nel formato mmggnr
					$nr_carico2 = substr($chiave_carico, 4, 4) . sprintf("%02s", $nr_carico2);
				}				
			} //se split carico
			
			
			if ($r['f_riversa_su_altra_spedizione'] == 'Y' && $r['by_sped_car'] == 'Y'){

				//creo legame spedizioni collegate
				//dalla sped principale tolgo il volume della spedizione secondaria
				$altra_sped_exp = explode('|', $r['f_storeId_rx']);
				$altra_sped_id  = $altra_sped_exp[0];
				$altra_sped = $this->get_spedizione($altra_sped_id);
				$sped_princ = $this->get_spedizione($sped);

				$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET CSNSPC = ?, CSFG02 = ?, CSVOLD = ?
						WHERE CSDT = '{$id_ditta_default}' AND CSCALE = '*SPR' AND CSPROG = ? ";
				$stmtColl = db2_prepare($conn, $sql);
				$result = db2_execute($stmtColl, array($sped, 'S', $altra_sped['CSVOLD'], $altra_sped_id));
				$result = db2_execute($stmtColl, array($altra_sped_id, '', $sped_princ['CSVOLD'] - $altra_sped['CSVOLD'], $sped));
			}
			
			
			$prog_volume = 0;
			while ($row = db2_fetch_assoc($stmt)){
				//aggiorno ordine
				// azzero la seq. carico
				
				$prog_volume += $row['TDVOLU'];
				
				if (
						(trim($r['f_seq_inizio_altro_carico']) != '' && (int)$row['TDSECA'] >= (int)trim($r['f_seq_inizio_altro_carico'])) ||
						(trim($r['f_volume_limite_primo_carico']) != '' && (float)$prog_volume >= (float)sql_f($r['f_volume_limite_primo_carico']))						
					){
					$ord_da_riversare = 'Y';
					$assenga_nr_carico = $nr_carico2;
				}
				else {
					$ord_da_riversare = 'N';
					$assenga_nr_carico = $nr_carico;
				}
				
				
				if ($funzione_riversa_su_carico_spedizione == 'Y'){
					if (trim($r['f_azzera_sequenza']) == 'Y')
						$applica_seq_ord = $applica_seq;						
					else
						$applica_seq_ord = $row['TDSECA'];
				} else {
					$applica_seq_ord = $applica_seq;
				}
					
					
				if ($funzione_riversa_su_carico_spedizione == 'Y' && $r['f_riversa_su_altra_spedizione'] == 'Y' && $ord_da_riversare == 'Y') {
					$ord_old = $this->get_ordine_by_k_docu($row['TDDOCU']);					
					
					//Mantieni allineato TDNBOC|TDNBOF
					if ($ord_old['TDNBOC'] == $ord_old['TDNBOF'])
					  $sql_allinea_TDNBOF = ", TDNBOF = {$altra_sped_id}";
					else
					  $sql_allinea_TDNBOF = "";
					
					$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
						  TDNBOC = " . $altra_sped_id . "
                          {$sql_allinea_TDNBOF}
 						, TDSWSP = 'Y' /* TODO: e' giusto?? */
						, TDTPCA = " . sql_t($tp_carico) . "
						, TDAACA = " . $aa_carico . "
 						, TDNRCA = " . $assenga_nr_carico . "										
 						WHERE " . $this->get_where_by_k_ordine($row['TDDOCU']) . " AND " . $this->get_where_std() . "
 						";

						
						$stmtU = db2_prepare($conn, $sql);
						$result = db2_execute($stmtU, array());
										
					
					//su altra spedizione
					$this->exe_allinea_dati_spedizione_su_testata($altra_sped, $row['TDDOCU']);
					$ord = $this->get_ordine_by_k_docu($row['TDDOCU']);
						
					$new_RICVES = $this->get_new_RICVES($ord);
					
					//HISTORY
					$sh = new SpedHistory();
					$sh->crea(
							'assegna_spedizione',
							array(									
									"k_ordine" 	=> $row['TDDOCU'],
									"sped"		=> $altra_sped,
									"op"		=> 'ASS',
									"ex"		=> "(#{$ord_old['TDNBOC']},C:{$ord_old['TDNRCA']},Sw:{$ord_old['TDSWSP']})",
										
									"k_carico"	=> implode("_", array($aa_carico, $tp_carico, $assenga_nr_carico)),
										
									"seca"		=> $ord['TDSECA'],
									"RIIVES"	=> $ord['TDTDES'],
									"RICVES"	=> $new_RICVES										
							)
					);
					
				} else {
					//assegna carico
						$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
								  	TDSWSP = 'Y'
									, TDTPCA = " . sql_t($tp_carico) . "
									, TDAACA = " . $aa_carico . "
									, TDNRCA = " . $assenga_nr_carico . "
									, TDFN01 = $tdfn01
									, TDSECA = " . sql_t($applica_seq_ord) . "
								WHERE TDDOCU = " . sql_t($row['TDDOCU']) . " AND " . $this->get_where_std();
						
						$stmt_upd = db2_prepare($conn, $sql_upd);
						echo db2_stmt_errormsg();
						$result = db2_execute($stmt_upd);				
						
						$sh = new SpedHistory();
						$sh->crea(
								'assegna_carico',
								array(
										"k_ordine" 	=> $row['TDDOCU'],
										"k_carico"	=> implode("_", array($aa_carico, $tp_carico, $assenga_nr_carico)),
										"seca"		=> $applica_seq_ord
								)
						);
				}					
				
			}
			
			 
		}
				
	$ret = array();
	$ret["success"] = true;
	return acs_je($ret); 		
 }




function exe_toggle_data_conferma($r){	
 		global $conn;
		global $cfg_mod_Spedizioni;
		
		$list_selected_id = $r->list_selected_id;
		
		foreach ($list_selected_id as $k1){	
			$sottostringhe = explode("|", $k1);
			$k_ord = $k1; //$sottostringhe[6];
			
			$ord = $this->get_ordine_by_k_ordine($k_ord);
			
			if (strlen($ord['TDDTVA']) == 8){ 	// *** RIMUOVO LA DATA CONFERMATA ********
				
				$new_TDTPCA = $ord['TDTPCA'];
				$new_TDAACA = $ord['TDAACA'];
				$new_TDNRCA = $ord['TDNRCA'];
				$new_TDSWSP = $ord['TDSWSP'];
				$new_TDFN04 = $ord['TDFN04'];	
				$new_TDNBOC = $ord['TDNBOC'];
				$new_TDNBOF = $ord['TDNBOF'];   //Mantieni TDNBOF allineata con TDNBOC
				$new_TDSECA = $ord['TDSECA'];				

				$data_confermata_per_ri = 0;
				
				if (trim($ord['TDTPRI']) == 'E'){ //priorita' esterna ... lo rimetto tra gli hold e reset carico/spedizione
					$new_TDSWSP = "N";
					$new_TDFN04 = 1;
					$new_TDTPCA = '';
					$new_TDAACA = 0;
					$new_TDNRCA = 0;
					$new_TDNBOC = 0;
					$new_TDNBOF = 0;
					$new_TDSECA = '';					
				}
				
				
				$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET 
							  TDDTVA = 0, TDFN06 = 0, TDSWSP = " . sql_t($new_TDSWSP) . ", TDFN04 = {$new_TDFN04} ,
							  TDTPCA = " . sql_t($new_TDTPCA) . ", TDAACA = {$new_TDAACA}, TDNRCA = {$new_TDNRCA} ,
							  TDNBOC = {$new_TDNBOC}, TDNBOF = {$new_TDNBOF},  
                              TDSECA = " . sql_t($new_TDSECA) . "
						WHERE " . $this->get_where_by_k_ordine($k_ord) . "
				";
			}	
			else { 								// *** ASSEGNO LA DATA CONFERMATA ********
			
				$data_confermata_per_ri = $ord['TDDTEP'];
				
				//TODO: e' giusto rimettere tdswp = Y e tdfn04 = 0? (indicazione da email Giorgio del 14/06/2013)
			
				$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET 
							  TDDTVA = TDDTEP, TDFN06 = 1, TDSWSP = 'Y', TDFN04 = 0
						WHERE " . $this->get_where_by_k_ordine($k_ord) . "
				";		
			}	
			
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);

			$sh = new SpedHistory();
			$sh->crea(
					'upd_data_confermata',
					array(
							"k_ordine" 	=> $k_ord,
							"data_confermata" => $data_confermata_per_ri 
					)
			);
			
		}
				
	return $result; 		
 }


 
 
 function exe_riallinea_data_conferma($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$list_selected_id = $r->list_selected_id;
 
 	foreach ($list_selected_id as $k1){
 		$sottostringhe = explode("|", $k1);
 		$k_ord = $k1; //$sottostringhe[6];
 			
 		$ord = $this->get_ordine_by_k_ordine($k_ord);
 			
	       // *** RIALLINEO LA DATA CONFERMATA ********
 					
 				$data_confermata_per_ri = $ord['TDDTEP'];
 
 				//TODO: e' giusto rimettere tdswp = Y e tdfn04 = 0? (indicazione da email Giorgio del 14/06/2013) 					
 				$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
		 				TDDTVA = TDDTEP
 						WHERE " . $this->get_where_by_k_ordine($k_ord) . "
 						";
 			
 				
 			$stmt = db2_prepare($conn, $sql);
 				$result = db2_execute($stmt);
 
 				$sh = new SpedHistory();
 				$sh->crea(
 						'upd_data_confermata',
 						array(
							"k_ordine" 	=> $k_ord,
 							"data_confermata" => $data_confermata_per_ri
 				)
 				);
 					
 	}
 
 	return $result;
 	}
 
 
 
 
 
 



 function next_num($cod){ 	
 		global $conn, $id_ditta_default;
		global $cfg_mod_Spedizioni;
		
		
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_numeratori']} WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		
		if ($row = db2_fetch_assoc($stmt)){
			$n = $row['NRPROG'] +1;
			$sql = "UPDATE {$cfg_mod_Spedizioni['file_numeratori']} SET NRPROG=$n WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);			
		} else {			
			$n = 1;
			$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_numeratori']}(NRDT, NRCOD, NRPROG)  VALUES(" . sql_t($id_ditta_default) . ", '$cod', $n)";
		
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);			
		}
		
	return $n; 		
 }

	
 function get_rows_ordine_by_num($ord, $ar_blocchi, $from_mto = null){
 		global $conn;
 		global $cfg_mod_Spedizioni, $id_ditta_default;
		$ar = array();		
		$ord_exp = explode('_', $ord);
		
		if($from_mto == 'Y'){
		    
		    global $cod_mod_provenienza;
		    if ($cod_mod_provenienza == 'DESK_PVEN'){
		        $sql_join = "
		                  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_proposte_MTO']} MT
		                  ON RDDT = MT.MTDT AND RDOTID = MT.MTTIDO AND RDOINU = MT.MTINUM AND RDOADO = MT.MTAADO AND RDONDO = MT.MTNRDO
		                  AND RDPMTO = SUBSTRING(MT.MTFILL, 11, 8)  AND MT.MTSTAR <>'R'
		        
		                  LEFT OUTER JOIN  {$cfg_mod_Spedizioni['file_wizard_MTS_2']} M2
		                  ON M2.M2DT = RDDT  AND M2.M2ART = RDART AND M2.M2MAGA = MT.MTDEPO";
		        $sql_select = ", MT.MTDEPO, M2.*";
		        
		    }else{
		        $sql_join = " LEFT OUTER JOIN  {$cfg_mod_Spedizioni['file_wizard_MTS_2']} M2
		                      ON M2.M2DT = RDDT  AND M2.M2ART = RDART AND M2.M2MAGA = TD.TDDEPO";
		        $sql_select = ", M2.*";
		     }
		     
		     
		     $sql_join .= " LEFT OUTER JOIN(
		                      SELECT COUNT(*) AS C_REVOCHE, MTDT, MTTIDO, MTINUM, MTAADO, MTNRDO
		                      FROM {$cfg_mod_Spedizioni['file_proposte_MTO']}
            		          WHERE MTDT = '{$id_ditta_default}' AND MTSTAR IN ('R', 'C')
            		          GROUP BY MTDT, MTTIDO, MTINUM, MTAADO, MTNRDO
            		      ) MT
		                  ON RDDT = MT.MTDT AND RDOTID = MT.MTTIDO AND RDOINU = MT.MTINUM AND RDOADO = MT.MTAADO AND SUBSTR(RDONDO, 1, 6) = MT.MTNRDO AND MT.MTDT = TD.TDDTOR";
		     $sql_select .= ", C_REVOCHE AS REVOCHE";
		     
		     
        }else
		    $sql_join = "";

		if (sizeof($ord_exp) == 2){
			$anno = $ord_exp[0];
			$ord = $ord_exp[1];
			
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe']} RD
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
					ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO AND TDPROG=RDPROG
					WHERE RDTPNO IN(" . implode(', ', array_map('sql_t', $ar_blocchi)) . ")
						AND RDONDO =  " . sql_t($ord) . " AND RDOADO = $anno ";			
		} else {
		    
		    $oe = $this->k_ordine_td_decode_prog($ord);
			$sql = "SELECT RD.*, TD.* {$sql_select}
					FROM {$cfg_mod_Spedizioni['file_righe']} RD
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
					ON TD.TDDT=RDDT AND TD.TDOTID=RDOTID AND TD.TDOINU=RDOINU AND TD.TDOADO=RDOADO AND TD.TDONDO=RDONDO AND TD.TDPROG=RDPROG
			        
                    {$sql_join}
                    WHERE RDTPNO IN(" . implode(', ', array_map('sql_t', $ar_blocchi)) . ") 
                    {$sql_where}
					AND TD.TDDT=" . sql_t($oe['TDDT']) . " AND TD.TDOTID=" . sql_t($oe['TDOTID'])  . "
				    AND TD.TDOINU=" . sql_t($oe['TDOINU']) . " AND TD.TDOADO=" . sql_t($oe['TDOADO'])  . "
				    AND TD.TDONDO=" . sql_t($oe['TDONDO']) . " AND TD.TDPROG=" . sql_t($oe['TDPROG']);
								
		}
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);	
	return $stmt;
 }	
	
 
 function get_rows_reso_by_cli_des($k_cli_des){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$cli_des_exp = explode('_', $k_cli_des);

 	$tddt 	= $cli_des_exp[0];
 	$tdccon = $cli_des_exp[1];
 	$tdcdes = $cli_des_exp[2]; 	
 	
 	$ar = array();
 	$sql = "SELECT *
 	FROM {$cfg_mod_Spedizioni['file_righe']}
 	INNER JOIN {$cfg_mod_Spedizioni['file_testate']}
 			ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
 			WHERE TDDT = " . sql_t($tddt) . " AND TDCCON = " . sql_t($tdccon) . " AND TDCDES = " . sql_t($tdcdes) .
 			" AND TDOTID = 'VO' AND TDOINU = '' AND TDOADO = 0
 			  AND TDSWPP = 'R' 
 			ORDER BY RDODER DESC
 			";
 	
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt);
 			return $stmt;
 } 
 
 
 function get_cols_ordine_by_num($ord){
 		global $conn, $id_ditta_default;
		global $cfg_mod_Spedizioni;
		$ord_exp = explode('_', $ord);
		$anno = $ord_exp[0];
		$ord = $ord_exp[1];
		$ar = array();  		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_colli']}
				WHERE PLNRDO =  $ord AND PLAADO = $anno
				  AND PLDT = " . sql_t($id_ditta_default) . " AND PLTIDO='VO' AND PLINUM='VO1'
				ORDER BY PLCOAB ";

		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
	return $stmt;
 }	

 
 function get_cols_ordine_by_k_ordine($k_ord, $trad = 'N'){
 		global $conn;
		global $cfg_mod_Spedizioni;

		$oe = $this->k_ordine_td_decode_xx($k_ord);		
		
		if($trad == 'N'){
		    $join = "";
		}else{
		    $join = "LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
				        ON PL.PLDT = TD.TDDT  AND PL.PLTIDO=TD.TDOTID  AND PL.PLINUM=TD.TDOINU  AND PL.PLAADO =TD.TDOADO	 AND digits(PL.PLNRDO) = TD.TDONDO
                     LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_art_trad']} AL	
                        ON AL.ALDT=PL.PLDT AND AL.ALART=PL.PLART AND AL.ALLING=TD.TDRFLO	
                     LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA
                        ON TA.TADT = PL.PLDT AND TA.TANR = PL.PLART AND TA.TAID = 'PUCI' AND TA.TALINV  = TD.TDRFLO	";
		}
		
		$ar = array();  		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_colli']} PL
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_colli_dc']} PS
				  ON PL.PLDT = PS.PSDT
				  AND PL.PLTIDO = PS.PSTIDO
				  AND PL.PLINUM = PS.PSINUM
				  AND PL.PLAADO = PS.PSAADO
				  AND PL.PLNRDO = PS.PSNRDO
				  AND PL.PLNREC = PS.PSNREC
                $join
                WHERE PLDT = " . sql_t($oe['TDDT']) . "  
				  AND PLTIDO=" . sql_t($oe['TDOTID']) . " 
				  AND PLINUM= ".  sql_t($oe['TDOINU']) . "
				  AND PLAADO = " . sql_f($oe['TDOADO']) . "
				  AND PLNRDO = " . sql_f($oe['TDONDO']) . "
				ORDER BY PLCOAB ";
		
                
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
	return $stmt;
 }	

 
 function get_cols_ordine_adv($filtro){
    global $conn, $id_ditta_default;
    global $cfg_mod_Spedizioni, $cfg_mod_DeskArt;
 
 	$sql_where = '';
 	
 	if (isset($filtro['k_ord']) && strlen($filtro['k_ord']) > 0){
 		$oe = $this->k_ordine_td_decode($k_ord);
 		$sql_where .= "AND PLNRDO = " . sql_f($oe['TDONDO']) . " AND PLAADO = " . sql_f($oe['TDOADO']) . "
 			AND PLDT = " . sql_t($oe['TDDT']) . "  AND PLTIDO=" . sql_t($oe['TDOTID']) . " AND PLINUM= ".  sql_t($oe['TDOINU']); 		
 	}
 	if (isset($filtro['sped']) && strlen($filtro['sped']) > 0)
 		$sql_where .= " AND TDNBOC = {$filtro['sped']}";
 	if (isset($filtro['lotto']) && strlen($filtro['lotto']) > 0)
 		$sql_where .= " AND TDNRLO = {$filtro['lotto']}";
 	
    $sql_where .= sql_where_by_combo_value('TDTPLO', $filtro['TDTPLO']);
    $sql_where .= sql_where_by_combo_value('TDAALO', $filtro['TDAALO']);
    $sql_where .= sql_where_by_combo_value('TDNRLO', $filtro['TDNRLO']);
    
    $sql_where .= sql_where_by_combo_value('TDTPCA', $filtro['TDTPCA']);
    $sql_where .= sql_where_by_combo_value('TDAACA', $filtro['TDAACA']);
    $sql_where .= sql_where_by_combo_value('TDNRCA', $filtro['TDNRCA']);
    
    $sql_where .= sql_where_by_combo_value('TDDTOR', $filtro['TDDTOR']);
    
    $sql_where .= sql_where_by_combo_value('PLFAS10', $filtro['PLFAS10'], null, 'I', true);
    $sql_where .= sql_where_by_combo_value('PLOPE1', $filtro['PLOPE1'], null, 'I', true);
    	
    if ($filtro['carico_assegnato'] == "Y")
        $sql_where .= " AND TDNRCA > 0 ";
    if ($filtro['carico_assegnato'] == "N")
        $sql_where .= " AND TDNRCA = 0 ";
    if ($filtro['lotto_assegnato'] == "Y")
        $sql_where .= " AND TDNRLO > 0 ";
    if ($filtro['lotto_assegnato'] == "N")
        $sql_where .= " AND TDNRLO = 0 ";
    
    if ($filtro['lotto1'] == 'Y'){
        $sql_l1 = ", LOTTO1";
        $j_lotto1 = "  LEFT OUTER JOIN (
                        SELECT COUNT(*) AS LOTTO1, REDT, RETPCO, REAACO, RENRCO, RETPSV, REPROG, RECOLC
                        FROM {$cfg_mod_DeskArt['file_ricom']}
                        WHERE REDT = '{$id_ditta_default}' AND RETIDE = 'L'
                        GROUP BY REDT, RETPCO, REAACO, RENRCO, RETPSV, REPROG, RECOLC
                        ) RE
                        ON PLDT=REDT AND PLTPCO=RETPCO AND PLAACO=REAACO AND PLNRCO=RENRCO AND PLTPSV=RETPSV
                        AND CASE WHEN PLRCOL <> '' THEN REPROG ELSE RECOLC END = PLNCOL";
        
    }
    
    
 	switch ($filtro['stato_spunta']){
 		case "COLLI_ND": //non disponibili
 			$sql_where .= " AND PLDTEN = 0"; 			
 		break;
 		case "COLLI_NS": //non spediti
 			$sql_where .= " AND PLDTUS = 0"; 
 		break;
 		case "colli_S_progress": //non disponibili
 			$sql_where .= " AND PLDTUS = 0";
 			break;
 		case "colli_D_progress": //non spediti
 			$sql_where .= " AND PLDTEN = 0";
 		break;
 	}
 	
 	if (strlen(trim($filtro['PLOPE2'])) > 0)
 		$sql_where .= " AND PLOPE2 = " .sql_t(trim($filtro['PLOPE2'])); 		
	
 		if($filtro['liv'] == 'liv_2'){
 		    if ($filtro['gruppo'] == 'NON ASSEGNATO')
 		        $gruppo = '';
 		    else
 		        $gruppo = $filtro['gruppo'];
 		    
 		    $sql_join_ta =  "  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} TA_LIPC
 		        ON CO.PLDT = TA_LIPC.TADT AND TA_LIPC.TAID = 'LIPC' AND TA_LIPC.TANR = CO.PLFAS10  ";
 		    $sql_where .= " AND TA_LIPC.TACINT = " .sql_t(trim($gruppo));
 		}
 		if($filtro['liv'] == 'liv_3'){
 		    if ($filtro['gruppo'] == 'NON ASSEGNATO')
 		        $gruppo = '';
	        else
	            $gruppo = $filtro['gruppo'];
	            
	        $sql_where .= " AND PLFAS10 = " .sql_t(trim($gruppo)); 	
 		}
 		
	
	

	
    if (strlen(trim($filtro['da_data'])) > 0)
        $sql_where .= " AND TDDTEP = " .sql_t(trim($filtro['da_data'])); 	
 		
    $sql_join_on_PL0 = "ON PLDT = CASE WHEN TD.TDDTOR = '' THEN '{$id_ditta_default}' ELSE TD.TDDTOR END AND PLTIDO=TDOTID AND PLINUM=TDOINU AND PLAADO=TDOADO AND PLNRDO=SUBSTR(TDONDO, 1, 6)";
 		
 	$ar = array();
 	$sql = "SELECT TD.TDDOCU, TD.TDNRCA, TD.TDAACA, TD.TDTPCA, CO.*, PS.* {$sql_l1}
 			 FROM {$cfg_mod_Spedizioni['file_colli']} CO
             INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
 			    {$sql_join_on_PL0}
                {$sql_join_ta}
                {$j_lotto1}
             LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_colli_dc']} PS
				  ON PLDT = PS.PSDT
				  AND PLTIDO = PS.PSTIDO
				  AND PLINUM = PS.PSINUM
				  AND PLAADO = PS.PSAADO
				  AND PLNRDO = PS.PSNRDO
				  AND PLNREC = PS.PSNREC
 			 WHERE 1=1 {$sql_where} ORDER BY PLNRDO, PLCOAB ";
                
                
	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	return $stmt;
 } 


 function get_art_disp($ditta, $art, $m2swtr = null, $m2fg01 = null, $from_wz = 'N', $maga = null, $progressivo = 0){
 		global $conn;
		global $cfg_mod_Spedizioni;  
		
		if(isset($from_wz) && $from_wz == 'Y'){
		    $tab = $cfg_mod_Spedizioni['file_dettaglio_wizard'];
		    $where = " AND M3MAGA = '{$maga}'";
		 
		}else{
		    $tab = $cfg_mod_Spedizioni['file_disponibilita'];
		    $where = "";
		   
		}

		$order_by = "M3DIMP, M3PROG";     //Default
		
		if ($m2swtr == 'O')               //MTO
		  $order_by = "M3DRIF, M3DIMP, M3PROG";
		else {                            //MTS
		  if ($m2fg01 == 'P')
		    $order_by = "M3PROI, M3DIMP, M3PROG, M3PROG2";
		}
		
		global $backend_ERP;
		if($backend_ERP == 'GL'){
		    $where .= " AND M3PROA = '{$progressivo}'";
		}
		
		
		$sql = "SELECT M3.*, M1.M1DTSP
		        FROM {$tab} M3
				  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_imp']} M1
				    ON M3DT=M1DT AND M3TIDO=M1TIDO AND M3INUM=M1INUM AND M3AADO=M1AADO AND M3NRDO=M1NRDO
			    {$join}
                WHERE M3DT = " . sql_t($ditta) . " AND M3ART = " . sql_t($art) . " {$where}
			    ORDER BY {$order_by}";
			  
	
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt);
			
	return $stmt;
 }	
 
 
 function get_data_agg_MRP($ditta, $art){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$sql = "SELECT max(M3DTGE) AS DATA, max(M3ORGE) AS ORA
		 	FROM {$cfg_mod_Spedizioni['file_disponibilita']}
		 	WHERE M3DT = " . sql_t($ditta) . " AND M3ART = " . sql_t($art);

		$stmt = db2_prepare($conn, $sql);
 		$result = db2_execute($stmt);
 		$row = db2_fetch_assoc($stmt); 		
 			
 		return $row;
 } 
 
 function get_data_agg_MRP_all(){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$sql = "SELECT max(M3DTGE) AS DATA, max(M3ORGE) AS ORA	
 			FROM {$cfg_mod_Spedizioni['file_disponibilita']}";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	$row = db2_fetch_assoc($stmt);
 
 	return $row;
 } 

 function get_art_giac($ditta, $art, $from_wz = null, $progressivo = 0){
 		global $conn;
		global $cfg_mod_Spedizioni;  	
		
		if(isset($from_wz) && $from_wz == 'Y')
		    $tab = $cfg_mod_Spedizioni['file_wizard_MTS_2'];
		else
		    $tab = $cfg_mod_Spedizioni['file_giacenze'];
		
	    global $backend_ERP;
	    if($backend_ERP == 'GL')
	        $sql_where = " AND M2PROG = '{$progressivo}'";
		
		$sql = "SELECT * 
		        FROM {$tab} 
				WHERE M2DT = " . sql_t($ditta) . " AND M2ART = " . sql_t($art) . "
                      {$sql_where}
				";
		
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
			
	return $stmt;
 }




 
 
 

	function get_options_area_spedizione($per_combo='N'){
 		global $conn, $id_ditta_default;
		global $cfg_mod_Spedizioni;
		$ar = array();  		
		$sql = "SELECT TAKEY1, TADESC
				FROM {$cfg_mod_Spedizioni['file_tabelle']}
				WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ASPE'
				ORDER BY TADESC
				";		
;

		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		$ar[] = array(null, '- Tutte le aree -');	
		while ($row = db2_fetch_assoc($stmt)){
			if($per_combo == 'Y')
					$ar[] = array("id"=>$row['TAKEY1'], "text" =>$row['TADESC']);
			else
					$ar[] = array($row['TAKEY1'], $row['TADESC']);			
		} 
	return $ar;
 }	
 
 
 
 function decod_stato_ordine($cod){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDSTAT, TDDSST
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE  " . $this->get_where_std() . " AND TDSTAT = ?
 	";
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($cod));
 	$row = db2_fetch_assoc($stmt);
 	if ($row == false)
 		 return '-';
 	else 
 		return $row['TDDSST'];
 }
 
 
 function decod_stato_ordine_by_tab_sys($cod){
 	global $conn, $cfg_mod_Admin, $id_ditta_default;
 	$ar = array();
 	$sql = "SELECT TADESC FROM {$cfg_mod_Admin['file_tab_sys']} WHERE TADT = ? AND TAID = 'BSTA' AND TANR = ?";
 
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt, array($id_ditta_default, $cod));
 			$row = db2_fetch_assoc($stmt);
 			if ($row == false)
 				return '-';
 			else
 				return $row['TADESC'];
  } 
 
 
 
 function get_options_stati_ordine($mostra_codice = 'Y', $filtri= array()){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	
 	$sql_WHERE = '';
 	
 	$sql_WHERE.= sql_where_by_combo_value('TDCDIV', $filtri['divisione']);
 	
 	$sql = "SELECT DISTINCT TDSTAT, TDDSST
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD " .  $this->add_riservatezza() . " 
 			WHERE  " . $this->get_where_std() . " {$sql_WHERE}			
 			ORDER BY TDDSST
 	";
 

 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
		while ($row = db2_fetch_assoc($stmt)){
			if ($mostra_codice == 'Y')
				$text = "[" . $row['TDSTAT'] . "] " . $row['TDDSST'];
			else $text = $row['TDDSST'];
 		$ar[] = array("id"=>$row['TDSTAT'], "text" => $text);
		}
 	return $ar;
 }

 
 function get_options_agenti($mostra_codice = 'Y'){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDCAG1, TDDAG1
 	FROM {$cfg_mod_Spedizioni['file_testate']} TD " .  $this->add_riservatezza() . "
 			WHERE  " . $this->get_where_std() . "
 			ORDER BY TDDAG1
 	";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	while ($row = db2_fetch_assoc($stmt)){
 		if ($mostra_codice == 'Y')
 			$text = "[" . $row['TDCAG1'] . "] " . $row['TDDAG1'];
 		else $text = $row['TDDAG1'];
 		$ar[] = array("id"=>$row['TDCAG1'], "text" => $text);
 	}
 	return $ar;
 }
 
 
 
 function get_options_rischio($mostra_codice = 'Y'){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDRISC AS COD, TDDSRI AS DESCR
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE  " . $this->get_where_std() . "
 			ORDER BY DESCR
 			";
 
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg(); 
  	$result = db2_execute($stmt);
  	while ($row = db2_fetch_assoc($stmt)){
  	if ($mostra_codice == 'Y')
  		$text = "[" . $row['COD'] . "] " . $row['DESCR'];
  				else $text = $row['COD'];
  				$ar[] = array("id"=>$row['COD'], "text" => $text);
  	}
  	return $ar;
 } 
 

 function get_options_causale_trasporto($mostra_codice = 'Y'){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDCAUT AS COD, TDCAUT AS DESCR
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE  " . $this->get_where_std() . "
 			ORDER BY TDCAUT
 			";
 
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg();
 	$result = db2_execute($stmt);
 	while ($row = db2_fetch_assoc($stmt)){
 		if ($mostra_codice == 'Y')
 			$text = "[" . $row['COD'] . "] " . $row['DESCR'];
 			else $text = $row['COD'];
 			$ar[] = array("id"=>$row['COD'], "text" => $text);
 	}
 	return $ar;
 }
 
 
 function get_options_pagamento($mostra_codice = 'Y'){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDCPAG AS COD, TDDPAG AS DESCR
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 			WHERE  " . $this->get_where_std() . "
 			ORDER BY DESCR
 			";
 
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg();
 	$result = db2_execute($stmt);
 	while ($row = db2_fetch_assoc($stmt)){
 		if ($mostra_codice == 'Y')
 			$text = "[" . $row['COD'] . "] " . $row['DESCR'];
 		else $text = $row['COD'];
 		$ar[] = array("id"=>$row['COD'], "text" => $text);
 	}
 	return $ar;
 }
 
 
 function get_options_priorita(){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDOPRI, TDDPRI
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD  " .  $this->add_riservatezza() . "
 			WHERE  " . $this->get_where_std() . " 	
 			ORDER BY TDDPRI
 			";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	while ($row = db2_fetch_assoc($stmt))
 		$ar[] = array("id"=>$row['TDOPRI'], "text" => acs_u8e($row['TDDPRI']));
 		return $ar;
 } 
 
 
 
 
 function get_options_modello(){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDCVN1, TDDVN1 FROM {$cfg_mod_Spedizioni['file_testate']} TD  " .  $this->add_riservatezza() . "
 			WHERE  " . $this->get_where_std() . " 	
 			ORDER BY TDDVN1 ";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	while ($row = db2_fetch_assoc($stmt))
 		$ar[] = array("id"=>$row['TDCVN1'], "text" => $row['TDDVN1']);
 				return $ar;
 } 
 
 function get_options_tipologia_ordini(){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	$sql = "SELECT DISTINCT TDCLOR
 			FROM {$cfg_mod_Spedizioni['file_testate']}
 	 		WHERE  " . $this->get_where_std() . "
 			ORDER BY TDCLOR
 	";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
 	$ar = array();
		while ($row = db2_fetch_assoc($stmt))
 		$ar[] = array($row['TDCLOR'], $row['TDCLOR']);
 	return $ar; 						
 } 
		

				
	
	

 function get_start_map($coordinate = 'N', $area_spedizione = null){
 		global $conn;
		global $cfg_mod_Spedizioni;
		global $id_ditta_default;
		
		$ar = array();  		
		$sql = "SELECT TA_ST.* 
				FROM {$cfg_mod_Spedizioni['file_tabelle']} TA_ST
				INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
				  ON TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = ". sql_t($area_spedizione) . "
				     AND TA_ASPE.TARIF2 = TA_ST.TAKEY1 AND TA_ST.TADT = TA_ASPE.TADT
				WHERE TA_ST.TADT = '{$id_ditta_default}' AND TA_ST.TATAID = 'START'
                ORDER BY TAKEY1
				";
				
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		
		$row = db2_fetch_assoc($stmt);
		
		
		if ($row == false){
			//prendo il primo, senza considerare l'area di spedizione

			$sql = "SELECT TA_ST.*
					FROM {$cfg_mod_Spedizioni['file_tabelle']} TA_ST
					WHERE TA_ST.TADT = '{$id_ditta_default}' AND TA_ST.TATAID = 'START'
                    ORDER BY TAKEY1
				";
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			
			$row = db2_fetch_assoc($stmt);
			
		}
		
		
		if ($row == false){
			return " INDIRIZZO DI PARTENZA NON IMPOSTATO ";
		} else {
			if ($coordinate == 'Y')
				return "({$row['TARIF1']} , {$row['TARIF2']})";
			elseif ($coordinate == 'AR')
				return array('LAT' => $row['TARIF1'] , 'LNG' => $row['TARIF2'], 'DESC' => $row['TADESC']);			
			else
				return $row['TADESC'];			
		}

			
 }	
		
	

 function exe_crt_promemoria($r){
 		global $conn;
		global $cfg_mod_Spedizioni;
		global $auth;
		$ar = array();  		
		$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_promemoria']}(EMDTIN, EMORIN, EMNMMI, EMEMMI, EMMAIL, EMOGGE, EMBODY)
				VALUES(
					{$r['f_data']}, {$r['f_ora']}, " . sql_t(substr(trim($auth->out_name()), 0, 20)) . ", " . sql_t(trim($r['to'])) . ",
					" . sql_t(trim($r['to'])) . ", '{$r['f_oggetto']}', '{$r['f_text']}'
				)				
				";

		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
		
		return $result;
 }	
 
		

 function exe_registra_sequenza_scarico($r){
 		global $conn;
		global $cfg_mod_Spedizioni;
		$ar = array();  		
				
		$ar_new_seq = explode(' ', $r['new_sequence']);
		$ar_old_seq = explode(' ', $r['old_sequence']);
		
		$ar_new_seq = preg_split("/[\r\n]+/", $r['new_sequence']); //seraro per BR eLF
		$ar_old_seq = preg_split("/[\r\n]+/", $r['old_sequence']); //seraro per BR eLF

		//elimino eventuali valori vuoti
		$ar_new_seq = remove_empty($ar_new_seq);		        		
		$ar_old_seq = remove_empty($ar_old_seq);		

		//se ha richiesto l'ordine inverso
		if ($r['inverse'] == 'Y'){
			$ar_new_seq = array_reverse($ar_new_seq);
		}
		
		//rigenero le vecchie sequenze		
		$ar_tmp = array();
		foreach ($ar_old_seq as $ks => $ns){
			$t = explode("##", $ns);
			$ar_tmp[$t[0]] = $t[1]; 
		}
	    $ar_old_seq = $ar_tmp;

		$seq = 0;				
		foreach ($ar_new_seq as $ks => $ns){
			if ($ns == 1) continue; //come prima e l'ultima ho la sequenza ho la start MAP
			
			$seq++;
			$k_ord = explode("|", $ar_old_seq[$ns]);

			//recupero l'elenco degli ordini da modificare
			$sql = "SELECT * FROM  {$cfg_mod_Spedizioni['file_testate']}
					WHERE " . $this->get_where_std() . " 
					  AND TDDTEP = " . $k_ord[0] . " 
					  AND TDCITI = " . sql_t($k_ord[1]) . "					  
					  AND TDVETT = " . sql_t($k_ord[2]) . "					  
					  AND TDTPCA = " . sql_t($k_ord[3]) . " 
					  AND TDAACA = " . sql_t($k_ord[4]) . "					  
					  AND TDNRCA = " . sql_t($k_ord[5]) . "					  
					  AND TDCCON = " . sql_t($k_ord[6]) . "
					  AND TDCDES = " . sql_t($k_ord[7]) . "					  					  
					";
						
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);
			
			
			//per ogni ordine
			while ( $row_upd = db2_fetch_assoc($stmt) ){
				$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
							SET TDSECA = " . sql_t(sprintf("%02s", $seq)) . "
							WHERE " . $this->get_where_std() . "
							AND TDDOCU = ?";
				$stmt_upd = db2_prepare($conn, $sql_upd);
				$result = db2_execute($stmt_upd, array($row_upd['TDDOCU']));

				$sh = new SpedHistory();
				$sh->crea(
						'assegna_seq_carico',
						array(
								"k_ordine" 	=> $row_upd['TDDOCU'],
								"seq" 		=> sprintf("%02s", $seq)
						)
				);				
			}			
			
			
			
		}
		
 }	

 
 


 function exe_progetta_spedizioni($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 	
 
 	foreach ($r->selected_id as $k_grid => $s_grid){
 		$seq = 0;
 
 		$sped_car_exp = explode("|", $r->sped_car_id[$k_grid]);
 		$sped_id = $sped_car_exp[0];
 		$sped = $this->get_spedizione($sped_id);
 		$ar_carico = $this->k_carico_td_decode($sped_car_exp[1]);
 
 		//spedizione di provenienza
 		if ($k_grid == 0)
 			$k_grid_from = 1;
 		else
 			$k_grid_from = 0;
 			
 		$sped_car_exp_from = explode("|", $r->sped_car_id[$k_grid_from]);
 		$sped_id_from 		= $sped_car_exp_from[0];
 		$carico_exp_from	= explode("_", $sped_car_exp_from[1]);
 		$nr_carico_from = $carico_exp_from[2];
 			
 			
 		foreach ($s_grid as $s){
 			$seq++;
 
 			if ($_REQUEST['from_gmap'] == 'Y'){
 				//da gmap ricevo in ar_cli_des l'array dei clienti selezionati (perche' vengono raggruppati per lat/lng)
 				$sql_where_k_cli_des = " AND CONCAT(TDDT, CONCAT('_', CONCAT(TDCCON, CONCAT('_', CONCAT(TDCDES, CONCAT('_', CONCAT(TDTDES, CONCAT('_', CONCAT(TDVET1, CONCAT('_', TDVET2)))))))))) IN (" . sql_t_IN($s->ar_cli_des_ext) . ") ";
 				$sql_ar_par = array($sped_id_from, $nr_carico_from);
 			} else {
 				$sql_where_k_cli_des = " AND TDCCON = " . sql_t($s->TDCCON) . " AND TDCDES = " . sql_t($s->TDCDES);
 				$sql_where_k_cli_des .= "AND TDDLOC = ? AND TDPROD = ? AND TDDCAP = ?";
 				$sql_ar_par = array($s->TDDLOC, $s->TDPROD, $s->TDDCAP, $sped_id_from, $nr_carico_from);
 			}
 
 			//$row['k_cli_des'] = implode("_", array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));
 
 
 			//recupero l'elenco degli ordini da modificare
 			$sql = "SELECT * FROM  {$cfg_mod_Spedizioni['file_testate']}
 			WHERE " . $this->get_where_std() . "
 			AND TDDTEP = " . $r->data . "
 			AND TDCITI = " . sql_t($r->cod_iti) . "
 				
 			" . $sql_where_k_cli_des . "
 			 
 
 			AND TDNBOC = ? AND TDNRCA = ?
	 				";
 
 			$stmt = db2_prepare($conn, $sql);
 			echo db2_stmt_errormsg();
 			$result = db2_execute($stmt, $sql_ar_par);
 
 			//per ogni ordine
 			while ( $row_upd = db2_fetch_assoc($stmt) ){
 			//solo se e' cambiato il numero di spedizioni o il carico
 				$ord_carico = $this->k_carico_td($row_upd);
 				if ($ord_carico != $sped_car_exp[1] || $row_upd['TDNBOC'] != $sped_id)
 					$result = $this->assegna_spedizione_a_ordine($row_upd, $sped, $ar_carico, '', $r, 'CMP');
 			}
 			}
 
 	}
 
 }
 
  
 
 
 
 
 function assegna_spedizione_a_ordine($ord, $sped, $ar_carico = null, $seq = null, $r = null, $op_h = ''){
 	global $conn, $cfg_mod_Spedizioni;
 	
 	$k_ord = $ord['TDDOCU'];
 	$to_sped_id = $sped['CSPROG'];
 	$to_carico  = $this->k_carico_td_encode($ar_carico); 	
 	
 	if ((int)$ar_carico['TDNRCA'] > 0)
 		$tdfn01 = 1;
 	else
 		$tdfn01 = 0;
 	
 	if (trim($ord['TDSWSP']) == 'N') { //da hold ... confermo subito la data
 		$new_TDDTVA = $sped['CSDTSP'];
 		$new_TDFN06 = 1;
 		$registra_data_confermata = 'Y';
 	} else {
 		//altrimenti lascio quella precedentemente confermata dall'operatore
 		$new_TDDTVA = $ord['TDDTVA'];
 		$new_TDFN06 = $ord['TDFN06'];
 		$registra_data_confermata = 'N';
 	}
 	
 	$new_TDSECA =  ''; //azzero sequenza  		 		
 		
 	if (strlen($ar_carico['TDAACA']) == 0) $ar_carico['TDAACA'] = 0; 
 	if (strlen($ar_carico['TDNRCA']) == 0) $ar_carico['TDNRCA'] = 0;

	//verifico se abbinare la sequenza e/o deposito (in base a sped/carico/cliente/dest)
	$ar_upd = $this->verifica_abbinamento_nel_carico($ord, $to_sped_id, $ar_carico);
 	
 	
	$sql_set = "";
	//gestione data di spedizione
	if ($sped['CSDTPG'] > 0){
		$sql_set .= " , TDDTSP = {$sped['CSDTPG']}";
		$sql_set .= " , TDDTCP = {$sped['CSDTPG']}";
	}	
	
	
	//Mantieni allineato TDNBOC|TDNBOF (o riallinea se azzero carico)
	if ($ord['TDNBOC'] == $ord['TDNBOF'] || $tdfn01 == 0)
	  $sql_allinea_TDNBOF = ", TDNBOF = {$to_sped_id}";
	else 
	  $sql_allinea_TDNBOF = "";
	  
    if ($tdfn01 == 1) {    
      $esegui_sincronizza = false;
      
      //assegno ad un carico:
      //il TDNBOF lo prendo dal primo ordine della stessa spedizione/carico (andranno via di sicuro insieme)
      $sql_spe_ca = "SELECT * FROM  {$cfg_mod_Spedizioni['file_testate']}
						 WHERE " . $this->get_where_std() . "
						 AND TDNBOC = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ?";
      
      $stmt_spe_ca = db2_prepare($conn, $sql_spe_ca);
      $result = db2_execute($stmt_spe_ca, array($to_sped_id,
          $ar_carico['TDTPCA'], $ar_carico['TDAACA'], $ar_carico['TDNRCA']));
      
      $row_spe_ca = db2_fetch_assoc($stmt_spe_ca);
      if ($row_spe_ca) { //Se trovato associo TDNBOF
          $sql_allinea_TDNBOF = ", TDNBOF = {$row_spe_ca['TDNBOF']}";
          $esegui_sincronizza = true;
      }
    }

	
 	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
 						TDNBOC = " . $to_sped_id . "
                        {$sql_allinea_TDNBOF}
 						, TDSWSP = 'Y' /* TODO: e' giusto?? */
						, TDTPCA = " . sql_t($ar_carico['TDTPCA']) . "
						, TDAACA = " . $ar_carico['TDAACA'] . "
 						, TDNRCA = " . $ar_carico['TDNRCA'] . "
 						, TDFN01 = $tdfn01
 						, TDDTVA = $new_TDDTVA
 						, TDFN06 = $new_TDFN06
 						
						, TDSECA = ?, TDTDES = ?, TDVET1 = ?, TDVET2 = ?, TDDDES = ?, TDIDES = ?, TDDCAP = ?, TDDLOC = ?, TDPROD = ? 
 	
 						, TDDTEP = " . $sped['CSDTSP'] . "
 						, TDCITI = " . sql_t(trim($sped['CSCITI'])) . "
 						, TDVETT = " . sql_t(trim($sped['CSCVET'])) . "
 						, TDAUTO = " . sql_t(trim($sped['CSCAUT'])) . "
 						, TDCOSC = " . sql_t(trim($sped['CSCCON'])) . $sql_set . "
 	
 						WHERE " . $this->get_where_by_k_ordine($k_ord) . " AND " . $this->get_where_std() . "
 						";
 								
 	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($ar_upd['TDSECA'], $ar_upd['TDTDES'], $ar_upd['TDVET1']
		,$ar_upd['TDVET2'], $ar_upd['TDDDES'], $ar_upd['TDIDES']
		,$ar_upd['TDDCAP'], $ar_upd['TDDLOC'], $ar_upd['TDPROD']));
 	
 		
 	
	$sh = new SpedHistory();
	$sh->crea(
				'assegna_spedizione',
			 	array(
			 			"k_ordine" 	=> $k_ord,
			 			"data"		=> $sped['CSDTSP'],
			 			"k_carico"	=> $to_carico,
			 			"sped"		=> $sped,
			 			"registra_data_confermata" => $registra_data_confermata,
			 			"data_confermata" => $sped['CSDTSP'],
			 			"op"		=> $op_h,
						"ex"		=> "(#{$ord['TDNBOC']},C:{$ord['TDNRCA']},Sw:{$ord['TDSWSP']})",

						"seca"		=> $ar_upd['TDSECA'],
						
						"RIIVES"	=> $ar_upd['RIIVES'],
						"RICVES"	=> $ar_upd['RICVES'],
												
						)
				); 	
	
	if ($esegui_sincronizza == true){
	    $sh = new SpedHistory();
	    $sh->crea(
	        'sincronizza_ordine',
	        array(
	            "k_ordine" 	=> $k_ord)
	        );
	}
 	
 	
 	if ($r->f_data > 0){
	 	//memorizzo la autorizzazione deroga
	 	$sh = new SpedAutorizzazioniDeroghe();
	 	$sh->crea(
 					'assegna_spedizione',
 					array(
 								"k_ordine" 		=> $k_ord,
								"data"			=> $r->f_data,
 								"commento"		=> $r->f_commento,
 								"autorizzazione"=> $r->f_autorizzazione,
 								"data_prog"		=> $r->f_data_programmata,
 								"data_disp"		=> $r->f_data_disponibilita
 					)
 			);
 	}


 	
 } 
 
 
 
 
 
 
 


 function exe_progetta_spedizioni_applica_sequenza($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 
 	
 	if ($r->no_stacco_carico == 'Y'){
 	    $sped_id = $r->sped_car_id;
 	} else { 	
     	$sped_car_id_exp = explode("|", $r->sped_car_id);
     	$sped_id = $sped_car_id_exp[0];
     	$carico	 = explode("_", $sped_car_id_exp[1]);
     	$nr_carico = $carico[2]; 
 	}
     	
 	$ind_ultimo_scarico = '--------';
 	
 	$seq = 0;
 	foreach ($r->selected_id as $s){
 		 		
 		$ind_scarico_attuale = implode("|||", array($s->TDDLOC, $s->TDPROD, $s->TDDCAP, $s->TDIDES)); 		

 		//a parita' di indirizzo non incremento la sequenza (es: per deposito, o per consegne ad agente..)
 		if ($ind_scarico_attuale != $ind_ultimo_scarico)
 		{
 			$seq++;
 			$ind_ultimo_scarico = $ind_scarico_attuale;
 		}
 		
 		$sql_params = array(utf8_decode($s->TDDLOC), utf8_decode($s->TDPROD), 
 		                     $s->TDDCAP, utf8_decode($s->TDIDES), $sped_id);
 		
 		if ($r->no_stacco_carico != 'Y'){
 		    $sql_where_carico = ' AND TDNRCA = ? ';
 		    $sql_params[] = $nr_carico;
 		} else {
 		    $sql_where_carico = '';
 		}
 		
 		//recupero l'elenco degli ordini da modificare
 		$sql = "SELECT * FROM  {$cfg_mod_Spedizioni['file_testate']}
 						WHERE " . $this->get_where_std() . "
 						AND TDDTEP = " . $r->data . "
 						AND TDCITI = " . sql_t($r->cod_iti) . "
					  	AND TDCCON = " . sql_t($s->TDCCON) . "
					  	AND TDCDES = " . sql_t($s->TDCDES) . "
				 		AND TDDLOC = ? AND TDPROD = ? AND TDDCAP = ? AND TDIDES = ?
					    AND TDNBOC = ? {$sql_where_carico}					  			
 					  ";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $sql_params);
 						
 		//per ogni ordine
 		while ( $row_upd = db2_fetch_assoc($stmt) ){

 				$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
				 				SET TDSECA = " . sql_t(sprintf("%02s", $seq)) . "
			 				WHERE " . $this->get_where_std() . "
 							AND TDDOCU = ?";

	 					$stmt_upd = db2_prepare($conn, $sql_upd);
	 					$result = db2_execute($stmt_upd, array($row_upd['TDDOCU']));
	 
	 					$sh = new SpedHistory();
	 					$sh->crea(
	 						'assegna_seq_carico',
	 						array(
	 						"k_ordine" 	=> $row_upd['TDDOCU'],
	 						"k_carico"	=> $this->k_carico_td_encode($row_upd),	 								
	 						"seq" 		=> sprintf("%02s", $seq)
	 						)
	 					);
 		}
 			
		
	}
 
 }
 

 
 

 
 function exe_progetta_spedizioni_applica_sequenza_percorso($r){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	$ar = array();
 
 	$sped_car_id_exp = explode("|", $r->sped_car_id);
 	$sped_id = $sped_car_id_exp[0];
 	$carico	 = explode("_", $sped_car_id_exp[1]);
 	$nr_carico = $carico[2];
 
 	$seq = 0;
 	foreach ($r->seq_coordinate as $s){

		$seq++;
 			
 		//recupero l'elenco degli ordini da modificare
 		$sql = "SELECT * FROM  {$cfg_mod_Spedizioni['file_testate']} TD
 				/*
 		          LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_coordinate_geografiche']} GL_CLI
 		             ON TD.TDTDES NOT IN ('1', '2') AND GL_CLI.GLDT = TD.TDDT AND GL_CLI.GLTP = 'CLI' AND GL_CLI.GLCANA = TD.TDCCON AND GL_CLI.GLCDES = TD.TDCDES
 		          LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_coordinate_geografiche']} GL_VET1
 		             ON TD.TDTDES IN ('1') AND GL_VET1.GLDT = TD.TDDT AND GL_VET1.GLTP = 'TRA' AND GL_VET1.GLCANA = TD.TDVET1
 		          LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_coordinate_geografiche']} GL_VET2
 		             ON TD.TDTDES IN ('2') AND GL_VET2.GLDT = TD.TDDT AND GL_VET2.GLTP = 'TRA' AND GL_VET2.GLCANA = TD.TDVET2
 		        */        
		 		WHERE " . $this->get_where_std() . "
		 		AND TDDTEP = " . $r->data . "
		 		AND TDCITI = " . sql_t($r->cod_iti) . "
				AND TDNBOC = ? AND TDNRCA = ?
		 		/*		
		 		 AND (
		 				(GL_CLI.GLLAT = ? AND GL_CLI.GLLNG = ?)
		 			OR  (GL_VET1.GLLAT = ? AND GL_VET1.GLLNG = ?)
		 			OR  (GL_VET2.GLLAT = ? AND GL_VET2.GLLNG = ?)
		 		 )
		 		*/		
		 		";
 		
			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt, array($sped_id, $nr_carico
 					//, trim($s->LAT), trim($s->LNG)
 					//, trim($s->LAT), trim($s->LNG)
 					//, trim($s->LAT), trim($s->LNG)
 			));
 
 
		 		//per ogni ordine
		 		while ( $row_upd = db2_fetch_assoc($stmt) ){
		 			
		 		//Verifico che combaciano le coordinate
		 		$gmap_ind = $this->get_gmap_ind($row_upd);
		 		$row_coordinate = $this->get_coordinate_by_ind($gmap_ind);
		 		
		 		if (is_null($row_coordinate) == true || $row_coordinate['GLLAT'] != $s->LAT || $row_coordinate['GLLNG'] != $s->LNG)
		 			continue; //salto questo ordine

		 		$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
		 			SET TDSECA = " . sql_t(sprintf("%02s", $seq)) . "
		 			WHERE " . $this->get_where_std() . "
		 			AND TDDOCU = ?";
		 
			 					$stmt_upd = db2_prepare($conn, $sql_upd);
			 					$result = db2_execute($stmt_upd, array($row_upd['TDDOCU']));
		 
		 	 					$sh = new SpedHistory();
		 	 					$sh->crea(
		 	 						'assegna_seq_carico',
			 	 					array(
			 	 						"k_ordine" 	=> $row_upd['TDDOCU'],
				 						"k_carico"	=> $this->k_carico_td_encode($row_upd),
			 	 						"seq" 		=> sprintf("%02s", $seq)
			 						)
		 			);
		 		}
 
 
 		}
 
 	}
 
 
 
 function get_gmap_ind($row){
 	return implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));
 }	

 
 

 
 
 
 
 
 function exe_progetta_spedizioni_memorizza_coordinate($r){
 	global $conn, $auth;
 	global $cfg_mod_Spedizioni;
 	$ar = array();

 	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_coordinate_geografiche']}(GLLAT, GLLNG, GLADR, GLVAL, GLDTGE, GLHRGE, GLUSGE, GLMANU)
				VALUES(?,?,?,?,?,?,?,?)";
 	$stmt = db2_prepare($conn, $sql); 	
 	
 	$adesso_data 	= oggi_AS_date();
 	$adesso_ora  	= oggi_AS_time();
 	$utente			= $auth->get_user();
 	$manuale		= 'N';
 	
 	foreach ($r->selected_id as $s){
 		
 		if ($s->ha_coordinate != 'Y'){
	 		
	 		$gllat = $s->lat;
	 		$gllng = $s->lng;
	 		$gladr = $s->gmap_ind;
	 		$glval = $s->valido;
	 		
	
	 		$result = db2_execute($stmt, array($gllat, $gllng, $gladr, $glval
	 											,$adesso_data, $adesso_ora, $utente, $manuale));
	 		
			if (!$result)
				echo db2_stmt_errormsg($stmt);
 		}	 
 
 	} //foreach
 
 } //function
 
 

 public function get_coordinate_by_ind($ind){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_coordinate_geografiche']} WHERE GLADR = ?";
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array(trim($ind)));
 	$row = db2_fetch_assoc($stmt);
 	return $row;
 }
 
 
 public function get_coordinate($gldt, $glcana, $glcdes, $vettore_id, $ind_confronto = ''){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_coordinate_geografiche']} WHERE GLADR = ?";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($ind_confronto));
 	$row = db2_fetch_assoc($stmt);
 
 	return $row; 
 }
 
 
 
 public function delete_get_coordinate($gldt, $glcana, $glcdes, $vettore_id, $ind_confronto = ''){
 	global $conn;
 	global $cfg_mod_Spedizioni; 	
 	
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_coordinate_geografiche']}
 			WHERE GLDT = ? AND GLTP = ? AND GLCANA = ? AND GLCDES = ?";
 		
 	if (trim($vettore_id) == ''){
 		$gltp	= 'CLI';
 		$glcana = $glcana;
 		$glcdes = $glcdes;
 	} else {
 		//e' un deposito/trasportatore (exchange)
 		$gltp	= 'TRA';
 		$glcana = $vettore_id;
 		$glcdes = '';
 	}
 	
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($gldt, $gltp, $glcana, $glcdes)); 	
 	$row = db2_fetch_assoc($stmt);

 	if ($row == false)	return $row; //non trovato

 	//se e' stato passato, lo confronto con l'indirizzo di confronto.
 	if (strlen(trim($ind_confronto)) > 0){
 		if (trim($ind_confronto) != trim($row['GLADR'])){
 			// Se e' diverso elimino la riga 			
 			$sql_del = "DELETE FROM {$cfg_mod_Spedizioni['file_coordinate_geografiche']} WHERE GLID = ?";
	 		$stmt_del = db2_prepare($conn, $sql_del);
	 		$result = db2_execute($stmt_del, array($row['GLID']));
	 		return false; 		
 		}
 	}
 	
 	return $row; 	
 } 
 

 
 public function exe_upd_coordinate($p){
 	global $conn, $auth;
 	global $cfg_mod_Spedizioni;
 
 	$adesso_data 	= oggi_AS_date();
 	$adesso_ora  	= oggi_AS_time();
 	$utente			= $auth->get_user();
 	$manuale		= 'Y'; 	
 	
 	$sql = "UPDATE {$cfg_mod_Spedizioni['file_coordinate_geografiche']}
 			SET GLLAT = ?, GLLNG = ?, GLVAL = 'Y', GLDTGE = ?, GLHRGE = ?, GLUSGE = ?, GLMANU = ?
 			WHERE GLID = ?";
 
 			$stmt = db2_prepare($conn, $sql);
 			$result = db2_execute($stmt, array($p['f_lat'], $p['f_lng'], $adesso_data, $adesso_ora, $utente, $manuale, $p['GLID']));
 			return $row;
 }
 
 


 function graph_dati_settimana_tipologi_area($tipo, $ar_to_merge = array()){
 		global $conn;
		global $cfg_mod_Spedizioni;
		$ar = array();
		
		$oggi = oggi_AS_date();
		
		$g_field = "TDASPE, TDCLOR, CSAARG, CSNRSE";
		$g_field_2 = "TDASPE, TDCLOR, 0, 0";		
		$s_field = " COUNT(*) AS T_ROW ";
		$o_field = " CSAARG, CSNRSE, TDASPE ";
		  		
		$sql = "
				SELECT TDASPE, TDCLOR AS TDCLOR, 0 AS CSAARG, 0 AS CSNRSE , $s_field 
				FROM {$cfg_mod_Spedizioni['file_calendario']} 
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} ON TDDTEP = CSDTRG AND " . $this->get_where_std() . "  
				WHERE CSCALE = '*CS' 
				 AND ( (TDSWSP='Y' AND TDDTEP <= " . $oggi . ") OR (TDSWSP = 'N'))
				GROUP BY $g_field
				
				UNION

				SELECT TDASPE, TDCLOR AS TDCLOR, CSAARG AS CSAARG, CSNRSE AS CSNRSE, $s_field 
				FROM {$cfg_mod_Spedizioni['file_calendario']} 
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} ON TDDTEP = CSDTRG AND " . $this->get_where_std() . "  
				WHERE CSCALE = '*CS' AND TDSWSP='Y' AND TDDTEP > " . $oggi . "
				GROUP BY $g_field
				ORDER BY $o_field				
								  
				";		
		
		//$sql .= " ORDER BY $o_field";		
		//echo $sql;
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		
		$ar = array();
		$ar_c = array();
		
		$ar_records = array();
		while ($r = db2_fetch_assoc($stmt)){
			$ar_records[] = $r;
		}

		
		//elenco etichette Y (perche' vuole sempre un valore per ogni etichetta, quindi al limite e' 0)
		$ar_campi = array();		
		foreach($ar_records as $r)
		{
					$l2 = implode("-", array(trim($r['TDASPE']) , trim($r['TDCLOR'])));			
					$ar_campi[$l2] = 0;
		}
		
		
		
//		while ($r = db2_fetch_assoc($stmt))
		foreach($ar_records as $r)
		{
			$l1 = implode("-", array(trim($r['CSAARG']) , trim($r['CSNRSE'])));
			$l2 = implode("-", array(trim($r['TDASPE']) , trim($r['TDCLOR'])));
			if (!isset($ar[$l1]))
				$ar[$l1] = array_merge(array("name" => $l1), $ar_campi);
			$ar[$l1][$l2] = + $r['T_ROW'];				
			if (!isset($ar_c[$l2]))
				$ar_c[$l2] = array();			

		}


		
		switch($tipo){
			case "dati":
				$ar_ret = array();
				foreach($ar as $ar1)
					$ar_ret[] = $ar1;				
				return acs_je($ar_ret);				
			break;			
			case "campi":
				$ar_ret = $ar_to_merge;
				foreach($ar_c as $k_ar=>$ar)
					$ar_ret[] = "$k_ar";
				return acs_je($ar_ret);				
			break;			
		} 

			
 }








	
	function des_vmc_sp($row){
		 $vet = $this->decod_std('AUTR', $row['CSCVET']);
		 $mez = $this->decod_std('AUTO', $row['CSCAUT']);
		 $cas = $this->decod_std('AUTO', $row['CSCCON']);
	  return implode(" ", array($vet, $mez, $cas)); 		
	}
	


	function k_ordine_out($k_ordine){
		$ar = $this->k_ordine_td_decode($k_ordine);

		global $cfg_mod_Spedizioni;
		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y')		
			return implode("_", array($ar['TDOADO'], trim($ar['TDONDO']), $ar['TDPROG']));
		else 	
			return implode("_", array($ar['TDOADO'], trim($ar['TDONDO'])));
	}	
	function k_ordine_td($row){
		return implode("_", array($row['TDDT'], $row['TDOTID'], $row['TDOINU'], $row['TDOADO'], $row['TDONDO']));
	}	
	function k_ordine_td_decode($ordine){
		$ar = explode("_", $ordine);
		$r = array();
		$r['TDDT'] 		= $ar[0];
		$r['TDOTID'] 	= $ar[1];		
		$r['TDOINU'] 	= $ar[2];
		$r['TDOADO'] 	= $ar[3];		
		$r['TDONDO'] 	= $ar[4];
		
		global $cfg_mod_Spedizioni;		
		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			$r['TDPROG'] 	= $ar[5];
		}
		
	 return $r;	
	}
	
	function k_ordine_td_decode_prog($ordine){
		$ar = explode("_", $ordine);
		$r = array();
		$r['TDDT'] 		= $ar[0];
		$r['TDOTID'] 	= $ar[1];
		$r['TDOINU'] 	= $ar[2];
		$r['TDOADO'] 	= $ar[3];
		$r['TDONDO'] 	= $ar[4];
		$r['TDPROG'] 	= $ar[5];
	
		return $r;
	}
	

	function k_ordine_td_decode_xx($ordine){

		$r = $this->k_ordine_td_decode($ordine);
	
		/* GESTIONE MULTI DITTA */
		$exp_ord = explode("-", $r['TDONDO']);
		if (sizeof($exp_ord) == 2){ //sono nel formato NUMERO-DITTA
			$r['TDONDO'] 	= $exp_ord[0];
			$r['TDDT']		= $exp_ord[1];
		}
	
		return $r;
	}	
	
	

	function k_ordine_to_anno_numero($k_ordine, $to_anno, $to_numero){
		$oe = explode("_", $k_ordine);
		$l_num = strlen($oe[4]);
		$oe[3] = $to_anno;
		$oe[4] = sprintf("%-{$l_num}s", $to_numero);
		return implode("_", $oe);
	}
	
	
	
	
	
	
	function k_carico_td($row){
		return implode("_", array((int)$row['TDAACA'], $row['TDTPCA'], (int)$row['TDNRCA']));
	}
	
	
	function k_carico_td_encode($ar_td){
		if (strlen($ar_td['TDAACA'])==0) $ar_td['TDAACA'] = 0;
		if (strlen($ar_td['TDTPCA'])==0) $ar_td['TDTPCA'] = "     ";		 
		if (strlen($ar_td['TDNRCA'])==0) $ar_td['TDNRCA'] = 0;		
		return implode("_", array($ar_td['TDAACA'], $ar_td['TDTPCA'], $ar_td['TDNRCA']));
	}	
	
	function k_carico_td_decode($carico){
		$ar = explode("_", $carico);
		$r = array();
		$r['TDAACA'] = $ar[0];
		$r['TDTPCA'] = $ar[1];		
		$r['TDNRCA'] = $ar[2];
	 return $r;	
	}	
	

	function get_where_by_k_ordine($k_ord){
		$oe = $this->k_ordine_td_decode($k_ord);
		$ret =  "
			TDDT=" . sql_t($oe['TDDT']) . " AND TDOTID=" . sql_t($oe['TDOTID'])  . " 
				  AND TDOINU=" . sql_t($oe['TDOINU']) . " AND TDOADO=" . sql_t($oe['TDOADO'])  . " AND TDONDO=" . sql_t($oe['TDONDO']);

		global $cfg_mod_Spedizioni;
		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			$ret .= " AND TDPROG=" . sql_t($oe['TDPROG']);
		}		
		
		return $ret;
	}
	
	function get_where_by_k_ordine_prog($k_ord){
		$oe = $this->k_ordine_td_decode_prog($k_ord);
		return "
			TDDT=" . sql_t($oe['TDDT']) . " AND TDOTID=" . sql_t($oe['TDOTID'])  . "
				  AND TDOINU=" . sql_t($oe['TDOINU']) . " AND TDOADO=" . sql_t($oe['TDOADO'])  . " 
				  AND TDONDO=" . sql_t($oe['TDONDO']) . " AND TDPROG=" . sql_t($oe['TDPROG']);
	}	
	
	function get_where_by_ordine_td($row){
		$oe = $row;
		return "
			TDDT=" . sql_t($oe['TDDT']) . " AND TDOTID=" . sql_t($oe['TDOTID'])  . "
				  AND TDOINU=" . sql_t($oe['TDOINU']) . " AND TDOADO=" . sql_t($oe['TDOADO'])  . " AND TDONDO=" . sql_t($oe['TDONDO']);
	}	
	
	
 public function get_titolo_panel_by_tipo($tipo){
 	return ucfirst(strtolower($tipo));
 }	
	
	
 public function ha_con_ordini_reso($tddt, $tdccon, $tdcdes){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 	
 	$sql = "SELECT COUNT(*) AS COUNT_R FROM {$cfg_mod_Spedizioni['file_testate']} 
 			WHERE TDDT = " . sql_t($tddt) . " AND TDCCON = " . sql_t($tdccon) . " AND TDCDES = " . sql_t($tdcdes) .
 			" AND TDOTID = 'VO' AND TDOINU = '' AND TDOADO = 0
 			  AND TDSWPP = 'R'
 			";
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);

 	$row = db2_fetch_assoc($stmt); 	
 	$n_row = $row['COUNT_R'];
 	if ( (int)$n_row > 0 ) return true;
 	return false;
 }	
 
 public function ha_mezzi($cliente, $destinazione){
     global $conn, $id_ditta_default;
     global $cfg_mod_Spedizioni;
     
     
     $sql = "SELECT COUNT(*) AS COUNT_R
             FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
             INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_MZ
             ON TA.TADT = TA_MZ.TADT AND TA_MZ.TATAID = 'MZESL' AND TA.TAKEY1 = TA_MZ.TAKEY3
             AND TA_MZ.TAKEY1 = '{$cliente}' AND TA_MZ.TAKEY2 = '{$destinazione}'
             WHERE TA.TADT = '{$id_ditta_default}' AND TA.TATAID = 'AUTO'";
     
     $stmt = db2_prepare($conn, $sql);
     $result = db2_execute($stmt);
     
     $row = db2_fetch_assoc($stmt);
     $n_row = $row['COUNT_R'];
     if ( (int)$n_row > 0 ) return true;
     return false;
 }
 
 
 public function get_tooltip_mezzi($cliente, $destinazione, $mezzo = null){
     global $conn, $id_ditta_default;
     global $cfg_mod_Spedizioni;
     $ret = array();
     
     $sql = "SELECT TA_MZ.TAKEY3 AS C_MEZZO, TA.TADESC AS D_MEZZO, TA_MZ.TADESC AS NOTA, TA_MZ.TAKEY4 AS I_E
             FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
             INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_MZ
             ON TA.TADT = TA_MZ.TADT AND TA_MZ.TATAID = 'MZESL' AND TA.TAKEY1 = TA_MZ.TAKEY3
             AND TA_MZ.TAKEY1 = '{$cliente}' AND TA_MZ.TAKEY2 = '{$destinazione}'
             WHERE TA.TADT = '{$id_ditta_default}' AND TA.TATAID = 'AUTO'";
     
     $stmt = db2_prepare($conn, $sql);
     $result = db2_execute($stmt);
     
     while($row = db2_fetch_assoc($stmt)){
        if(trim($row['I_E']) == 'I')
             $i_e = 'INCLUSI';
         else $i_e = 'ESLCUSI';
         $tooltip[] = "[".trim($row['C_MEZZO'])."] ".trim($row['D_MEZZO'])." - " .trim($row['NOTA']);
         $ar_mezzi[] = trim($row['C_MEZZO']);
     }
     
     $ret['tooltip'] = "<B>Mezzi {$i_e}:</B><br>" . implode("<br>", $tooltip);
     $ret['segnala_mezzo'] = false;
     if(!is_null($mezzo)){
         if($i_e == 'INCLUSI' && !in_array($mezzo, $ar_mezzi))
            $ret['segnala_mezzo'] = true;
         
         if($i_e == 'ESLCUSI' && in_array($mezzo, $ar_mezzi))
            $ret['segnala_mezzo'] = true;
         
     }  
      
     return $ret;
 }
 
 
 public function ha_note_cliente($tddt, $tdccon){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	//note da gestionale (legate solo al cliente)
 	$sql = "SELECT COUNT(*) AS COUNT_R FROM {$cfg_mod_Spedizioni['file_righe_note_cli']}
 			WHERE RDDT = " . sql_t($tddt) . " AND RDONDO = " . sql_t($tdccon); // . " AND RDTPNO = 'NCSV2' "; e' gia' filtrato il logico
 
 			$stmt = db2_prepare($conn, $sql);
 			echo db2_stmt_errormsg();
 			$result = db2_execute($stmt);
 
 			$row = db2_fetch_assoc($stmt);
 			$n_row = $row['COUNT_R'];
 			$n_row = (int)$n_row;
 			
 	//note acs_portal (CLIENTE)		
 	if ($this->ha_note_cliente_portal($tddt, $tdccon, '') == true) $n_row += 1; 	
 			
 	if ( (int)$n_row > 0 ) return true;
  	return false;
 } 
 
 
 public function ha_note_cliente_dest($tddt, $tdccon, $tdcdes){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$n_row = 0;
 
 	if (strlen(trim($tdcdes)) == 0) return false;
 	
 	//note acs_portal (CLIENTE)
 	if ($this->ha_note_cliente_portal($tddt, $tdccon, $tdcdes) == true) $n_row += 1;
 	if ( (int)$n_row > 0 ) return true;
 		return false;
 } 
 
 public function get_note_cliente_gest($tddt, $tdccon, $blocco = 'NCSV2'){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_righe']}
 			WHERE RDDT = " . sql_t($tddt) . " AND RDONDO = " . sql_t($tdccon) . " AND RDTPNO = '$blocco'
 			ORDER BY RDRINO";
 	
 	/*print_r($sql);
 	exit;*/
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt);
	return $stmt;
 } 
 
 
 
 public function ha_note_cliente_portal($tddt, $tdccon, $tdcdes){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$sql = "SELECT COUNT(*) AS COUNT_R FROM {$cfg_mod_Spedizioni['file_note']} WHERE NTDT=? AND NTKEY1=? AND NTKEY2=? AND NTTPNO = 'NDOVE'";
 
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg();
 	$result = db2_execute($stmt, array($tddt, $tdccon, $tdcdes));
 	$row = db2_fetch_assoc($stmt);
 	$n_row = $row['COUNT_R'];
 	if ( (int)$n_row > 0 ) return true;
  	return false;
 } 
 
 public function get_note_cliente_portal($tddt, $tdccon, $tdcdes){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_note']}
 				WHERE NTDT=? AND NTKEY1=? AND NTKEY2=? AND NTTPNO = 'NDOVE'";

// 	echo $sql;
// 	print_r(array($tddt, $tdccon, $tdcdes));
 	
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();  	
  	$result = db2_execute($stmt, array($tddt, $tdccon, $tdcdes));
  	$row = db2_fetch_assoc($stmt);
  	if ($row == false) return '';
  	return trim($row['NTMEMO']);
 } 
 
 public function update_note_cliente_portal($tddt, $tdccon, $tdcdes, $txt){
 	global $conn;
 	global $cfg_mod_Spedizioni;
 
 	$ha_note = $this->ha_note_cliente_portal($tddt, $tdccon, $tdcdes);
 	if ($ha_note == true){
 		
 		if (strlen(trim($txt)) == 0) //nota vuota
 			//DELETE
 			$sql = "DELETE FROM {$cfg_mod_Spedizioni['file_note']} WHERE ('__-__'=?) OR (NTDT=? AND NTKEY1=? AND NTKEY2=? AND NTTPNO = 'NDOVE')";
 		else
 			//UPDATE	
 			$sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} SET NTMEMO=?
 						WHERE NTDT=? AND NTKEY1=? AND NTKEY2=? AND NTTPNO = 'NDOVE'";
 	} else {
 		
 		if (strlen(trim($txt)) == 0) //nota vuota 		
 			$sql = "";
 		else
 			//INSERT
 			$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(NTMEMO, NTDT, NTKEY1, NTKEY2, NTSEQU, NTTPNO)
 						VALUES(?, ?, ?, ?, 0, 'NDOVE')"; 		
 	}
 	
 	if ($sql != ""){
 		$stmt = db2_prepare($conn, $sql);
 		echo db2_stmt_errormsg();
 		$result = db2_execute($stmt, array($txt, $tddt, $tdccon, $tdcdes));
 	}	 	
 } 
 
 
 

 
 public function exe_call_pgm($p){
 	global $tkObj;
 	global $libreria_predefinita, $libreria_predefinita_EXE;
 	
 	$m_pgm = explode("|", $p['f_pgm']);
 	
 	//$m_pgm[0] = programm
 	//$m_pgm[1] = library
 	
	//costruisco il parametro
 	$cl_p = str_repeat(" ", 246);
 	$cl_p .= $p['user_parameters'] . '';
 	
 	//ambiente S36
 	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
 	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
 	$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB($libreria_predefinita_EXE)", array(), $cl_return = array()); 	
 	 	
 	$cl_in 	= array(); 	
 	$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p);
 	$call_return = $tkObj->PgmCall(trim($m_pgm[0]), trim($m_pgm[1]), $cl_in, null, null);
 	
 	$ret = array();
 	$ret['success'] = true;
 	$ret['esito']	= $call_return;
 	$ret['esito_LK_AREA'] = trim($call_return['io_param']['LK-AREA']);
 	return acs_je($ret);
 	
 }
 
 
 
 
 public function exe_prima_data_promettibile($p){
 	global $tkObj, $id_ditta_default;
 	global $libreria_predefinita, $libreria_predefinita_EXE;
 	
 	//costruzione del parametro
 	$cl_p = str_repeat(" ", 246);
 	$cl_p .= $id_ditta_default; //ditta
 	$cl_p .= "VO"; //tido
 	$cl_p .= "   "; //inum
 	$cl_p .= "0000"; //anno
 	$cl_p .= "000000"; //numero
 	$cl_p .= sprintf("%09s", $p['f_cliente_cod']);
 	
 	if (strlen(trim($p['f_destinazione_cod'])) == 3)
 		$cl_p .= trim($p['f_destinazione_cod']);
 	else
 		$cl_p .= '   ';
 	
 	$cl_p .= '000000000'; //cliente collegato
 	$cl_p .= '  '; //tpdo
 	
 	//data evasione richiesta (input / output)
 	if (strlen(trim($p['data_evasione_richiesta'])) == 8)
 		$cl_p .= $p['data_evasione_richiesta'];
 	else
 		$cl_p .= '00000000'; 		
 	
 	$cl_p .= '00000000'; //dtcr 	 	 	 	
 	$cl_p .= '00000000'; //dtcp 	
 
 	$cl_p .= sprintf("%-2s", $p['priorita']); //priorita' 	
 	 	
 	$cl_p .= 'D'; //flag solo cliente

 	//20 parametri
 	 $cl_p .= $p['tipologia_art_critici'] . ' ';
 	 $cl_p .= 'Y';			//parametro utilizzato

 	for ($i=1; $i<=19; $i++){
 		$cl_p .= '    ';
 		$cl_p .= 'N';		//parametro non utilizzato 		
 	} 

 	$cl_p .= '   '; //modello
 	$cl_p .= ' '; 	//fine programma
 	$cl_p .= '00000000'; //data spedizione (come output) 	 	 
 	
 	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array()); 	
 	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array()); 	
 	$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array()); 	

 	$cl_in 	= array();
 	$cl_out = array(); 	
 	
 	// $cmd = "CALL PGM(SV2EXE/BR25X0) PARM('{$cl_p}')";
 	// $call_return = $tkObj->CLCommand($cmd, $cl_in, $cl_out); 	
 	
 	$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
 	$call_return = $tkObj->PgmCall('BR25X0', $libreria_predefinita_EXE, $cl_in, null, null);
 	
 	//echo $cmd;
// 	exit();


 	$ret = array(); 	
 	if ($call_return){
 		$ret['success'] = true;
 		$ret['data_promettibile'] 	= substr($call_return['io_param']['LK-AREA'], 286, 8); 		 		
 		$ret['data_spedizione'] 	= substr($call_return['io_param']['LK-AREA'], 417, 8); 		
 	} else {
 		$ret['success'] = false; 		
 	}
 	
 	
 	return acs_je($ret);
 }
 
 
 public function get_stato_aggiornamento(){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;

 	$ret = array();
 	$ret['success'] = true;
 	 	
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT=? AND TATAID=?";
	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($id_ditta_default, 'IELA'));
 	$row = db2_fetch_assoc($stmt); 	
 	
 	if ($row != false){
 		if ($row['TAFG01'] == 'F'){
 			 $image_name = "icone/48x48/rss_blue.png";
 			 $class_name = 'st-aggiornamento-ok';
 			 $data_ora = print_date($row['TADTGE']) . " " . print_ora($row['TAORGE']);
 			 $img_str = ""; 			 
 		} else {
 			$image_name = "under-construction_q_48.png";
 			$class_name = 'st-aggiornamento-warning';
 			$data_ora = print_ora($row['TAORGE']);
 			$img_str = "<img class='st-aggiornamento' src=" . img_path($image_name) . " height=20>"; 			 			
 		}
 			 	
 		
		$ret['html'] = "{$img_str}<span class='st-aggiornamento {$class_name}'>" . $data_ora . "</span>"; 		
 	} 
 	else $ret['html'] = '-';
 	
 	return $ret;
 }
 
 

 public function get_orari_aggiornamento(){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	 
 	 
 	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT=? AND TATAID=?";
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($id_ditta_default, 'IELA'));
 	$row = db2_fetch_assoc($stmt);
 	 
 	if ($row != false){
 		return $row['TADESC'];
 	}
 	else return '-';
 }
 	 
 
 public function get_descrizione_articolo($dt, $art){
     global $conn, $cfg_mod_Admin;
     
     global $backend_ERP;
     if ($backend_ERP == 'GL'){
         $sql = "SELECT MADES0 FROM {$cfg_mod_Admin['file_anag_art']} WHERE MACAR0=?";
         
         $stmt = db2_prepare($conn, $sql);
         $result = db2_execute($stmt, array($art));
         $row = db2_fetch_assoc($stmt);
         
         if ($row != false){
             return $row['MADES0'];
         }
         else return "[{$row['MACAR0']}]";
         
     } else {
         
         $sql = "SELECT ARDART FROM {$cfg_mod_Admin['file_anag_art']} WHERE ARDT=? AND ARART=?";
         
         $stmt = db2_prepare($conn, $sql);
         $result = db2_execute($stmt, array($dt, $art));
         $row = db2_fetch_assoc($stmt);
         
         if ($row != false){
             return $row['ARDART'];
         }
         else return "[{$row['ARART']}]";
         
     }     
 }
 
 
 
 
 public function ha_anomalie_sped_car($data, $tddt, $tdccon, $tdcdes, $aspe){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default; 	
 	
 	$sql = "SELECT TDNBOC, TDTPCA, TDAACA, TDNRCA
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD
 			
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG				    
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI				    
 			
 			WHERE " . $this->get_where_std() . "
 			AND TDDTEP=? AND TDDT=? AND TDCCON=? AND TDCDES=? AND TAASPE=?
 			GROUP BY TDNBOC, TDTPCA, TDAACA, TDNRCA
			";
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($data, $tddt, $tdccon, $tdcdes, $aspe)); 	
 	$c = 0; 	
 	while ($row = db2_fetch_assoc($stmt))
 		$c++;
 	
 	if ($c>1) return 1; //ha anomalie perche' e' presente in piu' sped/car nell'area di sped.
 	return 0;
 	
 }
 
 
 public function ha_scarichi_in_itinerario($data, $tddt, $tdccon, $tdcdes, $itin_id){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$sql = "SELECT COUNT(*) AS T_NUM
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD
 			WHERE " . $this->get_where_std() . "  AND TDSWSP = 'Y'
 			AND TDDTEP=? AND TDDT=? AND TDCCON=? AND TDCDES=? AND TDCITI=? 			
			";
 	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, array($data, $tddt, $tdccon, $tdcdes, $itin_id));

 	$row = db2_fetch_assoc($stmt);
 
 	if ($row['T_NUM'] > 0) return 1;
 	return 0;
 }
  
 
 public function el_sped_car($data, $tddt, $tdccon, $tdcdes, $aspe){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$sql = "SELECT TDNBOC, TDTPCA, TDAACA, TDNRCA
 				FROM {$cfg_mod_Spedizioni['file_testate']} TD
 	
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG				    
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI				    
 	
 	
			 	WHERE " . $this->get_where_std() . " AND TDSWSP = 'Y'
 				AND TDDTEP=? AND TDDT=? AND TDCCON=? AND TDCDES=? AND TAASPE=?
 				GROUP BY TDNBOC, TDTPCA, TDAACA, TDNRCA
			";
 	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, array($data, $tddt, $tdccon, $tdcdes, $aspe));
  	$ret_ar = array();
  	while ($row = db2_fetch_assoc($stmt))
 		$ret_ar[] = "Carico {$row['TDNRCA']} (#{$row['TDNBOC']})";
 
 				return implode(", ", $ret_ar);
 } 

 
 
 public function el_scarichi_by_sped_car($sped_id, $data, $tpca, $aaca, $nrca, $dt, $campo_data = "TDDTEP", $tipo_destinazione = "scarico"){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	
 	if ($tipo_destinazione == "finale"){
 		$sql_dest_select 	= "TDLOCA AS DEST_LOCA, TDPROV AS DEST_PROV, TDCAP AS DEST_CAP, TDNAZI AS DEST_NAZ_C, TDDNAZ AS DEST_NAZ_D,";
 		$sql_dest_group 	= "TDLOCA, TDPROV, TDCAP, TDNAZI, TDDNAZ,";
 	} else { //destinazione scarico
 		$sql_dest_select 	= "TDDLOC AS DEST_LOCA, TDPROD AS DEST_PROV, TDDCAP AS DEST_CAP, TDNAZD AS DEST_NAZ_C, TDDNAD AS DEST_NAZ_D,";
 		$sql_dest_group 	= "TDDLOC, TDPROD, TDDCAP, TDNAZD, TDDNAD,";
 	}
 	

 	$sql = "SELECT TDDT, TDNBOC, TDSECA, TDCCON, TDCDES, TDTDES, TDVET1, TDVET2, {$sql_dest_select} TDDCON, TDIDES, TDTPCA, TDAACA, TDNRCA,
 					SUM(TDVOLU) AS TDVOLU, sum(TDPLOR) as S_PESO, sum(TDTIMP) as S_IMPORTO, sum(TDTOCO) as S_COLLI 
 					FROM {$cfg_mod_Spedizioni['file_testate']} TD
 					WHERE " . $this->get_where_std() . "
 					AND TDNBOC = ? AND {$campo_data} = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ? AND TDDT = ?
					GROUP BY TDDT, TDNBOC, TDSECA, TDCCON, TDDCON, TDIDES, TDCDES, TDTDES, TDVET1, TDVET2, {$sql_dest_group} TDTPCA, TDAACA, TDNRCA
					ORDER BY TDSECA, TDDCON
			";
 	
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg();
 	$result = db2_execute($stmt, array($sped_id, $data, $tpca, $aaca, $nrca, $dt));
 	
 	echo db2_stmt_errormsg($stmt); 	
 	$ret_ar = array();
 	while ($row = db2_fetch_assoc($stmt))
 		$ret_ar[] = $row;
 
 	return $ret_ar;
 }
 
 
 
 
 public function el_ordini_by_scarico($sped_id, $data, $tpca, $aaca, $nrca, $dt, $campo_data = "TDDTEP", $tipo_destinazione = "scarico", $tdccon, $tdcdes, $tdtdes, $req = array()){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	if ($tipo_destinazione == "finale"){
 		$sql_dest_select 	= "TDLOCA AS DEST_LOCA, TDPROV AS DEST_PROV, TDCAP AS DEST_CAP, TDNAZI AS DEST_NAZ_C, TDDNAZ AS DEST_NAZ_D,";
 		$sql_dest_group 	= "TDLOCA, TDPROV, TDCAP, TDNAZI, TDDNAZ,";
 	} else { //destinazione scarico
 		$sql_dest_select 	= "TDDLOC AS DEST_LOCA, TDPROD AS DEST_PROV, TDDCAP AS DEST_CAP, TDNAZD AS DEST_NAZ_C, TDDNAD AS DEST_NAZ_D,";
 		$sql_dest_group 	= "TDDLOC, TDPROD, TDDCAP, TDNAZD, TDDNAD,";
 	}
 
 
 	$sql = "SELECT TDDT, TDNBOC, TDSECA, TDCCON, TDCDES, TDTDES, TDVET1, TDVET2, {$sql_dest_select} TDDCON, TDIDES, TDTPCA, TDAACA, TDNRCA,
 			SUM(TDVOLU) AS TDVOLU, sum(TDPLOR) as S_PESO, sum(TDTIMP) as S_IMPORTO, sum(TDTOCO) as S_COLLI,
 			TDDOCU, TDOTID, TDOADO, TDONDO, TDVSRF, TDCVN1, TDSTAB 
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD
 			WHERE " . $this->get_where_std() . "
 			AND TDNBOC = ? AND {$campo_data} = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ? AND TDDT = ?
 			AND TDCCON = ? AND TDCDES = ? AND TDTDES = ?
 			";
 	
 		if(isset($req['stabilimento']) && trim($req['stabilimento']) != '')
 			$sql.=" AND TDSTAB='{$req['stabilimento']}'";
 		
 			if(isset($req['modello']) && trim($req['modello'])!= '' && trim($req['modello'])!= '[]' && trim($req['modello'])!= 'null'){
 			$ar_modello = json_decode($req['modello']); //e' passato come array in formato testo
 			$sql .= " AND TDCVN1 IN (" . sql_t_IN($ar_modello) . ") ";
 		}

 	$sql .= "
 			GROUP BY TDDT, TDNBOC, TDSECA, TDCCON, TDDCON, TDIDES, TDCDES, TDTDES, TDVET1, TDVET2, {$sql_dest_group} TDTPCA, TDAACA, TDNRCA,
 				TDDOCU, TDOTID, TDOADO, TDONDO, TDVSRF, TDCVN1, TDSTAB
 			ORDER BY TDSECA, TDDCON
 	";

 
 
 	$stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg();
 	
 	$result = db2_execute($stmt, array($sped_id, $data, $tpca, $aaca, $nrca, $dt, $tdccon, $tdcdes, $tdtdes));
 	echo db2_stmt_errormsg($stmt); 	
 	$ret_ar = array();
 	while ($row = db2_fetch_assoc($stmt))
 		$ret_ar[] = $row;
 
 	return $ret_ar;
 }
  
 
 
 
 public function get_num_ord_by_car($dt, $tpca, $aaca, $nrca){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$sql = "SELECT count(*) AS T_ROW
 				FROM {$cfg_mod_Spedizioni['file_testate']} TD
 				WHERE " . $this->get_where_std() . "
 				AND TDDTOR = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ?
 			";
 
 			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, array($dt, $tpca, $aaca, $nrca));
  			 
  			$row = db2_fetch_assoc($stmt);
  			return $row['T_ROW'];
 } 
 
 

 
 public function get_totali_by_sped($sped_id, $dt){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$sql = "SELECT
         	SUM(TDVOLU) AS TDVOLU, sum(TDPLOR) as S_PESO, sum(TDTIMP) as S_IMPORTO, sum(TDTOCO) as S_COLLI, sum(TDPNET) as S_PESO_N
         	FROM {$cfg_mod_Spedizioni['file_testate']} TD
         	WHERE " . $this->get_where_std() . "
         	AND TDNBOC = ? AND TDDT = ?
 			GROUP BY TDDT, TDNBOC"; 	
 	
 			$stmt = db2_prepare($conn, $sql);
 			echo db2_stmt_errormsg();
  			$result = db2_execute($stmt, array($sped_id, $dt)); 
  			echo db2_stmt_errormsg($stmt);
  			$row = db2_fetch_assoc($stmt);
  			
  			return $row;




 }
 
 public function get_totali_by_TDNBOF($sped_id, $dt){
     global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
     
     $sql = "SELECT
         	SUM(TDVOLU) AS TDVOLU, sum(TDPLOR) as S_PESO, sum(TDTIMP) as S_IMPORTO, sum(TDTOCO) as S_COLLI, sum(TDPNET) as S_PESO_N
         	FROM {$cfg_mod_Spedizioni['file_testate']} TD
         	WHERE " . $this->get_where_std() . "
         	AND TDNBOF = ? AND TDDT = ?
 			GROUP BY TDDT, TDNBOF";
     
     $stmt = db2_prepare($conn, $sql);
     echo db2_stmt_errormsg();
     $result = db2_execute($stmt, array($sped_id, $dt));
     echo db2_stmt_errormsg($stmt);
     $row = db2_fetch_assoc($stmt);
     
     return $row;
 }
 
public function get_caparra_ordine($k_ordine){
    global $conn, $cfg_mod_Spedizioni;
    
    global $backend_ERP;
    if ($backend_ERP == 'GL') return 0;
     
    $oe = $this->k_ordine_td_decode_xx($k_ordine);
     
     $sql_a = "SELECT RT.RTPRZ AS CAPARRA
     FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
     INNER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest_valuta']} RT
     ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
     WHERE RD.RDART = 'CAPARRA' AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
     AND RD.RDTISR = '' AND RD.RDSRIG = 0";
     
     $stmt_a = db2_prepare($conn, $sql_a);
     echo db2_stmt_errormsg();
     $result = db2_execute($stmt_a, $oe);
     $row_a = db2_fetch_assoc($stmt_a);
         
     
     return $row_a['CAPARRA'];
 }
 
 
 public function get_totali_by_sped_car($sped_id, $tpca, $aaca, $nrca, $dt){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$sql = "SELECT 
 			SUM(TDVOLU) AS TDVOLU, sum(TDPLOR) as S_PESO, sum(TDTIMP) as S_IMPORTO, sum(TDTOCO) as S_COLLI
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD
 			WHERE " . $this->get_where_std() . "
 			AND TDNBOC = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ? AND TDDT = ?
 			GROUP BY TDDT, TDNBOC, TDTPCA, TDAACA, TDNRCA
 			";
 
 			$stmt = db2_prepare($conn, $sql);
 			
 			$result = db2_execute($stmt, array($sped_id, $tpca, $aaca, $nrca, $dt));
	 	
	 		$row = db2_fetch_assoc($stmt);
	 		return $row;
 }
 
 
 
 

 public function get_el_clienti_dest_by_sped($sped_id, $dt){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	$ret = array();
 
 	$sql = "SELECT TDDT, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $this->add_riservatezza() . "
 			WHERE " . $this->get_where_std() . "
 			AND TDNBOC = ? AND TDDT = ?
 				GROUP BY TDDT, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ
 			";
 
 			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, array($sped_id, $dt));
 
  			while ($row = db2_fetch_assoc($stmt)){
  			$row['K_COD'] = implode('|', array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));
   				$row['LOC_OUT'] = $this->scrivi_rif_destinazione($row['TDLOCA'], $row['TDNAZI'], $row['TDPROV'], $row['TDDNAZ']);
   						$ret[] = $row;
  			}
  	return $ret;
 }
 
 
 public function get_el_clienti_dest_by_sped_car($sped_id, $tpca, $aaca, $nrca, $dt){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	$ret = array();
 
 	$sql = "SELECT TDDT, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ
 				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $this->add_riservatezza() . "
 				WHERE " . $this->get_where_std() . "
 				AND TDNBOC = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ? AND TDDT = ?
 				GROUP BY TDDT, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ
 			";
 
 			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, array($sped_id, $tpca, $aaca, $nrca, $dt));
  			 
  			while ($row = db2_fetch_assoc($stmt)){
  				$row['K_COD'] = implode('|', array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));
  				$row['LOC_OUT'] = $this->scrivi_rif_destinazione($row['TDLOCA'], $row['TDNAZI'], $row['TDPROV'], $row['TDDNAZ']);  				
  				$ret[] = $row;  			
  			}
  			return $ret;
 } 

 
 public function get_el_clienti_hold($cod_iti, $k_cli_des){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	$ret = array();
 
 	$sql = "SELECT TDDT, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $this->add_riservatezza() . "
 			WHERE " . $this->get_where_std() . " AND TDSWSP = 'N'
 			AND TDCITI=?
 			";
 	$ar_par = array();
 	$ar_par[] = $cod_iti;
 	
 	if (strlen(trim($k_cli_des)) > 0){
 		$k_cli_des_exp = explode("_", $k_cli_des);
 		$sql .= " AND TDDT=? AND TDCCON=? AND TDCDES=? ";
 		$ar_par[] = $k_cli_des_exp[0];$ar_par[] = $k_cli_des_exp[1];$ar_par[] = $k_cli_des_exp[2];
 	}
 	
 	$sql .= " GROUP BY TDDT, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ";
 
  			$stmt = db2_prepare($conn, $sql);
  			echo db2_stmt_errormsg();
  			$result = db2_execute($stmt, $ar_par);
 
  			while ($row = db2_fetch_assoc($stmt)){
  			$row['K_COD'] = implode('|', array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));
  					$row['LOC_OUT'] = $this->scrivi_rif_destinazione($row['TDLOCA'], $row['TDNAZI'], $row['TDPROV'], $row['TDDNAZ']);
  							$ret[] = $row;
  			}
  			return $ret;
 } 
 
 

 public function get_el_agenti_by_sped($sped_id, $dt){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	$ret = array();
 
 	$sql = "SELECT TDDT, TDCAG1, TDDAG1
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $this->add_riservatezza() . "
 			WHERE " . $this->get_where_std() . "
 			AND TDNBOC = ? AND TDDT = ?
 			GROUP BY TDDT, TDCAG1, TDDAG1
 			";
 
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($sped_id, $dt));
 
 	while ($row = db2_fetch_assoc($stmt)){
 		$row['K_COD'] = implode('|', array($row['TDDT'], $row['TDCAG1']));
 		$ret[] = $row;
 	}
 	return $ret;
 }
 
 
 public function get_el_agenti_by_sped_car($sped_id, $tpca, $aaca, $nrca, $dt){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	$ret = array();
 
 	$sql = "SELECT TDDT, TDCAG1, TDDAG1
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $this->add_riservatezza() . "
 			WHERE " . $this->get_where_std() . "
 			AND TDNBOC = ? AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ? AND TDDT = ?
 				GROUP BY TDDT, TDCAG1, TDDAG1
 			";
 
 			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, array($sped_id, $tpca, $aaca, $nrca, $dt));
 
  			while ($row = db2_fetch_assoc($stmt)){
  			$row['K_COD'] = implode('|', array($row['TDDT'], $row['TDCAG1']));
   				$ret[] = $row;
  			}
  			return $ret;
  			} 
 
  			
  			
  	public function get_el_agenti_hold($cod_iti, $k_cli_des){
  		global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
  		$ret = array();
  			
  		$sql = "SELECT TDDT, TDCAG1, TDDAG1
  				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $this->add_riservatezza() . "
  				WHERE " . $this->get_where_std() . " AND TDSWSP = 'N'
 					AND TDCITI=?
 			";
 				$ar_par = array();
  			 	$ar_par[] = $cod_iti;
  			
  			 	if (strlen(trim($k_cli_des)) > 0){
  			 	$k_cli_des_exp = explode("_", $k_cli_des);
  			 	$sql .= " AND TDDT=? AND TDCCON=? AND TDCDES=? ";
 		$ar_par[] = $k_cli_des_exp[0];$ar_par[] = $k_cli_des_exp[1];$ar_par[] = $k_cli_des_exp[2];
  			 	}
  			
  			 	$sql .= " GROUP BY TDDT, TDCAG1, TDDAG1";
  			
  			$stmt = db2_prepare($conn, $sql);
  			  			echo db2_stmt_errormsg();
  			  			$result = db2_execute($stmt, $ar_par);
  			
  			  while ($row = db2_fetch_assoc($stmt)){
  				$row['K_COD'] = implode('|', array($row['TDDT'], $row['TDCAG1']));
   				$ret[] = $row;
  				}
  	return $ret;
  } 
  			  			  			  	
  			
  			
  			
public function get_des_agente($dt, $tdcag1){
	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
  	$ret = array();
  			
  	$sql = "SELECT TDDT, TDCAG1, TDDAG1
  				FROM {$cfg_mod_Spedizioni['file_testate']} TD
  				WHERE " . $this->get_where_std() . "
  				AND TDDT = ? AND TDCAG1 = ?
 				GROUP BY TDDT, TDCAG1, TDDAG1
 			";

  	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, array($dt, $tdcag1));

  	$row = db2_fetch_assoc($stmt);
  	
  	return trim($row['TDDAG1']);
  }

  
  public function get_des_cliente($dt, $tdccon){
  	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
  	$ret = array();
  		
  	$sql = "SELECT TDDT, TDCCON, TDDCON
		  	FROM {$cfg_mod_Spedizioni['file_testate']} TD
  			WHERE " . $this->get_where_std() . "
  			AND TDDT = ? AND TDCCON = ?
 				GROUP BY TDDT, TDCCON, TDDCON
 			";
  
  	$stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt, array($dt, $tdccon));
  
    $row = db2_fetch_assoc($stmt);
    	 
    return trim($row['TDDCON']);
  }  
  			
  			
 public function get_email_cliente_dest($dt, $tdccon, $tdcdes){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 	$ret = array();
 
 	$sql = "SELECT TDMAIL
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD
 			WHERE " . $this->get_where_std() . "
 			AND TDDT = ? AND TDCCON = ? AND TDCDES = ?
 				GROUP BY TDMAIL
 			";
 
 			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, array($dt, $tdccon, $tdcdes));
 
  			$row = db2_fetch_assoc($stmt);
  			
  			return trim($row['TDMAIL']);
  } 
 
  public function get_email_agente($dt, $cod){
  	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
  	$ret = array();
  
  	$sql = "SELECT TAMAIL
  			FROM {$cfg_mod_Spedizioni['file_tabelle']} TD
  				WHERE TADT = ? AND TAKEY1 = ? AND TATAID = 'AGENT'
 			";
  
  			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, array($dt, $cod));
  
  			$row = db2_fetch_assoc($stmt);
  				
  			return trim($row['TAMAIL']);
  }
   
 
 
 public function scarichi_ridondanti($data, $aspe){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$sql = "SELECT TDDT, TDCCON, TDCDES, MAX(TDDCON) AS TDDCON, MAX(TDDLOC) AS TDDLOC, MAX(TDDCAP) AS TDDCAP, MAX(TDPROD) AS TDPROD
 			FROM {$cfg_mod_Spedizioni['file_testate']} TD
 			
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG				    
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI				    
 			
 			
 			WHERE " . $this->get_where_std() . "
 			AND TDDTEP=? AND TAASPE =?
 			GROUP BY TDDT, TDCCON, TDCDES
 			HAVING COUNT(DISTINCT(CONCAT(TDNBOC, CONCAT(TDTPCA, CONCAT(TDAACA, TDNRCA))))) > 1
			";
 
 	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, array($data, $aspe));
	return $stmt; 
 
 }
 
 
 
 public function exe_allinea_limiti_mezzo_su_itve($p){
 	global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
 
 	$record = $p->record;
 	
 	$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} TA
 					SET TAVOLU = ?, TAPESO = ?, TABANC = ?
 				  WHERE TATAID = 'ITVE' AND TADT = ? AND TAKEY3 = ?  
			";
 	
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array($record->TAVOLU, $record->TAPESO, $record->TABANC, $id_ditta_default, $record->k_TAKEY1));
 	
 	$ret = array();
 	$ret['success'] = true;
 	return acs_je($ret);
 } 
 
 
 public function decod_stadio($stadio){
 	switch ((int)$stadio){
 		case 0: return 'Scaduto / Non lavorativo';
 		case 1: return 'ICE';
 		case 2: return 'SLUSH';
 		case 3: return 'WATER'; 		
 	}
 	//default
 	return "-{$stadio}-";
 }
 
 
 
 public function get_ord_fl_bloc($row){

 	if ($row['TDBLEV'] == 'Y' || $row['TDBLOC'] == 'Y'){
 		if ($row['TDBLEV'] == 'Y' && $row['TDBLOC'] == 'Y')	return 4;
 		else {
 			if ($row['TDBLEV'] == 'Y') return 3;
 			else if ($row['TDBLOC'] == 'Y') return 2;
 		} 	
 	}
 		
 		
 	if (($row['TDBLEV'] != 'Y' && $row['TDBLOC'] != 'Y') && ($row['TDBLEV'] == 'S' || $row['TDBLOC'] == 'S')) //sbloccato
 		return 1;
 	
 	return 0;
 }
 
 
 //su singolo ordine
 public function get_fl_art_manc($row){	
 	global $js_parameters;
 	

 	if ($row['TDFMTO'] == 'D') {                          //Segnalazione articoli danneggiati, CARRELLO ROSSO
 	    return 20;
 	}
 	
 	if ($row['TDFMTO'] == 'M') 							//SEGNALAZIONE IN BLU (ARTICOLI MANCANTI)
 	{
 		if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
 			$ret = 5;
 		}
 	}
 	if ($row['TDFMTO'] == 'Y' && $row['TDMTOO'] == 'Y') //CARRELLO GRIGIO (ordini inviati, NON ricevuti)
 	{
 		$ret = 4;
 	}
 	if ($row['TDFMTO'] == 'P' && $row['TDMTOO'] == 'Y') //CARRELLO BLU (ordini inviati, PARZIALMENTE ricevuti)
 	{
 		$ret = 3;
 	}
 	if ($row['TDFMTO'] == 'Y' && $row['TDMTOO'] == 'O') //CARRELLO BLU (presenza di TUTTI gli ordini d'acquisto)
 	{
 	    $ret = 3;
 	}
 	if (trim($row['TDFMTO']) == '' && in_array($row['TDMTOO'] , array('Y', 'O'))) //CARRELLO VERDE (ordini inviati, tutti ricevuti)
 	{
 		$ret = 2;
 	}
 	
 	
 	if ($row['TDFMTO'] == 'Y' && !in_array($row['TDMTOO'] , array('Y', 'O'))) //INFO GRIGIA (ordini non ancora inviati)
 	
 	{
 	    if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
 	        $ret = 1;
 	    }
 	}
 	
  return $ret;	
 }
 
 
 
 //su singolo ordine
 public function get_art_da_prog($row){
 	global $js_parameters;
 	/* ***** ICONA ARTICOLI DA PROGRAMMARE (INFO_BLACK) ****************/
 	if ($row['TDFMTS'] == 'Y')
 	{
 		if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
 			$ret = 1;
 		}
 	}
 	if ($row['TDFN15'] == 1){
 		if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
 			$ret = 2;
 		}
 	}
 	
 	if ($row['TDFN15'] == 2){
 		if ($js_parameters->alert_CF != 1 || $row['TDDTCF'] != 0){ //parametrizzazione "inibisci ALERT"
 			$ret = 3;
 		}
 	}
 	
 	
  return $ret; 	
 }
 
 
 
public function get_tooltip_acc_paga($row){
	if (trim($row['PSFG01']) == 'C') return 'Pagato';
	if (trim($row['PSFG01']) == 'N' || trim($row['PSFG01']) == '') return 'Non pagato';
	if (strlen(trim($row['PSPPAC'])) > 0)
		return("Acconto del " . (float)$row['PSPPAC']) . "%";
}


public function accoda_descrizione_sped($sped, $sped_from){
    global $id_ditta_default, $conn, $cfg_mod_Spedizioni;
    
    $new_desc = substr(trim(trim($sped['CSDESC']) . " " . trim($sped_from['CSDESC'])), 0, 100);
    
    $sqlU = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET CSDESC = ?	
                WHERE CSDT=? AND CSCALE = '*SPR' AND CSPROG = ?";
    $stmtU = db2_prepare($conn, $sqlU);
    $result = db2_execute($stmtU, array($new_desc, $id_ditta_default, $sped['CSPROG']));
    return $resul;
}
 
 

public function get_stato_pagamento_sped($sped_id, $priorita_B_su_P = false){
    global $conn, $id_ditta_default, $cfg_mod_Spedizioni;
        
    $ar = array();
    $sql = "SELECT TDTPCA, TDAACA, TDNRCA, PSFG01, PSFG02, MIN(TDAUX1) AS MIN_TDAUX1
            FROM {$cfg_mod_Spedizioni['file_testate']} TD
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
            WHERE " . $this->get_where_std() . " AND TDNBOC = $sped_id
            GROUP BY TDNBOC, TDTPCA, TDAACA, TDNRCA, PSFG01, PSFG02
            ORDER BY PSFG02, TDTPCA, TDAACA, TDNRCA";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while ($row = db2_fetch_assoc($stmt)) {
        $anno_numero=implode("_", array($row['TDAACA'], $row['TDNRCA']));
        $ar[$anno_numero] = $this->get_stato_pagamento_carico($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA'], $row['PSFG01'], $priorita_B_su_P);
    }
    
    
    $pagato = true;
    foreach($ar as $k => $v){
        if($v != 'C')
            $pagato=false;
            
    }
    if($pagato == true)	return 'C';
    
    //verifico se tutti i carichi sono NON pagati    
    $non_pagato = true;
    foreach($ar as $k => $v){
        if($v != 'N' && trim($v) != '')
            $non_pagato= false;
            
    }
    if ($priorita_B_su_P == false)
        if($non_pagato == true)		return 'N';
    
    //verifico se tutti hanno il tipo pagamento (TDAUX1) > 'C' (cioe' pagamenti differiti)
    $solo_pagamenti_differiti = true;
    foreach($ar as $k => $v){
        if($v != 'B')
            $solo_pagamenti_differiti = false;            
    }
    if($solo_pagamenti_differiti == true)		return 'B';
    
    if($non_pagato == true)		return 'N';
    
    //se sono qui sono in un pagamento parziale
    return 'P';    
}


public function get_stato_pagamento_ordine($k_ordine, $tdfg06){
           
 //verifico se tutti gli ordini del carico hanno un pagamento Differito (TDAUX1 > 'C')
   global $conn, $cfg_mod_Spedizioni;
   $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_testate']} TD
           WHERE " . $this->get_where_std() . " AND TDDOCU = ?
           AND TDAUX1 NOT IN ('D', 'E', 'F') AND TDFG06 = ''";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($k_ordine));
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if(trim($tdfg06) == ''){
        if ($row['C_ROW'] == 0) //nessun pagamento anticipato
            return "B";
    }else{
        if ($tdfg06 == 'P') //Pagamento parziale
            return "P";
            
        if ($tdfg06 == 'N') //Non pagato
            return "N";
                
         if ($tdfg06 == 'C') //Pagamento completo
            return "C";
        
    }
    
    
}

public function get_stato_pagamento_carico($tpca, $aaca, $nrca, $psfg01 = null, $priorita_B_su_P = false){
    
    global $conn, $cfg_mod_Spedizioni;
    
    if ($nrca == 0) return null;
    
    if (!is_null($psfg01))
        $psfg01 = trim($psfg01);
    
    
    if ($psfg01 == 'C') //Pagamento completo
      return "C";
          
    if ($priorita_B_su_P == false && $psfg01 == 'P') //Pagamento parziale
      return "P";
    
    if($cfg_mod_Spedizioni['no_TDAUX1'] == 'Y') return "";
    //verifico se tutti gli ordini del carico hanno un pagamento Differito (TDAUX1 > 'C')
   
    $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_testate']} TD
              WHERE " . $this->get_where_std() . " AND TDTPCA = ? AND TDAACA = ? AND TDNRCA = ?
              AND TDAUX1 NOT IN ('D', 'E', 'F') ";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($tpca, $aaca, $nrca));
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    if ($row['C_ROW'] == 0) //nessun pagamento anticipato
        return "B";
    
    if ($psfg01 == 'P') //Pagamento parziale
      return "P";
        
    if ($psfg01 == 'N') //Non pagato
        return "N";
}


public function get_icona_stato_pagamento_carico($tpca, $aaca, $nrca, $psfg01 = null){
    
    $stato_pagamento_carico = $this->get_stato_pagamento_carico($tpca, $aaca, $nrca, $psfg01);
    $icona = $this->gest_image_pagamento($stato_pagamento_carico);
    return $icona;
   
}

public function gest_image_pagamento($value){
    
    switch ($value) {
        case 'C':
            return "icone/128x128/euro_green.png";
            break;
        case 'P':
            return "icone/128x128/euro_yellow.png";
        case 'N':
            return "icone/128x128/euro_red.png";
        case 'B':
            return "icone/128x128/currency_blue_euro.png";
    }
    
    return null;
}

public function get_image_per_carico($tpca, $aaca, $nrca, $psfg01 = null){
    $img_com_name = $this->get_icona_stato_pagamento_carico($tpca, $aaca, $nrca, $psfg01);
    if (strlen($img_com_name) > 0)
        return '<span style="display: inline"><img class="cell-img" src=' . img_path($img_com_name) . ' height=15 ></a></span>';
}


public function ha_booking_by_sped($sped_id){
    global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
    //se ho i inserito i dati aggiuntivi (booking, etc...)
    $sqlInt = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
    $stmtInt = db2_prepare($conn, $sqlInt);
    $result = db2_execute($stmtInt, array($id_ditta_default, 'SPINT', sprintf("%08s", $sped_id)));
    $rowInt = db2_fetch_assoc($stmtInt);
    if ($rowInt)
        return true;
    else
        return false;
}
 
} //class

?>