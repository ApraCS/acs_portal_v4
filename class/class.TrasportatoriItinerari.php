<?php

class TrasportatoriItinerari extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TATAID,TAKEY1,TAKEY2";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "TRIT";
	protected $tadt_f 		= "TADT";	
	
	public function after_get_row($row){
		$s = new Spedizioni();
		//decodifico i vari valori
		$row['TAKEY1_D'] = $s->decod_std('AVET', $row['TAKEY1']);
		$row['TAKEY2_D'] = $s->decod_std('ITIN', $row['TAKEY2']);
		return $row;
	}	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Trasportatori - Itinerari',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'}, {name: 'k_TAKEY2', xtype: 'hidden'},
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden',
            }, {
				name: 'TAKEY1',
				xtype: 'combo',
				fieldLabel: 'Trasportatore',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: false,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('AVET', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
				name: 'TAKEY2',
				xtype: 'combo',
				fieldLabel: 'Itinerario',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: false,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('ITIN', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Trasportatore',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1_D',
                allowBlank: false
            }, {
                text: 'Itinerario',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY2_D',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'k_TAKEY2',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TAKEY2',        
        useNull: false
    }, 'TAKEY1_D', 'TAKEY2_D']
});
";

}	
	
	
	
}

?>