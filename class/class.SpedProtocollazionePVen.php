<?php

class SpedProtocollazionePVen extends AbstractDb {

	protected $table_k 		= "aaaaaaa";
	
	
	public function exe_protocollazione($form_values){
	    global $tkObj, $id_ditta_default, $conn, $useToolkit, $cfg_mod_Spedizioni, $cfg_mod_Gest;
		global $libreria_predefinita, $libreria_predefinita_EXE;
		global $auth;
		$ret = array();
		
		
		//se sono in un Ordine (doc_type = O) verifico la completezza dei dati anagrafici del clienti ****
		if ($form_values->doc_type == 'O'){
		  
		  $pVen = new DeskPVen(array('no_verify' => 'Y'));
		  $anagrafica_cliente_con_dati_completi = $pVen->anagrafica_cliente_con_dati_completi($form_values->f_cliente_cod);
		  if ($anagrafica_cliente_con_dati_completi['success'] == false){
		      $ret = $anagrafica_cliente_con_dati_completi;
		      return acs_je($ret);
		  }
		  
		} //if doc_type == O  
		  
		// ***********************************************************************************************
		
		
		
		//per useToolkit = N
		$m_time = microtime(true);

		//costruzione del parametro
		$cl_p = str_repeat(" ", 246);
		$cl_p .= $id_ditta_default; //ditta
		$cl_p .= sprintf("%-2s", $form_values->tido);
		$cl_p .= sprintf("%-2s", $form_values->tpdo);
		$cl_p .= sprintf("%-2s", $form_values->stdo);
		$cl_p .= sprintf("%0-4s", substr($form_values->dtrg, 0, 4));
		$cl_p .= sprintf("%09s", $form_values->f_cliente_cod);
		$cl_p .= sprintf("%0-8s", $form_values->dtrg);
		$cl_p .= sprintf("%0-8s", $form_values->dtep);
		$cl_p .= sprintf("%-2s", $form_values->prio);
		$cl_p .= sprintf("%-11s", $form_values->mode); //passato da 3 a 11 per GL

		$cl_p .= sprintf("%-1s", $form_values->endp);
		$cl_p .= sprintf("%-1s", $form_values->elab);
		$cl_p .= sprintf("%-2s", $form_values->dexp);

		$cl_p .= sprintf("%-30s", $form_values->vsrf);
		$cl_p .= sprintf("%-3s", trim($form_values->f_destinazione_cod));
		$cl_p .= sprintf("%0-6s", $form_values->nrdo);
		$cl_p .= sprintf("%-3s", $form_values->tip_prod);
		$cl_p .= sprintf("%-3s", $form_values->divisione);

		$cl_p .= sprintf("%50s", strtr($m_time, array( '.' => ',')));     //con useTookit=N mi serve per il risultato
		$cl_p .= sprintf("%-1s", '');		   //modifica ( /E/M)
		$cl_p .= sprintf("%-3s", '');		   //pagamento
		
		$cl_p .= sprintf("%-30s", $form_values->progetto);
		
		$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_invio_conferma));
		$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_attesa_conferma));
		$cl_p .= sprintf("%-1s", ''); //preferenza
		$cl_p .= sprintf("%-10s", trim(substr($auth->get_user(), 0, 10)));
		
		$cl_p .= sprintf("%012d", '') . '000'; //acconto 12+3
		$cl_p .= sprintf("%012d", '') . '000'; //caparra 12+3
		$cl_p .= sprintf("%012d", '') . '000'; //caparra 12+3
		$cl_p .= sprintf("%-3s", '');		   //referente
		
		$cl_p .= sprintf("%-1s", $form_values->doc_type); //O=ordine, P=preventivo
		$cl_p .= sprintf("%0-8s", '');			//data validita (qui non gestita)
		
		$dpv = new DeskPVen(array('no_verify' => 'Y'));
		$cl_p .= sprintf("%-3s", $dpv->get_stabilimento_in_uso());		   //stabilimento

		$cl_in 	= array();
		$cl_out = array();

		if ($useToolkit == 'N'){
			//per test in Apra
			$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21H5C('{$cl_p}')";
			$stmt1   = 	db2_prepare($conn, $qry1);
			$result1 = 	db2_execute($stmt1);
			
			//recupero il risultato da RI (nrdo e tddocu)
			$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_richieste']} WHERE RIDT=? AND RITIME=? AND RIRGES=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($id_ditta_default, $m_time, 'ESI_GEN_DOCU'));
			echo db2_stmt_errormsg($stmt);
			$r = db2_fetch_assoc($stmt);
			
			$m_return = str_repeat(" ", 333);
			$m_return .= sprintf("%0-6s", trim($r['RINRDO']));
			$m_return .= str_repeat(" ", 6);
			$m_return .= sprintf("%-50s", trim($r['RINOTE']));

			$call_return['io_param']['LK-AREA'] = $m_return;
			//FINE test per Apra
		} else {
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
				
			$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
			$call_return = $tkObj->PgmCall('UR21H5C', $libreria_predefinita_EXE, $cl_in, null, null);
			$tkObj->disconnect();
		}
			

		
		if ($call_return){
			$ret['success'] = true;
			$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
		} else {
			$ret['success'] = false;
		}

		return acs_je($ret);

	}

	
	
	
	public function get_grid_data(){
		global $conn;
		global $cfg_mod_DeskPVen;
	
		$sql= "SELECT TD.*, TA_TIPO.TADESC AS TIPOL
		FROM {$cfg_mod_DeskPVen['file_testate']} TD
		LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tab_sys']} TA_TIPO
		ON TD.TDDT=TA_TIPO.TADT AND TA_TIPO.TAID='PUVN' AND TA_TIPO.TACOR2 = 'MOD'
		FETCH FIRST 50 ROWS ONLY";
		
		$stmt= db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		
		while($row= db2_fetch_assoc($stmt)){
		
			//per la tipologia ambiente left outer join con TA0
		
			$nr = array();
			$nr['denom']	        = trim($row['TDDCON']). "[".trim($row['TDCCON']). "]" ;
			$nr['destinazione']	    = trim($row['TDDDES']);
			$nr['riferimento']	    = trim($row['TDVSRF']);
			$nr['data_reg']	        = trim($row['TDODRE']);
			$nr['tipo_ord']	        = trim($row['TDDOTD']);
			$nr['tipol_amb']	    = trim($row['TIPOL']);
			$nr['num_ord']	        = trim($row['TDONDO']);
		
			$ar[] = $nr;
		
		};
		
		return $ar;
	}

	public function get_stmt_rows($p = array()){
		global $conn;
		global $_REQUEST;
		global $cfg_mod_Spedizioni;

		$s = new Spedizioni();


		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} WHERE 1 = 0";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		return $stmt;
	}


	public function out_Writer_Form_initComponent($doc_type, $title, $cls = '') {
		global $cfg_mod_Spedizioni, $cfg_mod_DeskPVen;
		global $backend_ERP;

		

			$ret =  "
        Ext.apply(this, {
   			overflowY: 'scroll', autoScroll: 'true', height: 500,
            activeRecord: null,
            iconCls: 'icon-blog_add-16',
            frame: true,
            title: " . j($title) . ",
            cls: " . j($cls) . ",
            defaultType: 'textfield',
            bodyPadding: 20,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            	{name: 'doc_type', xtype: 'hidden', value: " . j($doc_type) . "},
            	{name: 'tido', xtype: 'hidden', value: 'VO'}
 			  , {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,
			allowBlank: false,
            store: {
            	pageSize: 1000,
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_anag&mostra_codice=Y',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },
				fields: ['cod', 'descr', 'out_ind', 'out_loc'],
            },

			valueField: 'cod',
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
           	forceSelection: true,
            anchor: '100%',


	        listeners: {
	            change: function(field,newVal) {
					console.log('aaa');
	            	var form = this.up('form').getForm();
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();
	            },
				select: function(combo, row, index) {
					console.log('bbb');
	            	var form = this.up('form').getForm();
	     			form.findField('out_ind').setValue(row[0].data.out_ind);
	     			form.findField('out_loc').setValue(row[0].data.out_loc);

 					//imposto quella a standard
 					console.log('select');
					form.findField('f_destinazione_cod').store.load({
	 					callback: function(records, operation, success) {
 							console.log(this);
 							if (records.length == 1){
 								this.setValue(records[0].get('cod'));
 							}
    					}, scope: form.findField('f_destinazione_cod')
 					});
 					console.log('ok_load');
 					//console.log(form.findField('f_destinazione_cod').getStore().data);

//					all_dest = Ext.pluck(form.findField('f_destinazione_cod').getStore().data.items, 'data')
// 					console.log(all_dest);
 	
				}
 	
 	
	        },


            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{descr}</span></h3>' +
 						' {out_ind}<br/>' +
                        ' {out_loc}' +
                    '</div>';
                }

            },

            pageSize: 1000
        }
 	
 		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo',
 			name: 'out_ind',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;',
 			name: 'out_loc',
 		}

		, {
            xtype: 'combo',
			name: 'f_destinazione_cod',
			fieldLabel: 'Destinazione',
			minChars: 2,
			allowBlank: true,
            store: {
            	pageSize: 1000,
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_des_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            },
                 extraParams: {
        		  	cliente_cod: ''
        		 }
		        },
				fields: ['cod', 'descr', 'denom', 'IND_D', 'LOC_D'],
            },

			valueField: 'cod',
            displayField: 'denom',
            typeAhead: false,
            hideTrigger: false, //mostro la freccetta di selezione
            forceSelection: true,
            anchor: '100%',

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessuna destinazione trovata',

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{denom}</span></h3>' +
                        '{IND_D}<BR/>' +
                        '{LOC_D}' +
                    '</div>';
                }

            },
 	
	        listeners: {
 	
				change: function(combo, row, index) {
					console.log('cccc');
	            	var form = this.up('form').getForm();
					if (Ext.isEmpty(this.valueModels)){
						form.findField('out_ind_dest').setValue('');
	     				form.findField('out_loc_dest').setValue('');
					} else {
	     				form.findField('out_ind_dest').setValue(this.valueModels[0].get('IND_D'));
	     				form.findField('out_loc_dest').setValue(this.valueModels[0].get('LOC_D'));
					}
				},
				select: function(combo, row, index) {
					console.log('ddddd');					
	            	var form = this.up('form').getForm();
					if (Ext.isEmpty(this.valueModels)){
	     				form.findField('out_ind_dest').setValue('');
	     				form.findField('out_loc_dest').setValue('');
					} else {
	     				form.findField('out_ind_dest').setValue(row[0].data.IND_D);
	     				form.findField('out_loc_dest').setValue(row[0].data.LOC_D);					
					}
				}
 	
 	
	        },

            pageSize: 1000
        }
 	
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo',
 			name: 'out_ind_dest',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;',
 			name: 'out_loc_dest',
 		}
 	
		, {
				name: 'tpdo',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Tipo ordine',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,

				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'VO', null, ar_tipo_by_doc_type($doc_type)), '') . "
					    ]
				}

				
			}
 	



				, {
				     name: 'vsrf'
				   , xtype: 'textfield'
				   , fieldLabel: 'Riferimento'
				   , maxLength: 30
				}

		, {
				name: 'mode',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Tipologia ambiente',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('PUVN', null, null, 'MOD'), '') . "
					    ]
				}
			}

		, {
				name: 'prio',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Priorit&agrave',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,
				value: " . j($cfg_mod_Spedizioni["protocollazione"]["prio"]["DEF"]) . ",
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}, {name:'tarest'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('BPRI', null, null, 'VO', null, null, 1), '') . "
					    ]
				}
			}

				, {
				     name: 'dtep'
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Consegna richiesta'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: false
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
					     		
			
			/*
				va in errore form is undefined... sistemare
				   , validator: function(value){					     		
					     	console.log('ssssss');
				
					     	if (Ext.Array.contains(['>', '<', 'W', '='], form.findField('prio').valueModels[0].get('tarest')))
					        	return 'Indicare una data consegna richiesta';
					     	return true;
					    }
			*/		     		
			

				}

			, {
				     name: 'dtrg'
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data registrazione'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: false
				   , value: '" . print_date(oggi_AS_date(), "%d/%m/%Y") . "'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
				}

		, {
				name: 'stdo',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Stato',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				value: " . j($cfg_mod_DeskPVen["protocollazione"][$doc_type]["stdo"]["DEF"]) . ",
				forceSelection: true,
			   	allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('BSTA', null, null, 'VO', null, explode_if_isset("|", $cfg_mod_DeskPVen["protocollazione"][$doc_type]["stdo"]["LIST_INS"])), '') . "
					    ]
				}
			}


            ],
			dockedItems: [" . self::form_buttons()  . "]
			
        });
        this.callParent();
   ";
		
		return $ret;
	}



	public static function form_buttons($b_pre = '') {
		return "
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [
 	
 	         	{
                     xtype: 'button',
	            	 scale: 'medium',
					 text: '#30',
	            	 iconCls: 'icon-rss_black-24',                     
			            handler: function(a, b, c) {
			            	m_grid = this.up('form').up('panel').down('grid');				
							doc_type = m_grid.up('panel').doc_type;
				
	 			
	 				             	Ext.Ajax.request({
									        url        : 'acs_panel_protocollazione.php?fn=get_grid_data_last_orders',
									        method     : 'POST',
						        			jsonData: {
												doc_type: doc_type
											},							        
									        success : function(response, opts){
									     
									       
									        jsonData = Ext.decode(response.responseText);
									        //console.log(jsonData);
									        
											Ext.each(jsonData, function(value) {
												m_grid.getStore().add(value);
											});
						            		
						            		  
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			            		
			            }
			     },
				

				
				{
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_anno', fieldLabel: 'Anno/Nr', width: 85, labelWidth: 45},
						{ xtype: 'textfield', name: 'f_numero', width: 50},
				
						{
		                     xtype: 'button',
		                     text: 'Carica',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
					            m_grid = this.up('form').up('form').up('panel').down('grid');
								doc_type = m_grid.up('panel').doc_type;
			 			
			 				             	Ext.Ajax.request({
											        url        : 'acs_panel_protocollazione.php?fn=get_grid_data_find_order',
											        method     : 'POST',
								        			jsonData: {
														doc_type: doc_type,
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												        jsonData = Ext.decode(response.responseText);
												        //console.log(jsonData);
												        
														Ext.each(jsonData, function(value) {
															m_grid.getStore().add(value);
														});
								            		  
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
					   ]
					},
				
			
 		
 					'->',

                    {
                    iconCls: 'icon-button_black_repeat_dx-24',
 					scale: 'medium',
                    itemId: 'listino',
                    text: 'Protocolla',
                    disabled: false,
                    scope: this,
                    handler: function(){
						console.log('aaaa');
 						form = this.getForm();
						console.log(form);
 						m_grid = this.up('panel').down('grid');

 						if(form.isValid()){
	 						//Call protocollazione
							Ext.Ajax.request({
							        url        : '".$_SERVER['PHP_SELF']."?fn=exe_protocollazione',
							        timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText);
            
                                        if (jsonData.success == false){
                                            acs_show_msg_error(jsonData.message, 'Dati obbligatori mancanti');
                                            return;
                                        }


	 									fv = form.getValues();
 	
 										//progressivo e ora generazione
	 										if (typeof(form.id_prog) === 'undefined') form.id_prog = 0;
	 										form.id_prog += 1;
	 										fv['id_prog'] = form.id_prog;
 	
							        		
							        	
										
 											ora = new Date().getHours() + '';
							        		min= new Date().getMinutes() + '';
							        		sec= new Date().getSeconds();
							        		
							        		newDate = ora + min + sec
 											fv['ora_generazione'] = newDate;   
 	
	 									fv['f_cliente_des'] = form.findField('f_cliente_cod').rawValue;
	 									fv['f_destinazione_des'] = form.findField('f_destinazione_cod').rawValue;
	 									fv['tpdo_des'] = form.findField('tpdo').rawValue;
 		 								fv['mode_des'] = form.findField('mode').rawValue;
	 									fv['nrdo'] = Ext.util.Format.substr(jsonData.call_return, 333, 6);
 										tddocu = Ext.util.Format.substr(jsonData.call_return, 345, 50);
							        	fv['tddocu'] = tddocu;
	 			
							        	
										m_grid.getStore().add(fv);
	            						// form.reset();
 	
	 									my_listeners = {
				        					afterUpdateRecord: function(from_win){
 												console.log('ccbb');
			 									//Pulisco alcuni campi della form
		 										form.findField('f_cliente_cod').setValue('');
		 										form.findField('out_ind').setValue('');
		 										form.findField('out_loc').setValue('');
		 										form.findField('f_destinazione_cod').setValue('');
		 										form.findField('out_ind_dest').setValue('');
		 										form.findField('out_loc_dest').setValue('');
		 										form.findField('vsrf').setValue('');
		 										form.findField('mode').setValue('');
		 										form.findField('dtep').setValue('');
 	
 												if (Ext.isEmpty(form.findField('progetto')) != true)
 													form.findField('progetto').setValue('');
 	
				        						from_win.close();
								        	}
										}
 	

 										//apro la form per visualizzare/modificare alcune info di testata
 										acs_show_win_std('Ordine protocollato. Modifica informazioni di testata', 
							        					 'acs_panel_protocollazione.php?fn=modifica_info_testata', 
							        					 {
							        						doc_type: m_grid.up('panel').doc_type,
							        						tddocu: tddocu
														 }, 550, 550, my_listeners, 'icon-shopping_cart_gray-16');
 	

							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
 						}
 	
                    }
                }]
            }
 ";
	}





	public function out_Writer_Grid_initComponent_columns() {
		$ret = "{
                text: '#',
                width: 20,
                sortable: true,
                dataIndex: 'id_prog'
            },{
                text: 'Ora',
                width: 60,
                sortable: true,
                dataIndex: 'ora_generazione',
				renderer: time_from_AS
            }, {
                text: 'Data',
                width: 70,
                sortable: true,
                dataIndex: 'dtrg',
                allowBlank: false, renderer: date_from_AS
            }, {
                text: 'Numero',
                width: 65,
                sortable: true,
                dataIndex: 'nrdo'
            },{
                text: 'livello',
                hidden: true,
                dataIndex: 'liv'
            }, {
                text: 'Tipo',
                width: 40,
                sortable: true,
                dataIndex: 'tpdo_des',
				renderer: function (value, metaData, record, row, col, store, gridView){
						return record.get('tpdo');
					}
			
            }, {
                text: 'Stato',
                width: 40,
                sortable: true,
                dataIndex: 'stdo'
			
            }, {
                text: 'Denominazione',
                flex: 60,
                sortable: true,
                dataIndex: 'f_cliente_des',
                allowBlank: false,
	    	    renderer: function (value, metaData, record, row, col, store, gridView){
						return value + ' [' + record.get('f_cliente_cod') + ']';
					}
            }, {
                text: 'Destinazione',
                flex: 40,
                sortable: true,
                dataIndex: 'f_destinazione_des',
                allowBlank: false,
				renderer: function (value, metaData, record, row, col, store, gridView){
						return record.get('out_loc_dest');
					}
            }, {
                text: 'Riferimento',
                flex: 30,
                sortable: true,
                dataIndex: 'vsrf'
            },{
                text: 'Tipol. amb.',
                width: 65,
                sortable: true,
                dataIndex: 'mode_des',
				tooltip: 'Tipologia ambiente'
            }

/*
			, {
	            xtype: 'actioncolumn',
				text: '<img src=" . img_path("icone/16x16/comment_edit.png") . ">',
	            width: 30, menuDisabled: true, sortable: false,
	            items: [{
	                icon: " . img_path("icone/16x16/comment_edit.png") . ",
	                // Use a URL in the icon config
	                tooltip: 'Annotazioni',
	                handler: function (grid, rowIndex, colIndex) {
	        			var rec = grid.getStore().getAt(rowIndex);	
 						acs_show_win_std('Ordine protocollato. Modifica informazioni di testata', 'acs_panel_protocollazione.php?fn=modifica_info_testata', {tddocu: rec.get('tddocu')}, 800, 600, {}, 'icon-shopping_cart_gray-16');
	                }
	            }]
	        }
*/
	                		
	                		
	        , {
	            xtype: 'actioncolumn',
				text: '<img src=" . img_path("icone/16x16/pencil.png") . ">',
	            width: 30, menuDisabled: true, sortable: false,
	            items: [{
	                icon: " . img_path("icone/16x16/pencil.png") . ",
	                // Use a URL in the icon config
	                tooltip: 'Modifica testata',
	                handler: function (grid, rowIndex, colIndex) {
	        			var rec = grid.getStore().getAt(rowIndex);
 						acs_show_win_std('Ordine protocollato numero ' + rec.get('nrdo') + '. Modifica informazioni di testata', 
	                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
	                				doc_type: grid.up('panel').up('panel').doc_type,
	                				tddocu: rec.get('tddocu')
								}, 800, 500, {}, 'icon-shopping_cart_gray-16');
	                }
	            }]
	        }, {
	            xtype: 'actioncolumn',
				text: '<img src=" . img_path("icone/16x16/comment_edit.png") . ">',
	            width: 30, menuDisabled: true, sortable: false,
	            items: [{
	                icon: " . img_path("icone/16x16/comment_edit.png") . ",
	                // Use a URL in the icon config
	                tooltip: 'Annotazioni',
	                handler: function (grid, rowIndex, colIndex) {
	                	var rec = grid.getStore().getAt(rowIndex);
	                    show_win_annotazioni_ordine_bl(rec.get('tddocu'));
	                }
	            }]
	        }, {
	            xtype: 'actioncolumn',
				text: '<img src=" . img_path("icone/16x16/leaf_1.png") . ">',
	            width: 30, menuDisabled: true, sortable: false,
	            items: [{
	                icon: " . img_path("icone/16x16/leaf_1.png") . ",
	                // Use a URL in the icon config
	                tooltip: 'Visualizza righe ordine',
	                handler: function (grid, rowIndex, colIndex) {
	                    var rec = grid.getStore().getAt(rowIndex);
	                	console.log(rec);
						  acs_show_win_std('Visualizza righe', 'acs_righe_ordine.php?fn=open_grid', {k_ordine: rec.get('tddocu')}, 900, 550, null, 'icon-leaf-16');
	                }
	            }]
	        },
	                		 {
	            xtype: 'actioncolumn',
				text: '<img src=" . img_path("icone/16x16/print.png") . ">',
	            width: 30, menuDisabled: true, sortable: false,
	            items: [{
	                icon: " . img_path("icone/16x16/print.png") . ",
	                // Use a URL in the icon config
	                tooltip: 'Stampa ordine',
	                handler: function (grid, rowIndex, colIndex) {
	                    var rec = grid.getStore().getAt(rowIndex);
	               		window.open('acs_report_pdf.php?k_ordine='+ rec.get('tddocu'));
	                		 
						  

	                }
	            }]
	        }
	";
		return $ret;
	}




	public function out_Writer_Model($class_name) {
		echo "
		Ext.define('Writer.Model.{$class_name}', {
		extend: 'Ext.data.Model',
		idProperty: 'CSPROG',
		fields: [{
		name: 'CSPROG',
		type: 'int',
		useNull: true
	}, 'f_cliente_cod', 'f_cliente_des', 'f_destinazione_cod', 'f_destinazione_des', 
	   'tpdo', 'tpdo_des', 'aado', 'nrdo', 'id_prog', 'ora_generazione', 'mode', 'stdo', 'liv',
	   'mode_des', 'dtrg', 'vsrf', 'tddocu', 'out_ind', 'out_loc', 'out_ind_dest', 'out_loc_dest', 'dtep']
	});
	";

	}



}

?>