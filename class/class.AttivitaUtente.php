<?php

class AttivitaUtente {
	
	private $ar_sql = array();
	private $mod;
	private $cfg_mod;
	private $nome_file;
	
	
	function __construct() {
	  global $cfg_mod_Admin, $libreria_predefinita;
	  if (strlen($cfg_mod_Admin['file_attivita_utente']) > 0)
	  	$this->nome_file = $cfg_mod_Admin['file_attivita_utente'];
	  else
	  	$this->nome_file = "{$libreria_predefinita}.WPI0AU0";
	}	
	
	
	
	
	

	
	public function crea($modulo, $tipo_ope, $dt, $ar_chiave, $memo = null){
		$this->ar_sql['AUTOPE'] = $tipo_ope;
		$this->ar_sql['AUDT'] 	= $dt;
		$this->ar_sql['AUMODU'] = $modulo->get_cod_mod();		
		$this->ar_sql['AUKEY1'] = $this->create_key($ar_chiave);		
		$this->ar_sql['AUMEMO'] = $memo;		
		$this->imposta_campi_base();		
		$sql = $this->get_insert_sql();
		$this->scrivi_messaggio($sql);
	}
	
	
	public function get_stmt_all($modulo, $tipo_ope, $dt, $ar_chiave){
		$this->ar_sql['AUTOPE'] = $tipo_ope;
		$this->ar_sql['AUDT'] 	= $dt;
		$this->ar_sql['AUMODU'] = $modulo->get_cod_mod();
		$this->ar_sql['AUKEY1'] = $this->create_key($ar_chiave);
		$this->ar_sql['AUMEMO'] = $memo;

		global $conn;
		$sql = "SELECT * FROM {$this->nome_file} WHERE AUMODU=? AND AUTOPE=? AND AUDT=? AND AUKEY1=? ORDER BY AUID";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($modulo->get_cod_mod(), $tipo_ope, $dt, $this->create_key($ar_chiave)));
		return $stmt;
	}	
	

	public function get_stmt_all_desc($modulo, $tipo_ope, $dt, $ar_chiave){
		$this->ar_sql['AUTOPE'] = $tipo_ope;
		$this->ar_sql['AUDT'] 	= $dt;
		$this->ar_sql['AUMODU'] = $modulo->get_cod_mod();
		$this->ar_sql['AUKEY1'] = $this->create_key($ar_chiave);
		$this->ar_sql['AUMEMO'] = $memo;
	
		global $conn;
		$sql = "SELECT * FROM {$this->nome_file} WHERE AUMODU=? AND AUTOPE=? AND AUDT=? AND AUKEY1=? ORDER BY AUID DESC";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($modulo->get_cod_mod(), $tipo_ope, $dt, $this->create_key($ar_chiave)));
		return $stmt;
	}
	
	
	
	public function create_key($ar){
		return implode('|', array_map('trim', $ar));
	}
	
	
	
	
	
	private function scrivi_messaggio($sql){
		global $conn;

		$stmt = db2_prepare($conn, $sql);
		if (!$stmt){
			echo db2_stmt_errormsg();
			$al = new AppLog();
			$al->write_log("prepare " . $sql);
		}
			
		$result = db2_execute($stmt);
		if (!$result){
			$al = new AppLog();
			$al->write_log("execute" . $sql);
		}
		
	}
	
	
	
	
	
	private function imposta_campi_base(){
		global $auth;

		$this->ar_sql['AUUSGE'] = $auth->get_user();
	}
	
	
	
	
	private function get_insert_sql(){
		//$this->ar_sql['RITIME'] = microtime(true);		
		
		$sql = "INSERT INTO {$this->nome_file} (" . $this->get_el_field_name() . ")
				VALUES (" . $this->get_el_field_values() . ") 
				";
		
		return $sql;
	}
	
	private function get_el_field_name(){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $kf;

		return implode(', ', $ar);
	}
	

	private function get_el_field_values(){
		$ar = array();
		foreach ($this->ar_sql as $kf => $v)
			$ar[] = $v;
	
		return implode(', ', array_map('sql_t_trim', $ar));
	}	
	
	
	
	
	
	
}

?>