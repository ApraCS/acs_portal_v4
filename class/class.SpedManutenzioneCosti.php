<?php

class SpedManutenzioneCosti extends AbstractDb {
    
    
    protected $table_k 		= "CSPROG";
    
    public function after_get_row($row, $escludi_depositi = 'N'){
        $s = new Spedizioni();
        $row['ITIN_D'] 		 = $s->decod_std('ITIN', $row['CSCITI']);
        $row['VMC_D'] 		 = $s->decod_vmc_by_sped_row($row);
        $row['VOL_EVASO_D']  = $s->get_volume_evaso_by_sped($row['CSPROG']);
        $row['EL_CARICHI_D'] = $s->get_el_carichi_by_sped($row['CSPROG']);
        
        $row['NR_SCARICHI_DEP'] = $s->get_nr_scarichi_dep_by_sped($row['CSPROG']);
        $row['CSTISP_D']	 = $s->decod_std('TSPE', $row['CSTISP']);
        $row['CSTITR_D']	 = $s->decod_std('TTRA', $row['CSTITR']);
        
        $row['TRASP_D']	 	 = $s->decod_trasportatore_by_sped_row($row);
        
        //se vuoto, al posto del trasportatore ricopio il vettore
        if (strlen(trim($row['TRASP_D'])) == 0)
            $row['TRASP_D'] = $row['VMC_D'];
            
            $row['TRASP_D'] = trim($row['TRASP_D']);
            
            $row['S_COLLI_OUT'] 			= $row['S_COLLI'];
            $row['S_VOLUME_OUT'] 			= $row['S_VOLUME'];
            $row['S_COLLI_SPUNTATI_OUT'] 	= $row['S_COLLI_SPUNTATI'];
            $row['VOL_EVASO_D_OUT'] 		= $row['VOL_EVASO_D'];
            $row['C_CLIENTI_DEST_OUT'] 		= $row['C_CLIENTI_DEST'];
            $row['CSCSTR_OUT'] 				= n($row['CSCSTR'], 2);
            
            
            if ($row['CSFG02'] == 'S'){
                //Dal numero scarichi (NO DEP) devo escludere quelli gia' considerati nella spedizione principale
                $nr_sped_secondaria = $s->get_nr_scarichi_by_sped($row['CSPROG'], null, $row['CSNSPC']);
                $row['C_CLIENTI_DEST_OUT'] =  $nr_sped_secondaria;
                $row['C_CLIENTI_DEST']     =  $nr_sped_secondaria;
                $row['c_yellow']     =  "Y";
                
            }
            
            
            if($row['CSNSPC'] > 0){
                $row['tooltip_spe'] = $row['CSNSPC'];
                $row['grassetto'] = "Y";
                $row['img_c'] = '<img class="cell-img" src=' . img_path("icone/48x48/add_link.png") . ' height=12>';
                                 
            }
            
            if ($escludi_depositi == 'N'){
                //eventuali altri trasportatori (exchange deposito)
                $stmt_exch = $s->get_trasportatori_exchange_by_sped($row['CSPROG']);
                while ($row_exch = db2_fetch_assoc($stmt_exch)){
                    
                    if ($row['CSFG02'] == 'S'){
                        //Dal numero scarichi (DEP) devo escludere quelli gia' considerati nella spedizione principale
                        $nr_sped_secondaria = $s->get_nr_scarichi_by_sped($row['CSPROG'], $row_exch, $row['CSNSPC']);
                        $row_exch['C_CLIENTI_DEP'] =  $nr_sped_secondaria;
                    }
                    
                    //decodifica trasportatore
                    $t = new Trasportatori;
                    $t->load_rec_data_by_k(array('TAKEY1' => $row_exch['TRASPORTATORE']));
                    
                    if (isset($t->rec_data['TADESC']))
                        $decod_trasp = $t->rec_data['TADESC'];
                        else
                            $decod_trasp = "[{$row_exch['TRASPORTATORE']}]";
                            $row['TRASP_D'] .= "<BR><img class=\"cell-img\" src=" . img_path("icone/16x16/exchange.png") . ">{$decod_trasp}";
                            
                            $row['S_COLLI_OUT'] .= "<BR>" . $row_exch['S_COLLI'];
                            $row['S_VOLUME_OUT'] .= "<BR>" . $row_exch['S_VOLUME'];
                            $row['S_COLLI_SPUNTATI_OUT']	.= "<BR>" . $row_exch['S_COLLI_SPUNTATI'];
                            $row['C_CLIENTI_DEST_OUT']		.= "<BR>" . $row_exch['C_CLIENTI_DEP'];
                            $v_evaso_exch = $s->get_volume_evaso_by_sped_trasportatore_exchange($row['CSPROG'], $row_exch['TRASPORTATORE']);
                            $row['VOL_EVASO_D_OUT']			.= "<BR>" . $v_evaso_exch;
                            
                            //se ho almeno un deposito, il costo (totale) dei depositi lo accodo sotto il costo trasporto (e' il totale di tutti i depositi)
                            $row['CSCSTR_OUT'] = $row['CSCSTR']  . "<br/>" . n($row['CSCSTD'], 2);
                }
            }
            
            
            return $row;
    }
    
    
    public function get_importo_riprogrammato_su_sped($sped_id){
        global $conn;
        global $cfg_mod_Spedizioni;
        $s = new Spedizioni;
        
        $sql = "SELECT MAX(TD.TDTIMP) AS S_IMPORTO
        FROM {$cfg_mod_Spedizioni['file_deroghe']} AD
        INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
        ON AD.ADRGES = TD.TDDOCU AND " . $s->get_where_std() . "
					WHERE AD.ADTP = 'R'
					  AND AD.ADNSPE = ?
					  AND TD.TDNBOC <> ?
				  GROUP BY AD.ADRGES
				";
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, array($sped_id, $sped_id));
        $ret = 0;
        while ($row = db2_fetch_assoc($stmt)) {
            $ret += $row['S_IMPORTO'];
        }
        return $ret;
    }
    
    
    public function get_stmt_rows($p = array()){
        global $conn;
        global $_REQUEST;
        global $cfg_mod_Spedizioni;
        
        $s = new Spedizioni();
        
        if (isSet($p['form_ep']))
            $form_ep = $p['form_ep'];
            else $form_ep = $_REQUEST['form_ep'];
            
            $k_field = "CSFG02, CSPROG, CSNSPC, CSVOLD, CSDTSP, CSCITI, SP.CSHMPG, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSCSTR, SP.CSCSTD, SP.CSDESC, TA_ASPE.TAKEY1 AS TAASPE ";
            $k_field_ord = "CSDTSP, /*CSCITI, */ SP.CSHMPG, CASE WHEN SP.CSFG02='S' THEN SP.CSNSPC ELSE SP.CSPROG END, CSFG02 ";
            $k_field_grp = "CSFG02, CSPROG, CSNSPC, CSVOLD, CSDTSP, CSCITI, SP.CSHMPG, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSCSTR, SP.CSCSTD, SP.CSDESC, TA_ASPE.TAKEY1 ";
            
            $s_field = $k_field;
            
            $sql = "SELECT $s_field
            , SUM(TDTOCO) AS S_COLLI, SUM(TDVOLU) AS S_VOLUME, SUM(TDBANC) AS S_PALLET
            , SUM(TDPLOR) AS S_PESOL, SUM(TDPNET) AS S_PESON, SUM(TDTIMP) AS S_IMPORTO
            , SUM(TDCOSP) AS S_COLLI_SPUNTATI
            , COUNT(DISTINCT(CONCAT(TDIDES, CONCAT(TDDLOC, CONCAT(TDDCAP, TDNAZD))))) AS C_CLIENTI_DEST
            FROM {$cfg_mod_Spedizioni['file_testate']}
            INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
            ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
            ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
            ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE
            
            /* vettore -- trasportatore */
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
            ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
            ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'
            
            
            WHERE " . $s->get_where_std('with_history') . " AND TDSWSP='Y' ";
            
            
            //parametri dalla form di ingresso (per BUG SD)
            global $is_linux;
            if ($is_linux == 'Y')
                $form_ep = str_replace("\\", "", $form_ep);
                $form_ep = json_decode($form_ep);
                
                
                //data
                if (strlen($form_ep->f_data_a) > 1)
                    $sql .= " AND CSDTSP >= " . $form_ep->f_data . " AND CSDTSP <= " . $form_ep->f_data_a;
                    else
                        $sql .= " AND CSDTSP = " . $form_ep->f_data;
                        
                        //area spedizione
                        if (strlen($form_ep->f_area_spedizione) > 0)
                            $sql .= " AND TA_ASPE.TAKEY1 = " . sql_t($form_ep->f_area_spedizione);
                            
                            //tipologia trasporto (multipla)
                            if (count($form_ep->f_trasporto) > 0)
                                $sql .= " AND SP.CSTITR IN (" . sql_t_IN($form_ep->f_trasporto) . ")";
                                
                                //trasportatore
                                if (count($form_ep->f_trasportatore) > 0)
                                    $sql .= " AND TA_TRAS.TAKEY1 IN (" . sql_t_IN($form_ep->f_trasportatore) . ")";
                                    
                                    
                                    //filtri con extraParams in ingresso di tio "ep_"
                                    foreach ($_REQUEST as $kv=>$v){
                                        if (substr($kv, 0, 3) == 'ep_' && strlen(trim($v)) > 0 ){
                                            $sql .= " AND " . substr($kv, 3, 100) . " = " . sql_t($v);
                                        }
                                    }
                                    
                                    $sql .= " GROUP BY {$k_field_grp} ORDER BY $k_field_ord";
                                    
                                    $stmt = db2_prepare($conn, $sql);
                                    $result = db2_execute($stmt);
                                    return $stmt;
    }
    
    
    
    
    
    public function update_jd($from_insert = false, $ar_field = null){
        global $conn, $cfg_mod_Spedizioni;
        global $id_ditta_default;
        
        $ar_field = acs_m_params_json_decode()->data;
        
        //array con i soli campi da modificare
        $ar_field_M = array();
        $ar_field_M['CSKMTR'] = $ar_field->CSKMTR;
        $ar_field_M['CSCSTR'] = $ar_field->CSCSTR;
        $ar_field_M['CSCSTD'] = $ar_field->CSCSTD;
        
        //aggiorno utente/data/ora ultima modifica su spedizione
        global $auth;
        $ar_field_M['CSUSGE'] = $auth->get_user();
        $ar_field_M['CSDTGE'] = oggi_AS_date();
        $ar_field_M['CSORGE'] = oggi_AS_time();
        
        //eseguo aggiornamento
        $sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']}";
        $sql .= $this->get_SET_sql_jd($ar_field_M);
        $sql .= " WHERE CSDT='$id_ditta_default' AND CSCALE = '*SPR' AND CSPROG = " . $ar_field->CSPROG;
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt);
        
        //scrivo su RI
        $sh = new SpedHistory();
        $sh->crea(
            'modifica_spedizione',
            array(
                "sped_id"		=> $ar_field->CSPROG
            )
            );
        
        $ret_ar = array();
        $ret_ar["success"] = $result;
        
        echo acs_je($ret_ar);
    }
    
    
    
    
    
    
    
    
    public function out_Writer_Form_initComponent() {
        $ret =  "
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-listino',
            frame: true,
            title: 'Spedizione',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_CSPROG', xtype: 'hidden'}
             , {
 				fieldLabel: 'Spedizione',
                name: 'CSPROG',
                disabled: true,
            }, {
                fieldLabel: 'Vettore',
                name: 'VMC_D',
                disabled: true
            }, {
                fieldLabel: 'Itinerario',
                name: 'ITIN_D',
                disabled: true
            }, {
                fieldLabel: 'Tipologia spedizione',
                name: 'CSTISP_D',
                disabled: true
            }, {
                fieldLabel: 'Tipologia trasporto',
                name: 'CSTITR_D',
                disabled: true
            }, {
                fieldLabel: 'Elenco carichi',
                name: 'EL_CARICHI_D',
                disabled: true,
                xtype     : 'textareafield'
            }, {
                fieldLabel: 'Km percorso',
                name: 'CSKMTR'
            }, {
                fieldLabel: 'Costo trasporto',
                name: 'CSCSTR',
            
				listeners:{
                        change:function(){
							costo_totale = parseFloat(this.up('form').form.findField('CSCSTR').getValue()) + parseFloat(this.up('form').form.findField('CSCSTD').getValue());
							this.up('form').form.findField('S_COSTO_TOTALE').setValue(costo_totale);
            
	    					s_importo = parseFloat(this.up('form').form.getRecord().raw['S_IMPORTO']);
	    					s_costo = costo_totale;
            
	    					if (s_importo > 0)
	    						this.up('form').form.findField('INC_COSTO').setValue(floatRenderer2(parseFloat( s_costo / s_importo * 100)));
	    					else
	    						this.up('form').form.findField('INC_COSTO').setValue(0);
                        }
 				}
            
            }, {
                fieldLabel: 'Costo deposito',
                name: 'CSCSTD',
            
				listeners:{
                        change:function(){
							costo_totale = parseFloat(this.up('form').form.findField('CSCSTR').getValue()) + parseFloat(this.up('form').form.findField('CSCSTD').getValue());
							this.up('form').form.findField('S_COSTO_TOTALE').setValue(costo_totale);
            
	    					s_importo = parseFloat(this.up('form').form.getRecord().raw['S_IMPORTO']);
	    					s_costo = costo_totale;
            
	    					if (s_importo > 0)
	    						this.up('form').form.findField('INC_COSTO').setValue(floatRenderer2(parseFloat( s_costo / s_importo * 100)));
	    					else
	    						this.up('form').form.findField('INC_COSTO').setValue(0);
                        }
 				}
            
            }, {
	                 fieldLabel: 'Costo totale',
	                 name: 'S_COSTO_TOTALE', renderer: floatRenderer2,
	                 disabled: true, width: 90
			}, {
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Importo totale',
					defaultType: 'textfield',
 			        layout: 'hbox',
 					items: [{
			                 fieldLabel: '',
			                 name: 'S_IMPORTO', renderer: floatRenderer2,
			                 disabled: true, width: 90
				            },{
			                 fieldLabel: '% Inc.',
			                 name: 'INC_COSTO',
			                 disabled: true, labelWidth: 50
			            	}]
 			}, {
                fieldLabel: 'Log calcolo',
                name: 'LOG_CALCOLO',
                disabled: true,
                xtype     : 'textareafield',
 				height: 180
            }],
			dockedItems: [" . self::form_buttons()  . "]
        });
        this.callParent();
";
        return $ret;
    }
    
    
    
    public static function form_buttons($b_pre = '') {
        return "
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    iconCls: 'icon-save',
                    itemId: 'save',
                    text: 'Salva',
                    disabled: true,
                    scope: this,
                    handler: this.onSave
                }, {
                    iconCls: 'icon-reset',
                    text: 'Reset',
                    scope: this,
                    handler: this.onReset
                }, {
                    iconCls: 'icon-button_black_repeat-16',
 					// scale: 'large',
                    itemId: 'listino',
                    text: 'Calcola costo',
                    disabled: false,
                    scope: this,
                    handler: function(){
            
 					//km
 					var m_km = sotto_main.child('form').form.findField('CSKMTR').getValue()
            
					//eseguo sul server il ricalcolo
					Ext.Ajax.request({
					        url        : 'acs_op_exe.php?fn=exe_ricalcolo_costo_sped&sped_id=' + this.activeRecord.data.CSPROG + '&km=' + m_km,
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){
					        	var jsonData = Ext.decode(result.responseText);
					            sotto_main.child('form').form.findField('CSCSTR').setValue(parseFloat(jsonData.costo));
								sotto_main.child('form').form.findField('CSCSTD').setValue(parseFloat(jsonData.tot_costo_deposito));
            
					            sotto_main.child('form').form.findField('LOG_CALCOLO').setValue(jsonData.log_calcolo + '');
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
            
            
                    }
                }, {
                    iconCls: 'icon-button_black_repeat-16',
 					// scale: 'large',
                    text: 'Ricalcolo globale',
                    disabled: false,
                    //scope: this,
                    handler: function(){
            
						var mask = new Ext.LoadMask(this, {msg: 'Please wait...'});
            
						Ext.MessageBox.confirm('Richiesta conferma', 'Confermi il ricalcolo per tutte le spedizioni visualizzate?', function(btn){
							if(btn === 'no')
 								return false;
            
            
								mask.show();
            
		 						var speds = new Array();
						        store_spedmanutenzionecosti.each(function(rec){
							       speds.push(rec.get('CSPROG'));
						        });
            
            
								//eseguo sul server il ricalcolo
								Ext.Ajax.request({
								        url        : 'acs_op_exe.php?fn=exe_ricalcolo_costo_globale',
			 							jsonData: {ar_sped_id: speds},
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
											mask.hide();
			 								store_spedmanutenzionecosti.load();
								        },
								        failure    : function(result, request){
											mask.hide();
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
            
 						});
            
                    }
                }]
            }
 ";
    }
    
    
    
    
    
    public function out_Writer_Grid_initComponent_columns() {
        $ret = "{
                text: 'N.Sped',
                tooltip: 'N�Spedizione',
                width: 70,
                sortable: true,
                dataIndex: 'CSPROG',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true,
				renderer: function (value, metaData, record, row, col, store, gridView){
			        var value_spe = value;
			
			        if(record.get('c_yellow') == 'Y') metaData.tdCls += ' td_bg_yellow_01' ;
					if(record.get('grassetto') == 'Y') metaData.tdCls += ' grassetto';
			        if(record.get('img_c') != '') value_spe = value_spe + ' ' + record.get('img_c');
					metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('tooltip_spe')) + '\"';
			
						return value_spe;
					}
            },{
                text: 'Trasportatore',
                flex: 80,
                sortable: true,
                dataIndex: 'TRASP_D',
                allowBlank: false,
                filter: {type: 'string'}, filterable: true,
				renderer: function (value, metaData, record, row, col, store, gridView){
					if(record.get('grassetto') == 'Y') metaData.tdCls += ' grassetto';
            
					return value;
					}
            }, {
                text: 'Itinerario',
                flex: 30,
                sortable: true,
                dataIndex: 'ITIN_D',
                filter: {type: 'string'}, filterable: true
            }, {
                text: 'Colli',
                width: 40,
                sortable: true,
                dataIndex: 'S_COLLI_OUT',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'Evasi',
                width: 40,
                sortable: true,
                dataIndex: 'S_COLLI_SPUNTATI_OUT',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'N.S.',
                tooltip: 'Scarichi',
                width: 45,
                sortable: true,
                dataIndex: 'C_CLIENTI_DEST_OUT',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'Vol. disp.',
                width: 60,
                sortable: true,
                dataIndex: 'CSVOLD',
                align: 'right',
					renderer: function (value, metaData, record, row, col, store, gridView){
						return floatRenderer0(parseFloat(value));
					},
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'Vol. progr.',
                width: 70,
                sortable: true,
                dataIndex: 'S_VOLUME_OUT',
                align: 'right',
					renderer: function (value, metaData, record, row, col, store, gridView){
						if (parseFloat(record.get('CSVOLD')) < parseFloat(record.get('S_VOLUME')))
							metaData.tdCls += ' td_warning_txt' ;
            
						//return floatRenderer4(parseFloat(value));
						return value;
					},
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'Vol. evaso',
                width: 70,
                sortable: true,
                dataIndex: 'VOL_EVASO_D_OUT',
                align: 'right',
					renderer: function (value, metaData, record, row, col, store, gridView){
            
						if (isNaN(parseFloat(record.get('VOL_EVASO_D'))))
							vol_evaso = 0;
						else
							vol_evaso = parseFloat(record.get('VOL_EVASO_D'));
            
						if (parseFloat(record.get('VOL_EVASO_D')) >= parseFloat(record.get('S_VOLUME')))
							metaData.tdCls += ' td_bg_green_01' ;
						if (vol_evaso < parseFloat(record.get('S_VOLUME')))
							metaData.tdCls += ' td_bg_yellow_01' ;
            
            
						//return floatRenderer4(parseFloat(value));
						return value;
					},
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'Km',
                width: 50,
                sortable: true,
                dataIndex: 'CSKMTR',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'Costo',
                width: 70,
                sortable: true,
                dataIndex: 'CSCSTR_OUT',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'TS',
                width: 35,
                sortable: true,
                dataIndex: 'CSTISP',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true
            }, {
                text: 'TT',
                width: 35,
                sortable: true,
                dataIndex: 'CSTITR',
                align: 'right',
                filter: {type: 'numeric'}, filterable: true
            }
	";
        return $ret;
    }
    
    
    
    
    public function out_Writer_Model($class_name) {
        echo "
        Ext.define('Writer.Model.{$class_name}', {
        extend: 'Ext.data.Model',
        idProperty: 'CSPROG',
        fields: [{
        name: 'CSPROG',
        type: 'int',
        useNull: true
    }, 'CSCVET', 'CSTISP', 'CSTITR', 'S_COLLI', 'S_COLLI_SPUNTATI', 'C_CLIENTI_DEST', 'NR_SCARICHI_DEP',
    'VMC_D', 'TRASP_D', 'CSCITI', 'CSDTSP', 'ITIN_D', 'S_VOLUME', 'CSVOLD', 'VOL_EVASO_D', 'CSKMTR', 'CSCSTR', 'EL_CARICHI_D', 'LOG_CALCOLO',
    'CSTISP_D', 'CSTITR_D', 'CSCSTD', 'CSNSPC', 'tooltip_spe', 'c_yellow', 'grassetto', 'img_c',
    'S_COLLI_OUT', 'S_VOLUME_OUT', 'S_COLLI_SPUNTATI_OUT', 'VOL_EVASO_D_OUT', 'C_CLIENTI_DEST_OUT', 'CSCSTR_OUT',
    {name: 'S_IMPORTO', type: 'string', convert: function(v, rec) { return floatRenderer2(parseFloat(v)); }},
    {name: 'INC_COSTO', type: 'string', convert: function(v, rec) {
    s_importo = parseFloat(rec.raw['S_IMPORTO']);
    s_costo = parseFloat(rec.raw['CSCSTR']) + parseFloat(rec.raw['CSCSTD']);
    if (s_importo > 0)
    return floatRenderer2(parseFloat( s_costo / s_importo * 100));
    else
    return 0;
    }
    }, {name: 'S_COSTO_TOTALE', type: 'string', convert: function(v, rec) {
    s_costo_trasporto = parseFloat(rec.raw['CSCSTR']);
    s_costo_deposito  = parseFloat(rec.raw['CSCSTD']);
    
    return floatRenderer2(s_costo_trasporto + s_costo_deposito);
    
    }
    }]
    });
    ";
        
    }
    
    
    
}

?>