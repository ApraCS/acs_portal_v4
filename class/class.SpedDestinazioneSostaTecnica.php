<?php

class SpedDestinazioneSostaTecnica extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TADT,TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "VUDS";
	protected $tadt_f 		= "TADT";	
	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-sticker_black-16',
            frame: true,
            title: 'Indice rottura',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TADT', xtype: 'hidden'},{name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},
			{
                name: 'TADT',
                xtype: 'hidden',
            }, {
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 10
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TADT',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }]
});
";

}	
	
	
	
	
 //-------------------------------------------------------	
  public function get_per_cliente($dt, $cli){
 //-------------------------------------------------------  	
  	global $cfg_mod_Spedizioni, $conn;
  	
  	$sql = "SELECT * 
  			 FROM {$cfg_mod_Spedizioni['file_tabelle']}
  			 WHERE TATAID = ?
  			   AND TADT = ? AND TAKEY2 = ? /* cod cliente */ 
  			 ORDER BY TAKEY1
  			";
  	
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();
  	$result = db2_execute($stmt, array($this->taid_k, $dt, $cli));
  		
  	$ret = array();
  	while ($r = db2_fetch_assoc($stmt)) {
  		$r['dt'] = trim($r['TADT']);
  		$r['cod'] = trim($r['TAKEY1']);
  		$r['cod_cli'] = trim($r['TAKEY2']);
  		$r['descr'] = trim(acs_u8e($r['TADESC']));
  		$r['denom'] = trim(acs_u8e($r['TADESC']));
  		$r['IND_D'] = trim(acs_u8e($r['TAINDI']));
  		$r['LOC_D'] = trim(acs_u8e($r['TALOCA']));
  		$r['CAP_D'] = trim(acs_u8e($r['TACAP']));
  		$r['PRO_D'] = trim(acs_u8e($r['TAPROV']));
  		$r['TEL_D'] = trim(acs_u8e($r['TATELE']));
  		$r['NAZIONE'] = trim(acs_u8e($r['TANAZI']));
  		$ret[] = $r;  		
  	}
  	 
   return $ret;	
  }	
	

  //-------------------------------------------------------
  public function get_cliente_des_anag($dt, $cli, $des){
  	//-------------------------------------------------------
  	global $cfg_mod_Spedizioni, $conn;
  	 
  	$sql = "SELECT *
  			FROM {$cfg_mod_Spedizioni['file_tabelle']}
  			WHERE TATAID = ?
  			AND TADT = ? 
  			AND TAKEY1 = ? /* cod destinazione */
  			AND TAKEY2 = ? /* cod cliente */
  			ORDER BY TAKEY1
  			";
  			 
  			$stmt = db2_prepare($conn, $sql);
  			echo db2_stmt_errormsg();
  			$result = db2_execute($stmt, array($this->taid_k, $dt, $des, $cli));

  			$r = db2_fetch_assoc($stmt);  			
  			return $r;
  }
  
  
  
  
  
  
  //-------------------------------------------------------
  public function create_new($p){
  	//-------------------------------------------------------
  	global $cfg_mod_Spedizioni, $conn, $id_ditta_default;
  	 
  	//insert
  	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(TADT, TATAID, TAKEY1, TAKEY2, TADESC,TAINDI,TACAP,TALOCA,TAPROV,TANAZI,TATELE)
  				VALUES(?,?,?,?,?,?,?,?,?,?,?)";
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();  	
  	$result = db2_execute($stmt, array(
  			$p['dt'],
  			$this->taid_k,
  			$p['cod_des'],
  			$p['cod_cli'],
  			$p['denom1'],
  			$p['ind1'],
  			$p['cap'],
  			$p['loc1'],
  			$p['pro'],
  			$p['nazione'],
  			$p['tel']
  	));  	
  
  	
  	echo db2_stmt_errormsg($stmt);  	
  	return true;
  }
  

  
  //-------------------------------------------------------
  public function save($p){
  	//-------------------------------------------------------
  	global $cfg_mod_Spedizioni, $conn, $id_ditta_default;
  
  	//UPDATE
  	$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']}
  			SET TADESC=?,TAINDI=?,TACAP=?,TALOCA=?,TAPROV=?,TANAZI=?,TATELE=?
  			WHERE TADT=? AND TATAID=? AND TAKEY1=? AND TAKEY2=?
  		  ";
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();
  	$result = db2_execute($stmt, array(
  			$p['denom1'],
  			$p['ind1'],
  			$p['cap'],
  			$p['loc1'],
  			$p['pro'],
  			$p['nazione'],
  			$p['tel'],
  			$p['dt'],
  			$this->taid_k,
  			$p['cod_des'],
  			$p['cod_cli']  				
  	));
  	 
  	echo db2_stmt_errormsg($stmt);
  	
  	
  	//AGGIORNO LE DESTINAZIONI IN TESTATA (DOVE TDTDES = 'T')
  	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
		  	  TDDDES = ?
  			, TDIDES = ?
  			, TDDCAP = ?
  			, TDDLOC = ?
  			, TDPROD = ?
  			, TDNAZD = ?
  			, TDDNAD = ?
  			
  			WHERE TDDT = ? AND TDCCON = ? AND TDCDEL = ? AND TDTDES = 'T'";
  	
  	
  	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, array( $p['denom1'] . $p['denom2'], $p['ind1'] . $p['ind2'], $p['cap'], $p['loc1'] . $p['loc2'], $p['pro'],
  			$p['nazione'], $p['nazione'], /*TODO: DESCRIZIONE NAZIONE */  			
  			$p['dt'], $p['cod_cli'], $p['cod_des']
  	));
  	
  	//REGISTRO VARIAZIONE sosta tecnica
  	$sh = new SpedHistory();
  	$sh->crea(
  			'variazione_destinazione_sosta_tecnica',
  			array("p" 	=> $p)
  	);

  	return true;
  }
  
  
  
  
  public function out_codice($ar){
  	return implode("_", array($ar['PSAACA'], sprintf("%04s", $ar['PSNRCA'])));
  }
  
  

  //-------------------------------------------------------
  public function salva_descrizione($form){
  	//-------------------------------------------------------
  	global $cfg_mod_Spedizioni, $conn, $id_ditta_default;
  	$sped = new Spedizioni();
  	 
  	$chiave_indice_rottura = explode("_", $form->f_codice);

  	$ar_parameters = array(
		$form->f_descrizione,
  		$id_ditta_default,
  		$this->taid_k,
  		$this->taid_k,
  		$chiave_indice_rottura[0],
  		$chiave_indice_rottura[1]
  	);
  	
  	
  	//update
  	$sql = "UPDATE {$cfg_mod_Spedizioni['file_carichi']}
  			SET PSDESC = ? 
  			WHERE PSDT=? AND PSTAID=? AND PSTPCA=? AND PSAACA=? AND PSNRCA=?";
  	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, $ar_parameters);
  	
  	//recupero il record aggiornato
  	$ar_parameters = array(
  			$id_ditta_default,
  			$this->taid_k,
  			$this->taid_k,
  			$chiave_indice_rottura[0],
  			$chiave_indice_rottura[1]
  	);
  	 
  	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_carichi']}
  			WHERE PSDT=? AND PSTAID=? AND PSTPCA=? AND PSAACA=? AND PSNRCA=?";
  			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, $ar_parameters);
  			$r = db2_fetch_assoc($stmt);
  
		  	$ar = array(
		  	'PSDT' 		=> $r['PSDT'],
		  	'PSAACA'	=> $r['PSAACA'],
		  	'PSNRCA'	=> $r['PSNRCA'],
		  	'PSDESC'	=> trim($r['PSDESC'])
		  	);
  
  	return $ar;
  }
  
  
  
}

?>