<?php

 class AppLog{

	private $log__break_time;
	private $log__break_start;
	private $log__break_point;

  function __construct() {

	//inizializzazioni
 	$this->log__break_time = microtime(true);
 	$this->log__break_start = $this->log__break_time;
 	$this->log__break_point = array();
   }



  function add_bp($txt = ''){
   $n = microtime(true);
   $ar = array(
	"txt" => $txt,
	"ts"  => round($n - $this->log__break_start, 5)
	);
   $this->log__break_point[] = $ar;
  }


 function save_db(){
  global $cfg_mod_Admin, $conn, $_SERVER, $auth;
  
  $sql = "INSERT INTO {$cfg_mod_Admin['file_app_log']}(LGBRPT, LGTIME, LGPAGE, LGSEID, LGUSER) VALUES(?, ?, ?, ?, ?)";
  $stmt = db2_prepare($conn, $sql);
  $result = db2_execute($stmt, array( $this->out_json() , number_format($this->out_total_time(), 4) , $_SERVER['SCRIPT_NAME'] , 
  						session_id(), $auth->get_user() ));
 }

 function write_log($txt){
 	global $cfg_mod_Admin, $conn, $_SERVER, $auth;
 
 	$sql = "INSERT INTO {$cfg_mod_Admin['file_app_log']}(LGBRPT, LGTIME, LGPAGE, LGSEID, LGUSER) VALUES(?, ?, ?, ?, ?)";
 	$stmt = db2_prepare($conn, $sql);
 	$result = db2_execute($stmt, array( "ERRORE: {$txt}" , number_format($this->out_total_time(), 4) , $_SERVER['SCRIPT_NAME'] ,
 			session_id(), $auth->get_user() ));
 } 
 
  
  function out_total_time(){
   $n = microtime(true);
   return $n - $this->log__break_start;
  }

  function out_json(){ return acs_je($this->log__break_point); }

  function out_print(){
   return print_r($this->log__break_point, true);
  }

 }

?>
