<?php

class UsersAttivita extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TADT,TATAID,TAKEY1,TAKEY2,TAKEY3,TAASPE";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "USATT";
	protected $tadt_f 		= "TADT";	
	
	public function after_get_row($row){
		$s = new Spedizioni(array('no_verify' => 'Y'));
		//decodifico i vari valori
		$row['TAKEY1_D'] = $s->decod_std('AVET', $row['TAKEY1']); //UTENTE
		$row['TAKEY2_D'] = $s->decod_std('ATTAV', $row['TAKEY2']); //ATTIVITA
		$row['TAKEY3_D'] = $s->decod_std('START', $row['TAKEY3']); //Stabilimento
		return $row;
	}	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-arrivi-16',
            frame: true,
            title: 'Utente selezionabile/attivit&agrave;',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [{name: 'k_TADT', xtype: 'hidden'}, {name: 'k_TAASPE', xtype: 'hidden'},
            		{name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'}, 
            		{name: 'k_TAKEY2', xtype: 'hidden'}, {name: 'k_TAKEY3', xtype: 'hidden'},
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden',
            }, {
				name: 'TAKEY1',
				xtype: 'combo',
				fieldLabel: 'Utente',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: false,
 				forceSelection: true,
                queryMode: 'local',
             	minChars: 1,	
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_users(), '') . " 	
					    ] 
				},  listeners: { 
        			 		beforequery: function (record) {
        	         		record.query = new RegExp(record.query, 'i');
        	         		record.forceAll = true;
    				 }
         			}						 
			}, {
				name: 'TAKEY2',
				xtype: 'combo',
				fieldLabel: 'Attivita',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: false,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('ATTAV', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
				name: 'TAKEY3',
				xtype: 'combo',
				fieldLabel: 'Stabilimento',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('START', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
				name: 'TAASPE',
				xtype: 'combo',
				fieldLabel: 'Area di spedizione',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('ASPE', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Utente',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1_D',
                allowBlank: false
            }, {
                text: 'Attivita',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY2_D',
                allowBlank: false
            }, {
                text: 'Stabilimento',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY3_D',
                allowBlank: false
            }, {
                text: 'Area di spedizione',
                flex: 20,
                sortable: true,
                dataIndex: 'TAASPE',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'k_TAKEY2',        
        useNull: false
    }, {
        name: 'k_TAKEY3',        
        useNull: false
    }, {
        name: 'k_TAASPE',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TAKEY2',        
        useNull: false
    }, {
        name: 'TAKEY3',        
        useNull: false
    }, {
        name: 'TAASPE',        
        useNull: false
    }, 'TAKEY1_D', 'TAKEY2_D', 'TAKEY3_D']
});
";

}	
	
	
	
}

?>