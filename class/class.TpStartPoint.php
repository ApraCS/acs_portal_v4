<?php

class TpStartPoint extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	//protected $table_k 		= "TATAID,TAKEY1";
	protected $table_k 		= "TADT,TATAID,TAKEY1";	
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "START";
	protected $tadt_f 		= "TADT";	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Sede inizio trasporto',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden'
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false
            }, {
                fieldLabel: 'Denominazione',
                name: 'TADESC',
                allowBlank: false
            }, {
                fieldLabel: 'Indirizzo',
                name: 'TAINDI',
 				maxLength: 60 		
            }, {
                fieldLabel: 'CAP',
                name: 'TACAP',
 				maxLength: 10 		
            }, {
                fieldLabel: " . j("Localit&agrave;") . ",
                name: 'TALOCA',
 				maxLength: 60                		
            }, {
                fieldLabel: 'Provincia',
                name: 'TAPROV',
                maxLength: 2
            }, {
                fieldLabel: 'Nazione',
                name: 'TANAZI',
                maxLength: 3                
            }, {
                fieldLabel: 'Latitudine',
                name: 'TARIF1',
                allowBlank: true
            }, {
                fieldLabel: 'Longitudine',
                name: 'TARIF2',
                allowBlank: true
            }, {
                fieldLabel: 'Elenco porte<br>(separate da \",\")',
                name: 'TAMAIL',
                allowBlank: true,
 				maxLength: 100
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Denominazione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }, {
                text: 'Porte',
                width: 100,
                sortable: true,
                dataIndex: 'TAMAIL',
                allowBlank: true
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TADT',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, {
        name: 'TARIF1',        
        useNull: false
    }, {
        name: 'TARIF2',        
        useNull: false
    }, 'TAASPE', 'TAMAIL', 'TAINDI', 'TACAP', 'TALOCA', 'TAPROV', 'TANAZI', 'YAML_PARAMETERS']
});
";

}	
	
	
	
}

?>