<?php

class SpedAssegnazioneClienti extends SpedAssegnazioneOrdini {
    
    
    protected function imposta_campi_base(){
        global $auth, $id_ditta_default;
        $this->ar_sql['ASUSAS'] = $auth->get_user();
        $this->ar_sql['ASDTAS'] = oggi_AS_date();
        $this->ar_sql['ASHMAS'] = oggi_AS_time();
        $this->ar_sql['ASDT'] = $id_ditta_default;
       
        //numeratore
        $modulo = $this->mod;
        $this->ar_sql['ASIDPR'] = $modulo->next_num('IDATT');
    }
    
    public function get_by_prog($prog){
        global $conn, $id_ditta_default, $cfg_mod_Gest;
        
        $sql = "SELECT * FROM {$cfg_mod_Gest['file_assegna_ord']} WHERE 
                ASDT = '{$id_ditta_default}' AND ASIDPR = ?";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, array($prog));
        
        $row = db2_fetch_assoc($stmt);
        return $row;
    }
    
    public function get_memo($id_prog, $blocco = 'ASMEM'){
        global $conn, $cfg_mod_Admin, $cfg_mod_Spedizioni, $id_ditta_default;
        $sql = "select NTMEMO FROM {$cfg_mod_Spedizioni['file_note']}
        WHERE NTDT = ? AND NTTPNO = '{$blocco}' AND INTEGER(NTKEY1) = ? AND NTSEQU=0";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($id_ditta_default, $id_prog));
        
        $tooltip = array();
        $att_open = array();
        $row = db2_fetch_assoc($stmt);
        
        return trim($row['NTMEMO']);
    }
    
    public function crea($tipo_messaggio, $ar_par){
        global $auth;
        
            if ($ar_par['form_values']['f_notifica_assegnazione'] == 'Y'){
            $mail = prepare_new_mail();
            
            $s = new Spedizioni(array('no_verify' => 'Y'));
            $deskGest = new DeskGest();
            $causale = $ar_par['form_values']['f_causale'];
            $causale_d = $deskGest->find_TA_std('ATTAV', trim($causale)); 
            
            //come destinatario prendo l'utente assegnato
            $user_to = new Users();
            $user_to->get_by_cod($ar_par['form_values']['f_utente_assegnato']);
            
            $mail->addAddress(trim($user_to->rec_data['UTMAIL']));
            
            $cliente = $ar_par['k_ordine'];
            $mail->Subject = 'Supply Desk - ' . $causale_d[0]['text'] . " [{$cliente}]";
            
            //corpo email
            $body = '';
            $body = "
                <h2>Inserimento nuova attivit&agrave;</h2>
			    <table BORDER=0 WIDTH=\"100%\">
					<tr><th align='left' width='200'>Da:</th><td>" . $auth->out_name() . "</td></tr>
					<tr><th align='left' width='200'>Attivit&agrave;</th><td>{$causale_d[0]['text']}</td></tr>
					<tr><th align='left' width='200'>Riferimento</th><td>{$ar_par['form_values']['f_note']}</td></tr>
					<tr><th align='left' width='200'>Cliente </th><td>{$cliente}</td></tr>
                    <tr><th align='left' width='200'>Memo:</th><td>{$ar_par['form_values']['f_memo']}</td></tr>
				</table>
					    
			";
            
            $mail->Body = $body;
            $mail->send();
        }
        
        
        switch ($tipo_messaggio){
            default:
                $prog = $this->crea_std($ar_par);
                break;
        }
        
        //se devo chiudere la segnalazione di partenza (provengo da un avanzamento)
        if (strlen($ar_par['as_selected']->prog) > 0){
            
            $this->avanza($ar_par['form_values']['f_entry_prog_causale'], array(
                'prog' 			=> $ar_par['as_selected']->prog,
                'prog_coll'		=> $prog,
                'form_values' => $ar_par['form_values']
                
            ));
        }
        
        return $prog;
        
    }
    
    public function avanza($causale_rilascio, $ar_par, $nrec = 0){
        global $auth, $conn, $cfg_mod_Gest;
 
                
        //In base ai parametri in RILAV verifico se scrivere il file RI0
        $row = $this->get_by_prog($ar_par['prog']);
        
              
        //$s = new Spedizioni(array('no_verify' => 'Y'));
        $modulo = $this->mod;
        $deskGest = new DeskGest();
        $row_rilav = $deskGest->get_TA_std('RILAV', trim($row['ASCAAS']), $causale_rilascio);
        
             
        @include '../../personal/ao_avanza.php';
        
        
        //se richiesto scrivo RI0 CON IL MESSAGGIO PERSONALIZZATO (e non rilascio il messaggio)
        if (strlen(trim($row_rilav['TATELE'])) > 0){
            $sh = new SpedHistory($this->mod);
            $sh->crea(
                'ril_entry_PERS',
                array(
                    "prog" 			=> $ar_par['prog'],
                    "k_ordine"		=> $row['ASDOCU'],
                    "op"			=> trim($row['ASCAAS']),
                    "ex"			=> trim($row['ASCARI']),
                    "messaggio"		=> trim($row_rilav['TATELE']),
                )
                );
        }
    
        if ($nrec > 0) return; //sono da visualizza righe, non chiudo l'intera riga di AS0
        
        //Gestisco il flag di rilasciato solo se NON ho una call da chiamare
        if (strlen(trim($row_rilav['TACOGE'])) == 0 && strlen(trim($row_rilav['TATELE'])) == 0){
            
            $ar_upd = array();
            $ar_upd['ASCARI'] = $causale_rilascio;
            $ar_upd['ASNORI'] = $ar_par['form_values']['f_entry_prog_note'] . '';
            $ar_upd['ASDTRI'] =  oggi_AS_date();
            $ar_upd['ASHMRI'] =  oggi_AS_time();
            if(isset($ar_par['prog_coll']))
                $ar_upd['ASIDAC'] =  $ar_par['prog_coll'];
            $ar_upd['ASFLRI'] =  'Y';
            
            $sql = "UPDATE {$this->cfg_mod['file_assegna_ord']}
                    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                    WHERE ASIDPR = {$ar_par['prog']}";
            if (!isset($ar_par['prog_coll'])) $ar_par['prog_coll'] = 0;
            
            
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt, $ar_upd);
            if (!$result) echo db2_stmt_errormsg($stmt);
        } //update flag rilascio
        
        
        //se richiesto scrivo RI0
        if ($row_rilav['TAFG01'] == 'Y'){
            $sh = new SpedHistory($this->mod);
            $sh->crea(
                'ril_entry',
                array(
                    "prog" 			=> $ar_par['prog'],
                    "k_ordine"		=> $row['ASDOCU'],
                    "op"			=> trim($row['ASCAAS']),
                    "ex"			=> trim($row['ASCARI'])
                )
                );
        }
 
        if($ar_par['form_values']['f_ril'] == 'R')
            $this->write_email($row_rilav, $row, $ar_par);
            
            
            @include '../../personal/ao_avanza_after.php';
            
    }
    
    public function write_email($row_rilav, $row, $ar_par){
        global $auth, $conn;
  
        
        $mail = prepare_new_mail();
        
        $s = new Spedizioni(array('no_verify' => 'Y'));
        
        //come destinatario prendo l'utente assegnato
        $user_to = new Users();
        $user_to->get_by_cod($auth->get_user());
        
        $mail->addAddress(trim($user_to->rec_data['UTMAIL']));
        //$mail->addAddress('s.cultrera@apracs.it');
       
        $cliente = $ar_par['cliente'];
        $causale_d = trim($row_rilav['TADESC']);
        $mail->Subject = 'Supply Desk - ' . $causale_d . " [{$cliente}]";
        
       
        $deskGest = new DeskGest();
        $desc_attav = $deskGest->find_TA_std('ATTAV', trim($row['ASCAAS'])); 
        $memo_attav = $this->get_memo($row['ASIDPR']);

        //corpo email
        $body = '';
        $body = "<h2>Chiusura attivit&agrave;</h2>
			    <table BORDER=0 WIDTH=\"100%\">
					<tr><th align='left' width='200'>Da:</th><td>" . $auth->out_name() . "</td></tr>
					<tr><th align='left' width='200'>Causale di rilascio:</th><td>{$causale_d}</td></tr>
					<tr><th align='left' width='200'>Note: </th><td>{$ar_par['form_values']['f_entry_prog_note']}</td></tr>	
                    <tr><td colspan=2>___________________________________________________</td></tr>		
                   <tr><th align='left' width='200'>Da:</th><td>" . $auth->out_name() . "</td></tr>
                   <tr><th align='left' width='200'>Attivit&agrave;</th><td>{$desc_attav[0]['text']}</td></tr>
					<tr><th align='left' width='200'>Riferimento</th><td>{$row['ASNOTE']}</td></tr>
					<tr><th align='left' width='200'>Cliente</th><td>{$cliente}</td></tr>
					<tr><th align='left' width='200'>Memo:</th><td>{$memo_attav}</td></tr>				
                    </table>
					    
			";
        
        $mail->Body = $body;
        $mail->send();
        
    }
    
    
    public function ha_entry_aperte_per_cliente($cliente){
        global $conn, $cfg_mod_Gest,$id_ditta_default;
        
        $sql = "SELECT count(*) AS T_ROW FROM {$cfg_mod_Gest['file_assegna_ord']}
        WHERE ASDT = '{$id_ditta_default}' AND ASDOCU = ? AND ASFLRI <> 'Y'";
        
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, array($cliente));
        
        $row = db2_fetch_assoc($stmt);
        
        if ($row['T_ROW'] > 0) return true;
        return false;
    }
    
    public function out_flag_rilasciato($flag, $mostra_causale_di_rilascio = 'N', $causale_di_rilascio = ''){
        if ($flag == 'Y'){
            if ($mostra_causale_di_rilascio == 'N')
                return 'Rilasciato';
            else
                return 
            
            trim($causale_di_rilascio);
        }
        return '';
    }
    
    public function stato_tooltip_entry_per_ordine($k_ordine, $causale = null, $per_report = 'N', $filtra_gruppo = null, $mostra_causale_di_rilascio = 'N', $escludi_rilasciate = 'N'){
        global $conn, $cfg_mod_Admin, $id_ditta_default, $cfg_mod_Gest;
        
        $ret = array();
        
        $sql_where = "";
        $sql_where_p = array();
        
        if (strlen(trim($causale)) > 0 ){
            $sql_where .= " AND ASCASS=?";
            $sql_where_p[] = $causale;
        }
        
        if (strlen(trim($filtra_gruppo)) > 0 ){
            $sql_where .= " AND TA_ATTAV.TARIF1=?";
            $sql_where_p[] = $filtra_gruppo;
        }
        
        if ($escludi_rilasciate == 'Y'){
            $sql_where .= " AND ASFLRI<>'Y'";
        }
        
        $sql = "SELECT * FROM {$cfg_mod_Gest['file_assegna_ord']} AS0
        INNER JOIN {$cfg_mod_Gest['file_tabelle']} TA_ATTAV
        ON TA_ATTAV.TATAID='ATTAV' AND AS0.ASCAAS = TA_ATTAV.TAKEY1
        WHERE ASDT = '{$id_ditta_default}' AND ASDOCU = ? " . $sql_where . "
         ORDER BY ASDTAS, ASHMAS";
        
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, array_merge(
            array($k_ordine),
            $sql_where_p
            ));
        
        $tooltip = array();
        $att_open = array();
        $att_distinct = array();
        while ($row = db2_fetch_assoc($stmt)){
            $ar['TOT']++;
            if ($row['ASFLRI'] == 'Y') $ar['CLOSE']++;
            if ($row['ASFLRI'] != 'Y') $ar['OPEN']++;
            
            $deskGest = new DeskGest();
            $causali_rilascio = $deskGest->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI']));
            
            if ($per_report != 'Y'){
                $tooltip[] = implode(" - " , array(
                    print_date($row['ASDTAS']),
                    print_ora($row['ASHMAS']),
                    trim($row['ASUSAS']),
                    trim($row['TADESC']),
                    "[" . trim($row['ASUSAT']) . "]",
                    $this->out_flag_rilasciato($row['ASFLRI'], $mostra_causale_di_rilascio, $row['ASCARI']),
                    $causali_rilascio[0]['text'],
                    trim($row['ASNORI']),
                    $this->get_memo($row['ASIDPR'])
                ));
            } else {
                $tooltip_out = "";
                $riga_bold = implode(" - " , array(
                    print_date($row['ASDTAS']),
                    trim($row['ASUSAS']),
                    trim($row['TADESC']),
                    "[" . trim($row['ASUSAT']) . "]",
                    $this->out_flag_rilasciato($row['ASFLRI'], $mostra_causale_di_rilascio, $row['ASCARI'])));
                $riga_memo = $this->get_memo($row['ASIDPR']);
                $tooltip_out = "<b>" . $riga_bold . "</b>";
                if (strlen(trim($row['ASNORI'])) > 0)
                    $tooltip_out .= "<br/>" . trim($row['ASNORI']);
                    if (strlen(trim($riga_memo)) > 0)
                        $tooltip_out .= "<br/>" . trim($riga_memo);
                        
                        $tooltip[] = $tooltip_out;
            }
            
            $att_distinct[trim($row['ASCAAS'])] = trim($row['ASCAAS']);
            
            if ($row['ASFLRI'] != 'Y'){
                //array con elenco attivita' aperte
                $att_open[] = trim($row['TADESC']);
            }
            
        }
        
        
        if ($ar['TOT'] > 0){
            //ho almeno un messaggio
            if ($ar['OPEN'] == 0){
                //sono tutti chiusi
                $ret['stato'] = 1;
            } else {
                //alcuni sono aperti
                $ret['stato'] = 2;
            }
            
        } else
            $ret['stato'] = 0;
            
            
            $ret['tooltip'] = implode("<br>", $tooltip);
            $ret['att_open'] = implode("<br>", $att_open);
            $ret['att_distinct'] = implode("<br>", $att_distinct);
            
            return $ret;
    }
    
}