<?php

class Auto extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "AUTO";
	protected $tadt_f 		= "TADT";	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Mezzo',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'}, 
            {
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 3
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }, {
                fieldLabel: 'Volume limite',
                name: 'TAVOLU',
                allowBlank: false
            }, {
                fieldLabel: 'Peso limite',
                name: 'TAPESO',
                allowBlank: false
            }, {
                fieldLabel: 'Pallet limite',
                name: 'TABANC',
                allowBlank: false
            }, {
                fieldLabel: 'Classe',
                name: 'TARIF1',
                allowBlank: true,
 				maxLength: 20
            }, {
                fieldLabel: 'Targa',
                name: 'TARIF2',
                allowBlank: true,
 				maxLength: 20
            } 		
 		],
            dockedItems: [" . self::form_buttons(self::form_buttons_pre()) . "]
        });
        this.callParent();
";	
 return $ret;
}





public function form_buttons_pre() {

	return "
			{
                    iconCls: 'iconRefresh',
                    text: 'Immagine',
                    disabled: false,
                    scope: this,
                    handler: function(){
						active = this.activeRecord;

						if(active === null){
							acs_show_msg_error('selezionare un mezzo');
							return false;
						};
		
				        acs_show_win_std('Carica immagine', 'acs_upload_image.php', {record: active.data}, 900, 550, null, 'icon-shopping_cart_gray-16');
		
                    }
                }, {
                    iconCls: 'iconRefresh',
                    itemId: 'applic_su_itve',
                    text: 'Aggiorna itinerari/vettori',
                    disabled: false,
                    scope: this,
                    handler: function(){
						active = this.activeRecord;


						if(active === null){
							acs_show_msg_error('selezionare un mezzo');
							return false;
						};
		
				        Ext.Ajax.request({
				            url : 'acs_op_exe.php?fn=allinea_limiti_mezzo_su_itve' ,
				        	jsonData: {record: active.data},
				            method: 'POST',
				            success: function ( result, request) {
								Ext.Msg.alert('Avviso', 'Operazione eseguita');
				            },
				            failure: function ( result, request) {
				            }
				        });
		
                    }
                },
 ";
}









	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }, {
                text: 'Classe',
                width: 80,
                sortable: true,
                dataIndex: 'TARIF1',
                allowBlank: false
            }, {
                text: 'Targa',
                width: 80,
                sortable: true,
                dataIndex: 'TARIF2'
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, {name: 'TAVOLU', defaultValue: 0}, 
    {name:'TAPESO', defaultValue: 0}, {name: 'TABANC', defaultValue: 0}, 'TARIF1', 'TARIF2']
});
";

}	
	
	
	
}

?>