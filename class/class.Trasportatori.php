<?php

class Trasportatori extends AbstractDb {

	protected $table_id 	= "file_tabelle";
	protected $table_k 		= "TATAID,TAKEY1";
	protected $table_calcolati 	= "HA_DEPOSITI,HA_LISTINI";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "AVET";
	protected $tadt_f 		= "TADT";


	public function after_get_row($row, $escludi_depositi = 'N'){
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$row['HA_DEPOSITI'] = $this->ha_scarichi_intermedi_by_row($row);
		$row['HA_LISTINI'] = $this->ha_listini_caricati_by_row($row);

		//verifico se ha depositi abbinati
		return $row;
	}


	function ha_listini_caricati_by_row($r){
		global $conn;
		global $cfg_mod_Spedizioni;

		$properties = array();

		$sql = "SELECT count(*) AS T_COUNT FROM {$cfg_mod_Spedizioni['file_listino_trasportatori']}
		WHERE LTDT = ? AND LTVETT = ?";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($r['TADT'], $r['TAKEY1']));
		$row = db2_fetch_assoc($stmt);
		if ($row['T_COUNT'] > 0)
			return 'Y';
			else
				return 'N';
	}



	function ha_scarichi_intermedi_by_row($r){
		global $conn;
		global $cfg_mod_Spedizioni;

		$properties = array();

		//calcolo la data disponibilita' piu' alta
		$sql = "SELECT count(*) AS T_COUNT FROM {$cfg_mod_Spedizioni['file_tabelle']}
		WHERE TATAID = 'TRIT' AND TADT = ? AND TAKEY1 = ?";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($r['TADT'], $r['TAKEY1']));
		$row = db2_fetch_assoc($stmt);
		if ($row['T_COUNT'] > 0)
			return 'Y';
			else
				return 'N';
	}


	public function load_rec_data_by_vettore($vet_id){
		global $conn, $cfg_mod_Spedizioni;

		$sql = "SELECT VET.* FROM " . $this->get_table('Y')
		.  " INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} VET
		ON VET.TATAID = 'AVET' AND PRI.TACOGE = VET.TAKEY1
		WHERE PRI.TATAID = 'AUTR' AND PRI.TAKEY1 = " . sql_t(trim($vet_id)) . "
		 ";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);

		$this->rec_data = db2_fetch_assoc($stmt);
	}




	public function out_Writer_Form_initComponent($azienda = 'N') {
		if($azienda == 'Y')
			$tab_name = 'Azienda';
			else
				$tab_name = 'Trasportatore';
				$ret =  "
				this.addEvents('create');
				Ext.apply(this, {
				activeRecord: null,
				iconCls: 'icon-address_book-16',
				frame: true,
				title: '$tab_name',
				defaultType: 'textfield',
				bodyPadding: 5,
				fieldDefaults: {
				anchor: '100%',
				labelAlign: 'right',
				allowBlank: true
	},
	items: [
	{name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},
	{
	name: 'TATAID',
	xtype: 'hidden',
	}, {
	fieldLabel: 'Codice',
	name: 'TAKEY1',
	allowBlank: false,
	maxLength: 10
	}, {
	fieldLabel: 'Descrizione',
	name: 'TADESC',
	allowBlank: false,
	maxLength: 100
	}, {
	fieldLabel: 'Indirizzo',
	name: 'TAINDI',
	maxLength: 60
	}, {
	fieldLabel: 'CAP',
	name: 'TACAP',
	maxLength: 10
	}, {
	fieldLabel: " . j("Localit&agrave;") . ",
                name: 'TALOCA',
 				maxLength: 60
            }, {
                fieldLabel: 'Provincia',
                name: 'TAPROV',
                maxLength: 2
            }, {
                fieldLabel: 'Nazione',
                name: 'TANAZI',
                maxLength: 3
            }, {
                fieldLabel: 'Telefono',
                name: 'TATELE',
 				maxLength: 20
            }, {
                fieldLabel: 'email',
                name: 'TAMAIL',
                vtype: 'email',
 				maxLength: 100
            }],
            dockedItems: [" . self::form_buttons(self::form_buttons_pre()) . ", " . self::form_buttons_2() . "]
        });
        this.callParent();
";
				return $ret;
	}



	public function form_buttons_pre() {
		global $s;
		return "
			{
                    itemId: 'applic_su_rplt',
                    text: 'Report listini',
					iconCls: 'iconPrint',
                    disabled: false,
                    scope: this,
                    handler: function(){

				        //varibile creazione FORM

						if (!this.activeRecord) mio_codice = 'null';
						else mio_codice = this.activeRecord.get('TAKEY1');

						var f = Ext.create('Ext.form.Panel', {
				        frame:true,
				        title: '',
				        bodyStyle:'padding:5px 5px 0',
				        width: 600,
				        url: 'acs_report_listini_trasportatori.php',
				        fieldDefaults: {
				            labelAlign: 'top',
				            msgTarget: 'side'
				        },

				        items: [{
                            name: 'f_trasportatore',
                            xtype: 'combo',
                            fieldLabel: 'Trasportatori',
                            displayField: 'text',
                            valueField: 'id',
							forceSelection: true,
                            emptyText: '- seleziona -',
                               allowBlank: true,
                            anchor: '-15',
							value: mio_codice,
                            store: {
                                autoLoad: true,
                                editable: false,
                                autoDestroy: true,
                                fields: [{name:'id'}, {name:'text'}],
                                data: [
                                     " . acs_ar_to_select_json(find_TA_std('AVET'),"", true, 'Y') . "
                                     ]
                            }
                          },{
                            name: 'f_itinerario',
                            xtype: 'combo',
                            fieldLabel: 'Itinerario',
                            displayField: 'text',
                            valueField: 'id',
                            emptyText: '- seleziona -',
                            allowBlank: true,
							forceSelection: true,
                            anchor: '-15',
                            store: {
                                autoLoad: true,
                                editable: false,
                                autoDestroy: true,
                                fields: [{name:'id'}, {name:'text'}],
                                data: [
                                     " . acs_ar_to_select_json(find_TA_std('ITIN'),"", true, 'Y') . "
                                     ]
                            }
                          },{
							name: 'f_mezzo',
							xtype: 'textfield',
							fieldLabel: 'Classe Mezzo'
						  },{
                            xtype: 'radiogroup',
                            layout: 'hbox',
                            fieldLabel: '',
                            flex: 3,
                            defaults: {
                             width: 100,
                            },
                            items: [{
                                xtype: 'label',
                                text: 'Scaduti: ',
                                width: 70
                            },
                            {
                                boxLabel: 'Tutti',
                                name: 'f_valido',
                                checked: true,
                                inputValue: 'T'
                            }, {
                                boxLabel: 'Solo',
                                checked: false,
                                name: 'f_valido',
                                inputValue: 'S'
                            }, {
                                boxLabel: 'Esclusi',
                                checked: false,
                                name: 'f_valido',
                                inputValue: 'E'
                            }
                           ]
                        }
				            ],

				       buttons: [{
		                text: 'Visualizza',
		                iconCls: 'icon-module-32',
		                scale: 'large',
		                handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
		                        form.submit({
		                                standardSubmit: true,
                                     	submitEmptyText: false,
		                                method: 'POST',
		                                target : '_blank'
		                        });
		                    }
		                }
		            }]

				    });


						// variabile creazione WINDOW

						var win = new Ext.Window({
				          width: 800
				        , height: 380
				        , minWidth: 300
				        , minHeight: 300
				        , plain: true
				        , title: 'Report listini trasporto/deposito'
				        , layout: 'fit'
				        , border: true
				        , closable: true
				        , items: f
				        });

				        win.show();


                    }
 			},
 ";
	}




	public function form_buttons_2() {

		$cl= new ListinoTrasportatori();
		$cl_itinerari = new TrasportatoriItinerari();

		return "
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    iconCls: 'iconScaricoIntermedio',
                    itemId: 'b_config_delivery_control',
                    text: 'Configurazione Delivery Control',
                    disabled: false,
                    scope: this,
                    handler: function(){           
                        acs_show_win_std('Configurazione', 
                                '../base/_gest_nt.php?fn=edit_single', {
            	            	blocco: 'C_TRA',
                                key1: this.activeRecord.data.k_TAKEY1,
								key2: 'DC'
                            });
                    }
                }, {
                    iconCls: 'icon-listino',
                    itemId: 'listino',
                    text: 'Listini',
                    disabled: false,
                    scope: this,
                    handler: function(){
           
						" . $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "listinotrasportatori") . "
						" . $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "listinotrasportatori") . "
						" . $cl->out_Writer_Model("listinotrasportatori") . "
						" .	$cl->out_Writer_Store("listinotrasportatori", "ep_LTVETT: this.activeRecord.data.k_TAKEY1 ") . "
						" .	$cl->out_Writer_sotto_main("listinotrasportatori", 0.4) . "
						" . $cl->out_Writer_main("Listino trasportatore: ",  "listinotrasportatori") . "
						" . $cl->out_Writer_window("Tabella listini trasportatori") . "

						//Imposto il titolo
						m_win.down('grid').setTitle('Listino trasportatore ' + this.activeRecord.data.k_TAKEY1 + ' ' + this.activeRecord.data.TADESC);
                    }
                }, {
                    iconCls: 'iconScaricoIntermedio',
                    itemId: 'trasportatori-itinerari',
                    text: 'Deposito',
                    disabled: false,
                    scope: this,
                    handler: function(){
           
						" . $cl_itinerari->out_Writer_Form($cl_itinerari->out_Writer_Form_initComponent(), "trasportatoriitinerari") . "
						" . $cl_itinerari->out_Writer_Grid($cl_itinerari->out_Writer_Grid_initComponent_columns(), "trasportatoriitinerari") . "
						" . $cl_itinerari->out_Writer_Model("trasportatoriitinerari") . "
						" .	$cl_itinerari->out_Writer_Store("trasportatoriitinerari", "ep_TAKEY1: this.activeRecord.data.k_TAKEY1 ") . "
						" .	$cl_itinerari->out_Writer_sotto_main("trasportatoriitinerari", 0.4) . "
						" . $cl_itinerari->out_Writer_main("Associazione itinerari per scarico in deposito: ",  "trasportatoriitinerari") . "
						" . $cl_itinerari->out_Writer_window("Tabella associazione itinerari per scarico in deposito") . "

                    }
                }]
            }
 ";
	}






	public function out_Writer_Grid_initComponent_columns() {
		$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }, {
                text: '<img src=" . img_path("icone/48x48/exchange.png") . " width=25>',	width: 30,
                tooltip: 'Con depositi caricati',
                width: 30,
                sortable: true,
                dataIndex: 'HA_DEPOSITI',
				renderer: function(value, p, record){
    			    	if (record.get('HA_DEPOSITI')=='Y') return '<img src=" . img_path("icone/48x48/exchange.png") . " width=18>';
    			    	}
            }, {
                text: '<img src=" . img_path("icone/48x48/currency_black_euro.png") . " width=25>',	width: 30,
                tooltip: 'Con listini caricati',
                width: 30,
                sortable: true,
                dataIndex: 'HA_LISTINI',
				renderer: function(value, p, record){
    			    	if (record.get('HA_LISTINI')=='Y') return '<img src=" . img_path("icone/48x48/currency_black_euro.png") . " width=18>';
    			    	}
            }
	";
		return $ret;
	}




	public function out_Writer_Model($class_name) {
		echo "
		Ext.define('Writer.Model.{$class_name}', {
		extend: 'Ext.data.Model',
		idProperty: 'id',
		fields: [{
		name: 'id',
		type: 'int',
		useNull: true
	}, {
	name: 'k_TATAID',
	useNull: false
	}, {
	name: 'k_TAKEY1',
	useNull: false
	}, {
	name: 'TATAID',
	useNull: false
	}, {
	name: 'TAKEY1',
	useNull: false
	}, {
	name: 'TADESC',
	useNull: false
	}, 'TAINDI', 'TACAP', 'TALOCA', 'TAPROV', 'TANAZI', 'TATELE', 'TAMAIL', 'HA_DEPOSITI', 'HA_LISTINI']
	});
	";

	}



}

?>