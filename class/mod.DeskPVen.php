<?php
//print_r('<pre>Aa'); print_r($ret); print_r('<br>');
/**********************************************
 * DESKTOP UTILITY
**********************************************/

class DeskPVen{

	private $mod_cod = "DESK_PVEN";
	private $mod_dir = "desk_punto_vendita";


	function __construct($parameters = array()) {
		global $auth;
		
		if (isset($parameters['no_verify']) && $parameters['no_verify'] == 'Y'){
			return true;
		}
		
		$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());		

		if (is_null($mod_permitted) || !$mod_permitted){

			//se mi ha passato un secondo modulo testo i permessi
			if (isset($parameters['abilita_su_modulo'])){
				$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
				if (is_null($mod_permitted) || !$mod_permitted){
					die("\nNon hai i permessi!!");
				}
			} else
				die("\nNon hai i permessi!!!");
		}
	}
	
	public function get_commento_ordine_by_k_ordine($k_ord, $bl = null){
		global $conn;
		global $cfg_mod_Spedizioni;
	
		if (is_null($bl))
			$bl = $cfg_mod_Spedizioni['bl_commenti_ordine'];
	
			$oe = $this->k_ordine_td_decode($k_ord);
	
			$sql = "SELECT RRN(RD) AS RRN, RD.*
			FROM {$cfg_mod_Spedizioni['file_righe']} RD
			WHERE RDDT=" . sql_t($oe['TDDT']) . " and RDOTID=" . sql_t($oe['TDOTID'])  . "
				  AND RDOINU=" . sql_t($oe['TDOINU']) . " AND RDOADO=" . sql_t($oe['TDOADO'])  . " AND RDONDO=" . sql_t($oe['TDONDO'])  . "
				  AND RDTPNO=" . sql_t($bl);
	
			if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
				$sql .= " AND (RDPROG=" . sql_t($oe['TDPROG']) . " OR RDPROG=' ')";
			}
	
			$sql .= "ORDER BY RDRINO";
				
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
	
			$ret = array();
			while ($row = db2_fetch_assoc($stmt)) {
				//$ret[] = $row['RDDES1'];
			    $nr = array();
			    $nr['text'] = $row['RDDES1'];
			    $nr['rrn'] = trim($row['RRN']);
			    $nr['user_ge'] = trim($row['RDUSGE']);
			    $nr['data_ge'] = trim($row['RDDTGE']);
			    
			    $nr['user_um'] = trim($row['RDRIFE']);
			    $nr['data_um'] = trim($row['RDDTEP']);
			    $ret[$row['RDRINO']] = $nr;
			}
	
			return $ret;
	}


	public function get_cod_mod(){
		return $this->mod_cod;
	}

	public function get_cfg_mod(){
		global $cfg_mod_DeskPVen;
		return $cfg_mod_DeskPVen;
	}


	public function get_mod_parameters(){
		$t = new Modules();
		$t->load_rec_data_by_k(array('TAKEY1' => $this->get_cod_mod()));
		return json_decode($t->rec_data['TAMAIL']);
	}


	public function fascetta_function(){
		global $auth, $main_module, $cfg_mod_Gest;
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
		
		$list_user_function = $auth->list_module_function_permission($main_module->get_cod_mod());

		echo "<span class='txt fascetta-stabilimento' style=\"float: left; margin-right: 10px; font-size: 13px;\">";
		 echo "<a href=\"javascript:show_scelta_stabilimento()\" style=\"color: white; text-decoration: none;\">[ " . $this->get_stabilimento_in_uso_d() . " ]</a>";
		echo "</span>";
		
		echo "<div class='fascetta-toolbar'>";
		
		//se da profilo ho l'abilitazione dei menu
		if ($js_parameters->p_solo_inter != 'Y'){
		    
		    if ($js_parameters->p_mod_param == 'Y')
		        echo "<img id='bt-toolbar' src=" . img_path("icone/48x48/archive.png") . " height=30>";
	        
	        echo "<img id='bt-ins_anagrafiche' src=" . img_path("icone/48x48/clienti.png") . " height=30>";
	        //	if ($cfg_mod_Gest['ins_new_anag']['disabilitato']!='Y')
	        echo "<img id='bt-protocollazione-prev' src=" . img_path("icone/48x48/bookmark.png") . " height=30>";
	        echo "<img id='bt-protocollazione' src=" . img_path("icone/48x48/blog_add.png") . " height=30>";
	        
	        if ($js_parameters->p_acc_stat == 'Y')
	            echo "<img id='bt-portafoglio-ordini' src=" . img_path("icone/48x48/chart_bar.png") . " height=30>";
	            
            echo "<img id='bt-anzianita_stato' src=" . img_path("icone/48x48/clessidra.png") . " height=30>";
            echo "<img id='bt-anticipi' src=" . img_path("icone/48x48/credit_cards.png") . " height=30>";
	            
            if ($js_parameters->p_acc_stat == 'Y')
                echo "<img id='bt-margini' src=" . img_path("icone/48x48/margini.png") . " height=30>";
	                
            echo "<img id='bt-wizard_MTS' src=" . img_path("icone/48x48/shopping_basket_2.png") . " height=30>";
            echo "<img id='bt-consegne_montaggio' src=" . img_path("icone/48x48/calendar.png") . " height=30>";
            echo "<img id='bt-allegati_doc_cli' src=" . img_path("icone/48x48/attachment.png") . " height=30>";
            echo "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
            echo "<img id='bt-doc_log' src=" . img_path("icone/48x48/exchange_black.png") . " height=30>";
            
		} else if (is_null($list_user_function) == false && empty($list_user_function) == false){
			
			if (isset($list_user_function['SETUP']))
				echo "<img id='bt-toolbar' src=" . img_path("icone/48x48/archive.png") . " height=30>";
			if (isset($list_user_function['CUSTOMER']))
				echo "<img id='bt-ins_anagrafiche' src=" . img_path("icone/48x48/clienti.png") . " height=30>";
			if (isset($list_user_function['PROSPECT']))
				echo "<img id='bt-protocollazione-prev' src=" . img_path("icone/48x48/bookmark.png") . " height=30>";
			if (isset($list_user_function['HEADING']))
				echo "<img id='bt-protocollazione' src=" . img_path("icone/48x48/blog_add.png") . " height=30>";
			if (isset($list_user_function['REPORT']))
				echo "<img id='bt-portafoglio-ordini' src=" . img_path("icone/48x48/chart_bar.png") . " height=30>";
			if (isset($list_user_function['AGING']))
				echo "<img id='bt-anzianita_stato' src=" . img_path("icone/48x48/clessidra.png") . " height=30>";
			if (isset($list_user_function['PREPAYMENT']))
				echo "<img id='bt-anticipi' src=" . img_path("icone/48x48/credit_cards.png") . " height=30>";
			if (isset($list_user_function['PROFIT']))
				echo "<img id='bt-margini' src=" . img_path("icone/48x48/margini.png") . " height=30>";
			if (isset($list_user_function['WIZARD_MTS']))
				echo "<img id='bt-wizard_MTS' src=" . img_path("icone/48x48/shopping_basket_2.png") . " height=30>";
			if (isset($list_user_function['DELIVERY']))
				echo "<img id='bt-consegne_montaggio' src=" . img_path("icone/48x48/calendar.png") . " height=30>";
			if (isset($list_user_function['ATTACHMENT']))
				echo "<img id='bt-allegati_doc_cli' src=" . img_path("icone/48x48/attachment.png") . " height=30>";
			if (isset($list_user_function['TODO']))
				echo "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
			if (isset($list_user_function['DOC_LOG']))
			    echo "<img id='bt-doc_log' src=" . img_path("icone/48x48/exchange_black.png") . " height=30>";
		   
		}
		
		
		//bottoni che devono essere obbligatoriamente abilitati per utente
		if (!is_null($list_user_function)){
		    
		  if (isset($list_user_function['ANAG_ART']))
		    echo "<img id='bt-anag-art' src=" . img_path("icone/48x48/barcode.png") . " height=30>";

		  if (isset($list_user_function['FAT_ENT']))
		    echo "<img id='bt-fatture_entrata' src=" . img_path("icone/48x48/currency_black_euro.png") . " height=30>";

	        
	}
		
		
		//Play
		if ($js_parameters->p_solo_inter != 'Y' || isset($list_user_function['START'])){
		  echo "<img id='bt-call-pgm' src=" . img_path("icone/48x48/button_blue_play.png") . " height=30>";
		}
		

		
		//orario aggiornamento dati
		echo "<div id='stato-aggiornamento'>[<img id='bt-stato-aggiornamento' class='st-aggiornamento' src=" . img_path("icone/48x48/rss_blue.png") . " height=20><span id='bt-stato-aggiornamento-esito'></span>]</div>";
			
		echo "</div>";
	}


	function get_TA_std($tab, $k1, $k2 = null){
	    
	    global $id_ditta_default, $conn;
	    $cfg_mod = $this->get_cfg_mod();
	    
	    $ar = array();
	    $sql = "SELECT * FROM {$cfg_mod['file_tabelle']} WHERE TADT='$id_ditta_default' AND TATAID = '$tab' ";
	    
	    if (isset($k1))	$sql .= " AND TAKEY1 = " . sql_t($k1);
	    if (isset($k2))	$sql .= " AND TAKEY2 = " . sql_t($k2);
	    
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    
	    $row = db2_fetch_assoc($stmt);
	    if (!$row) $row = array();
	    
	    return $row;
	}


	function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null, $order_by_seq = 'N',  $mostra_codice = 'N'){
		global $conn;
		$cfg_mod = $this->get_cfg_mod();
		$ar = array();
		
		//****** TODO: BUG: NON STO FILTRANDO PER DITTA!!!!!!!!!!!
		// Prima pero' verificare bene perche' su TA0 ho dei record con ditta '1 ' che forse servono
		// (fare update per portare tutto a ditta 6?????)
		
		$sql = "SELECT * FROM {$cfg_mod['file_tabelle']} WHERE TATAID = '$tab'";
		
		
		if (isset($k1))
			$sql .= " AND TAKEY1 = '$k1' ";

		if (isset($k2))
			$sql .= " AND TAKEY2 = '$k2' ";

		if (isset($aspe))
			$sql .= " AND TAASPE = '$aspe' ";

		if (isset($gruppo))
			$sql .= " AND TARIF1 = '$gruppo' ";


		if ($order_by_seq == 'Y')
			$sql .= "  ORDER BY TASITI, UPPER(TADESC)";
		else
			$sql .= "  ORDER BY UPPER(TADESC)";
		
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);

		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
			if (is_null($k1) || $is_get == 'Y') $m_id = $row['TAKEY1'];
			else $m_id = $row['TAKEY2'];

			$r = array();
				
			$r['id'] 		= $m_id;
			$r['text']		= acs_u8e(trim($row['TADESC']));
			
			if ($mostra_codice == 'Y')
			    $r['text'] = "[" . trim($m_id). "] " . $row['TADESC'];
		    else
		        $r['text'] = acs_u8e(trim($row['TADESC']));
			 
			if ($esporta_esteso == 'Y'){
				$r['TAKEY1'] = $row['TAKEY1'];
				$r['TAKEY2'] = $row['TAKEY2'];
				$r['TAKEY3'] = $row['TAKEY3'];
				$r['TAKEY4'] = $row['TAKEY4'];
				$r['TAKEY5'] = $row['TAKEY5'];
				$r['TACOGE'] = $row['TACOGE'];
				$r['TASTAL'] = $row['TASTAL'];
				$r['TARIF1'] = $row['TARIF1'];
				$r['TARIF2'] = $row['TARIF2'];
				$r['TAFG01'] = $row['TAFG01'];
				$r['TAFG02'] = $row['TAFG02'];
			}

			//		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
			$ret[] = $r;
		}
			
		return $ret;
	}





	function find_sys_TA($tab, $p=array()){
		global $conn, $id_ditta_default;
		$ar = array();
		$cfg_mod = $this->get_cfg_mod();

		//in join prendo il mezzo, per recuperare la targa (da impostare in automatico nella spedizione)
		$sql = "SELECT * FROM {$cfg_mod['file_tab_sys']} TA WHERE TAID = ? AND TADT = ? ORDER BY TADESC";
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($tab, $id_ditta_default));

		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {

			$nr = array(
					"id" 		=> $row['TANR'],
					"text" 		=> trim($row['TADESC']) . "[" . trim($row['TANR']) . "]"
			);
				
			if ($tab == 'CUAG'){
				//Recupero perc. provv. agente
				$nr['perc_provv'] = (float)(substr($row['TAREST'], 171, 2) . "." . substr($row['TAREST'], 173, 2));
			}
				
			$ret[] = $nr;

		}


		return $ret;
	}


	
	
	
	//DRY: anche in acs_panel_protocollazione_include.php
	function get_doc_type_by_tipo($tipo){
		if (in_array($tipo, $this->ar_tipo_by_doc_type('O')))
			return 'O'; //Ordine
		if (in_array($tipo, $this->ar_tipo_by_doc_type('P')))
			return 'P'; //Ordine
	}
	
	function ar_tipo_by_doc_type($doc_type){
		if ($doc_type =='O')
			return array('MO', 'MA', 'ME');
		if ($doc_type =='P')
			return array('MP');
	}
	
	
	

	function permessi_modifiche_prezzi($k_ordine){
		
		global $auth;
		
		$s = new Spedizioni(array('no_verify' => 'Y'));
		$row = $s->get_ordine_by_k_docu($k_ordine);
		
		//consento sempre per i Preventivi
		if ($this->get_doc_type_by_tipo($row['TDOTPD']) == 'P')
			return true;
	
		//parametri del modulo (legati al profilo)
		$js_parameters = $auth->get_module_parameters($this->get_cod_mod());
		
		if ($js_parameters->mod_prezzi == 1 || $row['TDODRE'] == oggi_AS_date()){  
			return true;
		}else{
			return false;
		}		
	}



	function exe_crea_segnalazione_arrivi($r){
		global $conn;

		$ret = array();
		$ret['success'] = true;

		$cfg_mod = $this->get_cfg_mod();

		//bug parametri linux
		global $is_linux;
		if ($is_linux == 'Y')
			$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));

			$ar_ordini = array();
			$list_selected_id = json_decode($r['list_selected_id']);

			$ar_ord = array();
			foreach ($list_selected_id as $k_ordine){
					
				//recupero l'ordine selezionato
				$sql = "SELECT * FROM {$cfg_mod['bollette_doganali']['file_testate']} WHERE TFDOCU = " . sql_t($k_ordine) . " ";

				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt);

				while ($row = db2_fetch_assoc($stmt)) {

					//creo la riga in WPI0AS0
					$na = new SpedAssegnazioneOrdini();
					$na->crea('POSTM', array(
							'k_ordine' => $k_ordine,
							'as_selected' => $ar_selected_AS,
							'form_values' => $r
					));
				}

			}



			return acs_je($ret);
	}



	function exe_modify_segnalazione_arrivi($r){
		global $conn, $cfg_mod_Spedizioni;

		$ret = array();
		$ret['success'] = true;

		$cfg_mod = $this->get_cfg_mod();

		//bug parametri linux
		global $is_linux;
		if ($is_linux == 'Y')
			$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));



			$ar_upd = array();
			$ar_upd['ASNOTE'] = $_REQUEST['f_note'];
			if (strlen($_REQUEST['f_scadenza']) > 0)
				$ar_upd['ASDTSC'] = $_REQUEST['f_scadenza'];
				else
					$ar_upd['ASDTSC'] = 0;
					$ar_upd['ASUSAT'] = $_REQUEST['f_utente_assegnato'];

					//UPDATE
					$sql = "UPDATE {$cfg_mod['bollette_doganali']['file_assegna_ord']}
					SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE ASIDPR = ?";
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt, array_merge(
							$ar_upd,
							array($_REQUEST['prog'])
							));
					echo db2_stmt_errormsg($stmt);


					//salvo eventuali note memo
					if (isset($_REQUEST['f_memo'])){
						if (strlen(trim($_REQUEST['f_memo'])) > 0){
							$sqlMemo = "UPDATE {$cfg_mod_Spedizioni['file_note']}
							SET NTMEMO=? WHERE NTKEY1=? AND NTSEQU=0 AND NTTPNO='ASMEM'";
								
							$stmtMemo = db2_prepare($conn, $sqlMemo);
							echo db2_stmt_errormsg();
							$result = db2_execute($stmtMemo, array(utf8_decode($_REQUEST['f_memo']), $_REQUEST['prog']));
						} else {
							$sqlMemo = "DELETE FROM {$cfg_mod_Spedizioni['file_note']}
							WHERE NTKEY1=? AND NTSEQU=0 AND NTTPNO='ASMEM'";
							$stmtMemo = db2_prepare($conn, $sqlMemo);
							echo db2_stmt_errormsg();
							$result = db2_execute($stmtMemo, array($_REQUEST['prog']));
						}
					}

					return acs_je($ret);
	}



	
	public function exe_upd_commento_ordine($p){
	    global $cfg_mod_DeskPVen, $auth, $conn, $id_ditta_default, $is_linux;
			
		$s = new Spedizioni(array('no_verify' => 'Y'));
		
		if(isset($p['bl']) && $p['bl'] != '')
		    $bl = $p['bl'];
		else
		    $bl = $cfg_mod_DeskPVen['bl_commenti_ordine'];
		
		$oe = $s->k_ordine_td_decode($p['k_ordine']);
		
		
		foreach($p as $k => $v){
		    if (substr($k, 0, 7) == 'f_text_'){
		        $ar_value = explode('_', $k);
		        $riga = $ar_value[2];
		        $rrn = $ar_value[3];
		        
		        $name = "f_h_text_{$riga}";
		        $old_value = trim($p["{$name}"]);
		        if ($is_linux == 'Y')
		            $old_value = strtr($old_value, array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	            if ($is_linux == 'Y')
	                $v = strtr($v, array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
		        
	            $ar_ins = array();
		        
		        $ar_ins['RDDES1'] 	= $v;
		        $ar_ins['RDDTEP'] 	= oggi_AS_date();     //data ultima modifica
		        $ar_ins['RDRIFE'] 	= $auth->get_user();  //utente ultima modifica
 		        
		        if($rrn != ""){
		            
		            if($old_value != trim($v)){
		                $sql = "UPDATE {$cfg_mod_DeskPVen['file_righe']} RD
		                        SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
		                        WHERE RRN(RD) = '{$rrn}'";
		                
		                $stmt = db2_prepare($conn, $sql);
		                echo db2_stmt_errormsg();
		                $result = db2_execute($stmt, $ar_ins);
		                echo db2_stmt_errormsg($stmt);
		                
		            }
		            
		        }else{
		            
		            if(trim($v) != ""){
		                
		                $ar_ins['RDDTGE'] 	= oggi_AS_date();
		                $ar_ins['RDORGE'] 	= oggi_AS_time();
		                $ar_ins['RDUSGE'] 	= SV2_db_user($auth->get_user());
		                $ar_ins['RDDT'] 	= $id_ditta_default;
		                $ar_ins['RDOTID'] 	= trim($oe['TDOTID']);
		                $ar_ins['RDOINU'] 	= trim($oe['TDOINU']);
		                $ar_ins['RDOADO'] 	= trim($oe['TDOADO']);
		                $ar_ins['RDONDO'] 	= trim($oe['TDONDO']);
		                $ar_ins['RDTPNO'] 	= $bl;
		                $ar_ins['RDRINO'] 	= $riga;
		                
		                $sql = "INSERT INTO {$cfg_mod_DeskPVen['file_righe']}(" . create_name_field_by_ar($ar_ins) . ")
			                    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		                
		                $stmt = db2_prepare($conn, $sql);
		                echo db2_stmt_errormsg();
		                $result = db2_execute($stmt, $ar_ins);
		                echo db2_stmt_errormsg();
		                
		                
		            }
		        }
		        
		    }
		    
		}
	
			
		$sh = new SpedHistory();
		$sh->crea(
				'upd_commento_ordine',
				array(
						"k_ordine" 	=> $p['k_ordine']
				)
		);
	
	}
		
  function set_stabilimento($cod){
  	$cfg_mod = $this->get_cfg_mod();
  	$_SESSION[$this->get_cod_mod]['stabilimento_attivo'] = $cod;
  	$_SESSION[$this->get_cod_mod]['stabilimento_attivo_selezionato'] = 'Y';
	return $cod;
  }	
  function get_stabilimento_in_uso(){
  	global $auth;
  	$cfg_mod = $this->get_cfg_mod();
  	if (!isset($_SESSION[$this->get_cod_mod]['stabilimento_attivo']))
  		$_SESSION[$this->get_cod_mod]['stabilimento_attivo'] = $this->get_stabilimento_default();  		
  	return $_SESSION[$this->get_cod_mod]['stabilimento_attivo'];
  }
  function get_stabilimento_in_uso_d(){
  	global $auth;
  	$s = new Spedizioni(array('no_verify' => 'Y'));
  	$stab_d = $s->decod_std('START', $this->get_stabilimento_in_uso($auth));
  	return $stab_d;
  }
  function get_stabilimenti(){
  	global $auth;
  	global $conn, $id_ditta_default;
  	$cfg_mod = $this->get_cfg_mod();  	
  	$sql = "SELECT * FROM {$cfg_mod['file_tabelle']} 
  			WHERE TADT='{$id_ditta_default}' AND TATAID='START'";
  	$stab_assegnati = $this->get_stabilimenti_assegnati();  	
  	if (count($stab_assegnati) > 0)
  		$sql .= " AND TAKEY1 IN (" . sql_t_IN($stab_assegnati) . ")";
  	
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();
  	$result = db2_execute($stmt);
  	while ($row = db2_fetch_assoc($stmt)) {
  		$row['cod'] = trim($row['TAKEY1']);
  		$row['des'] = acs_u8e(trim($row['TADESC']));
  		$ret[] = $row;
  	}
  	return $ret;
  }
  function get_stabilimenti_assegnati(){
  	global $auth;
  	global $conn, $id_ditta_default;
  	$cfg_mod = $this->get_cfg_mod();
  	$sql = "SELECT * FROM {$cfg_mod['file_tabelle']} 
  			WHERE TADT='{$id_ditta_default}' AND TATAID='USATT'
  			  AND TAKEY1=? AND TAKEY2=?
  			";
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();
  	$result = db2_execute($stmt, array($auth->get_user(), 'PV_U_STAB'));
  	$ret = array();
  	while ($row = db2_fetch_assoc($stmt)) {  		
  		$ret[] = $row['TAKEY3']; //cod. stabilimento
  	}
  	return $ret;
  }
  function get_stabilimento_default(){
  	global $auth;
  	$ar_stab = $this->get_stabilimenti($auth);
  	return $ar_stab[0]['cod'];
  }
  
  
  public function get_where_std_PV_TF(){
  	global $id_ditta_default, $cod_mod_provenienza;
  	$ret = '';
  	
  	$stab_assegnati = $this->get_stabilimenti_assegnati();
  	if (count($stab_assegnati) > 0)
  		$ret .= " AND TFNAZI IN (" . sql_t_IN($stab_assegnati) . ")";
  	
  
  	return $ret;
  }
  
	
  
  public function get_orari_aggiornamento(){
  	global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
  		
  		
  	$sql = "SELECT * FROM {$cfg_mod_DeskPVen['file_tabelle']} WHERE TADT=? AND TATAID=?";
  	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, array($id_ditta_default, 'IELA'));
  	$row = db2_fetch_assoc($stmt);
  		
  	if ($row != false){
  		return $row['TADESC'];
  	}
  	else return '-';
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _get_bc_from_nrec($rddt, $rdtido, $rdinum, $rdaado, $rdnrdo, $rdnrec){
      global $conn, $cfg_mod_DeskPVen;
      $sql_p_ar = array(
            $rddt 
          , $rdtido
          , $rdinum
          , $rdaado
          , $rdnrdo
          , $rdnrec
          );
      
      
      $sql = "Select T.MTART, T.MTTIDQ, T.MTINUQ, T.MTAADQ, T.MTNRDQ, T.MTNREQ from {$cfg_mod_DeskPVen['file_proposte_MTO']} T"
      . " where T.MTDT =? and T.MTTIDO =? and T.MTINUM =? and T.MTAADO =? and T.MTNRDO =? and T.MTNREC =?";
      
      $ret = '';
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt, $sql_p_ar);
      $row = db2_fetch_assoc($stmt);
      
      //2019-01-04: prima cerco se trovo il BC per il nu
      $ord_acq = $row['MTTIDQ'] . $row['MTINUQ'] . $row['MTAADQ'] . sprintf("%06s", $row['MTNRDQ']);
      $row_bc = $this->_get_bc_from_art_ord_acq($row['MTART'], $ord_acq);
      if ($row_bc) return $row_bc['BCCODE'];
      
      //altrimenti continuo il giro come in precedenza
      $ret = $this->_comp_bc($row['MTAADQ'], $row['MTNRDQ'], $row['MTNREQ']);
      return $ret;
      
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _get_bc_from_art_ord_acq($art, $ord_acq){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $ret = array();
      $sql_p_ar = array($id_ditta_default);
      $sql_p_ar[] = $art;
      $sql_p_ar[] = $ord_acq;
      $sql = "Select T.BCCODE from {$cfg_mod_DeskPVen['file_ubicazioni']} T"
      . " where T.BCDT =? and T.BCART = ? AND SUBSTR(BCFILL, 1, 15) = ?";

      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt, $sql_p_ar);
      $row = db2_fetch_assoc($stmt);
      return $row;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _get_bc_from_art($art){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $ret = array();
      $sql_p_ar = array($id_ditta_default);
      $sql_p_ar[] = $art;
      $sql = "Select T.BCCODE from {$cfg_mod_DeskPVen['file_ubicazioni']} T"
      . " where T.BCDT =? and T.BCCODE like 'CA%' and T.BCART = ? ";
      //print_r('<pre>Aa'); print_r($sql_p_ar); print_r('<br>');
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt, $sql_p_ar);
      $row = db2_fetch_assoc($stmt);
      $ret['bc'] = $row['BCCODE'];
      $ret['fl_mts'] = 'N';
      if (substr($ret['bc'], 0, 2) =='CA') 
      $ret['fl_mts'] = 'Y';
       
      
      return $ret;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _comp_bc($a, $n, $r){
      $ret = substr(sprintf("%04s", $a), 3, 1)
      . sprintf("%06s",$n)
      . substr(sprintf("%06s", $r), 3, 3)
      . '01';
      
      return $ret;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _get_ubicazione($depo=null, $bc=null, $stacco=null, $ubi_blank=null){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      // $deposito : array di magazzini
      // $bc : barcode
      
      // Recupero la data di partenza che mi servir� per l'estrazione delle ubicazioni //
      $sql = "Select substr(TA.TADES2, 4, 8) AS DATA_PARTENZA" 
        . " FROM {$cfg_mod_DeskPVen['file_tab_sys']} TA WHERE TA.TADT = '{$id_ditta_default}' AND TAID = 'MUDV'";
      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt);
      $row_mudv = db2_fetch_assoc($stmt);
            
      $ret = array();
      $sql_where = '';
      $sql_p_ar = array();
      $sql_p_ar[] = $id_ditta_default;
      
      if (isset($depo) && strlen(trim($depo[0])) > 0) {
        $sql_in = componi_sql_in('T.MVDEPO', $depo);
        $sql_where .= $sql_in['stringa'];
        $sql_p_ar = array_merge($sql_p_ar, $sql_in['parametri']);
      }
            
      if (!is_null($bc) && strlen(trim($bc)) > 0) {
          $sql_where .= "and A.MEMACQ =?";
          $sql_p_ar[] = trim($bc);
      }
      
      if($ubi_blank != 'Y'){
          $sql_where .= " and A.MEUBIC <> ' '";
      }
      
      if(is_numeric($row_mudv['DATA_PARTENZA'])) { //USO DIGITS PER DATI ERRATI SU MVDTGE ++++++++++
          //$sql_where .= " and DIGITS(T.MVDTGE) >= ? ";
          $sql_where .= " and (digits(MVAARG) || digits(MVMMRG) ||  digits(MVGGRG)) >= ? ";
          $sql_p_ar[] = $row_mudv['DATA_PARTENZA'];
      }
      // STD $stacco UA Ubicazione/Articolo
      $sql_select  = ",T.MVDEPO as DEPO , A.MEUBIC as UBIC"; 
      $sql_orderby = " T.MVDEPO, A.MEUBIC, A.MEMACQ, T.MVART, B.ARDART";
      $sql_groupby = " T.MVDEPO, A.MEUBIC, A.MEMACQ, T.MVART, B.ARDART";
      if ($stacco == 'AU') { // Articolo/Ubicazione
          $sql_orderby = " T.MVDEPO, A.MEMACQ, T.MVART, B.ARDART, A.MEUBIC";
          $sql_groupby = " T.MVDEPO, A.MEMACQ, T.MVART, B.ARDART, A.MEUBIC";
      } elseif ($stacco == 'GENERALE') { // Somma la giacenza senza distinzione di deposito/ubicazione
          $sql_select  = "";
          $sql_orderby = " A.MEMACQ, T.MVART, B.ARDART";
          $sql_groupby = " A.MEMACQ, T.MVART, B.ARDART";
      }
      
      $sql = "Select A.MEMACQ as BC
                   , T.MVART  as ART
                   , B.ARDART as DART 
                   , sum(T.MVQTA * A.MESEGN) as GIAC
                   {$sql_select}" 
          . " from {$cfg_mod_DeskPVen['file_movimenti_gest']} T"
          . " join {$cfg_mod_DeskPVen['file_movimenti_gest_estensione']} A on(A.MEDT = T.MVDT and A.MENRRG = T.MVNRRG)"
          . " left outer join {$cfg_mod_DeskPVen['file_anag_art']} B on(B.ARDT = T.MVDT and B.ARART = T.MVART)"
          . " where T.MVDT=? {$sql_where}"
          . " group by {$sql_groupby}"
          . " having sum(T.MVQTA * A.MESEGN) <> 0"
              . " order by {$sql_orderby}";

      $stmt = db2_prepare($conn, $sql);
      $result = db2_execute($stmt, $sql_p_ar);
      while ($row = db2_fetch_assoc($stmt)) {
          $row['data_partenza'] = $row_mudv['DATA_PARTENZA'];
          $ret[] = $row;
      }
      

      return $ret;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _get_oa_from_bc($bc){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $ret = array();
      if (isset($bc) && strlen(trim($bc)) > 0) {
        $sql_p_ar = array($id_ditta_default, $bc);
        $sql = "Select substr(UB.BCFILL,  1, 2) as TIDO"
                  . ", substr(UB.BCFILL,  3, 3) as INUM"
                  . ", dec(substr(UB.BCFILL,  6, 4), 4, 0) as AADO"
                  . ", dec(substr(UB.BCFILL, 10, 6), 6, 0) as NRDO"
                  . ", dec(substr(UB.BCFILL, 16, 6), 6, 0) as NREC"
                  . " from {$cfg_mod_DeskPVen['file_ubicazioni']} UB"
                  . " where UB.BCDT =? and trim(UB.BCCODE) =? ";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, $sql_p_ar);
        $ret = db2_fetch_assoc($stmt);        
        if (isset($ret['AADO']) && $ret['AADO'] > 0) {
            $ret['ordine_forn'] = implode("_", array($ret['AADO'], $ret['TIDO'], $ret['NRDO']));
        }
      }
      
      return $ret;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _get_oc_from_oa_gest($in=null){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $ret = array();
      if (isset($in['NRDO']) && $in['NRDO'] > 0) {
        $sql_p_ar = array($id_ditta_default
            , $in['TIDO']
            , $in['INUM']
            , $in['AADO']
            , $in['NRDO']
            , $in['NREC']
        );
        $sql = "Select RD.RDTIOR, RD.RDINOR, RD.RDANOR, RD.RDNROR, RD.RDREOR, RD.RDDART"
              . " , clie.TDOADO as OADO_cli, clie.TDONDO as ONDO_cli, clie.TDOTPD as OTPD_cli"
              . " , clie.TDVSRF as VSRF_cli, clie.TDCCON as CCON_cli, clie.TDDCON as DCON_cli" 
              . " from {$cfg_mod_DeskPVen['file_righe_doc']} RD" 
                  
              . " Left outer join {$cfg_mod_DeskPVen['file_testate']} clie"
              . " on( clie.TDDT=RD.RDDT" 
              . " and clie.TDOTID = RD.RDTIOR"                     
              . " and clie.TDOINU = RD.RDINOR"  
              . " and clie.TDOADO = RD.RDANOR"                    
              . " and TRIM(clie.TDONDO) = TRIM(DIGITS(RD.RDNROR)))"
                  
              . " where RD.RDDT = ? and RD.RDTIDO = ?"                    
              . " and RD.RDINUM = ? and RD.RDAADO = ?"                   
              . " and RD.RDNRDO = ? and RD.RDNREC = ?";
              
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, $sql_p_ar);
        $ret = db2_fetch_assoc($stmt);
        if (isset($ret['OADO_CLI']) && $ret['OADO_CLI'] > 0) {
          $ret['ordine_clie'] = implode("_", array($ret['OADO_CLI'], $ret['OTPD_CLI'], $ret['ONDO_CLI']));
        }
        
      }
      
      return $ret;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _get_residuo($art){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $ret = '';
      $sql_p_ar = array($id_ditta_default,$art);
      
      $sql = "Select sum(RD.RDQTA - RD.RDQTE) as QTA_RIS_TOT from {$cfg_mod_DeskPVen['file_testate_doc_gest']} TD"
        . " left outer join {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD"
		. " on( TDDT = RDDT   and TDTIDO = RDTIDO " 
        . " and TDINUM=RDINUM and TDAADO=RDAADO and TDNRDO=RDNRDO)"
        . " where TD.TDDT = ? "
        . " and TD.TDTIDO ='MV' and TD.TDTPDO ='PM' and TD.TDSTAT <>'CC'"
        . " and RD.RDART = ? and RD.RDSTEV <> 'S' and RD.RDQTA > RD.RDQTE"
        . " group by RD.RDART"
        . " order by RD.RDART";

        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, $sql_p_ar);
        $row = db2_fetch_assoc($stmt);
        $ret = $row;

      return $ret;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _gest_abilita_ris_mat($art, $qta, $mttido=null, $mtinum=null, $mtaado=null, $mtnrdo=null){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $ret = array();
      $mts = $this->_get_bc_from_art($art);
      
      $ret['bc'] = $mts['bc'];
      $ret['fl_mts'] = $mts['fl_mts'];
      if (!is_null($ret['bc']) && strlen(trim($ret['bc'])) > 0) {
          $giac = $this->_get_ubicazione('',trim($ret['bc']),'GENERALE');
          $ret['giac'] = $giac[0]['GIAC'];
      } else $ret['giac'] = 0;
      
      $riservata = $this->_get_residuo($art);
      $ret['qta_gia_riservata'] = $riservata['QTA_RIS_TOT'];
      
      
      $ret['abilita_ris_mat'] = 'Y';
      
      if ($qta == 0 ||
          $qta > ($ret['giac']-$ret['qta_gia_riservata']) ||
          ($v_m3['M3FG01'] =='P' || $v_m3['M3FG01'] =='R') ) 
        { $ret['abilita_ris_mat'] = 'N'; }
      
          
      if (!is_null($mttido) && strlen(trim($mttido)) > 0) {
        $v_m3 = $this->_verifica_m3($mttido, $mtinum, $mtaado, $mtnrdo);
      }
      
          
      
      
      return $ret;
  }
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  public function _verifica_m3($mttido, $mtinum, $mtaado, $mtnrdo){
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $sql_p_ar = array($id_ditta_default, $mttido, $mtinum, $mtaado, $mtnrdo);
      
      $sql = "Select M3.M3FG01 from {$cfg_mod_DeskPVen['file_dettaglio_wizard']} M3"
		. " where M3.M3DT=? and M3.M3TIDO = ? and M3.M3INUM =? and M3.M3AADO = ? and M3.M3NRDO = ?";
	  $stmt = db2_prepare($conn, $sql);
	  $result = db2_execute($stmt, $sql_p_ar);
	  $row = db2_fetch_assoc($stmt);
      $ret = $row;

      return $ret;
  }
  
      
  //-----------------------------------------------------------------------------
  //-----------------------------------------------------------------------------
  
  public function _get_rd_gest_abbinate_by_k_ordine($k_ordine, $tipologia, $no_comm = ''){                  
      global $conn, $cfg_mod_DeskPVen, $id_ditta_default;
      $ret = array();
      $s = new Spedizioni(array('no_verify' => 'Y'));
      $sql_where = "";
      
      if (strlen($k_ordine) == 0) return $ret; 
      
      
     if($no_comm == 'Y')
          $sql_where .= " AND SUBSTRING(RD.RDART, 1, 1) <> '*'";
      
      $oe = $s->k_ordine_td_decode_xx($k_ordine);
      
      $sql_p_ar = $oe;
      
      $sql = "Select RD.*, RT0.RTPRZ, RT0.RTINFI, TD_ACQ.TDSTAT, TD_ACQ.TDDTRG"
              . " from {$cfg_mod_DeskPVen['file_righe_doc']} RD"
              . " left outer join {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT0
                  on(RT0.RTDT=RD.RDDT and RT0.RTTIDO=RD.RDTIDO and RT0.RTINUM=RD.RDINUM and RT0.RTAADO=RD.RDAADO and RT0.RTNRDO=RD.RDNRDO and RT0.RTNREC=RD.RDNREC AND RT0.RTVALU='EUR') "
                  . " left outer join {$cfg_mod_DeskPVen['file_testate_doc_gest']} TD_ACQ
	              on RD.RDDT = TD_ACQ.TDDT AND RD.RDTIDO = TD_ACQ.TDTIDO AND RD.RDINUM = TD_ACQ.TDINUM AND RD.RDAADO = TD_ACQ.TDAADO AND RD.RDNRDO = TD_ACQ.TDNRDO "
              . " where RD.RDDT = ? and RD.RDTIOR = ?"
              . " and RD.RDINOR = ? and RD.RDANOR = ?"
              . " and RD.RDNROR = ?"
              . " and RD.RDTIDO = " . sql_t($tipologia) . " {$sql_where}";
              
                   
          $stmt = db2_prepare($conn, $sql);
          $result = db2_execute($stmt, $sql_p_ar);
          while ($row = db2_fetch_assoc($stmt)){
              $ret[] = $row;
          }      
      
      return $ret;
  }
  
  
  
  public function anagrafica_cliente_con_dati_completi($cod_cli){
      global $conn, $cfg_mod_Gest, $id_ditta_default;
      $ret = array();
      
      $sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC WHERE GCDT='$id_ditta_default' AND GCCDCF = ?";
      $stmt = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      $result = db2_execute($stmt, array($cod_cli));
      echo db2_stmt_errormsg($stmt);
      $r_cli = db2_fetch_assoc($stmt);
      
      if ($r_cli){
          
          if(trim($r_cli['GCNAZI']) == 'ITA' || trim($r_cli['GCNAZI']) == 'IT' || trim($r_cli['GCNAZI']) == ''){
         
          if ($r_cli['GCTFIS'] == 'P'){ //if Privato
              if (
                  trim($r_cli['GCCOGN']) == '' ||
                  trim($r_cli['GCNOME']) == '' ||
                  trim($r_cli['GCCDFI']) == '' ||
                  trim($r_cli['GCINDI']) == '' ||
                  trim($r_cli['GCLOCA']) == '' ||
                  trim($r_cli['GCCAP']) == ''  ||
                  trim($r_cli['GCPROV']) == '' ||
                  trim($r_cli['GCNAZI']) == '' ||
                  trim($r_cli['GCTEL']) == ''
                  ){
                      $msg_error = 'I dati anagrafici del cliente non sono completi. Inserirli in anagrafica cliente prima di procedere';
                      $ret['success'] = false;
                      $ret['message'] = $msg_error;
                      return $ret;
              }
          } //if Privato
          else { //if Azienda
              if (
                  trim($r_cli['GCDCON']) == '' ||
                  trim($r_cli['GCPIVA']) == '' ||
                  trim($r_cli['GCINDI']) == '' ||
                  trim($r_cli['GCLOCA']) == '' ||
                  trim($r_cli['GCCAP']) == ''  ||
                  trim($r_cli['GCPROV']) == '' ||
                  trim($r_cli['GCNAZI']) == '' ||
                  trim($r_cli['GCTEL']) == ''
                  ){
                      $msg_error = 'I dati anagrafici del cliente non sono completi. Inserirli in anagrafica cliente prima di procedere';
                      $ret['success'] = false;
                      $ret['message'] = $msg_error;
                      return $ret;
              }
          } //if Azienda
        }
      } //if r_cli
      $ret['success'] = true;
      return $ret;
  }
  
  public function exe_evidenza_generica($p){
      global $conn, $id_ditta_default, $cfg_mod_DeskPVen, $auth;
      $ret = array();
      $ar_upd = array();
      $ar_value = array();
      
      $ar_upd['TDUSGE'] 	= SV2_db_user($auth->get_user());
      $ar_upd['TDDTGE']     = oggi_AS_date();
      $ar_upd['TDORGE'] 	= oggi_AS_time();
      $ar_upd['TDFG06'] 	= $p->flag;
      
      foreach($p->list_selected_id as $v){
    
          $sql = "UPDATE {$cfg_mod_DeskPVen['file_testate']} TD
                  SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                  WHERE TDDT = '{$id_ditta_default}' AND TDDOCU = '{$v->k_ordine}' ";
          
          $stmt = db2_prepare($conn, $sql);
          echo db2_stmt_errormsg();
          $result = db2_execute($stmt, $ar_upd);
          echo db2_stmt_errormsg();
          
          $sql_s = "SELECT TDDOCU, TDFG06 FROM {$cfg_mod_DeskPVen['file_testate']} TD
                    WHERE TDDT = '{$id_ditta_default}' AND TDDOCU = '{$v->k_ordine}' ";
          
          $stmt_s = db2_prepare($conn, $sql_s);
          echo db2_stmt_errormsg();
          $result = db2_execute($stmt_s);
          echo db2_stmt_errormsg();
          $r = db2_fetch_assoc($stmt_s);
          
          $ar_value[] = array('k_ordine' => trim($r['TDDOCU']),
                              'flag' => trim($r['TDFG06']));
      
      
      }   
  
      $ret['success'] = true;
      $ret['or_flag'] = $ar_value;
      return acs_je($ret);
  }
  
  
} //class

?>