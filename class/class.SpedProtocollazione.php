<?php

class SpedProtocollazione extends AbstractDb {
 
	protected $table_k 		= "aaaaaaa";
	

	public function exe_protocollazione($form_values){
	  
	    global $tkObj, $id_ditta_default, $conn, $useToolkit, $cfg_mod_Spedizioni, $s;
		global $libreria_predefinita, $libreria_predefinita_EXE;		
		
		ini_set('max_execution_time', 6000);
		
		//per useToolkit = N
		$m_time = microtime(true);
				
		//costruzione del parametro
		$cl_p = str_repeat(" ", 246);
		$cl_p .= $id_ditta_default; //ditta
		$cl_p .= sprintf("%-2s", $form_values->tido);
		$cl_p .= sprintf("%-2s", $form_values->tpdo);		
		$cl_p .= sprintf("%-2s", $form_values->stdo);		
		$cl_p .= sprintf("%0-4s", substr($form_values->dtrg, 0, 4));		
		$cl_p .= sprintf("%09s", $form_values->f_cliente_cod);
		$cl_p .= sprintf("%0-8s", $form_values->dtrg);				
		$cl_p .= sprintf("%0-8s", $form_values->dtep);		
		$cl_p .= sprintf("%-2s", $form_values->prio);		
		$cl_p .= sprintf("%-11s", $form_values->mode); //passato da 3 a 11 per GL

		$cl_p .= sprintf("%-1s", $form_values->endp);		
		$cl_p .= sprintf("%-1s", $form_values->elab);		
		$cl_p .= sprintf("%-2s", $form_values->dexp);
				
		$cl_p .= sprintf("%-30s", $form_values->vsrf);		
		$cl_p .= sprintf("%-3s", trim($form_values->f_destinazione_cod));		
		$cl_p .= sprintf("%0-6s", $form_values->nrdo);
		$cl_p .= sprintf("%-3s", $form_values->tip_prod);				
		$cl_p .= sprintf("%-3s", $form_values->divisione);
		
		//TDDOCU: sono in inserimento quindi passo ''. Adesso passiamo mktime (in ordini promo poi ci aggiunge le righe...)
		//$cl_p .= sprintf("%50s", '');
		$cl_p .= sprintf("%50s", strtr($m_time, array( '.' => ',')));     //con useTookit=N mi serve per il risultato
		
		$cl_p .= sprintf("%-1s", '');		   //modifica ( /E/M)
		$cl_p .= sprintf("%-3s", '');		   //pagamento
		
		
		global $backend_ERP;
		if ($backend_ERP == 'GL')
		  $cl_p .= sprintf("%-10s", $form_values->progetto);
        else
		  $cl_p .= sprintf("%-30s", $form_values->progetto);
		
		
		$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_invio_conferma));
		$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_attesa_conferma));
		$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_preferenza));
		
		global $auth;
		$cl_p .= sprintf("%-10s", trim(substr($auth->get_user(), 0, 10)));
		
		
		$cl_p .= sprintf("%-15s", ''); //acconto 12+3 qui non gestito
		$cl_p .= sprintf("%-15s", ''); //caparra 12+3 qui non gestito
		$cl_p .= sprintf("%-15s", ''); //imp. trasporto 12+3 qui non gestito
		
		$cl_p .= sprintf("%-3s", trim($form_values->referente)); //IN GL e' assoggettamento	
		
		$cl_p .= sprintf("%-1s", ''); //O=ordine, P=preventivo
		$cl_p .= sprintf("%0-8s", $form_values->dtva);			//data validita (qui non gestita)
		
		$cl_in 	= array();
		$cl_out = array();
		
		if ($useToolkit == 'N'){
			//per test in Apra
				$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21H5C('{$cl_p}')";
				$stmt1   = 	db2_prepare($conn, $qry1);
				$result1 = 	db2_execute($stmt1);
				//$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
			
				//recupero il risultato da RI (nrdo e tddocu)
				$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_richieste']} WHERE RIDT=? AND RITIME=? AND RIRGES=?";
				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt, array($id_ditta_default, $m_time, 'ESI_GEN_DOCU'));
				echo db2_stmt_errormsg($stmt);
				$r = db2_fetch_assoc($stmt);
				
				$m_return = str_repeat(" ", 333);
				$m_return .= sprintf("%0-6s", trim($r['RINRDO']));
				$m_return .= str_repeat(" ", 6);
				$m_return .= sprintf("%-50s", trim($r['RINOTE']));
				
				$call_return['io_param']['LK-AREA'] = $m_return;
			
			//FINE test per Apra			
		} else {
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
			$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
			$call_return = $tkObj->PgmCall('UR21H5C', $libreria_predefinita_EXE, $cl_in, null, null);		
		}
		
		
		
		$ret = array();
		if ($call_return){
		    
		    //MESSAGGIO PUNTO VENDITA 
		    $k_ordine = substr($call_return['io_param']['LK-AREA'], 345, 50);
		    
		    $sh = new SpedHistory();
		    $sh->crea(
		        'pers',
		        array(
		            "messaggio"	=> 'INS_PVEN',
		            "k_ordine" => $k_ordine,
		            "vals" => array(
		                "RICITI" 	=> $form_values->f_pven_cod,
		              
		            ),
		        )
		        );
		    
		    
		    if(isset( $form_values->f_promo) && strlen($form_values->f_promo) > 0){
		        
		        $d_art = $s->get_descrizione_articolo($id_ditta_default, $form_values->f_promo);
		        
    		    $sh = new SpedHistory();
    		    $sh->crea(
    		        'pers',
    		        array(
    		            "messaggio"	=> 'INS_COMP_PROM',
    		            "k_ordine" => $k_ordine,
    		            "vals" => array(
    		                "RIART" 	=> $form_values->f_promo,
    		                "RIDART" 	=> trim($d_art),
    		                "RIQTA" 	=> 1,
    		                
    		            ),
    		        )
    		        );
		    }
		    
			$ret['success'] = true;
			$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
		} else {
			$ret['success'] = false;
		}
	
		return acs_je($ret);		

	} 
	
	
	public function get_stmt_rows($p = array()){
		global $conn;
		global $_REQUEST;
		global $cfg_mod_Spedizioni;
		
		$s = new Spedizioni();
		
		
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} WHERE 1 = 0";

		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		return $stmt;				
	}






	
	
public function out_Writer_Form_initComponent() {
    global $cfg_mod_Spedizioni, $cfg_protocollazione;
 global $backend_ERP;

 ///////////////////////////////////////////////////
 // GL
 ///////////////////////////////////////////////////
 if ($backend_ERP == 'GL'){
 	$ret =  "
        Ext.apply(this, {
   			overflowY: 'auto', autoScroll: 'true', height: 500, 			
            activeRecord: null,
            iconCls: 'icon-blog_add-16',
            frame: true,
            title: 'Intestazione nuovo ordine',
            defaultType: 'textfield',
            bodyPadding: 20,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            	{name: 'tido', xtype: 'hidden', value: 'VO'}
 			  , {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,
			allowBlank: false,
            store: {
            	pageSize: 1000,
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },
				fields: ['cod', 'descr', 'out_ind', 'out_loc', 'out_ema', 'out_tel'],
            },
 	
			valueField: 'cod',
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
 	
 	
	        listeners: {
	            change: function(field,newVal) {
	            	var form = this.up('form').getForm();
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();
	            },
				select: function(combo, row, index) {
	            	var form = this.up('form').getForm();
	     			form.findField('out_ind').setValue(row[0].data.out_ind);
	     			form.findField('out_loc').setValue(row[0].data.out_loc);
 			        form.findField('out_ema').setValue(row[0].data.out_ema);
	     			form.findField('out_tel').setValue(row[0].data.out_tel);
 	
 					//imposto quella a standard
 					console.log('select');
					form.findField('f_destinazione_cod').store.load({
	 					callback: function(records, operation, success) {
 							if (records.length == 1){
 								this.setValue(records[0].get('cod'));
 							} else {
 								this.setValue('');
								form.findField('out_ind_dest').setValue('');
	     						form.findField('out_loc_dest').setValue(''); 			
 							}
    					}, scope: form.findField('f_destinazione_cod')
 					});
 					console.log('ok_load');
 					//console.log(form.findField('f_destinazione_cod').getStore().data);
 	
//					all_dest = Ext.pluck(form.findField('f_destinazione_cod').getStore().data.items, 'data')
// 					console.log(all_dest);
 	
				}
 	
 	
	        },
 	
 	
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
 	
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{descr}</span>[{cod}]</h3>' +
 						' {out_ind}<br/>' +
                        ' {out_loc}' +
                    '</div>';
                }
 	
            },
 	
            pageSize: 1000
        }
 	
 		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo',
 			name: 'out_ind',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;',
 			name: 'out_loc',
 		},  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Email',
 			name: 'out_ema',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Tel/Fax',
 			name: 'out_tel',
 		}
 	
		, {
            xtype: 'combo',
			name: 'f_destinazione_cod',
			fieldLabel: 'Destinazione',
			minChars: 2,
			allowBlank: true,
            store: {
            	pageSize: 1000,
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_des_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            },
                 extraParams: {
        		  	cliente_cod: ''
        		 }
		        },
				fields: ['cod', 'descr', 'denom', 'IND_D', 'LOC_D'],
            },
 	
			valueField: 'cod',
            displayField: 'denom',
            typeAhead: false,
            hideTrigger: false, //mostro la freccetta di selezione
            anchor: '100%',
 	
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessuna destinazione trovata',
 	
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{denom}</span></h3>' +
                        '{IND_D}<BR/>' +    			
                        '{LOC_D}' +
                    '</div>';
                }
 	
            },
 	
	        listeners: {
 	
				change: function(combo, row, index) {
	            	var form = this.up('form').getForm();
	     			form.findField('out_ind_dest').setValue(this.valueModels[0].get('IND_D'));
	     			form.findField('out_loc_dest').setValue(this.valueModels[0].get('LOC_D'));
				},
				select: function(combo, row, index) {
	            	var form = this.up('form').getForm();
	     			form.findField('out_ind_dest').setValue(row[0].data.IND_D);
	     			form.findField('out_loc_dest').setValue(row[0].data.LOC_D);
				},
 				render: function( component ) {
                	component.getEl().on('dblclick', function( event, el ) {
 						console.log('aaaaa');
 						var form = component.up('form').getForm();
                    	acs_show_win_std('Gestione destinazioni', 'acs_gestione_destinazioni_GL.php?fn=open_grid', {cod_cli: form.findField('f_cliente_cod').value}, 800, 550, {
 									beforeClose: function(){
 										component.store.load()
 									}								  
								  }, 'icon-shopping_cart_gray-16');
                	});
            	}
 	
 	
	        },

 			
			viewConfig: {
			    listeners: {
			        itemdblclick: function(dataview, index, item, e) {
			            alert('dblclick');
			        }
			    }
			}, 			
 			
 	
            pageSize: 1000
        }
 	
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo',
 			name: 'out_ind_dest',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;',
 			name: 'out_loc_dest',
 		}
 	
 	
 	
		, {
				name: 'divisione',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Divisione',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,
 			    value: " . j($cfg_mod_Spedizioni["protocollazione"]["divisione"]["DEF"]) . ",
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('DDOC'), '') . "
					    ]
				},
 	
		        listeners: {
					select: function(combo, row, index) {
		            	var form = this.up('form').getForm();
						tpdo_combo = form.findField('tpdo');
					    tpdo_combo.getStore().proxy.extraParams.ddoc = row[0].data.id;
					    tpdo_combo.getStore().load();
					}
 	
		        }
 	
			}
 	
 	
		, {
				name: 'tpdo',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Tipo ordine',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			    allowBlank: false,
					     		
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
                              " .  acs_ar_to_select_json(find_TA_sys('BDOC', null, null, null, null, null, 0, '', 'Y'), '') . "
					    ]
				},					     		
 	
/*					     		
	            store: {
	            	pageSize: 1000,
					proxy: {
			            type: 'ajax',
			            url : 'acs_get_select_json.php?select=get_el_combo_bdoc',
			            reader: {
			                type: 'json',
			                root: 'options',
			                totalProperty: 'totalCount'
			            },
	                 extraParams: {
	        		  	ddoc: '2222222'
	        		 }
			        },
					fields: ['id', 'text']
	            },
*/
 	
				
			}
 	
 	
 	
 	
				, {
				     name: 'vsrf'
				   , xtype: 'textfield'
				   , fieldLabel: 'Riferimento'
				   , maxLength: 30
				}
 	
		, {
				name: 'mode',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Modello',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('PUVN', null, null, 'MOD', null, null, 0, '', 'Y'), '') . "
					    ]
				},
					     		
							//ricerca locale
							typeAhead: true,
        					queryMode: 'local',                             
        					anyMatch: true, 		//disponibile solo dalla versione 4.2.1
        											//uso questo listerners per il filtro dei dati con anyMatch
							listeners   : {
								beforequery: function(record){  
								    record.query = new RegExp(record.query, 'ig');
        							record.forceAll = true;
								}
							}					     		
					     		
					     		
					     		
			}
				     							     		
					     		
			, {
				     name: 'progetto'
				   , xtype: 'textfield'
				   , fieldLabel: 'Progetto grafico'
				   , maxLength: 10
			}					     		
					     		
 	
		, {
				name: 'tip_prod',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Tipologia produzione',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,
				value: " . j($cfg_mod_Spedizioni["protocollazione"]["tip_prod"]["DEF"]) . ",
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('BLTO', null, null, null, null, explode_if_isset("|", $cfg_mod_Spedizioni["protocollazione"]["tip_prod"]["LIST"])), '') . "
					    ]
				}
			}
 	
 	
		, {
				name: 'prio',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Priorit&agrave',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,
				value: " . j($cfg_mod_Spedizioni["protocollazione"]["prio"]["DEF"]) . ",
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}, {name:'tarest'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('BPRI', null, null, null /*'VO' */, null, null, 1), '') . "
					    ]
				}
			}
 	
				, {
				     name: 'dtep'
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Cons. richiesta'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
 	
				   , validator: function(value){
                            if (!Ext.isEmpty(form.findField('prio').valueModels))
					     	if (Ext.Array.contains(['>', '<', 'W', '='], form.findField('prio').valueModels[0].get('tarest')))
					        	return 'Indicare una data consegna richiesta';
					     	return true;
					    }
 	
				}
 	
			, {
				     name: 'dtrg'
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Ricezione'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: false
				   , value: '" . print_date(oggi_AS_date(), "%d/%m/%Y") . "'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
				}




		, {
				name: 'referente',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Assoggett. C',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}, {name:'tarest'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('B0D'), '') . "
					    ]
				}
			}




 	
/*				   		
		, {
				name: 'stdo',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Stato',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				value: " . j($cfg_mod_Spedizioni["protocollazione"]["stdo"]["DEF"]) . ",
				forceSelection: true,
			   	allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " .  acs_ar_to_select_json(find_TA_sys('BSTA', null, null, 'VO', null, explode_if_isset("|", $cfg_mod_Spedizioni["protocollazione"]["stdo"]["LIST"])), '') . "
					    ]
				}
			}
 */	
 	
    
 	
 	
            ],
			dockedItems: [" . self::form_buttons()  . "]
        });
        this.callParent();
   "; 	 	
 } else {
     
   
   ///////////////////////////////////////////////////
   // SV2
   ///////////////////////////////////////////////////
   
     if($cfg_mod_Spedizioni['protocollazione_art_promo'] == 'Y'){
         
         $art_promo = " , {
						
			            xtype: 'combo',
						name: 'f_promo',
						fieldLabel: 'Composizione promozionale',
						anchor: '-15',	
						minChars: 2,	
						forceSelection:true,		
            			store: {
			            	pageSize: 1000,            	
							proxy: {
								   url: 'acs_proposte_ordine.php?fn=get_json_data_articoli', 
								   method: 'POST',								
								   type: 'ajax',
                                    actionMethods: {
							          read: 'POST'
							        },
							        extraParams: { sottogruppo : 'PRO'},
			        				 doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},      
							fields: ['id', 'text'],		             	
			            },
                        
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',
                        listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun articolo trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class=\"search-item\">' +
			                        '<h3><span>{id}</span></h3>' +
			                        '{text}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        	       }	";
         
     }
     
     
 
   $ret =  "        
        Ext.apply(this, {
   			overflowY: 'auto', autoScroll: 'true', height: 500,  		
            activeRecord: null,
            iconCls: 'icon-blog_add-16',
            frame: true,
            title: 'Intestazione nuovo ordine',
            defaultType: 'textfield',
            bodyPadding: 20,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            	{name: 'tido', xtype: 'hidden', value: 'VO'}
 			  , {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,			
			allowBlank: false,            
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
               /* change: function(field,newVal) {
	            	var form = this.up('form').getForm(); 	            	
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();
	            }, */

				select: function(combo, row, index) {
                    var form = this.up('form').getForm();
	     			form.findField('out_ind').setValue(row[0].data.out_ind);									     			
	     			form.findField('out_loc').setValue(row[0].data.out_loc);

 					//imposto quella a standard
					/*form.findField('f_destinazione_cod').store.load({
	 					callback: function(records, operation, success) {
 							if (records.length == 1){
 								this.setValue(records[0].get('cod'));
 							}
    					}, scope: form.findField('f_destinazione_cod') 	
 					});*/
				} 		
	        },
            
            
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{descr}</span>[{cod}]</h3>' +
 						' {out_ind}<br/>' +
                        ' {out_loc}' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        }
 		
 		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind', 		
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc', 		
 		}

		, {
			xtype: 'fieldcontainer',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_destinazione_cod',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'f_destinazione',
				    fieldLabel: 'Destinazione',
                    flex : 1,
                    labelAlign: 'right',
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: '<img src=" . img_path("icone/48x48/search.png") . " width=16>',
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_destinazione_cod').setValue(record_selected.cod);	
												form.findField('f_destinazione').setValue(record_selected.denom);
                                                form.findField('out_ind_dest').setValue(record_selected.IND_D);
                                                form.findField('out_loc_dest').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
							  
									acs_show_win_std('Seleziona destinazione', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, type : 'D'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
 		
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_dest', 		
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_dest', 		
 		} 		
 		, {
		     name: 'vsrf'                		
		   , xtype: 'textfield'
		   , fieldLabel: 'Riferimento'
		   , maxLength: 30
		}
 		,  {
				name: 'mode',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Modello',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,	
                queryMode: 'local',
				minChars: 1,												
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " .   acs_ar_to_select_json(find_TA_sys('PUVN', null, null, 'MOD', null, null, 0, '', 'Y'), '') . "
					    ] 
				},
		        listeners: {
            		beforequery: function (record) {
        			record.query = new RegExp(record.query, 'i');
        			record.forceAll = true;
            		}
                 }						 
			}
		 , {
				     name: 'dtrg'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data ricezione'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: false
				   , value: '" . print_date(oggi_AS_date(), "%d/%m/%Y") . "'				   		
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
				},{
				name: 'divisione',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Divisione',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
   				value: " . j($cfg_mod_Spedizioni["protocollazione"]["divisione"]["DEF"]) . ",														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " .  acs_ar_to_select_json(find_TA_sys('DDOC'), '') . " 	
					    ] 
				}, 

		        listeners: {
					select: function(combo, row, index) {
		            	var form = this.up('form').getForm();
						tpdo_combo = form.findField('tpdo');
					    tpdo_combo.getStore().proxy.extraParams.ddoc = row[0].data.id;
					    tpdo_combo.getStore().load();	
					} 		
	 		
		        }					     		
					     		
			} 		
 		
 		
		, {
				name: 'tpdo',
				xtype: 'combo',
            	anchor: '100%',
				fieldLabel: 'Tipo ordine',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			    allowBlank: false,
                queryMode: 'local',
					     		
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}, 'TACOR2'],
				    data: 
                              " .  acs_je(find_TA_sys('BDOC', null, null, $cfg_protocollazione['heading']['TDTPDO_TACOR2'], null, null, 0, '', 'Y', 'N', true, true)) . "                             
					    
				},
                
                listeners: {

					select: function(combo, row, index) {
                        var form = this.up('form').getForm();
                        var tacor2 = row[0].get('TACOR2').trim();
                        var cfg_heading = Ext.getCmp('panel-protocollazione').cfg_protocollazione.heading;
                        
                        form.findField('tido').setValue(tacor2);

                        if(!Ext.isEmpty(cfg_heading[tacor2])){ 
                               for (var camp in cfg_heading[tacor2]) { 
                               form.findField(camp).setValue(cfg_heading[tacor2][camp].def);
                               form.findField(camp).allowBlank = cfg_heading[tacor2][camp].allowBlank;
                               if(cfg_heading[tacor2][camp].hidden == true){
                                  form.findField(camp).hide();
                                  form.findField(camp).allowBlank = true;
                               }else
                                  form.findField(camp).show();
                               
                                if(!Ext.isEmpty(cfg_heading[tacor2][camp].list))
                                    form.findField(camp).getStore().proxy.extraParams.list = cfg_heading[tacor2][camp].list;
                               
                             }
                          }                    
                        

                        form.findField('stdo').getStore().proxy.extraParams.tacor2 = tacor2;
                        form.findField('stdo').getStore().load({
                               scope: this,
                               callback: function(records, operation, success) {
                                     form.findField('stdo').setValue(cfg_heading[tacor2].stdo.def);  
                               }
                            });

                        form.findField('prio').getStore().proxy.extraParams.tacor2 = tacor2;
                        form.findField('prio').getStore().load({
                               scope: this,
                               callback: function(records, operation, success) {
                                     form.findField('prio').setValue(cfg_heading[tacor2].prio.def);  
                               }
                            });
                      
					},

                	 beforequery: function (record) {
                        record.query = new RegExp(record.query, 'i');
                        record.forceAll = true;
                    }
                }				     		
 	
			}	
					     		
			, {
				name: 'stdo',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Stato',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,														
				store: {
                    xtype: 'store',
					autoLoad:false,
				    proxy: {
		               url : '../base/acs_get_stati_doc.php?fn=get_json_data',
		               method: 'POST',								
					   type: 'ajax',
                       actionMethods: {read: 'POST'},
					   extraParams: {
										list: '',
                                        tacor2 : '' 
			        				},
			           doRequest: personalizza_extraParams_to_jsonData,	
					   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
				      fields: ['id', 'text']
                 }, 
 						 
			}
            , {
				     name: 'dtva'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data validit&agrave;'
                   , format: 'd/m/Y'
                  // , hidden : true
				   , submitFormat: 'Ymd'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
									     		
					     		
				}	
	
		, {
				name: 'prio',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Priorit&agrave',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,					     																
				store: {
                    xtype: 'store',
					autoLoad:false,
				    proxy: {
		               url : '../base/acs_get_prio_doc.php?fn=get_json_data',
		               method: 'POST',								
					   type: 'ajax',
                       actionMethods: {read: 'POST'},
					   extraParams: {
									   tacor2 : '' 
			        				},
			           doRequest: personalizza_extraParams_to_jsonData,	
					   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
				      fields: ['id', 'text']
                 },					 
			}	

				, {
				     name: 'dtep'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Consegna richiesta'
                   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'		   		
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
					     		
				   , validator: function(value){
                            if (!Ext.isEmpty(form.findField('prio').valueModels))
					     	if (Ext.Array.contains(['>', '<', 'W', '='], form.findField('prio').valueModels[0].get('tarest')))
					        	return 'Indicare una data consegna richiesta';
					     	return true;
					    }					     		
					     		
				}					     		
	     		, {
			xtype: 'fieldcontainer',
			flex : 1,
			anchor: '-10',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_pven_cod',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'f_pven',
				    fieldLabel: 'Punto vendita',
                    labelAlign : 'right',
                    flex : 1,
                    allowBlank : true,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				    value: '<img src=" . img_path("icone/48x48/search.png") . " width=16>',
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_pven_cod').setValue(record_selected.cod);	
												form.findField('f_pven').setValue(record_selected.denom);
                                                form.findField('out_ind_pven').setValue(record_selected.IND_D);
                                                form.findField('out_loc_pven').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
							  
									acs_show_win_std('Seleziona punto vendita', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, type : 'P'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_pven', 	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_pven', 		
 			anchor: '100%'
				
			}
          {$art_promo}   		
            ],
			dockedItems: [" . self::form_buttons()  . "]
        });
        this.callParent();
   ";
 } 	
 return $ret;
}



public static function form_buttons($b_pre = '') {
    global $backend_ERP, $conn, $cfg_mod_Spedizioni, $auth, $id_ditta_default;
 
 //verifico se l'utente corrente ha abilitato un profilo di ricezione
 $sql = " SELECT TA_PR.* FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
                    INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_PR
                       ON TA.TADT = TA_PR.TADT AND TA_PR.TATAID = 'EMPRF' AND TA.TAKEY2 = TA_PR.TAKEY1
			             WHERE TA.TADT = '{$id_ditta_default}'
                         AND TA.TATAID = 'OEPRU' AND TA.TAKEY1 = ?";
 
 $stmt = db2_prepare($conn, $sql);
 echo db2_stmt_errormsg();
 $result = db2_execute($stmt, array($auth->get_user()));
 $row = db2_fetch_assoc($stmt); //per ora prendo il primo
 

 if ($row){
     
     $b_email = "
       {
            iconCls: 'icon-address_black-16',
			// scale: 'large',
            text: 'Flusso e-mail',
            disabled: false,
            scope: this,
            handler: function(b){

                var email_config = {
                    profilo_cod: " . j(trim($row['TAKEY1'])) . ",
                    server: " . j(trim($row['TAINDI'])) . ",
                    username:  " . j(trim($row['TARIF1'])) . ",
                    password: " . j(trim($row['TARIF2'])) . ",
                    folder: " . j(trim($row['TATELE'])) . ",
                    folder_to: " . j(trim($row['TALOCA'])) . "
                  };

                acs_show_win_std(null, '../webmail/acs_panel_webmail.php?fn=open_tab', {
                  email_config: email_config
                }, null, null, {
                    afterProtocolla: function(from_win, p){
                        console.log('afterProtocolla!!!!');
                        
                        form_values = p.form_values;
                        comp_cli = b.up('form').getForm().findField('f_cliente_cod');
                        comp_des = b.up('form').getForm().findField('f_destinazione');
                        comp_des_cod = b.up('form').getForm().findField('f_destinazione_cod');
                        comp_ind_dest = b.up('form').getForm().findField('out_ind_dest');
                        comp_loc_dest = b.up('form').getForm().findField('out_loc_dest');

                        comp_pven = b.up('form').getForm().findField('f_pven');
                        comp_pven_cod = b.up('form').getForm().findField('f_pven_cod');
                        comp_ind_pven = b.up('form').getForm().findField('out_ind_pven');
                        comp_loc_pven = b.up('form').getForm().findField('out_loc_pven');
                        
                        //carico nello store il cliente selezionato.
                        //eseguo l'evento 'select' per far aggiornare i campi di visualizzazione localita, ...
                        // (attenzione: in load Data serve sempre passare un array, non solo il record)
                        comp_cli.store.loadData([p.r_cli]);
                        comp_cli.setValue(form_values.f_cliente_cod);
                        comp_cli.fireEvent('select', comp_cli, comp_cli.valueModels);	

                        comp_des.setValue(form_values.f_destinazione);
						comp_des_cod.setValue(form_values.f_destinazione_cod);
                        comp_loc_dest.setValue(form_values.out_loc_dest_h);
                        comp_ind_dest.setValue(form_values.out_ind_dest_h);
                        console.log(form_values);
                        comp_pven.setValue(form_values.f_pven);
						comp_pven_cod.setValue(form_values.f_pven_cod);
                        comp_loc_pven.setValue(form_values.out_loc_pven_h);
                        comp_ind_pven.setValue(form_values.out_ind_pven_h);
                       
                        b.up('form').getForm().add_email_messages = {
                            email_config: email_config,
                            id_messages: p.open_request.id_messages
                        },
                        from_win.destroy();
                    }
                } //listeners
               );
            }
        },
    ";
 } else {
     $b_email = '';
 }
 
 
 $ret = "
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [
            
                    {$b_email}

 		        	'->',
                    
                    {
                    iconCls: 'icon-button_black_repeat-16',
 					// scale: 'large',
                    itemId: 'listino',
                    text: 'Protocolla',
                    disabled: false,
                    scope: this,
 					
 					call_protocolla: function(form, m_button, m_grid){
                            Ext.getBody().mask('Protocollazione in corso', 'loading').show();		
							Ext.Ajax.request({
							        url        : 'acs_op_exe.php?fn=exe_protocollazione',
 		                            timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										Ext.getBody().unmask();
                                        var jsonData = Ext.decode(result.responseText); 		
 										fv = form.getValues();

                                        var tddocu = Ext.util.Format.substr(jsonData.call_return, 345, 50);

                                        //se devo allegare messaggi di email selezionati
                                        if (!Ext.isEmpty(form.add_email_messages)){
                                            var clone_add_email_messages = form.add_email_messages;
                                            console.log('allego email selezionate!!!');
            								  Ext.each(form.add_email_messages.id_messages, function(message_id) {
                                                  
                                                  Ext.Ajax.request({
                							        url        : '../../module/webmail/get_binary_email.php',
                							        method     : 'POST',
                	 								jsonData:  {
                                                        open_request: {email_config: clone_add_email_messages.email_config},
                                                        message_id: message_id
                                                    },  
                							        success : function(result, request){
                                                        var jsonData2 = Ext.decode(result.responseText); 

                                                        //upload
                                                        Ext.Ajax.request({
            								                url        : 'acs_upload.php?fn=exe_upload_content',
            								                method     : 'POST',
            					        			        jsonData: {
            					        				       //k_ordine: '1_VO_VO1_2020_765432_ _1',
                                                               k_ordine: tddocu,
                                                               file_name: 'from_email.eml',
                                                               content: jsonData2.email,
            					        				       from_visualizza_order_img: 'Y'
            										        },							        
            								                success : function(result, request){
            								        	      
                                                              var jsonData3 = Ext.decode(result.responseText); 
                                                              //move to imortati
                                                              if (jsonData3.success){              
                                                                Ext.Ajax.request({
                            							          url        : '../../module/webmail/move_to_importati.php',
                             		                              method     : 'POST',
                            	 								  jsonData:  {
                                                                    open_request: {email_config: clone_add_email_messages.email_config},
                                                                    message_id: message_id
                                                                  }});
                                                              }
            								     	        },
            								                failure    : function(result, request){
            								                    Ext.Msg.alert('Message', 'No data to be loaded');
            								                }
            								            });		


                                                    }
                                                  });


                                                }, this);
                                            

                                            //pulisco variabili di appoggio
                                            form.add_email_messages = null;
                                        }

 		
 										//progressivo e ora generazione
	 										if (typeof(form.id_prog) === 'undefined') form.id_prog = 0;
	 										form.id_prog += 1;
	 										fv['id_prog'] = form.id_prog;
 		
 											newDate = new Date();
 											fv['ora_generazione'] = newDate; 
 		
	 									fv['f_cliente_des'] = form.findField('f_cliente_cod').rawValue;
	 									fv['f_destinazione_des'] = form.findField('f_destinazione_cod').rawValue; 		
	 									fv['tpdo_des'] = form.findField('tpdo').rawValue;
 		 								fv['mode_des'] = form.findField('mode').rawValue;
	 									fv['nrdo'] = Ext.util.Format.substr(jsonData.call_return, 333, 6);
 										
	 							        fv['tddocu'] = tddocu;
 		
										m_grid.getStore().add(fv);            
	            						// form.reset();
 		
	 									my_listeners = {
				        					afterUpdateRecord: function(from_win){
 		
			 									//Pulisco alcuni campi della form
		 										form.findField('f_cliente_cod').setValue('');
		 										form.findField('out_ind').setValue('');
		 										form.findField('out_loc').setValue('');
		 										form.findField('f_destinazione_cod').setValue('');
		 										form.findField('out_ind_dest').setValue('');
		 										form.findField('out_loc_dest').setValue(''); 		
		 										form.findField('vsrf').setValue('');
		 										form.findField('mode').setValue('');
		 										form.findField('dtep').setValue(''); 		
 		
 												if (Ext.isEmpty(form.findField('progetto')) != true)
 													form.findField('progetto').setValue('');
 		
				        						from_win.close();
								        	}								  
										}	
 		
	 		";
 
        if ($backend_ERP == 'GL'){
 		     $ret .= "						//apro la form per visualizzare/modificare alcune info di testata
 										acs_show_win_std('Ordine protocollato. Modifica informazioni di testata', 'acs_panel_protocollazione.php?fn=modifica_info_testata', {tddocu: tddocu}, 500, 350, my_listeners, 'icon-shopping_cart_gray-16');

                ";
        }
 		
 		
 		$ret .= "
 										m_button.enable();
	 		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	 		
 					},
 		
 		
                    handler: function(b){
 						form = this.getForm();
 						m_grid = this.up('panel').down('grid');
 						m_button = b;
 						//m_button.disable();
 		
 						if(form.isValid()){
 		                            m_button.disable();
 									Ext.Ajax.request({
							        url        : 'acs_panel_protocollazione.php?fn=check_cliente_riferimento',
 		                            timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText);
 										if (jsonData.num_find > 0){
 											//chiedo confirm
 											console.log('chiedo conferma');
 		
                         		       Ext.Msg.confirm('Richiesta conferma', 'Esistono ordini gi&agrave; presenti con cliente/riferimento: <br> '+ jsonData.message +'<br> Vuoi procedere?',
                             				 function(btn, text){																							    
                        								if (btn == 'yes'){																	         	
                        										b.call_protocolla(form, m_button, m_grid);							         	
                        																	      				         	
                        								}else{
                         	    								m_button.enable();
                        								}
                        					   });
 		
 		
 										} else 
 											b.call_protocolla(form, m_button, m_grid);
	 		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	 
 		
 		
 							 				
 						} else {
 							m_button.enable();
 						}
 		
                    }
                }]
            } 
 ";	
 return $ret;
}	



	
	
public function out_Writer_Grid_initComponent_columns() {
    global $backend_ERP;
    
	$ret = "{
                text: '#',
                width: 20,
                sortable: true,
                dataIndex: 'id_prog'
            },{
                text: 'Ora',
                width: 60,
                sortable: true,
                dataIndex: 'ora_generazione',
				xtype: 'datecolumn',
   				format: 'H:i:s'			
            }, {
                text: 'Denominazione',
                flex: 60,
                sortable: true,
                dataIndex: 'f_cliente_des',
                allowBlank: false,
	    	    	renderer: function (value, metaData, record, row, col, store, gridView){																						
						return value + ' [' + record.get('f_cliente_cod') + ']';			    
					}			
            }, {
                text: 'Destinazione',
                flex: 40,
                sortable: true,
                dataIndex: 'f_destinazione_des',
                allowBlank: false
            }, {
                text: 'Riferimento',
                flex: 30,
                sortable: true,
                dataIndex: 'vsrf'
            }, {
                text: 'Data',
                width: 40,
                sortable: true,
                dataIndex: 'dtrg',
                allowBlank: false, renderer: date_from_AS
            }, {
                text: 'Tipo',
                flex: 30,
                sortable: true,
                dataIndex: 'tpdo_des'
            }, {
                text: 'Numero',
                width: 65,
                sortable: true,
                dataIndex: 'nrdo'
            }, {
                text: 'Modello',
                width: 65,
                sortable: true,
                dataIndex: 'mode_des'
            }

        /*
            , {
	            xtype: 'actioncolumn',
				text: '<img src=" . img_path("icone/16x16/barcode.png") . " width=20>',
	            width: 30, menuDisabled: true, sortable: false, 
	            items: [{
	                icon: " . img_path("icone/16x16/barcode.png") . ",
	                // Use a URL in the icon config
	                tooltip: 'Avvia sma1bar',
	                handler: function (grid, rowIndex, colIndex) {
	                    var rec = grid.getStore().getAt(rowIndex);
						allegatiPopup('acs_panel_protocollazione.php?fn=get_bat_file&nrdo=' + rec.get('nrdo'));	                		
	                }
	            }]
	        }
        */
    ";

    
	if ($backend_ERP == 'GL')
        $ret .= "
               , {
    	            xtype: 'actioncolumn',
    				text: '<img src=" . img_path("icone/16x16/pencil.png") . " width=20>',
    	            width: 30, menuDisabled: true, sortable: false,
    	            items: [{
    	                icon: " . img_path("icone/16x16/pencil.png") . ",
    	                // Use a URL in the icon config
    	                tooltip: 'Modifica testata',
    	                handler: function (grid, rowIndex, colIndex) {
    	        			var rec = grid.getStore().getAt(rowIndex);
     						acs_show_win_std('Ordine protocollato numero ' + rec.get('nrdo') + '. Modifica informazioni di testata', 
    	                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
    	                				doc_type: grid.up('panel').up('panel').doc_type,
    	                				tddocu: rec.get('tddocu')
    								}, 600, 400, {}, 'icon-shopping_cart_gray-16');
    	                }
    	            }]
    	        }           		
    	";
	
        if ($backend_ERP == 'SV2')
            $ret .= "
               , {
    	            xtype: 'actioncolumn',
    				text: '<img src=" . img_path("icone/16x16/pencil.png") . " width=20>',
    	            width: 30, menuDisabled: true, sortable: false,
    	            items: [{
    	                icon: " . img_path("icone/16x16/pencil.png") . ",
    	                // Use a URL in the icon config
    	                tooltip: 'Modifica testata ordine',
    	                handler: function (grid, rowIndex, colIndex) {
    	        			var rec = grid.getStore().getAt(rowIndex);
     						acs_show_win_std('Ordine protocollato numero ' + rec.get('nrdo') + '. Modifica informazioni di testata',
    	                			'../base/acs_panel_testata_ordine_gest_SV2.php?fn=open_form', {
    	                				tddocu: rec.get('tddocu')
    								}, 800, 600, {}, 'icon-shopping_cart_gray-16');
    	                }
    	            }]
    	        }
    	";
        
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'CSPROG',
    fields: [{
        name: 'CSPROG',
        type: 'int',
        useNull: true
    }, 'f_cliente_cod', 'f_cliente_des', 'f_destinazione_cod', 'f_destinazione_des',
    'tpdo', 'tpdo_des', 'aado', 'nrdo', 'id_prog', 'ora_generazione', 'mode', 
    'mode_des', 'dtrg', 'vsrf', 'tddocu']
});
";

}	
	
	
	
}

?>