<?php

class DeskAcqRisorse extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TATAID,TAKEY1,TAKEY2";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "RISEN";
	protected $tadt_f 		= "TADT";	
	
	
	public function form_buttons_2() {
	
		$cl= new ListinoTrasportatori();
		$cl_itinerari = new TrasportatoriItinerari();
	
		return "
		{
			xtype: 'toolbar',
			dock: 'bottom',
			ui: 'footer',
			items: ['->', {
				iconCls: 'icon-calendar-16',
				itemId: 'listino',
				text: 'Disponibilit&agrave;',
				disabled: false,
				scope: this,
				handler: function(){
					cod_risorsa = this.activeRecord.get('TAKEY2').trim() + '|' + this.activeRecord.get('TAKEY1').trim();
					acs_show_win_std('Gestione disponibilit&agrave;', 'acs_panel_fo_presca_gestione.php?fn=get_open_form', {tipo_risorsa: 'RISEN', codice: cod_risorsa, descr: this.activeRecord.get('TADESC')}, 800, 400, null, 'icon-calendar-16');
					return false;
				}
			}]
		}";
	}
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-user_group-16',
            frame: true,
            title: 'Risorse',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'}, {name: 'k_TAKEY2', xtype: 'hidden'},
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden',
            }, {
				name: 'TAKEY2',
				xtype: 'combo',
				fieldLabel: 'Tipo risorsa',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,					     		
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(DeskAcqTipoRisorsa::get_ar_valori(), '') . " 	
					    ] 
				}						 
			}, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 10
            }, {
                fieldLabel: 'Denominazione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }],
            dockedItems: [" . self::form_buttons() . ", " . self::form_buttons_2() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Tipo risorsa',
                flex: 10,
                sortable: true,
                dataIndex: 'TAKEY2',
                allowBlank: false
            }, {
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'k_TAKEY2',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TAKEY2',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, 'TARIF1', 'TARIF2']
});
";

}	
	
	

// TABELLE UTILIZZZATE DALLA CLASSE
public function get_table($per_select = false){
	global $cfg_mod_DeskAcq;
	$ret = $cfg_mod_DeskAcq[$this->table_id];
	if ($per_select == "Y")
		$ret .= " PRI " . $this->get_table_joins();
	return $ret;
}

public function get_table_NR(){
	global $cfg_mod_DeskAcq;
	return $cfg_mod_DeskAcq['file_numeratori'];
}

public function get_table_TA(){
	global $cfg_mod_DeskAcq;
	return $cfg_mod_DeskAcq['file_tabelle'];
}




	
}

?>