<?php

class Profiles extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "PRAC";
	protected $tadt_f 		= "TADT";
	
	protected $table_k_user_module = "UPPRAC,UPCMOD";



public static function out_main_add_grid_selectionchange(){
	return "
		//aggiorno la grid dei moduli abilitati in base al cliente selezionato
		if (typeof(selected[0]) !== 'undefined')
        	sotto_main.child('#profileModuleGrid').getStore().getProxy().setExtraParam('UPPRAC', selected[0].data.TAKEY1);
        sotto_main.child('#profileModuleGrid').getStore().load();	
	";
}


public static function out_Writer_sotto_main($class_name, $flex=1) {
	echo "
						    var sotto_main = Ext.create('Ext.container.Container', {
						        padding: '0 0 0 0',
						        flex: 1,
						        layout: {
						            type: 'vbox',
						            align: 'stretch'
						        },
						        items: [{
						            itemId: 'form_{$class_name}',
						            flex: 1,            
						            xtype: " . j("writerform_{$class_name}") . ",
						            height: 150,
						            margins: '0 0 0 0',
						            listeners: {
						                create: function(form, data){
						                	//richiama l'insert del record
						                    store_{$class_name}.insert(0, data);
						                    store_{$class_name}.load();
						                }
						            }
						        }, {
						            itemId: 'profileModuleGrid',
						            xtype: 'profilemodulegrid',
						            title: 'Moduli abilitati',
						            flex: 1,
						            store: storeProfileModule
						        }]
						    });

	";
	
}




public static function out_Writer_Form_initComponent() {
 $m_profiles = new Profiles(array('no_verify' => 'Y'));
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-group-16',
            frame: true,
            title: 'Profilo',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'}, 
            {
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
				maxLength: 10, 	 		
                allowBlank: false
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
 				maxLength: 100,
                allowBlank: false
            }, {
							name: 'TARIF1',
							xtype: 'combo',
							fieldLabel: 'Apri modulo',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('MODUL', null, 1, null, null, null, $m_profiles), '') . " 
								    ] 
							}						 
			}],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}

public static function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }
	";
 return $ret;	
}




public static function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }, 'TARIF1']
});
";
}

	
	/******************************************************************************************
	 * MODLI ABILITATI PER UTENTE
	 *******************************************************************************************/
	
	public function moduli_utente_get_table(){
		global $cfg_mod_Admin;
		return $cfg_mod_Admin['file_moduli_utente'];
	}	
	
	public function profile_module_view_jd($p){		
		$ret_ar = array();
		$ret_ar["success"] = true;
		
		$utenti = $this->get_stmt_moduli_utente($p);
		while ($row = db2_fetch_assoc($utenti)) {
			//preparo i campi per le chiavi	
		    foreach(explode(',', $this->table_k_user_module) as $key)
		        $row["k_{$key}"] = $row[$key];				
			
				$row["UPCMOD_D"] = desc_TA_std('MODUL', $row["UPCMOD"]);
				$row["UPPRAC"] = trim($row["UPPRAC"]);
				
			$ret_ar["data"][] = array_map('trim',$row);
		}				
		echo acs_je($ret_ar);
	}	
	

	public function profile_module_update_jd($from_insert = false){			
		global $conn;		
		$ar_field = acs_m_params_json_decode()->data;		
		$ret_ar = array();
		
 		if ($from_insert=='Y'){ 			
			//copio il valore delle chiavi
			$ar_k_chiave = explode(',', $this->table_k_user_module);
			
	    	foreach($ar_k_chiave as $key) {
	    		$ar_field->{to_field_k($key)} = $ar_field->$key;
			}

		}		
		
		//eseguo aggiornamento
		$sql = "UPDATE " . $this->moduli_utente_get_table();
		$sql .= $this->get_SET_sql_jd($ar_field, $this->table_k_user_module, 'N');
		$sql .= " WHERE " . $this->get_WHERE_KEY_sql_jd($ar_field, $this->table_k_user_module);		

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
						
		$ret_ar["success"] = $result;				
		
		echo acs_je($ret_ar);
	}	


	public function profile_module_create_jd(){			
		global $conn;		
		$ar_field = acs_m_params_json_decode()->data;

			//eseguo inserimento
			$sql = "INSERT INTO " . $this->moduli_utente_get_table() . "(" . $this->get_INSERT_KEY_name($ar_field, $this->table_k_user_module, 'N') . ")";		
			$sql .= " VALUES (" . $this->get_INSERT_KEY_values($ar_field, $this->table_k_user_module, 'N') . ")";
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			
			//eseguo aggiornamento per aggiornare i dati passati
			$this->profile_module_update_jd('Y');						
	}


	public function profile_module_destroy_jd(){			
		global $conn;		
		$ar_field = acs_m_params_json_decode()->data;

			//eseguo inserimento
			$sql = "DELETE FROM " . $this->moduli_utente_get_table();		
			$sql .= " WHERE " . $this->get_WHERE_KEY_sql_jd($ar_field, $this->table_k_user_module);
			
			$stmt = db2_prepare($conn, $sql);		
			$result = db2_execute($stmt);
			
			//eseguo aggiornamento per aggiornare i dati passati
			$this->profile_module_update_jd('Y');						
	}

	
	private function get_stmt_moduli_utente($p){
		global $conn;
		$sql = "SELECT * FROM " . $this->moduli_utente_get_table();
		$sql .= " WHERE UPPRAC = " . sql_t($p['UPPRAC']);
		
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		return $stmt;				
	}	
	
		

	
}

?>