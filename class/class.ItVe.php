<?php

class ItVe extends AbstractDb {
	
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TATAID,TAKEY1,TAKEY2,TAKEY3,TAKEY4";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "ITVE";
	protected $tadt_f 		= "TADT";


	public function after_get_row($row){
		$s = new Spedizioni();
		//decodifico i vari valori
		$row['TAKEY1_D'] = $s->decod_std('ITIN', $row['TAKEY1']);
		$row['TAKEY2_D'] = $s->decod_std('AUTR', $row['TAKEY2']);
		$row['TAKEY3_D'] = $s->decod_std('AUTO', $row['TAKEY3']);
		$row['TAKEY4_D'] = $s->decod_std('COSC', $row['TAKEY4']);
		$row['TRASP_D'] = $this->get_trasportatore($row);
		return $row;
	}

	public function get_trasportatore($row){
		$trasp = new Trasportatori();
		$trasp->load_rec_data_by_vettore(trim($row['TAKEY2']));
		return $trasp->rec_data['TAKEY1'];
	}
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-user-16',
            frame: true,
            title: 'Itinerario / Vettore',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},       
            {name: 'k_TAKEY2', xtype: 'hidden'}, {name: 'k_TAKEY3', xtype: 'hidden'}, {name: 'k_TAKEY4', xtype: 'hidden'},
            {
				name: 'TAKEY1',
				xtype: 'combo',
				fieldLabel: 'Itinerario',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
 				forceSelection: true,
			   	allowBlank: false,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('ITIN', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
				name: 'TAKEY2',
				xtype: 'combo',
				fieldLabel: 'Vettore',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
 				forceSelection: true,					     		
			   	allowBlank: false,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}, {name:'TATISP'}, {name:'TATITR'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('AUTR', null, 1, null, null, null, $this), '') . " 	
					    ] 
				},
				listeners: {
					'select': function(combo, row, index) {
					     		//aggiorno TATISP e TATITR dal vettore
					     			var m_form = this.up('form').getForm();
					     			m_form.findField('TATISP').setValue(row[0].data.TATISP);
					     			m_form.findField('TATITR').setValue(row[0].data.TATITR);					     		
					     		
            					}
				}
					     		
					     		
					     		
			}, {
				name: 'TAKEY3',
				xtype: 'combo',
				fieldLabel: 'Mezzo',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,			
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}, {name:'TAVOLU'}, {name:'TABANC'}, {name:'TAPESO'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('AUTO', null, 1, null, null, null, $this), '') . " 	
					    ] 
				},
				listeners: {
					'select': function(combo, row, index) {
					     		//aggiorno VOLUME/PESO/PALLETT DA mezzo
					     			var m_form = this.up('form').getForm();
					     			m_form.findField('TAVOLU').setValue(row[0].data.TAVOLU);
					     			m_form.findField('TABANC').setValue(row[0].data.TABANC);					     		
					     			m_form.findField('TAPESO').setValue(row[0].data.TAPESO);									     		
            					}
				}
				
			}, {
				name: 'TAKEY4',
				xtype: 'combo',
				fieldLabel: 'Cassa',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,					     		
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('COSC', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
                fieldLabel: 'Sequenza impiego',
                name: 'TASTAL',
                allowBlank: true,
				maxLength: 1                
            }, {
                fieldLabel: 'Volume limite',
                name: 'TAVOLU',
                allowBlank: false
            }, {
                fieldLabel: 'Peso limite',
                name: 'TAPESO',
                allowBlank: false
            }, {
                fieldLabel: 'Pallet limite',
                name: 'TABANC',
                allowBlank: false
            }, {
				name: 'TATISP',
				xtype: 'combo',
				fieldLabel: 'Tipologia spedizione',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('TSPE', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}, {
				name: 'TATITR',
				xtype: 'combo',
				fieldLabel: 'Tipologia trasporto',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: true,														
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_std('TTRA', null, 1, null, null, null, $this), '') . " 	
					    ] 
				}						 
			}],
        dockedItems: [" . self::form_buttons(self::form_buttons_pre()) . "]
        });
        this.callParent();
";	
 return $ret;
}

public function form_buttons_pre() {
	global $s;
	return "
			{
                    itemId: 'applic_su_rplt',
                    text: 'Report',
					iconCls: 'iconPrint',
                    disabled: false,
                    scope: this,
                    handler: function(){

				        //variabile creazione FORM						
						
						tip_sped = 'null';
						tip_trasp = 'null';
						var_itin = 'null';
						var_trasp = 'null';
						
						if (this.activeRecord)
						{
							tip_sped = this.activeRecord.get('TATISP');
			
							tip_trasp = this.activeRecord.get('TATITR');
						
							var_itin = this.activeRecord.get('TAKEY1');
			
							var_trasp = this.activeRecord.get('TRASP_D');
						}

						var f = Ext.create('Ext.form.Panel', {
				        frame:true,
				        title: '',
				        bodyStyle:'padding:5px 5px 0',
				        width: 600,
				        url: 'acs_report_itinerario_vettore.php',
				        fieldDefaults: {
				            labelAlign: 'top',
				            msgTarget: 'side'
				        },

				        items: [{
                            name: 'f_spedizione',
                            xtype: 'combo',
                            fieldLabel: 'Tipologia spedizione',
                            displayField: 'text',
                            valueField: 'id',
							forceSelection: true,
                            emptyText: '- seleziona -',
                            allowBlank: true,
                            anchor: '-15',
							value: tip_sped,
                            store: {
                                autoLoad: true,
                                editable: false,
                                autoDestroy: true,
                                fields: [{name:'id'}, {name:'text'}],
                                data: [
                                     " . acs_ar_to_select_json($s->find_TA_std('TSPE'),"", true, 'Y', 'Y') . "
                                     ]
                            }
                          },{
                            name: 'f_trasporto',
                            xtype: 'combo',
                            fieldLabel: 'Tipologia trasporto',
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            emptyText: '- seleziona -',
                            allowBlank: true,
                            anchor: '-15',
                            value: tip_trasp,
                            store: {
                                autoLoad: true,
                                editable: false,
                                autoDestroy: true,
                                fields: [{name:'id'}, {name:'text'}],
                                data: [
                                     " . acs_ar_to_select_json($s->find_TA_std('TTRA'),"", true, 'Y', 'Y') . "
                                     ]
                            }
                          },{
                            name: 'f_itinerario',
                            xtype: 'combo',
                            fieldLabel: 'Itinerario',
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            emptyText: '- seleziona -',
                            allowBlank: true,
                            anchor: '-15',
                            value: var_itin,
                            store: {
                                autoLoad: true,
                                editable: false,
                                autoDestroy: true,
                                fields: [{name:'id'}, {name:'text'}],
                                data: [
                                     " . acs_ar_to_select_json($s->find_TA_std('ITIN'),"", true, 'Y') . "
                                     ]
                            }
                          },{
                            name: 'f_trasportatore',
                            xtype: 'combo',
                            fieldLabel: 'Trasportatore',
                            displayField: 'text',
                            valueField: 'id',
                            forceSelection: true,
                            emptyText: '- seleziona -',
                            allowBlank: true,
                            anchor: '-15',
                            value: var_trasp,
                            store: {
                                autoLoad: true,
                                editable: false,
                                autoDestroy: true,
                                fields: [{name:'id'}, {name:'text'}],
                                data: [
                                     " . acs_ar_to_select_json($s->find_TA_std('AVET'),"", true, 'Y', 'Y') . "
                                     ]
                            }
                          },{
                            xtype: 'checkboxgroup',
                            anchor: '100%',
                            fieldLabel: 'Ripetizione Itinerario-Trasportatore',
                            items: [{
                                        xtype: 'checkbox',
                                     	name: 'visualizza_itin_trasp',
                                     	boxLabel: 'Si',
                                      	inputValue: 'Y'
                                    }]
                                }
                                     		
				            ],

				       buttons: [{
		                text: 'Visualizza',
		                iconCls: 'icon-module-32',
		                scale: 'large',
		                handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
		                        form.submit({
		                                standardSubmit: true,
                                     	submitEmptyText: false,
		                                method: 'POST',
		                                target : '_blank'
		                        });
		                    }
		                }
		            }]

				    });


						// variabile creazione WINDOW

						var win = new Ext.Window({
				          width: 800
				        , height: 380
				        , minWidth: 300
				        , minHeight: 300
				        , plain: true
				        , title: 'Report listini trasporto/deposito'
				        , layout: 'fit'
				        , border: true
				        , closable: true
				        , items: f
				        });

				        win.show();


                    }
 			},
 ";
}

	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Itinerario',
                sortable: true,
                dataIndex: 'TAKEY1_D',
				filter: {type: 'string'}, filterable: true
            }, {
                header: 'Vettore',
                sortable: true,
                dataIndex: 'TAKEY2_D',
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Mezzo',
                sortable: true,
                dataIndex: 'TAKEY3_D'
            }, {
                header: 'Cassa',
                sortable: true,
                dataIndex: 'TAKEY4_D',
				flex: 1
            }, {
                header: 'Sq',
                sortable: true,
                dataIndex: 'TASTAL',
				width: 40
            }, {
                header: 'TS',
                sortable: true,
                dataIndex: 'TATISP',
				width: 50
            }, {
                header: 'TT',
                sortable: true,
                dataIndex: 'TATITR',
				width: 50
            }, {
                header: 'Vol',
                sortable: true,
                dataIndex: 'TAVOLU'
            }, {
                header: 'Peso',
                sortable: true,
                dataIndex: 'TAPESO'
            }, {
                header: 'Pallet',
                sortable: true,
                dataIndex: 'TABANC'
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'k_TAKEY2',        
        useNull: false
    }, {
        name: 'k_TAKEY3',        
        useNull: false
    }, {
        name: 'k_TAKEY4',        
        useNull: false
    }, 'TATAID', 'TATISP', 'TATITR', 'TAKEY1', 'TAKEY2', 'TAKEY3', 'TAKEY4', 'TASTAL', 'TAVOLU', 'TAPESO', 'TABANC', 'TAKEY1_D', 'TAKEY2_D', 'TAKEY3_D', 'TAKEY4_D', 'TRASP_D']
});

";

}	
	
	

	
	
	
	
	
}

?>