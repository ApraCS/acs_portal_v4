<?php

class SpedParametriOperatoriOE extends AbstractDb {
 
 	protected $table_id 	= "file_parametri_operatori_OE"; 
	protected $table_k 		= "OEDT,OEFLPS,OETPCA,OEUSER,OEDTRG,OETOR1,OETOR2,OETOR3,OETOR4,OETOR5,OETOR6";
	protected $tadt_f 		= "OEDT";	
	
	protected $default_fields = array(
			'OEDTRG' 	=> 0,			
			'OECOEU' 	=> 0,
			'OENRDO' 	=> 0,
			'OECOEM' 	=> 0,
			'OENDOC' 	=> 0,
			'OECOEC' 	=> 0,
			'OENDOA' 	=> 0,
			'OECOEA' 	=> 0
		);
	
	

public function get_table($per_select = false){
	global $cfg_mod_Spedizioni;
	$ret = $cfg_mod_Spedizioni[$this->table_id];
	if ($per_select == "Y")
		$ret .= " PRI " . $this->get_table_joins();
	return $ret;
}	


public function before_create_jd($ar_field){
	$ar_field->OECOEU = sql_f($ar_field->OECOEU); 
	$ar_field->OECOEM = sql_f($ar_field->OECOEM);
	$ar_field->OECOEC = sql_f($ar_field->OECOEC);
	return $ar_field;
}


public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-design-16',
            frame: true,
            title: 'Parametri operatori OE',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            //chiave --
            	{name: 'k_OEDT', xtype: 'hidden'},{name: 'k_OEFLPS', xtype: 'hidden'}, {name: 'k_OETPCA', xtype: 'hidden'},
				{name: 'k_OEUSER', xtype: 'hidden'},{name: 'k_OEDTRG', xtype: 'hidden'},
 		 		{name: 'k_OETOR1', xtype: 'hidden'},{name: 'k_OETOR2', xtype: 'hidden'},{name: 'k_OETOR3', xtype: 'hidden'},
 				{name: 'k_OETOR4', xtype: 'hidden'},{name: 'k_OETOR5', xtype: 'hidden'},{name: 'k_OETOR6', xtype: 'hidden'},
 			//-- chiave
			{
                name: 'OEDT',
                xtype: 'hidden',
            }, {
							name: 'OEFLPS',
							xtype: 'combo',
 							allowBlank: false,
							fieldLabel: 'Preventivo/Storico',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: 'P', text: 'Preventivo'}, 	
 									 {id: 'S', text: 'Storico'},
								    ] 
							}						 
			}, {
							name: 'OETPCA',
							xtype: 'combo',
							allowBlank: false, 		
							fieldLabel: 'Tipo',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('DECAL', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
				name: 'OEUSER',
				xtype: 'combo',
				fieldLabel: 'Operatore',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     " . acs_ar_to_select_json(find_TA_users(), '') . " 	
					    ] 
				}						 
			}, {
                fieldLabel: 'Data (ggmmyyyy)<br/>0 = *def',
                name: 'OEDTRG',
                allowBlank: false,
 				maxLength: 8
            }, {
							name: 'OETOR1',
							xtype: 'combo',
							fieldLabel: 'Tipologia 1',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
							name: 'OETOR2',
							xtype: 'combo',
							fieldLabel: 'Tipologia 2',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
							name: 'OETOR3',
							xtype: 'combo',
							fieldLabel: 'Tipologia 3',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
							name: 'OETOR4',
							xtype: 'combo',
							fieldLabel: 'Tipologia 4',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
							name: 'OETOR5',
							xtype: 'combo',
							fieldLabel: 'Tipologia 5',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
							name: 'OETOR6',
							xtype: 'combo',
							fieldLabel: 'Tipologia 6',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     " . acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $this), '') . " 	
								    ] 
							}						 
			}, {
                fieldLabel: 'Coefficiente unitario',
				xtype: 'numberfield',
                name: 'OECOEU',
                allowBlank: true
            }, {
                fieldLabel: 'Numero documenti',
				xtype: 'numberfield',
                name: 'OENRDO',
                allowBlank: true
            }, {
                fieldLabel: 'Coefficiente massimo',
				xtype: 'numberfield',
                name: 'OECOEM',
                allowBlank: true
            }],
			dockedItems: [" . self::form_buttons() . ", " . self::form_buttons_2() . "]            		
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'P/S',
                flex: 20,
                sortable: true,
                dataIndex: 'OEFLPS',
                allowBlank: false,
				tooltip: 'Preventivo / Storico',
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Tipo', tooltip: 'Tipo calendario',
                width: 60,
                sortable: true,
                dataIndex: 'OETPCA',
                allowBlank: false,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Operatore',
                flex: 80,
                sortable: true,
                dataIndex: 'OEUSER',
                allowBlank: true,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Data',
                flex: 80,
                sortable: true,
                dataIndex: 'OEDTRG',
                allowBlank: true,
				filter: {type: 'string'}, filterable: true
            }, {
                text: 'Tip.', tooltip: 'Tipologie',
                width: 70,
                sortable: true,
                dataIndex: 'OETOR1', //ToDo: concatenare tutte
                allowBlank: true,
				renderer: function (value, metaData, record, row, col, store, gridView){
						ret = '';
						if (!Ext.isEmpty(record.get('OETOR1'))) ret += record.get('OETOR1');
						if (!Ext.isEmpty(record.get('OETOR2'))) ret += record.get('OETOR2');
						if (!Ext.isEmpty(record.get('OETOR3'))) ret += record.get('OETOR3');
						if (!Ext.isEmpty(record.get('OETOR4'))) ret += record.get('OETOR4');
						if (!Ext.isEmpty(record.get('OETOR5'))) ret += record.get('OETOR5');
						if (!Ext.isEmpty(record.get('OETOR6'))) ret += record.get('OETOR6');			
					   return ret;			    
				}			
            }, {
                text: 'Coeff. Unit',
                width: 80,
                sortable: true,
                dataIndex: 'OECOEU',
                allowBlank: true,
				align: 'right'
            }, {
                text: 'Num.Doc.',
                width: 80,
                sortable: true,
                dataIndex: 'OENRDO',
                allowBlank: true,
				align: 'right'
            }, {
                text: 'Coef.Max',
                width: 80,
                sortable: true,
                dataIndex: 'OECOEM',
                allowBlank: true,
				align: 'right'
            }

			, {
                text: 'Coeff. Corrente',
                width: 80,
                sortable: true,
                dataIndex: 'OECOEC',
                allowBlank: true,
				align: 'right'
            }, {
                text: 'Num.Doc.',
                width: 80,
                sortable: true,
                dataIndex: 'OENDOA',
                allowBlank: true,
				align: 'right'
            }, {
                text: 'Coef.Assegn.',
                width: 80,
                sortable: true,
                dataIndex: 'OECOEA',
                allowBlank: true,
				align: 'right'
            }			
			
	";
 return $ret;	
}	
	
	




public function form_buttons_2() {	
	return "
	{
		xtype: 'toolbar',
		dock: 'bottom',
		ui: 'footer',
		items: ['->', {
				iconCls: 'icon-design-16',
				text: 'Manutenzione per data',
				disabled: false,
				scope: this,
				handler: function(){
					loc_win = this.up('window');
					acs_show_win_std('Parametri operatori OE - Manutenzione per calendario', 'acs_manuenzione_parametri_operatori_OE.php?fn=open_params', {main_panel_id: loc_win.id}, 450, 350, null, 'icon-calendar-16');
				}
			}
		]
	}				
	";
}	



	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }
     , {name: 'k_OEDT',   useNull: false} 
     , {name: 'k_OEFLPS', useNull: false}
     , {name: 'k_OETPCA', useNull: false}
     , {name: 'k_OEUSER', useNull: false}
     , {name: 'k_OEDTRG', useNull: false}
     , {name: 'k_OETOR1', useNull: false}
     , {name: 'k_OETOR2', useNull: false}
     , {name: 'k_OETOR3', useNull: false}
     , {name: 'k_OETOR4', useNull: false}
     , {name: 'k_OETOR5', useNull: false}
     , {name: 'k_OETOR6', useNull: false}
	 , {name: 'OEDT',   useNull: false} 
     , {name: 'OEFLPS', useNull: false}
     , {name: 'OETPCA', useNull: false}
     , {name: 'OEUSER', useNull: false}
     , {name: 'OEDTRG', useNull: false}
     , {name: 'OETOR1', useNull: false}
     , {name: 'OETOR2', useNull: false}
     , {name: 'OETOR3', useNull: false}
     , {name: 'OETOR4', useNull: false}
     , {name: 'OETOR5', useNull: false}
     , {name: 'OETOR6', useNull: false}
     , {name: 'OECOEU', useNull: false}
     , {name: 'OENRDO', useNull: false}
     , {name: 'OECOEM', useNull: false}
     , {name: 'OENDOC', useNull: false}
     , {name: 'OECOEC', useNull: false}
     , {name: 'OENDOA', useNull: false}
     , {name: 'OECOEA', useNull: false}
   ]
});
";

}	
  
  
}

?>