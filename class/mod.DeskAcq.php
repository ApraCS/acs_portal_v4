<?php

/**********************************************
 * DESKTOP ORDINI DI ACQUISTO
 **********************************************/

class DeskAcq {
	
	private $mod_cod = "DESK_ACQ";
	private $mod_dir = "desk_acq";
	

	public function get_cod_mod(){
		return $this->mod_cod;
	}	
	
	public function get_cfg_mod(){
		global $cfg_mod_DeskAcq;
		return $cfg_mod_DeskAcq;
	}	
	

	public function get_mod_parameters(){
		$t = new Modules();
		$t->load_rec_data_by_k(array('TAKEY1' => $this->get_cod_mod()));
		return json_decode($t->rec_data['TAMAIL']);
	}	
	
	
	
	/* *********************************************************************************************/
	/* ELENCO ORDINI */
	/* *********************************************************************************************/
	public function get_elenco_ordini($filtro, $ordina_per_tipo_ord = 'N', $tipo = ''){
		global $conn;
		global $cfg_mod_DeskAcq;
	
		$where = "";
		if (isset($filtro["area"]))
			$where .= " AND TDASPE='{$filtro["area"]}'";
	
		if (isset($filtro["itinerario"]) && strlen($filtro["itinerario"]) > 0)
			$where .= " AND TDCITI='{$filtro["itinerario"]}'";
	
		if (isset($filtro["vettore"]))
			$where .= " AND CSCVET='{$filtro["vettore"]}'";
		if (isset($filtro["data_spedizione"]))
		{
		if (isset($filtro["a_data_spedizione"]))
				$where .= " AND TDDTEP>={$filtro["data_spedizione"]} AND TDDTEP<={$filtro["a_data_spedizione"]} ";
				else
		$where .= " AND TDDTEP={$filtro["data_spedizione"]} ";
		}
	
		if (isset($filtro["cliente"]) && strlen($filtro["cliente"]) > 0){		    
		    if ($cfg_mod_DeskAcq['disabilita_sprintf_codice_cliente'] == 'Y')
		        $where .= " AND TDCCON=" . sql_t($filtro["cliente"]);
	        else
	            $where .= " AND TDCCON=" . sql_t(sprintf("%09s", $filtro["cliente"]));
		}
	
		if (isset($filtro["destinazione"]) && strlen($filtro["destinazione"]) > 0)
			$where .= " AND TDCDES =" . sql_t($filtro["destinazione"]);
			
		if (isset($filtro["TDSWSP"]))
			$where .= " AND TDSWSP='{$filtro["TDSWSP"]}'";
	
		if (isset($filtro["riferimento"]) && strlen($filtro["riferimento"]) > 0)
			$where .= " AND UPPER(TDVSRF) LIKE '%" . strtoupper($filtro["riferimento"]) . "%'";
	
		//data programmata
		if (isset($filtro["data"]) && strlen($filtro["data"]) > 0)
		$where .= " AND TDDTEP={$filtro["data"]}";
	
		//data programmata (dal - al)
		if (isset($filtro["data_dal"]) && strlen($filtro["data_dal"]) > 0)
		$where .= " AND TDDTEP >= {$filtro["data_dal"]}";
		if (isset($filtro["data_al"]) && strlen($filtro["data_al"]) > 0)
		$where .= " AND TDDTEP <= {$filtro["data_al"]}";
	
		//data spedizione (dal - al)
		if (isset($filtro["data_sped_dal"]) && strlen($filtro["data_sped_dal"]) > 0)
		$where .= " AND TDDTSP >= {$filtro["data_sped_dal"]}";
		if (isset($filtro["data_sped_al"]) && strlen($filtro["data_sped_al"]) > 0)
			$where .= " AND TDDTSP <= {$filtro["data_sped_al"]}";
	
		//data rilascio (dal - al)
		if (isset($filtro["data_ril_dal"]) && strlen($filtro["data_ril_dal"]) > 0)
			$where .= " AND TDDTRP >= {$filtro["data_ril_dal"]}";
				if (isset($filtro["data_ril_al"]) && strlen($filtro["data_ril_al"]) > 0)
			$where .= " AND TDDTRP <= {$filtro["data_ril_al"]}";

				
		if (isset($filtro["data_ricezione"]) && strlen($filtro["data_ricezione"]) > 0)
			$where .= " AND TDODRE={$filtro["data_ricezione"]}";
		
		//data ricezione (dal - al)
		if (isset($filtro["data_ricezione_dal"]) && strlen($filtro["data_ricezione_dal"]) > 0)
			$where .= " AND TDODRE >= {$filtro["data_ricezione_dal"]}";
		if (isset($filtro["data_ricezione_al"]) && strlen($filtro["data_ricezione_al"]) > 0)
			$where .= " AND TDODRE <= {$filtro["data_ricezione_al"]}";		
	
				if (isset($filtro["data_conferma"]) && strlen($filtro["data_conferma"]) > 0)
			$where .= " AND TDDTCF={$filtro["data_conferma"]}";
	
	
				if (isset($filtro["num_ordine"])  && strlen($filtro["num_ordine"]) > 0 )
			$where .= " AND TDONDO='{$filtro["num_ordine"]}'";
	
			if (isset($filtro["num_carico"])  && strlen($filtro["num_carico"]) > 0 )
				$where .= " AND " .  $this->add_where_num_carico($filtro["num_carico"]);
			
			if (isset($filtro["num_lotto"])  && strlen($filtro["num_lotto"]) > 0 )
				$where .= " AND " .  $this->add_where_num_lotto($filtro["num_lotto"]);
	
			if ($filtro['carico_assegnato'] == "Y")
			$where .= " AND TDNRCA > 0 ";
				if ($filtro['carico_assegnato'] == "N")
				$where .= " AND TDNRCA = 0 ";
				if ($filtro['lotto_assegnato'] == "Y")
				$where .= " AND TDNRLO > 0 ";
				if ($filtro['anomalie_evasione'] == "Y")
					$where .= " AND TDOPUN = 'N' ";
					if ($filtro['lotto_assegnato'] == "N")
					$where .= " AND TDNRLO = 0 ";
	
	
					if ($filtro['collo_disp'] == "Y")
					$where .= " AND TDCOPR > 0 ";
		if ($filtro['collo_disp'] == "N")
		$where .= " AND TDCOPR = 0 ";
	
		if ($filtro['collo_sped'] == "Y")
		$where .= " AND TDCOSP > 0 ";
				if ($filtro['collo_sped'] == "N")
				$where .= " AND TDCOSP = 0 ";
	
		
				if ($filtro['ordini_evasi'] == "Y"){
				$where .= " AND TDSWPP = 'Y' AND TDSWSP = 'E' ";
	}
	if ($filtro['ordini_evasi'] == "N"){
	$where .= " AND TDSWPP = 'Y' AND TDSWSP <> 'E' AND TDFN11 = 1 ";
		}
	
	if (isset($filtro["divisione"]) && strlen($filtro["divisione"]) > 0)
		$where .= " AND TDCDIV='{$filtro["divisione"]}'";
	
	if (isset($filtro["tipo_ordine"]) && strlen($filtro["tipo_ordine"]) > 0)
		$where .= " AND TDOTPD='{$filtro["tipo_ordine"]}'";
	
		//gestione multiselect (valori separati da ,) o array
		if (isset($filtro["stato_ordine"]) && is_array($filtro["stato_ordine"])){
		    $ar_stato_ordine = $filtro["stato_ordine"];
		    if (count($ar_stato_ordine) == 1)
		        $where .= " AND TDSTAT='{$filtro["stato_ordine"][0]}'";
		        else if (count($ar_stato_ordine) > 1)
		            $where .= " AND TDSTAT IN (" . sql_t_IN($ar_stato_ordine) . ") ";
		}
		else if (isset($filtro["stato_ordine"]) && strlen(trim($filtro["stato_ordine"])) > 0){
		    $ar_stato_ordine = explode(",", $filtro["stato_ordine"]);
		    if (count($ar_stato_ordine) == 1)
		        $where .= " AND TDSTAT='{$filtro["stato_ordine"]}'";
		        else
		            $where .= " AND TDSTAT IN (" . sql_t_IN($ar_stato_ordine) . ") ";
		}
		
	
	if (isset($filtro["priorita"]) && strlen($filtro["priorita"]) > 0)
		$where .= " AND TDOPRI='{$filtro["priorita"]}'";
	
		if ($filtro["solo_bloccati"] == 1)
					$where .= " AND (TDBLEV='Y' OR TDBLOC = 'Y') ";
	
	
					if ($ordina_per_tipo_ord == 'Y')
					$m_ord_f = "TDCLOR, TDOTPD, TDDTEP";
		else {
		 $m_ord_f = "TDDTEP, TDNBOC";
	}
	
	
	
	$sql = "SELECT *
			FROM {$cfg_mod_DeskAcq['file_testate']}
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
			ON TDDT = CSDT AND TDNBOC = CSPROG AND CSCALE = '*SPR'
		      WHERE (" . $this->get_where_std($tipo) . "  $where)
			ORDER BY {$m_ord_f}, TDAACA, TDNRCA, TDSECA, TDDCON, TDCCON, TDODRE, TDONDO
			   ";

	   $stmt = db2_prepare($conn, $sql);
	   $result = db2_execute($stmt);
	   return $stmt;
	}
	
		
	

	public function exe_upd_commento_ordine($p){
	    global $cfg_mod_DeskAcq, $auth, $conn, $id_ditta_default, $is_linux;
	
		$oe = $this->k_ordine_td_decode($p['k_ordine']);
		$bl = $cfg_mod_DeskAcq['bl_commenti_ordine'];
				
		foreach($p as $k => $v){
		    if (substr($k, 0, 7) == 'f_text_'){
		        $ar_value = explode('_', $k);
		        $riga = $ar_value[2];
		        $rrn = $ar_value[3];
		        
		        $name = "f_h_text_{$riga}";
		        $old_value = trim($p["{$name}"]);
		        
		        if ($is_linux == 'Y')
		            $old_value = strtr($old_value, array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	            if ($is_linux == 'Y')
	                $v = strtr($v, array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	            
		        $ar_ins = array();
		      
                $ar_ins['RDDES1'] 	= $v;
                $ar_ins['RDDTEP'] 	= oggi_AS_date();     //data ultima modifica
                $ar_ins['RDRIFE'] 	= $auth->get_user();  //utente ultima modifica
		            
        if($rrn != ""){
            
            if($old_value != trim($v)){
                $sql = "UPDATE {$cfg_mod_DeskAcq['file_commenti_ordine']} RD
                SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE RRN(RD) = '{$rrn}'";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
            }
		                
        }else{
		                
            if(trim($v) != ""){
                
                $ar_ins['RDDTGE'] 	= oggi_AS_date();
                $ar_ins['RDORGE'] 	= oggi_AS_time();
                $ar_ins['RDUSGE'] 	= $auth->get_user();
                $ar_ins['RDDT'] 	= $id_ditta_default;
                $ar_ins['RDOTID'] 	= trim($oe['TDOTID']);
                $ar_ins['RDOINU'] 	= trim($oe['TDOINU']);
                $ar_ins['RDOADO'] 	= trim($oe['TDOADO']);
                $ar_ins['RDONDO'] 	= trim($oe['TDONDO']);
                $ar_ins['RDTPNO'] 	= $bl;
                $ar_ins['RDRINO'] 	= $riga;
                
                $sql = "INSERT INTO {$cfg_mod_DeskAcq['file_commenti_ordine']}(" . create_name_field_by_ar($ar_ins) . ")
            VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg();
                
                
            }
            }
		            
		    }
		    
		}
		
		
	
		$sh = new SpedHistory();
		$sh->crea(
				'upd_commento_ordine',
				array(
						"k_ordine" 	=> $p['k_ordine']
				)
				);
	
	}
	
	function next_num($cod){
	    global $conn, $id_ditta_default;
	    global $cfg_mod_DeskAcq;
	    
	    
	    $sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_numeratori']} WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    
	    if ($row = db2_fetch_assoc($stmt)){
	        $n = $row['NRPROG'] +1;
	        $sql = "UPDATE {$cfg_mod_DeskAcq['file_numeratori']} SET NRPROG=$n WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	    } else {
	        $n = 1;
	        $sql = "INSERT INTO {$cfg_mod_DeskAcq['file_numeratori']}(NRDT, NRCOD, NRPROG)  VALUES(" . sql_t($id_ditta_default) . ", '$cod', $n)";
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	    }
	    
	    return $n;
	}
	
	public function has_commento_riga($riga, $bl = null){
	    global $conn;
	    global $cfg_mod_DeskAcq, $id_ditta_default;
	    
	    global $backend_ERP;
	    if ($backend_ERP == 'GL') return false;
	  
	    if (is_null($riga) || strlen(trim($riga)) == 0)
	        return false;
	        
	        if(isset($bl) && $bl != '')
	            $where = " AND RLRIFE2 = '{$bl}'";
            else
                $where = "";
            
          
	                
	                $sql = "SELECT count(*) AS NR
	                FROM {$cfg_mod_DeskAcq['file_note_anag']}
	                WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$riga}'
	                AND RLTPNO = 'RD' {$where}";
	                
	                $stmt = db2_prepare($conn, $sql);
	                echo db2_stmt_errormsg();
	                $result = db2_execute($stmt);
	                echo db2_stmt_errormsg($stmt);
	                $row = db2_fetch_assoc($stmt);
	                
	                if ($row['NR'] > 0) return TRUE;
	                else return FALSE;
	}
	
	public function get_commento_riga($riga, $bl){
	    global $conn;
	    global $cfg_mod_DeskAcq, $id_ditta_default;
	    
	    if (is_null($riga) || strlen(trim($riga)) == 0)
	        return array();
	   
	        $sql = "SELECT *
	        FROM {$cfg_mod_DeskAcq['file_note_anag']}
	        WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$riga}'
	        AND RLTPNO = 'RD' AND RLRIFE2 = '{$bl}'
	        ORDER BY RLRIGA";
	  
	        
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	        
	        $ret = array();
	        while ($row = db2_fetch_assoc($stmt)) {
	            $ret[] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
	        }
	        
	        return $ret;
	}
	
	public function crea_array_valori_el_ordini($its, $tipo_elenco = "", $ordina_per_tipo_ord = "N"){
		$ar = array();
		$ar_tot = array( "TDNBOC" => array() );
		$liv1_c = $liv2_c = $liv3_c = 0;
		$liv1_v = $liv2_v = $liv3_v = "--------------";
	
		while ($row = db2_fetch_assoc($its)) {
				
				
			if ($tipo_elenco == 'HOLD'){
				$row['hold_TDDTEP'] = $row['TDDTEP'];
				$row['TDDTEP'] = 0;
			}
	
			if ($ordina_per_tipo_ord == 'Y'){
				$m_stacco_liv0_c = implode("___", array($row['TDCLOR'], $row['TDOTPD']));
				$m_stacco_liv0_d = $row['TDDOTD'];
			}
			else {
				//non statto piu' per spedizione... perch� e' gi� a livello 0
				//$m_stacco_liv0_c = $row['TDNBOC'];
				//$m_stacco_liv0_d = $row['TDNBOC'];
				$m_stacco_liv0_c = 'riga_TOT';
				$m_stacco_liv0_d = 'Totali';								
			}
				
				
			if ($row['TDDTEP'] . "___" . $m_stacco_liv0_c . "___" . $row['TDNBOC'] != $liv1_v){
				$liv1_c += 1;
				$liv2_c = $liv3_c = 0;
				$liv2_v = $liv3_v = "-----------";
				$liv1_v = $row['TDDTEP'] . "___" . $m_stacco_liv0_c . "___" . $row['TDNBOC'];
	
				$d_carico = strtotime($row['TDDTEP']);
	
				if ((int)$row['TDNBOC'] == 0 && $tipo_elenco == "HOLD")
					$liv1_vt = "Ricezione non associata";
				else
					$liv1_vt = "Ricezione #" . $row['TDNBOC'];
	
				if (is_null($ar[$liv1_v]))
					$ar[$liv1_v] = array("cod"=>$liv1_v, "txt"=> $liv1_vt, "PSSTAT"=>$row['PSSTAT'], "dta"=>array(), "val"=>array());
				//$ar[$liv1_v]["riferimento"] = $row['TDDVET'] . $this->decod_std('AUTO', $row['TDAUTO']) . " " . $row['TDDCOS'];
				$ar[$liv1_v]["id_spedizione"] = $row['TDNBOC'];
				$ar[$liv1_v]["n_carico"] 	  = $row['TDNRCA'];
				$ar[$liv1_v]["k_carico"]	  = $this->k_carico_td($row);
	
				if ($ordina_per_tipo_ord != 'Y')
					$ar[$liv1_v]["sped_id"]	  	  = $row['TDNBOC'];
	
				$ar[$liv1_v]["stacco_liv0_c"]  = $m_stacco_liv0_c;
				$ar[$liv1_v]["stacco_liv0_d"]  = $m_stacco_liv0_d;
	
				//prendere la descrizione dalla tabella dei carichi
				$m_carico = $this->get_carico_td($row);
				$ar[$liv1_v]["riferimento"]   = $m_carico['PSDESC'];
			}
				
			//TODO: anche per ditta?
			if (($row['TDSECA'] . "___" . $row['TDCCON'] . "___" . $row['TDCDES'])  != $liv2_v){
				$liv2_c += 1;
				$liv3_c = 0; $liv3_v = "-----------";
				$liv2_v = $row['TDSECA'] . "___" . $row['TDCCON'] . "___" . $row['TDCDES'];
				if (is_null($ar[$liv1_v]["val"][$liv2_v]))
					$ar[$liv1_v]["val"][$liv2_v] = array("cod"=>$liv2_v, "txt"=> trim($row['TDDCON']), "dta"=>array(), "val"=>array());
	
				$ar[$liv1_v]["val"][$liv2_v]['k_cli_des'] = implode("_", array($row['TDDT'], $row['TDCCON'], $row['TDCDES']));
	
				$ar[$liv1_v]["val"][$liv2_v]["riferimento"]	= $this->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD']);
	
				//se scarico intermedio
				if ($row['TDTDES'] == '1' || $row['TDTDES'] == '2'){
					$ar[$liv1_v]["val"][$liv2_v]["scarico_intermedio"] = 1;
					$ar[$liv1_v]["val"][$liv2_v]["riferimento"] = '<img class="cell-img" src=' . img_path("icone/16x16/exchange.png") . '>&nbsp;&nbsp;' . $ar[$liv1_v]["val"][$liv2_v]["riferimento"];
				}
	
				//icona ordine di reso
				if ($this->ha_con_ordini_reso($row['TDDT'], $row['TDCCON'], $row['TDCDES']))
					$ar[$liv1_v]["val"][$liv2_v]["tipo"] = '<img class="cell-img" src=' . img_path("icone/16x16/recycle_bin.png") . '>';
	
	
				$ar[$liv1_v]["val"][$liv2_v]["seq_carico"]	= $row['TDSECA'];
				$ar[$liv1_v]["val"][$liv2_v]["cliente"]		= $row['TDDCON'];
				$ar[$liv1_v]["val"][$liv2_v]["gmap_ind"]	= implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));
	
				//cliente bloccato
				if ($row['TDCLBL'] == 'Y' || $row['TDCLBL'] == 'E' || $row['TDCLBL'] == 'P')
				{
					$ar[$liv1_v]["val"][$liv2_v]["fl_cli_bloc"] 	= 1;
				}
					
	
					
			}
			if ($row['TDDOCU'] != $liv3_v){
				$liv3_c += 1;
				$liv3_v = "{$row['TDDOCU']}";
				$liv3_txt = trim($row['TDOADO']) . "_" . trim($row['TDONDO']) . " " . trim($row['TDPROG']) . " " . trim($row['TDMODI']);
				if (trim($row['TDABBI']) > 0)
					$liv3_txt .= '&nbsp;&nbsp;&nbsp;<img class="cell-img" src=' . img_path("icone/16x16/link.png") . '>';
	
					
				$ha_commenti = $this->has_commento_ordine_by_k_ordine($this->k_ordine_td($row));
				if ($ha_commenti == FALSE)
					$img_com_name = "icone/16x16/comment_light.png";
				else
					$img_com_name = "icone/16x16/comment_edit_yellow.png";
	
	
				//icona commento ordine
				$liv3_txt .= '<span style="display: inline; float: right;"><a href="javascript:acs_show_win_std(\'Annotazioni ordine\', \'acs_form_json_annotazioni_ordine.php?k_ordine=' . $row['TDDOCU'] . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
				//$liv3_txt .= '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine(this)";><img class="cell-img" src=' . img_path("icone/16x16/comments_grey.png") . '></a></span>';
					
				//icona entry (per attivita' in corso sull'ordine)
				$sa = new SpedAssegnazioneOrdini();
				if ($sa->ha_entry_aperte_per_ordine($row['TDDOCU'])){
					$img_entry_name = "icone/16x16/arrivi_gray.png";
					$liv3_txt .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><a href="javascript:show_win_entry_ordine(\'' . $row['TDDOCU'] . '\', \'' . $liv1_v . '\', \'' . $liv2_v . '\')";><img class="cell-img" src=' . img_path($img_entry_name) . '></a> </span>';
				}
	
				if (is_null($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]))
					$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v] = array("cod"=>$liv3_v,
							"txt"=> $liv3_txt,
							"dta"=>array());
			}
	
				
			$ar[$liv1_v]["val"][$liv2_v]["k_carico"] = $ar[$liv1_v]["k_carico"];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_carico"] = $ar[$liv1_v]["k_carico"];
	
			if ($ordina_per_tipo_ord != 'Y'){
				$ar[$liv1_v]["val"][$liv2_v]["sped_id"] = $ar[$liv1_v]["sped_id"];
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["sped_id"] = $ar[$liv1_v]["sped_id"];
			}
				
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["riferimento"] =  $row['TDVSRF'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["nr"] 			=  $row['TDONDO'];
			//$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_ordine"]	=  $this->k_ordine_td($row);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_ordine"]	=  $row['TDDOCU'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["k_cli_des"]	=  $ar[$liv1_v]["val"][$liv2_v]['k_cli_des'];
	
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tipo"] 		=  $row['TDOTPD'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["raggr"] 		=  $row['TDCLOR'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["priorita"]	=  $row['TDOPRI'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"]		=  $this->get_tp_pri(trim($row['TDOPRI']), $row);
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_reg"]	=  $row['TDODRE'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_rich"]	=  $row['TDODER'];
				
				
			if ($tipo_elenco == 'HOLD'){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_prog"]	=  $row['hold_TDDTEP'];
			} else {
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_prog"]	=  $row['TDDTEP'];
			}
				
	
				
			//a livello di cliente riporto la MAX data disponibilita
			$ar[$liv1_v]["val"][$liv2_v]["data_disp"] 	= max($ar[$liv1_v]["val"][$liv2_v]["data_disp"], $row['TDDTDS']);
	
	
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["TDFN11"]		=  $row['TDFN11']; //evaso
				
			/*
				if ($tipo_elenco == 'HOLD')
				//data disponibilita'
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTDS'];
			else
				//data confermata
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTVA'];
			*/
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_conf"]	=  $row['TDDTVA'];
	
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_cons_conf"]	=  $row['TDFN06'];
	
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_evaso"]	=  $row['TDFN11'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["stato"]		=  $row['TDSTAT'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["volume"] 		+= $row['TDVOLU'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["peso"] 		+= $row['TDPLOR'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["importo"]		+= $row['TDTIMP'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["pallet"]		+= $row['TDBANC'];
	
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli"] 		+= $row['TDTOCO'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli_disp"] 	+= $row['TDCOPR'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["colli_sped"] 	+= $row['TDCOSP'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_disp"] 		= $row['TDDTDS'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_conf_ord"] 	= $row['TDDTCF'];
	
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["data_cons_cli"] = $row['TDDTCP'];
	
			//a livello di cliente riporto la MAX data disponibilita
			$ar[$liv1_v]["val"][$liv2_v]["data_disp"] 	= max($ar[$liv1_v]["val"][$liv2_v]["data_disp"], $row['TDDTDS']);
	
			//segnalo di rosa se data_disp > data prog
			if ($row['TDDTDS'] > $row['TDDTEP'] && $row['TDDTEP'] > 0){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_data_disp"] = 1; //segnalo di rosa
				$ar[$liv1_v]["val"][$liv2_v]["fl_data_disp"] = 1; //segnalo di rosa anche sul cliente
			}
				
				
			//flag
				
				
			//blocco amministrativo (TDBLEV) e/o commerciale (TDBLOC)
			//fl_bloc:
			// 4 - amministrativo e commerciale
			// 3 - amministrativo
			// 2 - commerciale
			// 1 - sbloccato
				
			$m_tipo_blocco = null;
			if ($row['TDBLEV'] == 'Y' || $row['TDBLOC'] == 'Y')
			{
				if ($row['TDBLEV'] == 'Y' && $row['TDBLOC'] == 'Y')	$m_tipo_blocco = 4;
				else {
					if ($row['TDBLEV'] == 'Y') $m_tipo_blocco = 3;
					else if ($row['TDBLOC'] == 'Y') $m_tipo_blocco = 2;
				}
	
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_bloc"] 	= $m_tipo_blocco;
	
				//riporto a livello superiore
				$ar[$liv1_v]["fl_bloc"] 				= $this->get_fl_bloc($ar[$liv1_v]["fl_bloc"], $m_tipo_blocco);
				$ar[$liv1_v]["val"][$liv2_v]["fl_bloc"] = $this->get_fl_bloc($ar[$liv1_v]["val"][$liv2_v]["fl_bloc"], $m_tipo_blocco);
	
			}
				
				
			if (($row['TDBLEV'] != 'Y' && $row['TDBLOC'] != 'Y') && ($row['TDBLEV'] == 'S' || $row['TDBLOC'] == 'S')) //sbloccato
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_bloc"] 	= 1;
	
	
			//fl_ref (stelline su pezzi)
			if ($row['TDTACO'] == 0)
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_ref"]	=  1; //stella verde
			elseif ($row['TDTAPB'] > 0) //ho delle prebolle
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_ref"]	=  3; //stella grigia
			else
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_ref"]	=  2; //niente
			//riporto a livello superrioe
			$ar[$liv1_v]["val"][$liv2_v]["fl_ref"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_ref"], $ar[$liv1_v]["val"][$liv2_v]["fl_ref"]);
			//se ho un verde ma non tutti... passo a liello superiore grigio
			if ($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_ref"] == 1 && $ar[$liv1_v]["val"][$liv2_v]["fl_ref"] == 2)
				$ar[$liv1_v]["val"][$liv2_v]["fl_ref"] = 3;
				
					
	
			if ($tipo_elenco == 'HOLD')
			{
				$ar[$liv1_v]["fl_da_prog"] 	= 1;
				$ar[$liv1_v]["val"][$liv2_v]["fl_da_prog"] 	= 1;
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_da_prog"] 	= 1;
			}
				
				
			if (trim($row['TDFG01']) == 'A' || trim($row['TDFG01']) == 'R') //indice modifica (icona + su new)
			{
				$ar[$liv1_v]["ind_modif"] = trim($row['TDFG01']);
				$ar[$liv1_v]["val"][$liv2_v]["ind_modif"] 	= trim($row['TDFG01']);
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["ind_modif"]	=  trim($row['TDFG01']);
			}
				
			if (oggi_AS_date() <= $row['TDDTNEW']){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"]	=  1;
			}
			if ($row['TDFN14'] == 1){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"]	=  2;
			}
			if ($row['TDFN13'] == 1){
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"]	=  4;
			}
			if ( to_AS_date_time($row['TDDTAR'], $row['TDHMAR']) > now_AS_date_time() ){ //attesa maturazione confermata
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_new"]	=  3;
			}
	
	
			//passo a livello superiore il tipo priorita
			$ar[$liv1_v]["tp_pri"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"], $ar[$liv1_v]["tp_pri"]);
			$ar[$liv1_v]["val"][$liv2_v]["tp_pri"] = max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["tp_pri"], $ar[$liv1_v]["val"][$liv2_v]["tp_pri"]);
			
			//data evasione progr
			$ar[$liv1_v]["val"][$liv2_v]["cons_prog"] = max($ar[$liv1_v]["val"][$liv2_v]["cons_prog"], $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_prog"]);
			//$ar[$liv1_v]["val"][$liv2_v]["cons_prog"] = max($ar[$liv1_v]["val"][$liv2_v]["cons_prog"], $ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["cons_prog"]);			
			
				
			if ($row['TDFMTO'] == 'M') //SEGNALAZIONE IN BLU (ARTICOLI MANCANTI)
			{
				$ar[$liv1_v]["fl_art_manc"] = max($ar[$liv1_v]["fl_art_manc"], 4);
				$ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"], 4);
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"], 4);
			}
			if ($row['TDFMTO'] == 'Y' && $row['TDMTOO'] == 'Y') //CARRELLO GRIGIO (ordini inviati, NON tutti ricevuti)
			{
				$ar[$liv1_v]["fl_art_manc"] = max($ar[$liv1_v]["fl_art_manc"], 3);
				$ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"], 3);
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"], 3);
			}
			if (trim($row['TDFMTO']) == '' && $row['TDMTOO'] == 'Y') //CARRELLO VERDE (ordini inviati, tutti ricevuti)
			{
				$ar[$liv1_v]["fl_art_manc"] = max($ar[$liv1_v]["fl_art_manc"], 2);
				$ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"], 2);
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"], 2);
			}
			if ($row['TDFMTO'] == 'Y' && $row['TDMTOO'] != 'Y') //SEGNALAZIONE GRIGIA (ordini non ancora inviati)
			{
				$ar[$liv1_v]["fl_art_manc"] = max($ar[$liv1_v]["fl_art_manc"], 1);
				$ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["fl_art_manc"], 1);
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"] 	= max($ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["fl_art_manc"], 1);
			}
				
	
			//if ($row['TDTPRI'] == 'E' || $row['TDDTVA'] == 0)
			if ($row['TDFMTS'] == 'Y')
			{
				$ar[$liv1_v]["art_da_prog"] 	= 1;
				$ar[$liv1_v]["val"][$liv2_v]["art_da_prog"] 	= 1;
				$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["art_da_prog"] 	= 1;
			}
	
			//referenze
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["referenze"] += $row['TDTART'];
			$ar[$liv1_v]["val"][$liv2_v]["val"][$liv3_v]["referenze_cons"] += $row['TDTAPB'];			
			$ar[$liv1_v]["val"][$liv2_v]["referenze"] += $row['TDTART'];
			$ar[$liv1_v]["val"][$liv2_v]["referenze_cons"] += $row['TDTAPB'];
			$ar[$liv1_v]["referenze"] += $row['TDTART'];
			$ar[$liv1_v]["referenze_cons"] += $row['TDTAPB'];						
			
			
			//somme parziali
			$ar[$liv1_v]["val"][$liv2_v]["colli"] 		+= $row['TDTOCO'];
			$ar[$liv1_v]["val"][$liv2_v]["pallet"] 		+= $row['TDBANC'];
			$ar[$liv1_v]["val"][$liv2_v]["colli_disp"] 	+= $row['TDCOPR'];
			$ar[$liv1_v]["val"][$liv2_v]["colli_sped"] 	+= $row['TDCOSP'];
			$ar[$liv1_v]["val"][$liv2_v]["volume"] 		+= $row['TDVOLU'];
			$ar[$liv1_v]["val"][$liv2_v]["peso"] 		+= $row['TDPLOR'];
			$ar[$liv1_v]["val"][$liv2_v]["importo"] 	+= $row['TDTIMP'];
			$ar[$liv1_v]["val"][$liv2_v]["fl_evaso"] 	+= $row['TDFN11'];
				
			$ar[$liv1_v]["colli"] 		+= $row['TDTOCO'];
			$ar[$liv1_v]["pallet"] 		+= $row['TDBANC'];
			$ar[$liv1_v]["colli_disp"]	+= $row['TDCOPR'];
			$ar[$liv1_v]["colli_sped"]	+= $row['TDCOSP'];
			$ar[$liv1_v]["volume"] 		+= $row['TDVOLU'];
			$ar[$liv1_v]["peso"] 		+= $row['TDPLOR'];
			$ar[$liv1_v]["importo"] 	+= $row['TDTIMP'];
			$ar[$liv1_v]["fl_evaso"] 	+= $row['TDFN11'];
				
			//totali per raggruppamento liv0
			if (is_null($ar_tot["LIV0"][$m_stacco_liv0_c]))
				$ar_tot["LIV0"][$m_stacco_liv0_c] = array();
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli"] 		+= $row['TDTOCO'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["pallet"] 	+= $row['TDBANC'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli_disp"]	+= $row['TDCOPR'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["colli_sped"]	+= $row['TDCOSP'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["volume"] 	+= $row['TDVOLU'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["peso"] 		+= $row['TDPLOR'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["importo"] 	+= $row['TDTIMP'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["referenze"] 			+= $row['TDTART'];
			$ar_tot["LIV0"][$m_stacco_liv0_c]["referenze_cons"] 	+= $row['TDTAPB'];			
			
				
			//data consegna cliente (la riporto sulla spedizione)
			if ($row['TDDTCP'] >= $row['TDDTEP'])
				$ar_tot["LIV0"][$m_stacco_liv0_c]["data_cons_cli"] = $row['TDDTCP'];
	
				
			//per colorare la priorita' nel primo/secondo livello
			$ar[$liv1_v]["val"][$liv2_v]["T_ROW"] 		+= 1;
			$ar[$liv1_v]["val"][$liv2_v]["T_TDFN06"] 	+= $row['TDFN06'];
			$ar[$liv1_v]["val"][$liv2_v]["T_TDFN11"] 	+= $row['TDFN11'];
			if (($row['TDFN07']==1 || $row['TDFN08']==1) && $row['TDFN06']==0 )
				$ar[$liv1_v]["val"][$liv2_v]["T_TDFN07_TDFN08"] 	+= 1;
	
			$ar[$liv1_v]["T_ROW"] 		+= 1;
			$ar[$liv1_v]["T_TDFN06"] 	+= $row['TDFN06'];
			$ar[$liv1_v]["T_TDFN11"] 	+= $row['TDFN11'];
			if (($row['TDFN07']==1 || $row['TDFN08']==1) && $row['TDFN06']==0 )
				$ar[$liv1_v]["T_TDFN07_TDFN08"] 	+= 1;
		}
	
		return array("det"=>$ar, "tot"=>$ar_tot);
	}
	
	
	public function get_fl_bloc($val_iniziale, $nuovo_valore){
	    switch($val_iniziale){
	        case 4: return 4;
	        case 3:
	            if ($nuovo_valore == 2) return 4; else return 3;
	        case 2:
	            if ($nuovo_valore == 4) return 4;
	            if ($nuovo_valore == 3) return 4;
	            return 2;
	        default:
	            return $nuovo_valore;
	    }
	    
	}
	
	
	public function crea_alberatura_el_ordini_json($ar, $id, $cod_itin, $titolo_panel, $tipo_elenco = "", $ordina_per_tipo_ord = "N", $data = 0){
		$ar_tot_sped = $ar["tot"]["LIV0"];
		if ($id == "root" || $id=='')
		{
			$pre_id = "";
			$liv = 1;
			$ar = $ar["det"];
		}
		else
		{
			//trovo liv1 e liv2 selezionati
			$pre_id = $id;
			$sottostringhe = explode("|", $id);
			$liv1 = $sottostringhe[2];
			$liv2 = $sottostringhe[4];
				
			if (strlen($liv2) == ""){
				$liv2 = null;
				$refresh_liv1 = "Y";
				$ar = $ar["det"][$liv1]["val"];
				$liv = 2;
			}
			else {
				$liv = 3;
				$ar = $ar["det"][$liv1]["val"][$liv2]["val"];
			}
				
		}
	
	
		$inline_id_liv0 = -99999999;
		
		$ret = '{"text":".",
		 		 "titolo_panel": "' . $titolo_panel . '",
		 		 "tipo_elenco": "' . $tipo_elenco . '",
		 		 "cod_iti": "' . $cod_itin . '",
		 		 "data": "' . $data . '",
				 "itinerario": ' . j($this->decod_std('ITIN', $cod_itin)) . ',
				 "children": [';
	
		foreach ($ar as $r){
	
			//se cambia, aggiungo la riga (foglia) con id spedizione
	
			if ($refresh_liv1 != "Y" && $liv == 1 && $inline_id_spedizione!=$r['stacco_liv0_c']){
					
				//riga principale				
				$liv0_r = $ar_tot_sped[$r['stacco_liv0_c']];

				$m_txt = $r['stacco_liv0_d'];
				$m_rif = '';
					
				$ret .= " {";
				$ret .="
				        task: " . j($m_txt) . ",
					        id: \"|SPED|{$r['stacco_liv0_c']}\",
					        liv: \"liv_0\",
					        riferimento: " . j(trim($m_rif)) . "," . "";
						        
				$ret .= "colli: \"{$liv0_r["colli"]}\"" . ",";
				$ret .= "pallet: \"" . number_format($liv0_r["pallet"], 0, ',', '.') ."\"" . ",";
				$ret .= "referenze: \"" . number_format($liv0_r["referenze"], 0, ',', '.') ."\"" . ",";		
				$ret .= "volume: \"" . number_format($liv0_r["volume"], 1, ',', '.') ."\"" . ",";				
				$ret .= "referenze_cons: \"" . number_format($liv0_r["referenze_cons"], 0, ',', '.') ."\"" . ",";		
				$ret .= "colli_disp: \"{$liv0_r["colli_disp"]}\"" . ",";
				$ret .= "colli_sped: \"{$liv0_r["colli_sped"]}\"" . ",";
				$ret .= "peso: \"" . number_format($liv0_r["peso"], 1,',','.') . "\"" . ",";
				$ret .= "importo: \"" . number_format($liv0_r["importo"], 2,',','.') . "\"" . ",";				
					        		
				$ret .= "					        		
					        leaf: true
						";
					$ret .=" },";
					
					$inline_id_spedizione = $r['stacco_liv0_c'];
			}
	
	
	
	
	
	
			$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];
			$ret .= " {";
			$ret .="
			id: \"{$m_id}\",
			task: " . j($r["txt"]) . ",
			liv: \"liv_{$liv}\",
			iconCls: " . j($this->get_iconCls($liv, $r)) . ",
			riferimento: " . j(trim($r["riferimento"])) . "," . "
			expanded: true,
			" . $this->nodo_dett_el_ordini($r, $liv)
			. $this->nodo_children_el_ordini($m_id, $liv +1, $r["val"]) .
			"
					";
	
				//if ($liv==1)
				 $ret .= ", n_carico: \"" . $r['n_carico'] . "\"";
					  
					 $ret .=" },";
		}
			
	
		$ret .= ']}';
		return $ret;
	}
	
	
	
	private function nodo_dett_el_ordini($r, $liv){
		$ret = "";
	
		$ret .= "iconCls: " . j($this->get_iconCls($liv, $r)) . ",";
		$ret .= "tipo: " . j($r["tipo"]) . ",";
		$ret .= "tp_pri: \"{$r["tp_pri"]}\"" . ",";
		$ret .= "fl_evaso: \"{$r["fl_evaso"]}\"" . ",";
		$ret .= "cons_prog: \"{$r["cons_prog"]}\"" . ",";		
	
		if ($liv==3){ //ORDINE
			$ret .= "raggr: \"{$r["raggr"]}\"" . ",";
			$ret .= "nr: \"{$r["nr"]}\"" . ",";
			$ret .= "k_ordine: \"{$r["k_ordine"]}\"" . ",";
			$ret .= "k_cli_des: \"{$r["k_cli_des"]}\"" . ",";
			$ret .= "cod_iti: \"{$r["cod_iti"]}\"" . ",";
			$ret .= "priorita: \"{$r["priorita"]}\"" . ",";
			$ret .= "fl_new: \"{$r["fl_new"]}\"" . ",";
			$ret .= "data_reg: \"{$r["data_reg"]}\"" . ",";
			$ret .= "cons_rich: \"{$r["cons_rich"]}\"" . ",";
			$ret .= "cons_conf: \"{$r["cons_conf"]}\"" . ",";
			$ret .= "data_cons_cli: \"{$r["data_cons_cli"]}\"" . ",";
			$ret .= "stato: \"{$r["stato"]}\"" . ",";
		}
	
		if ($liv==3 || $liv==2){ //ORDINE o CLIENTE
			$ret .= "data_disp: \"{$r["data_disp"]}\"" . ",";
			$ret .= "data_conf_ord: \"{$r["data_conf_ord"]}\"" . ",";
			$ret .= "fl_data_disp: \"{$r["fl_data_disp"]}\"" . ",";
		}
	
		/*
		 if ($liv==1 || $liv==2){
		if ($r['T_ROW'] == $r['T_TDFN06'])
			$ret .= "tp_pri: \"all_conf\"" . ",";
		else if ($r['T_TDFN07_TDFN08'] > 0)
			$ret .= "tp_pri: \"tp_pri_4\"" . ",";
		}
		*/
		 
		if ($liv==2){
			$ret .= "k_cli_des: \"{$r["k_cli_des"]}\"" . ",";
			$ret .= "seq_carico: \"{$r["seq_carico"]}\"" . ",";
			$ret .= "cliente: " . j(trim($r["cliente"])) . ",";
			$ret .= "gmap_ind: " . j($r['gmap_ind']) . ",";
			$ret .= "scarico_intermedio: " . j($r['scarico_intermedio']) . ",";
		}
		 
		$ret .= "k_carico: \"{$r["k_carico"]}\"" . ",";
		$ret .= "sped_id: \"{$r["sped_id"]}\"" . ",";
		$ret .= "riferimento: " . trim(j($r["riferimento"])) . ",";
		$ret .= "colli: \"{$r["colli"]}\"" . ",";
		$ret .= "pallet: \"" . number_format($r["pallet"], 0, ',', '.') ."\"" . ",";
		$ret .= "referenze: \"" . number_format($r["referenze"], 0, ',', '.') ."\"" . ",";		
		$ret .= "referenze_cons: \"" . number_format($r["referenze_cons"], 0, ',', '.') ."\"" . ",";		
		$ret .= "colli_disp: \"{$r["colli_disp"]}\"" . ",";
		$ret .= "colli_sped: \"{$r["colli_sped"]}\"" . ",";
		$ret .= "volume: \"" . number_format($r["volume"], 1,',','.') . "\"" . ",";
		$ret .= "peso: \"" . number_format($r["peso"], 1,',','.') . "\"" . ",";
		$ret .= "importo: \"" . number_format($r["importo"], 2,',','.') . "\"" . ",";
		$ret .= "fl_bloc: \"{$r["fl_bloc"]}\"" . ",";
		$ret .= "fl_art_manc: \"{$r["fl_art_manc"]}\"" . ",";
		$ret .= "art_da_prog: \"{$r["art_da_prog"]}\"" . ",";
		$ret .= "fl_da_prog: \"{$r["fl_da_prog"]}\"" . ",";
		$ret .= "fl_ref: \"{$r["fl_ref"]}\"" . ",";		
		$ret .= "ind_modif: \"{$r["ind_modif"]}\"" . ",";
	
	
		return $ret;
	}
	
	

	private function nodo_children_el_ordini($id_partenza, $liv, $ar){
		if ($liv > 3){
			return "leaf: true";
		}
	
		$ret = "children: [ ";
		foreach ($ar as $kr => $r){
			$m_id = $id_partenza . "|LIV{$liv}|" . $r["cod"];
			$ret .= " {";
			if ($liv == 3) $ret .= "leaf: true,";
			$ret .="
			id: \"{$m_id}\",
			task: " . j($r["txt"]) . ",
					liv: \"liv_{$liv}\",
					iconCls: " . j($this->get_iconCls($liv, $r)) . ",
					expanded: false,
					" . $this->nodo_dett_el_ordini($r, $liv)
       			;
				$ret .=" },";
		}
		$ret .= " ]";
		 return $ret;
		}
	
		public function get_riga_commento_ordine($k_ord, $bl, $tpno = null){
		    global $conn;
		    global $cfg_mod_DeskAcq, $id_ditta_default;
		    
		    if (is_null($k_ord) || strlen(trim($k_ord)) == 0)
		        return array();
		        
	        if(isset($tpno) && $tpno != '')
	            $where = " AND RLTPNO = '{$tpno}'";
            else
                $where = " AND RLTPNO = 'TD'";
		                
            $sql = "SELECT RRN(RL) AS RRN, RL.*
            FROM {$cfg_mod_DeskAcq['file_note_anag']} RL
            WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$k_ord}'
            AND RLRIFE2 = '{$bl}' {$where}
            ORDER BY RLRIGA";
            
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);

            $ret = array();
            while ($row = db2_fetch_assoc($stmt)) {
                $nr = array();
                $nr['text'] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
                
                $nr['rrn'] = trim($row['RRN']);
                
                $ret[$row['RLRIGA']] = $nr;
            }
            
            return $ret;
		}
		
		
		public function get_commento_ordine_by_k_ordine($k_ord, $bl, $tpno = null, $riga = null){
		    global $conn, $id_ditta_default;
			global $cfg_mod_DeskAcq;
		
			if (is_null($k_ord) || strlen(trim($k_ord)) == 0)
				return array();
			if(isset($tpno) && $tpno != '')
			    $where = " AND RLTPNO = '{$tpno}'";
		    else
		        $where = " AND RLTPNO = 'TD'";
		    
	        if(isset($riga) && strlen($riga) > 0)
	            $where .= " AND RLRIGA = {$riga}";
		        
	        $sql = "SELECT RRN(RL) AS RRN, RL.*
	                FROM {$cfg_mod_DeskAcq['file_note_anag']} RL
	                WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$k_ord}'
	                AND RLRIFE2 = '{$bl}' {$where}
	                ORDER BY RLRIGA";
			
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
		
			$ret = array('testi' =>array());
			while ($row = db2_fetch_assoc($stmt)) {
			    $ret['testi'][] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
			    
			    
			    if ((int)$ret['data_user']['min_data_ge'] > 0){
			        if ($row['RLDTGE'] < $ret['min_data_ge']){
			            $ret['data_user']['min_data_ge'] = $row['RLDTGE'];
			            $ret['data_user']['min_user_ge'] = trim($row['RLUSGE']);
			        }
			    }
			    else {
			        $ret['data_user']['min_data_ge'] = $row['RLDTGE'];
			        $ret['data_user']['min_user_ge'] = trim($row['RLUSGE']);
			    }
			    
			    if ((int)$ret['data_user']['max_data_um'] > 0){
			        if ($row['RLDTUM'] > $ret['data_user']['max_data_um']){
			            $ret['data_user']['max_data_um'] = $row['RLDTUM'];
			            $ret['data_user']['max_user_um'] = trim($row['RLUSUM']);
			        }
			    }
			    else {
			        $ret['data_user']['max_data_um'] = $row['RLDTUM'];
			        $ret['data_user']['max_user_um'] = trim($row['RLUSUM']);
			    }
			}
		
			return $ret;
		}
		
	
	
	
	
	
	
	
	
	/* *********************************************************************************************/
	/* CALENDARIO (PLAN)*/
	/* *********************************************************************************************/
	
	public function get_dati_plan($da_data , $n_giorni=14, $root_id=""){
		global $conn, $auth;
		global $cfg_mod_DeskAcq;
		$da_data_as_txt = $da_data->format('Y-m-d');
		$da_data_as = $da_data->format('Ymd');
		$a_data_as = date('Ymd', strtotime($da_data_as_txt . " +{$n_giorni} days"));
	
		$f_select_f = "	TDDT, TAASPE,
						TDCITI, TASITI,
						TDDTEP, TDSWSP";
		$f_select_sum = ", sum(TDVOLU) as T_TDVOLU,  sum(TDTART) as T_TDTART, sum(TDTOCO) as T_TDTOCO, sum(TDBANC) as T_TDBANC,
						   sum(TDFN01) as T_CON_CARICO, sum(TDFN15) as T_CON_PREBOLLA, sum(TDFN02) as T_cli_bloc, sum(TDFN03) as T_ord_bloc,
						   sum(TDFN05) as T_art_mancanti, sum(TDFN04) as T_DA_PROG, sum(TDFN10) as T_N_ORD,
						   sum(TDFN06) as T_data_confermata, sum(TDFN07 + TDFN08) as T_data_tassativa,
						   sum(TDFN11) as T_ORD_EVASI,
						   count(*) as T_ROW ";
		$f_select_liv3 = ", TDNBOC, TDCCON, TDDCON, CSDTSP, CSHMPG, CSTISP, CSTITR, CSVOLD, CSBAND, CSPESO ";
	
		if ($root_id == "root")
		{
			$pre_id = "";
			$liv = 1;
//			$sql_field = $f_select_f . $f_select_sum;
//			$sql_group_by = " GROUP BY " . $f_select_f;
			$sql_order = "";
			$sql_where = "";
			
			//gi� scendo a livello principale
			$sql_field = $f_select_f . $f_select_sum. $f_select_liv3;
			$sql_group_by = " GROUP BY " . $f_select_f . $f_select_liv3;
			$sql_order = ", TDDTEP, CSDTSP, CSHMPG, TDDCON";						
		}
		else
		{
			//trovo liv1 e liv2 selezionati
			$pre_id = $root_id;
			$sottostringhe = explode("|", $root_id);
			$liv1 = $sottostringhe[2]; //AREA
			$liv2 = $sottostringhe[4]; //ITINERARIO
			$liv = 3;
			$sql_field = $f_select_f . $f_select_sum. $f_select_liv3;
			$sql_group_by = " GROUP BY " . $f_select_f . $f_select_liv3;
			$sql_order = ", TDDTEP, CSDTSP, CSHMPG, TDDCON";
			$sql_where = " AND TAASPE = '{$liv1}' AND TDCITI = '{$liv2}' ";
		}
			
		if ($this->get_fl_solo_con_carico_assegnato() == 1) //solo ord. con carico assegnato
			$sql_where = " AND TDDOCL = " . sql_t(trim($auth->get_user())); //l'utente collegato e' quello referente cliente
		
		
		$sql = "SELECT 	{$sql_field}
		FROM {$cfg_mod_DeskAcq['file_testate']} TD
				LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
					  LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
					  WHERE " . $this->get_where_std(null, array('filtra_su' => 'SP,TD')) . " AND TDSWSP='Y' AND TD.TDDTEP >= {$da_data_as} AND TD.TDDTEP<{$a_data_as} {$sql_where}
					  /* AND TDCITI = 'FMO' */ 
					  {$sql_group_by}
					  ";
					  if ($liv==1)
					  $sql .= " UNION
					  SELECT 	{$sql_field}
					  FROM {$cfg_mod_DeskAcq['file_testate']} TD
				LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG					  
				LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
					  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
					  WHERE " . $this->get_where_std(null, array('filtra_su' => 'SP,TD')) . " AND TDSWSP='N' {$sql_where} /* ORD. NON PROGRAMMABILI */
					  /* AND TDCITI = 'FMO' */					  					  					  
					  {$sql_group_by}
					  ";
	
					  $sql .= " ORDER BY TAASPE, TASITI, TDDT " . $sql_order;

					  
					  
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
	
			if (!$result)
					  	echo db2_stmt_errormsg($stmt);
	
					  	return $stmt;
			}
	
	
			
			

			public function crea_array_valori_plan($its, $tipo_elenco = "", $node){
				$ar = array();
				$ar_tot = array();
				$ar_liv_mapping = array();
			
				$liv0_v = $liv1_v = $liv2_v = $liv3_v = $liv4_v = $liv5_v = "--------------";
				$liv0_c = $liv1_c = $liv2_c = $liv3_c = $liv4_c = $liv5_c = -1;
			
				$n_children = "children";
			
				while ($row = db2_fetch_assoc($its)) {

					if (strlen(trim($row['TAASPE'])) == 0) $row['TAASPE'] = 'ND';
					
					//definizione livello di alberatura
					$liv0_v = trim($row['TAASPE']); //AREA (DIVISIONE COMMERCIALE | STABILIMENTO)
					$liv1_v = trim($row['TDCITI']); //ITINERARIO (GRUPPO MERCEOLOGICO)
					$liv2_v = trim($row['TDCCON']);	//fornitore
						
					//FLAG BLOCCO
					// 4 - amministrativo e commerciale
					// 3 - amministrativo
					// 2 - commerciale
					// 1 - sbloccato
					$m_tipo_blocco = null;
					if ($row['TDBLEV'] == 'Y' || $row['TDBLOC'] == 'Y')
					{
						if ($row['TDBLEV'] == 'Y' && $row['TDBLOC'] == 'Y')	$m_tipo_blocco = 4;
						else {
							if ($row['TDBLEV'] == 'Y') $m_tipo_blocco = 3;
							else if ($row['TDBLOC'] == 'Y') $m_tipo_blocco = 2;
						}
					}
			
					if (($row['TDBLEV'] != 'Y' && $row['TDBLOC'] != 'Y') && ($row['TDBLEV'] == 'S' || $row['TDBLOC'] == 'S')) //sbloccato
						$m_tipo_blocco 	= 1;
			
			
					$row["fl_bloc_ordine"] = $m_tipo_blocco;
												
					$tmp_ar_id = array();
			
					// -----------------------------------------------------------------------
					
					// LIVELLO 0
					$s_ar = &$ar;
					$liv_c 	= $liv0_v;
					$tmp_ar_id[] = $liv_c;					
					if (is_null($s_ar[$liv_c])){
						$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");			
						$s_ar[$liv_c]["task"]   = $this->decod_std('ASPE', $liv_c);
						$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);			
					}										
					$this->crea_array_valori_plan_liv($s_ar[$liv_c], $row, 0);
					$s_ar = &$s_ar[$liv_c][$n_children];
						
			
					// LIVELLO 1
					$liv_c 	= $liv1_v;
					$tmp_ar_id[] = $liv_c;					
					if (is_null($s_ar[$liv_c])){
						$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_2");			
						$s_ar[$liv_c]["task"]   = $this->decod_std('ITIN', $liv_c);
						$s_ar[$liv_c]['itin'] = $liv1_v; //itinerario						
						$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);						
					} 					
										
					$this->crea_array_valori_plan_liv($s_ar[$liv_c], $row, 1);
					$s_ar = &$s_ar[$liv_c][$n_children];						
						
					// LIVELLO 2
					$liv_c 	= $liv2_v;
					$tmp_ar_id[] = $liv_c;					
					if (is_null($s_ar[$liv_c])){				
						$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_3");
						$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);						
						$s_ar[$liv_c]['leaf'] = true;
						$s_ar[$liv_c]['itin'] = $liv1_v; //itinerario
						$s_ar[$liv_c]['task'] = acs_u8e($row['TDDCON']);
						$s_ar[$liv_c]['sped_id'] = $row['TDNBOC'];
						$s_ar[$liv_c]['k_cliente'] = $row['TDCCON'];
						
						//Verifico se ha dei contratti
						$s_ar[$liv_c]["ha_contratti"] = $this->fornitore_ha_contratti($row['TDDT'], $row['TDCCON']);
							//passo anche ai livelli sopra
							$ar[$liv0_v]["ha_contratti"] = max($ar[$liv0_v]["ha_contratti"], $s_ar[$liv_c]["ha_contratti"]);
							$ar[$liv0_v]["children"][$liv1_v]["ha_contratti"] = max($ar[$liv0_v]["children"][$liv1_v]["ha_contratti"], $s_ar[$liv_c]["ha_contratti"]);							
					}

					
					$this->crea_array_valori_plan_liv($s_ar[$liv_c], $row, 2);
					$s_ar = &$s_ar[$liv_c][$n_children];
			
					// -----------------------------------------------------------------------

					$this->crea_array_valori_plan_liv($ar_tot['liv_totale'], $row, 'liv_totale');
					
					
				} //per ogni record
				
				return array("det"=>$ar, "tot"=>$ar_tot);
			}
					
			
			
			
			
			//Gestione per visualizzazione icona richieste_MTO (play) su plan
			public function crea_array_valori_plan_richieste_MTO($ar){
				global $conn, $cfg_mod_DeskAcq, $id_ditta_default;				
				$n_children = "children";
				
				$sql = "SELECT 	MTFORN, MIN(CASE WHEN TRIM(MTFLM15)='' OR MTFLM15='P' OR MTFLM15='8' THEN 9 ELSE INTEGER(MTFLM15) END) AS MTFLM15
						 FROM {$cfg_mod_DeskAcq['file_proposte_MTO']}
						 WHERE MTDT = '{$id_ditta_default}' AND MTAVAN = 'A' AND MTSTAR ='' AND MTFLM14 <> 'E'
						GROUP BY MTFORN
						";
				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt);
				while ($row = db2_fetch_assoc($stmt)) {
					
					//dal fornitore recupero il gruppo di fornitura (ITIN) e da questo l'area (ASPE)
					$forn = new DeskAcqFornitori();
					$forn->load_rec_data_by_k(array('TAKEY1' => $row['MTFORN']));

					if (!is_null($forn->rec_data['TAKEY1'])){
						
						$liv2_d = trim($forn->rec_data['TADESC']);
						
						$gr_fornitura = new DeskAcqItinerari();
						$gr_fornitura->load_rec_data_by_k(array('TAKEY1' => $forn->rec_data['TAASPE']));						
						
						if (!is_null($gr_fornitura->rec_data['TAKEY1'])){
							
							$area_ric = new DeskAcqAreeSpedizione();
							$area_ric->load_rec_data_by_k(array('TAKEY1' => $gr_fornitura->rec_data['TAASPE']));

							$liv1_v = trim($gr_fornitura->rec_data['TAKEY1']); $liv1_d = trim($gr_fornitura->rec_data['TADESC']);							
							
							if (!is_null($area_ric->rec_data['TAKEY1'])){
								
								$liv0_v = trim($area_ric->rec_data['TAKEY1']); $liv0_d = trim($area_ric->rec_data['TADESC']);								
							} else {
								$liv0_v = 'ERR_AREA'; $liv0_d = '*** AREA RICEZ.? ***';								
							}
														
							
						} else {							
							$liv0_v = 'ERR_GR'; $liv0_d = '*** GR FORNITURA? ***';
							$liv1_v = 'NON_PRESENTE'; $liv1_d = '*** FORNN. NON PRESENTE ***';							
						}
						
						
					} else {
						$liv0_v = 'NON_PRESENTE'; $liv0_d = '*** FORNN. NON PRESENTE ***';						
						$liv1_v = 'NON_PRESENTE'; $liv1_d = '*** FORNN. NON PRESENTE ***';						
						$liv2_d = '*** FORNN. *** ' . trim($row['MTFORN']);
					}

					//definizione livello di alberatura plan
					//$liv0_v ---> area di ricezione
					//$liv1_v ---> gruppo fornitura (da ITIN)
					$liv2_v = trim($row['MTFORN']);	//fornitore
					
					// -----------------------------------------------------------------------

				/*	
					echo "\n--------------------------------------";
					echo "\n{$liv0_v} - {$liv0_d}";
					echo "\n{$liv1_v} - {$liv1_d}";
					echo "\n{$liv2_v} - {$liv2_d}";
				*/	
					
					
					$tmp_ar_id = array();
					// LIVELLO 0
					$s_ar = &$ar;
					$liv_c 	= $liv0_v;
					$tmp_ar_id[] = $liv_c;
					if (is_null($s_ar[$liv_c])){
						$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");
						$s_ar[$liv_c]["task"]   = $liv0_d;
						$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
					}
					$this->crea_array_valori_plan_proposte_MTO($s_ar[$liv_c], $row, 0);
					$s_ar = &$s_ar[$liv_c][$n_children];
					
						
					// LIVELLO 1
					$liv_c 	= $liv1_v;
					$tmp_ar_id[] = $liv_c;
					if (is_null($s_ar[$liv_c])){
						$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_2");
						$s_ar[$liv_c]["task"]   = $liv1_d;
						$s_ar[$liv_c]['itin'] = $liv1_v; //itinerario
						$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
					}
					$this->crea_array_valori_plan_proposte_MTO($s_ar[$liv_c], $row, 1);
					$s_ar = &$s_ar[$liv_c][$n_children];
					
					// LIVELLO 2
					$liv_c 	= $liv2_v;
					$tmp_ar_id[] = $liv_c;
					if (is_null($s_ar[$liv_c])){
						$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_3");
						$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
						$s_ar[$liv_c]['leaf'] = true;
						$s_ar[$liv_c]['itin'] = $liv1_v; //itinerario
						$s_ar[$liv_c]['task'] = $liv2_d;
						$s_ar[$liv_c]['sped_id'] = -1;
						$s_ar[$liv_c]['k_cliente'] = $liv2_v;				
					}
					$this->crea_array_valori_plan_proposte_MTO($s_ar[$liv_c], $row, 1);
					
					
				}				
				return $ar;	
				
			}			
			
			
			
			public function crea_array_valori_plan_proposte_MTO(&$ar, $row, $n_liv){
				if (trim($row['MTFLM15']) == '') $row['MTFLM15'] = 9;
				if (!isset($ar['ha_proposte_MTO'])) $ar['ha_proposte_MTO'] = 9; 
				$ar['ha_proposte_MTO'] = min($ar['ha_proposte_MTO'], $row['MTFLM15']);
			}
			
			
			
	
			public function crea_array_valori_plan_liv(&$ar, $row, $n_liv){
			  if (trim($row['TDSWSP']) == 'Y'){				
				$ar["dta"][$row['TDDTEP']]['VOL'] += $row['T_TDVOLU'];
				$ar["dta"][$row['TDDTEP']]['COL'] += $row['T_TDTART'];
				$ar["dta"][$row['TDDTEP']]['PAL'] += $row['T_TDBANC'];
				$ar["dta"][$row['TDDTEP']]['ORD'] += $row['T_N_ORD'];
				$ar["dta"][$row['TDDTEP']]['ROW'] += $row['T_ROW'];
				$ar["dta"][$row['TDDTEP']]['ORD_CON_CAR'] += $row['T_CON_CARICO'];
				$ar["dta"][$row['TDDTEP']]['ORD_CON_PREB'] += $row['T_CON_PREBOLLA'];				
				$ar["dta"][$row['TDDTEP']]['ORD_EVASI']   += $row['T_ORD_EVASI'];
				$ar["dta"][$row['TDDTEP']]['ORD_DATA_CONF'] += $row['T_DATA_CONFERMATA'];
				$ar["dta"][$row['TDDTEP']]['ORD_DATA_TASS'] += $row['T_DATA_TASSATIVA'];
			  }	
				
				if ($row['T_ART_MANCANTI'] > 0)	$ar["fl_art_mancanti"] 	= 1;
				if ($row['T_DA_PROG'] > 0)	$ar["fl_da_prog"] 	= 1;
			}
				
			
			
			public function crea_json_dati_plan($ar, $node, $da_data_txt, $field_dett){	
				$ar_tot = $ar['tot'];			
				$da_data_iniziale = $da_data_txt;
				
				if ($node != "root"){
					$ar = $ar["det"][0]["children"][0]["children"][0]["children"];										
				} else {
					$ar = $ar["det"];

					
					//aggiungo le righe di totale
					foreach(array('ORD' => 'Ordini', 'VOL' => 'Volume', 'COL' => 'Pezzi') as $k_tot => $des_tot){
						$t = array();
						$t['id'] 	= "tot_{$k_tot}";
						$t['cod']	= $k_tot;
						$t['task']	= "Totale {$des_tot}";
						$t['liv']	= "liv_totale";
						$t['iconCls'] = "task-folder";
						$t['leaf']	= true;
						$t['dta'] 	= $ar_tot['liv_totale']['dta'];
						
						array_unshift($ar, $t);						
					}
					
										
				}
			
				$ret = array();	
				
				//entro in tutti i children per creare d_1, d_2.....
				$giorni_festivi 	= $this->get_giorni_festivi($da_data_txt, $n_giorni);				
				foreach($ar as $kar => $r){
					$this->crea_json_dati_plan_out_date($ar[$kar], $da_data_txt, $field_dett, $giorni_festivi);
				}				
												
				//esporto i dati
				foreach($ar as $kar => $r){																
					//$ret[] = $ar[$kar];
					
					$ret[] = array_values_recursive($ar[$kar]);		
								
				}
				
				

				
				//titoli colonne
					$ar_columns = array();
					
					$oldLocale = setlocale(LC_TIME, 'it_IT');
					for ($i=1; $i<=14; $i++)
					{
					$d = strtotime($da_data_txt);
					$ar_columns["c{$i}"] = ucfirst(strftime("%a", $d)) . "<br>" . strftime("%d/%m", $d);
					if ($i<14)
						$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " +1 days"));
					}
					setlocale(LC_TIME, $oldLocale);
			
				$ar_ret = array();
				$ar_ret['children'] = $ret;
				$ar_ret['columns']  = $ar_columns;
				$ar_ret['parameters']['da_data'] = strftime("%Y%m%d", strtotime($da_data_iniziale));				
				$ar_ret['parameters']['field_dett'] = $field_dett;
				$ar_ret['toggle_fl_solo_con_carico_assegnato'] = $this->get_fl_solo_con_carico_assegnato();								
									
				return acs_je($ar_ret);
			
			}
						
			
			
			function crea_json_dati_plan_out_date(&$ar, $da_data_txt, $field_dett, $giorni_festivi){
				
				if ($ar['liv'] == 'liv_totale'){
					$field_dett = $ar['cod'];
				}
				
				for ($i = 1; $i<= 14; $i++){
					$m_d = $i-1;
					$dd = date('Ymd', strtotime($da_data_txt . " +{$m_d} days"));
					
					if (strlen($ar['dta'][$dd][$field_dett]) > 0)
						$ar["d_{$i}"] = number_format($ar['dta'][$dd][$field_dett],0,',','.');
					
					if (isSet($giorni_festivi[$dd]))
						$ar["d_{$i}_f"] = $giorni_festivi[$dd];

					//coloro cella in base al carico
					if (strlen($ar['dta'][$dd][$field_dett]) > 0 && $ar['liv'] != 'liv_totale')
						$ar["d_{$i}_t"] = $this->out_tipo_carico($ar['dta'][$dd]);					
					
				}				
				
				
				if (isset($ar["children"])){
					//entro in tutti i children per creare d_1, d_2.....
					foreach($ar["children"] as $kar => $r){
						$this->crea_json_dati_plan_out_date($ar["children"][$kar], $da_data_txt, $field_dett, $giorni_festivi);
					}					
				}
			}			
			

			
			
			
			
			
/********************************************************************************
 *  INDICI
 ********************************************************************************/			
			
			public function get_elenco_raggruppato_itinerari_per_data($id){
				global $conn;
				global $cfg_mod_DeskAcq;
			
				$oggi = oggi_AS_date();
			
				switch($this->imposta_field_dett()){
					case "VOL": $sum_field = ", SUM(TDVOLU) AS TOT_F, MAX(SP.CSVOLD) AS DISP_F "; break;
					case "ORD": $sum_field = ", COUNT(*)    AS TOT_F, COUNT(*) AS DISP_F ";  break;
					case "COL": $sum_field = ", SUM(TDTART) AS TOT_F, SUM(TD.TDTART) AS DISP_F "; break;
					case "PAL": $sum_field = ", SUM(TDBANC) AS TOT_F, MAX(SP.CSBAND) AS DISP_F "; break;
					default: $val = 0; break;
				}
			
			
				if ($id == "root")
				{
					$sql_where_flt = " 1=1 ";
				}
				else
				{
					//trovo liv1 e liv2 selezionati
					$sottostringhe = explode("|", $id);
					$liv1 = $sottostringhe[2]; //anno-settimana
					$liv2 = $sottostringhe[4];
					$sql_where_flt = " ( TDDTEP='$liv2' ) ";
				}
			
			
				$sql = "SELECT TD.TDCCON, TDDCON, CAL.CSAARG, CAL.CSNRSE,	SP.CSCITI,
						SP.CSDTSP, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSPROG, SP.CSSTSP  $sum_field
						FROM {$cfg_mod_DeskAcq['file_calendario']} SP
						LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_testate']} TD
								ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
								AND  " . $this->get_where_std() . " AND TDSWSP = 'Y' AND $sql_where_flt
								LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
										ON TA_ITIN.TATAID='ITIN' AND TA_ITIN.TADT = SP.CSDT AND TA_ITIN.TAKEY1 = SP.CSCITI
										LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_calendario']} CAL
										ON CAL.CSDT = SP.CSDT AND CAL.CSCALE = '{$cfg_mod_DeskAcq['tipo_calendario']}' AND CAL.CSDTRG = SP.CSDTSP
										WHERE (SP.CSDTSP >= {$oggi} OR TD.TDONDO <> '') AND " . $this->get_where_std('none', array('filtra_su' => 'SP')) . "
				GROUP BY TD.TDCCON, TDDCON, CAL.CSAARG, CAL.CSNRSE, SP.CSCITI, SP.CSDTSP, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSPROG, SP.CSSTSP
				HAVING MAX(TD.TDONDO) IS NOT NULL OR SP.CSSTSP NOT IN('GA', 'AN')
				ORDER BY CAL.CSAARG, CAL.CSNRSE, SP.CSDTSP, TD.TDDCON";
			
				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt);
				return $stmt;
			}
				
			
			
			

			public function crea_alberatura_json_per_data($its, $field_dett, $id, $separa_per_settimana = 'N'){
			
				$liv1_c = $liv2_c = $liv3_c = 0;
				$liv1_v = $liv2_v = $liv3_v = "--------------";
			
				if ($id == "root")
					$liv = 1;
				else
					$liv = 3;
			
				while ($row = db2_fetch_assoc($its)) {
						
					$row['C_LIV1'] = $row['CSAARG'] . $row['CSNRSE'];
					$row['D_LIV1'] = "Settimana " . $row['CSNRSE'] . " / " . $row['CSAARG'];
						
					$row['C_LIV2'] = $row['CSDTSP'];
					$row['D_LIV2'] = $row['CSDTSP'];
					$row['C_LIV3'] = $row['TDNBOC'] . "-" . $row['TDCCON'];
					$row['D_LIV3'] = implode(" ", array($row['TDDCON'], "#{$row['CSPROG']}"));
					$row['D_LIV3_txt'] = "";
						
					if ($row['C_LIV1'] != $liv1_v){
						$liv1_c += 1;
						$liv2_c = $liv3_c = 0;
						$liv2_v = $liv3_v = "-----------";
						$liv1_v = $row['C_LIV1'];
						$ar[$liv1_v] = array("cod" => $row['C_LIV1'], "task"=>$row['D_LIV1'], "iconCls"=>"task-folder", "children"=>array());
					}
					if ($row['C_LIV2'] != $liv2_v){
						$liv2_c += 1;
						$liv3_c = 0; $liv3_v = "-----------";
						$liv2_v = $row['C_LIV2'];
						$m_txt = "";
						$m_week = "";
						if (strlen($row['C_LIV2']) == 8){
							$oldLocale = setlocale(LC_TIME, 'it_IT');
							$dd = date('D d/m', strtotime($row['C_LIV2']));
							$dd = ucfirst(strftime("%a %d/%m", strtotime($row['C_LIV2'])));
							$m_txt = $dd;
							$m_week = strftime("%V / %G", strtotime($row['C_LIV2']));
							setlocale(LC_TIME, $oldLocale);
						}
						$ar[$liv1_v]["children"][$liv2_v] = array("cod"=>$row['C_LIV2'], "task"=>$m_txt, "week"=>$m_week, "children"=>array());
					}
					if ($liv==3 && $row['C_LIV3'] != $liv3_v){
						$liv3_c += 1;
						$liv3_v = $row['C_LIV3'];
						$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v] = array("cod"=>$row['C_LIV3'], "task"=>trim($row['D_LIV3']));
						$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['txt'] = trim($row['D_LIV3_txt']);
			
						$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['assegnato'] += $row['TOT_F'];
						$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['disponibile'] += $row['DISP_F'];
						
						$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['sped_id'] = $row['CSPROG'];
						$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['k_cliente'] = $row['TDCCON'];
						$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['data'] = $row['CSDTSP'];
					}
						
				}
			
				
				
				if ($id == "root")
				{
					$pre_id = "";
					$liv = 1;
					$ar = $ar;
				}
				else
				{
					//trovo liv1 e liv2 selezionati
					$pre_id = $id;
					$sottostringhe = explode("|", $id);
					$liv1 = $sottostringhe[2];
					$liv2 = $sottostringhe[4];
					$liv = 3;
					$ar = $ar[$liv1]["children"][$liv2]["children"];
				}
			
				switch($this->imposta_field_dett()){
					case "VOL": $tipo_sum = "V: "; break;
					case "ORD": $tipo_sum = "O: ";  break;
					case "COL": $tipo_sum = "Nr: "; break; //referenze
					case "PAL": $tipo_sum = "P: "; break;
					default: $tipo_sum = ""; break;
				}
			
			
				if ($liv==3)
					$icon_class = "iconSpedizione";
				else
					$icon_class = "task-folder";
			
			
			
				$ret = '{"text":".","children": [';
			
				foreach ($ar as $r){
					$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];
			
					$m_txt = $r['task'];
					if ($liv==3){
						$m_txt .=  " (". $tipo_sum .  n($r['assegnato'], 0)  . ") ";
						$m_txt .= $r['txt'];
					}
			
						
					$ret .= " {";
					$ret .="
					id: \"{$m_id}\",
					task: " . j($m_txt) . ",
					iconCls:'$icon_class',
					expanded: false,
					sped_id: " . j($r["sped_id"]) . ",
					k_cliente: " . j($r["k_cliente"]) . ",																		
					data: " . j($r["data"]) . ",					
					"
					. $this->nodo_children($m_id, $liv +1, $r["children"], $separa_per_settimana) .
					"
					";
				$ret .=" },";
				}
					
			
				$ret .= ']}';
				return $ret;
				}
					
			
			

				private function nodo_children($id_partenza, $liv, $ar, $separa_per_settimana = "N"){
					if ($liv > 3){
						return "leaf: true";
					}
				
					$ret = "children: [ ";
				
					$cur_week = -9999999;
					foreach ($ar as $kr => $r){
						$m_id = $id_partenza . "|LIV{$liv}|" . $r["cod"];
						 
						$ret .= " {";
						$ret .="
							id: \"{$m_id}\",
							liv: \"liv_{$liv}\",
							task: " . j($r["task"]) . ",									
							iconCls:'task-folder',
					        expanded: false
				       			";
						$ret .=" },";
				    	}
				    	$ret .= " ]";
				    	return $ret;
					}			
			
			
			
			
			
					
					public function get_elenco_raggruppato_itinerari($id){
						global $conn;
						global $cfg_mod_DeskAcq;
					
						$oggi = oggi_AS_date();
					
						switch($this->imposta_field_dett()){
							case "VOL": $sum_field = ", SUM(TDVOLU) AS TOT_F, MAX(CSVOLD) AS DISP_F "; break;
							case "ORD": $sum_field = ", COUNT(*)    AS TOT_F, COUNT(*) AS DISP_F ";  break;
							case "COL": $sum_field = ", SUM(TDTART) AS TOT_F, SUM(TDTART) AS DISP_F "; break;
							case "PAL": $sum_field = ", SUM(TDBANC) AS TOT_F, MAX(CSBAND) AS DISP_F "; break;
							default: $val = 0; break;
						}
					
					
						if ($id == "root")
						{
							$sql_where_flt = " 1=1 ";
						}
						else
						{
							//trovo liv1 e liv2 selezionati
							$sottostringhe = explode("|", $id);
							$liv1 = $sottostringhe[2];
							$liv2 = $sottostringhe[4];
							$sql_where_flt = " ( /*TDASPE='$liv1' AND*/ TDCCON='$liv2' ) ";
						}
					
					
					
						$sql = "SELECT TD.TDCCON, TD.TDDCON, TA_ITIN.TAASPE as C_LIV1, TA_ITIN.TAASPE as D_LIV1,
								CSDTSP, CSCVET, CSCAUT, CSCCON, CSPROG, CSHMPG, CSSTSP $sum_field
								FROM {$cfg_mod_DeskAcq['file_calendario']} SP
								LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_testate']} TD
										ON SP.CSPROG = TD.TDNBOC AND " . $this->get_where_std() . " AND TDSWSP = 'Y'
										LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
												ON TA_ITIN.TATAID='ITIN' AND TA_ITIN.TADT = SP.CSDT AND TA_ITIN.TAKEY1 = SP.CSCITI
												WHERE  SP.CSCALE = '*SPR' AND $sql_where_flt AND " . $this->get_where_std('none', array('filtra_su' => 'SP')) . "
														AND (SP.CSDTSP >= {$oggi} OR TD.TDONDO <> '')
								GROUP BY TD.TDCCON, TD.TDDCON, TAASPE, CSCITI, CSDTSP, CSCVET, CSCAUT, CSCCON, CSPROG, CSHMPG, CSSTSP, TA_ITIN.TASITI
														HAVING MAX(TD.TDONDO) IS NOT NULL OR CSSTSP NOT IN('GA', 'AN')
														ORDER BY TAASPE, TD.TDDCON, SP.CSDTSP, CSHMPG, CSPROG
														";

						$stmt = db2_prepare($conn, $sql);
						$result = db2_execute($stmt);
						return $stmt;
					}
						
					
					
			
					public function crea_alberatura_json($its, $field_dett, $id){
					
						$liv1_c = $liv2_c = $liv3_c = 0;
						$liv1_v = $liv2_v = $liv3_v = "--------------";
					
					
						if ($id == "root")
							$liv = 1;
						else
							$liv = 3;
					
					
						while ($row = db2_fetch_assoc($its)) {
							$row['C_LIV2'] = implode("|", array($row['TDCCON']));
							$row['D_LIV2'] = implode(" ", array(trim(substr($row['TDDCON'], 0, 30))));							
							$row['C_LIV3'] = implode("|", array($row['CSPROG'], $row['CSDTSP']));
								
					
							if ($row['C_LIV1'] != $liv1_v){
								$liv1_c += 1;
								$liv2_c = $liv3_c = 0;
								$liv2_v = $liv3_v = "-----------";
								$liv1_v = $row['C_LIV1'];
								if (!isset($ar[$liv1_v]))
									$ar[$liv1_v] = array("cod" => $row['C_LIV1'], "task"=> $this->decod_std('ASPE', $row['C_LIV1']), "iconCls"=>"task-folder", "children"=>array());
							}
							if ($row['C_LIV2'] != $liv2_v){
								$liv2_c += 1;
								$liv3_c = 0; $liv3_v = "-----------";
								$liv2_v = $row['C_LIV2'];
					
								if (!isset($ar[$liv1_v]["children"][$liv2_v]))
									$ar[$liv1_v]["children"][$liv2_v] = array("cod"=>$row['C_LIV2'], "task"=>$row['D_LIV2'], "children"=>array());
							}
							if ($liv==3 && $row['C_LIV3'] != $liv3_v){
								$liv3_c += 1;
								$liv3_v = $row['C_LIV3'];
								$m_txt = "";
								if (strlen($row['CSDTSP']) == 8){
									$oldLocale = setlocale(LC_TIME, 'it_IT');
									$dd = date('D d/m', strtotime($row['CSDTSP']));
									$dd = ucfirst(strftime("%a %d/%m", strtotime($row['CSDTSP'])));
									$dd .= " " . print_ora($row['CSHMPG']);
									$m_txt = $dd;
									setlocale(LC_TIME, $oldLocale);
								}
					
								if (!isset($ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]))
								{
									$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v] = array("cod"=>$row['C_LIV3'], "task"=>trim($m_txt));
									$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['dd'] = $dd;
									$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['txt'] = $row['TDDCON'];
								}
					
								$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['assegnato'] += $row['TOT_F'];
								$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['disponibile'] += $row['DISP_F'];
								
								$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['sped_id'] = $row['CSPROG'];
								$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['k_cliente'] = $row['TDCCON'];
								$ar[$liv1_v]["children"][$liv2_v]["children"][$liv3_v]['data'] = $row['CSDTSP'];								
							}
								
						}
					
					
					
		
					
						if ($id == "root")
						{
							$pre_id = "";
							$ar = $ar;
						}
						else
						{
							//trovo liv1 e liv2 selezionati
							$pre_id = $id;
							$sottostringhe = explode("|", $id);
							$liv1 = $sottostringhe[2];
							$liv2 = $sottostringhe[4];
							
							$ar = $ar[$liv1]["children"][$liv2]["children"];
							//$ar = array(); //riprendo tutte le spedizioni dalla tabella spedizioni //TODO: essere sicuri che ci siano!!
	
						}
					
					
					
						switch($this->imposta_field_dett()){
							case "VOL": $tipo_sum = "V: "; break;
							case "ORD": $tipo_sum = "O: ";  break;
							case "COL": $tipo_sum = "Nr: "; break; //referenze
							case "PAL": $tipo_sum = "P: "; break;
							default: $tipo_sum = ""; break;
						}
					
						if ($liv==3)
							$icon_class = "iconSpedizione";
						else
							$icon_class = "task-folder";
					
						$ret = '{"text":".","children": [';
					
						foreach ($ar as $r){
					
							$m_txt = $r['task'];
					
							if ($liv==3){
								$m_txt = $r['dd'];
								$m_txt .=  " (". $tipo_sum .  n($r['assegnato'], 0) . ") ";
								$m_txt .= $r['txt'];
							}
					
							$m_id = "{$pre_id}|LIV{$liv}|" . $r["cod"];
							$ret .= " {";
							$ret .="
							id: \"{$m_id}\",
							liv: \"liv_{$liv}\",
							task: " . j($m_txt) . ",
							iconCls:'$icon_class',
							expanded: false,
							sped_id: " . j($r["sped_id"]) . ",
							k_cliente: " . j($r["k_cliente"]) . ",																		
							data: " . j($r["data"]) . ",							
							"
							. $this->nodo_children(	$m_id, $liv +1, $r["children"]) .
							"
							";
							$ret .=" },";
						}
							
					
						$ret .= ']}';
						return $ret;
	}
					
							
					
					
			
			
	
/*******************************************************************************
 * UTILITY
 ********************************************************************************/	
	
	
	
	function __construct($parameters = array()) {
		global $auth;
		
		if (isset($parameters['no_verify']) && $parameters['no_verify'] == 'Y'){
			return true;
		}
		
		
		$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
	
		if (is_null($mod_permitted) || !$mod_permitted){
			die("\nNon hai i permessi ({$this->mod_cod})!!");
		}
	
	}
	
	

	//coloro le celle in base ai carichi assegnati
	// 0 - ?
	// 1 - ?
	// 2 - ?
	// 3 - ?
	// 4 - ?
	private function out_tipo_carico($r){

/*		
		if ($r['ORD_EVASI'] > 0) {
			if ($r['ORD_CON_CAR'] == $r['ROW']) return 6; //verde scuro -- tutto evaso
			return 5; //verde chiavo - solo alcuni evasi
		}
	
		if ($r['ORD_CON_CAR'] == 0) {
			if ($r['ORD_DATA_CONF'] > 0)  return 3;
			if ($r['ORD_DATA_TASS'] > 0)  return 4;
			return 0;
		}
*/		
		
		if ($r['ORD_CON_CAR'] == $r['ROW'])	return 6;	

		if ($r['ORD_CON_PREB'] == $r['ROW']) return 2;		
		if ($r['ORD_CON_PREB'] > 0) return 1;				

		return 0;
	
	}
	
	
	
	
	public function fascetta_function(){
		global $auth, $main_module;
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
		
		echo "";
		echo "<div class='fascetta-toolbar'>";
			echo "<img id='bt-page-refresh' src=" . img_path("icone/48x48/button_grey_repeat.png") . " height=30>";
			
			if ($js_parameters->p_mod_param == 'Y')
				echo "<img id='bt-toolbar' src=" . img_path("icone/48x48/archive.png") . " height=30>";
			
			echo "<img id='bt-partenze' src=" . img_path("icone/48x48/fabbrica.png") . " height=30>";
			echo "<img id='bt-call-background' src=" . img_path("icone/48x48/warning_black.png") . " height=30>";
			echo "<img id='bt-gift' src=" . img_path("icone/48x48/gift.png") . " height=30>";			
			echo "<img id='bt-wizard_MTS' src=" . img_path("icone/48x48/shopping_basket_2.png") . " height=30>";
			echo "<img id='bt-plan-dispo' src=" . img_path("icone/48x48/grafici.png") . " height=30>";
			echo "<img id='bt-print' src=" . img_path("icone/48x48/print.png") . " height=30>";						
			echo "<img id='bt-fo_presca' src=" . img_path("icone/48x48/outbox.png") . " height=30>";			
			echo "<img id='bt-regole-riordino' src=" . img_path("icone/48x48/shopping_setup.png") . " height=30>";
			
			if ($js_parameters->p_acc_stat == 'Y')
				echo "<img id='bt-grafici' src=" . img_path("icone/48x48/grafici.png") . " height=30>";				
			
			echo "<img id='bt-fatture_entrata' src=" . img_path("icone/48x48/currency_black_euro.png") . " height=30>";
			//echo "<img id='add-promemoria' src=" . img_path("icone/48x48/clock.png") . " height=30>";
			echo "<img id='bt-call-pgm' src=" . img_path("icone/48x48/button_blue_play.png") . " height=30>";
			echo "<img id='bt-ricerca' src=" . img_path("icone/48x48/label_blue_new.png") . " height=30>";
			echo "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
			//orario aggiornamento dati
			echo "<div id='stato-aggiornamento'>[<img id='bt-stato-aggiornamento' class='st-aggiornamento' src=" . img_path("icone/48x48/rss_blue.png") . " height=20><span id='bt-stato-aggiornamento-esito'></span>]</div>";
			
		echo "</div>";		
		
	}

	
		
	function get_el_clienti_anag($p){
	    global $auth, $conn, $id_ditta_default;
	    global $cfg_mod_Spedizioni, $backend_ERP;
	    $ar = array();
	    $ret = array();
	    
	    if (strlen($auth->get_cod_riservatezza()) > 0){
	        //add_riservatezza
	        $join_riservatezza = "
	        INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
	        ON CFDT = TD.TDDT AND DIGITS(CFCD) = TD.TDCCON
	        " . $this->add_riservatezza();
	    } else {
	        $join_riservatezza = '';
	    }
	    
	    if ($backend_ERP == 'GL'){
	        /* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	        $sql = "SELECT distinct BACLF0, BARAG0, BAIND0, BALOC0, BAPRO0, BANAZ0, BATEL0, BAFAX0
	        FROM {$cfg_mod_Spedizioni['file_anag_cli']} {$join_riservatezza}
	        WHERE UPPER(
	        REPLACE(REPLACE(BARAG0, '.', ''), ' ', '')
	        )" . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
				AND BATCF0 = '1'
				AND BAQLA0 <> '98'
 			    order by BARAG0
 				";
	        
	        
	        $stmt = db2_prepare($conn, $sql);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt);
	        
	        while ($row = db2_fetch_assoc($stmt)) {
	            if (trim($row['BANAZ0']) == 'ITA' ||trim($row['BANAZ0']) == 'IT' || trim($row['BANAZ0']) == ''){
	                $out_loc = acs_u8e(trim($row['BALOC0']) . " (" . trim($row['BAPRO0']) . ")");
	            } else {
	                $des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
	                $out_loc = acs_u8e(trim($row['BALOC0']) . ", " . trim($des_naz['text']));
	            }
	            
	            $ret[] = array( "cod" 		=> $row['BACLF0'],
	                "descr" 	=> acs_u8e(trim($row['BARAG0'])),
	                "out_loc"	=> $out_loc,
	                "out_ind"	=> acs_u8e(trim($row['BAIND0'])),
	                "out_ema"	=> "",
	                "out_tel"	=> acs_u8e(trim($row['BATEL0']) ." / ".trim($row['BAFAX0']) )
	            );
	        }
	        
	        
	    } else {
	        
	        $m_params = acs_m_params_json_decode();
	        
	        
	        if (strlen(trim($m_params->form_values->f_cod_fi)) > 0)
	            $sql_where .= " AND CFCDFI LIKE '%{$m_params->form_values->f_cod_fi}%'";
	            
	        if (strlen(trim($m_params->form_values->f_tel)) > 0)
	            $sql_where .= " AND CFTEL LIKE '%{$m_params->form_values->f_tel}%'";
	                
	        if (strlen($m_params->form_values->f_email) > 0)
	             $sql_where .= " AND GC.GCMAIL LIKE '%{$m_params->form_values->f_email}%'";
	                    
	         if (strlen(trim($m_params->form_values->f_sel)) > 0)
	             $sql_where .= " AND CFSEL1 = '{$m_params->form_values->f_sel}'";
	                        
	                        
	                        /* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	                        $sql = "SELECT distinct CFCD, CFTEL, CFCDFI, CFRGS1, CFRGS2, CFIND1, CFIND2, CFLOC1, CFPROV, CFNAZ, GC.GCMAIL
	                        FROM {$cfg_mod_Spedizioni['file_anag_cli']} {$join_riservatezza}
	                        LEFT OUTER JOIN {$cfg_mod_Spedizioni['ins_new_anag']['file_appoggio']} GC
	                        ON CFDT = GC.GCDT AND DIGITS(CFCD) = GC.GCCDCF
	                        WHERE UPPER(
	                        REPLACE(REPLACE(CONCAT(CFRGS1, CFRGS2), '.', ''), ' ', '')
	                        ) " . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
 				AND CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = 'F' AND CFFLG3 = ''  {$sql_where}
 				order by CFRGS1, CFRGS2
 				";
	                        
	                        //print_r($sql);
	                        
	                        $stmt = db2_prepare($conn, $sql);
	                        echo db2_stmt_errormsg();
	                        $result = db2_execute($stmt);
	                        
	                        while ($row = db2_fetch_assoc($stmt)) {
	                            if (trim($row['CFNAZ']) == 'ITA' || trim($row['CFNAZ']) == ''){
	                                $out_loc = acs_u8e(trim($row['CFLOC1']) . " (" . trim($row['CFPROV']) . ")");
	                            } else {
	                                $des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
	                                $out_loc = acs_u8e(trim($row['CFLOC1']) . ", " . trim($des_naz[0]['text']));
	                            }
	                            
	                            
	                            $descr = trim($row['CFRGS1']) . " " . trim($row['CFRGS2']);
	                            if ($p['mostra_codice'] == 'Y')
	                                $descr .= " " . "[" . $row['CFCD'] . "]";
	                                
	                                $ret[] = array( "cod" 		=> $row['CFCD'],
	                                    "descr" 	=> acs_u8e($descr),
	                                    "out_loc"	=> $out_loc,
	                                    "out_ind"	=> acs_u8e(trim($row['CFIND1']) . " " . trim($row['CFIND2'])),
	                                    "tel" 		=> $row['CFTEL'],
	                                    "cod_fi" 	=> $row['CFCDFI']
	                                    
	                                );
	                        }
	    }
	    
	    return $ret;
	}
	
	
	public function get_initial_calendar_date(){
	
		if (!isSet($_SESSION[$this->get_cod_mod]['calendar_inizial_date'])){
			$_SESSION[$this->get_cod_mod]['calendar_inizial_date'] = date('Y-m-d');
		}
		return $_SESSION[$this->get_cod_mod]['calendar_inizial_date'];
	}
	
	
	
	public function set_initial_calendar_date($move) {
	
		$da_data_txt = $this->get_initial_calendar_date();
	
		switch ($_REQUEST['move']){
			case "next_1d":
				$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " +1 days"));
				break;
			case "next_1w":
				$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " +7 days"));
				break;
			case "prev_1d":
				$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " -1 days"));
				break;
			case "prev_1w":
				$da_data_txt = date('Y-m-d', strtotime($da_data_txt . " -7 days"));
				break;
		}
		$_SESSION[$this->get_cod_mod]['calendar_inizial_date'] = $da_data_txt;
	}
	
	
	
	public function imposta_field_dett(){
		if (!isSet($_SESSION[$this->get_cod_mod]['calendar_field'])){
			$_SESSION[$this->get_cod_mod]['calendar_field'] = "COL"; //COL--> REFERENZE
		}
		return $_SESSION[$this->get_cod_mod]['calendar_field'];
	}	
	
	public function set_calendar_field_dett($set_field) {
		switch ($set_field){
			case "tot_VOL":
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "VOL";
				break;
			case "tot_COL":
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "COL";
				break;
			case "tot_ORD":
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "ORD";
				break;
			case "tot_PAL":
				$_SESSION[$this->get_cod_mod]['calendar_field'] = "PAL";
				break;
		}
	}	
	
	
	
	public function toggle_fl_solo_con_carico_assegnato(){
		if (!isSet($_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'])){
			$_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] = 0;
		}
	
		$_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] = ($_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] == 0) ? 1 : 0;
	}
	public function get_fl_solo_con_carico_assegnato(){
		if (!isSet($_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'])){
			$_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'] = 0;
		}
		return $_SESSION[$this->get_cod_mod]['cal_fl_solo_con_carico_assegnato'];
	}
	
	
	
	
	public function get_where_std($tipo = '', $filtra_per_account = false){
		$ret = '';
		switch ($tipo){
			case 'search':
				$ret .= " (TDSWPP='Y' OR TDFDOE='Y') ";
				break;
			case 'none':
				$ret .= " 1=1 ";
				break;
			default:
				$ret .= " TDSWPP='Y' ";
		}
	
		if ($filtra_per_account != false){
			$ret .= " " . $this->get_where_std_account($tipo, $filtra_per_account) . " ";
		}
	
		return $ret;
	}
	
	

	public function get_where_std_account($tipo = '', $filtra_per_account = array()){
		global $auth;
		$js_parameters = $auth->get_module_parameters($this->get_cod_mod());
	
		$filtra_su = explode(",", $filtra_per_account['filtra_su']);
	
		$ret  = '';
	
	
		//filtro per area di spedizione
		if (isset($js_parameters->area_sped)){
			if (in_array('SP', $filtra_su))
				$ret .= " AND TA_ITIN.TAASPE IN (" . sql_t_IN($js_parameters->area_sped) . ") ";
			elseif (in_array('TD', $filtra_su))
			$ret .= " AND TDASPE IN (" . sql_t_IN($js_parameters->area_sped) . ") ";
		}
			
		//filtro in base all'utente (entry)
		if ($js_parameters->entry_flt == 1){
			if (in_array('AS', $filtra_su)){
				$ret .= " AND ASUSAT = " . sql_t_trim($auth->get_user()) . " ";
			}
		}
			
	
		return $ret;
	}
		
	
	function decod_cliente($cod){
		global $ar_decod_std;
		$tab = 'FORN';
		$a = $this->decod_std($tab, $cod);
		
		if (isset($ar_decod_std[$tab][$cod]))
			return $ar_decod_std[$tab][$cod];
		else {
			//lo recupero dal CF0
			global $id_ditta_default, $conn, $cfg_mod_DeskAcq, $cfg_mod_Admin;
			$sql = "SELECT CFRGS1 FROM {$cfg_mod_Admin['file_anag_cli']}
 				    WHERE CFDT = '{$id_ditta_default}' AND CFTICF = 'C' AND CFCD = ?";
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt, array($cod));	
			$row = db2_fetch_assoc($stmt);
			
			return $row['CFRGS1']; 
		}		
	}
	
	
	
	function decod_std($tab, $cod){
		global $conn, $cfg_mod_DeskAcq, $ar_decod_std;
		$tab = trim($tab);
		$cod = trim($cod);
	
		if (!isset($ar_decod_std[$tab])){
	
			$sql = "SELECT *
			FROM {$cfg_mod_DeskAcq['file_tabelle']}
			WHERE TATAID = '$tab'	/* AND TAKEY1 = '$cod' */
			";
				
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
	
			while ($row = db2_fetch_assoc($stmt)) {
			$r_cod = trim($row['TAKEY1']);
			$ar_decod_std[$tab][$r_cod] = trim($row['TADESC']);
			}
				
		}
	
	
			if (isset($ar_decod_std[$tab][$cod]))
				return $ar_decod_std[$tab][$cod];
				else
				return "$cod";
	}
	
	
	function get_giorni_festivi($da_data, $n_giorni){
		global $conn;
		global $cfg_mod_DeskAcq;
		$ar = array();
		$sql = "SELECT *
		FROM {$cfg_mod_DeskAcq['file_calendario']}
		WHERE CSCALE = '{$cfg_mod_DeskAcq['tipo_calendario']}'
				AND CSPROG=0
				";
				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt);
	
				while ($row = db2_fetch_assoc($stmt)) {
				$d = $row["CSDTRG"];
			$ar["{$d}"] = $row["CSTPGG"];
		}
			return $ar;
	}	
	
	
	
	function get_disponibilita_itinerari($da_data, $n_giorni, $solo_con_ordini = false){
		global $conn;
		global $cfg_mod_DeskAcq;
		$ar = array();
		$sql = "SELECT CSDTSP, CSCITI, SUM(CSVOLD) AS SUM_CSVOLD, SUM(CSBAND) AS SUM_CSBAND
		FROM {$cfg_mod_DeskAcq['file_calendario']}
		WHERE 1 = 1 ";
	
		if ($solo_con_ordini){
		$sql .= " AND CSPROG IN (SELECT DISTINCT TDNBOC FROM {$cfg_mod_DeskAcq['file_testate']}) ";
		}
	
			$sql .= " AND CSCALE = '*SPR'
				  GROUP BY CSDTSP, CSCITI
				";
	
		$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
	
			while ($row = db2_fetch_assoc($stmt)) {
			$d = $row["CSDTSP"];
			$i = $row["CSCITI"];
			if (!isset($ar["{$d}"]))
				$ar["{$d}"] = array();
				if (!isset($ar["{$d}"]["{$i}"]))
					$ar["{$d}"]["{$i}"] = array(
							"VOL" => number_format($row['SUM_CSVOLD'],0,',','.'),
							"PAL" => number_format($row['SUM_CSBAND'],0,',','.')
			);
		}
	
			return $ar;
		}
	
	
		
	
		
		function get_spedizione($id_spedizione){
			global $conn;
			global $cfg_mod_DeskAcq;
			$ar = array();
			$sql = "SELECT *
			FROM {$cfg_mod_DeskAcq['file_calendario']}
			WHERE CSCALE = '*SPR'
			AND CSPROG = '$id_spedizione'
			";
		
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
		
			$row = db2_fetch_assoc($stmt);
			return $row;
		}

		public function get_titolo_panel_by_tipo($tipo){
			return ucfirst(strtolower($tipo));
		}
		

		function get_carico_td($row){
			$c = $this->get_carico($row['TDTPCA'], $row['TDAACA'], $row['TDNRCA'], $row['TDDT']);
			return $c;
		}
		
		function get_carico($tp, $aa, $nr, $dt = '1 '){
			//TODO: i carici non eistono			
			return array();
		}		
		
		function k_carico_td($row){
			return implode("_", array((int)$row['TDAACA'], $row['TDTPCA'], (int)$row['TDNRCA']));
		}

		function k_carico_td_encode($ar_td){
			if (strlen($ar_td['TDAACA'])==0) $ar_td['TDAACA'] = 0;
			if (strlen($ar_td['TDTPCA'])==0) $ar_td['TDTPCA'] = "     ";
			if (strlen($ar_td['TDNRCA'])==0) $ar_td['TDNRCA'] = 0;
			return implode("_", array($ar_td['TDAACA'], $ar_td['TDTPCA'], $ar_td['TDNRCA']));
		}		
		
		
		function k_ordine_td_decode($ordine){
			$ar = explode("_", $ordine);
			$r = array();
			$r['TDDT'] 		= $ar[0];
			$r['TDOTID'] 	= $ar[1];
			$r['TDOINU'] 	= $ar[2];
			$r['TDOADO'] 	= $ar[3];
			$r['TDONDO'] 	= $ar[4];			
			$r['TDPROG'] 	= $ar[5];			
			
			return $r;
		}		
		
		function k_ordine_td_decode_xx($ordine){
		    
		    $r = $this->k_ordine_td_decode($ordine);
		    
		    /* GESTIONE MULTI DITTA */
		    $exp_ord = explode("-", $r['TDONDO']);
		    if (sizeof($exp_ord) == 2){ //sono nel formato NUMERO-DITTA
		        $r['TDONDO'] 	= $exp_ord[0];
		        $r['TDDT']		= $exp_ord[1];
		    }
		    
		    return $r;
		}	
		
		function k_ordine_td($row){
			return implode("_", array($row['TDDT'], $row['TDOTID'], $row['TDOINU'], $row['TDOADO'], $row['TDONDO']));
		}
		
		
		private function scrivi_rif_destinazione($loc, $naz, $prov, $naz_des)
		{
			if ($naz=="ITA" || substr($naz, 0,2)=="IT" || strlen(trim($naz))==0)
				return trim($loc) . " ($prov)";
			else
				return trim($loc) . " ($naz_des)";
		}
		
		private function get_tp_pri($pri, $row){
			if ($row['TDOPUN'] == 'N') //ordine puntuale
				return 6;
		
			if ($row['TDFN07'] == 1 || $row['TDFN08'] == 1)
				return 4;
			 
			return 0;
		}
		
		

		public function ha_con_ordini_reso($tddt, $tdccon, $tdcdes){
			//TODO
		 	return false;
		}		
		
		
		public function has_commento_ordine_by_k_ordine($k_ord, $bl = null, $tpno = null){
		    global $conn,$id_ditta_default;
			global $cfg_mod_DeskAcq;  	
			
			global $backend_ERP;
			if ($backend_ERP == 'GL') return false;			
			
			$where = "";
			if (is_null($k_ord) || strlen(trim($k_ord)) == 0)
			    return false;
			    
		    if(isset($bl) && $bl != '')
		        $where .= " AND RLRIFE2 = '{$bl}'";
	        else
	            $where .= "";
			            
			            
            if(isset($tpno) && $tpno != '')
                $where .= " AND RLTPNO = '{$tpno}'";
            else
                $where .= " AND RLTPNO = 'TD'";
			                    
		            
            $sql = "SELECT count(*) AS NR
            FROM {$cfg_mod_DeskAcq['file_note_anag']}
            WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$k_ord}'
            {$where}";
        
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            echo db2_stmt_errormsg($stmt);
            $row = db2_fetch_assoc($stmt);
            
            if ($row['NR'] > 0) return TRUE;
            else return FALSE;
						
		}

		
		private function get_iconCls($liv, $r){
			if ($liv == 1)
				return "iconSpedizione";			
			if ($liv == 2 && $r['fl_cli_bloc']==1)
				return "iconBlocco";
			elseif ($liv == 3 && $r["TDFN11"]==1) //evaso
			return "iconEvaso";
			elseif ($liv == 3 && $r["TDSWSP"]=='N') //non programmabile
			return "iconNonProgrammabile";
			elseif ($liv == 3 && $r["fl_cons_conf"]!=0)
			return "iconConf";
			elseif ($liv == 1 && trim($r['PSSTAT']) != '')
			return "carico-folder-" . trim($r['PSSTAT']);
			elseif ( ($liv == 1 || $liv == 2) && $r['T_ROW'] == $r['T_TDFN11'])
			return "iconEvaso";
			elseif ( ($liv == 1 || $liv == 2) && $r['T_ROW'] == $r['T_TDFN06'])
			return "iconConf";
			else
				return "task-folder";
		}		

		public function get_cronologia_ordine($k_ord){
			global $conn, $cfg_mod_DeskAcq, $cfg_mod_Admin;
		
			$oe = $this->k_ordine_td_decode($k_ord);
			$sql = "SELECT *
			FROM {$cfg_mod_DeskAcq['file_cronologia_ordine']}
					LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']}
							ON PQDT = TADT AND TAID = 'QUPR' AND PQFUNZ = TANR
							WHERE PQDT= ? AND PQTIDO = ?
							AND PQINUM = ? AND PQAADO = ? AND PQNRDO = ?
							ORDER BY PQDTGE, PQHMGE ";
		
							$stmt = db2_prepare($conn, $sql);
							$result = db2_execute($stmt, array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']));
							//$result =acs_execute($stmt, array());
			if (!$result){
			echo "bbbbbbbbb";
			echo db2_stmt_error($stmt);
			echo db2_stmt_errormsg($stmt);
			}
			 
			return $stmt;
			}
		

			
			public function get_ordine_by_k_docu($k_docu){
				global $conn;
				global $cfg_mod_DeskAcq;
			
				$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_testate']}	WHERE TDDOCU = ? AND " . $this->get_where_std();

				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt, array($k_docu));
				return db2_fetch_assoc($stmt);
			}			
			
			
			public function get_ordine_by_k_ordine($k_ord){
				global $conn, $cfg_mod_DeskAcq;
			
				$oe = $this->k_ordine_td_decode($k_ord);
				$sql = "SELECT *
						FROM {$cfg_mod_DeskAcq['file_testate']}
						WHERE TDDT=" . sql_t($oe['TDDT']) . " AND TDOTID=" . sql_t($oe['TDOTID'])  . "
						AND TDOINU=" . sql_t($oe['TDOINU']) . " AND TDOADO=" . sql_t($oe['TDOADO'])  . " AND TDONDO=" . sql_t($oe['TDONDO'])  . " ";
			
						$stmt = db2_prepare($conn, $sql);
						$result = db2_execute($stmt);
						return db2_fetch_assoc($stmt);
			}			

			
			function options_select_divisioni(){
				global $conn;
				global $cfg_mod_DeskAcq;
				$ar = array();
				$sql = "SELECT TDCDIV, TDDDIV FROM {$cfg_mod_DeskAcq['file_testate']} GROUP BY TDCDIV, TDDDIV ";
			
				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt);
			
				$ret = array();
				while ($row = db2_fetch_assoc($stmt)) {
						$ret[] = array( "id" 	=> $row['TDCDIV'],
										"text" 	=> $row['TDDDIV'] );
 				}
								return $ret;
			}
				
			
			function get_options_stati_ordine(){
				global $conn;
				global $cfg_mod_DeskAcq;
				$ar = array();
				$sql = "SELECT DISTINCT TDSTAT, TDDSST FROM {$cfg_mod_DeskAcq['file_testate']} ORDER BY TDDSST";
			
				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt);
				while ($row = db2_fetch_assoc($stmt))
					$ar[] = array("id"=>$row['TDSTAT'], "text" => "[" . $row['TDSTAT'] . "] " . $row['TDDSST']);
				return $ar;
			}		


			function get_options_priorita(){
				global $conn;
				global $cfg_mod_DeskAcq;
				$ar = array();
				$sql = "SELECT DISTINCT TDOPRI, TDDPRI FROM {$cfg_mod_DeskAcq['file_testate']} ORDER BY TDDPRI";
			
				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt);
				while ($row = db2_fetch_assoc($stmt))
					$ar[] = array("id"=>$row['TDOPRI'], "text" => $row['TDDPRI']);
				return $ar;
			}			

			
			

/***********************************************************
 * ANAGRAFICHE CLIENTI/FORNITORI
 ************************************************************/
			function get_el_fornitori_anag($p){
				global $conn, $id_ditta_default;
				global $cfg_mod_Admin;
				global $backend_ERP;
				$ar = array();
				
				
				if ($backend_ERP == 'GL'){
				    /* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
				    $sql = "SELECT distinct BACLF0, BARAG0, BAIND0, BALOC0, BAPRO0, BANAZ0, BATEL0, BAFAX0
                			FROM {$cfg_mod_Admin['file_anag_cli']} {$join_riservatezza}
                			WHERE (UPPER(
                				REPLACE(REPLACE(BARAG0, '.', ''), ' ', '')
                			) " . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
                				OR BACLF0 LIKE '%".$p['query']."%')
                                AND BATCF0 = '2'
                				AND BAQLA0 <> '98'
                 			    order by BARAG0
                 				";
				    
				    $stmt = db2_prepare($conn, $sql);
				    echo db2_stmt_errormsg();
				    $result = db2_execute($stmt);
				    
				    while ($row = db2_fetch_assoc($stmt)) {
				        if (trim($row['BANAZ0']) == 'ITA' || trim($row['BANAZ0']) == 'IT' || trim($row['BANAZ0']) == ''){
				            $out_loc = acs_u8e(trim($row['BALOC0']) . " (" . trim($row['BAPRO0']) . ")");
				        } else {
				            $des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
				            $out_loc = acs_u8e(trim($row['BALOC0']) . ", " . trim($des_naz['text']));
				        }
				        
				        $ret[] = array( "cod" 		=> $row['BACLF0'],
				            "descr" 	=> acs_u8e(trim($row['BARAG0'])),
				            "out_loc"	=> $out_loc,
				            "out_ind"	=> acs_u8e(trim($row['BAIND0'])),
				            "out_ema"	=> "",
				            "out_tel"	=> acs_u8e(trim($row['BATEL0']) ." / ".trim($row['BAFAX0']) )
				        );
				    }
				    
				    
				} else {
				    //Sv2
				    $sql = "SELECT CFCD, CFRGS1, CFRGS2 FROM {$cfg_mod_Admin['file_anag_cli']}
						WHERE UPPER(CONCAT(CFRGS1, CFRGS2) ) LIKE '%" . strtoupper($p['query']) . "%'
 						AND CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = 'F' AND CFFLG3 = ''
 						";
				    
				    $stmt = db2_prepare($conn, $sql);
				    $result = db2_execute($stmt);
				    
				    $ret = array();
				    while ($row = db2_fetch_assoc($stmt)) {
				        $ret[] = array( "cod" 		=> $row['CFCD'],
				                        "descr" 	=> acs_u8e(trim($row['CFRGS1']) . " " . trim($row['CFRGS2'])) );
				    }
				    
				}							  		
			  			
			  	return $ret;
			}
			
				
			function get_el_fornitori_des_anag($p){
				global $conn, $id_ditta_default;
				global $cfg_mod_Admin;
				$ar = array();
				$ret = array();
				
				
				if (strlen($p['cliente_cod']) == 0) return $ret;
				
	 				$sql = "SELECT TADT, TACOR1, TANR, TADESC, TADES2, TAREST FROM {$cfg_mod_Admin['file_anag_cli_des']}
				 	WHERE UPPER(TAREST) LIKE '%" . strtoupper($p['query']) . "%'
				 	AND TADT = " . sql_t($id_ditta_default) . " AND TAID='VUDE' AND TACOR1 = " . sql_t(sprintf("%09s", $p['cliente_cod'])) . "
				 	AND TATP <>'S' AND TANR <> '=AG'
	 				";
				
				
		   			$stmt = db2_prepare($conn, $sql);
					$result = db2_execute($stmt);
					
					while ($row = db2_fetch_assoc($stmt)) {
					$ret[] = array( "cod" 		=> $row['TANR'],
							"cod_cli"	=> $row['TACOR1'],
							"dt"		=> $row['TADT'],				
					
							"descr" 	=> acs_u8e(
							trim(substr($row['TAREST'], 90, 30)) . " " .
							trim(substr($row['TAREST'], 150, 10)) . " " .
									trim(substr($row['TAREST'], 160, 2)) . " "
							),
							"denom"  => acs_u8e(trim($row['TADESC'] . $row['TADES2'] )),
					
							//Decodifico i vari campi indirizzo
							'IND_D' => acs_u8e(substr($row['TAREST'],  30,  60)),
							'LOC_D' => acs_u8e(substr($row['TAREST'],  90,  60)),
							'CAP_D' => substr($row['TAREST'], 150,  10),
							'PRO_D' => substr($row['TAREST'], 160,   2),
							'TEL_D' => substr($row['TAREST'], 173,  20)
					
						);
					}
					return $ret;
		 }
				 	
			
			

		 function get_rows_ordine_by_num_dtep($ord, $dtep, $ar_blocchi = null, $nrec = null){
		 	global $conn;
		 	global $cfg_mod_DeskAcq, $cfg_mod_Spedizioni, $cfg_mod_Admin;
		 	global $backend_ERP;
		 	
		 	$oe = $this->k_ordine_td_decode_xx($ord);
		 	$ar = array();

		 	switch ($backend_ERP){
		 	    
		 	    case 'GL':		 	        
		 	        $sql = "SELECT 
                            RD.MECAR0 AS RDART,
                            MADES0    AS RDDART,
                            MEUNM0    AS RDUM,
                            MERID0    AS RDRIGA,
                            MEQTA0    AS RDQTA, MEQTS0 AS RDQTE, MERID0 AS RDRIGA
    					FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
    					LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} ART
    						ON RD.MECAR0 = ART.MACAR0
    					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_art_des_aggiuntive']} ART_DES_AGG
    						 ON RD.MECAD0 = ART_DES_AGG.MHCAD0
    						AND RD.METND0 = ART_DES_AGG.MHTND0
    						AND RD.MEAND0 = ART_DES_AGG.MHAND0
    						AND RD.MENUD0 = ART_DES_AGG.MHNUD0
    						AND RD.MERID0 = ART_DES_AGG.MHRID0
    					WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
    		 	        
		 	    break;
		 	    
		 	    default: //SV2		 	        

		 	      $sql = "SELECT RRN(RD) AS RRN, RD.*, RT.*, ANAG_ART.ARSOSP, ANAG_ART.ARESAU, ANAG_ART.ARFOR1
        				 	FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
        		 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
        		 	          ON RD.RDDT = ANAG_ART.ARDT AND RD.RDART = ANAG_ART.ARART
        		 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
        					  ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
         					WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
         					AND RD.RDTISR = '' AND RD.RDSRIG = 0";
		 	        
		 	        if (isset($nrec))
		 	            $sql .= " AND RDNREC={$nrec}";
		 	            
		 	} //switch $backend_ERP
		 	
		 	
		 	$stmt = db2_prepare($conn, $sql);
		 	echo db2_stmt_errormsg();

		 	$result = db2_execute($stmt, array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']));		 					 			
		 	echo db2_stmt_errormsg($stmt);
		 	return $stmt;
		 }
		 	

		 

		 public function get_stato_aggiornamento(){
		 	global $conn, $cfg_mod_DeskAcq, $id_ditta_default;
		 
		 	$ret = array();
		 	$ret['success'] = true;
		 
		 	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_tabelle']} WHERE TADT=? AND TATAID=?";
		 	$stmt = db2_prepare($conn, $sql);
		 	$result = db2_execute($stmt, array($id_ditta_default, 'IELA'));
		 	$row = db2_fetch_assoc($stmt);
		 
		 	if ($row != false){
		 		if ($row['TAFG01'] == 'F'){
		 			$image_name = "icone/48x48/rss_blue.png";
		 			$class_name = 'st-aggiornamento-ok';
		 			$img_str = "";
		 		} else {
		 			$image_name = "under-construction_q_48.png";
		 			$class_name = 'st-aggiornamento-warning';
		 			$img_str = "<img class='st-aggiornamento' src=" . img_path($image_name) . " height=20>";
		 		}


 				$ret['html'] = "{$img_str}<span class='st-aggiornamento {$class_name}'>" . print_date($row['TADTGE']) . " " . print_ora($row['TAORGE']) . "</span>";
		 	}
		 	else $ret['html'] = '-';
		 
		 	return $ret;
		 }
		 	

		 public function get_orari_aggiornamento(){
		 	global $conn, $cfg_mod_DeskAcq, $id_ditta_default;
		 		
		 		
		 	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_tabelle']} WHERE TADT=? AND TATAID=?";
		 	$stmt = db2_prepare($conn, $sql);
		 	$result = db2_execute($stmt, array($id_ditta_default, 'IELA'));
		 	$row = db2_fetch_assoc($stmt);
		 		
		 	if ($row != false){		 		 
		 		return $row['TADESC'];
		 	}
		 	else return '-';
		 }
		 	
		 
		 
		 function get_options_area_spedizione(){
		 	global $conn;
		 	global $cfg_mod_DeskAcq;
		 	$ar = array();
		 	$sql = "SELECT TAKEY1, TADESC FROM {$cfg_mod_DeskAcq['file_tabelle']} WHERE TATAID = 'ASPE'	ORDER BY TADESC";
		 
		 	$stmt = db2_prepare($conn, $sql);
		 	$result = db2_execute($stmt);
		 	$ar[] = array(null, '- Tutte le aree -');
			while ($row = db2_fetch_assoc($stmt))
		 		$ar[] = array($row['TAKEY1'], $row['TADESC']);
		 				return $ar;
		 }
		 
		 
		 function get_options_tipologia_ordini(){
		 	global $conn;
		 	global $cfg_mod_DeskAcq;
		 	$ar = array();
		 	$sql = "SELECT DISTINCT TDCLOR FROM {$cfg_mod_DeskAcq['file_testate']} ORDER BY TDCLOR";
		 
		 	$stmt = db2_prepare($conn, $sql);
		 	$result = db2_execute($stmt);
		 	$ar = array();
		 	while ($row = db2_fetch_assoc($stmt))
		 		$ar[] = array($row['TDCLOR'], $row['TDCLOR']);
 			return $ar;
		 }
		 	
		 
		 
		 public function get_table_TA(){
		 	global $cfg_mod_DeskAcq;
		 	return $cfg_mod_DeskAcq['file_tabelle'];
		 }
		 
		 
		 

		 

		 	function add_where_num_carico($num){
		 		return " TDELCA LIKE '%{$num};%'";
		 	}
		 	function add_where_num_lotto($num){
		 		return " TDELLO LIKE '%{$num};%'";
		 	}		 	
		 	
		 

		 	function des_vmc_sp($row){
		 		$vet = $this->decod_std('AUTR', $row['CSCVET']);
		 		$mez = $this->decod_std('AUTO', $row['CSCAUT']);
		 		$cas = $this->decod_std('AUTO', $row['CSCCON']);
		 		return implode(" ", array($vet, $mez, $cas));
		 	}

		 	
		 	/******************************************************************
		 	 * CONTRATTI
		 	 *******************************************************************/
		 	function fornitore_ha_contratti($ditta, $fornitore){
		 		global $conn;
		 		global $cfg_mod_DeskAcq;

		 		$sql = "SELECT count(*) AS NR FROM {$cfg_mod_DeskAcq['file_testate']}
		 					WHERE TDSWPP = 'P' AND TDDT= " . sql_t($ditta) . " AND TDCCON LIKE '{$fornitore}%'";

		 		$stmt = db2_prepare($conn, $sql);
		 		$result = db2_execute($stmt);
		 		$row = db2_fetch_assoc($stmt);
		 			
		 		if ($row['NR'] > 0) return 1;
		 		else return 0;		 		
		 	}

		 	
		 	function get_contratti_for($ditta, $fornitore){
		 		global $conn;
		 		global $cfg_mod_DeskAcq;
		 		 
		 		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_commenti_ordine']} RD
		 					WHERE RDTPNO='CONTR' AND RDDT= " .sql_t($ditta) . " AND RDOTID='AO' AND RDOINU='' AND RDOADO=0 AND RDONDO = '{$fornitore}'";

		 		$stmt = db2_prepare($conn, $sql);
		 		$result = db2_execute($stmt);		 				 		
		 		return $stmt;
		 	}
		 

		 	
		 	
		 	
		 	function exe_sincronizza_spedizione($p){
		 		global $conn;
		 		global $cfg_mod_DeskAcq;
		 	
		 		$sped = $this->get_spedizione($p->sped_id);
		 	
		 		//Recupero l'elenco degli ordini e aggiorno i dati in base alla spedizione
		 		$sql = "SELECT TDDOCU FROM {$cfg_mod_DeskAcq['file_testate']} WHERE " . $this->get_where_std() . " AND TDNBOC = ?";
		 	
		 		$stmt = db2_prepare($conn, $sql);
		 		$result = db2_execute($stmt, array($p->sped_id));
		 	
		 		while ($row = db2_fetch_assoc($stmt)) {
		 	
		 			$ord = $this->get_ordine_by_k_docu($row['TDDOCU']);
		 	
		 			//HISTORY
		 			$sh = new SpedHistory($this);
		 			$sh->crea(
		 					'sincronizza_spedizione',
		 					array(
		 							"k_ordine" 	=> $row['TDDOCU'],
		 							"sped"		=> $sped
		 					)
		 			);
		 		}
		 			 	
		 		return $result;
		 	}
		 	
		 	
		 	function exe_sincronizza_generica($p){
		 		global $conn;
		 		global $cfg_mod_DeskAcq;
		 	
		 			 				 		
		 		//Recupero l'elenco degli ordini e aggiorno i dati in base alla spedizione
		 		$filtro = array();
		 		$filtro['TDSWSP'] = 'Y';
		 		switch ($p->extraParams->from_type){
		 			case 'INDICI':
		 				$filtro["data_spedizione"] 		= $p->extraParams->data;
		 				$filtro["a_data_spedizione"] 	= $p->extraParams->data;
		 				$filtro["cliente"] 	= $p->extraParams->cliente;		 				
		 				break;
		 			default:
		 				//da plan
		 				if (substr($p->extraParams->col_name, 0, 2)=="d_")
		 					{ //ho selezionato un giorno
		 					 $add_day = (int)substr($p->extraParams->col_name, 2, 2) - 1;
		 					 $m_data = date('Ymd', strtotime($p->extraParams->da_data . " +{$add_day} days"));
		 					 $m_data_to = $m_data; //solo un giorno
		 				    }
		 						 				
		 				$filtro["data_spedizione"] 		= $m_data;
		 				$filtro["a_data_spedizione"] 	= $m_data_to;
		 				$filtro["itinerario"] 			= $p->extraParams->itin;
		 				if (isset($p->extraParams->cliente))
		 					$filtro["cliente"] = $p->extraParams->cliente;
		 				break;	
		 		}
		 		
		 		$stmt = $this->get_elenco_ordini($filtro);
		 	
		 		while ($row = db2_fetch_assoc($stmt)) {
		 			$ord = $this->get_ordine_by_k_docu($row['TDDOCU']);
		 	
		 			//HISTORY
		 			$sh = new SpedHistory($this);
		 			$sh->crea(
		 					'sincronizza_spedizione',
		 					array(
		 							"k_ordine" 	=> $row['TDDOCU'],
		 							"sped"		=> $sped
		 					)
		 			);
		 		}
		 	
		 		return $result;
		 	}		 	
		 	
		 			 	

		 	
		 	function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null){
		 		global $conn;
		 		global $cfg_mod_DeskAcq, $id_ditta_default;
		 		$ar = array();
		 		$sql = "SELECT *
		 		FROM {$cfg_mod_DeskAcq['file_tabelle']}
		 		WHERE TADT='$id_ditta_default' AND TATAID = '$tab'";
		 		
		 		if (isset($k1))
		 		$sql .= " AND TAKEY1 = '$k1' ";
		 	
		 		if (isset($k2))
		 		$sql .= " AND TAKEY2 = '$k2' ";
		 	
		 		$sql .= "  ORDER BY UPPER(TADESC)";
		 	
				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
		 		$result = db2_execute($stmt);
		 	
		 		$ret = array();
		 		while ($row = db2_fetch_assoc($stmt)) {
		 		if (is_null($k1) || $is_get == 'Y') $m_id = $row['TAKEY1'];
		 		else $m_id = $row['TAKEY2'];
		 			
		 		$r = array();
		
		 		$r['id'] 		= $m_id;
		 		$r['text']		= $row['TADESC'];
		 			
		 		if ($esporta_esteso == 'Y'){
		 		   $r['TAKEY1'] = $row['TAKEY1'];
		 		   $r['TAKEY2'] = $row['TAKEY2'];
 				   $r['TAKEY3'] = $row['TAKEY3'];
		 		   $r['TAKEY4'] = $row['TAKEY4'];
		 		   $r['TAKEY5'] = $row['TAKEY5'];
				   $r['TARIF2'] = $row['TARIF2'];
				   $r['TAFG01'] = $row['TAFG01'];
				   $r['TAPESO'] = (int)$row['TAPESO'];
				   $r['TASTAL'] = $row['TASTAL'];
		 		}
		 	
		 		//		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
		 		$ret[] = $r;
		 	}
		 		
		 	return $ret;
		 	}
		 	
	 	function find_sys_TA($tab, $p=array()){
		 	    global $conn, $id_ditta_default;
		 	    $ar = array();
		 	    $cfg_mod = $this->get_cfg_mod();
		 	    
		 	    //in join prendo il mezzo, per recuperare la targa (da impostare in automatico nella spedizione)
		 	    $sql = "SELECT * FROM {$cfg_mod['file_tab_sys']} TA
		 	    WHERE TAID = ? AND TADT = ?";
		 	    
		 	    if ($tab == 'CUAG' && $p['tipo'] == 'area_manager')
		 	        $sql .= " AND SUBSTRING(TA.TAREST, 196, 1) = 'M' ";
		 	        
	 	        if ($tab == 'CUAG' && $p['cli_cc'] == 'Y')
	 	            $sql .= " AND SUBSTRING(TA.TAREST, 196, 1) IN ('', 'A', 'P') ";
	 	            
	 	            $sql .= " ORDER BY TATP, TADESC";
		 	            
 	            $stmt = db2_prepare($conn, $sql);
 	            echo db2_stmt_errormsg();
 	            $result = db2_execute($stmt, array($tab, $id_ditta_default));
		 	            
 	            $ret = array();
 	            while ($row = db2_fetch_assoc($stmt)) {
		 	                
 	                $sospeso = false;
 	                $color = "";
 	                if($p['i_sosp'] == 'Y' && trim($row['TATP']) == 'S'){
 	                    $sospeso = true;
 	                    $color = "#F9BFC1";
 	                    
 	                }
            
 	                if($p['des2'] == 'Y')
 	                    $desc = trim($row['TADESC']).' '.trim($row['TADES2']);
 	                    else
 	                        $desc = trim($row['TADESC']);
 	                        
 	                        $nr = array(
 	                            "id" 		=> $row['TANR'],
 	                            "text" 		=> "[". trim($row['TANR']) ."] ".$desc,
 	                            "sosp"    => $sospeso,
 	                            "color"   => $color
 	                        );
 	                        
 	                        if ($tab == 'CUAG'){
 	                            //Recupero perc. provv. agente
 	                            $nr['perc_provv'] 	= (float)(substr($row['TAREST'], 171, 2) . "." . substr($row['TAREST'], 173, 2));
 	                            $nr['area_manager'] = substr($row['TAREST'], 235, 3);
 	                        }
 	                        
 	                        $ret[] = $nr;
 	                        
 	            }
		 	            
		 	            
		 	            return $ret;
		 	}
		 	

		 	function get_elenco_porte_itinerario($itin_id, $area_id = null){
		 		$aspe = new DeskAcqAreeSpedizione();
		 		$ret = array();		 		
		 				 		
		 		//dall'itinerario recupero l'area di spedizione -> e lo stabilimento		 		
		 		if (!isset($area_id)){
		 			$aspe->load_rec_data_by_itin($itin_id);
		 		} else {
		 			$aspe->load_rec_data_by_k(array("TAKEY1"=>$area_id));		 			
		 		}
		 				
		 		
		 		if (isset($aspe->rec_data['TARIF2']) && !is_null($aspe->rec_data['TARIF2']) && strlen(trim($aspe->rec_data['TARIF2'])) > 0){
		 			$stab = new DeskAcqStabilimenti();
		 			$stab->load_rec_data_by_k(array('TAKEY1' => $aspe->rec_data['TARIF2']));		 				
		 		} else {
		 			return $ret;
		 		}
		 	
		 		$el_porte = explode(",", $stab->rec_data['TAMAIL']);
		 		foreach ($el_porte as $porta){
		 			$ret[] = array("id" => trim($porta), "text" => trim($porta));
		 		}
		 		return $ret;
		 	}		 	
		 	
		 	
		 	function get_elenco_porte_stabilimento($cod){
		 	
		 		$stab = new DeskAcqStabilimenti();
		 		$stab->load_rec_data_by_k(array('TAKEY1' => $cod));
		 	
		 		$el_porte = explode(",", $stab->rec_data['TAMAIL']);
		 		$ret = array();
		 		foreach ($el_porte as $porta){
		 			$ret[] = array("id" => trim($porta), "text" => trim($porta));
		 		}
		 		return $ret;
		 	}
		 			 	
		 	


		 	function get_TA_std($tab, $k1, $k2 = null){
		 		global $conn;
		 		global $cfg_mod_DeskAcq;
		 		$ar = array();
		 		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_tabelle']} WHERE TATAID = '$tab' ";
		 	
		 		if (isset($k1))	$sql .= " AND TAKEY1 = '$k1' ";
		 		if (isset($k2))	$sql .= " AND TAKEY2 = '$k2' ";
		 	
		 		$stmt = db2_prepare($conn, $sql);
		 		$result = db2_execute($stmt);
		 	
		 		$row = db2_fetch_assoc($stmt);
		 		if (!$row) $row = array();
		 	
		 		return $row;
		 	}		 	
		 	
		 	
		 	
		 	function exe_avanzamento_segnalazione_arrivi($r){
		 	    global $conn;
		 	    global $cfg_mod_DeskAcq;
		 	    
		 	    //bug parametri linux
		 	    global $is_linux;
		 	    if ($is_linux == 'Y')
		 	        $r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
		 	   
		 	    $ret = array();
		 	    
		 	    $list_selected_id = json_decode($r['list_selected_id']);
		 	    
		 	    //per ogni riga selezionata
		 	    foreach($list_selected_id as $krow_selected => $row_selected){
		 	        
		 	        //$row_selected = $list_selected_id[0];
		 	        
		 	        //recupero causale/utente/ordine da usare come chiave per AS0
		 	        $m_k_exp = explode("|", $row_selected->id);
		 	        //aggiorno la riga in WPI1AS0
		 	        $na = new SpedAssegnazioneOrdini($this);
		 	        $na->avanza($r['f_entry_prog_causale'], array(
		 	            'prog' => $row_selected->prog,
		 	            'form_values' => $r
		 	        ));
		 	        
		 	    }
		 	    
		 	    $ret['success'] = true;
		 	    return acs_je($ret);
		 	}
		 	
		 	
		 	
		 	
			/***********************************************
			 * Recupero elenco spedizioni (per assegna spedizione)
			 ***********************************************/
		 	function get_el_spedizioni($r){
		 		
		 		global $conn;
		 		global $cfg_mod_DeskAcq;
		 	
		 		$properties = array();
		 	
		 		$list_selected_id = json_decode($r['list_selected_id']);
		 	
		 		//recuper l'elenco degli ordini interessati
		 		$ar_ord = array();
		 		$el_ord = array();
		 	
		 		foreach($list_selected_id as $obj_row){
		 	
		 			if ($r['from'] == 'BACKGROUND_MRP'){
		 				$el_ord[] = $obj_row->k_ordine;
		 				$oe = explode('_', $obj_row->k_ordine);
		 				$ar_ord[] = $oe[4];
		 			} else {
		 			
				 			if ($obj_row->liv == 'liv_3') { //singolo ordine
				 				$oer = $obj_row->k_ordine;
				 				$oe = explode('_', $oer);
				 	
				 				if (!in_array($obj_row->k_ordine, $el_ord)){
				 					$el_ord[] = $obj_row->k_ordine;
				 					$ar_ord[] = $oe[4];
				 				}
				 			}
				 	
				 			if ($obj_row->liv == 'liv_2') { //a livello di cliente
				 	
				 				//recupero gli ordini del livello selezionato
				 				$stmt = $this->get_stmt_ordini_by_liv2_plan($obj_row);
				 	
				 				while ( $row = db2_fetch_assoc($stmt) ){
				 					$k_ord = $this->k_ordine_td($row);
				 					//per ogni ordine
				 					if (!in_array($k_ord, $el_ord)){
				 						$el_ord[] = $k_ord;
				 						$ar_ord[] = $row['TDONDO'];
				 					}
				 				}		 	
				 	
				 			}		
		 			}		 				
		 				
		 		}
		 	
		 	
		 		if (count($ar_ord) > 0)
		 			$sql_where_tdondo .= " AND TDONDO IN (" . sql_t_IN($ar_ord) . ")";
		 		else $sql_where_tdondo = " AND 1=2"; //non devo selezionare nessun ordine
		 	
		 	
		 		//in base alle'area di spedizione (se indicata) imposto la minima data disponibile di assegnazione
		 		$itin_attuale_cod = $r['cod_iti'];
		 		$itin = $this->get_TA_std('ITIN', $itin_attuale_cod);
		 		$area = $this->get_TA_std('ASPE', $itin['TAASPE']);
		 		if ((int)$area['TARIF1'] > 0)
		 			$properties['area_min_dispo'] = (int)$area['TARIF1'];
		 		else
		 			$properties['area_min_dispo'] = 0;
		 	
		 		//calcolo la data disponibilita' piu' alta
		 		$sql = "SELECT max(TDDTDS) as MAX_DISPO,
		 					SUM(TDVOLU) as SUM_VOL,
		 					SUM(TDPLOR) as SUM_PES,
		 					SUM(TDBANC) as SUM_PAL,
		 					MIN(TDDTEP) AS TDDTEP
		 					FROM {$cfg_mod_DeskAcq['file_testate']}
		 					WHERE 1=1 {$sql_where_tdondo} ";
		 	
		 		$stmt = db2_prepare($conn, $sql);
		 		$result = db2_execute($stmt);
		 		$row = db2_fetch_assoc($stmt);
		 		$oggi_AS = oggi_AS_date();
		 		$max_dispo = max($row['MAX_DISPO'], $oggi_AS);
		 		$properties['max_dispo'] = $max_dispo;
		 		$properties['sum_vol'] = $row['SUM_VOL'];
		 		$properties['sum_pes'] = $row['SUM_PES'];
		 		$properties['sum_pal'] = $row['SUM_PAL'];
		 		$properties['el_ordini'] = $el_ord;
				$properties['num_ordini'] = count($ar_ord);
		 		$properties['TDDTEP'] = $row['TDDTEP'];
		 	
		 			//parametri del modulo (non legati al profilo)
		 			$mod_js_parameters = $this->get_mod_parameters();
		 	
		 			if ($r['solo_scadute'] == 'Y')
		 				$r['da_oggi'] = 'SC';
		 				
		 			$stmt = $this->get_spedizioni_per_assegnazione($r['cod_iti'], $r['da_oggi'], $ar_ord, $r['cod_fornitore'], $r['cod_art']);
		 	
					$ar = array();
		 			while ($row = db2_fetch_assoc($stmt)) {
		 				
		
		 					if (strlen(trim($row['CSTARG'])) > 0)
		 			  			$accoda_targa = " (T: " . trim($row['CSTARG']) . ")";
		 					else
		 						$accoda_targa = '';
		 							
		 						$ar[] = array( "CSPROG" 	=> $row['CSPROG'],
		 			 						"DATA"		=> $row['CSDTSP'],
		 			 						"DATA_PROG"	=> print_date($row['CSDTPG']),
		 			 						"ORA_PROG"	=> print_ora($row['CSHMPG']),
		 			 						"CSDESC"	=> acs_u8e($row['CSDESC']),
		 			 						"VETTORE" 	=> $vet,
		 			 						"MEZZO"		=> $mez,
		 			 						"CASSA"		=> $cas,
											"OUT_FORNITORE" => acs_u8e($this->decod_fornitore($r['cod_fornitore']) . " [{$r['cod_fornitore']}]"),
		 			 						//"carico"	=> $this->get_el_carichi_by_sped($row['CSPROG'], 'Y', 'Y'),
		 			 						//"carico_d"	=> $this->get_el_carichi_by_sped($row['CSPROG'], 'N', 'N'), //per la colonna
		 			 						"VOL_D"		=> n($row['CSVOLD'],1),
		 			 						"VOL_A"		=> n($row['ASS_VOL'],1),
 			 								"PES_D"		=> n($row['CSPESO'],1),
 			 								"PES_A"		=> n($row['ASS_PES'],1),
 			 								"PAL_D"		=> n($row['CSBAND'],0),
 			 								"PAL_A"		=> n($row['ASS_PAL'],0),
 			 								"S_RDQTA"	=> n($row['S_RDQTA'], 2),
 			 								"S_REF_ARTICOLO"	=> n($row['S_REF_ARTICOLO'], 0),
 	
 			 								"VOL_CARICABILE" => $row['CSVOLD'] - $row['ASS_VOL'],
 			 								"PES_CARICABILE" => $row['CSPESO'] - $row['ASS_PES'],
 			 								"PAL_CARICABILE" => $row['CSBAND'] - $row['ASS_PAL'],
 			 										 
 			 								"VOL_DA_CARICARE" => $properties['sum_vol'],
 			 								"PES_DA_CARICARE" => $properties['sum_pes'],
 			 								"PAL_DA_CARICARE" => $properties['sum_pal'],
 	
 			 								//"ESISTE_CLIENTE" => $this->verifica_cliente_in_sped($cli['TDCCON'], $cli['TDCDES'], $row['CSPROG'])
 			 								);
		 			 											
		 				}
		 	
		 				$ret = 	"{\"root\": " . acs_je($ar) . ",
		 				\"properties\": " . acs_je($properties) . "
 				}";
			 return $ret;
		 	}
		 	
		 	
		 	
		 	

		 	function get_spedizioni_per_assegnazione($itin = null, $da_oggi = null, $ordini_da_escludere = null, $cod_fornitore = null, $cod_art = null){
		 		global $conn;
		 		global $cfg_mod_DeskAcq;
		 		$ar = array();
		 	
		 		if (!isset($cod_art))
		 			$cod_art = '-__--___---';
		 		
		 		
		 		$g_field = "CSPROG, CSCVET, CSCAUT, CSCCON, CSDESC, CSDTSP, CSVOLD, CSBAND, CSPESO, CSSTSP, CSTARG ";
		 		$s_field = " SUM(TDVOLU) AS ASS_VOL, SUM(TDPLOR) AS ASS_PES, SUM(TDBANC) AS ASS_PAL, MAX(TDONDO) AS MAX_TDONDO, SUM(RDQTA) AS S_RDQTA,
		 				     SUM(CASE WHEN TRIM(RDART) = '{$cod_art}' THEN 1 ELSE 0 END) AS S_REF_ARTICOLO ";
		 		$o_field = " CSDTSP, CSCVET, CSCAUT, CSCCON ";
		 	
		 		$sql = "SELECT $g_field , $s_field
		 				FROM {$cfg_mod_DeskAcq['file_calendario']}
		 					LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_testate']} 
		 						ON TDNBOC = CSPROG AND " . $this->get_where_std() . "
		 							
							  LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest']}
								ON TDDT=RDDT AND TDOTID=RDTIDO AND TDOINU=RDINUM AND TDOADO=RDAADO AND TDONDO=RDNRDO		 							
		 							
		 				WHERE CSCALE = '*SPR'";
		 		
				if ($da_oggi == "Y")
		 			$sql .= " AND CSDTSP >= " . oggi_AS_date();
		 	
	 			if ($da_oggi == "SC") //solo scadute
		 			$sql .= " AND CSDTSP < " . oggi_AS_date();
		 	
	 			if (strlen(trim($itin)) > 0)
	 				$sql .= " AND CSCITI = " . sql_t($itin);
	 			
	 			if (strlen(trim($cod_fornitore)) > 0)
	 				$sql .= " AND CSCVET = " . sql_t($cod_fornitore);	 			
		 	
		 			//TODO: DOVRO' USARE LA CHIAVE DELL'ORDINE
		 		if (!is_null($ordini_da_escludere) && count($ordini_da_escludere) > 0)
		 		$sql .= " AND (TDONDO IS NULL OR TDONDO NOT IN (" . sql_t_IN($ordini_da_escludere) . ") )" ;
		 	
		 		$sql .= " GROUP BY $g_field";
		 	
		 		$sql .= " HAVING COUNT(TDONDO) > 0 OR CSSTSP NOT IN('GA', 'AN') ";
 				$sql .= " ORDER BY $o_field";

		 		$stmt = db2_prepare($conn, $sql);
		 		echo db2_stmt_errormsg();
		 		$result = db2_execute($stmt);
		 		return $stmt;
		 	}
		 			 	

		 	
		 	//sarebbe assegna_consegna
		 	function exe_assegna_spedizione($r){
		 		global $conn, $cfg_mod_DeskAcq;
		 	

		 		$list_selected_id = $r->list_selected_id;

		 		$data_progr_attuale = 0;		 		
		 		$k_articolo = '';
		 		
		 		foreach ($list_selected_id as $k => $k_ord){
			 			
		 				if ($r->from == 'BACKGROUND_MRP'){
		 					$to_sped_id = $r->to_sped_id;
		 					$sped = $this->get_spedizione($to_sped_id);		 							 					
							$oe = explode("_", $k_ord);
							$data_progr_attuale = $oe[5];
							$k_articolo 		= $oe[6];
							$to_date 		= $sped['CSDTSP'];		 					 
		 				} else if ($r->from == 'BACKGROUND_MRP_NUOVA_DATA'){
							$oe = explode("_", $k_ord);
							$data_progr_attuale = $oe[5];
							$k_articolo 		= $oe[6];
							$to_sped_id = 0;
							$to_date = $r->to_date;
							$sped = array('CSPROG' => 0, 'CSDTSP'=>$to_date);
									 					 
		 				} else {
		 					return false;
		 				}
		 																
						$sh = new SpedHistory($this);
		 				$sh->crea(
								'assegna_spedizione_desk_acq',
		 								array(
		 										"k_ordine" 	=> $k_ord,
		 										"data"		=> $to_date,
		 										"da_data"	=> $data_progr_attuale,
		 										"k_articolo"=> $k_articolo,
		 	
												"sped"		=> $sped,

 												"op"		=> "ASS",
 												"ex"		=> "(#{$ord['TDNBOC']})"
		 										)
 									);		 			
		 			
		 				if ($r->from == 'BACKGROUND_MRP'){
		 					//aggiorno il flag sul file
		 					$sql = "UPDATE {$cfg_mod_DeskAcq['file_WMAF30']}
		 							SET M3FU02 = 'X'
		 							WHERE M3DT = ? AND M3TIDO = ? AND M3INUM = ? AND M3AADO = ? AND M3NRDO = ?
		 							AND M3DTEP = ? AND M3ART = ?
		 							";
		 					$stmt = db2_prepare($conn, $sql);
		 					echo db2_stmt_errormsg();
		 					$result = db2_execute($stmt, $oe);
		 					
		 				}
		 				
		 				
		 			}
		 	
		 	
		 		return $result;
		 	 }
		 	
		 	
		 	
		 	
		 	
		 	
} //class

?>