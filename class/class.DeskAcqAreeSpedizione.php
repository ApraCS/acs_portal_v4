<?php

class DeskAcqAreeSpedizione extends AreeSpedizione {
 
	

	public function form_buttons_2() {
	
		$cl= new ListinoTrasportatori();
		$cl_itinerari = new TrasportatoriItinerari();
	
		return "
		{
			xtype: 'toolbar',
			dock: 'bottom',
			ui: 'footer',
			items: ['->']
		}";
	}

	

	public function out_Writer_Form_initComponent() {
		$ret =  "
        this.addEvents('create');
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
            title: 'Area spedizione',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},
            {
                fieldLabel: 'Tabella',
                name: 'TATAID',
                xtype: 'hidden'
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 3
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }, {
                fieldLabel: 'Programmazione area dopo il (aaaammdd)',
                name: 'TARIF1',
                allowBlank: true,
 				maxLength: 8
            }, {
				name: 'TARIF2',
				xtype: 'combo',
				fieldLabel: 'Stabilimento',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
			   	allowBlank: true,
 				forceSelection: true,
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [
					     " . acs_ar_to_select_json(find_TA_std('START', null, 1, null, null, null, $this), '') . "
					    ]
				}
			}],
            dockedItems: [" . self::form_buttons() . ", " . self::form_buttons_2() . "]
        });
        this.callParent();
";
		return $ret;
	}
	
	
	

	// TABELLE UTILIZZZATE DALLA CLASSE
	public function get_table($per_select = false){
		global $cfg_mod_DeskAcq;
		$ret = $cfg_mod_DeskAcq[$this->table_id];
		if ($per_select == "Y")
			$ret .= " PRI " . $this->get_table_joins();
		return $ret;
	}
	
	public function get_table_NR(){
		global $cfg_mod_DeskAcq;
		return $cfg_mod_DeskAcq['file_numeratori'];
	}
	
	public function get_table_TA(){
		global $cfg_mod_DeskAcq;
		return $cfg_mod_DeskAcq['file_tabelle'];
	}
	
}

?>