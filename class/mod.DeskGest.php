<?php

/**********************************************
 * DESKTOP UTILITY
 **********************************************/

class DeskGest {
	
	private $mod_cod = "DESK_GEST";
	private $mod_dir = "desk_gest";
	
	
	function __construct($parameters = array()) {
		global $auth;
	
		if (isset($parameters['no_verify']) && $parameters['no_verify'] == 'Y'){
			return true;
		}
	
		$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
	
		if (is_null($mod_permitted) || !$mod_permitted)
			//se mi ha passato un secondo modulo testo i permessi
			if (isset($parameters['abilita_su_modulo']))
				$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
	
				if (is_null($mod_permitted) || !$mod_permitted){
					global $cod_mod_provenienza;
					//verifico permessi su cod_mod_provenienza
					if (isset($cod_mod_provenienza))
						$mod_permitted = $auth->verify_module_permission($cod_mod_provenienza);
				}
	
	
				if (is_null($mod_permitted) || !$mod_permitted){
					die("\nNon hai i permessi!!");
				}
					
		  return true;
	}
	

	public function get_cod_mod(){
		return $this->mod_cod;
	}	
	
	public function get_cfg_mod(){
		global $cfg_mod_Gest;
		return $cfg_mod_Gest;
	}	
	

	public function get_mod_parameters(){
		$t = new Modules();
		$t->load_rec_data_by_k(array('TAKEY1' => $this->get_cod_mod()));
		return json_decode($t->rec_data['TAMAIL']);
	}	
	
	
	public function fascetta_function(){
		global $auth, $main_module, $cfg_mod_Gest;
		$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	
		echo "<div class='fascetta-toolbar'>";
		
		
		if ($cfg_mod_Gest['toolbar']['disabilitato']!='Y')
		    echo "<img id='bt-toolbar' src=" . img_path("icone/48x48/archive.png") . " height=30>";

		if ($cfg_mod_Gest['abi']['disabilitato']!='Y')
			echo "<img id='bt-abi' src=" . img_path("icone/48x48/bilancio2.png") . " height=30>";
		
		if ($cfg_mod_Gest['marginalita']['disabilitato']!='Y')		
			echo "<img id='bt-marginalita' src=" . img_path("icone/48x48/margini.png") . " height=30>";

		if ($cfg_mod_Gest['flussi_cassa']['disabilitato']!='Y')		
			echo "<img id='bt-flusso_cassa' src=" . img_path("icone/48x48/currency_black_euro.png") . " height=30>";

		if ($cfg_mod_Gest['provvigioni']['disabilitato']!='Y')		
			echo "<img id='bt-provvigioni' src=" . img_path("icone/48x48/briefcase.png") . " height=30>";
		
		if ($cfg_mod_Gest['bollette_doganali']['disabilitato']!='Y')		
			echo "<img id='bt-bollette_doganali' src=" . img_path("icone/48x48/globe.png") . " height=30>";
		
		if ($cfg_mod_Gest['ins_new_anag']['disabilitato']!='Y')		
			echo "<img id='bt-ins_anagrafiche' src=" . img_path("icone/48x48/clienti.png") . " height=30>";
		
		if ($cfg_mod_Gest['modulo_anag_cli_abilitato'] == 'Y')
		    echo "<img id='bt-modulo_anag_cli' src=" . img_path("icone/48x48/clienti.png") . " height=30>";
		
		if ($cfg_mod_Gest['fatture_anticipo']['disabilitato']!='Y')		
			echo "<img id='bt-fatture_anticipo' src=" . img_path("icone/48x48/credit_cards.png") . " height=30>";
		
		if ($cfg_mod_Gest['dichiarazione-intenti']['disabilitato']!='Y')
			echo "<img id='bt-dichiarazione-intenti' src=" . img_path("icone/48x48/email_compose.png") . " height=30>";
		
		if ($cfg_mod_Gest['controllo-cmr']['disabilitato']!='Y')
			echo "<img id='bt-controllo-rientro-cmr' src=" . img_path("icone/48x48/delivery.png") . " height=30>";
		
		echo "<img id='add-promemoria' src=" . img_path("icone/48x48/clock.png") . " height=30>";	
		echo "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
			
		echo "</div>";
	
	}	

	
	
	
	
	
	function decod_cliente($cod, $tipo = 'C'){
	    
	    global $id_ditta_default, $conn, $cfg_mod_Gest;
	            
	    $sql = "SELECT * FROM {$cfg_mod_Gest['file_anag_cli']}
	            WHERE CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = '{$tipo}' AND CFCD = ?";

	    $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt, array($cod));
        $row = db2_fetch_assoc($stmt);
    
	    return $row['CFRGS1'];
	}
	
	
	function next_num($cod){
	    global $conn, $id_ditta_default;
	    global $cfg_mod_Gest;
	    
	    
	    $sql = "SELECT * FROM {$cfg_mod_Gest['file_numeratori']} WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    
	    if ($row = db2_fetch_assoc($stmt)){
	        $n = $row['NRPROG'] +1;
	        $sql = "UPDATE {$cfg_mod_Gest['file_numeratori']} SET NRPROG=$n WHERE NRDT= " . sql_t($id_ditta_default) . " AND NRCOD='$cod'";
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	    } else {
	        $n = 1;
	        $sql = "INSERT INTO {$cfg_mod_Gest['file_numeratori']}(NRDT, NRCOD, NRPROG)  VALUES(" . sql_t($id_ditta_default) . ", '$cod', $n)";
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	    }
	    
	    return $n;
	}
	
	function get_TA_std($tab, $k1, $k2 = null){
	    global $conn;
	    global $cfg_mod_Spedizioni, $id_ditta_default;
	    $ar = array();
	    $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT='{$id_ditta_default}' AND TATAID = '{$tab}' ";
	    
	    if (isset($k1))	$sql .= " AND TAKEY1 = " . sql_t($k1);
	    if (isset($k2))	$sql .= " AND TAKEY2 = " . sql_t($k2);
	    
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    
	    $row = db2_fetch_assoc($stmt);
	    if (!$row) $row = array();
	    
	    return $row;
	}
	
	function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null, $order_by_seq = 'N', $mostra_codice = 'N', $join_sys = 'N'){
	    global $conn;
	    global $cfg_mod_Spedizioni, $id_ditta_default;
	    $ar = array();
	    
	    if($join_sys == 'Y'){
	        $sql_join = "LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tab_sys']} SY
                         ON TA.TADT = SY.TADT AND TA.TATAID = SY.TAID AND TA.TAKEY1 = SY.TANR
	                     ";
	        $sql_select = " , SY.TADESC AS SY_TADESC";
	    }else{
	        $sql_join = "";
	        $sql_select = "";
	    }
	    
	    $sql = "SELECT TA.* {$sql_select}
	            FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
	            {$sql_join}
	            WHERE TA.TADT='{$id_ditta_default}' AND TA.TATAID = '$tab'";
	    
	    if (isset($k1))
	        $sql .= " AND TA.TAKEY1 = '$k1' ";
	        
        if (isset($k2))
            $sql .= " AND TA.TAKEY2 = '$k2' ";
	            
        if (isset($aspe))
            $sql .= " AND TA.TAASPE = '$aspe' ";
	                
        if (isset($gruppo)){
            if (is_string($gruppo))	$gruppo = trim($gruppo);
            if ($gruppo == '')
                $sql .= " AND TARIF1 = '$gruppo' ";
                else
                    $sql .= sql_where_by_combo_value('TARIF1', $gruppo);
        }
            
	                    
	                    
        if ($order_by_seq == 'Y')
            $sql .= "  ORDER BY TASITI, UPPER(TA.TADESC)";
        else if ($order_by_seq == 'P')
            $sql .= "  ORDER BY TA.TAPESO, UPPER(TA.TADESC)";
        else
            $sql .= "  ORDER BY UPPER(TA.TADESC)";
        
                                    
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        $ret = array();
        while ($row = db2_fetch_assoc($stmt)) {
            if (is_null($k1) || $is_get == 'Y') $m_id = trim($row['TAKEY1']);
            else $m_id = trim($row['TAKEY2']);
            
            $r = array();
            
            $r['id'] 		= $m_id;
            
            if($join_sys == 'Y')
                $desc = acs_u8e(trim($row['SY_TADESC']));
            else
                $desc = acs_u8e(trim($row['TADESC']));
            
            if ($mostra_codice == 'Y')
                $r['text'] = "[" . $m_id. "] " . $desc;
            else
                $r['text'] = $desc;
                    
                    if ($esporta_esteso == 'Y'){
                        $r['TAKEY1'] = $row['TAKEY1'];
                        $r['TAKEY2'] = $row['TAKEY2'];
                        $r['TAKEY3'] = $row['TAKEY3'];
                        $r['TAKEY4'] = $row['TAKEY4'];
                        $r['TAKEY5'] = $row['TAKEY5'];
                        $r['TARIF2'] = $row['TARIF2'];
                        $r['TAFG01'] = $row['TAFG01'];
                        $r['TAFG02'] = $row['TAFG02'];
                        $r['TAMAIL'] = trim($row['TAMAIL']);
                        $r['TACOGE'] = $row['TACOGE'];
                    }
                    
                    //		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
                    $ret[] = $r;
        }
        
        return $ret;
	}
	
	
	

	function find_sys_TA($tab, $p=array()){
		global $conn, $id_ditta_default;
		$ar = array();
		$cfg_mod = $this->get_cfg_mod();	
	
		//in join prendo il mezzo, per recuperare la targa (da impostare in automatico nella spedizione)
		$sql = "SELECT * FROM {$cfg_mod['file_tab_sys']} TA 
				WHERE TAID = ? AND TADT = ?";
		
		$sql_where = '';
		
		if ($tab == 'CUAG' && $p['tipo'] == 'area_manager')
		    $sql_where .= " AND SUBSTRING(TA.TAREST, 196, 1) = 'M' ";
		
		if ($tab == 'CUAG' && $p['cli_cc'] == 'Y')
		    $sql_where .= " AND SUBSTRING(TA.TAREST, 196, 1) IN ('', 'A', 'P') ";
		
		if($p['i_sosp'] != 'Y')
		    $sql_where .= " AND TATP <> 'S' ";
		    
	    if (strlen($sql_where) > 0){
	       if (isset($p['value'])) 
	           $sql .= " AND ( (1=1 $sql_where) OR TANR = '{$p['value']}' ) ";
	       else 
	           $sql .= " {$sql_where}";
	    }
	    
		$sql .= " ORDER BY CASE WHEN TATP = 'S' THEN 9 ELSE 0 END, TADESC";
		
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($tab, $id_ditta_default));
	
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
		    
		    $sospeso = false;
		    $color = "";
		    if (trim($row['TATP']) == 'S'){
		        $sospeso = true;
		        $color = "#F9BFC1";
		        
		    }
		    
		    if($p['des2'] == 'Y')
		        $desc = trim($row['TADESC']).' '.trim($row['TADES2']);
		    else
		        $desc = trim($row['TADESC']);
		   
			$nr = array(
					"id" 		=> $row['TANR'],
			        "text" 		=> "[". trim($row['TANR']) ."] ".$desc,
			        "sosp"    => $sospeso,
			        "color"   => $color
			);
			
			if ($tab == 'CUAG'){ 
				//Recupero perc. provv. agente
				$nr['perc_provv'] 	= (float)(substr($row['TAREST'], 171, 2) . "." . substr($row['TAREST'], 173, 2));
				$nr['area_manager'] = substr($row['TAREST'], 235, 3);
			}
			
			$ret[] = $nr; 
	
		}
	
	
		return $ret;
	}
	
	
	function exe_avanzamento_segnalazione_arrivi($r){
	    global $conn;
	    global $cfg_mod_Gest;
	    
	    $ret = array();
	    $ret['success'] = true;
	    
	    
	    $list_selected_id = json_decode($r['list_selected_id']);
	    
	    //per ogni riga selezionata
	    foreach($list_selected_id as $krow_selected => $row_selected){
	        
	        //$row_selected = $list_selected_id[0];
	        
	        //recupero causale/utente/ordine da usare come chiave per AS0
	        $m_k_exp = explode("|", $row_selected->id);
	        
	        //aggiorno la riga in WPI3AS0
	        $na = new SpedAssegnazioneClienti($this);
	        $na->avanza($r['f_entry_prog_causale'], array(
	            'prog' => $row_selected->prog,
	            'form_values' => $r,
	            'cliente' => trim($row_selected->cliente)
	        ));
	        
	    }
	    
	    return acs_je($ret);
	}
	
	
	function get_vettore($p){
	    
	    global $auth, $conn, $id_ditta_default, $cfg_mod_Gest;
	    $ar = array();
	    $ret = array();
	    
	    $sql = "SELECT CFCD, CFRGS1
	            FROM {$cfg_mod_Gest['file_anag_cli']} CF
	            WHERE (UPPER(
	                REPLACE(REPLACE(CFRGS1, '.', ''), ' ', '')
	            ) " . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
	            OR CFCD " . sql_t_compare($p['query'], 'LIKE') .")
                AND CFDT = " . sql_t($id_ditta_default) . " AND CFTICF='F'
                order by CFRGS1
                ";
	    
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt);
	    
	    while ($row = db2_fetch_assoc($stmt)) {
	     
	        $descr = trim($row['CFRGS1']);
	        $ret[] = array( "cod" 		=> $row['CFCD'],
	                        "descr" 	=> acs_u8e($descr),
	                
	            );
	    }
	    
	    
	    return $ret;
	    
	}
	
	
	function get_el_fornitori($p){
	    global $auth, $conn, $id_ditta_default, $cfg_mod_Gest;
	    $ar = array();
	    $ret = array();
	    
	    $sql = "SELECT distinct CFCD, CFTEL, CFCDFI, CFRGS1, CFRGS2, CFIND1, CFIND2, CFLOC1, CFPROV, CFNAZ, GC.GCMAIL
	            FROM {$cfg_mod_Gest['file_anag_cli']} {$join_riservatezza}
	            LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
        	    ON CFDT = GC.GCDT AND DIGITS(CFCD) = GC.GCCDCF AND GC.GCTPAN='CLI'
        	    WHERE (UPPER(
        	    REPLACE(REPLACE(CONCAT(CFRGS1, CFRGS2), '.', ''), ' ', '')
        	    ) " . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
	        
                OR CFCD " . sql_t_compare($p['query'], 'LIKE') .")
                AND CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = 'F' AND CFFLG3 = ''  {$sql_where}
                order by CFRGS1, CFRGS2
                ";
	    
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt);
	    
	    while ($row = db2_fetch_assoc($stmt)) {
	        if (trim($row['CFNAZ']) == 'ITA' || trim($row['CFNAZ']) == ''){
	            $out_loc = acs_u8e(trim($row['CFLOC1']) . " (" . trim($row['CFPROV']) . ")");
	        } else {
	            $des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
	            $out_loc = acs_u8e(trim($row['CFLOC1']) . ", " . trim($des_naz['text']));
	        }
	        
	        
	        $descr = trim($row['CFRGS1']) . " " . trim($row['CFRGS2']);
	        if ($p['mostra_codice'] == 'Y')
	            $descr .= " " . "[" . $row['CFCD'] . "]";
	        
            $sospeso = false;
            $color = "";
            
            if(trim($row['GCSOSP']) == 'S'){
                $sospeso = true;
                $color = "#F9BFC1";
                
            }
	            
	            $ret[] = array( "cod" 		=> $row['CFCD'],
	                "descr" 	=> acs_u8e($descr),
	                "out_loc"	=> $out_loc,
	                "out_ind"	=> acs_u8e(trim($row['CFIND1']) . " " . trim($row['CFIND2'])),
	                "tel" 		=> $row['CFTEL'],
	                "cod_fi" 	=> $row['CFCDFI'],
	                "sosp"      => $sospeso,
	                "color"     => $colors
	                
	            );
	        
	    }
	    
	    
	    return $ret;
	} 
	
	
	
	function get_el_clienti_anag($p){
	    global $auth, $conn, $id_ditta_default, $cfg_mod_Gest;
	    $ar = array();
	    $ret = array();
	    
	     $sql = "SELECT distinct GCCDCF, GCTEL, GCCDFI, GCDCON, GCINDI, GCLOCA, GCPROV, GCNAZI, GCMAIL, GCSOSP
                FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                WHERE (UPPER(
                REPLACE(REPLACE(GCDCON, '.', ''), ' ', '')
                ) " . sql_t_compare(strtoupper(strtr($p['query'], array(" " => "", "." => ""))), 'LIKE') . "
	                            
                OR GCCDCF " . sql_t_compare($p['query'], 'LIKE') .")
                AND GCDT = " . sql_t($id_ditta_default) . " AND GCTPAN='CLI'  {$sql_where}
                order by GCSOSP, GCDCON
                ";
	            
	                     
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt);
	                        
                while ($row = db2_fetch_assoc($stmt)) {
                    if (trim($row['GCNAZI']) == 'ITA' || trim($row['GCNAZI']) == 'IT' || trim($row['GCNAZI']) == ''){
                        $out_loc = acs_u8e(trim($row['GCLOCA']) . " (" . trim($row['GCPROV']) . ")");
                    } else {
                        $des_naz = find_TA_sys('BNAZ', trim($row['GCNAZI']));
                        $out_loc = acs_u8e(trim($row['GCLOCA']) . ", " . trim($des_naz['text']));
                    }
                    
                    
                    $sospeso = false;
                    $color = "";
                    
                    if(trim($row['GCSOSP']) == 'S'){
                        $sospeso = true;
                        $color = "#F9BFC1";
                        
                    }
	                            
                $descr = trim($row['GCDCON']);
                if ($p['mostra_codice'] == 'Y')
                    $descr .= " " . "[" . $row['GCCDCF'] . "]";
                    
                    $ret[] = array( "cod" 		=> $row['GCCDCF'],
                        "descr" 	=> acs_u8e($descr),
                        "out_loc"	=> $out_loc,
                        "out_ind"	=> acs_u8e(trim($row['GCINDI'])),
                        "tel" 		=> $row['GCTEL'],
                        "cod_fi" 	=> $row['GCCDFI'],
                        "sosp"      => $sospeso,
                        "color"     => $color
                        
                    );
                    }
	    
	    
	    return $ret;
	} 
	
	public function has_scheda_cliente($cliente, $scheda){
	    global $conn, $cfg_mod_Gest, $id_ditta_default;
	    
	    if(trim($scheda) == 'XSPM')
	        $sql_where = " AND SUBSTRING(TAREST, 35, 9) = '{$cliente}'";
	    else{
            $sql_where = " AND TACOR1 = '{$cliente}'";
           /* if(trim($scheda) == 'BRCF')
                $sql_where .= " AND SUBSTRING(TAREST, 229, 2) = 'OV'";*/
	    }
	    
	    $sql = "SELECT count(*) AS NR
	    FROM {$cfg_mod_Gest['file_tab_sys']} TA
	    WHERE TADT = '{$id_ditta_default}' AND TAID = '{$scheda}'
	    {$sql_where}";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt);
	    echo db2_stmt_errormsg($stmt);
	    $row = db2_fetch_assoc($stmt);
	    
	    if ($row['NR'] > 0) return TRUE;
	    else return FALSE;
	    
	    
	}
	
	

	function exe_crea_segnalazione_arrivi($r){
		global $conn;
		
		$ret = array();
		$ret['success'] = true;
		
		$cfg_mod = $this->get_cfg_mod();
	
		//bug parametri linux
		global $is_linux;
		if ($is_linux == 'Y')
			$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	
		$ar_ordini = array();
		$list_selected_id = json_decode($r['list_selected_id']);
	
		$ar_ord = array();
		foreach ($list_selected_id as $k_ordine){
			
			//recupero l'ordine selezionato
			$sql = "SELECT * FROM {$cfg_mod['bollette_doganali']['file_testate']} WHERE TFDOCU = " . sql_t($k_ordine) . " ";
				
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
				
			while ($row = db2_fetch_assoc($stmt)) {
				
				//creo la riga in WPI0AS0
				$na = new SpedAssegnazioneOrdini();
				$na->crea('POSTM', array(
						'k_ordine' => $k_ordine,
						'as_selected' => $ar_selected_AS,
						'form_values' => $r
				));
			}
				
		}
	
	
	
		return acs_je($ret);
	}
		
	
	
	function exe_modify_segnalazione_arrivi($r){
		global $conn, $cfg_mod_Spedizioni;
	
		$ret = array();
		$ret['success'] = true;
	
		$cfg_mod = $this->get_cfg_mod();
	
		//bug parametri linux
		global $is_linux;
		if ($is_linux == 'Y')
			$r['list_selected_id'] = strtr($r['list_selected_id'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	


		$ar_upd = array();
		$ar_upd['ASNOTE'] = $_REQUEST['f_note'];
		if (strlen($_REQUEST['f_scadenza']) > 0)
			$ar_upd['ASDTSC'] = $_REQUEST['f_scadenza'];
		else
			$ar_upd['ASDTSC'] = 0;
		$ar_upd['ASUSAT'] = $_REQUEST['f_utente_assegnato'];
		
		//UPDATE
		$sql = "UPDATE {$cfg_mod['bollette_doganali']['file_assegna_ord']}
				SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE ASIDPR = ?";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($_REQUEST['prog'])
		));
		echo db2_stmt_errormsg($stmt);
		
		
		//salvo eventuali note memo
		if (isset($_REQUEST['f_memo'])){
			if (strlen(trim($_REQUEST['f_memo'])) > 0){
				$sqlMemo = "UPDATE {$cfg_mod_Spedizioni['file_note']}
									SET NTMEMO=? WHERE NTKEY1=? AND NTSEQU=0 AND NTTPNO='ASMEM'";
			
				$stmtMemo = db2_prepare($conn, $sqlMemo);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmtMemo, array(utf8_decode($_REQUEST['f_memo']), $_REQUEST['prog']));		
			} else {
				$sqlMemo = "DELETE FROM {$cfg_mod_Spedizioni['file_note']}
							WHERE NTKEY1=? AND NTSEQU=0 AND NTTPNO='ASMEM'";
				$stmtMemo = db2_prepare($conn, $sqlMemo);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmtMemo, array($_REQUEST['prog']));						
			}
		}
	
		return acs_je($ret);
	}	
	
	
	public function get_commento_cliente($cli, $bl, $tpno = null){
	    global $conn;
	    global $cfg_mod_DeskUtility, $id_ditta_default;
	    
	    if (is_null($cli) || strlen(trim($cli)) == 0)
	        return array();
	        
        if(isset($tpno) && $tpno != '')
            $where = " AND RLTPNO = '{$tpno}'";
        else
            $where = " AND RLTPNO = 'CF'";
	                
        $sql = "SELECT *
                FROM {$cfg_mod_DeskUtility['file_note_anag']} 
                WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$cli}'
                AND RLRIFE2 = '{$bl}' {$where}
                ORDER BY RLRIGA";
       
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        $ret = array('testi' =>array());
        while ($row = db2_fetch_assoc($stmt)) {
            $ret['testi'][] = acs_u8e($row['RLSWST']).acs_u8e($row['RLREST1']).acs_u8e($row['RLFIL1']);
            
            
            if ((int)$ret['data_user']['min_data_ge'] > 0){
                if ($row['RLDTGE'] < $ret['min_data_ge']){
                    $ret['data_user']['min_data_ge'] = $row['RLDTGE'];
                    $ret['data_user']['min_user_ge'] = trim($row['RLUSGE']);
                }                
            }    
            else {
                $ret['data_user']['min_data_ge'] = $row['RLDTGE'];
                $ret['data_user']['min_user_ge'] = trim($row['RLUSGE']);
            }
           
            if ((int)$ret['data_user']['max_data_um'] > 0){
                if ($row['RLDTUM'] > $ret['data_user']['max_data_um']){
                    $ret['data_user']['max_data_um'] = $row['RLDTUM'];
                    $ret['data_user']['max_user_um'] = trim($row['RLUSUM']);
                }
            }
            else {
                $ret['data_user']['max_data_um'] = $row['RLDTUM'];
                $ret['data_user']['max_user_um'] = trim($row['RLUSUM']);
            }
                    
         
          
        }
        
        return $ret;
	}
	
	public function get_riga_commento_cliente($cli, $bl, $tpno = null){
	    global $conn;
	    global $cfg_mod_DeskUtility, $id_ditta_default;
	    
	    if (is_null($cli) || strlen(trim($cli)) == 0)
	        return array();
	        
        if(isset($tpno) && $tpno != '')
            $where = " AND RLTPNO = '{$tpno}'";
        else
            $where = " AND RLTPNO = 'CF'";
        
        $sql = "SELECT RRN(RL) AS RRN, RL.*
                FROM {$cfg_mod_DeskUtility['file_note_anag']} RL
                WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$cli}'
                AND RLRIFE2 = '{$bl}' {$where}
                ORDER BY RLRIGA";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        $ret = array();
        while ($row = db2_fetch_assoc($stmt)) {
            $nr = array();
            $nr['text'] = $row['RLSWST'].$row['RLREST1'].$row['RLFIL1'];
            
            $nr['rrn'] = trim($row['RRN']);
           
            $ret[$row['RLRIGA']] = $nr;
        }
        
        return $ret;
	}
	
	public function has_commento_cliente($cli, $bl = null, $tpno = null){
	    global $conn;
	    global $cfg_mod_DeskUtility, $id_ditta_default;
	    
	    $where = "";
	    if (is_null($cli) || strlen(trim($cli)) == 0)
	        return false;
	        
        if(isset($bl) && $bl != '')
            $where .= " AND RLRIFE2 = '{$bl}'";
        else
            $where .= "";
	                
	                
        if(isset($tpno) && $tpno != '')
            $where .= " AND RLTPNO = '{$tpno}'";
        else
            $where .= " AND RLTPNO = 'CF'";
                
        $sql = "SELECT count(*) AS NR
                FROM {$cfg_mod_DeskUtility['file_note_anag']}
                WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$cli}'
                {$where}";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if ($row['NR'] > 0) return TRUE;
        else return FALSE;
	}
	
	
	public function exe_upd_commento_cliente($p, $tpno, $cliente, $bl){
	    global $conn, $id_ditta_default;
	    global $cfg_mod_DeskUtility, $auth;
	    
	 
	    //elimino il vecchio commento
	    $sql = "DELETE
	            FROM {$cfg_mod_DeskUtility['file_note_anag']}
	            WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$cliente}'
	            AND RLRIFE2 = '{$bl}'
	            AND RLTPNO = '{$tpno}'";
	
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    
	    
	    //divido il testo in blocchi da 80 caratteri e creo i record
	    //$str = str_split($p['f_text'], 80);
	    $str = array();
	    if (strlen($p['f_text_0']) > 0) $str[] = $p['f_text_0'];
	    if (strlen($p['f_text_1']) > 0) $str[] = $p['f_text_1'];
	    if (strlen($p['f_text_2']) > 0) $str[] = $p['f_text_2'];
	    if (strlen($p['f_text_3']) > 0) $str[] = $p['f_text_3'];
	    
	
	    $ar_ins = array();
	    $ar_ins['RLDTGE'] 	= oggi_AS_date();
	    $ar_ins['RLUSGE'] 	= $auth->get_user();
	    $ar_ins['RLDTUM'] 	= oggi_AS_date();
	    $ar_ins['RLUSUM'] 	= $auth->get_user();
	    $ar_ins['RLDT'] 	= $id_ditta_default;
	    $ar_ins['RLRIFE1'] 	= $cliente;
	    $ar_ins['RLRIFE2'] 	= $bl;
	    $ar_ins['RLTPNO'] 	= $tpno;
	    
	    $c=0;
	    foreach($str as $t){
	        
	        $ar_ins['RLRIGA']   = ++$c;
	        $ar_ins['RLSWST'] 	= substr($t, 0, 1);  //1
	        $ar_ins['RLREST1'] 	= substr($t, 1, 15);  //15
	        $ar_ins['RLFIL1'] 	= substr($t, 16, 64);  //64
	        
	        
	        $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_note_anag']}(" . create_name_field_by_ar($ar_ins) . ")
			         VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	        
	       /* print_r($ar_ins);
	        print_r($sql);
	        exit;*/
	        
	        $stmt = db2_prepare($conn, $sql);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt, $ar_ins);
	        echo db2_stmt_errormsg();
	    
	}
	
	}
	
	function get_gcrow_by_prog($id_prog){
	    global $conn, $cfg_mod_Gest, $id_ditta_default;
	    $sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC WHERE GCDT='$id_ditta_default' AND GCPROG = ?";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, array($id_prog));
	    $row = db2_fetch_assoc($stmt);
	    $row['GCTRAS'] = trim($row['GCTRAS']);
	    $row['GCCITI'] = trim($row['GCCITI']);
	    return $row;
	}
	
	function get_condizioni_commerciali_cliente($count = null, $params_gc, $scadute = 'N'){
	    $ret = array();
	    if(!is_null($count)){
	      $count = 0;
	      $max_val = array();
	      $tasco = $this->get_condizioni_commerciali_TASCO($count, $params_gc, $scadute);
	      $count += $tasco['count'];
	      $max_val[] = $tasco['max_val'];
	      
	      $camp = $this->get_condizioni_commerciali_CAMP($count, $params_gc, $scadute);
	      $count += $camp['count'];
	      $max_val[] = $camp['max_val'];
	      
	      $xspv = $this->get_condizioni_commerciali_XSPV($count, $params_gc, $scadute);
	      $count += $xspv['count'];
	      $max_val[] = $xspv['max_val'];

	      $ret['count'] = $count;
	      $ret['max_val'] = max($max_val);
	      
	    }else{
	      $tasco = $this->get_condizioni_commerciali_TASCO($count, $params_gc, $scadute);
	      $camp = $this->get_condizioni_commerciali_CAMP($count, $params_gc, $scadute);
	      $xspv = $this->get_condizioni_commerciali_XSPV($count, $params_gc, $scadute);
	      $ret = array_merge($tasco, $camp, $xspv);
	   }
	    	    
	    
	    return $ret;
	}
	
	
	function get_condizioni_commerciali_XSPV($count = null, $params_gc, $scadute = 'N'){
	    global $id_ditta_default, $conn, $cfg_mod_Gest;
	    
	    if(!is_null($count))
	        $sql_select = "COUNT(*) AS C_ROW, MAX(SUBSTRING(TA.TAREST, 34, 8)) AS MAX_VF";
        else
            $sql_select = "TA.*";
	            
        if(is_array($params_gc))
            $row_gc = $params_gc;
        if(is_numeric($params_gc))
            $row_gc = $this->get_gcrow_by_prog($params_gc);
        
        $case_value_sel = "
                  (CASE WHEN SUBSTRING (TAREST, 53, 1) = '1' THEN '{$row_gc['GCSEL1']}'
		          WHEN SUBSTRING (TAREST, 53, 1) = '2' THEN '{$row_gc['GCSEL2']}'
		          WHEN SUBSTRING (TAREST, 53, 1) = '3' THEN '{$row_gc['GCSEL3']}'
		          WHEN SUBSTRING (TAREST, 53, 1) = '4' THEN '{$row_gc['GCSEL4']}' END)";    
	                    
        $sql = "SELECT {$sql_select}
                FROM {$cfg_mod_Gest['file_tab_sys']} TA
                WHERE TA.TADT = '{$id_ditta_default}' AND TA.TAID ='XSPV'
                AND SUBSTRING(TAREST, 26, 8)  >='" .oggi_AS_date() ."'
                AND SUBSTRING(TAREST, 34, 8)  <='" .oggi_AS_date() ."'
                AND (SUBSTRING(TAREST, 44, 9) = '{$row_gc['GCCDCF']}' OR SUBSTRING(TAREST, 44, 9) = '') 
                
                /*selezioni*/
                AND (CASE 
                     WHEN SUBSTRING (TAREST, 54, 12) = '' THEN 1
                     WHEN SUBSTRING(TAREST, 66, 1) = 'I' AND
                          (SUBSTRING(TAREST, 54, 3) = {$case_value_sel}
                          OR SUBSTRING(TAREST, 57, 3) = {$case_value_sel}
                          OR SUBSTRING(TAREST, 60, 3) = {$case_value_sel}
                          OR SUBSTRING(TAREST, 63, 3) = {$case_value_sel})
                     THEN 1                
                     WHEN SUBSTRING(TAREST, 66, 1) = 'E' AND
                          (SUBSTRING(TAREST, 54, 3) <> {$case_value_sel}
                           AND SUBSTRING(TAREST, 57, 3) <> {$case_value_sel}
                           AND SUBSTRING(TAREST, 60, 3) <> {$case_value_sel} 
                           AND SUBSTRING(TAREST, 63, 3) <> {$case_value_sel})
                     THEN 1
                     ELSE 0
                END) = 1

                /*zona*/
                AND (CASE 
                     WHEN SUBSTRING (TAREST, 104, 12) = '' THEN 1
                     WHEN SUBSTRING(TAREST, 116, 1) = 'I' AND
                          (SUBSTRING(TAREST, 104, 3) = '{$row_gc['GCZONA']}'
                           OR SUBSTRING(TAREST, 107, 3) = '{$row_gc['GCZONA']}'
                           OR SUBSTRING(TAREST, 110, 3) = '{$row_gc['GCZONA']}'
                           OR SUBSTRING(TAREST, 113, 3) = '{$row_gc['GCZONA']}')
                     THEN 1                
                     WHEN SUBSTRING(TAREST, 116, 1) = 'E' AND
                          (SUBSTRING(TAREST, 104, 3) <> '{$row_gc['GCZONA']}'
                           AND SUBSTRING(TAREST, 107, 3) <> '{$row_gc['GCZONA']}' 
                           AND SUBSTRING(TAREST, 110, 3) <> '{$row_gc['GCZONA']}' 
                           AND SUBSTRING(TAREST, 113, 3) <> '{$row_gc['GCZONA']}')
                     THEN 1
                     ELSE 0
                END) = 1

                /*agente*/    
                AND (CASE 
                     WHEN SUBSTRING (TAREST, 183, 12) = '' THEN 1
                     WHEN SUBSTRING(TAREST, 195, 1) = 'I' AND
                          (SUBSTRING(TAREST, 183, 3) = '{$row_gc['GCAG1']}'
                           OR SUBSTRING(TAREST, 186, 3) = '{$row_gc['GCAG1']}'
                           OR SUBSTRING(TAREST, 189, 3) = '{$row_gc['GCAG1']}'
                           OR SUBSTRING(TAREST, 192, 3) = '{$row_gc['GCAG1']}')
                     THEN 1                
                     WHEN SUBSTRING(TAREST, 195, 1) = 'E' AND
                          (SUBSTRING(TAREST, 183, 3) <> '{$row_gc['GCZONA']}'
                           AND SUBSTRING(TAREST, 186, 3) <> '{$row_gc['GCAG1']}' 
                           AND SUBSTRING(TAREST, 189, 3) <> '{$row_gc['GCAG1']}' 
                           AND SUBSTRING(TAREST, 192, 3) <> '{$row_gc['GCAG1']}')
                     THEN 1
                     ELSE 0
                END) = 1
                
                /*nazione*/    
                AND (CASE 
                     WHEN SUBSTRING (TAREST, 215, 12) = '' THEN 1
                     WHEN SUBSTRING(TAREST, 227, 1) = 'I' AND
                          (SUBSTRING(TAREST, 215, 3) = '{$row_gc['GCNAZI']}'
                           OR SUBSTRING(TAREST, 218, 3) = '{$row_gc['GCNAZI']}'
                           OR SUBSTRING(TAREST, 221, 3) = '{$row_gc['GCNAZI']}'
                           OR SUBSTRING(TAREST, 224, 3) = '{$row_gc['GCNAZI']}')
                     THEN 1                
                     WHEN SUBSTRING(TAREST, 227, 1) = 'E' AND
                          (SUBSTRING(TAREST, 215, 3) <> '{$row_gc['GCNAZI']}'
                           AND SUBSTRING(TAREST, 218, 3) <> '{$row_gc['GCNAZI']}' 
                           AND SUBSTRING(TAREST, 221, 3) <> '{$row_gc['GCNAZI']}' 
                           AND SUBSTRING(TAREST, 224, 3) <> '{$row_gc['GCNAZI']}')
                     THEN 1
                     ELSE 0
                END) = 1

                /*categoria sconti cliente*/    
                AND (CASE 
                     WHEN SUBSTRING (TAREST, 228, 12) = '' THEN 1
                     WHEN SUBSTRING(TAREST, 240, 1) = 'I' AND
                          (SUBSTRING(TAREST, 228, 3) = '{$row_gc['GCCATC']}'
                           OR SUBSTRING(TAREST, 231, 3) = '{$row_gc['GCCATC']}'
                           OR SUBSTRING(TAREST, 234, 3) = '{$row_gc['GCCATC']}'
                           OR SUBSTRING(TAREST, 237, 3) = '{$row_gc['GCCATC']}')
                     THEN 1                
                     WHEN SUBSTRING(TAREST, 240, 1) = 'E' AND
                          (SUBSTRING(TAREST, 228, 3) <> '{$row_gc['GCCATC']}'
                           AND SUBSTRING(TAREST, 231, 3) <> '{$row_gc['GCCATC']}' 
                           AND SUBSTRING(TAREST, 234, 3) <> '{$row_gc['GCCATC']}' 
                           AND SUBSTRING(TAREST, 237, 3) <> '{$row_gc['GCCATC']}')
                     THEN 1
                     ELSE 0
                END) = 1";
              
        /*print_r($sql);
        exit;*/

        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        if(!is_null($count)){
            $ar_val = array();
            $r = db2_fetch_assoc($stmt);
            $ar_val['count'] = $r['C_ROW'];
            $ar_val['max_val'] = $r['MAX_VF'];
            return $ar_val;
        }else{
            $ret = array();
            while ($r = db2_fetch_assoc($stmt)) {
                
                $r['tipo'] = $r['TANR'];
                $r['val_ini'] = substr($r['TAREST'], 25, 8);
                $r['val_fin'] = substr($r['TAREST'], 33, 8);
                $r['descrizione'] = $r['TADESC']; //funzione
                $r['dettagli'] = "";
                
                $ret[] = $r;
                
            }
            
            return $ret;
        }
	                    
	                    
	}
	
	
	
	function get_condizioni_commerciali_CAMP($count = null, $params_gc, $scadute = 'N'){
	    global $id_ditta_default, $conn, $cfg_mod_Gest;
	    
	    if(!is_null($count))
	        $sql_select = "COUNT(*) AS C_ROW, MAX(SUBSTRING(TA.TAREST, 9, 8)) AS MAX_VF";
        else
            $sql_select = "TA.*";
	            
        if(is_array($params_gc))
            $row_gc = $params_gc;
        if(is_numeric($params_gc))
            $row_gc = $this->get_gcrow_by_prog($params_gc);
        
        if ($cfg_mod_Gest['abilita_digits_on_GCCDCF'] == 'Y')
            $where =  " AND digits(TA.TACOR1) =  '{$row_gc['GCCDCF']}' ";
        else 
            $where = " AND TA.TACOR1 =  '{$row_gc['GCCDCF']}' ";
                                
        $sql = "SELECT {$sql_select}
                FROM {$cfg_mod_Gest['file_tab_sys']} TA
                LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA2
                    ON TA2.TADT = '{$id_ditta_default}' AND TA2.TAID = 'CAMP' AND TA2.TANR = TA.TACOR2 
                    AND TA2.TACOR1 = '' AND TA2.TACOR2= '' 
                    AND SUBSTRING(TA2.TAREST, 62, 1) IN ('Y', 'C')
                WHERE TA.TADT = '{$id_ditta_default}' AND TA.TAID ='CFCA'


                AND TA.TACOR1 =  '{$row_gc['GCCDCF']}' {$where}
                AND SUBSTRING(TA.TAREST, 1, 8)  >=" .oggi_AS_date() ."
                AND SUBSTRING(TA.TAREST, 9, 8)  <=" .oggi_AS_date();

        
   

        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        if(!is_null($count)){
            $ar_val = array();
            $r = db2_fetch_assoc($stmt);
            echo db2_stmt_errormsg();
            $ar_val['count'] = $r['C_ROW'];
            $ar_val['max_val'] = $r['MAX_VF'];
            return $ar_val;
        }else{
            $ret = array();
            while ($r = db2_fetch_assoc($stmt)) {
                
                $r['tipo'] = $r['TANR'];
                $r['val_ini'] = substr($r['TAREST'], 0, 8);
                $r['val_fin'] = substr($r['TAREST'], 8, 8);
                $r['descrizione'] = $r['TADESC']; //funzione
                $r['dettagli'] = "";
           
                $ret[] = $r;
                
            }
            
            return $ret;
        }
	    
	    
	}
	
	function get_condizioni_commerciali_TASCO($count = null, $params_gc, $scadute = 'N'){
	    global $id_ditta_default, $conn, $cfg_mod_Gest;
	    
	    if(!is_null($count))
	        $sql_select = "COUNT(*) AS C_ROW, MAX(TSDTVF) AS MAX_VF";
        else
            $sql_select = "TS.*";
	    
        if(is_array($params_gc))
            $row_gc = $params_gc;
        if(is_numeric($params_gc))   
            $row_gc = $this->get_gcrow_by_prog($params_gc);
	            
        if($scadute == 'N')
            $sql_where = " AND TSDTVF >= ".oggi_AS_date();
        else
            $sql_where = "";
            
        $sql = "SELECT {$sql_select}
                FROM {$cfg_mod_Gest['file_tasco']} TS
                WHERE TS.TSDT = '{$id_ditta_default}' AND TS.TSTPLI='V'
                
                AND (
                (TSTPTB IN ('4', '2', '6', '9') AND TSCLI = '{$row_gc['GCCDCF']}')
                OR (TSTPTB IN ('3', '1') AND TSCATC = '{$row_gc['GCCATC']}')
                OR (TSTPTB = '5' AND TSCAPC = '{$row_gc['GCCAPC']}')
                ) {$sql_where}";
   
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
	            
        if(!is_null($count)){
            $ar_val = array();
            $r = db2_fetch_assoc($stmt);
            $ar_val['count'] = $r['C_ROW'];
            $ar_val['max_val'] = $r['MAX_VF'];
            return $ar_val;
        }else{
            $ret = array();
            
            
            while ($r = db2_fetch_assoc($stmt)) {
                //solo se non sca
                
                        $r['tipo'] = $r['TSTPTB']; 
                        $r['d_tipo'] = "[".$r['TSTPTB']."] ". $this->get_d_tipo_TASCO($r['TSTPTB']);
                        $r['val_ini'] = $r['TSDTVI'];
                        $r['val_fin'] = $r['TSDTVF'];
                        $r['descrizione'] = $this->get_desc_TASCO($r); //funzione
                        $r['dettagli'] = "";
                        if(trim($r['TSPRZ']) != 0){
                            if(trim($r['dettagli']) != '') $r['dettagli'] .= "<br>";
                            $r['dettagli'] .= "Prezzo: ".n($r['TSPRZ'],2);
                        }
                        if(trim($r['TSSC1']) > 0){
                            if(trim($r['dettagli']) != '') $r['dettagli'] .= "<br>";
                            $r['dettagli'] .= "Sconti: ".n($r['TSSC1'],2)."+".n($r['TSSC2'],2)."+".n($r['TSSC3'],2)."+".n($r['TSSC4'],2);
                        }
                        
                        if(trim($r['TSPR1']) != 0){
                            if(trim($r['dettagli']) != '') $r['dettagli'] .= "<br>";
                            $r['dettagli'] .= "Provvigione agente 1: ".n($r['TSPR1'],2);
                        }
                        
                        if(trim($r['TSPR5']) != 0){
                            if(trim($r['dettagli']) != '') $r['dettagli'] .= "<br>";
                            $r['dettagli'] .= "Provvigione agente 2: ".n($r['TSPR5'],2);
                        }
              
                $ret[] = $r;
                
            }
            return $ret;
        }
      
	}
	
	function get_d_tipo_TASCO($tipo){
	    global $cfg_mod_DeskUtility,$id_ditta_default, $conn;
	    
	    $sql = "SELECT ARDART FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
	            WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$row['TSART']}'";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt);
	    $art = db2_fetch_assoc($stmt);
	    
	    
	    switch($tipo){
	        case 1 :
	            $descrizione = "Categoria cliente/categoria articolo";
	            break;
	        case 2 :
	            $descrizione = "Cliente/Categoria articolo";
	            break;
	        case 3 :
	            $descrizione = "Categoria cliente/articolo";
	            break;
	        case 4 :
	            $descrizione = "Cliente/articolo";
	            break;
	        case 5 :
	            $descrizione = "Categoria provvigioni";
	            break;
	        case 6 :
	            $descrizione = "Modello/cliente";
	            break;
	        case 7 :
	            $descrizione =  "Documento/categoria articolo";
	            break;
	        case 9 :
	            $descrizione = "Cliente/categoria articolo/modello";
	            break;
	    }
	    
	    return $descrizione;
	    
	}
	
	function get_desc_TASCO($row){
	    global $cfg_mod_DeskUtility,$id_ditta_default, $conn;
	    
	    $sql = "SELECT ARDART FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
	    WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$row['TSART']}'";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt);
	    $art = db2_fetch_assoc($stmt);
	   
	    
	    switch($row['TSTPTB']){
	        case 1 :
	            $descrizione = $row['TSCATC']."/".$row['TSCATS']; //CATEGORIA CLIENTE/CATEGORIA ARTICOLO
	            break;
	        case 2 :
	            $descrizione = $row['TSCATS']; //CLIENTE/CATEGORIA ARTICOLO
	            break;
	        case 3 :
	            $descrizione = $row['TSCATC']."/[".trim($row['TSART']).'] '.$art['ARDART']; //CATEGORIA CLIENTE/ARTICOLO	           
	            break;
	        case 4 :
	            $descrizione = "[".trim($row['TSART'])."] ".$art['ARDART']; //CLIENTE/ARTICOLO
	            break;
	        case 5 :
	            $descrizione = $row['TSCAPC']; //CATEGORIA PROVVIGIONI
	            break;
	        case 6 :
	            $descrizione = $row['TSMOCO']; //MODELLO/CLIENTE
	            break;
	        case 7 :
	            $descrizione =  $row['TSDOCU']."/".$row['TSCATS']; //DOCUMENTO/CATEGORIA ARTICOLO
	            break;
	        case 9 :
	            $descrizione = $row['TSCATS']."/".$row['TSMOCO']; //CLIENTE/CATEGORIA ARTICOLO/MODELLO
	            break;
	    }
	    
	    return $descrizione;
	    
	}
	

		 	
		 	
} //class

?>