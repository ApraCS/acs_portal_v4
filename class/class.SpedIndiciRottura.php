<?php

class SpedIndiciRottura extends AbstractDb {
 
 	protected $table_id 	= "file_tabelle"; 
	protected $table_k 		= "TADT,TATAID,TAKEY1";
	protected $taid_f 		= "TATAID";
	protected $taid_k 		= "INRO";
	protected $taid_k_sec 	= "INRO2";
	protected $tadt_f 		= "TADT";	
	
	
	
public function out_Writer_Form_initComponent() {
 $ret =  "
        this.addEvents('create');        
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-sticker_black-16',
            frame: true,
            title: 'Indice rottura',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right'
            },
            items: [
            {name: 'k_TADT', xtype: 'hidden'},{name: 'k_TATAID', xtype: 'hidden'}, {name: 'k_TAKEY1', xtype: 'hidden'},
			{
                name: 'TADT',
                xtype: 'hidden',
            }, {
                name: 'TATAID',
                xtype: 'hidden',
            }, {
                fieldLabel: 'Codice',
                name: 'TAKEY1',
                allowBlank: false,
 				maxLength: 10
            }, {
                fieldLabel: 'Descrizione',
                name: 'TADESC',
                allowBlank: false,
 				maxLength: 100
            }],
            dockedItems: [" . self::form_buttons() . "]
        });
        this.callParent();
";	
 return $ret;
}


	
	
public function out_Writer_Grid_initComponent_columns() {
	$ret = "
			{
                text: 'Codice',
                flex: 20,
                sortable: true,
                dataIndex: 'TAKEY1',
                allowBlank: false
            }, {
                text: 'Descrizione',
                flex: 80,
                sortable: true,
                dataIndex: 'TADESC',
                allowBlank: false
            }
	";
 return $ret;	
}	
	
	
	
	
public function out_Writer_Model($class_name) {
echo "
Ext.define('Writer.Model.{$class_name}', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    }, {
        name: 'k_TADT',        
        useNull: false
    }, {
        name: 'k_TATAID',        
        useNull: false
    }, {
        name: 'k_TAKEY1',        
        useNull: false
    }, {
        name: 'TADT',        
        useNull: false
    }, {
        name: 'TATAID',        
        useNull: false
    }, {
        name: 'TAKEY1',        
        useNull: false
    }, {
        name: 'TADESC',        
        useNull: false
    }]
});
";

}	
	
	
	//-------------------------------------------------------
	public function assegna_indice_rottura($tddocu, $indice_rottura, $imposta_seq='N'){
	//-------------------------------------------------------
		global $cfg_mod_Spedizioni, $conn;
		$sped = new Spedizioni();		
		 
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDIRLO = ? WHERE " . $sped->get_where_std() . " AND TDDOCU = ?";		
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($indice_rottura, $tddocu));
		
		
		if ($imposta_seq == 'Y') { //IMPOSTO PROGRESSIVO
			$sql = "SELECT MAX(TDSELO) as MAX_TDSELO FROM {$cfg_mod_Spedizioni['file_testate']} WHERE TDIRLO = ? AND " . $sped->get_where_std();			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($indice_rottura));
			$r = db2_fetch_assoc($stmt);
			$ind = (int)$r['MAX_TDSELO'];

			$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDSELO = ? WHERE " . $sped->get_where_std() . " AND TDDOCU = ?";			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array(sprintf("%03s", $ind +1), $tddocu));						
		}
		
		//se devo replicare l'indice di rottura sul lotto
		if ($cfg_mod_Spedizioni['imposta_lotto_da_indice_rottura'] == 'Y'){
		 $indice_exp = explode("_", $indice_rottura);
		 $sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} 
		         SET TDTPLO='LP', TDAALO = ?, TDNRLO = ? 
		         WHERE " . $sped->get_where_std() . " AND TDDOCU = ?";
		 $stmt = db2_prepare($conn, $sql);
		 echo db2_stmt_errormsg();
		 $result = db2_execute($stmt, array($indice_exp[0], $indice_exp[1], $tddocu));
		}
		

		//HISTORY
		$sh = new SpedHistory();
		$sh->crea(
				'assegna_indice_rottura',
				array(
						"k_ordine" 	=> $tddocu,
						"ex"		=> "TO: " . $indice_rottura
				)
		);		
		
		return $result;
	}	
	
	
	
	public function scrivi_END_indice_rottura($indice_rottura){
	       //HISTORY END_INRO
               $sh = new SpedHistory();
               $sh->crea(
                     'END_assegna_indice_rottura',
                     array(
                       "ex"            => "TO: " . $indice_rottura
                     )
               );
             return true;
	}
	
	

	//-------------------------------------------------------
	public function remove_indice_rottura($tddocu){
        //-------------------------------------------------------
		global $cfg_mod_Spedizioni, $conn;
		$sped = new Spedizioni();
		
		$indice_rottura = '';
			
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET TDIRLO = ? WHERE " . $sped->get_where_std() . " AND TDDOCU = ?";
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($indice_rottura, $tddocu));
	
		//HISTORY
		$sh = new SpedHistory();
		$sh->crea(
				'assegna_indice_rottura',
				array(
						"k_ordine" 	=> $tddocu,
						"ex"		=> "TO: " . $indice_rottura
				)
		);
	
		return $result;
	}
	
	
	
 //-------------------------------------------------------	
  public function get_last_indici_rottura(){
 //-------------------------------------------------------  	
  	global $cfg_mod_Spedizioni, $conn;
  	
  	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_carichi']} WHERE PSTAID = ? ORDER BY PSDTGE DESC, PSAACA DESC, PSNRCA DESC FETCH FIRST 25 ROWS ONLY";
  	
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();
  	$result = db2_execute($stmt, array($this->taid_k));
  		
  	$ret = array();
  	while ($r = db2_fetch_assoc($stmt)) {
  		$r['DATA'] = $r['PSDTGE'];
  		$r['INDICE'] = trim($this->out_codice($r));
  		$r['DESCRIZIONE'] = trim(acs_u8e($r['PSDESC']));
  		$ret[] = $r;  		
  	}
  	 
   return $ret;	
  }	
	
  //-------------------------------------------------------
  public function create_new_indice_rottura($year = null, $tipo_numeratore = 'PRIMARIO', $aggiungi_valore = 0){
  	//-------------------------------------------------------
  	global $cfg_mod_Spedizioni, $conn, $id_ditta_default, $auth;
  	$sped = new Spedizioni();
  	
  	if (strlen($year) != 4) {  	
  		$day 	= oggi_AS_date();
  		$year 	= substr($day, 0, 4);
  	}	
  	
  	if ($tipo_numeratore == 'PRIMARIO')
  		$chiave_indice_rottura = implode("_", array($this->taid_k, $year));
  	if ($tipo_numeratore == 'SECONDARIO')
  		$chiave_indice_rottura = implode("_", array($this->taid_k_sec, $year));
  	
  	$prog = $sped->next_num($chiave_indice_rottura);
  	
  	$prog += $aggiungi_valore;
  	 
  	
  	$i_user = substr($auth->get_user(), 0, 8);
  	$i_data  = oggi_AS_date();
  	$i_ora 	= oggi_AS_time();
  	 
  	
  	//insert
  	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_carichi']}(PSUSGE, PSDTGE, PSORGE, PSDT, PSTAID, PSTPCA, PSAACA, PSNRCA)
  				VALUES(?, ?, ?, '$id_ditta_default', '{$this->taid_k}', '{$this->taid_k}', $year, $prog)";  	
  	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, array($i_user, $i_data, $i_ora));  	
  	 
  	//ritorno la chiave selezionata
  	$ar = array(
  			'PSDT' 		=> $id_ditta_default,
  			'PSAACA'	=> $year,
  			'PSNRCA'	=> $prog
  	 );
  
  	return $this->out_codice($ar);
  }
  
  
  
  public function out_codice($ar){
  	return implode("_", array($ar['PSAACA'], sprintf("%04s", $ar['PSNRCA'])));
  }
  
  

  //-------------------------------------------------------
  public function salva_descrizione($form){
  	//-------------------------------------------------------
  	global $cfg_mod_Spedizioni, $conn, $id_ditta_default;
  	$sped = new Spedizioni();
  	 
  	$chiave_indice_rottura = explode("_", $form->f_codice);

  	$ar_parameters = array(
		$form->f_descrizione,
  		$id_ditta_default,
  		$this->taid_k,
  		$this->taid_k,
  		$chiave_indice_rottura[0],
  		$chiave_indice_rottura[1]
  	);
  	
  	
  	//update
  	$sql = "UPDATE {$cfg_mod_Spedizioni['file_carichi']}
  			SET PSDESC = ? 
  			WHERE PSDT=? AND PSTAID=? AND PSTPCA=? AND PSAACA=? AND PSNRCA=?";
  	$stmt = db2_prepare($conn, $sql);
  	$result = db2_execute($stmt, $ar_parameters);
  	
  	//recupero il record aggiornato
  	$ar_parameters = array(
  			$id_ditta_default,
  			$this->taid_k,
  			$this->taid_k,
  			$chiave_indice_rottura[0],
  			$chiave_indice_rottura[1]
  	);
  	 
  	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_carichi']}
  			WHERE PSDT=? AND PSTAID=? AND PSTPCA=? AND PSAACA=? AND PSNRCA=?";
  			$stmt = db2_prepare($conn, $sql);
  			$result = db2_execute($stmt, $ar_parameters);
  			$r = db2_fetch_assoc($stmt);
  
		  	$ar = array(
		  	'PSDT' 		=> $r['PSDT'],
		  	'PSAACA'	=> $r['PSAACA'],
		  	'PSNRCA'	=> $r['PSNRCA'],
		  	'PSDESC'	=> trim($r['PSDESC'])
		  	);
  
  	return $ar;
  }
  
  
  
}

?>