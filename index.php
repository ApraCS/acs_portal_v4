<?php
require_once "config.inc.php";

/*
 * la login come supply_desk e' gestita come visualizzazione diversa con parametro da config.inc.php
if ($auth->is_authenticate() == 1 && $auth->is_from_supply_desk() == 1){
	header("location: ../supply_desk/");
	exit;
}
*/

$is_ajax = false;


//se il profile in ingresso ha un modulo di apertura automatica... apro direttamente il modulo
$cod_auto_mod = $auth->ha_auto_modulo();

if ($cod_auto_mod != false){
	$auto_mod = new Modules();
	$auto_mod->load_rec_data_by_k(array('TAKEY1' => $cod_auto_mod));
	
	$mod_path = implode("/", array("module", $auto_mod->rec_data['TARIF1']));
	
	header("location: {$mod_path}");
	exit;
} //se ha auto_modulo


if (isset($login_page)){
	include("index_{$login_page}.php");
	exit;
}
	

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>ACS Portal</title>
	<link rel="stylesheet" type="text/css" href="../extjs/resources/css/ext-all.css" />

	<link rel="stylesheet" type="text/css" href="js/extensible-1.5.2/resources/css/extensible-all.css" />
	<link rel="stylesheet" type="text/css" href="js/extensible-1.5.2/resources/css/calendar.css" />	
		
	<style type="text/css">
		@import "css/style.css";	
		@import "css/home-main.css";
	</style>
	
	<link rel="stylesheet" type="text/css" href="css/toolbars.css" />
	
	<!-- EXTJS  js-->
	<script type="text/javascript" src="../extjs/ext-all.js"></script>
	<script type="text/javascript" src="../extjs/locale/ext-lang-it.js"></script>
	
	<!-- EXTENSIBLE CALENDAR  -->
	<script type="text/javascript" src="js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
	<script type="text/javascript" src="js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>	
	
	<script src=<?php echo acs_url("js/acs_js.js") ?>></script>	
	
<script type="text/javascript">

	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
	        "Extensible": "js/extensible-1.5.2/src", 
			"Extensible.example": "js/extensible-1.5.2/examples"	                   
        }	    
	});Ext.Loader.setPath('Ext.ux', 'ux');


    Ext.require(['*', 'Ext.ux.GMapPanel', 'Ext.ux.grid.FiltersFeature', 'Ext.selection.CheckboxModel'
                 , 'Extensible.calendar.data.MemoryEventStore'
                 , 'Extensible.calendar.CalendarPanel'  
                 , 'Extensible.example.calendar.data.Events'
				 , 'Extensible.calendar.data.CalendarMappings',
    			 , 'Extensible.calendar.data.EventMappings'                 
                 ]);
    
Ext.onReady(function() {




 //PER RIMUOVERE L'ORARIO NEGLI EVENTI (WEEK)
 Extensible.calendar.view.DayBody.override({
    getTemplateEventData : function(evt){
        var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            colorCls = 'x-cal-default',
            title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ',
            recurring = evt[M.RRule.name] != '';
        
        this.getTemplateEventBox(evt);
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, 
                    evt[M.CalendarId.name]);
                
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '') + (Ext.isIE || Ext.isOpera ? '-x' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
        data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        data.Title =  (!title || title.length == 0 ? this.defaultEventTitleText : title);
        return Ext.applyIf(data, evt);
    }    
 });

	

//PER RIMUOVERE L'ORARIO NEGLI EVENTI (MONTH)
Extensible.calendar.view.Month.override({
    getTemplateEventData : function(evt){
		var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            recurring = evt[M.RRule.name] != '',
            colorCls = 'x-cal-default',
		    title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ';
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, evt[M.CalendarId.name]);
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
		data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        //data.Title = (evt[M.IsAllDay.name] ? '' : Ext.Date.format(evt[M.StartDate.name], fmt)) + 
        //        (!title || title.length == 0 ? this.defaultEventTitleText : title);
        data.Title = (!title || title.length == 0 ? this.defaultEventTitleText : title);
        
        
        return Ext.applyIf(data, evt);
    }	
});




//Per rimuovere l'orario ********************
Extensible.calendar.template.DayBody.override({
    constructor: function(config){
        
        Ext.apply(this, config);
    
        Extensible.calendar.template.DayBody.superclass.constructor.call(this,
            '<table class="ext-cal-bg-tbl" cellspacing="0" cellpadding="0" style="height:{dayHeight}px;">',
                '<tbody>',
                    '<tr height="1">',
                        '<td colspan="{dayCount}">',
                            '<div class="ext-cal-bg-rows">',
                                '<div class="ext-cal-bg-rows-inner">',
                                    '<tpl for="times">',
                                        '<div class="ext-cal-bg-row ext-row-{[xindex]}" style="height:{parent.hourHeight}px;">',
                                            '<div class="ext-cal-bg-row-div {parent.hourSeparatorCls}" style="height:{parent.hourSeparatorHeight}px;"></div>',
                                        '</div>',
                                    '</tpl>',
                                '</div>',
                            '</div>',
                        '</td>',
                    '</tr>',
                    '<tr>',
                        '<tpl for="days">',
                            '<td class="ext-cal-day-col">',
                                '<div class="ext-cal-day-col-inner">',
                                    '<div id="{[this.id]}-day-col-{.:date("Ymd")}" class="ext-cal-day-col-gutter" style="height:{parent.dayHeight}px;"></div>',
                                '</div>',
                            '</td>',
                        '</tpl>',
                    '</tr>',
                '</tbody>',
            '</table>'
        );
    }	
});	

Extensible.calendar.template.DayHeader.override({
    constructor: function(config){

    	console.log('in override DayHeader');
		console.log(this);
		console.log(config);
        
        Ext.apply(this, config);
    
        this.allDayTpl = Ext.create('Extensible.calendar.template.BoxLayout', config);
        this.allDayTpl.compile();
        
        Extensible.calendar.template.DayHeader.superclass.constructor.call(this,
            '<div class="ext-cal-hd-ct">',
                '<table class="ext-cal-hd-days-tbl" cellspacing="0" cellpadding="0">',
                    '<tbody>',
                        '<tr>',
                            '<td class="ext-cal-hd-days-td"><div class="ext-cal-hd-ad-inner">{allDayTpl}</div></td>',
                            '<td class="ext-cal-gutter-rt"></td>',
                        '</tr>',
                    '</tbody>',
                '</table>',
            '</div>'
        );
    }	
});







	
	
 <?php if ($auth->is_sec_admin() || $auth->is_system_configured()==false ) {  ?>	    


 <?php
 global $youtube_video_main_ID;
 if (isset($youtube_video_main_ID)){
 ?>
 Ext.get("logo_cliente").on('click', function(){

 	new Ext.Window({
 	    title : "Info",
 	    width : 300,
 	    height: 300,
 	    layout : 'fit',
 	    items : [{
 	        xtype : "component",
 	        id    : 'iframe-win',
 	        autoEl : {
 	            tag : "iframe",
 	            src : "http://www.youtube.com/embed/<?php echo $youtube_video_main_ID; ?>?autoplay=1" 
 	        }
 	    }]
 	}).show();
 	// Call below code through your any srcUpdate function.
 	// Ext.getDom('iframe-win').src = "http://www.yahoo.com";
  });

 <?php } 
 
 } ?>  
 
    Ext.get("main-tools").on('click', function(){
			print_w = new Ext.Window({
					  width: 600
					, height: 300
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Pannello di amministrazione'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show(); 
			
				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : 'module/admin/acs_form_json_amm_pannello.php',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
			   	
    });
 
 
    
});    
</script>	
	
	
	
</head>

<body class="home">

   <!-- Begin Wrapper -->
   <div id="wrapper">
   
   
         <!-- Begin Header -->
         <div id="header">		 
		    <div id="logo_cliente"><img src=<?php echo logo_path(); ?> height=50></div>		
			<span id="logo_portale"><img src="images/logo_portale.png"></span>	    	 			   
		 </div>
		 <!-- End Header -->
		 
         <!-- Eventuale messaggio di avviso -->
		 <!-- <h1 style="width: 900px; text-align: center; color: red; margin: 10px 0px 10px 0px; padding: 20px 20px; border: 1px solid red;">
		  Attenzione. Nella giornata di domani (gioved&igrave; 16/04/20) il sistema sar&agrave; inaccessibile dalle ore 10:00 alle ore 14:00 causa aggiornamento e manutenzione.
		 </h1> -->
		 		 
		 <!-- Begin Naviagtion -->
         <div id="navigation">		 
		       This is the Navigation		 			   
		 </div>
		 <!-- End Naviagtion -->
		 
		 <!-- Begin Content -->
		 <div id="content">		       
			<div id="middle">
		 	 <ul id="home-menu">  
			   <li>
			     
			     <a href="module/desk_vend/index.php?menu_type=PRO_MON" class="button-link b2" f-section="sped">Desktop<br>Programmazione/Monitor</a>
			     <a href="module/desk_vend/index.php?menu_type=ORD_VEN" class="button-link b2" f-section="sped">Desktop<br>Ordini di Vendita</a>
			     
			   	   
			   	 <?php if ($cfg_home_button_punto_vendita == 'Y'){ ?>	
			   	   <a href="module/desk_punto_vendita/" class="button-link b2" f-section="sped">Desktop<br>Punto Vendita</a>
			   	 <?php } ?>
			   	   
			   <li><a href="module/desk_acq/" class="button-link b2" f-section="utility">Desktop<br>Ordini di Acquisto</a>		   
			   
			   <?php if ($cfg_home_button_programma_di_produzione == 'Y' || 
			       (   $cfg_home_button_programma_di_produzione == 'T' && 
			           $auth->is_authenticate() && trim($auth->get_user()) == 'ACS')
			       ){ ?>
			   	   <a href="module/desk_prod/" class="button-link b2" f-section="utility">Desktop<br>Programmi di produzione</a>
			   <?php } ?>
			   
			   <?php
			    $b = array('link' => '#', 'text' => 'Desktop Lavorazioni', 'f-section' => "std");
			    if (isset($cfg_home_button_link))
			    	if (isset($cfg_home_button_link['desktop_lavorazioni']))
			    		$b = $cfg_home_button_link['desktop_lavorazioni'];
			   ?>			   
			   <li><a href="<?php echo $b['link']; ?>" class="button-link b2" f-section="<?php echo $b['f-section']; ?>"><?php echo $b['text']; ?></a>
			   
			     <?php if ($cfg_home_button_translations == 'Y'){ ?>
			   	   <a href="module/desk_trans/" class="button-link b2" f-section="std">Translation</a>
			   	 <?php } ?>



			   <?php if (isset($cfg_home_button_link) && (isset($cfg_home_button_link['SUD2_ticket_man']))){
			       $b = $cfg_home_button_link['SUD2_ticket_man'];
			   ?>			   
			     <a href="<?php echo $b['link']; ?>" target="_blank" class="button-link b2" f-section="<?php echo $b['f-section']; ?>"><?php echo $b['text']; ?></a>
			   <?php } ?>

			   
			   <li> <a href="module/desk_stats/" class="button-link b2" f-section="utility">Cruscotto Vendite</a>
			   		<a href="#" class="button-link b2" f-section="utility">Cruscotto Acquisti</a>			   	
			   		<a href="module/desk_gest/" class="button-link b2" f-section="utility">Desktop Amministrazione</a>
			 </ul>			   
			</div>					   
		 </div>
		 <!-- Begin Content -->
		 
		 <div id="user">
		 
		 <?php if (!$auth->is_authenticate()) : ?>		 
		  <form action="login.php">
		  	<input type=hidden name=fn value=login>
		 	<div class="user-div">
		 		<img src="images/icone/48x48/vcards.png" />
		 		<label>Utente</label><input type=text name=user>
		 		<label>Password</label><input type=password name=password>
		 		<INPUT class="user-submit" style="margin-left: 0px" TYPE="image" SRC="images/icone/48x48/button_blue_play.png" ALT="Effettua Login">
		 	</div>	
		  </form>
		  <?php else : ?>
		  	<div class="user-div">
		 		<img src="images/icone/48x48/vcards.png" />		  		
		  		<label>Utente <strong><?php echo $auth->out_name(); ?></strong></label>
		  		&nbsp;&nbsp;&nbsp;<a href="login.php?fn=logout"><img src="images/icone/48x48/button_grey_eject.png" width=40/></a>	
		  	</div>	
		  <?php endif; ?>
		  
		  	
		 </div>

		<div id="footer">
			<table width="100%" class="footerApra">
			 <tbody><tr valign="middle">
				<td valign="middle"><span style="margin-left: 8px;">ACS_ERP Portal, v RC 2.0.1 - Based on ZendServer</span></td>
						
				<td>		
					<div style="float: right;"><a href="module/desk_vend/acs_gestione_attav.php/" f-section="utility"><img src="images/under-construction.png" width=50></a></div>
				</td>
				<td valign="middle" align="right">			
				 <span>Powered by Apra Computer System</span>				 				 
				</td>
				<td width=10>
				  <img id="main-tools" src="images/icone/48x48/tools.png" height=45/>
				</td>
			 </tr>
			</tbody></table>
		</div>
		 
   </div>
   <!-- End Wrapper -->
   
</body>

</html>