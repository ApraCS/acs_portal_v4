<?php

require __DIR__ . '/config.inc.php';

/* TODO: in parametrizzare in base a fornitore */
$m_config = $cfg_delivery_control_ftp_trasp;

if (!isset($m_config)) die('ERROR: Configurazione mancante');
        
    //Connessione ftp
    $conn_ftp = ftp_connect($m_config['host']);
    $login_result = ftp_login($conn_ftp, $m_config['user'], $m_config['psw']) or die("Ftp login error");        
    $files = ftp_nlist($conn_ftp , $m_config['from_dir']);
    
    foreach($files as $remote_file){
        $path = pathinfo($remote_file);
        
        if ($path['extension'] == 'csv') {
            echo "\nElaboro il file {$path['basename']}";
            $temp_file = tempnam('/tmp', 'wpi8-');
            if (ftp_get($conn_ftp, $temp_file, $m_config['from_dir'] . '/' . $remote_file, FTP_ASCII)) {
                echo("\nSuccessfully written $remote_file to $temp_file");
            }
            
            //gestione file
            _elabora_file($temp_file, $path);
            
            //sposto il file nella cartella remote ftp
            ftp_rename($conn_ftp, 
                $m_config['from_dir'] . '/' .$remote_file, 
                $m_config['from_dir_done'] . '/' . $path['basename']);
            
            unlink($temp_file); //rimuovo il file temporaneo
        }
    }//foreach files
    

    echo "\nFine\n";
    
    
//*****************************************************************************
function _elabora_file($local_file, $path){
//*****************************************************************************
    //elaborazione in base a filename (carichi, colli, ...)
    $f = $path['basename'];
    $f_ar = explode('_', $f);
    switch ($f_ar[1]){
        case 'WPI8PS0.csv': 
            echo "\nFile carichi ----";
            _elabora_file_carichi($local_file, $path);
            break;
        case 'WPI8TD0.csv':
            echo "\nFile Testate ----";
            _elabora_file_testate($local_file, $path);
            break;
        case 'WPI8PL0.csv':
            echo "\nFile colli ----";
            _elabora_file_colli($local_file, $path);
            break;
        default:
            echo "nFile non conosciuto :" . $f_ar[1];
    }
}
    
        
//*****************************************************************************
function _elabora_file_carichi($local_file, $path){
//*****************************************************************************
    if (($handle = fopen($local_file, "r")) !== FALSE) {
        
        $codice_richiesta = date('YmdHis');
        $crow = 0;
        while(!feof($handle)) { //scorro le righe
            $crow++;
            $line = fgets($handle);
            
            if ($crow==1) {  //intestazioni colonna
                $rowH  = explode("|", trim($line));
            } else {
                $rowL   = explode('|', $line);
                $row    = _row($rowL, $rowH);
                if (strlen(trim($row['PSNSPE']))> 0 && strlen(trim($row['PSCARI']))> 0)
                    _update_carichi($row);                
            }
            
        }
    }
  return true;
}

//*****************************************************************************
function _elabora_file_testate($local_file, $path){
//*****************************************************************************
    if (($handle = fopen($local_file, "r")) !== FALSE) {
        
        $codice_richiesta = date('YmdHis');
        $crow = 0;
        while(!feof($handle)) { //scorro le righe
            $crow++;
            $line = fgets($handle);
            
            if ($crow==1) {  //intestazioni colonna
                $rowH  = explode("|", trim($line));
            } else {
                $rowL   = explode('|', $line);
                $row    = _row($rowL, $rowH);
                
                if (strlen(trim($row['TDNRDO']))> 0)
                    _update_testate($row);                
            }
            
        }
    }
  return true;
}

//*****************************************************************************
function _elabora_file_colli($local_file, $path){
//*****************************************************************************

    if (($handle = fopen($local_file, "r")) !== FALSE) {
    
        $codice_richiesta = date('YmdHis');
        $crow = 0;
        while(!feof($handle)) { //scorro le righe
            $crow++;
            $line = fgets($handle);

            if ($crow==1) {  //intestazioni colonna
                $rowH  = explode("|", trim($line));
            } else {
                $rowL   = explode('|', $line);
                $row    = _row($rowL, $rowH);
                if (strlen(trim($row['PLNRDO']))> 0 && strlen(trim($row['PLNREC']))> 0)
                    _update_colli($row);
            }
            
        }
    }
    
    return true;
}





//####################################################################
function _update_carichi($row){
//####################################################################
    echo "\n->update_carico";
    global $conn, $libreria_predefinita;
    $ar_upd = array();
    $ar_upd['PSSTAT'] = trim($row['PSSTAT']);
    $ar_upd['PSDTCI'] = _to_date($row['PSDTCI']);
    $ar_upd['PSHMCI'] = _to_time($row['PSHMCI']);
    $ar_upd['PSDTCF'] = _to_date($row['PSDTCF']);
    $ar_upd['PSHMCF'] = _to_time($row['PSHMCF']);
    $ar_upd['PSDTSI'] = _to_date($row['PSDTSI']);
    $ar_upd['PSHMSI'] = _to_time($row['PSHMSI']);
    $ar_upd['PSDTSF'] = _to_date($row['PSDTSF']);
    $ar_upd['PSHMSF'] = _to_time(preg_replace( "/\r|\n/", "", $row['PSHMSF'] )); //rimuovo \n
    
    $sql = "UPDATE {$libreria_predefinita}.WPI8PS0 PS
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE PSNSPE = " . sql_t(sprintf("%09s", trim($row['PSNSPE']))) . " AND PSCARI = " . sql_t($row['PSCARI']);
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    return $result;
}


//####################################################################
function _update_testate($row){
//####################################################################
    echo "\n->update_testata";
    global $conn, $libreria_predefinita;
    $ar_upd = array();
    $ar_upd['TDDTSC'] = _to_date($row['TDDTSC']);
    $ar_upd['TDHMSC'] = _to_time($row['TDMHSC']);
    $ar_upd['TDDTSS'] = _to_date($row['TDDTSS']);
    $ar_upd['TDHMSS'] = _to_time($row['TDHMSS']);
    
    //In Turi va in errore di CAST l'update. Non so perche'!!!
/*    $sql = "UPDATE {$libreria_predefinita}.WPI8TD0 TD
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE TDNRDO = " . sql_t($row['TDNRDO']) ;
*/
    $sql = "UPDATE {$libreria_predefinita}.WPI8TD0 TD
            SET TDDTSC= " . _to_date($row['TDDTSC']) . ", TDHMSC=" . _to_time($row['TDHMSC']) . ", 
                TDDTSS= " . _to_date($row['TDDTSS']) . ", TDHMSS=" . _to_time($row['TDHMSS']) . "
            WHERE TDNRDO = " . sql_t($row['TDNRDO']) ;
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    return $result;
}



//####################################################################
function _update_colli($row){
//####################################################################
    echo "\n->update_collo";
    global $conn, $libreria_predefinita;
    $ar_upd = array();
    $ar_upd['PLDTSC'] = _to_date($row['PLDTSC']);
    $ar_upd['PLHMSC'] = _to_time($row['PLHMSC']);
    $ar_upd['PLNOSC'] = $row['PLNOSC'];
    $ar_upd['PLDTSS'] = _to_date($row['PLDTSS']);
    $ar_upd['PLHMSS'] = _to_time($row['PLHMSS']);
    $ar_upd['PLNOSS'] = $row['PLNOSS'];
    $ar_upd['PLFNCO'] = $row['PLFNCO'];
    $ar_upd['PLFNCA'] = $row['PLFNCA'];

    //per evitare problemi di new line
    //ToDo: o sistemare oppure sapere esattamente quali valori puo' assumere
    if (substr($row['PLFNSC'], 0, 1) == 'Y' || substr($row['PLFNSC'], 0, 1) == 'N')    
        $ar_upd['PLFNSC'] = substr($row['PLFNSC'], 0, 1);
    
    $sql = "UPDATE {$libreria_predefinita}.WPI8PL0 cp
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE PLNRDO = " . sql_t($row['PLNRDO']) . " AND PLNREC = " . $row['PLNREC'];
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    return $result;
    
}

function _to_date($v){
    if (strlen(trim($v)) == 0) return 0;
    return substr($v, 6, 4) . substr($v, 3, 2) . substr($v, 0, 2);
}
function _to_time($v){
    if (strlen(trim($v)) == 0) return 0;
    return substr($v, 0, 2) . substr($v, 3, 2) . substr($v, 6, 2);
}

function _con_ritorno_colli($row){
    if (
           trim($row['PLDTSC']) != 0
        || trim($row['PLHMSC']) != 0
        || trim($row['PLNOSC']) != ''
        || trim($row['PLDTSS']) != 0
        || trim($row['PLHMSS']) != 0
        || trim($row['PLNOSS']) != ''
        || trim($row['PLFNCO']) != ''
        || trim($row['PLFNCA']) != ''
        || trim($row['PLFNSC']) != ''        
       )
    return true;
    else return false;
}


function _row($rowL, $rowH){
    $row = array();
    foreach($rowH as $k => $v){
       $row[$v] = $rowL[$k];
    }
    return $row;
}
        
/*        
        //*****************************************************************************
        function cerved_elabora_file($local_file, $path){
            //*****************************************************************************
            global $conn, $id_ditta_default, $file_cerved_payline;
            if (($handle = fopen($local_file, "r")) !== FALSE) {
                
                $codice_richiesta = date('YmdHis');
                $crow = 0;
                while(!feof($handle)) { //scorro le righe
                    $line = fgets($handle);
                    $crow++;
                    
                    if ($crow==1) {  //intestazioni colonna
                        $rowH  = explode("|", trim($line));
                    } else {
                        
                        $row  = explode("|", trim($line));
                        
                        //Non gestisco se non ho il codice cliente passato da cerved
                        if(strlen(trim($row[0])) == 0)
                            continue;
                            
                            $cod_cliente     = trim($row[0]); //codice cliente (AS) dalla prima colonna del file cerved
                            
                            for($i=0; $i<count($rowH); $i++){
                                
                                if(strlen($rowH[$i]) == 0)
                                    continue;
                                    
                                    $name_attributo      = trim($rowH[$i]);
                                    $val_attributo       = substr(trim($row[$i]), 0, 256);
                                    
                                    $row_cp = controllo_campo($cod_cliente,  $name_attributo);
                                    
                                    if($row_cp == false || trim($row_cp['CPVALP']) != $val_attributo){
                                        
                                        if($row_cp){ //se devo storicizzare il vecchio record
                                            
                                            $ar_upd = array();
                                            $ar_upd['CPRIGA'] 	= 'H';
                                            
                                            $sql = "UPDATE {$file_cerved_payline} cp
                                            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                                            WHERE RRN(cp) = '{$row_cp['RRN']}' ";
                                            
                                            $stmt = db2_prepare($conn, $sql);
                                            echo db2_stmt_errormsg();
                                            $result = db2_execute($stmt, $ar_upd);
                                        }
                                        
                                        //Inserimento
                                        $ar_ins = array();
                                        $ar_ins['CPUSCR'] = 'SD_AUTO';
                                        $ar_ins['CPDTCR'] = oggi_AS_date();
                                        $ar_ins['CPHHCR'] = oggi_AS_time();
                                        $ar_ins['CPBAND'] = 'SUD0'; //$row[1];
                                        $ar_ins['CPAZIE'] = $id_ditta_default;
                                        $ar_ins['CPAREA'] = 'AZIE';
                                        $ar_ins['CPCOPR'] = $cod_cliente;
                                        $ar_ins['CPATTR'] = $name_attributo;
                                        $ar_ins['CPVALP'] = $val_attributo;
                                        $ar_ins['CPDVAI'] = oggi_AS_date();
                                        $ar_ins['CPOVAI'] = oggi_AS_time();
                                        $ar_ins['CPDVAF'] = oggi_AS_date();
                                        $ar_ins['CPOVAF'] = oggi_AS_time();
                                        $ar_ins['CPCONP'] = 'CERVED';
                                        $ar_ins['CPSEQU'] = '';
                                        $ar_ins['CPRIGA'] = 'A';
                                        $ar_ins['CPDSTO'] = 0;//oggi_AS_date();
                                        $ar_ins['CPHSTO'] = 0;//oggi_AS_time();
                                        $ar_ins['CPTALI'] = 'E';
                                        $ar_ins['CPNOTE'] = $path['basename'];
                                        
                                        $sql = "INSERT INTO {$file_cerved_payline}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                                        
                                        $stmt = db2_prepare($conn, $sql);
                                        echo db2_stmt_errormsg();
                                        $result = db2_execute($stmt, $ar_ins);
                                        echo db2_stmt_errormsg($stmt);
                                        
                                    } else {
                                        //echo "\n Valore non modificato";
                                    }
                            } //per ogni campo
                    } //per ogni riga cliente
                } //per ogni riga
                
                fclose($handle);
            }
        } //elabora file
        
        
        function controllo_campo($cod_cli, $attributo){
            
            global $conn, $id_ditta_default, $file_cerved_payline;
            
            $sql = "SELECT RRN(CP) AS RRN, CPVALP
            FROM {$file_cerved_payline} CP
            WHERE CPAZIE = '{$id_ditta_default}'
            AND CPCOPR = '{$cod_cli}'
            AND CPATTR = '{$attributo}'
            AND CPRIGA = 'A'
            ";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            $row_cp = db2_fetch_assoc($stmt);
            
            if(!$row_cp)
                return false;
                else {
                    $row_cp['CPVALP'] = trim($row_cp['CPVALP']);
                    return $row_cp;
                }
        }
  */      