<?php

 class Auth {
 	
	private $user, $profile, $tipologia, $descr, $email, $is_autenticate = 0, $from_supply_desk, 
	       $cod_riservatezza = '', $cod_riservatezza2 = '', $cod_riservatezza3 = '', $cod_riservatezza4 = '', $cod_riservatezza5 = '';
	
	function __construct() {
		$this->reset();
	}

	
	public function set_authenticate($user, $profile = 'DISK_FIRMA'){
		$this->is_autenticate = 1;
		$this->user 	= $user;
		$this->descr 	= $user;			//Todo (prendere da utente)
		$this->profile	= $profile;			//Todo (prendere da utente)
	}
	
	
	private function reset(){
		$this->is_autenticate = 0;
		$this->user = $this->profile = $this->email = null;		
	}
	
	public function logout(){
		$this->reset();
	}
	
	
	public function is_authenticate($force = 'N'){
		if ($force == 'Y')
			return $this->is_autenticate;
	
			return $this->is_autenticate || !$this->is_system_configured();
	}
	
	
	
	public function is_from_supply_desk(){
		return $this->from_supply_desk;
	}
	
	
	public function is_sec_admin(){
	    if (trim($this->tipologia) == 'SECAM')
	        return 1;
	        else
	            return 0;
	}
	
	public function is_admin(){
		if (trim($this->tipologia) == 'AMM')
			return 1;
		else
			return 0;
	}	
	
	public function is_operatore_aziendale(){
		if (trim($this->tipologia) == 'AMM' || trim($this->tipologia) == 'USER')
			return 1;
		return 0;
	}
	
	
	public function out_name(){
		return $this->descr;
	}	
	
	public function get_user(){
		return $this->user;
	}	
	
	public function get_email(){
		return $this->email;		
	}	
	
	public function get_profile(){
		return $this->profile;
	}

	public function get_tipologia(){
		return trim($this->tipologia);
	}	
	
	public function get_cod_riservatezza(){
		return trim($this->cod_riservatezza);
	}	
	public function get_cod_riservatezza2(){
	    return trim($this->cod_riservatezza2);
	}
	public function get_cod_riservatezza3(){
	    return trim($this->cod_riservatezza3);
	}
	public function get_cod_riservatezza4(){
	    return trim($this->cod_riservatezza4);
	}
	public function get_cod_riservatezza5(){
	    return trim($this->cod_riservatezza5);
	}
	
	public function authenticate($req = array()){
		if ($req['fn'] == 'login'){
			$this->reset();
			$user = $this->find_user_in_table($req['user'], $req['password']);

			if (!is_null($user) && strlen($user['UTPRAC'])>0){								
				$this->is_autenticate = 1;
				$this->user 	= $user['UTCUTE'];
				$this->profile	= $user['UTPRAC'];
				$this->tipologia= $user['UTTPUT'];				
				$this->descr	= $user['UTDESC'];
				$this->email	= $user['UTMAIL'];
				$this->cod_riservatezza	= $user['UTCOGE'];
				$this->cod_riservatezza2	= $user['UTCOG2'];
				$this->cod_riservatezza3	= $user['UTCOG3'];
				$this->cod_riservatezza4	= $user['UTCOG4'];
				$this->cod_riservatezza5	= $user['UTCOG5'];
				
				global $authTrimData;
				if ($authTrimData == 'Y') {
					$this->user 	= trim($this->user);
					$this->profile	= trim($this->profile);
					$this->tipologia= trim($this->tipologia);
					$this->descr	= trim($this->descr);
					$this->email	= trim($this->email);
					$this->cod_riservatezza	   = trim($this->cod_riservatezza);
					$this->cod_riservatezza2   = trim($user['UTCOG2']);
					$this->cod_riservatezza3   = trim($user['UTCOG3']);
					$this->cod_riservatezza4   = trim($user['UTCOG4']);
					$this->cod_riservatezza5   = trim($user['UTCOG5']);
					
				}
				
				
				
				$this->from_supply_desk = 0;
				
				//aggiorno le sessioni nelal tabella utenti
				$this->user_in_table_after_login($this->user);
				
				return;				
			}
			else {
				$this->reset();
			}	
		}
		if ($req['fn'] == 'logout'){

			//aggiorno le sessioni nelal tabella utenti
			$this->user_in_table_after_logout($this->user);

			//pulisco la sessione
			$this->reset();				
		}		
		return;		
	}
	
	
	private function find_user_in_table($user, $password){
		global $conn, $cfg_mod_Admin;
		$sql = "SELECT * FROM " . $cfg_mod_Admin['file_utenti'] ;
		$sql .= " WHERE UTCUTE =" . sql_t($user) . " AND UTPSW = " . sql_t($password);

		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		$n_rows = db2_num_rows($stmt);
		
		$row = db2_fetch_assoc($stmt);
		
		return $row;
	}
	
	
	private function user_in_table_after_login($user){
		global $conn, $cfg_mod_Admin;
		$sql = "UPDATE {$cfg_mod_Admin['file_utenti']}
				SET UTDTIS = ?, UTORIS = ?,
				    UTDTFS = 0, UTORFS = 0,
				    UTNCON = UTNCON + 1
				WHERE UTCUTE = ?";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array(oggi_AS_date(), oggi_AS_time(), $user));
		return $row;
	}	
	
	private function user_in_table_after_logout($user){
		global $conn, $cfg_mod_Admin;
		$sql = "UPDATE {$cfg_mod_Admin['file_utenti']}
				SET
				UTDTFS = ?, UTORFS = ?
				WHERE UTCUTE = ?";
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array(oggi_AS_date(), oggi_AS_time(), $user));
		return $row;
	}	
	
	
	private function find_profile_in_table(){
		global $conn, $cfg_mod_Admin;
		$sql = "SELECT * FROM " . $cfg_mod_Admin['file_tabelle'] ;
		$sql .= " WHERE TATAID = 'PRAC' AND TAKEY1 = " . sql_t($this->profile);
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		$n_rows = db2_num_rows($stmt);
	
		$row = db2_fetch_assoc($stmt);
	
		return $row;
	}
	
	
	public function ha_auto_modulo(){
		$profilo = $this->find_profile_in_table();
		if (strlen(trim($profilo['TARIF1'])) >0 )
			return trim($profilo['TARIF1']);
		else
			return false;
	}
	

	public function verify_module_permission($module){
		global $conn, $cfg_mod_Admin;
		$sql = "SELECT * FROM " . $cfg_mod_Admin['file_moduli_utente'] ;
		$sql .= " WHERE UPPRAC =" . sql_t($this->profile) . " AND UPCMOD = " . sql_t($module);
	
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		
		$row = db2_fetch_assoc($stmt);
		
		return $row;
	}
	
	
	public function list_module_function_permission($module, $function = null, $tataid = 'UTMOP'){
		global $conn, $cfg_mod_Admin, $id_ditta_default;
		$sql = "SELECT * FROM " . $cfg_mod_Admin['file_tabelle'] ;
		$sql .= " WHERE TADT='{$id_ditta_default}' AND TATAID = '{$tataid}' AND TAKEY1 = " . sql_t($this->user);
		$sql .= "   AND TAKEY2 = " . sql_t($module);
		$ret = array();
		
		if (isset($function))
			$sql .= "   AND TAKEY3 = " . sql_t($function);
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
	
		while ($row = db2_fetch_assoc($stmt)) {
			$ret[trim($row['TAKEY3'])] = $row;
		}
		
		if (isset($function))
			return $ret[trim($row['TAKEY3'])];
		else
			return $ret;
	}
	

	
	public function is_config($module, $function = null, $tataid = 'UTMOP'){
	    global $conn, $cfg_mod_Admin, $id_ditta_default;
	    $sql = "SELECT COUNT(*) AS C_ROW FROM " . $cfg_mod_Admin['file_tabelle'] ;
	    $sql .= " WHERE TADT='{$id_ditta_default}' AND TATAID = '{$tataid}' ";
	    $sql .= "   AND TAKEY2 = " . sql_t($module);
	    
	    if (isset($function))
	        $sql .= "   AND TAKEY3 = " . sql_t($function);
	  
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if($row['C_ROW'] == 0)
           return false;
        else 
           return true;
}
	
	
	public function get_module_parameters($module){
		global $conn, $cfg_mod_Admin;
		$sql = "SELECT * FROM " . $cfg_mod_Admin['file_moduli_utente'] ;
		$sql .= " WHERE UPPRAC =" . sql_t_trim($this->profile) . " AND UPCMOD = " . sql_t_trim($module);
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
	
		$row = db2_fetch_assoc($stmt);
		
		if ($row!=false){
			if (strlen(trim($row['UPPARA'])) == 0)
				$ret = json_decode("{}");
			else
				$ret = json_decode($row['UPPARA']);
			
			$ret->p_solo_inter 			= $row['UPFLSI'];
			$ret->p_acc_stat 			= $row['UPFLSS'];
			$ret->p_mod_param 			= $row['UPFLMP'];
			$ret->p_nascondi_importi	= $row['UPFLNI'];			
			$ret->p_disabilita_Info		= $row['UPFG01'];
			
			
			return $ret;
		}
		return array();
	}	
	
	
	public function get_auto_module(){
		global $conn, $cfg_mod_Admin;
		$sql = "SELECT * FROM " . $cfg_mod_Admin['file_moduli_utente'] ;
		$sql .= " WHERE UPPRAC =" . sql_t_trim($this->profile) . " AND UPCMOD = " . sql_t_trim($module);
	
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
	
		$row = db2_fetch_assoc($stmt);
	
		if ($row!=false){
			if (strlen(trim($row['UPPARA'])) == 0)
				$ret = json_decode("{}");
			else
				$ret = json_decode($row['UPPARA']);
				
			$ret->p_solo_inter 			= $row['UPFLSI'];
			$ret->p_acc_stat 			= $row['UPFLSS'];
			$ret->p_mod_param 			= $row['UPFLMP'];
			$ret->p_nascondi_importi	= $row['UPFLNI'];
			$ret->p_disabilita_Info		= $row['UPFG01'];			
				
			return $ret;
		}
		return array();
	}	
	
	
	public function is_system_configured(){
		
		global $conn, $cfg_mod_Admin;
		
		$sql = "SELECT COUNT(*) AS NUM_UTENTI FROM " . $cfg_mod_Admin['file_utenti'] ;
	
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);
		
		$row = db2_fetch_assoc($stmt);
		
		if ($row['NUM_UTENTI'] > 0)
			return true;
		else
			return false;

	}	
	

	public static function get_session_auth(){
	 	if (!isSet($_SESSION['auth'])){
			$_SESSION['auth'] = new Auth();
		}
	 return $_SESSION['auth']; 		
	}
	

	public function verify_module($mod_cod){
		
	}
	
	
	
	public function update_table_user_session(){				
		global $conn, $cfg_mod_Admin;
		
		//recupero il modulo richiesto
		$exp_path = explode("/", $_SERVER['SCRIPT_NAME']);
		
		//non registro la funzione di aggiornamento stato elaborazione
		if (strtolower($exp_path[4]) == 'get_stato_aggiornamento.php')
			return;
		
		$mod = '';
		if (strtolower($exp_path[2]) == 'module')
			$mod = substr($exp_path[3], 0, 20);
		else
			$mod = substr($exp_path[2], 0, 20);
		
		//rimuovo eventuale .php finale
		if (strrpos($mod,".php") != false)
			$mod = basename($mod, ".php");
		
		$sql = "UPDATE {$cfg_mod_Admin['file_utenti']}
				SET	UTDTFSP = ?, UTORFSP = ?, UTCMOD = ?
				WHERE UTCUTE = ?";
		
				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt, array(oggi_AS_date(), oggi_AS_time(), $mod, $this->user));
				
		return true;
	}
	
	
 } //class

?>