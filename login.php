<?php
require_once "config.inc.php";

$cod_auto_mod = $auth->ha_auto_modulo();

switch($_REQUEST['fn']){
	case 'login':
		$auth->authenticate($_REQUEST);		
		break;
	case 'logout':
		session_destroy();
		$auth->authenticate($_REQUEST);
		break;
} //switch	

/*
 * la login come supply_desk e' gestita come visualizzazione diversa con parametro da config.inc.php
if ($cod_auto_mod)
	header("location: ../supply_desk/index.php");	
else
*/

	header("location: index.php");
  

?>