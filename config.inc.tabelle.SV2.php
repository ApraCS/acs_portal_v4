<?php 

$cfg_Global = array(
    "file_AH"      => "{$libreria_predefinita}.WPIGAH0",
);

//********************************************************************************
// DESKTOP VENDITE
//********************************************************************************
$cfg_mod_Spedizioni = array(
    
    "tipo_calendario"	                    => "*CS",
    "bl_commenti_ordine"                    => "SPE",       //nelle righe ordine
    
    //Allegati per ordine
    "visualizza_order_img"	=> "N",                         //disabilitati
    //"url_img_FROM_PC"		=> "http://192.168.2.77:82/acs_portal_img/get_order_images.php",
    
    //  "stati_carico_non_ammessi" => array('23', 'AB'),   
    //"stati_programmabili_HOLD" => array('CF', 'RI'),          
    //"disabilita_digits_on_PL0" => 'Y',                    //default: 'N'        
    //"print_barcode_piano_di_carico"	=> 'Y',             //default: 'N'
    
    //"perc_magg_volume_std" => 16,                         //default: 0
    //"aree_abilitate_a_filtro_sped" => array('EST'),       //default: empty (forza filtro spedizioni prima di apertura day da PLAN)
    //"no_refresh_dopo_ASS_SPED" => 'Y',		            //default: 'N' (dopo ASS_SPED non faccio refresh ma barro righe selezionate (solo INFO?)
    //"INFO_blocca_ordine"	=> 'N',			                //default: 'Y' (abilitato)
    //"INFO_sblocca_ordine"   => 'Y',			            //default: 'N' (disabilitato)
    //"SBLOC_use_check_list" => 'Y',                        //default: 'N'
    //"invia_dati_spedizione_a_mbm" => 'Y',	                //default: 'N' _mbm o _sav
    
    //abilita_invia_dati_dc_spedizione_trasportatore        //default: 'N' (invio dati sped in base a config trasportatore)
    
    //"an_cliente_per_ditta_origine" => 'Y',                //default: 'N' (group by anche per TDDTOR)
    
    //"abilita_assegna_posizione" => 'Y',                   //default: 'N'
    //"forza_scelta_itinerario_su_ASS_SPED_da_INFO" => 'Y', //default: 'N'
    //"disabilita_tutti_vincoli_su_assegna_sped" => 'N',    //default: 'N'
    //"ASS_SPED_disabilita_verifica_colli_spuntati" => 'Y'  //default: 'N'
    
    //integrazione lotto <-> indice rottura
    //"scrivi_END_indice_rottura" 		=> 'N',             //default: 'N'
    //"imposta_lotto_da_indice_rottura" 	=> 'N',         //default: 'N' 
    
    //"force_use_session_history" => 'Y',                   //default: 'N'
    //"overflow_SBLOC_per_area" => 'N',                     //default: 'Y'
    //"nr_max_in_el_spedizioni" => 9,                       //default: 200
    //"disabilita_sprintf_codice_cliente" => 'N',           //default: 'N'
    //"default_calendar_field" => "VOL",                    //default: 'VOL'
    //"sum_su_TDFN10"		 => "Y",                        //default: 'Y'
    //"gestione_per_riga"	 => "N",                        //default: 'N' (ad esempio in report scarichi mostro il dettaglio righe    
    
    //"tipo_carico"			=> '11111',                     //default: 'CA'
    //"carico_per_data"	 	=> "Y",                         //default: 'N' (ritorna il carico nel formato mmggnr
    
    //"report_scarichi_note_sotto_cliente" => "Y",          //default: 'N'
    
    //"IMMOR_mostra_liv_cliente" => "Y",                      //default: 'N'
    
    //"add_where_to_elenco_articoli_promo" => " AND ARST23 IN ('M', 'L', 'S') ",
    
    //
    //destinazioni GL
    //"file_anag_cli_des_escludi"  => array('01', '02', '03', '05', '06', '07', '08', '09', '10'),
    
    //in proprieta ordine visualizza campo selezione cliente (da GC):
    //"visualizza_campo_selezione_cliente" => array('1' => true, '2' => true),
    
    /* filtro su ricerca ordini gestionale */
    'doc_gest_search' => array(
        'CONTRACT_abbina' => array(
            'TDTIDO'     => array('VC'),
            'TDSTAT'     => array('AA'),
            'TDTPDO'     => array('CN'),
            'abilita_assegna_stato_CC' => 'Y'
        ),
        
        'ALLESTIMENTO_abbina' => array(
            'TDTIDO'     => array('VC'),
            'TDSTAT'     => array('AA'),
            'TDTPDO'     => array('AL'),
        ),
        
        'WEBMAIL_abbina' => array(
            'TDTIDO'     => array('VO'),
            'TDSTAT'     => array('AA'),
            //'TDTPDO'     => array('AL'),
        ),
      
    ),
    
    'doc_wpi_search' => array(
        'UPDATE_agente_provvigioni' => array(
            'TDTIDO'     => array('VO'),
            'abilita_filtro_divisione' => 'Y',
            'abilita_filtro_tipologia' => 'Y',
            'abilita_filtro_agente' => 'Y',
            'non_evasi' => 'Y',
            'multiselect' => true
        )
    ),
    'stato_abilita_chiusura_oe' => 'ME',
    'software_progettazione_grafica' => '3CAD',
    
    
    //--------------------------------------------------------------------------------
    //                         ***** FILE UTILIZZATI *****
    //--------------------------------------------------------------------------------    
    
    // ---------------- desktop ----------------------------
    "file_testate"                          => "{$libreria_predefinita}.WPI0TD0",
    "file_righe"                            => "{$libreria_predefinita}.WPI0RD0",
    "file_righe_note_cli"                   => "{$libreria_predefinita}.WPI0RD1", //logico filtrato per NCSV2
    "file_calendario" 	                    => "{$libreria_predefinita}.WPI0CS0",
    "file_tabelle" 		                    => "{$libreria_predefinita}.WPI0TA0",
    "file_numeratori"	                    => "{$libreria_predefinita}.WPI0NR0",
    "file_carichi"	 	                    => "{$libreria_predefinita}.WPI0PS0",

    
    "file_assegna_ord"                      => "{$libreria_predefinita}.WPI0AS0",
    "file_capacita_produttiva"              => "{$libreria_predefinita}.WPI0CP0",    
    "file_coordinate_geografiche"           => "{$libreria_predefinita}.WPI0GL2",    
    
    "file_promemoria"	                    => "{$libreria_predefinita}.WEMAIL0",
    "file_listino_trasportatori"            => "{$libreria_predefinita}.WPI0LT0",
    "file_richieste"                        => "{$libreria_predefinita}.WPI0RI0",
    
    "file_deroghe"	 	                    => "{$libreria_predefinita}.WPI0AD0",
    "file_note"				                => "{$libreria_predefinita}.WPI0NT0",
    
    "file_analisi_esposizione"				=> "{$libreria_predefinita}.WESPT0WPI",
    "file_analisi_esposizione_dett"			=> "{$libreria_predefinita}.WESPD0WPI", //ordini
    "file_analisi_esposizione_cont_dett"	=> "{$libreria_predefinita}.WESPR0WPI", //dati contabili
    
    "file_riservatezza"                     => "{$libreria_predefinita}.WPI0PG0",
    "file_parametri_operatori_OE"           => "{$libreria_predefinita}.WPI0OE0",
    
    "ins_new_anag" 	=> array(
        "file_appoggio"  	=> "{$libreria_predefinita}.WPI0GC0",
        "file_appoggio_cc"  => "{$libreria_predefinita}.WPI0CC0",
        "file_appoggio_crm"  => "{$libreria_predefinita}.WPI0GC0",
        "file_cerved"		=> "{$libreria_predefinita}.WPI0IC0"
    ),
    


    // ---------------- sistema ----------------------------
    "file_anag_cli" 	            => "{$libreria_predefinita}.XX0S2CF0",
    "file_anag_cli_des"             => "{$libreria_predefinita}.XX0S2TA0",
    
    "file_testate_doc_gest"         => "{$libreria_predefinita}.XX0S2TD0",
    "file_testate_doc_gest_cliente" => "{$libreria_predefinita}.XX1S2TDL",
    "file_righe_doc_gest" 	        => "{$libreria_predefinita}.XX1S2RD1",
    "file_righe_doc_gest_valuta" 	=> "{$libreria_predefinita}.XX0S2RT0",    
    "file_cronologia_ordine"        => "{$libreria_predefinita}.XX0S2PQ0",
    "file_colli"                    => "{$libreria_predefinita}.XX0S2PL0",
    "file_colli_dc"                 => "{$libreria_predefinita}.XX0S2PS0",    
    "file_righe_dom_risp"           => 	"{$libreria_predefinita}.XX0S2VD0",
    "file_righe_componenti"         =>  "{$libreria_predefinita}.XX1S2RE5",    
    
    "file_disponibilita"            => "{$libreria_predefinita}.WMWF30",
    "file_giacenze" 	            => "{$libreria_predefinita}.WMWF20",
    "file_testate_imp" 	            => "{$libreria_predefinita}.WMWF10",
    "file_anag_art_trad"           => "{$libreria_predefinita}.XX0S2AL0",
    "file_tab_sys" 	 		 	   => "{$libreria_predefinita}.XX0S2TA0",
    "file_wizard_MTS_2"		       => "{$libreria_predefinita}.WPI1M20",
    "file_proposte_MTO"		       => "{$libreria_predefinita}.XX0S2MT0",
    "file_dettaglio_wizard"        => "{$libreria_predefinita}.WPI1M30",
    "file_testate_commessa"	       => "{$libreria_predefinita}.XX0S2TE0",
    "file_ticket_no_std"	       => "{$libreria_predefinita}.XX0S2TK0",
    "file_righe_ticket_no_std"	   => "{$libreria_predefinita}.XX1S2RDD"
       
    //VARIE E TEST
    
    //"call_odbc_after_history" => "CALL SV2EXE.UR21H1C",
    //"call_odbc_after_history" => "CALL QSYS.QCMDEXC('CRTLIB LIB(PTF_MATTE7)', 0000000022.00000)",

    
     // TEST PER GL (protocollazione)
   /*  "file_anag_cli" 	 => 'E42E_$FILX.$BAANAF0',
     "file_anag_cli_des"  => 'E42E_$FILX.$BFINDF0',*/
   /* "protocollazione" 	=> array(
        'tipi_ordine_TACOR2' => array('VO', 'VC'),
       
    ),*/

);

//quali tipi ordini mostrare in protocollazione (in class.SpedProtocollazione.php)
//$cfg_mod_Spedizioni["protocollazione"]["tipi_ordine_TACOR2"] = array('VO', 'VC');

$cfg_protocollazione = array(
    
    'heading'   => array(
        
        'TDTPDO_TACOR2' => array('VO', 'VC'),      //filtro tipi ordine (in base a campo TACOR2 dei tipi ordine)
        
        'VO' => array(
            'prio' => array(
                'def'    => 'C',
                'allowBlank' => false 
            ),
            'stdo' => array(
                'def'    => 'AA',
                'list'   => array('PE', 'CO', 'AA', 'AP')
            ),
            'dtva' => array(
                'def'    => '',
                'allowBlank' => true,
                'hidden' => true
            ),
            'f_pven' => array(
                'def'    => '',
                'allowBlank' => false
            )
        ),
        'VC' => array(
            'prio' => array(
                'def'    => 'C',
                'allowBlank' => true 
            ),
            'stdo' => array(
                'def'    => 'AA'
            ),
            'dtva' => array(
                'def'    => oggi_AS_date(),
                'allowBlank' => false,
                'hidden' => false
            ),
            'f_pven' => array(
                'def'    => '',
                'allowBlank' => false
            )
        )
    ),
    
    //protocollazione std da info base dati
    'base_dati' => array(
        
        'TDTPDO' => array('DD', 'SC'),       //filtro tipi ordine (per codice tipo ordine)
        'TDTPDO_TACOR2' => array('VO'),      //filtro tipi ordine (in base a campo TACOR2 dei tipi ordine)
        
        //filtro stato ordini
        'TDSTAT' => array('AA'),
        'VO' => array(
            'prio' => array(
                'def'    => '5',
                'allowBlank' => false
            ),
            'stdo' => array(
                'def'    => 'AA',
                'list'   => array('PE', 'CO', 'AA', 'AP')
            ),
            'dtva' => array(
                'def'    => '',
                'allowBlank' => true,
                'hidden' => true
            ),
        ),
        'VC' => array(
            'prio' => array(
                'def'    => '5',
                'allowBlank' => true
            ),
            'stdo' => array(
                'def'    => 'AA'
            ),
            'dtva' => array(
                'def'    => oggi_AS_date(),
                'allowBlank' => false,
                'hidden' => false
            ),
        )
    )
);


/*
$cfg_info_columns_properties = array(
    "info_hidden_colli_DS" => "Y", //campo colli D/S per info list
    "info_show_vettore" => "Y", //campo vettore per info list
    "info_show_architetto" => "Y", //campo architetto per info list
);
*/

//********************************************************************************
// DESKTOP ACQUISTI
//********************************************************************************
$cfg_mod_DeskAcq = array(
        "file_tab_sys" 	 		 => "{$libreria_predefinita}.XX0S2TA0",
		"file_regole_riordino" 	 => "{$libreria_predefinita}.WPI0CR0",
		"file_testate" 		 => "{$libreria_predefinita}.WPI1TD0",
		"file_righe" 		 => "{$libreria_predefinita}.WPI1RD0",
		"file_righe_doc_gest" => "{$libreria_predefinita}.XX1S2RD1",
		"file_righe_doc_gest_valuta" 	 => "{$libreria_predefinita}.XX0S2RT0",
		"file_calendario" 	 => "{$libreria_predefinita}.WPI1CS0",
		"tipo_calendario"	 => "*CS",
		"file_tabelle" 		 => "{$libreria_predefinita}.WPI1TA0",
		"file_numeratori"	 => "{$libreria_predefinita}.WPI1NR0",
		"file_cronologia_ordine"   => "{$libreria_predefinita}.XX0S2PQ0",
        "visualizza_order_img"	=> "N",

		"file_commenti_ordine" => "{$libreria_predefinita}.WPI1RD0", //nelle righe ordine
		"bl_commenti_ordine" => "DORA", //nelle righe ordine
		"file_anag_art" 	 => "{$libreria_predefinita}.XX0S2AR0",
		"file_richieste"	 => "{$libreria_predefinita}.WPI1RI0",

		//LIBRERIE WMAF
        "file_disponibilita" 	=> "{$libreria_predefinita}.WMWF30",
        "file_giacenze" 	 	=> "{$libreria_predefinita}.WMWF20",
		"file_WMAF10"	 => "{$libreria_predefinita}.WMWF10",
		"file_WMAF20"	 => "{$libreria_predefinita}.WMWF20",
		"file_WMAF30"	 => "{$libreria_predefinita}.WMWF30",
		
		// 	"file_analisi_rivendita_promo" => "STO2M643.WBR26D40",
		"file_analisi_rivendita_promo" => "STO2M643.WBR26D70",
		
		"file_orari_disponibili"	=> "{$libreria_predefinita}.WPI1OD0",
		"file_proposte_MTO"		=> "{$libreria_predefinita}.XX0S2MT0",
		"def_area"				=> "A01",
		
		"file_wizard_MTS_2"		=> "{$libreria_predefinita}.WPI1M20",
		"file_wizard_MTS_3"		=> "{$libreria_predefinita}.WPI1M30",
		"file_testate_fatture"		=> "{$libreria_predefinita}.WPI1TF0",
		"file_righe_fatture"		=> "{$libreria_predefinita}.WPI1RF0",
		"file_to_fatture"		=> "{$libreria_predefinita}.WPI1TO0",
		"file_anag_cli" 	    => "{$libreria_predefinita}.XX0S2CF0",
		"file_scadenze"				=> "{$libreria_predefinita}.XX0S2PA0",
		"field_importo_scadenze" => "PAIMPV", 
		"file_note_anag"        => "{$libreria_predefinita}.XX0S2RL0",
		"ins_new_anag" 	=> array(
		    "file_appoggio"  	=> "{$libreria_predefinita}.WPI1GC0",
		    "file_appoggio_cc"  => "{$libreria_predefinita}.WPI1CC0",
		    "file_appoggio_crm"  => "{$libreria_predefinita}.WPI1GC0",
		    "file_cerved"	 => "{$libreria_predefinita}.WPI1CC0"
		),
		
		"file_assegna_ord"      => "{$libreria_predefinita}.WPI1AS0",
		"file_richieste_esito"  => "{$libreria_predefinita}.WPI0OC0"
		
		//abilita_digits_on_RD2RF => "Y"              //default: "N"        
);



//********************************************************************************
// DESKTOP UTILITY
//********************************************************************************
$cfg_mod_DeskUtility = array(
		"file_tab_sys" 	 		 	=> "{$libreria_predefinita}.XX0S2TA0",
		"file_tabelle" 	 		 	=> "{$libreria_predefinita}.WPI0TA0",
		"file_tabelle_acq" 	 	 	=> "{$libreria_predefinita}.WPI1TA0",
		"file_anag_art" 	 		=> "{$libreria_predefinita}.XX0S2AR0",
		"file_anag_art_sst" 		=> "{$libreria_predefinita}.XX1S2AR9",
		"file_est_anag_art" 	 	=> "{$libreria_predefinita}.XX0S2AX0",
		"file_est_anag_art_utente" 	=> "{$libreria_predefinita}.XX0S2AU0",
		"ins_new_anag" 	=> array(
				"file_appoggio"  	=> "{$libreria_predefinita}.WPI0GC0",
				"file_appoggio_cc"  => "{$libreria_predefinita}.WPI0CC0",
				"file_appoggio_crm"  => "{$libreria_predefinita}.WPI0GC0",
				"file_cerved"		=> "{$libreria_predefinita}.WPI0IC0"
		),
		#scorte RAV
		/*"file_fornitori_esistenti" 	=> "{$libreria_predefinita_scorte}.WPI1ANF0",
		"file_fornitori" 	 		=> "{$libreria_predefinita_scorte}.WPI1FOF0",
		"file_articoli" 	 		=> "{$libreria_predefinita_scorte}.WPI1SKF0",
		"file_scorte" 	 		 	=> "{$libreria_predefinita_scorte}.WPI1SCF0",
		"file_stag_mese" 	 		=> "{$libreria_predefinita_scorte}.WPI1SMF0",
		"file_art_sost" 	 		=> "{$libreria_predefinita_scorte}.WPI1SOF0",*/
		"file_listini" 	 		 	=> "{$libreria_predefinita}.XX0S2LI0",
		"file_listini_liraq"        => "{$libreria_predefinita}.XX0S2LR0",
		"file_listini_licon"        => "{$libreria_predefinita}.XX0S2LC0",
		"file_cicli" 	 		 	=> "{$libreria_predefinita}.XX0S2CI0",
		"file_cicli_ricom" 	 		=> "{$libreria_predefinita}.XX0S2RI0",
		"file_condizioni_cicli" 	=> "{$libreria_predefinita}.XX0S2CV0",
		"file_config" 	 		 	=> "{$libreria_predefinita}.XX0S2MD0",
		"file_valori_ammessi"	 	=> "{$libreria_predefinita}.XX0S2MO0",
		"file_distinta_base" 	 	=> "{$libreria_predefinita}.XX0S2DB0",
		"file_condizioni_distinta"  => "{$libreria_predefinita}.XX0S2DV0",
		"file_assegna_ord"          => "{$libreria_predefinita}.WPI0AS0",
		"file_anag_art_trad"        => "{$libreria_predefinita}.XX0S2AL0",
		"file_note_anag"        => "{$libreria_predefinita}.XX0S2RL0",
		"file_cronologia"        => "{$libreria_predefinita}.WPI0JM0",
		"file_variabili"        => "{$libreria_predefinita}.XX0S2AM0",	
		"file_monitoraggio"        => "{$libreria_predefinita}.WGMSV0",	
		"ar_lingue" 	=> array(
		    array('id' => 'ING', 'text' => 'Inglese'),
		    array('id' => 'FRA', 'text' => 'Francese')
		)
		
); 


//********************************************************************************
// PUNTO VENDITA
//********************************************************************************
$cfg_mod_DeskPVen = array(
    "file_righe" 		         => "{$libreria_predefinita}.WPI0RD0",
    "file_righe_doc_gest" 	     => "{$libreria_predefinita}.XX1S2RD1",
    "file_righe_doc_gest_valuta" => "{$libreria_predefinita}.XX0S2RT0",
    "file_righe_doc_gest_CAPARRA"  => "{$libreria_predefinita}.XX1S2RDB",    
    "file_anag_art" 	 	     => "{$libreria_predefinita}.XX0S2AR0",
    "file_anag_cli" 	         => "{$libreria_predefinita}.XX0S2CF0",
    "file_tabelle" 	 	         => "{$libreria_predefinita}.WPI0TA0",
    "file_tab_sys" 	 		     => "{$libreria_predefinita}.XX0S2TA0",
    "file_testate"      	     => "{$libreria_predefinita}.WPI0TD0",
    "file_testate_doc_gest"      => "{$libreria_predefinita}.XX0S2TD0",
    "file_testate_gest_valuta"   => "{$libreria_predefinita}.XX0S2TO0",
    "file_anag_art_trad"         => "{$libreria_predefinita}.XX0S2AL0",
    "file_allegati_righe"        => "{$libreria_predefinita}.XX0S2RA0",
    "file_righe_doc"             => "{$libreria_predefinita}.XX0S2RD0",
    "file_listini" 	 		 	 => "{$libreria_predefinita}.XX0S2LI0",
    "file_assegna_ord"           => "{$libreria_predefinita}.WPI0AS0",
    "file_history_att"           => "{$libreria_predefinita}.WPI0AH0",
    "bl_commenti_ordine"         => "SPE", //nelle righe ordine
    "pv_commenti_delivery"       => "DEL", //nelle righe ordine
    "traduzione"		         => "MIDI", //lingua per descrizione articoli
    "info_importo_field"         => "TDINFI", //campo importo per info list
    
    "fatture_anticipo" 	=> array(
        "file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
        "file_righe" 	 => "{$libreria_predefinita}.WPI0RF0"
    ),
    
    "allegati_root_C"            => "/SV2/ORDINI/",
    "allegati_root_F"            => "/SV2/ORDACQ/",
    "report_ec"                  => "GL_CLI",
    
    "ar_tipo_by_doc_type" 	=> array(
        'O' => array('MO', 'MA', 'ME'),
        'P' => array('MP')
    ),
    
    //"add_where_to_elenco_articoli" => " AND ARART like '111%' "
    
    "file_ubicazioni"           => "{$libreria_predefinita}.XX0S2BC0",
    "file_proposte_MTO"	           	 => "{$libreria_predefinita}.XX0S2MT0",
    "file_movimenti_gest" 	         => "{$libreria_predefinita}.XX0S2MV0",
    "file_movimenti_gest_estensione" => "{$libreria_predefinita}.XX0S2ME0",
    "file_dettaglio_wizard"        => "{$libreria_predefinita}.WPI1M30"
        
    //gen_fat_acc_vet_def          => 'VET'
 );




//********************************************************************************
// MOD GEST
//********************************************************************************
$cfg_mod_Gest = array(
    "file_testate"           => "{$libreria_predefinita}.WPI0TD0",
    "file_testate_doc_gest"  => "{$libreria_predefinita}.XX0S2TD0",
    "file_tabelle" 	 		 => "{$libreria_predefinita}.WPI0TA0",
    "file_tab_sys" 	 		 => "{$libreria_predefinita}.XX0S2TA0",
    "file_anag_cli" 	    => "{$libreria_predefinita}.XX0S2CF0",
    "file_ABI"		 => "{$libreria_predefinita}.ABIF0",
    "file_CAB"		 => "{$libreria_predefinita}.CABF0",
    "abi"	=> array(
        "file_conti" 	 => "ANALISICE.WPI0CM0",
        "file_movimenti" => "ANALISICE.WPI0MM0",
    ),
    "flussi_cassa" 	=> array(
        "disabilitato" => "N",
        //"file_conti" 	 => "ANALISICE.FLUSSTF0",
        //"file_movimenti" => "ANALISICE.FLUSSIF0",
        "file_conti" 	 => "{$libreria_predefinita}.WPI0FT0",
        "file_movimenti" => "{$libreria_predefinita}.WPI0FC0",
        "filtro_where" => 'N'
    ),
    "marginalita"	=> array(
        
    ),
    "provvigioni" 	=> array(
        "file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
        "file_righe" 	 => "{$libreria_predefinita}.WPI0RF0"
    ),
    "bollette_doganali" 	=> array(
        "file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
        "file_righe" 	 => "{$libreria_predefinita}.WPI0RF0",
        "anagrafiche_cliente_da_modulo" => 'Y',
        "file_assegna_ord"   => "{$libreria_predefinita}.WPI0AS0",
    ),
    "ins_new_anag" 	=> array(
        "file_appoggio"  	=> "{$libreria_predefinita}.WPI0GC0",
        "file_appoggio_cc"  => "{$libreria_predefinita}.WPI0CC0",
        "file_appoggio_crm"  => "{$libreria_predefinita}.WPI0GC0",
        "file_cerved"	 => "{$libreria_predefinita}.WPI0IC0",
        "file_listino_punti"  => "{$libreria_predefinita}.XX0S2CL0"
    ),
    "fatture_anticipo" 	=> array(
        "file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
        "file_righe" 	 => "{$libreria_predefinita}.WPI0RF0"
    ),
    "dichiarazione_intenti" 	=> array(
        "file_testate" 	 => "{$libreria_predefinita}.WPI0TF0",
        "file_righe" 	 => "{$libreria_predefinita}.WPI0RF0"
    ),
    "file_cerved_payline" => "{$libreria_predefinita}.SUD0CPR0",
    "file_tasco" => "{$libreria_predefinita}.XX0S2TS0",
    "cod_naz_italia" => 'IT',
    "file_assegna_ord"        => "{$libreria_predefinita}.WPI3AS0",
    "file_numeratori"	      => "{$libreria_predefinita}.WPI0NR0",
    "file_richieste_anag_cli" => "{$libreria_predefinita}.WPI0RA0",
    "file_testate_gest_valuta"   => "{$libreria_predefinita}.XX0S2TO0",
    "file_xspm"   => "{$libreria_predefinita}.XX0S2TV0",
    "file_scala_premi"   => "{$libreria_predefinita}.XX0T2SP0",
);




//********************************************************************************
// BOOKING
//********************************************************************************
$cfg_mod_Booking = array(
    "file_tabelle" 	 	=> "{$libreria_predefinita}.WPI0TA0",
    "file_indirizzi" 	=> "{$libreria_predefinita}.WPI0IG0",
    "file_testate"      => "{$libreria_predefinita}.WPI0TD0",
    "file_calendario" 	=> "{$libreria_predefinita}.WPI0CS0",
    "file_richieste" 	=> "{$libreria_predefinita}.WPI0BS0",
    
    "email_notifica_chiusura_scadute"         => 'm.arosti@apracs.it',
    "invia_cliente_notifica_chiusura_scadute" => 'N' //default 'Y'
);


//********************************************************************************
// ARTICOLI
//********************************************************************************
$cfg_mod_DeskArt = array(
    "file_tabelle" 	 	      => "{$libreria_predefinita}.WPI2TA0",
    "file_tabelle_sys" 	 	  => "{$libreria_predefinita}.XX0S2TA0",
    "file_cfg_distinte"       => "{$libreria_predefinita}.WPI2CD0",
    "file_cfg_distinte_liste" => "{$libreria_predefinita}.WPI2CL0",
    "file_anag_art" 	      => "{$libreria_predefinita}.XX0S2AR0",
    "file_anag_art_sst" 	  => "{$libreria_predefinita}.XX1S2AR9",
    "file_anag_art_formati"   => "{$libreria_predefinita}.WPI2AF0",
    "file_richieste"	      => "{$libreria_predefinita}.WPI2RI0",
    "file_richieste_anag_art" => "{$libreria_predefinita}.WPI2RA0",
    "file_anag_cli" 	      => "{$libreria_predefinita}.XX0S2CF0",
    "file_assegna_ord" 	      => "{$libreria_predefinita}.WPI2AS0",
    "file_numeratori" 	      => "{$libreria_predefinita}.WPI2NR0",
    "file_ricom" 	 	      => "{$libreria_predefinita}.XX0S2RE0",
    "file_fsc" 	 	          => "{$libreria_predefinita}.XX0S2FA0",
    "file_righe_doc_gest" 	  => "{$libreria_predefinita}.XX1S2RD1",
    "file_righe_doc_gest_valuta" 	 => "{$libreria_predefinita}.XX0S2RT0",
    "file_testate_doc_gest"   => "{$libreria_predefinita}.XX0S2TD0",
    "file_righe_commesse"     => "{$libreria_predefinita}.XX0S2RC0",
    "root_path"               => "/SV2/ARTICOLI/",
    "file_costi"              => "{$libreria_predefinita}.XX0S2AC0",
    "elab_configuratore" => array(
        "file_tabelle"          => "{$libreria_predefinita}.WPI2TA0",
        "file_richieste"        => "{$libreria_predefinita}.WPI4RI0",
        "file_config_testata"   => "{$libreria_predefinita}.WPI0TC0",
        "file_config_righe"     => "{$libreria_predefinita}.WPI0RC0"
    ),
    "file_parametri"     => "{$libreria_predefinita}.WPI0PI0",
    "file_esito_raffronto"      => "{$libreria_predefinita}.WPI2ER0",
    "file_proprieta"            => "{$libreria_predefinita}.XX0S2AF0",
    "file_ubicazioni"           => "{$libreria_predefinita}.XX0S2BC0",
    "file_import_t"           => "{$libreria_predefinita}.WPI2IT0",
    "file_import_d"           => "{$libreria_predefinita}.WPI2ID0",
  
);



//********************************************************************************
// DESKTOP PROGRAMMI DI PRODUZIONE
//********************************************************************************
$cfg_mod_DeskProd = array(
    
    "dir_duplica"             => 'DUP_PP',
    
    "file_tabelle"            => "{$libreria_predefinita}.WPI0TA0",
    "file_tabelle_prod"            => "{$libreria_predefinita}.WPI5TA0",
    "file_tab_sys" 	 		  => "{$libreria_predefinita}.XX0S2TA0",
    
    "taid_indici"             => "PPP",
        
    "ATTAV_TAKEY2"            => 'PP',
    "taid_progetto"           => 'PRPPJ',

    "file_richieste"          => "{$libreria_predefinita}.WPI5RI0",
    "file_richieste_prod"     => "{$libreria_predefinita}.WPI5RA0",
    
    "file_cfg_distinte"       => "{$libreria_predefinita}.WPI0CD0",
    "file_cfg_distinte_liste" => "{$libreria_predefinita}.WPI0CL0",
    "file_assegna_ord"        => "{$libreria_predefinita}.WPI5AS0",
    "file_assegna_ord_H"      => "{$libreria_predefinita}.WPI5AH0",
    "file_numeratori"	      => "{$libreria_predefinita}.WPI0NR0",
        
    "file_users"	          => "{$libreria_predefinita}.WPI0UT0",
    "file_ticket_T"	          => "{$libreria_predefinita}.WPI0AVT0",
    "file_ticket_R"	          => "{$libreria_predefinita}.WPI0AVR0",
    "file_ticket_N"	          => "{$libreria_predefinita}.WPI2RL0",
    "file_tabelle_man"	      => "{$libreria_predefinita}.WPI2TA0",
    
    "file_note"				  => "{$libreria_predefinita}.WPI0NT0",
    
    //file replicati in fase di generazione progetto (x lotto)
    "file_testate"            => "{$libreria_predefinita}.WPI5TD0",
    "file_colli"              => "{$libreria_predefinita}.XX5S2PL0",
    "file_righe_doc"          => "{$libreria_predefinita}.XX5S2RD0",
    "file_righe_doc_vd"       => "{$libreria_predefinita}.XX5S2VD0",
    "file_ricom" 	 	      => "{$libreria_predefinita}.XX5S2RE0",
    "file_note_gest" 	 	  => "{$libreria_predefinita}.XX5S2RL0",
    "file_gen_docu" 	 	  => "{$libreria_predefinita}.WGEND050"
    
);


//********************************************************************************
// ADMIN
//********************************************************************************
$cfg_mod_Admin = array(
    "file_utenti" 			 => "{$libreria_predefinita}.WPI0UT0",
    "file_tabelle" 		 	 => "{$libreria_predefinita}.WPI0TA0",
    "file_moduli_utente" 	 => "{$libreria_predefinita}.WPI0UP0",
    "file_app_log" 	 		 => "{$libreria_predefinita}.WPI0LG0",
    "file_app_log_history" 	 => "{$libreria_predefinita}.WPI0LH0",
    "file_tab_sys" 	 		 => "{$libreria_predefinita}.XX0S2TA0",
    
    "file_anag_cli" 	     => "{$libreria_predefinita}.XX0S2CF0",
    "file_anag_cli_des"      => "{$libreria_predefinita}.XX0S2TA0",
    
    "file_anag_art" 	     => "{$libreria_predefinita}.XX0S2AR0",
    
    "file_memorizzazioni"    => "{$libreria_predefinita}.WPI0DF0",
    
    //file appoggio (per sessione)
    "file_WPI0WS0"	         => "{$libreria_predefinita}.WPI0WS0",

    "file_attivita_utente"	 => "{$libreria_predefinita}.WPI0AU0",
    "file_help"				 => "{$libreria_predefinita}.WPI0HE0",
    "user_help_modify"		 => "marosti;GIO"    
);
