<?php

$file_path = '/FE/ILVALLEGATI/2019_42_010114.PDF';
$output_path = $file_path . '.txt';

/*
$file_content = file_get_contents($file_path);
$to_base64 = base64_encode($file_content);
echo $to_base64;
*/



$input_file = fopen($file_path, "rb");
$output_file = fopen($output_path, "w");
while(!feof($input_file))
{
    $plain = fread($input_file, 57 * 143);
    $encoded = base64_encode($plain);
    $encoded = chunk_split($encoded, 76, "\r\n");
    fwrite($output_file, $encoded);
    //echo $encoded;
}

fclose($input_file);
fclose($output_file);


/*
<?xml version="1.0" encoding="UTF-8"?>
<ordine>
  <numero>3</numero>
  <righe>
    <riga tipo="commento" da_stampare="Y">
      <num_riga>101</num_riga>
      <articolo>abcde</articol3>
      ....
    </riga>  
    <riga>
        <num_riga>10</num_riga>
        <articolo>abcdesssssssss</articolo>
        ....
    </riga>  
  </righe>
</ordine>



------ JSON -----
{
 ordine: {
   numero: 3,
   righe: [
   	{
   	 num_riga: 101,
   	 articolo: "abcde",
   	 }, {
   	 num_riga: 102,
   	 articolo: ffffff,
   	 tipo_riga: 'commento'
   	 }
   ]
 }
}


{ success: true, message: 'Ordine memoriizato', id_ordine: 3}
*/


  
