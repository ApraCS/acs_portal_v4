<?php 

echo "<pre>";

/*
$ordine = array();

$ordine['numero'] = 33;
$ordine['righe'] = array();
$ordine['righe'][] = array(
  'num_riga' => 35,
  'articolo' => 'abcde'
);
$ordine['righe'][] = array(
    'num_riga' => 38,
    'articolo' => 'fffff',
    'tipo_riga' => 'commento',
    'da_stampare' => true,
    'variabili' => array(
        'mod' => 'PIPPO',
        'col' => 'BIANCO'
    )
);


//print_r($ordine);

echo json_encode($ordine);
*/



$testo_json = '{"numero":33,"righe":[{"num_riga":35.23,"articolo":"abcde"},{"num_riga":38,"articolo":"fffff","tipo_riga":"commento","da_stampare":true,"variabili":{"mod":"PIPPO","col":"BIANCO"}}]}';
$ordine = json_decode($testo_json);

print_r($ordine);

foreach($ordine->righe as $kr => $r){
    echo "---------------------------\n";
    echo "({$kr}) Riga: {$r->num_riga}, Articolo: {$r->articolo}\n";
    
    if (isset($r->variabili)){
        foreach($r->variabili as $kv => $v){
            echo "Variabile [{$kv}]: {$v}\n";
        }
    }
    
}
