<?php

function write_main_tree($p){
    global $s;
    ?>
			{
				xtype: 'treepanel',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					    fields: ['trCls', 'task', 'liv', 'liv_data', 'liv_cod', 'liv_cod_out', 'liv_cod_qtip',
					    	'GCDT', 'GCPROG',
					    	'GCDCON', 'GCMAIL', 'GCWWW', 'GCMAI1', 'GCMAI2', 'GCMAI3', 'GCMAI4', 'GCTEL', 'GCTEL2', 'GCFAX',
					    	'GCINDI', 'GCCDCF', 'GCCDFI', 'GCPIVA', 'GCLOCA', 'GCTPAN', 'GCPROV'
					    ],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: <?php echo acs_module_url('anag_cli', 'c_main_tree_data') ?>,
							actionMethods: {read: 'POST'},
							timeout: 240000,
	                        
	                        reader: {
	                            root: 'children'
	                        },
	                        
							extraParams: {
							}
	                	    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		{text: 'Cliente', flex: 1, xtype: 'treecolumn', dataIndex: 'task', menuDisabled: true}
		    		, {text: 'Localit&agrave;', flex: 1, dataIndex: 'GCLOCA'}					
		    		, {text: 'Indirizzo', flex: 1, dataIndex: 'GCINDI'}
		    		, {text: 'Provincia', flex: 1, dataIndex: 'GCPROV'}
					, {text: 'Codice', width: 100, dataIndex: 'GCCDCF'}		    			   	
					, {text: 'Cod.Fiscale', width: 150, dataIndex: 'GCCDFI'}
					, {text: 'P.IVA', width: 130, dataIndex: 'GCPIVA'}
				],
				enableSort: true,
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
					    celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  
						    console.log('dblclick');
						  	iEvent.stopEvent();
						  
	            			my_tree = iView;
							my_listeners_upd_dest = {
	        					afterInsertRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_tree.getTreeStore().load();		        						
	        						from_win.close();
					        		}
			    				};					  
						  
						  
							rec = iView.getRecord(iRowEl);
							
								if (rec.get('GCTPAN').trim() == 'DES') {
									acs_show_win_std('Scheda destinazione', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 650, 550, my_listeners_upd_dest, 'icon-globe-16');							
								 	return;
								}
								
								if (rec.get('GCTPAN').trim() == 'PVEN') {
									acs_show_win_std('Modifica anagrafica', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 650, 550, my_listeners_upd_dest, 'icon-globe-16');							
								 	return;
								}								
													  						  	
								acs_show_win_std('Modifica anagrafica', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				i_e: 'I', //ToDo
						  				rec_id: rec.get('id')
						  			}, 650, 550, {}, 'icon-globe-16');						  
						  	
						  	return false;
						  				
						  }
					    },
			            
						itemclick: function(view, record, item, index, e) {
								//form = this.up('panel').down('form');
								//console.log(record);
								//form.loadRecord(record);
						},			            
			          
			          
			          itemcontextmenu : function(grid, rec, node, index, event) {			          				                          
			          }
			            	        
			        } //listeners
				

		, buttons: [
		
		
				{
							xtype: 'button',
				            text: 'Modifica anagrafica da gestionale', 
				            iconCls: 'icon-outbox-32', scale: 'large',
				            handler: function() {				            			
								acs_show_win_std('Ricerca cliente', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=search_anag_cli', 
									{tree_id: this.up('treepanel').id}, 650, 350, {}, 'icon-globe-16');
				            }
				}, { xtype: 'tbfill' },		
		
			, {
	         	xtype: 'splitbutton',
	            text: 'Crea nuova anagrafica',
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	
		        	<?php foreach ($s->find_TA_std('MODAN', null, 'N', 'Y') as $ec) { ?> 	
						{
							xtype: 'button',
				            text: <?php echo j(trim($ec['text'])) ?>,
				            //iconCls: 'icon-save-32', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_inserimento = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						//from_win.close();
								        		}
						    				};					            
										acs_show_win_std('Immissione nuova anagrafica cliente', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
											{
								  				mode: 'NEW',
								  				modan: <?php echo j(trim($ec['id'])) ?>,
								  				i_e: <?php echo j(trim($ec['TAFG01'])) ?>
								  			}, 650, 550, my_listeners_inserimento, 'icon-globe-16');
				            
				            }
				        },
				       <?php } ?>  
				            	        	
		        	]
		        }
	        }, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>destinazione',
	            iconCls: 'icon-button_black_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					  
						    				          	
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}
				            			
				            			parent_prog = selected_record[0].get('id');				            							            			
										acs_show_win_std('Nuova destinazione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				parent_prog: parent_prog
								  			}, 650, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					         
						    				
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
				            			parent_prog = selected_record[0].get('id');				            							            			
										acs_show_win_std('Nuova destinazione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				parent_prog: parent_prog
								  			}, 650, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				}, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>punto vendita',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_pven = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					  
						    				          	
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}
				            			
				            			parent_prog = selected_record[0].get('id');				            							            			
										acs_show_win_std('Nuovo punto vendita', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				parent_prog: parent_prog
								  			}, 650, 550, my_listeners_add_pven, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_pven2 = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					         
						    				
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
				            			parent_prog = selected_record[0].get('id');				            							            			
										acs_show_win_std('Nuovo punto vendita', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				parent_prog: parent_prog
								  			}, 650, 550, my_listeners_add_pven2, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				}, {
							xtype: 'button',
				            text: 'Invia a gestionale', 
				            iconCls: 'icon-outbox-32', scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					  
						    				          	
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			
				            			rec_prog = selected_record[0].get('id');
				            			
				            			Ext.getBody().mask('Loading... ', 'loading').show();
										Ext.Ajax.request({
										        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_send_to_gest',
										        jsonData: {
										        	rec_dt: selected_record[0].get('GCDT'),
										        	rec_prog: selected_record[0].get('GCPROG'),
										        },	
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){	            	
													Ext.getBody().unmask();
										            var jsonData = Ext.decode(result.responseText);
										            my_tree.store.load();
										        },
										        failure    : function(result, request){
										        	Ext.getBody().unmask();
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });				            							            			
										
				            
				            }
				        }        
         ]
			        
			        
			        
			, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');

			           if (record.get('trCls')=='grassetto')
					        return ret + ' grassetto';
			           
			           if (record.get('trCls')=='G')
					        return ret + ' colora_riga_grigio';
			           if (record.get('trCls')=='Y')
					        return ret + ' colora_riga_giallo';					        
			           		        	
			           return ret;																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree