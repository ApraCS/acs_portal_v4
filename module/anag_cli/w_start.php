<?php

require_once("../../config.inc.php");
ini_set('max_execution_time', 30000);

require_once 'c_main_tree.php';

$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));



// ******************************************************************************************
// OPEN TAB
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){ ?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {type: 'hbox', align: 'stretch', pack : 'start'},
    <?php echo make_tab_closable(); ?>,	
    title: 'Anagrafica aziende',
    tbar: new Ext.Toolbar({
      items:[
            '<b>Inserimento nuove anagrafiche gestionali</b>', '->',
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
      ]            
    }),  	
	items: [<?php write_main_tree(array()); ?>]
  }
 ]
}
<?php exit; } ?>
