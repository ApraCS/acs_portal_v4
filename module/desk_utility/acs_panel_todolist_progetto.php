<?php

require_once("../../config.inc.php");

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


//ELIMINA PROGETTO
if ($_REQUEST['fn'] == 'exe_delete'){
     
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_assegna_ord']}
            WHERE ASDT = '{$id_ditta_default}' AND ASPROG = '{$m_params->progetto}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $sql_ta = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle']}
               WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ARTPJ' AND TAKEY1 = '{$m_params->progetto}'";
    
    $stmt_ta = db2_prepare($conn, $sql_ta);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_ta);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}


// ******************************************************************************************
// SALVATAGGIO NT0 per dati CD
// ******************************************************************************************
if ($_REQUEST['fn'] == 'save_cd'){
    
     $row_nt = $desk_art->get_note_progetto($m_params->rec->prog);
               
        if(strlen($row_nt['NTMEMO']) > 0){
            
            $ar_upd = array();
            $ar_upd['NTMEMO'] = trim($m_params->rec->desc);
            
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
                    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                    WHERE NTDT = '{$id_ditta_default}' AND NTKEY1 = '{$m_params->rec->prog}'
                    AND NTSEQU = 0 AND NTTPNO = 'PRJCD'";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd);
            echo db2_stmt_errormsg($stmt);
        }else {
            
            $sqlMemo = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(NTDT, NTMEMO, NTKEY1, NTSEQU, NTTPNO)
            VALUES(?, ?, ?, 0, 'PRJCD')";
            
            $stmtMemo = db2_prepare($conn, $sqlMemo);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtMemo, array($id_ditta_default, trim($m_params->rec->desc), $m_params->rec->prog));
            
        }
   
    echo acs_je(array('success' => true));
    exit;
    
}

// ******************************************************************************************
// CREA PROGETTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_create_progetto'){

    $cod_indice = $m_params->cod;
    
    $ar_memo = array();
    foreach($m_params->list_memo as $v){
        $ar_value = explode('_', $v->memo);
        $voce = $ar_value[0];
        $memo = $ar_value[1];
        $ar_memo[$voce] = $memo;
    }
    
    
    $num_progetto = $desk_art->next_num(date('y') . '-' . 'PROG');    
    $prog_progetto = date('y') . '-' . sprintf('%04s', $num_progetto);
    
    //Genero tabella TA0 - ARTPJ (Tabella Progetti)
    $ar_ins = array();
    $ar_ins['TADT'] = $id_ditta_default;
    $ar_ins['TATAID'] = "ARTPJ";
    $ar_ins['TADTGE'] = oggi_AS_date();
    $ar_ins['TAORGE'] = oggi_AS_time();
    $ar_ins['TAUSGE'] = $auth->get_user();
    $ar_ins['TAKEY1'] = $prog_progetto;
    $ar_ins['TADESC'] = $m_params->form_values->f_descrizione;
 
    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);

    //inserimento attivita' di livello 0 (START)
    foreach($m_params->list_selected_id as $v){
        
            $to_form_values = array();
            $to_form_values['f_causale'] = $v->todo;
            $to_form_values['f_note'] = $v->note;
            $to_form_values['f_utente_assegnato'] = $auth->get_user();
            $to_form_values['f_attiva_dal'] = oggi_AS_date();
            //$to_form_values['f_scadenza'] = $v->data_format;
            $to_form_values['f_progetto'] = $prog_progetto;
                        
            $na = new SpedAssegnazioneArticoli($desk_art);
            $as_prog = $na->crea('POSTM', array(
                'k_ordine' => $m_params->form_values->f_descrizione,
                'form_values' => $to_form_values
            ));
                        
            $ar_ins_nt = array();
            $ar_ins_nt['NTMEMO'] = $v->desc;
            $ar_ins_nt['NTDT']   = $id_ditta_default;
            $ar_ins_nt['NTKEY1'] = $as_prog;
            $ar_ins_nt['NTKEY2'] = $v->voce;
            $ar_ins_nt['NTSEQU'] = 0;
            $ar_ins_nt['NTTPNO'] = 'PRJCD';
                      
            $sql_nt = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                       (" . create_name_field_by_ar($ar_ins_nt) . ")
                       VALUES (" . create_parameters_point_by_ar($ar_ins_nt) . ")";
               
            $stmt_nt = db2_prepare($conn, $sql_nt);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_nt, $ar_ins_nt);
            echo db2_stmt_errormsg();
             
            //creo le note standard per il progetto
            $ar_ins = array();
            $row_nt = $desk_art->get_note_indici($v->chiave);
            if($row_nt != false){
                $ar_ins['NTMEMO'] = $row_nt['NTMEMO'];
                $ar_ins['NTDT']   = $id_ditta_default;
                $ar_ins['NTKEY1'] = $as_prog;
                $ar_ins['NTKEY2'] = $v->chiave;
                $ar_ins['NTSEQU'] = 0;
                $ar_ins['NTTPNO'] = 'ASPRS';
                
                $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                        (" . create_name_field_by_ar($ar_ins) . ")
                        VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg();
            }
            
            //creo le note personalizzate per il progetto
            $ar_ins_memo = array();
            $ar_ins_memo['NTMEMO'] = $v->memo;
            $ar_ins_memo['NTDT']   = $id_ditta_default;
            $ar_ins_memo['NTKEY1'] = $as_prog;
            $ar_ins_memo['NTSEQU'] = 0;
            $ar_ins_memo['NTTPNO'] = 'ASMEM';
            $sqlMemo =  "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                        (" . create_name_field_by_ar($ar_ins_memo) . ")
                        VALUES (" . create_parameters_point_by_ar($ar_ins_memo) . ")";
            
            $stmtMemo = db2_prepare($conn, $sqlMemo);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtMemo, $ar_ins_memo);
            echo db2_stmt_errormsg();
            
  
        //recupero eventuali attivita' da sottolivello
        
        //per ogni sottolivello con ATTAV
        //creao una nuova ToDo collegata a quella START (f_prog_coll)
        $sql_f = "SELECT * FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
                  WHERE CDDT= '{$id_ditta_default}' AND CDDICL = '{$cod_indice}'
                  AND CDTODO <> '' AND CDCMAS = '{$v->voce}' ORDER BY CDSEQI";
        $stmt_f = db2_prepare($conn, $sql_f);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_f);
        
    
        
        while ($row = db2_fetch_assoc($stmt_f)){
            
            $voce = trim($row['CDSLAV']);
                         
            $to_form_values = array();
            $to_form_values['f_causale'] = $row['CDTODO'];
            $to_form_values['f_note'] = $v->note;
            $to_form_values['f_utente_assegnato'] = $auth->get_user();
            $to_form_values['f_attiva_dal'] = oggi_AS_date();
           // $to_form_values['f_scadenza'] = $v->data_format;
            $to_form_values['f_progetto'] = $prog_progetto;
            $to_form_values['f_prog_coll'] = $as_prog;
                        
            $na = new SpedAssegnazioneArticoli($desk_art);
            $as_prog_f = $na->crea('POSTM', array(
                'k_ordine' => $m_params->form_values->f_descrizione,
                'form_values' => $to_form_values
            ));
            
            $ar_ins_nt1 = array();
            $ar_ins_nt1['NTMEMO'] = $row['CDDESC'];
            $ar_ins_nt1['NTDT']   = $id_ditta_default;
            $ar_ins_nt1['NTKEY1'] = $as_prog_f;
            $ar_ins_nt1['NTKEY2'] = $row['CDSLAV'];
            $ar_ins_nt1['NTSEQU'] = 0;
            $ar_ins_nt1['NTTPNO'] = 'PRJCD';
            
            $sql_nt1 = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                    (" . create_name_field_by_ar($ar_ins_nt1) . ")
                   VALUES (" . create_parameters_point_by_ar($ar_ins_nt1) . ")";
                        
            $stmt_nt1 = db2_prepare($conn, $sql_nt1);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_nt1, $ar_ins_nt1);
            echo db2_stmt_errormsg();
            
            
          
            //creo le note standard per il progetto
            $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
            $chiave = implode ("|", $ar_comp);
            $row_nt_f = $desk_art->get_note_indici($chiave);
            if($row_nt_f != false){
                $ar_ins_nt_f = array();
                $ar_ins_nt_f['NTMEMO'] = $row_nt_f['NTMEMO'];
                $ar_ins_nt_f['NTDT']   = $id_ditta_default;
                $ar_ins_nt_f['NTKEY1'] = $as_prog_f;
                $ar_ins_nt_f['NTKEY2'] = $chiave;
                $ar_ins_nt_f['NTSEQU'] = 0;
                $ar_ins_nt_f['NTTPNO'] = 'ASPRS';
                
                $sql_nt_f = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                (" . create_name_field_by_ar($ar_ins_nt_f) . ")
                   VALUES (" . create_parameters_point_by_ar($ar_ins_nt_f) . ")";
                $stmt_nt_f = db2_prepare($conn, $sql_nt_f);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_nt_f, $ar_ins_nt_f);
                echo db2_stmt_errormsg();
            }
            
            //creo le note personalizzate per il progetto
            $ar_ins_memo_f = array();
            $ar_ins_memo_f['NTMEMO'] = $ar_memo["{$voce}"];
            $ar_ins_memo_f['NTDT']   = $id_ditta_default;
            $ar_ins_memo_f['NTKEY1'] = $as_prog_f;
            $ar_ins_memo_f['NTSEQU'] = 0;
            $ar_ins_memo_f['NTTPNO'] = 'ASMEM';
            
            $sqlMemo_f =  "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                          (" . create_name_field_by_ar($ar_ins_memo_f) . ")
                          VALUES (" . create_parameters_point_by_ar($ar_ins_memo_f) . ")";
            
            $stmtMemo_f = db2_prepare($conn, $sqlMemo_f);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtMemo_f, $ar_ins_memo_f);
            echo db2_stmt_errormsg();
                     
        
        }
        
        
        
    }
   
    
    
    echo acs_je(array('success' => true, 'prog_progetto' => $prog_progetto));    
  exit;
}


if ($_REQUEST['fn'] == 'get_json_data_indici'){
    
    $cod_indice = $m_params->cod;
    
    //recupero le attivita' (con Todo) legate all'indice
    $sql = "SELECT * FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
                ON CD.CDDT = TA_ATTAV.TADT AND CD.CDTODO = TA_ATTAV.TAKEY1 AND TATAID = 'ATTAV'
            WHERE CDDT= '$id_ditta_default' AND CDDICL = '{$cod_indice}'
            AND CDCMAS = 'START' ORDER BY CDSEQI";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    
    $sql_s = "SELECT * FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
             LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
                ON CD.CDDT = TA_ATTAV.TADT AND CD.CDTODO = TA_ATTAV.TAKEY1 AND TATAID = 'ATTAV'
             WHERE CDDT= '{$id_ditta_default}' AND CDDICL = '{$cod_indice}'
             AND CDCMAS = ? ORDER BY CDSEQI";
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        $nr['d_todo'] = "[".trim($row['CDTODO'])."] ".trim($row['TADESC']);
        $nr['todo'] = trim($row['CDTODO']);
        $nr['utente'] = trim($auth->get_user());
        $nr['voce'] = trim($row['CDSLAV']);
        $nr['gruppo'] = trim($row['CDCMAS']);
        $nr['desc'] = trim($row['CDDESC']);
        if(trim($row['CDTODO']) == '')
            $nr['check'] = 'N';
        else
            $nr['check'] = 'Y';
        $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
        $nr['chiave'] = implode ("|", $ar_comp);
        $nr['nota'] =  $desk_art->has_nota_indici($nr['chiave']);
        $row_nt = $desk_art->get_note_indici($nr['chiave']);
        $nr['t_nota'] = utf8_decode($row_nt['NTMEMO']);
        $ar[] = $nr;
            
        $result = db2_execute($stmt_s, array(trim($row['CDSLAV'])));
        while ($row_s = db2_fetch_assoc($stmt_s)) {
          if($row_s != false)
            $nr2 = array();
            $nr2['d_todo'] = "[".trim($row_s['CDTODO'])."] ".trim($row_s['TADESC']);
            $nr2['todo'] = trim($row_s['CDTODO']);
            $nr2['voce']  = trim($row_s['CDSLAV']);
            $nr2['desc'] = trim($row_s['CDDESC']);
            $nr2['f_giallo'] = 'Y';
            if(trim($row_s['CDTODO']) == '')
                $nr2['check'] = 'N';
            else
                $nr2['check'] = 'Y';
            $ar_comp2 = array(trim($row_s['CDDT']), trim($row_s['CDDICL']), trim($row_s['CDCMAS']), trim($row_s['CDSEQU']), trim($row_s['CDSLAV']));
            $nr2['chiave'] = implode ("|", $ar_comp2);
            $nr2['nota'] =  utf8_decode($desk_art->has_nota_indici($nr2['chiave']));
            $row_nt2 = $desk_art->get_note_indici($nr2['chiave']);
            $nr2['t_nota'] = $row_nt2['NTMEMO'];
            $ar[] = $nr2;
        }
       
        
        
    }
    
    
    echo acs_je($ar);
    exit();
    
}


// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    $as_where = "";
    
    
    if(isset($form_values->f_todo) && strlen($form_values->f_todo) > 0)
        $as_where .= " AND ASCAAS = '{$form_values->f_todo}'";
        
    if(isset($form_values->f_utente_assegnato) && strlen($form_values->f_utente_assegnato) > 0)
        $as_where .= " AND ASUSAT = '{$form_values->f_utente_assegnato}'";
    
    $show_rilav = 'Y';
    if(count($form_values->f_todo_ev) > 0){
        //$as_where .= " AND ASFLRI = 'Y'";
        $as_where .= sql_where_by_combo_value('ASCARI', $form_values->f_todo_ev);
    }
      
       
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.TADESC AS D_AT, TA_PJ.TADTGE AS DATA_PJ, 
            TA_PJ.TADESC AS D_PJ, NT_MEMO.NTMEMO AS MEMO, NT_CD.NTKEY2 AS CODICE, 
            NT_CD.NTMEMO AS DESC, TA_ALLEGATI.NR_AL AS NR_AL,
            TA_PJ.TADTGE AS DATA_PJ, TA_PJ.TAORGE AS ORA_PJ, TA_PJ.TAUSGE AS US_PJ  
            FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT_OPEN
            INNER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
                ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_PJ
                ON ATT_OPEN.ASDT = TA_PJ.TADT AND ATT_OPEN.ASPROG = TA_PJ.TAKEY1
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
	            ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_CD
	            ON NT_CD.NTTPNO = 'PRJCD' AND INTEGER(NT_CD.NTKEY1) = ATT_OPEN.ASIDPR AND NT_CD.NTSEQU=0 
            LEFT OUTER JOIN (
                SELECT COUNT(*) AS NR_AL, TADT, TATAID, TAKEY1
                FROM {$cfg_mod_DeskArt['file_tabelle']}
                GROUP BY TAKEY1, TADT, TATAID) TA_ALLEGATI
            ON TA_ALLEGATI.TADT = ATT_OPEN.ASDT AND TA_ALLEGATI.TATAID = 'TDPAL' AND TA_ALLEGATI.TAKEY1 = ATT_OPEN.ASPROG 
            WHERE ASDT = '{$id_ditta_default}' AND ASPROG <> '' {$as_where}
            ORDER BY ASIDPR";
 

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();

    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        //stacco dei livelli
        $cod_liv0 = trim($row['ASPROG']);
        
        if ($row['ASIDAC'] == 0){
            $cod_liv1 = trim($row['ASIDPR']);
            $cod_liv2 = null;
        }
        else {
            $cod_liv1 = trim($row['ASIDAC']);
            $cod_liv2 = $row['ASIDPR'];
        }
        
        
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //PROGETTO
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = trim($row['ASPROG']);
            $ar_new['imm'] =  "[".print_date($row['DATA_PJ'])."-".print_ora($row['ORA_PJ'])."]";
            $ar_new['desc'] = $row['D_PJ'];
            $ar_new['allegati'] = $row['NR_AL'];
            $ar_new['ut_ass'] = "[".trim($row['US_PJ'])."]";
            $ar_new['liv'] = 'liv_0';
            
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        
        //ATTIVITA'
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", array($row['ASCAAS'], $row['ASIDPR']));
            $ar_new['task'] = $row['CODICE'];
            $ar_new['desc'] =  trim($row['DESC']);
            $ar_new['causale'] =  $row['ASCAAS'];
            $ar_new['r_note'] =  utf8_decode($row['ASNORI']);
            if(trim($row['ASFLRI']) == 'Y'){
                $causali_rilascio = $desk_art->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                $ar_new['d_rilav'] = $causali_rilascio[0]['text'];
                $ar_new['t_rilav'] = "<b>{$row['ASCARI']}</b>";
                $ar_new['t_rilav'] .= "<br>" .trim($row['ASNORI']);
            }
            
            $ar_new['prog'] =  $row['ASIDPR'];
            $ar_new['prog_coll'] =  $row['ASIDAC'];
            $ar_new['note'] =  $desk_art->has_nota_progetto($row['ASIDPR']);
            $row_nt =  $desk_art->get_note_progetto($row['ASIDPR']);
            $ar_new['t_note'] =  utf8_decode($row_nt['NTMEMO']);
            $ar_new['imm'] = print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
            $ar_new['ut_ass'] = trim($row['ASUSAT']);
            $ar_new['ut_ins'] = trim($row['ASUSAS']);
            $ar_new['scadenza'] = trim($row['ASDTSC']);
            $ar_new['memo'] = utf8_decode(trim($row['MEMO']));
            $ar_new['riferimento'] = trim($row['ASNOTE']);
            $ar_new['f_ril'] =  trim($row['ASFLNR']);
            $ar_new['flag'] =  trim($row['ASFLRI']);
            $ar_new['liv'] = 'liv_1';
            $ar_new['leaf'] = true;
            $t_ar_liv1 = &$ar_r[$liv];
            $ar_r["{$liv}"] = $ar_new;
        }
        
        $ar_r = &$ar_r["{$liv}"];
        //sottolivello
        //se cod_liv2 is not null creo sotto livello
        //e imposto a false il leaf di liv1
        if(!is_null($cod_liv2)){
         
            $liv=$cod_liv2;
            $ar_r = &$ar_r['children'];
            $tmp_ar_id[] = $liv;
            if(!isset($ar_r[$liv])){
                $ar_new = $row;
                $ar_new['id'] = implode("|", array($row['ASCAAS'], $row['ASIDPR']));
                $ar_new['task'] =  $row['CODICE'];
                $ar_new['desc'] =  trim($row['DESC']);
                $ar_new['causale'] =  $row['ASCAAS'];
                $ar_new['r_note'] = utf8_decode($row['ASNORI']);
                if(trim($row['ASFLRI']) == 'Y'){
                    $causali_rilascio = $desk_art->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                    $ar_new['d_rilav'] = $causali_rilascio[0]['text'];
                    $ar_new['t_rilav'] = "<b>{$row['ASCARI']}</b>";
                    $ar_new['t_rilav'] .= "<br>" .trim($row['ASNORI']);
                  
                }
                $ar_new['prog'] =  $row['ASIDPR'];
                $ar_new['prog_coll'] =  $row['ASIDAC'];
                $ar_new['note'] =  $desk_art->has_nota_progetto($row['ASIDPR']);
                $row_nt =  $desk_art->get_note_progetto($row['ASIDPR']);
                $ar_new['t_note'] =  utf8_decode($row_nt['NTMEMO']);
                $ar_new['imm'] = print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
                $ar_new['ut_ass'] = trim($row['ASUSAT']);
                $ar_new['ut_ins'] = trim($row['ASUSAS']);
                $ar_new['scadenza'] = trim($row['ASDTSC']);
                $ar_new['memo'] = utf8_decode(trim($row['MEMO']));
                $ar_new['riferimento'] = trim($row['ASNOTE']);
                $ar_new['f_ril'] =  trim($row['ASFLNR']);
                $ar_new['flag'] =  trim($row['ASFLRI']);
                $ar_new['liv'] = 'liv_2';
                $ar_new['leaf'] = true;
                $t_ar_liv1['leaf'] = false;
                $ar_r["{$liv}"] = $ar_new;
            }
            
        }
        
    }       
    
   
   
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}

if ($_REQUEST['fn'] == 'open_filtri'){
    
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
        $ar_users_json = acs_je($user_to);
        
   ?> 
    
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            flex: 1,
	            items: [
	          		{
						xtype: 'textfield',
						fieldLabel: 'Descrizione progetto',
						labelWidth : 120,
						flex : 1,
						anchor: '-5',
						name: 'f_descrizione',
						maxLength : 50,
						forceSelection: true,	
        			    allowBlank: false
					   							
					}, 
					 { 	xtype: 'grid',
				  		loadMask: true,	
				  		flex : 1,
				  		anchor: '-5',
				  		multiSelect: true,
				  		autoScroll : true,
				  		plugins: [
        		          Ext.create('Ext.grid.plugin.CellEditing', {
        		            clicksToEdit: 1
        		            })
        		      	],
				  		selModel: {selType: 'checkboxmodel', checkOnly: true},
        		        store: {
							xtype: 'store',
							autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_indici', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										cod : <?php echo j($m_params->cod); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['gruppo', 'f_giallo', 'check', 'todo', 'd_todo', 'memo', 'nota', 't_nota', 'desc', 'voce', 'chiave']							
									
					},
					
					<?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
		
				columns: [	
				         {
			                header   : 'Voce',
			                dataIndex: 'voce', 
			                width : 40,
			             },{
			                header   : 'Descrizione',
			                dataIndex: 'desc', 
			                flex: 1
			             },{text: '<?php echo $nota; ?>', 	
            				width: 25,
            				align: 'center', 
            				dataIndex: 'nota',
            				tooltip: 'Note',		        			    	     
            		    	renderer: function(value, p, record){
        						   if(!Ext.isEmpty(record.get('t_nota')))
        						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_nota')) + '"';    		    	
            		    	
            		    	       if(record.get('nota') > 0)
            		    			 return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
            		    		   else
            		    		     return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
        		    		  }
            		        },{
			                header   : 'ToDo',
			                dataIndex: 'd_todo', 
			                width : 170,
			             }, {
			                header   : 'Memo',
			                dataIndex: 'memo', 
			                width : 130,
			                editor: {
            	                xtype: 'textfield',
            	                allowBlank: true
            	            }
			             }
		    	
		    		
		    		
				],
				
				listeners: {
                 
                    /*afterlayout : function(comp) {
                      var sm = comp.getSelectionModel();
        		      sm.selectAll(true);
                        
                    },*/
                  	beforeselect: function(grid, record, index, eOpts) {
						
            			if (record.get('check') =='N') {//replace this with your logic.
               		         // Ext.Msg.alert('Message', 'articolo bloccato o gi&agrave; ordinato');
               		         //alert('ToDo mancante');
               				 return false;
               				 
            			}
       				 } 
                }
				
				,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
						       if (record.get('check') =='N')
						           return ' colora_riga_rosso';
						       if (record.get('f_giallo')=='Y')
						          return ' colora_riga_giallo';			           		
						       
						       return '';																
					         },   
					    },
			
											    
				    		
                    }, //grid
				
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma',
				        iconCls: 'icon-button_blue_play-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                var win = this.up('window');
			            	var form = this.up('form').getForm();
			            	var grid = this.up('window').down('grid');
			            	id_selected = grid.getSelectionModel().getSelection();
    			     		list_selected_id = [];
    			     		list_memo = [];
    			     		for (var i=0; i<id_selected.length; i++){
    			     		    if(id_selected[i].get('gruppo') == 'START') 
    			     				list_selected_id.push(id_selected[i].data);
    			     			if(id_selected[i].get('f_giallo') == 'Y'){	
    			     			   var voce_memo = id_selected[i].get('voce').trim() +'_' +id_selected[i].get('memo').trim()
    			     			   list_memo.push({memo : voce_memo});
    			     			 }
    						}
    						if (form.isValid()){	
							Ext.Ajax.request({
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_create_progetto',
						        jsonData: {
						        	cod: <?php echo j($m_params->cod); ?>,
						        	form_values: form.getValues(),
						        	list_selected_id : list_selected_id,
						        	 list_memo :  list_memo
						        	},
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
						             if(jsonData.success == false){
					      	    		 acs_show_msg_error('Errore in creazione progetto');
							        	 return;
				        			  }else{
				        			    Ext.Msg.show({
                                           title:'Message',
                                           msg: 'Creato nuovo progetto di codifica base dati',
                                           buttons: Ext.Msg.OK,
                                           icon: Ext.MessageBox.INFO
                                        });
				        			     win.close(); 
				        			  }
						        	   										        	
						        }
						    });	            			
	            			}		                   	                	                
	            		}
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
    
    <?php 
    
    exit;
}


if ($_REQUEST['fn'] == 'open_panel'){
?>

{"success":true, "items": [

        {
        xtype: 'treepanel' ,
        multiSelect: true,
        cls: 's_giallo',
        title: 'To Do Project' ,
	    tbar: new Ext.Toolbar({
	            items:['<b> Gestione attivit&agrave; di manutenzione base dati articoli</b>', '->',
    	               {iconCls: 'icon-gear-16',
    	             text: 'CAUSALI', 
		           		handler: function(event, toolEl, panel){
			           		acs_show_panel_std('acs_gest_ATTAV.php?fn=open_tab', 
			           			'panel_gestione_attav_prj', {sezione: 'ART'});
			          
		           		 }
		           	 }
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,   
        	store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['r_note', 'prog_coll', 'causale', 'id', 'task', 'desc', 'liv', 'flag', 'prog', 'imm', 'ut_ins', 'ut_ass', 'scadenza', 'riferimento', 'memo', 'f_ril', 'd_rilav', 't_rilav', 'note', 't_note', 'allegati'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
	                      form_values: <?php echo acs_je($m_params->form_values); ?>
	                  
                      }
                    , doRequest: personalizza_extraParams_to_jsonData  ,     
						reader: {
                            root: 'children'
                        }        				
                    }

                }),
                
                <?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
    	    			
            columns: [ {xtype: 'treecolumn', 
        	    		text: 'Progetto/attivit&agrave;', 	
        	    		width: 150,
        	    		dataIndex: 'task',
        	    		 renderer: function(value, p, record){
						   	if(record.get('flag') == 'Y')
    						       p.tdCls += ' barrato';
    					    return value;
    		    	    }
        	    		},
        	    		{text: 'Descrizione', flex:1, dataIndex: 'desc',
        	    		renderer: function(value, p, record){
	            	        p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
	            	    
						   if(record.get('flag') == 'Y'){
						       p.tdCls += ' sfondo_verde_rilascio';
						       p.tdCls += ' barrato';
						    }  
						    
						    if(record.get('liv') == 'liv_1')
						     return '<b>' + value +'</b>';
						    else
    		    	   		 return value;
    		    	    }},	
	            	    {text: 'Attivit&agrave;', width: 70, 
	            	     dataIndex: 'causale',
        	    		 editor: {
	                			xtype: 'textfield',
	                			allowBlank: true
	            	    }, renderer: function(value, p, record){
						   	if(record.get('flag') == 'Y')
    						       p.tdCls += ' barrato';
    					    return value;
    		    	    }
	            	    
	            	    },	
        	    		{text: 'Rilascio', width: 120, dataIndex: 'd_rilav', 
        	    		 renderer: function(value, p, record){
						   if(!Ext.isEmpty(record.get('t_rilav')))
						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_rilav')) + '"';    		    	
    		    	   		
    		    	   		if(record.get('r_note').trim() != '' && record.get('liv') == 'liv_1'){
						       return value + '<br><b>['+record.get('r_note').trim() + ']</b>';
						    }
						    
						    if(record.get('r_note').trim() != '' && record.get('liv') == 'liv_2'){
						       return value +'<br><b>['+record.get('r_note').trim() + ']</b>';
						    } 
    		    	   		
    		    	   		
    		    	   		return value;
    		    	    }},
    		    	    {text: '<?php echo $nota; ?>', 	
    		    	     width: 30, 
    		    	     dataIndex: 'note', 
    		    	     tooltip : 'Note standard',
        	    		 renderer: function(value, p, record){
        	    		   if(record.get('liv') != 'liv_0'){
						    if(!Ext.isEmpty(record.get('t_note')))
						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_note')) + '"';    		    	
    		    	   		
    		    	   		if(record.get('note') > 0)
    		    			 	return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
    		    		    else
    		    		     	return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
    		    	    	}else{
    		    	    		if(record.get('allegati') > 0) return '<img src=<?php echo img_path("icone/48x48/camera.png") ?> width=15>';
    		    	    	}
    		    	      }
    		    	    },	
	 					{text: 'Data rilascio', width: 120, dataIndex: 'imm'},	
	 					{text: 'Utente assegnato', width: 110, dataIndex: 'ut_ass'},	
	 					{text: 'Scadenza', width: 80, dataIndex: 'scadenza',
	 					 renderer: function(value, p, record){
						   if(record.get('scadenza') > 0){		 					 
    						   if(record.get('scadenza') == <?php echo oggi_AS_date(); ?>)
    						       p.tdCls += ' sfondo_giallo';
    					       if(record.get('scadenza') < <?php echo oggi_AS_date(); ?>)
    					       	   p.tdCls += ' sfondo_rosso';
					       	} 
					       	if(record.get('flag') == 'Y')
    						       p.tdCls += ' barrato';
					       	  
    		    	   		return date_from_AS(value);
    		    	    }},	
	 					{text: 'Riferimento', width: 100, dataIndex: 'riferimento',
	 					 renderer: function(value, p, record){
	 					    p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
						   	if(record.get('flag') == 'Y')
    						       p.tdCls += ' barrato';
    					    return value;
    		    	    }},
	 					{text: 'Memo', width: 200, dataIndex: 'memo',
	 					renderer: function(value, p, record){
	            	        p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
						   	if(record.get('flag') == 'Y')
    						       p.tdCls += ' barrato';
    					    return value;
    		    	     }
	 					},	
        	    		
    	    ],
    	     listeners: {
    	     
    	     
    	     	beforeload: function(store, options) {
    		     var t_win = this.up('window');
                    Ext.getBody().mask('Loading... ', 'loading').show();
                    },
    
                load: function () {
                 Ext.getBody().unmask(); 
                },  
	         
    	     
    	     celldblclick: {	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){		           
		           		var col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	var rec = iView.getRecord(iRowEl);
					  	var loc_win = this.up('window');
					  	var grid = this;
					    	
					  	if(col_name == 'note'){	
    					  	if(rec.get('liv') == 'liv_0'){
       					  	  acs_show_win_std('ToDo Project ' +rec.get('task') , 'acs_todo_allegati.php?fn=open_tab', {progetto: rec.get('task')}, 600, 400, my_listeners, 'icon-camera-16');
    					  	
    					  	}else{
    					  	   var my_listeners = {
    	    		  				afterSave: function(from_win, values){
    	    		  			    rec.set('t_note', values.note);
    	    		  			    rec.set('note', values.icon);
    		     					from_win.close();
    		     					}	
    	     					};
    					  		acs_show_win_std('Note', 'acs_anag_artd_note_progetto.php?fn=open_panel', {prog : rec.get('prog')}, 650, 300, my_listeners, 'icon-comment_edit-16');
    					  	
    					  	}
					  	
					  		
					  	}
					  	           
	            	}	          
	           	},
	          
	          itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					     id_selected = grid.getSelectionModel().getSelection();
					     list_selected_id = [];
					     for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push({id: id_selected[i].get('id'), 
									prog: id_selected[i].get('prog'),
									articolo: id_selected[i].get('articolo'),
									f_ril: id_selected[i].get('f_ril')});
					     
					     if (rec.get('liv') != 'liv_0'){
					     
					     voci_menu.push({
                 		text: 'Modifica ToDo',
                		iconCls : 'icon-pencil-16',          		
                		handler: function () {
                		
                		 list_rows = [];
					     for (var i=0; i<id_selected.length; i++) 
							list_rows.push({prog: id_selected[i].get('prog'),
											flag: id_selected[i].get('flag')});
                		
    		    		   var my_listeners = {
	    		  			afterOkSave: function(from_win){
	    		  			    //grid.getTreeStore().load();
	    		  			    
	    		  			    for (var i=0; i<id_selected.length; i++){
	    		  			        
    	    		  			    var node = grid.getTreeStore().getNodeById(id_selected[i].get('id'));
                                    if (node){
                                        grid.getTreeStore().load({node:node});
                                    }
                                    
	    		  			    } 
	    		  			
        						from_win.close();  
				        		}
		    				};
				        	acs_show_win_std('Modifica ToDo', 'acs_modifica_todo.php?fn=open_mod', {list_selected_id : list_rows}, 370, 250, my_listeners, 'icon-pencil-16');          		
        	                }
            		  });
					
					      
					    voci_menu.push({
			      		text: 'Avanzamento/Rilascio attivit&agrave;',
			    		iconCls: 'icon-arrivi-16',
			    		handler: function() {

  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: 'acs_panel_todolist.php?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										            jsonData = Ext.decode(result.responseText);
										        	this.set('flag', jsonData.ASFLRI);
										        	this.set('d_rilav', jsonData.d_rilav);
										        	this.set('t_rilav', jsonData.t_rilav);
										        	this.set('imm', jsonData.data_ora);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							        list_selected_id: list_selected_id, 
							        grid_id: grid.id},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });
					  
			<?php
            $causali_rilascio = $desk_art->find_TA_std('RILAV', null, 'N', 'Y'); //recupero tutte le RILAV

            foreach($causali_rilascio as $ca) {
     
            ?>	

			
	if (rec.get('flag')!='Y' && rec.get('causale').trim() == <?php echo j(trim($ca['id'])); ?>){ 		  
	
	voci_menu.push({
	      		text: <?php echo j($ca['text']); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {

				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: 'acs_panel_todolist.php?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	jsonData = Ext.decode(result.responseText);
										        	this.set('flag', jsonData.ASFLRI);
										        	this.set('d_rilav', jsonData.d_rilav);
										        	this.set('t_rilav', jsonData.t_rilav); 
										        	this.set('imm', jsonData.data_ora);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							           			list_selected_id: list_selected_id, grid_id: grid.id,
												auto_set_causale: <?php echo j(trim($ca['TAKEY2'])); ?>							        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();	
							        			            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
	    
						    
		
			    }
			  });
			  
			  }
  <?php } ?>
  	  
		     }else{
		     
		      voci_menu.push({
         		text: 'Elimina progetto',
        		iconCls : 'icon-sub_red_delete-16',          		
        		handler: function () {
		    		   Ext.Msg.confirm('Richiesta conferma', 'CONFERMA cancellazione DEFINITIVA di tutte le righe del progetto?' , function(btn, text){
            	   	   if (btn == 'yes'){
		    		   Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
					        method     : 'POST',
		        			jsonData: {
		        				progetto : rec.get('task'),
							},							        
					        success : function(result, request){
					           grid.getTreeStore().load();
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
					    
					    }
					    	  });
	                }
    		});
    		
    		    voci_menu.push({
         		text: 'Report',
        		iconCls : 'icon-print-16',          		
    		 	handler: function() {
                   window.open('acs_panel_todo_report.php?progetto=Y&n_prog='+ rec.get('task'));
                                   	                	     
	          }
    		});
		     
		     }
		
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	        
				  
				 },
				 
				 viewConfig: {
				 toggleOnDblClick: false,
		         getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
 		           return v;																
		         }   
		    }	
    	    
    	    
    	}	
	 	
	]  	
  }


<?php 
exit;
}