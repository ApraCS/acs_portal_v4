<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();
$main_module = new DeskUtility();
$deskArt = new DeskArt();

function genera_TAREST($m_table_config, $values){
    $value_attuale = $values->TAREST;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
        foreach($m_table_config['TAREST'] as $k => $v){
            if(isset($values->$k)){
                $chars = $values->$k;
                
                if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
                    $len = "%{$v['riempi_con']}{$v['len']}s";
                else
                    $len = "%-{$v['len']}s";
                        
                $chars = substr(sprintf($len, $chars), 0, $v['len']);
                $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                $value_attuale = $new_value;
                            
            }
            
        }
        
    return $new_value;
}

$m_table_config = array(
    
    'TAREST' => array(
        'data_ini' 	=> array(
            "start" => 0,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'data_fin' 	=> array(
            "start" => 8,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'check' 	=> array(
            "start" => 16,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'data_t_r' 	=> array(
            "start" => 17,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'collo' 	=> array(
            "start" => 18,
            "len"   => 7,
            "riempi_con" => "0"
        ),
        
        'gr_collo' 	=> array(
            "start" => 25,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'collo_o' 	=> array(
            "start" => 28,
            "len"   => 1,
            "riempi_con" => ""
        ),
        
        'cls_1' 	=> array(
            "start" => 29,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cls_2' 	=> array(
            "start" => 32,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cls_3' 	=> array(
            "start" => 35,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cls_4' 	=> array(
            "start" => 38,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cls_ie' 	=> array(
            "start" => 41,
            "len"   => 1,
            "riempi_con" => ""
        ),
        
        
        'gr_1' 	=> array(
            "start" => 42,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gr_2' 	=> array(
            "start" => 45,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gr_3' 	=> array(
            "start" => 48,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gr_4' 	=> array(
            "start" => 51,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gr_ie' 	=> array(
            "start" => 54,
            "len"   => 1,
            "riempi_con" => ""
        ),
        
        
        'sgr_1' 	=> array(
            "start" => 55,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'sgr_2' 	=> array(
            "start" => 58,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'sgr_3' 	=> array(
            "start" => 61,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'sgr_4' 	=> array(
            "start" => 64,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'sgr_ie' 	=> array(
            "start" => 67,
            "len"   => 1,
            "riempi_con" => ""
        ),
        
        's_art' 	=> array(
            "start" => 68,
            "len"   => 15,
            "riempi_con" => ""
        ),
        'rielab' 	=> array(
            "start" => 83,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'varc' 	=> array(
            "start" => 84,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vanc' 	=> array(
            "start" => 87,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'sevc' 	=> array(
            "start" => 90,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'vrcs' 	=> array(
            "start" => 91,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vncs' 	=> array(
            "start" => 94,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tipo' 	=> array(
            "start" => 97,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'svcs' 	=> array(
            "start" => 100,
            "len"   => 1,
            "riempi_con" => ""
        )
    )
    
    
);

if ($_REQUEST['fn'] == 'exe_check'){
    
    $ret = array();
    $row = get_TA_sys('RC1L', $m_params->codice, $m_params->c_art);
    
    
    if($row != false && count($row) > 0){
        $ret['msg_error'] = "Codice esistente";
        $ret['success'] = false;
        
        $sql_s = "SELECT RRN (TA) AS RRN, TA.*
        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'RC1L'
        AND TANR = '{$m_params->codice}' AND TACOR1 = '{$m_params->c_art}'";
        
        $stmt_s = db2_prepare($conn, $sql_s);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_s);
        $row = db2_fetch_assoc($stmt_s);
        $row['TADESC'] = acs_u8e(trim($row['TADESC']));
        $row['TADES2'] = trim($row['TADES2']);
        $row['TACINT'] = trim($row['TACINT']);
        $row['TANR'] = trim($row['TANR']);
        if(is_array($m_table_config['TAREST'])){
            foreach($m_table_config['TAREST'] as $k => $v){
                if(substr($k, 0, 4) == 'data'){
                    $row[$k] = print_date(substr($row['TAREST'], $v['start'], $v['len']));
                }else{
                    $row[$k] = rtrim(substr($row['TAREST'], $v['start'], $v['len']));
                }
            }
        }
        
        for($i=0; $i <= 14; $i++){
            $row["s_art_{$i}"] = $row['s_art'][$i];
        }
       
        $ret['record'] = $row;
    }else{
        $ret['success'] = true;
    }
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ret = array();
    
    $ar_ins['TAUSGE'] 	= $auth->get_user();
    $ar_ins['TADTGE']   = oggi_AS_date();
    $ar_ins['TAUSUM'] 	= $auth->get_user();
    $ar_ins['TADTUM']   = oggi_AS_date();
    $ar_ins['TADT'] 	= $id_ditta_default;
    $ar_ins['TAID'] 	= 'RC1L';
    $ar_ins['TANR'] 	= $m_params->form_values->TANR;
    $ar_ins['TADESC'] 	= trim($m_params->form_values->TADESC);
    $ar_ins['TADES2'] 	= trim($m_params->form_values->TADES2);
    $s_art = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 's_art_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
            $s_art .= $m_params->form_values->$filtro;
            else
                $s_art .= sprintf("%-1s", '');
    }
    
    $m_params->form_values->s_art = $s_art;
     
    $tarest = genera_TAREST($m_table_config, $m_params->form_values);
    $ar_ins['TAREST'] 	= $tarest;
    
    
    if(isset($m_params->list_art) && count($m_params->list_art) > 0){
        $articoli = "";
        foreach($m_params->list_art as $v){
            
            $row = get_TA_sys('RC1L', $m_params->form_values->TANR, $v);
            
            
            if($row != false && count($row) > 0){
                $articoli .= "{$v}_";
                $count_e++;
            }else{
                
                $ar_ins["TACOR1"] = $v;
                
                $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_tab_sys']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
                $count++;
                
            }
        
        
        }
        
        $ret['msg_info'] = "Articoli selezionati: ".count($m_params->list_art);
        $ret['msg_info'] .= "<br>Schede inserite: {$count}";
        if(trim($articoli) != ''){
            $icona = '<span style="display: inline; float: left;"><a href="javascript:show_win_grid_articoli(\'' . $articoli. '\')";><img class="cell-img" src=' . img_path("icone/16x16/leaf_1.png") . '></a></span>';
            $ret['msg_info'] .= "<br>{$icona}Articoli esclusi: {$count_e}";
        }   
    
    
    
    }else{
        
        $row = get_TA_sys('RC1L', $m_params->form_values->TANR, $m_params->c_art);
        
        if($row != false && count($row) > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Codice esistente in R1CL";
            echo acs_je($ret);
            return;
        }
        
        $ar_ins['TACOR1'] 	= $m_params->c_art;
        
        $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
        
        
        $sql = "SELECT RRN (TA) AS RRN, TA.*
        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'RC1L'
        AND TACOR1 = '{$m_params->c_art}' AND TANR = '{$m_params->form_values->TANR}'
        ORDER BY TA.TANR, TA.TADESC";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        $row['TADESC'] = acs_u8e(trim($row['TADESC']));
        $row['TADES2'] = trim($row['TADES2']);
        $row['TANR'] = trim($row['TANR']);
        if(is_array($m_table_config['TAREST'])){
            foreach($m_table_config['TAREST'] as $k => $v){
                if(substr($k, 0, 4) == 'data'){
                    $row[$k] = print_date(substr($row['TAREST'], $v['start'], $v['len']));
                }else{
                    $row[$k] = rtrim(substr($row['TAREST'], $v['start'], $v['len']));
                }
            }
        }
        
        for($i=0; $i <= 14; $i++){
            $row["s_art_{$i}"] = $row['s_art'][$i];
        }
        
        $ret['success'] = true;
        $ret['record'] = $row;
        
    }
    
    
    
    
    
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $ar_upd = array();
    
    $articoli = "";
    $count = 0;
    $count_e = 0;
    
    $ar_upd['TAUSUM'] 	= $auth->get_user();
    $ar_upd['TADTUM']   = oggi_AS_date();
   
    $ar_upd['TADESC'] 	= utf8_decode(trim($m_params->form_values->TADESC));
    $ar_upd['TADES2'] 	= trim($m_params->form_values->TADES2);
    
    $s_art = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 's_art_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
            $s_art .= $m_params->form_values->$filtro;
        else
            $s_art .= sprintf("%-1s", '');
    }
    $m_params->form_values->s_art = $s_art;
        
    $tarest = genera_TAREST($m_table_config, $m_params->form_values);
    $ar_upd['TAREST'] 	= $tarest;
    
    if(isset($m_params->list_art) && count($m_params->list_art) > 0){
        foreach($m_params->list_art as $k=>$v){
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_tab_sys']} TA
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE TADT = '{$id_ditta_default}'
                    AND TANR = '{$m_params->form_values->TANR}' AND TAID = 'RC1L'
                    AND TACOR1 = '{$v}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            $error_msg = db2_stmt_errormsg($stmt);
            
            if (strlen($error_msg) > 0) {
                $articoli .= "{$v}_";
                $count_e++;
            } else{
                $count++;
            }
        }
        
        $ret['msg_info'] = "Articoli selezionati: ".count($m_params->list_art);
        $ret['msg_info'] .= "<br>Schede aggiornate: {$count}";
        if(trim($articoli) != ''){
            $icona = '<span style="display: inline; float: left;"><a href="javascript:show_win_grid_articoli(\'' . $articoli. '\')";><img class="cell-img" src=' . img_path("icone/16x16/leaf_1.png") . '></a></span>';
            $ret['msg_info'] .= "<br>{$icona}Articoli esclusi: {$count_e}";
        } 
    
    }else{
        $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
        WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg();
        
        $sql = "SELECT RRN (TA) AS RRN, TA.*
        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'RC1L'
        AND TACOR1 = '{$m_params->c_art}' AND TANR = '{$m_params->form_values->TANR}'
        ORDER BY TA.TANR, TA.TADESC";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        $row['TADESC'] = acs_u8e(trim($row['TADESC']));
        $row['TADES2'] = trim($row['TADES2']);
        $row['TANR'] = trim($row['TANR']);
        if(is_array($m_table_config['TAREST'])){
            foreach($m_table_config['TAREST'] as $k => $v){
                if(substr($k, 0, 4) == 'data'){
                    $row[$k] = print_date(substr($row['TAREST'], $v['start'], $v['len']));
                }else{
                    $row[$k] = rtrim(substr($row['TAREST'], $v['start'], $v['len']));
                }
            }
        }
        
        for($i=0; $i <= 14; $i++){
            $row["s_art_{$i}"] = $row['s_art'][$i];
        }
        
        
       
        $ret['record'] = $row;
        
        
    }
    
        
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}




if ($_REQUEST['fn'] == 'get_data_grid'){
    
    $ar_lin = array();
    foreach($cfg_mod_DeskUtility['ar_lingue'] as $v){
        $ar_lin[] = $v['id'];
    }
    
    
    $sql = "SELECT RRN (TA) AS RRN, TA.*
            FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'RC1L'
            AND TACOR1 = '{$m_params->c_art}'
            ORDER BY TA.TANR, TA.TADESC";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $data = array();
    
    while ($row = db2_fetch_assoc($stmt)) {
        
        $row['RRN'] = acs_u8e(trim($row['RRN']));
        $row['TAID'] = trim($row['TAID']);
       
        $row['TADESC'] = acs_u8e(trim($row['TADESC']));
        $row['TADES2'] = trim($row['TADES2']);
        $row['TATP'] = trim($row['TATP']);
        $row['TANR'] = trim($row['TANR']);
        $row['TADTGE'] = trim($row['TADTGE']);
        $row['TAUSGE'] = trim($row['TAUSGE']);
        $row['TADTUM'] = trim($row['TADTUM']);
        $row['TAUSUM'] = trim($row['TAUSUM']);
        
        if(is_array($m_table_config['TAREST'])){
            foreach($m_table_config['TAREST'] as $k => $v){
                if(substr($k, 0, 4) == 'data'){
                    $row[$k] = print_date(substr($row['TAREST'], $v['start'], $v['len']));
                }else{
                    $row[$k] = rtrim(substr($row['TAREST'], $v['start'], $v['len']));
                }
            }
        }
        
        for($i=0; $i <= 14; $i++){
            $row["s_art_{$i}"] = $row['s_art'][$i];
        }
        
        
        $data[] = $row;
                                
    }
    
    echo acs_je($data);
    
    exit();
    
}




if ($_REQUEST['fn'] == 'open_panel'){ ?>
{"success":true, "items": [

   {
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
        		{ xtype: 'grid',
                multiSelect : true,
                itemId : 'my_grid',
                flex : 0.7,
                <?php if($m_params->hide_grid == 'Y'){?>	
        			hidden : true,
        		<?php } ?>
        	    features: [{
        				ftype: 'filters',
        				encode: false, 
        				local: true,   
        		   		 filters: [
        		       {
        		 		type: 'boolean',
        				dataIndex: 'visible'
        		     }
        		      ]
        			}]
				,store: {
						
						xtype: 'store',
						autoLoad: true,
					   	proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },							        
							     extraParams: {
							        c_art : <?php echo j($m_params->c_art); ?>
							     },
			        			 doRequest: personalizza_extraParams_to_jsonData, 
						         reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        		 fields: [ 'RRN', 'TAID', 'TANR', 'TADESC', 'TADES2',
		        		          'TAREST', 'TADTGE', 'TAUSGE', 'TADTUM', 'TAUSUM',
		        		 		  'data_ini', 'data_fin' , 'check' , 'gr_collo', 's_art', 'varc', 'vanc', 'sevc', 'vrcs', 'vncs' , 'tipo' , 'svcs' 
		        		 		<?php 
		        		 		for($i=0; $i <= 14; $i++){?>
		        		 		    , 's_art_<?php echo $i; ?>'
		        		 		<?php }?>
		        		 		   , 'cls_1', 'cls_2', 'cls_3', 'cls_4', 'cls_ie'
		        		 		   , 'gr_1', 'gr_2', 'gr_3', 'gr_4', 'gr_ie'
		        		 		   , 'sgr_1', 'sgr_2', 'sgr_3', 'sgr_4', 'sgr_ie'
		        		 		   , 'collo', 'collo_o', 'rielab'
		        		 		   
		        		 		],
		        		
		        		},
		    		<?php 
		    		      $divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
		    		      $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
		    		?>
					columns: [ 
			          
			        	{
            			header: 'Riga',
            		 	dataIndex: 'TANR', 
            		 	width : 50,
            		 	filter: {type: 'string'}, filterable: true,
            		    },{
            			header: 'Descrizione',
            		 	dataIndex: 'TADESC', 
            		 	flex : 1,
            		 	filter: {type: 'string'}, filterable: true,
            		    },{
            			header: 'Note',
            		 	dataIndex: 'TADES2', 
            		 	flex : 1,
            		 	filter: {type: 'string'}, filterable: true,
            		    },
 						{header: 'Immissione',
	 					columns: [
	 						{header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
	 						renderer: function(value, metaData, record){
	         					if (record.get('TADTGE') != record.get('TADTUM')) 
	          						metaData.tdCls += ' grassetto';
	 
             					q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
			 					metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		     					return date_from_AS(value);	
							}
	 
	 						}
                    	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
                    	  renderer: function(value, metaData, record){
                    	  
                    	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
                    	          metaData.tdCls += ' grassetto';
                    	  
                                 q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
                    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
                    		     return value;	
                    	} 
                    	 }
                    	 ]}
			        
			        ],  
					
						listeners: {
						
					      	 selectionchange: function(selModel, selected) { 
	                           if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            		               rec_index = selected[0].index;
            		               form_dx.getForm().findField('rec_index').setValue(rec_index);
            	                   acs_form_setValues(form_dx, selected[0].data);
            	                 //  form_dx.getForm().setValues(selected[0].data);
            	                }
            	                
		       		  	 	 }
		       		  	 	 
		       		  	,celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	var titolo = 'Traduzione [' +rec.get('TANR') + '] ';
					  	
					  	 if(col_name == 'TRAD')
            				  	acs_show_win_std( titolo + rec.get('TADESC'), 'acs_anag_art_gest_trad_sys.php?fn=open_al', {tanr: rec.get('TANR'), taid : rec.get('TAID'), tacor2 : rec.get('TACOR2')}, 450, 500, null, 'icon-globe-16');
            			
					  	}
					  	}
					},
						viewConfig: {
        		        getRowClass: function(record, index) {	
        		        
        		        if(record.get('TATP') == 'S'){
        		           return ' colora_riga_rosso';
                		 }
		        														
		         }   
		    }
       
        }  
	, {		   xtype: 'form',
	            itemId: 'dx_form',
	            autoScroll : true,
	            title: 'Dettagli tabella regole collificazione 1 livello',
	            //bodyStyle: 'padding: 5px',
	            //bodyPadding: '2 0 0 0',
	            flex:0.3,
	            frame: true,
	            items: [ 
 		      	{
        		xtype: 'tabpanel',
        		items: [
        		
        		{
				xtype: 'panel',
				title: 'Dettagli', 
				autoScroll : true,
				frame : true,
				 layout: 'anchor',
				//layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
		    		{xtype : 'textfield',name : 'rec_index', hidden : true},
		    		{xtype : 'textfield',name : 'RRN', hidden : true},
		    		{xtype : 'textfield',name : 'TAREST', hidden : true},
		    	    {
    				xtype : 'textfield',
    		 		name: 'TANR', 
    		 		maxLength : 4,
    		 		fieldLabel: 'Riga',
    		 		width : 140,
    		 		listeners: {
    					'change': function(field){
    						 field.setValue(field.getValue().toUpperCase());
      				},
      				'blur': function(field) {
      				    var form = this.up('form').getForm();
      				    var grid = this.up('form').up('panel').down('grid');
      				    
      					Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				codice : field.getValue(),
			        				c_art : <?php echo j($m_params->c_art); ?>
			        			},				
							  success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        
							        if(jsonData.success == false){
					      	    		 acs_show_msg_error(jsonData.msg_error);
							        	 form.setValues(jsonData.record);
			        			   		 var rec_index = grid.getStore().findRecord('TANR', jsonData.record.TANR);
		  					   			 grid.getView().select(rec_index);
			  					   	     grid.getView().focusRow(rec_index);
				        			}
						
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
                    	 		
    
                		}
     				}
        		  },{
        			xtype : 'textfield',
        		 	name: 'TADESC', 
        		 	fieldLabel: 'Descrizione',
        		 	maxLength : 30,
        		 	anchor: '-15'
        		  }	,{
        			xtype : 'textfield',
        		 	name: 'TADES2', 
        		 	fieldLabel: 'Note',
        		 	maxLength : 30,
        		 	anchor: '-15'
        		  },
        	{ 
        		xtype: 'fieldcontainer',
        		flex:1,
        		anchor: '-15',
        		layout: { 	type: 'hbox',
        				    pack: 'start',
        				    align: 'stretch'},						
        		items: [
        		
        		{
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	   , fieldLabel: 'Validit&agrave; iniziale'
        	   , name: 'data_ini'
        	   , format: 'd/m/y'							   
        	   , submitFormat: 'Ymd'
        	   , allowBlank: false
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        	//	, anchor: '-15'
        	   , flex :1.3
        	   
        	}, {
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	   , fieldLabel: 'Finale'
        	   , labelAlign : 'right'
        	   , labelWidth : 40
        	   , name: 'data_fin'
        	   , format: 'd/m/y'						   
        	   , submitFormat: 'Ymd'
        	   , allowBlank: false
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        	   , anchor: '-15'
        	   , flex :0.9
        	}]},
        	
        	{
		name: 'check',
		xtype: 'combo',
		fieldLabel: 'Check data',
		forceSelection: true,							
		displayField: 'text',
		valueField: 'id',								
		emptyText: ' - seleziona -',
   		allowBlank: true,								
	    anchor: '-15',
	    queryMode: 'local',
        minChars: 1, 
		store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			    <?php echo acs_ar_to_select_json($deskArt->find_TA_std('CHKDT', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>
			    ]
			},
		listeners: { 
		 	beforequery: function (record) {
         	record.query = new RegExp(record.query, 'i');
         	record.forceAll = true;
         }
      }
	 }	,
	 {xtype: 'combo',
		flex: 1,
		name: 'gr_collo',
		fieldLabel: 'Gruppo collo',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		      data: [								    
				     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUCI'), ''); ?>	
				    ]
		}
		
   },	
   
     {   xtype: 'fieldcontainer',
     			anchor: '-15',
		          layout: 'hbox',
		          items: [
		          {
        			xtype : 'numberfield',
        		 	name: 'collo', 
        		 	fieldLabel: 'Collo',
        		 	maxValue : 9999999,
        		 	anchor: '-15',
        		 	hideTrigger : true,
        		 	width : 180
        		  },{xtype: 'combo',
            		flex: 1,
            		name: 'collo_o',
            		fieldLabel: 'Solo su ordine',
            		labelAlign : 'right',
            		labelWidth : 90,
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',
            		queryMode: 'local',
            		minChars: 1,							
            		emptyText: '- seleziona -',
               		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		      data: [								    
            				     {id: '', text : '- vuoto -'},
            				     {id: 'O', text : '[O] Si'}
            				    ]
            		}
            		
               }
		          
		          
		          ]},
   
     
      {   xtype: 'fieldcontainer',
		          layout: 'hbox',
		      	  margin : '0 0 0 105',
		          items: [
						<?php for ($i = 1; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', width : 16, fieldStyle:'font-size:10px;'},
						<?php }?>	
						 ]
				}, { 
						xtype: 'fieldcontainer',
						anchor: '-15',
						margin : '0 0 5 0',
						layout: { 	type: 'hbox',
									pack: 'start',
									align: 'stretch',
									frame: true},
						defaults: {xtype: 'textfield', width: 16, margin : '0 0 0 0', maxLength: 1},
						items: [
	                        {xtype: 'label', text: 'Sel. articolo', width: 105},
							<?php for ($i = 0; $i <= 14; $i++){?>
								{ name: '<?php echo "s_art_{$i}"; ?>',
								  listeners: {
								    
									'change': function(field){
									 value = this.getValue().toString();
									if (value.length == 1) {
										var nextfield = field.nextSibling();
										if (!Ext.isEmpty(nextfield))
											nextfield.focus(true, 0);
									  } 
									  
									  field.setValue(field.getValue().toUpperCase());
										
								  }
								}
							  },
							<?php }?>
							
						]						
					}
					
					,{xtype: 'combo',
            		flex: 1,
            		name: 'rielab',
            		fieldLabel: 'Rielaborazione',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',
            		queryMode: 'local',
            		minChars: 1,							
            		emptyText: '- seleziona -',
            		anchor: '-15',
               		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		      data: [								    
            				     {id: '', text : '- vuoto -'},
            				     {id: 'N', text : '[N] No'}
            				    ]
            		}
            		
               },
					
					     <?php
					     $proc = new ApiProc();
					     echo $proc->get_json_response(
					         extjs_combo_dom_ris(array(
					             'initial_value' => array('dom_c'  => $m_params->varc,
					                 'ris_c'  => $m_params->vanc,
					                 'tipo_c' => "",
					                 'out_d'  => $m_params->out_v1),
					             'risposta_a_capo' => true,
					             'label' =>'Variabile (Riga)<br>Variante',
					             'file_TA' => 'sys',
					             'tipo_opz_risposta' => array('output_domanda' => true,
					                 'tipo_cf' => '',
					                 'opzioni' => array(
					                     array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
					                 )),
					             'dom_cf'  => 'varc',  'ris_cf' => 'vanc'))
					         );
					     echo ",";
		        		 		 
		        		?>  	
		        		
		        		  {xtype: 'combo',
                		flex: 1,
                		name: 'sevc',
                		fieldLabel: 'Includi/Escludi',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		      data: [								    
                				     {id : 'I', text : '[I] Includi'},
                				     {id : 'E', text : '[E] Escludi'}
                				    ]
                		}
                		
                   },	
                     <?php
					     $proc = new ApiProc();
					     echo $proc->get_json_response(
					         extjs_combo_dom_ris(array(
					             'initial_value' => array('dom_c'  => $m_params->vrcs,
					                 'ris_c'  => $m_params->vncs,
					                 'tipo_c' => "",
					                 'out_d'  => $m_params->out_v1),
					             'risposta_a_capo' => true,
					             'label' =>'Variabile (S.riga)<br>Variante',
					             'file_TA' => 'sys',
					             'tipo_opz_risposta' => array('output_domanda' => true,
					                 'tipo_cf' => '',
					                 'opzioni' => array(
					                     array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
					                 )),
					             'dom_cf'  => 'vrcs',  'ris_cf' => 'vncs'))
					         );
					     echo ",";
                   ?>
                   
                   { name: 'tipo',
    	  xtype: 'combo',
          fieldLabel: 'Tipologia',
          forceSelection: true,					
          displayField: 'text',
    	  valueField: 'id',					
    	  emptyText: '- seleziona -',
          queryMode: 'local',
          minChars: 1,
          anchor: '-15',
    	  store: {
    		editable: false,
    		autoDestroy: true,
    	    fields: [{name:'id'}, {name:'text'}],
    	    data: [								    
    	     <?php echo acs_ar_to_select_json(find_TA_sys('PUTI', null, null, null, null, null, 0, '', 'Y'), ''); ?>		
    	    ]
    	},listeners: { 
    	beforequery: function (record) {
    	record.query = new RegExp(record.query, 'i');
    	record.forceAll = true;
    	 }
    	}
     }, {xtype: 'combo',
		flex: 1,
		name: 'svcs',
		fieldLabel: 'Includi/Escludi',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		      data: [								    
				     {id : 'I', text : '[I] Includi'},
				     {id : 'E', text : '[E] Escludi'}
				    ]
		}
		
   }
                   
                   
                     
        		  ]},
		    
    			{
    				xtype: 'panel',
    				title: 'Classificazioni', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    				
    				 {xtype: 'combo',
                		name: 'cls_ie',
                		fieldLabel: 'Includi/Escludi',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		      data: [								    
                				     {id : 'I', text : '[I] Includi'},
                				     {id : 'E', text : '[E] Escludi'}
                				    ]
                		}
                		
                   },
                   
                         {	name: 'cls_1',
								xtype: 'combo',
								fieldLabel: 'Classe',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   	   anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUCM'), ''); ?>	
								    ]
								},
								
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 },
    				       
    				   <?php for($i = 2; $i <= 4; $i++){
                             $campo = "cls_{$i}";?>
				    
    				       {	name: <?php echo j($campo); ?>,
								xtype: 'combo',
								margin : '0 0 5 105',
								fieldLabel: '',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   	   anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUCM'), ''); ?>	
								    ]
								},
								
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }, 
    				    <?php }?>
    				   
    				    {xtype: 'combo',
                		name: 'gr_ie',
                		fieldLabel: 'Includi/Escludi',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		      data: [								    
                				     {id : 'I', text : '[I] Includi'},
                				     {id : 'E', text : '[E] Escludi'}
                				    ]
                		}
                		
                   },{	name: 'gr_1',
								xtype: 'combo',
								fieldLabel: 'Gruppo',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   	   anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUGM'), ''); ?>	
								    ]
								},
								
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 },
                      <?php for($i = 2; $i <= 4; $i++){
                             $campo = "gr_{$i}";?>
				    
    				       {	name: <?php echo j($campo); ?>,
								xtype: 'combo',
								fieldLabel: '',
								margin : '0 0 5 105',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   	   anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUGM'), ''); ?>	
								    ]
								},
								
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }, 
    				    <?php }?>
    				    
    				     
    				        {xtype: 'combo',
                		name: 'sgr_ie',
                		fieldLabel: 'Incluedi/Escludi',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		      data: [								    
                				     {id : 'I', text : '[I] Includi'},
                				     {id : 'E', text : '[E] Escludi'}
                				    ]
                		}
                		
                   },	{	name: 'sgr_1',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   	   anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUSM'), ''); ?>	
								    ]
								},
								
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 },
            			<?php for($i = 2; $i <= 4; $i++){
                             $campo = "sgr_{$i}";?>
				    
    				       {	name: <?php echo j($campo); ?>,
								xtype: 'combo',
								fieldLabel: '',
								margin : '0 0 5 105',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   	   anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUSM'), ''); ?>	
								    ]
								},
								
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }, 
    				    <?php }?>
    				    
    				     
    				
    					]}	
    				
		    ]}
		    ],
		    		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [ '->',
                  {
                     xtype: 'button',
                    text: 'Elimina',
                    <?php if($m_params->hide_grid == 'Y'){?>	
						hidden : true,
					<?php } ?>
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
	       			  
	       			  if(form_values.RRN == ''){
							  acs_show_msg_error('Selezionare una riga dalla tabella di sinistra');
							  return false;
						 }
        		
        		      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				       Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
					      	    		 	acs_show_msg_error(jsonData.msg_error);
							        	 	return;
				        			     }else{
        					         		form.getForm().reset();
							        	    var rec_index = grid.getStore().findRecord('TANR', form_values.TANR);
							       		    grid.getStore().remove(rec_index);	
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
					
					    	}
            	   		  });		    
							    
				        }

			     },  
			      {
                     xtype: 'button',
                    text: 'Crea',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	 var form_values = this.up('form').getValues();
 			          var form = this.up('form').getForm();
	       			  var grid = this.up('form').up('panel').down('grid'); 
	       			  var loc_win = this.up('window');
 			        
 			         //if (form.isValid()){
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				c_art : <?php echo j($m_params->c_art); ?>,
	        				list_art : <?php echo acs_je($m_params->list_art)?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if(jsonData.success == false){
					      	    acs_show_msg_error(jsonData.msg_error);
					      	    return false;
					        }else if(!Ext.isEmpty(jsonData.msg_info)){
					        	acs_show_msg_info(jsonData.msg_info, 'Operazione completa');
					          	  loc_win.fireEvent('afterOkSave', loc_win, jsonData);
					        		
					        }else{
					        	//grid.getStore().load();
					        	
					        	 var new_rec = jsonData.record;
					        	 grid.getStore().add(new_rec);
					        	 var rec_index = grid.getStore().findRecord('TANR', new_rec.TANR);
			  					 grid.getView().select(rec_index);
			  					 grid.getView().focusRow(rec_index);
					           
					        }
					       },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		          // }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			var index = form_values.rec_index;
 			       			//var record_grid = grid.getStore().getAt(index);
 			       			var record_grid = grid.getSelectionModel().getSelection()[0];
 			       			
 			       			if(form_values.RRN == ''){
								  acs_show_msg_error('Impossibile modificare');
								  return false;
							 }			
 			       					       		 
 			       		 
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				c_art : <?php echo j($m_params->c_art); ?>,
			        				list_art : <?php echo acs_je($m_params->list_art)?>
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error(jsonData.msg_error);
					      	    		return false;
					        			}else{
					        			   var new_rec = jsonData.record;
					        			    record_grid.set(new_rec);
					        			   
					        			   
					        			}
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               
			            }

			     }
			     ]
		   }]
		   
			 	 }  
		 ],
					 
					
					
	}
]
}

<?php 

exit; 

}




