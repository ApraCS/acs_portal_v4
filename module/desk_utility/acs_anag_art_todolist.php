<?php

require_once("../../config.inc.php");

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	ini_set('max_execution_time', 300);	
	$m_params = acs_m_params_json_decode();
	global $cfg_mod_DeskArt;
	
	$sql_where = "";
	if(isset($m_params->c_art))
	    $sql_where .= " AND ASDOCU = '{$m_params->c_art}'";
	if(isset($m_params->rif))
        $sql_where .= " AND TA_ATTAV.TARIF1 = '{$m_params->rif}'";

	$sql = "SELECT ATT_OPEN.*, TA_ATTAV.* , NT_MEMO.NTMEMO 
	        FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT_OPEN
	        LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
			  ON TA_ATTAV.TADT = ATT_OPEN.ASDT AND TA_ATTAV.TATAID = 'ATTAV' AND TA_ATTAV.TAKEY1 = ATT_OPEN.ASCAAS
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
			   ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0			
			WHERE 1=1 {$sql_where}";


	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg();
	$ret = array();
	
	while ($row = db2_fetch_assoc($stmt)) {
		
		$n = array();

		$n['data'] 				= $row['ASDTAS'];
		$n['ora'] 				= $row['ASHMAS'];		
		$n['utente'] 			= trim($row['ASUSAS']);
		$n['k_ordine'] 			= trim($row['ASDOCU']);
		$n['causale']			= trim($row['ASCAAS']);
		$n['causale_out']		= trim($row['TADESC']);
		$n['note_base']			= acs_u8e(trim($row['ASNOTE']));
		$n['note_estese']		= acs_u8e(trim($row['NTMEMO']));
		$n['utente_assegnato']	= trim($row['ASUSAT']);
		$n['scadenza']			= $row['ASDTSC'];
		$n['rec_stato']			= trim($row['ASFLRI']);
		$n['prog']				= trim($row['ASIDPR']);
		
		$ret[] = $n;
	}
	
	echo acs_je($ret);
	
	
	exit;
} //get_json_data



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();	
?>
{
 success:true, 
 items: [
  {
            xtype: 'grid',
			flex: 1,
			title: 'Anagrafica articoli - ToDo',
			closable: true,
			
	        tbar: new Ext.Toolbar({
	            items:[
	            '<b>Gestione stato/attivit&agrave; articoli</b>', '->',
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),  
			
			
			store: {
					xtype: 'store',
					autoLoad: true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
											rif: <?php echo j($m_params->rif); ?>,
											c_art: <?php echo j($m_params->c_art); ?>,
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['id', 'prog', 'k_ordine', 'rec_stato', 'data', 'ora', 'utente', 'causale', 'causale_out', 'note_base', 'note_estese', 'utente_assegnato', 'scadenza']							
							
			
			}, //store
			
			
			
	        columns: [	
				  {header   : 'Data', 		dataIndex: 'data', 			width: 70, renderer: date_from_AS}
				, {header   : 'Ora', 		dataIndex: 'ora', 			width: 45, renderer: time_from_AS}
				, {header   : 'Immesso da', dataIndex: 'utente', 	width: 90}
				, {header   : 'Articolo', 	dataIndex: 'k_ordine', 		width: 100}
				, {header   : 'Causale', 	dataIndex: 'causale_out',	flex: 1}
				, {header   : 'Note', 		dataIndex: 'note_base', 	flex: 1}
				, {header   : 'Memo',		dataIndex: 'note_estese',	flex: 1}
				, {header   : 'Assegnato a', 	dataIndex: 'utente_assegnato', 		width: 90}
				, {header   : 'Scadenza', 	dataIndex: 'scadenza', 		width: 70,
						renderer: function (value, metaData, record, row, col, store, gridView){

							if (parseFloat(record.get('scadenza')) > 0){
								oggi = new Date();
							
								if (parseFloat(record.get('scadenza')) < parseFloat(Ext.Date.format(oggi, 'Ymd')))
									metaData.tdCls += ' tpSfondoRosa grassetto';																	
								if (parseFloat(record.get('scadenza')) == parseFloat(Ext.Date.format(oggi, 'Ymd')))
									metaData.tdCls += ' tpSfondoGrigio grassetto';								
								
							}

    						return date_from_AS(value);	
    					}				
				}
				, {header: '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=25>',  dataIndex: 'rec_stato', width: 32, tooltip: 'Evasa',				
							renderer: function(value, metaData, record){
							
								if (value == 'Y'){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Evasa') + '"';			    	
					    		}
							
						    	if (value=='Y') return '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=18>';			    				    	
						    }				
				}
	         ],																					

	         
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
						  }
					  },
					  
					  itemcontextmenu : function(grid, rec, node, index, event) {			  	
					  
						  event.stopEvent();		  
					      var voci_menu = [];
					      
						  id_selected = grid.getSelectionModel().getSelection();
						  list_selected_id = [];
						  for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push(id_selected[i].data);
														      
		      
						  voci_menu.push({
				      		text: 'Avanzamento/Rilascio attivit&agrave;',
				    		iconCls: 'icon-sub_blue_accept-16',
				    		handler: function() {
	  	
					    		//verifico che abbia selezionato solo righe non gia' elaborate 
								  for (var i=0; i<id_selected.length; i++){ 
									  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
										  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
										  return false;
									  }
								  }  	
	  	
	
				    			//apro form per richiesta parametri
								var mw = new Ext.Window({
								  width: 800
								, height: 380
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Avanzamento/Rilascio attivit&agrave;'
								, iconCls: 'iconAccept'			
								, layout: 'fit'
								, border: true
								, closable: true
								, id_selected: id_selected
								, listeners:{
						                 'close': function(win){
						                      for (var i=0; i<id_selected.length; i++){
						                      	//per i record che erano selezionato verifico se hanno 
						                      	//adesso il flag di rilasciato (e devo sbarrarli)
						                        
												Ext.Ajax.request({
													url: 'acs_form_json_avanzamento_entry.php?fn=exe_richiesta_record_rilasciato',
											        jsonData: {prog: id_selected[i].get('prog')},
											        method     : 'POST',
											        waitMsg    : 'Data loading',
											        success : function(result, request){
											        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
											        }, scope: id_selected[i],
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });					                        
						                      }
						                  }
						
						         }								
								});				    			
				    			mw.show();			    			
	
								//carico la form dal json ricevuto da php
								Ext.Ajax.request({
								        url        : 'acs_form_json_avanzamento_entry.php',
								        jsonData: {list_selected_id: list_selected_id, grid_id: grid.id},
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								            var jsonData = Ext.decode(result.responseText);
								            mw.add(jsonData.items);
								            mw.doLayout();				            
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
					    		
				    		}
						  });
						  
						  
								  my_listeners = {
		        					afterUpdateRecord: function(from_win){	
		        						from_win.close();
		        						grid.store.reload();
						        	}								  
								  }		  
						  

						 /* voci_menu.push({
				      		text: 'Modifica',
				    		iconCls: 'icon-leaf-16',
				    		handler: function() {

				    					acs_show_win_std('Modifica', 
											'acs_form_json_modify_entry.php', 
											{
								  				prog: rec.get('prog'),
								  			}, 450, 300, my_listeners, 'icon-leaf-16');
				    		
				    		}
						  });*/
						  
						  
						  
						      
					    var menu = new Ext.menu.Menu({
				            items: voci_menu
						}).showAt(event.xy);
					  
					      
					      
					  } //itemcontextmenu
					  
					  
					  	  
			}			
			
				  
	            
        }   
 ]
}
<?php exit; } ?>