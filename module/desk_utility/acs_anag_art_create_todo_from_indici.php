<?php

require_once("../../config.inc.php");

$main_module = new DeskUtility();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();




// ******************************************************************************************
// CREA PROGETTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_create_todo'){
    
    $na = new SpedAssegnazioneArticoli($desk_art);    
    
    //per ogni todo selezionata per essere generata
    foreach($m_params->list_selected_todo as $todo_config){
        
        //per ogni articolo selezionato
        foreach($m_params->list_selected_art as $cod_art){
            
            //verifico se gia ho l'attivita aperta per l'articolo (altrimenti non la rigenero)
            $as_exist = $na->ha_entry_aperte_per_art($cod_art, $todo_config->todo);
            
            //verifico se il tipo parte e' tra quelli (opzionalmente) indicati
            $row_art = $desk_art->get_row_art($cod_art);
            $ok_tipo_parte = false;
            
            
            if (!is_array($todo_config->l_tipo_parte) || 
                 count($todo_config->l_tipo_parte) == 0 ||
                 (count($todo_config->l_tipo_parte) == 1 || trim($todo_config->l_tipo_parte[0]) == '') ||
                 in_array($row_art['ARTPAR'], $todo_config->l_tipo_parte))
                $ok_tipo_parte = true;
            
            if (!$as_exist && $ok_tipo_parte == true) {
                
                $to_form_values = array();
                $to_form_values['f_causale'] = $todo_config->todo;
                //$to_form_values['f_note'] = $v->note; //????
                if (strlen(trim($todo_config->operatore)) > 0)
                    $to_form_values['f_utente_assegnato'] = trim($todo_config->operatore);
                else   
                    $to_form_values['f_utente_assegnato'] = $auth->get_user();                
                $to_form_values['f_attiva_dal'] = oggi_AS_date();
                
                $na = new SpedAssegnazioneArticoli($desk_art);
                $as_prog = $na->crea('POSTM', array(
                    'k_ordine' => $cod_art,
                    'form_values' => $to_form_values
                ));
                                	
                //note memo
                if (strlen(trim($todo_config->memo)) > 0 ) {
                    $ar_ins_nt = array();
                    $ar_ins_nt['NTMEMO'] = acs_u8e($todo_config->memo);
                    $ar_ins_nt['NTDT']   = $id_ditta_default;
                    $ar_ins_nt['NTKEY1'] = $as_prog;
                    $ar_ins_nt['NTSEQU'] = 0;
                    $ar_ins_nt['NTTPNO'] = 'ASMEM';
                    
                    $sql_nt = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
                           (" . create_name_field_by_ar($ar_ins_nt) . ")
                           VALUES (" . create_parameters_point_by_ar($ar_ins_nt) . ")";
                    
                    $stmt_nt = db2_prepare($conn, $sql_nt);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt_nt, $ar_ins_nt);
                    echo db2_stmt_errormsg();
                }
                
            } //solo se gia non esiste attivita per articolo
            
        } //per ogni articolo
        
        
    } //per ogni
    
        
    echo acs_je(array('success' => true));
    exit;
}





// ******************************************************************************************
// da indice recupero voci che diventeranno ToDo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_indici'){
    
    $cod_indice = $m_params->cod;
    
    //recupero le attivita' (con Todo) legate all'indice
    $sql = "SELECT * FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
                ON CD.CDDT = TA_ATTAV.TADT AND CD.CDTODO = TA_ATTAV.TAKEY1 AND TATAID = 'ATTAV'
            WHERE CDDT= '$id_ditta_default' AND CDDICL = '{$cod_indice}'
            AND CDCMAS = 'START' ORDER BY CDSEQI";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    
    $sql_s = "SELECT * FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
             LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
                ON CD.CDDT = TA_ATTAV.TADT AND CD.CDTODO = TA_ATTAV.TAKEY1 AND TATAID = 'ATTAV'
             WHERE CDDT= '{$id_ditta_default}' AND CDDICL = '{$cod_indice}'
             AND CDCMAS = ? ORDER BY CDSEQI";
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        $nr['d_todo'] = "[".trim($row['CDTODO'])."] ".trim($row['TADESC']);
        $nr['todo'] = trim($row['CDTODO']);
        $nr['utente'] = trim($auth->get_user());
        $nr['voce'] = trim($row['CDSLAV']);
        $nr['gruppo'] = trim($row['CDCMAS']);
        $nr['desc'] = trim($row['CDDESC']);
        if(trim($row['CDTODO']) == '')
            $nr['check'] = 'N';
        else
            $nr['check'] = 'Y';
        $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
        $nr['chiave'] = implode ("|", $ar_comp);
        $nr['nota'] =  $desk_art->has_nota_indici($nr['chiave']);
        $row_nt = $desk_art->get_note_indici($nr['chiave']);
        $nr['t_nota'] = utf8_decode($row_nt['NTMEMO']);
        
        //default da CDPARM
        $ar_cdparm = explode(";", trim($row['CDPARM']));
        foreach($ar_cdparm as $vc){
            $ar_c = explode(":", $vc);
            switch ($ar_c[0]){
                case "U":         $nr['operatore'] = $ar_c[1]; break;
                case "TP":        $nr['l_tipo_parte'] = explode(",", $ar_c[1]); break;
            }
        }
        
        $ar[] = $nr;
            
        $result = db2_execute($stmt_s, array(trim($row['CDSLAV'])));
        while ($row_s = db2_fetch_assoc($stmt_s)) {
          if($row_s != false)
            $nr2 = array();
            $nr2['d_todo'] = "[".trim($row_s['CDTODO'])."] ".trim($row_s['TADESC']);
            $nr2['todo'] = trim($row_s['CDTODO']);
            $nr2['voce']  = trim($row_s['CDSLAV']);
            $nr2['desc'] = trim($row_s['CDDESC']);
            $nr2['f_giallo'] = 'Y';
            if(trim($row_s['CDTODO']) == '')
                $nr2['check'] = 'N';
            else
                $nr2['check'] = 'Y';
            $ar_comp2 = array(trim($row_s['CDDT']), trim($row_s['CDDICL']), trim($row_s['CDCMAS']), trim($row_s['CDSEQU']), trim($row_s['CDSLAV']));
            $nr2['chiave'] = implode ("|", $ar_comp2);
            $nr2['nota'] =  utf8_decode($desk_art->has_nota_indici($nr2['chiave']));
            $row_nt2 = $desk_art->get_note_indici($nr2['chiave']);
            $nr2['t_nota'] = $row_nt2['NTMEMO'];
            
            //default da CDPARM
            $ar_cdparm = explode(";", trim($row['CDPARM']));
            foreach($ar_cdparm as $vc){
                $ar_c = explode(":", $vc);
                switch ($ar_c[0]){
                    case "U":         $nr2['operatore'] = $ar_c[1]; break;
                    case "TP":        $nr2['l_tipo_parte'] = explode(",", $ar_c[1]); break;
                }
            }
            
            
            $ar[] = $nr2;
        }
       
        
        
    }
    
    
    echo acs_je($ar);
    exit();
    
}


// ******************************************************************************************
// apertura form e grid
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_filtri'){
    
    //Params:
        //cod: codice indice: default: WIZDUPART??
        //list_selected_art: array dei codici articoli per cui generare i ToDo
    
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
        $ar_users_json = acs_je($user_to);
        
   ?>     
	{"success":true, "items": [	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            flex: 1,
	            items: [ 
					 { 	xtype: 'grid',
				  		loadMask: true,	
				  		flex : 1,
				  		anchor: '-5',
				  		multiSelect: true,
				  		autoScroll : true,
				  		plugins: [
        		          Ext.create('Ext.grid.plugin.CellEditing', {
        		            clicksToEdit: 1
        		            })
        		      	],
				  		selModel: {selType: 'checkboxmodel', checkOnly: true},
        		        store: {
							xtype: 'store',
							autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_indici', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										cod : <?php echo j($m_params->cod); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['gruppo', 'f_giallo', 'check', 'todo', 'd_todo', 'memo', 'nota', 't_nota', 'desc', 'voce', 'chiave', 'operatore', 'l_tipo_parte']							
									
					},
					
					<?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
		
				columns: [	
						{
			                header   : 'Descrizione',
			                dataIndex: 'desc', 
			                flex: 1
			             },{text: '<?php echo $nota; ?>', 	
            				width: 25,
            				align: 'center', 
            				dataIndex: 'nota',
            				tooltip: 'Note',		        			    	     
            		    	renderer: function(value, p, record){
        						   if(!Ext.isEmpty(record.get('t_nota')))
        						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_nota')) + '"';    		    	
            		    	
            		    	       if(record.get('nota') > 0)
            		    			 return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
            		    		   else
            		    		     return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
        		    		  }
            		        }, {
    			                header   : 'ToDo',
    			                dataIndex: 'd_todo', 
    			                flex: 1,
    			             }, {
    			                header   : 'Operatore',
    			                dataIndex: 'operatore', 
    			                width : 170,
    			                editor: {
    				                xtype: 'combobox',
    				                allowBlank: true,
    				                valueField: 'UTCUTE',
    				                displayField: 'UTDESC',
    				                	store: {
    									fields: [{name:'UTCUTE'}, {name:'UTDESC'}],
    								    data: [								    
    									     <?php echo acs_ar_to_select_json($ar_users, '') ?>	
    									    ] 
    								}
    							}
    			             }, {
    			                header   : 'Tipo parte',
    			                dataIndex: 'l_tipo_parte', 
    			                width : 220,
    			                editor: {
    				                xtype: 'combobox',
    				                allowBlank: true,
    				                valueField: 'id',
    				                multiSelect: true,
    				                displayField: 'text',
    				                	store: {
    									fields: [{name:'id'}, {name:'text'}],
    								    data: [								    
    									     <?php echo acs_ar_to_select_json(find_TA_sys('MTPA'), '', true, 'N', 'N', 'Y') ?>	
    									    ] 
    								}
    							}
    			             }, {
    			                header   : 'Memo',
    			                dataIndex: 'memo', 
    			                width : 130,
    			                editor: {
                	                xtype: 'textfield',
                	                allowBlank: true
                	            }
    			             }
				],
				
				listeners: {                 
                  	beforeselect: function(grid, record, index, eOpts) {						
            			if (record.get('check') =='N') {return false;}
       				 } 
                }
				
				,viewConfig: {
					        getRowClass: function(record, index) {
						       if (record.get('check') =='N')
						           return ' colora_riga_rosso';
						       if (record.get('f_giallo')=='Y')
						          return ' colora_riga_giallo';			           								       
						       return '';																
					         }   
					    }
				    		
                 }, //grid
				
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma generazione<br><font size=1>Articoli selezionati: <b><?php echo count($m_params->list_selected_art);?></b></font>',
				        iconCls: 'icon-button_blue_play-32',		            
				        scale: 'large',		            
			            handler: function() {
			            
							var win = this.up('window'),
			            		form = this.up('form').getForm(),
			            		grid = this.up('window').down('grid'),
			            		id_selected = grid.getSelectionModel().getSelection(),
    			     			list_selected_id = [],
    			     			list_memo = [];
    			     			
    			     		for (var i=0; i<id_selected.length; i++){    			     		     
    			     			list_selected_id.push(id_selected[i].data);
    						}
    						if (form.isValid()){
    							Ext.Ajax.request({
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_create_todo',
    						        jsonData: {
    						        	cod: <?php echo j($m_params->cod); ?>,
    						        	form_values: form.getValues(),
    						        	list_selected_todo:  list_selected_id,
    						        	list_selected_art: <?php echo acs_je($m_params->list_selected_art); ?>
    						        },
    						        method     : 'POST',
    						        waitMsg    : 'Data loading',
    						        success : function(result, request){
    						            var jsonData = Ext.decode(result.responseText);
    						             if(jsonData.success == false){
    					      	    		 acs_show_msg_error('Errore in creazione progetto');
    							        	 return;
    				        			  } else {
    				        			    Ext.Msg.show({
                                               title:'Message',
                                               msg: 'Operazione completata con successo',
                                               buttons: Ext.Msg.OK,
                                               icon: Ext.MessageBox.INFO
                                            });
                                            //win.destroy(); 
    				        			  }
    						        	   										        	
    						        }
    						    });	
    						} //if isValid
			            	                   	                	                
	            		} //handler
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}    
<?php     
    exit;
}
