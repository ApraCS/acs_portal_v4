<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          		{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						margin : '0 0 0 62',
						items: [
						
						<?php for ($i = 1; $i<=9; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 13 0 0' },
						<?php }?>	
						<?php for ($i = 10; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 6 0 0' },
						<?php }?>	
				
						  
						 ]
					},
						
					<?php for ($n = 1; $n<= 5; $n++){?>	
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{ xtype: 'displayfield', value : '<?php echo "cod. {$n}) ";?>', margin : '0 15 0 5' },
						
					<?php for ($i = 1; $i<=15; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_<?php echo "[{$n}]{$i}"; ?>', 
						  width: 20,
						  maxLength: 1,
						  enforceMaxLength : true,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
                              
                              field.setValue(field.getValue().toUpperCase());
      							
    					  }
 						 }},
						
						<?php }?>
					
						]},		
						<?php }?>
						
						 {
							name: 'f_sospeso',
							margin : '5 0 0 0',
							xtype: 'radiogroup',
							fieldLabel: 'Sospesi',
							labelWidth: 60,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'Y'
		                          , width: 45
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'T'
		                          , width: 60   
								  , checked: true                       
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Esclusi'	
		                          , inputValue: 'N'
		                          , width: 60
		                         
		                          
		                        }]
						  },{
							name: 'f_stringa_DB',
							margin : '5 0 0 0',
							xtype: 'radiogroup',
							fieldLabel: 'Stringa codice DB',
							labelWidth: 60,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_stringa_DB' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'
		                          , width: 45
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_stringa_DB' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          , width: 60   
								  , checked: true                       
		                        }, {
		                            xtype: 'displayfield'                       
		                        }]
						  }, {
							name: 'f_r_var',
							margin : '0 0 5 0',
							xtype: 'radiogroup',
							fieldLabel: 'Raggruppa per variante',
							labelWidth: 160,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_r_var' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'
		                          , width: 30
		                         
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_r_var' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          , width: 35   
								  , checked: true                       
		                        }]
						  },
						 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
					name: 'f_ciclo',
					xtype: 'combo',
					multiSelect : true,
					labelWidth : 60,
					width : 250,
					fieldLabel: 'Ciclo vita',
					forceSelection: true,								
					displayField: 'text',
					valueField: 'id',								
					emptyText: '- seleziona -',
			   		//allowBlank: false,	
			   	    anchor: '-15',
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'}],
					    data: [						
					    <?php echo acs_ar_to_select_json($deskArt->find_TA_std('AR012', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
					    ]
					},
					
					queryMode: 'local',
        			minChars: 1, 	
					listeners: { 
					 	beforequery: function (record) {
			         	record.query = new RegExp(record.query, 'i');
			         	record.forceAll = true;
		             }
		          }
					
			 	} ]}
			 	
	            ],
	            
				buttons: [	
				<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "RIEP_COD_ART");  ?>
				 {
	         	xtype: 'splitbutton',
	            text: 'Reports',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        		{ xtype: 'button',
		        		  text: 'Report per variante',
			              scale: 'medium',
			              handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                            t_report : 'V',
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	cod_variabile: '<?php echo $m_params->cod_variabile; ?>',
		                        	list_selected_id : '<?php echo json_encode($m_params->list_selected_id); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
		        		} ,
		        		
		        		{ xtype: 'button',
		        		  text: 'Report per fornitore',
			              scale: 'medium',
			              handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                            t_report : 'F',
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	list_selected_id : '<?php echo json_encode($m_params->list_selected_id); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
		        		} ,
		        	
		        	] <!-- end items -->
	        	} <!-- end men� -->
        	} <!-- end split button -->
		       ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}

if ($_REQUEST['fn'] == 'open_report'){

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   tr.liv_compo td{font-weight: normal; background-color: #eeeeee;}
   table.int1 td.no_border {border: 0px solid white;}
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 


$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$list_selected_id =	strtr($_REQUEST['list_selected_id'], array('\"' => '"'));
$list_selected_id = json_decode($list_selected_id);

$cod_variabile =	strtr($_REQUEST['cod_variabile'], array('\"' => '"'));


$ar_codici = array();
$ar_codici_raggr = array('hash' => array());
$ar_var_stampate = array();
$ar_totali = array();
$ar = array();
$ragg_var = $form_values->f_r_var;
  
    foreach($list_selected_id as $k => $v){

        //se ha scelto di utilizzare "Stringa codice DB" 
        // invece del codice variabile devo recuperare il campo yyyy dalla variabile/variante
        if ($_REQUEST['f_stringa_DB'] == 'Y'){
            $row_variante = get_TA_sys('PUVN', $v->cod, null, $cod_variabile, null, null, 9);
            $variante_cod = $row_variante['tarest'];
        } else {
          $variante_cod = $v->cod;
        }
        
        
        $ar_totali[trim($v->cod)]['desc'] = $v->desc;
        
        
        for($n = 1; $n <= 5; $n++){
            $cod_art = "";
            $a = 0;
            $compilato = 0;
                for($i = 1; $i <= 15; $i++){
                    
                    $cod = "f_[{$n}]{$i}";
                                    
                    if($form_values->$cod == '*'){   
                        $cod_art .= substr($variante_cod, $a, 1);
                        $a++;
                        $compilato = 1;
                    } else {
                        if(trim($form_values->$cod) == '')
                            $cod_art .= '_';
                        else {
                            $cod_art .= $form_values->$cod;
                            $compilato = 1;
                        }    
                    }
                    
                }
                
                if($compilato == 1)
                    $ar_codici[trim($v->cod)][$cod_art] = array('val' => array());
                
            }
      

    }   //foreach
    
    
    
ksort($ar_codici);
foreach ($ar_codici as $k => $v){
    
    
    $c_k1 = 0;
    foreach($v as $k1 => $v1){
        $c_k1++;
        $sql_where = " AND ARART LIKE '{$k1}%' ";
        
        if ($form_values->f_sospeso == 'Y')
            $sql_where.= " AND ARSOSP = 'S'";
        if ($form_values->f_sospeso == 'N')
            $sql_where.= " AND ARSOSP <> 'S'";
        
        if(isset($form_values->f_ciclo))
            $sql_where.= sql_where_by_combo_value('ARESAU', $form_values->f_ciclo);
        
        $sql = "SELECT COUNT(*) AS NR_ART, ARTPAR, ARSWTR, ARFOR1, ARTPAP, CF_FORNITORE.CFRGS1 AS D_FORNITORE
                FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
                ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
                WHERE ARDT = '{$id_ditta_default}' {$sql_where}
                GROUP BY ARTPAR, ARTPAP, ARSWTR, ARFOR1, CF_FORNITORE.CFRGS1
                ORDER BY ARFOR1, ARTPAR, ARTPAP, ARSWTR";   
        
        
    
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        while($row = db2_fetch_assoc($stmt)){
            
            $nr = array();
            $nr = $row;
            $nr['var'] = $k;
            
            
            $ar_totali[$k]['tot']['NR_ART'] += $nr['NR_ART'];
            $ar_totali[$k][$k1]['NR_ART'] += $nr['NR_ART'];
                
            $ar_codici[$k][$k1]['val'][] = $nr;
            
            
            $nr_ragg = $nr;
            unset($nr_ragg['var']);
            $ar_codici_raggr[$k][$c_k1]['val'][] = $nr_ragg;
            
            $liv = implode("_", array($k, trim($row['ARTPAR']), trim($row['ARTPAP']), trim($row['ARSWTR'])));
            
            if(!isset( $ar_fornitori[trim($row['ARFOR1'])]))
                $ar_fornitori[trim($row['ARFOR1'])] = array('val' => array());
            
            if(isset($ar_fornitori[trim($row['ARFOR1'])]['val'][$liv]))
                $ar_fornitori[trim($row['ARFOR1'])]['val'][$liv]['NR_ART'] +=  $nr['NR_ART'];
            else  
               $ar_fornitori[trim($row['ARFOR1'])]['val'][$liv] = $nr;
            
            $ar_fornitori[trim($row['ARFOR1'])]['desc'] = $nr['D_FORNITORE'];
            $ar_totali_for[trim($row['ARFOR1'])]['tot']['NR_ART'] += $nr['NR_ART'];
          
            $ar_tot_generale += $nr['NR_ART'];
        }
        
    } //per ogni stringa
   
    $hash_key = hash('ripemd160', print_r($ar_codici_raggr[$k], true));
    
    $ar_codici_raggr[$k]['hash'] = $hash_key;
    
    if (!isset($ar_codici_raggr['hash'][$hash_key]))
        $ar_codici_raggr['hash'][$hash_key] = array();
    $ar_codici_raggr['hash'][$hash_key][] = $k;
    
} //ar_codici (per ogni variante)



echo "<div id='my_content'>";
echo "<div class=header_page>";
if($_REQUEST['t_report'] == 'V')
    echo "<H2>Riepilogo codici articolo per variante</H2>";
else
    echo "<H2>Riepilogo codici articolo per fornitore</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";


echo "<tr class='liv_data'>
      <th>Cod.</th>
	  <th>Descrizione</th>
	  <th>Art.</th>
      <th>TP</th>   
      <th>ADP</th>
      <th>O/S</th>";
if($_REQUEST['t_report'] == 'V')
   echo "<th>Fornitore</th>"; 
else
   echo "<th>Variante</th>";
   
   echo  "</tr>";
     

echo " <tr class=liv3>
       <td colspan = 2>Totale generale</td>
       <td class=number>{$ar_tot_generale}</td>
       <td colspan = 4>&nbsp;</td>
       </tr>";


if($_REQUEST['t_report'] == 'V'){
//livello VARIANTI
    foreach($ar_codici as $k => $v){
        
        //esco se gia' ho stampato la variante (nel caso del raggruppamento)
        if (isset($ar_var_stampate[$k]))
            continue;
      
        echo "
            <tr class=liv3>
                <td>{$k}</td>
                <td>{$ar_totali[$k]['desc']}</td>
                <td class=number>" . n($ar_totali[$k]['tot']['NR_ART'], 0) . "</td>
                <td colspan = 5></td>
            </tr>";
        
        
        if ($_REQUEST['f_r_var'] == 'Y'){   //raggruppo per variabili con stessi risultati
            $m_hash = $ar_codici_raggr[$k]['hash'];
            
            foreach ($ar_codici_raggr['hash'][$m_hash] as $k_from_hash)
                if ($k_from_hash != $k){    
                  echo "
                    <tr class=liv3>
                        <td> -> {$k_from_hash}</td>
                        <td>{$ar_totali[$k_from_hash]['desc']}</td>
                        <td class=number>" . n($ar_totali[$k_from_hash]['tot']['NR_ART'], 0) . "</td>
                        <td colspan = 5></td>
                    </tr>";
                  $ar_var_stampate[$k_from_hash] = true;
                }
        }
        
        
        
            //livello sottostringa COD. ART
            foreach($v as $k1 => $v1){
                echo "
                <tr class=liv1>
                    <td colspan=2>{$k1}</td>
                    <td class=number>" . n($ar_totali[$k][$k1]['NR_ART'], 0) . "</td>
                    <td colspan = 5></td>
                </tr>";
                
                //livello valori raggruppati
                foreach($v1['val'] as $k2 => $v2){
                    
                    echo "
                        <tr class=liv0>
                            <td colspan=2>&nbsp;</td>
                            <td class=number>".$v2['NR_ART']."</td>
                            <td>".$v2['ARTPAR']."</td>
                            <td>".$v2['ARTPAP']."</td>
                            <td>".$v2['ARSWTR']."</td>
                            <td>[".$v2['ARFOR1']."] ".$v2['D_FORNITORE']."</td>
                    </tr>";
                    
                    
                    
                    
                }
                
            }  
                
            }
        
 }else{
 
          //livello fornitori
          foreach($ar_fornitori as $k => $v){
        
            
              
              echo "
              <tr class=liv3>
              <td>".$k."</td>
              <td>".$v['desc']."</td>
              <td class=number>" . n($ar_totali_for[$k]['tot']['NR_ART'], 0) . "</td>
                    <td colspan = 5></td>
                </tr>";
              
              //livello valori raggruppati
              foreach($v['val'] as $k1 => $v1){
                  
                      
                      echo "
                            <tr class=liv0>
                                <td colspan=2>&nbsp;</td>
                                <td class=number>".$v1['NR_ART']."</td>
                                <td>".$v1['ARTPAR']."</td>
                                <td>".$v1['ARTPAP']."</td>
                                <td>".$v1['ARSWTR']."</td>
                                <td>[".$v1['var']."] ".$ar_totali[$v1['var']]['desc']."</td>
                        </tr>";
                      
                      
                      
                      
                  }
                  
                  
          }
    
    }

?>
</div>
</body>
</html>	


<?php 
exit;
}




