<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   tr.liv_compo td{font-weight: normal; background-color: #eeeeee;}
   table.int1 td.no_border {border: 0px solid white;}
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$rp_listini =	$_REQUEST['rp_listini'];
$sql_where = sql_where_params($form_values);

if(isset($form_values->f_todo) && strlen($form_values->f_todo) > 0){
    
    if(isset($form_ep->f_utente_assegnato) && strlen($form_ep->f_utente_assegnato) > 0)
        $as_where = " AND ASUSAT = '{$form_ep->f_utente_assegnato}'";
        
        
        $sql_join .= "INNER JOIN (
        SELECT DISTINCT ASDOCU
        FROM {$cfg_mod_DeskArt['file_assegna_ord']}
        WHERE ASDT = '{$id_ditta_default}' AND ASFLRI <> 'Y' AND ASCAAS = '{$form_values->f_todo}' {$as_where}
        ) AS0
        ON AS0.ASDOCU = AR.ARART";
}

if(isset($form_values->f_desc_nota) && strlen($form_values->f_desc_nota) > 0){
    
    if(isset($form_ep->f_blocco) && strlen($form_ep->f_blocco) > 0)
        $bl_where = " AND RLRIFE2 = '{$form_ep->f_blocco}'";
        
        $sql_join .= " INNER JOIN (
        SELECT DISTINCT RLDT, RLRIFE1
        FROM {$cfg_mod_DeskUtility['file_note_anag']}
        WHERE RLTPNO = 'AR' {$bl_where}
        AND UPPER(CONCAT(RLSWST, CONCAT(RLREST1, RLFIL1))) LIKE '%" . strtoupper($form_values->f_desc_nota) . "%'
	      ) RL
	       ON RL.RLDT = AR.ARDT AND RL.RLRIFE1 = AR.ARART";
        
}


$sql = "SELECT AR.*, DB.*, DV.*, CF_FORNITORE.CFRGS1 AS D_FORNITORE,  AR_COMP.ARDART AS D_COMP
        FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
            ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
        LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
            ON AU.AUDT = AR.ARDT AND AU.AUART = AR.ARART AND AU.AUUTEN = '{$utente}'
        INNER JOIN {$cfg_mod_DeskUtility['file_distinta_base']} DB
            ON DB.DBDT = AR.ARDT AND DB.DBART = AR.ARART
        LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR_COMP
            ON DB.DBDT = AR_COMP.ARDT AND DB.DBCOMP = AR_COMP.ARART
        LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_condizioni_distinta']} DV
            ON AR.ARDT = DV.DVDT AND AR.ARART = DV.DVART AND DB.DBSEQ1 = DV.DVSEQ1 AND DB.DBSEQ2 = DV.DVSEQ2 AND DB.DBCOMP = DV.DVCOMP AND DB.DBSEQR = DV.DVSEQR  
        LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art']} AX
		ON AX.AXDT = AR.ARDT AND AR.ARART = AX.AXART
       WHERE AR.ARDT = '{$id_ditta_default}' {$sql_where}
       ORDER BY ARART, DB.DBSEQ1, DB.DBSEQ2, DB.DBCOMP, DB.DBSEQR
       FETCH FIRST 2000 ROWS ONLY";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $sql_a = "SELECT ARDIM1, ARDIM2, ARDIM3 FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
              WHERE ARDT = '{$id_ditta_default}' AND ARART = ?";
    
    $stmt_a = db2_prepare($conn, $sql_a);
    echo db2_stmt_errormsg();
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr = $row;
   
        $ar[] = $nr;
    }

echo "<div id='my_content'>";
echo "<div class=header_page>";
echo "<H2>Riepilogo distinta base</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";


echo "
  <tr class='liv_data'>
  <th rowspan=2 > <img src=" . img_path("icone/48x48/divieto.png") . " height=20></th>
  <th rowspan=2 >C</th>
  <th rowspan=1 colspan=2>Sequenza</th>  
  <th rowspan=2>Articolo</th>
  <th rowspan=2>R</th>
  <th rowspan=2>Descrizione</th>

  <th rowspan=2>UM</th>
  <th rowspan=2>Q.t&agrave;</th>  
  <th colspan = 7 class='center'> Condizioni </th>
  <th colspan = 2 class='center'> Immissione </th>
  </tr> 
  <tr class='liv_data'>

          <th>1</th>
          <th>2</th>
      
          <th>Cond.</th>
          <th>I/E</th>
          <th>Variabile</th>
          <th>N</th>
          <th>Test</th>
          <th>Valore</th>          
          <th>O/S</th>

          <th>Data</th>
          <th>Utente</th>

   </tr>";
echo "</tr>";  

$ultimo_art='';
$ultimo_liv_compo = '';
foreach ($ar as $kar => $r){
    
    $liv_compo = implode("_", array($r['ARART'], $r['DBSEQ1'], $r['DBSEQ2'], $r['DBCOMP'], $r['DBSEQR']));
    $m_anag_p = "LxHxP ".n($r['ARDIM1'], 2)."x".n($r['ARDIM2'], 2)."x".n($r['ARDIM3'], 2);
    
    //ARTICOLO PADRE
    if(trim($r['ARART']) != $ultimo_art){
        
        $c_art = "<b>".trim($r['ARART'])."</b><br>";
        $d_art = "<b>".trim($r['ARDART'])."</b><br>";
       
        if($ultimo_art != '')
            echo "<tr><td colspan = 18 class=no_border>&nbsp;</td></tr>";
        echo "<tr class = 'liv1'>";
        if(trim($r['ARSOSP']) == 'S')
            echo "<td><img src=" . img_path("icone/48x48/divieto.png") . " height=15></td>";
        else
            echo "<td>&nbsp;</td>";
        if(trim($r['ARESAU']) == 'E')
            echo "<td><img src=" . img_path("icone/48x48/clessidra.png") . " height=15></td>";
        elseif(trim($r['ARESAU']) == 'A')
            echo "<td><img src=" . img_path("icone/48x48/label_blue_new.png") . " height=15></td>";
        elseif(trim($r['ARESAU']) == 'C')
            echo "<td><img src=" . img_path("icone/48x48/design.png") . " height=15></td>";
        elseif(trim($r['ARESAU']) == 'R')
            echo "<td><img src=" . img_path("icone/48x48/folder_private.png") . " height=15></td>";
        elseif(trim($r['ARESAU']) == 'F')
            echo "<td><img src=" . img_path("icone/48x48/button_blue_play.png") . " height=15></td>";
        else
            echo "<td>&nbsp;</td>";
      echo "<td colspan=2>&nbsp;</td>
           <td valign = top>".$c_art."</td>
           <td colspan=1>&nbsp;</td>
   		   <td valign = top>".$d_art."</td>
           <td valign = top>".$r['ARUMTE']."</td>
           <td valign = top>&nbsp;</td>
           <td valign = top colspan=3>&nbsp;</td>
           <td valign = top colspan=4>".$m_anag_p."</td>
           <td valign = top>".print_date($r['ARDTUM'])."</td>
           <td valign = top>".$r['ARUSUM']."</td>
          </tr>";  
        
    }
   
    //DISTINTE CON CONDIZIONI
    
    $result = db2_execute($stmt_a, array($r['DBCOMP']));
    $row_a = db2_fetch_assoc($stmt_a);
    $m_anag_d = "LxHxP ".n($row_a['ARDIM1'], 2)."x".n($row_a['ARDIM2'], 2)."x".n($row_a['ARDIM3'], 2);
    
    if(trim($r['DBVRV1']) != ''){
        $des_var1 = get_TA_sys('PUVR', $r['DBVRV1']);
        $des_van1 = get_TA_sys('PUVN', $r['DBVNV1']);
        $m_anag_d .= "<br>Var.D1: [".trim($r['DBVRV1'])."] ".$des_var1['text']." [".trim($r['DBVNV1'])."] ".$des_van1['text'];
    }
    
    if(trim($r['DBVRV2']) != ''){
        $des_var2 = get_TA_sys('PUVR', $r['DBVRV2']);
        $des_van2 = get_TA_sys('PUVN', $r['DBVNV2']);
        $m_anag_d .= "<br>Var.D2: [".trim($r['DBVRV2'])."] ".$des_var2['text']." [".trim($r['DBVNV2'])."] ".$des_van2['text'];
    }
        
    if ($liv_compo != $ultimo_liv_compo){
       
       if (strlen(trim($r['DBRGCO'])) > 0){
           $des_MURC = get_TA_sys('MURC', $r['DBRGCO']);
           if(strlen(trim($r['DBTPAL'])) > 0)
               $out_DBCOMP = trim($r['DBCOMP']) ." [{$r['DBTPAL']}] <br><div style='text-align: right;'>[" . trim($r['DBRGCO']) . "]<div>";
           else
               $out_DBCOMP = trim($r['DBCOMP']) . "<br><div style='text-align: right;'>[" . trim($r['DBRGCO']) . "]<div>";
           $out_D_COMP = trim($r['D_COMP']) . "<br>Regola: " . $des_MURC['text']; 
       } else {
           $out_DBCOMP = trim($r['DBCOMP']);
           if(strlen(trim($r['DBTPAL'])) > 0)
               $out_DBCOMP .= " [{$r['DBTPAL']}]";
           $out_D_COMP = trim($r['D_COMP']);
       }

       
       $out_dim = "";
       for($i = 1; $i<=3; $i++){
          
           if($r["DBDIM{$i}"] > 0 || trim($r["DBRDI{$i}"]) != '' || trim($r["DBVDI{$i}"]) != '' || trim($r["DBQDI{$i}"]) != '' ){
           
               $dim = "";
              switch ($i){
                  case 1 :
                     $dim .= "L/D ";
                     break;
                  case 2:
                      $dim .= "H/L ";
                     break;
                  case 3:
                      $dim .= "S/P ";
                      break;
              }
              
              $d_var = get_TA_sys('PUVR', trim($r["DBVDI{$i}"]));
              $out_dim .= $dim .n($r["DBDIM{$i}"],3).", ".trim($r["DBRDI{$i}"]).", [".trim($r["DBVDI{$i}"])."] ".$d_var['text'].", ".trim($r["DBQDI{$i}"])."<br>";

           }
           
       }
       
       if(trim($r['DBVARQ']) != '' || trim($r['DBSWVQ']) != '' || trim($r['DBPO10']) != '' || trim($r['DBSWAR']) != ''){
           
           $d_var = get_TA_sys('PUVR', trim($r['DBVARQ']));
           $qta_10 = $deskArt->find_TA_std('QTA10', $r['DBPO10']);
           $out_dim .= "[".trim($r['DBVARQ'])."] ".$d_var['text'].", ".trim($r['DBSWVQ']).", /".$qta_10[0]['text'].", ".trim($r['DBSWAR']); 
       }
      
	   echo "
          <TR>
            <td colspan=2>&nbsp;</td>
            <td valign = top>".$r['DBSEQ1']."</td>
            <td valign = top>".$r['DBSEQ2']."</td>
	        <td valign = top>".$out_DBCOMP."</div></td>
            <td valign = top>".$r['DBSEQR']."</td>
   			<td valign = top>".$out_D_COMP."</div></td>
 			<td valign = top>".$r['DBUM']."</td>
            <td valign = top>".n($r['DBQTA'],2)."</td>
            <td valign = top colspan=3>".$out_dim."</td>
            <td valign = top colspan=4>".$m_anag_d."</td>
            <td valign = top>".print_date($r['DBDTUM'])."</td>
            <td valign = top>".$r['DBUSUM']."</td>
          </TR>
         ";
    }
	

	
	
	$ar_cond = array();
	
	
	if ($liv_compo != $ultimo_liv_compo){
	    for($i = 1; $i <= 4 ; $i++){
	        
	        $cond = "";
	        $variabile = "";
	        $var_num = "";
	        $variante = "";
	        $tipologia = "";
	        $tipologia2 = "";
	        $obb_scelta = "";
	        
	        //FILE DB0
	        if (strlen(trim($r["DBVRS{$i}"])) > 0){
	            $val_variabile = find_TA_sys('PUVR', trim($r["DBVRS{$i}"]));
	            $variabile .= "[".trim($r["DBVRS{$i}"])."] ".$val_variabile[0]['text']. "<br>";
	            
	            	            
	            if(trim($r["DBOPS{$i}"]) != '')
	                $tipologia .= trim($r["DBOPS{$i}"]). "<br>";
	            
	            if(trim($r["DBOPS{$i}"]) == 'T'){
	                //=
	                $val_variante = find_TA_sys('PUTI', trim($r["DBVNS{$i}"]));
	                if(trim($r["DBVNS{$i}"]) != '')
	                    $variante .= "[".trim($r["DBVNS{$i}"])."] ".$val_variante[0]['text']. "<br>";
	            } else {
	                //=	                
	                $val_variante = find_TA_sys('PUVN', trim($r["DBVNS{$i}"]), null, trim($r["DBVRS{$i}"]));
	                if(trim($r["DBVNS{$i}"]) != '')
	                    $variante .= "[".trim($r["DBVNS{$i}"])."] ".$val_variante[0]['text']. "<br>";
	            }
	                
	                


                
                if(trim($r["DBSWS{$i}"]) != '')
                    $tipologia2 .= trim($r["DBSWS{$i}"]). "<br>";
                
                if(trim($r["DBOSS{$i}"]) != '')
                    $obb_scelta .= trim($r["DBOSS{$i}"]). "<br>";
                
                $ar_cond[] = array(
                   'cond' => '',
                    'ie'        => out_IE($r['DBSWIE']),
                    'variabile' => $variabile,
                    'var_num'   => '',
                    'tipologia' => $tipologia,
                    'variante'  => $variante,
                    'obb_sce'   => $obb_scelta
                );
	        }
	        
	                       

	    }
	}
	
	
	

	
	
	
	

	for($i = 1; $i <= 12 ; $i++){
               
	    $cond = "";
	    $variabile = "";
	    $var_num = "";
	    $variante = "";
	    $tipologia = "";
	    $tipologia2 = "";
	    $obb_scelta = "";
	    
	    //FILE DV0
	    if (strlen(trim($r["DVVRC{$i}"])) > 0){
            $val_variabile_DV = find_TA_sys('PUVR', trim($r["DVVRC{$i}"]));                
            
            if(trim($r["DVVRC{$i}"]) != '')
              $cond .= trim($r["DVSEQC"]). "<br>";
            
            if(trim($r["DVVRC{$i}"]) != '')
                $variabile .= "[".trim($r["DVVRC{$i}"])."] ".$val_variabile_DV[0]['text']. "<br>";
    	              
                
            if(trim($r["DVTPV{$i}"]) != '')
                $tipologia .= trim($r["DVTPV{$i}"]). "<br>";
            
            if (trim($r["DVTPV{$i}"]) == 'T'){                
                $val_valore = find_TA_sys('PUTI', trim($r["DVVAL{$i}"]));
                if(trim($r["DVVAL{$i}"]) != '')
                    $variante .= "[".trim($r["DVVAL{$i}"])."] ".$val_valore[0]['text']. "<br>";
            }
            else { //=
                $val_valore = find_TA_sys('PUVN', trim($r["DVVAL{$i}"]), null, trim($r["DVVRC{$i}"]));
                if(trim($r["DVVAL{$i}"]) != '')
                    $variante .= "[".trim($r["DVVAL{$i}"])."] ".$val_valore[0]['text']. "<br>";
            }
          
            
            if(trim($r["DVOPV{$i}"]) != '')
                $obb_scelta .= trim($r["DVOPV{$i}"]). "<br>";
    
            if(trim($r["DVSWN{$i}"]) != '')
                $var_num .= trim($r["DVSWN{$i}"]). "<br>";
            

            
            $ar_cond[] = array(
                'cond'       => $r['DVSEQC'],
                'ie'        => out_IE($r['DVSWIE']),
                'variabile' => $variabile,
                'var_num'   => '',
                'tipologia' => $tipologia,
                'variante'  => $variante,
                'obb_sce'   => $obb_scelta
            );
	    }
	}
	
	
	
	

        
 /*       
	echo   "<td valign = top>".$cond."</td>
            <td valign = top>".out_IE($r['DBSWIE'])."<br>".out_IE($r['DVSWIE'])."</td>
            <td valign = top>".$variabile."</td>
            <td valign = top>".$var_num."</td>
	        <td valign = top>".$tipologia."</td>
            <td valign = top>".$variante."</td>
            <td valign = top>".$obb_scelta."</td>
            ";
*/            
	

	
	

    
    $n_cond = count($ar_cond);
    $old_cond = '';
    $old_ie   = '';
    foreach($ar_cond as $k_cond => $cond){
        echo "<tr>";
        
        //if ($k_cond == 0)
            echo "<td colspan=9 class=no_border>&nbsp;</td>";
        
        if ($old_cond != $cond['cond'])
            echo "<td valign = top>".$cond['cond']."</td>";
        else
            echo "<td valign = top>&nbsp;</td>";
        
        if ($old_ie != $cond['ie'])
            echo "<td valign = top>".$cond['ie']."</td>";
        else
            echo "<td valign = top>&nbsp;</td>";
            
        echo   "
            <td valign = top>".$cond['variabile']."</td>
            <td valign = top>".$cond['var_num']."</td>
	        <td valign = top>".$cond['tipologia']."</td>
            <td valign = top>".$cond['variante']."</td>
            <td valign = top>".$cond['obb_sce']."</td>
            <td colspan=2 class=no_border>&nbsp;</td>
         </tr>
            ";
        $old_cond = $cond['cond'];
        $old_ie = $cond['ie'];
    }
    
    
    
    
    $ultimo_art =  trim($r['ARART']);
    $ultimo_liv_compo = $liv_compo;
 }



 function out_IE($v){
     if (trim($v) == 'I')   return 'Incl.se:';
     if (trim($v) == 'E')   return 'Escl.se:';
     return $v;
 }

?>
</div>
</body>
</html>	





