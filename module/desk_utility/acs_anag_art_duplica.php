<?php
require_once "../../config.inc.php";

$m_DeskArt = new DeskArt(array('no_verify' => 'Y'));
$main_module = new DeskUtility();

$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'exe_duplica'){
    
    $m_params = acs_m_params_json_decode();

    $use_session_history = microtime(true);
    
    $sql = "SELECT COUNT(*) AS C_ROWS
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARART = '{$m_params->form_values->f_cod_art}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if($row['C_ROWS'] > 0){
        
        
        $ret['success'] = false;
        $ret['error_msg'] = 'Articolo gi&agrave; esistente';
        echo acs_je($ret);
        
        
    }else{
   
 
    
    
    $sh = new SpedHistory($m_DeskArt);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DUP_ANAG_ART',
            "vals" => array(
                "RICDOLD" 	=> $m_params->form_values->f_old_art,
                "RICDNEW" 	=> $m_params->form_values->f_cod_art,
                "RIDART" 	=> $m_params->form_values->f_desc_art,
                "RIFG01" 	=> $m_params->form_values->f_esau,
                "RIDBFB" 	=> $m_params->form_values->f_rif_tecnico,
                "RIARFO" 	=> $m_params->form_values->f_rif,
                "RIGRAL" 	=> $m_params->form_values->f_alt,
                "RIMOCO" 	=> $m_params->form_values->f_comm,
                "RICEEU" 	=> $m_params->form_values->f_cert,
                "RIDIM1" 	=> sql_f($m_params->form_values->f_l),
                "RIDIM2" 	=> sql_f($m_params->form_values->f_h),
                "RIDIM3" 	=> sql_f($m_params->form_values->f_p),
                "RIPLOR" 	=> sql_f($m_params->form_values->f_pl),
                "RIPNET" 	=> sql_f($m_params->form_values->f_pn),
                "RIVOLU" 	=> sql_f($m_params->form_values->f_v),
                "RIDT"  	=> $id_ditta_default,
            ),
        )
        );
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    }
    
    exit;
}

if ($_REQUEST['fn'] == 'exe_new_cod'){
    
    $m_params = acs_m_params_json_decode();
    $ret = array();
    $ar_ins = array();
    
    $sql_s = "SELECT *
    FROM {$cfg_mod_DeskArt['file_cfg_distinte']}
    WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$m_params->params->distinta}'
    AND CDCMAS = '{$m_params->params->master}' AND CDSLAV = '{$m_params->params->voce}'";
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_s);
    $row_s = db2_fetch_assoc($stmt_s);
    $prog = trim($row_s['CDPROG']);
    $new_prog = $prog + 1;
    
    $c_art = $m_params->params->radice.$m_params->t_id.$new_prog;
   
    
    $sql_a = "SELECT COUNT(*) AS C_ROWS
    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
    WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$c_art}'";
  
    
    $stmt_a = db2_prepare($conn, $sql_a);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_a);
    $row_a = db2_fetch_assoc($stmt_a);
    if($row_a['C_ROWS'] > 0){
        
        
        $ret['success'] = false;
        $ret['error_msg'] = 'Articolo gi&agrave; esistente';
   
        
    }else{
    
    $ar_ins['CDUSUM'] 	= $auth->get_user();
    $ar_ins['CDDTUM']   = oggi_AS_date();
    $ar_ins['CDORUM'] 	= oggi_AS_time();
    $ar_ins['CDPROG'] 	= $new_prog;
    
    $sql = "UPDATE {$cfg_mod_DeskArt['file_cfg_distinte']}
    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
    WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$m_params->params->distinta}' 
    AND CDCMAS = '{$m_params->params->master}' AND CDSLAV = '{$m_params->params->voce}' ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    
    $ret['success'] = true;
    $ret['new_prog'] = $new_prog;
    }
    
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    $c_art = $m_params->c_art;
    $d_art = $m_params->d_art;
    
    $sql = "SELECT *
    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
    WHERE ARART = '{$c_art}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    $descrizione = trim($row['ARDART']);
    $cee = substr($row['ARFIL1'], 26, 1);  //stosa
        
    
    $sql_s = "SELECT *
    FROM {$cfg_mod_DeskArt['file_cfg_distinte']}
    WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$m_params->distinta}'
    AND CDCMAS = '{$m_params->master}' AND CDSLAV = '{$m_params->voce}'";
    
   
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_s);
    $row_s = db2_fetch_assoc($stmt_s);
    $prog = trim($row_s['CDPROG']);
 
    
    ?>

{"success":true, "items": [

        {
				xtype: 'form',
	             bodyStyle: {
                            padding: '10px',
                            align: 'stretch'
                        },
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
				items: [
				       {xtype: 'textfield',
						name: 'f_old_art',
						hidden : true,
						value: <?php echo j($c_art); ?>
						},
				
				<?php if($m_params->from_indici == 'Y'){ ?>
				    
				       {xtype: 'textfield',
						name: 'f_cod_art',
						hidden : true
					
						},
			
							 {
					xtype: 'fieldset',
	                title: 'Configurazione articolo',
	                style: 'padding:5px',
                    bodyStyle: 'padding:5px',
	                layout: 'anchor',
					flex:1,
	                items: [
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
    					   {xtype: 'displayfield',
    						name: 'f_radice',
    						fieldLabel: 'Radice',
    						labelWidth : 50,
    						width : 130,
    					    value: <?php echo j($m_params->radice); ?>
    						},  
    						{xtype: 'displayfield',
    						name: 'f_prog',
    						fieldLabel: 'Ultimo ID',
    						labelWidth : 60,
    						width : 110,
    						value: <?php echo j($prog); ?>
    						}
    						
    				
						]}
							   
 					
					 
	             ]},
	             
	             	{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							{xtype: 'textfield',
    						name: 'f_cod',
    						fieldLabel: 'Nuovo codice',
    						labelWidth : 80,
    						width : 170,
    						readOnly : true,
    						value: <?php echo j($m_params->radice); ?>
    						}, 
    						{xtype: 'textfield',
    						name: 'f_new_c',
    					 	width : 80,
    					 	margin : '0 10 0 0'
    					 	},{xtype: 'button', 
    						  scale: 'small',
    						  text: 'Genera nuovo codice', 
    						//iconCls: 'icon-address_blue-16', 
    						  handler: function(event, toolEl, panel){
    						  
    						  form = this.up('form').getForm();
    						  form_values = form.getValues();
    						  var loc_win = this.up('window');
    						  
    						   var prog = parseInt(<?php echo $m_params->prog; ?>) + 1;  
							 	 var digits = prog.toString().length;  
							 	 var p_id = <?php echo $m_params->nr_id; ?> - digits;
							 	 var t_id = '';
								 for(var i = 1; i <= p_id ; i++){
								     t_id = t_id.concat('0')
								    
								 }
							
							     console.log(form_values);
							     
							    Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_new_cod',
 						        timeout: 2400000,
 						        method     : 'POST',
 			        			jsonData: {
 			        			    t_id : t_id, 
 			        			    params : <?php echo acs_je($m_params); ?>
 			        			},							        
 						        success : function(result, request){
 						            jsonData = Ext.decode(result.responseText);
 						        	   if(jsonData.success == false){
    	 						        	acs_show_msg_error(jsonData.error_msg);
    	 						        	return;
     						        	}else{
 						        			form.findField('f_prog').setValue(jsonData.new_prog);
     						       			var new_c = t_id + jsonData.new_prog;
							        		form.findField('f_new_c').setValue(new_c);
							        		var cod_art = <? echo j($m_params->radice); ?> + t_id + jsonData.new_prog;
							       			form.findField('f_cod_art').setValue(cod_art);
							       			loc_win.fireEvent('afterSave', loc_win);
							        
							        }
 						    					            			
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						       });	
							     
							     
							         						
    							}
    						}
    						
						   
						
						]},
						
						{xtype: 'textfield',
						name: 'f_desc_art',
						fieldLabel: 'Descrizione ',
						anchor: '-15',
						labelWidth : 80,	
						value: <?php echo j($descrizione); ?>,
						maxLength : 50
						},
				
				
				<?php }else{?>
				
				       { 
                    	xtype: 'fieldcontainer',
                    	flex: 1, 
                   		 layout: 'hbox',		
                    					
                    	items: [
                        	{xtype: 'textfield',
    						name: 'f_cod_art',
    						margin : '0 15 0 0',
    						fieldLabel: 'Codice',
    						labelWidth : 80,
    						maxLength : 15,
    						flex : 1,
    						value: <?php echo j($c_art); ?>,
    						listeners:{
                           		change:function(field){
                                field.setValue(field.getValue().toUpperCase());
                           			}
                      		 }
    						},{xtype: 'textfield',
    						name: 'f_rif_tecnico',
    						fieldLabel: 'Rif.tecnico',
    						labelWidth : 80,
    						maxLength : 15,
    						margin : '0 15 0 0',
                    		labelAlign : 'right',	
                    		flex : 1,
    						value: <?php echo j(trim($row['ARRIF'])); ?>,
    						listeners:{
                           		change:function(field){
                                field.setValue(field.getValue().toUpperCase());
                           			}
                      		 }
    						}
                    	
                    	]},
				       {xtype: 'textfield',
						name: 'f_desc_art',
						fieldLabel: 'Descrizione ',
						labelWidth : 80,
						maxLength : 50,
						anchor: '-15',	
						value: <?php echo j($descrizione); ?>
						},
				
				<?php }?>
				
				{ 
                    	xtype: 'fieldcontainer',
                    	flex: 1, 
                   		 layout: 'hbox',		
                    					
                    	items: [
                    		{xtype: 'textfield',
                    		name: 'f_rif',
                    		flex : 1,
                    		fieldLabel: 'Cod. riferim. ',
                    		margin : '0 15 0 0',
                    		maxLength : 15,
                    		labelWidth : 80,
                    		value: '<?php echo trim($row['ARARFO']); ?>'
                    	
                    		},
                    		{xtype: 'textfield',
                    		name: 'f_alt', 
                    		flex : 1, 
                    		margin : '0 15 0 0',
                    		labelAlign : 'right',
                    		fieldLabel: 'Cod. altern. ',
                    		labelWidth : 80,
                    		maxLength : 15,
                    		value: '<?php echo trim($row['ARGRAL']); ?>'
                    	                   		
                    		}
                    	]}, 
					{
							 
					    xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    
						{
    					xtype: 'fieldset',
    	                title: 'Dimensioni',
    	                layout: 'anchor',
    	                flex:1,
    	                items: [
    						{xtype: 'numberfield',
    						name: 'f_l',
    						fieldLabel: 'L',
    						labelWidth: 70,
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						value: '<?php echo $row['ARDIM1']; ?>',
    						decimalPrecision : 4
    						},
    						{xtype: 'numberfield',
    						name: 'f_h',
    						labelWidth: 70,
    						fieldLabel: 'H',
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						value: '<?php echo $row['ARDIM2']; ?>',
    						decimalPrecision : 4
    						},{xtype: 'numberfield',
    						name: 'f_p',
    						labelWidth: 70,
    						fieldLabel: 'S',
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						value: '<?php echo $row['ARDIM3']; ?>',
    						decimalPrecision : 4
    						}
                				
                		
        	             ]}, {
    					xtype: 'fieldset',
    	                title: 'Peso/Volume',
    	                layout: 'anchor',
    	                flex:1,
    	                items: [
    						{xtype: 'numberfield',
    						name: 'f_pn',
    						fieldLabel: 'Peso netto',
    						labelWidth: 70,
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						value: '<?php echo $row['ARPNET']; ?>',
    						decimalPrecision : 4
    						},
    						{xtype: 'numberfield',
    						name: 'f_pl',
    					    labelWidth: 70,
    						fieldLabel: 'Peso lordo',
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						value: '<?php echo $row['ARPLOR']; ?>',
    						decimalPrecision : 4
    						},{xtype: 'numberfield',
    						name: 'f_v',
    						labelWidth: 70,
    						fieldLabel: 'Volume',
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						value: '<?php echo $row['ARVOLU']; ?>',
    						decimalPrecision : 4
    						}
                				
                		
        	             ]}
						]},{
							name: 'f_esau',
							xtype: 'combo',
							fieldLabel: 'Ciclo di vita',
							value: '<?php echo trim($row['ARESAU']); ?>',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($m_DeskArt->find_TA_std('AR012', null, 'N', 'N', null, null, null, 'N', 'Y'), '');  ?> 	
								    ] 
							}						 
						}, {
							name: 'f_comm',
							xtype: 'combo',
							fieldLabel: 'Mod. commerc.',
							value: '<?php echo trim($row['ARMOCO']); ?>',
							queryMode: 'local',
							minChars: 1,
							displayField: 'text',
							valueField: 'id',
							width : 465,
							//flex : 1,
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [	
							        {id : '', text : 'Non definito []'},							    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUMC'), ''); ?>	
								    ] 
							},listeners: {
                            		beforequery: function (record) {
                            			record.query = new RegExp(record.query, 'i');
                            			record.forceAll = true;
                            		}
                              }						 
						}
					 ],
					 
			buttons: [					
				{
		            text: 'Conferma',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		             var form = this.up('form').getForm();
		             var form_values = form.getValues();
		             var loc_win = this.up('window');
		             list_rows_modified = [];
		             list_rows_modified.push({
		      			cod : form_values.f_old_art,
		              	n_cod : form_values.f_cod_art,
		             });
		             
		             if(form_values.f_cod_art.trim() == ''){
		                  acs_show_msg_error('Inserire un codice articolo');
	 					  return;
		             }
		             
		             
		             
		             	 Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_duplica',
 						        timeout: 2400000,
 						        method     : 'POST',
 			        			jsonData: {
 			        			    form_values : form_values, 
 			        			    art_dup : '<?php echo $m_params->art_dup; ?>'
 								},							        
 						        success : function(result, request){
 						           jsonData = Ext.decode(result.responseText);
     						        	if(jsonData.success == false){
    	 						        	acs_show_msg_error(jsonData.error_msg);
    	 						        	return;
     						        	}else{
     						        	
     						        		acs_show_win_std('Conferma dati accessori da duplicare', 'acs_anag_art_duplica_ass.php?fn=open_win', 
		  				    	 			{list_rows_modified : list_rows_modified}, 500, 400, null, 'icon-leaf-16');  
     						        	
    			    		  			     if (!Ext.isEmpty(loc_win.events.afteroksave))
												loc_win.fireEvent('afterOkSave', loc_win);
										     else
					    						loc_win.close(); 
        						        	  
     						        	
     						        	}
 						    		
 						    		
 						    						            			
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
		             
		       
		                
		            }
		        }
	        
	        
	        ]  
					
					
	}
	
]}


<?php 
}
