<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();


// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
// ******************************************************************************************

    //solo DV
    if ($m_params->form_values->type_cond == 'DV'){
        $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_condizioni_distinta']} DV WHERE RRN(DV) = ? ";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($m_params->form_values->rrn));
        $error_msg =  db2_stmt_errormsg($stmt);
        $ret = array();
        if (strlen($error_msg) > 0) {
            $ret['success'] = false;
            $ret['message'] = $error_msg;
        }
    } else {    //DB (reset dei campi)
        $ar_upd['DBUSUM'] 	= $auth->get_user();
        $ar_upd['DBDTUM'] 	= oggi_AS_date();
        $ar_upd['DBSWIE'] 	= '';
        for($i = 1; $i <= 4 ; $i ++){
              $ar_upd["DBVRS{$i}"]   = '';
              $ar_upd["DBVNS{$i}"]   = '';
              $ar_upd["DBOSS{$i}"]   = '';
              $ar_upd["DBOPS{$i}"]   = '';
        }
        
        $sql = "UPDATE {$cfg_mod_DeskUtility['file_distinta_base']} DB SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE RRN(DB) = '{$m_params->form_values->rrn}' ";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        
        $result = db2_execute($stmt, $ar_upd);
        $error_msg =  db2_stmt_errormsg($stmt);
        $ret = array();
        if (strlen($error_msg) > 0) {
            $ret['success'] = false;
            $ret['message'] = $error_msg;
        }
    }
    $ret['success'] = $result;
    echo acs_je($ret);
  exit;
}

// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_config'){
// ******************************************************************************************
 ini_set('max_execution_time', 3000);
 $form_values = $m_params->form_values;
 $ar_upd = array();
    
 if ($form_values->type_cond == 'DB'){
    $ar_upd['DBUSUM'] 	= $auth->get_user();
    $ar_upd['DBDTUM'] 	= oggi_AS_date();
    $ar_upd['DBSWIE'] 	= $form_values->swie;
    for($i = 1; $i <= 4 ; $i ++){
        $var = "variabile_{$i}";
        $val = "variante_{$i}";
        $tp =  "tipologia_{$i}";
        $os =  "obb_scelta_{$i}";
        if(isset($form_values->$var))
            $ar_upd["DBVRS{$i}"]   = $form_values->$var;
        if(isset($form_values->$val))
            $ar_upd["DBVNS{$i}"]   = $form_values->$val;
        if(isset($form_values->$val))
            $ar_upd["DBOSS{$i}"]   = $form_values->$os;
        if(isset($form_values->$val))
            $ar_upd["DBOPS{$i}"]   = $form_values->$tp;
    }

    $sql = "UPDATE {$cfg_mod_DeskUtility['file_distinta_base']} DB SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(DB) = '{$form_values->rrn}' ";       
 } //DB
 
 
 if ($form_values->type_cond == 'DV'){
     //$ar_upd['DBUSUM'] 	= $auth->get_user();
     //$ar_upd['DBDTUM'] 	= oggi_AS_date();
     $ar_upd['DVSEQC'] 	= $form_values->cond;
     $ar_upd['DVSWIE'] 	= $form_values->swie;
     $ar_upd['DVNOTE'] 	= acs_toDb($form_values->note);
     for($i = 1; $i <= 12 ; $i ++){
         $var = "variabile_{$i}";
         $val = "variante_{$i}";
         $tp =  "tipologia_{$i}";
         $os =  "obb_scelta_{$i}";
         if(isset($form_values->$var)){
            
             if(strlen($form_values->$var) > 0){
                 $row = get_TA_sys('PUVR', $form_values->$var, null, null, null, null, 1);
                 $numerica = $row['tarest'];
                 $ar_upd["DVVRC{$i}"]   = $form_values->$var;
                 $ar_upd["DVSWN{$i}"]   = $numerica;
             }else{
                 $ar_upd["DVVRC{$i}"]   = '';
                 $ar_upd["DVSWN{$i}"]   = '';
             }
             
         } 
         if(isset($form_values->$val))
           $ar_upd["DVVAL{$i}"]   = $form_values->$val;
         if(isset($form_values->$val))
           $ar_upd["DVOPV{$i}"]   = $form_values->$os;
         if(isset($form_values->$val))
           $ar_upd["DVTPV{$i}"]   = $form_values->$tp;
     }
     
      
     $sql = "UPDATE {$cfg_mod_DeskUtility['file_condizioni_distinta']} DV SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
             WHERE RRN(DV) = '{$form_values->rrn}' ";
 } //DV
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);        
    
    $ret = array();
    $ret['success'] = $result;
    echo acs_je($ret);
    
 exit;
}



// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi_config'){
// ******************************************************************************************
    ini_set('max_execution_time', 3000);
    $form_values = $m_params->form_values;
    $ar_upd = array();
    
    //posso aggiungere solo condizioni di tipo DV    

    //chiave
    $ar_upd['DVDT'] = $id_ditta_default;
    $ar_upd['DVART']    = $m_params->record->dbart_p;
    $ar_upd['DVSEQ1']   = $m_params->record->seq1;
    $ar_upd['DVSEQ2']   = $m_params->record->seq2;
    $ar_upd['DVCOMP']   = $m_params->record->cod_comp;
    $ar_upd['DVSEQR']   = $m_params->record->rip;
    //chiave
    
    $ar_upd['DVSEQC'] 	= $form_values->cond;
    $ar_upd['DVSWIE'] 	= $form_values->swie;
    $ar_upd['DVNOTE'] 	= acs_toDb($form_values->note);
    for($i = 1; $i <= 12 ; $i ++){
        $var = "variabile_{$i}";
        $val = "variante_{$i}";
        $tp =  "tipologia_{$i}";
        $os =  "obb_scelta_{$i}";
        if(strlen($form_values->$var) > 0){
             
            $row = get_TA_sys('PUVR', $form_values->$var, null, null, null, null, 1);
            $numerica = $row['tarest'];
            $ar_upd["DVVRC{$i}"]   = $form_values->$var;
            $ar_upd["DVSWN{$i}"]   = $numerica;
        }    
        if(strlen($form_values->$val) > 0)
            $ar_upd["DVVAL{$i}"]   = $form_values->$val;
        if(strlen($form_values->$val) > 0)
            $ar_upd["DVOPV{$i}"]   = $form_values->$os;
        if(strlen($form_values->$val) > 0)
            $ar_upd["DVTPV{$i}"]   = $form_values->$tp;
    }
    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_condizioni_distinta']}(" . create_name_field_by_ar($ar_upd) . ") VALUES (" . create_parameters_point_by_ar($ar_upd) . ")";
    
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    $ret = array();
    $ret['success'] = $result;
    echo acs_je($ret);
    
    exit;
}









// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid_cond'){
// ******************************************************************************************
    //chiave
    $db_rrn     = $m_params->open_request->record->rrn;
    $db_art     = $m_params->open_request->record->dbart_p;
    $seq1       = $m_params->open_request->record->seq1;
    $seq2       = $m_params->open_request->record->seq2;
    $cod_compo  = $m_params->open_request->record->cod_compo;
    $rip        = $m_params->open_request->record->rip;
    
    $ar = array();
    
    //record DB ******************
    $sql = "SELECT RRN(DB) AS RRN, DB.* FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB WHERE RRN(DB) = ?";    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($db_rrn));
    $row_db = db2_fetch_assoc($stmt);
    $nr = array(
      'type_cond'   => 'DB',
      'rrn'         => $row_db['RRN'],
      'cond'        => '*DB',
      'swie'        => $row_db['DBSWIE'],
      'note'        => "Condizione base"
    );
    for($i = 1; $i <= 4 ; $i++){        
        $nr["variabile_{$i}"]   = trim($row_db["DBVRS{$i}"]);
        $nr["tipologia_{$i}"]   = trim($row_db["DBOPS{$i}"]);
        $nr["variante_{$i}"]    = trim($row_db["DBVNS{$i}"]);
        $nr["obb_scelta_{$i}"]  = trim($row_db["DBOSS{$i}"]);
    }
    $ar[] = $nr;
    
    
    //record DV ******************
    $sql = "SELECT RRN(DV) AS RRN, DV.* FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB
                INNER JOIN {$cfg_mod_DeskUtility['file_condizioni_distinta']} DV
                    ON DB.DBDT = DV.DVDT AND DB.DBART = DV.DVART AND DB.DBSEQ1 = DV.DVSEQ1 AND DB.DBSEQ2 = DV.DVSEQ2 AND DB.DBCOMP = DV.DVCOMP AND DB.DBSEQR = DV.DVSEQR 
                WHERE RRN(DB) = ?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($db_rrn));
    while($row = db2_fetch_assoc($stmt)){
      $nr = array();
      $nr = array(
          'type_cond'   => 'DV',          
          'rrn'         => $row['RRN'],
          'cond'        => $row['DVSEQC'],
          'swie'        => $row['DVSWIE'],
          'note'        => acs_u8e(trim($row['DVNOTE']))
      );
      for($i = 1; $i <= 12 ; $i++){
          
          $nr["variabile_{$i}"]     = trim($row["DVVRC{$i}"]);
          $nr["tipologia_{$i}"]     = trim($row["DVTPV{$i}"]);
          $nr["variante_{$i}"]      = trim($row["DVVAL{$i}"]);
          $nr["obb_scelta_{$i}"]    = trim($row["DVOPV{$i}"]);
      }      
      $ar[] = $nr;
    } //whild DV
    
    echo acs_je($ar);
    exit;
}





// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_cond'){
// ******************************************************************************************
?>
{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				
					{
						xtype: 'grid',
						title: '',
						flex:0.3,
				        loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_cond', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['type_cond', 'cond', 'swie', 'note', 'valore', 't_val', 'MDVARI', 'rrn', 'decod',
		        			
		        			<?php for($i = 1; $i <= 12; $i++){?>
		        			'variabile_<?php echo $i?>',
		        			'tipologia_<?php echo $i?>',
		        			'variante_<?php echo $i?>',
		        			'obb_scelta_<?php echo $i?>',
		        			<?php }?>
		        			
		        			]							
									
			}, //store
			
			      columns: [	
			       
	                {
	                header   : 'Cond.',
	                dataIndex: 'cond',
	                filter: {type: 'string'}, filterable: true,
	                 width : 40
	                },
	                {
	                header   : 'I/E',
	                tooltip : 'Includi/Escludi',
	                dataIndex: 'swie',
	                filter: {type: 'string'}, filterable: true,
	                 width : 30
	                }, {
	                header   : 'Note',
	                dataIndex: 'note',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                }
	              
	               
	         ], listeners: {
	         
	           afterrender: function (comp) {
	                   
						//comp.up('window').setTitle('<?php echo "Configuratore articolo {$m_params->c_art} {$mode}";?>');	 				
		 			} ,
	    
	         
	          
				selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('panel').down('#dx_form');
		               //pulisco eventuali filtri
		               //////////////form_dx.getForm().reset(); //PROBLEMA DI LENTEZZA
		               //ricarico i dati della form
	                    acs_form_setValues(form_dx, selected[0].data);
	                    var rec = selected[0].data
	                    
	                     if(rec.type_cond == 'DB'){
	                       form_dx.getForm().findField('note').hide();
	                       this.up('panel').down('#p_cond').hide();
	                     }else{
	                       form_dx.getForm().findField('note').show();
	                       this.up('panel').down('#p_cond').show();
	                     
	                     }	
	                     
	                   }
		          }
				  
				 }
			, viewConfig: {
		        getRowClass: function(record, index) {
		        
		           if (record.get('tipo') == 'S')
		           		return ' colora_riga_giallo';	         		
		        		           		
		           return '';																
		         }   
		    }	
		
		
		}

		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettagli condizioni di elaborazione',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.7,
 		            frame: true,
 		            items: [
     		           {name: 'rrn', xtype: 'hiddenfield'},
     		           {name: 'type_cond', xtype: 'hiddenfield'},
				   
				   
				   {
					name: 'MDVARI',
					xtype: 'textfield',
					hidden : true								
				   },
				   
				   	{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							{
        					name: 'cond',
        					fieldLabel : 'Condizioni',
        					xtype: 'textfield',
        					anchor: '-15',
        					labelWidth : 60,
        					maxLength : 3,
        					forceSelection: true,	
        					allowBlank: false,			
        					width : 150,
        				  },{
                			name: 'swie',
                			xtype: 'combo',
                			margin : '0 0 0 5',
                			fieldLabel: 'Includi/escludi',
                			labelWidth : 80,
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: false,								
                		    anchor: '-15',
                		    width : 200,
                			store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 }
					]},
						 {
        					name: 'note',
        					fieldLabel : 'Note',
        					xtype: 'textfield',
        					anchor: '-5',
        					maxLength : 60,
        					labelWidth : 60,
        					flex : 1
        				  },
				  
				  <?php for($i = 1; $i <= 4; $i ++){
				  
				   $proc = new ApiProc();
				   echo $proc->get_json_response(
				      //extjs_combo_dom_ris(array('risposta_a_capo' => false, 'label' =>"{$i})Variabile", 'file_TA' => 'sys', 'dom_cf'  => "MDVAR{$i}",  'ris_cf' => "MDVAL{$i}"))
				      
				      extjs_combo_regola_config(array('risposta_a_capo' => false,
				          'label' => "Test {$i}",
				          'l_width' => 60,
				          'f_var' => "variabile_{$i}",
    				      'f_tp'  => "tipologia_{$i}",
    				      'f_van' => "variante_{$i}",
    				      'f_os'  => "obb_scelta_{$i}"
				      ))
				      
				  );
				  echo ",";
				  
				  }?> 
				  
				  {
        			xtype: 'container',
        			flex : 1,
        			itemId: 'p_cond',
        	 		layout: {
        				type: 'vbox', border: false, pack: 'start', align: 'stretch',				
        			}
        			, items: [
        			  
        				  <?php for($i = 5; $i <= 12; $i ++){
        				  
        				   $proc = new ApiProc();
        				   echo $proc->get_json_response(
        				      //extjs_combo_dom_ris(array('risposta_a_capo' => false, 'label' =>"{$i})Variabile", 'file_TA' => 'sys', 'dom_cf'  => "MDVAR{$i}",  'ris_cf' => "MDVAL{$i}"))
        				      
        				      extjs_combo_regola_config(array('risposta_a_capo' => false,
        				          'label' => "Test {$i}",
        				          'l_width' => 60,
        				          'f_var' => "variabile_{$i}",
            				      'f_tp'  => "tipologia_{$i}",
            				      'f_van' => "variante_{$i}",
            				      'f_os'  => "obb_scelta_{$i}"
        				      ))
        				      
        				  );
        				  echo ",";
        				  
        				  }?> 
        			]}
				  
			 		           ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			          Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
         			          Ext.Ajax.request({
        				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				form_values : form_values
        						},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					        if (jsonData.success == true){
        					        	var gridrecord = grid.getSelectionModel().getSelection();					        	
        					        	form.getForm().reset();
        					        	grid.store.load();
        					        }
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				    }); 		     
		           
				    	}
        	   		  });
			
			            }

			     }, {
                     xtype: 'button',
                    text: 'Refresh',
		            scale: 'small',	               
		            iconCls : 'icon-button_black_repeat_dx-16',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			  	  form = this.up('form'); 
 			          grid.getStore().load();
 			          form.getForm().reset();
 			        }
			     }, '->',
			         {
                     xtype: 'button',
                    text: 'HELP',
		            scale: 'small',	                     
					iconCls: 'icon-help_blue-16',
		          	handler: function() {
		           		acs_show_win_std('Modalit&agrave; elaborazione condizioni di distinta base', 'acs_condizioni_db.php?fn=open_mod', {}, 600, 400, null, 'icon-help_blue-16');   	     
	       			   }
 				   },
                  {
                     xtype: 'button',
                    text: 'Condizioni accantonate',
		            scale: 'small',	                     
					iconCls: 'icon-copy-16',
		          	handler: function() {
		           		acs_show_win_std('Condizioni accantonate disponibili alla copia', 'acs_gestione_valori_copiati.php?fn=open_tab', {}, 600, 400, null, 'icon-copy-16');   	     
	       			   }
 				   },
                 {xtype : 'tbfill'},
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
                    itemId : 'b_genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
		          	  if (!form.getForm().isValid()) return false;
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_config',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        			    cond : 'Y',
	        				record : <?php echo acs_je($m_params->record); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        grid.store.load();
					        form.getForm().reset();
					    },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		           			           	
		               var form = this.up('form');		               
		           	   if (!form.getForm().isValid()) return false;		               
		               
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_config',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				cond : 'Y'
			        				
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     }
			     ]
		   }]
			 	 }
							   
		
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}
