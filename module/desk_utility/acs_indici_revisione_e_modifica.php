<?php

require_once("../../config.inc.php");
$main_module = new DeskArt();
$m_params = acs_m_params_json_decode();


    function _add_to_array_by_type($open_request, $ar){
      if ($open_request->type == 'articolo'){
          $ar['RLTPNO']     = 'RA';
          $ar['RLRIFE1']    = $open_request->c_art;
      }
      return $ar;
    }

    function _get_progressivo($ar){
        global $conn, $cfg_mod_DeskUtility;
        $sql = "SELECT MAX(RLRIFE2) AS MAX_RIFE2 FROM {$cfg_mod_DeskUtility['file_note_anag']}
                WHERE RLDT = ? AND RLTPNO = ? AND RLRIFE1 = ?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($ar['RLDT'], $ar['RLTPNO'], $ar['RLRIFE1']));
        echo db2_stmt_errormsg($stmt);
        $row = db2_fetch_assoc($stmt);
        $max = $row['MAX_RIFE2'];
        if (trim($max) == '')
            $nuova_seq = 1;
        else {
            $nuova_seq = (int)$max + 1;
        }           
        return sprintf("%04s", $nuova_seq);
    }


    
// ******************************************************************************************
// DATI PER GRID (PROGRESSIVI PER ARTICOLO/COMPONENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    $ar = array();
    $ar_k['RLDT'] = $id_ditta_default;
    $ar_k = _add_to_array_by_type($m_params->open_parameters, $ar_k);
    
    $sql = "SELECT RRN(RL) AS RRN, RL.*, TA_TIPO.TADESC AS RLCINT_D FROM {$cfg_mod_DeskUtility['file_note_anag']} RL               
                LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_TIPO
				    ON TA_TIPO.TADT = RL.RLDT AND TA_TIPO.TATAID = 'TIREV' AND TA_TIPO.TAKEY1 = RL.RLCINT
                WHERE RLDT = ? AND RLTPNO = ? AND RLRIFE1 = ? ORDER BY RLRIFE2, RLRIGA
                ";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_k);
    echo db2_stmt_errormsg($stmt);
    
    $ar = array();
    $ultimo_rlrife2 = "";
    while($row = db2_fetch_assoc($stmt)){

        if (!isset($ar[$row['RLRIFE2']])){
            $ar[$row['RLRIFE2']] = $row; 
            $ar[$row['RLRIFE2']]['oggetto_descrizione'] .= "<b>".$row['RLSWST'] . $row['RLREST1'] . $row['RLFIL1'] . "</b><br/>";
            
        } else 
            $ar[$row['RLRIFE2']]['oggetto_descrizione'] .= $row['RLSWST'] . $row['RLREST1'] . $row['RLFIL1'] . "<br/>";            
        
            $ultimo_rlrife2 = $row['RLRIFE2'];
    } //while
    
    $ar[$ultimo_rlrife2]['tasto_dx'] = true;
  
    
    $ret = array();
    foreach($ar as $kar => $v) $ret[] = array_values_recursive($v);    
    echo acs_je($ret);
    exit;
}

    
    
  


// ******************************************************************************************
// CANCELLAZIONE ULTIMO INDICE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_canc_ab'){
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_note_anag']} RL 
            WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$m_params->row->RLRIFE1}'
            AND RLRIFE2 = '{$m_params->row->RLRIFE2}'
            AND RLTPNO = '{$m_params->row->RLTPNO}'";
    
          
     $stmt = db2_prepare($conn, $sql);
     $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}
    
// ******************************************************************************************
// INSERIMENTO NUOVO INDICE/MODIFICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_create_new'){
  
    if($m_params->open_request->mod == 'Y'){
        
        $sql = "DELETE
                FROM {$cfg_mod_DeskUtility['file_note_anag']}
                WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$m_params->open_request->c_art}'
                AND RLRIFE2 = '{$m_params->open_request->record->RLRIFE2}'
                AND RLTPNO = '{$m_params->open_request->record->RLTPNO}'";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
     
        
    }
        
        $ar_ins = $row;
        $ar_ins['RLDTUM'] 	= oggi_AS_date();
        $ar_ins['RLUSUM'] 	= substr($auth->get_user(), 0, 8);
        if($m_params->open_request->mod == 'Y')
            $ar_ins['RLUSGE'] 	= $m_params->open_request->record->RLUSGE;
        else
            $ar_ins['RLUSGE'] 	= substr($auth->get_user(), 0, 8);
        $ar_ins['RLDTGE'] 	= $m_params->formValues->data;
        $ar_ins['RLDT']     = $id_ditta_default;
        $ar_ins['RLCINT']   = $m_params->formValues->tipologia;
       
        $ar_ins = _add_to_array_by_type($m_params->open_request, $ar_ins);
        if($m_params->open_request->mod == 'Y')
            $ar_ins['RLRIFE2'] = $m_params->open_request->record->RLRIFE2;
        else    
            $ar_ins['RLRIFE2']  = _get_progressivo($ar_ins);


        for ($i = 0; $i <= 10; $i++){
            $ar_exe = $ar_ins;
            $campo = "des{$i}";
            $t = $m_params->formValues->$campo;
            if(trim($t) != ''){
                $ar_exe['RLRIGA'] = $i;
                $ar_exe['RLSWST'] 	= substr($t, 0, 1);  //1
                $ar_exe['RLREST1'] 	= substr($t, 1, 15);  //15
                $ar_exe['RLFIL1'] 	= substr($t, 16, 64);  //64
                
                $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_note_anag']}(" . create_name_field_by_ar($ar_exe) . ") VALUES (" . create_parameters_point_by_ar($ar_exe) . ")";
                $stmt_ins = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_ins, $ar_exe);
                echo db2_stmt_errormsg($stmt_ins);
            }
        } 
        
  
      
    
    $ret = array('success' => true);
    echo acs_je($ret);    
    exit;
}


// ******************************************************************************************
// INSERIMENTO NUOVO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'win_new'){
    
    if($m_params->mod == 'Y')
        $title = "Modifica indice di revisione";
    else 
        $title = "Nuovo indice di revisione";
    
    $tipologia = trim($m_params->record->RLCINT);
    $indice = trim($m_params->record->RLRIFE2);
    $data = trim($m_params->record->RLDTGE);
    $utente = trim($m_params->record->RLUSGE);
    
    //$oggetto_descrizione = $m_params->record->oggetto_descrizione;
    //$ar_od = explode('<br/>', $oggetto_descrizione);
    
    $sql = "SELECT * FROM {$cfg_mod_DeskUtility['file_note_anag']} RL
            WHERE RLDT = '{$id_ditta_default}' 
            AND RLTPNO = '{$m_params->record->RLTPNO}' 
            AND RLRIFE1 = '{$m_params->c_art}'
            AND RLRIFE2 = '{$m_params->record->RLRIFE2}'
            ORDER BY RLRIFE2, RLRIGA
            ";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $ar_od = array();
    while($row = db2_fetch_assoc($stmt)){
        $ar_od[$row['RLRIGA']] = trim($row['RLSWST']) . trim($row['RLREST1']) . trim($row['RLFIL1']);
    }
    
        
    $c = new Extjs_Form( layout_ar('vbox'), $title);
    $c->add_items(array(
        array('xtype' => 'fieldcontainer',
            'layout' => layout_ar('hbox'),
            'items' => array(
                array('xtype' => 'textfield',  'flex' => 1,  'name'=>'indice'  , 'fieldLabel' => 'Indice', 'disabled' => true, 'value' => $indice),
                array('xtype' => 'textfield',  'flex' => 1,  'name'=>'utente'  , 'fieldLabel' => 'Utente', 'disabled' => true, 'value' => $utente, 'labelAlign' => 'right')
            )),
        
        array('xtype' => 'fieldcontainer',
            'layout' => layout_ar('hbox'),
            'items' => array(
                extjs_datefield('data', 'Data', $data, array('flex' => 1)),
                extjs_combo(array(
                    'fieldLabel' => 'Tipologia',
                    'name' => 'tipologia',
                    'initialData'  => $main_module->find_TA_std('TIREV'),
                    'initialValue' => $tipologia,
                    'store_fields'  => array('id', 'text'),
                    'allowBlank'    => false,
                    'flex' => 1,
                    'labelAlign' => 'right',
                    'add_button_gest_tab' => array(
                        'taid' => 'TIREV',
                        'file' => 'acs_gest_TIREV.php'
                     )
                )),    
              
                
            )),
        
            array('xtype' => 'textfield',    'name'=>'des0'  ,    'fieldLabel' => 'Oggetto',        'maxLength' => 80, 'value' =>$ar_od[0]),
            array('xtype' => 'textfield',    'name'=>'des1'  ,    'fieldLabel' => 'Descrizione',    'maxLength' => 80, 'value' =>$ar_od[1]),
            array('xtype' => 'textfield',    'name'=>'des2'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[2]),
            array('xtype' => 'textfield',    'name'=>'des3'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[3]),
            array('xtype' => 'textfield',    'name'=>'des4'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[4]),
            array('xtype' => 'textfield',    'name'=>'des5'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[5]),
            array('xtype' => 'textfield',    'name'=>'des6'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[6]),
            array('xtype' => 'textfield',    'name'=>'des7'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[7]),
            array('xtype' => 'textfield',    'name'=>'des8'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[8]),
            array('xtype' => 'textfield',    'name'=>'des9'  ,    'fieldLabel' => '&nbsp;',         'maxLength' => 80,'value' =>$ar_od[9]),
            array('xtype' => 'textfield',    'name'=>'des10'  ,   'fieldLabel' => '&nbsp;',         'maxLength' => 80, 'value' =>$ar_od[10]),
        ));
    
    $c->set(array(
        'buttons' => array(
            array('text' => 'Salva', 'handler' => extjs_code("
                function(comp){                   
                    var form = this.up('form'),
                        formValues = form.getValues(),
                        m_win = this.up('window');
                    if (form.getForm().isValid()){
                        std_ajax_request('" . $_SERVER['PHP_SELF'] . "?fn=exe_create_new',
                                {formValues: formValues, open_request: " . json_encode($m_params) . "},
                                function(){
                                    m_win.fireEvent('afterSave', m_win);
                                });
                    }
                }
            ")))));
    
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => $c->code()
    ));
    exit;
}


// ******************************************************************************************
// APERTURA GRID CON ELENCO INDICI/MODIFICHE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_win'){
    
    $c = new Extjs_Grid(null, null);
    $c->set_features(array('filters' => array()));
    $c->set(array(
        'columns' => array(
            grid_column_h('Indice',         'RLRIFE2', 'w70',  'Indice', array(), array('filter_as' => 'string')),
            grid_column_h('Data',           'RLDTGE',  'w70',  'Data generazione', array('renderer' => extjs_code('date_from_AS')), array('filter_as' => 'string')),
            grid_column_h('Utente',         'RLUSGE',  'w120', 'Utente generazione', null, array('filter_as' => 'string')),
            grid_column_h('Tipo',           'RLCINT_D','w120', 'Tipo nota', null, array('filter_as' => 'string')),
            grid_column_h('Oggetto/Descrizione',    'oggetto_descrizione',  'f1', 'Oggetto / Descrizione', null, array('filter_as' => 'string'))
        ),
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('RLRIFE1', 'tasto_dx', 'RLTPNO', 'RLCINT', 'RLRIGA', 'RRN', 'RLRIFE2', 'RLDTGE', 'RLUSGE', 'RLCINT_D', 'oggetto_descrizione'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_json_data'),array('open_parameters' => $m_params))),
        'listeners'   => array(
            'itemcontextmenu' => extjs_code(grid_itemcontextmenu("	
                event.stopEvent();
                var voci_menu = [];
                voci_menu.push({
	         			text: 'Cancella',
	        			iconCls : 'icon-sub_red_delete-16',             		
	        			handler: function () {

                                if(rec.get('tasto_dx') == false){
                                    acs_show_msg_error('E\' possibile eliminare solo l\'ultimo indice');
                                    return;
                                }

                                Ext.Ajax.request({
								        url        : '". $_SERVER['PHP_SELF']."?fn=exe_canc_ab',
								        method     : 'POST',
					        			jsonData: {
					        				row : rec.data
										},							        
								        success : function(result, request){
								        
								   			view.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
	        		  	
		                }
	    		});

                 var menu = new Ext.menu.Menu({
			            items: voci_menu
        					}).showAt(event.xy);	
                  
           ")),
            'celldblclick' => extjs_code(grid_celldblclick("
                acs_show_win_std('Modifica indice di revisione'
                                   , '" . $_SERVER['PHP_SELF'] . "?fn=win_new'
                                   , { mod : 'Y'
                                     , c_art : ". j($m_params->c_art)."
                                     , record : rec.data
                                     , type : ". j($m_params->type)."
                                     }
                                   , 600, 500, {
                        afterSave: function(fromWin){ 
                            iView.getStore().load();
                            fromWin.close();
                        }
                   }); 
        
           "))
        ),
        'buttons' => array(
            array('text' => 'Crea nuovo', 'handler' => extjs_code("
                function(comp){
                   var m_grid = comp.up('grid');
                   acs_show_win_std('Inserimento indice di revisione'
                                   , '" . $_SERVER['PHP_SELF'] . "?fn=win_new'
                                   , " . json_encode($m_params) . ", 600, 500, {
                        afterSave: function(fromWin){                            
                            m_grid.store.load();
                            fromWin.destroy();
                        }
                   });                    
                }
            "))
        )
    ));
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => $c->code()
    ));
    exit;
}
