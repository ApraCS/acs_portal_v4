<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();

$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$cod_art = $form_values->form_values->f_cod_art;
$desc_art = $form_values->form_values->f_desc_art;
$cod_forn = $form_values->form_values->f_fornitore;
$magaz = $form_values->form_values->f_magazzino;

if (strlen($cod_art) > 0)
	$sql_where.= " AND UPPER(SKCAR0) LIKE '%" . strtoupper($cod_art) . "%' ";
if (strlen($desc_art) > 0)
	$sql_where.= " AND UPPER(SKDES0) LIKE '%" . strtoupper($desc_art) . "%' ";

if (strlen($cod_forn) > 0)
	$sql_where.= " AND UPPER(SKCFO0) = '" . strtoupper($cod_forn) . "' ";

if (strlen($magaz) > 0)
    $sql_where.= " AND SKCMA0 = '{$magaz}' ";

			$sql = "SELECT RRN(KF) AS RRN, SKDIV0, SKAZI0, SKCMA0, SKCFO0, SKRAG0, SKCAR0,
			SKDES0, SKBTO0, SKBTP0, SKCOM0, SKCOT0, SKDEV0, SKFLG1, SKFLA1, SKCVP0, SKCMC0
			FROM {$cfg_mod_DeskUtility['file_articoli']} KF
			WHERE SKDIT0 = '$id_ditta_default' AND SKDIV0 = '{$form_values->form_values->f_divisione}' {$sql_where}
			AND SKTPR0 = '' ORDER BY SKRAG0, SKCAR0";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			while($row = db2_fetch_assoc($stmt)){
				$nr = array();
				$nr['SKDIV0'] = trim($row['SKDIV0']);
				$nr['SKAZI0'] = trim($row['SKAZI0']);
				$nr['SKCMA0'] = trim($row['SKCMA0']);
				$nr['SKCMA0'] = trim($row['SKCMA0']);
				$nr['SKCFO0'] = trim($row['SKCFO0']);
				$nr['SKRAG0'] = trim($row['SKRAG0']);
				$nr['SKCAR0'] = trim($row['SKCAR0']);
				$nr['SKDES0'] = trim($row['SKDES0']);
				$nr['SKBTO0'] = trim($row['SKBTO0']);
				$nr['SKBTP0'] = trim($row['SKBTP0']);
				$nr['SKCOM0'] = trim($row['SKCOM0']);
				$nr['SKFLA1'] = trim($row['SKFLA1']);
				$nr['SKCOT0'] = trim($row['SKCOT0']);
				$nr['SKDEV0'] = trim($row['SKDEV0']);
				$nr['SKCVP0'] = trim($row['SKCVP0']);
				$nr['SKFLG1'] = trim($row['SKFLG1']);
				$nr['SKCMC0'] = trim($row['SKCMC0']);
				$nr['RRN'] 	  = $row['RRN'];
				if ($nr['SKFLG1'] == 'S')
					$nr['SKFLG1_v'] = true;
				else
					$nr['SKFLG1_v'] = false;

				//$nr = array_map('utf8_encode', $nr); //php7
						$ar[] = $nr;


			}
			
			
		


echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>Lista articoli</H2>
 		</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";

echo "<tr class='liv_data' >
  		<th>Fornitore</th>
  		<th>Articolo</th>
		<th>Descrizione</th>
		<th>Magazzino</th>
		<th>T.G.A.</th>
		<th>T.G.</th>
		<th>Consumo totale</th>
		<th>Consumo medio</th>
		</tr>";

foreach ($ar as $kar => $r){
	
	echo "<tr>";
    echo "  <td>".$r['SKRAG0']." [".$r['SKCFO0']."]</td>
   			<td>".$r['SKCAR0']."</td>
 			<td>".$r['SKDES0']."</td>
			<td>".$r['SKCMA0']."</td>
			<td>".$r['SKFLA1']."</td>
			<td>".$r['SKFLG1']."</td>
			<td class=number>".n($r['SKCOT0'],3)."</td>
   		  	<td class=number>".n($r['SKCOM0'],3)."</td>
    		      </tr>";
	

	
	
}


?>
</div>
</body>
</html>	

