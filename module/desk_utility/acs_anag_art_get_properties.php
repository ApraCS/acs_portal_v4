<?php

require_once("../../config.inc.php");

$main_module = new DeskUtility();

$articolo = $_REQUEST["m_art"];


$sql = "SELECT *
        FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
        WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$articolo}' ";

//print_r($sql);


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

while ($row = db2_fetch_assoc($stmt)) {
    
    $nr = array();
    $nr['Riferimento'] = $row['ARARTO'];
    $nr['Alternativo'] = $row['ARGRAL'];
    
    if(trim($row['ARRIF']) != '')
        $nr['Rif. tecnico'] = $row['ARRIF'];
    if(trim($row['ARDBFB']) != '')
        $nr['DBase'] = $row['ARDBFB'];
    if(trim($row['ARDBCO']) != '')
        $nr['DBase comm'] = $row['ARDBCO'];
    if(trim($row['ARDBCS']) != '')
        $nr['DBase alt. 1'] = $row['ARDBCS'];
    if(trim($row['ARDBPL']) != '')
        $nr['DBase alt. 2'] = $row['ARDBPL'];
    
    $nr['Appr. prin.'] = $row['ARTPAP'];
    $nr['Appr. MTS/O'] = $row['ARSWTR'];
    $nr['Gr. pianif.'] = $row['ARTAB1'];
    $nr['GG approvv.'] = $row['ARLTFO'];
    $nr['L. time prod'] = $row['ARLTPR'];
    $nr['L. time log.'] = '';
    $nr['L'] = n_auto($row['ARDIM1'], 4);
    $nr['H'] = n_auto($row['ARDIM2'], 4);
    $nr['S' ] = n_auto($row['ARDIM3'], 4); 
    $nr['Peso netto'] = n_auto($row['ARPNET'],4);
    $nr['Peso lordo'] = n_auto($row['ARPLOR'],4);
    
    $nr['Volume'] = n_auto($row['ARVOLU'],4);
    $nr['Colli'] = n($row['ARCOLL']);
    $nr['Gruppo imballo'] = $row['ARRCOL'];
    $nr['Collo su ordine'] = $row['ARFLG1'];
    $nr['Scorta'] = $row['ARSMIN'];
    $nr['Riord. max'] = $row['ARSMAX'];
    $nr['Riord./Pick'] = $row['ARRIOR'];
    $nr['Lotto appr.'] = $row['ARLOTT'];
    
    
    $ret['title'] = "Art. ".$row['ARART'] . " - " . $row['ARDART'];
    $ret['riferimenti'] = $nr;
    
}

echo acs_je($ret);
exit;