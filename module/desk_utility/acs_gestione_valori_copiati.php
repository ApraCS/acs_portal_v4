<?php

require_once "../../config.inc.php";
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_delete_all'){
    
    unset($_SESSION['regola_config']);
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    
    exit;
}

if ($_REQUEST['fn'] == 'exe_elimina'){
    $selected_rows = $m_params->list_selected_id;
    $ar_ret = array();
   // foreach($_SESSION['regola_config'] as $k => $v){
    for($i = 0; $i<count($_SESSION['regola_config']); $i++){
        $regola = $_SESSION['regola_config'][$i]->out_d;
        if (!in_array($regola, $selected_rows)) 
            $ar_ret[]  = $_SESSION['regola_config'][$i];
            //unset($_SESSION['regola_config'][$i]);
    }
    $_SESSION['regola_config'] = $ar_ret;
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
  
    exit;
}

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $ar = array();
    
    if(!is_null($_SESSION['regola_config']))
        $ar_value = $_SESSION['regola_config'];
    else
        $ar_value = array();
    
    
      foreach($ar_value as $v){
        $nr = array();  
        $nr['regola'] = $v->out_d;
 		$ar[] = $nr;
		 		
      }

      echo acs_je($ar);
      exit;
}



if ($_REQUEST['fn'] == 'open_tab'){
    

    
    
    ?>
    {"success":true, "items": [
    
 {
				xtype: 'grid',
				title: '',
		        loadMask: true,	
		        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
		         store: {
			        autoLoad: true,
					proxy: {
			            type: 'ajax',
			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
			            actionMethods: {
				          read: 'POST'
			        },
	                	extraParams: {
	    		    		
	    				},				            
			            doRequest: personalizza_extraParams_to_jsonData, 
			            reader: {
			            type: 'json',
						method: 'POST',						            
				            root: 'root'						            
				        }
			        },       
					fields: ['regola'],		             	
	            },
		   
		

	      columns: [
	
            {
            header   : 'Seleziona',
            dataIndex: 'regola',
            flex: 1
            }
     ], listeners: { }, 
		 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [ 
                  /*{
                    xtype: 'button',
                    text: 'Cancella tutto',
		            iconCls: 'icon-sub_red_delete-32',
		            scale: 'large',	                     

		           handler: function() {
		              
	            	  var loc_win = this.up('window');
	                  Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete_all',
						        method     : 'POST',
			        			jsonData: {},							        
						        success : function(result, request){
						           var jsonData = Ext.decode(result.responseText);
						           loc_win.close();	        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			
			            }

			     },*/
                
                '->',
                 {
                    xtype: 'button',
                    text: 'Elimina',
		            iconCls: 'icon-sub_red_delete-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                      var grid = this.up('grid');
	            	  var loc_win = this.up('window');
	                  var records =  grid.getSelectionModel().getSelection();  
	           		  list_selected_id = []; 
                            
                      for (var i=0; i<records.length; i++) {
		              	list_selected_id.push(records[i].get('regola'));
		              }
		              
					   Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_elimina',
						        method     : 'POST',
			        			jsonData: {
			        				list_selected_id : list_selected_id
								},							        
						        success : function(result, request){
						           var jsonData = Ext.decode(result.responseText);
						          grid.getStore().load();        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			
			            }

			     }
			     ]
		   }]
		 
		  ,viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			           
			           return '';																
			         }   
			    }
			       


}
    
    ]}

<?php 
exit;
}