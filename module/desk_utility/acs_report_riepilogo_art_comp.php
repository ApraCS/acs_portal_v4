<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

function sum_columns_value(&$ar_r, $r){       
    $ar_r['prezzo'] += $r['C_PREZZO'] * $r['REQTA'];
}

function detrai_value(&$liv, $ultimo_ins){
    $liv['prezzo'] = $liv['prezzo'] - $ultimo_ins['prezzo'];
    
}

if ($_REQUEST['fn'] == 'open_form'){
   
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	         
						 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
							xtype: 'textfield',
							name : 'cod_comp',
							fieldLabel : 'Cod. componente',
							labelWidth : 110
													 
						 },
						  {
							xtype: 'textfield',
							name : 't_comp',
							hidden : true
													 
						 },
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-search-16',
			             iconAlign: 'top',			                
			             handler : function() {
			             
			             var form = this.up('form').getForm();
    			             my_listeners = {
    			                 afterSelect: function(from_win, articolo){
    				        		   form.findField('cod_comp').setValue(articolo);
    				        		   form.findField('t_comp').setValue('Y');
    		        				   from_win.close();
    	        					}
    			             
    			             };
			             
									<?php 
									   $ord = $s->get_ordine_gest_by_k_docu($m_params->k_ordine);
									   $title = "{$ord['TDAADO']}_{$ord['TDNRDO']}_{$ord['TDTPDO']}";
									?>		             
									acs_show_win_std('Articoli distinta componenti ' + <?php echo j($title); ?>, '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_grid', {k_ordine : '<?php echo $m_params->k_ordine; ?>'}, 600, 500, my_listeners, 'icon-search-16');
								} 
						 
						 } 
					]},  {
							name: 'f_s_op',
							margin : '5 0 0 0',
							xtype: 'radiogroup',
							fieldLabel: 'Solo ordini di produzione',
							labelWidth: 150,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_s_op' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'
		                          , width: 30
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_s_op' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          , width: 40   
								  , checked: true                       
		                        }]
						  },
						   {
							name: 'f_report',
							margin : '5 0 0 0',
							xtype: 'radiogroup',
							fieldLabel: 'Report valorizzato',
							labelWidth: 150,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_report' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'
		                          , width: 30
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_report' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          , width: 40   
								  , checked: true                       
		                        }]
						  },
						  {
						    name: 'f_versione',
							margin : '5 0 0 0',
							xtype: 'textfield',
							fieldLabel: 'Versione costo',
							labelWidth: 150,
							maxLength : 3,
							width : 200
						  },
						 {
							name: 'f_ultimo',
							margin : '5 0 0 0',
							xtype: 'radiogroup',
							fieldLabel: 'Solo ultimo livello',
							labelWidth: 150,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_ultimo' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'
		                          , width: 30
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_ultimo' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          , width: 40   
								  , checked: true                       
		                        }]
						  },
						 
					  {
						xtype: 'checkboxgroup',
						fieldLabel: 'Raggruppa per',
						margin : '5 0 0 0',
						labelWidth : 100,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_raggr' 
                          , boxLabel: 'Categoria analitica'
                          , inputValue: 'C'
                        },
                        {
                            xtype: 'checkbox'
                          , name: 'f_raggr' 
                          , boxLabel: 'Fornitore'
                          , inputValue: 'F'
                        }]							
					},
					
					  {
							name: 'f_d_art',
							margin : '5 0 0 0',
							xtype: 'radiogroup',
							fieldLabel: 'Dettagli articolo',
							labelWidth: 150,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_d_art' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'
		                          , width: 30
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_d_art' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          , width: 40   
								                        
		                        }]
						  },
					
			 	
	            ],
	            
				buttons: [	
						{
		        		  text: 'Report',
			              scale: 'large',
			              iconCls: 'icon-print-32',
			              handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	k_ordine : '<?php echo $m_params->k_ordine; ?>'
		                        	
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
		        		}
		       ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}

if ($_REQUEST['fn'] == 'get_json_data_elenco'){
    
    $ar = array();
    
    $k_ordine = $m_params->k_ordine;
    $oe = $s->k_ordine_td_decode_xx($k_ordine);
    $ar_stmt = array('FB', $oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']);
    
    $sql = "SELECT SUM(REQTA) AS REQTA, REART, REDART, REUM
            FROM {$cfg_mod_Spedizioni['file_righe_componenti']} RE
            WHERE RETPSV= ? AND REDT = ? AND RETIDO = ? AND REINUM = ? AND REAADO = ? AND RENRDO = ? 
            GROUP BY REART, REDART, REUM
            ORDER BY REART, REDART";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_stmt);
    while ($row = db2_fetch_assoc($stmt)) {
        $ar[] = $row;
  
    }
    
    echo acs_je($ar);
    exit;
    
}

if ($_REQUEST['fn'] == 'open_grid'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
					{
						xtype: 'grid',
				        loadMask: true,	
				        stateful: true,
        				stateId: 'elenco_componenti',
        				stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow'],
        				features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],	
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										 k_ordine: '<?php echo $m_params->k_ordine?>',
																	
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: [{name : 'REQTA', type : 'int'}, 'REUM', 'REART', 'REDART']							
									
			}, //store
			
			 columns: [	
				
			      {
	                header   : 'Codice',
	                dataIndex: 'REART',
	                width : 100,
	                filter: {type: 'string'}, filterable: true,
	                 },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'REDART',
	                flex : 1,
	                filter: {type: 'string'}, filterable: true
	                 },
	                   {
	                header   : 'UM',
	                dataIndex: 'REUM',
	                width : 30,
	                filter: {type: 'string'}, filterable: true
	                 },
	                 {
	                header   : 'Q.t&agrave; totale',
	                dataIndex: 'REQTA',
	                width : 100,
	                align: 'right',
		            renderer : floatRenderer2,
		             filter: {type: 'string'}, filterable: true
	                 }
    	
	         ]
	    
	         
	         , listeners: {
	         
	         	      
	            celldblclick: {
	            
	            		//su elenco voci (grid dx) - Sto selezionando la risposta
	            
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  
							  var loc_win = this.up('window');
							  loc_win.fireEvent('afterSelect', loc_win, rec.get('REART'));
							
							 
						  }
			   		  }
	         
				   
				 }, viewConfig: {
		       		 getRowClass: function(record, index) {	
		       		 
		       		                 		
                															
		        		 }   
		    }
				 
		
		
		}
		
	]}
	
	
	<?php 
	
exit;	
    
    
}

if ($_REQUEST['fn'] == 'open_report'){

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #DDDDDD;}   
   tr.liv1 td{background-color: #DDDDDD; font-weight: bold;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   tr.liv_compo td{font-weight: normal; background-color: #eeeeee;}
   table.int1 td.no_border {border: 0px solid white;}
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 


$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$ar = array();


$sql_where = "";
if($form_values->t_comp == 'Y')
    $sql_where .= " AND RE.REART = '{$form_values->cod_comp}'";
else
    $sql_where .= " AND RE.REART LIKE '%{$form_values->cod_comp}%'";

if($form_values->f_s_op == 'Y')
    $sql_where .= " AND RE.RETIDE = 'L' ";


$k_ordine = $_REQUEST['k_ordine'];
$oe = $s->k_ordine_td_decode_xx($k_ordine);
$ar_stmt = array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']);

if(isset($cfg_mod_DeskArt['tipo_riordino']))
    $riordino = ", {$cfg_mod_DeskArt['tipo_riordino']} AS REMTO";
else
    $riordino = ", RE.REMTO ";

if(isset($form_values->f_versione) && strlen($form_values->f_versione) > 0)
        $where_rc = " AND RC.RCVERS = '{$form_values->f_versione}' ";

$sql = "SELECT RD.*, RE.*, RE2.REAACO AS P_ANNO, RE2.RENRCO AS P_NUM, RE2.RETPCO AS P_TPO,
        RT_D.RTPRZ AS D_PREZZO, RC.RCPRZ AS C_PREZZO, CF.CFRGS1 AS D_FOR, ARANAL, ARTPAR, ARSWTR {$riordino}
        FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
        LEFT OUTER JOIN {$cfg_mod_DeskArt['file_righe_doc_gest_valuta']} RT_D
           ON RT_D.RTDT=RD.RDDT AND RD.RDTIDO = RT_D.RTTIDO AND RD.RDINUM = RT_D.RTINUM AND RD.RDAADO = RT_D.RTAADO AND RD.RDNRDO = RT_D.RTNRDO AND RD.RDNREC=RT_D.RTNREC AND RT_D.RTVALU='EUR'
        INNER JOIN {$cfg_mod_DeskArt['file_ricom']} RE
          ON RE.REDT=RD.RDDT AND RE.RETIDO=RD.RDTIDO AND RE.REINUM=RD.RDINUM AND RE.REAADO=RD.RDAADO AND RE.RENRDO=RD.RDNRDO AND RE.RENREC=RD.RDNREC
        LEFT OUTER JOIN {$cfg_mod_DeskArt['file_righe_commesse']} RC
           ON RC.RCDT=RE.REDT AND RC.RCTPCO = RE.RETPCO AND RC.RCAACO = RE.REAACO AND RC.RCNRCO = RE.RENRCO AND RC.RCTPSV = RE.RETPSV AND RC.RCRIGA = RE.RERIGA AND RC.RCVALU='EUR' $where_rc
        LEFT OUTER JOIN {$cfg_mod_DeskArt['file_ricom']} RE2
          ON RE2.REDT = RE.REDT AND RE2.RETIDO = 'CP' AND RE2.REINUM = 'FB' AND RE2.REAADO = RE.REAADO AND RE2.RENRDO = RE.RENRDO AND RE2.RENREC = RE.RERIGA 
        LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_cli']} CF
          ON CF.CFDT = RE.REDT AND CF.CFTICF = 'F' AND digits(CF.CFCD) = RE.RECDRF 
        LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
          ON AR.ARDT = RE.REDT AND AR.ARART = RE.REART 
        WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ? {$sql_where}
        ORDER BY RERIGA, REART";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt, $ar_stmt);

        
while($row = db2_fetch_assoc($stmt)){

    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    $cod_liv0 = $row['RDRIGA'];
 
    if($form_values->f_raggr == 'C')
        $cod_liv0_1 = $row['ARANAL'];
    elseif($form_values->f_raggr == 'F')
        $cod_liv0_1 = $row['RECDRF']; 
    else
        $cod_liv0_1 = 'ALL';
       
        $cod_liv1 = $row['RERIGA'];
         
    $liv=$cod_liv0;
    if (!isset($ar_r["{$liv}"])){
        $ar_new= array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['riga']= $liv;
        $ar_new['articolo']= trim($row['RDART']);
        $ar_new['descrizione']= trim($row['RDDART']);
        $ar_new['u_m']= trim($row['RDUM']);
        $ar_new['qta']= trim($row['RDQTA']);

        $ar_new['dim1'] = $row['RDDIM1'];
        $ar_new['dim2'] = $row['RDDIM2'];
        $ar_new['dim3'] = $row['RDDIM3'];
        if(trim($row['RECDRF']) != 0)
            $ar_new['fornitore'] = "[".trim($row['RECDRF']). "] ".trim($row['D_FOR']);
        if($row['RELIVE'] == 0){
            
            $ar_new['varianti'] = "";
            $dom1 = find_TA_sys('PUVR', trim($row['REVAR1']));
            $risp1 = find_TA_sys('PUVN', trim($row['REVAN1']), null, trim($row['REVAR1']));
            if(trim($row['REVAR1']) != '')
                $ar_new['varianti'] .= "[".$row['REVAR1']."-".$row['REVAN1']."] ".$risp1[0]['text'];
                
            $dom2 = find_TA_sys('PUVR', trim($row['REVAR2']));
            $risp2 = find_TA_sys('PUVN', trim($row['REVAN2']), trim($row['REVAR2']));
            if(trim($row['REVAR2']) != '')
                $ar_new['varianti'] .= "<br>[".$row['REVAR2']."-".$row['REVAN2']."] ".$risp2[0]['text'];
                    
            $dom3 = find_TA_sys('PUVR', trim($row['REVAR3']));
            $risp3 = find_TA_sys('PUVN', trim($row['REVAN3']), trim($row['REVAR3']));
            if(trim($row['REVAR3']) != '')
                $ar_new['varianti'] .= "<br>[".$row['REVAR3']."-".$row['REVAN3']."] ".$risp3[0]['text'];
            
            $ar_new['tipo_parte'] = trim($row['ARTPAR']);
            if(trim($row['REMTO']) != '')
                $ar_new['tipo_rio'] = "[".trim($row['REMTO'])."]";
            else{
                if(trim($row['ARSWTR']) != '')
                    $ar_new['tipo_rio'] = trim($row['ARSWTR']);
            }
           
                
        }
        $ar_new['livello']= 0;
        $ar_r[$liv]=$ar_new;
    }
   
    $t_ar_liv0 = &$ar_r[$liv];
    $ar_r=&$ar_r[$liv];
    sum_columns_value($ar_r, $row);
    
    $liv=$cod_liv0_1;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['id'] = implode("|", $tmp_ar_id); 
        if($form_values->f_raggr == 'C'){
            $ar_new['codice']= $row['ARANAL'];
            $row_ca = get_TA_sys('MUAN', trim($row['ARANAL']));
            $ar_new['task']= $row_ca['text'];
        }
        if($form_values->f_raggr == 'F'){
            if(trim($row['RECDRF']) != 0){
                $ar_new['codice']= $row['RECDRF'];
                $ar_new['task']= trim($row['D_FOR']);
            }
        }
        $ar_r[$liv]=$ar_new;
    }
    
    $t_ar_liv1 = &$ar_r[$liv];
    $ar_r=&$ar_r[$liv];
    sum_columns_value($ar_r, $row);
 
    $liv=$cod_liv1;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['articolo']= trim($row['REART']);
        $ar_new['riga']= trim($row['RERIGA']);
        $ar_new['descrizione']= trim($row['REDART']);
        $ar_new['u_m']= trim($row['REUM']);
        $ar_new['qta']= trim($row['REQTA']);
        $ar_new['categoria']= trim($row['ARANAL']);
        if(trim($row['RECDRF']) != 0)
           $ar_new['fornitore']= "[".trim($row['RECDRF']). "] ".trim($row['D_FOR']);
        $ar_new['livello']= trim($row['RELIVE']);
       
        if($row['C_PREZZO'] > 0)
            $ar_new['prezzo']= $row['C_PREZZO'] * $row['REQTA'];
        $ar_new['varianti'] = "";
        $dom1 = find_TA_sys('PUVR', trim($row['REVAR1']));
        $risp1 = find_TA_sys('PUVN', trim($row['REVAN1']), null, trim($row['REVAR1']));
        if(trim($row['REVAR1']) != '')
            $ar_new['varianti'] .= "[".$row['REVAR1']."-".$row['REVAN1']."] ".$risp1[0]['text'];
    
        $dom2 = find_TA_sys('PUVR', trim($row['REVAR2']));
        $risp2 = find_TA_sys('PUVN', trim($row['REVAN2']), trim($row['REVAR2']));
        if(trim($row['REVAR2']) != '')
            $ar_new['varianti'] .= "<br>[".$row['REVAR2']."-".$row['REVAN2']."] ".$risp2[0]['text'];
        
        $dom3 = find_TA_sys('PUVR', trim($row['REVAR3']));
        $risp3 = find_TA_sys('PUVN', trim($row['REVAN3']), trim($row['REVAR3']));
        if(trim($row['REVAR3']) != '')
            $ar_new['varianti'] .= "<br>[".$row['REVAR3']."-".$row['REVAN3']."] ".$risp3[0]['text'];
        
        $ar_new['dim1'] = $row['REDIM1'];
        $ar_new['dim2'] = $row['REDIM2'];
        $ar_new['dim3'] = $row['REDIM3'];
        $ar_new['tipo_parte'] = trim($row['ARTPAR']);
        if(trim($row['REMTO']) != '')
            $ar_new['tipo_rio'] = "[".trim($row['REMTO'])."]";
        else{
            if(trim($row['ARSWTR']) != '')
                $ar_new['tipo_rio'] = trim($row['ARSWTR']);
        }
        
        if(trim($row['RETIDE']) == 'L')
            $ar_new['or_prod']= $row['P_ANNO']."_".$row['P_NUM']."_".$row['P_TPO'];
       
        $ar_new['ultimo_livello'] = 'Y';
        $ar_new['da_stampare'] = 'Y';
        $ar_r[$liv]=$ar_new;
        
        
 
        //verifico se record precedente e' ulitmo liv
        if (isset($ultimo_inserimento) && ($ar_new['livello'] > $ultimo_inserimento['livello'])){
            $ultimo_inserimento['ultimo_livello'] = 'N';
            if($form_values->f_ultimo == 'Y'){
                $ultimo_inserimento['da_stampare'] = 'N';
                //chiamare sempre la sum e poi detrarre qui
                
                detrai_value($ultimo_inserimento_liv0, $ultimo_inserimento);
                detrai_value($ultimo_inserimento_liv1, $ultimo_inserimento);
            }
        }
        
        $ultimo_liv = $liv;
        $ultimo_inserimento = &$ar_r[$liv];
        $ultimo_inserimento_liv0 = &$t_ar_liv0;
        $ultimo_inserimento_liv1 = &$t_ar_liv1;        
    }
    
    
    
    
    
}

   




echo "<div id='my_content'>";
echo "<div class=header_page>";

$ord = $s->get_ordine_gest_by_k_docu($k_ordine);

if($form_values->f_report == 'Y')
    echo "<H2>Riepilogo distinta articoli/componenti valorizzata [{$ord['TDAADO']}_{$ord['TDNRDO']}_{$ord['TDTPDO']}]</H2>";
else
    echo "<H2>Riepilogo distinta articoli/componenti [{$ord['TDAADO']}_{$ord['TDNRDO']}_{$ord['TDTPDO']}]</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";


    echo "<tr class='liv_data'>
          <th>Livello</th>
          <th>Riga</th>
          <th>Articolo</th>
          <th>Tp</th>
          <th>Tr</th>
          <th>Descrizione</th>
          <th>Varianti</th>
          <th>Dim.1</th>
          <th>Dim.2</th>
          <th>Dim.3</th>
          <th>UM</th>   
          <th>Q.t&agrave;</th>";
if($form_values->f_report != 'Y'){
    echo "<th>Ordine di produzione</th>";
}else{
    echo "<th>Costo</th>";
    echo "<th>Fornitore</th>";
}
    echo  "</tr>";
    
$ultimo_liv = $form_values->f_ultimo;

foreach($ar as $k => $v){
    
    //echo "<td>{$v1['tipo_parte']}</td>";
    if($ultimo_liv != 'Y' || $form_values->f_raggr != ''){
            echo "
            <tr class=liv1>
            <td>{$v['livello']}</td>
            <td>{$v['riga']}</td>
            <td>{$v['articolo']}</td>";
            
            echo "<td>{$v['tipo_parte']}</td>";
            echo "<td>{$v['tipo_rio']}</td>";
            echo "<td>{$v['descrizione']}</td>";
            echo "<td>{$v['varianti']}</td>";
            echo "<td class=number>".n($v['dim1'], 2)."</td>";
            echo "<td class=number>".n($v['dim2'], 2)."</td>";
            echo "<td class=number>".n($v['dim3'], 2)."</td>";
            
            echo"<td>{$v['u_m']}</td>
            <td class=number>".n($v['qta'], 2)."</td>";
            if($form_values->f_report == 'Y'){
                echo "<td class=number>".n($v['prezzo'], 2)."</td>";
                echo "<td>{$v['fornitore']}</td>";
            }else
                echo "<td>&nbsp;</td>";
                echo "</tr>";

    }
    
    
    
   // if(is_array($v['children']))
        foreach($v['children'] as $k01 => $v01){
        
        $liv_da_stampare = false;
        foreach($v01['children']  as $kt01 => $kv01){
            if($kv01['da_stampare'] == 'Y')
                $liv_da_stampare = true;
        }
        
        if($form_values->f_raggr != '' && $liv_da_stampare == true){
            echo "
            <tr class=liv2>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>{$v01['codice']}</td>";
            
            echo "<td>&nbsp;</td>";
            echo "<td>&nbsp;</td>";
            echo "<td>{$v01['task']}</td>";
            echo "<td>&nbsp;</td>";
            echo "<td class=number>&nbsp;</td>";
            echo "<td class=number>&nbsp;</td>";
            echo "<td class=number>&nbsp;</td>";
            
            echo"<td>&nbsp;</td>
            <td class=number>&nbsp;</td>";
            if($form_values->f_report == 'Y'){
                echo "<td class=number>".n($v01['prezzo'], 2)."</td>";
                echo "<td>&nbsp;</td>";
            }else
                echo "<td>&nbsp;</td>";
                echo "</tr>";
        }
   
      
        foreach($v01['children'] as $k1 => $v1){
           
           
          if(($ultimo_liv != 'Y' &&  $form_values->f_d_art == 'Y') || 
              ($v1['ultimo_livello'] == 'Y' && $ultimo_liv == 'Y')){
            
            echo "
                <tr>
                <td>{$v1['livello']}</td>
                <td>{$v1['riga']}</td>
                <td>{$v1['articolo']}</td>";
            
    
          echo "<td>{$v1['tipo_parte']}</td>";
          echo "<td>{$v1['tipo_rio']}</td>";
          echo "<td>{$v1['descrizione']}</td>";      
               echo "<td>{$v1['varianti']}</td>";
               echo "<td class=number>".n($v1['dim1'], 2)."</td>";
               echo "<td class=number>".n($v1['dim2'], 2)."</td>";
               echo "<td class=number>".n($v1['dim3'], 2)."</td>";
                       
           echo"<td>{$v1['u_m']}</td>
                <td class=number>".n($v1['qta'], 2)."</td>";
            if($form_values->f_report == 'Y'){
                echo "<td class=number>".n($v1['prezzo'], 2)."</td>";
                echo "<td>{$v1['fornitore']}</td>";
            }else
                echo "<td>&nbsp;</td>";
                echo "</tr>";
                
               
            }
            
           
            
        } 
        
    }
   
}
        


?>
</div>
</body>
</html>	


<?php 
exit;
}




