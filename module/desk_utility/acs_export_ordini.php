<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_export'){
    
    $ritime     = microtime(true);
    
    $ar_ins = array();
    $ar_ins['PITIME'] = $ritime;
    $ar_ins['PIDT']   = $id_ditta_default;
    
    $ar_ins['PIDTGE'] = oggi_AS_date();
    $ar_ins['PIORGE'] = oggi_AS_time();
    $ar_ins['PIUSGE'] = $auth->get_user();
    $ar_ins['PIDEF1'] = $m_params->form_values->f_default;
    /*$ar_ins['PIDEF2'] = substr($stringa, 250, 250);
     $ar_ins['PIDEF3'] = substr($stringa, 500, 250);
     $ar_ins['PIDEF4'] = substr($stringa, 750, 250);
     $ar_ins['PIDEF5'] = substr($stringa, 1000, 250);*/
    
    
    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_parametri']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    if($m_params->type == 'E'){
        foreach($m_params->list_selected_id as $v){
            
            $sh = new SpedHistory();
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'EXP_WEXDC',
                    "use_session_history" => $ritime,
                    'k_ordine' => $v,
                 )
                );
       
        }
        $sh = new SpedHistory();
        $ret_RI = $sh->crea(
            'pers',
            array(
                "RITIME"    => $ritime,
                "messaggio"	=> 'EXP_WEXDC',
                "end_session_history" => $ritime,
               
            )
            );
     }else{
         
         $sh = new SpedHistory();
         $ret_RI = $sh->crea(
             'pers',
             array(
                 "RITIME"    => $ritime,
                 "messaggio"	=> 'VAL_SVIL_FABB',
                 'k_ordine' => $m_params->list_selected_id[0],
                 // "vals" => $ar_vals
             )
             );
     }
     
    
    $ret['success'] = true;
    //$ret['ret_RI'] = $ret_RI;
    echo acs_je($ret);
    exit;    
 
}


//----------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------
    if($m_params->type == 'E'){
        $title = 'Export distinta componenti valorizzata';
        $icona = 'icon-outbox-16';
    }else{
        $title = 'Valorizzazione distinta componenti';
        $icona = 'icon-folder_search-16';
        
    }
    ?>

   {"success":true, 
   
   m_win: {
		title: <?php echo j($title); ?>,
		width: 400, height: 200,
		iconCls: <?php echo j($icona); ?>,
	}, 
   "items": [
		{
			xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            flex : 1,
		    autoScroll : true,
            title: '',
			items: [
			
			 {
				name: 'f_default',
				xtype: 'combo',
				fieldLabel: 'Default',
				forceSelection: true,								
				displayField: 'text',
				valueField: 'id',								
				emptyText: '- seleziona -',
		   		allowBlank: false,	
		   		queryMode: 'local',
     		    minChars: 1, 	
				labelWidth : 80,
				anchor: '-15',
				width : 150	,				
				store: {
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
				     <?php echo acs_ar_to_select_json(find_TA_sys('*PGM', null, $m_params->tacor1, null, null, null, 0, '', 'Y'), ''); ?>	
				    ]
				}
				,listeners: { 
			 		beforequery: function (record) {
    	         		record.query = new RegExp(record.query, 'i');
    	         		record.forceAll = true;
    				 }
 				 }
				
		 }
						
			
			 
			
			]
			
			, buttons: [ 
				{
                    text: 'Conferma',
                    scale: 'large',
                    iconCls: 'icon-button_blue_play-32',
                    handler: function () {
                   
                        var m_win = this.up('window');
                        var form = this.up('form').getForm();
    	            	var form_values = form.getValues();
    	            	
    	            	if (form.isValid()){
    	            	
    	            	   Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_export',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>,
			        				type : <?php echo j($m_params->type)?>
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        acs_show_msg_info('Fine operazione');
							        m_win.destroy();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
    	            	
    	            	
    	            	
    	            	}
    	            	
   
                    }
            	}]
                        	
            }
   
   ]}
    
<?php }