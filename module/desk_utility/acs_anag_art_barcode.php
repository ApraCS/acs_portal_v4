<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_sos_att'){
    
    $ar_upd = array();
    
    $sosp = trim($m_params->row->BCSOSP);
    
    if($sosp == "")
        $new_value = "S";
    else
        $new_value = "";
            
            
    $ar_upd['BCUSUM'] = $auth->get_user();
    $ar_upd['BCSOSP']   = $new_value;
    
    $sql = "UPDATE {$cfg_mod_DeskArt['file_ubicazioni']} BC
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(BC) = '{$m_params->row->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}


if ($_REQUEST['fn'] == 'exe_inserisci_bc'){
    $ret = array();
    
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
    
    
    $ar_ins['BCDTGE'] = oggi_AS_date();
    $ar_ins['BCUSGE'] = $auth->get_user();
    //$ar_ins['TADTUM'] = oggi_AS_date();
    $ar_ins['BCUSUM']  = $auth->get_user();
    $ar_ins["BCDT"]    = $id_ditta_default;
    $ar_ins["BCART"]   = $m_params->open_request->c_art;
    $ar_ins["BCTABB"]   = 'BC';
    if(strlen($form_values->BCCCON) > 0)
        $ar_ins["BCCCON"]  = $form_values->BCCCON;
    $ar_ins['BCCODE']  = $form_values->BCCODE;
   
    
    $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_DeskArt['file_ubicazioni']} BC
            WHERE BCDT = '{$id_ditta_default}' AND BCCODE = '{$form_values->BCCODE}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if($row['C_ROW'] > 0){
        $ret['success'] = false;
        $ret['msg_error'] = "Codice esistente";
        echo acs_je($ret);
        return;
    }
    
     $sql = "INSERT INTO {$cfg_mod_DeskArt['file_ubicazioni']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_bc'){
    $ret = array();
    
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
   
    $ar_ins['BCUSUM']  = $auth->get_user();
    $ar_ins["BCCCON"]  = $form_values->BCCCON;
    $ar_ins['BCCODE']  = $form_values->BCCODE;
    
    
    $sql = "UPDATE {$cfg_mod_DeskArt['file_ubicazioni']} BC
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(BC) = '{$form_values->rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc_bc'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->form_values->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_ubicazioni']} BC WHERE RRN(BC) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

if ($_REQUEST['fn'] == 'get_json_data_grid_barcode'){
    
    $ar = array();
    
    $c_art = $m_params->open_request->c_art;
    
    $sql = " SELECT RRN(BC) AS RRN, BC.*, CF.CFRGS1 AS D_FOR
            FROM {$cfg_mod_DeskArt['file_ubicazioni']} BC
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_cli']} CF
               ON CF.CFDT = BC.BCDT AND CF.CFCD = BC.BCCCON
            WHERE BCDT = '{$id_ditta_default}' AND BCTABB = 'BC' AND BCART = '{$c_art}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
 
    
    
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = $row;
        $nr['rrn'] = $row['RRN'];
        $nr['BCCODE'] = trim($row['BCCODE']);
        $nr['c_forn'] = "[".trim($row['BCCCON'])."] ".trim($row['D_FOR']);
        $nr['fornitore'] = "[".trim($row['BCCCON'])."]<br>".trim($row['D_FOR']);
        $ar[] = $nr;
        
    }
    
    
    echo acs_je($ar);
    exit;
}