<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'MURC',
    'title_grid' => 'MURC-Regole DC',
    'fields' => array(
        'TAID'        => array('label'	=> 'tabella', 'hidden' => 'true'),
        'TATP'        => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'        => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 3),
        'TADESC'      => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'      => array('label'	=> 'Note', 'maxLength' => 30),
        'RRN'         => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'        => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'var1'        => array('label'	=> 'Variabile', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'is1'         => array('label'	=> 'Sostituisci da (01-15)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'maxLength' => 2, 'listener' => 'Y'),
        'chr1'        => array('label'	=> 'Nr caratteri', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'labelWidth' => 170, 'width' => 195, 'maxLength' => 1, 'close' => 'Y'),
        'frz'         => array('label'	=> 'Forza sostituzione:', 'only_view' => 'F', 'xtype' => 'articolo'),
        'var2'        => array('label'	=> 'Variabile', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'is2'         => array('label'	=> 'Sostituisci da (01-15)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'maxLength' => 2, 'listener' => 'Y'),
        'chr2'        => array('label'	=> 'Nr caratteri', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 195, 'labelWidth' => 170, 'maxLength' => 1,  'close' => 'Y'),
        'var3'        => array('label'	=> 'Variabile', 'only_view' => 'F', 'xtype' => 'combo_tipo','tab_sys' => 'PUVR'),
        'is3'         => array('label'	=> 'Sostituisci da (01-15)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'maxLength' => 2, 'listener' => 'Y'),
        'chr3'        => array('label'	=> 'Nr caratteri', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 195, 'labelWidth' => 170, 'maxLength' => 1, 'close' => 'Y'),
        'var4'        => array('label'	=> 'Variabile', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'is4'         => array('label'	=> 'Sostituisci da (01-15)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'maxLength' => 2, 'listener' => 'Y'),
        'chr4'        => array('label'	=> 'Nr caratteri', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 195, 'labelWidth' => 170, 'maxLength' => 1, 'close' => 'Y'),
        'TAREST'      => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli regole di codifica distinta componenti',
    'tab_tipo' => 'PUVR',
    'title_tab' => 'Selezione articolo',
    'TAREST' => array(
        'var1' 	=> array(
            "start" => 0,
            "len"   => 3,
            "riempi_con" => ""
        ),'is1' 	=> array(
            "start" => 3,
            "len"   => 2,
            "riempi_con" => ""
        ),'chr1' 	=> array(
            "start" => 5,
            "len"   => 1,
            "riempi_con" => ""
        ),'frz' 	=> array(
            "start" => 6,
            "len"   => 15,
            "riempi_con" => ""
        ),'var2' 	=> array(
            "start" => 21,
            "len"   => 3,
            "riempi_con" => ""
        ),'is2' 	=> array(
            "start" => 24,
            "len"   => 2,
            "riempi_con" => ""
        ),'chr2' 	=> array(
            "start" => 26,
            "len"   => 1,
            "riempi_con" => ""
        ),'var3' 	=> array(
            "start" => 27,
            "len"   => 3,
            "riempi_con" => ""
        ),'is3' 	=> array(
            "start" => 30,
            "len"   => 2,
            "riempi_con" => ""
        ),'chr3' 	=> array(
            "start" => 32,
            "len"   => 1,
            "riempi_con" => ""
        ),'var4' 	=> array(
            "start" => 33,
            "len"   => 3,
            "riempi_con" => ""
        ),'is4' 	=> array(
            "start" => 36,
            "len"   => 2,
            "riempi_con" => ""
        ),'chr4' 	=> array(
            "start" => 38,
            "len"   => 1,
            "riempi_con" => ""
        ),'sel_art' 	=> array(
            "start" => 39,
            "len"   => 1,
            "riempi_con" => ""
        ),'art_in_ric' 	=> array(
            "start" => 40,
            "len"   => 15,
            "riempi_con" => ""
        ),'chr_ric' 	=> array(
            "start" => 55,
            "len"   => 15,
            "riempi_con" => ""
        ),'art_fn_ric' 	=> array(
            "start" => 70,
            "len"   => 15,
            "riempi_con" => ""
        ),'dim1' 	=> array(
            "start" => 85,
            "len"   => 1,
            "riempi_con" => ""
        ),'dim2' 	=> array(
            "start" => 86,
            "len"   => 1,
            "riempi_con" => ""
        ),'dim3' 	=> array(
            "start" => 87,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'vstd' 	=> array(
            "start" => 88,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gen_art' 	=> array(
            "start" => 91,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'contract' 	=> array(
            "start" => 92,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'ciclo' 	=> array(
            "start" => 93,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'todo' 	=> array(
            "start" => 94,
            "len"   => 10,
            "riempi_con" => ""
        ),
        'c_stringa' 	=> array(
            "start" => 104,
            "len"   => 5,
            "riempi_con" => ""
        ),
        'vst1' 	=> array(
            "start" => 109,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vst2' 	=> array(
            "start" => 112,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vst3' 	=> array(
            "start" => 115,
            "len"   => 3,
            "riempi_con" => ""
        )
        
        
        
    )
  
);

$m_table_config_altro = array(
    'TAID' => 'MURC',
    'fields' => array(
        'sel_art'     => array('label'	=> 'Selez.autom.articolo', 'only_view' => 'F',  'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
        'art_in_ric'  => array('label'	=> 'Selez.cod.articolo DA', 'only_view' => 'F'),
        'art_fn_ric'  => array('label'	=> 'Selez.cod.articolo A', 'only_view' => 'F'),
        'chr_ric'     => array('label'	=> 'Selez.posiz.articolo:', 'only_view' => 'F', 'xtype' => 'articolo'),
        'dim1'        => array('label'	=> 'Selez.dimensioni &nbsp;&nbsp; L', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 195, 'labelWidth' => 120, 'maxLength' => 1, 'type' =>'combo', 'tab_std' => 'TTDIM'),
        'dim2'        => array('label'	=> 'H', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 100, 'labelWidth' => 20, 'maxLength' => 1, 'type' =>'combo', 'tab_std' => 'TTDIM'),
        'dim3'        => array('label'	=> 'S', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 100, 'labelWidth' => 20, 'maxLength' => 1, 'type' =>'combo', 'tab_std' => 'TTDIM', 'close' => 'Y'),
        'vstd'        => array('label'	=> 'Valore standard var1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'gen_art'     => array('label'	=> 'Genera articolo', 'only_view' => 'F',  'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
        'contract'    => array('label'	=> 'Progetto/contract', 'only_view' => 'F',  'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
        'ciclo'       => array('label'	=> ' Ciclo di vita', 'only_view' => 'F',  'xtype' => 'combo_tipo', 'tab_std' => 'AR012'),
        'todo'        => array('label'	=> 'To Do', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ATTAV'),
    ),
    
);

$m_table_config_altro_cv = array(
    'TAID' => 'MURC',
    'title' => 'Stringa da varianti',
    'fields' => array(
        'c_stringa'     => array('label'	=> 'Codice stringa', 'only_view' => 'F',  'maxLength' => 5, 'width' => 160),
        'vst1'          => array('label'	=> 'Variante 1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'vst2'          => array('label'	=> 'Variante 2', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'vst3'          => array('label'	=> 'Variante 3', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
    ),
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';
