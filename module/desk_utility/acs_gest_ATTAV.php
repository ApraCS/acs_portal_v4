<?php
require_once("../../config.inc.php");

$main_module = new DeskArt(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'module'      => $main_module,
    'tab_name'    => $cfg_mod_DeskArt['file_tabelle'],
    'descrizione' => "To Do Entry",
    't_panel' =>  "ATTAV - ToDo Config",
    'form_title' => "Dettagli tabella attivit�",
    'maximize'    => 'Y',
    'conferma_elimina'    => 'Y',
    'fields_preset' => array(
        'TATAID' => 'ATTAV',
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
   
    'fields_key' => array('TAKEY1', 'TADESC'),
    
    'fields_grid' => array('TAKEY1', 'TADESC', 'TARIF1', 'TAB_ABB', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TARIF1', 'TAFG01', 'TAPESO', 'TANAZI'),

    
    
    'fields' => array(
        
        'TAPESO' => array('label'	=> 'Codice', 'type' => 'numeric'),
        'TAKEY1' => array('label'	=> 'Seq.', 'fw'=>'width: 170'),
        'TADESC' => array('label'	=> 'Descrizione'),
        'TARIF1' => array('label'	=> 'Riferimento'),
        'TAFG01' => array('label'	=> 'Utente', 'tooltip' => 'TAFG01'),
        'TANAZI' => array('label'	=> 'Codice lingua', 'tooltip' => 'TANAZI'),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        //TABELLA ABBINATA
        'TAB_ABB' =>  array('label' => 'rilav', 'type' => 'TA',
            'iconCls' => 'search',
            'tooltip' => 'Causali rilascio',
            'select' => "TA_RL.C_ROW AS TAB_ABB",
            'join' => "LEFT OUTER JOIN (
            SELECT COUNT(*) AS C_ROW, TA2.TAKEY1
            FROM {$cfg_mod['file_tabelle']} TA2
            WHERE TA2.TADT = '{$id_ditta_default}' AND TA2.TATAID = 'RILAV'
            GROUP BY TA2.TAKEY1) TA_RL
            ON TA.TAKEY1 = TA_RL.TAKEY1",
            'ta_config' => array(
                'tab' => 'RILAV',
                'file_acs'     => '../desk_utility/acs_gest_RILAV.php?fn=open_tab',
                
            )
            ),
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
        
    )
);


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
