<?php

require_once("../../config.inc.php");
require_once("../base/_rilav_g_history_include.php");

$desk_art = new DeskArt();
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_check'){
    $ret = array();
    
    $sh = new SpedHistory($desk_art);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'CHK_CATALOG',
            "vals" => array(
                "RIFOR1" => $m_params->id_prog
            )
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;

}

if ($_REQUEST['fn'] == 'open_filtri'){ ?>
    
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            anchor: '-10',
	            flex: 1,
	            items: [
	          			<?php write_combo_std('f_catalogo', 'Catalogo', '', acs_ar_to_select_json($desk_art->find_TA_std('CATIN', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('allowBlank' => 'false') ) ?>
	                  ,{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox'},
						//flex: 1,
						anchor: '-10',
						items: [
						  <?php write_datefield_std('f_data_ini', 'Data import da', '') ?>
						, <?php write_datefield_std('f_data_fin', 'a', '', 0, array('labelAlign' => 'right', 'labelWidth' => 20)) ?>
						
						]}
	            
	            ],
	            
				buttons: [
					 {
			            text: 'Visualizza',
				        iconCls: 'icon-windows-32',		            
				        scale: 'large',		            
			              handler: function() {
	            
	            			var form = this.up('form').getForm();
	            			acs_show_win_std(null, 'acs_panel_import.php?fn=open_panel', 
        		            	{form_values: form.getValues()}, 800, 500, {}, 'icon-F8-m-16');
        		         	  this.up('window').close();			                   	                	                
	            		}
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
    
    <?php 
    
    exit;
}



if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    
    $form_values = $m_params->open_request->form_values;
    
    if (strlen($form_values->f_catalogo) > 0)
        $sql_where.= " AND ITCATA = '{$form_values->f_catalogo}' ";
    
    if (strlen($form_values->f_data_ini) > 0)
        $sql_where.= " AND ITDTGE >= {$form_values->f_data_ini} ";
    if (strlen($form_values->f_data_fin) > 0)
        $sql_where.= " AND ITDTGE <= {$form_values->f_data_fin} ";
    
        
        
       $_rilav_H_for_query = _rilav_H_for_query(
            array('f_ditta' => 'ITDITT', 'field_IDPR' => 'ITIDPR'),
            $cfg_mod_DeskArt['file_import_t'],
            $cfg_mod_DeskArt['file_tabelle'],
            'IT',
            'CVICA',
            array('CV')
            );
        
    
    $sql = " SELECT ITCATA, TA_CT.TADESC AS D_CAT, ITVERS, TA_VR.TADESC AS D_VERS,
             ITDTGE, ITORGE, ITIDPR, ITDITT, ITNOTE, {$_rilav_H_for_query['select']}
             FROM {$cfg_mod_DeskArt['file_import_t']} IT
             LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_CT
              ON TA_CT.TATAID = 'CATIN' AND IT.ITCATA = TA_CT.TAKEY1 
             LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_VR
              ON TA_VR.TATAID = 'CAVER' AND IT.ITCATA = TA_VR.TAKEY1 AND IT.ITVERS = TA_VR.TAKEY2 
             {$_rilav_H_for_query['joins']}
             WHERE 1=1 {$sql_where}
             ";
           
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
  
    
    
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = array();
      
        $nr['data']   = $row['ITDTGE'];
        $nr['ora']    = $row['ITORGE'];
        $nr['id_prog']  = trim($row['ITIDPR']);
        $nr['catalogo']  = trim($row['ITCATA']);
        $nr['d_catalogo']  = "[".trim($row['ITCATA'])."] ".trim($row['D_CAT']);
        
        $nr['versione']  = trim($row['ITVERS']);
        $nr['d_versione']  = "[".trim($row['ITVERS'])."] ".trim($row['D_VERS']);
       // $nr['cv_stato']  = trim($row['ITSTATO']);
        $nr['note']  = trim($row['ITNOTE']);
        $nr['H_CV_COD']  = trim($row['H_CV_COD']);
        $nr['H_CV_ICONA']  = trim($row['H_CV_ICONA']);
        $nr['H_CV_STYLE_CLASS']  = trim($row['H_CV_STYLE_CLASS']);
        
        $ar[] = $nr;
        
    }

    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'get_json_data_dettaglio'){
    
    $ar = array();
    
    $id_prog = $m_params->open_request->id_prog;
    
    
    if($m_params->dettaglio == 'N'){
        $select = " ITCATA, ITVERS, ID.IDRIGA, ID.IDTIMP, ID.IDCART, DD.IDVALA AS V_DESC,
                    ARFO.IDVALA AS V_ARFO, EAN.IDVALA AS V_EAN, 
                    DM1.IDVALN AS V_DIM1, DM1.IDFG01 AS F_DIM1, 
                    DM2.IDVALN AS V_DIM2, DM2.IDFG01 AS F_DIM2,
                    DM3.IDVALN AS V_DIM3, DM3.IDFG01 AS F_DIM3";
        $join = " 
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_t']} IT
                    ON ID.IDDITT = IT.ITDITT AND ID.IDIDPR = IT.ITIDPR

                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_d']} DD
                    ON ID.IDIDPR = DD.IDIDPR AND ID.IDRIGA = DD.IDRIGA AND TRIM(DD.IDCCAM) = 'DESC'
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_d']} ARFO
                    ON ID.IDIDPR = ARFO.IDIDPR AND ID.IDRIGA = ARFO.IDRIGA AND TRIM(ARFO.IDCCAM) = 'ARFO'
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_d']} DM1
                    ON ID.IDIDPR = DM1.IDIDPR AND ID.IDRIGA = DM1.IDRIGA AND TRIM(DM1.IDCCAM) = 'DIM1'
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_d']} DM2
                    ON ID.IDIDPR = DM2.IDIDPR AND ID.IDRIGA = DM2.IDRIGA AND TRIM(DM2.IDCCAM) = 'DIM2'
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_d']} DM3
                    ON ID.IDIDPR = DM3.IDIDPR AND ID.IDRIGA = DM3.IDRIGA AND TRIM(DM3.IDCCAM) = 'DIM3'
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_d']} EAN
                    ON ID.IDIDPR = EAN.IDIDPR AND ID.IDRIGA = EAN.IDRIGA AND TRIM(EAN.IDCCAM) = 'EAN'
";
        $group_by = " GROUP BY ITCATA, ITVERS, ID.IDRIGA, ID.IDTIMP, ID.IDCART, DD.IDVALA, ARFO.IDVALA, 
                      DM1.IDVALN, DM2.IDVALN, DM3.IDVALN, DM1.IDFG01, DM2.IDFG01, DM3.IDFG01, EAN.IDVALA ";
        $order_by = " ID.IDRIGA";
    }else{
        $select = "IDCCAM, IDVALA, IDVALN, IDFG01, TA_DT.TADESC AS DESC, TA_DT.TAPROV AS FORMATO, TA_DT.TATELE AS SEQ";
        $group_by = "";
        $order_by = " TA_DT.TATELE";
        $join = " LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_DT
                  ON ID.IDDITT = TA_DT.TADT AND TA_DT.TATAID = 'DTIMP' AND ID.IDCCAM = TA_DT.TAKEY1";
        if(isset($m_params->riga))
            $sql_where = " AND IDRIGA = {$m_params->riga}";
    }
    
          
        $sql = " SELECT {$select}
                 FROM {$cfg_mod_DeskArt['file_import_d']} ID
                 {$join}
                 WHERE ID.IDIDPR = {$id_prog} {$sql_where}
                 {$group_by}
                 ORDER BY {$order_by}
        ";
         
                
     
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
                
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = array();
        $nr['ITCATA']   = trim($row['ITCATA']);
        $nr['ITVERS']   = trim($row['ITVERS']);
        $nr['riga']   = $row['IDRIGA'];
        $nr['tipologia']    = $row['IDTIMP'];
        $nr['descrizione_import']    = $row['V_DESC'];
        $nr['articolo']  = trim($row['IDCART']);
        $nr['arfo']  = trim($row['V_ARFO']);
        $nr['dim1']  = $row['V_DIM1'];
        $nr['dim2']  = $row['V_DIM2'];
        $nr['dim3']  = $row['V_DIM3'];
        $nr['ean']   = $row['V_EAN'];
        $nr['f_dim1']  = trim($row['F_DIM1']);
        $nr['f_dim2']  = trim($row['F_DIM2']);
        $nr['f_dim3']  = trim($row['F_DIM3']);
        $nr['codice']  = trim($row['IDCCAM']);
        if(in_array(trim($row['IDCCAM']), array('DIM1', 'DIM2', 'DIM3', 'PLOR', 'PNET')))
            $nr['f_dimensioni']  = trim($row['IDFG01']);
        $nr['descrizione']  = trim($row['DESC']);
        if(trim($row['FORMATO']) == 'T')
            $nr['valore']  = trim($row['IDVALA']);
        if(trim($row['FORMATO']) == 'N')
            $nr['valore']  = n($row['IDVALN'], 2);
        
        $ar[] = $nr;
        
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_panel'){

    $title = "Elenco cataloghi import dati ";
    $catalogo = trim($m_params->form_values->f_catalogo);
    $cat = $desk_art->find_TA_std('CATIN', $catalogo);
    $title .= " [{$catalogo}] {$cat[0]['text']}";
    
    ?>

{"success":true, 
	m_win: {
		title: <?php echo j($title); ?>,
	}, 

"items": [
    
	{
			xtype: 'grid',
			title: '',
	        loadMask: true,
        	autoScroll : true,
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],		
        	store: {
			//xtype: 'store',
			autoLoad:true,

		proxy: {
		   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
		   method: 'POST',								
		   type: 'ajax',

	       actionMethods: {
	          read: 'POST'
	        },
	        
	        
	           extraParams: {
				 open_request: <?php echo acs_je($m_params) ?>
			},
			
			doRequest: personalizza_extraParams_to_jsonData, 

		   reader: {
            type: 'json',
			method: 'POST',						            
            root: 'root'						            
        }
	},
	
	fields:  ['id_prog', 'data', 'ora', 'catalogo', 'd_catalogo', 'versione', 'd_versione', 'cv_stato',
				<?php echo _rilav_H_write_model_fields(array('CV')) ?>]
							
									
			}, //store
			
			 <?php $lcn = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
		
			      columns: [	
			       {
	                header   : 'ID',
	                dataIndex: 'id_prog',
	                width : 30,
	                },
	                {
	                header   : 'Versione',
	                dataIndex: 'd_versione',
	                flex : 1
	                },
	                {
	                header   : 'Data',
	                dataIndex: 'data',
	                width : 150,
	                renderer: function(value, p, record){
	                      if(value > 0){
		    			  	var ora = time_from_AS(record.get('ora'));
		    			  	return date_from_AS(value) + ' - '+ ora;
		    			  }
		    		  }
	                },
	                /*{text: 'St.', dataIndex: 'cv_stato', width : 30},*/
	                {text: 'CV',					
    	          		dataIndex: 'H_CV_COD', tooltip: 'Ciclo vita', width: 35, align: 'right',
    	          		exeGestRilavH: 'CV', exeGestRilavH_causale : 'CVICA',
    	          		renderer: function (value, metaData, rec, row, col, store, gridView){
    	          		  <?php echo _rilav_H_icon_style('CV') ?>
    	          			
    	        	}},
    	        	  {
	                header   : 'Note',
	                dataIndex: 'note',
	                flex : 1
	                },
    	        	   <? echo dx_mobile() ?>
	                
	         ], listeners: {
	         
	     
	      		itemcontextmenu : function(grid, rec, node, index, event) {
   		  	 	    event.stopEvent();
	  											  
			 		var voci_menu = [];
		     		var row = rec.data;
		     		var m_grid = this;
		     		var loc_win = this.up('window');
		     		   
					 	 voci_menu.push({
			         		text: 'Dettaglio catalogo',
			        		iconCls : 'icon-F8-m-16',          		
			        		handler: function () {
			        			acs_show_panel_std('acs_panel_import.php?fn=open_dettaglio', null, {
        		            	   id_prog : rec.get('id_prog')});   
        		            	   loc_win.destroy();
				                }
			    				});
			    				
    				 	 voci_menu.push({
			         		text: 'Controllo/ricerca articolo',
			        		iconCls : 'icon-check_green-16',          		
			        		handler: function () {
			        			  Ext.Ajax.request({
                				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check',
                				        timeout: 2400000,
                				        method     : 'POST',
                	        			jsonData: {
                	        				id_prog : rec.get('id_prog')
                						},							        
                				        success : function(result, request){
                					        var jsonData = Ext.decode(result.responseText);
                					         acs_show_msg_info('Operazione completata');
                				        },
                				        failure    : function(result, request){
                				            Ext.Msg.alert('Message', 'No data to be loaded');
                				        }
                				    }); 
				                }
			    				});
			    				
			    				
		    				var menu = new Ext.menu.Menu({
    				            items: voci_menu
    					}).showAt(event.xy);	
					     		
			     },
			     
			      celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				 	  
				 	  var rec = iView.getRecord(iRowEl);
					  var grid = this;
					  var col = iView.getGridColumns()[iColIdx];
					  var col_name = iView.getGridColumns()[iColIdx].dataIndex;	
					  
					  //Gestione History RILAV
					    if (!Ext.isEmpty(col.exeGestRilavH)){
					    	var my_listeners = {
					    		afterInsert: function(from_win, jsonData){
					    			rec.set(jsonData.item);
					        	 	rec.commit();
					    			from_win.destroy();
					    		}
					    	}
 							acs_show_win_std('History avanzamento gestione catalogo import dati', '../base/acs_panel_rilav_H.php?fn=open_tab', {
			        		   		file_H: <?php echo j(_rilav_H_file())?>,
			        		   		file: <?php echo j(_rilav_H_db_file_Orig($cfg_mod_DeskArt['file_import_t'])) ?>,
			        		   		file_TA: <?php echo j($cfg_mod_DeskArt['file_tabelle'])?>,
			        		   		rec_id: rec.get('id_prog'),
            	            		causale: col.exeGestRilavH_causale,
            	            		group: col.exeGestRilavH
            	               }, null, null, my_listeners);
            	        }
					  
					  }
	          
				  
				 }
			
		
		
		}
			
     
]} 

<?php 
exit;
}

if ($_REQUEST['fn'] == 'open_dettaglio'){
 
    ?>

{"success":true, "items": [
     {
	xtype: 'panel',
	autoScroll : true,
	title: 'Import ' + <?php echo j($m_params->id_prog)?>,
    <?php echo make_tab_closable(); ?>,
	layout: {
		type: 'hbox',
		align: 'stretch',
		pack : 'start',
	},
		items: [
		
		{
			xtype: 'grid',
			title: '',
			flex : 0.6,
	        loadMask: true,
	        autoScroll : true,
	        stateful: true,
			stateId: 'import_gr',
			stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow'],
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],		
        	store: {
			//xtype: 'store',
			autoLoad:true,

		proxy: {
		   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_dettaglio', 
		   method: 'POST',								
		   type: 'ajax',

	       actionMethods: {
	          read: 'POST'
	        },
	        
	        
	           extraParams: {
				 open_request: <?php echo acs_je($m_params) ?>,
				 dettaglio : 'N'
				 
			},
			
			doRequest: personalizza_extraParams_to_jsonData, 

		   reader: {
            type: 'json',
			method: 'POST',						            
            root: 'root'						            
        }
	},
	
	fields:  ['ITCATA', 'ITVERS', {name: 'riga', type : 'int'}, 'tipologia', 'descrizione_import', 'articolo', 'arfo', {name: 'dim1', type : 'int'}, {name: 'dim2', type : 'int'},{name: 'dim3', type : 'int'}, 'ean', 'f_dim1', 'f_dim2', 'f_dim3']
							
									
			}, //store
			
			 <?php $lcn = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
		
			      columns: [	
			
	                {
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width : 40,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Tipologia',
	                dataIndex: 'tipologia',
	                width : 80,
	                filter: {type: 'string'}, filterable: true
	                },
	                 {
	                header   : 'Descrizione import',
	                dataIndex: 'descrizione_import',
	                flex : 1,
	                filter: {type: 'string'}, filterable: true,
                 	renderer: function(value, metaData, record){
            			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';    			    	
                		     return value;	
                	}
	                },
	                  {
	                header   : 'Articolo',
	                dataIndex: 'articolo',
	                width : 90,
	                filter: {type: 'string'}, filterable: true
	                },
	                    {
	                header   : 'Articolo forn.',
	                dataIndex: 'arfo',
	                width : 80,
	                filter: {type: 'string'}, filterable: true
	                },
	                  {
	                header   : 'EAN',
	                dataIndex: 'ean',
	                width : 100,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Dim.1',
	                dataIndex: 'dim1',
	                width : 40,
	                filter: {type: 'string'}, filterable: true,
	                renderer: function(value, metaData, record){
            			if (record.get('f_dim1') == 'D') 
	                        metaData.tdCls += ' grassetto sfondo_rosso ';
                		return floatRenderer0(value);	
                	}
	                },
	                {
	                header   : 'Dim.2',
	                dataIndex: 'dim2',
	                width : 40,
	                filter: {type: 'string'}, filterable: true,
	                 renderer: function(value, metaData, record){
            			if (record.get('f_dim1') == 'D') 
	                        metaData.tdCls += ' grassetto sfondo_rosso ';
                		return floatRenderer0(value);	
                	}
	                },{
	                header   : 'Dim.3',
	                dataIndex: 'dim3',
	                hidden : true,
	                width : 50,
	                filter: {type: 'string'}, filterable: true,
	                 renderer: function(value, metaData, record){
            			if (record.get('f_dim1') == 'D') 
	                        metaData.tdCls += ' grassetto sfondo_rosso ';
                		return floatRenderer0(value);	
                	}
	                },
	                 <? echo dx_mobile() ?>
	                
	         ], listeners: {
        		itemclick: function(view,rec,item,index,eventObj) {
        						  
				  var grid = this.up('panel').down('#dx_grid');
				  var titolo = 'Dettaglio dati import [' + 
				  	rec.get('ITCATA') + ', '+
				  	rec.get('ITVERS') + ', '+ 
				  	rec.get('articolo') + ']';
				  grid.setTitle(titolo);
				  grid.getStore().proxy.extraParams.riga = rec.get('riga');
				  grid.getStore().load();
							        	
        	     },
        	     
        	       itemcontextmenu : function(view,record,item,index,e,eOpts) {
                 	    e.stopEvent();
                        var voci_menu = []; 
                      
        				//recupero la cella
        				var xPos = e.getXY()[0];
        			    var cols = view.getGridColumns();
        			    var colSelected = null;
        			
        			    for(var c in cols) {
        			
        			        var leftEdge = cols[c].getPosition()[0];
        			        var rightEdge = cols[c].getSize().width + leftEdge;
        			
        			        if(xPos>=leftEdge && xPos<=rightEdge) {
        			            colSelected = cols[c];
        			        }
        			    }	
        	  		
        	  		col_name = colSelected.dataIndex;
        	  		
        	  		if(col_name == 'articolo'){
        	  		
        	  		 voci_menu.push({
			         		text: 'Copia codice articolo',
			        		iconCls : 'icon-copy-16',          		
			        		handler: function () {
			        		 
			        		   const el = document.createElement('textarea');
                    	  		el.value = record.get('articolo');
                    	  		document.body.appendChild(el);
                    	  		el.select();
                    	  		document.execCommand('copy');
                    	  		document.body.removeChild(el);
                    	  		
					        }
			    		});
			    		
		    		}
		    		
		    		
        	  	if(col_name == 'arfo'){
        	  		
        	  		 voci_menu.push({
			         		text: 'Copia codice articolo fornitore',
			        		iconCls : 'icon-copy-16',          		
			        		handler: function () {
			        		  
			        		   const el = document.createElement('textarea');
                    	  		el.value = record.get('arfo');
                    	  		document.body.appendChild(el);
                    	  		el.select();
                    	  		document.execCommand('copy');
                    	  		document.body.removeChild(el);
                    	  		
					        }
			    		});
			    		
		    		}
			    		
	    		      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(e.xy);
				  
				  
				  }
				  
				 }
		
		},
		
			  {
			xtype: 'grid',
			itemId : 'dx_grid',
			flex : 0.4,
			title : 'Dettaglio',
	        loadMask: true,
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],		
        	store: {
			//xtype: 'store',
			autoLoad:false,

		proxy: {
		   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_dettaglio', 
		   method: 'POST',								
		   type: 'ajax',

	       actionMethods: {
	          read: 'POST'
	        },
	        
	        
	           extraParams: {
				 open_request: <?php echo acs_je($m_params) ?>,
				 dettaglio : 'Y',
				 riga : ''
			},
			
			doRequest: personalizza_extraParams_to_jsonData, 

		   reader: {
            type: 'json',
			method: 'POST',						            
            root: 'root'						            
        }
	},
	
	fields:  ['codice', 'descrizione', 'valore', 'f_dimensioni']
							
									
			}, //store
		
			      columns: [	
			
	                {
	                header   : 'Codice',
	                dataIndex: 'codice',
	                width : 70,
                    renderer: function(value, metaData, record){
            			if (record.get('f_dimensioni') == 'D') 
	                        metaData.tdCls += ' grassetto sfondo_rosso ';
                		return value;	
                	}
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'descrizione',
	                flex : 1
	                },
	                    {
	                header   : 'Valore',
	                dataIndex: 'valore',
	                flex : 1.5,
	                renderer: function(value, metaData, record){
	                    if (record.get('f_dimensioni') == 'D') 
	                        metaData.tdCls += ' grassetto sfondo_rosso ';
	                
            			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';    			    	
                		     return value;	
                	}
	                }
	            
	                
	                
	         ]
		
		}
			
     ]}
]} 

<?php 
exit;
}
