<?php

//Visualizzo le righe (da gestionale) con le relative sottorighe (tree) ed eventuale fabbisogno....

require_once "../../config.inc.php";
$main_module = new Spedizioni();
$desk_acq = new DeskAcq(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();
$k_ordine = $m_params->k_ordine;
$riga = $m_params->riga;
$ord =  $main_module->get_ordine_by_k_docu($k_ordine);
$oe = $main_module->k_ordine_td_decode_xx($k_ordine);


//--------------------------------------------------
if ($_REQUEST['fn'] == 'save_riga_config'){
//--------------------------------------------------
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> "INS_CONF_RIGA",
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array(
                "RIDTEP"  => $m_params->nrec,                
                "RINOTE"  => $m_params->idsc
            )));
    
    $ret = array();
    $ret['success'] = true; //ToDo: parametrizzare
    echo acs_je($ret);
    exit;
}

//--------------------------------------------------
if ($_REQUEST['fn'] == 'exe_ins_righe_ven'){
//--------------------------------------------------
  $sh = new SpedHistory();
  $sh->crea(
      'pers',
      array(
          "messaggio"	=> "INS_RIGA_VEN",
          "k_ordine"	=> $m_params->k_ordine,
          "vals" => array(
              "RIART"  => $m_params->art,
              "RIQTA"  => sql_f($m_params->qta), //??
              "RIIMPO" => sql_f(1), //??
          )));
  
  $ret = array();
  $ret['success'] = true; //ToDo: parametrizzare
  echo acs_je($ret);
  exit;
}



// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    $data = array();
    
    global $backend_ERP;
    switch ($backend_ERP){
        
        case 'GL':
            //Todo
            break;
            
            
        default: //SV2
            
            if(strlen($riga)>0)
              $sql_where = " AND RDRIGA = '{$riga}'";
              
            $sql = "SELECT RRN(RD) AS RRN, RD.*, RT.RTPRZ, AR.ARMODE
                    FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                    LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
                        ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
                    LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR
                        ON RD.RDDT=AR.ARDT AND RD.RDART = AR.ARART
                    WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
                    AND RD.RDTISR = '' {$sql_where}
                    ORDER BY RDRIGA, RDSRIG";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $oe);
            
            $sql_c = "SELECT PLNCOL
                      FROM {$cfg_mod_Spedizioni['file_colli']} PL
                      WHERE PLDT = ?
				      AND PLTIDO = ?
			          AND PLINUM = ?
				      AND PLAADO = ?
				      AND PLNRDO = ?
                      AND PLREDO = ?
                      AND PLLIVE = 0 AND PLRCOL = ''";
            
            $stmt_c = db2_prepare($conn, $sql_c);
            echo db2_stmt_errormsg();
            
            $ar = array();
            while ($row = db2_fetch_assoc($stmt)) {
                
                //stacco dei livelli
                $cod_liv0 = $row['RDRIGA'];
                $cod_liv1 = $row['RDSRIG'];
                
                
                $tmp_ar_id = array();
                $ar_r= &$ar;
                
                //N.RIGA
                $liv =$cod_liv0;
                $tmp_ar_id[] = $liv;
                if (!isset($ar_r["{$liv}"])){
                    $ar_new = $row;
                    $ar_new['children'] = array();
                    $ar_new['id'] = implode("|", $tmp_ar_id);
                    $ar_new['task'] = $row['RDRIGA'];
                    $ar_new['riga'] = $row['RDRIGA'];
                    $ar_new['liv'] = 'liv_0';
                    $ar_new['tipo_liv'] = 'riga';
                    $ar_new['rrn'] = $row['RRN'];
                    $ar_new['RDPROG'] = acs_u8e($row['RDPROG']);
                    $ar_new['ARMODE'] = trim(acs_u8e($row['ARMODE']));
                    $ar_new['RDNRDO'] = acs_u8e($row['RDNRDO']);
                    $ar_new['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
                    $ar_new['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
                    
                    $oe['PLREDO'] = sprintf("%06s", trim($row['RDNREC']));
                   
                    $result = db2_execute($stmt_c, $oe);
                    echo db2_stmt_errormsg();
                    $row_c = db2_fetch_assoc($stmt_c);
                    
                    $ar_new['nr_colli'] = $row_c['PLNCOL'];
                    
                    $rlrife1 = trim($row['RDTIDO']).trim($row['RDINUM']).trim($row['RDAADO']). sprintf("%06s", trim($row['RDNRDO'])).sprintf("%06s", trim($row['RDNREC']));
                    $ar_new['codice'] = trim($row['RDART']);
                    $ha_commenti = $desk_acq->has_commento_riga($rlrife1);
                    
                    if ($ha_commenti == FALSE)
                        $img_com_name = "icone/16x16/comment_light.png";
                    else
                        $img_com_name = "icone/16x16/comment_edit_yellow.png";
                            
                     $ar_new['codice']  .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_bl_riga(\'' . $rlrife1 . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
                    
                    
                    $ar_r["{$liv}"] = $ar_new;
                }
                $ar_r = &$ar_r["{$liv}"];
                
                if ($row['RDSRIG'] == 0)
                    continue;
                    
                    //SOTTORIGA
                    $liv=$cod_liv1;
                    $ar_r = &$ar_r['children'];
                    $tmp_ar_id[] = $liv;
                    if(!isset($ar_r[$liv])){
                        $ar_new = $row;
                        
                        $ar_new['id'] = implode("|", $tmp_ar_id);
                        $ar_new['task'] = $row['RDSRIG'];
                        $ar_new['riga'] = $row['RDRIGA'];
                        $ar_new['sriga'] = $row['RDSRIG'];
                        $ar_new['liv'] = 'liv_1';
                        $ar_new['tipo_liv'] = 'sottoriga';
                        $ar_new['RDPROG'] = acs_u8e($row['RDPROG']);
                        $ar_new['RDNRDO'] = acs_u8e($row['RDNRDO']);
                        $ar_new['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
                        $ar_new['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
                        $ar_new['rrn'] = $row['RRN'];
                        $rlrife1 = trim($row['RDTIDO']).trim($row['RDINUM']).trim($row['RDAADO']).sprintf("%06s", trim($row['RDNRDO'])).sprintf("%06s", trim($row['RDNREC']));
                        $ar_new['codice'] = trim($row['RDART']);
                        $ha_commenti = $desk_acq->has_commento_riga($rlrife1);
                        
                        if ($ha_commenti == FALSE)
                            $img_com_name = "icone/16x16/comment_light.png";
                        else
                            $img_com_name = "icone/16x16/comment_edit_yellow.png";
                                
                        $ar_new['codice']  .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_bl_riga(\'' . $rlrife1 . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
                        
                        $ar_new['leaf'] = true;
                        $ar_r["{$liv}"] = $ar_new;
                    }
                    
            }
            
            
    } //switch $backend_ERP
    

    
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array(
        'success' 	=> true,
        'children' 	=> $ret
    ));
    
    $appLog->save_db();
    exit();
}



//--------------------------------------------------
// TREEPANEL
//--------------------------------------------------

$ord_gest = $main_module->get_ordine_gest_by_k_docu($k_ordine);

?>
{"success": true,
  
 m_win: {
		title: 'Righe ordine',
		title_suf : <?php echo j("{$ord_gest['TDTPDO']} {$oe['TDOADO']} {$oe['TDONDO']} {$oe['TDPROG']}"); ?>,
		width: 1250, height: 450,
		iconCls: 'icon-folder_search-16'
	}, 
 "items":
	{
		xtype: 'treepanel',
		id : 'righe',
		stateful: true,
        stateId: 'righe-ordine-order-entry',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow'],	
		rootVisible: false,
	    loadMask: true,
		
  	    store: Ext.create('Ext.data.TreeStore', {								
					autoLoad:true,
					<!--  loadMask: true,-->	
								        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							timeout: 2400000,
							type: 'ajax',
							reader: {
					            type: 'json',
					            method: 'POST',
					            root: 'children'
					        },
					        actionMethods: {read: 'POST'},
					        
					        extraParams: <?php echo acs_je($m_params); ?>,
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 					        
					        
						},
	        			fields: [
	            			'id', 'task', 'liv', {name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART'
	            			,  'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna'
	            			,  'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO', 'RDPROG', 'ARMODE'
	            			, 'RTPRZ', 'RDSC1', 'RDSC2', 'RDSC3', 'RDSC4', 'RDMAGG', 'dom_risp', 'RDNRDO'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE'
	            			, 'codice', 'nr_colli', 'riga', 'sriga', 'RDLIST', 'rrn', 'RDIVRI', 'RDQTAT' 
	            			, 'RDUMTE', 'RDQTCO', 'RDUMCO', 'RDMODE', 'RDDEPO', 'RDCINT'
	        			]
	    			}),
	    		<?php $cf1 = "<img src=" . img_path("icone/48x48/warning_black.png") . " height=20>"; ?>	
		        columns: [
		        
		        <?php if($m_params->from_righe_info == 'Y'){?>
		               {text: '<?php echo $cf1; ?>',	
		                width: 30, tdCls: 'tdAction',
		                dataIndex: 'dom_risp', 
		        		tooltip: 'Visualizza configurazione',		        			    	     
		    			renderer: function(value, metaData, record){		    			  
		    			  if ((!Ext.isEmpty(record.get('ARMODE')) && record.get('RDPROG') == 0))
		    			  	metaData.tdCls += ' sfondo_rosso';

		    			  if (record.get('RDPROG') > 0 || !Ext.isEmpty(record.get('ARMODE')) ||  record.get('codice').substring(0, 2) == '*I') 
		    			  	return '<img src=<?php echo img_path("icone/48x48/game_pad.png") ?> width=18>';		    			  	
		    		  }},
		        <?php }?>
		        	{
			            xtype: 'treecolumn', 
			            columnId: 'task', 
			            width: 150,			            
			            dataIndex: 'task',
			            menuDisabled: true, 
			            sortable: false,
			            header: 'Riga/sottoriga'
			        },
					{
		                header   : '<br/>&nbsp;Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
		                hidden: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
									if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
				  					return date_from_AS(value);			    
								}		                
		                 
		             },		
		                     		        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'codice', 
		                width: 150
		             }
		             
		             
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
		    			    	}}			    			             
		             
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 1              			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             },  {
		                header   : '<br/>&nbsp;Nr collo',
		                dataIndex: 'nr_colli', 
		                width    : 50,
		                align: 'right'
								                		                
		             },
		             <?php if($m_params->from_prod != 'Y'){?>
		              {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, 
		             {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
							if (record.get('RDQTE') == 0) return ''; //non mostro																											
		  					return floatRenderer2(value);			    
						}		                
		                
		             },
		             <?php }?>
		             
		              {
		                header   : '<br/>&nbsp;S',
		                dataIndex: 'RDSTEV', 
		                width    : 20,
		                align: 'right'
		             }, {
		                header   : '<br/>&nbsp;T.R.',
		                dataIndex: 'RDT	PRI', 
		                width    : 30
		             },
		             <?php if($m_params->from_prod != 'Y'){?>
		              {
		                header   : '<br/>&nbsp;Pr.',
		                dataIndex: 'RDPRIO', 
		                width    : 30,
		                hidden: true
		             }, {
		                header   : '<br/>&nbsp;St.',
		                dataIndex: 'RDSTAT', 
		                width    : 30,
		                hidden: true
		             }, {
		                header   : '<br/>&nbsp;Carico',
		                dataIndex: 'RDNRCA', 
		                width    : 80,
		                hidden: true,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRCA') == 0) return ''; //non mostro																											
						  					return rec.get('RDTICA') + "_" + rec.get('RDNRCA');			    
										}		                
		             },
		             {
		                header   : '<br/>&nbsp;Prezzo',
		                dataIndex: 'RTPRZ', 
		                width    : 50,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RTPRZ') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}	
		             },
 		             {
		                header   : '<br/>List.',
		                dataIndex: 'RDLIST', 
		                width    : 50
		             },
		             {
		                header   : '<br/>&nbsp;Sc1',
		                dataIndex: 'RDSC1', 
		                width    : 50,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC1') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },
		             {
		                header   : '<br/>&nbsp;Sc2',
		                dataIndex: 'RDSC2', 
		                width    : 50,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC2') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },
		              {
		                header   : '<br/>&nbsp;Sc3',
		                dataIndex: 'RDSC3', 
		                width    : 50,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC3') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             }, {
		                header   : '<br/>&nbsp;Sc4',
		                dataIndex: 'RDSC4', 
		                width    : 50,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDSC4') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },{
		                header   : '<br/>&nbsp;Magg',
		                dataIndex: 'RDMAGG', 
		                width    : 50,
		                renderer: function (value, metaData, rec, row, col, store, gridView){
							if (rec.get('RDMAGG') == 0) return ''; //non mostro																											
		  					
		  					return floatRenderer2(value);			    
							}
		             },
		             <?php }?>
		             
		              <?php if($m_params->from_righe_info != 'Y'){?>
		               {text: '<?php echo $cf1; ?>',	
		                width: 30, tdCls: 'tdAction',
		                dataIndex: 'dom_risp', 
		        		tooltip: 'Visualizza domande/risposte',		        			    	     
		    			renderer: function(value, p, record){
		    			  if (record.get('RDPROG') > 0) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';
		    		  }},
		        <?php }?>
		             
		             
				/*<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             , {
		                header   : '<br/>&nbsp;Commessa',
		                dataIndex: 'COMMESSA', 
		                width    : 80
		             }				
		        <?php } else { ?>
		             , {
		                header   : '<br/>&nbsp;Lotto',
		                dataIndex: 'RDNRLO', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRLO') == 0) return ''; //non mostro																											
						  					return rec.get('RDTPLO') + "_" + rec.get('RDNRLO');			    
										}
		             }		        
		        <?php } ?> */
		        
		        <?php echo dx_mobile() ?>
		        
		         ]	    					
		
		, listeners: {
		
		   beforeload: function(store, options) { 
		      var win = this.up('window');
		      Ext.getBody().mask('Loading... ', 'loading').show();
            },

            load: function () {
             Ext.getBody().unmask(); 
            },  
		
			beforestaterestore: function(comp, state){
				//ESCLUDO SEMPRE LA MEMORIZZAZIONE DALLA STATEEVENTS
				delete state.sort;
			},
		
			
			celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  					  						  
					  	var col_name = iView.getGridColumns()[iColIdx].dataIndex,
					  		rec = iView.getRecord(iRowEl),
					  		m_comp = iView.panel;					  							  
						  
						if(col_name == 'dom_risp'){
						
							if(rec.get('liv') == 'liv_0') {
								
						    if(rec.get('RDPROG') > 0){						    
						    
						    	//se e' gia' stato configurato mostro le variabili di configurazione
						    	acs_show_win_std('Configurazione ordine ' +'<?php echo "{$oe['TDOADO']}_{$oe['TDONDO']}"; ?>'  + ' Riga ' +rec.get('riga') + ' S/R ' +rec.get('sriga'), 
						    		'../desk_utility/acs_get_domande_risposte_riga.php?fn=open_form', {
						    			k_ordine: <?php echo j($k_ordine); ?>, 
						    			prog: rec.get('RDPROG'), 
						    			from_righe_info : '<?php echo $m_params->from_righe_info; ?>'
						    		}, 900, 450, null, 'icon-folder_search-16');						    		
						    	} else {
						    	
						    	  if(rec.get('ARMODE').trim() != '' || rec.get('codice').substring(0, 2) == '*I')
        		        		   		m_comp.acs_actions.configura(m_comp, rec.data);
        		        				   		
                		  		}		  	      
						    } //liv_0																								          	
					  		else
					  			acs_show_win_std('Configurazione ordine ' +'<?php echo "{$oe['TDOADO']}_{$oe['TDONDO']}"; ?>'  + ' Riga ' +rec.get('riga') + ' S/R ' +rec.get('sriga'), '../desk_utility/acs_get_domande_risposte_riga.php?fn=open_form', {k_ordine: '<?php echo $k_ordine; ?>', prog: rec.get('RDPROG'), from_righe_info : '<?php echo $m_params->from_righe_info; ?>'}, 900, 450, null, 'icon-folder_search-16');          		
					  	}
						  
						  }
					  }
	 		
				  
				  
				  
				 , itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);				      		 
				      var voci_menu = [];
				      var t_win = this.up('window');
				      var grid = this;
				      var m_comp = grid;	
				     				   				      
				      if(rec.get('liv') == 'liv_0' && rec.get('RDQTA') > 0){
				
				     		voci_menu.push({
				         		text: 'Visualizza distinta componenti',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			
				        		 	var my_listeners = {
						            	onCompoSelected: function(from_win, row_selected){
					        				t_win.fireEvent('onCompoSelected', t_win, row_selected);
					        				from_win.close();
				        				}
									};
				        						        		
				        			acs_show_win_std('Distinta componenti', '../desk_utility/acs_get_riga_componenti.php', {k_ordine: '<?php echo $k_ordine; ?>', nrec: rec.get('RDNREC'), d_art: rec.get('RDDART'), c_art : rec.get('RDART'), riga : rec.get('RDRIGA'), from_anag_art : '<?php echo $m_params->from_anag_art; ?>'}, 1300, 450, my_listeners, 'icon-folder_search-16');          		
				        		}
				    		});
				    		
				    		
				    		voci_menu.push({
				         		text: 'Riepilogo articoli/componenti',
				        		iconCls : 'icon-print-16',          		
				        		handler: function() {
				        			acs_show_win_std('Riepilogo articoli/componenti', '../desk_utility/acs_report_riepilogo_art_comp.php?fn=open_form', {k_ordine: '<?php echo $k_ordine; ?>'}, 330, 150, {}, 'icon-print-16');   	     	
				        		}
				    		});
				    		
				    			
				    		voci_menu.push({
				         		text: 'Elenco completo articoli/componenti',
				        		iconCls : 'icon-leaf-16',          		
				        		handler: function() {
				        			acs_show_win_std('Riepilogo articoli/componenti', '../desk_utility/acs_elenco_riepilogo_art_comp.php?fn=open_elenco', {k_ordine: '<?php echo $k_ordine; ?>'}, 1300, 450, {}, 'icon-leaf-16');   	     	
				        		}
				    		});
				    		
				    		}
				    		
				    		
				    <?php if($m_params->from_prod != 'Y'){?>	
				    		
					 if(rec.get('RDPROG') > 0){
					 
					 <?php if ($m_params->modificabile == 'Y'){ ?>
    				    	     voci_menu.push({
        		         				text: 'Azzera e rigenera configurazione',
        		        				iconCls : 'iconDelete',             		
        		        				handler: function () {
        		        				    
        		        				   
        		        				   	m_comp.acs_actions.configura(m_comp, rec.data);
        		        				    						        		  								    		//avvio configurazione
                		               
        				                } //handler
        			    		  });			    			
    			    			
    			    		<?php } ?>	
    			    		
    			   <?php if ($m_params->only_view != 'Y'){ ?>	
				   	     voci_menu.push({
				         		text: 'Inserisci/modifica configurazione',
				        		iconCls : 'icon-pencil-16',          		
				        		handler: function() {
				        			
				        		 	acs_show_win_std('Configurazione ordine ' +'<?php echo "{$oe['TDOADO']}_{$oe['TDONDO']}"; ?>'  + ' Riga ' +rec.get('riga') + ' S/R ' +rec.get('sriga'), 
						    		'../desk_utility/acs_get_domande_risposte_riga.php?fn=open_form', {
						    			k_ordine: <?php echo j($k_ordine); ?>, 
						    			prog: rec.get('RDPROG'), 
						    			from_righe_info : '<?php echo $m_params->from_righe_info; ?>',
						    			modifica : 'Y'
						    		}, 900, 450, null, 'icon-pencil-16');		    		
				        		}
				    		});
				    		<?php }?>
				     }
				     
			      <?php if ($m_params->only_view != 'Y'){ ?>		
				     
				     
				     if (rec.get('RDART').substr(0,1) != '*'){			
			           voci_menu.push({
				         		text: 'Modifica prezzo',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		  
						        		  var my_listeners = {						        			
						        					afterModArt: function(from_win, jsonData){
						        						//aggiorno il record con il row ritornato
						        						rec.set(jsonData.row);
			 											from_win.close();
										        	}
												}
					            			acs_show_win_std('Modifica prezzo articolo', '../desk_utility/acs_modifica_righe.php?fn=form_mod_art', {k_ordine: <?php echo j($m_params->k_ordine); ?>, articolo: rec.data}, 400, 450, my_listeners, 'icon-pencil-16');          		
					            	}
				    		});
				    		
				    		voci_menu.push({
				         		text: 'Modifica quantit&agrave;',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		   
				        		       var my_listeners = {
						        			
						        					afterModArt: function(from_win, jsonData){
						        						//aggiorno il record con il row ritornato		
			 										    rec.set(jsonData.row);
			 											from_win.close();
			 											
										        	}
												}
												
					            			acs_show_win_std('Modifica quantit&agrave; articolo', '../desk_utility/acs_modifica_righe.php?fn=form_mod_art', {k_ordine: <?php echo j($m_params->k_ordine); ?>, articolo: rec.data, quant: 'Y'}, 400, 300, my_listeners, 'icon-pencil-16');          		
					            	}
				    		});
				    		
				    			voci_menu.push({
				         		text: 'Info',
				        		iconCls : 'icon-info_black-16',          		
				        		     handler: function() {
				        		   		acs_show_win_std('Info', '../desk_utility/acs_modifica_righe.php?fn=form_mod_art', {articolo: rec.data, info : 'Y'}, 400, 200, {}, 'icon-info_black-16');          		
					            	}
				    		});
				    		
				    		
				    		}
				    		
				    		voci_menu.push({
				         		text: 'Cancella riga',
				        		iconCls : 'icon-sub_red_delete-16',          		
				        		     handler: function() {
											
											   Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'operazione?', function(btn, text){																							    
											   if (btn == 'yes'){					        		
							        		
													Ext.Ajax.request({
													   url        : '../desk_utility/acs_modifica_righe.php?fn=exe_cancella_riga',
													   method: 'POST',
													   jsonData: {
													   		k_ordine: <?php echo j($m_params->k_ordine) ?>, 
													   		nrec : rec.get('RDNREC'),
													   		c_art : rec.get('RDART'),
													   		d_art : rec.get('RDDART')
													   }, 
													   
													   success: function(response, opts) {
													   	  Ext.getBody().unmask();
														  var jsonData = Ext.decode(response.responseText);
														  grid.getStore().load();									   	  
													   }, 
													   failure: function(response, opts) {
													      Ext.getBody().unmask();
													      alert('error on save_manual');
													   }
													});	
			
												  }
												});											
											
					            			          		
					            	} //handler
				    		});	
				    		
				    			voci_menu.push({
				         		text: 'Visualizza listino',
				        		iconCls : 'icon-currency_black_dollar-16',          		
				        		     handler: function() {
											
            				  	       acs_show_win_std('Listino di VENDITA [' +rec.get('RDART') + '] ' +rec.get('RDDART') , '../desk_utility/acs_anag_art_listini.php?fn=open_tab', {c_art: rec.get('RDART'), tipo_list: 'V', solo_visualizzazione : 'Y'}, 1250, 500, null, 'icon-currency_black_dollar-16');									
											
					            			          		
					            	} //handler
				    		});	
				    		<?php }?>
				    		
				    		<?php }?>
				    	
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
				      
				 } //itemcontextmenu
	 			 			
		}
			
		, viewConfig: {
		
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';	
		        	if (rec.get('liv') == 'liv_1') 	return 'rigaNonSelezionata'; //SOTTORIGA GRIGIA        	
		         }   
		    }												    
			
			
		, buttons: [
		
			<?php if ($m_params->modificabile == 'Y'){ ?>
					{
    					fieldLabel: 'Articolo',
    					labelAlign: 'right',
    					xtype: 'textfield',
    					itemId: 'add_product_code'
    				}, {
    					fieldLabel: 'Quantit&agrave;',
    					labelAlign: 'right',
    					labelWidth : 50,
    					width : 120,
    					xtype: 'numberfield',
    					hideTrigger : true,	
    					decimalPrecision : 4,
    					itemId: 'add_qta'
    				},{
    					 text: 'Aggiungi',
    					 iconCls: 'icon-button_blue_play-24', scale: 'medium',
    					 handler: function () {
    					 	var m_comp = this.up('treepanel');    					 	
    					 	m_comp.acs_actions.add_product_by_code(m_comp, m_comp.down('#add_product_code').getValue(), m_comp.down('#add_qta').getValue()); 					 	
    					 } //handler
    				}
			<?php } ?>
		] //buttons
		         
		         
		, acs_actions: {
		
			save_riga_config: function(treepanel, nrec, idsc){
				if (Ext.isEmpty(nrec) || Ext.isEmpty(idsc)){
					acs_show_msg_error('Dati mancanti');
					return false;
				}
				
		          Ext.Msg.confirm('Richiesta conferma', 'Confermi salvataggio?' , function(btn, text){
        	   		if (btn == 'yes'){
     			          Ext.Ajax.request({
    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_riga_config',
    				        timeout: 2400000,
    				        method     : 'POST',
    	        			jsonData: {
    	        				nrec: nrec,
    	        				idsc: idsc,
    	        				k_ordine: <?php echo j($m_params->k_ordine) ?>
    						},							        
    				        success : function(result, request){
    					        var jsonData = Ext.decode(result.responseText);
    					        if (jsonData.success == true){    					        	
    					        	treepanel.store.load();
    					        } else {
    					        	acs_show_msg_error('Errore');
    					        }
    				        },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    }); 		     
	           
			    	}
    	   		  });				
				
				
			}, //save_riga_config     
		
			add_product_by_code: function(treepanel, product_code, qta){
				
				if (Ext.isEmpty(product_code)){
					acs_show_msg_error('Indicare un codice articolo');
					return false;
				}
				

		          Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta inserimento?' , function(btn, text){
        	   		if (btn == 'yes'){
     			          Ext.Ajax.request({
    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ins_righe_ven',
    				        timeout: 2400000,
    				        method     : 'POST',
    	        			jsonData: {
    	        				art: product_code,
    	        				qta : qta,
    	        				k_ordine: <?php echo j($m_params->k_ordine) ?>
    						},							        
    				        success : function(result, request){
    					        var jsonData = Ext.decode(result.responseText);
    					        if (jsonData.success == true){    					        	
    					        	treepanel.store.load();
    					        } else {
    					        	acs_show_msg_error('Errore');
    					        }
    				        },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    }); 		     
	           
			    	}
    	   		  });
				
			},
			configura : function(treepanel, rec){
			
				var my_listeners = {
					afterConferma: function(from_win, idsc){
    						rec.idsc = idsc;
    						from_win.destroy();
    						treepanel.acs_actions.save_riga_config(treepanel, rec.RDNREC, idsc);                    						                    						 						        									           
			        		}
				};	
	    				
				if(rec.codice.substring(0, 2) == '*I'){
						    	  
    	  	  			var my_list = {
			    	  
        					afterAggiorna: function(from_win, idsc){
        						acs_show_win_std('Configuratore articolo', 
    		  				'../base/acs_configuratore_articolo.php?fn=open_tab', 
    		  				{	c_art :  rec.RDART, 
    		  					gestione_dim: 'Y',
    		  					k_ordine : <?php echo j($m_params->k_ordine); ?>,
    		  					nrec : rec.RDNREC
    		  				}, 600, 400, my_listeners, 'icon-copy-16');	
    		  				from_win.destroy();           
				        		}
		    			};
    	  
		    	   acs_show_win_std('Configuratore articolo', 
		  				'../base/acs_configuratore_articolo.php?fn=open_pumo', 
		  				{	c_art :  rec.RDART,
		  				    mode : rec.RDMODE,
		  				    rrn : rec.rrn
		  				}, 500, 250, my_list, 'icon-copy-16');
		    	   }else{
		    	   
		    	    <?php if ($m_params->modificabile == 'Y'){ ?>
		    	  
		    	     acs_show_win_std('Configuratore articolo', 
		  				'../base/acs_configuratore_articolo.php?fn=open_tab', 
		  				{	
		  					c_art :  rec.RDART, 
		  					k_ordine : <?php echo j($m_params->k_ordine); ?>,
		  					nrec : rec.RDNREC,
		  					mode : rec.ARMODE,
		  					gestione_dim: 'Y'
		  				}, 600, 400, my_listeners, 'icon-copy-16');
		  				
		  			<?php }?>
		    	  
		    	  }	
			
			}
		} //acs_actons         
		         
	}
}
