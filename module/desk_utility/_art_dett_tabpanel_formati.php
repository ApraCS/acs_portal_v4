<?php 

$desk_art = new DeskArt();

?>
                    new Ext.create('Ext.form.Panel', {
                        title: 'Formati',
                        flex : 1,
                        itemId: 'tp-dett-art-formati',
                        bodyPadding: 5,
                        autoScroll: true,
                        layout: 'anchor',
                        defaults: {
                        		anchor: '100%',
								labelWidth: 170
                            },
                            
                            
                       save_form: function(fmb){
			               var form = fmb.getForm();
			               var info = fmb.down('#info');
			               loc_win = fmb.up('window');
			               if (form.isValid()){	
			               
									Ext.Ajax.request({
		 						        url        : 'acs_anag_art_get_formati.php?fn=exe_save_form',
		 						        method     : 'POST',
		 			        			jsonData: {
		 			        				form_values: form.getValues()
		 								},							        
		 						        success : function(result, request){
		 						        	jsonData = Ext.decode(result.responseText);
		 						        	
		 						        	form.reset();
		 						        	info.show();
		 						        	form.findField('data').setValue(jsonData.record.data);
		 						        	form.findField('ora').setValue(jsonData.record.ora);
		 						        	form.findField('utente').setValue(jsonData.record.utente);
		 						        	form.findField('RRN').setValue(jsonData.record.RRN);
                       						form.setValues(jsonData.record[0]);
			 						        
		 			            		},
		 						        failure    : function(result, request){
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });	
							
			                }                       	
                       	
                       	
                       },
                            
                            
                       loadArt : function(cod_art){
                       		var m_form = this.getForm();                       		
                       		var form = this;
                       		form.down('#bordo').hide();
                       		form.down('#pannello').hide();
                       		form.down('#info').hide();
                       		//form.down('#b_salva').hide();
                       		m_form.reset();
							Ext.Ajax.request({
						   		url: 'acs_anag_art_get_formati.php?fn=open_f&m_art=' + cod_art,
						   		success: function(response, opts) {
						      		var src = Ext.decode(response.responseText);
						      		var sg = src.record.ARSGME;
						      	
						      		<?php  if(isset($formati_per_sottogruppo)){  ?>  

						  			config_gest_sg = <?php echo $formati_per_sottogruppo ?>;
						  			array_formati = config_gest_sg[sg];
						  			
						  			for (var chiave in array_formati) {
						  					id_fieldset = '#' + array_formati[chiave];
						  					form.down(id_fieldset).show();
						  					//form.down('#b_salva').show();
						  			}
						      		<?php } ?>
						      							      
						      		
						      		if(src.record.RRN != ''){
						      	    	form.down('#info').show();   
    						      		m_form.findField('data').setValue(src.record.data);
    						      		m_form.findField('ora').setValue(src.record.ora);
    						      		m_form.findField('utente').setValue(src.record.utente);
						      		}
						      		m_form.setValues(src.record);
						   }
						});
                       },  
                       refreshCombo : function(combo, from_win){
                       
                    				Ext.Ajax.request({
                				   		url: 'acs_anag_art_get_formati.php?fn=get_ta',
                				   		method: 'POST',
                	        			jsonData: {
                	        				tataid: combo.acs_tataid
                	        				},
                				   		success: function(response, opts) {
                				      		var src = Ext.decode(response.responseText);
                				      		combo.store.loadData(src.data);
                				      		from_win.close();
                					   	}
                					});
                       },   
					items: [
					     {
						 	xtype: 'textfield',
                            fieldLabel: 'Codice materiale',
                            name: 'AFCMAT',
                            hidden: true
                       		
                        }, {
						 	xtype: 'textfield',
                            fieldLabel: 'rrn',
                            name: 'RRN',
                            hidden: true
                       		
                        },{
						 	xtype: 'displayfield',
                            fieldLabel: 'Sottogruppo',
                            name: 'ARSGME',
                            hidden: true
                           
                        },
                   	 {
					xtype: 'fieldset',
	                title: 'Articolo',
	                style: 'padding: 0px 0px 5px 5px',
                    bodyStyle: 'padding: 0px 0px 5px 5px',
	                layout: 'hbox',
	                defaults: {
						xtype: 'displayfield',					                
						fieldStyle: 'font-weight: bold;'															                
					 },
					flex:1,
	                items: [
						
						      {
						 	xtype: 'displayfield',
                            fieldLabel: '',
                            margin : '0 10 0 0',
                            name: 'ARART'
                            
                        },{
						 	xtype: 'displayfield',
                            fieldLabel: '',
                            name: 'ARDART'
                           
                        },  
 					
					 
	             ]}  ,{
    					xtype: 'fieldset',
    	                title: 'Pannello',
    	                flex : 1,
    	                style: 'padding: 0px 5px 5px 5px',
                        bodyStyle: 'padding: 0px 5px 5px 5px',
    	                layout: 'anchor',
    	                itemId : 'pannello',
    	                hidden : true,
    	                items: [
    	                
    						{
							 	xtype: 'fieldcontainer',
								flex: 1,
            					layout: { 	type: 'hbox',
            							    pack: 'start',
            							    align: 'stretch'},						
            					items: [{            					
								name: 'AFCDEC',
								xtype: 'combo',
								itemId : 'DECAF',
								acs_tataid : 'DECAF',
								fieldLabel: 'Codice decoro',
								//labelWidth: 170,
								flex: 1,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								queryMode: 'local',
								minChars: 1,								
								emptyText: '- seleziona -',
						   		store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('DECAF', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                            		beforequery: function (record) {
                            			record.query = new RegExp(record.query, 'i');
                            			record.forceAll = true;
                            		}
                                 }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			             var combo = this.up('panel').down('#DECAF');
			             var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }	
						  
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_decoro.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},
						
						{
							 	xtype: 'fieldcontainer',
								layout: { 	type: 'hbox',
            							    pack: 'start',
            							    align: 'stretch'
            							    },	
            			       				
            					items: [
            					{ xtype: 'displayfield', value : 'Materiale (mm): ', margin : '0 10 0 0'},
            					{
                        	xtype: 'numberfield',
                            fieldLabel: 'Lunghezza',
                           // labelAlign : 'right',
                            labelWidth: 70,
                            width : 180,
                            name: 'AFLUNG',
                            hideTrigger  : true,
                            decimalPrecision: 4,
                            margin : '0 0 0 10'
                        },{
                        	xtype: 'numberfield',
                        	labelWidth: 70,
                        	width : 180,
                            fieldLabel: 'Larghezza',
                            labelAlign : 'right',
                            name: 'AFLARG',
                            hideTrigger  : true,
                            decimalPrecision: 4
                        },{
                        	xtype: 'numberfield',
                        	labelWidth: 65,
                        	width : 165,
                            fieldLabel: 'Spessore',
                            name: 'AFSPES',
                            hideTrigger  : true,
                            labelAlign : 'right',
                            decimalPrecision: 4
                        },
						
						]},
						
						{
						xtype: 'fieldcontainer',
						flex : 1, 
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
                        	xtype: 'numberfield',
                        	width : 285,
                            fieldLabel: 'Peso specifico',
                            name: 'AFPSPE',
                            hideTrigger  : true,
                            decimalPrecision: 4,
                            margin : '0 15 0 0'
                        },
						{
                        	xtype: 'numberfield',
                            fieldLabel: 'Quantit&agrave; di stock',
                            labelWidth : 230,
                            labelAlign : 'right',
                            name: 'AFQSTO',
                            width : 330,
                            hideTrigger  : true,
                            decimalPrecision: 0
                        },
						
						]
						
						}
						
						,{
						xtype: 'fieldcontainer',
						flex : 1, 
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFVENA',
								xtype: 'combo',
								fieldLabel: 'Venatura',
								itemId : 'ANFVE',
								acs_tataid : 'ANFVE',
								flex : 1,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',	
								queryMode: 'local',
								minChars: 1,							
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFVE', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                            		beforequery: function (record) {
                            			record.query = new RegExp(record.query, 'i');
                            			record.forceAll = true;
                            		}
                                 }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                var combo = this.up('panel').down('#ANFVE');
			                var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }	
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_venatura.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]}
						
						,	
						{xtype: 'fieldcontainer',
						flex : 1, 
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFTMAT',
								xtype: 'combo',
								fieldLabel: 'Tipo materiale',
								forceSelection: true,	
								itemId : 'ANFTM',
								acs_tataid : 'ANFTM',
								flex : 1,					
								displayField: 'text',
								valueField: 'id',	
								queryMode: 'local',
								minChars: 1,							
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFTM', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                            		beforequery: function (record) {
                            			record.query = new RegExp(record.query, 'i');
                            			record.forceAll = true;
                            		}
                                 }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                var combo = this.up('panel').down('#ANFTM');
			                var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }	
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_materiale.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFTPIL',
								xtype: 'combo',
								itemId : 'ANFTP',
								fieldLabel: 'Tipo pila',
								flex : 1,
								acs_tataid : 'ANFTP',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',	
								queryMode: 'local',
								minChars: 1,							
								emptyText: '- seleziona -',
						   		//allowBlank: false,
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFTP', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},
								listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                var combo = this.up('panel').down('#ANFTP');
			                var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }	
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_pila.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]}
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
					
						
						,						{
							 xtype: 'fieldcontainer',
							 flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFFUTI',
								xtype: 'combo',
								fieldLabel: 'Frequenza utilizzo',
								forceSelection: true,	
								itemId: 'ANFFU',
								acs_tataid : 'ANFFU',							
								displayField: 'text',
								valueField: 'id',		
								flex : 1,
								queryMode: 'local',
								minChars: 1,					
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFFU', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                            		beforequery: function (record) {
                            			record.query = new RegExp(record.query, 'i');
                            			record.forceAll = true;
                            		}
                                 }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			            	var combo = this.up('panel').down('#ANFFU');
			                var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_freq_util.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]}
						
						,{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFDIMM',
								xtype: 'combo',
								fieldLabel: 'Da immagazz.',
								forceSelection: true,	
								acs_tataid : 'ANFIM',
								itemId : 'ANFIM',							
								displayField: 'text',
								valueField: 'id',
								queryMode: 'local',
								minChars: 1,	
								flex : 1,					
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFIM', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
                        								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                var combo = this.up('panel').down('#ANFIM');
			                var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_da_immagaz.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},
						
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFPELL',
								xtype: 'combo',
								fieldLabel: 'Pellicola',
								flex : 1,
								itemId : 'ANFPL',
								acs_tataid : 'ANFPL',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',						
								emptyText: '- seleziona -',
								queryMode: 'local',
								minChars: 1,
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFPL', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                            		beforequery: function (record) {
                            			record.query = new RegExp(record.query, 'i');
                            			record.forceAll = true;
                            		}
                                 }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			             var combo = this.up('panel').down('#ANFPL');
			             var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }	
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_pellicola.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},
						
							{
							 	xtype: 'fieldcontainer',
								//flex: 1,
            					layout: { 	type: 'hbox',
            							    pack: 'start',
            							    align: 'stretch'
            							    },	
            			       				
            					items: [
            					{ xtype: 'displayfield', value : 'Margine da non utilizzare (mm):', margin : '0 30 0 0'},
            					
            				{
                        	xtype: 'numberfield',
                            fieldLabel: 'Dx',
                            name: 'AFMDES',
                            hideTrigger  : true,
                            labelWidth: 20,
                            width : 100,
                            decimalPrecision: 4
                        },{
                        	xtype: 'numberfield',
                            fieldLabel: 'Sopra',
                            name: 'AFMSOP',
                            hideTrigger  : true,
                            labelAlign : 'right',
                            labelWidth: 40,
                            width : 120,
                            decimalPrecision: 4
                        },{
                        	xtype: 'numberfield',
                            fieldLabel: 'Sx',
                            name: 'AFMSIN',
                            hideTrigger  : true,
                            labelWidth: 20,
                            labelAlign : 'right',
                            width : 100,
                            decimalPrecision: 4
                        },{
                        	xtype: 'numberfield',
                            fieldLabel: 'Sotto',
                            name: 'AFMSOT',
                            hideTrigger  : true,
                            labelAlign : 'right',
                            labelWidth: 40,
                            width : 112,
                            decimalPrecision: 4
                        }
						
						]},
						
                        
                        {
							 	xtype: 'fieldcontainer',
								//flex: 1,
            					layout: { 	type: 'hbox',
            							    pack: 'start',
            							    align: 'stretch'
            							    },	
            			       				
            					items: [
            					
            					{ xtype: 'displayfield', value : 'Resto minimo (mm): ', margin : '0 30 0 0'},
            					
                    			{
                                	xtype: 'numberfield',
                                    fieldLabel: ' In lunghezza',
                                    name: 'AFRMLU',
                                    hideTrigger  : true,
                                    labelAlign : 'right',
                                    labelWidth: 80,
                                    width : 160, 
                                    decimalPrecision: 4,
                                    margin : '0 60 0 0'
                                },{
                                	xtype: 'numberfield',
                                    fieldLabel: 'In larghezza ',
                                    name: 'AFRMLA',
                                    labelAlign : 'right',
                                    hideTrigger  : true,
                                    labelWidth: 80,
                                    width : 160, 
                                    decimalPrecision: 4
                                }
        						
						]},
                       
						 {
						xtype: 'fieldcontainer',
			   		    flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFBCOL',
								xtype: 'combo',
								fieldLabel: 'Colla',
								itemId : 'ANFCL',
								acs_tataid : 'ANFCL',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								flex : 1,
								queryMode: 'local',
								minChars: 1,					
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFCL', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},
								listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			             var combo = this.up('panel').down('#ANFCL');
			                   var pan = this.up('panel');
			            	   var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_colla.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},
                         
                        {
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFTSUP',
								xtype: 'combo',
								fieldLabel: 'Tipo superficie',
								flex : 1,
								forceSelection: true,	
								itemId : 'ANFTS',
								acs_tataid : 'ANFTS',							
								displayField: 'text',
								valueField: 'id',
								queryMode: 'local',
								minChars: 1,						
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFTS', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                var combo = this.up('panel').down('#ANFTS');
			                var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_tipo_superficie.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},
						
						 
						 {
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFBTON',
								xtype: 'combo',
								fieldLabel: 'Tonalit&agrave;',
								flex: 1,
								forceSelection: true,
								itemId : 'ANFTN',		
								acs_tataid: 'ANFTN',						
								displayField: 'text',
								valueField: 'id',		
								queryMode: 'local',
								minChars: 1,				
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFTN', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                   var combo = this.up('panel').down('#ANFTN');
			                   var pan = this.up('panel');
			            	   var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_tonalita.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},
						
						{
                    	xtype: 'numberfield',
                        fieldLabel: 'Tipo utensile',
                        name: 'AFTUTI',
                        hideTrigger  : true,
                        width : 160, 
                        margin : '0 60 0 0'
                    }
 					
	             ]},
                        
					 		 {
					xtype: 'fieldset',
	                title: 'Bordo',
	                style: 'padding: 0px 5px 5px 5px',
                    bodyStyle: 'padding: 0px 5px 5px 5px',
	                layout: 'anchor',
	                hidden : true,
	                itemId : 'bordo',
	                flex:1,
	                items: [
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFBSPE',
								xtype: 'combo',
								fieldLabel: 'Spessore',
								itemId : 'ANFSP',
								acs_tataid : 'ANFSP',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								labelWidth: 170,	
								queryMode: 'local',
								minChars: 1,					
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFSP', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                  var combo = this.up('panel').down('#ANFSP');
			                  var pan = this.up('panel');
			            	  var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_spessore.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFBRET',
								xtype: 'combo',
								fieldLabel: 'Rettifica',
								itemId : 'ANFRT',
								acs_tataid : 'ANFRT',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								labelWidth: 170,	
								queryMode: 'local',
								minChars: 1,					
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFRT', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								},
								listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                   var combo = this.up('panel').down('#ANFRT');
			                   var pan = this.up('panel');
			            	   var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_rettifica.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFBTIP',
								xtype: 'combo',
								fieldLabel: 'Tipo',
								itemId : 'ANFTI',
								acs_tataid : 'ANFTI',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								labelWidth: 170,	
								queryMode: 'local',
								minChars: 1,					
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFTI', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								}, listeners: {
                    		beforequery: function (record) {
                    			record.query = new RegExp(record.query, 'i');
                    			record.forceAll = true;
                    		}
                         }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			                   var combo = this.up('panel').down('#ANFTI');
			                   var pan = this.up('panel');
			            	   var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_bordo_tipo.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]},{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'AFBPEL',
								xtype: 'combo',
								fieldLabel: 'Pellicola',
								itemId : 'ANFBP',
								acs_tataid : 'ANFBP',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								labelWidth: 170,	
								queryMode: 'local',
								minChars: 1,					
								emptyText: '- seleziona -',
						   		//allowBlank: false,			
								store: {
									editable: false,
									autoDestroy: true,
								    fields: ['id', 'text'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ANFBP', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								}, listeners: {
                        		beforequery: function (record) {
                        			record.query = new RegExp(record.query, 'i');
                        			record.forceAll = true;
                        		}
                             }
								
						 },
						
						 {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
			             var combo = this.up('panel').down('#ANFBP');
			             var pan = this.up('panel');
			            	var my_listeners = {
		        				afterInsert: function(from_win){	
		        					pan.refreshCombo(combo, from_win);
		        					
						        	}								  
								  }	
									acs_show_win_std('Anagrafica articoli formati', 'acs_anag_art_pellicola_bordo.php?fn=open_tab', {}, 950, 500, my_listeners, 'icon-gear-16');
								} //handler function()
						 
						 }
						
						]}
 					
					 
	             ]},
                       
							 {
					xtype: 'fieldset',
	                title: 'Ultimo aggiornamento',
	                style: 'padding: 0px 5px 5px 5px',
                    bodyStyle: 'padding: 0px 5px 5px 5px',
	                layout: 'anchor',
					itemId : 'info',
				    hidden : true,
	                flex:1,
	                items: [
						
						 {
						xtype: 'fieldcontainer',
						layout: { 	type: 'vbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
    						{
						 	xtype: 'displayfield',
                            fieldLabel: 'Utente',
                            name: 'utente',
                            labelWidth: 50,
                           // margin : '0 20 0 0'
                            },{
						 	xtype: 'displayfield',
                            fieldLabel: 'Data',
                            name: 'data',
                            labelWidth: 50,
                            renderer : date_from_AS,
                          //  margin : '0 20 0 0'
                           	},{
						 	xtype: 'displayfield',
                            fieldLabel: 'Ora',
                            name: 'ora',
                            labelWidth: 50,
                            renderer : time_from_AS
                           
                        }
    						
    						
						]}
 					
					 
	             ]}
                     
                        
                        
                        ],
                        
                        
                        
                      buttons22222: [{
			            text: 'Salva',
			            itemId: 'b_salva',
			            hidden : true,
			            scale: 'small',	                     
			            handler: function() {
			               form = this.up('form').getForm();
			               var info = this.up('form').down('#info');
			               loc_win = this.up('window');
			               if (form.isValid()){	
			               
									Ext.Ajax.request({
		 						        url        : 'acs_anag_art_get_formati.php?fn=exe_save_form',
		 						        method     : 'POST',
		 			        			jsonData: {
		 			        				form_values: form.getValues()
		 								},							        
		 						        success : function(result, request){
		 						        	jsonData = Ext.decode(result.responseText);
		 						        	
		 						        	form.reset();
		 						        	info.show();
		 						        	form.findField('data').setValue(jsonData.record.data);
		 						        	form.findField('ora').setValue(jsonData.record.ora);
		 						        	form.findField('utente').setValue(jsonData.record.utente);
		 						        	form.findField('RRN').setValue(jsonData.record.RRN);
                       						form.setValues(jsonData.record[0]);
			 						        
		 			            		},
		 						        failure    : function(result, request){
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });	
							
			                }	                
			            }
			        }
		        ]
                       
                    })
