<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));



if ($_REQUEST['fn'] == 'get_data_puvn'){
    
    $ar = array();
    $m_params = acs_m_params_json_decode();        
    
    if(strlen($m_params->domanda) > 0){
        $where .= " AND TACOR2 = '{$m_params->domanda}'";
    }else{
        $where .= " ";
    }    
    
    $sql = "SELECT *
            FROM {$cfg_mod_Admin['file_tab_sys']}
            WHERE TADT = '$id_ditta_default' AND TAID = 'PUVN' AND TATP <> 'S' AND TALINV=''
            {$where} ORDER BY TANR, TADESC";
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $text = "[" . trim($row['TANR']) . "] " . trim($row['TADESC']);
        
        $segnala_no_valori_ammessi = 'N';
        
        if (isset($m_params->conta_valori_per)){
            $sql_count_va = "SELECT COUNT(*) AS C_ROW
                            FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                            WHERE MODT = '{$id_ditta_default}'
                            AND MOMODE = '{$m_params->conta_valori_per->mode}'
                            AND MOSEQU = '" . substr($row['TANR'], 0, 3) . "{$m_params->conta_valori_per->seq}'
                            AND MOVARI = '{$m_params->conta_valori_per->var}'
                    ";

            
            $stmt_count_va = db2_prepare($conn, $sql_count_va);
            db2_execute($stmt_count_va);
            $row_count_va = db2_fetch_assoc($stmt_count_va);
            
            if ($row_count_va['C_ROW'] == 0)
                $segnala_no_valori_ammessi = 'Y';
        }
        
        
 
        
        $ret[] = array( "id" 	=> sprintf("%-3s", trim($row['TANR'])),
                        "text" 	=> acs_u8e($text),
                        "segnala_no_valori_ammessi"  => $segnala_no_valori_ammessi
        );
    }
    
    
    
    
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    
    ?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll: true,
	            title: '',
				items: [
						{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_puvn', 
								   method: 'POST',								
								   type: 'ajax',

							        actionMethods: {
							          read: 'POST'
							        },							        
							        
							        extraParams: {
									  domanda: <?php echo j($m_params->dom) ?>,
									  conta_valori_per: <?php echo j($m_params->conta_valori_per) ?>,
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['id', 'text', 'segnala_no_valori_ammessi']							
									
			}, //store
			
			      columns: [	
			
	                {
	                header   : 'Varianti',
	                dataIndex: 'text',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                }
	                
	         ], listeners: {
	         
	         
	          celldblclick: {								
				   fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  var record = iView.getRecord(iRowEl);
					  col_name = iView.getGridColumns()[iColIdx].dataIndex;			
					  var loc_win = this.up('window');
					  
					  if(col_name == 'text')
					      loc_win.fireEvent('afterSelect', loc_win, record.get('id'));	
					  
						
							  							  
							 
				  }
			   	}
	         
	         
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					        
								if(record.get('segnala_no_valori_ammessi') == 'Y'){
            		           		return ' colora_riga_rosso';
                    		 	}					        
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}