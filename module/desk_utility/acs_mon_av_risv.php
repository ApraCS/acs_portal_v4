<?php

require_once("../../config.inc.php");

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

function sum_columns_value(&$ar_r, $r){
    $ar_r['sequenza'] = max($ar_r['sequenza'], $r['WGSRIGA']);
    return $ar;
}

function get_procedure_wgs(){
    global $conn, $cfg_mod_DeskUtility, $id_ditta_default;
    $ar = array();
    
    $sql = "SELECT DISTINCT WGSPROC
            FROM {$cfg_mod_DeskUtility['file_monitoraggio']} WS
 			WHERE WGSDT = '{$id_ditta_default}' AND WGSPROC <> ''
 			ORDER BY WGSPROC
 			";
    
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    while ($row = db2_fetch_assoc($stmt)){
        $ar[] = array("id"=> trim($row['WGSPROC']), "text" => trim($row['WGSPROC']));
    }
    return $ar;
}

function get_utenti_wgs(){
    global $conn, $cfg_mod_DeskUtility, $id_ditta_default;
 
   $ar = array();
    
    $sql = "SELECT DISTINCT WGSUSGE
            FROM {$cfg_mod_DeskUtility['file_monitoraggio']} WS
 			WHERE WGSDT = '{$id_ditta_default}' 
 			ORDER BY WGSUSGE
 			";
 
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    while ($row = db2_fetch_assoc($stmt)){
        $ar[] = array("id"=>trim($row['WGSUSGE']) , "text" => trim($row['WGSUSGE']));
    }
    return $ar;
}

// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
    $form_values = $m_params->open_request->form_values;
    $sql_where = "";
    
    if (strlen($form_values->f_data_in) > 0)
        $sql_where .= " AND WGSDTGE >= {$form_values->f_data_in}";
    if (strlen($form_values->f_data_fin) > 0)
        $sql_where .= " AND WGSDTGE <= {$form_values->f_data_fin}";
    
    if(isset($form_values->f_utente) && count($form_values->f_utente) > 0)
        $sql_where .= sql_where_by_combo_value('WGSUSGE', $form_values->f_utente);
    if(isset($form_values->f_procedura) && count($form_values->f_procedura) > 0)
        $sql_where .= sql_where_by_combo_value('WGSPROC', $form_values->f_procedura);
     
    $sql = "SELECT WS.*, TA.TADESC AS D_PROC
            FROM {$cfg_mod_DeskUtility['file_monitoraggio']} WS
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tabelle']} TA
              ON TA.TADT = WS.WGSDT AND TA.TATAID = 'PGMDS' AND TA.TAKEY1 = WS.WGSPROC
            WHERE WGSDT = '{$id_ditta_default}' {$sql_where}
            ORDER BY WGSDTGE DESC, WGSPROG";
  
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
            
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
                
        //stacco dei livelli
        $cod_liv0 = trim($row['WGSDTGE']);
        $cod_liv1 = trim($row['WGSPROG']);
        $cod_liv2 = trim($row['WGSRIGA']);
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //DATA
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = print_date($row['WGSDTGE']);
            $ar_new['liv'] = 'liv_1';
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        
        //SESSIONE
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            
            $ar_new['id']     = implode("|", $tmp_ar_id);
            $ar_new['task']   = "Sessione ".$row['WGSPROG'];
            $ar_new['ora']    = $row['WGSORGE'];
            $ar_new['utente'] = $row['WGSUSGE'];
            $ar_new['ws']     = $row['WGSWSGE'];
            $ar_new['procedura'] = $row['WGSPROC'];
            $ar_new['d_proc'] = trim($row['D_PROC']);
            $ar_new['liv'] = 'liv_2';
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //SEQUENZA
        $liv=$cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = $row['WGSRIGA'];
            $ar_new['liv'] = 'liv_3';
            $ar_new['ordine'] = implode('_', array($row['WGSAADO'], sprintf("%06s", $row['WGSNRDO']), $row['WGSTPDO']));
            $ar_new['k_ordine'] = implode('_', array($row['WGSDT'], $row['WGSTIDO'], $row['WGSINUM'], $row['WGSAADO'], sprintf("%06s", $row['WGSNRDO'])));
            $ar_new['stato'] = $row['WGSSTAT'];
            $ar_new['ora_inizio'] = $row['WGSORGI'];
            $ar_new['ora_fine']   = $row['WGSORGF'];
            $ar_new['data_fine']  = $row['WGSDTGF'];
            $ar_new['note'] = $row['WGSNOTE'];
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
        }
        
    }
  
            
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}



if ($_REQUEST['fn'] == 'open_panel'){

?>

{"success":true, 

m_win: {
		title: 'Monitoraggio avanzamento risviluppo ordini',
		width: 1100, height: 600,
		iconCls: 'icon-button_blue_play-16'
	}, 

"items": [

        {
        xtype: 'treepanel' ,
        stateful: true,
        stateId: 'righe-ordine-order-entry',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,            
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['id', 'task', 'liv', 'ora', 'utente', 'ws', 'procedura', 'd_proc', 'sequenza', 'ordine', 'k_ordine', 'stato', 'ora_inizio', 'ora_fine', 'data_fine', 'note'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
					 extraParams: {
                                open_request: <?php echo acs_je($m_params) ?>
                           
                            }
                    , doRequest: personalizza_extraParams_to_jsonData  ,     
						reader: {
                            root: 'children'
                        }        				
                    }

                }),
    	    			
            columns: [{xtype: 'treecolumn', 
        	    		text: 'Avvio risviluppo/Sessione/Sequenza', 	
        	    		width: 230,
        	    		dataIndex: 'task',
        	    		renderer: function(value, p, record){
		    			  if(record.get('liv') == 'liv_2') return value + ' [' + record.get('sequenza') + ']';
		    			  return value;
		    		    }
        	    		},
        	    		{text: 'Ora', width: 50, dataIndex: 'ora', renderer : time_from_AS},	
	 					{text: 'Utente', width: 70, dataIndex: 'utente'},	
	 					{text: 'WS', width: 40, dataIndex: 'ws'},	
	 					{text: 'Procedura', width: 80, dataIndex: 'procedura'},	
	 					{text: 'Descrizione', flex: 1, dataIndex: 'd_proc'},	
	 					{text: 'Ordine', width: 110, dataIndex: 'ordine'},	
	 					{text: 'St.', width: 40, dataIndex: 'stato'},
	 					{text: 'Ora inizio', width: 60, dataIndex: 'ora_inizio', renderer : time_from_AS},
	 					{text: 'Data fine', width: 60, dataIndex: 'data_fine', renderer : date_from_AS},
	 					{text: 'Ora fine', width: 60, dataIndex: 'ora_fine', renderer : time_from_AS},
	 					{text: 'Note', flex: 1, dataIndex: 'note'},
        	    		
    	    ],
    	     listeners: {
    	     
 					beforestaterestore: function(comp, state){
                        //ESCLUDO SEMPRE LA MEMORIZZAZIONE DALLA STATEEVENTS
                        delete state.sort;
                    },    	     
    	     
	                beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			            
	                  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							 rec = iView.getRecord(iRowEl);							  
							 col_name = iView.getGridColumns()[iColIdx].dataIndex;
							 iEvent.stopEvent();
							 if(rec.get('liv') == 'liv_3' && col_name == 'ordine'){
							      acs_show_win_std('Dettaglio ordini', '../desk_vend/acs_win_dettaglio_ordine.php?fn=open_win', 
	    			  		      {k_ordine : rec.get('k_ordine')}, 400, 550, null, 'icon-leaf-16');	            		
							 }
							
						  }
			   		  	},
			   		  	
			   		  	  itemcontextmenu : function(grid, rec, node, index, event) 
	         		      { event.stopEvent();
	         		        var voci_menu = [];
	         		        
	         		        if(rec.get('liv') == 'liv_3'){
	         		        voci_menu.push(
					          { text: 'Visualizza righe'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function() 
					              { acs_show_win_std( null
			                               , '../desk_utility/acs_get_order_rows_gest_order_entry.php'
			                               , { from_righe_info: 'Y'
			                                 , from_anag_art : 'Y'
			                                 , k_ordine: rec.get('k_ordine')
			                                 //, riga : rec.get('riga')
			                                 , only_view: 'Y'	//ToDo: solo per gli ordini creati da qui!
			                                 }); 
					                                 
					                                     		
		        		          }
		        		         
		    		          }
		    		        );	
		    		        
		    		          voci_menu.push(
					          { text: 'Visualizza packing list'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function()  {
					                show_win_colli_ord(rec, rec.get('k_ordine'));
					                          		
		        		          }
		    		          }
		    		        );
		    		        
		    		        
		    		         voci_menu.push(
    					          { text: 'Elenco esploso distinta componenti'
    					          , iconCls : 'icon-leaf-16'
    					          , handler: function()  {
    					          		acs_show_win_std('Riepilogo articoli/componenti ', '../desk_utility/acs_elenco_riepilogo_art_comp.php?fn=open_elenco', {k_ordine: rec.get('k_ordine'), elenco : 'Y'}, 1300, 450, {}, 'icon-leaf-16');   	     	          		
    		        		          }
    		    		          }
    		    		        );	
    		    		    
    		    		    }
    		    		        
    		    		        var menu = new Ext.menu.Menu({ items: voci_menu }).showAt(event.xy);
		    		        
	         		        
	         		        }
				  
				 },
				 
				 viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('flag') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }	
    	    
    	    
    	}	
	 	
	]  	
  }


<?php 
exit;
}
?>

{"success":true, 
m_win: {
		title: 'Parametri ' + <?php echo j($m_params->d_funz)?>,
		width: 500, height: 200,
		iconCls: 'icon-button_blue_play-16'
	}, 
"items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	                { 
		xtype: 'fieldcontainer',
		flex:1,
		anchor: '-15',		
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'
				    },						
		items: [
		 
        	    {
        	    xtype: 'datefield',
        		name: 'f_data_in',
        		fieldLabel : 'Data elaborazione da',
        		startDay: 1 ,
        	    format: 'd/m/Y',
        	    submitFormat: 'Ymd',
        	    labelWidth : 120,
        	    flex:1.5,
        	    allowBlank : false,
        	    value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'		
        	   },  {
        	    xtype: 'datefield',
        		name: 'f_data_fin',
        		labelAlign : 'right',
        		fieldLabel : 'a',
        		labelWidth : 30,
        		startDay: 1 ,
        		format: 'd/m/Y',
        		anchor: '-15',	
        	    submitFormat: 'Ymd',
        	    flex:1,	
        	    allowBlank : false,
        	    value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'					
        	   }]},
        	   
        	   {
	            xtype: 'combo',
	            store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     <?php echo acs_ar_to_select_json(get_utenti_wgs(), '') ?> 	
					    ] 
					},
	            displayField: 'text',
	            valueField: 'id',
	            fieldLabel: 'Utente',
	            labelWidth : 120,
	            queryMode: 'local',
	            selectOnTab: false,
	            name: 'f_utente',
	            anchor: '-15',	
				forceSelection: true,	
				multiSelection : true		            
	        },
	        
	        	   {
	            xtype: 'combo',
	            store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [								    
					     <?php echo acs_ar_to_select_json(get_procedure_wgs(), '') ?> 	
					    ] 
					},
	            displayField: 'text',
	            valueField: 'id',
	            fieldLabel: 'Procedura',
	            labelWidth : 120,
	            queryMode: 'local',
	            selectOnTab: false,
	            name: 'f_procedura',
	            anchor: '-15',	
				forceSelection: true,	
				multiSelect : true		            
	        }
	            ],
	            
	            
				buttons: [	
				  {
			            text: 'Visualizza',
				        scale: 'large',
                        iconCls: 'icon-windows-32',		            
			            handler: function() {
			             var form = this.up('form').getForm();
			             if (form.isValid()){	
			                	acs_show_win_std(null, 'acs_mon_av_risv.php?fn=open_panel', {
	        				 			form_values: form.getValues()
	        				 		 });
	        				 		 this.up('window').destroy();
        				 		 }
			               
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}

