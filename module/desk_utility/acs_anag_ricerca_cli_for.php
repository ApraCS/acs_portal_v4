<?php

require_once("../../config.inc.php");
$main_module = new DeskArt();

$m_params = acs_m_params_json_decode();

$all_params = array();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());

// ******************************************************************************************
// FORM APERTURA per ricerca anagrafica clienti/fornitori
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
	?>

{"success":true, "items": [

  { xtype: 'form'
  , bodyStyle: 'padding: 10px'
  , bodyPadding: '5 5 0'
  , frame: true
  , title: ''
  , flex: 1
  , layout:
      { type: 'vbox'
      , align: 'stretch'
      }
  , autoScroll: true

  , items: [
      { xtype: 'fieldcontainer'
	  , layout:
	      {	type: 'hbox'
  		  , pack: 'start'
    	  }
      , defaults:
          { labelWidth: 90
          , margin: '0 15 0 0'}
		  , items: [
  		      { name: 'f_codice'
  			  , xtype: 'textfield'
  			  , fieldLabel: 'Codice'
  			  , displayField: 'text'
  			  , labelWidth: 45
  			  , width: 130
  			  }
            , { name: 'f_ragsoc'
  		      , xtype: 'textfield'
  		      , fieldLabel: 'Ragione sociale'
  		      , displayField: 'text'
  		      , labelWidth: 90
  			  , width: 300
              }
              
            , { xtype: 'button'	
			  , text: 'Ricerca'
			  , margin: "0 10 10 10"
			  , anchor: '-15'
			  , width: 50
			  , handler: function() 
			      { l_form = this.up('form').getForm();
            		l_grid = this.up('window').down('grid');
					l_grid.store.proxy.extraParams.form_values = l_form.getValues();
            		l_grid.store.load();
            		l_grid.getView().refresh();
            		            			   
	              }  
	          }
            ] 
      } <!-- end fieldcontainer -->
      
	, { xtype: 'grid'
      , loadMask: true
      , store:
          { xtype: 'store'
          , autoLoad: false	
          , proxy:
              { url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_ricerca_anagrafica'
              , method: 'POST'
              , type: 'ajax'
              , extraParams: 
                  { form_values: ''
                  , cf: '<?php echo $m_params->cf ?>'}
              , doRequest: personalizza_extraParams_to_jsonData
              , actionMethods: { read: 'POST'}
	          , reader: 
	              { type: 'json'
	              , method: 'POST'
	              , root: 'root'
	              } <!-- end reader -->			
              } <!-- end proxy -->	
          , fields: ['CFCD', 'CFRGS1', 'CFFLG3']
          } <!-- end store -->
      , columns: [
      <?php  $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
         {text: '<?php echo $sosp; ?>', width: 40, dataIndex: 'CFFLG3', 
		  tooltip: 'Sospeso',
	      renderer: function(value, metaData, record){
  			  if(record.get('CFFLG3') == 'S') return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	   
		  }},
          { header: 'Codice'
          , dataIndex: 'CFCD'
          , width: 95
          }
        , { header: 'Ragione sociale'
          , dataIndex: 'CFRGS1'
          , flex: 2
          }
        ]	
      , listeners: 
          { celldblclick: {
                fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		        rec = iView.getRecord(iRowEl);
		       
		        var loc_win = this.up('window');
		       
		        if(rec.get('CFFLG3') == 'S'){
		        	acs_show_msg_error('Selezionare fornitori non sospesi');
    				return false;
		        }else{
		        	loc_win.fireEvent('afterSel', loc_win, rec.data);
		        }
		        
		        
		      }		
          }  
          }    
      } <!-- end grid -->		      
	      
	]
  } <!-- end form -->

]}		

<?php exit; }
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
if ($_REQUEST['fn'] == 'get_ricerca_anagrafica'){
    global $cfg_mod_DeskArt, $id_ditta_default;
	
	$cf = $m_params->cf;
	$cf_codice = $m_params->form_values->f_codice; 
	$f_ragsoc  = $m_params->form_values->f_ragsoc;
	$sql_where = '';
	if (isset($f_codice) && strlen($f_codice)>0) $sql_where .= "and CFCD like '%".$f_codice."%'";
	if (isset($f_ragsoc) && strlen($f_ragsoc)>0) $sql_where .= "and CFRGS1 like '%".strtoupper($f_ragsoc)."%'";
	$ret = array();
	$sql = "Select CFCD, CFRGS1, CFFLG3 from {$cfg_mod_DeskArt['file_anag_cli']}
	   where CFDT = '{$id_ditta_default}' and CFTICF = '{$cf}' {$sql_where}
	   fetch first 100 rows only";
	//print_r($sql); exit;
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	while ($r = db2_fetch_assoc($stmt)){
	    $r['CFFLG3'] = $r['CFFLG3'];
	    $ret[] = $r;
	    //print_r($r); exit;
	}
	
	echo acs_je($ret);
exit; }
