<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));



require_once 'utility/extjs_php_functions/components/config_art_seq.php';
$c_config_seq = new CompConfigArtSeq('seq_config', array(
    'f_seq' => 'seq_cfg',
    'f_tip' => 'tp_seq',
    'f_rig' => 'riga' ));



// ******************************************************************************************
// EXE SAVE CONFIG
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_config'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
    
    $ar_upd['MDUSUM'] 	= $auth->get_user();
    $ar_upd['MDDTUM'] 	= oggi_AS_date();
    $ar_upd['MDDTUM'] 	= oggi_AS_date();
   
    $ar_upd['MDVAL1']   = $form_values->valore;

    if (isset($form_values->t_val)) //?????????
        $ar_upd['MDTPV1']   = $form_values->t_val;
        
    
    $ar_upd['MDNOTE']   = $form_values->note;
    
    if (isset($form_values->cfg_val)){   
        $ar_upd['MDVAM2']   = $form_values->cfg_val;
        if(trim($form_values->cfg_val) != '')
            $ar_upd['MDTPM2']   = "=";
        else
            $ar_upd['MDTPM2']   = "";
   }
    if (isset($form_values->seq_cfg))   $ar_upd['MDVAM3']   = $form_values->seq_cfg;
    if (isset($form_values->tp_seq))    $ar_upd['MDTPM3']   = $form_values->tp_seq;
    if (isset($form_values->riga))      $ar_upd['MDVAM4']   = $form_values->riga;
    
    
    
    if($m_params->cond == 'Y'){
        $ar_upd['MDSEQC']   = $form_values->cond;
        $ar_upd['MDSWIE']   = $form_values->swie;
        $ar_upd['MDASSU']   = $form_values->valore;
        for($i = 1; $i <= 12 ; $i ++){
            $var = "MDVAR{$i}";
            $val = "MDVAL{$i}";
            $tp =  "MDTPV{$i}";
            $os =  "MDOPV{$i}";
            if(strlen($form_values->$var) > 0)
                $ar_upd["MDVAR{$i}"]   = $form_values->$var;
                else $ar_upd["MDVAR{$i}"] = '';
            if(strlen($form_values->$val) > 0)
                $ar_upd["MDVAL{$i}"]   = $form_values->$val;
                else $ar_upd["MDVAL{$i}"] = '';
            if(strlen($form_values->$os) > 0)
                $ar_upd["MDOPV{$i}"]   = $form_values->$os;
                else $ar_upd["MDOPV{$i}"] = '';
            if(strlen($form_values->$tp) > 0)
                $ar_upd["MDTPV{$i}"]   = $form_values->$tp;
                else $ar_upd["MDTPV{$i}"] = '';
        }
        
    }else{
        $ar_upd['MDVARI']   = $form_values->mdvari;
        $ar_upd['MDTIPO']   = $form_values->tipo;
        $ar_upd['MDVAM1']   = $form_values->val_min;
        $ar_upd['MDVAM5']   = $form_values->val_max;
        //$ar_upd['MDSEQC']   = '999';
        $ar_upd['MDSEQ3']   = $form_values->seq3;
        $ar_upd['MDSEQ4']   = trim($form_values->seq4);
        
    }
 
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_config']} MD
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    WHERE RRN(MD) = '{$form_values->rrn}' ";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
// ******************************************************************************************
// EXE AGGIUNGI LISTINO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi_config'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    
    $ar_ins = array();
    $ar_ins['MDUSUM'] 	= $auth->get_user();
    $ar_ins['MDDTUM'] 	= oggi_AS_date();
    $ar_ins['MDUSGE'] 	= $auth->get_user();
    $ar_ins['MDDTGE'] 	= oggi_AS_date();
    $ar_ins['MDDT'] 	= $id_ditta_default;
    $ar_ins['MDVAL1']   = trim($form_values->valore);
    if (isset($form_values->t_val)) //?????????
        $ar_ins['MDTPV1']   = $form_values->t_val;
    
    $ar_ins['MDNOTE']   = trim($form_values->note);
    
    if (isset($form_values->cfg_val)){
        $ar_ins['MDVAM2']   = $form_values->cfg_val;
        if(trim($form_values->cfg_val) != '')
            $ar_ins['MDTPM2']   = "=";
        else
            $ar_ins['MDTPM2']   = "";
        
    }
    if (isset($form_values->seq_cfg))   $ar_ins['MDVAM3']   = $form_values->seq_cfg;
    if (isset($form_values->tp_seq))    $ar_ins['MDTPM3']   = $form_values->tp_seq;
    if (isset($form_values->riga))      $ar_ins['MDVAM4']   = $form_values->riga;
    
    if($m_params->cond == 'Y'){
        $ar_ins['MDMODE'] 	= $m_params->record->mode;
        $ar_ins['MDVARI']   = $m_params->record->mdvari;
        $ar_ins['MDSEQ3']   = $m_params->record->seq3;
        $ar_ins['MDSEQ4']   = $m_params->record->seq4;
        $ar_ins['MDSEQC']   = trim($form_values->cond);
        $ar_ins['MDSWIE']   = trim($form_values->swie);
        $ar_ins['MDASSU']   = trim($form_values->valore);
        for($i = 1; $i <= 12 ; $i ++){
            $var = "MDVAR{$i}";
            $val = "MDVAL{$i}";
            $tp =  "MDTPV{$i}";
            $os =  "MDOPV{$i}";
        if(strlen($form_values->$var) > 0)
            $ar_ins["MDVAR{$i}"]   = $form_values->$var;
        if(strlen($form_values->$val) > 0)
            $ar_ins["MDVAL{$i}"]   = $form_values->$val;
        if(strlen($form_values->$os) > 0)
            $ar_ins["MDOPV{$i}"]   = $form_values->$os;
        if(strlen($form_values->$tp) > 0)
            $ar_ins["MDTPV{$i}"]   = $form_values->$tp;
        }
        
       
    }else{
        $ar_ins['MDMODE'] 	= $m_params->mode;
        $ar_ins['MDSEQC']   = '999';
        $ar_ins['MDVARI']   = $form_values->mdvari;
        $ar_ins['MDSEQ3']   = $form_values->seq3;
        $ar_ins['MDSEQ4']   = trim($form_values->seq4);
        $ar_ins['MDTIPO']   = trim($form_values->tipo);
        $ar_ins['MDVAM1']   = $form_values->val_min;
        $ar_ins['MDVAM5']   = $form_values->val_max;
    }
    
    
    
    if (isset($form_values->seq_cfg))   $ar_ins['MDVAM3']   = $form_values->seq_cfg;
    if (isset($form_values->tp_seq))    $ar_ins['MDTPM3']   = $form_values->tp_seq;
    if (isset($form_values->riga))      $ar_ins['MDVAM4']   = $form_values->riga;
  
    
    
    $sql_c = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_DeskUtility['file_config']} MD
            WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$ar_ins['MDMODE']}'
            AND MDSEQ3 = '{$ar_ins['MDSEQ3']}' AND MDSEQ4 = '{$ar_ins['MDSEQ4']}'
            AND MDVARI = '{$ar_ins['MDVARI']}' AND MDSEQC='{$ar_ins['MDSEQC']}'";
    
    
    $stmt_c = db2_prepare($conn, $sql_c);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_c);
    $row_c = db2_fetch_assoc($stmt_c);
    if($row_c['C_ROW'] > 0){
        $ret['success'] = false;
        $ret['msg_error'] = "Riga gi&agrave; esistente";
        echo acs_je($ret);
        return;
        
    }
    
    
    
    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_config']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
                            
                            
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
// ******************************************************************************************
// EXE DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    if($m_params->from_cond == 'Y'){
        
        $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_config']} MD
        WHERE RRN(MD) = '{$form_values->rrn}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $error_msg =  db2_stmt_errormsg($stmt);
        
        
    }else{
                
        $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_config']} MD
        WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$m_params->mode}'
        AND MDSEQ3 = '{$form_values->seq3}' AND MDSEQ4 = '{$form_values->seq4}'
        AND MDVARI = '{$form_values->mdvari}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $error_msg =  db2_stmt_errormsg($stmt);
               
        if($form_values->c_mosequ > 0){
                $mosequ = $form_values->seq3.$form_values->seq4;
                
                $sql_va = "DELETE FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                           WHERE MODT = '{$id_ditta_default}' AND MOMODE = '{$m_params->mode}'
                           AND MOSEQU = '{$mosequ}'
                           AND MOVARI = '{$form_values->mdvari}'";
                
                
                
                $stmt_va = db2_prepare($conn, $sql_va);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_va);
                $error_msg =  db2_stmt_errormsg($stmt_va);
        }
        
    }
        
    
    
    
    $ret = array();
    $ret['success'] = $result;
    if (strlen($error_msg) > 0) {
        $ret['success'] = false;
        $ret['message'] = $error_msg;
    }
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
// ******************************************************************************************
    
    $m_params = acs_m_params_json_decode();
    
    $sql_where = "";
    $mode = $m_params->open_request->mode;
    
    if($m_params->open_request->totalizza == 'Y'){
        
        $sql = "SELECT COUNT(*) AS TOT, MDVARI, TA.TADESC 
                FROM {$cfg_mod_DeskUtility['file_config']} MD
                LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                ON TA.TADT = MD.MDDT AND TA.TAID = 'PUVR' AND TA.TANR = MD.MDVARI AND TALINV = ''
                WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$mode}' AND MDSEQC='999'
                GROUP BY MDVARI, TA.TADESC ORDER BY TA.TADESC";
        
        $sql_du = "SELECT MDDTGE, MDUSGE
                   FROM {$cfg_mod_DeskUtility['file_config']} MD
                   WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$mode}' AND MDSEQC='999'
                   AND MDVARI = ?
                   ORDER BY MDDTGE DESC";
        
        $stmt_du = db2_prepare($conn, $sql_du);
        echo db2_stmt_errormsg();
        
    }else{
        
        $sql_where_md2 = "";
        if($m_params->open_request->from_puvn == 'Y'){
            
            $variabile = $m_params->open_request->variabile;
            $variante = $m_params->open_request->variante;
            
            $sql_where_md2 .= " AND (";
            for($i = 1; $i <= 12 ; $i++)
               $sql_where_md2 .= " (MD2.MDVAR{$i} = '{$variabile}' AND MD2.MDVAL{$i} = '{$variante}' AND MD2.MDTPV{$i} <> 'T') OR";
            $sql_where_md2 .= " 1=2 )";
            
            $sql_where .= " AND (
                            (MD.MDVARI = '{$variabile}' AND MD.MDVAL1 = '{$variante}')
                            OR(MD.MDVARI ='{$variabile}' AND MD.MDASSU = '{$variante}')
                            OR(MO.MOVARI ='{$variabile}' AND MO.MOVALO = '{$variante}')
                            OR MD_C.C_ROW > 0
                           ) ";
            
        }elseif($m_params->open_request->from_var == 'Y'){
            
            $variabile = $m_params->open_request->variabile;
            
            $sql_where_md2 .= " AND (";
            for($i = 1; $i <= 12 ; $i++)
                $sql_where_md2 .= " (MD2.MDVAR{$i} = '{$variabile}' AND MD2.MDTPV{$i} <> 'T') OR";
                $sql_where_md2 .= " 1=2 )";
                
                $sql_where .= " AND (
                (MD.MDVARI = '{$variabile}')
                OR(MD.MDVARI ='{$variabile}')
                OR(MO.MOVARI ='{$variabile}')
                OR MD_C.C_ROW > 0
                ) ";
    
            
        }else{ 
           
            $sql_where_md2 .= " AND MD2.MDMODE = '{$mode}'";
            $sql_where .= "  AND MD.MDSEQC='999' AND MD.MDMODE = '{$mode}'";
        
        }
            
            
       
        $sql = "SELECT MAX(RRN(MD)) AS RRN, MD.MDMODE, MD.MDSEQ3, MD.MDSEQ4, MD.MDVARI, MDTIPO, MDVAL1, MDTPV1, MDNOTE,
                MOSEQU, COUNT(MOSEQU) AS C_MOSEQU,  TA.TADESC AS D_MODE, MDDTGE, MDUSGE, MDVAM1, MDVAM5,
                MDVAM2, MDTPM2, MDVAM3, MDTPM3, MDVAM4, MD_C.C_ROW AS COND 
                FROM {$cfg_mod_DeskUtility['file_config']} MD
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                  ON MO.MODT=MD.MDDT AND MO.MOMODE=MD.MDMODE AND MO.MOSEQU = CONCAT (MD.MDSEQ3, MD.MDSEQ4) AND MO.MOVARI=MD.MDVARI
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA
                  ON TA.TADT = MD.MDDT AND TA.TANR = MD.MDMODE AND TA.TAID = 'PUMO'       
                LEFT OUTER JOIN (
                  SELECT COUNT(*) AS C_ROW, MD2.MDVARI, MD2.MDSEQ3, MD2.MDSEQ4, MD2.MDMODE, MD2.MDDT
                  FROM {$cfg_mod_DeskUtility['file_config']} MD2
                  WHERE MD2.MDDT = '{$id_ditta_default}' AND MD2.MDSEQC <> '999' {$sql_where_md2}
                  GROUP BY MD2.MDVARI, MD2.MDSEQ3, MD2.MDSEQ4, MD2.MDMODE, MD2.MDDT
                ) MD_C
                ON MD.MDDT= MD_C.MDDT AND MD.MDMODE = MD_C.MDMODE AND MD.MDVARI = MD_C.MDVARI AND MD.MDSEQ3 = MD_C.MDSEQ3 AND MD.MDSEQ4 = MD_C.MDSEQ4
                WHERE MD.MDDT = '{$id_ditta_default}'
                {$sql_where}
                GROUP BY MD.MDMODE, MD.MDSEQ3, MD.MDSEQ4, MD.MDVARI, MDTIPO, MDVAL1, MDTPV1, MDNOTE, MOSEQU,
                MDDTGE, MDUSGE, TA.TADESC, MDVAM1, MDVAM5, MDVAM2, MDTPM2, MDVAM3, MDTPM3, MDVAM4, MD_C.C_ROW 
                ORDER BY MDSEQ3, MDSEQ4, MDVARI";
        
        
    }
    
   
    $sql_t = "SELECT COUNT(DISTINCT MDVARI) AS C_VAR, COUNT(DISTINCT CONCAT(MDSEQ3, MDSEQ4)) AS C_SEQ
              FROM {$cfg_mod_DeskUtility['file_config']} MD
              WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$mode}' AND MDSEQC='999'";
    
    
    $stmt_t = db2_prepare($conn, $sql_t);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_t);
    $row_t = db2_fetch_assoc($stmt_t);
    

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
  
       
        if(trim($row['MDSEQ4']) != '')
            $nr['seq']    = $row['MDSEQ3']." - ".$row['MDSEQ4'];
        else
            $nr['seq'] = $row['MDSEQ3'];
        $nr['seq3']    = $row['MDSEQ3'];
        $nr['seq4']    = $row['MDSEQ4'];
        $val_dom = find_TA_sys('PUVR', trim($row['MDVARI']));
        $var = "[".trim($row['MDVARI'])."]";
        $nr['var']    = $var;
        $nr['denominazione']  = $val_dom[0]['text'];
        $nr['tipo']   = trim($row['MDTIPO']);
        $nr['valore'] = trim($row['MDVAL1']);
        $nr['t_val'] = $row['MDTPV1'];
        $nr['c_mosequ'] = $row['C_MOSEQU'];
        if(is_null($row['MOSEQU']))
            $nr['mosequ'] =  $row['MDSEQ3'].$row['MDSEQ4'];
        else
            $nr['mosequ'] = $row['MOSEQU'];
        $nr['mode'] = $row['MDMODE'];
        $nr['mdvari'] = trim($row['MDVARI']);
        $nr['note'] = trim($row['MDNOTE']);
        $nr['rrn'] = $row['RRN'];
        $nr['tot'] = $row['TOT'];
        $nr['c_var'] = $row_t['C_VAR'];
        $nr['c_seq'] =  $row_t['C_SEQ'];
                
        $nr['d_mode'] = $row['D_MODE'];
        $nr['val_min'] = trim($row['MDVAM1']);
        $nr['val_max'] = trim($row['MDVAM5']);
        $nr['cfg_val'] = trim($row['MDVAM2']);
        $nr['tp_cfg_val'] = trim($row['MDTPM2']);
        

        
        $nr['seq_mo']    = $row['MDSEQ3'].$row['MDSEQ4'];
        $nr['cond']      = $row['COND'];
        
        //seq. configurazione (gestito da component config_art_seq)
        $nr['seq_cfg'] = trim($row['MDVAM3']);
        $nr['tp_seq']  = trim($row['MDTPM3']);
        $nr['riga']    = trim($row['MDVAM4']);
        $nr = $c_config_seq->prepare_row($nr);   //imposta eventuali campi necessari poi in visualizzazione (come la decod)
        
        
        //Manutenzione automatica campo "Tipo"
        //Se ho "S" ---- Rimane "S"
        //Altrimenti imposto "C" se ho condizioni o blank se non ne ho
        if (trim($row['MDTIPO']) != 'S'){
            if ($nr['cond'] > 0 && trim($row['MDTIPO']) != 'C'){
                //reimposto 'C'  
                $sql_upd_tipo = "UPDATE {$cfg_mod_DeskUtility['file_config']} SET MDTIPO='C' WHERE RRN({$cfg_mod_DeskUtility['file_config']}) = {$row['RRN']}";
                $stmt_upd_tipo = db2_prepare($conn, $sql_upd_tipo);
                echo db2_stmt_errormsg();
                db2_execute($stmt_upd_tipo);
                $nr['tipo'] = 'C';
            }
            
            if ($nr['cond'] == 0 && trim($row['MDTIPO']) != ''){
                //reimposto blank
                $sql_upd_tipo = "UPDATE {$cfg_mod_DeskUtility['file_config']} SET MDTIPO='' WHERE RRN({$cfg_mod_DeskUtility['file_config']}) = {$row['RRN']}";
                $stmt_upd_tipo = db2_prepare($conn, $sql_upd_tipo);
                echo db2_stmt_errormsg();
                db2_execute($stmt_upd_tipo);
                $nr['tipo'] = '';
            }            
        }
        
        
        
        if($m_params->open_request->totalizza == 'Y'){
        
        $result = db2_execute($stmt_du, array(trim($row['MDVARI'])));
        $tip_data_user = "";
        $ultima_data = "";
        $ultimo_user = "";
        $count_date = 0;
        while($row_du = db2_fetch_assoc($stmt_du)){
                         
            
            if($ultima_data != $row_du['MDDTGE'] || $ultimo_user !=  $row_du['MDUSGE']){
                $tip_data_user .= "Immissione: ".print_date($row_du['MDDTGE']). ", User: {$row_du['MDUSGE']} <br>";
                $count_date++;
            }
                $ultima_data = $row_du['MDDTGE'];
                $ultimo_user = $row_du['MDUSGE'];
                
                              
          
        }
               
       
        $nr['MDDTGE'] = $ultima_data;
        $nr['MDUSGE'] = $ultimo_user;
        $nr['tooltip'] = $tip_data_user;  
        $nr['c_date'] = $count_date;  
        
        }else{
            $nr['MDDTGE'] = $row['MDDTGE'];
            $nr['MDUSGE'] = $row['MDUSGE'];
            
        }
       
        
        $ar[] = $nr;
    }
    
  
    
    echo acs_je($ar);
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid_cond'){
    
    $m_params = acs_m_params_json_decode();
    
    $mode = $m_params->open_request->record->mode;
    $vari = $m_params->open_request->record->mdvari;
    $seq3 = $m_params->open_request->record->seq3;
    $seq4 = $m_params->open_request->record->seq4;
    
    $sql = "SELECT RRN(MD) AS RRN, MD.*
            FROM {$cfg_mod_DeskUtility['file_config']} MD
            WHERE MDDT = '{$id_ditta_default}'  AND MDMODE = '{$mode}' AND MDVARI = '{$vari}'
            AND MDSEQ3 = '{$seq3}' AND MDSEQ4 = '{$seq4}' AND MDSEQC <> '999'";
 
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
  
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        $nr['cond']    = trim($row['MDSEQC']);
        $nr['swie']    = trim($row['MDSWIE']);
        $nr['note']    = trim($row['MDNOTE']);
        //$nr['valore'] = trim($row['MDVAL1']);
        $nr['valore'] = trim($row['MDASSU']);
        $nr['t_val'] = trim($row['MDTPAS']);
        $nr['MDVARI'] = trim($row['MDVARI']);

        $nr['decod'] = decod_dom_ris('sys', $vari, $nr['valore'], $nr['t_val'], array(
                                            'opzioni' => array(
                                                array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*'),
                                                array('id' => '?', 'text' => 'Altra variabile', 'taid' => 'PUVR')
                                            )
                                        ), array('solo_decod' => true));
        $nr['rrn']     = $row['RRN'];
        
        
        for($i = 1; $i <= 12 ; $i++){
            
            $nr["MDVAR{$i}"] = trim($row["MDVAR{$i}"]); //DOMANDA
            $nr["MDVAL{$i}"] = trim($row["MDVAL{$i}"]); //RISPOSTA
            $nr["MDTPV{$i}"] = trim($row["MDTPV{$i}"]);
            $nr["MDOPV{$i}"] = trim($row["MDOPV{$i}"]);
        }
        

        $ar[] = $nr;
        
        
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    $row_new_config = get_TA_sys('PUMO', $m_params->new_mode);
     
    if(isset($m_params->title)){
        $title = $m_params->title;
        $title2 = "Dettagli distinta configuratore";
    }else{
        $title = "Configuratore [{$m_params->mode}]";
        $title2 = "Dettagli distinta configuratore [{$m_params->mode}] {$m_params->d_mode} ";
    }  
    ?>
{"success":true, "items": [

        {
			xtype: 'panel',
		    title: '<?php echo $title; ?>',
        	<?php echo make_tab_closable(); ?>,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				
					{
						xtype: 'grid',
						//title: '<?php echo "Configuratore [{$m_params->mode}] {$m_params->d_mode}"; ?>',
						flex:0.7,
				        loadMask: true,
				        stateful: true,
				        <?php if($m_params->from_puvn == 'Y'){?>
				          multiSelect : true,
				        <?php }?>
        				//stateId: 'panel-configuratore',
        				stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
            			<?php if($m_params->d_cfg == 'Y'){?>
            				selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
            			<?php }?>
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['c_var', 'c_seq', 'note', 'd_mode','tot', 'seq', 'seq3', 'seq4', 'var', 'rrn', 
		        			         'tipo', 'valore', 't_val', 'mosequ', 'c_mosequ', 'mdvari', 'mode', 
		        			         'denominazione', 'MDDTGE', 'MDUSGE', 'val_min', 'val_max', 'c_date',
		        			         'cfg_val', 'tp_cfg_val', 'seq_cfg', 'tp_seq', 'riga', 'cond', 'tooltip',
		        			         'change_var', 'change_seq', 'n_seq3', 'n_seq4', 'n_mdvari',
		        			         <?php echo acs_je($c_config_seq->out_name_field())?>]							
									
			}, //store
			
			      columns: [	
			      
			       <?php if($m_params->from_puvn == 'Y' || $m_params->from_var == 'Y'){?>
	                {
	                header   : 'Mod.',
	                dataIndex: 'mode',
	                filter: {type: 'string'}, filterable: true,
	                 width : 40
	                },
	                <?php } ?>
			      
			       <?php if($m_params->totalizza != 'Y'){?>
	                {
	                header   : 'Sequenza',
	                dataIndex: 'seq',
	                filter: {type: 'string'}, filterable: true,
	                 width : 70
	                },
	                <?php } ?>
	                {
	                header   : 'Cod.',
	                dataIndex: 'var',
	                filter: {type: 'string'}, filterable: true,
	                width : 50
	                },  {
	                header   : 'Variabile',
	                dataIndex: 'denominazione',
	                sortable : true,
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                },  
	                 <?php if($m_params->totalizza != 'Y'){?>
	                
	                {
	                header   : 'Tipo',
	                dataIndex: 'tipo',
	                filter: {type: 'string'}, filterable: true,
	                width : 40,
	                renderer: function(value, p, record){
	    				if (record.get('cond') > 0)
        				     p.tdCls += ' sfondo_rosso';
            		    
            		        return value;
		    		  	}
	         	   },  {
	                header   : 'Valore',
	                dataIndex: 'valore',
	                filter: {type: 'string'}, filterable: true, width : 60,
	                	renderer: function(value, p, record){
    		    			  if(record.get('t_val').trim() != '') 
    		    			  		return record.get('t_val').trim() + ': ' + value;
    		    			  return value;		
    		    		  }
	                },
	                {text: 'VA', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'c_mosequ',
    				tooltip: 'Valori ammessi',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('c_mosequ') > 0) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=15>';
    		    		  }
    		        },
    		        
    		        {text: 'CV', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'tp_cfg_val',
    				tooltip: 'Configuratore valori ammessi',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('tp_cfg_val') == '=') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=15>';
    		    		  }
    		        },
    		        {text: 'Note', 	
    				flex: 1,
    				dataIndex: 'note'        			    	     
    		    
    		        },
    		        <?php } else {?>
    		         {
	                header   : 'Nr ricorrenze',
	                dataIndex: 'tot',
	                width : 90
	                },
    		       
    		        
    		        <?php }?>
    		        
    		        {header: 'Immissione',
 					columns: [
	 				{header: 'Data', dataIndex: 'MDDTGE', renderer: date_from_AS, width: 70, sortable : true,
	 				 renderer: function(value, metaData, record){
            	         if (record.get('c_date') > 1){ 
            	         	metaData.tdCls += ' grassetto';
            	         	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip')) + '"';    			    	
            		     }
            		     return date_from_AS(value);	
            			}
	 				}
	 				,{header: 'Utente', dataIndex: 'MDUSGE', width: 70, sortable : true,
	 				 renderer: function(value, metaData, record){
            	         if (record.get('c_date') > 1){ 
            	         	metaData.tdCls += ' grassetto';
            	         	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip')) + '"';    			    	
            		     }
            		     return value;	
            			}}
	 				]}
	                
	         ] 
	         
	         <?php if($m_params->d_cfg == 'Y'){?>
	         
	         	, bbar: ['->', {
							xtype: 'button',
				            text: 'Conferma',
				            iconCls: 'icon-button_blue_play-32', 
				            scale: 'large',
				            handler: function() {
				            	 var grid = this.up('grid');
				            	 var loc_win = this.up('window');
				                 var records =  grid.getSelectionModel().getSelection();  
				           		 list_selected_id = [];                           
                                 for (var i=0; i<records.length; i++) 
                            	 list_selected_id.push(records[i].data);
                            	
         			          Ext.Ajax.request({
        				        url        : 'acs_anag_art_config_set_values.php?fn=exe_duplica',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				 list_selected_id : list_selected_id,
        	        				 new_mode : <?php echo j($m_params->new_mode); ?>
        						},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					         if(jsonData.success == false){
					      	    		 acs_show_msg_error(jsonData.msg_error);
							        	 return;
				        			 }else{
        					         	loc_win.fireEvent('afterDupCfg', loc_win);
        					         }
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				    });
				           
				      
				            }
				        }]
				        
				 <?php }?>        
	         
	        , listeners: {
	         
	           afterrender: function (comp) {
	           
	           			<?php if($m_params->from_puvn == 'Y' && $m_params->totalizza == 'Y'){
	           			    
	           			    $sql_c = "SELECT COUNT(DISTINCT MDVARI) AS C_VAR, COUNT(DISTINCT CONCAT(MDSEQ3, MDSEQ4)) AS C_SEQ
	           			              FROM {$cfg_mod_DeskUtility['file_config']} MD
	           			              WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$m_params->mode}' AND MDSEQC='999'";
	           			    
	           			    $stmt_c = db2_prepare($conn, $sql_c);
	           			    echo db2_stmt_errormsg();
	           			    $result = db2_execute($stmt_c);
	           			    $row_c = db2_fetch_assoc($stmt_c);
	           			    $c_var = $row_c['C_VAR'];
	           			    $c_seq = $row_c['C_SEQ'];
	                    
	                   ?>
	                  
						comp.up('window').setTitle('<?php echo "Configuratore [{$m_params->mode}] - RIEPILOGO VARIABILI [{$c_var}, {$c_seq}]";?>');	 				
						
						<?php }elseif($m_params->from_duplica == 'Y'){?>
						   comp.up('window').setTitle(comp.up('window').title + <?php echo j($row_new_config['text']); ?>)
						<?php }?> 			
				 			
		 			} ,
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					     id_selected = grid.getSelectionModel().getSelection();
			     		 list_selected_id = [];
			     		 for (var i=0; i<id_selected.length; i++){
			     			list_selected_id.push(id_selected[i].data);
						 }
						 
						 if(list_selected_id.length > 1){
						 
						 	<?php if($m_params->from_puvn == 'Y'){?>
			    	     
        			    	 voci_menu.push({
        			         				text: 'Accoda valore ammesso',
        			        				iconCls : 'icon-info_blue-16',             		
        			        				handler: function () {
        			        				
        			        				 var my_listeners = {
            			    		  			afterConferma: function(from_win, n_valore){
            			    		  			    grid.getStore().load();
            			    		  			   	 /*for (var i=0; i<id_selected.length; i++){
                            			     			id_selected[i].set('valore', n_valore);
                            			     		 }*/
            			    		  			   from_win.close();
            			    		  			 
            			    		  			   }
            				    				};
        			        				
        			        		  		acs_show_win_std('Aggiorna lista valori ammessi', 'acs_anag_art_config_set_values.php?fn=open_form_va', {list_selected_id : list_selected_id}, 450, 150, my_listeners, 'icon-info_blue-16');   	     
            				                }
            			    				});     
        			    	     
        			    	<?php }?>
						 
						 }else{
						 
						 <?php if($m_params->from_puvn == 'Y'){?>
			    	     
        			    	 voci_menu.push({
        			         				text: 'Accoda valore ammesso',
        			        				iconCls : 'icon-info_blue-16',             		
        			        				handler: function () {
        			        				
        			        				 var my_listeners = {
            			    		  			afterConferma: function(from_win, n_valore){
            			    		  			    grid.getStore().load();
            			    		  			   	 /*for (var i=0; i<id_selected.length; i++){
                            			     			id_selected[i].set('valore', n_valore);
                            			     		 }*/
            			    		  			   from_win.close();
            			    		  			 
            			    		  			   }
            				    				};
        			        				
        			        		  		acs_show_win_std('Aggiorna lista valori ammessi', 'acs_anag_art_config_set_values.php?fn=open_form_va', {list_selected_id : list_selected_id}, 450, 150, my_listeners, 'icon-info_blue-16');   	     
            				                }
            			    				});     
        			    	     
        			    	<?php }?>
					     
					     
					       <?php if($m_params->d_cfg == 'Y'){?>
    		    		  voci_menu.push({
        		         		text: 'Sostituzione variabile',
        		        		iconCls : 'icon-pencil-16',          		
        		        		handler: function () {
        		        		
    		        		 		var my_listeners = {
    			    		  			afterSelect: function(from_win, n_value){
    			    		  			  
    			    		  			  var all_rec = grid.getStore().getRange();
    			    		  			  var variabile = rec.get('mdvari').trim();
    			    		  			  for (var i=0; i<all_rec.length; i++) {
    			    		  			   
    			    		  			  	if(all_rec[i].get('mdvari').trim() == rec.get('mdvari').trim()){
    			    		  			  	   all_rec[i].set('change_var', 'Y');
    			    		  			  	}	
    			    		  			  }
    			    		  			  for (var i=0; i<all_rec.length; i++) {
    			    		  			   
    			    		  			  	if(all_rec[i].get('change_var') == 'Y'){
    			    		  			  	  
    			    		  			  	   all_rec[i].set('var',  '[' + n_value.f_variabile + ']');
    			    		  			  	   all_rec[i].set('n_mdvari', n_value.f_variabile);
    			    		  			  	   all_rec[i].set('denominazione', n_value.f_descrizione);
    			    		  			  	}	
    			    		  			  }
    			    		  			  
    			    		  			  
    			    		  				from_win.close();
    			    		  			  }
    				    		   };
        		        		   acs_show_win_std('Sostituzione variabile', 'acs_anag_art_config_set_values.php?fn=open_form_var', {mode: rec.get('mode'), mdvari : rec.get('mdvari'), d_var : rec.get('denominazione')}, 400, 150, my_listeners, 'icon-pencil-16');   	     
        			              
        							}
    		    				});
    		    				
    		    		  voci_menu.push({
        		         		text: 'Sostituzione sequenza',
        		        		iconCls : 'icon-pencil-16',          		
        		        		handler: function () {
		        		   
		        		   var my_listeners = {
    			    		  			afterSelect: function(from_win, n_value){
    			    		  			
    			    		  			var all_rec = grid.getStore().getRange();
    			    		  			for (var i=0; i<all_rec.length; i++) {
    			    		  			   
    			    		  			  	if(all_rec[i].get('seq3').trim() == rec.get('seq3').trim()
    			    		  			  		&& (all_rec[i].get('seq4').trim() == rec.get('seq4').trim()
    			    		  			  		    || all_rec[i].get('seq4').trim() == '')){
    			    		  			  	   all_rec[i].set('change_seq', 'Y');
    			    		  			  	}	
    			    		  			  }
    			    		  			  for (var i=0; i<all_rec.length; i++) {
    			    		  			   
    			    		  			  	if(all_rec[i].get('change_seq') == 'Y'){
    			    		  			  	  
    			    		  			  	   all_rec[i].set('seq', n_value.f_seq3 + ' - ' + n_value.f_seq4);
    			    		  			  	   all_rec[i].set('n_seq3', n_value.f_seq3);
    			    		  			  	   all_rec[i].set('n_seq4', n_value.f_seq4);
    			    		  			  	}	
    			    		  			  }
    			    		  			  
    			    		  				from_win.close();
    			    		  			  }
    				    		   };
		        		   
		        		   			acs_show_win_std('Sostituzione sequenza', 'acs_anag_art_config_set_values.php?fn=open_form_seq', {seq3 : rec.get('seq3'), seq4 : rec.get('seq4')}, 400, 150, my_listeners, 'icon-pencil-16');   	    
														   	     
        			              
        						   }
    		    				});
		    		  <?php }else{?>
		    		  
		    		  
					     
					 	 voci_menu.push({
			         		text: 'Elenco varianti',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		   
					    		//acs_show_panel_std('acs_tab_sys_PUVN.php?fn=open_panel', 'panel_gestione_tabelle', {tacor2: rec.get('mdvari'), d_cod : rec.get('denominazione') });	 
					    	    acs_show_win_std('Elenco varianti', 'acs_tab_sys_PUVN.php?fn=open_panel', {tacor2: rec.get('mdvari'), d_cod : rec.get('denominazione')}, 1000, 400, null, 'icon-design-16');         	                	     
				               
									}
			    				});
			    				
			    				<?php if($m_params->totalizza == 'Y'){?>
			    			voci_menu.push({
    		         		text: 'Ricorrenza valori ammessi',
    		        		iconCls : 'icon-leaf-16',          		
    		        		handler: function () {
    		        		   acs_show_win_std('Configuratore [' + <?php echo j($m_params->mode); ?> + '] - RIEPILOGO VALORI AMMESSI', 'acs_anag_art_config_va.php?fn=open_riepilogo', {mode: <?php echo j($m_params->mode); ?>, var : rec.get('mdvari'), d_var : rec.get('denominazione')}, 600, 400, null, 'icon-design-16');   	     
    			              
    								}
		    				});
		    				<?php }?>
			    				
			    		<?php if($m_params->totalizza != 'Y'){?>		
		    		    voci_menu.push({
    		         		text: 'Ricorrenza variabili',
    		        		iconCls : 'icon-leaf-16',          		
    		        		handler: function () {
    		        		   acs_show_win_std('Configuratore [' + rec.get('mode') + '] - RIEPILOGO VARIABILI' + ' [' + rec.get('c_var') + ', ' + rec.get('c_seq') +']', 'acs_anag_art_config.php?fn=open_tab', {mode: rec.get('mode'), totalizza : 'Y', from_puvn : <?php echo j($m_params->from_puvn); ?>}, 600, 400, null, 'icon-design-16');   	     
    			              
    								}
		    				});
		    			
    					 voci_menu.push({
	         				text: 'Duplica configuratore',
	        				iconCls : 'icon-game_pad-16',             		
	        				handler: function () {
	        				
	        				 var my_listeners = {
		    		  			afterSelect: function(from_win, new_mode){
		    		  			   
		    		  			   from_win.close();
		    		  			   
		    		  			   acs_show_win_std('Duplica da configuratore [' + rec.get('mode')+ '] ' +  <?php echo j($m_params->d_mode); ?> , 'acs_anag_art_config.php?fn=open_tab', {mode: rec.get('mode'), d_cfg : 'Y', new_mode : new_mode, d_mode : <?php echo j($m_params->d_mode); ?> }, 1200, 400
		    		  			   , {afterDupCfg: function(from_win){
    			    		  			 grid.getStore().load();
    			    		  			
    			    		  			   from_win.close();
    			    		  			 }}
		    		  			   , 'icon-game_pad-16');
		    		  			   }
			    				};
	        				
	        		  		acs_show_win_std('Duplica distinta configurazione varianti', 'acs_anag_art_config_set_values.php?fn=open_form', {mode: rec.get('mode'), d_mode : <?php echo j($m_params->d_mode); ?>, d_cfg : 'Y'}, 450, 150, my_listeners, 'icon-game_pad-16');   	     
			                }
		    				});
		    				
		    							
		    			   	voci_menu.push({
		         				text: 'Duplica sequenza',
		        				iconCls : 'icon-pencil-16',             		
		        				handler: function () {
		        				
		        				 var my_listeners = {
			    		  			confirmDuplica : function(from_win, form_values){
			    		  			    var list_selected_id = []
			    		  			    list_selected_id.push(rec.data);
			    		  			     Ext.Ajax.request({
                    				        url        : 'acs_anag_art_config_set_values.php?fn=exe_duplica',
                    				        timeout: 2400000,
                    				        method     : 'POST',
                    	        			jsonData: {
                    	        			     d_seq : 'Y',
                    	        				 list_selected_id : list_selected_id,
                    	        				 form_values : form_values
                    						},							        
                    				        success : function(result, request){
                    					        var jsonData = Ext.decode(result.responseText);
                    					         if(jsonData.success == false){
            					      	    		 acs_show_msg_error(jsonData.msg_error);
            							        	 return;
            				        			 }else{
                    					         	grid.getStore().load();
    			    		  			            from_win.close();
                    					         }
                    				        },
                    				        failure    : function(result, request){
                    				            Ext.Msg.alert('Message', 'No data to be loaded');
                    				        }
                    				    });
			    		  			   
			    		  		
			    		  			   }
				    				};
				    			
		        				acs_show_win_std('Duplica sequenza', 'acs_anag_art_config_set_values.php?fn=open_form_seq', {seq3 : rec.get('seq3'), seq4 : rec.get('seq4'), d_seq : 'Y'}, 400, 200, my_listeners, 'icon-pencil-16');   	    
		        		  		   	     
				                }
			    				});
		    				<?php }?>
		    		
			    	<?php }?>
			    	
			    	}
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	         
	           , celldblclick: {								
				   fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  var rec = iView.getRecord(iRowEl);
					  var grid = this;
					  col_name = iView.getGridColumns()[iColIdx].dataIndex;			
					  if(col_name == 'c_mosequ'){
					  
					  var my_listeners = {
	    		  			afterClose: function(from_win){
	    		  			    grid.getStore().load();
	    		  			    from_win.close();
				        		}
		    				};
					  
					    var tit_va = 'Valore ammessi [' + rec.get('mode') + ', ' + rec.get('mdvari') + ', ' + rec.get('seq3') + ', '+ rec.get('seq4') +']'
             			acs_show_win_std(tit_va, 'acs_anag_art_config_va.php?fn=open_val', {mode: rec.get('mode'), seq : rec.get('mosequ'), var : rec.get('mdvari') }, 800, 400, my_listeners, 'icon-design-16');
            		 }	
            			
            		  if(col_name == 'tp_cfg_val' && rec.get('cfg_val').trim() != '' && rec.get('seq_cfg').trim() != ''){
            		  
            		              		   
            		    if(rec.get('tp_seq') == '?'){
            		     
            		     var variante = '';
            		    
            		     var my_listeners = {
    			    		  			afterSelect: function(from_win, id){
    			    		  			
    			    		  			     variante = id;
    			    		  			     var tit_va = 'Valore ammessi [' + rec.get('cfg_val') + ', ' + rec.get('mdvari') + ', ' + variante + rec.get('riga') + ']';
    			    		  			     
    			    		  			      var my_listeners = {
                                	  			afterClose: function(from_win){
                                	  			    grid.getStore().load();
                                	  			    from_win.close();
                                	        		}
                                				};
    			    		  			     
    			    		  			     acs_show_win_std(tit_va, 'acs_anag_art_config_va.php?fn=open_val', {mode: rec.get('cfg_val'), seq : variante + rec.get('riga'), var : rec.get('mdvari') }, 800, 400, my_listeners, 'icon-design-16');
    			    		  			     from_win.close();
    						        		}
    				    				};
            		    
            		    	acs_show_win_std('Selezione variante valori ammessi ' +rec.get('seq_cfg'), 'acs_tab_sys_grid.php?fn=open_tab', {
            		    			dom: rec.get('seq_cfg'),
            		    			conta_valori_per: {            		    			
										mode: rec.get('cfg_val'), 
										seq : variante + rec.get('riga'), 
										var : rec.get('mdvari')
									}
            		    		}, 300, 350, my_listeners, 'icon-design-16');         	                
      	        		}else{
      	        		    var tit_va = 'Valore ammessi [' + rec.get('cfg_val') + ', ' + rec.get('mdvari') + ', ' + rec.get('seq_cfg') + ', ' +  rec.get('riga') +']'
      	        		    
      	        		    var my_listeners = {
                                	  			afterClose: function(from_win){
                                	  			    grid.getStore().load();
                                	  			    from_win.close();
                                	        		}
                                				};
      	        		    
      	        		    acs_show_win_std(tit_va, 'acs_anag_art_config_va.php?fn=open_val', {mode: rec.get('cfg_val'), seq : rec.get('seq_cfg') + rec.get('riga'), var : rec.get('mdvari')},  800, 400, my_listeners, 'icon-design-16');
      	        		}	
					  }
					  
					  
					  
					  if(col_name == 'tipo')
							acs_show_win_std('Condizioni di elaborazione variabili - Sequenza ' + rec.get('seq') + ' ' + rec.get('var') + ' ' + rec.get('denominazione'), 'acs_anag_art_config.php?fn=open_cond', {
							record : rec.data
							}, 1100, 550, null, 'icon-design-16');
						
							  							  
							 
				  }
			   	},  
				selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('panel').down('#dx_form');
		               
		                form_dx.setTitle('Dettagli distinta configuratore [' + selected[0].get('mode') + '] ' + selected[0].get('d_mode'));
		               //pulisco eventuali filtri
		               form_dx.getForm().reset();
		               //ricarico i dati della form
	                   //form_dx.getForm().setValues(selected[0].data);
	                   acs_form_setValues(form_dx, selected[0].data);	
	                   }
		          }
				  
				 }
			, viewConfig: {
		        getRowClass: function(record, index) {
		        
		           if (record.get('tipo') == 'S')
		           		return ' colora_riga_giallo';	         		
		        		           		
		           return '';																
		         }   
		    }	
		
		
		}
		
	  <?php 
	  if($m_params->d_cfg != 'Y'){
	  
	       if($m_params->totalizza != 'Y'){?>
			, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: '<?php echo $title2; ?>',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [
 		           {
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					hidden : true						
				   },{
					name: 'c_mosequ',
					fieldLabel : 'c_mosequ',
					xtype: 'textfield',
					hidden : true						
				   }, 
				   { 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					{
					xtype : 'hiddenfield',
					name : 'attiva_confirm',
					itemId : 'attiva',
					value : ''				
					},
					
					 {
    					name: 'seq3',
    					fieldLabel : 'Sequenza',
    					xtype: 'textfield',
    					anchor: '-15',
    					maxLength : 3, 
    					width : 150,
    					listeners : {
    					  'change': function(field, newVal, oldVal){
            				  field.setValue(field.getValue().toUpperCase());
            				  if(!Ext.isEmpty(newVal) && !Ext.isEmpty(oldVal)){
            				  if(newVal != oldVal){
            				    	field.up('form').down('#attiva').setValue('Y');
        				    	}else{
            						field.up('form').down('#attiva').setValue('');
                				}
            				  }
          					}
					}
    				  },{
    					name: 'seq4',
    					fieldLabel : 'Riga',
    					xtype: 'textfield',
    					labelWidth : 90,
    					labelAlign : 'right',
    					anchor: '-15',
    					width : 120,
    					maxLength : 1,
    					listeners : {
					  		'change': function(field, newVal, oldVal){
        				  		field.setValue(field.getValue().toUpperCase());
        				        if(!Ext.isEmpty(newVal) && !Ext.isEmpty(oldVal)){
        				  	    if(newVal != oldVal){
            				    	field.up('form').down('#attiva').setValue('Y');
        				    	}else{
            						field.up('form').down('#attiva').setValue('');
                				}
            				}
      					}
					}
    				  }
					
					   
					]},
					

                
                {name: 'mdvari',
                		xtype: 'combo',
                		flex: 1,
                		fieldLabel: 'Variabile',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); ?>	
                		    ]
                		}, listeners: {
                        	change: function(field,newVal, oldVal) {
                        	    if(!Ext.isEmpty(newVal) && !Ext.isEmpty(oldVal)){
                				  	    if(newVal != oldVal){
                    				    	field.up('form').down('#attiva').setValue('Y');
                        				}else{
                    						field.up('form').down('#attiva').setValue('');
                        				}
                                 }
                
                                }, beforequery: function (record) {
                                record.query = new RegExp(record.query, 'i');
                                record.forceAll = true;
                            }
                   }
                		
                   },



				  <?php 
				  
				 $proc = new ApiProc();
				 echo $proc->get_json_response(
				      extjs_combo_dom_ris(array('label' =>'Valore assunto', 
				                'file_TA' => 'sys', 
				                'dom_cf'  => 'mdvari',  
				                'ris_cf'  => 'valore',
				                'per_valore_assunto' => true,
				                'risposta_non_obbligatoria' => true,				                
				                'tipo_opz_risposta' => array(
				                   // 'output_domanda' => true, TODO: gestire correttamente caso in cui ho domanda e risposta
				                    'tipo_cf' => 't_val',
				                    'opzioni' => array(
				                        array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*'),
				                        array('id' => '?', 'text' => 'Altra variabile', 'taid' => 'PUVR')
				                    )
				                )
				 ))
				      );
				  
				  ?>					
					
					

				 
				 
				 
				 
				  
				  
				  
				  
				  , {name: 'tipo',
                		xtype: 'combo',
                		flex: 1,
                		fieldLabel: 'Tipo',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		allowBlank: true,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     {id: '', text: ' - vuota -'}, {id: 'C', text: 'Condizionata'}, {id: 'S', text: 'Secondaria / non visibile'} 	
                		    ]
                		}
                   }				  
				  
				  
				  
				  
				  
				  
				  
				  
				    
				  			  				 

				  
				  ,{ 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
    				  {
    					name: 'val_min',
    					fieldLabel : 'Valore minimo',
    					xtype: 'textfield',
    					width : 180
    				  }, 	   ,{
    					name: 'val_max',
    					fieldLabel : 'Massimo',
    					labelAlign : 'right',
    					labelWidth : 60,
    					width : 140,
    					xtype: 'textfield',
    					anchor: '-15'
    				  }
					   
					]}
						
				  
				   ,{
					name: 'note',
					fieldLabel : 'Note',
					xtype: 'textarea',
					anchor: '-15',
					height : 40,
					maxLength : 60
				  }, 
				  
				  {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
                       {name: 'cfg_val',
                		xtype: 'combo',
                		flex: 1,
                		itemId: 'cfg',
                		fieldLabel: 'Config.val.amm.',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     <?php echo acs_ar_to_select_json(find_TA_sys('PUMO', null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); ?>	
                		    ]
                		}, listeners: {
                        	 beforequery: function (record) {
                                record.query = new RegExp(record.query, 'i');
                                record.forceAll = true;
                            }
                   		}
                		
                   },{										  
					xtype: 'displayfield',
					editable: false,
					fieldLabel: '',
					padding: '0 0 0 5',
				    value: <?php echo j("<img src=" . img_path("icone/48x48/sub_red_delete.png") . " width=16>"); ?>,
				    listeners: {
					            render: function( component ) {
					            	var m_form = this.up('form').getForm();
					                component.getEl().on('click', function( event, el ) {
										m_form.findField('cfg_val').setValue('');
										
										//azzero anche i tre valori (e il display) della sequenza
										m_form.findField('seq_cfg').setValue('');
										m_form.findField('tp_seq').setValue('');
										m_form.findField('riga').setValue('');
										m_form.findField('comp_decod_seq_config').setValue(''); //displayField
									});										            
					             }
							}										    
				    
				    
				}
				  ]},
				  
				  
<?php
  $proc = new ApiProc();
  echo $proc->get_json_response($c_config_seq->get_comp());
?>
				  
	
		  ],
		  
		 acs_actions : {
		 
		     check_chiave : function(form){
		       var attiva_confirm = form.findField('attiva_confirm').getValue();
		       return attiva_confirm;
		       
		     
		     },
		     
		     save_config : function(form, grid){
		     
		          var form_values = form.getValues();
		     
		          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_config',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
	        				
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        form.findField('attiva_confirm').setValue('');
					        grid.getStore().removeAll();	//per bug che non riposiziona bene il mouse sulla riga selezionata
					        grid.store.load();
					    },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });
		     
		     
		     
		     },
		     genera_config : function(form, grid){
		      var form_values = form.getValues();
		        Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_config',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				c_art : '<?php echo $m_params->c_art; ?>',
	        				mode : '<?php echo $m_params->mode; ?>'
						},							        
				        success : function(result, request){
				         var jsonData = Ext.decode(result.responseText);
				          if(jsonData.success == false){
		      	    		 acs_show_msg_error(jsonData.msg_error);
				        	 return;
	        			 }else{
				     	    grid.store.load();
				       		}
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		     
		     }
		 
		 
		 },
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			           Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   	  if (btn == 'yes'){
     			          Ext.Ajax.request({
    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
    				        timeout: 2400000,
    				        method     : 'POST',
    	        			jsonData: {
    	        				form_values : form_values,
    	        				mode : '<?php echo $m_params->mode; ?>'
    						},							        
    				        success : function(result, request){
    					        var jsonData = Ext.decode(result.responseText);
    					        if (jsonData.success == true){
    					        	var gridrecord = grid.getSelectionModel().getSelection();
    					        	grid.store.remove(gridrecord);	
    					        	form.getForm().reset();						        
    					        }
    				        },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    }); 		     
		           		}
            	   	});	
			
			            }

			     },
                 {
                     xtype: 'button',
                    text: 'Refresh',
		            scale: 'small',	               
		            iconCls : 'icon-button_black_repeat_dx-16',      
			        handler: function() {
		          	
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			          grid.getStore().load();
 			          
			            }

			     }, '->',
                 
                  
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var grid = this.up('panel').up('panel').down('grid'); 
	       			  var row = grid.getSelectionModel().getSelection()[0];
	       			 
	       			  if(!Ext.isEmpty(row) && (row.get('cond') > 0 || row.get('c_mosequ'))){
	       			  
	       			   Ext.Msg.confirm('Richiesta conferma', 'Confermi la creazione della sola sequenza senza duplica condizioni e valori ammessi?' , function(btn, text){
            	   		 if (btn == 'yes'){
         			        form.acs_actions.genera_config(form.getForm(), grid);
         			     }
        	   		  });
	       			  
	       			  }else{
	       			  		form.acs_actions.genera_config(form.getForm(), grid);
	       			   }
	       			  
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',  ///PRINCIPALE
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		           	   
		                var form = this.up('form');
		       			var grid = this.up('panel').up('panel').down('grid'); 
		       			
		       			var confirm = form.acs_actions.check_chiave(form.getForm());
		       			if(confirm == 'Y'){
		       			   Ext.Msg.confirm('Richiesta conferma', 'Confermi la modifica della chiave?' , function(btn, text){
            	   		   if (btn == 'yes'){
            	   		 		form.acs_actions.save_config(form.getForm(), grid);
            	   		     }
            	   		  });
		       			}else{
		       				   form.acs_actions.save_config(form.getForm(), grid);
		       			}
		       			    
		       	  }

			     }
			     ]
		   }]
			 	 }
					
				<?php } }?>		   
		
					 ],
					 
					
					
	}
	
]}
<?php

exit;
}


if ($_REQUEST['fn'] == 'open_cond'){
    $m_params = acs_m_params_json_decode();
    
    ?>
{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				
					{
						xtype: 'grid',
						title: '',
						flex:0.5,
				        loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_cond', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['cond', 'swie', 'note', 'valore', 't_val', 'MDVARI', 'rrn', 'decod',
		        			
		        			<?php for($i = 1; $i <= 12; $i++){?>
		        			'MDVAL<?php echo $i?>',
		        			'MDVAR<?php echo $i?>',
		        			'MDTPV<?php echo $i?>',
		        			'MDOPV<?php echo $i?>',
		        			<?php }?>
		        			
		        			]							
									
			}, //store
			
			      columns: [	
			       
	                {
	                header   : 'Cond.',
	                dataIndex: 'cond',
	                filter: {type: 'string'}, filterable: true,
	                 width : 40
	                },
	                {
	                header   : 'I/E',
	                tooltip : 'Includi/Escludi',
	                dataIndex: 'swie',
	                filter: {type: 'string'}, filterable: true,
	                 width : 30
	                }, {
	                header   : 'Valore assunto',
	                dataIndex: 'decod',
	                filter: {type: 'string'}, filterable: true,
	                width : 200
	                }, {
	                header   : 'Note',
	                dataIndex: 'note',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                }
	              
	               
	         ], listeners: {
	         
	           afterrender: function (comp) {
	                   
						//comp.up('window').setTitle('<?php echo "Configuratore articolo {$m_params->c_art} {$mode}";?>');	 				
		 			} ,
	    
	         
	          
				selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('panel').down('#dx_form');
		            
		               
		               //pulisco eventuali filtri
		               //////////////form_dx.getForm().reset(); //PROBLEMA DI LENTEZZA
		               //ricarico i dati della form
	                    acs_form_setValues(form_dx, selected[0].data);	
	                   }
		          }
				  
				 }
			, viewConfig: {
		        getRowClass: function(record, index) {
		        
		           if (record.get('tipo') == 'S')
		           		return ' colora_riga_giallo';	         		
		        		           		
		           return '';																
		         }   
		    }	
		
		
		}

		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettagli condizioni di elaborazione',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.5,
 		            frame: true,
 		            items: [
 		           {
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					hidden : true						
				   },
				   
				   
				   {
					name: 'MDVARI',
					xtype: 'textfield',
					hidden : true								
				   },
				   
				   	{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							{
        					name: 'cond',
        					fieldLabel : 'Condizioni',
        					xtype: 'textfield',
        					anchor: '-15',
        					labelWidth : 60,
        					maxLength : 2,
        					width : 150,
        				  },{
                			name: 'swie',
                			xtype: 'combo',
                			margin : '0 0 0 5',
                			fieldLabel: 'Includi/escludi',
                			labelWidth : 80,
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: false,								
                		    anchor: '-15',
                		    width : 200,
                			store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 }
			]},
						 {
        					name: 'note',
        					fieldLabel : 'Note',
        					xtype: 'textfield',
        					anchor: '-5',
        					maxLength : 60,
        					labelWidth : 60,
        					flex : 1
        				  },
						
						<?php
				 $proc = new ApiProc();
				 echo $proc->get_json_response(
				     extjs_combo_dom_ris(array('label' =>'Valore', 'l_width' => 60,
				                'file_TA' => 'sys', 
				                'dom_cf'  => 'MDVARI',  
				                'ris_cf'  => 'valore',
				                'per_valore_assunto' => true,
				                'risposta_non_obbligatoria' => false,
				                'tipo_opz_risposta' => array(
				                    'tipo_cf' => 't_val',
				                    'opzioni' => array(
				                        array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*'),
				                        array('id' => '?', 'text' => 'Altra variabile', 'taid' => 'PUVR')
				                    )
				                )
				        ))
				 );
?>
						  , 
				  
				  <?php for($i = 1; $i <= 12; $i ++){
				  
				   $proc = new ApiProc();
				  echo $proc->get_json_response(
				      //extjs_combo_dom_ris(array('risposta_a_capo' => false, 'label' =>"{$i})Variabile", 'file_TA' => 'sys', 'dom_cf'  => "MDVAR{$i}",  'ris_cf' => "MDVAL{$i}"))
				      
				      extjs_combo_regola_config(array('risposta_a_capo' => false,
				          'label' => "Test {$i}",
				          'l_width' => 60,
				          'f_var' => "MDVAR{$i}",
    				      'f_tp'  => "MDTPV{$i}",
    				      'f_van' => "MDVAL{$i}",
    				      'f_os'  => "MDOPV{$i}"
				      ))
				      
				  );
				  echo ",";
				  
				  }?> 
				  
			 		           ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
		           Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
        	   		if (btn == 'yes'){
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				from_cond : 'Y'
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);							        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });
				    
		    		}
    	   		  }); 		     
		           
			
			            }

			     },'->',
			     
			          
			       {
                     xtype: 'button',
                    text: 'Condizioni accantonate',
		            scale: 'small',	                     
					iconCls: 'icon-copy-16',
		          	handler: function() {
		           		acs_show_win_std('Condizioni accantonate disponibili alla copia', 'acs_gestione_valori_copiati.php?fn=open_tab', {}, 600, 400, null, 'icon-copy-16');   	     
	       			   }
 				   },
                 {xtype : 'tbfill'},
                  
		           
			      {
                    xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid');
 			         			        
 			       	  if (form.getForm().isValid()){
     			          Ext.Ajax.request({
    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_config',
    				        timeout: 2400000,
    				        method     : 'POST',
    	        			jsonData: {
    	        				form_values : form_values,
    	        			    cond : 'Y',
    	        				record : <?php echo acs_je($m_params->record); ?>
    						},							        
    				        success : function(result, request){
    					        var jsonData = Ext.decode(result.responseText);
    					        
    					        if (jsonData.success == false){
    					        	if (!Ext.isEmpty(jsonData.msg_error))
    					        		acs_show_msg_error(jsonData.msg_error);
    					        	else
    					        		acs_show_msg_error('Errore');
    					        }    					        
    					        grid.store.load();
    					    },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				      }); 
		           	  }			
			       }

			     },
			      {
                    xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid');
 			       			 
 			       			if (form.getForm().isValid()){
     			       			Ext.Ajax.request({
    						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_config',
    						        timeout: 2400000,
    						        method     : 'POST',
    			        			jsonData: {
    			        				form_values : form_values,
    			        				cond : 'Y'    			        				
    								},							        
    						        success : function(result, request){
    							        var jsonData = Ext.decode(result.responseText);
    							        
    							        //ToDo: show win se jsonData.success == false
    							        //ToDo: verificare chiave duplicata..
    							        
    							        grid.store.load();    							            							        
    							    },
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });
							}
			            }	

			     }
			     ]
		   }]
			 	 }
							   
		
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}
