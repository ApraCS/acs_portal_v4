<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'ABAR',
    'title_grid' => 'ABAR- Abbinamenti',
    'fields' => array(
        'TAID'     => array('label'	=> 'tabella', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 300),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'articolo'   => array('label'	=> 'Articolo', 'maxLength' => 15, 'only_view' => 'F'),
        'tipo'       => array('label'	=> 'Tipo abbinamento', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_std' =>'ABATP'),
        'disp'       => array('label'	=> 'DisponibilitÓ', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_std' =>'ARY'),
        'data_ini'   => array('label'	=> 'ValiditÓ da', 'only_view' => 'F', 'xtype' => 'datefield'),
        'data_fin'   => array('label'	=> 'ValiditÓ a', 'only_view' => 'F', 'xtype' => 'datefield'),
        'f_giacenza' => array('label'	=> 'Giacenza', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_std' =>'ARY'),
        //'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella abbinamenti',
    'TAREST' => array(
        'articolo' 	=> array(
            "start" => 0,
            "len"   => 15,
            "riempi_con" => ""
        ),
        'tipo' 	=> array(
            "start" => 15,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'disp' 	=> array(
            "start" => 16,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'data_ini' 	=> array(
            "start" => 17,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'data_fin' 	=> array(
            "start" => 25,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'f_giacenza' 	=> array(
            "start" => 48,
            "len"   => 1,
            "riempi_con" => ""
        ),
        
        
        
    )
  
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';
