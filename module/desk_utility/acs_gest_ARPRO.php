<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();
$main_module =  new DeskArt();
$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "ARPRO - Propriet&agrave;",
    'descrizione' => "Gestione propriet&agrave;/caratteristiche articolo",
    'form_title' => "Dettagli propriet&agrave;/caratteristiche articolo",
    'fields_preset' => array(
        'TATAID' => 'ARPRO'
    ),
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAPESO', 'TAASPE', 'TAB_ABB', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAPESO', 'TAASPE'),
    
    'fields' => array(				
        'TAKEY1' => array('label'	=> 'Codice',  'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
		    'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
            'TAASPE' => array('label'	=> 'Tabella opzioni',  'maxLength' => 5),
            'TAPESO' => array('label'	=> 'Sequenza',  'type' => 'numeric', 'maxLength' => 3),
          //immissione
            'immissione' => array(
                'type' => 'immissione', 'fw'=>'width: 70',
                'config' => array(
                    'data_gen'   => 'TADTGE',
                    'user_gen'   => 'TAUSGE'
                )
                
            ),
        //TABELLA ABBINATA
        'TAB_ABB' =>  array('label' => 'opzioni', 'type' => 'TA',
            'iconCls' => 'search',
            'tooltip' => 'Tabella opzioni',
            'select' => "TA_OP.C_ROW AS TAB_ABB",
            'join' => "LEFT OUTER JOIN (
            SELECT COUNT(*) AS C_ROW, TA2.TATAID 
            FROM {$cfg_mod['file_tabelle']} TA2
            WHERE TA2.TADT = '{$id_ditta_default}' AND SUBSTRING(TA2.TATAID, 1, 2) ='P#'
            GROUP BY TA2.TATAID) TA_OP
            ON TA.TAASPE = TA_OP.TATAID",
            'ta_config' => array(
                'only_opz' => 'Y',
                'file_acs'     => '../desk_utility/acs_gest_TAB_OPZ.php?fn=open_tab',
                
            )
            ),
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione')
		),
		'buttons' => array(
		    array(
		        'text'  => '',
		        'iconCls' => 'icon-print-16',
		        'tooltip' => 'Report proprietÓ/opzioni',
		        'scale' => 'small',
		        'style' => 'border: 1px solid gray;',
		        'handler' =>  new ApiProcRaw("function(){
                    window.open('acs_gest_ARPRO_report.php?fn=open_report');
             }")
		    )
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
