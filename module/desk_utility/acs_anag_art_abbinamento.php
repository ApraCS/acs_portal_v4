<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

function genera_TAREST($m_table_config, $values){
    $value_attuale = $values->TAREST;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
        foreach($m_table_config['TAREST'] as $k => $v){
            if(isset($values->$k)){
                $chars = $values->$k;
                $len = "%-{$v['len']}s";
                $chars = substr(sprintf($len, $chars), 0, $v['len']);
                $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                $value_attuale = $new_value;
                
            }
            
        }
        
        return $new_value;
}

$m_table_config = array(
    
    'TAREST' => array(
        'articolo' 	=> array(
            "start" => 0,
            "len"   => 15,
            "riempi_con" => ""
        ),
        'tipo' 	=> array(
            "start" => 15,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'disp' 	=> array(
            "start" => 16,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'data_ini' 	=> array(
            "start" => 17,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'data_fin' 	=> array(
            "start" => 25,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'f_giacenza' 	=> array(
            "start" => 46,
            "len"   => 1,
            "riempi_con" => ""
        ),
    
        
       
    )
    
    
);

if ($_REQUEST['fn'] == 'exe_sos_att'){
    
    $ar_upd = array();
    
    $sosp = trim($m_params->row->TATP);
    
    if($sosp == "")
        $new_value = "S";
    else
        $new_value = "";
            
        
    $ar_upd['TAUSUM'] = $auth->get_user();
    $ar_upd['TADTUM'] = oggi_AS_date();
    $ar_upd['TATP']   = $new_value;
    
    $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    WHERE RRN(TA) = '{$m_params->row->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}


if ($_REQUEST['fn'] == 'exe_inserisci'){
    $ret = array();
    
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
    
    $ar_ins['TANR']   = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = sprintf("%-30s", substr($form_values->note, 0, 30));
    $ar_ins['TADES2'] = sprintf("%-30s", substr($form_values->note, 30, 30));
    $tarest = genera_TAREST($m_table_config, $form_values);
    $ar_ins['TAREST'] 	= $tarest;
    
    $row = get_TA_sys('ABAR', $form_values->riga, $m_params->open_request->c_art);
        
        if($row != false && count($row) > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Codice esistente";
            echo acs_je($ret);
            return;
        }
        
        $ar_ins['TADTGE'] = oggi_AS_date();
        $ar_ins['TAUSGE'] = $auth->get_user();
        $ar_ins['TADTUM'] = oggi_AS_date();
        $ar_ins['TAUSUM'] = $auth->get_user();
        $ar_ins["TADT"]   = $id_ditta_default;
        $ar_ins["TAID"]   = 'ABAR';
        $ar_ins["TACOR1"] = $m_params->open_request->c_art;
        //aggiungere progressivo?
        
        $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_tab_sys']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_mod_ab'){
    $ret = array();

    $form_values = $m_params->form_values;
    
    $ar_ins = array();
    
     $ar_ins['TANR']   = sprintf("%-3s", $form_values->riga);
     $ar_ins['TADESC'] = sprintf("%-30s", substr($form_values->note, 0, 30));
     $ar_ins['TADES2'] = sprintf("%-30s", substr($form_values->note, 30, 30));
     $tarest = genera_TAREST($m_table_config, $form_values);
     $ar_ins['TAREST'] 	= $tarest;
     
    $ar_ins['TADTUM'] = oggi_AS_date();
    $ar_ins['TAUSUM'] = $auth->get_user();
   
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_tab_sys']} TA
    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
    WHERE RRN(TA) = '{$form_values->rrn}'";
        
    
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc_ab'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->form_values->rrn;
   
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA WHERE RRN(TA) = '{$rrn}'";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}





if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    
    $c_art = $m_params->open_request->c_art;
    
        $sql = " SELECT RRN(TA) AS RRN, TA.*
                 FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
                 WHERE TADT = '{$id_ditta_default}' AND TAID = 'ABAR' AND TACOR1 = '{$c_art}'";
  
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        
        $sql_a = "SELECT ARART, ARDART
                 FROM {$cfg_mod_DeskUtility['file_anag_art']}
                 WHERE ARDT = '{$id_ditta_default}' AND ARART = ?";
        
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
       
        
        
        while($row = db2_fetch_assoc($stmt)){
         
            $nr = array();
            $nr['tipo']   = substr($row['TAREST'], 15, 1);
            $nr['disp']   = substr($row['TAREST'], 16, 1);
          
            switch ($nr['tipo']){
                case 'Y' :
                    $desc = 'Sostituito da';
                    break;
                case 'C' :
                    $desc = 'Abbinamento commerciale';
                    break;
                case 'D' :
                    $desc = 'Abbinamento disponibilit&agrave;';
                    break;
                case 'G':
                    $desc = 'Gruppo equivalente';
                    break;
                case 'A':
                    $desc = 'Articolo equivalente';
                    break;
             }
            
            $nr['s_t']   = "[".$nr['tipo']."] ".$desc;
            $nr['articolo']  = trim(substr($row['TAREST'], 0, 15));
            $nr['riga'] = $row['TANR'];
            $result = db2_execute($stmt_a, array(substr($row['TAREST'], 0, 15)));
            $row_a = db2_fetch_assoc($stmt_a);
            $nr['d_art'] = $row_a['ARDART'];
            $nr['data_ini_df'] = substr($row['TAREST'], 17, 8);
            $nr['data_fin_df'] = substr($row['TAREST'], 25, 8);
            
            $nr['data_ini']  = print_date($nr['data_ini_df']);
            $nr['data_fin']  = print_date($nr['data_fin_df']);
            
            $nr['f_giacenza'] = substr($row['TAREST'], 46, 1);
            $nr['note'] = $row['TADESC']."".$row['TADES2'];
            $nr['rrn'] = $row['RRN'];
            $nr['TADTGE'] = $row['TADTGE'];
            $nr['TADTUM'] = $row['TADTUM'];
            $nr['TAUSGE'] = $row['TAUSGE'];
            $nr['TAUSUM'] = $row['TAUSUM'];
            $nr['TATP'] = $row['TATP'];
            $ar[] = $nr;
            
        }
        
  
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){ ?>
    
    {"success": true, items: [
      {xtype: 'panel',
			autoScroll : true,
        	layout: {
				type: 'vbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
    
       	{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            height : 300,
            frame: true,
            layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
            items: [
			{
    		xtype: 'grid',
    		flex:0.65,
        	multiSelect: true,
        	autoScroll: true,
        	features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}],
			 tbar: new Ext.Toolbar({
                items:[ '->',
                {
        		 iconCls: 'icon-gear-16',
	             text: 'Gruppo equivalente', 
		           		handler: function(event, toolEl, panel){
		           		 	acs_show_win_std('Gruppo equivalenti', 'acs_anag_art_gruppo_eq.php?fn=open_tab', {}, 950, 500, null, 'icon-gear-16');
		           		
		           		 }
		           	 }
	         ]            
	        }),	 
        	store: {
        	xtype: 'store',
    		autoLoad:true,
    		proxy: {
    			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
    			type: 'ajax',
    			actionMethods: {
    				read: 'POST'
    			},
    				extraParams: {
                        open_request : <?php echo acs_je($m_params); ?>		
				},
        				
    			doRequest: personalizza_extraParams_to_jsonData, 
		
				reader: {
		            type: 'json',
					method: 'POST',						            
		            root: 'root'						            
		        }
			},
        	fields: [ 'riga', 's_t' , 'disp', 'tipo' , 'd_art', 'data_ini', 'data_fin', 'data_ini_df', 'data_fin_df',
        	          'note', 'f_giacenza', 'rrn', 'articolo', 'TATP', 'TADTGE', 'TADTUM', 'TAUSGE', 'TAUSUM']
    	}, //store
    		
    		<?php $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
    		
    		columns: [
    		   {text: '<?php echo $cf1; ?>', 	
				width: 30, 
				dataIndex: 'TATP',
				tooltip: 'Sospeso',		        			    	     
		    	renderer: function(value, metaData, record){
		    			  if(record.get('TATP') == 'S'){ 
		    			   metaData.tdCls += ' sfondo_rosso';
		    			   return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	}
		    		  }
		        },
			      	{
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width : 40
	                },
			      {
	                header   : 'TA',
	                dataIndex: 'tipo',
	                width : 30
	                },
	                 {header: 'Val.ini.', dataIndex: 'data_ini_df', width: 70,
                      renderer: function(value, p, record){
		    			  if(record.get('data_ini_df').trim() == '') return '';
		    			  if(record.get('data_ini_df') == '99999999') return '31/12/2099';
		    			  
		    			  return date_from_AS(value);
		    		  }
                      },
	                {
	                header   : 'Articolo',
	                dataIndex: 'articolo',
	                width : 80
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'd_art',
	                flex: 1
	                },
	              
                 	  {
	                header   : 'Note',
	                dataIndex: 'note',
	                flex: 1
	                },
	                  {
	                header   : 'IG',
	                dataIndex: 'f_giacenza',
	                width : 30
	                },
	                  {header: 'Immissione',
        	 columns: [
        	 {header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
        	 renderer: function(value, metaData, record){
        	         if (record.get('TADTGE') != record.get('TADTUM')) 
        	          metaData.tdCls += ' grassetto';
        	 
                     q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
        			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
        		     return date_from_AS(value);	
        	}
        	 
        	 }
        	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
        	  renderer: function(value, metaData, record){
        	  
        	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
        	          metaData.tdCls += ' grassetto';
        	  
                     q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
        			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
        		     return value;	
        	} 
        	 }
        	 ]}
	                
	         ],
			listeners: {
			  	 selectionchange: function(selModel, selected) { 
				   if(selected.length > 0){
					   var form_dx = this.up('form').up('panel').down('#dx_form');
					   form_dx.getForm().reset();
					   rec_index = selected[0].index;
					   form_dx.getForm().findField('rec_index').setValue(rec_index);
					   acs_form_setValues(form_dx, selected[0].data);
					 //  form_dx.getForm().setValues(selected[0].data);
					}					
				 }
				 	 ,itemcontextmenu : function(grid, rec, node, index, event) {
   		  	 	    event.stopEvent();
	  											  
			 		var voci_menu = [];
		     		var row = rec.data;
		     		var m_grid = this;
		     		
		     		   
					 	 voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : 'acs_anag_art_abbinamento.php?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          rec.set('TATP', jsonData.new_value);
								          rec.commit();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
		    				var menu = new Ext.menu.Menu({
    				            items: voci_menu
    					}).showAt(event.xy);	
					     		
			     }
			
	         }

			 ,viewConfig: {
	         getRowClass: function(record, index) {
	         //return ret;																
	         }   
	    },
			    
	        
        } //grid
	
	 ,{
			xtype: 'form',
			itemId: 'dx_form',
			autoScroll : true,
			title: 'Dettagli tabella abbinamenti',
			bodyStyle: 'padding: 10px',
			bodyPadding: '5 5 0',
			flex:0.35,
			frame: true,
			items: [
			   {xtype: 'textfield', name: 'rrn', hidden : true},
			   {xtype : 'textfield',name : 'rec_index',hidden : true}, 
		      {xtype: 'textfield',
    						name: 'riga',
    						fieldLabel: 'Riga',
    						labelWidth : 110,
    						width : 150,
    						//anchor: '-15',	
    						},
	                   {
								name: 'tipo',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'Tipo abbinamento',
								itemId: 'c_tipo',
								labelWidth : 110,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'Y', text : '[Y] Sostituito da'},
								     	 {id: 'C', text : '[C] Abbinamento commerciale'},
								     	 {id: 'D', text : '[D] Abbinamento disponibilitÓ'},
								     	 {id: 'G', text : '[G] Gruppo equivalente'},
								     	 {id: 'A', text : '[A] Articolo equivalente'},
								     	 {id: 'M', text : '[M] Migrazione'},
								     	
								    ]
								}, listeners: {
                    				change: function(field,newVal) {
                    				    combo_art = this.up('form').down('#cod_art');
                    				    combo_art.allowBlank = true;
                    				
                      					if (newVal == 'G'){                      		 
                                     	 combo_art.store.proxy.extraParams.gruppo = 'Y';
                                    	 combo_art.store.load();                             
                             			}
                             			
                             			if (newVal == 'M')
                             			    combo_art.allowBlank = false;
                                    
                             			if (!Ext.isEmpty(newVal) && newVal != 'D'){
                             			
                         			     c_giac = field.up('form').down('#c_giac'); 
                                 	     c_giac.setValue('');
                                 	      
                                	     
                                	     }
                             

                            }
                        }
								
						   },
						  {
						flex: 1,
			            xtype: 'combo',
						name: 'articolo',
						fieldLabel: 'Articolo',
						labelWidth : 110,
						itemId: 'cod_art',
						anchor: '-15',
						minChars: 2,
						allowBlank : true,
						store: {
			            	pageSize: 1000,           	
							proxy: {
					            type: 'ajax',
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_articoli',
					            actionMethods: {
						          read: 'POST'
						        },
					            extraParams: {
				    		    		gruppo: '',
				    				},				            
						        doRequest: personalizza_extraParams_to_jsonData, 
					            reader: {
					                type: 'json',
					                root: 'root',
					                method: 'POST',	
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            },
                      
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun articolo trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {text}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
        		},{
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	   , labelWidth : 110
        	   , fieldLabel: 'Validit&agrave; iniziale'
        	   , name: 'data_ini'
        	   , format: 'd/m/y'							   
        	   , submitFormat: 'Ymd'
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        		, anchor: '-15'
        	   
        	},{
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	   , labelWidth : 110
        	   , fieldLabel: 'Validit&agrave; finale'
        	   , name: 'data_fin'
        	   , format: 'd/m/y'						   
        	   , submitFormat: 'Ymd'
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        	   , anchor: '-15'
        	},{xtype: 'textfield',
    						name: 'note',
    						fieldLabel: 'Note',
    						labelWidth : 110,
    						maxLength: 60,
    						anchor: '-15'	
    						},
    						
    						   {
								name: 'f_giacenza',
								xtype: 'combo',
								flex: 1,
								itemId: 'c_giac',
								fieldLabel: 'Ignora giacenza',
								labelWidth : 110,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'Y', text : '[Y] Yes'},
								     	 {id: '', text : '- vuoto -'},
								     	
								    ]
								}, listeners: {
                    				change: function(field,newVal) {
                    				     if(newVal == 'Y'){
                    				       c_tipo = this.up('form').down('#c_tipo');                   		 
                                     	   if (c_tipo.getValue() != 'D'){ 
                                     	     field.setValue('');
                                    	     acs_show_msg_error('Manca la disponibilit&agrave;'); 
                                    	     return;
                                    	     }                         
                             			}
                                 }
                               }
								
						   }
			
			
			],
			
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
			    '->'   
			   ,{
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
	       			  
	       			  Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?', function(btn, text){
            	   				if (btn == 'yes'){
	       		
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_ab',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		 
				    
	       			 		}
                	   				
    	   				});
	       			   
		           
			
			            }

			     }, 
			      
                 {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				open_request : <?php echo acs_je($m_params); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					         form.getForm().reset();
					         if(form_values.tipo == 'D'){	
					      	 	acs_show_win_std(null, '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								 c_art : '<?php echo $m_params->c_art; ?>', abar : form_values.riga  
								});
							}
					    
					    },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_ab',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							        form.getForm().reset();	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
			
			}	
				
				
		]} , //abar
		
	   	{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title : 'Abbinamento barcode/EAN13',
            height : 270,
            layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
            items: [
			
				
		   {
    		xtype: 'grid',
        	multiSelect: true,
        	autoScroll: true,
        	flex:0.65,
        	features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}],
        	store: {
        	xtype: 'store',
    		autoLoad:true,
    		proxy: {
    			url: 'acs_anag_art_barcode.php?fn=get_json_data_grid_barcode',
    			type: 'ajax',
    			actionMethods: {
    				read: 'POST'
    			},
    				extraParams: {
                        open_request : <?php echo acs_je($m_params); ?>		
				},
        				
    			doRequest: personalizza_extraParams_to_jsonData, 
		
				reader: {
		            type: 'json',
					method: 'POST',						            
		            root: 'root'						            
		        }
			},
        	fields: [ 'BCART', 'BCSOSP' , 'BCCODE', 'BCCCON' , 'BCDTGE', 'BCUSGE', 'BCUSUM', 'fornitore', 'c_forn', 'rrn']
    	}, //store
    		columns: [
			      	{
	                header   : 'Sosp.',
	                dataIndex: 'BCSOSP',
	                width : 40,
	                renderer: function(value, metaData, record){
		    			  if(record.get('BCSOSP') == 'S'){ 
		    			   metaData.tdCls += ' sfondo_rosso';
		    			   return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	}
		    		 	
		    		  }
	                },
	                {
	                header   : 'Codice',
	                dataIndex: 'BCCODE',
	                 width : 150
	                },
	                {
	                header   : 'Fornitore',
	                dataIndex: 'c_forn',
	                flex : 1,
	                },
                  {header: 'Immissione',
                	 columns: [
                	 {header: 'Data', dataIndex: 'BCDTGE', renderer: date_from_AS, width: 70, sortable : true,
                	 renderer: function(value, metaData, record){
                	        
                             q_tip = 'Modifica: ' + date_from_AS(record.get('BCDTUM')) + ', Utente ' +record.get('BCUSUM');
                			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
                		     return date_from_AS(value);	
                	}
                	 
                	 }
                	 ,{header: 'Utente', dataIndex: 'BCUSGE', width: 70, sortable : true,
                	  renderer: function(value, metaData, record){
                	  
                	  		if (record.get('BCUSGE') != record.get('BCUSUM')) 
                	          metaData.tdCls += ' grassetto';
                	  
                             q_tip = 'Modifica: ' + date_from_AS(record.get('BCDTUM')) + ', Utente ' +record.get('BCUSUM');
                			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
                		     return value;	
                	} 
                	 }
                	 ]}
	                
	         ],
			listeners: {
			  	 selectionchange: function(selModel, selected) { 
				   if(selected.length > 0){
					   var form_dx = this.up('form').up('panel').down('#br_form');
					   form_dx.getForm().reset();
					   rec_index = selected[0].index;
					   form_dx.getForm().findField('rec_index').setValue(rec_index);
					   acs_form_setValues(form_dx, selected[0].data);
					 //  form_dx.getForm().setValues(selected[0].data);
					}					
				 }
				 ,itemcontextmenu : function(grid, rec, node, index, event) {
   		  	 	    event.stopEvent();
	  											  
			 		var voci_menu = [];
		     		var row = rec.data;
		     		var m_grid = this;
		     		
		     		   
					 	 voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : 'acs_anag_art_barcode.php?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          rec.set('BCSOSP', jsonData.new_value);
								          rec.commit();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
		    				var menu = new Ext.menu.Menu({
    				            items: voci_menu
    					}).showAt(event.xy);	
					     		
			     }
			
	         }

			 ,viewConfig: {
	         getRowClass: function(record, index) {
	         //return ret;																
	         }   
	    },
			    
	        
        } //grid
        
	
		,{
			xtype: 'form',
			itemId: 'br_form',
			autoScroll : true,
			title: 'Dettagli barcode',
			bodyStyle: 'padding: 10px',
			bodyPadding: '5 5 0',
			flex:0.35,
			frame: true,
			items: [
			   {xtype: 'textfield', name: 'rrn', hidden : true},
			   {xtype : 'textfield',name : 'rec_index',hidden : true},
			   {xtype : 'textfield', fieldLabel : 'Codice', name : 'BCCODE', maxLength : 40, allowBlank : false},
			   { xtype: 'fieldcontainer'
                          , layout:
                              {	type: 'hbox'
                              , pack: 'start'
                        	  , align: 'stretch'
                                }
                          , items: [ 
	                    {
            				name: 'BCCCON',
            				xtype: 'hiddenfield',					
            			   }, 
	           		       {
            				name: 'fornitore',
            				fieldLabel : 'Fornitore',
            				xtype: 'displayfield', 
            				anchor: '-15'							
            			   }, 
            			   {xtype: 'component',
                                flex : 1,
                            },
                          { xtype: 'button'
                         , margin: '0 15 0 0'
                         , anchor: '-15'	
                         , scale: 'small'
                         , iconCls: 'icon-search-16'
                         , iconAlign: 'top'
                         , width: 25			
                         , handler : function() {
                               var m_form = this.up('form').getForm();
                               var my_listeners = 
                                 { afterSel: function(from_win, row) {
                                     m_form.findField('BCCCON').setValue(row.CFCD);
                                     m_form.findField('fornitore').setValue('['+row.CFCD+'] <br>' +row.CFRGS1);
                                     from_win.close();
                                   }
                                 }
                             
                               acs_show_win_std('Anagrafica clienti/fornitori'
                                               , 'acs_anag_ricerca_cli_for.php?fn=open_tab'
                                               , {cf: 'F'}, 600, 500, my_listeners
                                               , 'icon-search-16');
                             }
                                    
                        }
                        
                        ]}, 
			
			
			],
			
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
             
						
			    '->'   
			   ,{
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
	       			  
	       			  Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?', function(btn, text){
            	   				if (btn == 'yes'){
	       		
 			        
 			          Ext.Ajax.request({
				        url        : 'acs_anag_art_barcode.php?fn=exe_canc_bc',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		 
				    
	       			 		}
                	   				
    	   				});
	       			   
		           
			
			            }

			     }, 
			      
                 {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : 'acs_anag_art_barcode.php?fn=exe_inserisci_bc',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				open_request : <?php echo acs_je($m_params); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if(jsonData.success == false){
			      	    		 acs_show_msg_error(jsonData.msg_error);
					        	 form.getForm().reset();
		        			}else{
		        			 grid.store.load();
		        			 form.getForm().reset();	
		        			}
					        
					        
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : 'acs_anag_art_barcode.php?fn=exe_modifica_bc',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							        form.getForm().reset();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
			
			}	
		]}
		
		]}

]
}
 
 <?php } 

	
	if ($_REQUEST['fn'] == 'get_json_data_articoli'){
	    
	    $ar = array();
	    $m_params = acs_m_params_json_decode();
	    
	    
	    if($m_params->gruppo == 'Y'){
	        
	        $sql = "SELECT *
	        FROM {$cfg_mod_DeskUtility['file_tabelle']} TA
	        WHERE TADT = '$id_ditta_default'
	        AND TATAID = 'GREQU'";
	        
	       
	        $stmt = db2_prepare($conn, $sql);
	        $result = db2_execute($stmt);
	        
	        $ret = array();
	        while ($row = db2_fetch_assoc($stmt)) {
	            
	            $text = "[" . trim($row['TAKEY1']) . "] " . trim($row['TADESC']);
	            
	            $ret[] = array( "id" 	=> trim($row['TAKEY1']),
	                "text" 	=> acs_u8e($text) );
	        }
	        
	    }else{
	   	    
	    $sql = "SELECT ARART, ARDART
	    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
	    WHERE ARDT = '$id_ditta_default'
        AND UPPER(ARART) LIKE '%" . strtoupper($_REQUEST['query']) . "%'
	    ORDER BY ARART";
	    
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    
	    $ret = array();
	    while ($row = db2_fetch_assoc($stmt)) {
	        
	        $text = "[" . trim($row['ARART']) . "] " . trim($row['ARDART']);
	        
	        $ret[] = array( "id" 	=> trim($row['ARART']),
	            "text" 	=> acs_u8e($text) );
	    }
	    
	    }
	    
	    
	    echo acs_je($ret);
	    exit;
	}
	
	
if ($_REQUEST['fn'] == 'exe_modifica_ao'){
    $ret = array();
    
    $ritime     = microtime(true);
    
    $ar_ins = array();
    $ar_ins['PITIME'] = $ritime;
    $ar_ins['PIDT']   = $id_ditta_default;
    
    $ar_ins['PIDTGE'] = oggi_AS_date();
    $ar_ins['PIORGE'] = oggi_AS_time();
    $ar_ins['PIUSGE'] = $auth->get_user();
    $data_da = $m_params->form_values->f_data_da;
    $data_a = $m_params->form_values->f_data_a;
    $tipo = "";
    $stato = "";
    for($i=0; $i <= 9; $i++){
        $tipo .= sprintf("%-2s", $m_params->form_values->f_tipo[$i]);
        $stato .= sprintf("%-2s", $m_params->form_values->f_stato[$i]);
        
    }
   
    $ar_ins['PIDEF1'] = $data_da.$data_a.$tipo.$stato;
    
   
    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_parametri']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $sh = new SpedHistory($desk_art);
    $sh->crea(
        'pers',
        array(
            "RITIME"    => $ritime,
            "messaggio"	=> 'AGG_DTEP_OACQ',
            "vals" => array(
                "RICDNEW" => $m_params->c_art,
                "RICLME" => $m_params->abar)
            
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
  
}
	
	
if ($_REQUEST['fn'] == 'open_mod'){

    $data_finale = "31/12/".date('Y', strtotime('+1 years'));
    
    ?>
    
 {"success":true, 
	m_win: {
		title: 'Modifica data consegna ordini a fornitore aperti',
		width: 500, height: 200,
		iconCls: 'icon-pencil-16'
	}, 
	"items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [
        		{
				 	xtype: 'fieldcontainer',
				 	anchor: '-15',
				 	flex : 1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					{
           					xtype: 'datefield',
        					fieldLabel: 'Data evasione da',
        					value: '<?php echo print_date(oggi_AS_date()); ?>',
        					labelWidth : 110,
        					submitFormat : 'Ymd',
        					name: 'f_data_da',
        					flex : 1.5,
        					allowBlank: false,
                    	},
                        {
           					xtype: 'datefield',
        					fieldLabel: 'a',
        					value: '<?php echo $data_finale; ?>',
        					labelWidth : 20,
        					labelAlign: 'right',	
        					name: 'f_data_a',
        					submitFormat : 'Ymd',
        					anchor: '-15',
        					flex : 1,
            			    allowBlank: false,
                    	}
            	]},
            	{
            			name: 'f_tipo',
            			xtype: 'combo',
            			fieldLabel: 'Tipi documento',
            			flex: 1,
            			labelWidth : 110,
            			anchor: '-15',
            			displayField: 'text',							
            			valueField: 'id',
            			emptyText: '- seleziona -',
            			forceSelection:true,
            			allowBlank: true,
            		  	multiSelect: true,	
            		  	queryMode: 'local',
                        minChars: 1,													
            			store: {
            				autoLoad: true,
            				editable: false,
            				autoDestroy: true,	 
            				fields: [{name:'id'}, {name:'text'}],
            				 data: [
                                      <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'AO', null, null, 0, "", 'Y'), ""); ?>
            				    ]
            				},
            			listeners: { beforequery: function (record) {
            		         record.query = new RegExp(record.query, 'i');
            		         record.forceAll = true;
            	             },
            	              beforeselect : function(combo,record,index,opts) {
                              if (combo.getValue().length == 10) {
                                  acs_show_msg_info('Selezionare solo 10 righe');
                              }


                          }
            	              }							 
            			},
            			{
            			name: 'f_stato',
            			flex: 1,
            			xtype: 'combo',
            			fieldLabel: 'Stati documento',
            			labelAlign: 'right',
            			displayField: 'text',
            			labelAlign:'left',
            			anchor: '-15',
            			labelWidth : 110,							
            			valueField: 'id',
            			emptyText: '- seleziona -',
            			forceSelection:true,
            			allowBlank: true,
            			multiSelect: true,
            			queryMode: 'local',
                        minChars: 1,
            			store: {
            				autoLoad: true,
            				editable: false,
            				autoDestroy: true,	 
            				fields: [{name:'id'}, {name:'text'}],
            				 data: [
                                      <?php echo acs_ar_to_select_json(find_TA_sys('BSTA', null, null, 'AO', null, null, 0, "", 'Y'), ""); ?>
            				    ]
            				},
            			listeners: { beforequery: function (record) {
            	        	 record.query = new RegExp(record.query, 'i');
            	        	 record.forceAll = true;
                         },
                           beforeselect : function(combo,record,index,opts) {
                              if (combo.getValue().length == 10) {
                                  acs_show_msg_info('Selezionare solo 10 righe');
                              }


                          }}						 
            			}
            ],
            
			buttons: [{
		            text: 'Conferma',
		            iconCls: 'icon-button_blue_play-32',
			       	scale: 'large',		            
		            handler: function() {
		            	var form = this.up('form').getForm(),
			                loc_win = this.up('window');
			                
		               if (form.isValid()){
							Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_ao',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   form_values: form.getValues(),
 			        			   c_art: <?php echo j($m_params->c_art); ?>,
 			        			   abar: <?php echo j($m_params->abar); ?>
 								},
 								
 								success: function(response, opts) {
						        	 var jsonData = Ext.decode(response.responseText);
						        	 	 
									 acs_show_msg_info('Operazione completata');
									 loc_win.destroy();
			            		   
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
			            }
		            }
		        }
	        ]
	  }
]}
 
 <?php 
    
}