<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

function get_sql_by_dektop($desk){
    
    global $cfg_mod_Spedizioni, $cfg_mod_DeskArt, $cfg_mod_DeskAcq, $cfg_mod_Gest, $id_ditta_default;
    $ar_m_sql = array();
   
    if($desk == 'DOV'){
        $ar_m_sql['cfg_mod_AS0'] = $cfg_mod_Spedizioni['file_assegna_ord'];
        $ar_m_sql['cfg_mod_TA0'] = $cfg_mod_Spedizioni['file_tabelle'];
        $ar_m_sql['where_ta'] = "";
        $ar_m_sql['where_as'] = "";
    }
    if($desk == 'DBD'){
        $ar_m_sql['cfg_mod_AS0'] = "{$cfg_mod_DeskArt['file_assegna_ord']}";
        $ar_m_sql['cfg_mod_TA0'] = "{$cfg_mod_DeskArt['file_tabelle']}";
        $ar_m_sql['where_ta']  = " AND ATT_OPEN.ASDT = TA_ATTAV.TADT AND TA_ATTAV.TARIF1 = 'ART'";
        $ar_m_sql['where_as']  = " AND ASDT = '{$id_ditta_default}'";
    }
    if($desk == 'DAC'){
        $ar_m_sql['cfg_mod_AS0'] = $cfg_mod_DeskAcq['file_assegna_ord'];
        $ar_m_sql['cfg_mod_TA0'] = $cfg_mod_DeskAcq['file_tabelle'];
        $ar_m_sql['where_ta'] = "";
        $ar_m_sql['where_as'] = " AND ASDT = '{$id_ditta_default}'";
    }
    
    if($desk == 'DAM'){
        $ar_m_sql['cfg_mod_AS0'] = $cfg_mod_Gest['file_assegna_ord'];
        $ar_m_sql['cfg_mod_TA0'] = $cfg_mod_Gest['file_tabelle'];
        $ar_m_sql['where_ta'] = " AND ATT_OPEN.ASDT = TA_ATTAV.TADT AND TA_ATTAV.TARIF1 = 'CLI'";
        $ar_m_sql['where_as'] = " AND ASDT = '{$id_ditta_default}'";
    }
    
    return $ar_m_sql;
}

//******************************************
//  GRAFICO: DATI: UTENTE
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_utenti'){
    
    $ar_m_sql = get_sql_by_dektop($m_params->desktop);
    
    if($m_params->immissione == 'Y')
        $campo = 'ASUSAS';
    if($m_params->assegnato == 'Y')
        $campo = 'ASUSAT';
    
       
    $sql = "SELECT {$campo},  COUNT(*) AS NREF
            FROM {$ar_m_sql['cfg_mod_AS0']} ATT_OPEN
            INNER JOIN {$ar_m_sql['cfg_mod_TA0']} TA_ATTAV
            ON ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1 AND TA_ATTAV.TATAID = 'ATTAV' {$ar_m_sql['where_ta']}
            {$ar_m_sql['where_as']}
            GROUP BY {$campo}
            ORDER BY {$campo}";
    
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r["{$campo}"]] += $r['NREF'];
    }
    
    function decod_utente_immissione($v){
        return $v;
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_utente_immissione($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}

//******************************************
//  GRAFICO: TODO APERT/EVASI
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_todo'){
    
    $ar_m_sql = get_sql_by_dektop($m_params->desktop);
    
    if($m_params->attivi == 'Y')
        $as_where = " AND ASFLRI <> 'Y'";
    else
        $as_where = " AND ASFLRI = 'Y'";
    
  
    $sql = "SELECT ASCAAS, COUNT(*) AS NREF
            FROM {$ar_m_sql['cfg_mod_AS0']} ATT_OPEN
            INNER JOIN {$ar_m_sql['cfg_mod_TA0']} TA_ATTAV
                ON ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1 AND TA_ATTAV.TATAID = 'ATTAV' {$ar_m_sql['where_ta']}
            {$ar_m_sql['where_as']} {$as_where} 
            GROUP BY ASCAAS
            ORDER BY ASCAAS";
    
    
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
         $ret_tmp[$r['ASCAAS']] += $r['NREF'];
    }
    
    function decod_todo($v){
        
        global $desk_art;
        
        $ta_std = $desk_art->find_TA_std('ATTAV', trim($v));
    
        return "[".trim($v)."] ".$ta_std[0]['text'];
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("codice" => $k, "name" => decod_todo($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}


//******************************************
//  ELENCO TODO
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    ini_set('max_execution_time', 3000);
    
    $ar_m_sql = get_sql_by_dektop($m_params->desktop);
    
    $oltre_10 = "ASFLRI <> 'Y' AND DATE(TIMESTAMP_FORMAT (CHAR(ASDTAS), 'YYYYMMDD'))  < CURRENT TIMESTAMP - 10 DAYS 
                AND DATE(TIMESTAMP_FORMAT (CHAR(ASDTAS), 'YYYYMMDD'))  >= CURRENT TIMESTAMP - 30 DAYS ";
    $oltre_30 = "ASFLRI <> 'Y' AND DATE(TIMESTAMP_FORMAT (CHAR(ASDTAS), 'YYYYMMDD'))  < CURRENT TIMESTAMP - 30 DAYS";
    
    
    $sql = "SELECT ASCAAS, TA_ATTAV.TADESC, COUNT(*) AS TOTALE,
            MIN(TADTGE) AS DATA_CODIFICA,
            SUM(CASE WHEN ASFLRI ='Y' THEN 1 ELSE 0 END) AS RILASCIATE,
            SUM(CASE WHEN {$oltre_10} THEN 1 ELSE 0 END) AS NR_10G,
            SUM(CASE WHEN {$oltre_30} THEN 1 ELSE 0 END) AS NR_30G,
            MIN(CASE WHEN ASFLRI <> 'Y' THEN ASDTAS ELSE 99999999 END) AS MIN_DT_OPEN,
            MAX(CASE WHEN ASFLRI <> 'Y' THEN ASDTAS ELSE 0 END) AS MAX_DT_OPEN,
            MAX(CASE WHEN ASFLRI <> 'Y' THEN ASUSAS ELSE '' END) AS UT_OPEN,
            MAX(ASDTAS) AS DT_UA,
            MAX(ASUSAS) AS UT_UA,
            MAX(ASDTRI) AS DT_UE
            FROM {$ar_m_sql['cfg_mod_AS0']} ATT_OPEN
            INNER JOIN {$ar_m_sql['cfg_mod_TA0']} TA_ATTAV
            ON TA_ATTAV.TADT = '{$id_ditta_default}' AND TA_ATTAV.TATAID = 'ATTAV' AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1 {$ar_m_sql['where_ta']}
             AND TA_ATTAV.TAKEY2 = '' AND TA_ATTAV.TAKEY3 = '' AND TA_ATTAV.TAKEY4 = '' AND TA_ATTAV.TAKEY5 = ''
            WHERE ASDTAS > 0 {$ar_m_sql['where_as']}
            GROUP BY ASCAAS, TADESC 
            ORDER BY ASCAAS, TADESC";
        
  
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $ar = array();
    
    $result = db2_execute($stmt);
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        $nr["todo"]       = "[".trim($row['ASCAAS'])."] ".trim($row['TADESC']);
        $nr["g_totale"]   = $row['TOTALE'];
        $nr["r_totale"]   = $row['RILASCIATE'];
        $nr["a_totale"]   = $row['TOTALE'] - $row['RILASCIATE'];
        $nr["min_data_open"]   = $row['MIN_DT_OPEN'];
        $nr["max_data_open"]   = $row['MAX_DT_OPEN'];
        $nr["nr_10"]        = $row['NR_10G'];
        $nr["nr_30"]        = $row['NR_30G'];
        $nr["ut_open"]      = $row['UT_OPEN'];
        $nr["ut_ass"]       = $row['UT_UA'];
        $nr["data_ass"]     = $row['DT_UA'];
        $nr["data_eva"]     = $row['DT_UE'];
        $nr["desk"]         = $m_params->desktop;
        $ar[] = $nr;
       
    }
   
  
    echo acs_je($ar);
    exit;
}

//******************************************
//  GRAFICO
//******************************************
if ($_REQUEST['fn'] == 'open'){


?>


{"success":true, "items": [
		 {
			xtype: 'container',
			title: 'Elenco To Do',
        	    <?php echo make_tab_closable(); ?>,
	 		layout: {
				type: 'hbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [
			
			
				{			
				xtype: 'panel',
				flex: 25,
				layout: 'fit',
				items: [
				
					{
						xtype: 'grid',
						title: '',
					    loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad:true,
			                 proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   timeout: 2400000,
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							         extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>,
										 desktop : 'DBD'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							fields: [
							'todo', 'g_totale', 'r_totale',  'a_totale', 'ut_ass', 'data_ass', 'desk',
							'data_eva', 'min_data_open', 'max_data_open', 'ut_open', 'nr_10', 'nr_30'
							]
							
		        									
				}, //store
				columns: [ {
        	                header   : 'Desk.',
        	                dataIndex: 'desk',
        	                width: 40,
        	                filter: {type: 'string'}, filterable: true
        	                },{
        	                header   : '',
        	                dataIndex: 'todo',
        	                flex : 1,
        	                filter: {type: 'string'}, filterable: true
        	                },
				        
        	                {header: 'Generati',
                        	 columns: [
                        	  {header: 'Nr', dataIndex: 'g_totale', width: 40}
                        	 ,{header: 'Data', dataIndex: 'data_ass', width: 70, renderer: date_from_AS}
                        	 ,{header: 'Utente', dataIndex: 'ut_ass', width: 70}
                        	
                        	 ]},
                        	 {header: 'Aperti',
                        	 columns: [
                        	  {header: 'Nr', dataIndex: 'a_totale', width: 40}
                        	 ,{header: 'Data min', dataIndex: 'min_data_open', width: 70, renderer: date_from_AS}
                        	 ,{header: 'Data max', dataIndex: 'max_data_open', width: 70, renderer: date_from_AS}
                        	 ,{header: 'Utente', dataIndex: 'ut_open', width: 70}
                        	 ,{header: 'Oltre 10 gg', dataIndex: 'nr_10', width: 80}
                        	 ,{header: 'Oltre 30 gg', dataIndex: 'nr_30', width: 80}
                        	 ]},
                        	 {header: 'Evasi',
                        	 columns: [
                        	  {header: 'Nr', dataIndex: 'r_totale', width: 40}
                        	 ,{header: 'Data', dataIndex: 'data_eva', width: 70,  renderer: date_from_AS}
                        	 ]}
				
			      	
			       
	         ], dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                    xtype: 'button',
                    text: 'DESKTOP ORDINI CLIENTI',
                    style:'border: 1px solid gray;', 
		            scale: 'large',	               
		            width : 200,     
		            margin : '0 0 0 35',       
					//iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	 
	       			  var grid = this.up('grid');
	       			  grid.store.proxy.extraParams.desktop = 'DOV';
 			          grid.getStore().load();
		              
		              var c_aperti = Ext.getCmp('m-panel').down('#todo_aperti');
		              c_aperti.store.proxy.extraParams.desktop = 'DOV';
 			          c_aperti.getStore().load();
 			          
 			          var c_evasi = Ext.getCmp('m-panel').down('#todo_evasi');
		              c_evasi.store.proxy.extraParams.desktop = 'DOV';
 			          c_evasi.getStore().load();
 			          
 			          var c_ut_gen = Ext.getCmp('m-panel').down('#utente_generazione');
		              c_ut_gen.store.proxy.extraParams.desktop = 'DOV';
 			          c_ut_gen.getStore().load();
 			          
 			          var c_ut_ass = Ext.getCmp('m-panel').down('#utente_assegnato');
		              c_ut_ass.store.proxy.extraParams.desktop = 'DOV';
 			          c_ut_ass.getStore().load();
		              
		              }

			     },
			        {
                     xtype: 'button',
                    text: 'DESKTOP BASE DATI',
                    style:'border: 1px solid gray;', 
		            scale: 'large',	 
		            width : 200,     
		            margin : '0 50 0 50',               
					//iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var grid = this.up('grid');
 			          grid.store.proxy.extraParams.desktop = 'DBD';
 			          grid.getStore().load();    
 			          
		              var c_aperti = Ext.getCmp('m-panel').down('#todo_aperti');
		              c_aperti.store.proxy.extraParams.desktop = 'DBD';
 			          c_aperti.getStore().load();
 			          
 			          var c_evasi = Ext.getCmp('m-panel').down('#todo_evasi');
		              c_evasi.store.proxy.extraParams.desktop = 'DBD';
 			          c_evasi.getStore().load();
 			          
 			          var c_ut_gen = Ext.getCmp('m-panel').down('#utente_generazione');
		              c_ut_gen.store.proxy.extraParams.desktop = 'DBD';
 			          c_ut_gen.getStore().load();
 			          
 			          var c_ut_ass = Ext.getCmp('m-panel').down('#utente_assegnato');
		              c_ut_ass.store.proxy.extraParams.desktop = 'DBD';
 			          c_ut_ass.getStore().load(); 
		           }

			     },
			        {
                     xtype: 'button',
                    text: 'DESKTOP ACQUISTI',
                    style:'border: 1px solid gray;', 
		            scale: 'large',	    
		            width : 200,     
		            margin : '0 50 0 0',               
					//iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var grid = this.up('grid');
 			          grid.store.proxy.extraParams.desktop = 'DAC';
 			          grid.getStore().load();  
 			          
 			          var c_aperti = Ext.getCmp('m-panel').down('#todo_aperti');
		              c_aperti.store.proxy.extraParams.desktop = 'DAC';
 			          c_aperti.getStore().load();
 			          
 			          var c_evasi = Ext.getCmp('m-panel').down('#todo_evasi');
		              c_evasi.store.proxy.extraParams.desktop = 'DAC';
 			          c_evasi.getStore().load();
 			          
 			          var c_ut_gen = Ext.getCmp('m-panel').down('#utente_generazione');
		              c_ut_gen.store.proxy.extraParams.desktop = 'DAC';
 			          c_ut_gen.getStore().load();
 			          
 			          var c_ut_ass = Ext.getCmp('m-panel').down('#utente_assegnato');
		              c_ut_ass.store.proxy.extraParams.desktop = 'DAC';
 			          c_ut_ass.getStore().load();
 			          
		              }

			     },
			        {
                     xtype: 'button',
                    text: 'DESKTOP AMMINISTRAZIONE',
                    style:'border: 1px solid gray;', 
		            scale: 'large',	
		            width : 200,                     
					//iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var grid = this.up('grid');
 			          grid.store.proxy.extraParams.desktop = 'DAM';
 			          grid.getStore().load();
 			          
		              var c_aperti = Ext.getCmp('m-panel').down('#todo_aperti');
		              c_aperti.store.proxy.extraParams.desktop = 'DAM';
 			          c_aperti.getStore().load();
 			          
 			          var c_evasi = Ext.getCmp('m-panel').down('#todo_evasi');
		              c_evasi.store.proxy.extraParams.desktop = 'DAM';
 			          c_evasi.getStore().load();
 			          
 			          var c_ut_gen = Ext.getCmp('m-panel').down('#utente_generazione');
		              c_ut_gen.store.proxy.extraParams.desktop = 'DAM';
 			          c_ut_gen.getStore().load();
 			          
 			          var c_ut_ass = Ext.getCmp('m-panel').down('#utente_assegnato');
		              c_ut_ass.store.proxy.extraParams.desktop = 'DAM';
 			          c_ut_ass.getStore().load();
 			          
			          }

			     }
			     ]
		   }], 
		   
		   viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			           
			           return '';																
			         }   
			    }
	
		   }
				
				]
			}
			
		, {
			xtype: 'container',
			width: 300,
			title: '',
	 		layout: {
				type: 'vbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [
					{
					 xtype:  'tabpanel',
					 height: 230,
					 items: [
                        <!-- --------------------- -->
                        <!-- grafico  ToDo aperti -->
                        <!-- --------------------- -->  
    					{
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'ToDo aperti',					 
    					 items: 	  				  		
    						{
    			            xtype: 'chart',
    			            animate: true,
    			            id: 'todo_aperti',
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_todo',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {
    								open_request: <?php echo acs_raw_post_data(); ?>, 
    								attivi : 'Y',
    								desktop : 'DBD'},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    								
    							},
    		
    							fields: ['codice', 'name', 'referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			           /* legend: {
    			            	field: 'name',
    			                position: 'right'
    			            },*/
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial',
    								renderer: function (label){
    									// this will change the text displayed on the pie
    									var cmp = Ext.getCmp('todo_aperti'); // id of the chart
    									var index = cmp.store.findExact('name', label); // the field containing the current label
    									var data = cmp.store.getAt(index).data;
    									return data.codice; // the field containing the label to display on the chart
    								}			                    
    			                }
    			            }]
    			        }	  			
    	  			}
                        <!-- ---------------------------------- -->
                        <!-- grafico TODO evasi -->
                        <!-- ---------------------------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'ToDo evasi',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'todo_evasi',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_todo',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {
    								 open_request: <?php echo acs_raw_post_data(); ?>,
    								 attivi : 'N',
    								 desktop : 'DBD'
    								},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['codice', 'name', 'referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            /*legend: {
    			                position: 'right'
    			            },*/
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                  label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial',
    								renderer: function (label){
    									// this will change the text displayed on the pie
    									var cmp = Ext.getCmp('todo_evasi'); // id of the chart
    									var index = cmp.store.findExact('name', label); // the field containing the current label
    									var data = cmp.store.getAt(index).data;
    									return data.codice; // the field containing the label to display on the chart
    								}			                    
    			                }
    			            }]
    			        }	 	  			
    	  			} 
				] <!-- end items tab panel  -->
			   } <!-- end tab panel  -->
				  , {
					 xtype:  'tabpanel',
					 height: 230,
					 items: [
					   
                        <!-- ----------------- -->
                        <!-- utente immissione -->
                        <!-- ----------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Utente di generazione',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'utente_generazione',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_utenti',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {
    								  open_request: <?php echo acs_raw_post_data(); ?>, 
    								  immissione : 'Y',
    								  desktop : 'DBD'
    								  },
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            /*legend: {
    			                position: 'right'
    			            },*/
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end utente immissione -->				
                        <!-- --------- -->
                        <!-- Fornitore -->
                        <!-- --------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Utente assegnato',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'utente_assegnato',			            
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_utenti',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {
    								    open_request: <?php echo acs_raw_post_data(); ?>, 
    								    assegnato : 'Y',
    								    desktop : 'DBD'
    								    },
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            shadow: true,
    			            /*legend: {
    			                position: 'right'
    			            },*/
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end utente fornitore -->				
					 
				] <!-- end items tab panel  -->
			   } <!-- end tab panel  -->
			
			]
		}				
			
			]
        }
      
	]
}

<?php 
exit;
}?>