<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

function evidenza_n_articolo($file, $flag, $articolo){
    
    global $conn, $id_ditta_default, $auth, $cfg_mod_DeskUtility;
    
    if($file == 'AX'){
        
        $sql_a = "SELECT COUNT (*) AS C_ROW
                  FROM {$cfg_mod_DeskUtility['file_est_anag_art']} AX
                  WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$articolo}'";
     
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_a);
        
        $row = db2_fetch_assoc($stmt_a);
    
        $ar_ins = array();
        
        $ar_ins['AXUSGE'] 	= trim($auth->get_user());
        $ar_ins['AXDTGE']   = oggi_AS_date();
        $ar_ins['AXTMGE'] 	= oggi_AS_time();
        $ar_ins['AXSFON'] 	= $flag;
        
        if($row['C_ROW'] == 0){
            
            
            $ar_ins['AXDT'] 	= $id_ditta_default;
            $ar_ins['AXART'] 	= $articolo;
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art']}(" . create_name_field_by_ar($ar_ins) . ")
			    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
            
            
        }else{
            
                        
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art']} AX
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$articolo}' ";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
        }
        
        
    }else{
        
        
        $sql_a = "SELECT COUNT (*) AS C_ROW
        FROM {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
        WHERE AUDT = '{$id_ditta_default}' AND AUART = '{$articolo}'";
        
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_a);
        
        $row = db2_fetch_assoc($stmt_a);
              
        $ar_ins = array();
        
        $ar_ins['AUUSUM'] 	= trim($auth->get_user());
        $ar_ins['AUDTUM']   = oggi_AS_date();
        $ar_ins['AUTMUM'] 	= oggi_AS_time();
        $ar_ins['AUSFAR'] 	= $flag;
        $ar_ins['AUUTEN'] 	= trim($auth->get_user());
        
        if($row['C_ROW'] == 0){
                        
            $ar_ins['AUUSGE'] 	= trim($auth->get_user());
            $ar_ins['AUDTGE']   = oggi_AS_date();
            $ar_ins['AUTMGE'] 	= oggi_AS_time();
            $ar_ins['AUDT'] 	= $id_ditta_default;
            $ar_ins['AUART'] 	= $articolo;
           
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art_utente']}(" . create_name_field_by_ar($ar_ins) . ")
			         VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
            
            
        }else{
            
                        
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE AUDT = '{$id_ditta_default}' AND AUART = '{$articolo}' ";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
        }
        
    }

}


if ($_REQUEST['fn'] == 'exe_duplica'){
    
    $list_rows = $m_params->list_rows_modified;
            
    foreach($list_rows as $v){
        
        $old_cod = $v->cod;
        $new_cod = $v->n_cod;
        
        if($m_params->form_values->f_note == 'Y'){
        
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'DUP_ANAG_NOTE',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default,
                    ),
                )
                );
        }
        
        
            
            foreach($m_params->form_values as $k => $v){
                
                if(substr($k, 0, 6) == 'scheda'){
                    $ar_value = explode("_", $k);
                    $scheda = $ar_value[1];
                    
                $sh = new SpedHistory($desk_art);
                $sh->crea(
                    'pers',
                    array(
                        "messaggio"	=> 'DUP_ANAG_SCHE',
                        "vals" => array(
                            "RICDOLD" 	=> $old_cod,
                            "RICDNEW" 	=> $new_cod,
                            "RIDT"  	=> $id_ditta_default,
                            "RIDART"  	=> $scheda
                        ),
                    )
                    );
                
                } 
            }
         
        
        if($m_params->form_values->f_trad == 'Y'){
            
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'DUP_ANAG_TRAD',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default
                    ),
                )
                );
            
        }
        
        if($m_params->form_values->f_az_ft == 'Y'){
            
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'AZZ_FORN_TERZ',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default
                    ),
                )
                );
            
        }
        
        if($m_params->form_values->f_az_gg == 'Y'){
            
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'AZZ_LEAD_GG',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default
                    ),
                )
                );
            
        }
        
        if($m_params->form_values->f_nazi == 'Y'){
            
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'AZZ_NAZI_ORG',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default
                    ),
                )
                );
            
        }
        
        if($m_params->form_values->f_intra == 'Y'){
            
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'AZZ_INTRA_VEN',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default
                    ),
                )
                );
            
        }
        
        if($m_params->form_values->f_var == 'Y'){
            
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'DUP_ANAG_VAR',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default
                    ),
                )
                );
            
        }
        
        if($m_params->form_values->f_prop == 'Y'){
            
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'DUP_ANAG_PROP',
                    "vals" => array(
                        "RICDOLD" 	=> $old_cod,
                        "RICDNEW" 	=> $new_cod,
                        "RIDT"  	=> $id_ditta_default
                    ),
                )
                );
            
        }
        
        
    }
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'exe_conferma_mod'){

    $list_rows = $m_params->list_rows_modified;
    $ar_control = array();
    $error_cod = array();
        
    foreach($list_rows as $v){
             
        $new_cod = $v->n_cod;
     
        //CONTROLLO CODICI INSERITI UGUALI
        $array_cod = array();
        foreach($list_rows as $cod){
            $array_cod[] = $cod->n_cod;
        }
        $array_count = array_count_values($array_cod);
        if (array_key_exists($new_cod, $array_count) && ($array_count["$new_cod"] > 1))
        {
            $error_cod['c_art'] = $new_cod;
            $error_cod['msg'] = "Hai inserito pi� volte lo stesso codice";
            $error_cod['red'] = "Y";
            
        }
        
        //CONTROLLO CODICE ARTICOLO ESISTENTE
        $sql = "SELECT COUNT(*) AS C_ROWS
        FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
        WHERE ARART = '{$new_cod}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if($row['C_ROWS'] > 0){
            
            $error_cod['c_art'] = $new_cod;
            $error_cod['msg'] = "Codice articolo gi&agrave; esistente";
            $error_cod['red'] = "Y";
            
        }
        
        $ar_control[] = $error_cod;

    }
    
    if($m_params->verifica == 'Y'){
        
        if(count($ar_control[0]) > 0)
             echo acs_je($ar_control);
        else{ 
            $ret = array();
            $ret['corretto'] = 'Y';
            echo acs_je($ret);
        }
    }else{
        
        
        if(count($ar_control[0]) > 0){
            
            $ret = array();
            $ret['success'] = false;
            $ret['error_msg'] = "Codici articoli non corretti, verificare";
            echo acs_je($ret);
            
            
        }else{
            
            
            
            foreach($list_rows as $v){
              
            $sh = new SpedHistory($desk_art);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'DUP_ANAG_ARTM',
                    "vals" => array(
                        "RICDOLD" 	=> $v->cod,
                        "RICDNEW" 	=> $v->n_cod,  
                        "RIDART" 	=> $v->n_desc,
                        "RIDT"  	=> $id_ditta_default,
                    ),
                )
                );
            
            evidenza_n_articolo('AU', $m_params->evid_u, $v->n_cod);
            evidenza_n_articolo('AX', $m_params->evid_g, $v->n_cod);
       
                       
            //creo la riga in WPI2AS0
            if(strlen($m_params->todo) > 0){
                $todo =	strtr($m_params->todo, array('\"' => '"'));
                $todo =	json_decode($todo);
                
                $na = new SpedAssegnazioneArticoli($desk_art);
                $na->crea('POSTM', array(
                    'k_ordine' => $v->n_cod,
                    'form_values' => (array)$todo
                ));
            
            }
            
            
            
            $ar_ins = array();
            if (!isset($m_params->ciclo))            
                $ar_ins['ARESAU'] = '';
            else 
                $ar_ins['ARESAU'] = $m_params->ciclo;
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_anag_art']} AR
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$v->n_cod}' ";            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
            
            
            }
            
            
            
            $ret = array();
            $ret['success'] = true;
            echo acs_je($ret);
        }
  
    }
    
    
    exit;
}

if ($_REQUEST['fn'] == 'open_mod'){

    ?>
    {"success":true, "items": [
    
 			{
				xtype: 'grid',
				title: '',
		        loadMask: true,	
		        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      	],
		      	
		      	
		      	confirm_duplica: function(grid, list_rows_modified, ciclo, evid_g, evid_u, todo, b_bar, t_confirm, loc_win, count_i, with_error){
				  		
				  		//primo richiamo: creo la progress bar e inizio
				  		if (count_i === undefined) {
				  		
				  			loc_win.down('grid').down('toolbar').hide();				  		
				  		
    				  		//Apro la progress bar
    				  		var pb = Ext.create('Ext.ProgressBar', {                           
                               text: 'Elaborazione in corso',
                               autoRender: true,
                               alwaysOnTop: true,
                               value: 0.0,
                               renderTo: loc_win.id
                               //renderTo: Ext.getBody()
                            });
                            loc_win.pb = pb;
                            count_i = 0;
                            with_error = false;
                        }
                        
                        var i = count_i;
				  		//eseguo una richiesta per ogni articolo
			  			if (count_i < list_rows_modified.length){			  			 
			  			 	 loc_win.pb.updateText('Duplica: ' +  list_rows_modified[i].cod + ' -> ' + list_rows_modified[i].n_cod);			  			 	 
			  			 
    				  		 Ext.Ajax.request({
    				  		 		//async: false, //non funziona correttamente in Chrome
    						        url        : 'acs_anag_art_duplica_ass.php?fn=exe_conferma_mod',
    						        method     : 'POST',
    			        			jsonData: {
    			        				list_rows_modified : [list_rows_modified[i]],
    			        				evid_g : evid_g,
    			        				evid_u : evid_u,
    			        				todo : todo,
    			        				ciclo : ciclo
    			        				
    								},							        
    						        success : function(result, request){
    						           var jsonData = Ext.decode(result.responseText);
    						           
    						           if(jsonData.success == false){
    						           	    acs_show_msg_error(jsonData.error_msg);
    						           	    with_error = true;
    						            } else {    						            
    						            	loc_win.pb.updateProgress((i+1) / list_rows_modified.length);
    						            	
    						            	//fine ciclo articoli
    						            	if (count_i == list_rows_modified.length - 1){
                            					    if (!with_error){
                            					    	loc_win.down('grid').down('toolbar').show();
                            					    	loc_win.pb.destroy();
                            					    	loc_win.destroy();					                                							
                                        				acs_show_win_std('Conferma dati accessori da duplicare', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_win', 
                                		  				   {list_rows_modified : list_rows_modified, multi : 'Y'}, 500, 400, null, 'icon-leaf-16');
                                		  			}
    						            	} else {
    						            		//duplica articolo successivo
    						            		grid.confirm_duplica(grid, list_rows_modified, ciclo, evid_g, evid_u, todo, b_bar, t_confirm, loc_win, count_i + 1, with_error);
    						            	}
    						           }
    						          							        
    						        },
    						        failure    : function(result, request){
    						        	with_error = true;
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });			  			 
			  			 
			  			} //per ogni articolo
					    
	
				  			  		
				 },
		        store: {
				xtype: 'store',
				autoLoad:true,
				data : [
					
				    <?php foreach($m_params->list_selected_art as $v){
				        $desc = html_entity_decode(utf8_decode($v->d_art));
				    ?>
				 
		 			 
							{cod : '<?php echo $v->c_art; ?>', 
							 n_cod : '<?php echo $v->c_art; ?>',  
							 len : '<?php echo strlen($v->c_art); ?>',
							 n_desc : <?php echo j($desc); ?>, 
							 n_len : <?php echo mb_strlen($desc); ?>,	//NO strlen(), altrimenti i caratteri speciali vengono considerati 2 volte
							 sosp : '<?php echo $v->sosp; ?>',
							 esau : '<?php echo $v->esau; ?>',  
							 dclick : '',
							},

			  		<?php } ?>
			  		
			  		   ],
					
        			fields: ['cod', 'n_cod', 'n_desc', 'len', 'n_len', 'red', 'dclick', 'sosp', 'esau']							
							
	}, //store
		

	      columns: [
	
            {
            header   : 'Codice',
            dataIndex: 'cod',
            width : 120
            },
            {
            header   : 'Nuovo codice',
            dataIndex: 'n_cod',
             width : 120,
                 renderer: function(value, p, record){
	    	       if (record.get('len') > 15)
				     p.tdCls += ' sfondo_rosso';
    		    
    		        return value;
    		 },
            editor: {
	                xtype: 'textfield',
	                allowBlank: true
	            }
            },
            {
            header   : 'Lung.',
            dataIndex: 'len',
            width : 40
            }, {
            header   : 'Nuova descrizione',
            dataIndex: 'n_desc',
            flex: 1,
            renderer: function(value, p, record){
	    	       if (record.get('n_len') > 50)
				     p.tdCls += ' sfondo_rosso';
    		    
    		        return Ext.util.Format.htmlEncode(value);
    		 },
            editor: {
	                xtype: 'textfield',
	                allowBlank: true
	            }
	            
            },
            {
            header   : 'Lung.',
            dataIndex: 'n_len',
            width : 40
            },
     ], listeners: {
     
 			beforeedit: function(editor, e, a, b){
							var record= e.record;	
							var col = e.column.dataIndex;
							if(record.get('dclick') == 'Y')
								return false;
					}, 
     		edit: function(editor, e, a, b){
                 
              	grid = editor.grid;
        		record= e.record;	
        		new_value =  record.data.n_cod;
        		old_value =  record.data.cod;
        		record.set('len', record.data.n_cod.length);
        		record.set('n_len', record.data.n_desc.length);
        		
        		if(new_value == old_value)
        		     record.commit();
         
        }
        
        ,celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	
					  	if(col_name == 'n_cod'){			
					   		 acs_show_win_std('Dettaglio articolo', 'acs_win_dettaglio_articoli.php?fn=open_win', 
	    			  	     {c_art : rec.get('n_cod'), d_art : rec.get('n_desc')}, 700, 600, null, 'icon-leaf-16');
	    			  								   		
					   		}
					  	
					  	}
					  	
		}
     
     }, 
		 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
        			name: 'f_text',
        			xtype: 'displayfield',
        			margin : '5 5 5 5',
        			value: 'Duplica eseguita correttamente',
        			itemId : 't_confirm',
        			hidden : true,
        			flex : 1,
        			style:{ "text-align": 'center'}
    			   },
                
                {
            	xtype: 'container',
            	itemId : 'b_bar',
            	layout: {
            		type: 'hbox'				
            	},
            	items: [
            	
            	{
                        xtype: 'form',
                        itemId : 'f_sos',
                    	frame: true,
                 		items: [
                 		{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						margin : '0 0 0 65',
						items: [
						<?php for ($i = 1; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', width : 18, fieldStyle:'font-size:10px;'},
						<?php }?>	
						 ]
					},   { 
						xtype: 'fieldcontainer',
						margin : '0 0 0 0',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},
					    defaults: {xtype: 'textfield', width: 16, margin : '0 2 0 0', maxLength: 1},
						items: [
							{xtype: 'label', text: 'Sostituisci', width: 60},    						
    						<?php for ($i = 1; $i<=15; $i++){?>
    							{ name: 'f_controlla_<?php echo $i; ?>',
    							  listeners: {
            						'change': function(field){
            						
            						 value = this.getValue().toString();
            						if (value.length == 1) {
                                        var nextfield = field.nextSibling();
                                        nextfield .focus(true, 0);
                                      } 
                                      
                                      field.setValue(field.getValue().toUpperCase());
              							
            					  }
			 					}
    						  },
    						<?php }?>
            						
        						]						
    						},                 		
                 		
    						{ 
        						xtype: 'fieldcontainer',
        						margin : '0 0 0 0',
        						layout: { 	type: 'hbox',
        								    pack: 'start',
        								    align: 'stretch',
        								    frame: true},
        					    defaults: {xtype: 'textfield', width: 16, margin : '0 2 0 0', maxLength: 1},
        						items: [    	
        							{xtype: 'label', text: 'Con', width: 60},    			
            						<?php for ($i = 1; $i<=15; $i++){?>
            							{ name: 'f_sostituisci_<?php echo $i; ?>',
            							  m_values : [],
            							  listeners: {
                    						'change': function(field){
                    						
                    						 value = this.getValue().toString();
                    						if (value.length == 1) {
                                                var nextfield = field.nextSibling();
                                                nextfield .focus(true, 0);
                                              } 
                                              
                                              field.setValue(field.getValue().toUpperCase());
                      							
                    					  },
                    					  afterRender: function( component ) {
								            	//m_form = this.up('form').getForm();
								                component.getEl().on('dblclick', function( event, el ) {
												
											    var my_listeners_search_loc = {											    
						        					afterConfirm: function(from_win, form_values){
						        					        component.m_values = form_values;
						        					          
					        					            if(!Ext.isEmpty(form_values.f_stringa) || 
					        					            	!Ext.isEmpty(form_values.f_pos) ||
					        					            	!Ext.isEmpty(form_values.f_rimuovi)
					        					            	){
					        					              el.style = 'background-color: #F9BFC1; background-image: none;';
					        					              el.value = '*';
					        					             } else {
																if(!Ext.isEmpty(form_values.f_char)) {
						        					          		component.setValue(form_values.f_char);
						        					          		el.style = '';
						        					         	} else {
						        					         		//nessuna scelta
						        					         		component.setValue('');
						        					          		el.style = '';
						        					         	}					        					             
					        					             }
							        						
							        						from_win.close();
											        	}										    
											    }
											    
												acs_show_win_std(null, 
													'acs_anag_art_duplica_ass_avanzata.php?fn=open_form', 
													{ m_values: component.m_values}, 450, 250, my_listeners_search_loc);
												});										            
								             }
					 					}
            							 },
            						<?php }?>
            						
        							         							    						
        						]						
    						}
    						                 		
                 		]
                 		
						 
                 		
					},   
					   
            	
            	 /*{ 
				xtype: 'fieldcontainer',
				layout: { 	type: 'vbox',
						    pack: 'start',
						    align: 'stretch'},						
				items: [
				
				 {
        			name: 'f_sos',
        			xtype: 'textfield',
        			fieldLabel: 'Sostituisci',
        			itemId : 'w_sos',
        			labelWidth : 50,
        			width : 170
    			   },  {
        			name: 'f_con',
        			xtype: 'textfield',
        			fieldLabel: 'Con',
        			itemId : 'w_con',
        			labelWidth : 50,
        			width : 150
        									
        		 	}
				   
				]},*/
				
				{
        			name: 'f_evid_g',
        			itemId : 'evid_g',
        			xtype: 'hiddenfield'
        								
        		 	},{
        			name: 'f_evid_u',
        			itemId : 'evid_u',
        			xtype: 'hiddenfield'
        								
        		 	},{
        			name: 'f_todo',
        			itemId : 'todo',
        			xtype: 'hiddenfield'
        								
        		 	}, {
                     xtype: 'button',
                    text: 'Applica',
					margin : '5 5 5 5',
                    iconCls: 'icon-button_black_play-32',
		            scale: 'large',	      
		             handler: function() {
		             
                         //recupero gli item dello store e sostituisco i caratteri come richiesto
			            	var loc_grid = this.up('window').down('grid'),
			            		sostituisci_values = this.up('window').down('#f_sos').getValues(),
			            	    all_rec = loc_grid.getStore().getRange();
						       
			            	for (var i=0; i<all_rec.length; i++) {
			            		var m_rec = all_rec[i];
			            		if (!Ext.isEmpty(m_rec.get('cod'))){
    			            		var cod_iniziale = m_rec.get('cod'),
    			            			new_cod = '';
    			            			
									//controllo se in base a "controlla" va modifica
									var da_modificare = true;
									for (var j=1; j<=15; j++) {
									    if (
									    		!Ext.isEmpty(sostituisci_values['f_controlla_' + j]) &&
									    		(cod_iniziale.length < j ||
    			            					 cod_iniziale.substr(j-1, 1) != sostituisci_values['f_controlla_' + j]
    			            					)
                    			           )
    			            			da_modificare = false;
									}
									
    			            		if (da_modificare == true){	
        			            		for (var j=1; j<=15; j++) {
        			            			var s_field = this.up('window').down('#f_sos').getForm().findField('f_sostituisci_' + j);	
        			            		                			 
        			            		    //if (new_cod.length == j-1){
        			            		    if (1 == 1) {
        			            		    	if (Ext.isEmpty(s_field.m_values)){
        			            		    		if (Ext.isEmpty(sostituisci_values['f_sostituisci_' + j]))
                			            				new_cod = new_cod + cod_iniziale.substr(j-1, 1); 
                			            			else 
                			            				new_cod = new_cod + sostituisci_values['f_sostituisci_' + j];
                			            		} else {
                			            			//gestione avanzata sostituzione
                			            			if (!Ext.isEmpty(s_field.m_values.f_char))
                			            				new_cod = new_cod + s_field.m_values.f_char;
                			            			else if (!Ext.isEmpty(s_field.m_values.f_stringa))
                			            				new_cod = new_cod + s_field.m_values.f_stringa;
                			            			else if (!Ext.isEmpty(s_field.m_values.f_rimuovi))
                			            				new_cod = new_cod;
                			            			else if (!Ext.isEmpty(s_field.m_values.f_pos))
                			            				new_cod = new_cod + cod_iniziale.substr(s_field.m_values.f_pos-1, 1);                			            				
                			            			else
                			            				new_cod = new_cod + cod_iniziale.substr(j-1, 1); 
                			            		}
            			            		} //if
            			            			
        			            		} //for
        			            		                        			                  
									  m_rec.set('n_cod', new_cod);                        			            		
									}	
    			            	}		
			            	}
                        } //handler
		            }, 
		            
		            
		            { 
						xtype: 'fieldcontainer',
						margin : '5 10 0 10',
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
			                xtype:'splitbutton',
			                text: 'Evidenza generica&nbsp;&nbsp;&nbsp;',
			                scale: 'small',
			                arrowAlign:'right',
			                handler: function(){
        	                 this.maybeShowMenu();
        	           			},
			                menu: {
			                	xtype: 'menu',
		        				items: [	
		        				  	
						{
							xtype: 'button',
				            text: 'Rosso',
							//iconCls : 'icon-sub_blue_accept-16',
							style: 'background-color: #F9BFC1;', 
							ui:'plain',
				            scale: 'small',
				            handler: function() {
				                 this.up('window').down('#evid_g').setValue('1');
				                 this.up('menu').hide();
				            	 
				            }
				        },
						
						{
							xtype: 'button',
				            text: 'Giallo',
							//iconCls : 'icon-sub_blue_accept-16',
                            style: 'background-color: #F4E287;',
                            ui:'plain',
				            scale: 'small',
				            handler: function() {
				            	 this.up('window').down('#evid_g').setValue('2');
				            	 this.up('menu').hide();
				            }
				        },
						{
							xtype: 'button',
				            text: 'Verde',
							//iconCls : 'icon-sub_blue_accept-16',
                            style: 'background-color: #A0DB8E;',
                            ui:'plain',
				            scale: 'small',
				            handler: function() {
				            	this.up('window').down('#evid_g').setValue('3');
				            	this.up('menu').hide();
				            	 
				            }
				        }, {
							xtype: 'button',
				            text: 'Nessuno',
							//iconCls : 'icon-sub_blue_accept-16',
							ui:'plain',
				            scale: 'small',
				            handler: function() {
				            	 this.up('window').down('#evid_g').setValue('');
				            	 this.up('menu').hide();
				            }
				        },
		        				 ]
			                
			                }
			            }, 
			            
			            {
			                xtype:'splitbutton',
			                text: 'Evidenza per utente',
			                scale: 'small',
			                arrowAlign:'right',
			                handler: function(){
        	                 this.maybeShowMenu();
        	           			},
			                menu: {
			                	xtype: 'menu',
		        				items: [	
		        				  	
						{
							xtype: 'button',
				            text: 'Rosso',
							//iconCls : 'icon-sub_blue_accept-16',
                            style: 'background-color: #F9BFC1;', 
                            ui:'plain',
				            scale: 'small',
				            handler: function() {
				                 this.up('window').down('#evid_u').setValue('1');
				                 this.up('menu').hide();
				                 
				            	 
				            }
				        },
						
						{
							xtype: 'button',
				            text: 'Giallo',
							//iconCls : 'icon-sub_blue_accept-16',
                            style: 'background-color: #F4E287;',
                            ui:'plain',
				            scale: 'small',
				            handler: function() {
				            	 this.up('window').down('#evid_u').setValue('2');
				            	 this.up('menu').hide();
				            }
				        },
						{
							xtype: 'button',
				            text: 'Verde',
							//iconCls : 'icon-sub_blue_accept-16',
                            style: 'background-color: #A0DB8E;',
                            ui:'plain',
				            scale: 'small',
				            handler: function() {
				            	this.up('window').down('#evid_u').setValue('3');
				            	this.up('menu').hide();
				            	 
				            }
				        }, {
							xtype: 'button',
				            text: 'Nessuno',
							//iconCls : 'icon-sub_blue_accept-16',
							ui:'plain',
				            scale: 'small',
				            handler: function() {
				            	 this.up('window').down('#evid_u').setValue('');
				            	 this.up('menu').hide();
				            }
				        },
		        				 ]
			                
			                }
			            }
						
						   
						]}, 
						
						
						 {
                     xtype: 'button',
                    text: '',
		            iconCls: 'icon-arrivi-32',
		            margin : '5 10 0 10',
		            scale: 'large',	                     
                     handler: function() {
                      var grid = this.up('grid');
                      var rows_modified = grid.getStore().getUpdatedRecords();
                      list_rows_modified = [];
                            
                      for (var i=0; i<rows_modified.length; i++) {
		              	list_rows_modified.push(rows_modified[i].data.n_cod);
		              }
		              
		               var todo = this.up('window').down('#todo');
                     
                     	my_listeners_inserimento = {
    						afterFromDuplica: function(from_win, form_values){	
    						    todo.setValue(JSON.stringify(form_values));
    						    
        						from_win.close();
			        		}
	    				};
		              
                   		acs_show_win_std('Nuovo stato/attivit&agrave;', 
						<?php echo j('acs_anag_art_create_attivita.php'); ?>, 
						{	tipo_op: 'ANART',
							rif : 'ART',
			  				list_selected_id: list_rows_modified
			  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
                	
			
			            }

			     }, 
					
					 {
					name: 'f_ciclo',
					itemId : 'ciclo',
					xtype: 'combo',
					labelWidth : 70,
					margin : '5 75 5 5',
					width : 200,
					fieldLabel: 'Ciclo di vita',
					forceSelection: true,								
					displayField: 'text',
					valueField: 'id',								
					emptyText: '- seleziona -',
			   		//allowBlank: false,	
			   	   anchor: '-15',
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'}],
					    data: [						
					    <?php echo acs_ar_to_select_json($desk_art->find_TA_std('AR012', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
					    ]
					},
					
					queryMode: 'local',
        			minChars: 1, 	
					listeners: { 
					 	beforequery: function (record) {
			         	record.query = new RegExp(record.query, 'i');
			         	record.forceAll = true;
		             }
		          }
					
			 }	, {
                     xtype: 'button',
                    text: 'Reset',
                    margin : '5 0 5 0',
		            iconCls: 'icon-button_blue_repeat-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                   		var grid = this.up('grid');                   		
                   		this.up('window').down('#w_sos').setValue('');
                   		this.up('window').down('#w_con').setValue('');
                   		grid.getStore().load();
                	
			
			            }

			     },  {
                     xtype: 'button',
                    text: 'Verifica',
                     margin : '5 5 5 5',
		            iconCls: 'icon-help_black-32',
		            scale: 'large',	                     

		           handler: function() {
		           
                   	 var grid = this.up('grid');
                   	 var loc_win = this.up('window');
                  
                      rows_modified = grid.getStore().getUpdatedRecords();
                
                      list_rows_modified = [];
                            
                      for (var i=0; i<rows_modified.length; i++) {
		              	list_rows_modified.push(rows_modified[i].data);
		              }
		              
		               for (var i=0; i<rows_modified.length; i++) {
		               
		               if(rows_modified[i].get('len') == 0){
		              	 	acs_show_msg_error('Inserire un codice articolo');
	 					 	return;
	 					 }
		               
		              	if(rows_modified[i].get('len') > 15){
		              	 	acs_show_msg_error('Il codice articolo deve essere massimo di 15 caratteri');
	 					 	return;
	 					 }
	 					 
	 					  if(rows_modified[i].get('n_len') > 50){
		              	 	acs_show_msg_error('La descrizione articolo deve essere massimo di 50 caratteri');
	 					 	return;
	 					 }
	 					 
	 					} 
	 					 
	 					          
					   Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_mod',
						        method     : 'POST',
			        			jsonData: {
			        				list_rows_modified : list_rows_modified,
			        				verifica : 'Y'
								},							        
						        success : function(result, request){
						           var jsonData = Ext.decode(result.responseText);
						          
						           if(jsonData.corretto == 'Y'){
						           
						           grid.getStore().each(function(record){
						           
						                  if(record.get('red') == 'Y')
                                          	record.set('red', 'N');
                                          
                                    });
                                    
                                    acs_show_msg_info('Verifica terminata con successo');
                                    
						           
						           }else{
						            var error_msg = "";
						            var c_art = "";
						            for (var i=0; i<jsonData.length; i++) {
						                
						               if(rows_modified[i].get('n_cod') == jsonData[i].c_art){
						                    rows_modified[i].set('red', jsonData[i].red);
						                }
						            
						                var err_art = jsonData[i].c_art + ': ';
						            	if(jsonData[i].c_art.trim() != c_art) 
						            		error_msg =  error_msg.concat(err_art, jsonData[i].msg) + '<br>';
						               
						                c_art = jsonData[i].c_art.trim();
						                
	 								} 
	 								
	 								acs_show_msg_error(error_msg);
	 								
	 								}
						           								        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			
			            }

			     }, {
                    xtype: 'button',
                    text: 'Conferma',	//in win ** "Duplica assistita articoli" **
                     margin : '5 0 0 0',
		            iconCls: 'icon-save-32',
		            scale: 'large',	                     

    		           handler: function() {
    		              
    		           var b_bar = this.up('window').down('#b_bar');  
    		           var t_confirm = this.up('window').down('#t_confirm');   
                       var grid = this.up('grid');
                       var loc_win = this.up('window');
                       var evid_g =  this.up('window').down('#evid_g').getValue();
                       var evid_u =  this.up('window').down('#evid_u').getValue();
                       var todo = this.up('window').down('#todo').getValue();
                       var ciclo = this.up('window').down('#ciclo').getValue();
                   
                                     
                      rows_modified = grid.getStore().getUpdatedRecords();
                      list_rows_modified = [];
                      list_selected_id = [];
                      check_list = [];
                      var count_art_sosp = 0;  
                      var count_art = 0;
                        
                      for (var i=0; i<rows_modified.length; i++) {
		              	list_rows_modified.push(rows_modified[i].data);
		              	list_selected_id.push(rows_modified[i].get('n_cod'));
		              }
		              
		               for (var i=0; i<rows_modified.length; i++) {
		               
		                 if(rows_modified[i].get('len') == 0){
		              	 	acs_show_msg_error('Inserire un codice articolo');
	 					 	return;
	 					 }
		               
		              	if(rows_modified[i].get('len') > 15){
		              	 acs_show_msg_error('Il codice articolo deve essere massimo di 15 caratteri');
	 					 return;
	 					 }
	 					 
	 					 if(rows_modified[i].get('n_len') > 50){
		              	 	acs_show_msg_error('La descrizione articolo deve essere massimo di 50 caratteri');
	 					 	return;
	 					 }
	 					 
	 					 if(rows_modified[i].get('esau') != '' || rows_modified[i].get('sosp') == 'S'){
	 					    check_list.push(rows_modified[i].get('cod'));
	 				     }
		              }
		              
		              

		              if(check_list.length > 0){
			              
		                    var my_listeners = {
		    		  			afterConfirm: function(from_win){
		    		  			    grid.confirm_duplica(grid, list_rows_modified, ciclo, evid_g, evid_u, todo, b_bar, t_confirm, from_win);  
					        		}
			    				};
		              
		              		acs_show_win_std('Ciclo di vita articoli da duplicare', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_check', {check_list : check_list, ciclo : ciclo}, 450, 300, my_listeners, 'icon-blog_compose-16');
		              		return false;
		              }
		              
		               grid.confirm_duplica(grid, list_rows_modified, ciclo, evid_g, evid_u, todo, b_bar, t_confirm, loc_win);
		              
			
			            }

			     }
            	
            	]}
			     ]
		   }]
		 
		  ,viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			        
			        
			           if(record.get('red') == 'Y')
			               return ' colora_riga_rosso';
			           
			           return '';																
			         }   
			    }
			       


}
    
    ]}

<?php 
exit;
}

// ******************************************************************************************
// FORM 
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_win'){
    
    $m_params = acs_m_params_json_decode();
    
      
    ?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
            title: '',
            items: [ 
            
                   { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
						xtype: 'checkboxgroup',
						flex: 1,
						fieldLabel: 'Duplica note',
					    labelWidth : 120,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_note' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					   },
					   {
						xtype: 'checkboxgroup',
						flex: 1,
						labelWidth : 150,
						fieldLabel: 'Azzera fornitori/terzista',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_az_ft' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					   }
						
						   
						]} ,
						
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
						xtype: 'checkboxgroup',
						flex: 1, 
					    labelWidth : 120,
						fieldLabel: 'Duplica traduzioni',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_trad' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					  },  {
						xtype: 'checkboxgroup',
						flex: 1,
						labelWidth : 150,
						fieldLabel: 'Azzera GG lead time/fissi',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_az_gg' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					   }
						   
						]} ,
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
						xtype: 'checkboxgroup',
						flex: 1, 
						fieldLabel: 'Variante anagrafiche',
						labelWidth : 120,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_var' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					  },  {
						xtype: 'checkboxgroup',
						flex: 1,
						labelWidth : 150,
						fieldLabel: 'Propriet�/Caratteristiche',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_prop' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					   }
						   
						]} ,
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
						xtype: 'checkboxgroup',
						flex: 1, 
						fieldLabel: 'Azzera nazione origine',
						labelWidth : 120,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_nazi' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                          , checked : true
                        }]							
					  },  {
						xtype: 'checkboxgroup',
						flex: 1,
						labelWidth : 150,
						fieldLabel: 'Azzera intra vendite',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_intra' 
                          , boxLabel: ''
                          , checked : true
                          , inputValue: 'Y'
                        }]							
					   }
						   
						]} 
						   
            		 ,{
						xtype: 'checkboxgroup',
						fieldLabel: 'Duplica schede',
						labelWidth : 120,
						layout: {
                            type: 'vbox',
                            //align: 'left'
                        },
					    items: [
					    
					    <?php
                        $sql = "SELECT RRN(TA) AS RRN, TAKEY1, TADESC
                                FROM {$cfg_mod_DeskArt['file_tabelle']} TA
                                WHERE TADT='{$id_ditta_default}' AND TATAID = 'SCHED'
                                AND TAKEY1 <> 'BCER'";
                        
                        $stmt = db2_prepare($conn, $sql);
                        $result = db2_execute($stmt);
                                
                        while($row = db2_fetch_assoc($stmt)){
                            $txt_bl = "[".trim($row['TAKEY1'])."] ".trim($row['TADESC']);
                            
                            if(count($m_params->list_rows_modified) == 1){
                                $ha_commenti = $desk_art->has_scheda_articolo($m_params->list_rows_modified[0]->cod, $row['TAKEY1']);
                            }else{
                                $ha_commenti = false;
                            }
                         
                            if ($ha_commenti == FALSE)
                                $img_com_name = "icone/16x16/tag_grey.png";
                            else
                                $img_com_name = "icone/16x16/tag.png"
                        ?>
    				     { xtype: 'checkbox'
                          , name: 'scheda_<?php echo trim($row['TAKEY1']) ?>' 
                          , boxLabel: '<img src=<?php echo img_path($img_com_name) ?> width=15> <?php echo $txt_bl; ?>' 
                          , inputValue: 'Y'
                          , checked : true
                         },
    										    
                        <?php }?>
                        
                        ]							
					}
				     
				
					        
				],
			buttons: [
			
			<?php if($m_params->multi == 'Y'){?>
			  {
	            text: 'Modifica descrizione',
	            iconCls: 'icon-blog_compose-32',
	            scale: 'large',
	            handler: function () {
			        		       		     
        			        	list_selected_art = [];
        			        	var id_selected = <?php echo acs_je($m_params->list_rows_modified); ?>;
                                  
                                  for (var i=0; i<id_selected.length; i++){ 
        				            list_selected_art.push({
        				            	c_art: id_selected[i].n_cod,
        				            	d_art: id_selected[i].n_desc       				            
        				            	});
        				           }
			        					        		
			        			 var my_listeners = {
		        					afterConfirm: function(list, from_win){	
		        					from_win.close();
		        						
						        		}
				    				};
			        		
					    		  acs_show_win_std('Modifica descrizione', 'acs_anag_art_mod_desc_art.php?fn=open_mod', {list_selected_art : list_selected_art}, 1000, 300, my_listeners, 'icon-blog_compose-16');
				                }		
	        }, '->',
			<?php }?>
			{
	            text: 'Conferma',			//conferma duplica dati accessori
	            iconCls: 'icon-save-32',
	            scale: 'large',
	            
	            esegui_duplica: function(bt, form, form_values, loc_win, list_rows_modified, count_i){
	            
	            	if (count_i === undefined) {	            	
	            		loc_win.down('toolbar').hide();
				  		//Apro la progress bar
				  		var pb = Ext.create('Ext.ProgressBar', {
                           flex: 1,
                           text: 'Elaborazione in corso',
                           autoRender: true,
                           value: 0.0,
                           renderTo: loc_win.id
                        }).show();
						loc_win.pb = pb;
	            		count_i = 0;
	            	}
	            	
	            		            	
					var i = count_i;
				  	//eseguo una richiesta per ogni articolo
			  		if (count_i < list_rows_modified.length){
		  			 
		  			 	 loc_win.pb.updateText('In Elaborazione ' +  list_rows_modified[i].cod + ' -> ' + list_rows_modified[i].n_cod);
            	
        	            		Ext.Ajax.request({
        	            			   //async: false, //non funziona correttamente in Chrome
        							   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_duplica',
        							   method: 'POST',
        							   jsonData: {
        							   	form_values : form_values,
        							   	list_rows_modified : [list_rows_modified[i]]            							   
        							   }, 
        							   
        							   success: function(response, opts) {            							   	  
        								  var jsonData = Ext.decode(response.responseText);
        								  loc_win.pb.updateProgress((i+1) / list_rows_modified.length);
        								  
        								  //fine ciclo articoli
        								  if (count_i == list_rows_modified.length - 1){
        								  	loc_win.close();
        								  	        								  
        								  	<?php 
        								  	//se presente richiamo la generazione dei ToDo da Indici
        								  	$sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
                                                    LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
                                                    ON CD.CDDT = TA_ATTAV.TADT AND CD.CDTODO = TA_ATTAV.TAKEY1 AND TATAID = 'ATTAV'
                                                    WHERE CDDT= '$id_ditta_default' AND CDDICL = 'WIZDUPART'
                                                    AND CDCMAS = 'START'";        								  	
        								  	$stmtWDA = db2_prepare($conn, $sql);
        								  	echo db2_stmt_errormsg();
        								  	$result = db2_execute($stmtWDA);
        								  	echo db2_stmt_errormsg();
        								  	$rowWDA = db2_fetch_assoc($stmtWDA);
        								  	if ($rowWDA['C_ROW'] > 0){
        								  	?>        								  	        								  	
            								  	var list_selected_art = [];
            								  	for (var i=0; i<list_rows_modified.length; i++){ 
                    				            	list_selected_art.push(list_rows_modified[i].n_cod);
                    				           	}
                    				           	acs_show_win_std('Attivit&agrave; da assegnare agli articoli generati', 
        				    						'acs_anag_art_create_todo_from_indici.php?fn=open_filtri', {
        				    							cod: 'WIZDUPART',
        				    							list_selected_art : list_selected_art
        				    			     	}, 1200, 300, my_listeners, 'icon-blog_compose-16');
                    				        <?php } ?>
        								  	
        								  } else {
        								  	//articolo successivo
        								  	bt.esegui_duplica(bt, form, form_values, loc_win, list_rows_modified, count_i + 1);
        								  }
        								  
        							   }, 
        							   failure: function(response, opts) {
        							      alert('error on save_manual');
        							   }
        							});	
        							
        				} //per ogni articolo
	            	
	            }, //esegui_duplica
	            
	            handler: function(bt) {
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
	            	var loc_win = this.up('window');
	            	var id_selected = <?php echo acs_je($m_params->list_rows_modified); ?>;
	            	if(form.isValid()){	            	
	            		bt.esegui_duplica(bt, form, form_values, loc_win, id_selected);
	            	 }
	        		}			
	        }
	        
	        ]
			
        }
]}
<?php
exit;
} //open_form_filtri

if ($_REQUEST['fn'] == 'get_json_data_art'){ 
    $ar = array();
    $ar_articoli = $m_params->list_art;
    $sql_where = " AND ARART IN (" . sql_t_IN($ar_articoli) . ")";
      
    $sql = "SELECT COUNT(*) AS C_ART, ARSOSP, ARESAU
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' {$sql_where} 
            GROUP BY ARSOSP, ARESAU";
  
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['sosp']        = $row['ARSOSP'];
        $row_ciclo = $desk_art->find_TA_std('AR012', trim($row['ARESAU']));
        $nr['ciclo']       = $row['ARESAU'];
        $nr['d_ciclo']     = $row_ciclo[0]['text'];
        $row_n_ciclo = $desk_art->find_TA_std('AR012', $m_params->n_ciclo); 
        $nr['n_ciclo']     = $m_params->n_ciclo;
        $nr['d_n_ciclo']   = $row_n_ciclo[0]['text'];
        $nr['nr_articoli'] = $row['C_ART'];
        
        
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
    
}


if ($_REQUEST['fn'] == 'open_check'){ 

    
    ?>
    {"success":true, "items": [
		{ 	xtype: 'grid',
	        rootVisible: false,
	         multiSelect: true,
	        store: {
				xtype: 'store',
				autoLoad:true,
				proxy: {
					   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_art', 
					   method: 'POST',								
					   type: 'ajax',
					   actionMethods: {
				          read: 'POST'
				        },
				       extraParams: {
								 list_art: <?php echo acs_je($m_params->check_list) ?>,
								 n_ciclo : <?php echo j($m_params->ciclo); ?>
	        				},
	        			doRequest: personalizza_extraParams_to_jsonData, 
						reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
					
        			fields: ['nr_articoli', 'ciclo', 'n_ciclo', 'sosp', 'd_ciclo', 'd_n_ciclo']							
									
			}, //store
		        
			    <?php $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>	
				columns: [	
				   		{
			               text: 'Nr articoli',
			               dataIndex: 'nr_articoli',
			         	},
		    		  {
			              text: '<?php echo $cf1; ?>',
			              width: 30, 
						  dataIndex: 'sosp',
						  tooltip: 'Sospeso',		        			    	     
		    			  renderer: function(value, metaData, record){
		    			  	if(record.get('sosp') == 'S'){ 
		    			   	metaData.tdCls += ' sfondo_rosso';
		    			   	return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 		}
		    			  }  
			                
		             },{
			               text: 'Ciclo di vita corrente',
			               flex: 1, 
			               dataIndex: 'd_ciclo',
			         	},
			         	{
			               text: 'Ciclo di vita da assegnare',
			               flex: 1, 
			               dataIndex: 'd_n_ciclo',
			         	},
		    		
		    		
				], 	buttons: [					
					{
			            text: 'Annulla duplica',
				        iconCls : 'icon-sub_red_delete-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var loc_win = this.up('window');
			                loc_win.close();
			                
			            }
			        }, '->', {
			            text: 'Conferma duplica',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			            	loc_win.fireEvent('afterConfirm', loc_win);
			               
			            }
			        }  
		        
		        
		        ] 
				
											    
				    		
                    }, //grid
        
]}
    



<?php 
    exit;
}