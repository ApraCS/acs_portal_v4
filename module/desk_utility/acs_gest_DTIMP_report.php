<?php

require_once "../../config.inc.php";

$main_module =  new DeskArt();
$cfg_mod = $main_module->get_cfg_mod();

// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   .grassetto{font-weight: bold;}
   .normal{font-weight: normal;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff; font-size: 13px;}   
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 


<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);
$sql_where = "";
$sql_select = "";
$sql_join = "";
$ar = array();


$sql = "SELECT TA.*
        FROM {$cfg_mod['file_tabelle']} TA
        WHERE TA.TADT = '{$id_ditta_default}' AND TA.TATAID = 'DTIMP'
        ORDER BY TATELE, TAKEY1";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$ar = array();
while ($row = db2_fetch_assoc($stmt)) {

    $ar[] = $row;
    
}//while

echo "<div id='my_content'>"; 

echo "<div class=header_page>";
echo "<H2>Riepilogo codifica dati import</H2>";
echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
        
echo "<table class=int1>";
       echo "<tr class='liv_data'>
             <th>Codice</th>
             <th>Descrizione</th>
             <th>Note</th>
             <th>Seq.</th>
             <th>Tipologie</th>
             <th>Tipologia da-a</th>
             <th>Etichetta colonna</th>
             <th>T/N</th>";
      echo  "</tr>";
   
 //PROPRIETA'
foreach($ar as $k => $v){
    
    $ar_tipologie = array();
    $val_cor = "";
    $val_tra = "";
    
    if(trim($v['TASITI']) != ''){ 
        $dat_opz = $main_module->find_TA_std(trim($v['TASITI']), null, 'N', 'Y');
    
        $cd = array();
        $vc = array();
        $vt = array();
        
        foreach($dat_opz as $dt){
        
            
            $cd[] = "{$v['TASITI']} : {$dt['id']}";
            $vc[] = "{$dt['TAMAIL']}";
            $vt[] = "{$dt['text']}";
        }
        $codice = implode('<br>', $cd);
        $val_cor = implode('<br>', $vc);
      //  if(count($vc) > 0) $val_cor = $val_cor;
        $val_tra = implode('<br>', $vt);
      //  if(count($vt) > 0) $val_tra = $val_tra;
        //$ar_tipologie[] = $v['TASITI'];
    
    
    }
   
    if(trim($v['TAASPE']) != '') $ar_tipologie[] = $v['TAASPE'];
    if(trim($v['TATISP']) != '') $ar_tipologie[] = $v['TATISP'];
    if(trim($v['TATITR']) != '') $ar_tipologie[] = $v['TATITR'];
    if(trim($v['TARIF1']) != '') $ar_tipologie[] = $v['TARIF1'];
    if(trim($v['TARIF2']) != '') $ar_tipologie[] = $v['TARIF2'];
    if(trim($v['TASITI']) != '') $ar_tipologie[] = $codice;
    $tipologie = implode('<br>', $ar_tipologie);
    
    $da_a = "";
    if(trim($v['TALOCA']) != '')
        $da_a .= $v['TALOCA'];
    if(trim($v['TACAP']) != '')
        $da_a .= " - ".$v['TACAP'];
    
    $br = "";
    if(count($ar_tipologie) > 1){
        for( $i = 0; $i <= count($ar_tipologie) - 2;  $i++){
            $br .= "<br>";
        }
    }
 
    echo "<tr>
          <td valign = top>{$v['TAKEY1']}</td>
          <td valign = top>{$v['TADESC']}</td>
          <td valign = top>{$v['TAMAIL']}</td>
          <td valign = top>{$v['TATELE']}</td>
          <td valign = top>{$tipologie}</td>
          <td valign = top>{$da_a} {$br} {$val_cor}</td>
          <td valign = top>{$v['TAINDI']}{$br}{$val_tra}</td>
          <td valign = top>{$v['TAPROV']}</td>";
    echo "</tr>";
   
  
}
echo "</div>";

 		
?>

 </body>
</html>


<?php
exit;
}



