<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

ini_set('memory_limit', '2048M');

if ($_REQUEST['fn'] == 'get_json_data_elenco'){
    
    $ar = array();
    $k_ordine = $m_params->k_ordine;
    $oe = $s->k_ordine_td_decode_xx($k_ordine);
    $ar_stmt = array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']);
    
   /* $sql_where = '';
    if(isset($m_params->riga) && $m_params->riga > 0)
        $sql_where .= " AND RERIGA = {$m_params->riga}";*/ 
    
    if(isset($m_params->collo) && strlen($m_params->collo) > 0)
        $sql_where .= " AND RECOLC = {$m_params->collo}";
                
    $sql = "SELECT RE.*, RDRIGA, RDSTAT, ARPNET, ARPLOR, FASI
            FROM {$cfg_mod_DeskArt['file_ricom']} RE
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                ON RE.REDT=RD.RDDT AND RE.RETIDO=RD.RDTIDO AND RE.REINUM=RD.RDINUM AND RE.REAADO=RD.RDAADO AND RE.RENRDO=RD.RDNRDO AND RE.RENREC=RD.RDNREC
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
                ON AR.ARDT = RE.REDT AND AR.ARART = RE.REART 
            LEFT OUTER JOIN(
                SELECT COUNT(*) AS FASI, RITPCO, RIAACO, RINRCO, RITPSV, RIRIGA
                FROM {$cfg_mod_DeskUtility['file_cicli_ricom']} RI
                WHERE RIDT = '{$id_ditta_default}' AND RITIPO = 'S'
                GROUP BY RITPCO, RIAACO, RINRCO, RITPSV, RIRIGA
            ) RI
            ON RI.RITPCO = RE.RETPCO AND RI.RIAACO = RE.REAACO AND RI.RINRCO = RE.RENRCO AND RI.RITPSV = RE.RETPSV AND RI.RIRIGA = RE.RERIGA
                
            WHERE RE.REDT = ? AND RE.RETIDO = ? AND RE.REINUM = ? AND RE.REAADO = ? AND RE.RENRDO = ?
                  AND RE.RETPSV = 'FB' {$sql_where}
            ORDER BY RERIGA, REART
            ";
    
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_stmt);
    
    
    $sql_op = "SELECT REAACO AS P_ANNO, RENRCO AS P_NUM, RETPCO AS P_TPO
               FROM {$cfg_mod_DeskArt['file_ricom']} RE
               WHERE REDT = '{$id_ditta_default}' AND RETIDO =  ? /*'CP'*/ 
               AND REINUM = 'FB' AND REAADO = ? AND RENRDO = ? AND RENREC = ?
               LIMIT 1";
    
  
    
    /*$stmt_op = db2_prepare($conn, $sql_op);
    echo db2_stmt_errormsg();*/
  
    while($row = db2_fetch_assoc($stmt)){
       $nr = array();
   
        $nr['s_giallo']	= 'N';
        $nr['REDTGE']	= $row['REDTGE'];
        $nr['REUSGE']	= $row['REUSGE'];
        $nr['livello']	= $row['RELIVE'];
        $nr['riga']	    = $row['RERIGA'];
        $nr['c_art']    = $row['REART'];
        $nr['d_art']    = $row['REDART'];
        $nr['qta']      = (float)$row['REQTA'];
        $nr['u_m']      = $row['REUM'];
        $nr['nr_or']    = $row['RDRIGA']; //voglio il numero di riga ordine, sistemare
        
        $nr['RETPCO'] = trim($row['RETPCO']);
        $nr['RETPSV'] = trim($row['RETPSV']);
        $nr['REAACO'] = trim($row['REAACO']);
        $nr['RENRCO'] = trim($row['RENRCO']);
        $nr['RERIGA'] = trim($row['RERIGA']);
        $nr['var1'] = $row['REVAR1'];
        $dom1 = find_TA_sys('PUVR', trim($row['REVAR1']));
        $risp1 = find_TA_sys('PUVN', trim($row['REVAN1']), null, trim($row['REVAR1']));
        $nr['qtip_var1'] = "[".$row['REVAR1']."] ".$dom1[0]['text'] ."<br>[".$row['REVAN1']."] ".$risp1[0]['text'];
        
        $nr['var2'] = $row['REVAR2'];
        $dom2 = find_TA_sys('PUVR', trim($row['REVAR2']));
        $risp2 = find_TA_sys('PUVN', trim($row['REVAN2']), trim($row['REVAR2']));
        $nr['qtip_var2'] = "[".$row['REVAR2']."] ".$dom2[0]['text'] ."<br>[".$row['REVAN2']."] ".$risp2[0]['text'];
        
        $nr['var3'] = $row['REVAR3'];
        $dom3 = find_TA_sys('PUVR', trim($row['REVAR3']));
        $risp3 = find_TA_sys('PUVN', trim($row['REVAN3']), trim($row['REVAR3']));
        $nr['qtip_var3'] = "[".$row['REVAR3']."] ".$dom3[0]['text'] ."<br>[".$row['REVAN3']."] ".$risp3[0]['text'];
        
        $nr['dim1'] = $row['REDIM1'];
        $nr['dim2'] = $row['REDIM2'];
        $nr['dim3'] = $row['REDIM3'];
        $nr['collo'] = $row['REPROG'];
        $nr['lotto'] = trim($row['RETIDE']);
        $nr['fasi'] = $row['FASI'];
        $nr['peso_netto'] = text_to_number(substr($row['REFILL'], 20, 6), 4, 2);
        $nr['peso_netto_anag']  = $row['ARPNET'];
        if($nr['peso_netto'] > 0 || $nr['peso_netto_anag'] > 0)
            $nr['peso_lordo_anag']  = $row['ARPLOR'];
        // AND RE2.REAADO = RE.REAADO AND RE2.RENRDO = RE.RENRDO AND RE2.RENREC = RE.RERIGA
        $stmt_op = db2_prepare($conn, $sql_op);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_op, array($row['RETPCO'], $row['REAADO'], $row['RENRDO'], $row['RERIGA']));
        $row_op = db2_fetch_assoc($stmt_op);
        
        $nr['P_ANNO'] = $row_op['P_ANNO'];
        $nr['P_NUM'] = $row_op['P_NUM'];
        $nr['P_TPO'] = $row_op['P_TPO'];
        $nr['qtip_lotto'] = $row_op['P_ANNO']."_".$row_op['P_NUM']."_".$row_op['P_TPO'];
        $ar[] = $nr;
   
    }
  
    for($i = 0; $i < count($ar); $i++){
        
        $mio_livello = $ar[$i]['livello'];
        $liv_succ = $ar[$i + 1]['livello'];
        
        if($m_params->gr_collo != 'F'){
            if($liv_succ <= $mio_livello )
                $ar[$i]['s_giallo'] = 'Y';
        }
        
    }
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_elenco'){
    $m_params = acs_m_params_json_decode();
    $ar_ordine = explode('_', $m_params->k_ordine);
    $title_suf = "ordine ".$ar_ordine[3]."_".$ar_ordine[4];
    if($m_params->elenco != 'Y')
        $title_suf .= " collo ".$m_params->collo. " (Peso N. ".n($m_params->peso_n, 3). ")"; 
    
    ?>
	{"success":true,
			m_win: {
				width: 1300,
				height: 450,
				iconCls: 'icon-leaf-16',
				title: 'Riepilogo articoli/componenti',
				//title_pre: 'aaa',
				title_suf: <?php echo j($title_suf) ?>	
			}, 
			items: [
					{
						xtype: 'grid',
				        loadMask: true,	
				        stateful: true,
        				stateId: 'elenco_componenti',
        				stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow'],
        				features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],	
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco', 
								   method: 'POST',								
								   type: 'ajax',
								   timeout: 240000,
							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										 k_ordine: <?php echo j($m_params->k_ordine); ?>,
										 riga : <?php echo j($m_params->riga); ?>,
										 collo : <?php echo j($m_params->collo); ?>,
										 gr_collo : <?php echo j($m_params->gr_collo); ?>
																	
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['s_giallo', 'peso_netto_anag', 'peso_lordo_anag', 'peso_netto', 'nr_or', {name : 'qta', type : 'float'}, 'livello', {name : 'riga', type : 'int'}, 'c_art', 'd_art', 'u_m', 'RETPCO', 'RETPSV', 'REAACO', 'RENRCO', 'RERIGA',
		        					 'var1', 'var2', 'var3', 'qtip_var1', 'qtip_var2', 'qtip_var3', {name : 'dim1', type : 'float'},  {name : 'dim2', type : 'float'},  {name : 'dim3', type : 'float'}, {name : 'collo', type : 'int'}, 'lotto', 'qtip_lotto', 
		        					 'fasi', 'P_TPO', 'P_ANNO', 'P_NUM', 'REDTGE', 'REUSGE']							
									
			}, //store
			
			<?php $cl = "<img src=" . img_path("icone/48x48/tools.png") . " height=18>"; ?>
			<?php $fl = "<img src=" . img_path("icone/48x48/gear.png") . " height=18>"; ?>
			
			 columns: [	
				    {
	                header   : 'Riga ord.',
	                dataIndex: 'nr_or',
	                width : 50,
	                filter: {type: 'string'}, filterable: true,
	                 },
			      {
	                header   : 'Liv.',
	                dataIndex: 'livello',
	                width : 30,
	                filter: {type: 'numeric'}, filterable: true,
	                 },
	                {
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width : 45,
	                filter: {type: 'numeric'}, filterable: true
	                 }, 
	                 
	                  {
	                header   : 'Data gen.',
	                dataIndex: 'REDTGE',
	                width : 60,
	                filter: {type: 'numeric'}, filterable: true,
	                renderer : date_from_AS,
	                hidden : true
	                 }, 
	                   {
	                header   : 'Utente gen.',
	                dataIndex: 'REUSGE',
	                width : 100,
	                filter: {type: 'string'}, filterable: true,
	                hidden : true
	                 }, 
	                   {
	                header   : 'Articolo',
	                dataIndex: 'c_art',
	                width : 150,
	                filter: {type: 'string'}, filterable: true
	                 },
	                 {
	                header   : 'Descrizione',
	                dataIndex: 'd_art',
	                flex : 1,
	                filter: {type: 'string'}, filterable: true
	                 },
	                 {
		                header   : 'V1',
		                dataIndex: 'var1', 
		                width     : 40,
		                filter: {type: 'string'}, filterable: true,
		                renderer: function (value, metaData, record, row, col, store, gridView){
							
							if (record.get('var1') != 0)	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_var1')) + '"';								
							
							if (record.get('var1') == 0) return ''; //non mostro la data se e' quella selezionata																	
		  					return '<b>*</b>';			    
							}	              			                
		             }, {
		                header   : 'V2',
		                dataIndex: 'var2', 
		                width     : 40,
		                filter: {type: 'string'}, filterable: true,
		                renderer: function (value, metaData, record, row, col, store, gridView){
		                    if (record.get('var2') != 0)	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_var2')) + '"';			
		                
							if (record.get('var2') == 0) return ''; //non mostro la data se e' quella selezionata																	
		  					return '<b>*</b>';			    
							}              			                
		             }, {
		                header   : 'V3',
		                dataIndex: 'var3', 
		                width     : 40,
		                filter: {type: 'string'}, filterable: true,
		                renderer: function (value, metaData, record, row, col, store, gridView){
							
							if (record.get('var3') != 0)	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_var3')) + '"';			
							
							if (record.get('var3') == 0) return ''; //non mostro la data se e' quella selezionata																	
		  					return '<b>*</b>';			    
							}              			                
		             },{
		                header   : 'Dim. 1',
		                dataIndex: 'dim1', 
		                width    : 80,
		                align: 'right',
		                filter: {type: 'numeric'}, filterable: true,
		                renderer : floatRenderer2
		             },{
		                header   : 'Dim. 2',
		                dataIndex: 'dim2', 
		                width    : 80,
		                align: 'right',
		                filter: {type: 'numeric'}, filterable: true,
		                renderer : floatRenderer2
		             },{
		                header   : 'Dim. 3',
		                dataIndex: 'dim3', 
		                width    : 80,
		                align: 'right',
		                filter: {type: 'numeric'}, filterable: true,
		                renderer : floatRenderer2
		             },{
		                header   : 'Um',
		                dataIndex: 'u_m', 
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
		             }, {
		                header   : 'Q.t&agrave;',
		                dataIndex: 'qta', 
		                width    : 50,
		                filter: {type: 'numeric'}, filterable: true,
		                align: 'right',
		                renderer: floatRenderer2         		                
		             },{
		                header   : 'Collo',
		                dataIndex: 'collo', 
		                width    : 50,
		                filter: {type: 'numeric'}, filterable: true,
		                align: 'right'
					                		                
		             },{ text: '<br><?php echo $fl; ?>' 
	        		    , width: 30 
            	        , dataIndex: 'fasi'
	        		    , tooltip: 'Fasi di lavorazione'
	        		    , filter: {type: 'string'}, filterable: true
	        		    , renderer: function(value, p, record) {
	    			  	    if(record.get('fasi') > 0)  			    	
								return '<img src=<?php echo img_path("icone/48x48/gear.png") ?> width=15>';
	    		            
	    		            }
		        	    }, { text: '<br><?php echo $cl; ?>' 
	        		    , width: 30 
            	        , dataIndex: 'lotto'
	        		    , tooltip: 'Distinta di lavorazione'
	        		    , filter: {type: 'string'}, filterable: true
	        		    , renderer: function(value, p, record) {
	    			  	     if(record.get('lotto') == 'L'){     			    	
								p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_lotto')) + '"';
	    			  	     	return '<img src=<?php echo img_path("icone/48x48/tools.png") ?> width=15>';
	    		            }
	    		             if(record.get('lotto') == 'C')  			    	
								return '<img src=<?php echo img_path("icone/48x48/gear.png") ?> width=15>';
	    		            
	    		            }
		        	    },
		        	      {
		                header   : 'Peso N.',
		                dataIndex: 'peso_netto', 
		                width    : 70,
		                filter: {type: 'numeric'}, filterable: true,
		                align: 'right',
		                renderer: function(value, p, record) {
		                     var p_n = toFixedTruncate(parseFloat(record.get('peso_netto')), 2);
		                     var p_a = toFixedTruncate(parseFloat(record.get('peso_nett_anag')), 2);
	    			  	    
	    			  	     if(1 == 1 || p_n != p_a){ 
	    			  	         			    	
								p.tdCls += ' grassetto';
								var qtip = 'Peso netto anagrafico: ' + floatRenderer4(record.get('peso_netto_anag')) + '<br>Peso lordo anagrafico : ' +floatRenderer4(record.get('peso_lordo_anag'));
								p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(qtip) + '"';
	    			  	     }	
	    			  	     
	    			  	     if(record.get('peso_netto') == 0 && record.get('peso_netto_anag') > 0)
	    			  	         return '[' + floatRenderer4(record.get('peso_netto_anag')) + ']';
	    			  	     	
	    			  	      return floatRenderer2(value);
	    		            
	    		            }        		                
		             },{
		                header   : 'Peso L.',
		                dataIndex: 'peso_lordo_anag', 
		                width    : 70,
		                filter: {type: 'numeric'}, filterable: true,
		                align: 'right',
		                renderer: floatRenderer4        		                
		             }
    	
	         ]
	    
	         
	         , listeners: {
	         
     	      	beforestaterestore: function(comp, state){
        				//ESCLUDO SEMPRE LA MEMORIZZAZIONE DALLA STATEEVENTS
        				delete state.sort;
        			},
	            celldblclick: {
	            
	            		//su elenco voci (grid dx) - Sto selezionando la risposta
	            
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  
							  if(col_name == 'lotto' && rec.get('lotto') == 'L'){			
			   			           acs_show_win_std('Distinta di lavorazione'
			   							   , 'acs_get_riga_componenti.php'
			   							   , {row : rec.data, from_lotto : 'Y', k_ordine : '<?php echo $m_params->k_ordine; ?>', riga_comp : rec.get('riga'), desc_art: rec.get('d_art'), cod_art : rec.get('c_art')}
			   							   , 1300, 450, null
			   							   , 'icon-folder_search-16'
			   							   );      
			   							   		
					          } 
					          
					           if(col_name == 'fasi' && rec.get('fasi') > 0){			
			   			           acs_show_win_std(null
			   							   , 'acs_get_ciclo.php?fn=open_tab'
			   							   , {row : rec.data, k_ordine : '<?php echo $m_params->k_ordine; ?>', riga_comp : rec.get('riga'), d_art: rec.get('d_art'), c_art : rec.get('c_art')}
			   							  );      
			   							   		
					          } 
							
							 
						  }
			   		  }
			   		
	         
				   
				 }, viewConfig: {
		       		 getRowClass: function(record, index) {	
		       	     v = record.get('livello');
			          
			           if (record.get('livello') == 0 ) //rilasciato
		           			v = v + ' grassetto';
		    
		           	   if(record.get('s_giallo') == 'Y')
		           		   return v + ' colora_riga_giallo';
 		           
 		           			return v;		
                															
		        		 }   
		    }
				 
		
		
		}
		
	]}
	
	
	<?php 
	
exit;	
}


