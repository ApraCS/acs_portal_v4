<?php

require_once "../../config.inc.php";
$main_module = new DeskUtility();

$m_params = acs_m_params_json_decode();


if (isset($_REQUEST['function']) && $_REQUEST['function'] == 'view_image'){
    
    $sql = "SELECT *
            FROM {$cfg_mod_DeskArt['file_tabelle']} TA
            WHERE RRN(TA) = '{$_REQUEST['RRN']}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if(isset($_REQUEST['p_todo']) && $_REQUEST['p_todo'] == 'Y'){
        $path = $cfg_mod_DeskArt['root_path']."PROJECTTODO/".trim($id_ditta_default) . "/" .trim($row['TAMAIL']);
    }else{
        $path = $cfg_mod_DeskArt['root_path'].trim($id_ditta_default) . "/" .trim($row['TAMAIL']);
    }
 
    
	//mi viene passato il path del file... lo apro e ne restituisco il contenuto
    $handle = fopen($path, "rb");
    $cont_file = fread($handle, filesize($path));
    $path_info = pathinfo($path);
	

	$tipo_estensione = $path_info['extension'];
	
	switch(strtolower($tipo_estensione)){
		case 'pdf':
			header('Content-type: application/pdf');
			echo $cont_file;
			break;
		case 'tif':
		case 'tiff':
			header('Content-type: image/tiff');
			echo $cont_file;
			break;
		case 'png':
			header('Content-type: image/png');
			echo $cont_file;
			break;
		case 'jpg':
			header('Content-type: image/jpeg');
			echo $cont_file;
			break;
		default:
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
			header("Content-disposition: attachment; filename=\"" . preg_replace('/^.+[\\\\\\/]/', '', $path_info['basename']) . "\"");
			echo $cont_file;
			break;
	}
	
	exit;
}

