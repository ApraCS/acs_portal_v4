<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();
$main_module =  new DeskArt();
$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    'descrizione' => "Gestione tabella gruppi",
    'form_title' => "Dettagli tabella gruppi",
    'fields_preset' => array(
        'TATAID' => 'BATCG'
    ),
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL'),
    
    'fields' => array(				
        'TAKEY1' => array('label'	=> 'Codice',  'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
		    'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
            'TAMAIL' => array('label'	=> 'Note',  'maxLength' => 100),
            //immissione
            'immissione' => array(
                'type' => 'immissione', 'fw'=>'width: 70',
                'config' => array(
                    'data_gen'   => 'TADTGE',
                    'user_gen'   => 'TAUSGE'
                )
                
            ),
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione')
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
