<?php

require_once("../../config.inc.php");
require_once("../base/acs_gestione_nota_nt_include.php");

$m_params = acs_m_params_json_decode();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $desk_art->get_cfg_mod();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 

$m_table_config = array(
    'module'      => $desk_art,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "CATIN - Catalogo Intestazione",
    'descrizione' => "Cataloghi import/aggiornamento",
    'form_title' => "Dettagli tabella catalogo intestazione",
    'f_desc'  => 'TADESC',
    'field_NOTE' => 'TAKEY1',   //SOLO PER LA CHIAVE FILE NT
    'fields_preset' => array(
        'TATAID' => 'CATIN'
    ),
   
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array(/*'TASTAL',*/ 'TAKEY1', 'TADESC', 'TAMAIL', 'TAINDI', 'nt_CONFIG', 'TAB_ABB', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TAINDI', 'TACOGE'),
    
    'fields' => array(
        //' TASTAL'     => array('label'	=> $divieto, 'fw'=>'width: 40'),
        'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        'TAINDI' => array('label'	=> 'Riferimento', 'maxLength' => 30),
        'TACOGE' => array('label'	=> 'Fornitore', 'type' => 'fornitore'),
        'TACOGE_DE' => array('label'	=> 'Fornitore', 'type' => 'fornitore'),
       
        'nt_CONFIG' => array('type' => 'NT', 'fw'=>'width: 40',
            'iconCls' => "info_blue",
            'tooltip'=>'Note assistenza',
            'nt_config' => array(
                'title' => 'Note assistenza',
                'file'     => $cfg_mod_Spedizioni['file_note'],
                'NTTPNO'   => 'CATIN'
                //ToDo:
                //come default uso come chiave il valore di field_IDPR. Si potrebbe dover configurare
            )),
        
        //TABELLA ABBINATA
            'TAB_ABB' =>  array( 'label' => 'varsione', 'type' => 'TA',
                'iconCls' => 'search',
                'tooltip' => 'Catalogo Versione',
                'select' => "TA_CV.C_ROW AS TAB_ABB",
                'join' => "LEFT OUTER JOIN (
                SELECT COUNT(*) AS C_ROW, TA2.TAKEY1
                FROM {$cfg_mod['file_tabelle']} TA2
                WHERE TA2.TADT = '{$id_ditta_default}' AND TA2.TATAID = 'CAVER'
                GROUP BY TA2.TAKEY1) TA_CV
                ON TA.TAKEY1 = TA_CV.TAKEY1",
                'ta_config' => array(
                    'tab' => 'CAVER',
                    'file_acs'     => 'acs_gest_CAVER.php?fn=open_tab',
                    
                )
                ),
                
                
                //immissione
                'immissione' => array(
                    'type' => 'immissione', 'fw'=>'width: 70',
                    'config' => array(
                        'data_gen'   => 'TADTGE',
                        'user_gen'   => 'TAUSGE'
                    )
                    
                ),
                'TADTGE' => array('label'	=> 'Data generazione'),
                'TAUSGE' => array('label'	=> 'Utente generazione'),
                
                
                    ),
                    
            /*'tasto_dx' => array(
              array(
                    'text'  => 'Sospendi/Attiva',
                    'iconCls' => 'icon-divieto-16',
                    'tooltip' => '',
                    'scale' => 'small',
                    'handler' =>  new ApiProcRaw("function(){
                           Ext.Ajax.request({
						        url        : '../base/_gest_table_std.php?acs_?fn=exe_sos_att',
						        method     : 'POST',
			        			jsonData: {
			        				rrn : ''
								},							        
						        success : function(result, request){
						          var jsonData = Ext.decode(result.responseText);
						          rec.set('TATP', jsonData.new_value);	
			            		   
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
               }")
                )
            ),*/
                   
                    );

$_row_post_fn = function($row, $m_table_config){
    $row['nt_CONFIG']  = _ha_nota_nt_config($row[$m_table_config['field_NOTE']], $m_table_config['fields']['nt_CONFIG']['nt_config']);
    
    return $row;
};


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
