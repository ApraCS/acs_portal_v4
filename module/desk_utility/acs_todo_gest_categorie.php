<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_DeskArt['file_tabelle'],
    
    'TATAID' => 'TDPCA',
    'descrizione' => 'Gestione categorie ToDo Project',
    
    'fields_key' => array('TAKEY1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice', 'fw' =>'width : 100'),
        'TADESC' => array('label'	=> 'Descrizione')
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
