<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_listini_liraq']} LR
            WHERE RRN(LR) = '{$m_params->form_values->RRN}'";
    
       
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_aggiorna'){
    
    $ret = array();
    $ar_ins = array();
    
    foreach ($m_params->list_rows_modified as $k=> $v){
    
        $i = $v->index;
        if($v->qta > 0)
            $ar_ins["LRQTA{$i}"] = sql_f($v->qta);
        else 
            $ar_ins["LRQTA{$i}"] = 0;
        if($v->prz > 0)
            $ar_ins["LRPRZ{$i}"] = sql_f($v->prz);
        else 
            $ar_ins["LRPRZ{$i}"] = 0;
        if($v->sc1 > 0)
            $ar_ins["LRSC1{$i}"] = sql_f($v->sc1);
        else
            $ar_ins["LRSC1{$i}"] = 0;
        if($v->sc2 > 0)
            $ar_ins["LRSC2{$i}"] = sql_f($v->sc2);
        else
            $ar_ins["LRSC2{$i}"] = 0;
        if($v->sc3 > 0)
            $ar_ins["LRSC3{$i}"] = sql_f($v->sc3);
        else 
            $ar_ins["LRSC3{$i}"] = 0;
        if($v->sc4)
            $ar_ins["LRSC4{$i}"] = sql_f($v->sc4);
        else
            $ar_ins["LRSC4{$i}"] = 0;
        if($v->magg)
            $ar_ins["LRMAG{$i}"] = sql_f($v->magg);
        else
            $ar_ins["LRMAG{$i}"] = 0;
        
    }
    
    
    $ar_ins['LRDT'] 	= $id_ditta_default;
    $ar_ins['LRNOTE'] 	= $m_params->form_values->f_note;
    $ar_ins['LRDIC1']   = $m_params->form_values->dim1;
    $ar_ins['LRDIC2']   = $m_params->form_values->dim2;
    $ar_ins['LRDIC3']   = $m_params->form_values->dim3;
    $ar_ins['LRPOCO']   = $m_params->form_values->p10;
    $ar_ins['LRMFIM']   = $m_params->form_values->min_fatt;
   
   
    
    
    if($m_params->form_values->rrn == ''){
                
       
        $ar_ins['LRTPLI'] = $m_params->params->tipo;
        $ar_ins['LRCCON'] = $m_params->params->forn;
        $ar_ins['LRLIST'] = $m_params->params->list;
        $ar_ins['LRVALU'] = "EUR";
        $ar_ins['LRART'] =  $m_params->params->c_art;

            
        $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_listini_liraq']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
    }else{
        
        
        $sql = "UPDATE {$cfg_mod_DeskUtility['file_listini_liraq']} LR
        SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
        WHERE RRN(LR) = '{$m_params->form_values->rrn}'";
   
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
        
        
    }
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}




if ($_REQUEST['fn'] == 'open_tab'){
 
    $sql = "SELECT RRN (LR) AS RRN, LR.*
            FROM {$cfg_mod_DeskUtility['file_listini_liraq']} LR
            WHERE LRDT = '{$id_ditta_default}' AND LRART = '{$m_params->c_art}'
            AND LRCCON = '{$m_params->forn}' AND LRLIST = '{$m_params->list}' AND LRTPLI = '{$m_params->tipo}'
            ";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    if(is_array($row)){
    
       /* $dim1 = substr($row['LRFIL1'], 0, 1);
        $dim2 = substr($row['LRFIL1'], 1, 1);
        $dim3 = substr($row['LRFIL1'], 2, 1);
        $p10 = substr($row['LRFIL1'], 3, 1);
        $min_fatt = substr($row['LRFIL1'], 4, 1);*/
        
        $dim1 = $row['LRDIC1'];
        $dim2 = $row['LRDIC2'];
        $dim3 = $row['LRDIC3'];
        $p10  = $row['LRPOCO'];
        $min_fatt = $row['LRMFIM'];
        
        $rrn = $row['RRN'];
    }else{
        $rrn = "";
    }
    
    
       
    ?>
{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'vbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				
					{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,
				        plugins: [
		          			Ext.create('Ext.grid.plugin.CellEditing', {
		            		clicksToEdit: 1
		          			})
		      			],
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
			        	store: {
								editable: false,
								autoDestroy: true,
							    fields: [{name:'qta'}, {name:'prz'}, {name:'sc1'}, {name:'sc2'}, {name:'sc3'}, {name:'sc4'}, {name:'magg'}, {name : 'index'}],
							    data: [								    
							    <?php 
							    
							         for($i = 1; $i<=10; $i++){
							             if($i == 10) $i = 0;
							             
							             if($row["LRQTA{$i}"] != '')
							                 $qta = $row["LRQTA{$i}"];
							             else 
							                 $qta = 0;
							             if($row["LRPRZ{$i}"] != '')
							                 $prz = $row["LRPRZ{$i}"];
							             else
							                 $prz = 0;
							             if($row["LRSC1{$i}"] != '')
							                 $sc1 = $row["LRSC1{$i}"];
							             else 
							                 $sc1 = 0;
						                 if($row["LRSC2{$i}"] != '')
						                     $sc2 = $row["LRSC2{$i}"];
					                     else
					                         $sc2 = 0;
				                         if($row["LRSC3{$i}"] != '')
				                             $sc3 = $row["LRSC3{$i}"];
			                             else
			                                 $sc3 = 0;
		                                 if($row["LRSC4{$i}"] != '')
		                                     $sc4 = $row["LRSC4{$i}"];
	                                     else
	                                         $sc4 = 0;
	                                     if($row["LRMAG{$i}"] != '')
	                                         $mag = $row["LRMAG{$i}"];
                                         else
                                             $mag = 0;
							             
							             $index = $i;
							             
							             if($i == 0) $i = 10;
							             
							         ?>    
							             
							         {index : '<?php echo $index; ?>', qta : '<?php echo $qta; ?>', prz : '<?php echo $prz; ?>', sc1 : '<?php echo $sc1; ?>', sc2 : '<?php echo $sc2; ?>', sc3 : '<?php echo $sc3; ?>', sc4 : '<?php echo $sc4; ?>', magg : '<?php echo $mag; ?>'},
							         
							         <?php     
							         }
							      ?>
							    ]
							},
		
			      columns: [	
			
	                {
	                header   : 'Q.t&agrave; minima',
	                dataIndex: 'qta',
	                align : 'right',
	                renderer: floatRenderer4,
		    		editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            		},
	                flex : 1
	                }, {
	                header   : '',
	                dataIndex: '',
	                width : 10 },
	                {
	                header   : 'Prezzo',
	                dataIndex: 'prz',
	                align : 'right',
	                flex : 1,
	                renderer: floatRenderer5,
		    		editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            		}
	                },{
	                header   : '',
	                dataIndex: '',
	                width : 10 },  
	                {
	                header   : '% Sconto1',
	                dataIndex: 'sc1',
	                align : 'right',
	                flex : 1,
	                renderer: floatRenderer2,
		    		editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            		}
	                },
	                {
	                header   : '',
	                dataIndex: '',
	                width : 10 },
	                {
	                header   : '% Sconto2',
	                dataIndex: 'sc2',
	                align : 'right',
	                flex : 1,
	                renderer: floatRenderer2,
		    		editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            		}
	                },
	                {
	                header   : '',
	                dataIndex: '',
	                width : 10 },
	                {
	                header   : '% Sconto3',
	                dataIndex: 'sc3',
	                align : 'right',
	                flex : 1,
	                renderer: floatRenderer2,  
		    		editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            		}
	                },{
	                header   : '',
	                dataIndex: '',
	                width : 10 }
	                ,{
	                header   : '% Sconto4',
	                dataIndex: 'sc4',
	                align : 'right',
	                flex : 1,
	                renderer: floatRenderer2,
		    		editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            		}
	                },{
	                header   : '',
	                dataIndex: '',
	                width : 10 },
	                {
	                header   : '% Maggior.',
	                dataIndex: 'magg',
	                align : 'right',
	                flex : 1,
	                renderer: floatRenderer2,
		    		editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            		}
	                }
	                
	         ],  listeners: {
	         
	     
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					   
					   
					   
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	         
	          
				  
				 }
			
		
		
		}
		
	
		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.35,
 		            frame: true,
 		            items: [
 		                {
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					value : <?php echo j($rrn); ?>,
					hidden : true				
				   },      {
					name: 'filler',
					fieldLabel : 'lrfil1',
					xtype: 'textfield',
					value : <?php echo j($row['LRFIL1']); ?>,
					hidden : true				
				   },
 		           {
					name: 'f_note',
					fieldLabel : 'Note',
				    labelWidth : 50,
					xtype: 'textfield',
					anchor: '-15',
					maxLength : 40,
					value : <?php echo j(trim($row['LRNOTE']))?>				
				   },  { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
        					 {
            					name: 'dim1',
            					fieldLabel : 'Calcolo quantit&agrave in base alle dimensioni (L/H/S)',
            					labelWidth : 265,
            					xtype: 'textfield',
            					anchor: '-15',
            					maxLength : 1,
            					width : 290,
            					value : <?php echo j(trim($dim1)); ?>
            				  },
            				   {
            					name: 'dim2',
            					fieldLabel : '*',
            				    labelWidth : 5,
            					xtype: 'textfield',
            					anchor: '-15',
            					width : 30,
            					maxLength : 1,
            					labelSeparator : '' ,
            					value : <?php echo j(trim($dim2)); ?>
            				  }, {
            					name: 'dim3',
            					fieldLabel : '*',
            					xtype: 'textfield',
            					anchor: '-15',
            					labelWidth : 5,
            					width : 30,
            					maxLength : 1,
            					labelSeparator : '' ,
            					value : <?php echo j(trim($dim3)); ?>
            				  },
						      {
            					name: 'p10',
            					fieldLabel : 'Diviso 10 "alla"',
            					labelWidth : 100,
            					labelAlign : 'right',
            					xtype: 'textfield',
            					anchor: '-15',
            					maxLength : 1,
            					width : 125,
            					value : <?php echo j(trim($p10)); ?>
            				  },
            				   {fieldLabel : '(0-9)',
            					labelWidth : 40,
            					labelAlign : 'right',
            					xtype: 'displayfield',
            					labelSeparator : '' ,
            					anchor: '-15'
            				  },
						   
						]},
				   
				  { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
        					    {
        					name: 'min_fatt',
        					fieldLabel : 'Minimi fatturabili a importo',
        					xtype: 'textfield',
        					labelWidth : 150,
        					anchor: '-15',
        					width : 175,
        				    maxLength : 1,
        					value : <?php echo j($min_fatt); ?>
        				  },
        				  {fieldLabel : '( /Y)',
        					labelWidth : 40,
        					labelAlign : 'right',
        					xtype: 'displayfield',
        					labelSeparator : '' ,
        					anchor: '-15',
        					maskRe : /Y/
        				  },
						   
						]}
				
				  ], dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
		              var loc_win = this.up('window');
	       			 
        				Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
									},							        
							        success : function(response, opts){
							        	form.getForm().reset();
							        	loc_win.fireEvent('afterModifica', loc_win);
							      
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				        }

			     },   '->',  
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var loc_win = this.up('window'); 
	       			  
	       			  var grid = this.up('window').down('grid');
	       			                             
                      rows_modified = grid.getStore().getUpdatedRecords();
                      list_rows_modified = [];
                            
                      for (var i=0; i<rows_modified.length; i++) {
		              	list_rows_modified.push(rows_modified[i].data);
		              }
 			         
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				params : <?php echo acs_je($m_params); ?>,
	        				list_rows_modified : list_rows_modified
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if(jsonData.success == false){
					      	    acs_show_msg_error(jsonData.msg_error);
					      	    return false;
					        }else{
					        	 loc_win.fireEvent('afterModifica', loc_win);
					        }
					       },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			     
			     ]
		  
			 	 }  
		 ]},
	
					 ],
					 
					
					
	}
	
]}
<?php

exit;
}

