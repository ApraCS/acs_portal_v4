<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];  //descrizione
    $nr['tades2'] = $row['TADES2'];  //marca
    
    $val_tacor2 = find_TA_sys('VAEL', trim($row['TACOR2']));
    if(trim($row['TACOR2']) != ""){
        $nr['tacor2']= "[".trim($row['TACOR2'])."] ".$val_tacor2[0]['text'];
        $nr['val_tacor2'] = trim($row['TACOR2']);
    }else{
        $nr['tacor2'] = "";
    }
    
    
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'tacor2', 'val_tacor2');

    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
     	{
        header   : 'Descrizione',
        dataIndex: 'tadesc',
        flex : 1
        },{
        header   : 'Codice riferimento 2',
        dataIndex: 'tacor2',
        flex : 1
        }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "tacor2" => trim($row->val_tacor2)
        
    );
 
    return $ar_values;
    
}

function get_values_from_array($row){
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "tacor2" => trim($row['val_tacor2'])
        
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		anchor: '-15',	
		maxLength : 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
      {xtype: 'textfield',
		name: 'descrizione',
		fieldLabel: 'Descrizione',
		maxLength : 30,
		width : 300,
		anchor: '-15',	
		value : <?php echo j($ar_values['descrizione']); ?>
		},
       {xtype: 'combo',
    	name: 'tacor2',
    	fieldLabel: 'Riferimento 2',
    	forceSelection: true,	
    	width : 300,							
    	displayField: 'text',
    	valueField: 'id',
    	queryMode: 'local',
    	minChars: 1,							
    	emptyText: '- seleziona -',
    	//allowBlank: false,								
        anchor: '-15',
        value : <?php echo j($ar_values['tacor2']); ?>,
    	store: {
    		editable: false,
    		autoDestroy: true,
    	    fields: [{name:'id'}, {name:'text'}],
    	      data: [								    
    			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('VAEL'), ''); ?>	
    			    ]
    	}
    	 , listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
    	
    	}   
    	
    
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TANR']   = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;
    $ar_ins['TACOR2'] = $form_values->tacor2;
    
    return $ar_ins;
    
}