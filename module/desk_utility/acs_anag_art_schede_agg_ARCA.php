<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    $nr['tades2'] = $row['TADES2'];  
    $nr['coeff'] = substr($row['TAREST'], 0, 6);
    $nr['sigla'] = substr($row['TAREST'], 6, 4);
    $nr['ordine'] = $row['TAORDI'];
    $desc_camp = get_TA_sys('CAMP', trim($row['TACOR2']));
    $nr['d_camp'] = "[".trim($row['TACOR2'])."] ".$desc_camp['text'];
    $nr['camp'] = trim($row['TACOR2']); 
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'ordine', 'coeff', 'sigla', 'camp', 'd_camp');

    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
			{
            header   : 'Ordine',
            dataIndex: 'ordine',
            width : 70
            },
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }, {
            header   : 'Note',
            dataIndex: 'tades2',
            flex : 1
            },  {
            header   : 'Coefficiente',
            dataIndex: 'coeff',
            flex : 1
            },  {
            header   : 'Sigla',
            dataIndex: 'sigla',
            flex : 1
            },
             {
            header   : 'Campagna',
            dataIndex: 'd_camp',
            flex : 1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "note" => trim($row->tades2),
        "coeff" => trim($row->coeff),
        "sigla" => trim($row->sigla),
        "ordine" => trim($row->ordine),
        "camp" => trim($row->camp),
    );
 
    return $ar_values;
    
}

function get_values_from_array($row){
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "note" => trim($row['tades2']),
        "coeff" => trim($row['coeff']),
        "sigla" => trim($row['sigla']),
        "ordine" => trim($row['ordine']),
        "camp" => trim($row['camp']),
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		maxLength: 3,
		anchor: '-15',	
		value:  <?php echo j($ar_values['riga']); ?>
	  },
         {xtype: 'textfield',
		name: 'ordine',
		fieldLabel: 'Ordine',
		maxLength: 5,
		anchor: '-15',	
		value:  <?php echo j($ar_values['ordine']); ?>
		},
       {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
    	  {xtype: 'textfield',
    	name: 'note',
    	fieldLabel: 'Note',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['note']); ?>
    	},   {xtype: 'textfield',
    	name: 'coeff',
    	fieldLabel: 'Coefficiente',
    	maxLength : 6,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['coeff']); ?>
    	},   {xtype: 'textfield',
    	name: 'sigla',
    	fieldLabel: 'Sigla',
    	maxLength : 4,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['sigla']); ?>
    	},{name: 'camp',
			xtype: 'combo',
			fieldLabel: 'Campagna',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['camp']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            width : 400,
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('CAMP'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	 }
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-6s", $form_values->coeff);
    $ar_ins['TAREST'] .= sprintf("%-4s", $form_values->sigla);
    $ar_ins['TANR']  .= sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] .= $form_values->descrizione;
    $ar_ins['TADES2'] .= $form_values->note;
    $ar_ins['TAORDI'] .= $form_values->ordine;
    $ar_ins['TACOR2'] .= $form_values->camp;
    
    
    return $ar_ins;
    
}