<?php


function get_array_data($row){
    global $s, $conn, $cfg_mod_DeskUtility, $id_ditta_default;
    
    $nr = array();
    $ar_mail = array();
    
    $rlrife1 = trim($id_ditta_default)." ".$row['TAID'].trim($row['TANR']);
    
    $sql =  "SELECT *
             FROM {$cfg_mod_DeskUtility['file_note_anag']}
             WHERE RLDT = '{$id_ditta_default}' AND RLTPNO = 'TA' AND RLRIFE1 = '{$rlrife1}'
             AND RLRIFE2 = '*GE'";
    
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);

    while($r = db2_fetch_assoc($stmt)){
     
        $ar['mail'] = trim($r['RLSWST']).trim($r['RLREST1']).trim($r['RLFIL1']);
        $ar_mail[] = $ar;
  
    }

    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = trim($row['TADESC']);
    $nr['articolo'] = trim($row['TACOR1']);
    //$nr['art_in'] = substr($row['TAREST'], 24, 15);
    //$nr['art_fin'] = substr($row['TAREST'], 39, 15);
    $nr['qta_max'] = trim(substr($row['TAREST'], 54, 6));
    $nr['data_in'] = substr($row['TAREST'], 60, 8);
    $nr['data_fin'] = substr($row['TAREST'], 68, 8);
    $nr['blocco'] = substr($row['TAREST'], 76, 1);
    $nr['tot_ric'] = substr($row['TAREST'], 77, 1);
    $nr['qta_min'] = trim(substr($row['TAREST'], 125, 6));
    if(substr($row['TAREST'], 131, 1) == 'Y')
        $nr['mail_soglia'] = $ar_mail[1]['mail'];
    if(substr($row['TAREST'], 132, 1) == 'Y')
        $nr['mail_limite'] = $ar_mail[0]['mail'];
    
       
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'qta_max', 'data_in', 'articolo',
                       'data_fin', 'blocco', 'tot_ric', 'qta_min', 'mail_soglia', 'mail_limite'
    );

    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
			{
            header   : 'Codice',
            dataIndex: 'riga',
            width : 70
            },
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}


function out_component_form(){
    
    global $main_module;
    
    ob_start();
    
    ?>
      {
		name: 'rrn',
		fieldLabel : 'rrn',
		xtype: 'textfield',
		hidden : true						
	   }, {
		name: 'tataid',
		fieldLabel : 'tataid',
		xtype: 'textfield',
		hidden : true						
	   }, {
		name: 'riga',
		fieldLabel : 'Codice',
		xtype: 'textfield',
		hidden : true								
	   },  {
		name: 'articolo',
		fieldLabel : 'articolo',
		xtype: 'textfield',
		hidden : true						
	   },{
		name: 'tadesc',
		fieldLabel : 'Descrizione',
		xtype: 'textfield',
		anchor: '-5'							
	   }, { 
		xtype: 'fieldcontainer',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
		{
		name: 'qta_max',
		fieldLabel : 'Qta max',
		xtype: 'textfield',
		width : 160,
		maxLength : 6							
	    }, {
		name: 'mail_limite',
		fieldLabel : 'Mail limite',
		labelWidth : 60,
		margin : '0 0 0 5',
		xtype: 'textfield',
		labelAlign : 'right',
		anchor: '-5',
		width : 175,
		maxLength : 80							
	   }
		]},
	    { 
		xtype: 'fieldcontainer',
		//flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'
				    },						
		items: [
		 
	    {
	    xtype: 'datefield',
		name: 'data_in',
		fieldLabel : 'Data iniziale',
		startDay: 1 ,
	    format: 'd/m/Y',
	    submitFormat: 'dmY',
	    width : 200				
	   },  {
	    xtype: 'datefield',
		name: 'data_fin',
		labelAlign : 'right',
		fieldLabel : 'finale',
		labelWidth : 35,
		margin : '0 0 0 5',
		startDay: 1 ,
		format: 'd/m/Y',
	    submitFormat: 'dmY',
	    anchor: '-15',
	    width : 135									
	   }
		
		   
		]},
	    {
		name: 'blocco',
		fieldLabel : 'Blocco',
		xtype: 'textfield',
		anchor: '-5',
		maxLength : 1,
		maskRe : /Y/							
	   }, {
		name: 'tot_ric',
		fieldLabel : 'Totalizza/ricalcola',
		xtype: 'textfield',
		anchor: '-5',
		maxLength : 1,
		maskRe : /[R,T]/							
	   },{ 
		xtype: 'fieldcontainer',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
		
		 {
		name: 'qta_min',
		fieldLabel : 'Qta min',
		xtype: 'textfield',
		width : 160,
		maxLength : 6									
	   }, {
		name: 'mail_soglia',
		fieldLabel : 'Mail soglia',
		xtype: 'textfield',
	    labelWidth : 60,
		margin : '0 0 0 5',
		xtype: 'textfield',
		labelAlign : 'right',
		anchor: '-5',
		width : 175,
		maxLength : 80						
	   },
		   
		]}
	   
	   
	 
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}


function out_ar_ins($form_values, $articolo){
    
    $ar_ins = array();
    
    $ar_ins['TADESC'] .= $form_values->tadesc;
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-24s", "");
    $ar_ins['TAREST'] .= sprintf("%-15s", $articolo);
    $ar_ins['TAREST'] .= sprintf("%-15s", $articolo);
    $ar_ins['TAREST'] .= sprintf("%6s", $form_values->qta_max);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_in);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_fin);
    $ar_ins['TAREST'] .= sprintf("%-1s", $form_values->blocco);
    $ar_ins['TAREST'] .= sprintf("%-1s", $form_values->tot_ric);
    $ar_ins['TAREST'] .= sprintf("%-47s", "");
    $ar_ins['TAREST'] .= sprintf("%6s", $form_values->qta_min);
    if(strlen(trim($form_values->mail_soglia)) > 0)
        $ar_ins['TAREST'] .= sprintf("%-1s", "Y");
    if(strlen(trim($form_values->mail_limite)) > 0)
        $ar_ins['TAREST'] .= sprintf("%-1s", "Y");
    
       /* print_r($ar_ins);
        exit;*/
    
    return $ar_ins;
    
}