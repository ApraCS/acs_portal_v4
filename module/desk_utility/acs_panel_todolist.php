<?php

require_once("../../config.inc.php");

$main_module = new DeskUtility(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// RECUPERO FLAG RILASCIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_richiesta_record_rilasciato'){
    
   
    $prog = $m_params->prog;
    
    $sql = "SELECT * FROM {$cfg_mod_DeskArt['file_assegna_ord']} WHERE ASDT = '{$id_ditta_default}' AND ASIDPR = ?";
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt, array($prog));
    
    $r = db2_fetch_assoc($stmt);
    $causali_rilascio = $desk_art->find_TA_std('RILAV', trim($r['ASCAAS']), 'N', 'N', trim($r['ASCARI'])); //recupero tutte le RILAV
    $t_rilav = "<b>{$r['ASCARI']}</b>";
    $t_rilav .= "<br>" .trim($r['ASNORI']);
    
    
    $ret = array();
    $ret['success'] = true;
    $ret['ASFLRI'] = $r['ASFLRI'];
    $ret['d_rilav'] = $causali_rilascio[0]['text'];
    $ret['data_ora'] = print_date(trim($r['ASDTRI']))." - ".print_ora(trim($r['ASHMRI']));
    echo acs_je($ret);
    exit;
} //get_json_data

// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    $as_where = "";
    
    if(isset($form_values->f_todo) && strlen($form_values->f_todo) > 0)
       $as_where .= " AND ASCAAS = '{$form_values->f_todo}'";
        
   if(isset($form_values->f_utente_assegnato) && strlen($form_values->f_utente_assegnato) > 0)
       $as_where .= " AND ASUSAT = '{$form_values->f_utente_assegnato}'";
            
   if(isset($form_values->f_todo_ev) && count($form_values->f_todo_ev) > 0){
        $show_rilav = 'Y';
        $as_where .= " AND ASFLRI = 'Y'";
        $as_where .= sql_where_by_combo_value('ASCARI', $form_values->f_todo_ev);
    }elseif(strlen($m_params->c_art) > 0){
        $as_where .= " AND ASDOCU = '{$m_params->c_art}'";
    }else{
        $show_rilav = 'N';
        $as_where .= " AND ASFLRI <> 'Y'";
    }

    global $backend_ERP;
    if ($backend_ERP != 'GL'){
        $join = "INNER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
                 ON ATT_OPEN.ASDT = AR.ARDT AND ATT_OPEN.ASDOCU = AR.ARART";
        $select = ", AR.ARART AS C_ART, AR.ARDART AS D_ART, AR.ARUMCO AS UM_CO, AR.ARUMTE AS UM_TE,
                    AR.ARUMAL AS UM_AL";
    }else{
        $join = "INNER JOIN {$cfg_mod_DeskArt['file_anag_art']} MA
                 ON ATT_OPEN.ASDOCU = MA.MACAR0";
        $select = ", MA.MACAR0 AS C_ART, MA.MADES0 AS D_ART, MA.MAUM00 AS UM_CO, MA.MAUM10 AS UM_TE,
                    MA.MAUM20 AS UM_AL";
        
    }
    
    if($m_params->params->tab_std == 'Y'){
        $as_where .= " AND ASDOCU = '{$m_params->TAKEY1}'";
    }
    
       
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.*, NT_MEMO.NTMEMO AS MEMO {$select}
    FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT_OPEN
    INNER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
        ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
    {$join}
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
	 ON NT_MEMO.NTDT = '{$id_ditta_default}' AND NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0			
    WHERE ASDT = '{$id_ditta_default}' AND TA_ATTAV.TARIF1 = 'ART' {$as_where} AND ASPROG = ''";
 

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();

    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        //stacco dei livelli
        $cod_liv0 = trim($row['ASCAAS']);
        $cod_liv1 = trim($row['ASDOCU']);
        
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //CAUSALE
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = $row['TADESC'];
            if($show_rilav == 'Y')
                $ar_new['expanded'] = true;
            $ar_new['liv'] = 'liv_0';
            
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        
        //ARTICOLO
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            
            $ar_new['id'] = implode("|", $tmp_ar_id);
            
            if ($backend_ERP == 'GL'){
                if($row['ASPRGA'] > 0)
                    $ar_new['task'] =  trim($row['ASDOCU']).".".$row['ASPRGA'];
                else
                    $ar_new['task'] =  $row['ASDOCU'];
            }else{
                $ar_new['task'] =  $row['ASDOCU'];
           }
            if($show_rilav == 'Y')
                $ar_new['task'] .= "<br><span style='float: right;'>{$row['ASCARI']}</span>" ;
            
               
            $ar_new['articolo'] =  $row['ASDOCU'];
            
            //if($show_rilav != 'Y')
                $ar_new['flag'] =  trim($row['ASFLRI']);
            
            $ar_new['prog'] =  $row['ASIDPR'];
            $ar_new['d_art'] =  $row['D_ART'];
            if($show_rilav == 'Y'){
                $causali_rilascio = $desk_art->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                $ar_new['d_art'] .=  "<br><span style='float: right;'>".$causali_rilascio[0]['text']."</span>";
            } 
      
            if(trim($row['UM_CO']) == trim($row['UM_TE']) && trim($row['UM_CO']) == trim($row['UM_AL'])){
                $um = $row['UM_TE'];
            }elseif((trim($row['UM_AL']) == trim($row['UM_TE']) && trim($row['UM_AL']) != trim($row['UM_CO'])) ||
                (trim($row['UM_AL']) == trim($row['UM_CO']) && trim($row['UM_AL']) != trim($row['UM_TE']))){
                    $um = $row['UM_TE']. " - " . trim($row['UM_CO']);
            }else{
                $um = trim($row['UM_TE']) . " - " . trim($row['UM_CO']) . " - " . trim($row['UM_AL']);
            }
            $ar_new['um'] = $um;
            $ar_new['imm'] = print_date(trim($row['ASDTAS']))." - ".print_ora(trim($row['ASHMAS']));
            if($show_rilav == 'Y')
                $ar_new['imm'] .= "<br>" .print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
            
            $ar_new['ut_ass'] = trim($row['ASUSAT']);
            $ar_new['ut_ins'] = trim($row['ASUSAS']);
            $ar_new['scadenza'] = trim($row['ASDTSC']);
            $ar_new['memo'] = trim($row['MEMO']);
            if($show_rilav == 'Y')
                $ar_new['memo'] .= trim($row['ASNORI']);
            $ar_new['riferimento'] = trim($row['ASNOTE']);
            $ar_new['f_ril'] =  trim($row['ASFLNR']);
            $ar_new['liv'] = 'liv_1';
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
        }
        
    }
    
    
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}

if ($_REQUEST['fn'] == 'open_filtri'){
    
    
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
    $ar_users_json = acs_je($user_to);
    
   ?> 
    
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            flex: 1,
	            items: [
	          
                  		 {
						name: 'f_todo',
						xtype: 'combo',
						flex : 1,
						fieldLabel: 'To Do',
						labelWidth : 110,
						displayField: 'text',
						anchor: '-15',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: true,													
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php
							     // echo acs_ar_to_select_json($s->find_TA_std('ATTAV', N, 'Y'), "");
							     echo acs_ar_to_select_json($desk_art->find_TA_std('ATTAV', null, 'Y', 'N', null, null, 'ART'), "");
							       ?>	
							    ] 
						},listeners: {
                    				change: function(field,newVal) {
                    				
                                    	 combo_todo_ev = this.up('form').down('#todo_ev');    
                                         combo_todo_ev.store.proxy.extraParams.takey1 = newVal;
                                    	 combo_todo_ev.store.load();                             
                             			
                             

                            }
                        }						 
					}, {
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente',
			            queryMode: 'local',
			            selectOnTab: false,
			            flex : 1,
			            anchor: '-15',
			            name: 'f_utente_assegnato',
			            allowBlank: true,
						forceSelection: true,			            
						labelWidth : 110
			        }, {
						name: 'f_todo_ev',
						xtype: 'combo',
						itemId: 'todo_ev',
						fieldLabel: 'To Do evase',
						multiSelect : true, 
						labelWidth : 110,
						anchor: '-15',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
						flex : 1,
					   	allowBlank: true,	
					   	store: {
                            autoLoad: true,
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
                                url : 'acs_panel_anag_art.php?fn=get_json_data_rilav',
					            reader: {
					                type: 'json',
	                                method: 'POST',		
					                root: 'root',
					            },
                                actionMethods: {
    							          read: 'POST'
    							        },
                                extraParams: {
               		    		    		takey1: ''
               		    		       },               						        
				        		doRequest: personalizza_extraParams_to_jsonData
					        },       
								fields: ['id', 'text'],            	
			            }												
							 
					}
	            ],
	            
				buttons: [
					 {
			            text: 'Progetti',
				        iconCls: 'icon-windows-32',		            
				        scale: 'large',		            
			              handler: function() {
	            
	            			var form = this.up('form').getForm();
	            			acs_show_panel_std('acs_panel_todolist_progetto.php?fn=open_panel', null, {
        		            	form_values: form.getValues()}, null, null);
        		         	  this.up('window').close();			                   	                	                
	            		}
			        }, {
			            text: 'Progetti',
			            iconCls: 'icon-print-32',
			            scale: 'large',
			           	handler: function() {
	                      var form = this.up('form').getForm();
	                      var form_values = form.getValues();
	                      this.up('form').submit({
                              url: 'acs_panel_todo_report.php',
                          	  target: '_blank', 
                              standardSubmit: true,
                              method: 'POST',                        
                              params: {
                                  form_values: Ext.encode(form.getValues()),
                                  progetto: 'Y'
    					      }
                  			}); 
	                            	                	     
        	          }
			         }, {
			            text: 'Grafici',
			            iconCls: 'icon-grafici-32',
			            scale: 'large',
			           	handler: function() {
	                      var form = this.up('form').getForm();
	                      var form_values = form.getValues();
	                    
	            			  acs_show_panel_std('acs_panel_todolist_grafici.php?fn=open', null, {
        		              form_values: form.getValues()}, null, null);
        		         	  this.up('window').close();	
	                            	                	     
        	          }
			         }, {xtype: 'tbfill'},									
				      {
			            text: 'Attivit&agrave;',
				        iconCls: 'icon-windows-32',		            
				        scale: 'large',		            
			            handler: function() {
	            
	            			var form = this.up('form').getForm();
	            			acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_panel', null, {
        		            	form_values: form.getValues()}, null, null);
        		         	  this.up('window').close();
			                   	                	                
	            		}
			         },{
			            text: 'Attivit&agrave;',
			            iconCls: 'icon-print-32',
			            scale: 'large',
			           	handler: function() {
	                      var form = this.up('form').getForm();
	                      var form_values = form.getValues();
	                      this.up('form').submit({
                              url: 'acs_panel_todo_report.php',
                          	  target: '_blank', 
                              standardSubmit: true,
                              method: 'POST',                        
                              params: {
                                  form_values: Ext.encode(form.getValues())
                               }
                  			}); 
	                            	                	     
        	          }
			         }
		        
		        
		        ]          
	            
	           
	}
		
	]}
    
    <?php 
    
    exit;
}


if ($_REQUEST['fn'] == 'open_panel'){
?>

{"success":true, "items": [

        {
        xtype: 'treepanel' ,
        multiSelect: true,
        cls: 's_giallo',
        title: 'To Do list' ,
	    tbar: new Ext.Toolbar({
	            items:['<b> Gestione attivit&agrave; di manutenzione base dati articoli</b>', '->',
    	             {iconCls: 'icon-gear-16',
    	             text: 'CAUSALI', 
		           		handler: function(event, toolEl, panel){
			           		acs_show_panel_std('acs_gest_ATTAV.php?fn=open_tab', 
			           			'panel_gestione_attav', {sezione: 'ART'});
			          
		           		 }
		           	 }
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,            
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['id', 'task', 'liv', 'd_art', 'um', 'flag', 'prog', 'articolo', 'imm', 'ut_ins', 'ut_ass', 'scadenza', 'riferimento', 'memo', 'f_ril'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
	                      form_values: <?php echo acs_je($m_params->form_values); ?>,
	                      c_art : <?php echo j($m_params->c_art); ?>
	                  
                      }
                    , doRequest: personalizza_extraParams_to_jsonData  ,     
						reader: {
                            root: 'children'
                        }        				
                    }

                }),
    	    			
            columns: [{xtype: 'treecolumn', 
        	    		text: 'Causale/Articolo', 	
        	    		width: 150,
        	    		dataIndex: 'task'
        	    		},
        	    		{text: 'Descrizione', flex:1, dataIndex: 'd_art',
        	    		  renderer: function(value, p, record){
	            	        p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
						   	
    					    return value;
    		    	     }},	
	 					{text: 'UM', width: 40, dataIndex: 'um'},	
	 					{text: 'Immissione', width: 120, dataIndex: 'imm'},	
	 					{text: 'Utente', width: 100, dataIndex: 'ut_ins'},	
	 					{text: 'Utente assegnato', width: 110, dataIndex: 'ut_ass'},	
	 					{text: 'Scadenza', width: 80, dataIndex: 'scadenza', renderer : date_from_AS},	
	 					{text: 'Riferimento', width: 100, dataIndex: 'riferimento',
	 					renderer: function(value, p, record){
	            	        p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
						   	
    					    return value;
    		    	     }},
	 					{text: 'Memo', width: 200, dataIndex: 'memo',
	 					renderer: function(value, p, record){
	            	        p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
						   	
    					    return value;
    		    	     }},	
        	    		
    	    ],
    	     listeners: {
    	     
    	     	beforeload: function(store, options) {
    		     var t_win = this.up('window');
                    Ext.getBody().mask('Loading... ', 'loading').show();
                    },
    
                load: function () {
                 Ext.getBody().unmask(); 
                },  
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					     id_selected = grid.getSelectionModel().getSelection();
					     list_selected_id = [];
					     for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push({id: id_selected[i].get('id'), 
									prog: id_selected[i].get('prog'), 
									articolo: id_selected[i].get('articolo'),
									f_ril: id_selected[i].get('f_ril')});
					     
					     if (rec.get('liv') == 'liv_1'){
					     
					        voci_menu.push({
                 		text: 'Modifica ToDo',
                		iconCls : 'icon-pencil-16',          		
                		handler: function () {
                		
                		 list_rows = [];
					     for (var i=0; i<id_selected.length; i++) 
							list_rows.push({prog: id_selected[i].get('prog'),
											flag: id_selected[i].get('flag')});
                		
    		    		   var my_listeners = {
	    		  			afterOkSave: function(from_win){
	    		  			   // grid.getTreeStore().load();
        						from_win.close();  
				        		}
		    				};
				        	acs_show_win_std('Modifica ToDo', 'acs_modifica_todo.php?fn=open_mod', {list_selected_id : list_rows, only_rilav : rec.get('only_rilav')}, 370, 250, my_listeners, 'icon-pencil-16');          		
        	                }
            		  });
					      
					    voci_menu.push({
			      		text: 'Avanzamento/Rilascio attivit&agrave;',
			    		iconCls: 'icon-arrivi-16',
			    		handler: function() {

  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('flag', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							        list_selected_id: list_selected_id, 
							        grid_id: grid.id},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });
					  
			<?php
            $causali_rilascio = $desk_art->find_TA_std('RILAV', null, 'N', 'Y'); //recupero tutte le RILAV

            foreach($causali_rilascio as $ca) {
     
            ?>	

			
	if (rec.get('flag')!='Y' && rec.get('id').split("|")[0] == <?php echo j(trim($ca['id'])); ?>){ 		  
	
	voci_menu.push({
	      		text: <?php echo j($ca['text']); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {

				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('flag', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							           			list_selected_id: list_selected_id, grid_id: grid.id,
												auto_set_causale: <?php echo j(trim($ca['TAKEY2'])); ?>							        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
	    
						    
		
			    }
			  });
			  
			  }
  <?php } ?>
					  
					  
					     }
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	        
				  
				 },
				 
				 viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('flag') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }	
    	    
    	    
    	}	
	 	
	]  	
  }


<?php 
exit;
}