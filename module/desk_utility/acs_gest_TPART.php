<?php

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $desk_art->get_cfg_mod();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 

$m_table_config = array(
    'module'      => $desk_art,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "TPART - Tipologia import",
    'descrizione' => "Tipologia articoli",
    'form_title' => "Dettagli tabella tipologia import dati articolo",
    
    'fields_preset' => array(
        'TATAID' => 'TPART'
    ),
   
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),
    'fields_grid' => array(/*'TASTAL',*/ 'TAKEY1', 'TADESC', 'TAMAIL', 'TAINDI', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TAINDI', 'TALOCA'),
    
    'fields' => array(
        //'TASTAL'     => array('label'	=> $divieto, 'fw'=>'width: 40'),
        'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        
        'TAINDI' => array('label'	=> 'Etic. foglio', 'maxLength' => 60),
        'TALOCA' => array('label'	=> 'Immagini', 'maxLength' => 60),
        
          //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
                
    
        )

        );


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
