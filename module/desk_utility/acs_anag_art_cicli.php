<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_mod_cicli'){
    $ret = array();
  
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
  
    $ar_ins['CISEQ1'] =  $form_values->seq;
    if(strlen($form_values->c_fase) > 0)
        $ar_ins['CIFASE'] =  $form_values->c_fase;
    if(strlen($form_values->c_centro) > 0)
        $ar_ins['CICELO'] =  $form_values->c_centro;
    $ar_ins['CITEMO'] =  sql_f($form_values->man);
    $ar_ins['CITEAT'] =  sql_f($form_values->att);
    $ar_ins['CITESO'] =  sql_f($form_values->sos);
    $ar_ins['CITEMU'] =  sql_f($form_values->macchina);
    $ar_ins['CITETR'] =  sql_f($form_values->trasferimento);
    if(isset($form_values->um_man) && strlen($form_values->um_man) > 0)
        $ar_ins['CIUMMO'] =  $form_values->um_man;
    if(isset($form_values->um_att) && strlen($form_values->um_att) > 0)
        $ar_ins['CIUMAT'] =  $form_values->um_att;
    if(isset($form_values->um_sos) && strlen($form_values->um_sos) > 0)
        $ar_ins['CIUMSO'] =  $form_values->um_sos;
    if(isset($form_values->um_mac) && strlen($form_values->um_mac) > 0)
        $ar_ins['CIUM'] =  $form_values->um_mac;
    if(isset($form_values->um_trasf) && strlen($form_values->um_trasf) > 0)
        $ar_ins['CIUMTR'] =  $form_values->um_trasf;
    
        $ar_ins['CISWIE'] 	=  $form_values->swie;
  
    for($i = 1; $i <= 4; $i++){
        $v = "CIVAR{$i}";
        if (isset($form_values->$v)){
            $v = "CIVAR{$i}"; $ar_ins["CIVAR{$i}"] =  $form_values->$v;
            $v = "CITPS{$i}"; $ar_ins["CITPS{$i}"] =  $form_values->$v;
            $v = "CIVAN{$i}"; $ar_ins["CIVAN{$i}"] =  $form_values->$v;
            $v = "CIOSS{$i}"; $ar_ins["CIOSS{$i}"] =  $form_values->$v;
        }
    }
    
    if(isset($m_params->form_values->rrn) && $m_params->form_values->rrn != ''){
        
        $ar_ins['CIDTUM'] = oggi_AS_date();
        $ar_ins['CIUSUM'] = $auth->get_user();
        
        
        $sql = "UPDATE {$cfg_mod_DeskUtility['file_cicli']} CI
        SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
        WHERE RRN(CI) = '{$m_params->form_values->rrn}'";
        
    }else{
        
        $ar_ins['CIDTGE'] = oggi_AS_date();
        $ar_ins['CIUSGE'] = $auth->get_user();
        $ar_ins['CIDTUM'] = oggi_AS_date();
        $ar_ins['CIUSUM'] = $auth->get_user();
        
        $ar_ins["CIDT"] = $id_ditta_default;
        $ar_ins["CIART"] = $m_params->c_art;
        
        
        //aggiungere progressivo?
        
        $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_cicli']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    }
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_canc'){
    
    $rrn= $m_params->form_values->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_cicli']} CI WHERE RRN(CI) = '{$rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;

}


if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $c_art = $m_params->open_request->c_art;
    
    $sql = "SELECT RRN(CI) AS RRN, CI.*, CV.NR_COND AS COND
            FROM {$cfg_mod_DeskUtility['file_cicli']} CI
            LEFT OUTER JOIN (
               SELECT COUNT(*) AS NR_COND, CVSEQ1, CVSEQ2, CVART
               FROM {$cfg_mod_DeskUtility['file_condizioni_cicli']} 
               WHERE CVDT = '{$id_ditta_default}' AND CVART = '{$c_art}'
               GROUP BY CVSEQ1, CVSEQ2, CVART) CV
            ON CV.CVSEQ1 = CI.CISEQ1 AND CV.CVSEQ2 = CI.CISEQ2 AND CV.CVART = CI.CIART 
            WHERE CIDT = '{$id_ditta_default}' AND CIART = '{$c_art}'
            ORDER BY CISEQ1";
              
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
            while($row = db2_fetch_assoc($stmt)){
                $nr = array();
                
                $nr['seq']   = trim($row['CISEQ1']);
                $nr['seq2']   = trim($row['CISEQ2']);
                $nr['c_art']   = trim($row['CIART']);
                $nr['rrn']   = $row['RRN'];
                $ta_sys = find_TA_sys('PUFA', trim($row['CIFASE']));
                $nr['fase'] 	=  "[".trim($row['CIFASE'])."] ".$ta_sys[0]['text'];
                $ta_sys = find_TA_sys('PUCE', trim($row['CICELO']));
                $nr['centro'] 	=  "[".trim($row['CICELO'])."] ".$ta_sys[0]['text'];
                if($row['CITEMO'] > 0)
                    $nr['t_man'] = "[".$row['CIUMMO']."] ".n($row['CITEMO'],3);
                if($row['CITEAT'] > 0)
                    $nr['t_att'] = "[".$row['CIUMAT']."] ".n($row['CITEAT'],3);
                if($row['CITESO'] > 0)
                    $nr['t_sos'] = "[".$row['CIUMSO']."] ".n($row['CITESO'],3);
             
                if($row['CIDTUM'] > 0)
                    $nr['data_imm']  = $row['CIDTUM'];
                else
                    $nr['data_imm']  = $row['CIDTGE'];
                
                if(trim($row['CIUSUM']) !=  '')
                    $nr['ut_imm']  = $row['CIUSUM'];
                else
                    $nr['ut_imm']  = $row['CIUSGE'];
                
                $nr['c_fase']  = trim($row['CIFASE']);
                $nr['c_centro']  = trim($row['CICELO']);
                $nr['man']  = $row['CITEMO'];
                $nr['att']  = $row['CITEAT'];
                $nr['sos']  = $row['CITESO'];
                $nr['um_man']  = trim($row['CIUMMO']);
                $nr['um_att']  = trim($row['CIUMAT']);
                $nr['um_sos']  = trim($row['CIUMSO']);
                
                $nr['macchina']  = $row['CITEMU'];
                $nr['trasferimento']  = $row['CITETR'];
                $nr['um_mac']  = trim($row['CIUM']);
                $nr['um_trasf']  = trim($row['CIUMTR']);
                
                $nr['swie']  = trim($row['CISWIE']);
                $count = 0;
                for($i = 1; $i <= 4; $i++){
                     $nr["CIVAR{$i}"]  = trim($row["CIVAR{$i}"]);
                     $nr["CITPS{$i}"]  = trim($row["CITPS{$i}"]);
                     $nr["CIVAN{$i}"]  = trim($row["CIVAN{$i}"]);
                     $nr["CIOSS{$i}"]  = trim($row["CIOSS{$i}"]);
                     
                     if(trim($row["CIVAR{$i}"]) != '')
                         $count++;
		        }
		        $nr['cond']  = $count + $row['COND'];
		        $nr['cond_cv']  = $row['COND'];
		        $nr['cond_ci']  = $count;
                
                $ar[] = $nr;
            }
            
            echo acs_je($ar);
            exit;
}

if ($_REQUEST['fn'] == 'open_tab'){?>

{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
						{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['cond', 'cond_cv', 'cond_ci', 'rrn', 'seq', 'seq2', 'c_art', 'fase', 'centro', 't_man', 't_att', 't_sos', 'data_imm', 'macchina', 'trasferimento',
		        			'ut_imm', 'c_fase', 'c_centro', 'man', 'att', 'sos', 'um_man', 'um_att', 'um_sos', 'swie', 'um_trasf', 'um_mac',
		        			<?php for($i = 1; $i <= 4; $i++){?>
		        				'CIVAR<?php echo $i?>',
		        				'CITPS<?php echo $i?>',
		        				'CIVAN<?php echo $i?>',
		        				'CIOSS<?php echo $i?>',
		        			<?php }?>
		        			]							
									
			}, //store
				<?php $cond = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
			
			      columns: [	
			      {
	                header   : 'Seq.',
	                dataIndex: 'seq',
	                width : 60
	                },
	                {
	                header   : 'Fase',
	                dataIndex: 'fase',
	                flex: 1
	                },
	                {
	                header   : 'Centro',
	                dataIndex: 'centro',
	                flex: 1
	                },{header: 'Tempo',
                    columns: [
                      {header: 'Manodopera', dataIndex: 't_man', width: 80},
                      {header: 'Attrezzaggio', dataIndex: 't_att', width: 80}, 
                      {header: 'Sosta', dataIndex: 't_sos', width: 70}, 
                 	 ]},
                 	{text: '<?php echo $cond; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'cond',
    			    filter: {type: 'string'}, filterable: true,
    				tooltip: 'condizioni',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('cond_cv') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_black_yuan.png") ?> width=15>';
    		    			  if(record.get('cond_ci') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_blue_yuan.png") ?> width=15>';
    		    		  }
    		        },
                 	 {header: 'Immissione',
                    columns: [
                      {header: 'Data', dataIndex: 'data_imm', width: 80, renderer : date_from_AS},
                      {header: 'Utente', dataIndex: 'ut_imm', width: 80}
                   	 ]}    
	                
	         ],   listeners: {
	           selectionchange: function(selModel, selected) { 
	               if(selected.length > 0){
	                   var form_dx = this.up('panel').down('#dx_form');
		               //pulisco eventuali filtri
		               form_dx.getForm().reset();
		               acs_form_setValues(form_dx, selected[0].data);
	                 }
		          }		    	
			   , celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           		           						  						  	
					  	var grid = this, 
					  		col_name = iView.getGridColumns()[iColIdx].dataIndex,
					  		rec = iView.getRecord(iRowEl);
					  		
					  				
					    if(col_name == 'cond'){
							acs_show_win_std('Condizioni di elaborazione variabili [' + rec.get('c_art') + ', ' + rec.get('seq') + ', ' + rec.get('seq2') + ']', 
								'acs_anag_art_cicli_cond.php?fn=open_cond', {
								record : rec.data
							}, 1100, 550, {
								close: function(comp){
										if (!Ext.isEmpty(grid)) grid.store.load();
            				  		}
							}, 'icon-tools-16');
						  return;
						}
					  		
					  		
					  		
					  		}
					  		
					  		
					  		}
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            title : 'Dettagli ciclo',
 		            flex:0.3,
 		            frame: true,
 		            items: [    
 		              {
    					name: 'rrn',
    					fieldLabel : 'rrn',
    					xtype: 'textfield',
    					hidden : true							
    				   }, 
 		                 {xtype: 'textfield',
    						name: 'seq',
    						fieldLabel: 'Seq.',
    						anchor: '-15',	
    				     },
	                     {
								name: 'c_fase',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'Fase',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('PUFA', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
								    ]
								}, 
								
						   } ,
    						    {
								name: 'c_centro',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'Centro',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('PUCE', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
								    ]
								}, 
								
						   }, { 
						xtype: 'fieldset',
						title: 'Tempo',
						//border : false,
						margin : '0 0 0 0',
	                    layout: 'anchor',
	                    items: [
	                    
	                    		 {xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {xtype: 'numberfield',
            			    name: 'man',
    						fieldLabel: 'Manodopera',
    						hideTrigger : true,
    						anchor: '-15',	
    						},
								{
								name: 'um_man',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'UM',
								margin : '0 0 0 5',
								labelWidth : 20,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
							    store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'M', text : '[M] Minuti'},
								     	 {id: 'S', text : '[S] Secondi'},
								     	 {id: 'H', text : '[H] Ore'}
								     	
								    ]
								}
								
						   }
						
						]},  {xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						              	{xtype: 'numberfield',
    						name: 'att',
    						fieldLabel: 'Attrezzaggio',
    					    hideTrigger : true,
    						anchor: '-15',	
    						},
								{
								name: 'um_att',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'UM',
								margin : '0 0 0 5',
								labelWidth : 20,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'M', text : '[M] Minuti'},
								     	 {id: 'S', text : '[S] Secondi'},
								     	 {id: 'H', text : '[H] Ore'}
								     	
								    ]
								}
								
						   }
						
						]}, 
    											
				
							 {xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {xtype: 'numberfield',
    						hideTrigger : true,
    						name: 'sos',
    						fieldLabel: 'Sosta',
    						anchor: '-15',	
    					   }, {
								name: 'um_sos',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'UM',
								margin : '0 0 0 5',
								labelWidth : 20,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'M', text : '[M] Minuti'},
								     	 {id: 'S', text : '[S] Secondi'},
								     	 {id: 'H', text : '[H] Ore'}
								     	
								    ]
								}
								
						   }
						
						]},
						
					       {xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	{xtype: 'numberfield',
    						name: 'macchina',
    						fieldLabel: 'Macchina',
    					    hideTrigger : true,
    						anchor: '-15',	
    						}, 
    						{
								name: 'um_mac',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'UM',
								margin : '0 0 0 5',
								labelWidth : 20,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'M', text : '[M] Minuti'},
								     	 {id: 'S', text : '[S] Secondi'},
								     	 {id: 'H', text : '[H] Ore'}
								     	
								    ]
								}
								
						   }
    						]},
    						 {xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
					      	{xtype: 'numberfield',
    						name: 'trasferimento',
    						fieldLabel: 'Trasferimento',
    					    hideTrigger : true,
    						anchor: '-15',	
    						},{
								name: 'um_trasf',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'UM',
								margin : '0 0 0 5',
								labelWidth : 20,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'M', text : '[M] Minuti'},
								     	 {id: 'S', text : '[S] Secondi'},
								     	 {id: 'H', text : '[H] Ore'}
								     	
								    ]
								}
								
						   }]},
						
						{
                			name: 'swie',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [		
                				     {id: '', text : '- vuoto -'},						    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
                		 
                		 
                		 <?php
                		 $proc = new ApiProc();
                		 for($i = 1; $i <=4 ; $i++){
                		   echo $proc->get_json_response(
        				      extjs_combo_regola_config(array('label' =>"Condizione ({$i})",
        				      'f_var' => "CIVAR{$i}",
        				      'f_tp'  => "CITPS{$i}",
        				      'f_van' => "CIVAN{$i}",
        				      'f_os'  => "CIOSS{$i}"
                              ))
        				  );
        				 echo ",";
                        		  
						}?>
 		            
 		            
 		            ]}
					 ],
					 
					 <?php if($m_params->from_lotto1 != 'Y'){?>
					  dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
 			         
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);							        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     },'->',
                 
                  
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			    var grid = this.up('form').up('panel').down('grid');
 			         form_values.rrn = '';
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_cicli',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				c_art : '<?php echo $m_params->c_art ?>',
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			  var grid = this.up('form').up('panel').down('grid');
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_cicli',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				c_art : '<?php echo $m_params->c_art ?>',
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     }
			     ]
		   }]	
		   <?php }?>				 
					 
					 
				
					 
					
					
	} //form
	
]} 
]}

<?php

exit;
}

