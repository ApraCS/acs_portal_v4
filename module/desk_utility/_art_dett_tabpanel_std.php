<?php
require_once '_art_dett_include_f.php';
$desk_art = new DeskArt();

/* p : panel */
$p_defaults = "labelWidth: 140, width: 650";

/* f_c : field container */
$f_c_hbox_op = "
  { xtype: 'fieldcontainer'
  , layout:
      {	type: 'hbox'
      , pack: 'start'
	  , align: 'stretch'
        }
  , defaults:
      { labelWidth: 140
      , margin: '0 10 0 0'}
  , items: [";
$f_c_hbox_cl = " ]
            }";

if($m_params->g_multi == 'Y')
   $multi = 'Y';
else
   $multi = 'N';
?>

<!-- ----------------------------------------------------------------------------------  -->
<!--  Classificazioni                                                                    -->
<!-- ----------------------------------------------------------------------------------  -->
new Ext.create('Ext.form.Panel', {
    title: 'Classificazioni',
    gestione_std: 'Y',
    flex : 1,
    bodyPadding: 5,
    autoScroll: true,
    layout: 'anchor',
    defaults: { <?php echo $p_defaults ?> },
    items: [
	  <?php if($multi != 'Y') echo _intestazione() ?>,
	  <?php echo _outfield('ARTPAR') ?>, 
      <?php echo _outfield('ARCLME') ?>,   
      <?php echo _outfield('ARGRME') ?>,    
      <?php echo _outfield('ARSGME') ?>,   
      <?php echo _outfield('ARSTA1') ?>,    
      <?php echo _outfield('ARST21') ?>,    
      <?php echo _outfield('ARST22') ?>,    
      <?php echo _outfield('ARST23') ?>,    
      <?php echo _outfield('ARCDIV') ?>,    
      <?php echo _outfield('ARCLFI') ?>,    
      <?php echo _outfield('ARGRFI') ?>,    
      <?php echo _outfield('ARCENC') ?>,    
      <?php echo _outfield('ARCENA') ?>,    
      <?php echo _outfield('ARTBVE') ?>,    
      <?php echo _outfield('ARTBAC') ?>,    
      <?php echo _outfield('ARANAL') ?>, 
      <?php echo _outfield('ARROYA') ?>,  
       
    ]
}),

<!-- ----------------------------------------------------------------------------------  -->
<!--  Speficiche                                                                         -->
<!-- ----------------------------------------------------------------------------------  -->
new Ext.create('Ext.form.Panel', {
    title: 'Specifiche',
    gestione_std: 'Y',
    flex : 1,
    bodyPadding: 5,
    autoScroll: true,
    layout: 'anchor',
    defaults: { <?php echo $p_defaults ?> },
  	items: [
    	<?php if($multi != 'Y') echo _intestazione() ?>,

    <!-- "$f_c_hbox_op e $f_c_hbox_cl  servono a creare il fieldcontainer orizzontale" -->
     
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARDIM1', $multi) ?>,
        <?php echo _outfield('ARARFO', $multi)?>, 
    	<?php echo $f_c_hbox_cl ?>,
         
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARDIM2',$multi) ?>,
        <?php echo _outfield('ARGRAL', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,
         
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARDIM3', $multi) ?>,
    	<?php echo _outfield('ARRIF', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,
         
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARPNET', $multi) ?>,
        <?php echo _outfield('ARDIM4', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,
         
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARPLOR', $multi) ?>,
        <?php echo _outfield('ARDIM5', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,
         
        <?php echo $f_c_hbox_op ?>,
    	<?php echo _outfield('ARPSPE', $multi) ?>,
    	<?php if($m_params->g_multi != 'Y'){?>
    	<?php echo _outfield('U_M', $multi) ?>,
    	<?php }?>
    	<?php echo _outfield('U_M_cb') ?>,
    	<?php echo $f_c_hbox_cl ?>,
    	
    	<?php echo _outfield('ARUMTE') ?>,
    	<?php echo _outfield('ARUMAL') ?>,
    	<?php echo _outfield('ARUMCO') ?>,
    
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARVOLU', $multi) ?>,
        <?php echo _outfield('ARVOLK', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,
         
        <?php echo $f_c_hbox_op ?>,
    	<?php echo _outfield('ARCOLL', $multi) ?>,
    	<?php echo _outfield('ARFLG1', $multi) ?>,  //ARCOLK
    	<?php echo $f_c_hbox_cl ?>,
    	
    	<?php echo _outfield('ARRCOL') ?>,
    	
    	<?php echo $f_c_hbox_op ?>,
    	<?php echo _outfield('ARCOLK', $multi) ?>,
    	<?php echo _outfield('ARFLG2', $multi) ?>,  
    	<?php echo $f_c_hbox_cl ?>,
    	
        <?php echo _outfield('ARRCOK') ?>,
               
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('data_val_ini') ?>,
        <?php echo _outfield('data_val_fin') ?>,
        <?php echo $f_c_hbox_cl ?>,
        
        <?php echo $f_c_hbox_op ?>,    

        <?php echo _outfield('ARFLR1') ?>,
        <?php echo _outfield('ARFLR2') ?>, 
        <?php echo $f_c_hbox_cl ?>,

        <?php echo $f_c_hbox_op ?>,               
        <?php echo _outfield('ARFLR7') ?>,
        <?php echo _outfield('ARFLR4', $multi) ?>,
        <?php echo $f_c_hbox_cl ?>,
        
        <?php echo $f_c_hbox_op ?>,                       
        <?php echo _outfield('ARFLR6') ?>,
        <?php echo _outfield('ARSWES') ?>,
        <?php echo $f_c_hbox_cl ?>,        

        <?php echo $f_c_hbox_op ?>,               
        <?php echo _outfield('ARFLR8') ?>,
        <?php echo _outfield('ARSWE1') ?>,
        <?php echo $f_c_hbox_cl ?>,        

        <?php echo $f_c_hbox_op ?>,               
        <?php echo _outfield('ARMAUT') ?>,    
        <?php echo _outfield('ARSWE2') ?>,
        <?php echo $f_c_hbox_cl ?>,     
    ]
}),

<!-- ----------------------------------------------------------------------------------  -->
<!--  Movimentazione   // al momento tab inibita //                                      -->
<!-- ----------------------------------------------------------------------------------  -->
/*
new Ext.create('Ext.form.Panel', {
    title: 'Movimentazione',
    gestione_std: 'Y',
    flex : 1,
    bodyPadding: 5,
    autoScroll: true,
    layout: 'anchor',
    defaults: { <?php echo $p_defaults ?> },
    items: [
        <?php echo _intestazione() ?>,

    ]
}), 
*/

<!-- ----------------------------------------------------------------------------------  -->
<!--  Configurazione                                                                     -->
<!-- ----------------------------------------------------------------------------------  -->
new Ext.create('Ext.form.Panel', {
    title: 'Configurazione',
    gestione_std: 'Y',
    flex : 1,
    bodyPadding: 5,
    autoScroll: true,
    layout: 'anchor',
    defaults: { <?php echo $p_defaults ?> },
    items: [
        <?php if($m_params->g_multi != 'Y') echo _intestazione() ?>,
      	<?php echo _outfield('ARMODE') ?>,     
        <?php echo _outfield('ARMOCO') ?>, 
             
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARVAR1') ?>,     
        <?php echo _outfield('ARGVA1') ?>,
    	<?php echo $f_c_hbox_cl ?>,
            
        <?php echo $f_c_hbox_op ?>, 
        <?php echo _outfield('ARVAR2') ?>,     
        <?php echo _outfield('ARGVA2') ?>,
    	<?php echo $f_c_hbox_cl ?>,
    
        <?php echo $f_c_hbox_op ?>, 
        <?php echo _outfield('ARVAR3') ?>,     
        <?php echo _outfield('ARGVA3') ?>, 
    	<?php echo $f_c_hbox_cl ?>,
    
        <?php echo $f_c_hbox_op ?>,        
        <?php echo _outfield('ARVDI1') ?>,     
        <?php echo _outfield('ARGDI1') ?>,
    	<?php echo $f_c_hbox_cl ?>,
    
        <?php echo $f_c_hbox_op ?>, 
        <?php echo _outfield('ARVDI2') ?>,     
        <?php echo _outfield('ARGDI2') ?>,
    	<?php echo $f_c_hbox_cl ?>,
    
        <?php echo $f_c_hbox_op ?>, 
        <?php echo _outfield('ARVDI3') ?>,     
        <?php echo _outfield('ARGDI3') ?>, 
    	<?php echo $f_c_hbox_cl ?>,

        <?php echo $f_c_hbox_op ?>,     
        <?php echo _outfield('ARCLIP') ?>,     
        <?php echo $f_c_hbox_cl
        ?>,  
        <?php echo _outfield('ARTBRI') ?>,       
        <?php echo _outfield('ARCATS') ?>,
        <?php echo _outfield('ARCATP') ?>,
    ]
}),  

<!-- ----------------------------------------------------------------------------------  -->
<!--  Approvvigionamento                                                                 -->
<!-- ----------------------------------------------------------------------------------  -->
new Ext.create('Ext.form.Panel', {
    title: 'Approvvigionamento',
    gestione_std: 'Y',
    flex : 1,
    bodyPadding: 5,
    autoScroll: true,
    layout: 'anchor',
    defaults: { <?php echo $p_defaults ?> },
    items: [
       <?php if($m_params->g_multi != 'Y') echo _intestazione() ?>,
        

        <?php echo $f_c_hbox_op ?>,
		<?php echo _outfield('ARFOR1') ?>,
		<?php echo $f_c_hbox_cl ?>,    
        
    	<?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARLTFO', $multi) ?>,
        <?php echo _outfield('ARBSCA', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,
    	
    	<?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARPZCO', $multi) ?>,
        <?php echo _outfield('ARCKIN') ?>,
    	<?php echo $f_c_hbox_cl ?>,

		<?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARFLR3') ?>,
        <?php echo _outfield('ARCKOU') ?>,
    	<?php echo $f_c_hbox_cl ?>,
    	
        <?php echo _outfield('ARDEPO') ?>,

        <?php echo _outfield('AXTAB1') ?>,
        <?php echo _outfield('ARISO ') ?>,
        <?php echo _outfield('AXNAZO') ?>,
        <?php echo _outfield('ARTAB2') ?>,
        

        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARFOR2') ?>,
		<?php echo $f_c_hbox_cl ?>,    

        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARTER') ?>,
		<?php echo $f_c_hbox_cl ?>,    

        <?php echo _outfield('ARTAB4') ?>,
        <?php echo _outfield('ARTAB5') ?>,
        
        
         <?php echo $f_c_hbox_op ?>,
		 <?php echo _outfield('space') ?>,
		 <?php echo _outfield('lun') ?>,
		 <?php echo _outfield('mar') ?>,
         <?php echo _outfield('mer') ?>,
         <?php echo _outfield('gio') ?>,
         <?php echo _outfield('ven') ?>,
         <?php echo _outfield('sab') ?>,
		 <?php echo $f_c_hbox_cl ?>,
         
         <?php echo $f_c_hbox_op ?>,
         <?php echo _outfield('gg1') ?>,
         <?php echo _outfield('AXGGI1') ?>,
         <?php echo _outfield('AXGGI2') ?>,
         <?php echo _outfield('AXGGI3') ?>,
         <?php echo _outfield('AXGGI4') ?>,
         <?php echo _outfield('AXGGI5') ?>,
         <?php echo _outfield('AXGGI6') ?>,
		 <?php echo $f_c_hbox_cl ?>,   
		 
		
		 
		 <?php echo $f_c_hbox_op ?>,
		  <?php echo _outfield('gg2') ?>,
		 <?php echo _outfield('AXGGR1') ?>,
         <?php echo _outfield('AXGGR2') ?>,
         <?php echo _outfield('AXGGR3') ?>,
         <?php echo _outfield('AXGGR4') ?>,
         <?php echo _outfield('AXGGR5') ?>,
         <?php echo _outfield('AXGGR6') ?>,
		 <?php echo $f_c_hbox_cl ?>, 
		 
		
		 <?php echo $f_c_hbox_op ?>,
		  <?php echo _outfield('gg3') ?>,
         <?php echo _outfield('AXGGC1') ?>,
         <?php echo _outfield('AXGGC2') ?>,
         <?php echo _outfield('AXGGC3') ?>,
         <?php echo _outfield('AXGGC4') ?>,
         <?php echo _outfield('AXGGC5') ?>,
         <?php echo _outfield('AXGGC6') ?>,
		 <?php echo $f_c_hbox_cl ?>,
		 
		  <?php echo $f_c_hbox_op ?>,
		  <?php echo _outfield('gg4') ?>,
         <?php echo _outfield('AXGCR1') ?>,
         <?php echo _outfield('AXGCR2') ?>,
         <?php echo _outfield('AXGCR3') ?>,
         <?php echo _outfield('AXGCR4') ?>,
         <?php echo _outfield('AXGCR5') ?>,
         <?php echo _outfield('AXGCR6') ?>,
		 <?php echo $f_c_hbox_cl ?>,  
        

    ]
}),

<!-- ----------------------------------------------------------------------------------  -->
<!--  Programmazione                                                                     -->
<!-- ----------------------------------------------------------------------------------  -->
new Ext.create('Ext.form.Panel', {
    title: 'Programmazione',
    gestione_std: 'Y',
    flex : 1,
    bodyPadding: 5,
    autoScroll: true,
    layout: 'anchor',
    defaults: { <?php echo $p_defaults ?> },
    items: [
      	<?php if($m_params->g_multi != 'Y') echo _intestazione() ?>,
      	
        <?php echo _outfield('ARESAU') ?>,
        
        <?php echo $f_c_hbox_op ?>,            
        <?php echo _outfield('ARLTLO', $multi) ?>,
        <?php echo _outfield('ARLTPR', $multi) ?>, 
        <?php echo $f_c_hbox_cl ?>,

        
    	<?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARTPAP', $multi) ?>,
        <?php echo _outfield('ARDBFB', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,
    	
    	<?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('AXTAPO', $multi) ?>,
        <?php echo _outfield('ARDBCS', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,

    	<?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('AXTADP', $multi) ?>,
        <?php echo _outfield('ARDBPL', $multi) ?>,
    	<?php echo $f_c_hbox_cl ?>,    	
    	
    	<?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('AREOAQ', $multi) ?>,
        <?php echo _outfield('ARDBCO', $multi) ?>,
        <?php echo $f_c_hbox_cl ?>,
        
        <?php echo $f_c_hbox_op ?>,
        <?php echo _outfield('ARTAB1') ?>,
        <?php echo _outfield('ARBGRM', $multi) ?>,
         <?php echo $f_c_hbox_cl ?>,

        <?php echo $f_c_hbox_op ?>,
		<?php echo _outfield('ARSWTR', $multi) ?>,
        <?php echo _outfield('ARFLR5', $multi) ?>,
        <?php echo $f_c_hbox_cl ?>,
        
        <?php echo _outfield('ARTAB3') ?>,

        <?php echo $f_c_hbox_op ?>,        
        <?php echo _outfield('ARSMIN', $multi) ?>,
        <?php echo _outfield('ARLOTT', $multi) ?>,
        <?php echo $f_c_hbox_cl ?>,       

        <?php echo $f_c_hbox_op ?>,       
        <?php echo _outfield('ARSMAX', $multi) ?>,
        <?php echo _outfield('ARRIOR', $multi) ?>,
        <?php echo $f_c_hbox_cl ?>,        
        
        <?php echo $f_c_hbox_op ?>,   
        <?php echo _outfield('ARPRE1', $multi) ?>,
        <?php echo _outfield('AXLMIR', $multi) ?>,
        <?php echo $f_c_hbox_cl ?>,   
        
        <?php echo $f_c_hbox_op ?>,   
        <?php echo _outfield('AROPE1', $multi) ?>,
        <?php echo _outfield('AXLMUR', $multi) ?>,
        <?php echo $f_c_hbox_cl ?>,   

    
        <?php echo _outfield('AROPE2') ?>,
   

        <?php echo $f_c_hbox_op ?>, 
        <?php echo _outfield('ARPRE2') ?>,    
        <?php echo _outfield('ARPRE3') ?>, 
        <?php echo $f_c_hbox_cl ?>,


// esempio di bottone        
//    		{ xtype: 'button'
//			, margin: '0 0 0 5'
//    	    , scale: 'small'			                 
//        	, iconCls: 'icon-search-16'
//        	, iconAlign: 'top'
//        	, width: 25			                
//	        , handler : function() {
//	            var m_form = this.up('form').getForm();
//	            var my_listeners = {
//				  afterSel: function(from_win, row) {
//				      console.log(m_form);
//	     		 	  m_form.findField('ARFOR1').setValue(row.CFCD);
//	     		 	  m_form.findField('ARFOR1_DE').setValue('['+row.CFCD+']'+' '+row.CFRGS1);
//	     			  from_win.close();
//				    }								  
//			    }
//				acs_show_win_std('Anagrafica clienti/fornitori'
//				               , 'acs_anag_ricerca_cli_for.php?fn=open_tab'
//				               , {cf:'F'}, 950, 500, my_listeners
//				               , 'icon-search-16');
//			  } 
		 
//			}, 

	]
}),