<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// EXE SAVE CONFIG
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save'){
    ini_set('max_execution_time', 3000);
   
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
 
    $ar_upd['MOVALO']   = $form_values->valore;
    $ar_upd['MOSEQS']   = $form_values->seq;

    $sql = "UPDATE {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(MO) = '{$form_values->rrn}' ";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
// ******************************************************************************************
// EXE AGGIUNGI LISTINO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi'){
    ini_set('max_execution_time', 3000);
    
    $form_values = $m_params->form_values;
    $ret = array();
    $ar_ins = array();
    
    $sql_c = "SELECT COUNT(*) AS C_ROW
              FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
              WHERE MODT = '{$id_ditta_default}'
              AND MOMODE = '{$m_params->params->mode}'
              AND MOSEQU = '{$m_params->params->seq}'
              AND MOVARI = '{$m_params->params->var}'
              AND MOVALO = '{$m_params->form_values->valore}'";
    
        $stmt_c = db2_prepare($conn, $sql_c);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_c);
        $row = db2_fetch_assoc($stmt_c);
        
        if($row['C_ROW'] > 0){
            $ret['success'] = false;
            $ret['msg_error'] = 'Valore ammesso gi&agrave; esistente';
            echo acs_je($ret);
            return;
            
        }else{
            
            $ar_ins['MODT'] 	= $id_ditta_default;
            $ar_ins['MOMODE'] 	= $m_params->params->mode;
            $ar_ins['MOSEQU'] 	= $m_params->params->seq;
            $ar_ins['MOSEQS'] 	= $m_params->form_values->seq;
            $ar_ins['MOVALO'] 	= $m_params->form_values->valore;
            $ar_ins['MOVARI'] 	= $m_params->params->var;
            
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_valori_ammessi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            $error_msg =  db2_stmt_errormsg($stmt);
        
        
       }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
// ******************************************************************************************
// EXE DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
            WHERE RRN(MO) = '{$form_values->rrn}' ";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $error_msg =  db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = $result;
    if (strlen($error_msg) > 0) {
        $ret['success'] = false;
        $ret['message'] = $error_msg;
    }
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid_val'){
// ******************************************************************************************    
    $m_params = acs_m_params_json_decode();
    
    $mode = $m_params->open_request->mode;
    $seq = $m_params->open_request->seq;
    $var = $m_params->open_request->var;
    
    $sql = "SELECT RRN(MO) AS RRN, MO.*, TA.TAREST , TA.TADESC AS DESC
            FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA
             ON TA.TADT = MO.MODT AND TA.TAID = 'PUVN' AND TA.TANR = MO.MOVALO AND TA.TACOR2 = MO.MOVARI AND TA.TALINV = ''
            WHERE MODT = '{$id_ditta_default}' AND MOMODE = '{$mode}' AND MOSEQU='{$seq}' AND MOVARI = '{$var}'
            ORDER BY MOSEQS, MOVALO";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        
        $nr['seq']    = trim($row['MOSEQS']);
        $nr['rrn']    = $row['RRN'];
        $nr['valore']    = trim($row['MOVALO']);
        $nr['cod']    = trim($row['MOVALO']);
        $nr['cod_var']    = trim($row['MOVARI']);
        $val_risp = find_TA_sys('PUVN', trim($row['MOVALO']), null, trim($row['MOVARI']), null, null, 0, '', 'N', 'Y');
        if($val_risp[0]['sosp'] == true){
            $nr['sosp'] = 'S';
            $text = str_replace('(*S*)', '', $val_risp[0]['text']); 
        }else{
            $text = $val_risp[0]['text'];
        }
        
        $val = "[".trim($row['MOVALO'])."] ".$text;
        $nr['val']    = $val;
        
        
        $nr['desc'] = $row['DESC'];
        $nr['red']  = substr($row['TAREST'], 84, 3);
        $nr['green']= substr($row['TAREST'], 87, 3);
        $nr['blue'] = substr($row['TAREST'], 90, 3);
        $nr['tag']  = substr($row['TAREST'], 97, 5);
        $nr['esau']  = substr($row['TAREST'], 127, 1);
        //$nr['sosp'] = $row['SOSP'];
       
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}



if ($_REQUEST['fn'] == 'open_val'){
    ?>


{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				{
						xtype: 'grid',
						title: '',
						multiSelect : true,
						flex:0.5,
				        loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_val', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['esau', 'cod', 'cod_var', 'desc', 'seq', 'val', 'valore', 'rrn', 'sosp', 'red', 'green', 'blue', 'rgb', 'tag']							
									
			}, //store
			
			<?php  $divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
			
			      columns: [	
			      {
	                text: '<?php echo $divieto; ?>',
	                dataIndex: 'sosp',
	                width : 30,
	                tooltip : 'Sospeso',
	                renderer: function(value, metaData, record){
		    			  if(record.get('sosp') == 'S') return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=15>';
		    			 }
	                }, 
	                
	                {
	                header   : 'Valore ammesso',
	                dataIndex: 'val',
	                filter: {type: 'string'}, filterable: true,
	                flex: 1,
	                renderer: function(value, metaData, record){
	                  if (record.get('esau') == 'E')
        				     metaData.tdCls += ' sfondo_giallo';
		    			  
		    			  return value;
		    			 }
	                }, 
	                 {
	                header   : 'RGB',
	                dataIndex: 'rgb',
	                filter: {type: 'string'}, filterable: true,
	                width : 35,
	                renderer: function(value, metaData, record){
   							var red = record.get('red');
                            var green = record.get('green');
                            var blue = record.get('blue');
                            metaData.style += 'background-color: rgb(' + red + ', '+ green + ', ' + blue + ');';
		    		 	  
		    			  	return '';
		    		  }
	                },
	                 {
	                header   : 'Tag',
	                dataIndex: 'tag',
	                filter: {type: 'string'}, filterable: true,
	                 width : 50
	                },
	                {
	                header   : 'Sequenza',
	                dataIndex: 'seq',
	                filter: {type: 'string'}, filterable: true,
	                 width : 70
	                }
	                
	         ], listeners: {
	         
	         	selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('panel').down('#dx_form');
		               //pulisco eventuali filtri
		               form_dx.getForm().reset();
		               form_dx.getForm().setValues(selected[0].data);	
	                   }
		          },
		          
		         afterrender: function(comp) {
			        var win = comp.up('window');
			        win.tools.close.hide();
			        var myCloseTool = Ext.create('Ext.panel.Tool', {
			            type: 'close',
			            handler: function() {
			               win.fireEvent('afterClose', win)
			            }
			        })
			        win.header.insert(3, myCloseTool); // number depends on other items in header
			    }, itemcontextmenu : function(grid, rec, node, index, event) {
				  			event.stopEvent();
				  													  
						 	var voci_menu = [];
					     	row = rec.data;
					     
					     	id_selected = grid.getSelectionModel().getSelection();
			     		 	list_selected_id = [];
			     		 	for (var i=0; i<id_selected.length; i++){
			     				list_selected_id.push(id_selected[i].data);
						 	}
						 	
						 	 voci_menu.push({
	         				 	text: 'Riepilogo codici articolo',
	        				 	iconCls : 'icon-print-16',             		
	        				 	handler: function () {
	        				 		console.log(rec);
	        				 		acs_show_win_std('Riepilogo codici articolo', 'acs_report_riepilogo_art.php?fn=open_form', {
	        				 			cod_variabile: rec.get('cod_var'),
	        				 			list_selected_id : list_selected_id
	        				 		 }, 400, 380, {}, 'icon-print-16');   	     
			                	}
		    				 });
						 
				      		var menu = new Ext.menu.Menu({
				            items: voci_menu
							}).showAt(event.xy);	
			    	
			    		} 
	         
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					        
					        if(record.get('sosp') == 'S'){
            		           return ' colora_riga_rosso';
                    		 }
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
		
	
		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettagli valore ammesso',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.5,
 		            frame: true,
 		            items: [
 		           {
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					hidden : true						
				   }, {
        			xtype : 'combo',
        		 	name: 'valore', 
        		 	labelWidth : 120,
		 			fieldLabel: 'Variante ammessa',
    		 		forceSelection: true,  							
					displayField: 'text',
		    		valueField: 'id',	
		    		anchor: '-15',						
        			emptyText: '- seleziona -',
        		   	queryMode: 'local',
             		minChars: 1,	
        			store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}, {name:'color'}, {name:'sosp'}, {name : 'esau'}],
            		    data: [								    
    		     	<?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $m_params->var, null, null, 0, '', 'Y', 'Y'), ''); ?>	
            		    ]
        			}, tpl: [
    				 '<ul class="x-list-plain">',
                        '<tpl for=".">',
                        '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                        '</tpl>',
                        '</ul>'
    				 ],
                       // template for the content inside text field
                    displayTpl: Ext.create('Ext.XTemplate',
                           '<tpl for=".">',

	                            '{text}',

	                        '</tpl>'
                     
                    ),listeners: { 
        			 		beforequery: function (record) {
        	         		record.query = new RegExp(record.query, 'i');
        	         		record.forceAll = true;
    				 },
    				  afterrender : function(comp){
    				     comp.store.each(function(item){
    				          var esau = item.get('esau');
    				          console.log(esau);
    				          if(esau == 'E')
				                  item.set('color', '#F4E287');
                                  
                            });
    				  }
         			},	
        		 	
        		  }, 
        		  
        		  	 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							{
            					name: 'seq',
            					labelWidth : 120,
            					fieldLabel : 'Sequenza successiva',
            					xtype: 'textfield',
            					anchor: '-15',
            					maxLength : 4,
            					width : 170
            				  }
						
						   
						]}
        		  
        		  
				  		 
			 		           ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
	       			  
	       			  var righe = grid.getSelectionModel().getSelection();
	       			  if (righe.length > 1){
							  acs_show_msg_error('Selezionare solo una riga alla volta per cancellare');
							  return false;
						  }
	       			  
	       			  
 			          Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	      if (btn == 'yes'){
     			           Ext.Ajax.request({
    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
    				        timeout: 2400000,
    				        method     : 'POST',
    	        			jsonData: {
    	        				form_values : form_values
    						},							        
    				        success : function(result, request){
    					        var jsonData = Ext.decode(result.responseText);
    					        if (jsonData.success == true){
    					        	var gridrecord = grid.getSelectionModel().getSelection();
    					        	grid.store.remove(gridrecord);	
    					        	form.getForm().findField('valore').setValue('');
    					        	form.getForm().findField('seq').setValue('');						        
    					        }
    				        },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    }); 
				    
				     }
            	  });
				    		     
		           
			
			            }

			     },'->',
                 
                  
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				params : <?php echo acs_je($m_params); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					      
					      if(jsonData.success == false){
					         acs_show_msg_error(jsonData.msg_error);
				        	 form.getForm().findField('valore').setValue('');
    					     form.getForm().findField('seq').setValue('');	
				        	 return;
	        			  }else{
	        			  	 grid.store.load();
	        			  }
					      
					         
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
			        				
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     }
			     ]
		   }]
			 	 }
						   
		
					 ]
					 
					
					
	}
	
]}

<?php

exit;
}


if ($_REQUEST['fn'] == 'get_json_data_riepilogo'){
    
    $m_params = acs_m_params_json_decode();
        
    $sql = "SELECT COUNT(*) AS TOT, MOVALO, TA.TADESC AS DESC, TA.TAREST AS TAREST, TA.TATP AS SOSP
            FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            ON TA.TADT = MO.MODT AND TA.TAID = 'PUVN' AND TA.TANR = MO.MOVALO AND TA.TACOR2 = '{$m_params->open_request->var}' AND TALINV = ''
            WHERE MODT = '{$id_ditta_default}' AND MOMODE = '{$m_params->open_request->mode}' AND MOVARI = '{$m_params->open_request->var}'
            GROUP BY MOVALO, TA.TAREST, TA.TADESC, TA.TATP ORDER BY TA.TADESC";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
        
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = array();
        
        $nr['tot']  = $row['TOT'];
        $nr['cod']  = $row['MOVALO'];
        $nr['desc'] = $row['DESC'];
        $nr['red']  = substr($row['TAREST'], 84, 3);
        $nr['green']= substr($row['TAREST'], 87, 3);
        $nr['blue'] = substr($row['TAREST'], 90, 3);
        $nr['tag']  = substr($row['TAREST'], 97, 5);
        $nr['sosp'] = $row['SOSP'];
        $nr['c_val'] = $row['C_VAL'];
        
        $ar[] = $nr;
    }
    
    
    echo acs_je($ar);
    exit;
    
}

if ($_REQUEST['fn'] == 'open_riepilogo'){
  
    
    ?>


{"success":true, "items": [

				
				{
						xtype: 'grid',
						title: 'Variabile [<?php echo $m_params->var."] ".$m_params->d_var; ?>',
						multiSelect : true,
						loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_riepilogo', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['tot', 'cod', 'desc', 'red', 'green', 'blue', 'tag', 'sosp', 'rgb']							
									
			}, //store
			
			<?php  $divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
			
			      columns: [	
			      	{
	                text: '<?php echo $divieto; ?>',
	                dataIndex: 'sosp',
	                width : 30,
	                tooltip : 'Sospeso',
	                renderer: function(value, metaData, record){
		    			  if(record.get('sosp') == 'S') return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=15>';
		    			 }
	                }, {
	                header   : 'Cod.',
	                dataIndex: 'cod',
	                filter: {type: 'string'}, filterable: true,
	                width : 50
	                }, {
	                header   : 'RGB',
	                dataIndex: 'rgb',
	                filter: {type: 'string'}, filterable: true,
	                width : 35,
	                renderer: function(value, metaData, record){
   							var red = record.get('red');
                            var green = record.get('green');
                            var blue = record.get('blue');
                            metaData.style += 'background-color: rgb(' + red + ', '+ green + ', ' + blue + ');';
		    		 	  
		    			  	return '';
		    		  }
	                }, {
	                header   : 'Descrizione variante',
	                dataIndex: 'desc',
	                filter: {type: 'string'}, filterable: true,
	                flex: 1
	                }, {
	                header   : 'Tag',
	                dataIndex: 'tag',
	                filter: {type: 'string'}, filterable: true,
	                width : 50
	                }, {
	                header   : 'Nr ricorrenze',
	                dataIndex: 'tot',
	                filter: {type: 'string'}, filterable: true,
	                width : 80
	                }
	                
	         ], listeners: {
	         
    	         		'afterrender': function(comp){
    	         		   var win = comp.up('window');
							comp.store.on('load', function(store, records, options) {
							    
							   	var list_value = store.proxy.reader.jsonData;
								var tot_ric = 0;
								var tot_var = 0;
								var ultimo_cod = '';
								for (var i=0; i<list_value.length; i++){
								        tot_ric += list_value[i].tot;
								        if(list_value[i].cod != ultimo_cod){
								          tot_var++;
								        }
								      
								      ultimo_cod = list_value[i].cod;  
								  } 
														
								  win.setTitle(win.title + ' [' + tot_var +', '+ tot_ric +']');
								  
								  
							}, comp);
						},
						
						itemcontextmenu : function(grid, rec, node, index, event) {
				  			event.stopEvent();
				  													  
						 	var voci_menu = [];
					     	row = rec.data;
					     
					     	id_selected = grid.getSelectionModel().getSelection();
			     		 	list_selected_id = [];
			     		 	for (var i=0; i<id_selected.length; i++){
			     				list_selected_id.push(id_selected[i].data);
						 	}
						 	
						 	 voci_menu.push({
	         				 	text: 'Riepilogo codici articolo',
	        				 	iconCls : 'icon-print-16',             		
	        				 	handler: function () {
	        				 		acs_show_win_std('Riepilogo codici articolo', 'acs_report_riepilogo_art.php?fn=open_form', {
	        				 			cod_variabile: rec.get('TACOR2'),
	        				 			list_selected_id : list_selected_id
	        				 		 }, 400, 380, {}, 'icon-print-16');   	     
			                	}
		    				 });
						 
				      		var menu = new Ext.menu.Menu({
				            items: voci_menu
							}).showAt(event.xy);	
			    	
			    		} 
	         
	         
	         }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					        
					        if(record.get('sosp') == 'S'){
            		           return ' colora_riga_rosso';
                    		 }
					           
					           return '';																
					         }   
					    }
					       
		
		
					 
					
					
	}
	
]}

<?php

exit;
}
