<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'PUSQ',
    'title_grid' => 'PUSQ-Sequenze configuratore',
    'fields' => array(
        'TAID'   => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'TACOR2'   => array('label'	=> 'tacor2', 'hidden' => 'true'),
        'SEQUENZE' => array('label'	=> 'Sequenze', 'only_view' => 'C'),
        'seq1'    => array('label'	=> 'Sequenza 1', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq2'   => array('label'	=> 'Sequenza 2', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq3'   => array('label'	=> 'Sequenza 3', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq4'   => array('label'	=> 'Sequenza 4', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq5'   => array('label'	=> 'Sequenza 5', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq6'   => array('label'	=> 'Sequenza 6', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq7'   => array('label'	=> 'Sequenza 7', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq8'   => array('label'	=> 'Sequenza 8', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq9'   => array('label'	=> 'Sequenza 9', 'only_view' => 'F', 'width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'seq10'   => array('label'	=> 'Sequenza 10', 'only_view' => 'F','width' => 160, 'maxLength' => 3, 'maskRe' => '/[0-9]/'),
        'RRN'    => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella sequenze',
    'tab_tipo' => 'PUSQ',
    'TAREST' => array(
        'seq1' 	=> array(
            "start" => 0,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq2' 	=> array(
            "start" => 4,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq3' 	=> array(
            "start" => 8,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq4' 	=> array(
            "start" => 12,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq5' 	=> array(
            "start" => 16,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq6' 	=> array(
            "start" => 20,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq7' 	=> array(
            "start" => 24,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq8' 	=> array(
            "start" => 28,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq9' 	=> array(
            "start" => 32,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'seq10' 	=> array(
            "start" => 36,
            "len"   => 3,
            "riempi_con" => ""
        )
        
    )
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

