<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];  //descrizione
    $nr['note'] = $row['TADES2'];  //marca
    //$nr['ordine'] = $row['TAORDI'];
    $nr['tarest'] = $row['TAREST'];
    $nr['certif'] = substr($row['TAREST'], 0, 1);
    $row_c = get_TA_sys('BCEU',  $nr['certif']);
    $nr['d_cert'] = "[{$nr['certif']}] {$row_c['text']}";
    
    $giorno_i = substr($row['TAREST'], 1, 2);
    $mese_i = substr($row['TAREST'], 3, 2);
    $anno_i = substr($row['TAREST'], 5, 4);
    $nr['data_iniz'] = $anno_i.$mese_i.$giorno_i;
    //$nr['data_iniz'] = substr($row['TAREST'], 1, 8);
    
    $giorno_f = substr($row['TAREST'], 9, 2);
    $mese_f = substr($row['TAREST'], 11, 2);
    $anno_f = substr($row['TAREST'], 13, 4);
    $nr['data_fin'] = $anno_f.$mese_f.$giorno_f;
    //$nr['data_fin'] = substr($row['TAREST'], 9, 8);
    $nr['rifer'] = substr($row['TAREST'], 17, 30);
    
    $nr['doga1'] = substr($row['TAREST'], 50, 3);
    if(trim($nr['doga1']) != ''){
        $row_d = get_TA_sys('BGDN',  $nr['doga1']);
        $nr['d_doga'] .= "[{$nr['doga1']}] {$row_d['text']}";
    }
    $nr['doga2'] = substr($row['TAREST'], 53, 3);
    if(trim($nr['doga2']) != ''){
        $row_d = get_TA_sys('BGDN',  $nr['doga2']);
        $nr['d_doga'] .= "<br>[{$nr['doga2']}] {$row_d['text']}";
    }
    $nr['doga3'] = substr($row['TAREST'], 56, 3);
    if(trim($nr['doga3']) != ''){
        $row_d = get_TA_sys('BGDN',  $nr['doga3']);
        $nr['d_doga'] .= "<br>[{$nr['doga3']}] {$row_d['text']}";
    }
    $nr['doga4'] = substr($row['TAREST'], 59, 3);
    if(trim($nr['doga4']) != ''){
        $row_d = get_TA_sys('BGDN',  $nr['doga4']);
        $nr['d_doga'] .= "<br>[{$nr['doga4']}] {$row_d['text']}";
    }
    $nr['doga5'] = substr($row['TAREST'], 62, 3);
    if(trim($nr['doga5']) != ''){
        $row_d = get_TA_sys('BGDN',  $nr['doga5']);
        $nr['d_doga'] .= "<br>[{$nr['doga5']}] {$row_d['text']}";
    }
    
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'note', 'tarest', 'certif', 'd_cert', 'd_area', 
                       'data_iniz' , 'data_fin', 'rifer', 'area', 'doga1', 'doga2', 'doga3', 'doga4', 'doga5',
                       'd_doga'
                        
    );
    
    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start();
    
    ?>
    
			        {
	                header   : 'Descrizione',
	                dataIndex: 'tadesc',
	                width : 300
	                },
	                
	                {
	                header   : 'Note',
	                dataIndex: 'note',
	                width : 200
	                },    {
	                header   : 'Certificazione',
	                dataIndex: 'd_cert',
	                width : 150
	                },
	                {
	                header   : 'Validit&agrave; iniziale',
	                dataIndex: 'data_iniz',
	                width : 100,
	                renderer : date_from_AS
	                },
	                {
	                header   : 'Validit&agrave; finale',
	                dataIndex: 'data_fin',
	                width : 100,
	                renderer : date_from_AS
	                },{
	                header   : 'Riferimento',
	                dataIndex: 'rifer',
	                flex : 1
	                },{
	                header   : 'Gruppo doganale',
	                dataIndex: 'd_doga',
	                flex : 1
	                }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "note" => trim($row->note),
        "certif" => trim($row->certif),
        "data_iniz" => trim($row->data_iniz),
        "data_fin" => trim($row->data_fin),
        "rifer" => trim($row->rifer),
        "doga1" => trim($row->doga1),
        "doga2" => trim($row->doga2),
        "doga3" => trim($row->doga3),
        "doga4" => trim($row->doga4),
        "doga5" => trim($row->doga5),
        "tarest" => trim($row->tarest),
       
        
    );
 
    return $ar_values;
    
}


function get_values_from_array($row){
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "note" => trim($row['note']),
        "certif" => trim($row['certif']),
        "data_iniz" => trim($row['data_iniz']),
        "data_fin" => trim($row['data_fin']),
        "rifer" => trim($row['rifer']),
        "doga1" => trim($row['doga1']),
        "doga2" => trim($row['doga2']),
        "doga3" => trim($row['doga3']),
        "doga4" => trim($row['doga4']),
        "doga5" => trim($row['doga5']),
        "tarest" => trim($row['tarest']),
        
        
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    
    global $main_module;
    
    ob_start();
    
    ?>
      {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		anchor: 'none',
		maxLength: 3,	
		width : 150,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
        {xtype: 'textfield',
        name: 'desc',
        fieldLabel: 'Descrizione',
        maxLength: 30,
        anchor: '-15',
        value:  <?php echo j($ar_values['descrizione']); ?>
        },
        {xtype: 'textfield',
        name: 'note',
        fieldLabel: 'Note',
        maxLength: 30,
        anchor: '-15',	
        value:  <?php echo j($ar_values['note']); ?>
        },
		   {name: 'certif',
			xtype: 'combo',
			fieldLabel: 'Certificazione',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['certif']); ?>,							
			emptyText: '- seleziona -',
	   		//allowBlank: false,								
		    anchor: '-15',
		    queryMode: 'local',
            minChars: 1,
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BCEU'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	 },
		{ 
			xtype: 'fieldcontainer',
			flex:1,
			anchor: '-15',
			layout: { 	type: 'hbox',
					    pack: 'start',
					    align: 'stretch'},						
			items: [
			
			{
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Validit&agrave; iniziale'
	   , name: 'data_iniz'
	   , format: 'd/m/Y'
	   , value: '<?php echo print_date($ar_values['data_iniz'], "%d/%m/%Y"); ?>'							   
	   , submitFormat: 'dmY'
	   , allowBlank: true
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
		}
		, anchor: '-15'
	   , flex :1.3
	   
	}, {
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Finale'
	   , labelAlign : 'right'
	   , labelWidth : 45
	   , name: 'data_fin'
	   , format: 'd/m/Y'
	   , value: '<?php echo print_date($ar_values['data_fin'], "%d/%m/%Y"); ?>'							   
	   , submitFormat: 'dmY'
	   , allowBlank: true
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
		}
	   , anchor: '-15'
	   , flex :1
	}
			
			]}
	, 
	{xtype: 'textfield',
	name: 'rifer',
	fieldLabel: 'Riferimento',
	maxLength: 30,
	anchor: '-15',	
	value:  <?php echo j($ar_values['rifer']); ?>
	},{name: 'doga1',
	xtype: 'combo',
	fieldLabel: 'Gruppo dogana 1',
	forceSelection: true,								
	displayField: 'text',
	valueField: 'id',
	value:  <?php echo j($ar_values['doga1']); ?>,							
	emptyText: '- seleziona -',
    queryMode: 'local',
    minChars: 1,
    anchor: '-15',
	store: {
		editable: false,
		autoDestroy: true,
	    fields: [{name:'id'}, {name:'text'}],
	    data: [								    
	     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BGDN'), ''); ?>	
	    ]
	},listeners: { 
	 		beforequery: function (record) {
     		record.query = new RegExp(record.query, 'i');
     		record.forceAll = true;
	 }
	}
	
},{name: 'doga2',
	xtype: 'combo',
	fieldLabel: 'Gruppo dogana 2',
	forceSelection: true,								
	displayField: 'text',
	valueField: 'id',
	value:  <?php echo j($ar_values['doga2']); ?>,							
	emptyText: '- seleziona -',
    queryMode: 'local',
    minChars: 1,
     anchor: '-15',
	store: {
		editable: false,
		autoDestroy: true,
	    fields: [{name:'id'}, {name:'text'}],
	    data: [								    
	     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BGDN'), ''); ?>	
	    ]
	},listeners: { 
	 		beforequery: function (record) {
     		record.query = new RegExp(record.query, 'i');
     		record.forceAll = true;
	 }
	}
	
},{name: 'doga3',
	xtype: 'combo',
	fieldLabel: 'Gruppo dogana 3',
	forceSelection: true,								
	displayField: 'text',
	valueField: 'id',
	value:  <?php echo j($ar_values['doga3']); ?>,							
	emptyText: '- seleziona -',
    queryMode: 'local',
    minChars: 1,
     anchor: '-15',
	store: {
		editable: false,
		autoDestroy: true,
	    fields: [{name:'id'}, {name:'text'}],
	    data: [								    
	     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BGDN'), ''); ?>	
	    ]
	},listeners: { 
	 		beforequery: function (record) {
     		record.query = new RegExp(record.query, 'i');
     		record.forceAll = true;
	 }
	}
	
},{name: 'doga4',
	xtype: 'combo',
	fieldLabel: 'Gruppo dogana 4',
	forceSelection: true,								
	displayField: 'text',
	valueField: 'id',
	value:  <?php echo j($ar_values['doga4']); ?>,							
	emptyText: '- seleziona -',
    queryMode: 'local',
    minChars: 1,
     anchor: '-15',
	store: {
		editable: false,
		autoDestroy: true,
	    fields: [{name:'id'}, {name:'text'}],
	    data: [								    
	     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BGDN'), ''); ?>	
	    ]
	},listeners: { 
	 		beforequery: function (record) {
     		record.query = new RegExp(record.query, 'i');
     		record.forceAll = true;
	 }
	}
	
}, {name: 'doga5',
	xtype: 'combo',
	fieldLabel: 'Gruppo dogana 5',
	forceSelection: true,								
	displayField: 'text',
	valueField: 'id',
	value:  <?php echo j($ar_values['doga5']); ?>,							
	emptyText: '- seleziona -',
    queryMode: 'local',
    minChars: 1,
     anchor: '-15',
	store: {
		editable: false,
		autoDestroy: true,
	    fields: [{name:'id'}, {name:'text'}],
	    data: [								    
	     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BGDN'), ''); ?>	
	    ]
	},listeners: { 
	 		beforequery: function (record) {
     		record.query = new RegExp(record.query, 'i');
     		record.forceAll = true;
	 }
	}
	
}
	  
	
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
  
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-1s", $form_values->certif);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_iniz);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_fin);
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->rifer);
    
    $ar_ins['TAREST'] .= sprintf("%-3s", ''); //$form_values->f_area, al momento non gestita
    
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->doga1);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->doga2);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->doga3);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->doga4);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->doga5);
    
    
    
    $ar_ins['TANR']   .= sprintf("%-3s", $form_values->riga);
   // $ar_ins['TAORDI'] .= $form_values->f_ordine;
    if(strlen(trim($form_values->desc)) > 0)
        $ar_ins['TADESC'] .= $form_values->desc;
    else
        $ar_ins['TADESC'] .= $form_values->rifer;
    
    $ar_ins['TADES2'] .= $form_values->note;
    
    return $ar_ins;
    
}

if ($_REQUEST['fn'] == 'exe_aggiorna'){
    
    if($m_params->per_forn == 'Y')
        $messaggio = 'AGG_BCER_FORN';
    else
        $messaggio = 'AGG_BCER';
    
    $sh = new SpedHistory($deskArt);
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> $messaggio,
            "vals" => array(
                "RICDOLD" 	=> $m_params->tipo_scheda,
                "RIMOCO" 	=> $m_params->rec->riga,
                "RIFOR2" 	=> sprintf("%09s", $m_params->form_values->f_fornitore),
                "RIFG01" 	=> $m_params->form_values->f_sosp,
                "RIFG02" 	=> $m_params->form_values->f_agg_sche,
                "RIFG03" 	=> $m_params->form_values->f_ins_sche,
                "RIDT"  	=> $id_ditta_default
            ),
        )
        );
    
    
    //RIFOR1 RITORNO NUMERO ARTICOLI
    $ret = array();
    $ret['success'] = true;
    $ret['msg_articoli'] = "AGGIORNATI {$ret_RI['RIFOR1']} ARTICOLI";
    echo acs_je($ret);
    exit;
}

if($_REQUEST['fn'] == 'form_aggiorna'){ 
    ?>
    
     {"success":true, "items": [
        {
           
			xtype: 'form',
            border: false,
            bodyStyle: {
                padding: '10px',
                align: 'stretch'
            },
            items: [
			  { xtype: 'combo',
				name: 'f_fornitore',
				fieldLabel: 'Fornitore',
				anchor: '-15',
				labelWidth : 80,
				minChars: 2,	
				<?php if($m_params->per_forn == 'Y'){?>
				allowBlank : false,
				<?php }?>		
    			store: {
	            	pageSize: 1000,            	
					proxy: {
			            type: 'ajax',
			            url : '../base/acs_get_anagrafica.php?fn=get_data&cod=F',
			            reader: {
			                type: 'json',
			                root: 'root',
			                totalProperty: 'totalCount'
			            }
			        },       
						fields: [{name:'id'}, {name:'text'}, {name:'loca'}, {name:'color'}],            	
	            },
				valueField: 'id',                        
	            displayField: 'text',
	            typeAhead: false,
	            hideTrigger: true,
	            triggerAction: 'all',
	            listConfig: {
	                loadingText: 'Searching...',
	                emptyText: 'Nessun fornitore trovato',
        
	                // Custom rendering template for each item
	                getInnerTpl: function() {
	                    return '<div class="search-item" style="background-color:{color}">' +
	                        '<h3><span>{text}</span></h3>' +
	                        '[{id}] {loca}' + 
	                    '</div>';
	                	}                
                
            	},
            	pageSize: 1000,
                 listeners: {
                    beforequery: function(qeb){
                   
                       qeb.combo.getStore().removeAll();
						
					}
                }
				}, 
				{
                    fieldLabel: 'Elabora articoli sospesi',
                    name: 'f_sosp',	
                    labelWidth: 150,
                   // margin : '0 10 0 0',
                    xtype: 'checkboxfield',
                    inputValue: 'Y',             
                }	
                
                <?php if($m_params->per_forn == 'Y'){?>
               , {
                    fieldLabel: 'Aggiorna schede esistenti',
                    name: 'f_agg_sche',	
                    labelWidth: 150,
                    xtype: 'checkboxfield',
                    checked : true,
                    inputValue: 'Y',             
                },
                {
                    fieldLabel: 'Inserisci nuova scheda',
                    name: 'f_ins_sche',	
                    labelWidth: 150,
                    xtype: 'checkboxfield',
                    inputValue: 'Y',             
                }			
                <?php }?>
                	
				],
							
    					
    		buttons: [ {xtype: 'tbfill'}, 
    					        {
                                text: 'Aggiorna',
                                scale: 'medium',
                                handler: function () {
                               
                                    var m_win = this.up('window');
                                    var form = this.up('form').getForm();
                	            	var form_values = form.getValues();
                	            	if(form.isValid()){	
                	            		Ext.MessageBox.confirm('Richiesta conferma', 'Confermi l\'aggiornamento di tutti gli articoli in cui presente?', function(btn){
                			   			if(btn === 'yes'){
                			    		  Ext.Ajax.request({
            						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
            						        jsonData: {
            						        	tipo_scheda : <?php echo j($m_params->tipo_scheda); ?>,
            						        	rec         : <?php echo acs_je($m_params->rec); ?>,
            						        	form_values : form_values,
            						        	per_forn    : <?php echo j($m_params->per_forn); ?>
            						        },
            						        method     : 'POST',
            						        waitMsg    : 'Data loading',
            						        success : function(result, request){
            						          var jsonData = Ext.decode(result.responseText);
            						          m_win.fireEvent('afterOkSave', m_win, jsonData.msg_articoli)
            						                         
            						        },
            						        failure    : function(result, request){
            						            Ext.Msg.alert('Message', 'No data to be loaded');
            						        }
            						    });
                    			    }else{
                    			   }
                    			 });
                    			 }
                	            	
         
                               
                                }
                        	}]
          
			}
			]}
    
    
    <?php 
}