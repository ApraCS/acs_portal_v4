<?php
require_once "../../config.inc.php";
require_once "acs_gestione_tabelle.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_conferma_mod'){
    
    $list_rows = $m_params->list_rows_modified;
    $ar_control = array();
    $error_cod = array();
    
    foreach($list_rows as $v){
        
        $new_cod = $v->n_cod_var;
        $puvr = $v->cod_val;
        
              
        //CONTROLLO CODICE ARTICOLO ESISTENTE
        $sql = "SELECT COUNT(*) AS C_ROWS
                FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN' AND TANR = '{$new_cod}'
                AND TACOR2 = '{$puvr}' AND  TALINV = ''";       
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if($row['C_ROWS'] > 0){
            
            $error_cod['cod_var'] = $new_cod;
            $error_cod['cod_val'] = $puvr;
            $error_cod['msg'] = "Codice variante gi&agrave; esistente nella variabile {$puvr}";
            $error_cod['red'] = "Y";
            $ar_control[] = $error_cod;
            
        }
        
               
    }
    
    if($m_params->verifica == 'Y'){
        
        if(count($ar_control) > 0){
            echo acs_je($ar_control);
            return;
        }else{
            $ret = array();
            $ret['corretto'] = 'Y';
            echo acs_je($ret);
            
        }
    }else{
        
        
        if(count($ar_control) > 0){
            
            $ret = array();
            $ret['success'] = false;
            $ret['error_msg'] = "Codici varianti non corretti, verificare";
            echo acs_je($ret);
            return;
            
            
        }else{
            
            foreach($list_rows as $v){
                
                
                $sql = "SELECT *
                        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                        WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUVN' 
                        AND TANR = '{$v->cod_var}' AND TACOR2 = '{$v->cod_val}'";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt);
                $row = db2_fetch_assoc($stmt);
                
                $row['TAUSGE']   = $auth->get_user();
                $row['TADTGE']   = oggi_AS_date();
                $row['TAUSUM']   = $auth->get_user();
                $row['TADTUM']   = oggi_AS_date();
                $row['TANR']  	 =  $v->n_cod_var;
                $row['TADESC']   =  $v->n_desc_var;
                $row['TADES2']   =  $v->n_note;
                //variante origine
                $value_attuale = $v->tarest;
                if(trim($value_attuale) == "") $value_attuale = sprintf("%-248s", "");
                $chars = $v->cod_var;
                $len = "%-3s";
                $chars = substr(sprintf($len, $chars), 0, 3);
                $new_value = substr_replace($value_attuale, $chars, 138, 3);
                $tarest = $new_value;
                $row['TAREST'] 	= $tarest;
                                
                $sql_i = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($row) . ") VALUES (" . create_parameters_point_by_ar($row) . ")";
                $stmt = db2_prepare($conn, $sql_i);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $row);
                echo db2_stmt_errormsg($stmt);
                                
            }
            
            
            
            $ret = array();
            $ret['success'] = true;
            echo acs_je($ret);
        }
        
    }
    
    
    exit;
}


if ($_REQUEST['fn'] == 'open_form'){
?>

{"success":true, "items": [

     {
						xtype: 'grid',
						title: '',
				        loadMask: true,
				        autoScroll : true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
            			plugins: [
        		          Ext.create('Ext.grid.plugin.CellEditing', {
        		            clicksToEdit: 1,
        		          })
        		      	],
				        store: {
						xtype: 'store',
						autoLoad:true,
						data : [
					
				    	<?php foreach($m_params->list_selected_id as $v){?>
				 
		 			 
							{cod_val : '<?php echo $v->cod_val; ?>', 
							 desc_val : '<?php echo $v->desc_val; ?>', 
							 cod_var : '<?php echo $v->cod_var; ?>',
							 desc_var : '<?php echo $v->desc_var; ?>',
							 n_cod_var : '', n_desc_var: '', n_note : '',
							 tarest : '<?php echo $v->tarest; ?>'
							 },

			  			<?php } ?>
			  		
			  		   ],
							
		        			fields: ['cod_var', 'desc_var', 'cod_val', 'desc_val', 'n_cod_var', 'n_desc_var', 'red', 'n_note', 'tarest']							
									
			}, //store
			
			      columns: [	
			
	               {
	                header   : 'Cod.',
	                dataIndex: 'cod_val',
	                filter: {type: 'string'}, filterable: true,
	                width : 50
	                }, 
	                
	                {
	                header   : 'Descrizione variabile',
	                dataIndex: 'desc_val',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                }, {
	                header   : 'Cod.',
	                dataIndex: 'cod_var',
	                filter: {type: 'string'}, filterable: true,
	                width : 50
	                }, 
	                
	                {
	                header   : 'Descrizione variante',
	                dataIndex: 'desc_var',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                },
	                 {
	                header   : 'Nuovo cod.',
	                dataIndex: 'n_cod_var',
	                width : 70,
	                editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            	},
	            	renderer: function(value, p, record){
    	    	       if (record.get('n_cod_var').length > 3) 
    	    	            acs_show_msg_error('Il codice deve essere massimo 3 caratteri');
	 					 
    				    return value;
        		 	}
	                },
	                  {
	                header   : 'Nuova descrizione',
	                dataIndex: 'n_desc_var',
	                flex : 1,
                    editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            	},
	            	renderer: function(value, p, record){
    	    	       if (record.get('n_desc_var').length > 30) 
    	    	          acs_show_msg_error('La descrizione deve essere massimo 30 caratteri');
	 					
    				    return value;
        		 	}
	                },
	                 {
	                header   : 'Nuove note',
	                dataIndex: 'n_note',
	                flex : 1,
                    editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            	},
	            	renderer: function(value, p, record){
    	    	       if (record.get('n_note').length > 30) 
    	    	          acs_show_msg_error('Le note devono essere massimo 30 caratteri');
	 					
    				    return value;
        		 	}
	                }
	                
	         ], listeners: {}
			
					       
		
		 , dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
            
				
				 {
        			name: 'f_n_cod',
        			xtype: 'textfield',
        			fieldLabel: 'Nuovo codice',
        			itemId : 'w_cod',
        			labelWidth : 75,
        			width : 120,
        			maxLength : 4,
        			listeners: {
        				'change': function(field){
        					 field.setValue(field.getValue().toUpperCase());
          				}
                  	  }
    			    }, {
        			name: 'f_n_des',
        			xtype: 'textfield',
        			fieldLabel: 'Nuova descrizione',
        			itemId : 'w_desc',
        			maxLength : 30,
        			width : 250
        									
        		 	}, {
                     xtype: 'button',
                    text: 'Aggiorna',
                    iconCls: 'icon-button_black_play-32',
		            scale: 'large',	      
		             handler: function() {
		             
		               var grid = this.up('grid');
		               var rows = grid.getStore().data.items;
		               var cod = this.up('window').down('#w_cod').getValue();
		               var desc = this.up('window').down('#w_desc').getValue();
		              
		               id_selected = grid.getSelectionModel().getSelection();	
		                  						
					   list_desc = [];             
		                 for (var i=0; i<rows.length; i++){
		                      rows[i].set('n_cod_var', cod);
		                      rows[i].set('n_desc_var', desc);
		                     }
                         }
		            }, 		
			 		
			 		'->',	
					{
						xtype: 'button',
			            text: 'Verifica',
				        iconCls: 'icon-help_black-32',
		            	scale: 'large',	 		            
			            handler: function() {
			            
			            	var grid = this.up('grid');
                   	 		var loc_win = this.up('window');
                  
                      		rows_modified = grid.getStore().getUpdatedRecords();
                
                      		list_rows_modified = [];
                            
                      		for (var i=0; i<rows_modified.length; i++)
		              			list_rows_modified.push(rows_modified[i].data);
		              			
		              	    Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_mod',
						        method     : 'POST',
			        			jsonData: {
			        				list_rows_modified : list_rows_modified,
			        				verifica : 'Y'
								},							        
						        success : function(result, request){
						           var jsonData = Ext.decode(result.responseText);
						          
						           if(jsonData.corretto == 'Y'){
						           
						           grid.getStore().each(function(record){
						           
						                  if(record.get('red') == 'Y')
                                          	record.set('red', 'N');
                                          
                                    });
                                    
                                    acs_show_msg_info('Verifica terminata con successo');
                                    
						           
						           }else{
						            var error_msg = "";
						            var cod_var = "";
						            for (var i=0; i<jsonData.length; i++) {
						            
						            for (var a=0; a<rows_modified.length; a++){
						             if(rows_modified[a].get('n_cod_var') == jsonData[i].cod_var && rows_modified[a].get('cod_val') == jsonData[i].cod_val){
						                    rows_modified[a].set('red', jsonData[i].red);
						                }
						             } 
						                						            
						                var err_cod_var = jsonData[i].cod_var + ': ';
						            	if(jsonData[i].cod_var.trim() != cod_var){ 
						            		error_msg =  error_msg.concat(err_cod_var, jsonData[i].msg) + '<br>';
						            		rows_modified[i].set('red', jsonData[i].red);
						            	}	
						               
						                cod_var = jsonData[i].cod_var.trim();
						                
	 								} 
	 								
	 								acs_show_msg_error(error_msg);
	 								
	 								}
						           								        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
		              
			               			                
			            }
			        } ,
			        
			        {
			            xtype: 'button',
			            text: 'Conferma',
				        iconCls: 'icon-save-32',
		            	scale: 'large',	 		            
			            handler: function() {
			            
			             var grid = this.up('grid');
                         var loc_win = this.up('window');
                         
                          rows_modified = grid.getStore().getUpdatedRecords();
                      	  list_rows_modified = [];
                      	  for (var i=0; i<rows_modified.length; i++)
		              			list_rows_modified.push(rows_modified[i].data);
		              			
		              			    
		          
					   Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_mod',
						        method     : 'POST',
			        			jsonData: {
			        				list_rows_modified : list_rows_modified
			        			},							        
						        success : function(result, request){
						           var jsonData = Ext.decode(result.responseText);
						           
						           if(jsonData.success == false){
						           
						            acs_show_msg_error(jsonData.error_msg);
	 					 			return;
						           
						           
						           }else{
						             loc_win.fireEvent('afterConfirm', loc_win);	
						           
						           }
						           
						          							        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               			                
			            }
			        } 
		        ]
		        }]
		        
		        ,viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			        
			        
			           if(record.get('red') == 'Y')
			               return ' colora_riga_rosso';
			           
			           return '';																
			         }   
			    }
		        
	
		
  }
]}


<?php

exit;
}


