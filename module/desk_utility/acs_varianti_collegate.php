<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

//*************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi'){
    //*************************************************************
    $ret = array();
    
    
    foreach($m_params->list_selected_id as $v){
        $sql = "SELECT *
        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUVN'
        AND TACOR2 = '{$m_params->master}' AND TANR = '{$v->codice}'";
             
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        while($row = db2_fetch_assoc($stmt)) {
            $row['TACOR2']   = $m_params->variabile;
            
            $sql_dup = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($row) . ") VALUES (" . create_parameters_point_by_ar($row) . ")";
            $stmt_dup = db2_prepare($conn, $sql_dup);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_dup, $row);
            echo db2_stmt_errormsg($stmt_dup);
            
            
        }
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


//*************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_van'){
    //*************************************************************
    $ret = array();
    
    //$rgb = substr($tarest, 84, 9);
    $master = $m_params->form_values->TACOR2;
    $tanr   = $m_params->form_values->TANR;
    $tarest = $m_params->form_values->TAREST;
    
    
    $m_table_config = array('TAREST' =>
        array(
            'red'	=> array(
                "start" => 84,
                "len"   => 3,
                "riempi_con" => "0"
            )
            ,'green'	=> array(
                "start" => 87,
                "len"   => 3,
                "riempi_con" => "0"
            )
            ,'blue'	=> array(
                "start" => 90,
                "len"   => 3,
                "riempi_con" => "0"
            )
        )
    );
    
    $sql = "SELECT TA2.*, RRN(TA2) AS RRN
            FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            INNER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA2
            ON TA.TADT = TA2.TADT AND TA2.TANR = '{$tanr}' AND TA2.TACOR2 = TA.TANR AND TA2.TAID = 'PUVN'
            WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'PUVR'
            AND SUBSTRING(TA.TAREST, 68, 3) = '{$master}' AND TA.TANR <> '{$master}'
            ";
        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        $value_attuale = $row['TAREST'];
        if(trim($value_attuale) == "")
            $value_attuale = sprintf("%-248s", "");
            
            //$new_value = "";
          foreach($m_table_config['TAREST'] as $k => $v){
              if(isset($m_params->form_values->$k)){
                  $chars = $m_params->form_values->$k;
              if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
                  $len = "%{$v['riempi_con']}{$v['len']}s";
              else
                  $len = "%-{$v['len']}s";
                            
            $chars = substr(sprintf($len, $chars), 0, $v['len']);
            $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
            $value_attuale = $new_value;
                 }
                
        }
        
        $nr['TAREST'] = $new_value;
                
        $sql_up = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        SET " . create_name_field_by_ar_UPDATE($nr) . "
                   WHERE RRN(TA) = '{$row['RRN']}'";
      
        $stmt_up = db2_prepare($conn, $sql_up);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_up, $nr);
        echo db2_stmt_errormsg($stmt_up);
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

//*************************************************************
if ($_REQUEST['fn'] == 'exe_duplica'){
    //*************************************************************
   $ret = array();
 
   
   foreach($m_params->list_selected_id as $v){
   $sql = "SELECT *
           FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
           WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUVN'
           AND TACOR2 = '{$m_params->master}' AND TANR = '{$v->variante}'";
   
   $stmt = db2_prepare($conn, $sql);
   echo db2_stmt_errormsg();
   $result = db2_execute($stmt);
   while($row = db2_fetch_assoc($stmt)) {
       $row['TACOR2']   = $v->codice;
       $row['TADESC']   = $v->desc_van;
       
       $sql_dup = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($row) . ") VALUES (" . create_parameters_point_by_ar($row) . ")";
       $stmt_dup = db2_prepare($conn, $sql_dup);
       echo db2_stmt_errormsg();
       $result = db2_execute($stmt_dup, $row);
       echo db2_stmt_errormsg($stmt_dup);
           
           
   }
   }
    
   $ret['success'] = true;
   echo acs_je($ret);
   exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid_master'){
    
    $master = $m_params->open_request->master;
    $variabile = $m_params->open_request->variabile;
 
    $sql = "SELECT *
    FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    WHERE TADT='{$id_ditta_default}' AND TALINV = ''
    AND TAID = 'PUVN' AND TACOR2= '{$master}' 
    AND TANR NOT IN (
            SELECT TANR FROM {$cfg_mod_DeskArt['file_tabelle_sys']} 
            WHERE TADT='{$id_ditta_default}' AND TAID = 'PUVN' AND TACOR2 = '{$variabile}'
            )
    ORDER BY TANR, TADESC";
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        $nr['codice']        = trim($row['TANR']);
        $nr['descrizione']   = $row['TADESC'];
        $nr['red']        = substr($row['TAREST'], 84, 3);
        $nr['green']      = substr($row['TAREST'], 87, 3);
        $nr['blue']       = substr($row['TAREST'], 90, 3);
        if(trim($nr['red']) != '')
            $nr['t_rgb']      = "RGB: {$nr['red']}.{$nr['green']}.{$nr['blue']}";
        $nr['tarest']     = $row['TAREST'];
        $nr['TADTGE']     = $row['TADTGE'];
        $nr['TADTUM']     = $row['TADTUM'];
        $nr['TAUSGE']     = $row['TAUSGE'];
        $nr['TAUSUM']     = $row['TAUSUM'];
        $ar[] = $nr;
        //$ta_sys = find_TA_sys('PUFA', trim($row['CIFASE']));
        
    }
    
    echo acs_je($ar);
    exit;
    
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $master = $m_params->open_request->master;
    $sql = "SELECT *
            FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            WHERE TADT='{$id_ditta_default}' AND TALINV = '' 
            AND SUBSTRING(TAREST, 68, 3) = '{$master}' AND TAID = 'PUVR' AND TANR <> '{$master}'
            ORDER BY TANR, TADESC";
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        $nr['codice']        = trim($row['TANR']);
        $nr['descrizione']   = $row['TADESC'];
        $nr['variante']      = $m_params->open_request->variante->TANR;
        $nr['desc_van']      = $m_params->open_request->variante->descrizione;
        $ar[] = $nr;
        //$ta_sys = find_TA_sys('PUFA', trim($row['CIFASE']));
    
    }
    
    echo acs_je($ar);
    exit;
}



if ($_REQUEST['fn'] == 'open_grid'){?>
	
	{"success":true, "items": [ 
	
	{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,	
				        autoScroll : true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
            			plugins: [
        		          Ext.create('Ext.grid.plugin.CellEditing', {
        		            clicksToEdit: 1,
        		          })
        		      	],
        		      	selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
				        store: {
						xtype: 'store',
						autoLoad:true,
							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							          extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['codice', 'descrizione', 'variante', 'desc_van'
		        			]							
									
			}, //store
				<?php $cond = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
			
			      columns: [	
			      {
	                header   : 'Cod.',
	                dataIndex: 'codice',
	                width : 60,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Descrizione variabile',
	                dataIndex: 'descrizione',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Cod.',
	                dataIndex: 'variante',
	                width : 60,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Descrizione variante',
	                dataIndex: 'desc_van',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true,
	                editor: {
	                	xtype: 'textfield',
	                	allowBlank: true
	            	},
	                }
                 	
                
	                
	         ],   listeners: {
	           
				 }
				 
			  ,viewConfig: {
				        //Return CSS class to apply to rows depending upon data values
				        getRowClass: function(record, index) {
				           
				           return '';																
				         }   
				    },
					    
					    			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                  '->',
        	       {
                     xtype: 'button',
                     text: 'Conferma',
		             scale: 'large',	                     
					 iconCls: 'icon-button_blue_play-32',
		          	 handler: function() {
		          	   var grid = this.up('grid');
				       var loc_win = this.up('window');
		          	   var records =  grid.getSelectionModel().getSelection();
	       			   list_selected_id = [];                           
                             for (var i=0; i<records.length; i++) 
                        	 list_selected_id.push(records[i].data);
	       			  
	    			         Ext.Ajax.request({
        				        url        : 'acs_varianti_collegate.php?fn=exe_duplica',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				 list_selected_id : list_selected_id,
        	        				 master : <?php echo j($m_params->master); ?>
        	        			},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					        loc_win.close();
        					         
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				      });
	       			  
	       			    }
	       			  
	       			  }
                	
                	]
            
                }]
					       
		
		
		}
	
	]}

	
<?php }


if ($_REQUEST['fn'] == 'open_grid_master'){?>
	
	{"success":true, "items": [ 
	
	{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,	
				        autoScroll : true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
            			plugins: [
        		          Ext.create('Ext.grid.plugin.CellEditing', {
        		            clicksToEdit: 1,
        		          })
        		      	],
        		      	selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
				        store: {
						xtype: 'store',
						autoLoad:true,
							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_master', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							          extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['codice', 'descrizione', 'variante', 'desc_van', 'rgb', 
		        			         'red', 'green', 'blue', 'tarest', 't_rgb', 'TADTGE', 'TADTUM', 'TAUSGE', 'TAUSUM'
		        			]							
									
			}, //store
			<?php $cond = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
			
			      columns: [	
			      {
	                header   : 'Cod.',
	                dataIndex: 'codice',
	                width : 60,
	                filter: {type: 'string'}, filterable: true
	                },
	                    {
	                header   : 'RGB',
	                dataIndex: 'rgb',
	                filter: {type: 'string'}, filterable: true,
	                width : 35,
	                renderer: function(value, metaData, record){
   							var red = record.get('red');
                            var green = record.get('green');
                            var blue = record.get('blue');
                            metaData.style += 'background-color: rgb(' + red + ', '+ green + ', ' + blue + ');';
                            if(record.get('t_rgb').trim() != '')
		    		 	    	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_rgb')) + '"';    			    	
		    			  	return '';
		    		  }
	                },
	                {
	                header   : 'Descrizione variante',
	                dataIndex: 'descrizione',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                },
	                {header: 'Immissione',
                	 columns: [
                	 {header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
                	 renderer: function(value, metaData, record){
                	         if (record.get('TADTGE') != record.get('TADTUM')) 
                	          metaData.tdCls += ' grassetto';
                	 
                             q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
                			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
                		     return date_from_AS(value);	
                	}
                	 
                	 }
            	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
            	  renderer: function(value, metaData, record){
            	  
            	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
            	          metaData.tdCls += ' grassetto';
            	  
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return value;	
            	} 
            	 }
            	 ]}
                 	
                
	                
	         ],   listeners: {
	           
				 }
				 
			  ,viewConfig: {
				        //Return CSS class to apply to rows depending upon data values
				        getRowClass: function(record, index) {
				           
				           return '';																
				         }   
				    },
					    
					    			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                  '->',
        	       {
                     xtype: 'button',
                     text: 'Conferma',
		             scale: 'large',	                     
					 iconCls: 'icon-button_blue_play-32',
		          	 handler: function() {
		          	   var grid = this.up('grid');
				       var loc_win = this.up('window');
		          	   var records =  grid.getSelectionModel().getSelection();
	       			   list_selected_id = [];                           
                             for (var i=0; i<records.length; i++) 
                        	 list_selected_id.push(records[i].data);
	       			  
	    			         Ext.Ajax.request({
        				        url        : 'acs_varianti_collegate.php?fn=exe_aggiungi',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				 list_selected_id : list_selected_id,
        	        				 master : <?php echo j($m_params->master); ?>,
        	        				 variabile : <?php echo j($m_params->variabile); ?>
        	        			},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
    					        	loc_win.fireEvent('afterInsert', loc_win)
        					         
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				      });
	       			  
	       			    }
	       			  
	       			  }
                	
                	]
            
                }]
					       
		
		
		}
	
	]}

	
<?php }
