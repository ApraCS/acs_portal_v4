<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_DeskArt['file_tabelle'],
    
    'TATAID' => 'ANFBP',
    'CL_AF' => 'AFBPEL',
    'descrizione' => 'Gestione anagrafica articoli formati - PELLICOLA BORDO',
    
    'fields_key' => array('TAKEY1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice',  'maxLength' => 4, 'fw' =>'width : 100'),
        'TADESC' => array('label'	=> 'Descrizione')
    ),
    
    'fireEvent' => 'Y'
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
