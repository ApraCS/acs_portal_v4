<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();

$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);


$cod_art = $form_values->form_values->f_cod_art;
$desc_art = $form_values->form_values->f_desc_art;
$cod_forn = $form_values->form_values->f_fornitore;
$magaz = $form_values->form_values->f_magazzino;

if (strlen($cod_art) > 0)
    $sql_where.= " AND UPPER(SCCAR0) LIKE '%" . strtoupper($cod_art) . "%' ";
if (strlen($desc_art) > 0)
    $sql_where.= " AND UPPER(SCDES0) LIKE '%" . strtoupper($desc_art) . "%' ";
        
if (strlen($cod_forn) > 0)
    $sql_where.= " AND UPPER(SCCFO0) = '" . strtoupper($cod_forn) . "' ";

if (strlen($magaz) > 0)
    $sql_where.= " AND SCCMA0 = '{$magaz}' ";
        
    
    $sql = "SELECT SCDIV0, SCCFO0, SCRAG0, SCCAR0, SCDES0, SCDOM0, SCDEV0, SCGRI0, SCGGT0, SCLSH0, SCSCS0,
    SCSCM0, SCSCF0, SCRIO0, SCDTM0, SCDTF0, SCCMA0, SCDMA0, SCCAL0, SCSCT0, SCLOT0, SCCOM0, SCCFS0, SCUM00,
    TA_LS.TADESC AS LS_DESC
    FROM {$cfg_mod_DeskUtility['file_scorte']} SC
    LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tabelle_acq']} TA_LS
    ON SCDIT0 = TA_LS.TADT AND SCLSH0 = TA_LS.TAKEY1  AND TA_LS.TATAID = 'LIVSR'
    WHERE SCDIT0 = '$id_ditta_default' AND SCDIV0 = '{$form_values->form_values->f_divisione}'
    {$sql_where} ORDER BY  SCCFO0, SCRAG0, SCCAR0, SCDES0, SCCMA0";
            
    /*print_r($sql);
    exit;*/
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['SCCFO0'] = trim($row['SCCFO0']);
        $nr['SCRAG0'] = trim($row['SCRAG0']);
        $nr['SCCAR0'] = trim($row['SCCAR0']);
        $nr['SCDES0'] = trim($row['SCDES0']);
        $nr['SCDOM0'] = trim($row['SCDOM0']);
        $nr['SCDEV0'] = trim($row['SCDEV0']);
        $nr['SCGRI0'] = trim($row['SCGRI0']);
        $nr['SCGGT0'] = trim($row['SCGGT0']);
        $nr['SCLSH0'] = trim($row['SCLSH0']);
        if($row['LS_DESC'] != 0)
            $nr['LS_DESC'] = substr(trim($row['LS_DESC']), 0, 4);
        $nr['SCSCS0'] = trim($row['SCSCS0']);
        $nr['SCSCM0'] = trim($row['SCSCM0']);
        $nr['SCSCF0'] = trim($row['SCSCF0']);
        $nr['SCRIO0'] = trim($row['SCRIO0']);
        $nr['SCDIV0'] = trim($row['SCDIV0']);
        $nr['SCDTM0'] = trim($row['SCDTM0']);
        $nr['SCCMA0'] = trim($row['SCCMA0']);
        $nr['SCDMA0'] = trim($row['SCDMA0']);
        $nr['SCCAL0'] = trim($row['SCCAL0']);
        $nr['SCSCT0'] = trim($row['SCSCT0']);
        $nr['SCLOT0'] = trim($row['SCLOT0']);
        $nr['SCCOM0'] = trim($row['SCCOM0']);
        $nr['SCDTF0'] = trim($row['SCDTF0']);
        $nr['SCUM00'] = trim($row['SCUM00']);
        $nr['SCCFS0'] = trim($row['SCCFS0']);
        
        $ar[] = $nr;
    }

   

echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>Scorte</H2>
 		</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";

echo "<tr class='liv_data' >
  		<th>Fornitore</th>
  		<th>Articolo</th>
		<th>Descrizione</th>
		<th>Magaz.</th>
		<th>UM</th>
		<th>Coeff. stag.</th>
		<th>Domanda prevista</th>
		<th>Consumo medio</th>
		<th>Dev. std</th>
		<th>LT</th>
		<th>Gg trasp.</th>
		<th>LS%</th>
		<th>SS</th>
		<th>Tipo calc.</th>
		<th>ROP</th>
		<th>ROP forzato</th>
		<th>Data forz.</th>
		<th>Lotto calc.</th>
		<th>Scorte trasf.</th>
		<th>Lotto trasf.</th>
		</tr>";

foreach ($ar as $kar => $r){
	
	echo "<tr>";
    echo "  <td>".$r['SCRAG0']." [".$r['SCCFO0']."]</td>
   			<td>".$r['SCCAR0']."</td>
 			<td>".$r['SCDES0']."</td>
			<td>".$r['SCCMA0']."</td>
			<td>".$r['SCUM00']."</td>
            <td class=number>".n($r['SCCFS0'],3)."</td>
		    <td class=number>".n($r['SCDOM0'],3)."</td>
   		  	<td class=number>".n($r['SCCOM0'],3)."</td>
   		  	<td class=number>".n($r['SCDEV0'],2)."</td>
   		  	<td class=number>".n($r['SCGRI0'],2)."</td>
   		  	<td class=number>".$r['SCGGT0']."</td>
   		  	<td class=number>".$r['LS_DESC']."</td>
   		  	<td class=number>".$r['SCSCS0']."</td>
   		  	<td>".$r['SCCAL0']."</td>
   		  	<td class=number>".$r['SCSCM0']."</td>
   		  	<td class=number>".$r['SCSCF0']."</td>
   		  	<td>".print_date($r['SCDTF0'])."</td>
   		  	<td class=number>".$r['SCRIO0']."</td>
   		  	<td class=number>".$r['SCSCT0']."</td>
   		  	<td class=number>".$r['SCLOT0']."</td>
            </tr>";
	

	
	
}


?>
</div>
</body>
</html>	

