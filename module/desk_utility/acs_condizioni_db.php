<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_upd'){
 
    $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_note']}
            WHERE NTDT = {$id_ditta_default} AND NTTPNO = 'DBCON' 
            AND NTKEY1 = 'HELP-CON-DB' AND NTSEQU=0";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
        
    if($row['C_ROW'] > 0){
            
            $ar_upd = array();
            $ar_upd['NTMEMO'] = trim($m_params->form_values->f_memo);
            
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
                    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                    WHERE NTDT = '{$id_ditta_default}' AND NTKEY1 = 'HELP-CON-DB'
                    AND NTSEQU = 0 AND NTTPNO = 'DBCON'";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd);
            echo db2_stmt_errormsg($stmt);
    }else {
            
            $sqlMemo = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(NTDT, NTMEMO, NTKEY1, NTSEQU, NTTPNO)
                        VALUES(?, ?, 'HELP-CON-DB', 0, 'DBCON')";
            
            $stmtMemo = db2_prepare($conn, $sqlMemo);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmtMemo, array($id_ditta_default, trim($m_params->form_values->f_memo)));
            
    }

    
    echo acs_je(array('success' => true));
    exit;
    
}


if ($_REQUEST['fn'] == 'open_mod'){

$sql = "SELECT NTMEMO 
        FROM {$cfg_mod_Spedizioni['file_note']} NT
        WHERE NTDT = '{$id_ditta_default}'  AND NTSEQU = 0
        AND NTTPNO = 'DBCON' AND NTKEY1 = 'HELP-CON-DB'";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$row = db2_fetch_assoc($stmt);


?>

{"success":true, "items": [

        {
            xtype: 'form',
            autoScroll : true,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            items: [ 
                	
                	{
						name: 'f_memo',
						xtype: 'textarea',
						height : 300,
						labelWidth : 50,
						fieldLabel: 'Note',
					    anchor: '-5',
					    value : <?php echo j($row['NTMEMO']); ?>							
					} 	
                	
				],
				
					buttons: [
			
			{
	            text: 'Salva',
	            handler: function() {
        	        var form = this.up('form').getForm();
		            var loc_win = this.up('window');
			            Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upd',
				        jsonData: {
				           form_values: form.getValues(),
				        },
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				          var jsonData = Ext.decode(result.responseText);
			              loc_win.close();	           
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
					    });
						                        	                	                
	            }
	        }
        ],             
				
        }
]}
<?php 

exit;
}