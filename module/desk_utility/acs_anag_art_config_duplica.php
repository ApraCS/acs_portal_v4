<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// EXE CHECK
// ******************************************************************************************

if ($_REQUEST['fn'] == 'exe_check'){
    
    ini_set('max_execution_time', 3000);
    $ret = array();
    $codice = $m_params->codice;
    
    $row = get_TA_sys('PUMO', $codice);
    
    if($row != false && count($row) > 0){
        $ret['success'] = false;
        $ret['msg_error'] = "Nuovo codice modello gi� esistente!";
        
    } else{
        $ret['success'] = true;
        
    }
    
    
    
    
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// EXE DUPLICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_duplica'){
    ini_set('max_execution_time', 3000);
    
    $form_values = $m_params->form_values;
        
    $sql_pumo = "SELECT *
    FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUMO' AND TANR = '{$m_params->mode}'";
    
    $stmt_pumo = db2_prepare($conn, $sql_pumo);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_pumo);
    $row_pumo = db2_fetch_assoc($stmt_pumo);
        
    $row_pumo['TAUSGE']   = $auth->get_user();
    $row_pumo['TADTGE']   = oggi_AS_date();
    $row_pumo['TAUSUM']   = $auth->get_user();
    $row_pumo['TADTUM']   = oggi_AS_date();
    $row_pumo['TANR']   = $m_params->form_values->new_mode;
    $row_pumo['TADESC']   = acs_toDb($m_params->form_values->new_d_mode);
    $row_pumo['TADES2']   = acs_toDb($m_params->form_values->new_note);
    
    $sql_ip = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($row_pumo) . ") VALUES (" . create_parameters_point_by_ar($row_pumo) . ")";
    $stmt = db2_prepare($conn, $sql_ip);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $row_pumo);
    echo db2_stmt_errormsg($stmt);
      
   
    $sql_pusx = "SELECT *
	             FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                 WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUSX' AND TANR = '{$m_params->mode}'";
    
    $stmt_pusx = db2_prepare($conn, $sql_pusx);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_pusx);
    
    while ($row_pusx = db2_fetch_assoc($stmt_pusx)) {
        
        $row_pusx['TAUSGE']   = $auth->get_user();
        $row_pusx['TADTGE']   = oggi_AS_date();
        $row_pusx['TAUSUM']   = $auth->get_user();
        $row_pusx['TADTUM']   = oggi_AS_date();
        $row_pusx['TANR']  	  = $m_params->form_values->new_mode;
        $row_pusx['TADESC']   = $m_params->form_values->new_d_mode;
        $row_pusx['TADES2']   = $m_params->form_values->new_note;
    
        $sql_i = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($row_pusx) . ") VALUES (" . create_parameters_point_by_ar($row_pusx) . ")";
        $stmt = db2_prepare($conn, $sql_i);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $row_pusx);
        echo db2_stmt_errormsg($stmt);
        
        
    }
    
    
    $sql_pusq = "SELECT *
                FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUSQ' AND TACOR2 = '{$m_params->mode}'";
    
    $stmt_pusq = db2_prepare($conn, $sql_pusq);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_pusq);
    
    while ($row_pusq = db2_fetch_assoc($stmt_pusq)) {
        
        $row_pusq['TAUSGE']   = $auth->get_user();
        $row_pusq['TADTGE']   = oggi_AS_date();
        $row_pusq['TAUSUM']   = $auth->get_user();
        $row_pusq['TADTUM']   = oggi_AS_date();
        $row_pusq['TACOR2']   = $m_params->form_values->new_mode;
        $row_pusq['TADESC']   = $m_params->form_values->new_d_mode;
        $row_pusq['TADES2']   = $m_params->form_values->new_note;
        
        $sql_i2 = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($row_pusq) . ") VALUES (" . create_parameters_point_by_ar($row_pusq) . ")";
        $stmt = db2_prepare($conn, $sql_i2);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $row_pusq);
        echo db2_stmt_errormsg($stmt);
        
        
    }
    
                        
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}



if ($_REQUEST['fn'] == 'open_form'){
    ?>


{"success":true, "items": [

        {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.5,
 		            frame: true,
 		            items: [
 		           
        		  	 { 
						xtype: 'fieldcontainer',
						margin : '0 0 10 0',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							{
            					name: 'mode',
            					labelWidth : 120,
            					fieldLabel : 'Modello corrente',
            					xtype: 'displayfield',
            					value : <?php echo j("<b>[{$m_params->mode}] {$m_params->d_mode}</b>"); ?>,
            					anchor: '-15',
            					flex : 1
            				  }
						]},
						
						{ 
						xtype: 'fieldcontainer',
						margin : '0 0 5 0',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							 {
            					name: 'new_mode',
            					labelWidth : 120,
            					fieldLabel : 'Nuovo codice',
            					allowBlank : false,
            					xtype: 'textfield',
                                anchor: '-15',
            					maxLength : 3,
            					width : 160,
            					listeners: {
                				'change': function(field){
                					 field.setValue(field.getValue().toUpperCase());
                  				},
                  				'blur': function(field) {
      				    				var form = this.up('form').getForm();
      				    
      				
                      					Ext.Ajax.request({
                						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check',
                						        timeout: 2400000,
                						        method     : 'POST',
                			        			jsonData: {
                			        				codice : field.getValue()
                								},				
                										        
                						        success : function(result, request){
                							        var jsonData = Ext.decode(result.responseText);
                							        
                							        if(jsonData.success == false){
                					      	    		 acs_show_msg_error(jsonData.msg_error);
                							        	 form.reset();
                				        			}
                						
                					      	    	
                							    },
                						        failure    : function(result, request){
                						            Ext.Msg.alert('Message', 'No data to be loaded');
                						        }
                						    });
                	 		

            		}
             				}
            				  }
						]},
						
												
						   {
            					name: 'new_d_mode',
            					labelWidth : 120,
            					fieldLabel : 'Nuova descrizione',
            					allowBlank : false,
            					xtype: 'textfield',
            					value : <?php echo j($m_params->d_mode); ?>,
                                anchor: '-15',
            					maxLength : 30,
            					flex : 1
            				  },
            				  
            				   {
            					name: 'new_note',
            					labelWidth : 120,
            					fieldLabel : 'Nuove note',
            					allowBlank : true,
            					xtype: 'textfield',
            					value : <?php echo j($m_params->note); ?>,
                                anchor: '-15',
            					maxLength : 30,
            					flex : 1
            				  }
            			 
			 		           ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                ,'->',
                  {
                     xtype: 'button',
                    text: 'Conferma',
		            scale: 'large',	                     
					iconCls: 'icon-button_blue_play-32',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
		          	  var form_values = form.getValues();
	       			  var loc_win = this.up('window');
	       			  
 			        if(form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_duplica',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				mode : <?php echo j($m_params->mode) ?>
						},							        
				       success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        loc_win.fireEvent('afterDuplica', loc_win, form_values.new_mode);
					        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           	}
			
			            }

			     }
			     ]
		   }]
			 	 }
	
]}

<?php

exit;
}


