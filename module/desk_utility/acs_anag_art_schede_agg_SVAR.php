<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    $nr['stampa_var'] = substr($row['TAREST'], 0, 1);
    $desc_var = get_TA_sys('PUVR', trim($row['TACOR2']));
    $nr['d_var'] = "[".trim($row['TACOR2'])."] ".$desc_var['text'];
    $nr['var'] = trim($row['TACOR2']); 
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'stampa_var', 'var', 'd_var');

    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }, {
            header   : 'Stampa variabile',
            dataIndex: 'stampa_var',
            flex : 1
            },{
            header   : 'Variabile',
            dataIndex: 'd_var',
            flex : 1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "stampa_var" => trim($row->stampa_var),
        "var" => trim($row->var)
        
    );
 
    return $ar_values;
    
}


function get_values_from_array($row){
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "stampa_var" => trim($row['stampa_var']),
        "var" => trim($row['var'])
        
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
        {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		anchor: '-15',	
		maxLength : 3,
		width : 150,
		value:  <?php echo j($ar_values['riga']); ?>
	    },
	    {xtype: 'textfield',
		name: 'descrizione',
		fieldLabel: 'Descrizione',
		width : 400,
		maxLength : 30,
		anchor: '-15',
		value:  <?php echo j($ar_values['descrizione']); ?>
		},
		{xtype: 'combo',
		name: 'stampa_var',
		fieldLabel: 'Stampa variabile',
		forceSelection: true,	
		width : 200,							
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	    value : <?php echo j($ar_values['stampa_var']); ?>,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		    data: [								    
			     {id : ' ', text : '(vuoto)'},
			     {id : 'S', text : 'S'},
			     {id : 'N', text : 'N'}
			    ]
		}
			
	   },{name: 'var',
			xtype: 'combo',
			fieldLabel: 'Variabile',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['var']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            width : 400,
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUVR'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	 }
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-1s", $form_values->stampa_var);
    $ar_ins['TANR']   = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;
    $ar_ins['TACOR2'] = $form_values->var;
    
    return $ar_ins;
    
}