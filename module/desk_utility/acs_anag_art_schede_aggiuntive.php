<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

require_once "acs_anag_art_schede_agg_{$m_params->tipo_scheda}.php";



if ($_REQUEST['fn'] == 'exe_record'){

    $riga = $m_params->riga;
        
    $sql = " SELECT RRN(TA) AS RRN, TA.*
    FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
    WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_params->tipo_scheda}' 
    AND TACOR1 = '{$_SESSION['cod_art']}' AND TANR = '{$riga}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    $row = get_array_data($row);
    $ar_values = get_values_from_array($row);
    
    $ret['success'] = true;
    $ret['values'] = $ar_values;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_mod_scheda'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
    $articoli = "";
    $count = 0;
    $count_e = 0;
    if($m_params->from_multi == 'Y'){
        $rec = $m_params->rec;
        $riga = trim($rec->riga);
        foreach($m_params->list_art as $k=>$v){
            if($v == $m_params->c_art){
                unset($m_params->list_art[$k]);
                continue;
            }
            
            $ar_ins['TADTUM'] = oggi_AS_date();
            $ar_ins['TAUSUM'] = substr($auth->get_user(), 0, 8);
            $ar_ins['TAREST'] = $rec->tarest;
            $ar_ins['TADESC'] = $rec->tadesc;  
            $ar_ins['TADES2'] = $rec->note;
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_tab_sys']} TA
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE TADT = '{$id_ditta_default}' 
                    AND TANR = '{$riga}' AND TAID = '{$m_params->tipo_scheda}' 
                    AND TACOR1 = '{$v}'";
                        
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            $error_msg = db2_stmt_errormsg($stmt);
            
            if (strlen($error_msg) > 0) {
                 $articoli .= "{$v}_";
                 $count_e++;
             } else{
                 $count++;
             }
            
        }
        
            $ret['msg_info'] = "Articoli selezionati: ".count($m_params->list_art);
            $ret['msg_info'] .= "<br>Schede aggiornate: {$count}";
            if(trim($articoli) != ''){
                $icona = '<span style="display: inline; float: left;"><a href="javascript:show_win_grid_articoli(\'' . $articoli. '\')";><img class="cell-img" src=' . img_path("icone/16x16/leaf_1.png") . '></a></span>';
                $ret['msg_info'] .= "<br>{$icona}Articoli esclusi: {$count_e}";
            } 
  
    
    }else{
        
        $ar_ins = out_ar_ins($form_values);
        
        if(isset($m_params->list_art) && count($m_params->list_art) > 0){
            $articoli = "";
            foreach($m_params->list_art as $v){
                
                $row = get_TA_sys($m_params->tipo_scheda, $m_params->form_values->riga, $v);
                
                
                if($row != false && count($row) > 0){
                    $articoli .= "{$v}_";
                    $count_e++;
                }else{
                
                $ar_ins['TADTGE'] = oggi_AS_date();
                $ar_ins['TAUSGE'] = substr($auth->get_user(), 0, 8);
                $ar_ins['TADTUM'] = oggi_AS_date();
                $ar_ins['TAUSUM'] = substr($auth->get_user(), 0, 8);
                $ar_ins["TADT"]   = $id_ditta_default;
                $ar_ins["TAID"]   = $m_params->tipo_scheda;
                $ar_ins["TACOR1"] = $v;
                
                $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_tab_sys']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
                $count++;
                
                }
                
            }
            
            $ret['msg_info'] = "Articoli selezionati: ".count($m_params->list_art);
            $ret['msg_info'] .= "<br>Schede inserite: {$count}";
            if(trim($articoli) != ''){
                $icona = '<span style="display: inline; float: left;"><a href="javascript:show_win_grid_articoli(\'' . $articoli. '\')";><img class="cell-img" src=' . img_path("icone/16x16/leaf_1.png") . '></a></span>';
                $ret['msg_info'] .= "<br>{$icona}Articoli esclusi: {$count_e}";
            }   
            
            
        }else{
            
            
            if(isset($m_params->row->rrn) && $m_params->row->rrn != ''){
                
                $ar_ins['TADTUM'] = oggi_AS_date();
                $ar_ins['TAUSUM'] = substr($auth->get_user(), 0, 8);
                
                $sql = "UPDATE {$cfg_mod_DeskUtility['file_tab_sys']} TA
                SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE RRN(TA) = '{$m_params->row->rrn}'";
                
            }else{
                
                $row = get_TA_sys($m_params->tipo_scheda, $m_params->form_values->riga, $m_params->c_art);
               
                
                if($row != false && count($row) > 0){
                    $ret['success'] = false;
                    $ret['msg_info'] = "Codice esistente!";
                    echo acs_je($ret);
                    return;
                }
                
                $ar_ins['TADTGE'] = oggi_AS_date();
                $ar_ins['TAUSGE'] = substr($auth->get_user(), 0, 8);
                $ar_ins['TADTUM'] = oggi_AS_date();
                $ar_ins['TAUSUM'] = substr($auth->get_user(), 0, 8);
                $ar_ins["TADT"]   = $id_ditta_default;
                $ar_ins["TAID"]   = $m_params->tipo_scheda;
                $ar_ins["TACOR1"] = $m_params->c_art;
               
                $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_tab_sys']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
            }
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
            $ret['msg_info'] = "Scheda articolo aggiornata";
            
            
        }
        
        
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc_scheda'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->row->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA WHERE RRN(TA) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}





if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    $ar = array();
    
    $c_art = $m_params->open_request->c_art;
    $tipo_scheda = $m_params->tipo_scheda;
   
    $sql = "SELECT RRN(TA) AS RRN, TA.*
            FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}' AND TAID = '{$tipo_scheda}' AND TACOR1 = '{$c_art}' ";
    
  
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    while($row = db2_fetch_assoc($stmt)){
  
        $nr = get_array_data($row);
        $ar[] = $nr;
        
    }
    
    
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
				items: [
						{
						xtype: 'grid',
						title: '',
						itemId : 'm_grid',
						flex:0.7,
				        loadMask: true,	
				        tbar: new Ext.Toolbar({
	                    items:[ '->',
	                    
	            		{
	            		 //iconCls: 'icon-gear-16',
        	             text: 'AGGIUNGI', 
        		           		handler: function(event, toolEl, panel){
        		           		 var m_grid = this.up('window').down('#m_grid');
        		           		 var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    m_grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					  	 acs_show_win_std('['+<?php echo j($m_params->tipo_scheda); ?>+'] Aggiungi scheda', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								 c_art : '<?php echo $m_params->c_art; ?>', tipo_scheda : '<?php echo $m_params->tipo_scheda; ?>', from_sys : '<?php echo $m_params->from_sys; ?>' 
							}, 500, 390, my_listeners, 'icon-pencil-16');
        			          
        		           		 
        		           		 }
        		           	 }
	           
	         ]            
	        }),	 
				   		store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>,
										 tipo_scheda : <?php echo acs_je($m_params->tipo_scheda) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
							
		        			fields: <?php echo out_fields(); ?>
			}, //store
				
			      columns: [
			      
			      {
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width : 40
	                },
	                
	                <?php echo out_columns(); ?>
			
                 	 
	                
	         ], listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var row = rec.data;
					     var m_grid = this;
					     
					 	 voci_menu.push({
			         		text: 'Cancella',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_scheda',
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				tipo_scheda : <?php echo acs_je($m_params->tipo_scheda); ?>
										},							        
								        success : function(result, request){
								        
								   			m_grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    		});
			    		
			    		 voci_menu.push({
			         		text: 'Modifica',
			        		iconCls : 'icon-pencil-16',          		
			        		handler: function () {
			        		   	var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					  	 acs_show_win_std('Modifica scheda [' + <?php echo j($m_params->tipo_scheda); ?> + '] ' + <?php echo j($m_params->c_art); ?> , '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								row: row, c_art : '<?php echo $m_params->c_art; ?>', tipo_scheda : '<?php echo $m_params->tipo_scheda; ?>', from_sys : '<?php echo $m_params->from_sys; ?>'  
							}, 500, 400, my_listeners, 'icon-pencil-16');
				                }
			    		});	
			    		
			    		<?php if($m_params->from_multi == 'Y'){?>
			    		 voci_menu.push({
			         		text: 'Aggiorna articoli selezionati',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		 var win = m_grid.up('form').up('window');
			        		 
			        		 Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_scheda',
						        jsonData: {
						        	list_art : <?php echo acs_je($m_params->list_art) ?>,
						        	c_art : <?php echo j($m_params->c_art); ?>,
						        	tipo_scheda : '<?php echo $m_params->tipo_scheda ?>',
						        	from_multi : <?php echo j($m_params->from_multi)?>,
						        	rec : rec.data
						        },
    						        method     : 'POST',
    						        waitMsg    : 'Data loading',
    						        success : function(result, request){
    						          var jsonData = Ext.decode(result.responseText);
    						          acs_show_msg_info(jsonData.msg_info);
    				      	    	  win.close();
    							                
    						        },
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
						    });
			        		
				            }
			    		});	
			    		<?php }?>	
			    	
			    	
			    	<?php if(strlen($m_params->c_art) == 0){?>
			    		 voci_menu.push({
			         		text: 'Aggiorna articoli',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		 var win = m_grid.up('form').up('window');
			        		 
			        		    var my_listeners = {
    			    		  			afterOkSave: function(from_win, msg){
    			    		  			    Ext.Msg.alert('Message', msg);
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
			        		 
			        		  	acs_show_win_std('Aggiorna articoli', 'acs_anag_art_schede_agg_BCER.php?fn=form_aggiorna', {
									tipo_scheda : '<?php echo $m_params->tipo_scheda ?>',
						        	rec : rec.data
							    }, 400, 200, my_listeners, 'icon-leaf-16');
			 
			        		
			        		
				            }
			    		});	
			    		
			    			 voci_menu.push({
			         		text: 'Aggiorna articoli per fornitore',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		 var win = m_grid.up('form').up('window');
			        		 
			        		    var my_listeners = {
    			    		  			afterOkSave: function(from_win, msg){
    			    		  			    Ext.Msg.alert('Message', msg);
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
			        		 
			        		  	acs_show_win_std('Aggiorna articoli per fornitore', 'acs_anag_art_schede_agg_BCER.php?fn=form_aggiorna', {
									tipo_scheda : '<?php echo $m_params->tipo_scheda ?>',
						        	rec : rec.data,
						        	per_forn : 'Y'
							    }, 400, 200, my_listeners, 'icon-leaf-16');
			 
			        		
			        		
				            }
			    		});	
			    		
			    		<?php }?>
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	  
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}


if ($_REQUEST['fn'] == 'open_mod'){
    
    $m_params = acs_m_params_json_decode();
   
    $ar_lista = array();
    if($_SESSION['cod_art'] != '' && $m_params->l_insert == 'Y'){
        
        $c_art = $_SESSION['cod_art'];
        $tipo_scheda = $m_params->tipo_scheda;
                
        $sql = " SELECT RRN(TA) AS RRN, TA.*
        FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
        WHERE TADT = '{$id_ditta_default}' AND TAID = '{$tipo_scheda}' AND TACOR1 = '{$c_art}'";
                
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        while($row = db2_fetch_assoc($stmt)){
            $nr = get_array_data($row);
            $ar_lista[] = $nr;
            
        }
        
        if(count($ar_lista) == 1){
            $ar_lista_value = $ar_lista[0];
            $ar_values = get_values_from_array($ar_lista_value);
        }
        
    }else{
        $ar_values = get_values_from_row($m_params->row);
       
    }
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            layout: {pack: 'start', align: 'stretch'},
	            items: [
	            <?php 
	           
	            if(count($ar_lista) > 1){?>
	               {
					name: 'f_row',
					xtype: 'combo',
					fieldLabel: 'Seleziona riga',
					forceSelection: true,								
					displayField: 'id',
					valueField: 'id',								
					emptyText: '- seleziona -',
			   		//allowBlank: false,								
				    anchor: '-15',
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
					    <?php echo acs_ar_to_select_json(find_TA_sys($m_params->tipo_scheda, null, $_SESSION['cod_art']), ''); ?>
					    ]
					}, listeners :{
				        select : function(comp) {
                               var form = comp.up('form').getForm();
                            	Ext.Ajax.request({
									url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_record',
							        jsonData: {
							          riga: comp.value,
							          tipo_scheda : <?php echo acs_je($m_params->tipo_scheda); ?>},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            jsonData = Ext.decode(result.responseText);
							        	form.setValues(jsonData.values);
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	
                            
                        }
					}
					
			      },
	            
	            <?php }?>
	            
	     			<?php echo out_component_form($ar_values)?>
 		       ],
	            
				buttons: [	
				
		
				<?php if($m_params->tipo_scheda == 'BCER'){
				    //recupero codice fornitore
				    if(strlen($m_params->c_art) > 0){
				        require_once '_art_dett_include_f.php';
				        $row = _get_data_articolo($m_params->c_art);
				    }else{
				        $row = array('ARFOR1' => '');
				    }
				    ?>
				
				     {
			            text: 'Elenco certificati fornitore - Adiuto',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			            	
			            	acs_show_win_std('Elenco certificati', 
    		    				    'adiuto_elenco_certificati.php?fn=open_tab', 
    		    					{
    		    						c_fornitore: <?php echo j($row['ARFOR1'])?>
    		    					}, 1200, 400,
    	        					{
    	        						'afterSelect': function(from_win, record_selected){
    	        							var l_f = form.getForm().findField('rifer');
    	        							l_f.setValue(record_selected.get('certificato_des'));
    	        							from_win.close();
    	        						}
    	        					}, 'icon-tag-16');
			            
						    }
			                
			            }, '->',
			        
				<?php }?>
				
				
				
				<?php if($m_params->tipo_scheda == 'BCER' && $m_params->l_insert == 'Y'){  ?>
				
				     {
			            text: 'Richiama',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			            	
			            	acs_show_win_std('<?php echo "BCER articolo {$m_params->list_art[0]}"; ?>', 
    		    				    'acs_anag_art_schede_aggiuntive.php?fn=open_tab', 
    		    					{/*c_art: <?php echo j($m_params->list_art[0]); ?>,*/ tipo_scheda: 'BCER',
    		    					from_multi : 'Y', list_art : <?php echo acs_je($m_params->list_art)?>}, 1200, 400,
    	        					null, 'icon-tag-16');
			            
						    }
			                
			            }, '->',
			        
				<?php }?>
				     {
				     
				     <?php if($m_params->l_insert == 'Y'){?>
				        text: 'Inserisci scheda<br>(Non aggiorna preesistenti)',
				     <?php }else{?>
				        text: 'Salva',
				     <?php }?>
			            iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			            	
			            	 if(form.getForm().isValid()){	
			                Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_scheda',
						        jsonData: {
						        	form_values : form.getValues(),
						        	row : <?php echo acs_je($m_params->row) ?>,
						        	c_art : <?php echo j($m_params->c_art); ?>,
						        	list_art : <?php echo acs_je($m_params->list_art)?>,
						        	tipo_scheda : '<?php echo $m_params->tipo_scheda ?>'
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						          var jsonData = Ext.decode(result.responseText);
						          acs_show_msg_info(jsonData.msg_info, 'Operazione completa');
					          	  loc_win.fireEvent('afterOkSave', loc_win, jsonData);
						          		            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
						    }
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	exit;
   }
   

