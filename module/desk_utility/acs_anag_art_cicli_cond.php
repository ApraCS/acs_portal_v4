<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
// ******************************************************************************************
  
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_condizioni_cicli']} CV WHERE RRN(CV) = ? ";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->form_values->rrn));
    $error_msg =  db2_stmt_errormsg($stmt);
    $ret = array();
    if (strlen($error_msg) > 0) {
        $ret['success'] = false;
        $ret['message'] = $error_msg;
    }

    $ret['success'] = $result;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_config'){
    // ******************************************************************************************
    ini_set('max_execution_time', 3000);
    $form_values = $m_params->form_values;
    $ar_upd = array();
      
    $ar_upd['CVSEQC'] 	= $form_values->cond;
    $ar_upd['CVSWIE'] 	= $form_values->swie;
    $ar_upd['CVNOTE'] 	= acs_toDb($form_values->note);
    $ar_upd['CVDTUM']   = oggi_AS_date();
    $ar_upd['CVORUM']   = oggi_AS_time();
    $ar_upd['CVUSUM']   = $auth->get_user();
    
    for($i = 1; $i <= 12 ; $i ++){
        $var = "variabile_{$i}";
        $val = "variante_{$i}";
        $tp =  "tipologia_{$i}";
        $os =  "obb_scelta_{$i}";
        if(isset($form_values->$var)){
            
            if(strlen($form_values->$var) > 0){
                $row = get_TA_sys('PUVR', $form_values->$var, null, null, null, null, 1);
                $numerica = $row['tarest'];
                $ar_upd["CVVRC{$i}"]   = $form_values->$var;
                $ar_upd["CVSWN{$i}"]   = $numerica;
            }else{
                $ar_upd["CVVRC{$i}"]   = '';
                $ar_upd["CVSWN{$i}"]   = '';
            }
            
        }
        if(isset($form_values->$val))
            $ar_upd["CVVAL{$i}"]   = $form_values->$val;
        if(isset($form_values->$val))
            $ar_upd["CVOPV{$i}"]   = $form_values->$os;
        if(isset($form_values->$val))
            $ar_upd["CVTPV{$i}"]   = $form_values->$tp;
    }
    
    
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_condizioni_cicli']} CV SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(CV) = '{$form_values->rrn}' ";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    $ret = array();
    $ret['success'] = $result;
    echo acs_je($ret);
    
    exit;
}

// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi_config'){
// ******************************************************************************************
    ini_set('max_execution_time', 3000);
    $form_values = $m_params->form_values;
    $ar_upd = array();
    
    //posso aggiungere solo condizioni di tipo DV
    
    //chiave
    $ar_upd['CVDT'] = $id_ditta_default;
    $ar_upd['CVART']    = $m_params->record->c_art;
    $ar_upd['CVDTGE']   = oggi_AS_date();
    $ar_upd['CVORGE']   = oggi_AS_time();
    $ar_upd['CVUSGE']   = $auth->get_user();
    $ar_upd['CVDTUM']   = oggi_AS_date();
    $ar_upd['CVORUM']   = oggi_AS_time();
    $ar_upd['CVUSUM']   = $auth->get_user();
    
    $ar_upd['CVART']    = $m_params->record->c_art;
    $ar_upd['CVSEQ1']   = $m_params->record->seq;
    $ar_upd['CVSEQ2']   = $m_params->record->seq2;
   
    $ar_upd['CVSEQC'] 	= $form_values->cond;
    $ar_upd['CVSWIE'] 	= $form_values->swie;
    $ar_upd['CVNOTE'] 	= acs_toDb($form_values->note);
    for($i = 1; $i <= 12 ; $i ++){
        $var = "variabile_{$i}";
        $val = "variante_{$i}";
        $tp =  "tipologia_{$i}";
        $os =  "obb_scelta_{$i}";
        if(strlen($form_values->$var) > 0){
            
            $row = get_TA_sys('PUVR', $form_values->$var, null, null, null, null, 1);
            $numerica = $row['tarest'];
            $ar_upd["CVVRC{$i}"]   = $form_values->$var;
            $ar_upd["CVSWN{$i}"]   = $numerica;
        }
        if(strlen($form_values->$val) > 0)
            $ar_upd["CVVAL{$i}"]   = $form_values->$val;
        if(strlen($form_values->$val) > 0)
            $ar_upd["CVOPV{$i}"]   = $form_values->$os;
        if(strlen($form_values->$val) > 0)
            $ar_upd["CVTPV{$i}"]   = $form_values->$tp;
    }
    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_condizioni_cicli']}(" . create_name_field_by_ar($ar_upd) . ") VALUES (" . create_parameters_point_by_ar($ar_upd) . ")";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    $ret = array();
    $ret['success'] = $result;
    echo acs_je($ret);
    
    exit;
}

// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid_cond'){
// ******************************************************************************************
    //chiave
    $rrn     = $m_params->open_request->record->rrn;
    $art     = $m_params->open_request->record->c_art;
    $seq1    = $m_params->open_request->record->seq;
    $seq2    = $m_params->open_request->record->seq2;
    
    $ar = array();
    
    //record CV ******************
    $sql = "SELECT RRN(CV) AS RRN, CV.*
    FROM {$cfg_mod_DeskUtility['file_cicli']} CI
    INNER JOIN {$cfg_mod_DeskUtility['file_condizioni_cicli']} CV
    ON CI.CIDT = CV.CVDT AND CI.CIART = CV.CVART AND CI.CISEQ1 = CV.CVSEQ1 AND CI.CISEQ2 = CV.CVSEQ2
    WHERE RRN(CI) = ?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($rrn));
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr = array(
            'rrn'         => $row['RRN'],
            'cond'        => $row['CVSEQC'],
            'swie'        => $row['CVSWIE'],
            'note'        => acs_u8e(trim($row['CVNOTE']))
        );
        for($i = 1; $i <= 12 ; $i++){
            
            $nr["variabile_{$i}"]     = trim($row["CVVRC{$i}"]);
            $nr["tipologia_{$i}"]     = trim($row["CVTPV{$i}"]);
            $nr["variante_{$i}"]      = trim($row["CVVAL{$i}"]);
            $nr["obb_scelta_{$i}"]    = trim($row["CVOPV{$i}"]);
        }
        $ar[] = $nr;
    } //while CV
    
    echo acs_je($ar);
    exit;
}

// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_cond'){
// ******************************************************************************************
    ?>
{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				
					{
						xtype: 'grid',
						title: '',
						flex:0.3,
				        loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_cond', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['type_cond', 'cond', 'swie', 'note', 'valore', 't_val', 'MDVARI', 'rrn', 'decod',
		        			
		        			<?php for($i = 1; $i <= 12; $i++){?>
		        			'variabile_<?php echo $i?>',
		        			'tipologia_<?php echo $i?>',
		        			'variante_<?php echo $i?>',
		        			'obb_scelta_<?php echo $i?>',
		        			<?php }?>
		        			
		        			]							
									
			}, //store
			
			      columns: [	
			       
	                {
	                header   : 'Cond.',
	                dataIndex: 'cond',
	                filter: {type: 'string'}, filterable: true,
	                 width : 40
	                },
	                {
	                header   : 'I/E',
	                tooltip : 'Includi/Escludi',
	                dataIndex: 'swie',
	                filter: {type: 'string'}, filterable: true,
	                 width : 30
	                }, {
	                header   : 'Note',
	                dataIndex: 'note',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                }
	              
	               
	         ], listeners: {
	              selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('panel').down('#dx_form');
		              
	                    acs_form_setValues(form_dx, selected[0].data);
	                    var rec = selected[0].data
	                    form_dx.getForm().findField('note').show();
	                     this.up('panel').down('#p_cond').show();
	                     
	                     
	                     
	                   }
		          }
				  
				 }
			, viewConfig: {
		        getRowClass: function(record, index) {
		        
		           if (record.get('tipo') == 'S')
		           		return ' colora_riga_giallo';	         		
		        		           		
		           return '';																
		         }   
		    }	
		
		
		}

		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettagli condizioni di elaborazione',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.7,
 		            frame: true,
 		            items: [
     		           {name: 'rrn', xtype: 'hiddenfield'},
     		           {name: 'type_cond', xtype: 'hiddenfield'},
				   
				   
				   {
					name: 'MDVARI',
					xtype: 'textfield',
					hidden : true								
				   },
				   
				   	{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							{
        					name: 'cond',
        					fieldLabel : 'Condizioni',
        					xtype: 'textfield',
        					anchor: '-15',
        					labelWidth : 60,
        					maxLength : 3,
        					forceSelection: true,	
        					allowBlank: false,			
        					width : 150,
        				  },{
                			name: 'swie',
                			xtype: 'combo',
                			margin : '0 0 0 5',
                			fieldLabel: 'Includi/escludi',
                			labelWidth : 80,
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: false,								
                		    anchor: '-15',
                		    width : 200,
                			store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 }
					]},
						 {
        					name: 'note',
        					fieldLabel : 'Note',
        					xtype: 'textfield',
        					anchor: '-5',
        					maxLength : 60,
        					labelWidth : 60,
        					flex : 1
        				  },
				  
				  <?php for($i = 1; $i <= 4; $i ++){
				  
				   $proc = new ApiProc();
				   echo $proc->get_json_response(
				      //extjs_combo_dom_ris(array('risposta_a_capo' => false, 'label' =>"{$i})Variabile", 'file_TA' => 'sys', 'dom_cf'  => "MDVAR{$i}",  'ris_cf' => "MDVAL{$i}"))
				      
				      extjs_combo_regola_config(array('risposta_a_capo' => false,
				          'label' => "Test {$i}",
				          'l_width' => 60,
				          'f_var' => "variabile_{$i}",
    				      'f_tp'  => "tipologia_{$i}",
    				      'f_van' => "variante_{$i}",
    				      'f_os'  => "obb_scelta_{$i}"
				      ))
				      
				  );
				  echo ",";
				  
				  }?> 
				  
				  {
        			xtype: 'container',
        			flex : 1,
        			itemId: 'p_cond',
        	 		layout: {
        				type: 'vbox', border: false, pack: 'start', align: 'stretch',				
        			}
        			, items: [
        			  
        				  <?php for($i = 5; $i <= 12; $i ++){
        				  
        				   $proc = new ApiProc();
        				   echo $proc->get_json_response(
        				      //extjs_combo_dom_ris(array('risposta_a_capo' => false, 'label' =>"{$i})Variabile", 'file_TA' => 'sys', 'dom_cf'  => "MDVAR{$i}",  'ris_cf' => "MDVAL{$i}"))
        				      
        				      extjs_combo_regola_config(array('risposta_a_capo' => false,
        				          'label' => "Test {$i}",
        				          'l_width' => 60,
        				          'f_var' => "variabile_{$i}",
            				      'f_tp'  => "tipologia_{$i}",
            				      'f_van' => "variante_{$i}",
            				      'f_os'  => "obb_scelta_{$i}"
        				      ))
        				      
        				  );
        				  echo ",";
        				  
        				  }?> 
        			]}
				  
			 		           ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			          Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
         			          Ext.Ajax.request({
        				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				form_values : form_values
        						},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					        if (jsonData.success == true){
        					        	var gridrecord = grid.getSelectionModel().getSelection();					        	
        					        	form.getForm().reset();
        					        	grid.store.load();
        					        }
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				    }); 		     
		           
				    	}
        	   		  });
			
			            }

			     }, {
                     xtype: 'button',
                    text: 'Refresh',
		            scale: 'small',	               
		            iconCls : 'icon-button_black_repeat_dx-16',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			  	  form = this.up('form'); 
 			          grid.getStore().load();
 			          form.getForm().reset();
 			        }
			     }/*,
                  {
                     xtype: 'button',
                    text: 'Condizioni accantonate',
		            scale: 'small',	                     
					iconCls: 'icon-copy-16',
		          	handler: function() {
		           		acs_show_win_std('Condizioni accantonate disponibili alla copia', 'acs_gestione_valori_copiati.php?fn=open_tab', {}, 600, 400, null, 'icon-copy-16');   	     
	       			   }
 				   },*/
                ,  {xtype : 'tbfill'},
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
                    itemId : 'b_genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
		          	  if (!form.getForm().isValid()) return false;
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_config',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				record : <?php echo acs_je($m_params->record); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        grid.store.load();
					        form.getForm().reset();
					    },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		           			           	
		               var form = this.up('form');		               
		           	   if (!form.getForm().isValid()) return false;		               
		               
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_config',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
			        			},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        form.getForm().reset();
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     }
			     ]
		   }]
			 	 }
							   
		
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}
