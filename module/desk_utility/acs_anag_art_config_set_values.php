<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();


//*************************************************************
if ($_REQUEST['fn'] == 'exe_assegna_valore'){
//*************************************************************

    $ret = array();
    //VERIFICARE DOMANI
    foreach($m_params->list_selected_id as $k => $v){
        
        $mode = $v->mode;
        $variabile = $v->mdvari;
        $valore = $v->valore;
        $n_valore = $m_params->form_values->f_valore_ammesso;
        $sequ = $v->seq3.trim($v->seq4);
        
        
        $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                WHERE MODT = '{$id_ditta_default}' AND MOMODE = '{$mode}' 
                AND MOSEQU = '{$sequ}' AND MOVARI = '{$variabile}' AND MOVALO = '{$n_valore}'";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        if($row['C_ROW'] > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Valore ammesso gi&agrave; presente";
            echo acs_je($ret);
            return;
            
        }else{
            
            $ar_ins = array();
            
                       
            $ar_ins['MODT'] 	= $id_ditta_default;
            $ar_ins['MOMODE'] 	= $mode;
            $ar_ins['MOSEQU'] 	= $sequ;
            $ar_ins['MOVALO'] 	= $n_valore;
            $ar_ins['MOVARI'] 	= $variabile;
            
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_valori_ammessi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
            
        }
        
        
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

//*************************************************************
if ($_REQUEST['fn'] == 'exe_duplica'){
//*************************************************************
    $ret = array();
    $form_values = $m_params->form_values;
         
    foreach($m_params->list_selected_id as $k => $v){
        
        if($m_params->d_seq == 'Y'){
           
            if($form_values->f_seq3 != '')
                $mdseq3 = $form_values->f_seq3;
            else
                $mdseq3 = $v->seq3;
            
            $mdseq4 = $form_values->f_seq4;
            /*if($form_values->f_seq4 != '')
                $mdseq4 = $form_values->f_seq4;
            else
                $mdseq4 = $v->seq4;*/
                    
                $mdvari = $v->mdvari;
                $mdmode = $v->mode;
                
                $mosequ = $v->seq3.$v->seq4;
                if(trim($form_values->f_seq3) != '' && trim($form_values->f_seq4) != '')
                    $mosequ_n = $form_values->f_seq3.$form_values->f_seq4;
                if(trim($form_values->f_seq3) != '' && trim($form_values->f_seq4) == '')
                    $mosequ_n = $form_values->f_seq3.trim($form_values->f_seq4);    //$v->seq4;
                if(trim($form_values->f_seq3) == '' && trim($form_values->f_seq4) != '')
                    $mosequ_n = $v->seq3.$form_values->f_seq4;
                if(trim($form_values->f_seq3) == '' && trim($form_values->f_seq4) == '')
                    $mosequ_n = $mosequ;
            
         
        }else{
            
            if($v->n_seq3 != '')
                $mdseq3 = $v->n_seq3;
            else
                $mdseq3 = $v->seq3;
            
            if($v->n_seq4 != '')
                $mdseq4 = $v->n_seq4;
            else
                $mdseq4 = $v->seq4;
                    
            if($v->n_mdvari != '')
                $mdvari = $v->n_mdvari;
            else
                $mdvari = $v->mdvari;
            
                $mdmode = $m_params->new_mode;
                
            $mosequ = $v->seq3.$v->seq4;
            if(trim($v->n_seq3) != '' && trim($v->n_seq4) != '')
                $mosequ_n = $v->n_seq3.$v->n_seq4;
            if(trim($v->n_seq3) != '' && trim($v->n_seq4) == '')
                $mosequ_n = $v->n_seq3.$v->seq4;
            if(trim($v->n_seq3) == '' && trim($v->n_seq4) != '')
                $mosequ_n = $v->seq3.$v->n_seq4;
            if(trim($v->n_seq3) == '' && trim($v->n_seq4) == '')
                $mosequ_n = $mosequ;
                
        }
        
                
        $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_DeskUtility['file_config']} MD
                WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$mdmode}' 
                AND MDSEQ3 = '{$mdseq3}' AND MDSEQ4 = '{$mdseq4}'
                AND MDVARI = '{$mdvari}'/*AND MDSEQC='999'*/";
      
        
         $stmt = db2_prepare($conn, $sql);
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt);
         $row = db2_fetch_assoc($stmt);
         if($row['C_ROW'] > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Righe da duplicare gi&agrave; esistenti";
            echo acs_je($ret);
            return;
             
         }
         
         if($m_params->d_seq == 'Y'){
             
             
             if($form_values->f_d_cond == 'Y'){
                 
                 $sql_md = "SELECT * FROM {$cfg_mod_DeskUtility['file_config']} MD
                            WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$v->mode}'
                            AND MDSEQ3 = '{$v->seq3}' AND MDSEQ4 = '{$v->seq4}'
                            AND MDVARI = '{$v->mdvari}'/*AND MDSEQC='999'*/";
                 
             }else{
                 
                 $sql_md = "SELECT * FROM {$cfg_mod_DeskUtility['file_config']} MD
                            WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$v->mode}'
                            AND MDSEQ3 = '{$v->seq3}' AND MDSEQ4 = '{$v->seq4}'
                            AND MDVARI = '{$v->mdvari}' AND MDSEQC='999'";
             }
             
             $stmt_md = db2_prepare($conn, $sql_md);
             echo db2_stmt_errormsg();
             $result = db2_execute($stmt_md);
             while($row_md = db2_fetch_assoc($stmt_md)) {
                 
                 
                 $row_md['MDUSGE']   = $auth->get_user();
                 $row_md['MDDTGE']   = oggi_AS_date();
                 $row_md['MDUSUM']   = $auth->get_user();
                 $row_md['MDDTUM']   = oggi_AS_date();
                 $row_md['MDMODE']   = $mdmode;
                 
                 if(trim($form_values->f_seq3) != '')
                     $row_md['MDSEQ3']   = $form_values->f_seq3;
                 //if(trim($form_values->f_seq4) != '')
                     $row_md['MDSEQ4']   = $form_values->f_seq4;
                          
                 $sql_i = "INSERT INTO {$cfg_mod_DeskUtility['file_config']}(" . create_name_field_by_ar($row_md) . ") VALUES (" . create_parameters_point_by_ar($row_md) . ")";
                 $stmt = db2_prepare($conn, $sql_i);
                 echo db2_stmt_errormsg();
                 $result = db2_execute($stmt, $row_md);
                 echo db2_stmt_errormsg($stmt);
             
             
             
           
                         
             }
             
             
         }else{
             
             $sql_md = "SELECT * FROM {$cfg_mod_DeskUtility['file_config']} MD
             WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$v->mode}'
             AND MDSEQ3 = '{$v->seq3}' AND MDSEQ4 = '{$v->seq4}'
             AND MDVARI = '{$v->mdvari}'/*AND MDSEQC='999'*/";
             
             
             $stmt_md = db2_prepare($conn, $sql_md);
             echo db2_stmt_errormsg();
             $result = db2_execute($stmt_md);
             while($row_md = db2_fetch_assoc($stmt_md)) {
                 
                 
                 $row_md['MDUSGE']   = $auth->get_user();
                 $row_md['MDDTGE']   = oggi_AS_date();
                 $row_md['MDUSUM']   = $auth->get_user();
                 $row_md['MDDTUM']   = oggi_AS_date();
                 $row_md['MDMODE']   = $mdmode;
                 
                 if(trim($v->n_seq3) != '')
                     $row_md['MDSEQ3']   = $v->n_seq3;
                 if(trim($v->n_seq4) != '')
                     $row_md['MDSEQ4']   = $v->n_seq4;
                 if(trim($v->n_mdvari) != '')
                     $row_md['MDVARI']   = $v->n_mdvari;
                                 
                 
                 
                 $sql_i = "INSERT INTO {$cfg_mod_DeskUtility['file_config']}(" . create_name_field_by_ar($row_md) . ") VALUES (" . create_parameters_point_by_ar($row_md) . ")";
                 $stmt = db2_prepare($conn, $sql_i);
                 echo db2_stmt_errormsg();
                 $result = db2_execute($stmt, $row_md);
                 echo db2_stmt_errormsg($stmt);
             }
             
             
         }
       
       
         
         
         
         //valori ammessi
         if( (!isset($form_values->f_d_va) && $m_params->d_seq  != 'Y')  ||
             ($m_params->d_seq  == 'Y' && isset($form_values->f_d_va) && $form_values->f_d_va == 'Y')){
         $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                 WHERE MODT = '{$id_ditta_default}' AND MOMODE = '{$mdmode}'
                 AND MOSEQU = '{$mosequ_n}'
                 AND MOVARI = '{$mdvari}'";
         
         $stmt = db2_prepare($conn, $sql);
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt);
         $row = db2_fetch_assoc($stmt);
         if($row['C_ROW'] > 0){
             $ret['success'] = false;
             $ret['msg_error'] = "Righe da duplicare gi&agrave; esistenti nei valori ammessi";
             echo acs_je($ret);
             return;
             
         }
                  
         $sql_mo = "SELECT * FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                    WHERE MODT = '{$id_ditta_default}' AND MOMODE = '{$v->mode}'
                    AND MOSEQU = '{$mosequ}'
                    AND MOVARI = '{$v->mdvari}'";
                  
         $stmt_mo = db2_prepare($conn, $sql_mo);
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt_mo);
         while($row_mo = db2_fetch_assoc($stmt_mo)) {
            
             $row_mo['MOMODE']   = $mdmode;
             if(trim($v->n_mdvari) != '')
                 $row_mo['MOVARI']   = $v->n_mdvari;
             $row_mo['MOSEQU']   = $mosequ_n;
             
             $sql_i2 = "INSERT INTO {$cfg_mod_DeskUtility['file_valori_ammessi']}(" . create_name_field_by_ar($row_mo) . ") VALUES (" . create_parameters_point_by_ar($row_mo) . ")";
             $stmt = db2_prepare($conn, $sql_i2);
             echo db2_stmt_errormsg();
             $result = db2_execute($stmt, $row_mo);
             echo db2_stmt_errormsg($stmt);
             
             
         }
        
         }
    }
    
    
   $ret['success'] = true; 
   echo acs_je($ret);
   exit;
}



if ($_REQUEST['fn'] == 'open_form'){
    ?>


{"success":true, "items": [

        {
 		            xtype: 'form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:1,
 		            frame: true,
 		            items: [
 		            
 		            {
    					name: 'mode',
    					labelWidth : 120,
    					fieldLabel : 'Modello corrente',
    					xtype: 'displayfield',
    					value : <?php echo j("<b>[{$m_params->mode}] {$m_params->d_mode}</b>"); ?>,
    					anchor: '-15',
    					flex : 1
    				  },
    				  
    				  
                       {name: 'new_mode',
                		xtype: 'combo',
                		flex: 1,
                		fieldLabel: 'Nuovo modello',
                		labelWidth : 120,
                		margin : '10 5 5 0',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     <?php echo acs_ar_to_select_json(find_TA_sys('PUMO', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                		    ]
                		}, listeners: {
                        	 beforequery: function (record) {
                                record.query = new RegExp(record.query, 'i');
                                record.forceAll = true;
                            }
                   		}
                		
                   }
			 		    
			 		    
			 		    ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                ,'->',
                  {
                     xtype: 'button',
                    text: 'Conferma',
		            scale: 'large',	                     
					iconCls: 'icon-button_blue_play-32',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
		          	  var form_values = form.getValues();
	       			  var loc_win = this.up('window');
	       			  if(form.getForm().isValid())
	       			  	loc_win.fireEvent('afterSelect', loc_win, form_values.new_mode);
			
			       }

			     }
			     ]
		   }]
			 	 }
	
]}

<?php

exit;
}



if ($_REQUEST['fn'] == 'open_form_var'){
    ?>


{"success":true, "items": [

        {
 		            xtype: 'form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.5,
 		            frame: true,
 		            items: [
 		            
 		            { xtype : 'hiddenfield',
 		              name : 'f_descrizione',
 		              itemId : 'desc'
 		            },{ xtype : 'displayfield',
 		              name : 'f_var',
 		              fieldLabel : 'Variabile corrente',
 		              value : '<?php echo "[{$m_params->mdvari}] {$m_params->d_var}"?>',
 		              margin : '0 0 10 0'
 		            },
 		           	  {name: 'f_variabile',
                		xtype: 'combo',
                		flex: 1,
                		fieldLabel: 'Nuova variabile',
                		forceSelection: true,								
                		displayField: 'text',
                		valueField: 'id',
                		queryMode: 'local',
                		minChars: 1,							
                		emptyText: '- seleziona -',
                   		//allowBlank: false,								
                	    anchor: '-15',
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                		    ]
                		}, listeners: {
                        	beforequery: function (record) {
                                record.query = new RegExp(record.query, 'i');
                                record.forceAll = true;
                            }
                   	   }, select: function(field,newVal) {	
                          		var descrizione = field.data.text;
                          		var ar_d = descrizione.split(']');
                          		this.up('window').down('#desc').setValue(ar_d[1]);
                
                                }
                		
                   },
			 		    
			 		    
			 		    ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                ,'->',
                  {
                     xtype: 'button',
                    text: 'Conferma',
		            scale: 'large',	                     
					iconCls: 'icon-button_blue_play-32',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
		          	  var form_values = form.getValues();
	       			  var loc_win = this.up('window');
	       			  loc_win.fireEvent('afterSelect', loc_win, form_values);
			
			       }

			     }
			     ]
		   }]
			 	 }
	
]}

<?php

exit;
}


if ($_REQUEST['fn'] == 'open_form_seq'){
    ?>


{"success":true, "items": [

        {
 		            xtype: 'form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.5,
 		            frame: true,
 		            items: [
 		             { xtype : 'displayfield',
 		              name : 'f_seq_corrente',
 		              labelWidth : 150,
 		              fieldLabel : 'Sequenza/Riga corrente',
 		              value : '<?php echo "{$m_params->seq3} - {$m_params->seq4}"?>',
 		              maxLength : 3, 
    				  width : 200,
    				  listeners : {
    					  'change': function(field){
            				  field.setValue(field.getValue().toUpperCase());
          					}
					}
 		            },
 		            
 		            { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{ xtype : 'textfield',
 		              name : 'f_seq3',
 		              fieldLabel : 'Sequenza/Riga nuova',
 		              maxLength : 3, 
 		              labelWidth : 150,
    			       width : 190,
    					listeners : {
    					  'change': function(field){
            				  field.setValue(field.getValue().toUpperCase());
          					}
					}
 		            },
 		           	 { xtype : 'textfield',
 		              name : 'f_seq4',
 		              fieldLabel : '',
 		              maxLength : 1, 
    				  width : 20,
    				  listeners : {
    					  'change': function(field){
            				  field.setValue(field.getValue().toUpperCase());
          					}
					}
 		            }
						
						   
						]},
						<?php if($m_params->d_seq == 'Y') {?>
						 {
						xtype: 'checkboxgroup',
						fieldLabel: 'Duplica condizioni',
						margin : '0 0 0 0',
						labelWidth : 150,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_d_cond' 
                          , boxLabel: ''
                          , checked : true
                          , inputValue: 'Y'
                        }]							
					},
					 {
						xtype: 'checkboxgroup',
						fieldLabel: 'Duplica valori assunti',
						labelWidth : 150,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_d_va' 
                          , boxLabel: ''
                          , checked : true
                          , inputValue: 'Y'
                        }]							
					}
 		            
 		            <?php }?>
			 		    
			 		    
			 		    ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                ,'->',
                  {
                     xtype: 'button',
                    text: 'Conferma',
		            scale: 'large',	                     
					iconCls: 'icon-button_blue_play-32',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
		          	  var form_values = form.getValues();
	       			  var loc_win = this.up('window');
	       			  <?php if($m_params->d_seq == 'Y'){?>
	       			  	loc_win.fireEvent('confirmDuplica', loc_win, form_values);
	       			  <?php }else{?>
	       			  	loc_win.fireEvent('afterSelect', loc_win, form_values);
			          <?php }?>
			       }

			     }
			     ]
		   }]
			 	 }
	
]}

<?php

exit;
}


if ($_REQUEST['fn'] == 'open_form_va'){
    
    
    $variabile = "<b>[{$m_params->list_selected_id[0]->mdvari}] {$m_params->list_selected_id[0]->denominazione}</b>";
  
    
    ?>


{"success":true, "items": [

        {
 		            xtype: 'form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.5,
 		            frame: true,
 		            items: [
 		            
 		            { xtype : 'displayfield',
 		              name : 'f_variabile',
 		              fieldLabel : 'Variabile',
 		              value : <?php echo j($variabile);?>
 		            
				    }, {
        			xtype : 'combo',
        		 	name: 'f_valore_ammesso',
		 			fieldLabel: 'Variante',
    		 		forceSelection: true,  							
					displayField: 'text',
		    		valueField: 'id',	
		    		anchor: '-15',						
        			emptyText: '- seleziona -',
        		   	queryMode: 'local',
             		minChars: 1,	
        			store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}],
            		    data: [								    
    		     			<?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $m_params->list_selected_id[0]->mdvari, null, null, 0, '', 'Y'), ''); ?>	
            		    ]
        			}, listeners: { 
        			 		beforequery: function (record) {
        	         		record.query = new RegExp(record.query, 'i');
        	         		record.forceAll = true;
    				 }
         			},	
        		 	
        		  } 
			 		    ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                ,'->',
                  {
                     xtype: 'button',
                    text: 'Conferma',
		            scale: 'large',	                     
					iconCls: 'icon-button_blue_play-32',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
		          	  var form_values = form.getValues();
	       			  var loc_win = this.up('window');
	       			 
	       			 
	       			  Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_valore',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>,
			        				form_values : form_values
			        				
								},							        
						         success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					         if(jsonData.success == false){
					      	    		 acs_show_msg_error(jsonData.msg_error);
							        	 return;
				        			 }else{
        					         	loc_win.fireEvent('afterConferma', loc_win, form_values.f_valore_ammesso);
        					         }
        				        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						    
						    
	       			 
			
			       }

			     }
			     ]
		   }]
			 	 }
	
]}

<?php

exit;
}