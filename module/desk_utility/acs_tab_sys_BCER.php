<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'BCER',
    //'title_grid' => '',
    'fields' => array(
        'TAID'   => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'certif'   => array('label'	=> 'Certificazione', 'maxLength' => 1, 'width' => 160),
        'data_iniz'   => array('label'	=> 'Data val. iniziale', 'only_view' => 'F', 'xtype' => 'datefield'),
        'data_fin'   => array('label'	=> 'Data val. finale', 'only_view' => 'F', 'xtype' => 'datefield'),
        'rifer'   => array('label'	=> 'Riferimento', 'maxLength' => 30, 'only_view' => 'F'),
        'area'   => array('label'	=> 'Area geografica', 'maxLength' => 30, 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'RRN'    => array('label'	=> 'RRN', 'hidden' => 'true'),
        //'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TACOR1'      => array('label'	=> 'articolo', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli',
    'tab_tipo' => 'BAGE',
    'TAREST' => array(
        'certif' => array(
            "start" => 0,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'data_iniz' => array(
            "start" => 1,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'data_fin' => array(
            "start" => 9,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'rifer' => array(
            "start" => 17,
            "len"   => 30,
            "riempi_con" => ""
        ),
        'area' => array(
            "start" => 47,
            "len"   => 3,
            "riempi_con" => ""
        ),
        
    )

);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

