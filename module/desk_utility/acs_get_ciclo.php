<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();
$c_art = acs_u8e($m_params->c_art);
$d_art = acs_u8e($m_params->d_art);
$riga_orig = $m_params->riga_comp;
$k_ordine = $m_params->k_ordine;

$oe = $s->k_ordine_td_decode_xx($k_ordine);

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $c_art = $m_params->open_request->c_art;
    $row = $m_params->open_request->row;
    
    $sql = "SELECT *
            FROM {$cfg_mod_DeskUtility['file_cicli_ricom']} RI
            WHERE RIDT = '{$id_ditta_default}' AND RITPCO = '{$row->RETPCO}'
            AND RIAACO = {$row->REAACO} AND RINRCO = {$row->RENRCO} AND RIRIGA = {$row->RERIGA} 
            AND RITPSV = '{$row->RETPSV}' AND RITIPO = 'S'";
  
              
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
            while($row = db2_fetch_assoc($stmt)){
                $nr = array();
                
                $nr['seq']      = trim($row['RISEQ1']);
                $nr['fase'] 	=  "[".trim($row['RIFASE'])."] ".trim($row['RIDFAS']);
                $nr['centro'] 	=  "[".trim($row['RICELO'])."] ".trim($row['RIDCEL']);
               
                $nr['c_fase']    = trim($row['RIFASE']);
                $nr['c_centro']  = trim($row['RICELO']);
                $nr['t_man']     = $row['RITEMO'];
                $nr['um_man']    = $row['RIUMMO'];
                if($row['RITEMO'] > 0)
                    $nr['man'] = "[".$row['RIUMMO']."] ".n($row['RITEMO'],3);
                
                
                $ar[] = $nr;
            }
            
            echo acs_je($ar);
            exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $title = "Riga fabbisogno {$oe['TDOADO']}_{$oe['TDONDO']}_FB riga {$riga_orig} [{$c_art}] {$d_art}";  			
    
    ?>

{"success":true, 
  m_win: {
		width: 800,
		height: 450,
		iconCls: 'icon-gear-16',
		title: <?php echo j($title) ?>,
	
	}, 
  items: [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
						{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['rrn', 'seq', 'fase', 'centro', 'man']							
									
			}, //store
				<?php $cond = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
			
			      columns: [	
			      {
	                header   : 'Seq.',
	                dataIndex: 'seq',
	                width : 60
	                },
	                {
	                header   : 'Fase',
	                dataIndex: 'fase',
	                flex: 1
	                },
	                {
	                header   : 'Centro',
	                dataIndex: 'centro',
	                flex: 1
	                }
	                ,{header: 'Manodopera', dataIndex: 'man',  flex: 1},
	                /*,{header: 'Tempo',
                    columns: [
                      
                      {header: 'Attrezzaggio', dataIndex: 't_att', width: 80}, 
                      {header: 'Sosta', dataIndex: 't_sos', width: 70}, 
                 	 ]},
                 	{text: '<?php echo $cond; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'cond',
    			    filter: {type: 'string'}, filterable: true,
    				tooltip: 'condizioni',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('cond') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_blue_yuan.png") ?> width=15>';
    		    		  }
    		        }
                   ,{header: 'Immissione',
                    columns: [
                      {header: 'Data', dataIndex: 'data_imm', width: 80, renderer : date_from_AS},
                      {header: 'Utente', dataIndex: 'ut_imm', width: 80}
                   	 ]}    */
	                
	         ]
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
	
]} 
]}

<?php

exit;
}

