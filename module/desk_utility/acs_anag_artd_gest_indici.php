<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_DeskArt['file_tabelle'],
    
    'TATAID' => 'ARTIC',
    'descrizione' => 'Gestione indici di documentazione regole/attivit� di codifica',
    
    'fields_key' => array('TAKEY1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice'),
        'TADESC' => array('label'	=> 'Descrizione'),
        'TAFG01' => array('label'	=> 'Attivit�')
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
