<?php
require_once "../../config.inc.php";

require_once "acs_anag_art_include.php";
		
$m_params = acs_m_params_json_decode();	

function format_periodo($periodo){
    
    $format = "";
    
   if(strlen($periodo) == 6){
       $format = ucfirst(print_date($periodo . '01', "%b.'%y"));
   }elseif(strlen($periodo) > 6){
       $format = ucfirst(print_date($periodo, "%d/%m"));
  }else{
        $format = $periodo;
  }
    
    return $format;
}


function _sql_where_by_periodo($params){
    
    //$tipo_periodo = 'ANNO', $data_riferimento
    if (!isset($params->tipo_periodo))
        $tipo_periodo = 'ANNO';
    else
        $tipo_periodo = $params->tipo_periodo;
    
    if (!isset($params->form_values->data_riferimento))
      $data_riferimento = oggi_AS_date();
    else
      $data_riferimento = $params->form_values->data_riferimento;
                

    //print_date($data, $format="%d/%m/%y")
    $data_iniziale_strtotime = print_date($data_riferimento, "%Y-%m-%d");
    
    if ($tipo_periodo == 'MESE') {
        $sql_where_by_periodo = ' AND substr(ARDTGE, 1, 6)>=' . date('Ym', strtotime("{$data_iniziale_strtotime} -13 month"));
    }elseif ($tipo_periodo == 'GIORNO') {
        $sql_where_by_periodo = ' AND substr(ARDTGE, 1, 8)>=' . date('Ymd', strtotime("{$data_iniziale_strtotime} -28 day"));
    }
     else { //anno (default)
       // $sql_where_by_periodo = ' AND substr(ARDTGE, 1, 4)>=' . date('Y', strtotime("{$data_iniziale_strtotime} -12 year"));
    }
    $sql_where_by_periodo .= " AND ARDTGE <= " . $data_riferimento;
    return $sql_where_by_periodo;
}


//******************************************
//  GRAFICO: DATI: TIPO DI RIORDINO PER ANNO
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart'){
    
    if ($m_params->tipo_periodo == 'MESE') {
        $sql_field_periodo = "SUBSTR(ARDTGE, 1, 6)";
    } elseif ($m_params->tipo_periodo == 'GIORNO') {
        $sql_field_periodo = "SUBSTR(ARDTGE, 1, 8)";
    }
    else { //anno (default)
        $sql_field_periodo = "SUBSTR(ARDTGE, 1, 4)";
    }
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
    $sql_where = anag_art_sql_where($m_params->open_request->form_open);
    $sql = "SELECT ARSWTR, {$sql_field_periodo} AS PERIODO, COUNT(*) AS NUM_ART
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            GROUP BY ARSWTR, {$sql_field_periodo}
            ORDER BY PERIODO
            ";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ar_records = array();
    $ar_tipo = array();
    $ar_fields = array();
    while ($r = db2_fetch_assoc($stmt)){
        if (isset($ar_records[($r['PERIODO'])]) === 'undefined'){
              $ar_records[($r['PERIODO'])] = array();
        }
               
        $ar_records[($r['PERIODO'])]['PERIODO'] =  format_periodo($r['PERIODO']);
        $ar_records[($r['PERIODO'])]['TOTALE'] += $r['NUM_ART'];
        $ar_records[($r['PERIODO'])][($r['ARSWTR'])] = $r['NUM_ART'];
        
        if (!(in_array($r['ARSWTR'], $ar_tipo))){
            array_push($ar_tipo, $r['ARSWTR']);
            array_push($ar_fields, $r['ARSWTR']);
        }
    }
    
    array_push($ar_fields, "PERIODO");
    array_push($ar_fields, "TOTALE");        
    
    $ar_ret = array();
    foreach($ar_records as $ar1){
        for($i=0; $i<count($ar_tipo); $i++){
            if (!($ar1[($ar_tipo[$i])] > 0))
                $ar1[($ar_tipo[$i])] = 0;
        }
        $ar_ret[] = $ar1;
    }
    

    $fields = acs_je($ar_fields);
    echo acs_je($ar_ret);
    exit;
}


//******************************************
//  GRAFICO: DATI: CICLO DI VITA
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_ciclo_di_vita'){
    
  $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
  $sql_where = anag_art_sql_where($m_params->open_request->form_open);
  $sql = "SELECT ARSOSP, ARESAU, COUNT(*) AS NREF
        FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
        WHERE ARDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
        GROUP BY ARSOSP, ARESAU
        ORDER BY ARSOSP, ARESAU
        ";
            
  $stmt = db2_prepare($conn, $sql);
  $result = db2_execute($stmt);
        
  $ret = array();
  $ret_tmp = array();
  while ($r = db2_fetch_assoc($stmt)) {
    if ($r['ARSOSP'] == 'S')
      $ret_tmp['S'] += $r['NREF'];
    else
      $ret_tmp[$r['ARESAU']] += $r['NREF'];
  }
            
            function decod_ciclo_vita($v){
                switch(trim($v)){
                    case 'S':return 'Sospesi';
                    case 'C': 	return 'Nuovi (incompleti)';
                    case 'E': 	return 'In esaurimento';
                    case 'F': 	return 'Nuovi (completi)';
                    case 'A': 	return 'In avviamento';
                    case 'R': 	return 'Riservati';
                    case '': 	return 'Attivi';
                    default: return $v;
                }
            }
            
            function decod_k_ciclo_vita($v){
                switch(trim($v)){
                    case '': 	return 'Attivi';
                    default: return $v;
                }
            }
            
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("des_name" => decod_ciclo_vita($k), "name" => decod_k_ciclo_vita($k), "referenze" => $ar);
    }
    
    echo acs_je($ret);
    exit;
}




//******************************************
//  GRAFICO: DATI: TIPO RIORDINO
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_tipo_riordino'){
    
  $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
  $sql_where = anag_art_sql_where($m_params->open_request->form_open);
  $sql = "SELECT ARSWTR AS WSTIPO, COUNT(*) AS NREF
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
           /* AND ARSOSP <> 'S'*/
            GROUP BY ARSWTR
            ORDER BY ARSWTR
            ";
            
  $stmt = db2_prepare($conn, $sql);
  $result = db2_execute($stmt);

  $ret = array();
  $ret_tmp = array();
  while ($r = db2_fetch_assoc($stmt)) {
    $ret_tmp[$r['WSTIPO']] += $r['NREF'];
  }
            
            function decod_tipo_riordino($v){
                switch(trim($v)){
                    case 'O':	return 'Mto';
                    case 'D': 	return 'Discreto';
                    case 'S': 	return 'Mts';
                    case 'P': 	return 'Mts a Programma';
                    default: return $v;
                }
            }
            
  foreach($ret_tmp as $k => $ar){
    $ret[] = array("name" => decod_tipo_riordino($k), "referenze" => $ar);
  }
  echo acs_je($ret);
  exit;
}


//******************************************
//  GRAFICO: DATI: TIPO DI APPROVIGIONAMENTO
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_tipo_approvvigionamento'){
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
    $sql_where = anag_art_sql_where($m_params->open_request->form_open);
    $sql = "SELECT ARTPAP AS WSTIPO, COUNT(*) AS NREF
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            AND ARSOSP <> 'S'
            GROUP BY ARTPAP
            ORDER BY ARTPAP
            ";
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['WSTIPO']] += $r['NREF'];
    }
    
    function decod_tipo_approvvigionamento($v){
        switch(trim($v)){
            case 'P':	return 'Produzione';
            case 'F': 	return 'Fornitore';
            case 'T': 	return 'Terzista';
            case 'E': 	return 'Escluso da analisi';
            default: return $v;
        }
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_tipo_approvvigionamento($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}

//******************************************
//  GRAFICO: DATI: TIPO PARTE
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_tipo_parte'){
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
    $sql_where = anag_art_sql_where($m_params->open_request->form_open);
    $sql = "SELECT ARTPAR AS WSTIPP, COUNT(*) AS NREF
    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
    WHERE ARDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
    AND ARSOSP <> 'S'
    GROUP BY ARTPAR
    ORDER BY ARTPAR
    ";
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['WSTIPP']] += $r['NREF'];
    }
    
    function decod_tipo_parte($v){
        
        $ta_sys = find_TA_sys('MTPA',trim($v));
        return "[".$v."] ".$ta_sys[0]['text'];
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_tipo_parte($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}


//******************************************
//  GRAFICO: DATI: UTENTE DI IMMISSIONE
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_utente_immissione'){
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
    $sql_where = anag_art_sql_where($m_params->open_request->form_open);
    $sql = "SELECT ARUSGE AS WSTIPO, COUNT(*) AS NREF
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            AND ARSOSP <> 'S'
            GROUP BY ARUSGE
            ORDER BY ARUSGE
            ";
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['WSTIPO']] += $r['NREF'];
    }
    
    function decod_utente_immissione($v){
        return $v;
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_utente_immissione($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}

//******************************************
//  GRAFICO: DATI: UTENTE DI IMMISSIONE
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_fornitore'){
    
   
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
    $sql_where = anag_art_sql_where($m_params->open_request->form_open);
    $sql = "SELECT ARFOR1 AS WSTIPO, COUNT(*) AS NREF
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            AND ARSOSP <> 'S'
            GROUP BY ARFOR1
            ORDER BY ARFOR1
            ";
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['WSTIPO']] += $r['NREF'];
    }
    
    function decod_utente_fornitore($v){
        
        global $cfg_mod_Spedizioni, $id_ditta_default, $conn;
        
        $sql = "SELECT CFRGS1 AS D_FOR
        FROM {$cfg_mod_Spedizioni['file_anag_cli']} CF
        WHERE CFDT = '{$id_ditta_default}' AND CFCD = '{$v}'";
           
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        return $row['D_FOR'];
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_utente_fornitore($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}


//i dati di codifica vengono elaborati direttamente qui
$sql_where = anag_art_sql_where($m_params->form_open);
$sql = "SELECT ARSWTR, SUBSTR(ARDTGE, 1, 4) AS ANNO, COUNT(*) AS NUM_ART 
        FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
        WHERE 1=1 {$sql_where}
        GROUP BY ARSWTR, SUBSTR(ARDTGE, 1, 4)
        ORDER BY ANNO
  ";
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$ar_records = array();
$ar_tipo = array();
$ar_fields = array();
while ($r = db2_fetch_assoc($stmt)){	
	if (isset($ar_records[($r['PERIODO'])]) === 'undefined'){
		$ar_records[($r['PERIODO'])] = array();    		 		
	}
	$ar_records[($r['PERIODO'])]['PERIODO'] = $r['PERIODO'];
	$ar_records[($r['PERIODO'])]['TOTALE'] += $r['NUM_ART'];
	$ar_records[($r['PERIODO'])][($r['ARSWTR'])] = $r['NUM_ART'];
	
	if (!(in_array($r['ARSWTR'], $ar_tipo))){
		array_push($ar_tipo, $r['ARSWTR']);
		array_push($ar_fields, $r['ARSWTR']);
	} 
}

array_push($ar_fields, "PERIODO");
array_push($ar_fields, "TOTALE");



$ar_ret = array();
foreach($ar_records as $ar1){
	for($i=0; $i<count($ar_tipo); $i++){
		if (!($ar1[($ar_tipo[$i])] > 0))
			$ar1[($ar_tipo[$i])] = 0;
	}
	$ar_ret[] = $ar1;
}


$tipo_appr = acs_je($ar_ret);
$fields = acs_je($ar_fields);

?>


{"success":true, "items": [
		 {
			xtype: 'container',
			title: '',
	 		layout: {
				type: 'hbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [
			
			
				{			
				xtype: 'panel',
				
				buttons: [
				
				{
					xtype: 'form',
	            	bodyStyle: 'padding: 10px',
	            	bodyPadding: '5 5 0',
	            	frame: true,
	            	autoScroll : true,
	            	title: '',
					items: [
        				{
        				   xtype: 'datefield'
        				   , startDay: 1 //lun.
        				   , fieldLabel: 'Data iniziale'
        				   , name: 'data_riferimento'
        				   , format: 'd/m/Y'
        				   , submitFormat: 'Ymd'
        				   , allowBlank: true
        				   , anchor: '-15'
        				   , value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
        				   , listeners: {
        				       invalid: function (field, msg) {
        				       Ext.Msg.alert('', msg);}				       
        				   }
        				}
        			]
        		}										
				
				
				,{ xtype: 'tbfill' },
				  {
			            text: 'Anni',
			            scale: 'medium',	                     
			            handler: function() {
			            	var gs = this.up('window').query('chart');
			            	var form = this.up('container').down('form').getForm();
			            	Ext.each(gs, function(g) {
			            			var st = g.getStore();
			            			st.proxy.extraParams.tipo_periodo = 'ANNO';
			            			st.proxy.extraParams.form_values = form.getValues();
									st.load();
					        	});
			            }
			       }, {
			            text: 'Ultimi 13 mesi',
			            scale: 'medium',	                     
			            handler: function() {
			            	var gs = this.up('window').query('chart');
			            	var form = this.up('container').down('form').getForm();
			            	Ext.each(gs, function(g) {
			            			var st = g.getStore();
			            			st.proxy.extraParams.tipo_periodo = 'MESE';
			            			st.proxy.extraParams.form_values = form.getValues();									
									st.load();
					        	});			            
			            }
			       }, {
			            text: 'Ultime 4 settimane',
			            scale: 'medium',	                     
			            handler: function() {
			            	var gs = this.up('window').query('chart');
			            	var form = this.up('container').down('form').getForm();
			            	Ext.each(gs, function(g) {
			            			var st = g.getStore();
			            			st.proxy.extraParams.tipo_periodo = 'GIORNO';
			            			st.proxy.extraParams.form_values = form.getValues();									
									st.load();
					        	});			            
			            }
			       }		 
				],
				title: '',
				flex: 25,
				layout: 'fit',
				items: [
				
				
				
				 {
					xtype: 'chart',
					id: 'chartCmp2',
					animate: true,
					shadow: true,
					flex: 0.8, 
					height: 300,
					legend: {
						position: 'bottom'
					},
					
					listeners: {					
			 			afterrender: function (comp) {
			 				Ext.getBody().unmask();
			 			}
			 		},						
					
					
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									type: 'json',
									read: 'POST'
								},
					
								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
								
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'							
								},
								doRequest: personalizza_extraParams_to_jsonData
								
							},
		
							fields: <?php echo acs_je($ar_fields) ?>,
										
						}, //store					
					
										
					store222222: {
						fields: <?php echo ($fields) ?>,
						data: <?php echo($tipo_appr)?>
					},					
					axes: [{
						type: 'Numeric',
						position: 'left',
						fields: ['TOTALE'],
						title: 'Nr articoli IMMESSI/Tipo riordino',
						grid: true
					}, {
						type: 'Category',
						position: 'bottom',
						fields: ['PERIODO'],
						title: false
					}],
					series: [{
						type: 'column',
						axis: 'left',
						gutter: 80,
						xField: ['PERIODO'],
						yField: ['TOTALE'],
						stacked: true,
							tips: {
								trackMouse: true,
								width: 65,
								height: 28,
								renderer: function(storeItem, item) {
									this.setTitle(String(item.value[1]));
								}
							}						
					}, 
					<?php
					
					for($i=0; $i<count($ar_tipo); $i++){
						echo "
						{
							type: 'line',
							axis: 'left',
							smooth: true,								
							xField: ['PERIODO'],
							yField: ['$ar_tipo[$i]'],
							tips: {
								trackMouse: true,
								width: 65,
								height: 28,
								renderer: function(storeItem, item) {
									this.setTitle(String(item.value[1]));
								}
							}
						}, 
						"; 
					}?>
					]
				}
				
				]
			}
			
		, {
			xtype: 'container',
			width: 400,
			title: '',
	 		layout: {
				type: 'vbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [
					{
					 xtype:  'tabpanel',
					 height: 300,
					 items: [
                        <!-- --------------------- -->
                        <!-- grafico ciclo di vita -->
                        <!-- --------------------- -->  
    					{
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Ciclo di vita',					 
    					 items: 	  				  		
    						{
    			            xtype: 'chart',
    			            animate: true,
    			            id: 'classi_giacenze_chart_ciclo_vita',
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_ciclo_di_vita',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    								
    							},
    		
    							fields: ['name', 'des_name', 'referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			            	field: 'des_name',
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('des_name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'des_name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial',
    								renderer: function (label){
    									// this will change the text displayed on the pie
    									var cmp = Ext.getCmp('classi_giacenze_chart_ciclo_vita'); // id of the chart
    									var index = cmp.store.findExact('des_name', label); // the field containing the current label
    									var data = cmp.store.getAt(index).data;
    									return data.name; // the field containing the label to display on the chart
    								}			                    
    			                }
    			            }]
    			        }	  			
    	  			}
                        <!-- ---------------------------------- -->
                        <!-- grafico per tipo approvigionamento -->
                        <!-- ---------------------------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Tipo approvvigionamento',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_tipo_approvigionamento',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_tipo_approvvigionamento&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			}   <!-- ---------------------------------- -->
                        <!-- grafico per tipo approvigionamento -->
                        <!-- ---------------------------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Tipo Parte',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_tipo_parte',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_tipo_parte&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			         /*   legend: {
    			                position: 'right'
    			            },*/
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end tipo parte -->
				] <!-- end items tab panel  -->
			   } <!-- end tab panel  -->
				  , {
					 xtype:  'tabpanel',
					 height: 300,
					 items: [
					    <!-- ------------------------- -->
                        <!-- grafico per tipo riordino -->
                        <!-- ------------------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Tipo riordino',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_tipo_riordino',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_tipo_riordino&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end tipo riordino -->
                        <!-- ----------------- -->
                        <!-- utente immissione -->
                        <!-- ----------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Utente immissione',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_utente_immissione',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_utente_immissione&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end utente immissione -->				
                        <!-- --------- -->
                        <!-- Fornitore -->
                        <!-- --------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Fornitore',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_fornitore',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_fornitore&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			          
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			               /* label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }*/
    			            }]
    			        }	 	  			
    	  			} <!-- end utente fornitore -->				
					 
				] <!-- end items tab panel  -->
			   } <!-- end tab panel  -->
			
			]
		}				
			
			]
        }
      
	]
}