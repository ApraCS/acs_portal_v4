<?php

require_once("../../config.inc.php");
$desk_art = new DeskArt();
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'PUFD',
    'title_grid' => 'PUFD',
    'fields' => array(
        'TAID'   => array('label'	=> 'taid', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 40, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TACOR2'   => array('label'	=> 'tacor2', 'hidden' => 'true'),
        'rangeH'     => array('label'	=> 'Range H', 'c_width' => 70, 'only_view' => 'C'),
        'rangeL'     => array('label'	=> 'Range L', 'c_width' => 70, 'only_view' => 'C'),
        'rangeH2'     => array('label'	=> 'Range H2', 'c_width' => 70, 'only_view' => 'C'),
        'selez1'     => array('label'	=> 'Selez.1', 'c_width' => 70, 'only_view' => 'C'),
        'selez2'     => array('label'	=> 'Selez.2', 'c_width' => 70, 'only_view' => 'C'),
        'selez3'     => array('label'	=> 'Selez.3', 'c_width' => 70, 'only_view' => 'C'),
        //'vlhd1'     => array('label'	=> 'Val. range H da', 'maxLength' => 3, 'only_view' => 'F'),
        'vlhd'     => array('label'	=> 'Val. range H da', 'maxLength' => 4, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 240, 'labelWidth' => 120),
        'vlha'     => array('label'	=> 'Range H a', 'maxLength' => 4, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 175, 'labelWidth' => 80, 'close' => 'Y'),
        'vlld'     => array('label'	=> 'Val. range L da', 'maxLength' => 4, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'left', 'width' => 240, 'labelWidth' => 120),
        'vlla'     => array('label'	=> 'Range L a', 'maxLength' => 4, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'right', 'width' => 175, 'labelWidth' => 80, 'close' => 'Y'),
        'vl2d'     => array('label'	=> 'Val. range H2 da', 'maxLength' => 4, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'left', 'width' => 240, 'labelWidth' => 120),
        'vl2a'     => array('label'	=> 'Range H2 a', 'maxLength' => 4, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'right', 'width' => 175, 'labelWidth' => 80, 'close' => 'Y'),
        'vlsd'     => array('label'	=> 'Val. sel. art. da', 'xtype' => 'combo_tipo', 'only_view' => 'F','tab_sys' => 'PUVN', 'tacor2' => $m_params->var_rif1),
        'vlsa'     => array('label'	=> 'Val. sel. art. a', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUVN', 'tacor2' => $m_params->var_rif1),
       /* 'elvq1'     => array('label'	=> 'Scontri ferram. 1', 'maxLength' => 4, 'only_view' => 'F'),
        'elvq2'     => array('label'	=> 'Scontri ferram. 2', 'maxLength' => 4, 'only_view' => 'F'),
        'elvq3'     => array('label'	=> 'Scontri ferram. 3', 'maxLength' => 4, 'only_view' => 'F'),
        'elvq4'     => array('label'	=> 'Scontri ferram. 4', 'maxLength' => 4, 'only_view' => 'F'),
        'elvq5'     => array('label'	=> 'Scontri ferram. 5', 'maxLength' => 4, 'only_view' => 'F'),
        'elvq6'     => array('label'	=> 'Scontri ferram. 6', 'maxLength' => 4, 'only_view' => 'F'),*/
        'vnsc'     => array('label'	=> 'Variante art. scheda', 'only_view' => 'F',  'maxLength' => 3),
        'coaf'     => array('label'	=> 'Cod. art. fornit.', 'xtype' => 'articolo', 'only_view' => 'F'),
        'vs2d'     => array('label'	=> 'Val. sel. art. 2 da', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUVN', 'tacor2' => $m_params->var_rif2),
        'vs2a'     => array('label'	=> 'Val. sel. art. 2 a', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUVN', 'tacor2' => $m_params->var_rif2),
        'vs3d'     => array('label'	=> 'Val. sel. art. 3 da', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUVN', 'tacor2' => $m_params->var_rif3),
        'vs3a'     => array('label'	=> 'Val. sel. art. 3 a', 'xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUVN', 'tacor2' => $m_params->var_rif3),
        //'TADES2'    => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TAREST'    => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'    => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli riga scheda',
    'tab_tipo' => 'PUVR',
    //'title_tab' => 'Altro',
    
    'TAREST' => array(
       /* 'vlhd1' 	=> array(
            "start" => 0,
            "len"   => 3,
            "riempi_con" => ""
        ),*/
        'vlhd' 	=> array(
            "start" => 3,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vlha' 	=> array(
            "start" => 7,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vlld' 	=> array(
            "start" => 11,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vlla' 	=> array(
            "start" => 15,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vl2d' 	=> array(
            "start" => 19,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vl2a' 	=> array(
            "start" => 23,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vlsd' 	=> array(
            "start" => 27,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vlsa' 	=> array(
            "start" => 30,
            "len"   => 3,
            "riempi_con" => ""
        ),
      /*  'elvq1' 	=> array(
            "start" => 33,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'elvq2' 	=> array(
            "start" => 37,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'elvq3' 	=> array(
            "start" => 41,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'elvq4' 	=> array(
            "start" => 45,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'elvq5' 	=> array(
            "start" => 49,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'elvq6' 	=> array(
            "start" => 53,
            "len"   => 4,
            "riempi_con" => ""
        ),*/
        'vnsc' 	=> array(
            "start" => 57,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'coaf' 	=> array(
            "start" => 60,
            "len"   => 15,
            "riempi_con" => ""
        ),
        'vs2d' 	=> array(
            "start" => 75,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vs2a' 	=> array(
            "start" => 78,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vs3d' 	=> array(
            "start" => 81,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vs3a' 	=> array(
            "start" => 84,
            "len"   => 3,
            "riempi_con" => ""
        ),
    
    )

    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

