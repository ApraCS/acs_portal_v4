<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'PUOP',
    'title_grid' => 'PUOP-Operazioni',
    'fields' => array(
        'TAID'   => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'   => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'   => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2' => array('label'	=> 'Note', 'maxLength' => 30),
        'pesi'   => array('label'	=> 'Pesi DB', 'c_width' => 50,  'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
        'tipo_pr'   => array('label'	=> 'Tipologia'),
        'RRN'    => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'l_ini'  => array('label'	=> 'Scheda iniz.oper.1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUFI'),
        'l_fin'  => array('label'	=> 'Scheda fin.oper.1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUFI'),
        'var1'   => array('label'	=> 'var1', 'hidden' => 'true'),
        'var2'   => array('label'	=> 'var2', 'hidden' => 'true'),
        'var3'   => array('label'	=> 'var3', 'hidden' => 'true'),
        'var4'   => array('label'	=> 'var4', 'hidden' => 'true'),
        'var5'   => array('label'	=> 'var5', 'hidden' => 'true'),
        'var6'   => array('label'	=> 'var6', 'hidden' => 'true'),
        'var7'   => array('label'	=> 'var7', 'hidden' => 'true'),
        'var8'   => array('label'	=> 'var8', 'hidden' => 'true'),
        'var9'   => array('label'	=> 'var9', 'hidden' => 'true'),
        'var10'   => array('label'	=> 'var10', 'hidden' => 'true'),
        'var11'   => array('label'	=> 'var11', 'hidden' => 'true'),
        'var12'   => array('label'	=> 'var12', 'hidden' => 'true'),
        'var13'   => array('label'	=> 'var13', 'hidden' => 'true'),
        'var14'   => array('label'	=> 'var14', 'hidden' => 'true'),
        'var15'   => array('label'	=> 'var15', 'hidden' => 'true'),
        'var16'   => array('label'	=> 'var16', 'hidden' => 'true'),
        'var17'   => array('label'	=> 'var17', 'hidden' => 'true'),
        'var18'   => array('label'	=> 'var18', 'hidden' => 'true'),
        'var19'   => array('label'	=> 'var19', 'hidden' => 'true'),
        'var20'   => array('label'	=> 'var20', 'hidden' => 'true'),
        'config'  => array('label'	=> 'Configuratore','xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUMO'),
        'var_std'  => array('label'	=> 'Variabile standard','xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo1'  => array('label'	=> 'Tipologia variabile 1','xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUTI'),
        'tipo2'  => array('label'	=> 'Tipologia variabile 2','xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUTI'),
        'tipo3'  => array('label'	=> 'Tipologia variabile 3','xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_sys' => 'PUTI'),
        'posiz'  => array('label'	=> 'Ind. var. posizion.', 'only_view' => 'F', 'maxLength' => 2, 'c_num' => 'Y'),
        'gr_rott'  => array('label'	=> 'Gruppo rottura lista', 'only_view' => 'F', 'maxLength' => 3),
        'TAREST' => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE' => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE' => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM' => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM' => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Gruppi omogenei programmazione produzione',
    'title_tab' => 'Variabili',
    'tab_tipo' => 'PUVR',
    'TAREST' => array(
        'l_ini' 	=> array(
            "start" => 34,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'l_fin' 	=> array(
            "start" => 37,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var1' 	=> array(
            "start" => 40,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var2' 	=> array(
            "start" => 43,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var3' 	=> array(
            "start" => 46,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var4' 	=> array(
            "start" => 49,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var5' 	=> array(
            "start" => 52,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var6' 	=> array(
            "start" => 55,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var7' 	=> array(
            "start" => 58,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var8' 	=> array(
            "start" => 61,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var9' 	=> array(
            "start" => 64,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var10' 	=> array(
            "start" => 67,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var11'	=> array(
            "start" => 70,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var12' => array(
            "start" => 73,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var13' 	=> array(
            "start" => 76,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var14' 	=> array(
            "start" => 79,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var15' 	=> array(
            "start" => 82,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var16' 	=> array(
            "start" => 85,
            "len"   => 3,
            "riempi_con" => ""
        ), 
        'var17' 	=> array(
            "start" => 88,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'var18' 	=> array(
            "start" => 91,
            "len"   => 3,
            "riempi_con" => ""
        )
        , 'var19' 	=> array(
            "start" => 94,
            "len"   => 3,
            "riempi_con" => ""
        )
        , 'var20' 	=> array(
            "start" => 97,
            "len"   => 3,
            "riempi_con" => ""
        ) 
        ,'config' 	=> array(
            "start" => 100,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'var_std' 	=> array(
            "start" => 103,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'tipo1' 	=> array(
            "start" => 106,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'tipo2' 	=> array(
            "start" => 109,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'tipo3' 	=> array(
            "start" => 112,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'posiz' 	=> array(
            "start" => 115,
            "len"   => 2,
            "riempi_con" => ""
        )
        ,'gr_rott' 	=> array(
            "start" => 118,
            "len"   => 3,
            "riempi_con" => ""
        ), 
        'pesi' 	=> array(
            "start" => 121,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'tipo_pr' 	=> array(
            "start" => 140,
            "len"   => 15,
            "riempi_con" => ""
        )
        
      
        
    )
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

