<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'grid_tab_risposte'){
?>
{"success":true, "items": [
						{
						xtype: 'grid',
						features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],	
				   		store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_tab_risposte', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        extraParams: <?php echo acs_je($m_params) ?>,
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: [ 'id', 'text']							
									
			}, //store
				

			      columns: [
			       {
	                header   : 'Codice',
	                dataIndex: 'id',
	                width : 100,
	                filter: {type: 'string'}, filterable: true
	                },
			      	{
	                header   : 'Descrizione',
	                dataIndex: 'text',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                }
	              ]
	              
	        , listeners: {
	         	celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
	         		var rec = iView.getRecord(iRowEl);
	         		var m_win = this.up('window');
	         		m_win.fireEvent('afterSelected', m_win, rec.data);
	         	}
	         }  

	      } //
	 ]
}
<?php
    exit;
}







//caricamento risposte nei combo proprieta
if ($_REQUEST['fn'] == 'get_tab_risposte'){
    
    if($m_params->codice == 'Y')
        $mostra_codice = 'Y';
    else 
        $mostra_codice = 'N';
        
    
    $ar_risposte = $desk_art->find_TA_std(trim($m_params->tab), null, 'N', 'N', null, null, null, 'N', $mostra_codice);
   
    echo acs_je($ar_risposte);
    exit;
}
    


if ($_REQUEST['fn'] == 'exe_save_risp'){
    
    $ret = array();
    $ar_ins = array();
    $ar_ins['AFVALO'] = $m_params->new_value;
    $ar_ins['AFUSGE'] 	= trim($auth->get_user());
    $ar_ins['AFDTGE']   = oggi_AS_date();
    
    if(!is_null($m_params->record->rrn) && strlen($m_params->record->rrn)> 0){
       
        $sql = "UPDATE {$cfg_mod_DeskArt['file_proprieta']} PROP
                SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE RRN(PROP) = {$m_params->record->rrn}";
  
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
    
    }else{
        
        $ar_ins['AFDT']     = $id_ditta_default;
        $ar_ins['AFART']    = $m_params->c_art;
        $ar_ins['AFTDAT']   = $m_params->record->prop;    //proprieta (NO LISTA)   
        
        $sql = "INSERT INTO {$cfg_mod_DeskArt['file_proprieta']}
        (" . create_name_field_by_ar($ar_ins) . ")
            VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
       
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
        
        $sql_s = "SELECT RRN(AF) AS RRN 
                FROM {$cfg_mod_DeskArt['file_proprieta']}  AF
                WHERE AFDT = '{$id_ditta_default}' AND AFART = '{$m_params->c_art}' 
                AND ADTDAT = '{$m_params->record->prop}' AND AFVALO = '{$m_params->new_value}'";
     
        
        $stmt_s = db2_prepare($conn, $sql_s);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_s);
        echo db2_stmt_errormsg();
        $row = db2_fetch_assoc($stmt_s);
        $ret['rrn'] = $row['RRN'];
    }

    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'get_data_proprieta'){
    
    $ar = array();
  
    
    $sql = "SELECT TA_ARPRO.*, AFVALO, TA_RISP.TADESC AS D_RISPOSTA, RRN(AF) AS RRN
            FROM {$cfg_mod_DeskArt['file_tabelle']} TA_ARPRO
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_proprieta']} AF
              ON TA_ARPRO.TADT = AF.AFDT AND TA_ARPRO.TAKEY1 = AF.AFTDAT
              AND AF.AFART = '{$m_params->open_request->c_art}'
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_RISP
              ON TA_ARPRO.TADT = TA_RISP.TADT AND TA_ARPRO.TAASPE = TA_RISP.TATAID AND TA_RISP.TAKEY1 = AF.AFVALO
            WHERE TA_ARPRO.TADT = '{$id_ditta_default}' AND TA_ARPRO.TATAID = 'ARPRO'
            AND (TA_ARPRO.TAPESO < 900 OR AF.AFTDAT <> '')
            ORDER BY TA_ARPRO.TAPESO";

    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $i = 0;
    while ($row = db2_fetch_assoc($stmt)) {
      
        if(trim($row['AFVALO']) != ''){
            $risposta = "[".trim($row['AFVALO'])."] ".trim($row['D_RISPOSTA']);
        }else{
            $risposta = "";
        }
        
        $ret[] = array( 
            "num"  => $i++,
            "prop" 	=> trim($row['TAKEY1']),
            "desc"  => "[".trim($row['TAKEY1'])."] ".acs_u8e(trim($row['TADESC'])),
            "tab" 	=> trim($row['TAASPE']),
            "risposta" 	=> $risposta,
            "rrn" => $row['RRN'],
           
        );
    }
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc'){
    
    $m_params = acs_m_params_json_decode();
   
    $n= $m_params->row->num;
    
    $ar_ins = array();
    $ar_ins["AMVAR{$n}"] = "";
    $ar_ins["AMVAL{$n}"] = "";
    $ar_ins["AMTPV{$n}"] = "";
    $ar_ins["AMSWN{$n}"] = "";
   
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_variabili']} AM
    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
    WHERE AMDT = '{$id_ditta_default}' AND AMART = '{$m_params->c_art}'";  
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

if ($_REQUEST['fn'] == 'get_data_puvn'){
    
    $ar = array();
    $m_params = acs_m_params_json_decode();
    
    
    if(trim($m_params->tipologia) == 'T'){
      
        $sql = "SELECT *
        FROM {$cfg_mod_Admin['file_tab_sys']}
        WHERE TADT = '$id_ditta_default' AND TAID = 'PUTI' AND TALINV=''
        ORDER BY TANR, TADESC";
        
    }else{
        
        if(strlen($m_params->domanda)>0){
            $where .= " AND TACOR2 = '{$m_params->domanda}'";
        }else{
            $where .= " ";
        }
        
        
        $sql = "SELECT *
        FROM {$cfg_mod_Admin['file_tab_sys']}
        WHERE TADT = '$id_ditta_default' AND TAID = 'PUVN' AND TALINV=''
        {$where} ORDER BY TANR, TADESC";
        
        
        
    }
  
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $sospeso = false;
        $color = "";
        
        $text = "[" . trim($row['TANR']) . "] " . trim($row['TADESC']);
        
        if(trim($row['TATP']) == 'S'){
            $sospeso = true;
            $color = "#F9BFC1";
            
        }
        
        $ret[] = array( "id" 	=> trim($row['TANR']),
                        "text" 	=> acs_u8e($text),
                        "sosp"    => $sospeso,
                        "color"   => $color
        );
    }
    
    
    
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_mod_variante'){
    $ret = array();
    $m_params = acs_m_params_json_decode();
  
    
    $n = $m_params->row->num;
    $var_num = $m_params->row->var_num;
    
    $ar_ins = array();
    
    if(isset($m_params->form_values->f_variabile) && $m_params->form_values->f_variabile != '')
        $ar_ins["AMVAR{$n}"] = acs_toDb($m_params->form_values->f_variabile);
    if(isset($m_params->form_values->f_valore) && $m_params->form_values->f_valore != ''){
        if(isset($var_num) && $var_num == 'N'){
            if($m_params->form_values->f_valore > 0)
                $ar_ins["AMVAL{$n}"] = $m_params->form_values->f_valore;
            else
                $ar_ins["AMVAL{$n}"] = abs($m_params->form_values->f_valore)."-";
        }else{
          $ar_ins["AMVAL{$n}"] = acs_toDb($m_params->form_values->f_valore);
        }
    }   
    if(isset($m_params->form_values->f_tipo) && $m_params->form_values->f_tipo != '')
        $ar_ins["AMTPV{$n}"] = acs_toDb($m_params->form_values->f_tipo);
    
    $sql_c = "SELECT COUNT(*) AS C_ROW 
    FROM {$cfg_mod_DeskUtility['file_variabili']} AM
    WHERE AMDT = '{$id_ditta_default}' AND AMART = '{$m_params->c_art}'";
    
    
    $stmt_c = db2_prepare($conn, $sql_c);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_c);
    $row_c = db2_fetch_assoc($stmt_c);
    
    if($row_c['C_ROW'] == 0){
        
        $ar_insert["AMDT"] = $id_ditta_default;
        $ar_insert["AMART"] = $m_params->c_art;
        
        $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_variabili']}
        (" . create_name_field_by_ar($ar_insert) . ") 
         VALUES (" . create_parameters_point_by_ar($ar_insert) . ")";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_insert);
        echo db2_stmt_errormsg($stmt);
    }

    $sql = "UPDATE {$cfg_mod_DeskUtility['file_variabili']} AM
    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
    WHERE AMDT = '{$id_ditta_default}' AND AMART = '{$m_params->c_art}'";
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}



if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    
    $c_art = $m_params->open_request->c_art;
    
    $sql_c = "SELECT COUNT(*) AS C_ROW
    FROM {$cfg_mod_DeskUtility['file_variabili']} AM
    WHERE AMDT = '{$id_ditta_default}' AND AMART = '{$c_art}'";
    
    
    $stmt_c = db2_prepare($conn, $sql_c);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_c);
    $row_c = db2_fetch_assoc($stmt_c);
    
    if($row_c['C_ROW'] == 0){
        
        for($i = 1 ; $i <= 10; $i++){
            
            $nr[]=array(
                'num'=> $i,
                'variabile'=>"",
                'var_cod' => "",
                'val_ass'=> "",
                'val_cod' => "",
                'tipo'=> "",
                'var_num'=>"",
                
            );
        }
        
    }else{
        
        $sql = "SELECT AM.*
        FROM {$cfg_mod_DeskUtility['file_variabili']} AM
        WHERE AMDT = '{$id_ditta_default}' AND AMART = '{$c_art}'";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        while($row = db2_fetch_assoc($stmt)){
            $nr = array();
            
            for($i = 1 ; $i <= 10; $i++){
                
                
                $val_dom = find_TA_sys('PUVR', trim($row["AMVAR{$i}"]), null, null, null, null,  0, '',  'N', 'Y');
                if(trim($row["AMVAR{$i}"]) != '')
                    $value_D = "[".trim($row["AMVAR{$i}"])."] ".$val_dom[0]['text'];
                else
                    $value_D = "";
                        
                if($row["AMSWN{$i}"] == 'N'){
                    $value_R =  $row["AMVAL{$i}"];
                }else{
                    $val_risp = find_TA_sys('PUVN', $row["AMVAL{$i}"], null, trim($row["AMVAR{$i}"]), null, null,  0, '',  'N', 'Y');
                    if(trim($row["AMVAL{$i}"]) != '')
                        $value_R = "[".trim($row["AMVAL{$i}"])."] ".$val_risp[0]['text'];
                        else
                            $value_R = "";
                }
                        
                $nr[]=array(
                    'num'=> $i,
                    'variabile'=>$value_D,
                    'var_cod' => $row["AMVAR{$i}"],
                    'val_ass'=> $value_R,
                    'val_cod' => $row["AMVAL{$i}"],
                    'tipo'=> $row["AMTPV{$i}"],
                    'var_num'=>$row["AMSWN{$i}"],
                    
                    
                );
            }
            
        }
        
        
        
        
    }

    
    echo acs_je($nr);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
				xtype: 'form', width: '100%',
				layout: {type: 'vbox', pack: 'start', align: 'stretch'},	
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
				items: [
						{
						xtype: 'grid',
						title: '',
				        loadMask: true,	
				   		store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['num', 'variabile', 'val_ass', 'tipo', 'var_num', 'val_cod', 'var_cod', 'rrn']							
									
			}, //store
				

			      columns: [
			      {
	                header   : '',
	                dataIndex: 'num',
	                width : 30
	                },	
			      {
	                header   : 'Variabile',
	                dataIndex: 'variabile',
	                 flex: 1
	                },
	                {
	                header   : 'Valore assunto',
	                dataIndex: 'val_ass',
	                flex: 1
	                },
	                {
	                header   : 'Tipo',
	                dataIndex: 'tipo',
	                width: 50
	                },
	                
	         ], listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					 	 voci_menu.push({
			         		text: 'Cancella',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				c_art : <?php echo j($m_params->c_art); ?>
										},							        
								        success : function(result, request){
					            			grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    		});	
			    	
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	         
	           , celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
    						col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
    					  	rec = iView.getRecord(iRowEl);
    					  	var grid = this;	
    					  	
    					  	
    					  <?php for($i = 1 ; $i <= 10; $i++){ ?>
    					  	 
    					  	
    					  	if(rec.get('num') == <?php echo $i; ?>){
    					  	
    					  	var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    grid.getStore().proxy.extraParams.open_request.new = '';
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					  	 acs_show_win_std('Modifica variante', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								row: rec.data, c_art : '<?php echo $m_params->c_art; ?>'
							}, 400, 200, my_listeners, 'icon-sticker_blue-16');
    					  	
    					  	}
    					  	
    					 <?php }?>						  
							 
						  }
			   		  }
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}, {
				xtype: 'grid',
				title: '',
				flex:0.7,
				autoScroll : true,
		        loadMask: true,	
		          plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		            listeners:{
		            
		            	beforeedit: function(cellEditor){
		            		console.log('beforeedit');
		            		console.log(cellEditor);
		            	},
		            
			        	afteredit: function(cellEditor, context, eOpts){
				        	var value = context.value;
				        	var grid = context.grid;
				        	var record = context.record;			        	
			        	   	Ext.Ajax.request({
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_risp',
					            method: 'POST',
			        		    jsonData: {
					      			   record : record.data,
									   c_art : <?php echo j($m_params->c_art); ?>
									   				      			  
					      			   }, 					            
					            success: function ( result, request) {
					                var jsonData = Ext.decode(result.responseText);
					                record.set('stato_MTO', value);
					   						                															
					            },
					            failure: function (result, request) {
					            }
					        });
					        ////cellEditor.destroy();
			        	}
					 }
		          })
		      ],
		      
		       tbar: new Ext.Toolbar({
                items:[ 
                '<b>ProprietÓ/Caratteristiche articolo</b>', '->',
                   {
        		 iconCls: 'icon-F8-m-16',
        		 style  : 'border: 1px solid gray;',
	             text: 'Import', 
	             handler: function(event, toolEl, panel){
	             
	             	acs_show_win_std(null, 'acs_import_articolo.php?fn=open_grid', {articolo : <?php echo j($m_params->c_art); ?>}, 700, 500, {}, 'icon-F8-m-16');
	             }
	             },
                {
        		 iconCls: 'icon-sub_blue_add-16',
        		 style  : 'border: 1px solid gray;',
	             text: 'Aggiungi', 
		           		handler: function(event, toolEl, panel){
		           		   
		           		   var grid = this.up('grid');
		           		   var my_listeners = {
                    	  			afterSelected: function(from_win, value){
                    	  			var add_row = true;
                    	  			rows =   grid.getStore().getRange();
                    	  			Ext.each(rows, function(row) {
                    	  			     if(row.get('prop') == value.id){
                    	  			         add_row = false;
		                    			 }else{
		                    			     add_row = true;
		                    			 }
	                    			});	
                    	  			
                    	  			if(add_row == true){
                    	  				    grid.getStore().add({
            									desc: value.text,
            									tab : value.TAASPE,
            									prop : value.id
        									});
    								}else{
        								acs_show_msg_error('Propriet&agrave; gi&agrave; inserita');
    								}	
                    	  			    from_win.close();
                    	        		}
                    				};
		           		
		           		 	acs_show_win_std('Propriet&agrave; aggiuntive', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_prop', {}, 500, 300, my_listeners, 'icon-sub_blue_add-16');
		           		
		           		 }
		           	 }
	         ]            
	        }),	
		   		store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_proprieta', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['num', 'prop', 'desc', 'tab', 'risposta', 'rrn']							
									
			}, //store

			      columns: [
			      
			      {
	                header   : 'ProprietÓ',
	                dataIndex: 'desc',
	                 flex: 1,
	                },
	                {
	                 header   : 'Valore assunto',
	                 dataIndex: 'risposta',
	                 flex: 1,
	                 
	                 setEditor: function(){
	                 	console.log('setEditor');
	                 },
	                 
	                 getEditor: function (record) {
	                 	var grid = this.up('grid');
	                 	var m_prop_id = record.get('prop');
	                 	var m_from_var = record.get('tab');
	                 	console.log('tab:::: ' + m_from_var);
	                 	
	                 		acs_show_win_std('Elenco opzioni ' + record.get('desc')
                                   , '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_tab_risposte'
                                   , {tab: m_from_var}, null, null, {
                                   	afterSelected: function(from_win, new_value){
                                   	       if(!Ext.isEmpty(new_value)){
                                   	             new_value_id = new_value.id;
                                   	             var risposta = '[' +  new_value.id + '] ' + new_value.text;
                                   	       }else{
                                   	             new_value_id = '';
                                   	             var risposta = '';
                                   	       }
                                   	
                                   			Ext.Ajax.request({
                					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_risp',
                					            method: 'POST',
                			        		    jsonData: {
                					      			   record : record.data,
                									   c_art : <?php echo j($m_params->c_art); ?>,
                									   new_value: new_value_id				      			  
                					      			   }, 					            
                					            success: function (result, request) {
                					                var jsonData = Ext.decode(result.responseText);
                					                from_win.destroy();
                					             	record.set('risposta', risposta);
                					   				record.commit('rrn', jsonData.rrn);          															
                					            },
                					            failure: function (result, request) {
                					            }
                					        });
                                   	}
                                   }, 'icon-sticker_blue-16');
                            return false;
	                 	
	                 	
	                 	
                            var m = Ext.create('Ext.form.field.ComboBox', {
                                store: {
                                	autoLoad: false,
									fields: [{name:'id'}, {name:'text'}],
								        
    	  							proxy: {
    	  									
        								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_tab_risposte', 
        								   method: 'POST',								
        								   type: 'ajax',
        
        							       actionMethods: {
        							          read: 'POST'
        							        },
        							        xxxextraParams: {
        							        	 prop_id:  m_prop_id,
        										 from_var: m_from_var
        			        				},
        			        				
        			        				doRequest: personalizza_extraParams_to_jsonData, 
        						
        								   reader: {
        						            type: 'json',
        									method: 'POST',						            
        						            root: 'root'						            
        						        }
        							}, 
								},
                                
                                displayField: 'text',
                                queryMode: 'local',
                                typeAhead: true,
                                
                                listeners: {
                                	aaafocus: function(c){
                                		console.log('on focus!!!!');
                                		console.log(this);
                                		c.store.proxy.extraParams.from_var = m_from_var;
                                		c.store.load();
                                	}
                                }
                            });
                            console.log(record.get('tab'));
                            m.store.loadData([{id: record.get('tab'), text: record.get('tab')}]);
                            return m;
                            
                        }
	                },
	               
	                { //AZZERA
    		            xtype:'actioncolumn', width:35,
    		            text: '<img src=<?php echo img_path("icone/16x16/sub_red_delete.png") ?> width=18>',
    		            tdCls: 'tdAction', tooltip: 'Azzera',
    		            menuDisabled: true, sortable: false,
    		            items: [{
    		                icon: <?php echo img_path("icone/16x16/sub_red_delete.png") ?>,
    		                handler: function(view, rowIndex, colIndex, item, e, record, row) {
    		                   
                               Ext.Ajax.request({
    					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_risp',
    					            method: 'POST',
    			        		    jsonData: {
    					      			   record : record.data,
    									   c_art : <?php echo j($m_params->c_art); ?>,
    									   new_value: ''				      			  
    					      			   }, 					            
    					            success: function ( result, request) {
    					                record.set('risposta', '');
    					   				record.commit();          															
    					            },
    					            failure: function (result, request) {
    					            }
    					        });
    		                
    		                },
    		                 isDisabled: function(view, rowIndex, colIndex, item, record) {
                              if (record.get('risposta') == '')
                                return true;
            				},
            				getClass: function(value,metadata,record){
                				 if(record.get('risposta') == '')
                				    return 'x-hide-display';    
                                  
                        	}
    		            }]
    		          }
	              
	                
	         ], 
				 
				  viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}


if ($_REQUEST['fn'] == 'open_mod'){
    $m_params = acs_m_params_json_decode();
    
  
    $n = $m_params->row->num;
    $var = trim($m_params->row->var_cod);
    $val = trim($m_params->row->val_cod);
    $val_d = trim($m_params->row->val_ass);
    $tipo = trim($m_params->row->tipo);
    $var_num = trim($m_params->row->var_num);
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            
	            items: [
	          
						 {
								name: 'f_variabile',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'Variabile',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								queryMode: 'local',
								minChars: 1,							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
							     tpl: [
                                    '<ul class="x-list-plain">',
                                    '<tpl for=".">',
                                    '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                    '</tpl>',
                                    '</ul>'
                                ],
							    value : <?php echo j($var); ?>,
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, {name : 'color'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y', 'Y'), ''); ?>	
								    ]
								}, listeners: {
                                	change: function(field,newVal) {	
                                  		if (!Ext.isEmpty(newVal)){
                                        	combo_risp = this.up('form').down('#c_risp');                      		 
                                         	combo_risp.store.proxy.extraParams.domanda = newVal;
                                        	combo_risp.store.load();                             
                                         }
                                         
            
                                        }, beforequery: function (record) {
                                        record.query = new RegExp(record.query, 'i');
                                        record.forceAll = true;
                                    }
                           }
								
						   } 
						   <?php if($var_num == 'N'){?>
						   
						   ,{xtype: 'numberfield',
    						name: 'f_valore',
    						fieldLabel: 'Valore',
    						anchor: '-15',	
    						hideTrigger : true,
    						decimalPrecision: 3,
    						value:  <?php echo j($val); ?>,
    						}
						   
						   <?php }else{?>
						   
						 ,{
								name: 'f_valore',
								xtype: 'combo',
								flex: 1,
								itemId: 'c_risp',
								fieldLabel: 'Valore',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
								queryMode: 'local',
								minChars: 1,	
						   		//allowBlank: false,								
							    anchor: '-15',
							    value : <?php echo j($val); ?>,
							    tpl: [
                                    '<ul class="x-list-plain">',
                                    '<tpl for=".">',
                                    '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                    '</tpl>',
                                    '</ul>'
                                ],
                                 displayTpl: Ext.create('Ext.XTemplate',
                                               '<tpl for=".">',
                    
                    	                            '{text}',
                    
                    	                        '</tpl>'
                                         
                                        ),
                    	 <?php if(trim($var) != ''){?>		
							   store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, {name:'color'}, {name:'sosp'}],
								    data: [		
								    <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $var, null, null, 0, '', 'Y', 'Y'), '');  ?>
								   
								    ]
								}
								<?php }else{?>
								
								store: {
									        autoLoad: true,
											proxy: {
									            type: 'ajax',
									            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_puvn',
									            actionMethods: {
            							          read: 'POST'
           							        },
							                	extraParams: {
							    		    		domanda: '',
							    				},				            
									            doRequest: personalizza_extraParams_to_jsonData, 
									            reader: {
               						            type: 'json',
               									method: 'POST',						            
                						            root: 'root'						            
                						        }
									        },       
											fields: [{name:'id'}, {name:'text'}, {name:'color'}, {name:'sosp'}],		             	
							            }
								
								<?php }?>
								
								, listeners: {
                                	    beforequery: function (record) {
                                        record.query = new RegExp(record.query, 'i');
                                        record.forceAll = true;
                                    },
                                    select: function(field, selected) {
                                        if(selected[0].data.sosp === true)
                                           field.setValue('');
                    				  }
                           }
						   }
						   
						   <?php }?>
						   ,{
								name: 'f_tipo',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'Tipo',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
							    value : <?php echo j($tipo); ?>,
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'CA', text: '[CA] Variante configurazione riga'},
								     	 {id: 'SC', text: '[SC] Sostituzione/modifica componente'},
								     	 {id: 'ST', text: '[ST] Attributo per stampe'}
								     	
								    ]
								}
								
						   }
	 	
	 	
	           
	          		
	            ],
	            
				buttons: [					
					{
			            text: 'Salva',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			                Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_variante',
						        jsonData: {
						        	form_values : form.getValues(),
						        	row : <?php echo acs_je($m_params->row) ?>,
						        	c_art : '<?php echo $m_params->c_art ?>'
						        	
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						          var jsonData = Ext.decode(result.responseText);
						          loc_win.fireEvent('afterOkSave', loc_win, jsonData);		            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	
	
if($_REQUEST['fn'] == 'open_prop'){ ?>

{
		success:true,
		items : [
		  {
    		xtype: 'grid',
        	multiSelect: true,
        	autoScroll: true,
        	features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}],
        	store: {
    			editable: false,
    			autoDestroy: true,
    			fields: [{name:'id'}, {name:'text'}, 'TAASPE'],
    		    data: [								    
	     	<?php echo acs_ar_to_select_json($desk_art->find_TA_std('ARPRO', null, 'N', 'Y', null, null, null, 'N', 'Y', " AND TAPESO >= 900 "), ''); ?>	
    		    ]
			}, //store
    		columns: [
			      	/*{
	                header   : 'Codice',
	                dataIndex: 'id',
	                width : 80
	                },*/
	                {
	                header   : 'Propriet&agrave;',
	                dataIndex: 'text',
	                flex : 1
	                }
	                
	         ],
			listeners: {
			  	 celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
	         		var rec = iView.getRecord(iRowEl);
	         		var m_win = this.up('window');
	         		m_win.fireEvent('afterSelected', m_win, rec.data);
	         	}
			
	         }

			 ,viewConfig: {
	         getRowClass: function(record, index) {
	         //return ret;																
	         }   
	    },
			    
	        
        } //grid
		
		]	
		}    

<?php 
}	