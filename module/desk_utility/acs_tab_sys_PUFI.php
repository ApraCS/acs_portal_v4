<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
$pufd = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";



$m_table_config = array(
    'TAID' => 'PUFI',
    'title_grid' => 'PUFI-Scheda ferramenta',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TACINT'   => array('label'	=> 'Interfaccia', 'only_view' => 'F',  'maxLength' => 10),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'var_range'  => array('label'	=> 'Variab.range', 'only_view' => 'C', 'c_width' => 100),
        'var_sel'    => array('label'	=> 'Variab.selez.', 'only_view' => 'C','c_width' => 100),
        'vrrh'     => array('label'	=> 'Variab. range H', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'corh'     => array('label'	=> 'Correz. range H', 'maxLength' => 4, 'only_view' => 'F'),
        'vrrl'     => array('label'	=> 'Variab. range L', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'corl'     => array('label'	=> 'Correz. range L', 'maxLength' => 4, 'only_view' => 'F'),
        'vrh2'     => array('label'	=> 'Variab. range H2', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'coh2'     => array('label'	=> 'Correz. range H2', 'maxLength' => 4, 'only_view' => 'F'),
        'vras'     => array('label'	=> 'Variab. selez. art 1', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
     //   'vrsc'     => array('label'	=> 'Var. art. scheda', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
     //   'vrvd'     => array('label'	=> 'Variab. vers. dett', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'vrs2'     => array('label'	=> 'Variab. selez. art 2', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'vrs3'     => array('label'	=> 'Variab. selez. art 3', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TRAD'     => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'PUFD'     => array('label'	=> $pufd, 'only_view' => 'C', 'c_width' => 40, 'tooltip' => 'Scheda ferramenta - Dettaglio'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella scheda ferramenta',
    'tab_tipo' => 'PUVR',
    'j_pufd' => 'Y',
    'TAREST' => array(
        'vrrh' 	=> array(
            "start" => 0,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'corh' 	=> array(
            "start" => 3,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vrrl' 	=> array(
            "start" => 7,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'corl' 	=> array(
            "start" => 10,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vrh2' 	=> array(
            "start" => 14,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'coh2' 	=> array(
            "start" => 17,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vras' 	=> array(
            "start" => 21,
            "len"   => 3,
            "riempi_con" => ""
        ),
      /*  'elqf1' 	=> array(
            "start" => 24,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'elqf2' 	=> array(
            "start" => 27,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'elqf3' 	=> array(
            "start" => 30,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'elqf4' 	=> array(
            "start" => 33,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'elqf5' 	=> array(
            "start" => 36,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'elqf6' 	=> array(
            "start" => 39,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vrsc' 	=> array(
            "start" => 42,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vrvd' 	=> array(
            "start" => 45,
            "len"   => 3,
            "riempi_con" => ""
        ),*/
        'vrs2' 	=> array(
            "start" => 48,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vrs3' 	=> array(
            "start" => 51,
            "len"   => 3,
            "riempi_con" => ""
        ),
    )
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';
