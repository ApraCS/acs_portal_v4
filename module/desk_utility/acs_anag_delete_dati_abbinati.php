<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql_sx = "DELETE FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA 
               WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUSX' 
               AND TANR = '{$m_params->codice}'";
    
    $stmt_sx = db2_prepare($conn, $sql_sx);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_sx);
    
    $sql_sq = "DELETE FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA 
               WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUSQ' 
               AND TACOR2 = '{$m_params->codice}'";
   
    $stmt_sq = db2_prepare($conn, $sql_sq);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_sq);
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
               WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUMO'
               AND TANR = '{$m_params->codice}'";
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}
        
if ($_REQUEST['fn'] == 'open_tab'){
    
    $row = get_TA_sys('PUSX', trim($m_params->codice));
    if($row != false && count($row) > 0)
       $pusx = "PUSX [".trim($row['id'])."] ".trim($row['text']);
   
        
    ?>
      {"success":true, "items": [
        {
           xtype: 'form',
            border: false,
            frame : true,
            bodyStyle: {
                padding: '10px',
                align: 'stretch'
            },
            items: [
            
            <?php if($row != false && count($row) > 0){?>
            
           /* {
				xtype: 'checkboxgroup',
				fieldLabel: <?php echo j($pusx); ?>,
			    labelWidth : 300,
			    items: [{
                    xtype: 'checkbox'
                  , name: 'f_pusx' 
                  , boxLabel: ''
                  , inputValue: 'Y'
                }]							
			},*/
			
			 {
				xtype: 'displayfield',
				fieldLabel: <?php echo j($pusx); ?>,
			    labelWidth : 3550,
			    labelSeparator : ''
			   						
			},
            
            <?php }?>
            
            <?php 
            
            $row_q = find_TA_sys('PUSQ', null, null, trim($m_params->codice));
              if($row_q != false && count($row_q) > 0){
                foreach ($row_q as $v){
                  $pusq = "PUSQ [".trim($v['id'])."] ".trim($v['text']);
            
            ?>
            
             /*{
				xtype: 'checkboxgroup',
				fieldLabel: <?php echo j($pusq); ?>,
				labelWidth : 250,
			    items: [{
                    xtype: 'checkbox'
                  , name: '<?php echo $v['id']; ?>' 
                  , boxLabel: ''
                  , inputValue: 'Y'
                }]							
			},*/
			
			 {
				xtype: 'displayfield',
				fieldLabel: <?php echo j($pusq); ?>,
			    labelWidth : 350,
			    name : <?php echo j(trim($v['id'])); ?>,
			    labelSeparator : ''
			   						
			},
            
            <?php }}?>
            
            ],
            dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                    '->'   
			   ,{
                    xtype: 'button',
                    text: 'Conferma',
		            scale: 'large',	                     
					iconCls: 'icon-button_blue_play-32',
		          	handler: function() {
		          	
	       			  var loc_win = this.up('window');
	       			 
	       			  Ext.Msg.confirm('Richiesta conferma', 'Confermi cancellazione definitiva righe tabelle PUSX/PUSQ abbinate?', function(btn, text){
        	   				if (btn == 'yes'){
	       			  
         			          Ext.Ajax.request({
        				        url        : 'acs_anag_delete_dati_abbinati.php?fn=exe_canc',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				codice : <?php echo j(trim($m_params->codice)); ?>
        						},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					        if (jsonData.success == true){
        						       loc_win.fireEvent('afterConferma', loc_win);     
        					        }
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				    }); 		 
				    
	       			 		
	       			   }
                	   				
    	   				});
		           
			
			            }

			     }
                ]
                
                }]
            
            
            
            }
            
            
            
            ]}
    
    <?php 
}
       
    
     