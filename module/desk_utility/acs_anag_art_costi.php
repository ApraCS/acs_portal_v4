<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility(array('abilita_su_modulo' => 'DESK_ACQ'));

$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_table_config = array(
    
    'FILLER' => array(
        
        'sigla' => array(
            "start" => 3,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'LIFLG2' => array(
            "start" => 4,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'f_imp' => array(
            "start" => 5,
            "len"   => 1,
            "riempi_con" => ""
        )
    )
);


function genera_filler($m_table_config, $values){
    
    $value_attuale = $values->filler;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-50s", "");
        
        //$new_value = "";
        foreach($m_table_config['FILLER'] as $k => $v){
            if(isset($values->$k)){
                $chars = $values->$k;
                $len = "%-{$v['len']}s";
                $chars = substr(sprintf($len, $chars), 0, $v['len']);
                $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                $value_attuale = $new_value;
                
            }
            
        }
        
        
        return $new_value;
}


// ******************************************************************************************
// EXE SAVE LISTINO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_listino'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
    
    $ar_upd['LIUSUM'] 	= SV2_db_user($auth->get_user());
    $ar_upd['LIDTUM'] 	= oggi_AS_date();
    
        
        if(strlen($form_values->listino) > 0)
            $ar_upd['LILIST'] 	= $form_values->listino;
            
            if(strlen($form_values->valuta) > 0)
                $ar_upd['LIVALU'] 	= $form_values->valuta;
                $ar_upd['LIPRZ'] 	= sql_f($form_values->prezzo);
                if(strlen($form_values->LIVAR1) > 0){
                    $ar_upd['LIVAR1'] 	= $form_values->LIVAR1;
                    $var1 = $form_values->LIVAR1;
                }else
                    $var1 = "";
                    
        if(strlen($form_values->LIVAR2) > 0){
            $ar_upd['LIVAR2'] 	= $form_values->LIVAR2;
            $var2 = $form_values->LIVAR2;
        }else
            $var2 = "";
            if(strlen($form_values->LIVAR3) > 0){
                $ar_upd['LIVAR3'] 	= $form_values->LIVAR3;
                $var3 = $form_values->LIVAR3;
            }else
                $var3 = "";
                if(strlen($form_values->data_val_ini_df) > 0)
                    $ar_upd['LIDTDE'] 	= $form_values->data_val_ini_df;
                    if(strlen($form_values->data_val_fin_df) > 0)
                        $ar_upd['LIDTVA'] 	= $form_values->data_val_fin_df;
                        $ar_upd['LIPRZP'] 	= sql_f($form_values->LIPRZP);
                        $ar_upd['LIPRZF'] 	= sql_f($form_values->LIPRZF);
                        if(strlen($form_values->decorr) > 0)
                            $ar_upd['LIDTDP'] 	= $form_values->decorr;
                                        
        
        if($cfg_mod_DeskArt['filler_listini'] != ''){
            $filler = genera_filler($m_table_config, $form_values);
            $ar_upd['LIFIL1'] 	= $filler;
        }else{
            if(strlen($form_values->LIFLG2) > 0)
                $ar_upd['LIFLG2'] 	= $form_values->LIFLG2;
            if(strlen($form_values->f_imp) > 0)
                $ar_upd['LIFLG3'] 	= $form_values->f_imp;
            $ar_upd['LIFLG1'] 	= $form_values->sigla;
        }
        
        
        
        $ar_upd['LISC1'] 	= sql_f($form_values->LISC1);
        $ar_upd['LISC2'] 	= sql_f($form_values->LISC2);
        $ar_upd['LISC3'] 	= sql_f($form_values->LISC3);
        $ar_upd['LISC4'] 	= sql_f($form_values->LISC4);
        $ar_upd['LIMAGG'] 	= sql_f($form_values->LIMAGG);
        $ar_upd['LINOTE'] 	= $form_values->LINOTE;
        if(strlen($form_values->u_m) > 0)
            $ar_upd['LIUM'] 	= $form_values->u_m;
            
            $ar_upd['LIMF'] 	= sql_f($form_values->min_fatt);
            
        $sql = "UPDATE {$cfg_mod_DeskUtility['file_listini']} LI
        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
        WHERE RRN(LI) = '{$form_values->rrn}' ";
   
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg($stmt);
        

        $ret = array();
        $ret['success'] = true;
        echo acs_je($ret);
        exit;
}
// ******************************************************************************************
// EXE AGGIUNGI LISTINO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi_listino'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
    $ar_ins['LIUSUM'] 	= SV2_db_user($auth->get_user());
    $ar_ins['LIDTUM'] 	= oggi_AS_date();
    $ar_ins['LIUSGE'] 	= $auth->get_user();
    $ar_ins['LIDTGE'] 	= oggi_AS_date();
    $ar_ins['LIDT'] 	= $id_ditta_default;
    
    $ar_ins['LIART'] 	= $m_params->c_art;
    $ar_ins['LILIST'] 	= $form_values->listino;
    $ar_ins['LINOTE'] 	= $form_values->LINOTE;
    if(strlen($form_values->valuta) > 0)
        $ar_ins['LIVALU'] 	= $form_values->valuta;
        $ar_ins['LIPRZ'] 	= sql_f($form_values->prezzo);
        if(strlen($form_values->LIVAR1) > 0){
            $ar_ins['LIVAR1'] 	= $form_values->LIVAR1;
            $var1 = $form_values->LIVAR1;
        }else
            $var1 = "";
            
            if(strlen($form_values->LIVAR2) > 0){
                $ar_ins['LIVAR2'] 	= $form_values->LIVAR2;
                $var2 = $form_values->LIVAR2;
            }else
                $var2 = "";
                
                if(strlen($form_values->LIVAR3) > 0){
                    $ar_ins['LIVAR3'] 	= $form_values->LIVAR3;
                    $var3 = $form_values->LIVAR3;
                }else
                    $var3 = "";
                        
    if(strlen($form_values->data_val_ini_df) > 0)
        $ar_ins['LIDTDE'] 	= $form_values->data_val_ini_df;
        if(strlen($form_values->data_val_fin_df) > 0)
            $ar_ins['LIDTVA'] 	= $form_values->data_val_fin_df;
            $ar_ins['LITPLI'] 	= $m_params->tipo_listino;
            $ar_ins['LIPRZP'] 	= sql_f($form_values->LIPRZP);
            $ar_ins['LIPRZF'] 	= sql_f($form_values->LIPRZF);
            if(strlen($form_values->decorr) > 0)
                $ar_ins['LIDTDP'] 	= $form_values->decorr;
                
                if($cfg_mod_DeskArt['filler_listini'] != ''){
                    $filler = genera_filler($m_table_config, $form_values);
                    $ar_ins['LIFIL1'] 	= $filler;
                }else{
                    if(strlen($form_values->LIFLG2) > 0)
                        $ar_ins['LIFLG2'] 	= $form_values->LIFLG2;
                    if(strlen($form_values->f_imp) > 0)
                        $ar_ins['LIFLG3'] 	= $form_values->f_imp;
                    $ar_ins['LIFLG1'] 	= $form_values->sigla;
                }
                
                $ar_ins['LISC1'] 	= sql_f($form_values->LISC1);
                $ar_ins['LISC2'] 	= sql_f($form_values->LISC2);
                $ar_ins['LISC3'] 	= sql_f($form_values->LISC3);
                $ar_ins['LISC4'] 	= sql_f($form_values->LISC4);
                $ar_ins['LIMAGG'] 	= sql_f($form_values->LIMAGG);
                if(strlen($form_values->u_m) > 0)
                    $ar_ins['LIUM'] 	= $form_values->u_m;
                    $ar_ins['LIMF'] 	= sql_f($form_values->min_fatt);
                    
                    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_listini']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt, $ar_ins);
                    echo db2_stmt_errormsg($stmt);
                    
                    
                    $sh = new SpedHistory($deskArt);
                    $sh->crea(
                        'pers',
                        array(
                            "messaggio"	=> 'LIST_AZZ_SC',
                            "vals" => array(
                                "RICDNEW" => trim($m_params->c_art),
                                "RIFG02" => sprintf("%-1s", $form_values->azzera),
                                "RIFG01" => $m_params->tipo_listino,
                                "RIFOR1" => $fornitore,
                                "RICLME" => $form_values->listino,
                                "RIGRME" => $form_values->LIZONA,
                                "RIRCOL" => $form_values->valuta,
                                "RISGME" => $var1,
                                "RICLFI" => $var2,
                                "RIGRFI" => $var3)
                        )
                        );
                    
                    
                    
                    $ret = array();
                    $ret['success'] = true;
                    echo acs_je($ret);
                    exit;
}
// ******************************************************************************************
// EXE DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    //LETTURA RECORD LISTINO
    $sql = "SELECT * FROM {$cfg_mod_DeskUtility['file_listini']} LI WHERE RRN(LI) = '{$form_values->rrn}' ";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $error_msg =  db2_stmt_errormsg($stmt);
    $row_li = db2_fetch_assoc($stmt);
    
    //licon
    $sql_licon = "DELETE FROM {$cfg_mod_DeskUtility['file_listini_licon']}
                    WHERE LCDT = '{$row_li['LIDT']}' 
                      AND LCSZLI = '{$row_li['LITPLI']}'
                      AND LCCCON = '{$row_li['LICCON']}'
                      AND LCLIST = '{$row_li['LILIST']}'
                      AND LCZONA = '{$row_li['LIZONA']}'
                      AND LCVALU = '{$row_li['LIVALU']}'
                      AND LCART  = '{$row_li['LIART']}'
                ";
    
    $stmt_licon = db2_prepare($conn, $sql_licon);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_licon);
    
    //liraq
    $sql_liraq = "DELETE FROM {$cfg_mod_DeskUtility['file_listini_liraq']}
                    WHERE LRDT = '{$id_ditta_default}' AND LRART = '{$form_values->LIART}' AND
                    LRTPLI = '{$form_values->LITPLI}' AND LRLIST = '{$form_values->listino}' AND LRCCON = '{$form_values->LICCON}'
                    ";
    
    $stmt_liraq = db2_prepare($conn, $sql_liraq);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_liraq);
    
    //DELETE RECORD LISTINO
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_listini']} LI WHERE RRN(LI) = '{$form_values->rrn}' ";    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $error_msg =  db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = $result;
    if (strlen($error_msg) > 0) {
        $ret['success'] = false;
        $ret['message'] = $error_msg;
    }
    echo acs_je($ret);
    exit;
}

 function has_nota_costi($chiave){
     global $conn, $cfg_mod_DeskUtility, $id_ditta_default;
    
    $sql = "SELECT COUNT(*) AS C_ROW, RLREST1
            FROM {$cfg_mod_DeskUtility['file_note_anag']}
            WHERE RLDT = '{$id_ditta_default}' AND RLTPNO = 'AC' AND RLRIFE2 = 'DCS'
            AND RLRIFE1 = '{$chiave}'
            GROUP BY RLREST1";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    return $row;
    
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    
    $c_art = $m_params->open_request->c_art;
   
            
    $sql = "SELECT AC.*, RRN(AC) AS RRN, ARFOR1, CF_FORNITORE.CFRGS1 AS D_FORNITORE
            FROM {$cfg_mod_DeskArt['file_costi']} AC
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
                ON AC.ACDT = AR.ARDT AND AC.ACART = AR.ARART
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
                ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1 AND CFTICF = 'F'
            WHERE ACDT = '{$id_ditta_default}' AND ACART = '{$c_art}' 
            ORDER BY ACANNO DESC, ACMESE DESC";

           
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
              
    while($row = db2_fetch_assoc($stmt)){
   
        $nr = array();
        $nr = $row;
        $nr['rrn'] = $row['RRN'];
        $nr['data_agg_ini']  = $row['ACAAIC'].sprintf("%02s", $row['ACMMIC']).sprintf("%02s", $row['ACGGIC']);
        $nr['data_agg_fin']  = $row['ACAACU'].sprintf("%02s", $row['ACMMCU']).sprintf("%02s", $row['ACGGCU']);
        $nr['ACUMCO'] = trim($row['ACUMCO']);
        $nr['anno_mese'] = $row['ACANNO']."/".sprintf("%02s", $row['ACMESE']);
        if($row['ACANOR'] > 0)
             $nr['ultimo_docu'] = $row['ACTPOR']."/".$row['ACANOR']."/".$row['ACNROR'];
        $nr['k_ordine'] = implode("_", array($row['ACDT'], $row['ACTIOR'], $row['ACINOR'], $row['ACANOR'], $row['ACNROR']));
        $nr['fornitore'] = $row['D_FORNITORE'];
        $chiave = trim($row['ACART']).trim($row['ACVALU']).$row['ACANNO'].sprintf("%02s", $row['ACMESE']).$row['ACTPCS'];
        $ac_nota = has_nota_costi($chiave);
      
        $nr['note'] = $ac_nota['C_ROW'];
        $nr['t_note'] = $ac_nota['RLREST1'];
        //$nr['data_agg_ini_df']  = print_date($row['LIDTDE']);
        //$nr['data_agg_fin_df']  = print_date($row['LIDTVA']);
      
        $ar[] = $nr;
             
    }
     
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
							{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            autoScroll: true,
            title: '',
            flex:0.7,
            items: [
						
				{
			xtype: 'grid',
			title: '',
			flex:0.75,
			autoScroll: true,
	        loadMask: true,
	        stateful: true,
	        stateId: 'seleziona-costi',
	        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],	
	        store: {
			//xtype: 'store',
			autoLoad:true,

					proxy: {
					   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
					   method: 'POST',								
					   type: 'ajax',

				       actionMethods: {
				          read: 'POST'
				        },
				        
				        
				           extraParams: {
							 open_request: <?php echo acs_je($m_params) ?>
        				},
        				
        				doRequest: personalizza_extraParams_to_jsonData, 
			
					   reader: {
			            type: 'json',
						method: 'POST',						            
			            root: 'root'						            
			        }
				},
				
    			fields: ['fornitore', 'ultimo_docu', 'anno_mese', 'rrn', 'data_agg_ini', 'data_agg_fin', 'ACART', 
    			          'ACDEPO', 'ACVALU', 'ACANNO', 'ACANOR', 'ACNROR', 'ACTPOR', 'note', 't_note'
    					, 'ACMESE', 'ACTPCS', 'ACCSTO', 'ACUMCO', 'ACSPES', 'ACPCST', 'ACPSPE', 'ACPQTA', 'ACMCST'	
    					
    					]							
						
			}, //store
				<?php  $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>"; ?>
			      columns: [	
			      
			       {
	                header   : 'Tp.',
	                dataIndex: 'ACTPCS',
	                filter: {type: 'string'}, filterable: true,
	                 width: 30,
	                },
	                {
				    text: '<?php echo $note; ?>',
				    width: 32, 
				    tooltip: 'Blocco note',
				    tdCls: 'tdAction',  
				    dataIndex : 'note',       		       		        
					renderer: function(value, metaData, record){
					    metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_note')) + '"';
    					if (record.get('note') > 0) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
    			    	if (record.get('note') == 0) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
    			    	 		
			    	}
			       },
	                 {
	                header   : 'Depo.',
	                dataIndex: 'ACDEPO',
	                filter: {type: 'string'}, filterable: true,
	                width: 40,
	                },
	                {
	                header   : 'Divisa',
	                dataIndex: 'ACVALU',
	                filter: {type: 'string'}, filterable: true,
	                width: 50,
	                },
	                 {
	                header   : 'Anno/mese',
	                dataIndex: 'anno_mese',
	                filter: {type: 'string'}, filterable: true,
	                width: 80,
	                },
					{
	                header   : 'UM',
	                dataIndex: 'ACUMCO',
	                filter: {type: 'string'}, filterable: true,
	                width: 30,
	                },
	                {
	                header   : 'Valore<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ultimo',
	                dataIndex: 'ACCSTO',
	                width: 75,
	                align : 'right',
	                filter: {type: 'string'}, filterable: true,
	                renderer : floatRenderer5
	                },
	                {
	                header   : 'Valore<br>&nbsp;&nbsp;&nbsp;medio',
	                dataIndex: 'ACMCST',
	                width: 65,
	                align : 'right',
	                filter: {type: 'string'}, filterable: true,
	                renderer : floatRenderer5
	                },
	               
	                {
	                header   : 'Valore<br>progressivo',
	                dataIndex: 'ACPCST',
	                width: 80,
	                align : 'right',
	                filter: {type: 'string'}, filterable: true,
	                renderer : floatRenderer5
	                },
	               
	                {
	                header   : 'Quantit&agrave;<br>progressiva',
	                dataIndex: 'ACPQTA',
	                width: 80,
	                align : 'right',
	                filter: {type: 'string'}, filterable: true,
	                renderer : floatRenderer4
	                },
	               {header: 'Data aggiornamento',
                    columns: [
                      {header: 'Iniziale', dataIndex: 'data_agg_ini', renderer: date_from_AS, width: 70, sortable: true}
                     ,{header: 'Finale', dataIndex: 'data_agg_fin', renderer: date_from_AS, width: 70, sortable: true}
                 	 ]},
                 	 
                 	 
	                /*{header: 'Immissione',
                    columns: [
                      {header: 'Data', dataIndex: 'data_gen', renderer: date_from_AS, width: 60, sortable: true}
                     ,{header: 'Utente', dataIndex: 'utente_gen', width: 70}
                 	 ]}*/
	                     
	                
	                
	         ], listeners: {
	         
	         selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('form').up('panel').down('#dx_form');
		               //pulisco eventuali filtri
		               form_dx.getForm().reset();
		               //ricarico i dati della form
	                   form_dx.getForm().setValues(selected[0].data);
	                   
	                   }
		          }
		        , itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  			  		
				  		 var voci_menu = [];
			    		  
    						 voci_menu.push({
    			         		text: 'Duplica costo',
    			        		iconCls : 'icon-comment_add-16',          		
    			        		handler: function () {
    			        		        acs_show_win_std('Duplica costo', 
				                			'acs_anag_art_costi_duplica.php?fn=open_form', {
				                             row: rec.data
									}, 300, 250, {}, 'icon-comment_add-16');		        			 
    			        		 }
    			    		 });			    		 	
			    		 
			    	
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
		   	 }
				 
			, viewConfig: {
		        getRowClass: function(record, index) {
		                   		
		        		           		
		           return '';																
		         }   
		    }		
					       
		
		
		}
		]}, 
		
	
		    
		
		   {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettaglio costi',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [
 		           {
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					hidden : true							
				   },  {
					name: 'filler',
					fieldLabel : 'filler',
					xtype: 'textfield',
					hidden : true				
				   },  
				   {
					name: 'ACART',
					fieldLabel : 'ACART',
					xtype: 'textfield',
					hidden : true							
				   },
				   {
    					name: 'ACTPCS',
    					xtype: 'combo',
    					fieldLabel: 'Tipo costo',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		allowBlank: false,	
    			   		queryMode: 'local',
             		    minChars: 1, 	
        				labelWidth : 80,
        				anchor: '-15',
        				width : 150	,				
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [								    
    					     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BVCS'), ''); ?>	
    					    ]
    					}
    					,listeners: { 
        			 		beforequery: function (record) {
            	         		record.query = new RegExp(record.query, 'i');
            	         		record.forceAll = true;
            				 }
         				 }
    					
    			 },
				    {
    					name: 'ACVALU',
    					xtype: 'combo',
    					fieldLabel: 'Divisa',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		allowBlank: false,	
    			   		queryMode: 'local',
             		    minChars: 1, 	
        				labelWidth : 80,
        				anchor: '-15',
        				width : 150	,				
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [								    
    					     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('VUVL'), ''); ?>	
    					    ]
    					}
    					,listeners: { 
        			 		beforequery: function (record) {
            	         		record.query = new RegExp(record.query, 'i');
            	         		record.forceAll = true;
            				 }
         				 }
    					
    			 },
						
				   {
                			name: 'ACUMCO',
                			xtype: 'combo',
                			fieldLabel: 'UM',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: 'select',
                	   		allowBlank: true,								
                		    anchor: '-15',
                		    queryMode: 'local',
                            minChars: 1,
                            labelWidth : 80,
                			store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: [								    
        					     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUUM'), ''); ?>	
        					    ]
    						},
                			listeners: { 
                			 	beforequery: function (record) {
                	         	record.query = new RegExp(record.query, 'i');
                	         	record.forceAll = true;
                             }
                          }
                		 },
                		 
                		   {
                			name: 'ACDEPO',
                			xtype: 'combo',
                			fieldLabel: 'Deposito',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: 'select',
                	   		allowBlank: true,								
                		    anchor: '-15',
                		    queryMode: 'local',
                            minChars: 1,
                            labelWidth : 80,
                			store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: [								    
        					     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUFD'), ''); ?>	
        					    ]
    						},
                			listeners: { 
                			 	beforequery: function (record) {
                	         	record.query = new RegExp(record.query, 'i');
                	         	record.forceAll = true;
                             }
                          }
                		 } ,
                		 
                		 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {
                			name: 'ACANNO',
                			xtype: 'numberfield',
                			fieldLabel: 'Anno',
                			labelWidth : 80,
                			hideTrigger : true,
                			maxValue : 2999,
                			width : 150
                			},
                			{
                			name: 'ACMESE',
                			xtype: 'numberfield',
                			fieldLabel: 'Mese',
                			labelWidth : 40,
                			hideTrigger : true,
                			labelAlign : 'right',
                			maxValue : 31,
                			width : 80
                			}
                		]} 
                		,{
                			name: 'ACCSTO',
                			xtype: 'numberfield',
                			fieldLabel: 'Valore ultimo',
                			labelWidth : 80,
                			hideTrigger : true,
                			decimalPrecision : 5
                			}
                		,{
                			name: 'ACMCST',
                			xtype: 'numberfield',
                			fieldLabel: 'Valore medio',
                			labelWidth : 80,
                			hideTrigger : true,
                			decimalPrecision : 5
                			}
                		,{
                			name: 'ACSPES',
                			xtype: 'numberfield',
                			fieldLabel: 'Spese',
                			labelWidth : 80,
                			hideTrigger : true,
                			decimalPrecision : 5
                			},
                			
                    		{
        					xtype: 'fieldset',
        	                title: 'Progressivi',
        	                layout: 'anchor',
        	                flex:1,
        	                items: [
        	                	{
                			name: 'ACPCST',
                			xtype: 'numberfield',
                			fieldLabel: 'Valore',
                			labelWidth : 80,
                			hideTrigger : true,
                			decimalPrecision : 5
                			}
                		   ,{
                			name: 'ACPSPE',
                			xtype: 'numberfield',
                			fieldLabel: 'Spese',
                			labelWidth : 80,
                			hideTrigger : true,
                			decimalPrecision : 5
                			},{
                			name: 'ACPQTA',
                			xtype: 'numberfield',
                			fieldLabel: 'Quantit&agrave;',
                			labelWidth : 80,
                			hideTrigger : true,
                			decimalPrecision : 5
                			}
        	                
        	                ]},
        	                
        	                { xtype: 'fieldcontainer'
                        	 , layout: 
                        	     { type: 'hbox'
                        		 ,	pack: 'start'
                        	     , align: 'stretch'
                        	     }
                        	 , items: [
                               { xtype: 'displayfield',
                    	         fieldLabel: 'Ultimo docum.',
                    	       },
                    	       { 
                    	       
                    	       xtype: 'numberfield'
                    	       , hideTrigger : true
                               , name: 'ACANOR'
                               , anchor: '-15'
                               , margin: "0 0 0 0"	
                               , width : 60
                               , keyNavEnabled : false
                               , mouseWheelEnabled : false
                    	       },{ 
                    	       
                    	         xtype: 'numberfield'
                    	       , hideTrigger : true
                               , name: 'ACNROR'
                               , anchor: '-15'
                               , margin: "0 0 0 0"	
                               , width : 60
                               , keyNavEnabled : false
                               , mouseWheelEnabled : false
                    	       },
                    	       { xtype: 'textfield',
                    	         margin: "0 0 0 0",
                    	         name : 'ACTPOR',
                    	         width : 40
                    	       }
                    	       ]},
                    	        { xtype: 'textfield',
                    	          fieldLabel: 'Fornitore',
                    	          name : 'fornitore',
                    	          anchor: '-10',
                    	          flex : 1
                    	       }
                			
                	 ],
			 		           
	<?php if(!isset($m_params->solo_visualizzazione) || $m_params->solo_visualizzazione != 'Y'){?>	           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     }, {
                     xtype: 'button',
                    text: 'Refresh',
		            scale: 'small',	               
		            iconCls : 'icon-button_black_repeat_dx-16',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			  	  form = this.up('form'); 
 			          grid.getStore().load();
 			          form.getForm().reset();
 			        }
			     },{
                     xtype: 'button',
                    text: 'Costi',
		            scale: 'small',	               
		            iconCls : 'icon-listino',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			 	  form = this.up('form'); 
	       			 	  
	       			 	  acs_show_win_std('Costi articolo [' +rec.get('LIART') + '] ' , 'acs_anag_art_costi.php?fn=open_tab', {c_art: rec.get('LIART')}, 1300, 530, null, 'icon-listino');
 			        
 			        }
			     }, '->',
                 
                  
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_listino',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				c_art : '<?php echo $m_params->c_art; ?>',
	        				tipo_listino : '<?php echo$m_params->tipo_list; ?>'
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_listino',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
		   <?php }?>
			 	 }
					
						   
						
						
				
						

								
					
					
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}

