<?php

require_once "../../config.inc.php";

$m_params = acs_m_params_json_decode();

function getRGB($hex){
    
    $hex = "#{$hex}";
    list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
    $rgb = sprintf("%03s", $r).sprintf("%03s", $g).sprintf("%03s", $b);
    return $rgb;
}


function getHex($rgb){
    
    $r = substr($rgb, 0, 3);
    $g = substr($rgb, 3, 3);
    $b = substr($rgb, 6, 3);
    $hex = sprintf("%02x%02x%02x", $r, $g, $b);
    return $hex;
}

function genera_TAREST($m_table_config, $values){
    $value_attuale = $values->TAREST;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
    //$new_value = "";
    foreach($m_table_config['TAREST'] as $k => $v){
        if(isset($values->$k)){
                
            if($v['type'] == 'decimal'){
                $int = $v['len'] - $v['decimal_positions'];
                $dec = $v['decimal_positions'];
                $chars = number_to_text($values->$k, $int, $dec);
            }else
                $chars = $values->$k;
                    
            //riempi_con lo uso solo se mi ha passato un valore
            // se volessi sempre forzarlo (anche quindi nel caso di blank)
            // potrei ad esempio usare 'riempi_sempre_con'
                    
                if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
                    $len = "%{$v['riempi_con']}{$v['len']}s";
                else
                    $len = "%-{$v['len']}s";
                            
                $chars = substr(sprintf($len, $chars), 0, $v['len']);
                $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                $value_attuale = $new_value;
                            
            }
            
        }

    return $new_value;
}


function get_m_row($row, $m_table_config){
    
    $row['RRN'] = acs_u8e(trim($row['RRN']));
    $row['TADESC'] = acs_u8e(trim($row['TADESC']));
    $row['descrizione'] = htmlentities($row['TADESC']);
    $row['TACINT'] = trim($row['TACINT']);
    $row['TANR'] = trim($row['TANR']);
    
    if(trim($row['TAID']) == 'PUFD')
        $row['TAREST'] = $row['TADES2'].$row['TAREST'];
    
    $row['TADES2'] = trim($row['TADES2']);
     
    if(trim($row['TAID']) == 'PUVR'){
        $var_master = substr($row['TAREST'], 67, 3);
        if(trim($row['TANR']) == $var_master)
            $row['is_master'] = 'Y';
    }
    
    if(is_array($m_table_config['TAREST'])){
        foreach($m_table_config['TAREST'] as $k => $v){
            if(substr($k, 0, 4) == 'data'){
                $row[$k] = print_date(substr($row['TAREST'], $v['start'], $v['len']));
            }else{
                $row[$k] = rtrim(substr($row['TAREST'], $v['start'], $v['len']));
            }
        }
    }
    
    for($i=0; $i <= 14; $i++){
        $row["frz_{$i}"] = $row['frz'][$i];
        $row["chr_ric_{$i}"] = $row['chr_ric'][$i];
        $row["coaf_{$i}"] = $row['coaf'][$i];
    }
    
    $row['TIPOLOGIE'] = "";
    for($i=1; $i<= 10; $i++){
        $row['TIPOLOGIE'] .= "{$row["tipo{$i}"]}, ";
        
    }
    
    $row['tp_list'] = "";
    for($i=1; $i<= 4; $i++){
        if($row["tpli{$i}"] != '')
            $row['tp_list'] .= "{$row["tpli{$i}"]}; ";
            
    }
    
    $row['psp1'] = text_to_number($row["psp1"], 3, 2);
    $row['psp2'] = text_to_number($row["psp2"], 3, 2);
    $row['ricarico'] = text_to_number($row["ricarico"], 3, 2);
    if($row["lis1"] != '' || $row["psp1"] != '')
        $row['list_sp1'] = $row["lis1"]. " " .n($row["psp1"], 2)."%";
    if($row["lis2"] != '' || $row["psp1"] != '')
        $row['list_sp2'] = $row["lis2"]. " " .n($row["psp2"], 2)."%";
    
    $row['list_p'] = $row["lpun"];
    
        
    $row['SEQUENZE'] = "";
    for($i=1; $i<= 10; $i++){
        $row_seq = trim($row["seq{$i}"]);
        $row['SEQUENZE'] .= "{$row_seq}, ";
        
    }
    
    $row['var_range'] = "";
    if($row["vrrh"] != '')
        $row['var_range'] .= "{$row["vrrh"]}";
    if($row["vrrl"] != '')
        $row['var_range'] .= ", {$row["vrrl"]}";
    if($row["vrh2"] != '')
        $row['var_range'] .= ", {$row["vrh2"]}";
                
    $row['var_sel'] = "";
    if($row["vras"] != '')
        $row['var_sel'] .= "{$row["vras"]}";
    if($row["vrs2"] != '')
        $row['var_sel'] .= ", {$row["vrs2"]}";
    if($row["vrs3"] != '')
        $row['var_sel'] .= ", {$row["vrs3"]}";
                            

    if($row["vlhd"] != '' || $row["vlha"] != '')
        $row['rangeH'] = "{$row["vlhd"]} - {$row["vlha"]}";
    if($row["vlld"] != '' || $row["vlla"] != '')
        $row['rangeL'] = "{$row["vlld"]} - {$row["vlla"]}";
    if($row["vl2d"] != '' || $row["vl2a"] != '')
        $row['rangeH2'] = "{$row["vl2d"]} - {$row["vl2a"]}";
                         
    if($row["vlsd"] != '' || $row['vlsa'] != '')
        $row['selez1'] = "{$row["vlsd"]} - {$row["vlsa"]}";
    if($row["vs2d"] != '' || $row['vs2a'] != '')
        $row['selez2'] = "{$row["vs2d"]} - {$row["vs2a"]}";
    if($row["vs3d"] != '' || $row['vs3a'] != '')
        $row['selez3'] = "{$row["vs3d"]} - {$row["vs3a"]}";
            
    
    
    return $row;
}


if ($_REQUEST['fn'] == 'get_data_tab'){
   
    $where = "";
    if(strlen($m_params->domanda)>0)
        $where .= " AND TACOR2 = '{$m_params->domanda}'";
    
    
    if(strlen($m_params->cgs)>0)
        $where .= " AND TACOR2 = '{$m_params->cgs}'";
    
    
    
    $sql = "SELECT *
    FROM {$cfg_mod_Admin['file_tab_sys']}
    WHERE TADT = '$id_ditta_default' AND TAID = '{$m_params->tabella}' AND TATP <> 'S' AND TALINV=''
    {$where} ORDER BY TANR, TADESC";
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $text = "[" . trim($row['TANR']) . "] " . trim($row['TADESC']);
        
        $ret[] = array( "id" 	=> trim($row['TANR']),
            "text" 	=> acs_u8e($text) );
    }
    
    
    
    
    echo acs_je($ret);
    exit;
}

