<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];  
    $nr['tades2'] = $row['TADES2'];  
    
    $nr['data_iniz'] = substr($row['TAREST'], 0, 4).substr($row['TAREST'], 4, 2).substr($row['TAREST'], 6, 2);
    $nr['data_fin'] = substr($row['TAREST'], 8, 4).substr($row['TAREST'], 12, 2).substr($row['TAREST'], 14, 2);
    $nr['controllo_data'] = substr($row['TAREST'], 16, 1);
    
    
    $val_gr = find_TA_sys('PUCI', substr($row['TAREST'], 25, 3));
    if(trim(substr($row['TAREST'], 25, 3)) != ''){
        $nr['gruppo_collo'] = " [".trim(substr($row['TAREST'], 25, 3))."] ".$val_gr[0]['text'];
        $nr['val_gruppo_collo'] = trim(substr($row['TAREST'], 25, 3));
    }else{
        $nr['gruppo_collo'] = "";
    }
    
    $nr['s_articolo'] = substr($row['TAREST'], 68, 15);
    //riga
    $val_dom = find_TA_sys('PUVR', substr($row['TAREST'], 84, 3));
    if(trim(substr($row['TAREST'], 84, 3)) != ''){
        $nr['var'] = " [".trim(substr($row['TAREST'], 84, 3))."] ".$val_dom[0]['text'];
        $nr['val_var'] = trim(substr($row['TAREST'], 84, 3));
    }else{
        $nr['var'] = "";
    }
    $val_risp = find_TA_sys('PUVN', substr($row['TAREST'], 87, 3), null, substr($row['TAREST'], 84, 3));
    if(trim(substr($row['TAREST'], 87, 3)) != ""){
        $nr['van']= "[".trim(substr($row['TAREST'], 87, 3))."] ".$val_risp[0]['text'];
        $nr['val_van'] = trim(substr($row['TAREST'], 87, 3));
    }else{
        $nr['van'] = "";
    }
    $nr['sele_ie'] = substr($row['TAREST'], 90, 1);
    
    //sottoriga
    $val_dom2 = find_TA_sys('PUVR', substr($row['TAREST'], 91, 3));
    if(trim(substr($row['TAREST'], 91, 3)) != ''){
        $nr['var1'] = " [".trim(substr($row['TAREST'], 91, 3))."] ".$val_dom[0]['text'];
        $nr['val_var1'] = trim(substr($row['TAREST'], 91, 3));
    }else{
        $nr['var1'] = "";
    }
    $val_risp = find_TA_sys('PUVN', substr($row['TAREST'], 94, 3), null, substr($row['TAREST'], 94, 3));
    if(trim(substr($row['TAREST'], 94, 3)) != ""){
        $nr['van1']= "[".trim(substr($row['TAREST'], 94, 3))."] ".$val_risp[0]['text'];
        $nr['val_van1'] = trim(substr($row['TAREST'], 94, 3));
    }else{
        $nr['van1'] = "";
    }
    
    $val_tipo = find_TA_sys('PUTI', substr($row['TAREST'], 97, 3));
    if(trim(substr($row['TAREST'], 97, 3)) != ''){
        $nr['tipo'] = " [".trim(substr($row['TAREST'], 97, 3))."] ".$val_dom[0]['text'];
        $nr['val_tipo'] = trim(substr($row['TAREST'], 97, 3));
    }else{
        $nr['tipo'] = "";
    }
    
    
    $nr['sele_ie1'] = substr($row['TAREST'], 100, 1);
    
    
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2','data_iniz', 'data_fin', 'controllo_data', 'gruppo_collo', 'val_gruppo_collo', 's_articolo', 'var', 'val_var', 'van', 'val_van', 'sele_ie',
                       'var1', 'val_var1', 'van1', 'val_van1', 'sele_ie1', 'tipo', 'val_tipo'
    );
    
    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start();
    
    ?>
    
     	           {
	                header   : 'Descrizione',
	                dataIndex: 'tadesc',
	                width : 150
	                }, 
	                 {
	                header   : 'Note',
	                dataIndex: 'tades2',
	                width : 150
	                },
	                {
	                header   : 'Data in.',
	                dataIndex: 'data_iniz',
	                flex : 1,
	                renderer: date_from_AS
	                },{
	                header   : 'Data fin.',
	                dataIndex: 'data_fin',
	                flex : 1,
	                renderer: date_from_AS
	                },{
	                header   : 'Contr. data',
	                dataIndex: 'controllo_data',
	                flex : 1,
	                },{
	                header   : 'Gr. collo',
	                dataIndex: 'gruppo_collo',
	                flex : 1,
	                },{
	                header   : 'Sel. articoli',
	                dataIndex: 's_articolo',
	                flex : 1,
	                },{
	                header   : 'Variabile',
	                dataIndex: 'var',
	                flex : 1,
	                },{
	                header   : 'Variante',
	                dataIndex: 'van',
	                flex : 1,
	                },{
	                header   : 'Sel I/E',
	                dataIndex: 'sele_ie',
	                width : 50
	                }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){

       
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "note" => trim($row->tades2),
        "data_iniz" => trim($row->data_iniz),
        "data_fin" => trim($row->data_fin),
        "controllo_data" => trim($row->controllo_data),
        "gruppo_collo" => trim($row->val_gruppo_collo),
        "s_articolo" => trim($row->s_articolo),
        "val_var" => trim($row->val_var),
        "val_van" => trim($row->val_van),
        "desc_variante" => trim($row->van),
        "sele_ie" => trim($row->sele_ie),
        "val_var1" => trim($row->val_var1),
        "val_van1" => trim($row->val_van1),
        "desc_variante1" => trim($row->van1),
        "val_tipo" => trim($row->val_tipo),
        "desc_tipo" => trim($row->tipo),
        "sele_ie1" => trim($row->sele_ie1)
        
        
    );
 
    return $ar_values;
    
}

function get_values_from_array($row){
    
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "note" => trim($row['tades2']),
        "data_iniz" => trim($row['data_iniz']),
        "data_fin" => trim($row['data_fin']),
        "controllo_data" => trim($row['controllo_data']),
        "gruppo_collo" => trim($row['val_gruppo_collo']),
        "s_articolo" => trim($row['s_articolo']),
        "val_var" => trim($row['val_var']),
        "val_van" => trim($row['val_van']),
        "desc_variante" => trim($row['van']),
        "sele_ie" => trim($row['sele_ie']),
        "val_var1" => trim($row['val_var1']),
        "val_van1" => trim($row['val_van1']),
        "desc_variante1" => trim($row['van1']),
        "sele_ie1" => trim($row['sele_ie1']),
        "val_tipo" => trim($row['val_tipo']),
        "desc_tipo" => trim($row['tipo']),
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    global $main_module;
    $deskArt = new DeskArt();
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		labelWidth : 110,
		anchor: '-15',	
		maxLength : 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
    
       {xtype: 'textfield',
		name: 'descrizione',
		fieldLabel: 'Descrizione',
		maxLength : 30,
		labelWidth : 110,
		anchor: '-15',
		value:  <?php echo j($ar_values['descrizione']); ?>
		},
		{xtype: 'textfield',
		name: 'note',
		fieldLabel: 'Note',
		maxLength : 30,
		labelWidth : 110,
		anchor: '-15',
		value:  <?php echo j($ar_values['note']); ?>
		},{
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Data in.'
	   , labelWidth :110
	   , name: 'data_iniz'
	   , format: 'd/m/Y'
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	   , anchor: '-15'
	   ,value: '<?php echo print_date($ar_values['data_iniz'] , "%d/%m/%Y"); ?>'
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
			}
		},{
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Data fin.'
	   , labelWidth :110
	   , name: 'data_fin'
	   , format: 'd/m/Y'
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	   , anchor: '-15'
	   ,value: '<?php echo print_date($ar_values['data_fin'] , "%d/%m/%Y"); ?>'
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
			}
	  },{
		name: 'controllo_data',
		xtype: 'combo',
		fieldLabel: 'Contr. data',
		labelWidth : 110,
		forceSelection: true,	
		value:  <?php echo j($ar_values['controllo_data']); ?>,							
		displayField: 'text',
		valueField: 'id',								
		emptyText: ' - seleziona -',
   		allowBlank: true,								
	    anchor: '-15',
	    queryMode: 'local',
	    width : 200,
        minChars: 1, 
		store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			    <?php echo acs_ar_to_select_json($deskArt->find_TA_std('CHKDT', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>
			    ]
			},
		listeners: { 
		 	beforequery: function (record) {
         	record.query = new RegExp(record.query, 'i');
         	record.forceAll = true;
         }
      }
	 },
		{xtype: 'combo',
		flex: 1,
		name: 'gruppo_collo',
		fieldLabel: 'Gruppo collo',
		labelWidth : 110,
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	  value:  <?php echo j($ar_values['gruppo_collo']); ?>,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		      data: [								    
				     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUCI'), ''); ?>	
				    ]
		}
		
   },
		  {
			flex: 1,
            xtype: 'combo',
			name: 's_articolo',
			fieldLabel: 'Sel. articoli',
			labelWidth : 110,
			itemId: 'cod_art',
			value:  <?php echo j($ar_values['s_articolo']); ?>,
			anchor: '-15',
			minChars: 2,			
			store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_anag_art_abbinamento.php?fn=get_json_data_articoli',
		            actionMethods: {
			          read: 'POST'
			        },
		            extraParams: {
	    		    		gruppo: '',
	    				},				            
			        doRequest: personalizza_extraParams_to_jsonData, 
		            reader: {
		                type: 'json',
		                root: 'root',
		                method: 'POST',	
		                totalProperty: 'totalCount'
		            }
		        },       
					fields: [{name:'id'}, {name:'text'}],            	
            },
          
			valueField: 'id',                        
            displayField: 'text',
            typeAhead: false,
            hideTrigger: true,
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun articolo trovato',
    
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{text}</span></h3>' +
                        '[{id}] {text}' + 
                    '</div>';
                	}                
            
        	},

			pageSize: 1000,
	},
	{name: 'val_var',
		xtype: 'combo',
		flex: 1,
		labelWidth : 110,
		fieldLabel: 'Variabile (riga)',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	    value:  <?php echo j($ar_values['val_var']); ?>,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		    data: [								    
		     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
		    ]
		}, listeners: {
        	change: function(field,newVal) {	
          		if (!Ext.isEmpty(newVal)){
                	combo_risp = this.up('form').down('#c_var');                      		 
                 	combo_risp.store.proxy.extraParams.domanda = newVal;
                	combo_risp.store.load();                             
                 }
                 

                }, beforequery: function (record) {
                record.query = new RegExp(record.query, 'i');
                record.forceAll = true;
            }
   }
		
   } ,
   
  {
		name: 'val_van',
		xtype: 'combo',
		flex: 1,
		labelWidth : 110,
		itemId: 'c_var',
		fieldLabel: 'Variante',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',								
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	   value:  <?php echo j($ar_values['val_van']); ?>,
	    store: {
			        autoLoad: true,
					proxy: {
			            type: 'ajax',
			            url : 'acs_anag_art_variabili.php?fn=get_data_puvn',
			            actionMethods: {
				          read: 'POST'
			        },
	                	extraParams: {
	    		    		domanda: <?php echo j($ar_values['val_var']); ?>,
	    				},				            
			            doRequest: personalizza_extraParams_to_jsonData, 
			            reader: {
			            type: 'json',
						method: 'POST',						            
				            root: 'root'						            
				        }
			        },       
					fields: ['id', 'text'],		             	
	            }
		, listeners: {
                afterrender: function(comp){
                    data = [
                        {id: <?php echo j($ar_values['val_van'])?>, text: <?php echo j($ar_values['desc_variante']) ?>}
                    ];
                    comp.store.loadData(data);
                    comp.setValue(<?php echo j($ar_values['val_van'])?>);
                }
		
		}
		
   	},   {xtype: 'combo',
		flex: 1,
		name: 'sele_ie',
		fieldLabel: 'Sel. I/E',
		labelWidth : 110,
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	    value : <?php echo j($ar_values['sele_ie']); ?>,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		      data: [								    
				     {id : 'I', text : 'I'},
				     {id : 'E', text : 'E'}
				    ]
		}
		
   },
      {name: 'val_var1',
		xtype: 'combo',
		flex: 1,
		labelWidth : 110,
		fieldLabel: 'Variabile (sottoriga)',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	    value:  <?php echo j($ar_values['val_var1']); ?>,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		    data: [								    
		     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
		    ]
		}, listeners: {
        	change: function(field,newVal) {	
          		if (!Ext.isEmpty(newVal)){
                	combo_risp = this.up('form').down('#c_var1');                      		 
                 	combo_risp.store.proxy.extraParams.domanda = newVal;
                	combo_risp.store.load();                             
                 }
                 

                }, beforequery: function (record) {
                record.query = new RegExp(record.query, 'i');
                record.forceAll = true;
            }
   }
		
   } ,
   
  {
		name: 'val_van1',
		xtype: 'combo',
		flex: 1,
		labelWidth : 110,
		itemId: 'c_var1',
		fieldLabel: 'Variante',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',								
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	   value:  <?php echo j($ar_values['val_van1']); ?>,
	    store: {
			        autoLoad: true,
					proxy: {
			            type: 'ajax',
			            url : 'acs_anag_art_variabili.php?fn=get_data_puvn',
			            actionMethods: {
				          read: 'POST'
			        },
	                	extraParams: {
	    		    		domanda: <?php echo j($ar_values['val_var1']); ?>,
	    				},				            
			            doRequest: personalizza_extraParams_to_jsonData, 
			            reader: {
			            type: 'json',
						method: 'POST',						            
				            root: 'root'						            
				        }
			        },       
					fields: ['id', 'text'],		             	
	            }
		, listeners: {
                afterrender: function(comp){
                    data = [
                        {id: <?php echo j($ar_values['val_van1'])?>, text: <?php echo j($ar_values['desc_variante1']) ?>}
                    ];
                    comp.store.loadData(data);
                    comp.setValue(<?php echo j($ar_values['val_van1'])?>);
                }
		
		}
		
   	},    { name: 'val_tipo',
	  xtype: 'combo',
      fieldLabel: 'Tipologia',
      forceSelection: true,		
      labelWidth : 110,						
      displayField: 'text',
	  valueField: 'id',
	 value : <?php echo j($ar_values['val_tipo']); ?>,						
	  emptyText: '- seleziona -',
      queryMode: 'local',
      minChars: 1,
      anchor: '-15',
	  store: {
		editable: false,
		autoDestroy: true,
	    fields: [{name:'id'}, {name:'text'}],
	    data: [								    
	     <?php echo acs_ar_to_select_json(find_TA_sys('PUTI', null, null, null, null, null, 0, '', 'Y'), ''); ?>		
	    ]
	},listeners: { 
	beforequery: function (record) {
	record.query = new RegExp(record.query, 'i');
	record.forceAll = true;
	 }
	}
 }, {xtype: 'combo',
		flex: 1,
		name: 'sele_ie',
		fieldLabel: 'Sel. I/E',
		labelWidth : 110,
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	    value : <?php echo j($ar_values['sele_ie1']); ?>,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		      data: [								    
				     {id : 'I', text : 'I'},
				     {id : 'E', text : 'E'}
				    ]
		}
		
   }
    
   
   
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_iniz);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_fin);
    $ar_ins['TAREST'] .= sprintf("%-1s", $form_values->controllo_data);
    $ar_ins['TAREST'] .= sprintf("%-8s", " ");
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->gruppo_collo);
    $ar_ins['TAREST'] .= sprintf("%-40s", " ");
    $ar_ins['TAREST'] .= sprintf("%-15s", $form_values->s_articolo);
    $ar_ins['TAREST'] .= sprintf("%-1s", " ");
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->val_var);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->val_van);
    $ar_ins['TAREST'] .= sprintf("%-1s", $form_values->sele_ie);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->val_var1);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->val_van1);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->val_tipo);
    $ar_ins['TAREST'] .= sprintf("%-1s", $form_values->sele_ie1);
    
    
    $ar_ins['TANR']   = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;
    $ar_ins['TADES2'] = $form_values->note;
    
    return $ar_ins;
    
}