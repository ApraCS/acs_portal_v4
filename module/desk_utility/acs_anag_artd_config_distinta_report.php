<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

            $indice = trim($_REQUEST['cod']);

			$sql = "SELECT *
			FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
			WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$indice}'
            ORDER BY CDSEQI, CDCMAS, CDSLAV";
						

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			while($row = db2_fetch_assoc($stmt)){
				$nr = array();
				$nr = $row;
				$ar[] = $nr;


			}
			
			
		


echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>Distinta indice classificazione[".$_REQUEST['cod']."] ".$_REQUEST['des']."</H2>
 		</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";

echo "<tr class='liv_data' >
  		<th>Seq. Indice</th>
  		<th>Gruppo</th>
		<th>Seq.voce</th>
		<th>Voce</th>
		<th>Descriz. voce</th>
		<th>Da</th>
		<th>A</th>
		<th>Radice</th>
        <th>Segue</th>
        <th>Tipo lista</th>
        <th>Codice lista</th>
        <th>Nuovo</th>
        <th>Ultimo ID</th>
        <th>Nr ID</th>
        <th>Duplica</th>
		</tr>";

foreach ($ar as $kar => $r){
	
	echo "<tr>";
    echo "  <td>".$r['CDSEQI']."</td>
   			<td>".$r['CDCMAS']."</td>
 			<td>".$r['CDSEQU']."</td>
			<td>".$r['CDSLAV']."</td>
			<td>".$r['CDDESC']."</td>
			<td>".$r['CDSTIN']."</td>
            <td>".$r['CDSTFI']."</td>
 			<td>".$r['CDRSTR']."</td>
			<td>".$r['CDFLGA']."</td>
			<td>".$r['CDFLLG']."</td>
			<td>".$r['CDCDLI']."</td>
            <td>".$r['CDNEWC']."</td>
 			<td>".$r['CDPROG']."</td>
			<td>".$r['CDNCAR']."</td>
			<td>".$r['CDARDU']."</td>
			</tr>";
	

	
	
}


?>
</div>
</body>
</html>	

