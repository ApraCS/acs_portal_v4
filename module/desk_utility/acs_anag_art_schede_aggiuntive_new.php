<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

   
if ($_REQUEST['fn'] == 'open_form'){    
    ?>


{"success":true, "items": [

    {
        xtype: 'form',
        c_articolo : '',
        autoScroll : true,
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        flex:1,
        frame: true,
        items: [
        
        <?php
        $sql = "SELECT RRN(TA) AS RRN, TAKEY1, TADESC
                FROM {$cfg_mod_DeskArt['file_tabelle']} TA
                WHERE TADT='{$id_ditta_default}' AND TATAID = 'SCHED' AND TAKEY1 <> 'FSC'";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
                
        while($row = db2_fetch_assoc($stmt)){
            $txt_bl = "[".trim($row['TAKEY1'])."] ".trim($row['TADESC']);
            if(isset($_SESSION['cod_art']) && strlen($_SESSION['cod_art']) > 0 && $m_params->l_insert == 'Y')
                $ha_commenti = $deskArt->has_scheda_articolo($_SESSION['cod_art'], $row['TAKEY1']);
            else
                $ha_commenti = $deskArt->has_scheda_articolo($m_params->c_art, $row['TAKEY1']);
        
            if ($ha_commenti == FALSE)
                $img_com_name = "icon-tag_grey-16";
            else
                $img_com_name = "icon-tag-16";
        ?>
        
		            {
				 	xtype: 'fieldcontainer',
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
						
						{
								xtype: 'button',
								text: '',
								itemId: 'f_bt_<?php echo $row['RRN']; ?>',
								margin : '0 5 0 0',
								iconCls: '<?php echo $img_com_name; ?>',
								scale: 'small',
								handler : function(){
								
								var form = this.up('form').getForm();
								var win = this.up('window');
							
								
								
								<?php if(isset($m_params->list_selected_art) && count($m_params->list_selected_art) > 0){ ?>
								    
                                 	var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    var bt = win.down('#f_bt_<?php echo $row['RRN']; ?>');
			        					    bt.setIconCls('icon-tag-16');
    		        						from_win.close();  
    						        		}
    				    				};
    				    				
    				    				
    				    				<?php if(trim($row['TAKEY1']) == 'RC1L'){ ?>
								          
								          acs_show_win_std('<?php echo "{$txt_bl} articolo {$m_params->c_art}"; ?>',
								              'acs_tab_sys_RC1L.php?fn=open_panel',
								              {list_art: <?php echo acs_je($m_params->list_selected_art); ?>, l_insert : <?php echo j($m_params->l_insert)?>, hide_grid : 'Y'}, 500, 550,
    	        							my_listeners, 'icon-tag-16');
								          
								      <?php }else{ ?>
								          
								       acs_show_win_std('<?php echo "{$txt_bl} articolo {$m_params->c_art}"; ?>', 
        		    				    'acs_anag_art_schede_aggiuntive.php?fn=open_mod', 
        		    					{list_art: <?php echo acs_je($m_params->list_selected_art); ?>, tipo_scheda: '<?php echo trim($row['TAKEY1']); ?>'
        		    					, l_insert : <?php echo j($m_params->l_insert)?>}, 500, 350,
        	        					my_listeners, 'icon-tag-16');
    	        					
								    <?php  } 
        
        
                                }else{
								  
								      if(trim($row['TAKEY1']) == 'RC1L'){ ?>
								          
								          acs_show_win_std('<?php echo "{$txt_bl} articolo {$m_params->c_art}"; ?>',
								              'acs_tab_sys_RC1L.php?fn=open_panel',
								              {c_art: <?php echo j($m_params->c_art); ?>}, 1300, 550,
    	        							null, 'icon-tag-16');
								          
								      <?php }else{ ?>
								          
								          acs_show_win_std('<?php echo "{$txt_bl} articolo {$m_params->c_art}"; ?>', 
    		    				    		'acs_anag_art_schede_aggiuntive.php?fn=open_tab', 
    		    							{c_art: <?php echo j($m_params->c_art); ?>, tipo_scheda: '<?php echo trim($row['TAKEY1']); ?>'}, 1200, 400,
    	        							null, 'icon-tag-16');
    	        					
								    <?php  }
								      
								       }?>

								
			        			
			        											
								} 
							},
						
						{
						name: 'f_text_<?php echo $row['RRN'] ?>',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: <?php echo j($txt_bl); ?>						
					  }
					
					
				]},
 		    	<?php }?>
 		    	   {
				 	xtype: 'fieldcontainer',
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					<?php 
					$sql = " SELECT COUNT(*) AS C_ROW
					         FROM {$cfg_mod_DeskArt['file_fsc']} FA
					         WHERE FADT = '{$id_ditta_default}' AND FAART = '{$m_params->c_art}'";
					
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					$row = db2_fetch_assoc($stmt);
					
					if ($row['C_ROW'] > 0)
					    $img_com_name = "icon-tag-16";
				    else
				        $img_com_name = "icon-tag_grey-16";
					
					
					?>
 		    	, {
					xtype: 'button',
					margin : '0 5 0 0',
					iconCls: '<?php echo $img_com_name; ?>',
					scale: 'small',
					handler : function(){
					
					<?php if(isset($m_params->list_selected_art) && count($m_params->list_selected_art) > 0){ ?>
						//multiselezione: consento di definire una nuova FSC 
						//e poi modificarla/inserirla sugli articoli selezionati
						
						 var my_listeners = {
						 	afterOkSave: function(from_win, jsonData){
						 		if (jsonData.success){
    						 		from_win.destroy();						 	
    						 		if (jsonData.ar_esito){
    						 			acs_show_msg_info('Numero schede modificate: ' + jsonData.ar_esito.UPD + '. Numero schede inserite: ' + jsonData.ar_esito.INS);
    						 		}
    						 	}						 		
						 	}
						 };
						
						 acs_show_win_std('Inserimento / Modifica multipla FSC', 
    		    				    'acs_anag_art_fsc.php?fn=open_mod', 
    		    					{   list_art: <?php echo acs_je($m_params->list_selected_art); ?>
    		    					  , l_insert: <?php echo j($m_params->l_insert)?>}, 500, 550,
    	        					my_listeners, 'icon-tag-16');
					
					<?php } else { ?>					
						//sono su un singolo articolo: mostro grid con elenco FSC abbinate
					    acs_show_win_std('<?php echo "FSC articolo {$m_params->c_art}"; ?>', 
    		    				    'acs_anag_art_fsc.php?fn=open_tab', 
    		    					{c_art: <?php echo j($m_params->c_art); ?>}, 1200, 400,
    	        					null, 'icon-tag-16');
    	        	<?php } ?>				
					
					}
					
					},{
						name: 'f_text_fsc',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: '[FSC] FSC anagrafica articoli'						
					  }
					
					]}
	
 		    	
 		    ], listeners : {
 		    	afterrender : function(comp){
 		    	 <?php if(trim($_SESSION['cod_art']) != "" && $m_params->l_insert == 'Y'){?>
 		    	    var articolo = comp.c_articolo;
 		    	    comp.up('window').setTitle("Seleziona scheda [schede articolo " + <?php echo j($_SESSION['cod_art']); ?> +"]");
 		    	 <?php }?>
 		    	}			
 		    
 		    },
 		     	dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items:[
                  {
					name: 'f_accantona',
					xtype: 'button',
					text: 'Accantona per duplica',
					style:'border: 1px solid gray;',
				    anchor: '-15',		
				    handler: function() {
				       var loc_win = this.up('window');
				        Ext.Ajax.request({
                			   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=session_copy',
                			   method: 'POST',
                			   jsonData: {
                			      cod_art: <?php echo j($m_params->c_art); ?>}, 
                			   
                			   success: function(response, opts) {
                			   	 loc_win.close();
                			   }, 
                			   failure: function(response, opts) {
                			      Ext.getBody().unmask();
                			      alert('error');
                			   }
                			});	
		               }						
				  }
                
                	]
              
                }]           
		
 	 }
	
]}

<?php

exit;
}

if ($_REQUEST['fn'] == 'session_copy'){
    if (is_null( $_SESSION['regola_config']))
        $_SESSION['cod_art'] = "";
        
        $_SESSION['cod_art'] = $m_params->cod_art;
        exit;
}
	
