<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));


$m_table_config = array(
    
    'FILLER' => array(
        'ch_collo' 	=> array(
            "start" => 0,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'nr_collo' 	=> array(
            "start" => 1,
            "len"   => 7,
            "riempi_con" => ""
        ),
        'gr_collo' 	=> array(
            "start" => 8,
            "len"   => 3,
            "riempi_con" => ""
        )
    )
);

function genera_filler($m_table_config, $values){
    $value_attuale = $values->filler;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-96s", "");
        
        //$new_value = "";
        foreach($m_table_config['FILLER'] as $k => $v){
            if(isset($values->$k)){
          
                if($k == 'nr_collo')
                    $chars = number_to_text($values->$k, 3, 4);
                else  
                    $chars = $values->$k;
                
                    $len = "%-{$v['len']}s";
                    $chars = substr(sprintf($len, $chars), 0, $v['len']);
                    $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                    $value_attuale = $new_value;
                
                
            }
            
        }
             
        return $value_attuale;
}

if ($_REQUEST['fn'] == 'exe_stor'){
    $m_params = acs_m_params_json_decode();
    $ar_upd = array();
    $stor = trim($m_params->row->DBFEXP);
    
    if($stor == "" || $stor == "V")
        $new_value = "S";
    else
        $new_value = "";
            
            
    $ar_upd['DBUSUM'] = $auth->get_user();
    $ar_upd['DBDTUM'] = oggi_AS_date();
    $ar_upd['DBFEXP'] = $new_value;

    
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_distinta_base']} DB
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    WHERE RRN(DB) = '{$m_params->row->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}


if ($_REQUEST['fn'] == 'exe_check_val'){
    $m_params = acs_m_params_json_decode();
    $ar_upd = array();
    $stor = trim($m_params->row->DBFEXP);
    
    if($stor == "" || $stor == "S")
        $new_value = "V";
    else
        $new_value = "";
            
            
    $ar_upd['DBUSUM'] = $auth->get_user();
    $ar_upd['DBDTUM'] = oggi_AS_date();
    $ar_upd['DBFEXP'] = $new_value;
    $val_ini = $m_params->form_values->f_val_ini;
    if(isset($val_ini) && strlen($val_ini) > 0){
        $ar_upd['DBAAIG'] 	= substr($val_ini, 0, 4);
        $ar_upd['DBMMIG'] 	= substr($val_ini, 4, 2);
        $ar_upd['DBGGIG'] 	= substr($val_ini, 6, 2);
    }
    
    $val_fin = $m_params->form_values->f_val_fin;
    if(isset($val_fin) && strlen($val_fin) > 0){
        $ar_upd['DBAAFG'] 	= substr($val_fin, 0, 4);
        $ar_upd['DBMMFG'] 	= substr($val_fin, 4, 2);
        $ar_upd['DBGGFG'] 	= substr($val_fin, 6, 2);
    }
    
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_distinta_base']} DB
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(DB) = '{$m_params->row->rrn}'";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);

    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}


if ($_REQUEST['fn'] == 'exe_new_art'){
    $m_params = acs_m_params_json_decode();
    $ret = array();
    
    $sql = "SELECT ARUMTE, ARUMCO, ARUMAL, ARDART FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$m_params->codice}'";
       
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    if(trim($row['ARUMCO']) == trim($row['ARUMTE']) && trim($row['ARUMCO']) == trim($row['ARUMAL'])){
        $ar_um[] = array('id' => trim($row['ARUMTE']));
    } elseif((trim($row['ARUMAL']) == trim($row['ARUMTE']) && trim($row['ARUMAL']) != trim($row['ARUMCO'])) ||
        (trim($row['ARUMAL']) == trim($row['ARUMCO']) && trim($row['ARUMAL']) != trim($row['ARUMTE']))){
        $ar_um[] = array('id' => trim($row['ARUMTE']));
        $ar_um[] = array('id' => trim($row['ARUMCO']));
    } else{
        $ar_um[] = array('id' => trim($row['ARUMTE']));
        $ar_um[] = array('id' => trim($row['ARUMCO']));
        $ar_um[] = array('id' => trim($row['ARUMAL']));
     }
    
    
    $row['u_m'] = $ar_um;
    $row['ARUMTE'] = trim($row['ARUMTE']);
        
    $ret['success'] = true;
    $ret['rec'] = $row;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// EXE SAVE DISTINTA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_distinta'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
    
    $ar_upd['DBUSUM'] 	= $auth->get_user();
    $ar_upd['DBDTUM'] 	= oggi_AS_date();
    $ar_upd['DBUM'] 	= $form_values->u_m;
    $ar_upd['DBQTA'] 	= sql_f($form_values->quant);
    
    if($m_params->tipo_dist == 'P'){
        $ar_upd['DBART'] 	= $m_params->c_art;
        $ar_upd['DBCOMP'] 	= $form_values->cod_comp;
        $ar_upd['DBSEQR'] 	= $form_values->rip;
        
        
    }else{
        $ar_upd['DBART'] 	= $form_values->dbart_p;
        $ar_upd['DBCOMP'] 	= $m_params->c_art;
        $ar_upd['DBSEQR'] 	= $form_values->rip;
        
    }
    
    
    //larghezza
    $ar_upd['DBDIM1']   = sql_f($form_values->dim1);
    if(isset($form_values->var_dim1))
        $ar_upd['DBVDI1']   = $form_values->var_dim1; //variabile dim1
    $ar_upd['DBRDI1']   = $form_values->rip_dim1; //riporto dim1
    $ar_upd['DBQDI1']   = $form_values->qta_dim1; //dimensione in qta 1
    //lunghezza
    $ar_upd['DBDIM2']   = sql_f($form_values->dim2);
    if(isset($form_values->var_dim2))
        $ar_upd['DBVDI2']   = $form_values->var_dim2; //variabile dim1
    $ar_upd['DBRDI2']   = $form_values->rip_dim2; //riporto dim1
    $ar_upd['DBQDI2']   = $form_values->qta_dim2; //dimensione in qta 1
    //spessore
    $ar_upd['DBDIM3']   = sql_f($form_values->dim3);
    if(isset($form_values->var_dim3))
        $ar_upd['DBVDI3']   = $form_values->var_dim3; //variabile dim1
    $ar_upd['DBRDI3']   = $form_values->rip_dim3; //riporto dim1
    $ar_upd['DBQDI3']   = $form_values->qta_dim3; //dimensione in qta 1
    
    
    if (isset($form_values->DBVRV1)){
        $ar_upd['DBVRV1'] 	= $form_values->DBVRV1;
        $ar_upd['DBVNV1'] 	= $form_values->DBVNV1;
    }    
     
    if (isset($form_values->DBVRV2)){
        $ar_upd['DBVRV2'] 	= $form_values->DBVRV2;    
        $ar_upd['DBVNV2'] 	= $form_values->DBVNV2;
    }
        
    $ar_upd['DBVARQ'] 	= v_or_blank($form_values->var_qta);
    $ar_upd['DBSWVQ'] 	= v_or_blank($form_values->s_m_c);    
    $ar_upd['DBPO10'] 	= v_or_blank($form_values->qta_10);
    $ar_upd['DBSWAR'] 	= v_or_blank($form_values->arr);
    
    $ar_upd['DBRGCO'] 	= v_or_blank($form_values->r_codifica); 
    $ar_upd['DBTPAL'] 	= $form_values->controllo; 
 
    $val_ini = $form_values->val_ini;
    if(isset($val_ini) && strlen($val_ini) > 0){
        $ar_upd['DBAAIG'] 	= substr($val_ini, 0, 4);
        $ar_upd['DBMMIG'] 	= substr($val_ini, 4, 2);
        $ar_upd['DBGGIG'] 	= substr($val_ini, 6, 2);
    }
    
    $val_fin = $form_values->val_fin;
    if(isset($val_fin) && strlen($val_fin) > 0){
        $ar_upd['DBAAFG'] 	= substr($val_fin, 0, 4);
        $ar_upd['DBMMFG'] 	= substr($val_fin, 4, 2);
        $ar_upd['DBGGFG'] 	= substr($val_fin, 6, 2);
    }
    
    $ar_upd['DBSCAR'] 	= sql_f($form_values->scarto);
    $ar_upd['DBSCAQ'] 	= sql_f($form_values->v_scarto); 
    
    $filler = genera_filler($m_table_config, $form_values);
    $ar_upd['DBFIL1'] 	= $filler;
    
    /*print_r($filler);
    exit;*/
   
       
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_distinta_base']} DB
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    WHERE RRN(DB) = '{$form_values->rrn}' ";
            
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// EXE AGGIUNGI distinta
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi_distinta'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
    $ar_ins['DBUSUM'] 	= $auth->get_user();
    $ar_ins['DBDTUM'] 	= oggi_AS_date();
    $ar_ins['DBUSGE'] 	= $auth->get_user();
    $ar_ins['DBDTGE'] 	= oggi_AS_date();
    $ar_ins['DBDT'] 	= $id_ditta_default;
    $ar_ins['DBSEQ1'] 	= $form_values->seq1;
    $ar_ins['DBSEQ2'] 	= $form_values->seq2;
    
    
    $ar_ins['DBQTA'] 	= $form_values->quant;
    $ar_ins['DBUM'] 	= $form_values->u_m;
    $ar_ins['DBVRV1'] 	= $form_values->DBVRV1;
    $ar_ins['DBVRV2'] 	= $form_values->DBVRV2;
    $ar_ins['DBVNV1'] 	= $form_values->DBVNV1;
    $ar_ins['DBVNV2'] 	= $form_values->DBVNV2;
    $ar_ins['DBRGCO'] 	= v_or_blank($form_values->r_codifica); 
    $ar_ins['DBTPAL'] 	= $form_values->controllo; 
    $ar_ins['DBVARQ'] 	= v_or_blank($form_values->var_qta);
    $ar_ins['DBSWVQ'] 	= v_or_blank($form_values->s_m_c);
    $ar_ins['DBPO10'] 	= v_or_blank($form_values->qta_10);
    $ar_ins['DBSWAR'] 	= v_or_blank($form_values->arr);
    
    //larghezza
    $ar_ins['DBDIM1']   = sql_f($form_values->dim1);
    if(isset($form_values->var_dim1))
        $ar_ins['DBVDI1']   = $form_values->var_dim1; //variabile dim1
    $ar_ins['DBRDI1']   = $form_values->rip_dim1; //riporto dim1
    $ar_ins['DBQDI1']   = $form_values->qta_dim1; //dimensione in qta 1
    //lunghezza
    $ar_ins['DBDIM2']   = sql_f($form_values->dim2);
    if(isset($form_values->var_dim2))
        $ar_ins['DBVDI2']   = $form_values->var_dim2; //variabile dim1
    $ar_ins['DBRDI2']   = $form_values->rip_dim2; //riporto dim1
    $ar_ins['DBQDI2']   = $form_values->qta_dim2; //dimensione in qta 1
    //spessore
    $ar_ins['DBDIM3']   = sql_f($form_values->dim3);
    if(isset($form_values->var_dim3))
        $ar_ins['DBVDI3']   = $form_values->var_dim3; //variabile dim1
    $ar_ins['DBRDI3']   = $form_values->rip_dim3; //riporto dim1
    $ar_ins['DBQDI3']   = $form_values->qta_dim3; //dimensione in qta 1
    
    
    $ar_ins['DBSCAR'] 	= sql_f($form_values->scarto);
    $ar_ins['DBSCAQ'] 	= sql_f($form_values->v_scarto);
    
    $val_ini = $form_values->val_ini;
    if(isset($val_ini) && strlen($val_ini) > 0){
        $ar_ins['DBAAIG'] 	= substr($val_ini, 0, 4);
        $ar_ins['DBMMIG'] 	= substr($val_ini, 4, 2);
        $ar_ins['DBGGIG'] 	= substr($val_ini, 6, 2);
    }
    
    $val_fin = $form_values->val_fin;
    if(isset($val_fin) && strlen($val_fin) > 0){
        $ar_ins['DBAAFG'] 	= substr($val_fin, 0, 4);
        $ar_ins['DBMMFG'] 	= substr($val_fin, 4, 2);
        $ar_ins['DBGGFG'] 	= substr($val_fin, 6, 2);
    }
    
    $filler = genera_filler($m_table_config, $form_values);
 
    $ar_ins['DBFIL1'] 	= $filler;
    
    if($m_params->tipo_dist == 'P'){
        $ar_ins['DBART'] 	= $m_params->c_art;
        $ar_ins['DBCOMP'] 	= $form_values->cod_comp;
        $ar_ins['DBSEQR'] 	= $form_values->rip;
        
        
    }else{
        $ar_ins['DBART'] 	= $form_values->dbart_p;
        $ar_ins['DBCOMP'] 	= $m_params->c_art;
        $ar_ins['DBSEQR'] 	= $form_values->rip;
   
    }
     
        
    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_distinta_base']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
                            
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// EXE DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB
    WHERE RRN(DB) = '{$form_values->rrn}' ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $error_msg =  db2_stmt_errormsg($stmt);
 
    //record DV ******************
    $sql_s = "SELECT COUNT(*) AS C_ROWS FROM {$cfg_mod_DeskUtility['file_condizioni_distinta']} DV
              WHERE DVDT = '{$id_ditta_default}' AND DVART = '{$m_params->c_art}' AND DVSEQ1 = '{$form_values->seq1}' AND DVSEQ2 ='{$form_values->seq2}' 
              AND DVCOMP = '{$form_values->cod_comp}' AND DVSEQR = '{$form_values->rip}'";
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_s);
    $row = db2_fetch_assoc($stmt_s);
    
    if($row['C_ROWS'] > 0){
    
        $sql_dv = "DELETE FROM {$cfg_mod_DeskUtility['file_condizioni_distinta']} DV
                   WHERE DVDT = '{$id_ditta_default}' AND DVART = '{$m_params->c_art}' AND DVSEQ1 = '{$form_values->seq1}' AND DVSEQ2 ='{$form_values->seq2}' 
                   AND DVCOMP = '{$form_values->cod_comp}' AND DVSEQR = '{$form_values->rip}'";
        
        
        $stmt_dv = db2_prepare($conn, $sql_dv);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_dv);
        $error_msg =  db2_stmt_errormsg($stmt_dv);
    }
    
    
    
    $ret = array();
    $ret['success'] = $result;
    if (strlen($error_msg) > 0) {
        $ret['success'] = false;
        $ret['message'] = $error_msg;
    }
    echo acs_je($ret);
    exit;
}





// ******************************************************************************************
//DUPLICA DISTINTA, SCRIVO RI0
if ($_REQUEST['fn'] == 'exe_dup_dist'){
// ******************************************************************************************
    
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    $ar_value = array();
    
    //distinta iniziale modificata
    $dist = "";
    for($i = 0; $i <= 14; $i++){
        $filtro = 'f_a_'.$i;
        if($form_values->$filtro != '')
            $dist .= $form_values->$filtro;
    }

    //componenti modificati
    foreach($m_params->list_comp as $k => $v){
        $art = "";
        for($i = 0; $i <= 14; $i++){
            $filtro = 'f_'.$k.'_'.$i;
            if($form_values->$filtro != '')
                $art .= $form_values->$filtro;
        }
      
        $ar_value[]=$art;
        
    }
    
    foreach($ar_value as $v){
        
        //articolo vecchio  = $m_params->c_art
        //articolo nuovo = $dist
        //comp vecchi  = $m_params->list_comp
        //comp nuovi = $v
        /*$sh = new SpedHistory($m_DeskArt);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'DUP_DIST_ART',
                "vals" => array(
                    "RICDOLD" 	=> $m_params->c_art,
                    "RICDNEW" 	=> $dist,
                    "RIARFO"    => $v,
                   // "RIDART" 	=> $m_params->form_values->f_desc_art,
                    "RIDT"  	=> $id_ditta_default,
                ),
            )
            );*/
    }
    
 $ret = array(); 
 $ret['success'] = true;
 echo acs_je($ret);
 exit;
}



// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
// ******************************************************************************************    
    $m_params = acs_m_params_json_decode();    
    $c_art = $m_params->open_request->c_art;
    
    $tipo_dist = $m_params->open_request->tipo_dist;
    
    if($tipo_dist == 'P'){
        
        $order = "ORDER BY DB.DBSEQ1, DB.DBSEQ2, DB.DBCOMP, DB.DBSEQR";
        $sql_where = " AND DB.DBART = '{$c_art}'";
        $sql_select = ", DB_COMP.NR_DB AS NR_COMP, DB_COMP.DBART AS ART_COMP, ATT.N_ATT AS N_ATT";
        $sql_join = "
            /*COMPONENTI*/
            LEFT OUTER JOIN (
               SELECT COUNT(*) AS NR_DB, DB2.DBART
               FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB2
               WHERE DB2.DBDT = '{$id_ditta_default}'
               GROUP BY DB2.DBART) DB_COMP
            ON DB.DBCOMP = DB_COMP.DBART
             /*ATTAV*/
            LEFT OUTER JOIN ( 
                SELECT COUNT(*) AS N_ATT, ASDT, ASDOCU FROM {$cfg_mod_DeskArt['file_assegna_ord']}
                    GROUP BY ASDT, ASDOCU) ATT 
                ON ATT.ASDT = DB.DBDT AND ATT.ASDOCU = DB.DBCOMP";
    }else{
        $order = "ORDER BY DB.DBSEQ1, DB.DBSEQ2, DB.DBART, DB.DBSEQR";
        $sql_where = " AND DB.DBCOMP = '{$c_art}'";
        $sql_select = ", DB_COMP.NR_DB AS NR_COMP, DB_COMP.DBART AS ART_COMP, ATT.N_ATT AS N_ATT"; 
        $sql_join = "
                /*COMPONENTI*/
                LEFT OUTER JOIN (
                    SELECT COUNT(*) AS NR_DB, DB2.DBART
                    FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB2
                    WHERE DB2.DBDT = '{$id_ditta_default}'
                    GROUP BY DB2.DBART) DB_COMP
                ON DB.DBART = DB_COMP.DBART
                /*ATTAV*/
                LEFT OUTER JOIN ( 
                SELECT COUNT(*) AS N_ATT, ASDT, ASDOCU FROM {$cfg_mod_DeskArt['file_assegna_ord']}
                    GROUP BY ASDT, ASDOCU) ATT 
                ON ATT.ASDT = DB.DBDT AND ATT.ASDOCU = DB.DBART";
    }

    
    $sql = "SELECT RRN(DB) AS RRN, DB.*, AR_ART.ARDART AS D_ART,  AR_ART.ARUMTE AS UM_ART,
            AR_COMP.ARDART AS D_COMP, DV.NR_COND AS COND, RL.C_ROW AS COMM, RL.RLRIFE2 AS BL_N
             {$sql_select}, AR_COMP.ARUMTE AS UMTE, AR_COMP.ARUMCO AS UMCO, AR_COMP.ARUMAL AS UMAL
            FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR_ART
                ON DB.DBDT = AR_ART.ARDT AND DB.DBART = AR_ART.ARART
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR_COMP
                ON DB.DBDT = AR_COMP.ARDT AND DB.DBCOMP = AR_COMP.ARART
            {$sql_join}
             LEFT OUTER JOIN (
               SELECT COUNT(*) AS NR_COND, DVSEQ1, DVSEQ2, DVCOMP, DVSEQR
               FROM {$cfg_mod_DeskUtility['file_condizioni_distinta']} 
               WHERE DVDT = '{$id_ditta_default}' AND DVART = '{$c_art}'
               GROUP BY DVSEQ1, DVSEQ2, DVCOMP, DVSEQR) DV
            ON DB.DBSEQ1 = DV.DVSEQ1 AND DB.DBSEQ2 = DV.DVSEQ2 AND DB.DBCOMP = DV.DVCOMP AND DB.DBSEQR = DV.DVSEQR  
            LEFT OUTER JOIN (
                SELECT COUNT(*) AS C_ROW, RLRIFE1, RLRIFE2
                FROM {$cfg_mod_DeskUtility['file_note_anag']}
                WHERE RLDT = '{$id_ditta_default}' AND RLTPNO = 'DB' 
                AND RLRIFE2 IN ('*NT', '*NC', '*NA')
                GROUP BY RLRIFE1, RLRIFE2
               ) RL
	        ON RL.RLRIFE1 = CONCAT(DB.DBART, CONCAT(DB.DBSEQ1, CONCAT(DB.DBSEQ2, CONCAT(DB.DBCOMP, DB.DBSEQR))))
       
            WHERE DB.DBDT = '{$id_ditta_default}' $sql_where
            {$order}";
            
             
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
            $sql_a = "SELECT ARSOSP, ARESAU FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
                      WHERE ARDT = '{$id_ditta_default}' AND ARART = ?";
            
            $stmt_a = db2_prepare($conn, $sql_a);
            echo db2_stmt_errormsg();
            
            
            while($row = db2_fetch_assoc($stmt)){
                $nr = array();
                $nr['seq1']         = trim($row['DBSEQ1']);
                $nr['seq2']         = trim($row['DBSEQ2']);
                $nr['rip']          = trim($row['DBSEQR']);
                $nr['comp']         = $row['NR_COMP'];
                $nr['art_comp']     = $row['ART_COMP'];
                $nr['cond']         = $row['COND'];
                
                //accendo le condizini se ci sono quello su DB 
                for($i = 1; $i <= 4 ; $i ++)
                    if (strlen(trim($row["DBVRS{$i}"])) > 0) $nr['cond'] += 1;

                
                $nr['comm']         = $row['COMM'];
                $nr['blocco']       = $row['BL_N'];
                $nr['rife1']        = $row['DBART'].$row['DBSEQ1'].$row['DBSEQ2'].$row['DBCOMP'].$row['DBSEQR'];
                $nr['dbart_p']      = trim($row['DBART']);
                
                $nr['cod_comp']     = trim($row['DBCOMP']);
                $nr['desc_comp']    = $row['D_COMP'];
                $nr['desc_dbart']   = $row['D_ART'];
                $nr['quant']        = $row['DBQTA'];
                $nr['data_imm']     = $row['DBDTUM'];
                $nr['ut_imm']       = $row['DBUSUM'];
                if(trim($row['DBUM']) =='')
                    $nr['u_m'] 	= trim($row['UM_ART']);
                else
                    $nr['u_m'] 	= trim($row['DBUM']);
                
                $ar_um = array();
                if(trim($row['UMCO']) == trim($row['UMTE']) && trim($row['UMCO']) == trim($row['UMAL'])){
                    $ar_um[] = array('id' => trim($row['UMTE']));
                } elseif((trim($row['UMAL']) == trim($row['UMTE']) && trim($row['UMAL']) != trim($row['UMCO'])) ||
                    (trim($row['UMAL']) == trim($row['UMCO']) && trim($row['UMAL']) != trim($row['UMTE']))){
                        $ar_um[] = array('id' => trim($row['UMTE']));
                        $ar_um[] = array('id' => trim($row['UMCO']));
                } else{
                    $ar_um[] = array('id' => trim($row['UMTE']));
                    $ar_um[] = array('id' => trim($row['UMCO']));
                    $ar_um[] = array('id' => trim($row['UMAL']));
                }
                
                
                $nr['ar_um'] = $ar_um;
                    
                    
                $nr['rrn']          = $row['RRN'];
                $nr['attav']        = $row['N_ATT'];
                if($tipo_dist == 'P')
                    $nr['tooltip']      =  get_att_from_art(trim($row['DBCOMP']));
                else
                    $nr['tooltip']      =  get_att_from_art(trim($row['DBART']));
                   
                $nr['var_qta']      = $row['DBVARQ'];
                $nr['qta_10']       = $row['DBPO10'];
                $nr['s_m_c']        = $row['DBSWVQ'];
                $nr['arr']          = $row['DBSWAR'];
                $nr['qta_tot']      = "";
                $nr['i_e']          = $row['DBSWIE'];
                $nr['variabile']    = $row['DBVRS1'];
                $nr['variante']     = $row['DBVNS1'];
                $nr['tip1']         = $row['DBOPS1'];
                $nr['tip2']         = $row['DBOPS2'];
                $nr['o_s']          = $row['DBOSS1'];
                $nr['ind_espl']     = $row['DBSWES'];
                $nr['qta_ass']      = $row['DBQTAS'];
                //larghezza
                $nr['dim1']         = $row['DBDIM1']; //larghezza
                $nr['var_dim1']     = trim($row['DBVDI1']); //variabile dim1
                $nr['rip_dim1']     = trim($row['DBRDI1']); //riporto dim1
                $nr['qta_dim1']     = trim($row['DBQDI1']); //dimensione in qta 1
                //lunghezza
                $nr['dim2']         = $row['DBDIM2']; //lunghezza
                $nr['var_dim2']     = trim($row['DBVDI2']); //variabile dim1
                $nr['rip_dim2']     = trim($row['DBRDI2']); //riporto dim1
                $nr['qta_dim2']     = trim($row['DBQDI2']); //dimensione in qta 1
                //spessore
                $nr['dim3']         = $row['DBDIM3']; //spessore
                $nr['var_dim3']     = trim($row['DBVDI3']); //variabile dim3
                $nr['rip_dim3']     = trim($row['DBRDI3']); //riporto dim3
                $nr['qta_dim3']     = trim($row['DBQDI3']); //dimensione in qta 3
                 
                $nr['r_codifica']  = trim($row['DBRGCO']);
                $nr['controllo']   = trim($row['DBTPAL']);
                $nr['DBVRV1']      = trim($row['DBVRV1']);
                $nr['DBVNV1']      = trim($row['DBVNV1']);
                $nr['DBVRV2']      = trim($row['DBVRV2']);
                $nr['DBVNV2']      = trim($row['DBVNV2']);
                $nr['DBFEXP']      = trim($row['DBFEXP']);
                
                if($tipo_dist == 'F')
                    $result = db2_execute($stmt_a, array($row['DBART']));
                else
                    $result = db2_execute($stmt_a, array($row['DBCOMP']));
                
                $row_a = db2_fetch_assoc($stmt_a);
                
                $nr['sosp']        = trim($row_a['ARSOSP']);
                $nr['esau']        = trim($row_a['ARESAU']);
                
                $nr['scarto']      = $row['DBSCAR'];
                $nr['v_scarto']    = $row['DBSCAQ'];
                
                $nr['filler']      = $row['DBFIL1'];
                $nr['ch_collo']    = substr($row['DBFIL1'], 0, 1);
                $value_nr_collo    = substr($row['DBFIL1'], 1, 7);
                
                $nr['nr_collo']    = text_to_number($value_nr_collo, 3, 4);
                
                $nr['gr_collo']    = trim(substr($row['DBFIL1'], 8, 3));
                
                $nr['val_ini'] = sprintf("%02s",  $row['DBGGIG']) . sprintf("%02s",$row['DBMMIG']) . sprintf("%04s",$row['DBAAIG']);
                $nr['val_fin'] = sprintf("%02s",  $row['DBGGFG']) . sprintf("%02s",$row['DBMMFG']) . sprintf("%04s",$row['DBAAFG']);
                
                
                if($row['DBSCAR'] > 0 || $row['DBSCAQ'] > 0){
                    $nr['qta_scarto'] = 'Y';
                    $nr['t_scarto'] = "Scarto % " .n($row['DBSCAR'], 2).", valore ".n($row['DBSCAQ'], 2);
                }
                
                $ar[] = $nr;
            }
            
            echo acs_je($ar);
            exit;
}






// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
// ******************************************************************************************
    $m_params = acs_m_params_json_decode();
    
    ?>


{"success":true, "items": [

        {
			xtype: 'panel',
			flex : 1,
			autoScroll : true,
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
					
				{
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            flex:0.7,
	            autoScroll : true,
	            title: '',
				items: [
						{
						xtype: 'grid', 
						title: '',
						flex:0.7,
						height : 400,
				        loadMask: true,	
				        multiSelect : true,
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['tooltip', 'attav', 'rrn', 'seq1', 'seq2', 'cod_comp', 'desc_comp', 'desc_dbart', 
		        					'quant', 'u_m', 'dbart_p', 'rip', 'data_imm', 'ut_imm','var_qta', 'qta_10', 's_m_c',  
		        					'arr', 'qta_tot', 'i_e', 'variabile', 'variante', 'tip1', 'tip2', 'o_s', 'ind_espl', 
		        					'qta_ass', 'dim1', 'var_dim1', 'rip_dim1', 'qta_dim1', 'dim2', 'var_dim2', 
		        					'rip_dim2', 'qta_dim2', 'dim3', 'var_dim3', 'DBFEXP', 'ar_um',
		        					'rip_dim3', 'qta_dim3', 'cond', 'controllo', 'r_codifica', 'qta_scarto', 't_scarto',
		        					'DBVRV1', 'DBVNV1', 'DBVRV2', 'DBVNV2', 'comm', 'blocco', 'rife1', 'comp', 'art_comp',
		        					'sosp', 'esau', 'scarto', 'v_scarto', 'filler', 'ch_collo', 'nr_collo', 'gr_collo',
		        					'val_ini', 'val_fin', 'DBAAIG', 'DBMMIG', 'DBGGIG', 'DBAAFG', 'DBMMFG', 'DBGGFG'
		        					]							
									
			}, //store
				<?php $attav = "<img src=" . img_path("icone/48x48/arrivi.png") . " height=20>"; ?>
 				<?php $cond = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
 				<?php $comm = "<img src=" . img_path("icone/48x48/comment_light.png") . " height=20>"; ?>
 				<?php $comp = "<img src=" . img_path("icone/48x48/folder_download.png") . " height=20>"; ?>
 				<?php $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
 				  
			      columns: [
			      {text: '<?php echo $sosp; ?>', 	
    				width: 30, 
    				dataIndex: 'sosp',
    				tooltip: 'Sospeso',		        			    	     
    		    	renderer: function(value, metaData, record){
		    			  	if(record.get('sosp') == 'S'){ 
    		    			   metaData.tdCls += ' sfondo_rosso';
    		    			   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Sospeso') + '"';
    		    			   return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
    		    		 	}else{
    		    		 	
    		    		 	  if(record.get('DBFEXP') == 'S'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Storicizzato') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=15>';}
					   			
					   		  if(record.get('DBFEXP') == 'V'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Controllo validit�') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/calendar.png") ?> width=15>';}
    		    		 	
		    		 		  if (record.get('esau') == 'E'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In esaurimento') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/clessidra.png") ?> width=15>';}
							  if (record.get('esau') == 'A'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In avviamento') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';}
				    		  if (record.get('esau') == 'C'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In codifica') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=15>';}
    		    		 	  if (record.get('esau') == 'R'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Riservato') + '"';
					   			return '<img src= <?php echo img_path("icone/48x48/folder_private.png") ?> width=15>';}
				   			  if (record.get('esau') == 'F'){	    			    	
			   		   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Fine codifica') + '"';
			   		   			return '<img src= <?php echo img_path("icone/48x48/button_blue_play.png") ?> width=15>';}
    		    		 	}
    		    		 
    		    		  }
    		        },
			      
			       {text: '<?php echo $comm; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'comm',
    				tooltip: 'Attivit&agrave;',		        			    	     
    		    	renderer: function(value, metaData, record, row, col, store, gridView){
    		    			  if(record.get('comm') > 0) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
    		    			  else return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
    		    		 }
    		        },
			      
			       {header: 'Sequenza',
    			       columns: [
    			       		{
        	                header   : '1',
        	                dataIndex: 'seq1',
        	                width: 50
        	               
        	                },	      {
        	                header   : '2',
        	                dataIndex: 'seq2',
        	                width: 30
        	               
        	                },			       
                        ]},
			      	
			   
	                <?php if($m_params->tipo_dist == 'P'){?>
	                 {
	                header   : 'Articolo',
	                dataIndex: 'cod_comp',
	                width: 110,
	                renderer: function(value, metaData, record){
        				 metaData.tdCls += ' auto-height';	
        				 if(record.get('r_codifica').trim() != '') metaData.tdCls += ' sfondo_giallo';
          	  			
        				return value;	
						}
	                }, {
	                header   : 'R',
	                tooltip : 'Ripetizione componente', 
	                dataIndex: 'rip',
	                width: 30
	                },{
	                header   : 'Descrizione',
	                dataIndex: 'desc_comp',
	                flex: 1,    
	                renderer: function(value, metaData, record){
        			   if(record.get('controllo') == 'S') metaData.tdCls += ' sfondo_verde';
        			 
        				return value;	
						}
	                },
	                
	                <?php }else{?>
	                    {
	                header   : 'Articolo',
	                dataIndex: 'dbart_p',
	                width: 110,
	                renderer: function(value, metaData, record){
        				 metaData.tdCls += ' auto-height';	
          	  			 if(record.get('r_codifica') != '') metaData.tdCls += ' sfondo_giallo';
        				return value;	
						}
	                },
	                {
	                header   : 'R',
	                tooltip : 'Ripetizione componente', 
	                dataIndex: 'rip',
	                width: 30
	                },{
	                header   : 'Descrizione',
	                dataIndex: 'desc_dbart',
	                flex: 1,
	                renderer: function(value, metaData, record){
        			   if(record.get('controllo') == 'S') metaData.tdCls += ' sfondo_verde';
          	  			
        				return value;	
						}
	                },
	                
	               <?php }?>
	               
	                {text: '<?php echo $attav; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'attav',
    				tooltip: 'Attivit&agrave;',		        			    	     
    		    	renderer: function(value, metaData, record, row, col, store, gridView){
	    			  if(record.get('attav') > 0){
	    			  
		 				metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip')) + '"';
		 				
		 				return '<img src=<?php echo img_path("icone/48x48/arrivi.png") ?> width=15>';
	    			  
	    			  }
    		    			  
    		    			   
    		    		  }
    		        }
	            
	                ,{
	                header   : 'UM',
	                dataIndex: 'u_m',
	                width: 40
	                } ,{
	                header   : 'Quantit&agrave;',
	                dataIndex: 'quant',
	                width: 60,
	                renderer: function(value, metaData, record, row, col, store, gridView){
    		    			  if(record.get('qta_scarto') == 'Y'){
    		    			    metaData.tdCls += ' sfondo_giallo';
    		    			    metaData.tdCls += ' grassetto';
	    		 				metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_scarto')) + '"';
	    		 			  }
    		    			  
    		    			  return  floatRenderer4(value);
    		    		  }
	                },
	               {header: 'Immissione',
    			       columns: [
			       		{
    	                header   : 'Data',
    	                dataIndex: 'data_imm',
    	                width: 70,
    	                 renderer : date_from_AS
    	                }  ,{
    	                header   : 'Utente',
    	                dataIndex: 'ut_imm',
    	                width: 80
    	                }   			       
                    ]},
			     {text: '<?php echo $cond; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'cond',
    			    filter: {type: 'string'}, filterable: true,
    				tooltip: 'Condizioni di elaborazione componenti',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('cond') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_blue_yuan.png") ?> width=15>';
    		    		  }
    		        },
    		        
    		   
	              {text: '<?php echo $comp; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'comp',
    				tooltip: 'Componenti',		        			    	     
    		    	renderer: function(value, metaData, record, row, col, store, gridView){
    		    			  if(record.get('comp') > 0) return '<img src=<?php echo img_path("icone/48x48/folder_download.png") ?> width=15>';
    		    		 }
    		        }
	               
	                         
	                
	                
	         ], listeners: {
	         
	                  
	         selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('form').up('panel').down('#dx_form');
		               //pulisco eventuali filtri
		               form_dx.getForm().reset();
		               //ricarico i dati della form
		               acs_form_setValues(form_dx, selected[0].data);	               
	                   form_dx.getForm().findField('u_m').store.loadData(selected[0].data.ar_um);
	                   form_dx.getForm().findField('u_m').setValue(selected[0].data.u_m);
	                   }
		          }
		          
		          
		         	          
		          , itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					     id_selected = grid.getSelectionModel().getSelection();
					     list_selected_id = [];
					     for (var i=0; i<id_selected.length; i++){ 
					     
					      <?php if($m_params->tipo_dist == 'P'){?>
						 	list_selected_id.push(id_selected[i].get('cod_comp'));
						 <?php }else{?>
						 	list_selected_id.push(id_selected[i].get('dbart_p'));
						 <?php }?>
					     
							
						}	
									
							<?php $funzioni = $m_DeskArt->find_TA_std('ATFUN', 'CHK_DB', 'Y'); ?>
					 		    voci_menu.push({
        			      		text: '<?php echo $funzioni[0]['text']; ?>',
        			    		iconCls: 'icon-arrivi-16',
        			    		handler: function() {
        			    		
        			    		   	my_listeners_inserimento = {
		        					afterInsertRecord: function(from_win){	
		        					   from_win.close();
						        		}
				    				};
        			    		
        			    		acs_show_win_std('Nuovo stato/attivit&agrave;', 
											<?php echo j('acs_anag_art_distinta_atfun.php'); ?>, 
											{	tipo_op: '',
												attav : 'Y', 
												p_art : '<?php echo $m_params->c_art; ?>',
								  				list_selected_id: list_selected_id
								  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
            					}
        					  });
					    
					       <?php 
					     
					       $atfta = $m_DeskArt->find_TA_std('ATFTA', $funzioni[0]['id'], 'Y', 'Y'); 
					 
					         foreach($atfta as $op) {
					             
					             $attav = $m_DeskArt->find_TA_std('ATTAV', $op['TAKEY2'], 'Y', 'Y'); 
                         
					             foreach($attav as $at) {
					             
					           ?>	
                    		  	voci_menu.push({
                    	      		text: <?php echo j($at['text']); ?>,
                    	    		iconCls: 'iconAccept',
                    	    		handler: function() {
                    	    		
                    	    		   	my_listeners_inserimento = {
    		        					afterInsertRecord: function(from_win){	
    		        					   from_win.close();
    						        		}
    				    				};
        			    		
                    	    		
                    	    		acs_show_win_std('Nuovo stato/attivit&agrave;', 
											<?php echo j('acs_anag_art_distinta_atfun.php'); ?>, 
											{	tipo_op: <?php echo j($at['id']); ?>,
												attav : 'N', 
												p_art : '<?php echo $m_params->c_art; ?>',
								  				list_selected_id: list_selected_id
								  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
                    
                    			    }
                    			  });
                    			  
                    			  
                      <?php }  } ?>
                      
                      
                      
                      voci_menu.push({
        	      		text: 'Attiva/Disattiva storicizzazione',
        	    		iconCls : 'icon-divieto-16',         
        	    		handler: function() {
        	    		
    	    				if(rec.get('DBFEXP').trim() == '' || rec.get('DBFEXP').trim() == 'V'){
        	    		
        	    			Ext.Msg.confirm('Richiesta conferma', 'Conferma storicizzazione componente <br> (Verr� ESCLUSO dalla distinta componenti esecutiva)' , function(btn, text){
            	   		    if (btn == 'yes'){
        	    		
            	    		   	  Ext.Ajax.request({
        					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_stor',
        					        method     : 'POST',
        		        			jsonData: {
        		        				row : row
        							},							        
        					        success : function(result, request){
        					          var jsonData = Ext.decode(result.responseText);
        					          grid.getStore().load();	
        		            		   
        		            		},
        					        failure    : function(result, request){
        					            Ext.Msg.alert('Message', 'No data to be loaded');
        					        }
        					    	});	
					    
					    	}
            	   		  });

							}else{
							
							Ext.Ajax.request({
					        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_stor',
					        	method     : 'POST',
		        				jsonData: {
		        				row : row
								},							        
					        	success : function(result, request){
					          		var jsonData = Ext.decode(result.responseText);
					          		grid.getStore().load();	
		            		   
		            			},
					        	failure    : function(result, request){
					            	Ext.Msg.alert('Message', 'No data to be loaded');
					       		 }
					    		});	
							
							}
                
        	    		

        			    }
        			  });
        			  
        			  
        			   voci_menu.push({
        	      		text: 'Attiva/Disattiva controllo data validit&agrave;',
        	    		iconCls : 'icon-calendar-16',         
        	    		handler: function() {
        	    		
    	    				
                	    		   	my_listeners = {
		        					afterModifica: function(from_win){	
		        					   grid.getStore().load();
		        					   from_win.close();
						        		}
				    				};
    			    		
    			    		if(rec.get('DBFEXP').trim() == 'V'){
    			    		
    			    		 	  Ext.Ajax.request({
            				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_val',
            				        method     : 'POST',
            	        			jsonData: {
            	        				row : row
            						},							        
            				        success : function(result, request){
            				          var jsonData = Ext.decode(result.responseText);
            				          grid.getStore().load();		
            	            		   
            	            		},
            				        failure    : function(result, request){
            				            Ext.Msg.alert('Message', 'No data to be loaded');
            				        }
            				    	});	
    			    			
    			    		}else{
    			    		   acs_show_win_std('Attiva/Disattiva controllo data validit&agrave;', 
								'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_check',
			                	{row : row}, 300, 150, my_listeners, 'icon-calendar-16');	
    			    		
    			    		}
                	    	
                
        	    		

        			    }
        			  });
        			  
        			   voci_menu.push({
			         		text: 'Copia codice articolo',
			        		iconCls : 'icon-copy-16',          		
			        		handler: function () {
			        		   const el = document.createElement('textarea');
			        		   <?php if($m_params->tipo_dist == 'P'){?>
			        		      el.value = rec.get('cod_comp');
						 	   <?php }else{?>
						 		  el.value = rec.get('dbart_p');
						 	   <?php }?>
			        		    document.body.appendChild(el);
                    	  		el.select();
                    	  		document.execCommand('copy');
                    	  		document.body.removeChild(el);
                    	  		
					        }
			    		});
			    		
			    		if(rec.get('r_codifica').trim() != ''){
    			    		voci_menu.push({
    			         		text: 'Regola di codifica',
    			        		iconCls : 'icon-design-16',          		
    			        		handler: function () {
    			        		   acs_show_win_std('Regola di codifica', 'acs_tab_sys_MURC.php?fn=open_panel', {autoload : 'firstRec', only_view : 'Y', hide_grid : 'Y', tanr : rec.get('r_codifica')}, 410, 570, null, 'icon-design-16');         	          
    					        }
    			    		});
			    		}
			    		
			    			  
			    		
			    		
			    		
					  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
			   
			    	
			    	 , celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           		           						  						  	
					  	var grid = this, 
					  		col_name = iView.getGridColumns()[iColIdx].dataIndex,
					  		rec = iView.getRecord(iRowEl);
					  			

					  	
						if(col_name == 'attav'){
						 <?php if($m_params->tipo_dist == 'P'){?>
						 	acs_show_win_std('Gestione stato/attivit&agrave; articolo ' +rec.get('cod_comp') , 'acs_anag_art_todolist.php?fn=open_tab', {c_art: rec.get('cod_comp'), rif : 'ART'}, 900, 400, null, 'icon-arrivi-16');
						 <?php }else{?>
						 	acs_show_win_std('Gestione stato/attivit&agrave; articolo ' +rec.get('dbart_p') , 'acs_anag_art_todolist.php?fn=open_tab', {c_art: rec.get('dbart_p'), rif : 'ART'}, 900, 400, null, 'icon-arrivi-16');
						 <?php }?>
							
						}
						if(col_name == 'comm'){
						   var title = rec.get('dbart_p') + ', ' +rec.get('seq1')+ ', ' +rec.get('seq2')+ ', ' + rec.get('cod_comp')+ ', '+ rec.get('rip');
						   acs_show_win_std('Note distinta ' + title , 'acs_anag_art_note.php?fn=open_bl', {rife1: rec.get('rife1'), f_db : 'Y', title : title}, 400, 400, null, 'icon-comment_edit-16');
					    
					    }
						if(col_name == 'comp'){
						
						<?php if($m_params->tipo_dist == 'P'){?>
							acs_show_win_std('Componenti ' + rec.get('cod_comp') + ' - ' +rec.get('desc_comp'), 'acs_anag_art_distinta_base.php?fn=open_tab', {c_art: rec.get('cod_comp'), tipo_dist : 'P', nr_db : rec.get('comp')}, 1300, 500, {
								close: function(comp){
										if (!Ext.isEmpty(grid)) grid.store.load();
            				  	}
							}, 'icon-folder_download-16');
						 <?php }else{?>
						 	acs_show_win_std('Componenti ' + rec.get('dbart_p') + ' - ' +rec.get('desc_dbart'), 'acs_anag_art_distinta_base.php?fn=open_tab', {c_art: rec.get('dbart_p'), tipo_dist : 'P'}, 1300, 500, {
						 		close: function(comp){
									if (!Ext.isEmpty(grid)) grid.store.load();
            				  	}
						 	}, 'icon-folder_download-16');
						 <?php }?>
						
						  
						}
						
					    if(col_name == 'cond'){
							acs_show_win_std('Condizioni di elaborazione variabili [' + rec.get('dbart_p') + ', ' + rec.get('seq1') + ', ' + rec.get('seq2') + ', ' + rec.get('cod_comp') + ', ' + rec.get('rip') + ']', 
								'acs_anag_art_compo_cond.php?fn=open_cond', {
								record : rec.data
							}, 1100, 550, {
								close: function(comp){
										if (!Ext.isEmpty(grid)) grid.store.load();
            				  		}
							}, 'icon-design-16');
						  return;
						}
						
						if(col_name == 'cod_comp'){
							acs_show_win_std('Dettaglio articolo', 'acs_win_dettaglio_articoli.php?fn=open_win', 
		   				 	{c_art : rec.get('cod_comp'), d_art : rec.get('desc_comp')}, 700, 600, null, 'icon-leaf-16');     
						}	
						
						if(col_name == 'dbart_p'){
							acs_show_win_std('Dettaglio articolo', 'acs_win_dettaglio_articoli.php?fn=open_win', 
		   				 	{c_art : rec.get('dbart_p'), d_art : rec.get('desc_dbart')}, 700, 600, null, 'icon-leaf-16');     
						}	
					   			
						
					    }
	           	 } 
				  
				 }
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					            if(record.get('DBFEXP') == 'S')  return ' colora_riga_rosso';
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					 <?php if($m_params->tipo_dist == 'P'){?>
					   buttons: [{
			           text: 'Duplica componenti',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',	                     
			            handler: function() {
			                var form = this.up('form').getForm();
			                var loc_win = this.up('window');
			               	var grid = this.up('window').down('grid');
			               	
			               	id_selected = grid.getSelectionModel().getSelection();	
			                list_selected_id = [];
						  	for (var i=0; i<id_selected.length; i++) 
								list_selected_id.push(id_selected[i].data.cod_comp);
											     
							var my_listeners = {
		    		  			afterConferma: function(from_win){
			    		  			    grid.getStore().load();
		        						//from_win.close();  
						        		}
				    				};

         	
			               	acs_show_win_std('Duplica distinta componenti', 
	    					'acs_anag_art_db_duplica.php?fn=open_dup', 
	    					{c_art: '<?php echo $m_params->c_art; ?>', d_art: <?php echo j($m_params->d_art); ?>,nr_db : '<?php echo $m_params->nr_db; ?>', list_selected_id : list_selected_id}, 1100, 500, my_listeners, 'icon-folder_download-16');
			               	             
			            }
			        }
			        
			        
			        /*, {
			           text: 'Duplica',
			            iconCls: 'icon-print-32',
			            scale: 'large',	                     
			            handler: function() {
			                var form = this.up('form').getForm();
			                var loc_win = this.up('window');
			               	var grid = this.up('window').down('grid');
			               	
			               	id_selected = grid.getSelectionModel().getSelection();	
			                list_selected_id = [];
						  	for (var i=0; i<id_selected.length; i++) 
								list_selected_id.push(id_selected[i].data.cod_comp);
			               	
			               	acs_show_win_std('Duplica distinta', 
	    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_dup', 
	    					{c_art: '<?php echo $m_params->c_art; ?>', list_selected_id : list_selected_id}, 400, 270,  {}, 'icon-folder_download-16');
			               	             
			            }
			        }*/
		        ]
				<?php }?>	 
					
					
	}
	
		,
				
				{
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: '',
 		            //bodyStyle: 'padding: 10px',
		            bodyPadding: '5 0 0',
 		            flex:0.33,
 		            frame: true,
 		            items: [
 		           
 		           			
				  	{
        		xtype: 'tabpanel',
        		items: [
			
			{
				xtype: 'panel',
				title: 'Dettagli distinta', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
				{
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					hidden : true						
				   }, 
				    { 
				xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox',
						    pack: 'start',
						    align: 'stretch'},
				anchor: '-15',						
				items: [
				 {
					name: 'seq1',
					fieldLabel : 'Sequenza',
					xtype: 'textfield',
					anchor: '-15',
					width : 150,
					maxLength : 3,
					listeners : {
					  'change': function(field){
        				  field.setValue(field.getValue().toUpperCase());
      					}
					}
				  }, {
					name: 'seq2',
					fieldLabel : '+',
					labelWidth : 10,
					margin : '0 0 0 5',
					labelAlign : 'right',
					xtype: 'textfield',
					labelSeparator : '', 
					anchor: '-15',
					width : 60,
					maxLength : 1,
					listeners : {
					  'change': function(field){
        				  field.setValue(field.getValue().toUpperCase());
      					}
					}
				  }
				  
				  	, {
				  		xtype: 'checkbox',
    					name: 'controllo',
    					labelAlign : 'right',
    					fieldLabel : 'Standard', //Controllo sviluppo
    					labelWidth: 60,
    					inputValue: 'S', uncheckedValue: ' '    				
    				  }
				  
				  	]},
	        	 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						  <?php if($m_params->tipo_dist == 'P'){?>
				   {
					name: 'cod_comp',
					fieldLabel : 'Articolo',
					xtype: 'textfield',
					anchor: '-15',
					allowBlank: false,
					listeners : {
						'blur': function(field){
    					 var n_art = field.getValue();
    					 var form = this.up('form').getForm();
    					 
    					 Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_new_art',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				codice : n_art
								},				
										        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        
							        form.findField('u_m').store.loadData(jsonData.rec.u_m);
									form.findField('u_m').setValue(jsonData.rec.ARUMTE);
									form.findField('desc_comp').setValue(jsonData.rec.ARDART);
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
      				}
					
					}
				  } 
				  
				  <?php }else{?>
				    {
					name: 'dbart_p',
					fieldLabel : 'Articolo',
					xtype: 'textfield',
					anchor: '-15'
				  } 
				  
				  <?php }?>
				  
				   
				 , {
					name: 'rip',
					fieldLabel : 'R',
					labelWidth : 20,
					margin : '0 0 0 5',
					labelAlign : 'right',
					xtype: 'textfield',
					anchor: '-15',
					width : 90,
					maxLength : 1
				  }	
					]},
					
							
				 <?php if($m_params->tipo_dist == 'P'){?>
				     {
					name: 'desc_comp',
					fieldLabel : 'Descrizione',
					xtype: 'displayfield',
					fieldStyle: 'font-weight: bold;',
					anchor: '-15'
				  }
				  
				  <?php }else{?>
				  
				      {
					name: 'desc_dbart',
					fieldLabel : 'Descrizione',
					xtype: 'displayfield',
					fieldStyle: 'font-weight: bold;',
					anchor: '-15'
				  }
				  
				  <?php }?>
		  
		       ,{ 
				xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox',
						    pack: 'start',
						    align: 'stretch'},						
				items: [
				
				{
					name: 'quant',
					fieldLabel : 'Quantit&agrave;',
					xtype: 'numberfield',
					hideTrigger : true,
					anchor: '-15',
					width : 220,
					decimalPrecision : 4,
					allowBlank: false
				  }
				  , {
    					name: 'u_m',
    					xtype: 'combo',
    					width : 170,
					    labelAlign : 'right',
					    labelWidth : 80,
    					fieldLabel: 'UM',
    					//forceSelection: true,								
    					displayField: 'id',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		allowBlank: false,								
    				    anchor: '-15',
    				    queryMode: 'local',
     					minChars: 1,
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [
    					        
    					    ]
    					},listeners: { 
			 				   beforequery: function (record) {
    	         				record.query = new RegExp(record.query, 'i');
    	         				record.forceAll = true;
    				 		}
 					}    					
    			   }	
				  
				 					   
			      ]}
			      
			      
			      
				  , {
    					name: 'var_qta',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: '&nbsp;&nbsp;&nbsp;Var',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		//allowBlank: false,								
    				    anchor: '-15',
    				    value : <?php echo j($fase); ?>,
    				    queryMode: 'local',
     					minChars: 1,
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [
    					     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); ?>    					     
    					    ]
    					},listeners: { 
			 				   beforequery: function (record) {
    	         				record.query = new RegExp(record.query, 'i');
    	         				record.forceAll = true;
    				 		}
 					}    					
    			   }			      
			      
			      
			      
			      
				  , {
    					name: 's_m_c',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: 'Q.t&agrave; da var./dim.',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		//allowBlank: false,								
    				    anchor: '-15',
    				    value : <?php echo j($fase); ?>,
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: <?php echo acs_je(array(    					         
    					               array('id' => 'S', 'text' => '[S] Sostituisci'),
    					               array('id' => 'M', 'text' => '[M] Moltiplica'),
    					               array('id' => '2', 'text' => "[2] Moltiplica per u.m. costo 'padre'"),
    					               array('id' => '3', 'text' => "[3] Moltiplica per u.m. altern. 'padre'"),
    					               array('id' => 'C', 'text' => '[C] Moltiplica per coeff. configurazione'),
    					               array('id' => '>', 'text' => '[>] Sostituisci se maggiore q.t&agrave; distinta'),
    					               array('id' => 'T', 'text' => '[T] Sostituisci se maggiore minimo fatturaz.'),
    					               array('id' => 'U', 'text' => "[U] Ignora q.t� padre MQ/ML su comp. ML"),
    					               array('id' => 'N', 'text' => "[N] Ignora q.t� padre/non moltiplica")
    					         )); ?>	
    					    
    					}    					
    			   }			      
			      
			      
			      
				,{ 
    				xtype: 'fieldcontainer',
    				flex:1,
    				layout: { 	type: 'hbox',
    						    pack: 'start',
    						    align: 'stretch'},		
					anchor: '-15',    						    				
    				items: [
					  {
    					name: 'qta_10',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: 'Diviso',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		//allowBlank: false,								
    				    flex: 2,
    				    value : <?php echo j($fase); ?>,
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [								    
    					    <?php echo acs_ar_to_select_json($m_DeskArt->find_TA_std('QTA10'), ''); ?>	
    					    ]
    					}     					
    			   }    , {
    					name: 'arr',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: 'Arrot', labelWidth: 40, labelAlign: 'right',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',    				    
    				    value : <?php echo j($fase); ?>,
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: <?php echo acs_je(array(    					         
    					               array('id' => 'D', 'text' => 'Difetto'),
    					               array('id' => 'E', 'text' => 'Eccesso'),
    					               array('id' => 'R', 'text' => 'Giustifica')
    					         )); ?>	
    					    
    					}    					
    			   }				
					]
				}      
			      
			      
			     ,  { 
						xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox',
        						    pack: 'start',
        						    align: 'stretch'},		
    					anchor: '-15',    						    				
        				items: [
						
						{
        					name: 'dim1',
        					fieldLabel : 'Larghezza',
        					xtype: 'numberfield',
        					width : 220,
        					hideTrigger : true,
        					anchor: '-15'
        				  }, {
        					name: 'rip_dim1',
        					xtype: 'combo',
        					fieldLabel: '+-M', 
        					labelWidth: 30,
        					width : 105,
        					labelAlign: 'right',
        					forceSelection: true,								
        					displayField: 'text',
        					valueField: 'id',								
        					emptyText: '- seleziona -',    				    
        				    value : <?php echo j($fase); ?>,
        					store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: <?php echo acs_je(array(    					         
        					               array('id' => '+', 'text' => 'Val.incr.'),
        					               array('id' => '-', 'text' => 'Val.decr.'),
        					               array('id' => 'M', 'text' => 'Multiplo'),
        					               array('id' => '', 'text'  => 'Val.dim.'),
        					         )); ?>	
        					    
        					}    					
        			   }, {
				  		xtype: 'checkbox',
    					name: 'qta_dim1',
    					labelAlign : 'right',
    					fieldLabel : 'Q.t&agrave', 
    					labelWidth: 40,
    					inputValue: 'Q', uncheckedValue: ' '    				
    				  }
						   
					    ]}, 
				   {
    					name: 'var_dim1',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: '&nbsp;&nbsp;Variab.',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		//allowBlank: false,								
    				    anchor: '-15',
    				    value : <?php echo j($fase); ?>,
    				    queryMode: 'local',
     					minChars: 1,
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [
    					    <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); ?>    					     
    					    ]
    					}, listeners: { 
			 					beforequery: function (record) {
    	         					record.query = new RegExp(record.query, 'i');
    	         					record.forceAll = true;
    				 				}
 							}    					
    			   },
				     { 
						xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox',
        						    pack: 'start',
        						    align: 'stretch'},		
    					anchor: '-15',    						    				
        				items: [
						
						{
        					name: 'dim2',
        					fieldLabel : 'Lunghezza',
        					xtype: 'numberfield',
        					width : 220,
        					hideTrigger : true,
        					anchor: '-15'
        				  }, {
        					name: 'rip_dim2',
        					xtype: 'combo',
        					fieldLabel: '+-M', 
        					labelWidth: 30,
        					width : 105,
        					labelAlign: 'right',
        					forceSelection: true,								
        					displayField: 'text',
        					valueField: 'id',								
        					emptyText: '- seleziona -',    				    
        				    value : <?php echo j($fase); ?>,
        					store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: <?php echo acs_je(array(    					         
        					               array('id' => '+', 'text' => 'Val.incr.'),
        					               array('id' => '-', 'text' => 'Val.decr.'),
        					               array('id' => 'M', 'text' => 'Multiplo'),
        					               array('id' => '', 'text'  => 'Val.dim.'),
        					         )); ?>	
        					    
        					}    					
        			   }, {
				  		xtype: 'checkbox',
    					name: 'qta_dim2',
    					labelAlign : 'right',
    					fieldLabel : 'Q.t&agrave', 
    					labelWidth: 40,
    					inputValue: 'Q', uncheckedValue: ' '    				
    				  }
						   
					    ]}, 
				   {
    					name: 'var_dim2',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: '&nbsp;&nbsp;Variab.',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		//allowBlank: false,								
    				    anchor: '-15',
    				    value : <?php echo j($fase); ?>,
    				    queryMode: 'local',
     					minChars: 1,	
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [
    					    <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); ?>    					     
    					    ]
    					},  listeners: { 
			 				   beforequery: function (record) {
    	         				record.query = new RegExp(record.query, 'i');
    	         				record.forceAll = true;
    				 		}
 					}    					
    			   }, { 
						xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox',
        						    pack: 'start',
        						    align: 'stretch'},		
    					anchor: '-15',    						    				
        				items: [
						
						{
        					name: 'dim3',
        					fieldLabel : 'Spessore',
        					xtype: 'numberfield',
        					width : 220,
        					hideTrigger : true,
        					anchor: '-15'
        				  }, {
        					name: 'rip_dim3',
        					xtype: 'combo',
        					fieldLabel: '+-M', 
        					labelWidth: 30,
        					width : 105,
        					labelAlign: 'right',
        					forceSelection: true,								
        					displayField: 'text',
        					valueField: 'id',								
        					emptyText: '- seleziona -',    				    
        				    value : <?php echo j($fase); ?>,
        					store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: <?php echo acs_je(array(    					         
        					               array('id' => '+', 'text' => 'Val.incr.'),
        					               array('id' => '-', 'text' => 'Val.decr.'),
        					               array('id' => 'M', 'text' => 'Multiplo'),
        					               array('id' => '', 'text'  => 'Val.dim.'),
        					         )); ?>	
        					    
        					}    					
        			   }, {
				  		xtype: 'checkbox',
    					name: 'qta_dim3',
    					labelAlign : 'right',
    					fieldLabel : 'Q.t&agrave', 
    					labelWidth: 40,
    					inputValue: 'Q', uncheckedValue: ' '    				
    				  }
						   
					    ]},  {
    					name: 'var_dim3',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: '&nbsp;&nbsp;Variab.',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		//allowBlank: false,								
    				    anchor: '-15',
    				    value : <?php echo j($fase); ?>,
    				    queryMode: 'local',
     		            minChars: 1,	
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [
    					    <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); ?>    					     
    					    ]
    					},  listeners: { 
			 					beforequery: function (record) {
    	         					record.query = new RegExp(record.query, 'i');
    	         					record.forceAll = true;
    				 	}
 			}    					
    			   }
				  
				]
			},
			{
				xtype: 'panel',
				title: 'Scarto/Colli/Varianti', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
				{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
        					name: 'scarto',
        					fieldLabel : 'Scarto %',
        					width : 200,
        					xtype: 'numberfield',
        					hideTrigger : true,
        					anchor: '-15'
        				  },{
        					name: 'v_scarto',
        					fieldLabel : 'Scarto a valore',
        					xtype: 'numberfield',
        					labelAlign : 'right',
        					width : 190,
        					hideTrigger : true,
        					anchor: '-15'
        				  }
						   
						]},
						{ xtype : 'textfield',
						  name : 'filler',
						  hidden : true
						},
						
							{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
				  		xtype: 'checkbox',
    					name: 'ch_collo',
    					fieldLabel : 'Collo', //Controllo sviluppo
    					inputValue: 'Y',
    					uncheckedValue: '',
    					listeners: {
            				change: function(comp, check){
            					var form = comp.up('form').getForm();
                     			if(check == true){
                         			form.findField('nr_collo').enable();
                         			form.findField('gr_collo').enable();
                          		}else{
                          			form.findField('nr_collo').disable();
                         			form.findField('gr_collo').disable();
                    	 		}
                   		 }
                	   }     				
    				  },
    				  {
    					name: 'nr_collo',
    					fieldLabel : 'Nr colli',
    					labelWidth : 50,
    					labelAlign : 'right',
    					width : 180,
    					xtype: 'numberfield',
    					hideTrigger : true,
    					disabled : true,
    					decimalPrecision : 4
    				  },
						
						]},
						
						
    				  {
						name: 'gr_collo',
						disabled : true,
						xtype: 'combo',
						fieldLabel: 'Gruppo collo',
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona -',
				   		//allowBlank: false,								
					    anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUCI'), ''); ?>	
						    ]
						},
						queryMode: 'local',
            			minChars: 1, 	
						listeners: { 
    					 	beforequery: function (record) {
    			         	record.query = new RegExp(record.query, 'i');
    			         	record.forceAll = true;
    		             }
    		          }
								
						 },
						 
						 {
    					name: 'r_codifica',
    					xtype: 'combo',
    					flex: 1,
    					fieldLabel: 'Regola di codifica',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		//allowBlank: false,								
    				    anchor: '-15',
    				    value : <?php echo j($fase); ?>,
    				    queryMode: 'local',
     					minChars: 1,	
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [								    
    					     <?php echo acs_ar_to_select_json(find_TA_sys('MURC', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
    					    ]
    					}, listeners: { 
			 					beforequery: function (record) {
    	         					record.query = new RegExp(record.query, 'i');
    	         					record.forceAll = true;
    				 		}
 			}    					
    			   },
						 
						  <?php 
				  $proc = new ApiProc();
				  echo $proc->get_json_response(
				      extjs_combo_dom_ris(array(
				          'label' =>'Variab.distinta(1)', 
				          'file_TA' => 'sys',
				          'tipo_opz_risposta' => array('output_domanda' => true),
				          'dom_cf'  => 'DBVRV1',  'ris_cf' => 'DBVNV1'))
				  );
				  echo ",";
				  echo $proc->get_json_response(
				      extjs_combo_dom_ris(array(
				          'label' =>'Variab.distinta(2)',
				          'tipo_opz_risposta' => array('output_domanda' => true),
				          'file_TA' => 'sys', 'dom_cf'  => 'DBVRV2',  'ris_cf' => 'DBVNV2'))
				      );
				  ?>
				  
				  , 	{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
				  
					   { xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Validit&agrave; dal'
					   , name: 'val_ini'
					   , format: 'd/m/Y'
					   , width : 240						   
					   , submitFormat: 'Ymd'
					 //  , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'al'
					   , labelWidth : 20
					   , width : 150
					   , labelAlign : 'right'
					   , name: 'val_fin'
					   , format: 'd/m/Y'							   
					   , submitFormat: 'Ymd'
					  // , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					}		
					]}		
					
				]
			}
			
			]
			
			}
				  
				  				        
		],
			 		           
			 <?php if($m_params->tipo_dist == 'P'){?>		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
       			      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
        	   		  	if (btn == 'yes'){
 			          		Ext.Ajax.request({
    				        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
    				        	timeout: 2400000,
    				        	method     : 'POST',
    	        				jsonData: {
    	        				    c_art : '<?php echo $m_params->c_art; ?>',
    	        					form_values : form_values
    						},							        
    				        	success : function(result, request){
    					        	var jsonData = Ext.decode(result.responseText);
    					        	if (jsonData.success == true){
    					        	var gridrecord = grid.getSelectionModel().getSelection();
    					        	grid.store.remove(gridrecord);	
    					        	form.getForm().reset();						        
    					        }
    				        },
    				        	failure    : function(result, request){
    				            	Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    	}); 
				     }
    	   		  });	
		           
			
			            }

			     },
			     {
                    xtype: 'button',
                    text: 'Reset',
		            scale: 'small',	                     
					iconCls: 'icon-reset',
		            handler: function() {
		            	  this.up('form').getForm().reset();
	       			     }

			     }, {
                     xtype: 'button',
                    text: 'Refresh',
		            scale: 'small',	               
		            iconCls : 'icon-button_black_repeat_dx-16',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			  	  form = this.up('form'); 
 			          grid.getStore().load();
 			          form.getForm().reset();
 			        }
			     } ,'->',
                 
                  
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid');
	       			  
	       			  if (!form.getForm().isValid()) return false; 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_distinta',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				c_art : '<?php echo $m_params->c_art; ?>',
	        				tipo_dist : '<?php echo $m_params->tipo_dist; ?>'
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid');
 			       			
 			       			if (!form.getForm().isValid()) return false; 
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_distinta',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        			    c_art : '<?php echo $m_params->c_art; ?>',
	        				        tipo_dist : '<?php echo $m_params->tipo_dist; ?>'
			        				
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     }
			     ]
		   }]
		   
		   <?php }?>
			 	 }, //fine form
			 	 
			 	 
				
				
				
		
		
	
					
						   
						
						
				
						

								
					
					
					 ],
					 
					
					
	}
	
]}

<?php

exit;
}

if ($_REQUEST['fn'] == 'open_dup'){
    $m_params = acs_m_params_json_decode(); 
    $c_art = $m_params->c_art;
     
    ?>
    
    {"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
				items: [
				
				      { xtype: 'displayfield', value : 'Distinta da duplicare : '},
				      
				      		{ xtype: 'fieldcontainer',
							layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
							items: [
							<?php for ($i = 0; $i<=14; $i++){?>
							{ 
						  xtype : 'textfield',
						  name: 'f_<?php echo "a_".$i; ?>', 
						  value : '<?php echo $c_art[$i]; ?>',
						  width: 20,
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
							
							]}
						,	{ xtype: 'displayfield', value : 'Componenti da duplicare : '},
							
							<?php foreach($m_params->list_selected_id as $k => $v){ ?>
							
						   { xtype: 'fieldcontainer',
							layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
							items: [
							
							<?php for ($i = 0; $i<=14; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_<?php echo $k."_".$i; ?>', 
						  value : '<?php echo $v[$i]; ?>',
						  width: 20,
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
							
							
							]},
						<?php }?>
						
				
					 ],
					   buttons: [{
			            text: 'Conferma (To Do)',
			            iconCls: 'icon-print-32',
			            scale: 'large',	                     
			            handler: function() {
			            
			               var form = this.up('form').getForm();
			               var loc_win = this.up('window');
			               
			               if(form.isValid()){
	            	
                    	   	Ext.Ajax.request({
        				        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_dup_dist',
        				        jsonData: {
        				        	c_art : '<?php echo $m_params->c_art; ?>',
        				        	form_values : form.getValues(),
        				        	list_comp : <?php echo acs_je($m_params->list_selected_id); ?>			        	
        				         },
        				        method     : 'POST',
        				        waitMsg    : 'Data loading',
        				        success : function(result, request){	
        				            jsonData = Ext.decode(result.responseText);
        				            loc_win.fireEvent('afterDup', loc_win, jsonData);	
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				            console.log('errorrrrr');
        				        }
        				    });	
                    	
	            		
	            	
			        } 
			               
			               	             
			            }
			        }
		        ]
					 
					
					
	}
	
]}
    
    
    

 <?php 
 exit;
}


if ($_REQUEST['fn'] == 'open_check'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	                  { xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Validit&agrave; iniziale'
					   , value : '<?php echo $m_params->row->val_ini; ?>'
					   , name: 'f_val_ini'
					   , format: 'd/m/Y'					   
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Validit&agrave; finale'
					   , value : '<?php echo $m_params->row->val_fin; ?>'
					   , name: 'f_val_fin'
					   , format: 'd/m/Y'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					}	
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma',
				        scale: 'large',	                     
						iconCls: 'icon-button_blue_play-32',	            
			            handler: function() {
			                 var form = this.up('form');
			                 var form_values = form.getValues();
			                 var loc_win = this.up('window');
			                 if(form.getForm().isValid()){
			             	  Ext.Ajax.request({
            				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_val',
            				        method     : 'POST',
            	        			jsonData: {
            	        				row : <?php echo acs_je($m_params->row); ?>,
            	        				form_values : form_values
            						},							        
            				        success : function(result, request){
            				          var jsonData = Ext.decode(result.responseText);
            				          loc_win.fireEvent('afterModifica', loc_win);	
            	            		   
            	            		},
            				        failure    : function(result, request){
            				            Ext.Msg.alert('Message', 'No data to be loaded');
            				        }
            				    	});	
        				   }

						
			               
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}


function get_att_from_art($asdocu){
    
    global $cfg_mod_DeskArt, $conn;
    
    $sql = "SELECT ATT_OPEN.ASCAAS, TA_ATTAV.TADESC AS D_ATTAV
    FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT_OPEN
    INNER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
        ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
    INNER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
      ON ATT_OPEN.ASDT = AR.ARDT AND ATT_OPEN.ASDOCU = AR.ARART
    WHERE ASDOCU = '{$asdocu}'";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $ar[] = $row;
    }
    
    $ret = array();
    if(count($ar) > 0){
        foreach($ar as $k => $v){
           $ret[] = "[".trim($v['ASCAAS'])."] ".$v['D_ATTAV'];
        }
    }
    return implode(', ', $ret);
      
}
