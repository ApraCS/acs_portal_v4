<?php
require_once "../../config.inc.php";
require_once("acs_anag_artd_include.php");

$main_module = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data_indici'){
    
    $sql = "SELECT TAKEY1, TADESC, TAFG01
    FROM {$cfg_mod_DeskArt['file_tabelle']} TA
    WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ARTIC'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['codice'] = trim($row['TAKEY1']);
        $nr['desc'] = trim($row['TADESC']);
        $nr['att'] = trim($row['TAFG01']);
        
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
    
    
    
}

if ($_REQUEST['fn'] == 'get_json_data_elenco'){
        $ar = array();
        $distinta = trim($m_params->distinta);
        $gruppo = trim($m_params->gruppo);
        $radice_temp = trim($m_params->radice);
        $form_values = $m_params->form_values;
        
            
        if(isset($gruppo) && strlen($gruppo) >0)
            $cdcmas = $gruppo;
        else
            $cdcmas = "START";
                
        $sql = "SELECT *
                FROM {$cfg_mod_DeskArt['file_cfg_distinte']}
                WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$distinta}'
                AND CDCMAS = '{$cdcmas}'";
                
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        while($row = db2_fetch_assoc($stmt)){
            $master = trim($row['CDCMAS']);
            
            if($master == 'START'){
                $ar = array_merge($ar, get_risposte_START($row, $form_values));                
            }else {
                switch (trim($row['CDFLLG'])){
                    case 'L':
                        $ar = array_merge($ar, get_risposte_L($row, $radice_temp, $form_values));
                        break;
                    case 'G':
                    case 'A':
                    case '' :
                        $ar = array_merge($ar, get_risposte_G($master, $row, $radice_temp, $form_values));
                        break;
                }
            }
            
        } //while
        
        
    
    
    echo acs_je($ar);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'open_indici'){
   
    ?>
    
     {
        success:true,
        items: [
        
        	{
						xtype: 'grid',
						title: '',
						flex: 1,
						itemId : 'g_indici',
				        loadMask: true,	
				        bbar: new Ext.Toolbar({
            	            items:[
            	            {
            	            
            	             iconCls: 'icon-gear-16',
            	             tooltip: 'Aggiungi indice di classificazione',
            	             handler: function(event, toolEl, panel){
            	                    	acs_show_win_std('Indici di documentazione', 'acs_anag_artd_gest_indici.php?fn=open_tab', {}, 950, 500, null, 'icon-gear-16');
            			          
            		           		 
            		           		 }
            		           	 }, 
							{
            	             iconCls: 'icon-windows-16',
            	             tooltip: 'Configurazione liste',
            	             handler: function(event, toolEl, panel){
            	                    acs_show_win_std('Lista opzioni', 'acs_anag_artd_config_distinta_lista.php?fn=open_tab', {}, 950, 500, null, 'icon-windows-16');
            		         }
            		        } , '->',
            		        
            	            
            	            {
            				xtype: 'form',
            				itemId : 'init_form',
            	            frame: true,
            	            autoScroll : true,
            	            title: '',
            				items: [
            				
            				{ 
                        	xtype: 'fieldcontainer',
                        	margin : '0 0 0 0',
                        	layout: { 	type: 'hbox',
                        				pack: 'start',
                        				align: 'stretch',
                        				frame: true},	
                        					
                        	items: [
                				 {
                                            fieldLabel: 'Debug',
                                            name: 'f_debug',	
                                            labelWidth: 35,
                                            margin : '0 20 0 0',
                                            xtype: 'checkboxfield',
                                            inputValue: 'Y'         
                                        }
                						,	 {
                                            fieldLabel: 'Sospesi',
                                            name: 'f_sosp',	
                                            labelWidth: 40,
                                            margin : '0 0 0 0',
                                            xtype: 'checkboxfield',
                                            inputValue: 'Y'         
                                        }
                        		
                	
                					]}
            				]}
            	
            	         ]            
	      			  }),
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_indici', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
									
										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['codice', 'desc', 'att']							
									
			}, //store
				columns: [	
			        {
 	                  header   : 'Codice',
	                  dataIndex: 'codice',
	                  width : 70
	                },
	                {
	                  header   : 'Descrizione',
	                  dataIndex: 'desc',
	                  flex: 1
	                }
	                
	         ]
	         
	         , listeners: {
	       			  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  var grid = this;
							  var form = grid.down('#init_form').getForm();
							  
        			  		 	acs_show_win_std('Indici di documentazione regole/attivit&agrave; di codifica', 'acs_anag_artd_crea_nuovo.php?fn=open_config', {
                        		cod: rec.get('codice'), desc : rec.get('desc'), att : rec.get('att'), form_values : form.getValues()}, 
                        		800, 400, null, 'icon-windows-16');
                        		this.up('window').close();
                        		
							 
						  }
			   		  },   itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						  var voci_menu = [];
						 	 voci_menu.push({
			         		text: 'Configurazione distinte',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
					    		  acs_show_win_std('Categorie articoli', 'acs_anag_artd_config_distinta.php?fn=open_tab', {cod: rec.get('codice'), des : rec.get('desc')}, 1300, 500, null, 'icon-gear-16');
				                }
			    		 });	 
			    		
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
	         
				   
				 }
				 
		
		
		}, //grid
        
            ]
    }
    
    
  <?php 
 exit;
}


if ($_REQUEST['fn'] == 'open_config'){
    
    $form_values = $m_params->form_values;
   
 
    ?>
	
	{
		success:true,
		items: [
					
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
					
		
			{
						xtype: 'grid',
						title: '',
						flex: 1,
						itemId : 'g_sx',
				        loadMask: true,	
				        store: {
        						xtype: 'store',
        						autoLoad:true,
        					 data : [
        							{codice: '<?php echo $m_params->cod; ?>', desc : '<?php echo $m_params->desc; ?>', distinta : '<?php echo $m_params->cod; ?>'},
							 

							        ],
				   	fields: ['codice', 'desc', 'gruppo', 'distinta', 'radice']
						

				}, //store
				columns: [	
				
				
			        {
	                  header   : 'Radice',
	                  dataIndex: 'radice',
	                  width : 50,
	                  <?php if($form_values->f_debug == 'Y'){?>
	                   hidden : false
	                  <?php }else{?>
	                   hidden : true
	                  <?php }?>
				
	                },
	            
	                {
	                  header   : 'Codice',
	                  dataIndex: 'codice',
	                  width : 50
	                },
	                {
	                  header   : 'Descrizione',
	                  dataIndex: 'desc',
	                  sortable : false,
	                  flex: 1
	                }
	                
	                
	         ],	 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'small',
                     iconCls : 'icon-bookmark-16',
			            handler: function() {
			            
			            acs_show_win_std('Indici di classificazione', 'acs_anag_artd_crea_nuovo.php?fn=open_indici', {}, 
	            		400, 400, null, 'icon-bookmark-16');
	            		this.up('window').close();
			           
			            }
			     },
			     '->',
			     {
                            fieldLabel: 'Debug',
                            itemId : 'debug',
                            name: 'f_debug',	
                            labelWidth: 40,
                            margin : '0 5 0 0',
                            xtype: 'checkboxfield',
                            inputValue: 'Y',
                            <?php if($form_values->f_debug == 'Y'){?>
                             checked : true,
                            <?php }?>
                            listeners: {
    						'change': function(field){
    						
    							grid_sx = this.up('window').down('#g_sx');
    							col_radice = grid_sx.down('[dataIndex=radice]');
    						    if(field.getValue() == true)
    								col_radice.show();
    							else
    						        col_radice.hide();
    						
    						    grid_dx = this.up('window').down('#g_dx');
    						    col_cr = col_radice = grid_dx.down('[dataIndex=conf_radice]');
    						    col_rad = col_radice = grid_dx.down('[dataIndex=radice]');
    						    
    						        if(field.getValue() == true){
    								col_cr.show();
    								col_rad.show();
    						    }else{
    						        col_cr.hide();
    								col_rad.hide();
    						    }    
    						   
    					  }
 						 }         
                        }
						,	 {
                            fieldLabel: 'Sospesi',
                            itemId : 'sospesi',
                            name: 'f_sosp',	
                            labelWidth: 45,
                            xtype: 'checkboxfield',
                            inputValue: 'Y',
                            <?php if($form_values->f_sosp == 'Y'){?>
                             checked : true,
                            <?php }?>         
                        }
			     
			     
			     ]
		   }]
	         
	         , listeners: {
	         
	             celldblclick: {
	             		
	             		//su grid con elenco voci selezionate (grid sx)
	             			             								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  var grid = this;
							  var grid_dx = this.up('window').down('#g_dx');
							  var rows = grid.getStore().data.items;
							
							  var gruppo = '';
							  for(var i = 1 ; i < iRowIdx + 1; i++) {
                                 gruppo += rows[i]['data']['codice'].trim();                                 
                              }
                          
        		              grid_dx.getStore().proxy.extraParams.gruppo = gruppo;
        		              grid_dx.getStore().proxy.extraParams.radice = rec.get('radice');
        		              grid_dx.getStore().load();
        		               
        		              var idx =  iRowIdx + 1 ; 
                              var lng =  rows.length ; 
                                  
                              ar_row = [];
                              for(var i = idx ; i < lng; i++) {
                                     ar_row.push(rows[i]);
                              }
                               
                              grid.getStore().remove(ar_row);
					 }
							 							  
				}
	         
	         	,itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  		
				  		id_selected = grid.getSelectionModel().getSelection();	
				  		var all_rec = grid.getStore().getRange();
				  		var grid_dx = this.up('window').down('#g_dx');
				  	
		                var gr_config = '';
		                var title = '';        
		                 for(var i=0; i<id_selected.length; i++){
		                   gr_config +=  id_selected[i].data.gruppo;
		                  
		                  }
		                   
		                   for(var i=0; i <= index ; i++){
		                    if(i == index)
		                    	title += ' [' + all_rec[i].get('codice').trim() + '] ' +all_rec[i].get('desc').trim();
		                    else
		                        title += ' [' + all_rec[i].get('codice').trim() + '] ' +all_rec[i].get('desc').trim() + ', ';
		                   }	
		                   
		                   
						  var voci_menu = [];
						 	 voci_menu.push({
			         		text: 'Configurazione indici',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        		
			        		 
			        		
			        		    if(rec.get('gruppo').trim() == ''){
			        		        acs_show_win_std('Configurazione indici di documentazione regole/attivit&agrave; di codifica', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form', {distinta: rec.get('distinta'), desc : rec.get('desc')}, 500, 200, null, 'icon-bookmark-16');
			        		    }else{
			        		    	acs_show_win_std('Configurazione indici di documentazione regole/attivit&agrave; di codifica', 'acs_anag_artd_config_distinta.php?fn=open_tab', {cod: rec.get('distinta'), des : rec.get('desc'), gruppo : gr_config, title : title}, 1300, 500, null, 'icon-bookmark-16');
			        		    }
			        		
					    		  
				                }
			    		 });	 
			    		 
			    		 if(rec.get('radice') != ''){
			    		 voci_menu.push({
			         		text: 'Verifica stringhe',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
					    		  acs_show_win_std('Elenco stringhe', 'acs_anag_artd_elenco_stringhe.php?fn=open_elenco', {gruppo : gr_config, distinta: rec.get('distinta'), radice : rec.get('radice'), from_dist : 'Y'}, 1050, 500, null, 'icon-gear-16');
				                }
			    		 });	 
			    		}
			    		
			    		
			    		if(grid_dx.cugino != ''){
			    		 
						 	voci_menu.push({
			         		text: 'Configura da cugino',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        		     
			        		      var cugino = grid_dx.cugino;
			        		      
			        		      Ext.Ajax.request({
						        url: 'acs_anag_art_duplica_indici.php?fn=exe_rec_cd',
						        jsonData: {
						            rec : rec.data					        	
						         },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        	jsonData = Ext.decode(result.responseText);
						        	
 						        	acs_show_win_std('Codice articolo', '../desk_utility/acs_anag_artd_elenco_stringhe.php?fn=open_codice', {
            	            		row : jsonData.row, row_c : cugino, row_p : rec.data, distinta: rec.get('distinta'), from_cugino : 'Y'}, 
            	            		450, 220, null, 'icon-leaf-16');
 						        	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						            console.log('errorrrrr');
						        }
						    });	
			        		  
		 
			        		 
			        		 }
			    		 });
				  	    }
			    		
			    		
			    	    <?php if($m_params-> att == 'A'){?>
			    		 
		    		 	 voci_menu.push({
			         		text: 'Nuovo progetto',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		      acs_show_win_std('Nuovo progetto di codifica', 
			        		      			'acs_panel_todolist_progetto.php?fn=open_filtri', 
			        		      			{cod : '<?php echo $m_params->cod; ?>'} , 800, 450, {}, 'icon-arrivi-16');             
			        			  
			        		 }
			    		 });
			    		 
			    		 <?php }?>
			    	
			    		
			    		
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
	       	
				   
				 }
				 
		
		
		}, //grid
				
						{
						xtype: 'grid',
						title: '',
						flex: 2,
						itemId : 'g_dx',
				        loadMask: true,	
				        stateful: true,
				        stateId: 'grid_sx_indici',
        				stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
				        cugino : '',
				        tasto_dx_title : '',
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
										 distinta: '<?php echo $m_params->cod?>',
										 form_values : <?php echo acs_je($form_values); ?>, 
										 gruppo: '',
										 radice : ''										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['distinta', 'voce', 'gruppo', 'desc', 'radice', 'conf_radice', 'new', 
		        			         'ant', 'art', 'prog', 'nr_id', 'ardu', 'idme', 'master', 'nota', 't_nota',
		        			         'ul_row', 'flag', 'tipo_lista', 'red_ant', 'p_a', 'p_da', 'opzione', 'todo', 't_todo']							
									
			}, //store
			
			 <?php $ant = "<img src=" . img_path("icone/48x48/search.png") . " height=15>"; ?>
	         <?php $gen = "<img src=" . img_path("icone/48x48/exchange.png") . " height=15>"; ?>
	         <?php $art = "<img src=" . img_path("icone/48x48/barcode.png") . " height=15 >"; ?>
	         <?php $nota = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=15 >"; ?>
			
				columns: [	
				
				   {
	                header   : 'Posizione',
	                dataIndex: 'conf_radice',
	                width : 90,
	                <?php if($form_values->f_debug == 'Y'){?> 
	                 hidden : false
	                <?php }else{?>
	                 hidden : true
	                <?php }?>
	                },{
	                header   : 'Stringa',
	                dataIndex: 'radice',
	                width : 50,
	                <?php if($form_values->f_debug == 'Y'){?> 
	                 hidden : false
	                <?php }else{?>
	                 hidden : true
	                <?php }?>
	                } ,
			      {
	                header   : 'Voce',
	                dataIndex: 'voce',
	                width : 50,
	                renderer: function(value, p, record){
    		    	       if (record.get('tipo_lista') == 'L')
						    return record.get('opzione');
						    
						    return value;
    		    		  }
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'desc',
	                flex: 1
	                }, {text: '<?php echo $nota; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'nota',
    				tooltip: 'Note',		        			    	     
    		    	renderer: function(value, p, record){
						   if(!Ext.isEmpty(record.get('t_nota')))
						      p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_nota')) + '"';    		    	
    		    	
    		    	       if(record.get('nota') > 0)
    		    			 return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
    		    		   else
    		    		     return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
		    		  }
    		        }, {text: '<?php echo $ant; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'ant',
    				tooltip: 'Anteprima',		        			    	     
    		    	renderer: function(value, p, record){
    		    	       if (record.get('red_ant') == '0')
						     p.tdCls += ' sfondo_grigio';
    		    	
    		    	        if(record.get('todo').trim() != ''){
    		    			  p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_todo')) + '"';  
    		    			  return '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=15>';
    		    			}
    		    			
    		    			if(record.get('flag') == 'Z' || record.get('flag') == 'A')
    		    			 return '<img src=<?php echo img_path("icone/48x48/search.png") ?> width=15>';
    		    		  }
    		        }, {text: '<?php echo $art; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'art',
    				tooltip: 'Visualizza articoli',		        			    	     
    		    	renderer: function(value, p, record){
    		    		  if(record.get('flag') == 'A')
    		    			 return '<img src=<?php echo img_path("icone/48x48/barcode.png") ?> width=15>';
    		    		  }
    		        }, {text: '<?php echo $gen; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'new',
    				tooltip: 'Duplica',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('new') == 'Y') return '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=15>';
    		    		  }
    		        }
    	
	         ]
	    
	         
	         , listeners: {
	         
	         	 /* cellclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
        		         var rec = iView.getRecord(iRowEl);
        		         var col_name = iView.getGridColumns()[iColIdx].dataIndex;
        		 
        		        
        		    }*/
	         
	      
	            celldblclick: {
	            
	            		//su elenco voci (grid dx) - Sto selezionando la risposta
	            
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  
							  var grid = this;
							  var grid_sx = this.up('window').down('#g_sx');
							  
							  if(col_name == 'art'){
							 
							  		if(rec.get('flag') == 'A'){							  		
    							  		acs_show_panel_std('acs_panel_anag_art.php?fn=open_tab', null, {
                                    	form_open: {radice :  rec.get('radice')}
                                    	}, null, null);
                                    	this.up('window').close();                                    	
							  		}							   
							  }else if(col_name == 'ant' && (rec.get('flag') == 'Z' || rec.get('flag') == 'A')){
							  		acs_show_win_std('Elenco articoli', 'acs_anag_artd_elenco_stringhe.php?fn=open_art', {
                        			c_art : rec.get('gruppo'), radice : rec.get('radice'), from_ant : 'Y', rows : rec.data,
                        			form_values : <?php echo acs_je($form_values); ?>}, 
                        			450, 450, null, 'icon-leaf-16');                              
							  }else if(col_name == 'new' && rec.get('new') == 'Y'){
							     
							     acs_show_win_std('Duplica articolo ' + rec.get('ardu'), 
				                			'acs_anag_art_duplica.php?fn=open_form', {
				                             d_art_new: rec.get('desc'),
				                             c_art: rec.get('ardu'),
				                             from_indici : 'Y',
				                             radice : rec.get('gruppo'),
				                             nr_id : rec.get('nr_id'),
				                             prog : rec.get('prog'),
				                             voce : rec.get('voce'),
				                             master : rec.get('master'),
				                             distinta : rec.get('distinta')
									       }, 500, 450, {
				        					'afterSave': function(from_win){
				        						grid.getStore().load();
	 										}
				        				}, 'icon-comment_add-16');
							  
							  
							  }else{
							
							  if(rec.get('ul_row') != 'Y'){
							
            		             grid.getStore().proxy.extraParams.distinta = rec.get('distinta');
            		             grid.getStore().proxy.extraParams.gruppo = rec.get('gruppo');
            		             grid.getStore().proxy.extraParams.radice = rec.get('radice');
            		             grid.getStore().load();
            		          
            		             if(rec.get('flag') == 'L')
						    		var voce = rec.get('opzione').trim();
						    	 else	
						    	 	var voce = rec.get('voce').trim();
            		          
            		             var values = {codice: voce, desc : rec.get('desc'), gruppo : rec.get('gruppo'), distinta : rec.get('distinta'), radice : rec.get('radice')};
                                 grid_sx.getStore().add(values); 
                                 grid.tasto_dx_title += '[' + voce + '] ' + rec.get('desc').trim() +' ';                                
                                 }
        		             }
							 
						  }
			   		  }
			   		  
			   	, itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  		
				  						  		
				  		 var voci_menu = [];
						 	voci_menu.push({
			         		text: 'Configurazione indici',
			        		iconCls : 'icon-search-16',          		
			        		handler: function () {
			        		
			        				if(rec.get('master') == '')
			        				 	var gruppo =  'START';
			        				else
			        				    var gruppo =  rec.get('master');
			        		

			        		      title  = '<?php  echo "[{$m_params->cod}] {$m_params->desc} "?>';
			        		
					    		  acs_show_win_std('Configurazione indici di documentazione regole/attivit&agrave; di codifica', 'acs_anag_artd_config_distinta.php?fn=open_tab', {
					    		  distinta: rec.get('distinta'), gruppo : gruppo, cod : '<?php echo $m_params->cod; ?>', des : '<?php echo $m_params->desc; ?>' , title : title
					    		  }, 1300, 500, null, 'icon-bookmark-16');

				                }
			    		 });
			    		 
			    		 
			    		  
						 	voci_menu.push({
			         		text: 'Memorizza per configurare',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
			        		       grid.panel.cugino = rec.data;
			        			  
			        		 }
			    		 });
			    		 
			    		 
			    	
			    		   var menu = new Ext.menu.Menu({
				            items: voci_menu
					        }).showAt(event.xy);	
						  
						  }
	         
				   
				 }, viewConfig: {
		       		 getRowClass: function(record, index) {	
		       		 
		       		    if(record.get('ul_row') == 'Y' && record.get('new') == 'Y')
                		   return ' colora_riga_verde';
        		        
        		        if(record.get('ul_row') == 'Y')
                		   return ' colora_riga_giallo';
                		
                															
		         }   
		    }
				 
		
		
		}, //grid
						   
						
						]}
							
					
		]}
		
		
		<?php 
		exit;
}

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	         		 {
							flex: 1,							
							name: 'f_seq_ind',
							xtype: 'combo',
							fieldLabel: 'Sequenza indice',
							anchor: '-5',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_seq_ind',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								},
								   extraParams: {
								   
								    distinta : '<?php echo $m_params->distinta; ?>'
        		 				},
        		 					doRequest: personalizza_extraParams_to_jsonData        		
							},
							
							fields: [{name:'id'}, {name:'text'}],
							}					 
							},
							 {
							flex: 1,							
							name: 'f_gruppo',
							xtype: 'combo',
							fieldLabel: 'Gruppo',
							anchor: '-5',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								},
								   extraParams: {
								   distinta : '<?php echo $m_params->distinta; ?>'
        		 				},
        		 					doRequest: personalizza_extraParams_to_jsonData        		
							},
							
							fields: [{name:'id'}, {name:'text'}],
							}					 
							},{
							xtype : 'textfield',
							name : 'f_d_voce',
							fieldLabel : 'Descrizione voce',
						    anchor: '-5',
							},{
							xtype : 'textfield',
							name : 'f_radice',
							fieldLabel : 'Radice',
						    anchor: '-5',
							}
	          
	          
				 
	            ],
	            
				buttons: [	
				{
			            text: 'Visualizza',
				        iconCls: 'icon-windows-32',	            
				        scale: 'large',		            
			            handler: function() {
			            
			            var form = this.up('form').getForm();
			            			            
			            acs_show_win_std('Configurazione indice di classificazione', 'acs_anag_artd_config_distinta.php?fn=open_tab', {cod: '<?php echo $m_params->distinta; ?>', des: '<?php echo $m_params->desc; ?>', form_values : form.getValues()}, 1300, 500, null, 'icon-gear-16');
			         
			                
			            }
			        }
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	exit;
	}
	
	
if ($_REQUEST['fn'] == 'get_json_data_seq_ind'){
    $m_params = acs_m_params_json_decode();

    $distinta = $m_params->distinta;
    
    $sql = "SELECT DISTINCT CDSEQI
    FROM {$cfg_mod_DeskArt['file_cfg_distinte']}
    WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$distinta}' ";
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while ($row = db2_fetch_assoc($stmt)){
        
        $ret[] = array("id"=>trim($row['CDSEQI']), "text" =>trim($row['CDSEQI']));
    }
    
    echo acs_je($ret);
    exit();


}

if ($_REQUEST['fn'] == 'get_json_data_gruppo'){
    $m_params = acs_m_params_json_decode();
    
    $distinta = $m_params->distinta;
    
    $sql = "SELECT DISTINCT CDCMAS
    FROM {$cfg_mod_DeskArt['file_cfg_distinte']}
    WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$distinta}' ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while ($row = db2_fetch_assoc($stmt)){
        
        $ret[] = array("id"=>trim($row['CDCMAS']), "text" =>trim($row['CDCMAS']));
    }
    
    echo acs_je($ret);
    exit();
    
    
}
