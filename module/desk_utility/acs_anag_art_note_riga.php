<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_acq = new DeskAcq();

$m_params = acs_m_params_json_decode();


if($_REQUEST['fn'] == 'exe_upd_commento'){
    
    $form_values = $m_params->form_values;
    //elimino il vecchio commento
    $sql = "DELETE
            FROM {$cfg_mod_DeskUtility['file_note_anag']}
            WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$m_params->rife1}'
            AND RLRIFE2 = '{$m_params->bl}'
            AND RLTPNO = 'RD'";
			
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    
    //divido il testo in blocchi da 80 caratteri e creo i record
    //$str = str_split($p['f_text'], 80);
    $str = array();
    if (strlen($form_values->f_text_0) > 0) $str[] = $form_values->f_text_0;
    if (strlen($form_values->f_text_1) > 0) $str[] = $form_values->f_text_1;
    if (strlen($form_values->f_text_2) > 0) $str[] = $form_values->f_text_2;
    if (strlen($form_values->f_text_3) > 0) $str[] = $form_values->f_text_3;
    if (strlen($form_values->f_text_4) > 0) $str[] = $form_values->f_text_4;
    
    if (strlen($form_values->f_text_5) > 0) $str[] = $form_values->f_text_5;
    if (strlen($form_values->f_text_6) > 0) $str[] = $form_values->f_text_6;
    if (strlen($form_values->f_text_7) > 0) $str[] = $form_values->f_text_7;
    if (strlen($form_values->f_text_8) > 0) $str[] = $form_values->f_text_8;
    if (strlen($form_values->f_text_9) > 0) $str[] = $form_values->f_text_9;

    $ar_ins = array();
    $ar_ins['RLDTGE'] 	= oggi_AS_date();
    $ar_ins['RLUSGE'] 	= $auth->get_user();
    $ar_ins['RLDT'] 	= $id_ditta_default;
    $ar_ins['RLRIFE1'] 	= $m_params->rife1;
    $ar_ins['RLRIFE2'] 	= $m_params->bl;
    $ar_ins['RLTPNO'] 	= 'RD';

    $c=0;
    foreach($str as $t){
        
        $ar_ins['RLRIGA']   = ++$c;
        $ar_ins['RLSWST'] 	= substr($t, 0, 1);  //1
        $ar_ins['RLREST1'] 	= substr($t, 1, 15);  //15
        $ar_ins['RLFIL1'] 	= substr($t, 16, 64);  //64

             
        $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_note_anag']}(" . create_name_field_by_ar($ar_ins) . ")
			    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
     
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
    }

    $ha_commenti = $desk_acq->has_commento_riga($m_params->rife1, $m_params->bl);
   
    if ($ha_commenti == FALSE)
        $img_com_name = "iconCommGray";
    else
        $img_com_name = "iconCommYellow";
    
    $ret = array();
    $ret['success'] = true;
    $ret['icon'] = $img_com_name;
    echo acs_je($ret);
    exit;
}


if($_REQUEST['fn'] == 'open_bl'){
 
    
?>    
    
 {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
            frame: true,
            title: '',
            items: [ 
                
                <?php 
                
                 
                
                    $sql = "SELECT RRN(TA) AS RRN, TANR, TADESC
                    FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
                    WHERE TADT = '{$id_ditta_default}'
                    AND TAID = 'NUSN'
                    AND TACOR1 = 'RD' AND TACOR2 = 'VO' ORDER BY TANR";
                
               
                $stmt = db2_prepare($conn, $sql);
                $result = db2_execute($stmt);
                
                                
                while($row = db2_fetch_assoc($stmt)){
                    
                    $c++;
                  
                    $txt_bl = "[".trim($row['TANR'])."] ".trim($row['TADESC']);
                    $ha_commenti = $desk_acq->has_commento_riga($_REQUEST['rife1'], $row['TANR']);
                
                    if ($ha_commenti == FALSE)
                        $img_com_name = "iconCommGray";
                    else
                        $img_com_name = "iconCommYellow";
                
                   
                    
                  
                ?>
                	
                		{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								xtype: 'button',
								text: '',
								itemId: 'f_bt_<?php echo $row['RRN']; ?>',
								margin : '0 5 0 0',
								iconCls: '<?php echo $img_com_name; ?>',
								scale: 'small',
								handler : function(){
								
								var form = this.up('form').getForm();
								var win = this.up('window');
								
								
			        		
			        			acs_show_win_std('Annotazioni riga ' + '<?php echo "blocco note {$row['TANR']}"?>', 
				    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
				    					{rife1: '<?php echo $_REQUEST['rife1']; ?>', bl: '<?php echo $row['TANR']; ?>'}, 800, 400,  {
			        					'afterSave': function(from_win, src){
			        					    var bt = win.down('#f_bt_<?php echo $row['RRN']; ?>');
			        					    bt.setIconCls(src.icon);
			        						from_win.close();
			        					}
			        				}, 'iconCommGray');
			        		
			        				//this.up('window').close();
									
								} 
							},
						
						{
						name: 'f_text_<?php echo $row['RRN'] ?>',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: <?php echo j($txt_bl); ?>						
					},
					
				
						]},
						
					<?php  if($ha_commenti == true){
					    
					    $commento_txt = $desk_acq->get_commento_riga($_REQUEST['rife1'], $row['TANR']);
					       
					    
					    $comm = "";
					    for($i = 0; $i <= count($commento_txt); $i++){
					    
					        $comm .= "$commento_txt[$i]";
					    }
					        ?>
					        
					        {
					            name: 'f_text_<?php echo $i ?>',
					            //xtype: 'displayfield',
					            xtype     : 'textareafield',
				                height: 40, width: '100%',
					            readOnly:true,
					            margin: '0 0 0 25',
					            anchor: '-15',
					            value: <?php echo j($comm); ?>
					         },
					
					
					
					<?php  }?>
					
					<?php }?>	
                	
                	
				]            
				
        }
]}   
    
    
    
    
<?php  
exit;
}


if($_REQUEST['fn'] == 'open_tab'){
    
 $bl = $m_params->bl;
 $rife1 = $m_params->rife1;

 $commento_txt = $desk_acq->get_commento_riga($rife1, $bl);
 
 $num_ann = 9;
    
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            items: [ 
                
                <?php for($i=0; $i<= $num_ann;$i++){ ?>
                	
                	{
						name: 'f_text_<?php echo $i ?>',
						xtype: 'textfield',
						fieldLabel: '',
					    anchor: '-15',
					    maxLength: 80,					    
					    value: <?php echo j(trim($commento_txt[$i])); ?>,							
					},	
					
					<?php }?>	
                	
                	
				],
			buttons: [
		
				{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	  
					
						Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upd_commento',
					        jsonData: {
					        	form_values: form.getValues(),
					        	rife1 : '<?php echo $rife1; ?>',
					        	bl : '<?php echo $bl; ?>'
					        	
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	
					        try {
                               var jsonData = Ext.decode(result.responseText);
							   loc_win.fireEvent('afterSave', loc_win, jsonData);
                            }
                            catch(err) {
                                Ext.Msg.alert('Try&catch error', err.message);
                            }
					       
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					            console.log('errorrrrr');
					        }
					    });	  
					
	

				    }            	                	                
	            }
	        }
   
	        ]             
				
        }
]}

<?php }
exit;