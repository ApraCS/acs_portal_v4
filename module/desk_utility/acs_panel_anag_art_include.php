<?php 

function sql_where_params($form_ep){
    
    global $auth;
    
    $sql_where = "";
    
    $sql_where.= sql_where_by_combo_value('AR.ARTPAR', $form_ep->f_tipo_parte);
    $sql_where.= sql_where_by_combo_value('AR.ARCLME', $form_ep->f_classe);
    $sql_where.= sql_where_by_combo_value('AR.ARGRME', $form_ep->f_gruppo);
    $sql_where.= sql_where_by_combo_value('AR.ARSGME', $form_ep->f_sottogruppo);
    $sql_where.= sql_where_by_combo_value('AR.ARTAB1', $form_ep->f_pianificazione);
    $sql_where.= sql_where_by_combo_value('AR.ARFOR1', $form_ep->f_fornitore);
    $sql_where.= sql_where_by_combo_value('AR.ARMODE', $form_ep->f_configuratore);
    
    
    if (strlen($form_ep->f_data_dal) > 0)
        $sql_where.= " AND AR.ARDTGE >= $form_ep->f_data_dal ";
    if (strlen($form_ep->f_data_al) > 0)
        $sql_where.= " AND AR.ARDTGE <= $form_ep->f_data_al ";
    
    
    
   
    if($form_ep->f_cod_art != ''){
        $sql_where.= " AND TRIM(UPPER(AR.ARART)) LIKE '{$form_ep->f_cod_art}'";
        //$sql_where.= artd_ARART_add_where($form_ep->f_cod_art);
    }elseif($form_ep->radice != ''){
        $sql_where.= artd_ARART_add_where($form_ep->radice);
        
    }else{
        
        $ar_codici = array();
        
        $text = "";
        for($i = 1; $i <= 15; $i++){
            $filtro = 'f_'.$i;
            if($form_ep->$filtro == '')
                $form_ep->$filtro = '_';
            $text .= $form_ep->$filtro;
        }
        
        if($text != "_______________")
            $ar_codici[] = $text;
        
      for($n = 1; $n <= 20; $n++){
         $cod_art = "";
         for($i = 1; $i <= 15; $i++){
             $cod = "f_[{$n}]{$i}";
             if($form_ep->$cod == '')
                $form_ep->$cod = '_';
             $cod_art .= $form_ep->$cod;
         }
         
         if(trim($cod_art) != "_______________")
             $ar_codici[] = $cod_art;
        
      }
    
      if(count($ar_codici)>0){
             
             $sql_where.= " AND ( 1 = 2";
             foreach($ar_codici as $cod){
                 $sql_where.= " OR AR.ARART LIKE '{$cod}' "; //OR
             }
             $sql_where .= ")";
        
      }
  
    }

    
   
    if($form_ep->f_riferimento != ''){
        $sql_where.= " AND TRIM(UPPER(ARARFO)) LIKE '{$form_ep->f_riferimento}'";
    }
    
    if($form_ep->f_alternativo != ''){
        $sql_where.= " AND TRIM(UPPER(ARGRAL)) LIKE '{$form_ep->f_alternativo}'";
    }
    
    if($form_ep->f_rif_tecnico != ''){
        $sql_where.= " AND TRIM(UPPER(ARRIF)) LIKE '{$form_ep->f_rif_tecnico}'";
    }
    
    $sql_where.= sql_where_by_combo_value('AR.ARSWTR', $form_ep->f_tipo_riordino);
 
    
    
    $ar_esau = array();
    if ($form_ep->f_esau == 'E')
        array_push($ar_esau, $form_ep->f_esau);
    if ($form_ep->f_avv == 'A')
        array_push($ar_esau, $form_ep->f_avv);
    if ($form_ep->f_corso == 'C')
        array_push($ar_esau, $form_ep->f_corso);
    if ($form_ep->f_riser == 'R')
        array_push($ar_esau, $form_ep->f_riser);
    if ($form_ep->f_fine == 'F')
        array_push($ar_esau, $form_ep->f_fine);
    if ($form_ep->f_attivo == 'AT')
        array_push($ar_esau, '');
    if (count($ar_esau) > 0)
        $sql_where.= sql_where_by_combo_value('AR.ARESAU', $ar_esau);
    
    if ($form_ep->f_obs == 'Y')
        $sql_where.= " AND AR.ARFLR1 <> ''";
    
    if ($form_ep->f_l_mov == 'Y')
        $sql_where.= " AND AR.ARFLR2 <> ''";
    
        
    if (strlen($form_ep->f_desc_art) > 0)
        $sql_where.= " AND UPPER(AR.ARDART) LIKE '%" . strtoupper($form_ep->f_desc_art) . "%' ";
    
    if ($form_ep->f_sospeso == 'Y')
        $sql_where.= " AND AR.ARSOSP = 'S'";
    if ($form_ep->f_sospeso == 'N')
        $sql_where.= " AND AR.ARSOSP <> 'S'";
    
    if (strlen($form_ep->f_ut_imm) > 0)
        $sql_where.= " AND AR.ARUSGE = '{$form_ep->f_ut_imm}' ";
    
    if($form_ep->f_sfg[0] == 'N')
        $sql_where .= " AND (AXSFON IS NULL  OR AXSFON = '')";
    else
        $sql_where.= sql_where_by_combo_value('AXSFON', $form_ep->f_sfg);
    
        
    if($form_ep->f_sfp[0] == 'N')
        $sql_where .= " AND (AUSFAR IS NULL  OR AUSFAR = '')";
    else
        $sql_where.= sql_where_by_combo_value('AUSFAR', $form_ep->f_sfp);

    
     // TODO, IL JOIN SI TROVA NELL'ALTRO FILE   

   
   
    
    //LISTINI, I JOIN SONO NELL'ALTRO FILE      
    
    /*if(isset($form_ep->f_lv) && count($form_ep->f_lv) > 0){
       $sql_where.= sql_where_by_combo_value('LI.LILIST', $form_ep->f_lv);
    }
    if(isset($form_ep->f_la) && count($form_ep->f_la) > 0){
        $sql_where .= sql_where_by_combo_value('LI.LILIST', $form_ep->f_la);
    }*/
       
    
    return $sql_where;
        
}