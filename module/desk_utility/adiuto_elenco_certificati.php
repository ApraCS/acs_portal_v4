<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$deskArt = new DeskArt();

$m_params = acs_m_params_json_decode();


//**************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
//**************************************************************

    global $mssql_adiuto;
    $host 	= $mssql_adiuto['host'];
    $dbname = $mssql_adiuto['dbname'];
    $usr 	= $mssql_adiuto['usr'];
    $psw  	= $mssql_adiuto['psw'];
    $db = new PDO("dblib:host={$host};dbname={$dbname};charset=utf8",$usr,$psw);	
    
    if(strlen($m_params->open_request->c_fornitore) > 0)
        $where = " AND F1012 = '{$m_params->open_request->c_fornitore}'";
    
    $sqlA = "
	 	SELECT F1012 as c_forn,
               F1004 as d_forn,
               F1086 as certificato_des_estesa,
               F1157 as certificato_des,
               F1096 as tipo_certificato,
               F11   as nome_file,
               '\\\\192.168.1.138' + REPLACE(F10, '/', '\') as path_file

               FROM [STOSA_DATA].[dbo].[A1065]
               WHERE F1088='CERT-FORN-' 
                 and F1112>=convert(varchar(112),getdate(),112)
                {$where}
	 		";

    
    $stmtA=$db->prepare($sqlA);
    $stmtA->execute();
    
    $ret = array();
    
    foreach ($stmtA as $rowA) {
        $n = array();
        $ret[] = $rowA;
    }
    
    
    
    
    echo acs_je($ret);
    
 exit;
}



//**************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
//**************************************************************
?>
{"success":true, "items": [
	{
			xtype: 'grid',
	        loadMask: true,	 
	   		store: {
				xtype: 'store',
				autoLoad:true,

					proxy: {
					   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
					   method: 'POST',								
					   type: 'ajax',

				        actionMethods: {
				          read: 'POST'
				        },
				        
				        extraParams: {
							 open_request: <?php echo acs_je($m_params) ?>
        				},
        				
        				doRequest: personalizza_extraParams_to_jsonData, 
			
					    reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
			        	}
				},
				
    			fields: ['d_forn', 'certificato_des', 'tipo_certificato', 'nome_file', 'path_file']
			}, //store
	
			      columns: [
			        {
	                header   : 'Fornitore',
	                dataIndex: 'd_forn',
	                flex: 1
	                },
			      	{
	                header   : 'Certificato',
	                dataIndex: 'certificato_des',
	                flex: 1
	                },
			      	{
	                header   : 'Tipo',
	                dataIndex: 'tipo_certificato',
	                width: 100
	                },
			      	{
	                header   : 'File',
	                dataIndex: 'nome_file',
	                flex: 1
	                }
	                
	                //ToDo: integrarsi con adiuto? sul portale usano una loro funzione openDetail
	                /*
	                , {
	                	header: 'Pdf',
	                	dataIndex: 'path_file',
	                	width: 100,
	                	renderer: function(value, metaData, record){
	                		var ret = '';
	                		ret = '<a href="' + value + '" target=_blank>Pdf</a>';
	                		return ret;
	                	}
	                	
	                }*/
	                
	               ] //columns
	                

			
			, listeners: {
				celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           	console.log('aaa');
		           	var rec = iView.getRecord(iRowEl);
		           	var loc_win = iView.up('window');
		           	loc_win.fireEvent('afterSelect', loc_win, rec);	
		           	}
		        }
			} //listeners
					       
		
		
		}
	]
}
<?php 
    exit;
}