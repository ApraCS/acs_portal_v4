<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 


   
    $sql = "SELECT AR.*, LI.*, CF_FORNITORE.CFRGS1 AS D_FORNITORE
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
                ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_listini']} LI
            ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART AND LI.LITPLI = 'V'
            WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$_REQUEST['articolo']}'
            ORDER BY ARART";
    
    

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
    
        //stacco dei livelli
        $cod_liv0 = trim($row['ARART']);
        $cod_liv1 = implode("_", array(trim($row['LIVAR1']), trim($row['LIVAR2']), trim($row['LIVAR3']), trim($row['LILIST']), trim($row['LIUM']), trim($row['LIVALU'])));
            
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //ARTICOLO
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
           // $ar_new = $row;
          //  $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = "[{$row['ARART']}] {$row['ARDART']}";
            $ar_new['articolo'] = $row['ARART'];
            $ar_new['liv'] = 'liv_0';
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        
        //VARIANTI
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] =  $row['ASDOCU'];
            $ar_new['liv'] = 'liv_1';
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
            
        }
        
        $ar_r=&$ar_r[$liv];
        
    }

echo "<div id='my_content'>";
echo "<div class=header_page>";
echo "<H2>Riepilogo prezzi di vendita per variante/listino</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";
echo "<tr class='liv_data'>";

echo "
	<th rowspan=2>Articolo/Variante</th>
	
    <th rowspan=2>U.M.</th>
    <th rowspan=2>Val.</th>
    <th rowspan=2>Listino</th>
	<th rowspan=2>Prezzo</th>
    
    <th rowspan=2>Sconti</th>
    <th rowspan=2>Magg.</th>
    
    <th colspan = 2 class='center'> Validit&agrave; </th>
    <th colspan = 2 class='center'> Immissione </th>
      <tr class='liv_data'>
          <th>Iniziale</th>
          <th>Finale</th>
          <th>Data</th>
          <th>Utente</th>
      </tr>
 


      ";

    
echo "</tr>";  


if(is_array($ar)){
foreach ($ar as $kar => $r){
    
    echo "<tr class = liv0>";
    echo "<td colspan = '11'>{$r['task']}</td>";
    echo "</tr>";
    
    foreach($r['children'] as $kar1 => $r1){
        
        $varianti = "";
        
         
         if(trim($r1['ARVAR1']) == '' && isset($cfg_mod_DeskArt["var_def_listino_1"]))
             $dom1 = $cfg_mod_DeskArt["var_def_listino_1"];
         else
             $dom1 = trim($r1['ARVAR1']);
                 
         if(trim($r1['ARVAR2']) == '' && isset($cfg_mod_DeskArt["var_def_listino_2"]))
             $dom2 = $cfg_mod_DeskArt["var_def_listino_2"];
         else
             $dom2 = trim($r1['ARVAR2']);
                         
         if(trim($r1['ARVAR3']) == '' && isset($cfg_mod_DeskArt["var_def_listino_3"]))
             $dom3 = $cfg_mod_DeskArt["var_def_listino_3"];
         else
             $dom3 = trim($r1['ARVAR3']);
                                 
         $val_risp = find_TA_sys('PUVN', trim($r1['LIVAR1']), null, $dom1, null, null, 0, '', 'N', 'Y');
         if(trim($r1['LIVAR1']) != '')
             $varianti .= "[".trim($r1['LIVAR1'])."] ".$val_risp[0]['text'];
         $val_risp = find_TA_sys('PUVN', trim($r1['LIVAR2']), null, $dom2, null, null, 0, '', 'N', 'Y');
         if(trim($r1['LIVAR2']) != '')
             $varianti .= "<br>[".trim($r1['LIVAR2'])."] ".$val_risp[0]['text'];
         $val_risp = find_TA_sys('PUVN', trim($r1['LIVAR3']), null, $dom3, null, null, 0, '', 'N', 'Y');
         if(trim($r1['LIVAR3']) != '')
             $varianti .= "<br>[".trim($r1['LIVAR3'])."] ".$val_risp[0]['text'];
         
         $sconti = n($r1['LISC1'],2)."%";
         if($r1['LISC2'] > 0)
             $sconti .= " - ". n($r1['LISC2'],2)."%";
         if($r1['LISC3'] > 0)
             $sconti .=  " - ". n($r1['LISC3'],2)."%";
         if($r1['LISC4'] > 0)
             $sconti .=  " - ". n($r1['LISC4'],2)."%";
             
         if($r1['LIMAGG'] > 0)
            $magg = "<b>+</b>".n($r1['LIMAGG'],2)."%";
         else
            $magg = "&nbsp;";
                                 
        
        echo "<tr>";
        echo "<td>{$varianti}</td>";
        echo "<td>{$r1['LIUM']}</td>";
        echo "<td>{$r1['LIVALU']}</td>";
        echo "<td>{$r1['LILIST']}</td>";
        echo "<td class = 'number'>".n($r1['LIPRZ'])."</td>";
        echo "<td>".$sconti."</td>";
        echo "<td>".$magg."</td>";
        echo "<td>".print_date($r1['LIDTDE'])."</td>";
        echo "<td>".print_date($r1['LIDTVA'])."</td>";
        echo "<td>".print_date($r1['LIDTGE'])."</td>";
        echo "<td>".$r1['LIUSGE']."</td>";
        echo "</tr>";
        
    }


    
        
        
    
  
    
	/*echo "<tr>";
	echo "  <td>".$c_art."</td>
   			<td>".$d_art."</td>
            <td>".$r['LILIST']."</td>
            <td>".$r['LIUM']."</td>
           	<td class = 'number'>".n($r['LIPRZ'])."</td>
            <td>".$r['LIVALU']."</td>
            <td>".$sconti."</td>
            <td>".$magg."</td>
            <td>".$varianti."</td>
            <td>".print_date($r['LIDTGE'])."</td>
            <td>".print_date($r['LIDTDE'])."</td>
            <td>".print_date($r['LIDTVA'])."</td>";
    echo "</tr>";*/
            

            
}

}


?>
</div>
</body>
</html>	

