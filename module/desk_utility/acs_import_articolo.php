<?php

require_once("../../config.inc.php");
require_once("../base/_rilav_g_history_include.php");

$desk_art = new DeskArt();
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    
    $articolo = $m_params->articolo;
    
    $sql_max = " SELECT MAX(IDIDPR) AS M_IDIDPR
                 FROM {$cfg_mod_DeskArt['file_import_d']} ID
                 WHERE ID.IDCART = '{$articolo}'";
    
    $stmt_max = db2_prepare($conn, $sql_max);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_max);
    $rmax = db2_fetch_assoc($stmt_max);
    $max_id = $rmax['M_IDIDPR'];
    
    $_rilav_H_for_query = _rilav_H_for_query(
        array('f_ditta' => 'ITDITT', 'field_IDPR' => 'ITIDPR'),
        $cfg_mod_DeskArt['file_import_t'],
        $cfg_mod_DeskArt['file_tabelle'],
        'IT',
        'CVICA',
        array('CV')
        );
    
    
    if($m_params->riepilogo == 'Y'){
        $select = "IDIDPR, ITCATA, TA_CT.TADESC AS D_CAT, ITVERS, TA_VR.TADESC AS D_VERS, 
                   ITDTGE, {$_rilav_H_for_query['select']}";
        $join = " LEFT OUTER JOIN {$cfg_mod_DeskArt['file_import_t']} IT
                    ON /*IT.ITDITT = ID.IDDITT AND*/ IT.ITIDPR = ID.IDIDPR
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_CT
                    ON TA_CT.TATAID = 'CATIN' AND IT.ITCATA = TA_CT.TAKEY1 
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_VR
                    ON TA_VR.TATAID = 'CAVER' AND IT.ITCATA = TA_VR.TAKEY1 AND IT.ITVERS = TA_VR.TAKEY2 
                  {$_rilav_H_for_query['joins']}";
        $group_by = "GROUP BY IDIDPR, ITCATA, TA_CT.TADESC, ITVERS, TA_VR.TADESC, ITDTGE, {$_rilav_H_for_query['group_by']}";
        $order_by = "ORDER BY IDIDPR DESC, ITCATA, TA_CT.TADESC, ITVERS, TA_VR.TADESC, ITDTGE";
    }else{
        $select = "IDIDPR, IDCCAM, IDVALA, IDVALN, TA_DT.TADESC AS DESC, TA_DT.TAPROV AS FORMATO, TA_DT.TATELE AS SEQ";
        $join = " LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_DT
                    ON /*ID.IDDITT = TA_DT.TADT AND*/ TA_DT.TATAID = 'DTIMP' AND ID.IDCCAM = TA_DT.TAKEY1";
        
        if(strlen($m_params->id_prog) > 0)
            $sql_where = " AND IDIDPR = {$m_params->id_prog}";
        else
            $sql_where = " AND IDIDPR = {$max_id}";
        $order_by = "ORDER BY TA_DT.TATELE";
    }
    
    
     
    $sql = " SELECT {$select}
             FROM {$cfg_mod_DeskArt['file_import_d']} ID
             {$join}
             WHERE ID.IDCART = '{$articolo}' {$sql_where}
             {$group_by} 
             {$order_by}
             ";
         
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
       
        $nr['id_prog']  = $row['IDIDPR'];
        $nr['max_prog']  = $max_id;
        if($nr['max_prog'] == $nr['id_prog'])
            $nr['recente'] = 'Y';
        $nr['catalogo']  = "[".trim($row['ITCATA'])."] ".trim($row['D_CAT']);
        $nr['versione']  = "[".trim($row['ITVERS'])."] ".trim($row['D_VERS']);
        $nr['H_CV_COD']          = trim($row['H_CV_COD']);
        $nr['H_CV_ICONA']        = trim($row['H_CV_ICONA']);
        $nr['H_CV_STYLE_CLASS']  = trim($row['H_CV_STYLE_CLASS']);
        $nr['data']  = trim($row['ITDTGE']);
        $nr['codice']  = trim($row['IDCCAM']);
        $nr['descrizione']  = trim($row['DESC']);
        if(trim($row['FORMATO']) == 'T')
            $nr['valore']  = trim($row['IDVALA']);
        if(trim($row['FORMATO']) == 'N')
            $nr['valore']  = n($row['IDVALN'], 2);
        if($m_params->riepilogo != 'Y'){
             if(strlen($nr['valore']) > 0 && $nr['valore'] != '-')    
                 $ar[] = $nr;
        }else{
            $ar[] = $nr;
        }
               
    }
    
    echo acs_je($ar);
    exit;
}


if ($_REQUEST['fn'] == 'open_grid'){ ?>

{"success":true, 
	m_win: {
		title: 'Dettaglio dati import [' + <?php echo j($m_params->articolo)?> + ']',
	}, 

"items": [

     {
		xtype: 'panel',
		autoScroll : true,
    	layout: {
			type: 'vbox',
			align: 'stretch',
			pack : 'start',
		},
			items: [ 
    {
			xtype: 'grid',
			title: 'Cataloghi import',
	        loadMask: true,
        	autoScroll : true,
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],		
        	store: {
			//xtype: 'store',
			autoLoad:true,

		proxy: {
		   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
		   method: 'POST',								
		   type: 'ajax',

	       actionMethods: {
	          read: 'POST'
	        },
	        
	        
	           extraParams: {
				 articolo: <?php echo j($m_params->articolo) ?>,
				 riepilogo : 'Y',
				
				 
			},
			
			doRequest: personalizza_extraParams_to_jsonData, 

		   reader: {
            type: 'json',
			method: 'POST',						            
            root: 'root'						            
        }
	},
	
	fields:  [ 'recente', 'max_prog', 'id_prog', 'catalogo', 'versione', 'data',
			   <?php echo _rilav_H_write_model_fields(array('CV')) ?>]
							
									
			}, //store
					
			      columns: [	
			       {
	                header   : 'ID',
	                dataIndex: 'id_prog',
	                width : 70,
	                },
	                {
	                header   : 'Catalogo',
	                dataIndex: 'catalogo',
	                flex : 1
	                },
	                {
	                header   : 'Versione',
	                dataIndex: 'versione',
	                flex : 1
	                },
                   {text: 'CV',					
	          		dataIndex: 'H_CV_COD', tooltip: 'Ciclo vita', width: 35, align: 'right',
	          		exeGestRilavH: 'CV', exeGestRilavH_causale : 'CVICA',
	          		renderer: function (value, metaData, rec, row, col, store, gridView){
	          		  <?php echo _rilav_H_icon_style('CV') ?>
	          			
    	        	}},
	                {
	                header   : 'Data generazione',
	                dataIndex: 'data',
	                width : 150,
	                renderer: date_from_AS
	                },
	         ], listeners: {
	         
	         	itemclick: function(view,rec,item,index,eventObj) {
				  var m_grid = this;
				  var rows =   m_grid.getStore().getRange();
				  Ext.each(rows, function(row) {
    	  			    row.set('recente', '');
    	  			    row.commit();
        			});	
				 
				  rec.set('recente', 'Y');
				  rec.commit();
				  var grid = this.up('panel').down('#dx_grid');
				  grid.getStore().proxy.extraParams.id_prog = rec.get('id_prog');
				  grid.getStore().load();
							        	
        	     },
	     
			
		},
		 viewConfig: {
       		 getRowClass: function(record, index) {	
       	      if(record.get('recente') == 'Y')
           		   return  ' grassetto';
           														
        		 }   
    }
		
		},
	     {
			xtype: 'grid',
			title: '',
			itemId : 'dx_grid',
	        loadMask: true,
        	autoScroll : true,
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],		
        	store: {
			//xtype: 'store',
			autoLoad:true,

		proxy: {
		   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
		   method: 'POST',								
		   type: 'ajax',

	       actionMethods: {
	          read: 'POST'
	        },
	        
	        
	           extraParams: {
				 articolo: <?php echo j($m_params->articolo) ?>
			},
			
			doRequest: personalizza_extraParams_to_jsonData, 

		   reader: {
            type: 'json',
			method: 'POST',						            
            root: 'root'						            
        }
	},
	
	fields:  ['id_prog', 'codice', 'descrizione', 'valore']
							
									
			}, //store
					
			      columns: [	
			      /*{
	                header   : 'ID',
	                dataIndex: 'id_prog',
	                width : 70,
	                },*/{
	                header   : 'Codice',
	                dataIndex: 'codice',
	                width : 70,
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'descrizione',
	                flex : 1
	                },
	                {
	                header   : 'Valore',
	                dataIndex: 'valore',
	                flex : 2,
                 	renderer: function(value, metaData, record){
	                  	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';    			    	
            		    return value;	
                	}
	                },
	                
	         ], listeners: {
	         
	     
			
		}
		
		}
		
		]}
			
     
]} 

<?php 
exit;
}
