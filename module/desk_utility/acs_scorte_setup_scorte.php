<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

ini_set('max_execution_time', 90000);

if ($_REQUEST['fn'] == 'exe_mod_rop'){
	$ret = array();
	$m_params = acs_m_params_json_decode();


	$sh = new SpedHistory($m_DeskAcq);
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> 'SCO_SAVE_ROP',
					"vals" => array("RICITI" =>$m_params->values->form_values->f_divisione),
			)
			);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}

/*************************************************************************
 * SALVA QUANTITA'
 *************************************************************************/
if ($_REQUEST['fn'] == 'exe_save_quant'){

	global $conn, $auth;
	global $cfg_mod_DeskUtility, $id_ditta_default;

	
	$m_params = acs_m_params_json_decode();
	$r = $m_params->list_selected_id; //elenco chiavi di fabbisogno

	$ar_upd = array();
	$ar_upd['SCSCF0'] 	=  $r->SCSCF0;  //(float)
	$ar_upd['SCUTM0'] 	=  $auth->get_user();
	$ar_upd['SCDTM0'] 	=  oggi_AS_date();
	$ar_upd['SCORM0'] 	=  oggi_AS_time();
	if($r->SCSCF0 == 0){
	  	$ar_upd['SCDTF0'] =  0;
	}else{
		$ar_upd['SCDTF0'] = oggi_AS_date();
	}
	
	
	$sql = "UPDATE {$cfg_mod_DeskUtility['file_scorte']} SC
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE  RRN(SC) = '{$r->rrn}' AND SCDIT0 = '{$id_ditta_default}' 
			AND SCDIV0 = '{$r->SCDIV0}' AND SCCAR0 = '{$r->SCCAR0}'";

	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_upd);
	echo db2_stmt_errormsg($stmt);
	
	$sql_s = "SELECT *
			  FROM {$cfg_mod_DeskUtility['file_scorte']} SC
			  WHERE  RRN(SC) = '{$r->rrn}' AND SCDIT0 = '{$id_ditta_default}' 
			  AND SCDIV0 = '{$r->SCDIV0}' AND SCCAR0 = '{$r->SCCAR0}'";
			
	$stmt_s = db2_prepare($conn, $sql_s);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_s);
	echo db2_stmt_errormsg($stmt_s);
    $row = db2_fetch_assoc($stmt_s);
		
	//print_r($r);

	$ret['success'] = true;
	$ret['record'] = $row;
	echo acs_je($ret);
	exit;

}


if ($_REQUEST['fn'] == 'grid_scorte'){

	$m_params = acs_m_params_json_decode();
	
	$cod_art = $m_params->open_request->form_values->f_cod_art;
	$desc_art = $m_params->open_request->form_values->f_desc_art;
	$cod_forn = $m_params->open_request->form_values->f_fornitore;
	$magaz = $m_params->open_request->form_values->f_magazzino;
	
	if (strlen($cod_art) > 0)
		$sql_where.= " AND UPPER(SCCAR0) LIKE '%" . strtoupper($cod_art) . "%' ";
	if (strlen($desc_art) > 0)
		$sql_where.= " AND UPPER(SCDES0) LIKE '%" . strtoupper($desc_art) . "%' ";
	
	if (strlen($cod_forn) > 0)
		$sql_where.= " AND UPPER(SCCFO0) = '" . strtoupper($cod_forn) . "' ";
	
	if (strlen($magaz) > 0)
	    $sql_where.= " AND SCCMA0 = '{$magaz}' ";
		
	/*$start = $_REQUEST['start'];   //pageSize
	$limit = $_REQUEST['limit'];  //leadingBufferZone*/

	$sql = "SELECT RRN(SC) AS RRN, SCDIV0, SCCFO0, SCRAG0, SCCAR0, SCDES0, SCDOM0, SCDEV0, SCGRI0, SCGGT0, SCLSH0, SCSCS0, 
			SCSCM0, SCSCF0, SCRIO0, SCDTM0, SCDTF0, SCCMA0, SCDMA0, SCCAL0, SCSCT0, SCLOT0, SCCOM0, SCCFS0, SCUM00,
			TA_LS.TADESC AS LS_DESC
			FROM {$cfg_mod_DeskUtility['file_scorte']} SC
			LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tabelle_acq']} TA_LS
					ON SCDIT0 = TA_LS.TADT AND SCLSH0 = TA_LS.TAKEY1  AND TA_LS.TATAID = 'LIVSR'
			WHERE SCDIT0 = '$id_ditta_default' AND SCDIV0 = '{$m_params->open_request->form_values->f_divisione}'
			{$sql_where} ORDER BY  SCCFO0, SCRAG0, SCCAR0, SCDES0, SCCMA0";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['SCCFO0'] = trim($row['SCCFO0']);
		$nr['SCRAG0'] = trim($row['SCRAG0']);
		$nr['SCCAR0'] = trim($row['SCCAR0']);
		$nr['SCDES0'] = trim($row['SCDES0']);
		$nr['SCDOM0'] = trim($row['SCDOM0']);
		$nr['SCDEV0'] = trim($row['SCDEV0']);
		$nr['SCGRI0'] = trim($row['SCGRI0']);
		$nr['SCGGT0'] = trim($row['SCGGT0']);
		$nr['SCLSH0'] = trim($row['SCLSH0']);
		if($row['LS_DESC'] != 0)
			$nr['LS_DESC'] = substr(trim($row['LS_DESC']), 0, 4);
		$nr['SCSCS0'] = trim($row['SCSCS0']);
		$nr['SCSCM0'] = trim($row['SCSCM0']);
		$nr['SCSCF0'] = trim($row['SCSCF0']);
		$nr['SCRIO0'] = trim($row['SCRIO0']);
		$nr['SCDIV0'] = trim($row['SCDIV0']);
		$nr['SCDTM0'] = trim($row['SCDTM0']);
		$nr['SCCMA0'] = trim($row['SCCMA0']);
		$nr['SCDMA0'] = trim($row['SCDMA0']);
		$nr['SCCAL0'] = trim($row['SCCAL0']);
		$nr['SCSCT0'] = trim($row['SCSCT0']);
		$nr['SCLOT0'] = trim($row['SCLOT0']);
		$nr['SCCOM0'] = trim($row['SCCOM0']);
		$nr['SCDTF0'] = trim($row['SCDTF0']);
		$nr['SCUM00'] = trim($row['SCUM00']);
		$nr['SCCFS0'] = trim($row['SCCFS0']);
		$nr['rrn'] = acs_u8e(trim($row['RRN']));
	
		if ($nr['SCDTM0'] == 0)
			$nr['SCDTM0_V'] = 'N';
		else
			$nr['SCDTM0_V'] = 'Y';
		$nr = array_map('utf8_encode', $nr); //php7
		$ar[] = $nr;

	}

	echo acs_je(array('root' => $ar, 'total' => 200));
	exit;
}


function options_select_fornitore($mostra_codice = 'N', $div){
	global $conn;
	global $cfg_mod_DeskUtility, $id_ditta_default;
	$ar = array();

	$sql = "SELECT SFCFO0, SFRAG0 FROM {$cfg_mod_DeskUtility['file_fornitori']} AF
	WHERE SFDIT0 = '$id_ditta_default' AND SFDIV0 = '{$div}'";


	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		if ($mostra_codice == 'Y')
			$text = "[" . $row['SFCFO0'] . "] " . $row['SFRAG0'];
			else $text = $row['SFRAG0'];
	 	$ret[] = array( "id" 	=> $row['SFCFO0'],
	 			"text" 	=> $text );
	}
	return $ret;
}



if ($_REQUEST['fn'] == 'open_tab'){

	$m_params = acs_m_params_json_decode();
	
	$divisione = $m_params->form_values->f_divisione;
	$main_module->setup_scorte_save_divisione($divisione);
	?>
	
	{
		success:true,
		items: [
		
		{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            layout: 'fit',
            //autoScroll: true,
          	title: 'Scorte [' + '<?php echo $divisione?>' + ']',
          	<?php echo make_tab_closable(); ?>,
            flex:0.7,
           items: [	
		
		{
			xtype: 'grid',
		
			
			bbar: [{xtype: 'button',
					text: 'Stampa',
		            iconCls: 'icon-print-32',
		            scale: 'large',
		            handler: function() {
	                    form = this.up('form').getForm();
                        this.up('form').submit({
                        url: 'acs_scorte_setup_scorte_report.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',                        
                        params: {
                            form_values: Ext.encode (<?php echo acs_je($m_params) ?>)
					    }
              			});            	                	     
            	                	                
	          	  }
			    },'->', {
				xtype: 'button',
	            text: 'Conferma modifiche ROP',
	            iconCls: 'icon-button_blue_play-32', 
	            scale: 'large',
	            handler: function() {
	            
	          acs_show_win_std('Salvataggio modifiche ROP', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_sincro', {form_values: <?php echo acs_je($m_params) ?>} , 350, 180, {}, 'icon-listino');
	          

	            }
			 }], 
			
			flex:1,
	        loadMask: true,	
	        features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
		}],
			  plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
	
			store: {
						xtype: 'store',
						autoLoad:true,
						/*buffered :true,						
						pageSize: 100,
    					leadingBufferZone: 1000,*/
    					
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_scorte', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
						
		        			fields: ['date_mod', 'SCDIV0', 'SCCFO0', 'SCRAG0', 'SCCAR0', 'SCDES0', 
		        			{name: 'SCDOM0', type: 'float'}, 
		        			{name: 'SCDEV0', type: 'float'},
		        			{name: 'SCGRI0', type: 'float'},
		        			{name: 'SCGGT0', type: 'float'},
		        			{name: 'SCLSH0', type: 'float'},
		        			{name: 'SCSCS0', type: 'float'},
		        			{name: 'SCSCM0', type: 'float'},
		        			{name: 'SCSCF0', type: 'float'},
		        			{name: 'SCRIO0', type: 'float'},
		        			{name: 'SCDTM0', type: 'float'},
		        			{name: 'SCDTM0', type: 'float'},
		        			{name: 'SCCAL0', type: 'float'},
		        			{name: 'SCCOM0', type: 'float'},
		        			{name: 'SCSCT0', type: 'float'},
		        			{name: 'SCLOT0', type: 'float'},
		        			{name: 'SCCFS0', type: 'float'},
		        			 'LS_DESC', 'SCCAL0', 'SCDTM0_V', 'SCDTF0', 'SCCMA0', 'SCDMA0', 'SCUM00', 'rrn']							
									
			}, //store
				

			      columns: [	
			     	 {
	                header   : 'Fornitore',
	                dataIndex: 'SCRAG0',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                },{
	                header   : 'Articolo',
	                dataIndex: 'SCCAR0',
	                width: 90,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'SCDES0',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Magaz.',
	                dataIndex: 'SCCMA0',
	                 width: 50,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'UM',
	                dataIndex: 'SCUM00',
	                width: 40,
	                filter: {type: 'string'}, filterable: true
	                },{
	                header   : 'Coeff. <br> stag.',
	                dataIndex: 'SCCFS0',
	                width: 45,
	                renderer: floatRenderer3N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Domanda <br> prevista',
	                dataIndex: 'SCDOM0',
	                width: 70,
	                renderer: floatRenderer3N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Consumo <br> medio',
	                dataIndex: 'SCCOM0',
	                width: 70,
	                renderer: floatRenderer3N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Dev. std',
	                dataIndex: 'SCDEV0',
	                width: 70,
	                align: 'right',
	                renderer: floatRenderer2N,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'LT',
	                dataIndex: 'SCGRI0',
	              	width: 40,
	              	renderer: floatRenderer2N,
	              	align: 'right',
	              	filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Gg <br> trasp.',
	                dataIndex: 'SCGGT0',
	                width: 40,
	                renderer: floatRenderer0N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'LS%',
	                dataIndex: 'LS_DESC',
	                width: 40,
	                //renderer: floatRenderer2,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'SS',
	                dataIndex: 'SCSCS0',
	                width: 40,
	                renderer: floatRenderer0N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Tipo <br> calc.',
	                dataIndex: 'SCCAL0',
	                width: 40,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'ROP',
	                dataIndex: 'SCSCM0',
	                width: 40,
	                renderer: floatRenderer0N, 
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
		                header   : 'ROP forzato',
		                dataIndex: 'SCSCF0',
		                width: 80,
		                renderer: function (value, metaData, record, row, col, store, gridView){
						
						            metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';
				                	
				                	return floatRenderer0N(value);
				        },
		                align: 'right',
		                editor: {
				                xtype: 'numberfield',
				                allowBlank: true
				            },
				        filter: {type: 'string'}, filterable: true
	                }, {
		                header   : 'Data forz.', tooltip: 'Data ultimo ROP forzato',
		                dataIndex: 'SCDTF0',
		                width: 70,
		                renderer: date_from_AS,
		                filter: {type: 'string'}, filterable: true	   
	                },{
	                header   : 'Lotto calc.',
	                dataIndex: 'SCRIO0',
	                width: 70,
	                renderer: floatRenderer0N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Scorte trasf.',
	                dataIndex: 'SCSCT0',
	                width: 80,
	                renderer: floatRenderer0N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Lotto trasf.',
	                dataIndex: 'SCLOT0',
	                width: 70,
	                renderer: floatRenderer0N,
	                align: 'right',
	                filter: {type: 'string'}, filterable: true
	                }, 
	                {
	                header   : '<img src=<?php echo img_path("icone/48x48/pencil.png") ?> width=20>',
	                tooltip: 'ROP modificato',  
	                dataIndex: 'SCDTM0_V',
	                width:40,
	                renderer: function(value, p, record){
    			    	if (record.get('SCDTM0_V')== 'Y') return '<img src=<?php echo img_path("icone/48x48/pencil.png") ?> width=18>';
    			    	
    			    	}
	                }
	         ], 
	         
	         listeners: {
	         
	        		edit: function(editor, e, a, b){
                 
                 			list_selected_id = [];
							grid = editor.grid;
							record= e.record;	
							old_value = record.raw.SCSCF0;
							new_value =  record.data.SCSCF0;
							console.log('valore precedente');	
							console.log(old_value);	
							console.log('valore nuovo');
							console.log(new_value);			
							list_selected_id.push(record.data); 
							 
							Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_quant',
							            timeout: 2400000,
							            method: 'POST',
					        			jsonData: {list_selected_id: list_selected_id[0]},						            
							            success: function (result, request) {
							             var jsonData = Ext.decode(result.responseText);
							        	 var new_rec = jsonData.record;
							                 if(old_value != new_value){
							                 record.set('SCDTM0_V', 'Y');
							                 record.set(new_rec);
							                 }
							                 
							        			
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
                  

            }
	
			
				}
				,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('SCDTM0_V') == 'Y')
					           		return ' segnala_riga_verde';
					           return '';																
					         }   
					    }
		
		
		}  // grid
		]}
	 ]
	}
<?php exit; 
}

if ($_REQUEST['fn'] == 'form_sincro'){
	$m_params = acs_m_params_json_decode();
	$fornitore = $m_params->form_values->form_values->f_fornitore;
	$divisione = $m_params->form_values->form_values->f_divisione;
	?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
				items: [
					{
							name: 'f_hold',
							xtype: 'radiogroup',
							//fieldLabel: 'Hold',
							itemId: 's_check',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: 'A'
		                          , width: 50
		                          <?php if(strlen($fornitore)>0){?>
		                          , checked: false
		                          <?php }else{?>
		                          , checked: true
		                          <?php } ?>
		                          , listeners :{
		                           change: function(comp) {
		                          			var s_forn = this.up('form').down('#s_forn');
		                          			
		                          			if( comp.inputValue == 'A')
		                          				s_forn.enable();
		                          		    else
		                          		        s_forn.disable();
		                          		}
		                           
		                          }	
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Fornitore'
		                          , inputValue: 'F'
		                          <?php if(strlen($fornitore)>0){?>
		                          , checked: true
		                          <?php } ?>
		                         , listeners :{
		                           change: function(comp) {
		                          			var s_forn = this.up('form').down('#s_forn');
		                          			
		                          			if( comp.inputValue == 'F')
		                          				s_forn.disable();
		                          		    else
		                          		        s_forn.enable();
		                          		}
		                           
		                          }	
		                   
		                        }]
						}, {
						name: 'f_fornitore',
						xtype: 'combo',
						itemId: 's_forn',
						fieldLabel: 'Fornitore',
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona -',
		                value: '<?php echo $fornitore; ?>',							
					    anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json(options_select_fornitore('Y', $divisione), ''); ?>	
						    ]
						}, listeners :{
		                          afterrender: function(comp) {
		                          			var s_check = this.up('form').down('#s_check');
		                          			console.log(s_check.getValue().f_hold);
											if(s_check.getValue().f_hold == 'A')
		                          				comp.disable();
		                          		    
		                          		}
		                           
		                          }	
					 }
					 ],
					 
			buttons: [					
				{
		            text: 'Conferma',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		             var form = this.up('form').getForm();
		             var form_values = form.getValues();
		             var win = this.up('window');
		             
		             console.log(form_values.f_fornitore);
		           	if (Ext.isEmpty(form_values.f_fornitore)){  
		           	  forn = '';
		           	}else{
		           	  forn = form_values.f_fornitore;
		           	}
		               
		             acs_show_win_std('Conferma modifiche ROP', 'acs_submit_job.php?fn=open_form', { 
           				chiave : {RIRGES:'SCO_SAVE_ROP', RICITI :  <?php echo j($m_params->form_values->form_values->f_divisione) ?>, RIART : forn},
           				vals   : {RIRGES:'SCO_SAVE_ROP', RICITI :  <?php echo j($m_params->form_values->form_values->f_divisione) ?>, RIART : forn}
           				} , 650, 250, {}, 'icon-listino');
		       
		                
		            }
		        }
	        
	        
	        ]  
					
					
	}
	
]}


<?php 
}
