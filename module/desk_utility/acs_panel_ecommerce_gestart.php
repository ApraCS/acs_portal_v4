<?php

require_once("../../config.inc.php");
require_once("./_utility.php");

$main_module = new DeskUtility();



class PerPageEcommerceGestArt extends AbstractDb {

////	protected $table_id 	= "file_tabelle";
	protected $table_k 		= "GRDT,GRNAZ,GRART,GRMATE,GRFINE,GRCDCO";
	protected $file_tab		= "QS36F.XX0S21GR";
	protected $key_with_lng = array();
	
	protected $default_fields = array(
			'GRALTE' 	=> 0,
			'GRLARG'	=> 0,
			'GRSPES'	=> 0);
	
	
	public function get_table($per_select = false){
		return $this->file_tab;
	}

	public function implode_key($glue = "", $pieces = array()){
		$row = $glue;
		return implode("|", array($row['GRDT'], $row['GRNAZ'], $row['GRART'], $row['GRMATE'], $row['GRFINE'], $row['GRCDCO']));
	}
	
	public function explode_key($row){
		$ar_k = explode("|", $row['R_KEY']);
		$row['GRDT'] 	= $ar_k[0];
		$row['GRNAZ'] 	= $ar_k[1];
		$row['GRART'] 	= $ar_k[2];
		$row['GRMATE'] 	= $ar_k[3];
		$row['GRFINE'] 	= $ar_k[4];
		$row['GRCDCO'] 	= $ar_k[5];
		return $row;
	}	
	
	
	protected function get_stmt_rows($p = array()){
		global $conn, $_REQUEST;
		
		//Costanti
		$grdt = '1 ';
		$grnaz = 'IT';
		
		//Variabili da filtri
		$filtri_where = '';
		$filtri_where_lng = '';
		$m_params = acs_m_params_json_decode();

		$form_filtri = json_decode($_REQUEST['form_filtri']);
		
			//stato
			if (isset($form_filtri->f_stato))
				if (is_array($form_filtri->f_stato))
					$filtri_where .= " AND GRFEXP IN (" . sql_t_IN($form_filtri->f_stato) . ") ";
				else
					$filtri_where .= " AND GRFEXP = " . sql_t($form_filtri->f_stato);

			//data ultima modifica
				if (isset($form_filtri->f_data_dal) && $form_filtri->f_data_dal != ""){
					$filtri_where .= " AND GRDTUM >= {$form_filtri->f_data_dal} ";		
					$filtri_where_lng .= " AND GRDTUM >= {$form_filtri->f_data_dal} ";
				}	
				if (isset($form_filtri->f_data_al) && $m_params->f_data_al != ""){
					$filtri_where .= " AND GRDTUM <= {$form_filtri->f_data_al} ";
					$filtri_where_lng .= " AND GRDTUM <= {$form_filtri->f_data_al} ";
				}				
		
				//articolo
				if (isset($form_filtri->f_art) && $form_filtri->f_art != ""){
					$filtri_where .= " AND GRART LIKE '{$form_filtri->f_art}%' ";
					$filtri_where_lng .= " AND GRART LIKE '{$form_filtri->f_art}%' ";
				}
				
		$sql = "SELECT rrn(t) as RRN_ID, t.* FROM {$this->get_table()} t WHERE 1=1 {$filtri_where} AND GRDT=? AND GRNAZ=?";		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt, array($grdt, $grnaz));
		
		//recupero le key che hanno traduzione in lingua
		$sql_lng = "SELECT GRDT || GRART || GRMATE || GRFINE || GRCDCO AS AR_KEY
					 FROM {$this->get_table()} WHERE 1=1 {$filtri_where_lng} AND GRDT=? AND GRNAZ <> ?";		
		$stmt_lng = db2_prepare($conn, $sql_lng);
		$result_lng = db2_execute($stmt_lng, array($grdt, $grnaz));

		while ($row = db2_fetch_assoc($stmt_lng)){
			$this->key_with_lng[$row['AR_KEY']] = +1;
		}


		return $stmt;
	}
	
	public function after_get_row($row){
		//sostituzione vai a capo
		$ar_sost = array(" <br> " => chr(13). chr(10));
		$ar_sost2 = array(", " => chr(13). chr(10));
		$ar_sost3 = array("|" => chr(13). chr(10));

		$row['GRDBRE'] = strtr($row['GRDBRE'], $ar_sost);
		$row['GRDLUN'] = strtr($row['GRDLUN'], $ar_sost);
		$row['GRCHIA'] = strtr($row['GRCHIA'], $ar_sost2);
		//$row['GRALBE'] = strtr($row['GRALBE'], $ar_sost3);
		
		//verifico se ha traduzione in lingua
		if (isset($this->key_with_lng[implode('', array($row['GRDT'], $row['GRART'], $row['GRMATE'], $row['GRFINE'], $row['GRCDCO']))]))
		 	$row['FL_TRAD'] = $this->key_with_lng[implode('', array($row['GRDT'], $row['GRART'], $row['GRMATE'], $row['GRFINE'], $row['GRCDCO']))];
		
		return $row;
	}	
	
	
	public function before_create_jd($ar_field){
		
		//Costanti
		$grdt = '1 ';
		$grnaz = 'IT';		
		
		if (strlen($ar_field->GRDT) == 0)
			$ar_field->GRDT = $grdt;
		if (strlen($ar_field->GRNAZ) == 0)
			$ar_field->GRNAZ = $grnaz;		
		
		//data/ora di creazione
		$ar_field->GRFEXP = ''; //in immissione		
		$ar_field->GRDTGE = oggi_AS_date();
        
        //converto i valori in numerici
        $ar_field->GRALTE =  strtr($ar_field->GRALTE, array("," => "."));
        $ar_field->GRLARG =  strtr($ar_field->GRLARG, array("," => "."));
        $ar_field->GRSPES =  strtr($ar_field->GRSPES, array("," => "."));
	
		return $ar_field;
	}	
	
	public function before_update_jd($ar_field){
		$clrf = chr(13). chr(10);
		$ar_sost_inv = array($clrf => " <br> ");
		$ar_sost2_inv = array($clrf => ", ");		
		$ar_sost3_inv = array($clrf => "|");
		
		//sostituzione caratteri clrf del textarea		
		$ar_field->GRDBRE = preg_replace('/\r\n|\r|\n/', " <br> ", 	$ar_field->GRDBRE);
		$ar_field->GRDLUN = preg_replace('/\r\n|\r|\n/', " <br> ", 	$ar_field->GRDLUN);
		$ar_field->GRCHIA = preg_replace('/\r\n|\r|\n/', ", ", 		$ar_field->GRCHIA);
		//$ar_field->GRALBE = preg_replace('/\r\n|\r|\n/', "|", 	$ar_field->GRALBE);
		
		//data/ora di modifica
		$ar_field->GRDTUM = oggi_AS_date();
		
		unset($ar_field->FL_TRAD);
		
		unset($ar_field->RRN_ID);
		
		return $ar_field;
	}	
	
	public function out_Writer_Form_initComponent() {
		global $id_ditta_default;
		$ret =  "
        this.addEvents('create');
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-module-16',
            frame: true,
			autoScroll: true,
            title: 'Articolo',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right',
				allowBlank: false
            },
            items: [
			{ 
				xtype: 'fieldcontainer', defaultType: 'textfield', itemId: 'cod_container',
				fieldDefaults: {labelAlign: 'left', allowBlank: false},				
				layout: {type: 'hbox', pack: 'start', align: 'stretch'},
	            items :[

					{fieldLabel: 'Codice', 	name: 'GRART', labelAlign: 'right', readOnly: true, width: 170},
					{ 
						xtype: 'displayfield',
						editable: false,
						fieldLabel: '',
					    value: " . j("&nbsp;&nbsp;<img src=" . img_path("icone/48x48/search.png") . " width=16>") . ",
						listeners: {
						    afterrender: function(component) {
						      component.getEl().on('click', function() {
					    		this.up('form').getComponent('cod_container').getComponent('search_GRART').show(); 
						      }, component);  
						    }
						  }					    		
					},    		
					{
					    itemId: 'search_GRART',
			            xtype: 'combo',
						fieldLabel: 'Ricerca', labelAlign: 'right', labelWidth: 50,
						forceSelection:true, allowBlank: true,
						minChars: 2, flex: 1,
					    hidden: true,		
			            
			            store: {
			            	pageSize: 1000,
			            	
							proxy: {
					            type: 'ajax',
					            url : '" . $_SERVER['PHP_SELF'] . "?fn=get_data_articolo',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['id', 'text', 'ARDART'],
			            },
			                        
						valueField: 'id', displayField: 'id',
			            typeAhead: false, hideTrigger: true,
			            
				        listeners: {
				            change: function(field,newVal) {
								//var form = this.up('form').getForm();
				            	//form.findField('GRART').setValue(newVal);
				            }, 
					        select: function(field,newVal) {
								var form = this.up('form').getForm();
				            	form.findField('GRART').setValue(newVal[0].get('id'));
					            		
					            //se vuota modifico la descrizione
					            if (form.findField('GRNOME').getValue() == '')
					            	form.findField('GRNOME').setValue(newVal[0].get('ARDART'));

					            this.hide(); 		
				            }
				        },            
			
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun record trovato',
			
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class=\"search-item\">' +
			                        '{id}' +
					                '<H3>{text}</H3>' + 
			                    '</div>';
			                }                
			                
			            },
			            
			            pageSize: 1000
			
			        }				
				
				
				
				
				
			]},					
				
								
			{ 
				xtype: 'fieldcontainer', defaultType: 'textfield',
				fieldDefaults: {labelAlign: 'left', allowBlank: false},				
				layout: {type: 'hbox', pack: 'start', align: 'stretch'},
	            items :[
					{xtype: 'hidden', 			name: 'GRDT'},
					{xtype: 'hidden', 			name: 'GRNAZ'},
				]
			},					
				

			{fieldLabel: 'Descrizione', 	name: 'GRNOME', maxLength: 120, flex: 1},					            		
					            		
			{
				name: 'GRMATE', fieldLabel: 'Materiale',
				flex: 1, xtype: 'combo',				
				displayField: 'text', valueField: 'id',
				forceSelection:true, allowBlank: false, queryMode: 'local', // typeAhead : true, triggerAction: 'filter',
				store: {
					autoLoad: true, autoDestroy: true,	 
				    fields: [{name:'id'}, {name:'text'}],
				    data: [" . acs_ar_to_select_json(find_PUVN_std('MAT', $id_ditta_default, 'A'), '') . "]		 
				},

				listeners2: {
				    buffer: 50,
				    change: function() {
				      var store = this.store;
				      store.clearFilter();
				      store.filter({
				          property: 'text',
				          anyMatch: true,
				          value   : this.getValue()
				      });
				    }
				  }				    		
				    		
			},

				    		
			, { 
						xtype: 'fieldcontainer', defaultType: 'textfield',
						fieldDefaults: {labelAlign: 'right', allowBlank: false},				
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [					    		
				    		
				    		
				    		
				    		
									    		
								{
									name: 'GRFINE', fieldLabel: 'Finitura',
									flex: 1, xtype: 'combo',				
									displayField: 'text', valueField: 'id',
									forceSelection:true, allowBlank: true, queryMode: 'local',
									store: {
										autoLoad: true, autoDestroy: true,	 
									    fields: [{name:'id'}, {name:'text'}],
									    data: [" . acs_ar_to_select_json(find_PUVN_std('FFI', $id_ditta_default, 'A'), '') . "] 
									}, 
									listeners2: {
									    buffer: 50,
									    change: function() {
									      var store = this.store;
									      store.clearFilter();		
									      store.filter({
									          property: 'text',
									          anyMatch: true,
									          value   : this.getValue()
									      });		
									    }
									}							    								 
								},				    		
									    		
					
								{
									name: 'GRCDCO', fieldLabel: 'Colore',								    		
									flex: 1, xtype: 'combo',				
									displayField: 'text', valueField: 'id',
									forceSelection:true, allowBlank: false, queryMode: 'local',
									store: {
										autoLoad: true, autoDestroy: true,	 
									    fields: [{name:'id'}, {name:'text'}],
									    data: [" . acs_ar_to_select_json(find_PUVN_std('COL', $id_ditta_default, 'A'), '') . "] 
									},
									listeners2: {
									    buffer: 50,
									    change: function() {
									      var store = this.store;
									      store.clearFilter();
									      store.filter({
									          property: 'text',
									          anyMatch: true,
									          value   : this.getValue()
									      });
									    }
									}		
								}
					]
			},
									    		
									    		
			
			{fieldLabel: 'Descrizione estesa',name: 'GRDBRE', xtype: 'textareafield'},				
			{fieldLabel: 'Descrizione fashion',name: 'GRDLUN', xtype: 'textareafield'},
			{fieldLabel: 'Chiavi di ricerca', name: 'GRCHIA', xtype: 'textareafield'},
			{ 
						xtype: 'fieldcontainer', defaultType: 'textfield',
						fieldDefaults: {labelAlign: 'right', allowBlank: false},				
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
							{fieldLabel: 'Alt.',  	name: 'GRALTE', 	maxLength: 15, flex: 1, xtype: 'numberfield', decimalPrecision : 5},
							{fieldLabel: 'Largh.', 	name: 'GRLARG', 	maxLength: 15, flex: 1, xtype: 'numberfield', decimalPrecision : 5},
							{fieldLabel: 'Prof.',	name: 'GRSPES', 	maxLength: 15, flex: 1, xtype: 'numberfield', decimalPrecision : 5}				
				]
			}
			
									    		
									    		
/*				    		
			, { 
						xtype: 'fieldcontainer', defaultType: 'textfield',
						fieldDefaults: {labelAlign: 'right', allowBlank: false},				
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
							{fieldLabel: 'Imballo Alt.',  	name: 'GRALTI', 	maxLength: 15, flex: 1},
							{fieldLabel: 'Largh.', 	name: 'GRLARI', 	maxLength: 15, flex: 1},
							{fieldLabel: 'Prof.',	name: 'GRSPEI', 	maxLength: 15, flex: 1},				
				]
			}
*/
									    						    		

				    		
			, { 
						xtype: 'fieldcontainer', defaultType: 'textfield',
						fieldDefaults: {labelAlign: 'right', allowBlank: false},				
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [				    		
				    		
							{
								name: 'GRCOLO', fieldLabel: 'ColoreB2B',								    		
								flex: 1, xtype: 'combo',				
								displayField: 'text', valueField: 'id',
								forceSelection:true, allowBlank: false, queryMode: 'local',
								store: {
									autoLoad: true, autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [" . acs_ar_to_select_json(find_PUVN_std('@EC', $id_ditta_default, 'A', 'TADESC'), '') . "] 
								},
								listeners2: {
								    buffer: 50,
								    change: function() {
								      var store = this.store;
								      store.clearFilter();
								      store.filter({
								          property: 'text',
								          anyMatch: true,
								          value   : this.getValue()
								      });
								    }
								}		
							}
								    		
								    		
							, {
								name: 'GRIMBA', fieldLabel: 'Imballo',								    		
								flex: 1, xtype: 'combo',				
								displayField: 'text', valueField: 'id',
								forceSelection:true, allowBlank: false, queryMode: 'local',
								store: {
									autoLoad: true, autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [" . acs_ar_to_select_json(find_PUVN_std('', $id_ditta_default, 'A', 'TANR', 'PUCI'), '') . "] 
								},
								listeners2: {
								    buffer: 50,
								    change: function() {
								      var store = this.store;
								      store.clearFilter();
								      store.filter({
								          property: 'text',
								          anyMatch: true,
								          value   : this.getValue()
								      });
								    }
								}		
							}								    		
								    		
						]
			}
								    		
	/*			    				
			, {fieldLabel: 'Albero tipologia',name: 'GRALBE', xtype: 'textareafield'},
	*/							    						    		

			, { 
						xtype: 'fieldcontainer', defaultType: 'textfield',
						fieldDefaults: {labelAlign: 'right', allowBlank: false},				
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [				    		
				    		
							{
								name: 'GRALB1', fieldLabel: 'Alberatura(1)',								    		
								flex: 1, xtype: 'combo',				
								displayField: 'text', valueField: 'id',
								forceSelection:true, allowBlank: false, queryMode: 'local',
								store: {
									autoLoad: true, autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [" . acs_ar_to_select_json(find_PUVN_std('VP1', $id_ditta_default, 'A'), '') . "] 
								}		
							}
								    		
								    		
							, {
								name: 'GRALB2', fieldLabel: 'Alberatura(2)',								    		
								flex: 1, xtype: 'combo',				
								displayField: 'text', valueField: 'id',
								forceSelection:true, allowBlank: false, queryMode: 'local',
								store: {
									autoLoad: true, autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [" . acs_ar_to_select_json(find_PUVN_std('VP2', $id_ditta_default, 'A'), '') . "] 
								}		
							}								    		
								    		
						]
			}								    		
								    		
								    		
								    		
								    		
			],
            dockedItems: [" . self::form_buttons(self::form_buttons_pre()) . "]
        });
        this.callParent();
";
		return $ret;
	}


	
	
	public function form_buttons_pre() {
		global $s;
		return "
			{
				text: 'Forza reinvio',
				scope: this,
				handler: function(){
				
						if (!this.activeRecord){
							alert('Selezionare un record');
							return false;
						}		

				
					rrn_id = this.activeRecord.get('RRN_ID');

				    Ext.Msg.confirm('Attenzione!', 'Confermi la cancellazione del flag di inviato sul record corrente?', function(btn, text){
				      if (btn == 'yes'){				
				
							Ext.Ajax.request({
						        url        : '" . $_SERVER['PHP_SELF'] . "?fn=forza_reinvio',
						        jsonData: {rrn_id: rrn_id},
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            acs_show_msg_info('Richiesta completata. Eseguire il refresh dei dati della tabella');			            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
						        		
						} else {
				        //nothing
				      }
				    });						        		
				
				}
			}, 
		";
	}


	public function out_Writer_Grid_initComponent_columns() {
		$ret = "
			{ text: 'Exp',			width: 30, 	dataIndex: 'GRFEXP', renderer: function(value, p, record){
    			    	if (value.trim() == '') 	return '<img src=" . img_path("icone/48x48/pencil.png") . " width=18>';			    	    			    	    			    	
						if (value.trim() == 'I') 	return '<img src=" . img_path("icone/48x48/sub_blue_accept.png") . " width=18>'; //in attesa di invio    			    			
						if (value.trim() == 'T') 	return '<img src=" . img_path("icone/48x48/shopping_bag.png") . " width=18>'; //inviato								
    			    	}},				
			{ text: 'Codice',		width: 60, 	dataIndex: 'GRART', 	filter: {type: 'string'}, filterable: true},
			{ text: 'Articolo',		flex: 80, 	dataIndex: 'GRNOME',	filter: {type: 'string'}, filterable: true},				
			{ text: 'Mat.',			width: 40, 	dataIndex: 'GRMATE', 	filter: {type: 'string'}, filterable: true},
			{ text: 'Finit.',		width: 40, 	dataIndex: 'GRFINE',	filter: {type: 'string'}, filterable: true},
			{ text: 'Col.',			width: 40, 	dataIndex: 'GRCDCO', 	filter: {type: 'string'}, filterable: true},
			{ text: 'Modif.',		width: 60, 	dataIndex: 'GRDTUM', renderer: date_from_AS},				
			{ text: 'Lng',			width: 30, 	dataIndex: 'FL_TRAD',	filter: {type: 'boolean'}, filterable: true, 
					renderer: function(value, p, record){
    			    	if (parseInt(value) > 0) 	return '<img src=" . img_path("icone/48x48/globe.png") . " width=18>';			    	    			    	    			    	
 			 }},
	";
		return $ret;
	}




	public function out_Writer_Model($class_name) {
		echo "
		Ext.define('Writer.Model.{$class_name}', {
		extend: 'Ext.data.Model',
		idProperty: 'id',
		fields: [
				{name: 'id', type: 'int', useNull: true},
				'RRN_ID',
				'k_GRDT', 'k_GRNAZ', 'k_GRART', 'k_GRMATE', 'k_GRFINE', 'k_GRCDCO',
				'GRDT', 'GRNAZ', 'GRART', 'GRMATE', 'GRFINE', 'GRCDCO', 'GRCOLO', 
				'GRNOME', 'GRCHIA', 'GRDBRE', 'GRDLUN', 'GRDTUM', 'GRFEXP',
				'GRALTE', 'GRLARG', 'GRSPES',  
				'GRALBE', 'FL_TRAD',
				'GRIMBA', 'GRALB1', 'GRALB2'
		]
		});
	";

	}

} //class


// ******************************************************************************************
// CANCELLAZIONE FLAG DI INVIATO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'forza_reinvio'){
	global $cfg_mod_Spedizioni;
	$m_params = acs_m_params_json_decode();
	
	$obj = new PerPageEcommerceGestArt();
	$sql = "UPDATE " . $obj->get_table() . " t SET GRFEXP = '' WHERE RRN(t) = " . $m_params->rrn_id;
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);	
	
	$ret = array();
	$ret['success'] = $result;
	echo acs_je($ret);	
	exit;
}



// ******************************************************************************************
// FORM per TRADUZIONE (fl_trad)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_fl_trad'){
	//conversione di alcuni parametri nel formato richiesto
	$m_params = acs_m_params_json_decode();
	
	$rec = $m_params->rec;
	
	//passo in lingua
	$rec->k_GRNAZ 	= $m_params->lng;
	$rec->GRNAZ 	= $m_params->lng;
	
	$obj = new PerPageEcommerceGestArt();	
	$obj->load_rec_data_by_k($rec);
	
	$to_fn = 'upd_lng_rec';
	
	if (strlen($obj->rec_data['GRDT']) == 0){ //non trovato, preparo la chiave
		$to_fn = 'crt_lng_rec';
		$obj->rec_data['k_GRDT'] = $rec->k_GRDT;
		$obj->rec_data['k_GRART'] = $rec->k_GRART;
		$obj->rec_data['k_GRCDCO'] = $rec->k_GRCDCO;
		$obj->rec_data['k_GRFINE'] = $rec->k_GRFINE;
		$obj->rec_data['k_GRMATE'] = $rec->k_GRMATE;
		$obj->rec_data['k_GRNAZ'] = $rec->k_GRNAZ;

		$obj->rec_data['GRDT'] = $rec->k_GRDT;
		$obj->rec_data['GRART'] = $rec->k_GRART;
		$obj->rec_data['GRCDCO'] = $rec->k_GRCDCO;
		$obj->rec_data['GRFINE'] = $rec->k_GRFINE;
		$obj->rec_data['GRMATE'] = $rec->k_GRMATE;
		$obj->rec_data['GRNAZ'] = $rec->k_GRNAZ;		
	} else {
		
		//valori per chiave
		$ar_k_chiave = explode(',', $obj->get_table_k());
		foreach ($ar_k_chiave as $kc){
			$obj->rec_data[to_field_k($kc)] = $obj->rec_data[$kc];
		}

		
		//trovato: sostituzione caratteri
		$ar_sost = array(" <br> " => chr(13). chr(10));
		$ar_sost2 = array(", " => chr(13). chr(10));
		$ar_sost3 = array("|" => chr(13). chr(10));
		
		$obj->rec_data['GRDBRE'] = trim(strtr($obj->rec_data['GRDBRE'], $ar_sost));
		$obj->rec_data['GRDLUN'] = trim(strtr($obj->rec_data['GRDLUN'], $ar_sost));
		$obj->rec_data['GRCHIA'] = trim(strtr($obj->rec_data['GRCHIA'], $ar_sost2));
	}			



	
	
	?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right',
				allowBlank: false
            },            
            
            items: [
            	{xtype: 'hiddenfield', name: 'k_GRDT'},
            	{xtype: 'hiddenfield', name: 'k_GRART'},
            	{xtype: 'hiddenfield', name: 'k_GRCDCO'},
            	{xtype: 'hiddenfield', name: 'k_GRFINE'},
            	{xtype: 'hiddenfield', name: 'k_GRMATE'},
            	{xtype: 'hiddenfield', name: 'k_GRNAZ'},
            	
            	{xtype: 'hiddenfield', name: 'GRDT'},
            	{xtype: 'hiddenfield', name: 'GRART'},
            	{xtype: 'hiddenfield', name: 'GRCDCO'},
            	{xtype: 'hiddenfield', name: 'GRFINE'},
            	{xtype: 'hiddenfield', name: 'GRMATE'},
            	{xtype: 'hiddenfield', name: 'GRNAZ'},            	
            	
				{fieldLabel: 'Descrizione', 	name: 'GRNOME', maxLength: 120, flex: 1},
				{fieldLabel: 'Descrizione estesa',name: 'GRDBRE', xtype: 'textareafield'},
				{fieldLabel: 'Descrizione fashion',name: 'GRDLUN', xtype: 'textareafield'},
				{fieldLabel: 'Chiavi di ricerca', name: 'GRCHIA', xtype: 'textareafield'}								            
            ],
            
			buttons: [
			
			
			{
	            text: 'Riproponi',
	            handler: function() {
	            	console.log('riproponi');
					var form = this.up('form').getForm();
					form.findField('GRNOME').setValue(<?php echo acs_je($m_params->riproponiFormValues->GRNOME); ?>);	            	
					form.findField('GRDBRE').setValue(<?php echo acs_je($m_params->riproponiFormValues->GRDBRE); ?>);
					form.findField('GRDLUN').setValue(<?php echo acs_je($m_params->riproponiFormValues->GRDLUN); ?>);
					form.findField('GRCHIA').setValue(<?php echo acs_je($m_params->riproponiFormValues->GRCHIA); ?>);
	            }
	        },				
			
			{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var m_win = this.up('window');

					if(form.isValid()){
						            	
						Ext.Ajax.request({
						        url: '<?php echo $_SERVER['PHP_SELF'] ?>?fn=<?php echo $to_fn; ?>',
						        jsonData: {data: form.getValues()},
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
						            m_win.fireEvent('onFormSubmit', form.getValues());					
				            		m_win.close();
				            		return false;						            						            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });										
					
				    }            	                	                
	            }
	        }],            
            
	     listeners: {
			afterrender: function (comp) {				
				comp.getForm().setValues(<?php echo acs_je($obj->rec_data); ?>);
			}	     
	     }            
            
         }
         ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
	//conversione di alcuni parametri nel formato richiesto
	$m_params = acs_m_params_json_decode();
	$form_filtri = json_decode($m_params->form_filtri);

	if (isset($form_filtri->f_stato) && !is_array($form_filtri->f_stato) )
		$form_filtri->f_stato = array($form_filtri->f_stato);
	if (isset($form_filtri->f_data_dal))
		$form_filtri->f_data_dal = print_date($form_filtri->f_data_dal, "%d/%m/%Y");	
	if (isset($form_filtri->f_data_al))
		$form_filtri->f_data_al = print_date($form_filtri->f_data_al, "%d/%m/%Y");	


	?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            items: [{
		                    xtype: 'checkboxgroup',
		                    layout: 'hbox',
		                    fieldLabel: 'Stato',
		                    flex: 3,
		                    defaults: {
		                     padding: '0 10 10 0'
		                    },
							items: [{
			                            xtype: 'checkbox'
			                          , name: 'f_stato' 
			                          , boxLabel: 'In modifica'
			                          , inputValue: ''		                          
			                        }, {
			                            xtype: 'checkbox'
			                          , name: 'f_stato' 
			                          , boxLabel: 'Da inviare'
			                          , inputValue: 'I'		                          
			                        }, {
			                            xtype: 'checkbox'
			                          , name: 'f_stato' 
			                          , boxLabel: 'Inviato'
			                          , inputValue: 'T'		                          
			                        }]		                    
		           }, 
		           
					{xtype: 'textfield', name: 'f_art', fieldLabel: 'Articolo'},		           
					{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Ultima modifica',
						items: [		           
		           
				            {
								   xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'dal'
								   , labelWidth: 30, width: 150
								   , name: 'f_data_dal'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '-15'
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
										}
							}, {
								   xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'al'
								   , labelWidth: 30, width: 150, labelAlign: 'right'
								   , name: 'f_data_al'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '-15'
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
										}
							}
							
						]}
						
					
					
					],
		           
		    
		    
			buttons: [{
	            text: 'Applica',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	m_win = this.up('window');

					if(form.isValid()){	            	
							m_win.fireEvent('onFormSubmit', form.getValues());					
				            this.up('window').close();
				            return false;
				    }            	                	                
	            }
	        }]
	        
	     , listeners: {
			afterrender: function (comp) {				
				comp.getForm().setValues(<?php echo acs_je($form_filtri); ?>);
			}	     
	     }		    
	}
]}
<?php exit; } ?>
<?php

// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'view_jd' || $_REQUEST['fn'] == 'update_jd' || $_REQUEST['fn'] == 'create_jd' || $_REQUEST['fn'] == 'destroy_jd'){	
	$obj = new PerPageEcommerceGestArt();
	$fn = $_REQUEST['fn'];
	$obj->$fn($_REQUEST);
	exit;
} //get_json_data


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_data_articolo'){
	global $id_ditta_default;
	$ar_art = find_ART_like($_REQUEST['query'],  $id_ditta_default, 'B');
	echo acs_je(array("root" => $ar_art));
	exit;
} //get_json_data


// ******************************************************************************************
// UPD STATO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'upd_stato'){
	global $id_ditta_default;
	$obj = new PerPageEcommerceGestArt();
	$fn = 'update_jd';
	$obj->$fn($_REQUEST);	
	exit;
} //get_json_data

// ******************************************************************************************
// UPD LNG
// ******************************************************************************************
if ($_REQUEST['fn'] == 'upd_lng_rec'){
	global $id_ditta_default;
	$obj = new PerPageEcommerceGestArt();
	$fn = 'update_jd';
	$obj->$fn($_REQUEST);
	exit;
} //get_json_data
// ******************************************************************************************
// CRT LNG
// ******************************************************************************************
if ($_REQUEST['fn'] == 'crt_lng_rec'){
	global $id_ditta_default;
	$obj = new PerPageEcommerceGestArt();
	$fn = 'create_jd';
	$obj->$fn($_REQUEST);
	exit;
} //get_json_data
// ******************************************************************************************
// EXE INVIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_invia'){
	global $id_ditta_default;
	
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$call_return = $tkObj->PgmCall('ZERO_INVIO', $libreria_predefinita_EXE, null, null, null);
	exit;
} //get_json_data



?>
{"success":true, "items": [
	{
		id: 'panel-ecommerce-gestart',
		title: 'Articoli',
		xtype: 'panel',
        loadMask: true,
        closable: true,		
        layout: 'fit',
		
		listeners: {
		
	 			afterrender: function (comp) { 			
								<?php $cl = new PerPageEcommerceGestArt(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "perpageecommercegestart") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "perpageecommercegestart") ?>
								<?php echo $cl->out_Writer_Model("perpageecommercegestart") ?>
								<?php echo $cl->out_Writer_Store("perpageecommercegestart", "form_filtri: '{\"f_stato\": [\"\", \"I\"]}' ", 'Y', 'N') ?>
								<?php echo $cl->out_Writer_sotto_main("perpageecommercegestart", 1.0) ?>
								<?php echo $cl->out_Writer_main("Ecommerce - Anagrafica articoli", "perpageecommercegestart", "
										//disabilito 'Salva' se il flag di export non e' blank
										if (selected.length > 0 && selected[0].get('GRFEXP') != '')
											sotto_main.down('#save').disable();

										//rimuovo i filtri sui combo
										sotto_main.down('form').getForm().findField('GRFINE').store.clearFilter();
										sotto_main.down('form').getForm().findField('GRCDCO').store.clearFilter();
										sotto_main.down('form').getForm().findField('GRMATE').store.clearFilter();
										sotto_main.down('form').getForm().findField('GRCOLO').store.clearFilter();
										
										", 1.2, 'true', "new Ext.Toolbar({
									            items:['Ecommerce - Anagrafica articoli', '->',
										
												, {xtype: 'button', text: 'Invia', iconCls: 'icon-address_blue-16', handler: function(event, toolEl, panel){
										
													Ext.getBody().mask('Loading... ', 'loading').show();
										
													my_grid = event.up('grid');
										
													//eseguo invio
													Ext.Ajax.request({
													        url        : '" . $_SERVER['PHP_SELF'] . "?fn=exe_invia',
													        jsonData: {},
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
																Ext.getBody().unmask();																										
																my_grid.store.load();				            
													        },
													        failure    : function(result, request){
																console.log('failure');
																Ext.getBody().unmask();										
													            Ext.Msg.alert('Message', 'No data to be loaded');
																my_grid.store.load();
													        }
													    });													
										
										
													return false;										
													}, scope: this}
																				
												, {xtype: 'button', text: 'Filtri', iconCls: 'icon-filter-16', handler: function(event, toolEl, panel){
										
								                   	my_listeners = {
							        					onFormSubmit: function(form_values){	
							        						//ToDO: quando confermo i parametri devo modificare extraParams del mio proxy
															comp.down('grid').getStore().proxy.extraParams.form_filtri = Ext.encode(form_values);
															comp.down('grid').getStore().load();
									    				}
													}								
										
													acs_show_win_std('Imposta filtri', '" .  $_SERVER['PHP_SELF'] . "?fn=open_form_filtri', this.up('panel').down('grid').getStore().proxy.extraParams, 500, 220, my_listeners, 'icon-filter-16');
													return false;										
													}, scope: this}

																						
										
									           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){
													this.up('panel').down('grid').getStore().load();										
													}, scope: this}									       		
									         ]            
											})", 
										
											//grid celldblclick
											"
			  								col_name = iView.getGridColumns()[iColIdx].dataIndex;	
			  								rec = iView.getRecord(iRowEl);
										
											if (col_name == 'FL_TRAD'){
										
													//esco se non e' in modifica
													if (rec.get('GRFEXP') != '') return false
										
													my_listeners = {
							        					onFormSubmit: function(form_values){	
							        						//ToDO: quando confermo i parametri devo modificare extraParams del mio proxy
															iView.riproponiFormValues = form_values;
															console.log(iView);
															rec.set('FL_TRAD', 1);
									    				}
													}											
												
												if (Ext.isEmpty(iView.riproponiFormValues) == true)
													riproponiFormValues = {};
												else
													riproponiFormValues = iView.riproponiFormValues;
										
												acs_show_win_std('Inglese', '" .  $_SERVER['PHP_SELF'] . "?fn=open_fl_trad', {lng: 'EN', rec: rec.data, riproponiFormValues: riproponiFormValues}, 500, 420, my_listeners, 'icon-filter-16');										
												return false;
											}
										
											if (col_name == 'GRFEXP' && rec.get('GRFEXP') != 'T'){

												//se e' confermato -> in modifica e vice versa
												if (rec.get('GRFEXP') == 'I')
													rec.set('GRFEXP', '')
												else if (rec.get('GRFEXP') == '')
													rec.set('GRFEXP', 'I');										
										
										
												Ext.Ajax.request({
												        url: '" . $_SERVER['PHP_SELF'] . "?fn=upd_stato',
												        jsonData: {data: rec.data, old_stato: rec.get('GRFEXP')},
												        method     : 'POST',
												        waitMsg    : 'Data loading',
												        success : function(result, request){
												            var jsonData = Ext.decode(result.responseText);
												        },
												        failure    : function(result, request){
												            Ext.Msg.alert('Message', 'No data to be loaded');
												        }
												    });										
										

												
											}
										
											") ?>								

								comp.add(main);
								comp.doLayout();
	 				}
	 	} 	
	}
]
}