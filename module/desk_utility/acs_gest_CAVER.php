<?php 

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $desk_art->get_cfg_mod();

$row_ca = $desk_art->get_TA_std('CATIN', $m_params->TAKEY1);
$t_dettagli = trim($row_ca['TADESC']);

$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "CAVER - Catalogo Versione [{$m_params->TAKEY1}]",
    'descrizione' => "Catalogo versione",
    'form_title' => "Dettagli tabella catalogo versione [{$m_params->TAKEY1}] {$t_dettagli}",
    'fields_preset' => array(
        'TATAID' => 'CAVER',
        'TAKEY1' => $m_params->open_request->TAKEY1
    ),
  
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TAKEY2', 'TADESC'),
    'fields_grid' => array('TAKEY2', 'TADESC', 'TAMAIL', 'TAINDI', 'ATTAV', 'immissione'),
    'fields_form' => array('TAKEY2', 'TADESC', 'TAMAIL', 'TAINDI', 'TARIF2'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Catalogo intestazione'),
        'TAKEY2' => array('label'	=> 'Codice',  'maxLength' => 10, 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        'TAINDI' => array('label'	=> 'Riferimento', 'maxLength' => 60),
        'TARIF2' => array('label'	=> 'Data', 'type' => 'date'),
        
        'ATTAV' => array( 'label' => 'attav', 'type' => 'TA', 'fw'=>'width: 40',
         'iconCls' => "arrivi",
         'tooltip'=>'ToDo',
         'select' => "ATTAV.C_ROW AS ATTAV",
         'join' => "LEFT OUTER JOIN (
             SELECT COUNT(*) AS C_ROW, ASDOCU
             FROM {$cfg_mod['file_assegna_ord']}
             WHERE ASDT = '{$id_ditta_default}' AND ASFLRI <> 'Y'
             GROUP BY ASDOCU) ATTAV
             ON TA.TAKEY1 = ATTAV.ASDOCU",  
         'ta_config' => array(
         'params' => array('raggruppamento' => 'ART', 'tab_std' => 'Y'),
         'file_acs'     => 'acs_panel_todolist.php?fn=open_panel',
             
         )
            
         ),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
        
        
    ),
    'tasto_dx' => array(
       array(
            'text'  => 'Inserimento nuovo stato/attivit&agrave;',
            'iconCls' => 'icon-arrivi-16',
            'tooltip' => '',
            'scale' => 'small',
            'handler' =>  new ApiProcRaw("function(){
                            acs_show_win_std('Nuovo stato/attivit&agrave;', '../desk_utility/acs_anag_art_create_attivita.php',
                    				{	tipo_op: 'MACMA',
                    					rif : 'MACMA',
                    	  				list_selected_id : list_selected_id
                    	  			}, 500, 500, {
                                        afterInsertRecord: function(from_win){
                                            m_grid.getStore().load();
                    						from_win.close();
                    		        	}}
                                    , 'icon-arrivi-16');
               }")
        )
        
    ),
    
    'buttons' => array(
     array(
     'text'  => '',
     'iconCls' => 'icon-gear-16',
     'tooltip' => 'Attav ToDo',
     'scale' => 'small',
     'style' => 'border: 1px solid gray;',
     'handler' =>  new ApiProcRaw("function(){
     acs_show_panel_std('../desk_utility/acs_gest_ATTAV.php?fn=open_tab',
     'panel_gestione_attav', {sezione: 'ART'});
     }")
     )
     )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
