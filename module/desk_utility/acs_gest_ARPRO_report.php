<?php

require_once "../../config.inc.php";

$main_module =  new DeskArt();
$cfg_mod = $main_module->get_cfg_mod();

// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   .grassetto{font-weight: bold;}
   .normal{font-weight: normal;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff; font-size: 13px;}   
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 


<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);
$sql_where = "";
$sql_select = "";
$sql_join = "";
$ar = array();


$sql = "SELECT TA.TAKEY1 AS C_PRO, TA.TADESC AS D_PRO, TA.TAASPE AS T_OPZ, TA_OPZ.TAKEY1 AS C_OPZ, TA_OPZ.TADESC AS D_OPZ
        FROM {$cfg_mod['file_tabelle']} TA
        LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_OPZ
         ON TA.TADT = TA_OPZ.TADT AND TA.TAASPE = TA_OPZ.TATAID AND SUBSTRING(TA_OPZ.TATAID, 1, 2) = 'P#'
        WHERE TA.TADT = '{$id_ditta_default}' AND TA.TATAID = 'ARPRO'";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$ar = array();
while ($row = db2_fetch_assoc($stmt)) {

    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    $cod_liv0 = trim($row['C_PRO']);
    $cod_liv1 = implode("_", array(trim($row['T_OPZ']), trim($row['C_OPZ'])));
 
   //PROPRIETA
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['p_task'] = trim($row['D_PRO']);
        $ar_new['p_codice'] = trim($row['C_PRO']);
        $ar_new['tab_opz'] = trim($row['T_OPZ']);
        $ar_new['o_codice'] = trim($row['C_OPZ']);
        $ar_new['o_task'] =  trim($row['D_OPZ']);
        
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    
    //OPZIONI
    $liv=$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        $ar_new['id'] = implode("|", $tmp_ar_id);
        
        $ar_new['p_task'] = trim($row['D_PRO']);
        $ar_new['p_codice'] = trim($row['C_PRO']);
        $ar_new['tab_opz'] = trim($row['T_OPZ']);
        $ar_new['o_codice'] = trim($row['C_OPZ']);
        $ar_new['o_task'] =  trim($row['D_OPZ']);
        
        $ar_r["{$liv}"] = $ar_new;
        
    }
    
    $ar_r=&$ar_r[$liv];

    
}//while

echo "<div id='my_content'>"; 

echo "<div class=header_page>";
echo "<H2>Riepilogo proprietÓ/opzioni</H2>";
echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
        
echo "<table class=int1>";
       echo "<tr class='liv_data'>
             <th>Codice</th>
             <th>Descrizione proprietÓ</th>
             <th>Tabella opzioni</th>
             <th>Codice</th>
             <th>Descrizione opzione</th>";
      echo  "</tr>";
   
 //PROPRIETA'
foreach($ar as $k => $v){

    echo "<tr class = liv1>
          <td><b>{$v['p_codice']}</b></td>
          <td><b>{$v['p_task']}</b></td>
          <td>{$v['tab_opz']}</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>";
    echo "</tr>";
    
    //OPZIONI
    foreach($v['children'] as $k1 => $v1){
       if($v1['o_codice'] != ''){
         echo "<tr>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>{$v1['tab_opz']}</td>
               <td>{$v1['o_codice']}</td>
               <td>{$v1['o_task']}</td>";
        }
    echo "</tr>";
            
                        
   
    
      }
  
}
echo "</div>";

 		
?>

 </body>
</html>


<?php
exit;
}



