<?php
require_once "../../config.inc.php";

ini_set('max_execution_time', 6000);
ini_set('memory_limit', '2048M');

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   tr.liv_p td{font-weight: bold; font-size: 0.9em;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

if(isset($form_values->f_todo) && strlen($form_values->f_todo) > 0)
    $where .= " AND ASCAAS = '{$form_values->f_todo}'";
    
if(isset($form_values->f_utente_assegnato) && strlen($form_values->f_utente_assegnato) > 0)
    $where .= " AND ASUSAT = '{$form_values->f_utente_assegnato}'";
        
    if($_REQUEST['progetto'] != 'Y'){
        
        global $backend_ERP;
        if ($backend_ERP != 'GL'){
            $select = ", AR.ARART AS C_ART, AR.ARDART AS D_ART, AR.ARUMCO AS UM_CO, AR.ARUMTE AS UM_TE,
                        AR.ARUMAL AS UM_AL,   NT_CD.NTKEY2 AS CODICE, NT_CD.NTMEMO AS DESC";
            $join = "INNER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
                    ON ATT_OPEN.ASDT = AR.ARDT AND ATT_OPEN.ASDOCU = AR.ARART
                    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_CD
	                ON NT_CD.NTTPNO = 'PRJCD' AND INTEGER(NT_CD.NTKEY1) = ATT_OPEN.ASIDPR AND NT_CD.NTSEQU=0 ";
        }else{
            
            $join = "INNER JOIN {$cfg_mod_DeskArt['file_anag_art']} MA
                     ON ATT_OPEN.ASDOCU = MA.MACAR0";
            $select = ", MA.MACAR0 AS C_ART, MA.MADES0 AS D_ART, MA.MAUM00 AS UM_CO, MA.MAUM10 AS UM_TE,
                       MA.MAUM20 AS UM_AL";
        }
        $where .= " AND TA_ATTAV.TARIF1 = 'ART' {$as_where} AND ASPROG = ''";
        
    if(count($form_values->f_todo_ev) > 0){
            $show_rilav = 'Y';
            $where .= " AND ASFLRI = 'Y'";
            $where .= sql_where_by_combo_value('ASCARI', $form_values->f_todo_ev);
        }elseif(strlen($m_params->c_art) > 0){
            $where .= " AND ASDOCU = '{$m_params->c_art}'";
        }else{
            $show_rilav = 'N';
            $where .= " AND ASFLRI <> 'Y'";
        }
    }else{
        $show_rilav = 'Y';
        if(isset($form_values->f_todo_ev) && count($form_values->f_todo_ev) > 0)
            $where .= sql_where_by_combo_value('ASCARI', $form_values->f_todo_ev);
        
        if(isset($_REQUEST['n_prog']) && strlen($_REQUEST['n_prog']) > 0)
            $where .= " AND ASPROG = '{$_REQUEST['n_prog']}'";
        
        $select = " , NT_CD.NTKEY2 AS CODICE, NT_CD.NTMEMO AS DESC";
        $join = "    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_CD
	          ON NT_CD.NTTPNO = 'PRJCD' AND INTEGER(NT_CD.NTKEY1) = ATT_OPEN.ASIDPR AND NT_CD.NTSEQU=0 ";
        $where .= " AND ASPROG <> '' {$where}";    
        $order = " ORDER BY ASIDPR";
    }

    
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.TADESC AS D_AT, TA_PJ.TADESC AS D_PJ, NT_MEMO.NTMEMO AS MEMO,
            TA_PJ.TADTGE AS DATA_PJ, TA_PJ.TAUSGE AS US_PJ, TA_PJ.TAORGE AS ORA_PJ 
            {$select}
            FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT_OPEN
            INNER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
              ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_PJ
              ON ATT_OPEN.ASDT = TA_PJ.TADT AND ATT_OPEN.ASPROG = TA_PJ.TAKEY1
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
              ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
          
            {$join}
            WHERE ASDT = '{$id_ditta_default}' {$where}
            {$order}";
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
$ar = array();
while ($row = db2_fetch_assoc($stmt)) {

    
    if($_REQUEST['progetto'] != 'Y'){
        //stacco dei livelli
        $cod_liv0 = trim($row['ASCAAS']);
        $cod_liv1 = trim($row['ASDOCU']);
        
        
    }else{
        
        $cod_liv0 = trim($row['ASPROG']);
        
        if ($row['ASIDAC'] == 0){
            $cod_liv1 = trim($row['ASIDPR']);
            $cod_liv2 = null;
        }
        else {
            $cod_liv1 = trim($row['ASIDAC']);
            $cod_liv2 = $row['ASIDPR'];
        }
        
    }
    
    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    //PROGETTO
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = $row;
        $ar_new['children'] = array();
        $ar_new['liv'] = 'liv_0';
        $ar_new['id'] = implode("|", $tmp_ar_id);
        if($_REQUEST['progetto'] != 'Y'){
            $ar_new['task'] =  utf8_encode($row['D_AT']);
        }else{
            $ar_new['task'] = trim($row['ASPROG']);
            $ar_new['desc'] = $row['D_PJ'];
            $ar_new['imm'] =  "[".print_date($row['DATA_PJ'])."-".print_ora($row['ORA_PJ'])."]";
            $ar_new['utente'] = "[".trim($row['US_PJ'])."]";
        } 
        
        
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    
    //ATTIVITA'
    $liv=$cod_liv1;
    $ar_r = &$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new = $row;
        $ar_new['children'] = array();
        
        
        if($_REQUEST['progetto'] != 'Y'){
            $ar_new['id'] = implode("|", $tmp_ar_id);
            if ($backend_ERP == 'GL'){
                if($row['ASPRGA'] > 0)
                    $ar_new['task'] =  trim($row['ASDOCU']).".".$row['ASPRGA'];
                else
                    $ar_new['task'] =  $row['ASDOCU'];
            }else{
                $ar_new['task'] =  $row['ASDOCU'];
            }
            if($show_rilav == 'Y')
                $ar_new['task'] .= "<br><span style='float: right;'>{$row['ASCARI']}</span>" ;
            
            $ar_new['articolo'] =  $row['ASDOCU'];
            $ar_new['desc'] =  $row['D_ART'];
            if($show_rilav == 'Y'){
                $causali_rilascio = $desk_art->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                $ar_new['d_art'] .=  "<br><span style='float: right;'>".$causali_rilascio[0]['text']."</span>";
            } 
            if(trim($row['ARUMCO']) == trim($row['ARUMTE']) && trim($row['ARUMCO']) == trim($row['ARUMAL'])){
                $um = $row['ARUMTE'];
            }elseif((trim($row['ARUMAL']) == trim($row['ARUMTE']) && trim($row['ARUMAL']) != trim($row['ARUMCO'])) ||
                (trim($row['ARUMAL']) == trim($row['ARUMCO']) && trim($row['ARUMAL']) != trim($row['ARUMTE']))){
                    $um = $row['ARUMTE']. " - " . trim($row['ARUMCO']);
            }else{
                $um = trim($row['ARUMTE']) . " - " . trim($row['ARUMCO']) . " - " . trim($row['ARUMAL']);
            }
            $ar_new['u_m'] = $um;
            if($show_rilav != 'Y')
                $ar_new['flag'] =  trim($row['ASFLRI']);
            
            $ar_new['imm'] = print_date(trim($row['ASDTAS']))." - ".print_ora(trim($row['ASHMAS']));
            if($show_rilav == 'Y')
                $ar_new['imm'] .= "<br>" .print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
        }else{
            $ar_new['id'] = implode("|", array($row['ASCAAS'], $row['ASDOCU']));
            $ar_new['task'] =  $row['CODICE'];
            $ar_new['desc'] =  $row['DESC'];
            $ar_new['note'] =  $desk_art->has_nota_progetto($row['ASIDPR']);
            $row_nt =  $desk_art->get_note_progetto($row['ASIDPR']);
            $ar_new['t_note'] =  utf8_decode($row_nt['NTMEMO']);
            if(trim($row['ASFLRI']) == 'Y'){
                $causali_rilascio = $desk_art->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                $ar_new['d_rilav'] = $causali_rilascio[0]['text'];
                $ar_new['t_rilav'] = trim($row['ASNORI']);
            }
            $ar_new['flag'] =  trim($row['ASFLRI']);
            $ar_new['imm'] = print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
        }
        
        $ar_new['prog'] =  $row['ASIDPR'];
        $ar_new['ut_ass'] = trim($row['ASUSAT']);
        $ar_new['ut_ins'] = trim($row['ASUSAS']);
        $ar_new['scadenza'] = trim($row['ASDTSC']);
        $ar_new['memo'] = trim($row['MEMO']);
        $ar_new['riferimento'] = utf8_encode(trim($row['ASNOTE']));
        $ar_new['f_ril'] =  trim($row['ASFLNR']);
        $ar_new['liv'] = 'liv_1';
        $ar_new['leaf'] = true;
        $t_ar_liv1 = &$ar_r[$liv];
        $ar_r["{$liv}"] = $ar_new;
    }
    
    $ar_r = &$ar_r["{$liv}"];
    
    if($_REQUEST['progetto'] == 'Y'){
        if(!is_null($cod_liv2)){
            
            $liv=$cod_liv2;
            $ar_r = &$ar_r['children'];
            $tmp_ar_id[] = $liv;
            if(!isset($ar_r[$liv])){
                $ar_new = $row;
                $ar_new['id'] = implode("|", array($row['ASCAAS'], $row['ASDOCU']));
                $ar_new['task'] =  $row['CODICE'];
                $ar_new['desc'] =  $row['DESC'];
                if(trim($row['ASFLRI']) == 'Y'){
                    $causali_rilascio = $desk_art->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI'])); //recupero tutte le RILAV
                    $ar_new['d_rilav'] = $causali_rilascio[0]['text'];
                    $ar_new['t_rilav'] = trim($row['ASNORI']);
                }
                $ar_new['prog'] =  $row['ASIDPR'];
                $ar_new['note'] =  $desk_art->has_nota_progetto($row['ASIDPR']);
                $row_nt =  $desk_art->get_note_progetto($row['ASIDPR']);
                $ar_new['t_note'] =  utf8_decode($row_nt['NTMEMO']);
                $ar_new['imm'] = print_date(trim($row['ASDTRI']))." - ".print_ora(trim($row['ASHMRI']));
                $ar_new['ut_ass'] = trim($row['ASUSAT']);
                $ar_new['ut_ins'] = trim($row['ASUSAS']);
                $ar_new['scadenza'] = trim($row['ASDTSC']);
                $ar_new['memo'] = trim($row['MEMO']);
                $ar_new['riferimento'] = trim($row['ASNOTE']);
                $ar_new['f_ril'] =  trim($row['ASFLNR']);
                $ar_new['flag'] =  trim($row['ASFLRI']);
                $ar_new['liv'] = 'liv_2';
                $ar_new['leaf'] = true;
                $t_ar_liv1['leaf'] = false;
                $ar_r["{$liv}"] = $ar_new;
            }
            
        }
        
    }
    
   
}



echo "<div id='my_content'>";
echo "<div class=header_page>";
if($_REQUEST['progetto'] == 'Y')
    echo "<H2>Riepilogo attivit� progetti di rilascio codifiche</H2>";
else 
    echo "<H2>Riepilogo ToDo</H2>";


echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";
echo "<tr class='liv_data'>";

if($_REQUEST['progetto'] == 'Y')
    echo "<th>Progetto/attivit�</th>";
else
    echo "<th>Causale/articolo</th>";
     
    echo "<th>Descrizione</th>";
if($_REQUEST['progetto'] == 'Y')
    echo "<th>Rilascio</th>";
else
    echo "<th>UM</th>";
echo "  <th>Data rilascio</th>
    	<th>Utente assegnato</th>
        <th>Scadenza</th>
        <th>Riferimento</th>
        <th>Memo</th>";
echo "</tr>";  

/*echo "<pre>";
print_r($ar);
exit;*/

foreach ($ar as $kar => $r){
    
    echo "<tr class = 'liv_p'>
          <td>".$r['task']."</td>
          <td>".$r['desc']."</td>
          <td>&nbsp;</td>
          <td>".$r['imm']."</td>
          <td>".$r['utente']."</td>";
          echo "<td colspan = 4>&nbsp;</td>";
          echo "</tr>";
	       
    foreach ($r['children'] as $kar1 => $r1){
     
            echo "<tr>
                  <td valign = top>".$r1['task']."</td>
                  <td valign = top><b>".$r1['desc']."</b>";
            if($r1['note'] > 0)
               echo "<br>".$r1['t_note']."</td>";
            else 
               echo "</td>"; 
            
            if($_REQUEST['progetto'] == 'Y')
                echo "<td valign = top>".$r1['d_rilav']."<br>".$r1['t_rilav']."</td>";
            else
                echo "<td valign = top>".$r1['u_m']."</td>";

                echo "<td valign = top>".$r1['imm']."</td>";
                echo "<td valign = top>".$r1['ut_ass']."</td>";
                echo "<td valign = top>".print_date($r1['scadenza'])."</td>";
                echo "<td valign = top>".$r1['riferimento']."</td>";
                echo "<td valign = top>".$r1['memo']."</td>";
            echo "</tr>";
        
            if($_REQUEST['progetto'] == 'Y'){
                foreach ($r1['children'] as $kar2 => $r2){
                
                    echo "<tr class = 'liv1'>
                          <td valign = top> -> ".$r2['task']."</td>
                          <td valign = top><b>".$r2['desc']."</b>";
                    if($r2['note'] > 0)
                        echo "<br>".$r2['t_note']."</td>";
                    else
                        echo "</td>";
                    echo "<td valign = top>".$r2['d_rilav']."</td>
                          <td valign = top>".$r2['imm']."</td>
                          <td valign = top>".$r2['ut_ass']."</td>
                          <td valign = top>".print_date($r2['scadenza'])."</td>
                          <td valign = top>".$r2['riferimento']."</td>
                          <td valign = top>".$r2['memo']."</td>
                          </tr>";
                
                
                }   
                
            }
	                
    }
            
}

function sum_columns_value(&$ar_r, $r){
    
    global $s;
    
    $ar_r['importo'] += $r['TDTIMP'];
    
    $ar_r['fl_art_manc'] = max($ar_r['fl_art_manc'], $s->get_fl_art_manc($r));
    $ar_r['art_da_prog'] = max($ar_r['art_da_prog'], $s->get_art_da_prog($r));
    $ar_r['fl_bloc'] = $s->get_fl_bloc($ar_r['fl_bloc'], $s->calc_fl_bloc($r));
    $ar_r['fl_new'] = $s->get_fl_new($r);
    $ar_r['fl_da_prog'] += $r['TDFN04'];
    
    $ar_r['scadenza'] = max($ar_r['scadenza'] ,$r['ASDTSC']);
    if(trim($r['TDCLOR']) == 'M') $ar_r['n_M']++;
   
}


?>
</div>
</body>
</html>	

