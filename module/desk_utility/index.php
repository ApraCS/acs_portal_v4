<?php 
	require_once("../../config.inc.php");
	$_module_descr = "Desktop Dati di base";
			
	$main_module = new DeskUtility();
	$desk_art = new DeskArt();
	
	$cfg_mod = $desk_art->get_cfg_mod();
	
	$users = new Users;
	$ar_users = $users->find_all();
	
/*	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}
*/	
	
	$ar_email_json = acs_je($ar_email_to);	
		
?>




<html>
<head>
<title>ACS Portal_DOrA</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />

<!-- <link rel="stylesheet" type="text/css" href="resources/css/calendar.css" /> -->
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/extensible-all.css" />
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/calendar.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css?20200110_3"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">
 /* REGOLE RIORDINO */
 .x-grid-cell.lu{background-color: #dadada;} 
 .x-grid-cell.ma{background-color: #e5e5e5;} 
 .x-grid-cell.me{background-color: #eeeeee;} 
 .x-grid-cell.gi{background-color: #FFFFFF;} 
 .x-grid-cell.ve{background-color: #F4EDAF;} 
 .x-grid-cell.sa{background-color: #FFFFFF;}
 
 .x-grid-cell.attivi{background-color: #A0DB8E;}
 .x-grid-cell.elaborazione-old-1{background-color: #e5e5e5;} 
 .x-grid-cell.elaborazione-old-2{background-color: #F9BFC1;} 

.x-panel td.x-grid-cell.festivo{opacity: 0.6;}

.x-panel td.x-grid-cell.con_carico_1{background-color: #909090; color: white; font-weight: bold;} /* SOLO ALCUNI HANNO IL CARICO */
.x-panel td.x-grid-cell.con_carico_2{background-color: #3c3c3c; color: white; font-weight: bold;}  /* HANNO TUTTI IL CARICO */
.x-panel td.x-grid-cell.con_carico_3xxx{background-color: #F9827F; font-weight: bold;}  /* NON CARICO. CON DATE CONF. */
.x-panel td.x-grid-cell.con_carico_4xxx{background-color: #F9BFC1; font-weight: bold;}  /* NON CARICO. CON DATE TASS. */
.x-panel td.x-grid-cell.con_carico_5xxx{background-color: #CEEA82; font-weight: bold;}  /* ALCUNI EVASI */
.x-panel td.x-grid-cell.con_carico_6{background-color: #8CD600; font-weight: bold;}  /* TUTTI EVASI */


.x-panel .x-column-header-text{font-weight: bold;}


tr.liv_totale td.x-grid-cell{background-color: #F4EDAF; border: 1px solid white;}
tr.liv_totale td.x-grid-cell.festivo{opacity: 0.6;}
tr.liv_0 td.x-grid-cell{border: 1px solid white; font-weight: bold;}
.x-panel.s_giallo tr.liv_0 td.x-grid-cell{background-color: #F4EDAF; border: 1px solid white; font-weight: normal;}
tr.liv_1 td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: bold;}
tr.liv_1_no_b td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: normal;}
tr.liv_1.no_carico td.x-grid-cell{background-color: #C4D8E2}

.x-panel.elenco_ordini tr.liv_1 td.x-grid-cell{font-weight: normal;}
.x-panel.elenco_ordini tr.liv_2 td.x-grid-cell{font-weight: bold;}

tr.liv_totale.dett_selezionato td.x-grid-cell{background-color: #F4ED7C; font-weight: bold;}

/*TIPO ORDINE (raggr)*/
.x-grid-cell.tipoOrd.A {background-color: white;}   /* assistenze */
.x-grid-cell.tipoOrd.A.toDoListTp {background-color: dddddd;}   /* assistenze in ToDoList  */
.x-grid-cell.tipoOrd.M {background-color: #93C6E0;} /* mostre */
.x-grid-cell.tipoOrd.O {background-color: #A0DB8E;} /* ordini */
.x-grid-cell.tipoOrd.P {background-color: #E2D67C;} /* composizioni promozionali */
.x-grid-cell.tipoOrd.C {background-color: #D3BFB7;} /* cccc */
.x-grid-cell.tipoOrd.V {background-color: #D3BFB7;} /* materiale promozionale */


/*RIGHE ORDINE*/
.x-grid-row.rigaRevocata .x-grid-cell{background-color: #F9BFC1}
.x-grid-row.rigaChiusa .x-grid-cell{background-color: #CEEA82}
.x-grid-row.rigaParziale .x-grid-cell{background-color: #F4EDAF}
.x-grid-row.rigaNonSelezionata .x-grid-cell{background-color: #cccccc}


/*TIPO PRIORITA*/
 .x-grid-cell.tpSfondoRosa{background-color: #F9BFC1;}
 .x-grid-cell.tpSfondoCelesteEl{background-color: #99D6DD;}


/*ICONE*/
.iconSpedizione {background-image: url(<?php echo img_path("icone/16x16/spedizione.png") ?>) !important;}
 
/* celle classe ABC */
.x-panel.x-table-index .x-panel-body {
		text-align: center;
    	background-color: #D3D3D3;
		font-size: 18;
		/*font-weight: bold;*/
}
 .x-panel.x-table-index.leg .x-panel-body {		
		font-size: 14;
}
.x-panel.x-table-index .x-panel-body span {font-size: 10px;}
 
/* bordi laterali tabella non visualizzabili */
.table-border {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	text-align: right;
}
.table-border-final {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	border-right: 1px solid #99BBE8;
	text-align: right;
}

a.view_dett_class_gc{text-decoration: none;}
 
/* GRADIAZIONE CELLE GIACENZE/CONSUMI */
.x-panel.GPACPA .x-panel-body-default{background-color: #6BC9DB;} 

.x-panel.GPACPB .x-panel-body-default{background-color: #99D6DD;}
.x-panel.GPBCPA .x-panel-body-default{background-color: #99D6DD;}
.x-panel.GPBCPB .x-panel-body-default{background-color: #99D6DD;}

.x-panel.GPCCPA .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPCCPB .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPCCPC .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPBCPC .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPACPC .x-panel-body-default{background-color: #BAE0E0;}

.x-panel.GPZCPA .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPB .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPC .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPD .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPDCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPCCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPBCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPACPZ .x-panel-body-default{background-color: #F4EDAF;}

.x-toolbar .strong{font-weight: bold;}

.x-grid-cell-inner p.sottoriga-liv_2{text-indent: 70px}

.x-form-fieldcontainer.row_d .x-form-display-field {background: #D1CEDD; }

</style>

<script type="text/javascript" src="../../../extjs/ext-all.js"></script>

<script type="text/javascript" src="../../js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
<script type="text/javascript" src="../../js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>


<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src=<?php echo acs_url("js/gmaps.js") ?>></script>
<script src=<?php echo acs_url("js/acs_js.js?d=20200210") ?>></script>



<script type="text/javascript">



	var refresh_stato_aggiornamento = {
        run: doRefreshStatoAggiornamento,
        interval: 60000  // 30 * 1000 millisecondi
      }

	function doRefreshStatoAggiornamento() {
		 Ext.Ajax.request({
            url : 'get_stato_aggiornamento.php' ,
            method: 'GET',
            success: function ( result, request) {
               // var jsonData = Ext.decode(result.responseText);
               
               // var btStatoAggiornamento = document.getElementById('bt-stato-aggiornamento-esito');
              //  btStatoAggiornamento.innerHTML = jsonData.html;
            },
            failure: function ( result, request) {
                console.log('failed');
            }
        });
	}


	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
            //'Ext.calendar': 'src',
	        "Extensible": "../../js/extensible-1.5.2/src", 
			"Extensible.example": "../../js/extensible-1.5.2/examples"	                   
        }	    
	});Ext.Loader.setPath('Ext.ux', '../../ux');


    Ext.require(['*'
                 , 'Ext.ux.grid.FiltersFeature',                 
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
                 , 'Extensible.calendar.data.MemoryEventStore'
                 , 'Extensible.calendar.CalendarPanel'  
                 , 'Extensible.example.calendar.data.Events'
				 , 'Extensible.calendar.data.CalendarMappings'
				 , 'Extensible.calendar.data.EventMappings'
				 , 'Ext.ux.grid.Printer'
      			 , 'Ext.ux.RowExpander'
      			 , 'Ext.selection.CheckboxModel' 
      			               
                 ]);

//    ,
//    'Ext.calendar.data.MemoryCalendarStore',
//    'Ext.calendar.data.MemoryEventStore',
//    'Ext.calendar.data.Events',
//    'Ext.calendar.data.Calendars'	  




    

    Ext.define('ModelOrdini', {
        extend: 'Ext.data.Model',
        fields: [
        			{name: 'task'}, {name: 'n_carico'}, {name: 'k_carico'}, {name: 'sped_id'}, {name: 'cod_iti'},
        			{name: 'k_ordine'}, {name: 'prog'}, {name: 'n_O'}, {name: 'n_M'}, {name: 'n_P'}, {name: 'n_CAV'},
        			{name: 'liv'},
					{name: 'cliente'}, {name: 'k_cli_des'}, {name: 'gmap_ind'}, {name: 'scarico_intermedio'},
					{name: 'nr'},
					{name: 'volume'},{name: 'pallet'},{name: 'peso'},
					{name: 'volume_disp'},{name: 'pallet_disp'},{name: 'peso_disp'},
					{name: 'colli'},{name: 'colli_disp'},{name: 'colli_sped'},{name: 'data_disp'}, {name: 'data_cons_cli'}, {name: 'data_conf_ord'}, {name: 'fl_data_disp'},
					{name: 'riferimento'},
					{name: 'tipo'},{name: 'raggr'},
					{name: 'cons_rich'},					
					{name: 'priorita'},{name: 'tp_pri'},
					{name: 'seq_carico'},					
					{name: 'stato'},
					{name: 'cons_conf'},{name: 'cons_prog'},{name: 'ind_modif'},
					{name: 'data_reg'},{name: 'data_scadenza'},					
					{name: 'importo'}, {name: 'referenze'}, {name: 'referenze_cons'},
					{name: 'fl_evaso'}, {name: 'fl_ref'}, {name: 'fl_bloc'}, {name: 'fl_art_manc'}, {name: 'fl_da_prog'}, {name: 'art_da_prog'}, {name: 'fl_new'}
        ]
    });


    Ext.define('RowCalendar', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task',     type: 'string'},
            {name: 'liv',      type: 'string'},            
            {name: 'flag1',    type: 'string'},            
            {name: 'user',     type: 'string'},
            {name: 'duration', type: 'string'},
            {name: 'done',     type: 'boolean'},
            {name: 'da_data',  type: 'float'}, {name: 'itin'}, {name: 'sped_id'}, {name: 'k_cliente'},            
            {name: 'd_1'}, {name: 'd_1_f'}, {name: 'd_1_t'}, {name: 'd_1_d'},
            {name: 'd_2'}, {name: 'd_2_f'}, {name: 'd_2_t'}, {name: 'd_2_d'},
            {name: 'd_3'}, {name: 'd_3_f'}, {name: 'd_3_t'}, {name: 'd_3_d'},
            {name: 'd_4'}, {name: 'd_4_f'}, {name: 'd_4_t'}, {name: 'd_4_d'},
            {name: 'd_5'}, {name: 'd_5_f'}, {name: 'd_5_t'}, {name: 'd_5_d'},
            {name: 'd_6'}, {name: 'd_6_f'}, {name: 'd_6_t'}, {name: 'd_6_d'},
            {name: 'd_7'}, {name: 'd_7_f'}, {name: 'd_7_t'}, {name: 'd_7_d'},
            {name: 'd_8'}, {name: 'd_8_f'}, {name: 'd_8_t'}, {name: 'd_8_d'},
            {name: 'd_9'}, {name: 'd_9_f'}, {name: 'd_9_t'}, {name: 'd_9_d'},
            {name: 'd_10'}, {name: 'd_10_f'}, {name: 'd_10_t'}, {name: 'd_10_d'},
            {name: 'd_11'}, {name: 'd_11_f'}, {name: 'd_11_t'}, {name: 'd_11_d'},
            {name: 'd_12'}, {name: 'd_12_f'}, {name: 'd_12_t'}, {name: 'd_12_d'},
            {name: 'd_13'}, {name: 'd_13_f'}, {name: 'd_13_t'}, {name: 'd_13_d'},
            {name: 'd_14'}, {name: 'd_14_f'}, {name: 'd_14_t'}, {name: 'd_14_d'},
			{name: 'fl_bloc'}, {name: 'fl_ord_bloc'}, {name: 'fl_art_mancanti'}, {name: 'fl_da_prog'}, {name: 'ha_contratti'}, {name: 'ha_proposte_MTO'}                                   
        ]
    });    

    //Model per albero itinerari
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task', type: 'string'}, {name: 'user', type: 'string'}, 'liv', 'sped_id', 'k_cliente', 'data'
        ]
    });    

    Ext.define('Ux.InputTextMask', {
 	   constructor: function(mask, clearWhenInvalid) {
 	     if(clearWhenInvalid === undefined)
 	         this.clearWhenInvalid = true;
 	      else
 	         this.clearWhenInvalid = clearWhenInvalid;
 	      this.rawMask = mask;
 	      this.viewMask = '';
 	      this.maskArray = new Array();
 	      var mai = 0;
 	      var regexp = '';
 	      for(var i=0; i<mask.length; i++){
 	         if(regexp){
 	            if(regexp == 'X'){
 	               regexp = '';
 	            }
 	           if(mask.charAt(i) == 'X'){
 	               this.maskArray[mai] = regexp;
 	               mai++;
 	               regexp = '';
 	            } else {
 	               regexp += mask.charAt(i);
 	           }
 	         } else if(mask.charAt(i) == 'X'){
 	            regexp += 'X';
 	            this.viewMask += '_';
 	         } else if(mask.charAt(i) == '9' || mask.charAt(i) == 'L' || mask.charAt(i) == 'l' || mask.charAt(i) == 'A') {
 	            this.viewMask += '_';
 	            this.maskArray[mai] = mask.charAt(i);
 	            mai++;
 	         } else {
 	            this.viewMask += mask.charAt(i);
 	            this.maskArray[mai] = RegExp.escape(mask.charAt(i));
 	            mai++;
 	         }
 	      }

 	      this.specialChars = this.viewMask.replace(/(L|l|9|A|_|X)/g,'');
 	      return this;
 	   },

 	   init: function(field) {
 	      this.field = field;

 	      if (field.rendered){
 	         this.assignEl();
 	      } else {
 	        field.on('render', this.assignEl, this);
 	     }

 	      field.on('blur',this.removeValueWhenInvalid, this);
 	      field.on('focus',this.processMaskFocus, this);
 	   },

 	   assignEl: function() {
 	      this.inputTextElement = this.field.inputEl.dom;
 	      this.field.inputEl.on('keypress', this.processKeyPress, this);
 	      this.field.inputEl.on('keydown', this.processKeyDown, this);
 	      if(Ext.isSafari || Ext.isIE){
 	         this.field.inputEl.on('paste',this.startTask,this);
 	         this.field.inputEl.on('cut',this.startTask,this);
 	      }
 	      if(Ext.isGecko || Ext.isOpera){
 	         this.field.inputEl.on('mousedown',this.setPreviousValue,this);
 	      }
 	      if(Ext.isGecko){
 	        this.field.inputEl.on('input',this.onInput,this);
 	      }
 	      if(Ext.isOpera){
 	        this.field.inputEl.on('input',this.onInputOpera,this);
 	      }
 	   },
 	   onInput: function(){
 	      this.startTask(false);
 	   },
 	   onInputOpera: function(){
 	      if(!this.prevValueOpera){
 	         this.startTask(false);
 	      }else{
 	         this.manageBackspaceAndDeleteOpera();
 	      }
 	   },

 	   manageBackspaceAndDeleteOpera: function(){
 	      this.inputTextElement.value=this.prevValueOpera.cursorPos.previousValue;
 	      this.manageTheText(this.prevValueOpera.keycode,this.prevValueOpera.cursorPos);
 	      this.prevValueOpera=null;
 	   },

 	   setPreviousValue: function(event){
 	      this.oldCursorPos=this.getCursorPosition();
 	   },

 	   getValidatedKey: function(keycode, cursorPosition) {
 	      var maskKey = this.maskArray[cursorPosition.start];
 	      if(maskKey == '9'){
 	         return keycode.pressedKey.match(/[0-9]/);
 	      } else if(maskKey == 'L'){
 	         return (keycode.pressedKey.match(/[A-Za-z]/))? keycode.pressedKey.toUpperCase(): null;
 	      } else if(maskKey == 'l'){
 	         return (keycode.pressedKey.match(/[A-Za-z]/))? keycode.pressedKey.toLowerCase(): null;
 	      } else if(maskKey == 'A'){
 	         return keycode.pressedKey.match(/[A-Za-z0-9]/);
 	      } else if(maskKey){
 	         return (keycode.pressedKey.match(new RegExp(maskKey)));
 	      }
 	      return(null);
 	   },

 	   removeValueWhenInvalid: function() {
 	      if(this.clearWhenInvalid && this.inputTextElement.value.indexOf('_') > -1){
 	         this.inputTextElement.value = '';
 	      }
 	   },

 	   managePaste: function() {
 	      if(this.oldCursorPos==null){
 	        return;
 	      }
 	      var valuePasted=this.inputTextElement.value.substring(this.oldCursorPos.start,this.inputTextElement.value.length-(this.oldCursorPos.previousValue.length-this.oldCursorPos.end));
 	      if(this.oldCursorPos.start<this.oldCursorPos.end){
 	         this.oldCursorPos.previousValue =
 	            this.oldCursorPos.previousValue.substring(0,this.oldCursorPos.start)+
 	            this.viewMask.substring(this.oldCursorPos.start,this.oldCursorPos.end)+
 	            this.oldCursorPos.previousValue.substring(this.oldCursorPos.end,this.oldCursorPos.previousValue.length);
 	         valuePasted=valuePasted.substr(0,this.oldCursorPos.end-this.oldCursorPos.start);
 	      }
 	      this.inputTextElement.value=this.oldCursorPos.previousValue;
 	      keycode = {
 	         unicode: '',
 	         isShiftPressed: false,
 	         isTab: false,
 	         isBackspace: false,
 	         isLeftOrRightArrow: false,
 	         isDelete: false,
 	         pressedKey: ''
 	      }
 	      var charOk=false;
 	      for(var i=0;i<valuePasted.length;i++){
 	         keycode.pressedKey=valuePasted.substr(i,1);
 	         keycode.unicode=valuePasted.charCodeAt(i);
 	         this.oldCursorPos=this.skipMaskCharacters(keycode,this.oldCursorPos);
 	         if(this.oldCursorPos===false){
 	            break;
 	         }
 	         if(this.injectValue(keycode,this.oldCursorPos)){
 	            charOk=true;
 	            this.moveCursorToPosition(keycode, this.oldCursorPos);
 	            this.oldCursorPos.previousValue=this.inputTextElement.value;
 	            this.oldCursorPos.start=this.oldCursorPos.start+1;
 	         }
 	      }
 	      if(!charOk && this.oldCursorPos!==false){
 	         this.moveCursorToPosition(null, this.oldCursorPos);
 	      }
 	      this.oldCursorPos=null;
 	   },

 	   processKeyDown: function(e){
 	      this.processMaskFormatting(e,'keydown');
 	   },

 	   processKeyPress: function(e){
 	      this.processMaskFormatting(e,'keypress');
 	   },

 	   startTask: function(setOldCursor){
 	      if(this.task==undefined){
 	         this.task=new Ext.util.DelayedTask(this.managePaste,this);
 	     }
 	      if(setOldCursor!== false){
 	         this.oldCursorPos=this.getCursorPosition();
 	     }
 	     this.task.delay(0);
 	   },

 	   skipMaskCharacters: function(keycode, cursorPos){
 	      if(cursorPos.start!=cursorPos.end && (keycode.isDelete || keycode.isBackspace))
 	         return(cursorPos);
 	      while(this.specialChars.match(RegExp.escape(this.viewMask.charAt(((keycode.isBackspace)? cursorPos.start-1: cursorPos.start))))){
 	         if(keycode.isBackspace) {
 	            cursorPos.dec();
 	         } else {
 	            cursorPos.inc();
 	         }
 	         if(cursorPos.start >= cursorPos.previousValue.length || cursorPos.start < 0){
 	            return false;
 	         }
 	      }
 	      return(cursorPos);
 	   },

 	   isManagedByKeyDown: function(keycode){
 	      if(keycode.isDelete || keycode.isBackspace){
 	         return(true);
 	      }
 	      return(false);
 	   },

 	   processMaskFormatting: function(e, type) {
 	      this.oldCursorPos=null;
 	      var cursorPos = this.getCursorPosition();
 	      var keycode = this.getKeyCode(e, type);
 	      if(keycode.unicode==0){//?? sometimes on Safari
 	         return;
 	      }
 	      if((keycode.unicode==67 || keycode.unicode==99) && e.ctrlKey){//Ctrl+c, let's the browser manage it!
 	         return;
 	      }
 	      if((keycode.unicode==88 || keycode.unicode==120) && e.ctrlKey){//Ctrl+x, manage paste
 	         this.startTask();
 	         return;
 	     }
 	      if((keycode.unicode==86 || keycode.unicode==118) && e.ctrlKey){//Ctrl+v, manage paste....
 	         this.startTask();
 	        return;
 	      }
 	     if((keycode.isBackspace || keycode.isDelete) && Ext.isOpera){
 	        this.prevValueOpera={cursorPos: cursorPos, keycode: keycode};
 	        return;
 	     }
 	      if(type=='keydown' && !this.isManagedByKeyDown(keycode)){
 	         return true;
 	      }
 	      if(type=='keypress' && this.isManagedByKeyDown(keycode)){
 	        return true;
 	     }
 	     if(this.handleEventBubble(e, keycode, type)){
 	        return true;
 	     }
 	     return(this.manageTheText(keycode, cursorPos));
 	   },

 	   manageTheText: function(keycode, cursorPos){
 	      if(this.inputTextElement.value.length === 0){
 	         this.inputTextElement.value = this.viewMask;
 	      }
 	      cursorPos=this.skipMaskCharacters(keycode, cursorPos);
 	      if(cursorPos===false){
 	         return false;
 	     }
 	    if(this.injectValue(keycode, cursorPos)){
 	        this.moveCursorToPosition(keycode, cursorPos);
 	      }
 	      return(false);
 	   },

 	   processMaskFocus: function(){
 	      if(this.inputTextElement.value.length == 0){
 	         var cursorPos = this.getCursorPosition();
 	         this.inputTextElement.value = this.viewMask;
 	         this.moveCursorToPosition(null, cursorPos);
 	      }
 	   },

 	   isManagedByBrowser: function(keyEvent, keycode, type){
 	      if(((type=='keypress' && keyEvent.charCode===0) ||
 	         type=='keydown') && (keycode.unicode==Ext.EventObject.TAB ||
 	         keycode.unicode==Ext.EventObject.RETURN ||
 	         keycode.unicode==Ext.EventObject.ENTER ||
 	         keycode.unicode==Ext.EventObject.SHIFT ||
 	         keycode.unicode==Ext.EventObject.CONTROL ||
 	         keycode.unicode==Ext.EventObject.ESC ||
 	         keycode.unicode==Ext.EventObject.PAGEUP ||
 	         keycode.unicode==Ext.EventObject.PAGEDOWN ||
 	         keycode.unicode==Ext.EventObject.END ||
 	         keycode.unicode==Ext.EventObject.HOME ||
 	         keycode.unicode==Ext.EventObject.LEFT ||
 	         keycode.unicode==Ext.EventObject.UP ||
 	        keycode.unicode==Ext.EventObject.RIGHT ||
 	        keycode.unicode==Ext.EventObject.DOWN)){
 	            return(true);
 	      }
 	      return(false);
 	   },

 	   handleEventBubble: function(keyEvent, keycode, type) {
 	      try {
 	         if(keycode && this.isManagedByBrowser(keyEvent, keycode, type)){
 	            return true;
 	         }
 	         keyEvent.stopEvent();
 	         return false;
 	      } catch(e) {
 	         alert(e.message);
 	      }
 	   },

 	  getCursorPosition: function() {
 	      var s, e, r;
 	      if(this.inputTextElement.createTextRange){
 	         r = document.selection.createRange().duplicate();
 	         r.moveEnd('character', this.inputTextElement.value.length);
 	         if(r.text === ''){
 	            s = this.inputTextElement.value.length;
 	         } else {
 	            s = this.inputTextElement.value.lastIndexOf(r.text);
 	         }
 	         r = document.selection.createRange().duplicate();
 	         r.moveStart('character', -this.inputTextElement.value.length);
 	         e = r.text.length;
 	      } else {
 	         s = this.inputTextElement.selectionStart;
 	         e = this.inputTextElement.selectionEnd;
 	      }
 	      return this.CursorPosition(s, e, r, this.inputTextElement.value);
 	   },

 	   moveCursorToPosition: function(keycode, cursorPosition) {
 	      var p = (!keycode || (keycode && keycode.isBackspace ))? cursorPosition.start: cursorPosition.start + 1;
 	      if(this.inputTextElement.createTextRange){
 	         cursorPosition.range.move('character', p);
 	         cursorPosition.range.select();
 	      } else {
 	         this.inputTextElement.selectionStart = p;
 	         this.inputTextElement.selectionEnd = p;
 	      }
 	   },

 	   injectValue: function(keycode, cursorPosition) {
 	      if (!keycode.isDelete && keycode.unicode == cursorPosition.previousValue.charCodeAt(cursorPosition.start))
 	         return true;
 	      var key;
 	      if(!keycode.isDelete && !keycode.isBackspace){
 	         key=this.getValidatedKey(keycode, cursorPosition);
 	      } else {
 	         if(cursorPosition.start == cursorPosition.end){
 	            key='_';
 	            if(keycode.isBackspace){
 	               cursorPosition.dec();
 	            }
 	         } else {
 	            key=this.viewMask.substring(cursorPosition.start,cursorPosition.end);
 	         }
 	}
 	      if(key){
 	         this.inputTextElement.value = cursorPosition.previousValue.substring(0,cursorPosition.start)
 	            + key +
 	            cursorPosition.previousValue.substring(cursorPosition.start + key.length,cursorPosition.previousValue.length);
 	         return true;
 	      }
 	      return false;
 	   },

 	   getKeyCode: function(onKeyDownEvent, type) {
 	     var keycode = {};
 	      keycode.unicode = onKeyDownEvent.getKey();
 	      keycode.isShiftPressed = onKeyDownEvent.shiftKey;

 	      keycode.isDelete = ((onKeyDownEvent.getKey() == Ext.EventObject.DELETE && type=='keydown') || ( type=='keypress' && onKeyDownEvent.charCode===0 && onKeyDownEvent.keyCode == Ext.EventObject.DELETE))? true: false;
 	      keycode.isTab = (onKeyDownEvent.getKey() == Ext.EventObject.TAB)? true: false;
 	     keycode.isBackspace = (onKeyDownEvent.getKey() == Ext.EventObject.BACKSPACE)? true: false;
 	      keycode.isLeftOrRightArrow = (onKeyDownEvent.getKey() == Ext.EventObject.LEFT || onKeyDownEvent.getKey() == Ext.EventObject.RIGHT)? true: false;
 	      keycode.pressedKey = String.fromCharCode(keycode.unicode);
 	      return(keycode);
 	   },

 	  CursorPosition: function(start, end, range, previousValue) {
 	      var cursorPosition = {};
 	      cursorPosition.start = isNaN(start)? 0: start;
 	      cursorPosition.end = isNaN(end)? 0: end;
 	      cursorPosition.range = range;
 	     cursorPosition.previousValue = previousValue;
 	      cursorPosition.inc = function(){cursorPosition.start++;cursorPosition.end++;};
 	    cursorPosition.dec = function(){cursorPosition.start--;cursorPosition.end--;};
 	      return(cursorPosition);
 	  }
 	});


    Ext.define('Ext.ux.ColorField', {
        extend: 'Ext.form.field.Trigger',
        alias: 'widget.colorfield',    
        requires: ['Ext.form.field.VTypes', 'Ext.layout.component.field.Text'],

        lengthText: "Color hex values must be either 3 or 6 characters.",
        blankText: "Must have a hexidecimal value in the format ABCDEF.",
        
        regex: /^[0-9a-f]{3,6}$/i,
        
        validateValue : function(value){
            if(!this.getEl()) {
                return true;
            }
            if(value.length!=3 && value.length!=6) {
                this.markInvalid(Ext.String.format(this.lengthText, value));
                return false;
            }
            if((value.length < 1 && !this.allowBlank) || !this.regex.test(value)) {
                this.markInvalid(Ext.String.format(this.blankText, value));
                return false;
            }
            
            this.markInvalid();
            this.setColor(value);
            return true;
        },

        markInvalid : function( msg ) {
            Ext.ux.ColorField.superclass.markInvalid.call(this, msg);
            this.inputEl.setStyle({
                'background-image': 'url(../resources/themes/images/default/grid/invalid_line.gif)'
            });
        },
        
        setValue : function(hex){
            Ext.ux.ColorField.superclass.setValue.call(this, hex);
            this.setColor(hex);
        },
        
        setColor : function(hex) {
            Ext.ux.ColorField.superclass.setFieldStyle.call(this, {
                'background-color': '#' + hex,
                'background-image': 'none'
            });
        },

        menuListeners : {
            select: function(m, d){
                this.setValue(d);
            },
            show : function(){
                this.onFocus();
            },
            hide : function(){
                this.focus();
                var ml = this.menuListeners;
                this.menu.un("select", ml.select,  this);
                this.menu.un("show", ml.show,  this);
                this.menu.un("hide", ml.hide,  this);
            }
        },
        
        onTriggerClick : function(e){
            if(this.disabled){
                return;
            }
            
            this.menu = new Ext.menu.ColorPicker({
                shadow: true,
                autoShow : true,
                height : 120
            });
            this.menu.alignTo(this.inputEl, 'tl-bl?');
            this.menu.doLayout();
            
            this.menu.on(Ext.apply({}, this.menuListeners, {
                scope:this
            }));
            
            this.menu.show(this.inputEl);
        }
    });

 	Ext.applyIf(RegExp, {
 	   escape: function(str) {
 	      return new String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
 	   }
 	}); 

    
	//TODO: da spostare in PLAN
	function getCellClass (val, meta, rec, rowIndex, colIndex, store) {
		idx = colIndex - 6; //colonna5 corrisponde a d_1
		 
		if (rec.get('d_' + idx + '_f') == 'F' || rec.get('d_' + idx + '_f') == 'S' || rec.get('d_' + idx + '_f') == 'D'){
			meta.tdCls += ' festivo';
		}
		if (rec.get('d_' + idx + '_t') == '1'){
			meta.tdCls += ' con_carico_1';
		}		
		if (rec.get('d_' + idx + '_t') == '2'){
			meta.tdCls += ' con_carico_2';
		}
		if (rec.get('d_' + idx + '_t') == '3'){
			meta.tdCls += ' con_carico_3';
		}		
		if (rec.get('d_' + idx + '_t') == '4'){
			meta.tdCls += ' con_carico_4';
		}		
		if (rec.get('d_' + idx + '_t') == '5'){
			meta.tdCls += ' con_carico_5';
		}
		if (rec.get('d_' + idx + '_t') == '6'){
			meta.tdCls += ' con_carico_6';
		}				
		
		if ((rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3') && rec.get('d_' + idx).length > 0 && rec.get('d_' + idx + '_d').length > 0)		
			return val += "<span class=c_disp>/" + rec.get('d_' + idx + '_d') + "</span>";
		else
			return val;	
	}

    

    

    //PLAN ARRIVI
    function show_el_arrivi(){
    	mp = Ext.getCmp('m-panel');
    	arrivi = Ext.getCmp('panel-arrivi');

    	if (arrivi){
        	arrivi.store.reload();
        	arrivi.show();		    	
    	} else {

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_arrivi_json_plan.php',
    		        method     : 'GET',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            mp.add(jsonData.items);
    		            mp.doLayout();
    		            mp.show();
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });

    	}
    }



    //PANEL fo_presca
    function show_fo_presca(){
       	   acs_show_win_std('Prenotazione scarichi', 'acs_panel_fo_presca.php?fn=get_open_form', null, 530, 420);	
    }

    function show_win_bl_cliente(cliente, desc){	
    	// create and show window
    	print_w = new Ext.Window({
    			  width: 700
    			, height: 400
    			, plain: false
    			, title: 'Blocco note fornitore: ' + desc
    			, layout: 'fit'
    			, border: true
    			, closable: true
    			, maximizable: false										
    		});	
    	print_w.show();

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : '../desk_acq/acs_anag_cli_note.php?fn=open_bl&cliente=' + cliente+ '&desc=' +desc,
    		        method     : 'POST',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            print_w.add(jsonData.items);
    		            print_w.doLayout();				            
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });		
    } 
    
   
    
	function allegatiPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=600, height=600, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}	


    function show_win_righe_ord(ord, k_ord){
 	   acs_show_win_std('Righe ordine', <?php echo acs_url('module/desk_acq/acs_get_order_rows.php') ?>, {k_ord: k_ord, dtep: ord.get('cons_prog')}, 1150, 480);        
    }


    function show_win_disponibilita(dt, art, rec, tpno){
      	acs_show_win_std('Loading.', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('ditta_origine'), rdart: rec.get('RDART')}, 1100, 600, null, 'icon-shopping_cart_gray-16');
    }

    function show_win_art_critici(ord, type){
		// create and show window
		var win = new Ext.Window({
		  width: 950
		, height: 500
		, minWidth: 600
		, minHeight: 400
		, plain: true
		, title: 'Segnalazione articoli critici'
		, layout: 'fit'
		, border: true
		, closable: true
		, items:  
			new Ext.grid.GridPanel({
				
			        listeners: {
			        	
							  celldblclick: {								
								  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){									  
								  	rec = iView.getRecord(iRowEl);								  	
								  	show_win_disponibilita(rec.get('ditta_origine'), rec.get('RDART'), rec, rec.get('RDTPNO'));
								  }
							  }	  
					},

				
					store: new Ext.data.Store({
						
						listeners: {
						            load: function () {
						                win.setTitle('Segnalazione articoli critici ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO + ' - Disponibilit&agrave; prevista: ' + this.proxy.reader.jsonData.ordine.TDDTDS);
						            }
						         },
						
						groupField: 'RDTPNO',			
						autoLoad:true,				        
	  					proxy: {
								url: '../desk_vend/acs_get_order_rows.php?type=' + type + '&nord=' + ord,
								type: 'ajax',
								reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
		        			fields: [
		        				'REVOCHE', 'ditta_origine', 'TDDTEP', 'RDFMAN', 'RDDTDS', 'RDDFOR', 'RDDTEP', 'RDTPNO', 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDART', 'RDDES1', 'RDDES2', 'RDUM', 'RDDT', {name: 'RDQTA', type: 'float'}
		        			]
		    			}),
		    		features: new Ext.create('Ext.grid.feature.Grouping',{
        							groupHeaderTpl: 'Tipo: {name}',
        							hideGroupedHeader: true
    						}),
    						
					viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('RDFMAN')=='M')
					           		return ' segnala_riga_rosso';
					           if (record.get('RDFMAN')=='C')
					           		return ' segnala_riga_giallo';				           		
					           if (record.get('RDDTDS')==record.get('TDDTEP'))
					           		return ' segnala_riga_viola';					           		
					           return '';																
					         }   
					    },    						
    						
			        columns: [
			             {
			                header   : 'Disp. Sped.',
			                dataIndex: 'RDDTDS', 
			                width     : 70,
			                renderer: date_from_AS
			             },			        
			             {
			                header   : 'Stato',
			                dataIndex: 'RDRIFE', 
			                width     : 95
			             }, {
			                header   : 'Fornitore',
			                dataIndex: 'RDDFOR', 
			                flex    : 50
			             }, {
                            header    : 'Descrizione',
                            dataIndex : 'RDDES1',
                            flex      : 150
                         }, {
                            header    : 'Codice',
                            dataIndex : 'RDART',
                            width      : 150
                         }, {
			                header   : 'Um',
			                dataIndex: 'RDUM', 
			                width    : 30
			             }, {
			                header   : 'Q.t&agrave;',
			                dataIndex: 'RDQTA', 
			                width    : 50,
			                align: 'right', renderer: floatRenderer2
			             }, {
			                header   : 'Consegna',
			                dataIndex: 'RDDTEP', 
			                flex    : 30,
			                renderer: date_from_AS
			             }, {
			                header   : 'Documento',
			                dataIndex: 'RDDES2', 
			                flex    : 60
			             }
			            
			         ],
			         listeners: {
			              afterrender: function(comp){
				                 //autocheck degli ordini che erano stati selezionati
			            	  		comp.store.on('load', function(store, records, options) {
				            	  		b_rev = comp.up('window').down('#b_revoche');
										if(records[0].data.REVOCHE > 0){
											revoche = records[0].data.REVOCHE;
											b_rev.setText( '<b>' + b_rev.text + ' ('+ revoche + ') </b>');
											 
										}
				            	  							
			    					}, comp);
			    			 }
				          }
	 					 ,dockedItems: [{
			                    dock: 'bottom',
			                    xtype: 'toolbar',
			                    scale: 'large',
			                    items: [
			                     {
			                        xtype: 'button',
			                        text: 'Revoche MTO',
			                        itemId : 'b_revoche',
			                        style:'border: 1px solid gray;', 
			    		            scale: 'large',	               
			    		            width : 150,     
			    		            margin : '0 0 0 400',       
			    		          	handler: function() {
			    		          	    acs_show_win_std(null,
			      						  		'../base/acs_revoche_mto.php?fn=open_win', {
			      						  		k_ordine: ord
			  						  	}, null, null, null, null);
			    	       			}
						        }
			                    ]
			                    
			                    }]
		                    })   

		});

		win.show();
}
    
    function show_win_grid_articoli(list_art){
   	 	acs_show_win_std('Riepilogo articoli esclusi', 'acs_riepilogo_articoli.php?fn=open_win', 
	    {list_art : list_art}, 400, 200, null, 'icon-leaf-16');        	
   }


     function show_win_bl_articolo(c_art){	
    	// create and show window
    	print_w = new Ext.Window({
    			  width: 600
    			, height: 400
    			, plain: false
    			, title: 'Blocco note articolo ' +c_art
    			, layout: 'fit'
    			, border: true
    			, closable: true
    			, maximizable: false										
    		});	
    	print_w.show();

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_anag_art_note.php?fn=open_bl&c_art=' + c_art,
    		        method     : 'POST',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            print_w.add(jsonData.items);
    		            print_w.doLayout();				            
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });		
    } //show_win_annotazioni_articolo


    function show_win_bl_riga(rife1){	
    	// create and show window
    	print_w = new Ext.Window({
    			  width: 400
    			, height: 400
    			, plain: false
    			, title: 'Blocco note riga'
    			, layout: 'fit'
    			, border: true
    			, closable: true
    			, maximizable: false										
    		});	
    	print_w.show();

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_anag_art_note_riga.php?fn=open_bl&rife1=' + rife1,
    		        method     : 'POST',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            print_w.add(jsonData.items);
    		            print_w.doLayout();				            
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });		
    } //show_win_annotazioni_articolo


    // show/refresh win dettaglio articoli
    function show_win_dett_art(c_art, rec){
    	var tp = Ext.getCmp('tp-dett-art');
    	if (tp) {
          //refresh dati
    	  tp.loadArt(c_art);
    	} else {
   		  acs_show_win_std('Dettaglio articolo', 'acs_win_dettaglio_articoli.php?fn=open_win', 
		    {c_art : rec.get('codice'), d_art : rec.get('task')}, 700, 600, null, 'icon-leaf-16');        	
    	}
    	
    }



    function show_win_colli_ord(ord, k_ord){
    		// create and show window
    		var win = new Ext.Window({
    		  width: 1200
    		, height: 500
    		, minWidth: 300
    		, minHeight: 400
    		, plain: true
    		, title: 'Loading...'
    		, layout: 'fit'
    		, border: true
    		, closable: true
    		, items:  
    			new Ext.grid.GridPanel({
    					store: new Ext.data.Store({

    						listeners: {
    				            load: function () {
    				                win.setTitle('Elenco corrente colli ordine ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO);
    				                if(this.proxy.reader.jsonData.trad == 'Y')
    										 win.setTitle( win.title + ' - Traduzione ' +this.proxy.reader.jsonData.lng);
    						    }
    				         },
    					  	autoLoad:true,				        
    	  					proxy: {
    								url: '../desk_vend/acs_get_order_cols.php?k_ord=' + k_ord + '&trad=Y',
    								type: 'ajax',
    								reader: {
    						            type: 'json',
    						            root: 'root'
    						        }
    							},
    		        			fields: [
    		            			'PLPNET', 'PLPLOR', 'PLRIGA', 'PLVOLU', 'PLAADO', 'PLNRDO', {name: 'PLCOAB', type: 'int'}, 
    		            			'PLDART', 'PLUM', 'PLQTA', 'PLDTEN', 'PLTMEN', 'PLDTUS', 'PLTMUS', 'PLDTRM', 'PLTMRM',
    		            			'PSDTSC', 'PSTMSC', 'PSRESU', 'PSCAUS', 'tooltip_scarico', 'PSPALM', 'PSLATI', 'PSLONG', 
    		            			'PLART', 'PLRCOL','ALDAR1', 'TADESC', 'TDRFLO', 'PLNCOL', 'gr_collo'
    		        			]
    		    			}),
  		    			   stateful: true,
    				       stateId: 'elenco_packing_list',
    				       stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
    				     
    		    			<?php $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>"; ?>
    			        columns: [			        			        
       			        	 {
      			                header   : 'Riga', align: 'right',
      			                dataIndex: 'PLRIGA', 
      			                width    : 35
      			             },{
    			                header   : 'Nr collo', align: 'right',
    			                dataIndex: 'PLCOAB', 
    			                flex     : 20
    			             }, {
    			                header   : 'Codice',
    			                dataIndex: 'PLART', 
    			                width    : 100
    				         },{
    			                header   : 'Descrizione',
    			                dataIndex: 'PLDART', 
    			                flex    : 100
    			             },{
    			            	text: '&nbsp;<br><?php echo $trad; ?>',
    			                dataIndex: 'ALDAR1',
    			                tooltip: 'Traduzione',
    			                menuDisabled: true,
    			                width : 30,
    			                renderer: function(value, metaData, record){
    				    			  if(!Ext.isEmpty(record.get('ALDAR1')) || !Ext.isEmpty(record.get('TADESC'))){
    										if (record.get('PLART').trim() == record.get('PLRCOL').trim())	    			    	
    											metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('TADESC')) + '"';
    					    			  	else
    					    			  		metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('ALDAR1')) + '"';  			    
    					    			  
    					    			 return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=15>';
    				    			  }else{
    					    			 return '';
    				    			  }
    				    			  
    				    		  }
    			             }, {
    			                header   : 'UM',
    			                dataIndex: 'PLUM', 
    			                flex    : 10
    			             }, {
    			                header   : 'Quantit&agrave;',
    			                dataIndex: 'PLQTA', 
    			                flex    : 30,
    			                align: 'right',
    			                renderer : floatRenderer2
    			             },{
    			                header   : 'Volume',
    			                dataIndex: 'PLVOLU', 
    			                flex    : 30,
    			                align: 'right',
    			                renderer : floatRenderer3
    			             }, {
    			                header   : 'Disponibilit&agrave;',
    			                dataIndex: 'PLDTEN', 
    			                flex    : 30,
    			                renderer: function(value, p, record){
    			                	return datetime_from_AS(record.get('PLDTEN'), record.get('PLTMEN'))
    			    			}
    			             }, {
    			                header   : 'Spedizione',
    			                dataIndex: 'PLDTUS', 
    			                flex    : 30,
    			                renderer: function(value, p, record){
    			                	return datetime_from_AS(record.get('PLDTUS'), record.get('PLTMUS'))
    			    			}			                
    			             }, {
    			                header   : 'Aggiunte',
    			                dataIndex: 'PLDTRM', 
    			                flex    : 30,
    			                renderer: function(value, p, record){
    			                	return datetime_from_AS(record.get('PLDTRM'), record.get('PLTMRM'))
    			    			}			                			                
    			             }, {
    				                header   : 'Scarico',
    				                dataIndex: 'PSDTSC', 
    				                flex    : 30,
    				                renderer: function(value, metaData, record){
    				                	metaData.tdAttr = 'data-qtip="Scanner: ' + Ext.String.htmlEncode(record.get('PSPALM')) + ' - Coordinate: ' + Ext.String.htmlEncode(record.get('PSLATI')) + ' - ' + Ext.String.htmlEncode(record.get('PSLONG')) + '"';
    				                	return datetime_from_AS(record.get('PSDTSC'), record.get('PSTMSC'))
    				    			}			                			                
    				          }, {
    				                header   : 'Info',
    				                dataIndex: 'PSDTSC', 
    				                width: 50,
    				                renderer: function(value, metaData, record){

    				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)
    				                		metaData.tdAttr = 'data-qtip="Segnalazione allo scarico: ' + Ext.String.htmlEncode(record.get('PSCAUS')) + '"';
    			                		
    				                	if (Ext.isEmpty(record.get('PSRESU'))==false && record.get('PSRESU') == 'E')			    	
    							    		return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
    				                	if (Ext.isEmpty(record.get('PSCAUS'))==false && record.get('PSCAUS').trim().length > 0)			    	
    							    		return '<img src=<?php echo img_path("icone/48x48/comments.png") ?> width=18>';
    				    			}			                			                
    				          },
    				          {
        			                header   : 'Peso netto',
        			                dataIndex: 'PLPNET', 
        			                flex    : 30,
        			                align: 'right',
        			                renderer : floatRenderer3
        			             },
        			             {
          			                header   : 'Peso lordo',
          			                dataIndex: 'PLPLOR', 
          			                flex    : 30,
          			                align: 'right',
            			            renderer : floatRenderer3
          			             },
          			             <? echo dx_mobile() ?>

    			        
    			         ],
    			         listeners: {
    			        	 celldblclick: {
    			        		  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
    				   		            
    					           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
    								  	rec = iView.getRecord(iRowEl);

    								  	if(Ext.isEmpty(rec.get('TDRFLO'))){
    										var lingua = 'lingua non definita';
    										var cod_lingua = null;
    									 }else{
    										var lingua = rec.get('TDRFLO').trim();
    										var cod_lingua = rec.get('TDRFLO').trim();
    									}

    									if(rec.get('PLART').trim() == rec.get('PLRCOL').trim())
    										var gest_type = 'C';
    									else
    										var gest_type = 'A';

    			    				    if(col_name == 'ALDAR1' && !Ext.isEmpty(cod_lingua))	
    			    					   acs_show_win_std('Traduzione in ' + lingua, 
    					    					'acs_gest_trad_colli_articoli.php?fn=open_tab', 
    					    					{codice: rec.get('PLART'), desc : rec.get('PLDART'), lng: lingua, type : gest_type}, 400, 270,  {
    				        					'afterSave': function(from_win){
    				        						iView.getStore().load();
    	 											from_win.close();
    				        					}
    				        				}, 'icon-globe-16');
    			    				
    				            	}

    				        	 }
   				        	  , itemcontextmenu : function(grid, rec, node, index, event) {	
									
									event.stopEvent();
									var grid = this;
				             		var rows = grid.getSelectionModel().getSelection();
					  				
									var voci_menu = [];
									
									voci_menu.push({
						         		text: 'Elenco completo articoli/componenti',
						        		iconCls : 'icon-leaf-16',          		
						        		handler: function() {
						        			acs_show_win_std(null, 'acs_elenco_riepilogo_art_comp.php?fn=open_elenco', 
								        				{k_ordine: k_ord, riga : rec.get('PLRIGA'), collo : rec.get('PLNCOL'), gr_collo : rec.get('gr_collo'), peso_n : rec.get('PLPNET')});   	     	
						        		}
						    		});
						    		
						    		var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);	
										
										
										
							}

    				         }




    				         })   

    		});

    		win.show();
    }
    


    Ext.onReady(function() {

        Ext.QuickTips.init();






    Extensible.calendar.data.EventMappings = {
        // These are the same fields as defined in the standard EventRecord object but the
        // names and mappings have all been customized. Note that the name of each field
        // definition object (e.g., 'EventId') should NOT be changed for the default fields
        // as it is the key used to access the field data programmatically.
        EventId:     {name: 'EventId', mapping:'id', type:'int'}, // int by default
        CalendarId:  {name: 'CalendarId', mapping: 'cid', type: 'string'}, // int by default
        Title:       {name: 'Title', mapping: 'title'},
        StartDate:   {name: 'StartDate', mapping: 'start', type: 'date', dateFormat: 'c'},
        EndDate:     {name: 'EndDate', mapping: 'end', type: 'date', dateFormat: 'c'},
        RRule:       {name: 'RecurRule', mapping: 'rrule'},
        Location:    {name: 'Location', mapping: 'loc'},
        Notes:       {name: 'Notes', mapping: 'notes'},
        Url:         {name: 'Url', mapping: 'url'},
        IsAllDay:    {name: 'IsAllDay', mapping: 'ad', type: 'boolean'},
        Reminder:    {name: 'Reminder', mapping: 'rem'},        


        //Acs
        Risorsa: 	  {name:'Risorsa', mapping: 'risorsa', type: 'string'},
        Stabilimento: {name:'Stabilimento', mapping: 'stabilimento', type: 'string'},
        Trasp:		  {name:'Trasp', mapping: 'trasp', type: 'string'},
        Porta:    	  {name:'Porta', mapping: 'porta'},        
        
        
        // We can also add some new fields that do not exist in the standard EventRecord:
        CreatedBy:   {name: 'CreatedBy', mapping: 'created_by'},
        IsPrivate:   {name: 'Private', mapping:'private', type:'boolean'}
        
    };
    // Don't forget to reconfigure!
    Extensible.calendar.data.EventModel.reconfigure();






 /* PER DRAG & DROP **********/
    /*
     * Currently the calendar drop zone expects certain drag data to be present, so if dragging
     * from the grid, default the data as required for it to work
     */
    var nodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag1';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
        }
    };
    
    /*
     * Need to hook into the drop to provide a custom mapping between the existing grid
     * record being dropped and the new calendar record being added
     */
    var nodeDropInterceptor = function(n, dd, e, data){
        if(n && data){
            if(typeof(data.type) === 'undefined'){
                var rec = Ext.create('Extensible.calendar.data.EventModel'),
                    M = Extensible.calendar.data.EventMappings;
                
                // These are the fields passed from the grid's record
                //var gridData = data.selections[0].data;
                var gridData = data.records[0].data
                rec.data[M.Title.name] = 'salvataggio...';
                rec.data[M.CalendarId.name] = gridData.cid;
                
                // You need to provide whatever default date range logic might apply here:
                rec.data[M.StartDate.name] = n.date;
                rec.data[M.EndDate.name] = Extensible.Date.add(n.date, { hours: 1 });;
                rec.data[M.IsAllDay.name] = false;

                rec.data[M.CreatedBy.name] = gridData.TDNBOC;  //lo appoggio qui altrimenti non mi esegui il create               
                rec.data[M.Stabilimento.name] = gridData.TDSTAB;
                rec.data[M.Porta.name] = this.view.store.proxy.extraParams.f_porta;
                
                // save the evrnt and clean up the view
                ///////this.view.onEventDrop(rec, n.date);
                this.view.store.add(rec.data);                                                
                this.onCalendarDragComplete();
				this.view.fireEvent('eventadd');
                return false; //per non eseguire onDrop Normale
            }
        }
    };
    
    /*
     * Day/Week view require special logic when dragging over to create a 
     * drag shim sized to the event being dragged
     */
    var dayViewNodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag2';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
    
            var dayCol = e.getTarget('.ext-cal-day-col', 5, true);
            if (dayCol) {
                var box = {
                    height: Extensible.calendar.view.Day.prototype.hourHeight,
                    width: dayCol.getWidth(),
                    y: n.timeBox.y,
                    x: n.el.getLeft()
                }
                this.shim(n.date, box);
            }
        }
    };
    
    // Apply the interceptor functions to each class:
    var dropZoneProto = Extensible.calendar.dd.DropZone.prototype,
        dayDropZoneProto = Extensible.calendar.dd.DayDropZone.prototype 
    
    dropZoneProto.onNodeOver = Ext.Function.createInterceptor(dropZoneProto.onNodeOver, nodeOverInterceptor);
    dropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dropZoneProto.onNodeDrop, nodeDropInterceptor);
    
    dayDropZoneProto.onNodeOver = Ext.Function.createInterceptor(dayDropZoneProto.onNodeOver, dayViewNodeOverInterceptor);
    dayDropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dayDropZoneProto.onNodeDrop, nodeDropInterceptor);



    
    
    /*
     * This is a simple override required for dropping from outside the calendar. By default it
     * assumes that any drag originated within itelf, so a drag would be a move of an existing event.
     * This is no longer the case and it must check to see what the record state is.
     */
    Extensible.calendar.view.AbstractCalendar.override({
        onEventDrop : function(rec, dt){
        	console.log('index.php - onEventDrop override');
            if (rec.phantom) {
                this.onEventAdd(null, rec);
            }
            else {
                this.moveEvent(rec, dt);
            }
        }
    });    	



/*
    Ext.define('acsNumberField', {
    	extend: 'Ext.form.field.NumberField',
    	alias: 'acsNumberField'
    });
*/    


/*    
	var myNumberField = Ext.extend(Ext.form.NumberField, {
        setValue : function(v){
            v = typeof v == 'number' ? v : String(v).replace(this.decimalSeparator, ".");
            v = isNaN(v) ? '' : String(v).replace(".", this.decimalSeparator);
            //  if you want to ensure that the values being set on the field is also forced to the required number of decimal places.
            // (not extensively tested)
            // v = isNaN(v) ? '' : this.fixPrecision(String(v).replace(".", this.decimalSeparator));
            return Ext.form.NumberField.superclass.setValue.call(this, v);
        },
        fixPrecision : function(value){
            var nan = isNaN(value);
            if(!this.allowDecimals || this.decimalPrecision == -1 || nan || !value){
               return nan ? '' : value;
            }
            return parseFloat(value).toFixed(this.decimalPrecision);
        }
    });
*/    

    

    //COMBO: possibilita di rimuovere valore selezionato (con forceSelection lasciava sempre l'ultimo)
    Ext.form.field.ComboBox.override({
     beforeBlur: function(){
         var value = this.getRawValue();
         if(value == ''){
             this.lastSelection = [];
         }
         this.doQueryTask.cancel();
         this.assertValue();
     }
    });


	//disabilito il menu sugli eventi
	Extensible.calendar.menu.Event.override({
		showForEvent: function(rec, el, xy){
				return false;
		}
	});
    

	//formato data negli header
	Extensible.calendar.template.BoxLayout.prototype.singleDayDateFormat = 'd / m';	

    
    //PER RIMUOVERE L'ORARIO NEGLI EVENTI
    Extensible.calendar.view.DayBody.override({
    getTemplateEventData : function(evt){
        var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            colorCls = 'x-cal-default',
            title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ',
            recurring = evt[M.RRule.name] != '';
        
        this.getTemplateEventBox(evt);
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, 
                    evt[M.CalendarId.name]);
                
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '') + (Ext.isIE || Ext.isOpera ? '-x' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
        data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        data.Title =  (!title || title.length == 0 ? this.defaultEventTitleText : title);
        return Ext.applyIf(data, evt);
    }    
    });
    
  











            

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            // create instance immediately
            Ext.create('Ext.Component', {
                region: 'north',
                id: 'page-header',
                height: 110, // give north and south regions a height
                contentEl: 'header'
            }), {
                // lazily created panel (xtype:'panel' is default)
                region: 'south',
                contentEl: 'south',
                split: true,
                height: 300,
                minSize: 100,
                maxSize: 500,
                collapsible: true,
                collapsed: true,
                title: 'Legenda',
                margins: '0 0 0 0'
            }, {
                region: 'west',
                stateId: 'navigation-panel',
                id: 'west-panel',
                title: 'Menu',
                split: true,
                width: 200,
                minWidth: 175,
                maxWidth: 400,
                collapsible: true,
                collapsed: true,
                animCollapse: true,
                margins: '0 0 0 5',
                layout: 'accordion'
            },
            
            
            Ext.create('Ext.tab.Panel', {
            	id: 'm-panel',
        		cls: 'supply_desk_main_panel',            	            	
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 1,     // first tab initially active
                items: [
			                                   
                             					                                                
                ]
            })]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------

       if (Ext.get("main-site-search") != null) {

         	Ext.QuickTips.register({
    			target: "main-site-search",
    			title: 'Ricerca ordini',
    			text: 'Ricerca ordini gestionale'
    		});    	 

          Ext.get("main-site-search").on('click', function(){
        	  acs_show_win_std('Ricerca su archivio ordini di vendita', 'acs_ricerca_ordini_gestionale.php?fn=ricerca_ordini_grid', {}, 800, 450, null, 'icon-folder_search-16');
          });       	

       }  
      	

        if (Ext.get("bt-agenda_settimanale_spedizioni") != null) {

         	Ext.QuickTips.register({
    			target: "bt-agenda_settimanale_spedizioni",
    			title: 'Week',
    			text: 'Agenda settimanale spedizioni'
    		});    	 

          Ext.get("bt-agenda_settimanale_spedizioni").on('click', function(){
       	   acs_show_win_std('Agenda settimanale spedizioni', 'acs_panel_agenda_settimanale.php?fn=get_form_params', null, 530, 420);	
          });       	

       }   

        if (Ext.get("bt-toolbar") != null){
         	Ext.QuickTips.register({
    			target: "bt-toolbar",
    			title: 'Setup',
    			text: 'Manutenzione voci/parametri gestionali di base'
    		});
                
    	    Ext.get("bt-toolbar").on('click', function(){
    				print_w = new Ext.Window({
    						  width: 1160
    						, height: 300
    						, minWidth: 300
    						, minHeight: 300
    						, plain: true
    						, title: 'Manutenzione voci/parametri gestionali di base'
    						, layout: 'fit'
    						, border: true
    						, closable: true
    						, scrollable: 'auto'
    						, tools: [
    									{
    									    type:'help',
    									    tooltip: 'Help',
    									    handler: function(event, toolEl, panel){
    									        show_win_help('SETUP_MAIN');
    									    }
    									}								
    								]										
    					});	
    				print_w.show(); 
    				
    					//carico la form dal json ricevuto da php
    					Ext.Ajax.request({
    					        url        : 'acs_form_json_toolbar.php',
    					        method     : 'GET',
    					        waitMsg    : 'Data loading',
    					        success : function(result, request){
    					            var jsonData = Ext.decode(result.responseText);
    					            print_w.add(jsonData.items);
    					            print_w.doLayout();
    					        },
    					        failure    : function(result, request){
    					            Ext.Msg.alert('Message', 'No data to be loaded');
    					        }
    					    });						   	
    	    });
        }
        
        
      	
    	//Ecommerce: gestione articoli
       if (Ext.get("bt-ecommerce-gestart") != null){
        	Ext.QuickTips.register({
    			target: "bt-ecommerce-gestart",
    			title: 'Ecommerce',
    			text: 'Gestione anagrafica articoli'
    		});
                    
	        Ext.get("bt-ecommerce-gestart").on('click', function(){
	        	acs_show_panel_std('acs_panel_ecommerce_gestart.php?fn=open_tab', 'panel-ecommerce-gestart');
	        });
        }          
    	

    	//Anagrafica articoli
        if (Ext.get("bt-anag-art") != null){
        	Ext.QuickTips.register({
    			target: "bt-anag-art",
    			title: 'Anagrafica articoli',
    			text: 'Gestione anagrafica articoli'
    		});
                    
	        Ext.get("bt-anag-art").on('click', function(){
	        	acs_show_win_std('Parametri selezione anagrafica articoli', 'acs_panel_anag_art.php?fn=open_form_filtri', 
	    	        	null, 850, 430, null, 'icon-barcode', null, null, null, null, 'f_params');
	        });
        }            
        


      	//Indici
       if (Ext.get("bt-anag-art") != null){
       	Ext.QuickTips.register({
   			target: "bt-indici",
   			title: 'Indici',
   			text: 'Gestione configurazione indici'
   		});
                   
	        Ext.get("bt-indici").on('click', function(){
	        	acs_show_win_std('Indici di documentazione regole/attivit&agrave; di codifica', 'acs_anag_artd_crea_nuovo.php?fn=open_indici', {}, 
            		400, 400, null, 'icon-bookmark-16');
            		
	        });
       } 


      //Setup scorte
        if (Ext.get("bt-setup-scorte") != null){
        	Ext.QuickTips.register({
    			target: "bt-anag-forn",
    			title: 'Setup scorte',
    			text: 'Setup scorte'
    		});
                    
	        Ext.get("bt-setup-scorte").on('click', function(){
	        	
	        	acs_show_win_std('Setup scorte', 'acs_scorte_init_form.php?fn=open_form', null , 650, 220, {}, 'icon-listino');
	        });
        }  


        if (Ext.get("bt-arrivi") != null){
           	Ext.QuickTips.register({
       			target: "bt-arrivi",
       			title: 'To Do List',
       			text: 'Gestione attivit&agrave; di manutenzione base dati articoli'
       		});            
   	        Ext.get("bt-arrivi").on('click', function(){
   	        	acs_show_win_std('Parametri interrogazione To Do list', 'acs_panel_todolist.php?fn=open_filtri', null , 600, 250, {}, 'icon-arrivi-16');             
   	        	});	    
           }

        if (Ext.get("bt-import") != null){
           	Ext.QuickTips.register({
       			target: "bt-import",
       			title: 'Import',
       			text: 'Import/aggiornamento dati anagrafici'
       		});            
   	        Ext.get("bt-import").on('click', function(){
   	        	acs_show_win_std('Parametri interrogazione import dati anagrafici articolo', 'acs_panel_import.php?fn=open_filtri', null , 510, 200, {}, 'icon-F8-m-16');             
   	        	});	    
           }

      	// ------------------- REGOLE RIORDINO FORNITORI ------------------------        
        if (Ext.get("bt-regole-riordino") != null) {
          	Ext.QuickTips.register({
   			target: "bt-regole-riordino",
   			title: 'ATP Setup',
   			text: 'Regole riordino forniture'
   		});    	 
            
           Ext.get("bt-regole-riordino").on('click', function(){
        	   acs_show_win_std('Regole riordino forniture', <?php echo acs_url('module/desk_acq/acs_regole_riordino.php') ?>, {from_anag_art : 'Y'}, 600, 600, null, 'icon-shopping_setup-16');	
           });        
        }   

    	//Anagrafica fornitori
        if (Ext.get("bt-anag-forn") != null){
        	Ext.QuickTips.register({
    			target: "bt-anag-forn",
    			title: 'Anagrafica fornitori',
    			text: 'Gestione anagrafica fornitori'
    		});
                    
	        Ext.get("bt-anag-forn").on('click', function(){
	          	acs_show_win_std('Parametri selezione anagrafiche fornitori', '../desk_acq/acs_panel_ins_new_anag_form.php?fn=open_form_filtri', 
	    	        	null, 950, 500, null, 'iconClienti');
	        });
        }  

        if (Ext.get("bt-call-pgm") != null) {

           	Ext.QuickTips.register({
    			target: "bt-call-pgm",
    			title: 'Start',
    			text: 'Avvio elaborazioni non interattive'
    		});    	 

            Ext.get("bt-call-pgm").on('click', function(){

             	acs_show_win_std('Avvio processi batch', '../base/acs_processi_batch.php?fn=open_tab', 
	    	        	{file_tabelle : <?php echo j($cfg_mod); ?>}, 950, 500, null, 'icon-button_blue_play-16');
         	   
            });       	

         }  
        

    	     
		//---------------------------------------------------------------
		//carico gli indici
		//---------------------------------------------------------------
     		mp_indici = Ext.getCmp('west-panel');
            	

  		//schedulo la richiesta dell'ora ultimo aggiornamento
   		Ext.TaskManager.start(refresh_stato_aggiornamento);	
     		

        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
