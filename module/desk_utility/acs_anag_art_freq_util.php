<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_DeskArt['file_tabelle'],
    
    'TATAID' => 'ANFFU',
    'CL_AF' => 'AFFUTI',
    'descrizione' => 'Gestione anagrafica articoli formati - FREQUENZA UTILIZZO',
    
    'fields_key' => array('TAKEY1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice', 'type' => 'numeric', 'fw' =>'width : 100'),
        'TADESC' => array('label'	=> 'Descrizione')
    ),
    
    'fireEvent' => 'Y'
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
