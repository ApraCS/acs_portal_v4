<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();

    $m_params = acs_m_params_json_decode();
    
    
    if ($_REQUEST['fn'] == 'exe_save_form'){
   
        $data = (array)$m_params->form_values;
        
        $rrn = $data['RRN'];
        $cod_art = $data['AFCMAT'];
        
        unset($data['RRN']);
        unset($data['AFCMAT']);
        
        $field_numerici = array('AFLUNG', 'AFLARG', 'AFQSTO','AFSPES', 'AFPSPE', 'AFMDES', 
                                'AFMSOP', 'AFMSIN', 'AFMSOT', 'AFRMLU', 'AFRMLA',
                                'AFFUTI', 'AFDIMM', 
          
                                
        );
        foreach($field_numerici as $f){
            if(strlen($data[$f]) == 0)
                $data[$f] = 0;
            $data[$f] = sql_f($data[$f]);
        }
     

      
        if(isset($rrn) && strlen($rrn)>0){
             
            $data['AFUSUM'] = $auth->get_user();
            $data['AFDTUM'] = oggi_AS_date();
            $data['AFORUM'] = oggi_AS_time();

            $sql = "UPDATE {$cfg_mod_DeskArt['file_anag_art_formati']} AF
                    SET " . create_name_field_by_ar_UPDATE($data) . "
                    WHERE RRN(AF) = '{$rrn}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
       
            $result = db2_execute($stmt, $data);
            echo db2_stmt_errormsg($stmt);
            
            
        }else{
            
          
            $chiave = array('AFDT' => $id_ditta_default, 
                            'AFCMAT' => $cod_art,
                            'AFUSGE' => $auth->get_user(),
                            'AFDTGE' => oggi_AS_date(),
                            'AFORGE' => oggi_AS_time(),
                            'AFUSUM' => $auth->get_user(),
                            'AFDTUM' => oggi_AS_date(),
                            'AFORUM' => oggi_AS_time(),
               
            );
            
            $data = array_merge($data, $chiave);
            
                      
            $sql = "INSERT INTO {$cfg_mod_DeskArt['file_anag_art_formati']}(" . create_name_field_by_ar($data) . ") VALUES (" . create_parameters_point_by_ar($data) . ")";
            $stmt = db2_prepare($conn, $sql);
            
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $data);
            echo db2_stmt_errormsg($stmt);
            
            
            
        }
        
        
        $sql = "SELECT RRN(AF) AS RRN, AF.*, ARART, ARDART
                FROM {$cfg_mod_DeskArt['file_anag_art_formati']} AF
                INNER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR
                ON AR.ARDT = AF.AFDT AND AR.ARART = AF.AFCMAT
                WHERE AFDT = '{$id_ditta_default}' AND AFCMAT = '{$cod_art}' 
                ";
      
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        $row = db2_fetch_assoc($stmt);
        $row = gest_row($row);
       
        $rec = array();
        $rec['data'] = $row['AFDTUM'];
        $rec['ora'] = $row['AFORUM'];
        $rec['utente'] = $row['AFUSUM'];
        $rec['RRN'] = $rrn;
        $rec[] = $row;
        
      /*  if(strlen(trim($row['AFUSUM'])) == 0){
            
            $rec = array();
            $rec['data'] = $row['AFDTGE'];
            $rec['ora'] = $row['AFORGE'];
            $rec['utente'] = $row['AFUSGE'];
            $rec['RRN'] = $row['RRN'];
            $rec[] = $row;
            
        }else{
            
            $rec = array();
            $rec['data'] = $row['AFDTUM'];
            $rec['ora'] = $row['AFORUM'];
            $rec['utente'] = $row['AFUSUM'];
            $rec['RRN'] = $rrn;
            $rec[] = $row;
            
        }*/
     
        $ret['success'] = true;
        $ret['record'] = $rec;
        echo acs_je($ret);
        exit;
    
    }
    
    
    if ($_REQUEST['fn'] == 'open_f'){
    
    $sql = "SELECT RRN(AF) AS RRN, AF.*, ARART, ARDART, ARSGME
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art_formati']} AF
            ON AR.ARDT = AF.AFDT AND AR.ARART = AF.AFCMAT
            WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$_REQUEST['m_art']}' ";
  
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $row = db2_fetch_assoc($stmt);
    
    if(trim($row['ARART']) == trim($row['AFCMAT'])){
        
        $dati = array(
            'utente' => $row['AFUSUM'],
            'data' => $row['AFDTUM'],
            'ora' => $row['AFORUM']
        );
        
        
        $row = gest_row($row);
        $rec = array_merge($row, $dati);
        
    }else{
        
        $rec = array('AFDT' =>$id_ditta_default, 
                     'AFCMAT' => $_REQUEST['m_art'],
                     'ARART' =>  $_REQUEST['m_art'],
                     'ARDART' => $row['ARDART'],
                     'ARSGME' => $row['ARSGME'],
                      'RRN' =>''
                     
                     
        );
    }
   

    $ret = array();
    $ret['success'] = true;
    $ret['record'] = $rec;
    echo acs_je($ret);
    exit;
    }
  
    if ($_REQUEST['fn'] == 'get_ta'){
        
        $m_params = acs_m_params_json_decode();
        
        $data = $desk_art->find_TA_std($m_params->tataid, null, 'N', 'N', null, null, null, 'N', 'Y');

        $ret = array();
        $ret['success'] = true;
        $ret['data'] = $data;
        echo acs_je($ret);
        exit;
    }
    
    
    function gest_row($row){
        //BORDO
        $row['AFBSPE'] = trim($row['AFBSPE']);
        $row['AFBCOL'] = trim($row['AFBCOL']);
        $row['AFBRET'] = trim($row['AFBRET']);
        $row['AFBTIP'] = trim($row['AFBTIP']);
        $row['AFBTON'] = trim($row['AFBTON']);
        $row['AFCOLB'] = trim($row['AFCOLB']);
        $row['AFBPEL'] = trim($row['AFBPEL']);
        // PELLICOLA
        $row['AFCDEC'] = trim($row['AFCDEC']);
        $row['AFVENA'] = trim($row['AFVENA']);
        $row['AFTMAT'] = trim($row['AFTMAT']);
        $row['AFTPIL'] = trim($row['AFTPIL']);
        $row['AFPELL'] = trim($row['AFPELL']);
        $row['AFTSUP'] = trim($row['AFTSUP']);
        $row['AFTUTI'] = $row['AFTUTI'];
        
        return $row;
    }
    
