<?php
require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$m_DeskArt = new DeskArt();

$m_params = acs_m_params_json_decode();

require_once "acs_anag_art_schede_agg_{$m_params->tipo_scheda}.php";

if ($_REQUEST['fn'] == 'exe_mod_scheda'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    $articolo = $m_params->c_art;
  
    $ar_upd = out_ar_ins($form_values, $articolo);
    $ar_upd['TADTUM'] = oggi_AS_date();
    $ar_upd['TAUSUM'] = $auth->get_user();
    $ar_upd['TANR']   = sprintf("%-3s", $form_values->riga);
    
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_tab_sys']} TA
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(TA) = '{$form_values->rrn}'";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
   
    if($m_params->tipo_scheda == 'BBPR'){
        
        
        $sh = new SpedHistory($m_DeskArt);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> "UPDATE_{$m_params->tipo_scheda}",
                "vals" => array(
                    "RICDNEW"    => $m_params->c_art,
                    "RICLME"     => $form_values->riga,
                    "RIPLOR"     => $form_values->qta_max,
                    "RIPNET"     => $form_values->qta_min,
                    "RIFG01"     => $form_values->blocco,
                    "RIFG02"     => $form_values->tot_ric,
                    "RIFOR1"     => $form_values->data_in,
                    "RIFOR2"     => $form_values->data_fin
                    
                )
                )
            );
        
        $sh = new SpedHistory($m_DeskArt);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'BBPR_MAIL',
                "vals" => array(
                    "RICDNEW"     => $m_params->c_art,
                    "RICLME"     => $form_values->riga,
                    "RINOTE"     => $form_values->mail_soglia,
                    "RIDART"     => $form_values->mail_limite,
                )
            )
            );
        
    }
 
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $form_values = $m_params->form_values;
    $articolo = $m_params->c_art;
    //unset($form_values->rrn);
       
    $tanr = sprintf("%03s", $s->next_num('BBPR_TANR'));
    
    $ar_ins = out_ar_ins($form_values, $articolo);
    $ar_ins['TAUSGE'] 	= $auth->get_user();
    $ar_ins['TADTGE']   = oggi_AS_date();
    $ar_ins['TADTUM']   = oggi_AS_date();
    $ar_ins['TAUSUM']   = $auth->get_user();
    $ar_ins['TADT']     = $id_ditta_default;
    $ar_ins['TAID']     = $m_params->tipo_scheda;
    $ar_ins['TANR']     = $tanr;
  
    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_tab_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    
    if($m_params->tipo_scheda == 'BBPR'){
    
        $sh = new SpedHistory($m_DeskArt);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> "INSERT_{$m_params->tipo_scheda}",
                "vals" => array(
                    "RICDNEW"    => $m_params->c_art,
                    "RICLME"     => $tanr,
                    "RIPLOR"     => $form_values->qta_max,
                    "RIPNET"     => $form_values->qta_min,
                    "RIFG01"     => $form_values->blocco,
                    "RIFG02"     => $form_values->tot_ric,
                    "RIFOR1"     => $form_values->data_in,
                    "RIFOR2"     => $form_values->data_fin
                    
                )
                )
            );
        
        $sh = new SpedHistory($m_DeskArt);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'BBPR_MAIL',
                "vals" => array(
                    "RICDNEW"     => $m_params->c_art,
                    "RICLME"     => $tanr,
                    "RINOTE"     => $form_values->mail_soglia,
                    "RIDART"     => $form_values->mail_limite,
                )
            )
            );
    }
    

    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc_scheda'){
    
    $form_values = $m_params->form_values;
   
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA WHERE RRN(TA) = '{$form_values->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);

    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
  
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    $ar = array();
    
    $c_art = sprintf("%-15s", $m_params->open_request->c_art);
    $tipo_scheda = $m_params->tipo_scheda;
    
    
    $sql = "SELECT RRN(TA) AS RRN, TA.*
            FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}' AND TAID = '{$tipo_scheda}' AND 
            SUBSTR(TAREST, 25, 15) = '{$c_art}'";
     
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
       
    while($row = db2_fetch_assoc($stmt)){
  
        $nr = get_array_data($row);
        $ar[] = $nr;
        
    }
    
    
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
				xtype: 'panel',
			title: '',
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},					
						items: [
						
							{
						xtype: 'grid',
						title: '',
						itemId : 'm_grid',
						flex:0.7,
				        loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad:true,
							      proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							       extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>,
										 tipo_scheda : <?php echo acs_je($m_params->tipo_scheda) ?>
			        				},
			        				
			        			   doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: <?php echo out_fields(); ?>
					}, //store
				
			      columns: [<?php echo out_columns(); ?>]
			     
			      , listeners: {
	         
	         
	          			selectionchange: function(selModel, selected) { 
	               
            	               if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            	                   form_dx.getForm().setValues(selected[0].data);
	                 			}
		       		  	 
		       		  	      }
	         			,itemclick: function(view,rec,item,index,eventObj) {
	
			     					  this.up('panel').down('#save').enable();							        	
			        	 }
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}, //fine grid
		
		{
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettagli',
 		            //bodyStyle: 'padding: 10px',
		            bodyPadding: '5 0 0 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [  <?php echo out_component_form()?> ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: ['->',
			     {
                     xtype: 'button',
                    text: 'Salva',
                    itemId : 'save',
                    disabled: true,
		            scale: 'small',	                     
					iconCls: 'icon-save',
		           	handler: function() {
		               		var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_scheda',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				tipo_scheda : <?php echo acs_je($m_params->tipo_scheda); ?>,
			        				c_art : <?php echo j($m_params->c_art); ?>
			        				
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.getStore().load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     },  
			      {
                     xtype: 'button',
                    text: 'Crea',
		            scale: 'small',	                     
					iconCls: 'icon-user-add',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				tipo_scheda : <?php echo j($m_params->tipo_scheda); ?>,
	        				c_art : <?php echo j($m_params->c_art); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        grid.getStore().load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     }, {
                     xtype: 'button',
                    text: 'Reset',
		            scale: 'small',	                     
					iconCls: 'icon-reset',
		            handler: function() {
		            	 this.up('form').getForm().reset();
	       			     }

			     }, {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-delete',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
        		
        				Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_scheda',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values,
										tipo_scheda : <?php echo acs_je($m_params->tipo_scheda); ?>
									},							        
							        success : function(response, opts){
							        	form.getForm().reset();
							       		grid.getStore().load();
							       		
								 
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				        }

			     }
			      
			     ]
		   }]
			 	 }

				
					
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}


	
