<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();
$main_module =  new DeskArt();
$cfg_mod = $main_module->get_cfg_mod();


$m_table_config = array(
    'module'      => $main_module,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    'descrizione' => "Gestione tabella parametri",
    'form_title' => "Dettagli tabella parametri",
    'fields_preset' => array(
        'TATAID' => 'BATCP',
        'TAKEY1' => $m_params->open_request->takey1
    ),
    
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
	
    'fields_key' => array('TAKEY1', 'TAKEY2', 'TADESC'),
    'fields_grid' => array('TAKEY1', 'TAKEY2', 'TADESC', 'TAMAIL',  'immissione'),
    'fields_form' => array('TAKEY1', 'TAKEY2', 'TADESC', 'TAMAIL', 'TAFG01',  'TARIF1', 'TAFG02', 'TAPROV', 'TACOGE', 'TAFG03', 'TATELE', 'TAASPE', 'TASITI', 'TACAP', 'TARIF2', 'TATISP'),
    
	'fields' => array(				
	    'TAKEY1' => array('label'	=> 'Batch', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10, 'value' => $m_params->takey1),
	    'TAKEY2' => array('label'	=> 'Parametro', 'maxLength' => 10, 'fw'=>'width: 170'),
	    'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 100),
	    'TAFG01' => array('label'	=> 'Tipo campo', 'type' => 'from_TA', 'TAID' => 'BTYPE'),
	    'TARIF1' => array('label'	=> 'Formato'),
	    'TAFG02' => array('label'	=> 'Obbligatorio', 'type' => 'from_TA', 'TAID' => 'ARY'),
	    'TAPROV' => array('label'	=> 'Campo lettura', 'maxLength' => 2),
	    'TACOGE' => array('label'	=> 'Tabella', 'maxLength' => 5),
	    'TAFG03' => array('label'	=> 'Mod. art', 'maxLength' => 1),
	    'TATELE' => array('label'	=> 'TACOR1', 'maxLength' => 20),
	    'TAASPE' => array('label'	=> 'TACOR2', 'maxLength' => 10),
	    'TASITI' => array('label'	=> 'Sequenza', 'maxLength' => 5),
	    'TACAP'  => array('label'	=> 'Campo RI', 'maxLength' => 10),
	    'TAMAIL' => array('label'	=> 'Note',  'maxLength' => 100),
	    'TARIF2' => array('label'	=> 'Posizione su WPI0PI0'),
	    'TATISP' => array('label'	=> 'Campo in linea con'),
	    //immissione
	    'immissione' => array(
	        'type' => 'immissione', 'fw'=>'width: 70',
	        'config' => array(
	            'data_gen'   => 'TADTGE',
	            'user_gen'   => 'TAUSGE'
	        )
	        
	    ),
	    'TADTGE' => array('label'	=> 'Data generazione'),
	    'TAUSGE' => array('label'	=> 'Utente generazione')
	    
	)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
