<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$cfg = "<img src=" . img_path("icone/48x48/game_pad.png") . " height=20>"; 	
$barcode = "<img src=" . img_path("icone/48x48/barcode.png") . " height=20>";
$seq = "<img src=" . img_path("icone/48x48/button_black_play.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'PUMO',
    'title_grid' => 'PUMO-Modelli di configurazione',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'esec' => array('label'	=> 'Esecuzione immediata', 'width' => 200, 'only_view' => 'F', 'maxLength' => 1, 'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
        'prior' => array('label'	=> 'Priorit&agrave; default', 'width' => 150, 'only_view' => 'F', 'maxLength' => 1),
        'gen_conf' => array('label'	=> 'Gen. conf. riga da *I', 'width' => 200, 'only_view' => 'F', 'maxLength' => 1, 'xtype' => 'combo_tipo', 'tab_std' => 'ARN'),
        'fine_conf' => array('label'=> 'Fine config. da pag.', 'width' => 150, 'only_view' => 'F', 'maxLength' => 1, 'maskRe' => '/[1-9]/'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'MODE'   => array('label'	=> $cfg, 'c_width' => 40, 'only_view' => 'C', 'tooltip' => 'Distinta configuratore variabili'),
        'PUSX'   => array('label'	=> $barcode, 'only_view' => 'C', 'c_width' => 40, 'tooltip' => 'Stringhe analisi codice articolo'),
        'PUSQ'   => array('label'	=> $seq, 'only_view' => 'C', 'c_width' => 40, 'tooltip' => 'Sequenze esecuzione configuratore'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
        
    ),
    'title_form' => 'Dettagli tabella modelli di configurazione',
    
    'j_mode' => 'Y',
    'j_pusx' => 'Y',
    'j_pusq' => 'Y',
    'TAREST' => array(
        'esec' 	=> array(
            "start" => 51,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'prior' => array(
            "start" => 52,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'gen_conf' 	=> array(
            "start" => 54,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'fine_conf' 	=> array(
            "start" => 55,
            "len"   => 1,
            "riempi_con" => ""
        )
        
    )

    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';
