<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$oggi = oggi_AS_date();

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_crea_segnalazione_arrivi'){
    
    $m_params = acs_m_params_json_decode();
    
    foreach ($m_params->list_selected_id as $v){
    
        //creo la riga in WPI2AS0
        $na = new SpedAssegnazioneArticoli($desk_art);
        $na->crea_std(array(
            'k_ordine' => $v,
            'form_values' => (array)$m_params->form_values
        ));
        
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

//elenco possibili utenti destinatari
$user_to = array();
$users = new Users;
/* TO DO: sto passando BDSOLLE per recuperare gli utenti a cui associare l'ttivita'. Non sarebbe giusto */
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");

$ar_users_json = acs_je($user_to);


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [ 
                	{
						name: 'f_causale',
						xtype: 'combo',
						fieldLabel: 'Causale',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: false,							
					    value: <?php echo j($m_params->tipo_op) ?>,							
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php
							     if($m_params->attav == 'Y'){
							         $funzioni = $desk_art->find_TA_std('ATFUN', 'CHK_DB', 'Y');
							         $atfta = $desk_art->find_TA_std('ATFTA', $funzioni[0]['id'], 'Y', 'Y'); 
							         foreach($atfta as $op) {
							             echo acs_ar_to_select_json($desk_art->find_TA_std('ATTAV', $op['TAKEY2'], 'Y'), "");
							         }
							     }else{
							         
							         echo acs_ar_to_select_json($desk_art->find_TA_std('ATTAV', $m_params->tipo_op, 'Y'), "");
							         
							     }
							     
							     ?>	
							    ] 
						}						 
					}, {
						name: 'f_note',
						xtype: 'textfield',
						fieldLabel: 'Note',
					    maxLength: 100,
					    value : 'Distinta ' + <?php echo j($m_params->p_art); ?>,
					    						
					}, {
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente assegnato',
			            queryMode: 'local',
			            selectOnTab: false,
			            name: 'f_utente_assegnato',
			            allowBlank: false,
						forceSelection: true,			            
			            value: '<?php echo $v_utente_assegnato; ?>'
			        }, {
						xtype: 'checkboxgroup',
						fieldLabel: 'Notifica assegnazione',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_notifica_assegnazione' 
                          , boxLabel: 'Si'
                          , inputValue: 'Y'
                        }]							
					}, {
						xtype: 'checkboxgroup',
						fieldLabel: 'Notifica rilascio',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_notifica_rilascio' 
                          , boxLabel: 'Si'
                          , inputValue: 'Y'
                        }]							
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Attiva dal'
					   , name: 'f_attiva_dal'
					   , format: 'd/m/Y'
					   , value: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'
					   , minValue: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   , width: 240, anchor: 'none'
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Scadenza'
					   , name: 'f_scadenza'
					   , format: 'd/m/Y'
					   , minValue: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: true
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   , width: 240, anchor: 'none'
					}, {
			            xtype: 'textareafield',
			            grow: true,
			            name: 'f_memo',
			            fieldLabel: 'Memo',
			            anchor: '100%',
			            height: 200
			            
			        }
				],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-24', scale: 'medium',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
	            	var loc_win = this.up('window');

					if(form.isValid()){	
					
					list_selected_row = <?php echo acs_je($m_params->list_selected_id); ?>;
	                    			
					list_selected_id = [];
					for (var i=0; i<list_selected_row.length; i++) 
						list_selected_id.push(list_selected_row[i]);
					
					Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_crea_segnalazione_arrivi',
 						        timeout: 2400000,
 						        method     : 'POST',
 			        			jsonData: {
 			        			    form_values : form_values,
 			        			    list_selected_id : list_selected_id,
 			        			    p_art : <?php echo j($m_params->p_art); ?>
 								},							        
 						        success : function(result, request){
 						          /*if (typeof(window.id_selected)!=='undefined')				                            
						                      for (var i=0; i<window.id_selected.length; i++){
						                      	//per i record che erano selezionato verifico se hanno 
						                      	//adesso il flag di rilasciato (e devo sbarrarli)
						                        
												Ext.Ajax.request({
													url: 'acs_panel_todolist.php?fn=exe_richiesta_record_rilasciato',
											        jsonData: {prog: id_selected[i].get('prog')},
											        method     : 'POST',
											        waitMsg    : 'Data loading',
											        success : function(result, request){
											        	this.set('flag', Ext.decode(result.responseText).ASFLRI);    										        	
											        }, scope: id_selected[i],
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });					                        
						                      }*/
 						        
 						    		loc_win.fireEvent('afterInsertRecord', loc_win); 			            			
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });  
				            
				    }            	                	                
	            }
	        }],             
				
        }
]}