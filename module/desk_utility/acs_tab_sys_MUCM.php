<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'MUCM',
    'title_grid' => 'MUCM-Classe merceologica',
    'fields' => array(
        'TAID'   => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'RRN'    => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
       // 'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli classe merceologica',
   // 'tab_tipo' => 'PUSQ',

);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

