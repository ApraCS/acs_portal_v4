<?php
require_once "../../config.inc.php";
require_once "acs_gestione_tabelle.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_assegna_RGB'){
    
    $ar_upd = array();
    $rgb = $m_params->form_values->f_red.$m_params->form_values->f_green.$m_params->form_values->f_blue;
    $az_rgb   = $m_params->form_values->f_az_rgb;
    $ciclo    = $m_params->form_values->f_ciclo;
    $az_ciclo = $m_params->form_values->f_az_ciclo;
    
    foreach($m_params->list_selected_id as $v){
        
        $ar_upd['TAUSUM'] 	= $auth->get_user();
        $ar_upd['TADTUM']   = oggi_AS_date();
       
        $value_attuale = $v->tarest;
        
        if(trim($value_attuale) == "") $value_attuale = sprintf("%-248s", "");
        
        if(trim($rgb) != ''){
            $chars = $rgb;
            $len = "%-9s";
            $chars = substr(sprintf($len, $chars), 0, 9);
            $new_value = substr_replace($value_attuale, $chars, 84, 9);
            $value_attuale = $new_value;
        }
        if($az_rgb == 'Y'){
            $chars = substr(sprintf("%-9s", ""), 0, 9);
            $new_value = substr_replace($value_attuale, $chars, 84, 9);
            $value_attuale = $new_value;
            
        }
        
        if(trim($ciclo) != ''){
            $chars_ciclo = $ciclo;
            $len_chars = "%-1s";
            $chars_ciclo = substr(sprintf($len_chars, $chars_ciclo), 0, 1);
            $new_value = substr_replace($value_attuale, $chars_ciclo, 127, 1);
            $value_attuale = $new_value;
        }
        
        if($az_ciclo == 'Y'){
            $chars_ciclo = substr(sprintf("%-1s", ""), 0, 1);
            $new_value = substr_replace($value_attuale, $chars_ciclo, 127, 1);
            $value_attuale = $new_value;
            
        }
        
        $tarest = $value_attuale;
        $ar_upd['TAREST'] 	= $tarest;
                
        $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN' AND TANR = '{$v->cod_var}'
                AND TACOR2 = '{$v->cod_val}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        
        
    }
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

if ($_REQUEST['fn'] == 'exe_sospendi_attiva'){
    
    $ar_upd = array();
     
    foreach($m_params->list_selected_id as $k => $v){
                
        $tatp = trim($v->sosp);
        
        if($tatp == "")
            $new_value = "S";
        else
            $new_value = "";
                 
        $ar_upd['TAUSUM'] = $auth->get_user();
        $ar_upd['TADTUM'] = oggi_AS_date();
        $ar_upd['TATP']   = $new_value;
        
        $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN' AND TANR = '{$v->cod_var}'
                AND TACOR2 = '{$v->cod_val}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        
        
    }
   
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}

if ($_REQUEST['fn'] == 'exe_assegna_ciclo'){
    
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $ar_upd = array();
    
    $da = $m_params->da;
    $a = $m_params->a;
    
    foreach($m_params->list_selected_id as $k => $v){
        
        if($k >= $da && $k<= $a){
            $ar_upd['TAUSUM'] 	= $auth->get_user();
            $ar_upd['TADTUM']   = oggi_AS_date();
            
            
            $value_attuale = $v->TAREST;
            if(trim($value_attuale) == "") $value_attuale = sprintf("%-248s", "");
            $chars = $m_params->ciclo;
            $len = "%-1s";
            $chars = substr(sprintf($len, $chars), 0, 1);
            $new_value = substr_replace($value_attuale, $chars, 127, 1);
            $tarest = $new_value;
            $ar_upd['TAREST'] 	= $tarest;
            
            $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(TA) = '{$v->RRN}'";
                    
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd);
        }
        
    }
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'exe_assegna_tag'){
    
   $m_params = acs_m_params_json_decode();
   $ar_upd = array();
    
   foreach($m_params->list_selected_id as $v){
       
       $ar_upd['TAUSUM'] 	= $auth->get_user();
       $ar_upd['TADTUM']   = oggi_AS_date();
       
       
       $value_attuale = $v->TAREST;
       if(trim($value_attuale) == "") $value_attuale = sprintf("%-248s", "");
       $chars = $m_params->tag;
       $len = "%-5s";
       $chars = substr(sprintf($len, $chars), 0, 5);
       $new_value = substr_replace($value_attuale, $chars, 97, 5);
       $tarest = $new_value;
       $ar_upd['TAREST'] 	= $tarest;
       
       $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    	        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    	        WHERE RRN(TA) = '{$v->RRN}'";
       
       $stmt = db2_prepare($conn, $sql);
       echo db2_stmt_errormsg();
       $result = db2_execute($stmt, $ar_upd);
       
       
   }
            
   $ret = array();
   $ret['success'] = true;
   echo acs_je($ret);
   exit;
 
}


if ($_REQUEST['fn'] == 'get_json_data'){
    
    $descrizione = $m_params->descrizione;
    $codice = $m_params->codice;
    $tanr = $m_params->tanr;
    $sql_where = "";
    
    if($m_params->from_tipo == 'Y'){
        $sql_where = " AND (";
        for($i = 10; $i<= 37; $i+=3){
            $sql_where .= " (SUBSTRING(TAREST, {$i}, 3) = '{$tanr}') OR";
        }
        $sql_where .= " 1=2 )";
     }else{

        
        if(trim($descrizione) != '')
            $sql_where .= " AND UPPER(TADESC)  LIKE '%" . strtoupper($descrizione) . "%'";
            
        if(trim($codice) != '')
            $sql_where .= " AND UPPER(TANR)  LIKE '%" . strtoupper($codice) . "%'";
      
        
    }

    $sql = "SELECT *
            FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN' AND  TALINV = '' {$sql_where}
            ";
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        
        $nr['cod_var']    = trim($row['TANR']);
        $nr['desc_var']   = $row['TADESC'];
        $nr['cod_val']    = trim($row['TACOR2']);
        $val = get_TA_sys('PUVR', trim($row['TACOR2']), null, null, null, null, 0, '', 'N', 'Y');
        $nr['desc_val']   = $val['text'];
        $nr['sosp']       = $row['TATP'];
        $nr['ciclo']      = substr($row['TAREST'], 127, 1);
        $nr['red']        = substr($row['TAREST'], 84, 3);
        $nr['green']      = substr($row['TAREST'], 87, 3);
        $nr['blue']       = substr($row['TAREST'], 90, 3);
        if(trim($nr['red']) != '')
            $nr['t_rgb']      = "RGB: {$nr['red']}.{$nr['green']}.{$nr['blue']}";
        $nr['tarest']     = $row['TAREST'];
        $nr['TADTGE']     = $row['TADTGE'];
        $nr['TADTUM']     = $row['TADTUM'];
        $nr['TAUSGE']     = $row['TAUSGE'];
        $nr['TAUSUM']     = $row['TAUSUM'];
        
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}


if ($_REQUEST['fn'] == 'open_form'){
?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
				flex:1,	           
	            items: [
				
				{ xtype : 'textfield',
				  fieldLabel  : 'Descrizione variante',
				  labelWidth : 120,
				  name : 'f_desc',
				  width : 350,
				  maxLength : 30
				
				}, { xtype : 'textfield',
				  fieldLabel  : 'Codice variante',
				  labelWidth : 120,
				  name : 'f_cod',
				  width : 180,
				  maxLength : 3
				
				}
						
			 ], buttons: [					
					{
			            text: 'Cerca',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                var win = this.up('window');
			            	var form = this.up('form');
			            	var form_values = form.getForm().getValues();
			            	var loc_win = this.up('window');
			            				            	
			            	acs_show_win_std('Ricerca per denominazione variante', 'acs_tab_sys_ricerca_PUVN.php?fn=open_tab', {form_values : form_values, grid_id : <?php echo j($m_params->grid_id); ?>}, 800, 500, null, 'icon-search-16');
			                win.close();
			                
			            }
			        } 
		        
		        
		        ] 
				
	}
	
]}


<?php

exit;
}


if ($_REQUEST['fn'] == 'open_tab'){
 
?>

{"success":true, "items": [

    
						{
						xtype: 'grid',
						title: '',
				        loadMask: true,
				        autoScroll : true,
				        multiSelect : true, 
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							        actionMethods: {
							          read: 'POST'
							        },
							        extraParams: {
										 descrizione : <?php echo j($m_params->form_values->f_desc); ?>,
										 codice : <?php echo j($m_params->form_values->f_cod); ?>,
										 from_tipo : <?php echo j($m_params->from_tipo); ?>,
										 tanr : <?php echo j($m_params->tanr); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['cod_var', 'desc_var', 'cod_val', 'desc_val', 'sosp', 'ciclo', 'rgb', 
		        			         'red', 'green', 'blue', 'tarest', 't_rgb', 'TADTGE', 'TADTUM', 'TAUSGE', 'TAUSUM']							
									
			}, //store
			
			<?php $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
			
			      columns: [
			       {text: '<?php echo $sosp; ?>', 	
    				width: 30, 
    				dataIndex: 'sosp',
    				tooltip: 'Sospeso',		        			    	     
    		    	renderer: function(value, metaData, record){
		    			  	if(record.get('sosp') == 'S'){ 
    		    			   metaData.tdCls += ' sfondo_rosso';
    		    			   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Sospeso') + '"';
    		    			   return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
    		    		 	}else{
    		    		 	
		    		 		  if (record.get('ciclo') == 'E'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In esaurimento') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/clessidra.png") ?> width=15>';}
							  if (record.get('ciclo') == 'A'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In avviamento') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';}
				    		  if (record.get('ciclo') == 'C'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In codifica') + '"';
					   			return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=15>';}
    		    		 	  if (record.get('ciclo') == 'R'){	    			    	
					   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Riservato') + '"';
					   			return '<img src= <?php echo img_path("icone/48x48/folder_private.png") ?> width=15>';}
				   			  if (record.get('ciclo') == 'F'){	    			    	
			   		   			metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Fine codifica') + '"';
			   		   			return '<img src= <?php echo img_path("icone/48x48/button_blue_play.png") ?> width=15>';}
    		    		 	}
    		    		 
    		    		  }
    		        },	
			
	               {
	                header   : 'Cod.',
	                dataIndex: 'cod_val',
	                filter: {type: 'string'}, filterable: true,
	                width : 50
	                }, 
	                
	                {
	                header   : 'Descrizione variabile',
	                dataIndex: 'desc_val',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                }, {
	                header   : 'Cod.',
	                dataIndex: 'cod_var',
	                filter: {type: 'string'}, filterable: true,
	                width : 50
	                }, 
	                {
	                header   : 'RGB',
	                dataIndex: 'rgb',
	                filter: {type: 'string'}, filterable: true,
	                width : 35,
	                renderer: function(value, metaData, record){
   							var red = record.get('red');
                            var green = record.get('green');
                            var blue = record.get('blue');
                            metaData.style += 'background-color: rgb(' + red + ', '+ green + ', ' + blue + ');';
                            if(record.get('t_rgb').trim() != '')
		    		 	    	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_rgb')) + '"';    			    	
		    			  	return '';
		    		  }
	                }, 
	                {
	                header   : 'Descrizione variante',
	                dataIndex: 'desc_var',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                },
	               {header: 'Immissione',
                	 columns: [
                	 {header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
                	 renderer: function(value, metaData, record){
                	         if (record.get('TADTGE') != record.get('TADTUM')) 
                	          metaData.tdCls += ' grassetto';
                	 
                             q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
                			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
                		     return date_from_AS(value);	
                	}
                	 
                	 }
            	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
            	  renderer: function(value, metaData, record){
            	  
            	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
            	          metaData.tdCls += ' grassetto';
            	  
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return value;	
            	} 
            	 }
            	 ]}
	                
	         ], listeners: {
	         
	         	celldblclick: {								
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				      col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  rec = iView.getRecord(iRowEl);
					  var loc_win = this.up('window');
					  
					  if(col_name == 'desc_val')
					  	  acs_show_panel_std('acs_tab_sys_PUVN.php?fn=open_panel', 'panel_gestione_tabelle', {tacor2: rec.get('cod_val'), d_cod : rec.get('desc_val') });	          	        	          
											  
					  if(col_name == 'desc_var')
					  	  acs_show_win_std('Tabella variante [' + rec.get('cod_var') +'] ' + rec.get('desc_var'), 'acs_tab_sys_PUVN.php?fn=open_panel', {autoload : 'firstRec', hide_grid : 'Y', tanr : rec.get('cod_var'), tacor2: rec.get('cod_val')}, 410, 580, null, 'icon-design-16');         	          	          	      
					  
					  /*var grid_id = <?php echo j($m_params->grid_id); ?>;
					  var grid_origine = Ext.getCmp(grid_id);
					  var rec_index = grid_origine.getStore().findRecord('TANR', rec.get('cod_val'));
					  grid_origine.getView().select(rec_index);
					  grid_origine.getView().focusRow(rec_index);
					  loc_win.destroy();*/  						  
							 
						  }
			   		  },
			   		  
			   		  
			   		  
			   		  itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     var loc_win = this.up('window')
					     
					     id_selected = grid.getSelectionModel().getSelection();
					     		list_selected_id = [];
					     		for (var i=0; i<id_selected.length; i++){
					     			list_selected_id.push({
					     			   cod_val : id_selected[i].get('cod_val'),
					     			   desc_val : id_selected[i].get('desc_val'),
					     			   cod_var : id_selected[i].get('cod_var'),
					     			   desc_var : id_selected[i].get('desc_var'),
					     			   sosp : id_selected[i].get('sosp'),
					     			   tarest : id_selected[i].get('tarest')});
								}
						
								
						voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : 'acs_tab_sys_ricerca_PUVN.php?fn=exe_sospendi_attiva',
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id : list_selected_id
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
		    				voci_menu.push({
    		         		text: 'Genera nuove varianti',
    		        		iconCls : 'icon-button_blue_play-16',          		
    		        		handler: function () {
	        		   
	        		   			var my_listeners = {
			    		  			afterConfirm: function(from_win, n_value){
			    		  			     from_win.close();
			    		  			  }
				    		   };
	        		   
	        		   			acs_show_win_std('Genera nuove varianti', 'acs_tab_sys_genera_PUVN.php?fn=open_form', {list_selected_id : list_selected_id}, 1100, 300, my_listeners, 'icon-button_blue_play-16');   	    
													   	     
    			              
    						   }
		    				});
								
					     
					     voci_menu.push({
        		         		text: 'Aggiorna RGB/Ciclo vita',
        		        		iconCls : 'icon-pencil-16',          		
        		        		handler: function () {
		        		   
		        		   var my_listeners = {
    			    		  			afterConfirm: function(from_win){
    			    		  			    grid.getStore().load();
    			    		  			    from_win.close();
    			    		  			  }
    				    		   };
		        		   
		        		   			acs_show_win_std('Aggiorna varianti', 'acs_tab_sys_ricerca_PUVN.php?fn=open_form_RGB', {list_selected_id : list_selected_id}, 300, 150, my_listeners, 'icon-pencil-16');   	    
								    	   	     
        			              
        						   }
    		    				});
		    		  
		    		  
		    		  		
						if(list_selected_id.length == 1){
						
							voci_menu.push({
    			         					text: 'Configurazione variante',
    			        					iconCls : 'icon-game_pad-16',             		
    			        					handler: function () {
    			        					
    			        					    var title = 'Configurazione [' +rec.get('cod_val') + ': ' + rec.get('cod_var') + '] ';
    			        				
    			        				    	acs_show_panel_std('acs_anag_art_config.php?fn=open_tab', 'panel_configuratore_puvn', {variabile :  rec.get('cod_val'), variante : rec.get('cod_var'), title: title, from_puvn : 'Y'});
    			        						loc_win.destroy(); 	
    			        					
    			            		    }
        			    				});
						
						
						}		
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	         
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
	
	
]}


<?php

exit;
}

if ($_REQUEST['fn'] == 'open_form_ciclo'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	            
	            	{
					    xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						 {
        					name: 'f_ciclo',
        					xtype: 'combo',
        					fieldLabel: 'Ciclo di vita',
        					labelWidth : 110,
        					forceSelection: true,								
        					displayField: 'text',
        					valueField: 'id',								
        					emptyText: '- seleziona -',
        			   		//allowBlank: false,								
        				    anchor: '-15',
        					store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: [								    
        					    <?php echo acs_ar_to_select_json($desk_art->find_TA_std('AR012', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>
        					    ]
        					}
        					
        			     }						
	            
			 ]}
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma',
				        scale: 'large',	                     
						iconCls: 'icon-button_blue_play-32',	            
			            handler: function() {
			                 var form = this.up('form');
			                 var form_values = form.getValues();
			                 var loc_win = this.up('window');
			                 
			                 var list_selected_id =  <?php echo acs_je($m_params->list_selected_id);?> ;
			                 var c_list = list_selected_id.length;
			                 var range = Math.ceil(c_list/4); 
			                 Ext.MessageBox.show({
        							   msg: 'Aggiornati 4/' + c_list,
        							   progressText: 'Running...',
        							   width:300,						   
        							   wait:true, 
        							   waitConfig: {interval:500},
        							   icon:'ext-mb-download',
        					  });
			                 
			                 var count = 0;
			                 for (var i=0; i<= c_list-1; i+=4){
			                    count++;
			                 	 if(i <= c_list-1){
			                 	 var da = i;
			                 	 var a = i + 4;
			                 	    Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_ciclo',
								        timeout    : 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id : <?php echo acs_je($m_params->list_selected_id);?>,
					        				ciclo : form_values.f_ciclo,
					        				da : da,
					        				a : a
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
							          	  var text = Ext.MessageBox.msg;
							   				  text.setValue('Aggiornati ' + da + '/' +  c_list);
							   			      if(count == range){
											  	Ext.MessageBox.hide();
											  	loc_win.fireEvent('afterAssegna', loc_win, form_values.f_ciclo);
											  } 
									
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
            			    }
			             }
			               
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php
	
	exit;
}

if ($_REQUEST['fn'] == 'open_form_tag'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	            
	            	{
					    xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						<?php if($cfg_mod_DeskArt['tag_stringa'] == 'Y'){?>
						
						    {
        					name: 'f_tag',
        					xtype: 'textfield',
        					fieldLabel: 'TAG da assegnare',
        					labelWidth : 110,
        					maxLength : 5
        					
        					}
						<?php }else{?>
						
						
        	              {
        					name: 'f_tag',
        					xtype: 'combo',
        					fieldLabel: 'TAG da assegnare',
        					labelWidth : 110,
        					forceSelection: true,								
        					displayField: 'text',
        					valueField: 'id',								
        					emptyText: '- seleziona -',
        			   		//allowBlank: false,								
        				    anchor: '-15',
        					store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: [								    
        					    <?php echo acs_ar_to_select_json($desk_art->find_TA_std('VNTAG', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>
        					    ]
        					}
        					
        			     },{
    						 xtype: 'button',
    						 margin: '0 0 0 5',
    			             scale: 'small',			                 
    			             iconCls: 'icon-gear-16',
    			             iconAlign: 'top',			                
    			             handler : function() {
    									acs_show_win_std('Gestione tag', 'acs_gest_tag.php?fn=open_tab', {}, 600, 500, null, 'icon-gear-16');
    								} //handler function()
    						 
    						 } 
        						
						
						<?php }?>
						
	            
			 ]}
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma',
				        scale: 'large',	                     
						iconCls: 'icon-button_blue_play-32',	            
			            handler: function() {
			                 var form = this.up('form');
			                 var form_values = form.getValues();
			                 var loc_win = this.up('window');
			                 Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_tag',
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id : <?php echo acs_je($m_params->list_selected_id);?>,
					        				tag : form_values.f_tag
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          loc_win.fireEvent('afterAssegna', loc_win, form_values.f_tag);	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}

if ($_REQUEST['fn'] == 'open_form_RGB'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	            
	            	 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						 { xtype : 'textfield',
	          		  	   name : 'f_red',
	          		  	   fieldLabel : 'RGB',
	          		  	   labelWidth : 60,
	          		  	   width : 110,
	          		       maxLength : 3,
	          		       listeners: {
            				'blur': function(field) {
            					var n_value = field.getValue();
            					if(parseInt(n_value) > 255){
            						field.setValue('');	 
            						acs_show_msg_error('Impossibile inserire un valore maggiore di 255');
    					      	    return false;
    					      	}
            				}
        				}
	          		  }, { xtype : 'textfield',
	          		       name : 'f_green',
	          		  	   fieldLabel : '',
	          		       maxLength : 3,
	          		       width : 45,
	          		       listeners: {
            				'blur': function(field) {
            					var n_value = field.getValue();
            					if(parseInt(n_value) > 255){
            						field.setValue('');	 
            						acs_show_msg_error('Impossibile inserire un valore maggiore di 255');
    					      	    return false;
    					      	}
            				}
        				}
	          		  }, { xtype : 'textfield',
	          		       name : 'f_blue',
	          		       fieldLabel : '',
	          		       maxLength : 3,
	          		       width : 45,
	          		       listeners: {
            				'blur': function(field) {
            					var n_value = field.getValue();
            					if(parseInt(n_value) > 255){
            						field.setValue('');	 
            						acs_show_msg_error('Impossibile inserire un valore maggiore di 255');
    					      	    return false;
    					      	}
            				}
        				}
	          		  },
	          		  {
						xtype: 'checkboxgroup',
						fieldLabel: 'Azzera',
						margin : '0 0 0 5',
						labelWidth : 40,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_az_rgb' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					}
	          		  ]},
	               { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
					    {
        					name: 'f_ciclo',
        					xtype: 'combo',
        					labelWidth : 60,
        					width : 200,
        					fieldLabel: 'Ciclo vita',
        					forceSelection: true,								
        					displayField: 'text',
        					valueField: 'id',								
        					emptyText: '- seleziona -',
        			   		//allowBlank: false,	
        			   	   anchor: '-15',
        					store: {
        						editable: false,
        						autoDestroy: true,
        					    fields: [{name:'id'}, {name:'text'}],
        					    data: [						
        					    <?php echo acs_ar_to_select_json($desk_art->find_TA_std('ASSCV', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
        					    ]
        					},
        					
        					queryMode: 'local',
                			minChars: 1, 	
        					listeners: { 
        					 	beforequery: function (record) {
        			         	record.query = new RegExp(record.query, 'i');
        			         	record.forceAll = true;
        		             }
        		          }
        					
        			   },   {
						xtype: 'checkboxgroup',
						fieldLabel: 'Attivo',
						margin : '0 0 0 5',
						labelWidth : 40,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_az_ciclo' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					}
	           		
						]}		  
	          	
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma',
				        scale: 'large',	                     
						iconCls: 'icon-button_blue_play-32',	            
			            handler: function() {
			                 var form = this.up('form');
			                 var form_values = form.getValues();
			                 var loc_win = this.up('window');
			                 Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_RGB',
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id : <?php echo acs_je($m_params->list_selected_id);?>,
					        				form_values : form_values
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          loc_win.fireEvent('afterConfirm', loc_win);	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php
	
	exit;
}
