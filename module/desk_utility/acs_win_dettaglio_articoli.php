<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();

if ($_REQUEST['fn'] == 'open_win'){
    $m_params = acs_m_params_json_decode();

    ?>

{"success":true, "items": [

    {
                xtype: 'tabpanel',
                id: 'tp-dett-art',	//usato per non aprire piu volte la win
                region: 'east',
                title: '',
                margins: '0 5 0 0',
                tabPosition: 'bottom',
                
                original_record: [],
                
                 buttons: [{
			        text: 'Salva',
			        scale: 'small',	                     
			        handler: function() {
			            var v_modified = {};
			            var is_modified = false;
			            var tp = this.up('tabpanel');
			            var fmb = this.up('tabpanel').getActiveTab();
			        	var fm = this.up('tabpanel').getActiveTab().getForm();
			        	//var fs = f.getValues(false, false, true);  <!-- NON RESTITUISCE I NULL  -->			        	
			        	var fs = fm.getFieldValues();
			        	
			        	if (fmb.hasOwnProperty('save_form')){
			        		fmb.save_form(fmb);
			        		return;
			        	}
                        
                        for (var f in fs) {
                                                    
                        	if (fm.findField(f).xtype == 'numberfield'){
                        		var vn = fs[f];
                        		var vo = tp.original_record[f];
                        	
                        	<?php if($m_params->g_multi != 'Y'){?>
                        	    if (Ext.isEmpty(vn)) vn = 0;
                        		if (Ext.isEmpty(vo)) vo = 0;
                        		
                        		vn = parseFloat(vn);
                        		vo = parseFloat(vo);
                        	<?php } ?>
                        		                        		
                        		if (vn != vo){
                        			v_modified[f] = vn;
                        			is_modified = true;
                        		}
                        			
                        } else {
                            	if (fm.findField(f).xtype == 'datefield'){
                            		var vn = fm.findField(f).getSubmitValue();
                            		var vo = tp.original_record[f + '_val'];
                            		
                            		if (Ext.isEmpty(vn)) vn = 0;
                                	if (Ext.isEmpty(vo)) vo = 0;
                            		
                            		vn = parseInt(vn);
                            		vo = parseInt(vo);
									
                            		if (vn != vo){
                            			v_modified[f] = vn;
                            			is_modified = true;
                            		}									
									
								} else {
										
										if (fm.findField(f).xtype == 'combo'){
										
										    var vn = fs[f];
                                    		var vo = tp.original_record[f];
                                    		<?php if($m_params->g_multi != 'Y'){?>
                                    		
                                    			if (Ext.isEmpty(vn)) vn = "";
                                    		    if (Ext.isEmpty(vo)) vo = "";
                                    		
                                    		    vn = vn.rtrim();
                                    		    vo = vo.rtrim();
                                    		<?php }?>
                                    		if (vn != vo){
                                    			v_modified[f] = vn;
                                    			is_modified = true;
                                    		}      
										}else{
										
										  if(f.substring(0, 5) == 'clear'){
                                            console.log(fs[f]);
                                          }else{
                                            var vn = fs[f];
                                    		var vo = tp.original_record[f];
                                    		
                                    		
                                    			if (Ext.isEmpty(vn)) vn = "";
                                    		    if (Ext.isEmpty(vo)) vo = "";
                                    		
                                    		    vn = vn.rtrim();
                                    		    vo = vo.rtrim();
                                    		
                                    		if (vn != vo){
                                    			v_modified[f] = vn;
                                    			is_modified = true;
                                    		} 
                                          
                                          }
										
    										     
										
										}
										
                                		
                                }                  		                        		
                        	}

                        }			        	
			        	
			        	if (is_modified){			        	
	        		        Ext.Ajax.request({
        				   		url: 'acs_anag_art_exe_upd.php?fn=exe_aggiorna_dett_art',
        				   		method: 'POST',
        	        			jsonData: {
        	        			    <?php if($m_params->g_multi == 'Y'){?>
        	        			    	multi_selected_id : <?php echo acs_je($m_params->multi_selected_id); ?>,
        	        			     <?php }else{?>
        	        			    	c_art: tp.original_record['ARART'],
        	        			    <?php }?>
        	        					update_values: v_modified,
        	        					g_multi : <?php echo j($m_params->g_multi); ?>
        	        			},
        				   		success: function(response, opts) {
        				   			try{
	        				      		var src = Ext.decode(response.responseText);
	        				      	} catch(e) {
	        				      		//Ext.Msg.alert('No data to be saved', response.responseText);
	        				      		Ext.Msg.alert('No data to be saved', 'ERRORE IN FASE DI SALVATAGGIO');
	        				      	}
        				      		if(src.success == false){
    	 						        	acs_show_msg_error(src.error_msg);
    	 						        	tp.loadArt(src.row_art.ARART);
    	 						        	return;
     						        }
     						        if (src.success == true) {
     						        	acs_show_msg_info('Aggiornamento eseguito');
     						        	tp.loadArt(src.row_art.ARART);
     						        }
        				      	},
        				      	failure: function(result, request){
		 						         Ext.Msg.alert('Message', 'No data to be saved');
		 						        }
        					});
			        	}
			        	
			        }
			      }],                  
                
               <?php if($m_params->g_multi != 'Y'){?>
        		imposta_title: function(titolo){
        				this.up('window').setTitle(titolo);
        			},
        		<?php }?>	
        	
        	
				loadArt : function(cod_art){
				  var me = this;			  
				  
				  //dati anagrafica std articoli
				  Ext.Ajax.request({
				   		url: 'acs_win_dettaglio_articoli_get_dati.php?fn=open_f&m_art=' + cod_art,
				   		success: function(result, request) {
				   			var jsonData = Ext.decode(result.responseText);
				   			var art_data = jsonData.data;
				   			me.original_record = art_data;
				   			var el_panel = me.query('[xtype="form"]');
				   			Ext.each(el_panel, function(p) {
								p.getForm().setValues(art_data);
								if(p.getForm().owner.title == 'Specifiche'){
    								if(art_data.ARUMCO == art_data.ARUMTE && art_data.ARUMCO == art_data.ARUMAL){
    								  p.getForm().findField('U_M').hide();
									  p.getForm().findField('U_M_cb').setValue(art_data.ARUMTE);
									}else{
									  p.getForm().findField('U_M_cb').hide();
    								  var u_m = art_data.ARUMTE + ', ' + art_data.ARUMCO + ', ' +art_data.ARUMAL
    								  p.getForm().findField('U_M').setValue(u_m);
    								  p.getForm().findField('U_M').setReadOnly(true);
								 	}
								}

					        });
        				   var wf = me.down('#tp-dett-art-formati');
    				    	if (!Ext.isEmpty(wf)){
		            		 		wf.loadArt(cod_art);
		            		}
          			  	   
				    }
				  });
				  
                },
                
               
                
                listeners: {
                  afterrender: function (comp) {
                  <?php if($m_params->g_multi != 'Y'){?>
                  	comp.loadArt(<?php echo j($m_params->c_art); ?>);
                 <?php }?> 	
                  } //afterrender
                  
                  
                  , 'beforetabchange': function(tabp, panTo, panFrom) {
                       console.log("beforetabchange");
                       
						var activeTabIndex = tabp.items.findIndex('id', panFrom.id);                       
                       
						//Verifico se nel panel in uscita (From) avevo fatto delle modifiche non non salvate
						
						//Dry (vedi sopra)						
						var fm = panFrom.getForm();
						var fs = fm.getFieldValues();
						var tp = tabp;
						var is_modified = false;
						var v_modified = {};
						  
                        for (var f in fs) {
                        	if (fm.findField(f).xtype == 'numberfield'){
                        		var vn = fs[f];
                        		var vo = tp.original_record[f];
                        		                        		 
                        		<?php if($m_params->g_multi != 'Y'){?>
                        		if (Ext.isEmpty(vn)) vn = 0;
                        		if (Ext.isEmpty(vo)) vo = 0;
                        		
                        		vn = parseFloat(vn);
                        		vo = parseFloat(vo);
                        		
                        		
                        		<?php } ?> 
                        		
                        		if (vn != vo){
                        			v_modified[f] = vn;
                        			is_modified = true;
                        		}	                     		                        		
                        		
                        	} else {
                            	if (fm.findField(f).xtype == 'datefield'){
                            		var vn = fm.findField(f).getSubmitValue();
                            		var vo = tp.original_record[f + '_val'];
                            		
                            		if (Ext.isEmpty(vn)) vn = 0;
                                	if (Ext.isEmpty(vo)) vo = 0;
                            		
                            		vn = parseInt(vn);
                            		vo = parseInt(vo);
									
                            		if (vn != vo){
                            			v_modified[f] = vn;
                            			is_modified = true;
                            		}									
									
								} else {
									
                                        if(f.substring(0, 5) == 'clear'){
                                            console.log(fs[f]);
                                        }else{
                                            var vn = fs[f];
                                    		var vo = tp.original_record[f];
                                    		                            		
                                    		                        		                        		
                                    		if (Ext.isEmpty(vn)) vn = "";
                                    		if (Ext.isEmpty(vo)) vo = "";
                                    		
                                    		vn = vn.rtrim();
                                    		vo = vo.rtrim();
            
                                    		if (vn != vo){
                                    			//escludo per ora quelli undefined (non presenti nel original_record)
                                    			// (vedi ad esempio U_M_cb che non e' presente nel record originale)
                                    			if(typeof(tp.original_record[f]) != 'undefined'){
                                    				v_modified[f] = vn;
                                    				is_modified = true;
                                    			}	
                                    		} 
                                        }
                                		     
                                	  }                  		                        		
                        	}

                        } //per ogni field						                       
                       
                       
                       if (is_modified){
        	    		Ext.Msg.confirm('Richiesta conferma', 'Attenzione!!!! Campi modificati non ancora salvati.<br/>Proseguire ugualmente?' , function(btn, text){
            	   		    if (btn == 'yes'){
        	    				return true;				    
					    	} else
					    		tabp.setActiveTab(activeTabIndex);
            	   		  });                       
                       }    //if is_modified     
                      
                   } //beforechange            
                  
                },
                
                items: [                    
                  <?php require_once("_art_dett_tabpanel_std.php"); ?>,
                  
                  <?php if( $cfg_mod_DeskArt["tab_formati"] == 'Y'
                            && $m_params->g_multi != 'Y'){
                  require_once("_art_dett_tabpanel_formati.php"); ?>,
                  <?php }?>
                ]
            }
	
]}


<?php 
exit;
}