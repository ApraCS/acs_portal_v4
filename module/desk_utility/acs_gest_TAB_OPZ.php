 <?php
require_once("../../config.inc.php");


$main_module = new DeskArt(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

$m_table_config = array(
    'module'      => $main_module,
    'tab_name'    => $cfg_mod_DeskArt['file_tabelle'],
    't_panel' =>  "[{$m_params->TAASPE}]",
    'form_title' => "Dettagli tabella {$m_params->TAASPE} Opzioni proprietÓ articoli",
    'maximize'    => 'Y',
    'conferma_elimina'    => 'Y',
    'fields_preset' => array(
        'TATAID' => $m_params->open_request->TAASPE,
        //'TARIF1' => $m_params->open_request->sezione
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
   
    'fields_key' => array('TAKEY1', 'TADESC'),
    
    'fields_grid' => array('TAKEY1', 'TADESC', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC'),
    
    
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione'),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        
      
            
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
        
    )
);


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
