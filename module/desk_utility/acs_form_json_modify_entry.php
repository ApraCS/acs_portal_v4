<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));


$oggi = oggi_AS_date();

$m_params = acs_m_params_json_decode();

//recupero l'elenco degli ordini interessati
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));


//recupere gli ordini
global $conn, $cfg_mod_Spedizioni;
$cfg_mod = $main_module->get_cfg_mod();

$sql = "SELECT *
        FROM {$cfg_mod_DeskArt['file_assegna_ord']} AS0
		WHERE ASDT = '{$id_ditta_default}' AND ASIDPR = ?";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt, array($m_params->prog));
$row_AS0 = db2_fetch_assoc($stmt);


//elenco possibili utenti destinatari
$user_to = array();
$users = new Users;
/* TO DO: sto passando BDSOLLE per recuperare gli utenti a cui associare l'ttivita'. Non sarebbe giusto */
$ar_users = $users->find_all(); //al momento non gestisco l'area di spedizione

foreach ($ar_users as $ku=>$u)
	$user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");

$ar_users_json = acs_je($user_to);


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'exe_modify_segnalazione_arrivi'
                	}, {
	                	xtype: 'hidden',
	                	name: 'prog',
	                	value: '<?php echo $m_params->prog; ?>'
                	}, {
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente assegnato',
			            queryMode: 'local',
			            selectOnTab: false,
			            name: 'f_utente_assegnato',
			            allowBlank: false,
						forceSelection: true,			            
			            value: '<?php echo trim($row_AS0['ASUSAT']); ?>'
			        }, {
						name: 'f_note',
						xtype: 'textfield',
						fieldLabel: 'Note',
					    maxLength: 100,
					    value: '<?php echo acs_u8e(trim($row_AS0['ASNOTE'])); ?>'
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Scadenza'
					   , name: 'f_scadenza'
					   , format: 'd/m/Y'
					   , value: '<?php echo print_date($row_AS0['ASDTSC'], "%d/%m/%Y"); ?>'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: true
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   , width: 240, anchor: 'none'
					}, {
			            xtype: 'textareafield',
			            grow: true,
			            name: 'f_memo',
			            fieldLabel: 'Memo',
			            anchor: '100%',
			            height: 200,
			            value: <?php $as = new SpedAssegnazioneOrdini; echo j($as->get_memo($m_params->prog)); ?>			            
			        }
				],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-24', scale: 'medium',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	            	
			                form.submit({
				                            //waitMsg:'Loading...',
				                            success: function(form,action) {
												loc_win.fireEvent('afterUpdateRecord', loc_win);				                                 	
				                            },
				                            failure: function(form,action){
				                                Ext.MessageBox.alert('Erro');
				                            }
				                        });
				            
				    }            	                	                
	            }
	        }],             
				
        }
]}