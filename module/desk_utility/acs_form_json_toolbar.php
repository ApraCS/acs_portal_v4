<?php
require_once "../../config.inc.php";


?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
			cls: 'acs_toolbar',
			tbar: [
		   {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Tabelle',
			            items: [
			              {
			                text: '<br>Varianti',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_PUVR.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'PUVR'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },    {
			                text: '<br>Tipologie',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_PUTI.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'PUTI'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			               {
			                text: '<br>Configuratori',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_PUMO.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'PUMO'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            }, 
			            {
			                text: '<br>Regole DC',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_MURC.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'MURC'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			             {
			                text: '<br>Imballi',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_PUCI.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'PUCI'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            }, 
			            {
			                text: '<br>Operazioni',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_PUOP.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'PUOP'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },{
			                text: '<br>Linee',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_LIPC.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'LIPC'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            }, {
			                text: '<br>Modelli',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_PUMC.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'PUMC'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },{
			                text: '<br>GruPiani',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_GRPA.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'GRPA'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },{
			                text: '<br>Cee',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_CENC.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'CENC'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },{
			                text: '<br>Materiale',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_BCMP.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'BCMP'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },{
			                text: '<br>Certificazione',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								    /*acs_show_panel_std('acs_tab_sys_BCER.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'BCER'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();*/
				           			
				           			acs_show_win_std('BCER', 'acs_anag_art_schede_aggiuntive.php?fn=open_tab', {c_art : '', tipo_scheda : 'BCER'}, 1200, 400, null, 'icon-tag-16');
								    panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            }, 
			            {
			                text: '<br>Listini',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_BITL.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'BITL'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			            {
			                text: '<br>Ferramenta',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_PUFI.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'PUFI'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			             {
			                xtype:'splitbutton',
			                text: '<br>Classificazioni',
			                scale: 'large',
			                iconAlign: 'top',
			                iconCls: 'icon-database_active-32',
			                arrowAlign:'bottom',
			                handler: function(){
        	                 this.maybeShowMenu();
        	           			},
			                menu: {
			                	xtype: 'menu',
		        				items: [
		        				 {
        			                text: 'Classe',	
        			                scale: 'small',	                
        			                handler : function() {
        								acs_show_panel_std('acs_tab_sys_MUCM.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'MUCM'});	          	                	                
        				           			panel = this.up('buttongroup').up('panel').up('window');
        				           			panel.close();
        								} //handler function()
        			            },
        			            {
        			                text: 'Gruppo',
        			                scale: 'small',				                
        			                 handler : function() {
        								acs_show_panel_std('acs_tab_sys_MUGM.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'MUGM'});	          	                	                
        				           			panel = this.up('buttongroup').up('panel').up('window');
        				           			panel.close();
        								} //handler function()
        			            },
		        				 {
        			                text: 'Sottogruppo',
        			                scale: 'small',				                
        			                handler : function() {
        								acs_show_panel_std('acs_tab_sys_MUSM.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'MUSM'});	          	                	                
        				           			panel = this.up('buttongroup').up('panel').up('window');
        				           			panel.close();
        								} //handler function()
        			            },{
        			                text: 'Gruppo ricarico',
        			                scale: 'small',				                
        			                 handler : function() {
        								acs_show_panel_std('acs_tab_sys_MURI.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'MURI'});	          	                	                
        				           			panel = this.up('buttongroup').up('panel').up('window');
        				           			panel.close();
        								} //handler function()
        			            },{
        			                text: 'Propriet&agrave;/Caratteristiche',
        			                scale: 'small',				                
        			                 handler : function() {
        								acs_show_panel_std('acs_gest_ARPRO.php?fn=open_tab');	          	                	                
        				           			panel = this.up('buttongroup').up('panel').up('window');
        				           			panel.close();
        								} //handler function()
        			            }
		        				]}
		        				
		        			},
		        			 {
			                xtype:'splitbutton',
			                text: '<br>Import',
			                scale: 'large',
			                iconAlign: 'top',
			                iconCls: 'icon-database_active-32',
			                arrowAlign:'bottom',
			                handler: function(){
        	                 this.maybeShowMenu();
        	           			},
			                menu: {
			                	xtype: 'menu',
		        				items: [
		        				 {
        			                text: 'Cataloghi',	
        			                scale: 'small',	                
        			                 handler : function() {
        								acs_show_panel_std('acs_gest_CATIN.php?fn=open_tab', 'panel_gestione_CATIN');
        								panel = this.up('buttongroup').up('panel').up('window');
            				            panel.close();
        								} //handler
        			            },
        			         	 {
        			                text: 'Tipologia import',	
        			                scale: 'small',	                
        			                 handler : function() {
        								acs_show_panel_std('acs_gest_TPART.php?fn=open_tab', 'panel_gestione_TPART');
        								panel = this.up('buttongroup').up('panel').up('window');
            				            panel.close();
        								} //handler
        			            },
        			             {
        			                text: 'Dati import',	
        			                scale: 'small',	                
        			                 handler : function() {
        								acs_show_panel_std('acs_gest_DTIMP.php?fn=open_tab', 'panel_gestione_DTIMP');
        								panel = this.up('buttongroup').up('panel').up('window');
            				            panel.close();
        								} //handler
        			            },
		        				]}
		        				
		        			},
			           
			            
			            
			            	]
			        }
			]
		}
	]		
}            