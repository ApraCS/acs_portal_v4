<?php
require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();

//L'INSERT PER IL DUPLICA
if ($_REQUEST['fn'] == 'exe_ins_dup'){
    $m_params = acs_m_params_json_decode();
    $ar_ins = array();    
    
    
 
    if($m_params->from_cl == 'Y'){
        
        foreach($m_params->rows as $v){
            
            $sql = "SELECT COUNT(*) AS C_ROW
            FROM {$cfg_mod_DeskArt['file_cfg_distinte_liste']} CL
            WHERE CLDT= '$id_ditta_default' AND CLCDLI = '{$m_params->or_row->CLCDLI}'
            AND CLCELE = '{$v->c_art}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            $row = db2_fetch_assoc($stmt);
            
            if($row['C_ROW'] > 0){
                
                $err_msg .= "Lista {$m_params->or_row->CLCDLI} con opzione {$v->c_art} esistente <br>";
                
                
            }else{
                
                $ar_ins['CLUSGE'] 	= $auth->get_user();
                $ar_ins['CLORGE'] 	= oggi_AS_time();
                $ar_ins['CLDTGE'] 	= oggi_AS_date();
                $ar_ins['CLUSUM'] 	= $auth->get_user();
                $ar_ins['CLORUM'] 	= oggi_AS_time();
                $ar_ins['CLDTUM'] 	= oggi_AS_date();
                $ar_ins['CLDT'] 	= $id_ditta_default;
                $ar_ins['CLCDLI'] 	= $m_params->or_row->CLCDLI;
                $ar_ins['CLSEQU'] 	= $m_params->or_row->CLSEQU;
                
                $ar_ins['CLCELE'] 	= $v->c_art; //substr($v->c_art, strlen($m_params->or_row->CDRSTR));
                $ar_ins['CLDESC'] 	= $v->d_art;
                
                $ar_ins['CLSTIN'] 	= $m_params->or_row->CLSTIN;
                $ar_ins['CLSTFI'] 	= $m_params->or_row->CLSTFI;
                $ar_ins['CLRSTR'] 	= $m_params->or_row->CLRSTR;
                $ar_ins['CLNEWC'] 	= $m_params->or_row->CLNEWC;
                $ar_ins['CLPROG'] 	= $m_params->or_row->CLPROG;
                $ar_ins['CLNCAR'] 	= $m_params->or_row->CLNCAR;
                $ar_ins['CLARDU'] 	= $m_params->or_row->CLARDU;
                $err_msg = "";
                
                $sql = "INSERT INTO {$cfg_mod_DeskArt['file_cfg_distinte_liste']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
            }
            
        }
        
        
    } else {         // from_cl != 'Y'
        

        
        
        
        if(isset($m_params->rows) && count($m_params->rows) > 0){
            foreach($m_params->rows as $v){
       
                
                $sql = "SELECT COUNT(*) AS C_ROW
                        FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
                        WHERE CDDT= '$id_ditta_default' AND CDDICL = '{$m_params->indice}'
                        AND CDSLAV = '{$v->c_art}' AND CDCMAS = '{$m_params->or_row->CDCMAS}'";                
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt);
                $row = db2_fetch_assoc($stmt);
                                
                if($row['C_ROW'] > 0){                    
                    $err_msg .= "Configurazione con gruppo {$m_params->or_row->CDCMAS} e voce {$v->c_art} esistente <br>";                                        
                }else{
                    
                    $ar_ins = array();
                    $ar_ins['CDUSGE'] 	= $auth->get_user();
                    $ar_ins['CDORGE'] 	= oggi_AS_time();
                    $ar_ins['CDDTGE'] 	= oggi_AS_date();
                    $ar_ins['CDUSUM'] 	= $auth->get_user();
                    $ar_ins['CDORUM'] 	= oggi_AS_time();
                    $ar_ins['CDDTUM'] 	= oggi_AS_date();
                    $ar_ins['CDDT'] 	= $id_ditta_default;
                    $ar_ins['CDSEQI'] 	= $m_params->or_row->CDSEQI;
                    $ar_ins['CDSEQU'] 	= $m_params->or_row->CDSEQU;
                    $ar_ins['CDSTIN'] 	= $m_params->or_row->CDSTIN;
                    $ar_ins['CDSTFI'] 	= $m_params->or_row->CDSTFI;
                    $ar_ins['CDFLGA'] 	= $m_params->or_row->CDFLGA;
                    $ar_ins['CDFLLG'] 	= $m_params->or_row->CDFLLG;
                    $ar_ins['CDCDLI'] 	= $m_params->or_row->CDCDLI;
                    $ar_ins['CDNEWC'] 	= $m_params->or_row->CDNEWC;
                    $ar_ins['CDPROG'] 	= $m_params->or_row->CDPROG;
                    $ar_ins['CDNCAR'] 	= $m_params->or_row->CDNCAR;
                    $ar_ins['CDARDU'] 	= $m_params->or_row->CDARDU;
                    $ar_ins['CDDICL'] 	= $m_params->indice;
                    
                    $voce = "";
                    for($i = 0; $i <= 14; $i++){
                        $filtro = 'f_'.$i;
                        
                        if($m_params->form_values->$filtro == '?')
                            $voce .= substr($v->c_art, $i, 1);
                    }
                    
                    $ar_ins['CDSLAV'] = $voce;
                    $ar_ins['CDDESC'] 	= $v->d_art;
                    $ar_ins['CDCMAS'] 	= $m_params->form_values->f_gruppo;
                    $ar_ins['CDRSTR'] 	= $v->c_art;  //radice
                   
                    $err_msg = "";
                    
                    $sql_i = "INSERT INTO {$cfg_mod_DeskArt['file_cfg_distinte']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                    
                    $stmt_i = db2_prepare($conn, $sql_i);                    
                    echo db2_stmt_errormsg();
                        
                    $result = db2_execute($stmt_i, $ar_ins);                        
                    echo db2_stmt_errormsg($stmt_i);
                }
                
            }
            
        } 
        else //not rows 
        {
    
            $ar_ins = array();
            $ar_ins['CDUSGE'] 	= $auth->get_user();
            $ar_ins['CDORGE'] 	= oggi_AS_time();
            $ar_ins['CDDTGE'] 	= oggi_AS_date();
            $ar_ins['CDUSUM'] 	= $auth->get_user();
            $ar_ins['CDORUM'] 	= oggi_AS_time();
            $ar_ins['CDDTUM'] 	= oggi_AS_date();
            $ar_ins['CDDT'] 	= $id_ditta_default;
            $ar_ins['CDSEQI'] 	= $m_params->or_row->CDSEQI;
            $ar_ins['CDSEQU'] 	= $m_params->or_row->CDSEQU;
            $ar_ins['CDSTIN'] 	= $m_params->or_row->CDSTIN;
            $ar_ins['CDSTFI'] 	= $m_params->or_row->CDSTFI;
            $ar_ins['CDFLGA'] 	= $m_params->or_row->CDFLGA;
            $ar_ins['CDFLLG'] 	= $m_params->or_row->CDFLLG;
            $ar_ins['CDCDLI'] 	= $m_params->or_row->CDCDLI;
            $ar_ins['CDNEWC'] 	= $m_params->or_row->CDNEWC;
            $ar_ins['CDPROG'] 	= $m_params->or_row->CDPROG;
            $ar_ins['CDNCAR'] 	= $m_params->or_row->CDNCAR;
            $ar_ins['CDARDU'] 	= $m_params->or_row->CDARDU;
            $ar_ins['CDDICL'] 	= $m_params->indice;
     
            
            $sql = "SELECT COUNT(*) AS C_ROW
            FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
            WHERE CDDT= '$id_ditta_default' AND CDDICL = '{$m_params->indice}'
            AND CDSLAV = '{$m_params->form_values->f_voce}' AND CDCMAS = '{$m_params->form_values->f_gruppo}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            $row = db2_fetch_assoc($stmt);
            if($row['C_ROW'] > 0){
                
                $err_msg .= "Configurazione con gruppo {$m_params->form_values->f_gruppo} e voce {$m_params->form_values->f_voce} esistente <br>";
                
                
            }else{
                
                if(isset($m_params->form_values->f_voce) && strlen($m_params->form_values->f_voce) > 0)
                    $voce = $m_params->form_values->f_voce;
                else 
                    $voce = $m_params->voce;
                
                $ar_ins['CDCMAS'] 	= $m_params->form_values->f_gruppo; //substr($v->c_art, strlen($m_params->or_row->CDRSTR));
                $ar_ins['CDSLAV'] 	= $voce;
                $ar_ins['CDDESC'] 	= $m_params->or_row->CDDESC;  
                $ar_ins['CDRSTR'] 	= $m_params->form_values->f_gruppo.$voce;//$m_params->form_values->f_voce; 
                
                $err_msg = "";
         
                
                
                $sql = "INSERT INTO {$cfg_mod_DeskArt['file_cfg_distinte']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
            }
        }
        
        
    }
    
    $ret = array();
    $ret['success'] = true;
    $ret['error_msg'] = $err_msg;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_rec_cd'){
    $m_params = acs_m_params_json_decode();
    
    $sql = "SELECT *
    FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
    WHERE CDDT= '$id_ditta_default' AND CDDICL = '{$m_params->rec->distinta}'
    AND CDSLAV = '{$m_params->rec->codice}' AND CDCMAS = '{$m_params->rec->gruppo}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);

    $ret = array();
    $ret['success'] = true;
    $ret['row'] = $row;
    echo acs_je($ret);
    exit;
}