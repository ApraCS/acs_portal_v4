<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));

ini_set('max_execution_time', 90000);

if ($_REQUEST['fn'] == 'exe_lancia'){

	$m_params = acs_m_params_json_decode();
	
	global $cfg_mod_DeskAcq;
	
	$ar_chiave = (array)$m_params->chiave;
	$ar_vals = (array)$m_params->vals;

	$sh = new SpedHistory($m_DeskAcq);
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> $ar_chiave['RIRGES'],
					"vals" => $ar_vals
			)
			);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid'){

	global $cfg_mod_DeskAcq;
	$m_params = acs_m_params_json_decode();
	$ar_chiave = (array)$m_params->open_request->chiave;
	$ar_vals = (array)$m_params->open_request->vals;

	
	$sql = "SELECT RIDTRI, RIHMRI, RIUSRI,RIESIT, RINOTE, RIDTES, RIHMES, RIART
			FROM {$cfg_mod_DeskAcq['file_richieste']}
			WHERE  RIRGES = '{$ar_chiave['RIRGES']}' AND RICITI = '{$ar_chiave['RICITI']}'
			ORDER BY RITIME DESC FETCH FIRST 5 ROWS ONLY";
	

	/*print_r($ar_vals);
	exit;*/
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_vals);

	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['data_i'] = print_date(trim($row['RIDTRI']));
		$nr['ora_i'] = print_ora(trim($row['RIHMRI']));
		$nr['utente'] = trim($row['RIUSRI']);
		$nr['stato'] = trim($row['RIESIT']);
		$nr['note'] = trim($row['RINOTE']);
		$nr['data_f'] = print_date(trim($row['RIDTES']));
		$nr['ora_f'] = print_ora(trim($row['RIHMES']));
		$nr['forn'] = trim($row['RIART']);
		$ar[] = $nr;
	}

	echo acs_je($ar);
	exit;
}



if ($_REQUEST['fn'] == 'open_form'){
	$m_params = acs_m_params_json_decode();
	
	global $cfg_mod_DeskAcq;
	
	$ar_chiave = (array)$m_params->chiave;
	$ar_vals = (array)$m_params->vals;
	
	$sql = "SELECT COUNT(*) AS C_ROWS
			FROM {$cfg_mod_DeskAcq['file_richieste']}
			WHERE RIESIT <> 'C' AND RIRGES = '{$ar_chiave['RIRGES']}' AND RICITI = '{$ar_chiave['RICITI']}'";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);
	$r = db2_fetch_assoc($stmt);

	
?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            	buttons: [  {
 			            text: 'Lancia',
 			            iconCls: 'icon-button_blue_play-32',
 			            <?php 	if($r['C_ROWS'] == 0){?>   
 			            disabled : false,
 			            <?php }else{?>
 			            disabled : true,
 			            <?php }?>
 			            scale: 'large',
 			               handler: function() {
 			               
 			               var m_grid = this.up('window').down('grid');
 			               
 						    this.disable(); 			               
 			          
 							 Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_lancia',
 						        timeout: 2400000,
 						        method     : 'POST',
 			        			jsonData: {
 			        			    chiave: <?php echo acs_je($m_params->chiave); ?>,
 			        			    vals: <?php echo acs_je($m_params->vals); ?>
 								},							        
 						        success : function(result, request){
 						    		m_grid.getStore().load(); 			            			
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
 						    


 						
 			
 			            }
 			         }],  
					items: [
						{
						xtype: 'grid',
						title: 'Stato elaborazione',
						flex:0.7,
				        loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['data_i', 'ora_i', 'utente', 'stato', 'note', 'data_f', 'ora_f', 'forn']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Data inizio',
	                dataIndex: 'data_i',
	                flex: 1
	                },
	                {
	                header   : 'Ora inizio',
	                dataIndex: 'ora_i',
	                flex: 1
	                },
	                {
	                header   : 'Utente',
	                dataIndex: 'utente',
	                flex: 1
	                },{
	                header   : 'Fornitore',
	                dataIndex: 'forn',
	                flex: 1
	                },
	                {
	                header   : 'Stato',
	                dataIndex: 'stato',
	                flex: 1
	                },
	                {
	                header   : 'Note',
	                dataIndex: 'note',
	                flex: 1
	                }, {
	                header   : 'Data fine',
	                dataIndex: 'data_f',
	                flex: 1
	                },
	                {
	                header   : 'Ora fine',
	                dataIndex: 'ora_f',
	                flex: 1
	                },                
	                
	                
	         ], listeners: {
				   afterrender: function(comp){
				       var task = {
				        run: function(){
				        	comp.store.load();
						},
				        interval: 10000 //3*60*1000  3 minuti (in millisecondi)
				    }
				
				    Ext.TaskManager.start(task);
				   }
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('stato') == 'C')
					           		return 'colora_riga_grigio';
					           return '';																
					         }   
					    }
					       
		
		
		}
				
					 ],
					 
		  
					
					
	}
	
]}


<?php 
}