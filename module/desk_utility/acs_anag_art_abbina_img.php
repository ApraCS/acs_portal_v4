<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

//****************************************************
if ($_REQUEST['fn'] == 'open'){
//****************************************************
  $proc = new ApiProc();
  $proc->out_json_response(array(
     'success'=>true
   , 'items' => _aa_img_w()
  ));
  exit;
}


//****************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
//****************************************************
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _aa_get_json_data($m_params)
    ));
    exit;
}


//****************************************************
if ($_REQUEST['fn'] == 'abbina_da_rrn'){
//****************************************************    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>_exe_abbina_da_rrn($m_params)
    ));
    exit;
}

######################################################################
######################################################################
######################################################################

######################################################################
function _aa_img_w(){       //Main WIN
######################################################################
  $c = new Extjs_compo('panel', layout_ar('vbox'));
  $c->add_items(array(
      _aa_img_form_filter(),
      _aa_img_grid() ));

  return $c->code();
}




function _aa_img_grid(){
    global $m_params;
    $c = new Extjs_compo('grid', null, 'Elenco allegati');
    $c->set(array(
        'multiSelect' => true,
        'flex' => 1,
        'columns'     => array(
              array('width' => 150, 'dataIndex' => 'TAKEY1',  'header' => 'Cod. Articolo')
            , array('width' => 50,  'dataIndex' => 'TAFG01',  'header' => 'F/L', 'tooltip'=>'File/Url')
            , array('flex'  => 1,   'dataIndex' => 'TADESC',  'header' => 'Descrizione allegato')
            , array('flex'  => 1,   'dataIndex' => 'TALOCA',  'header' => 'Nome file')
            , array('width' =>80,   'dataIndex' => 'TAUSGE',  'header' => 'Utente')
            , array('width' =>70,   'dataIndex' => 'TADTGE',  'header' => 'Data', 'renderer' => extjs_code('date_from_AS'))
        ),
        'store'       => array(
            'autoLoad'    => false,
            'fields'      => array('RRN', 'TAKEY1', 'TADESC', 'TALOCA', 'TAFG01', 'TAUSGE', 'TADTGE'),
            'proxy'       => extjs_grid_proxy(
                extjs_url('get_json_data')) ),
        'listeners'   => array(
            'itemclick' => extjs_code(grid_itemclick("
              //ToDo                
                return false;
                var me = this,
                    m_panel  = this.up('#art_adv'),
                    form_add = m_panel.down('#form_add');
                form_add.loadRecord(rec);
           "))
        )
    ));
    $c->add_button(array(
        'xtype' => 'button',
        'text' => 'Abbina',
        'handler' => extjs_code("
            function(a, b, c, d){
                var loc_grid = this.up('grid'),
                    loc_win  = this.up('window'),
                    rec_selected = loc_grid.getSelectionModel().getSelection(),
		    	    list_selected_id = [];
		    	for (var i=0; i<rec_selected.length; i++) 
		    	 list_selected_id.push(rec_selected[i].get('RRN'));
                var from_grid = Ext.getCmp('{$m_params->from_grid}');

				Ext.Ajax.request({
				    url        : " . j(extjs_url('abbina_da_rrn')) . ",
			        method     : 'POST',
        			jsonData: {
                        c_art: " . j($m_params->c_art) . ",
        				list_selected_id: list_selected_id
					},							        
			        success : function(result, request){
			        	//jsonData = Ext.decode(result.responseText);
			        	
                        loc_win.close();
                        if (!Ext.isEmpty(from_grid))
                            from_grid.store.load();
				        
            		},
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });	

            }
        ")
        ));
    return $c->code();
}





function _aa_img_form_filter(){
    global $m_params;
    $c = new Extjs_Form( layout_ar('vbox'), 'Aggiungi a ordine');
    $c->set(array('height' => 100));
    
    $c->add_items(array(
        extjs_compo('container', layout_ar('hbox'), null, array('flex'=>1, 'items' => array(
            array('xtype' => 'textfield', 'name'=>'ARART', 'fieldLabel' => 'Codice articolo'),
            array('flex'=>1, 'xtype' => 'textfield', 'name'=>'file_name', 'fieldLabel' => 'Nome file', 'labelAlign'=>'right')
            ))),
        array('flex'=>1, 'xtype' => 'textfield', 'name'=>'file_desc', 'fieldLabel' => 'Descrizione file')
    ));
    $c->add_button(array(
        'xtype' => 'button',
        'text' => 'Filtra',
        'flex' => 1,
        'handler' => extjs_code("
            function(){
                var me = this, m_win = this.up('window'), m_form = this.up('form'), m_grid = m_win.down('grid');
                m_grid.store.proxy.extraParams = m_form.getValues();
                m_grid.store.load();
            }
        ")
    ), 'right');
    
    return $c->code();
}





/* ------------------------------------------------------------- */
function _aa_get_json_data($m_params){
/* ------------------------------------------------------------- */
    global $conn, $id_ditta_default, $cfg_mod_DeskPVen, $cfg_mod_DeskArt;
    
    $sql_where = '';
    $sql_where.= sql_where_by_combo_value('UPPER(TA.TAKEY1)', strtoupper($m_params->ARART));
    $sql_where.= sql_where_by_combo_value('UPPER(TA.TADESC)', strtoupper($m_params->file_desc), 'LIKE');
    $sql_where.= sql_where_by_combo_value('UPPER(TA.TALOCA)', strtoupper($m_params->file_name), 'LIKE');
        
    $sql="SELECT RRN(TA) as RRN, TA.* FROM {$cfg_mod_DeskArt['file_tabelle']} TA WHERE TADT = '{$id_ditta_default}' AND TATAID='ARTAL' {$sql_where}";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ret = array();
    
    while ($row = db2_fetch_assoc($stmt)) {
        $ret[] = $row;
    }
    return grid_to_json_data($ret);
}



function _exe_abbina_da_rrn($m_params){
  global $auth, $conn, $id_ditta_default, $cfg_mod_DeskPVen, $cfg_mod_DeskArt;

  $sql="SELECT * FROM {$cfg_mod_DeskArt['file_tabelle']} TA WHERE TADT = '{$id_ditta_default}' AND RRN(TA) IN(" . sql_t_IN($m_params->list_selected_id) . ")";
  $stmt = db2_prepare($conn, $sql);
  echo db2_stmt_errormsg();
  $result = db2_execute($stmt);
  
  while ($row = db2_fetch_assoc($stmt)) {
      $ar_ins = $row;
      $ar_ins['TAUSGE'] 	= $auth->get_user();
      $ar_ins['TADTGE'] 	= oggi_AS_date();
      $ar_ins['TAORGE'] 	= oggi_AS_time();
      $ar_ins['TAKEY1']     = $m_params->c_art;
    
      $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
      $stmt_ins = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      $result = db2_execute($stmt_ins, $ar_ins);
      echo db2_stmt_errormsg($stmt_ins);
  } //per ogni rrn
  return true;
}
