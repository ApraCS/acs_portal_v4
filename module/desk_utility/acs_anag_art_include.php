<?php

function anag_art_sql_where($form_ep){
  $sql_where = '';
  $sql_where.= sql_where_by_combo_value('ARTPAR', $form_ep->f_tipo_parte);
  $sql_where.= sql_where_by_combo_value('ARCLME', $form_ep->f_classe);
  $sql_where.= sql_where_by_combo_value('ARGRME', $form_ep->f_gruppo);
  $sql_where.= sql_where_by_combo_value('ARSGME', $form_ep->f_sottogruppo);
  $sql_where.= sql_where_by_combo_value('ARTAB1', $form_ep->f_pianificazione);
  $sql_where.= sql_where_by_combo_value('ARFOR1', $form_ep->f_fornitore);
    
    
  if (strlen($form_ep->f_data_dal) > 0)
    $sql_where.= " AND ARDTGE >= $form_ep->f_data_dal ";
  if (strlen($form_ep->f_data_al) > 0)
    $sql_where.= " AND ARDTGE <= $form_ep->f_data_al ";
            
  if($form_ep->f_cod_art != ''){
    $sql_where.= " AND UPPER(ARART) LIKE '{$form_ep->f_cod_art}'";               
  } elseif($m_params->radice != ''){
    $sql_where.= artd_ARART_add_where($m_params->radice);
  } else {
    $text = '';
    for($i = 1; $i <= 15; $i++){
      $filtro = 'f_'.$i;
      if($form_ep->$filtro == '')
        $form_ep->$filtro = '_';
      $text .= $form_ep->$filtro;
    }
                
    if($text != "_______________")
      $sql_where.= " AND UPPER(ARART) LIKE '" . strtoupper($text) . "' ";
  }
            
            
  $ar_esau = array();
  if ($form_ep->f_esau == 'E')
    array_push($ar_esau, $form_ep->f_esau);
  if ($form_ep->f_avv == 'A')
    array_push($ar_esau, $form_ep->f_avv);
  if ($form_ep->f_corso == 'C')
    array_push($ar_esau, $form_ep->f_corso);
  if (count($ar_esau) > 0)
    $sql_where.= sql_where_by_combo_value('ARESAU', $ar_esau);
                            
  if (strlen($form_ep->f_desc_art) > 0)
    $sql_where.= " AND UPPER(ARDART) LIKE '%" . strtoupper($form_ep->f_desc_art) . "%' ";
                                
  if($form_values->f_scaduti == 'N'){ //esclusi scaduti
    $sql_where .= "AND BSDTSR >= ".oggi_AS_date();
  }
  if($form_values->f_scaduti == 'Y'){ //solo scaduti
    $sql_where .= "AND BSDTSR < ".oggi_AS_date();
  }

  if ($form_ep->f_sospeso == 'Y')
    $sql_where.= " AND ARSOSP = 'S'";
  if ($form_ep->f_sospeso == 'N')
    $sql_where.= " AND ARSOSP <> 'S'";
                                        
  if($form_ep->f_sfg[0] == 'N')
    $sql_where .= " AND (AXSFON IS NULL  OR AXSFON = '')";
  else
    $sql_where.= sql_where_by_combo_value('AXSFON', $form_ep->f_sfg);

  if($form_ep->f_sfp[0] == 'N')
    $sql_where .= " AND (AUSFAR IS NULL  OR AUSFAR = '')";
  else
    $sql_where.= sql_where_by_combo_value('AUSFAR', $form_ep->f_sfp);
  return $sql_where;
}
