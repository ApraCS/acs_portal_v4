<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

//*****************************************************
if ($_REQUEST['fn'] == 'open_tab'){
//*****************************************************
?>
{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
			items: [
				
			  {
				xtype: 'grid',
				itemId: 'g_divisione',
				title: '',
				selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},
				flex:0.7,
		        loadMask: true,	
		        features: [	
    				{
    					ftype: 'filters',
    					encode: false, 
    					local: true,   
    			   		 filters: [
    			       {
    			 		type: 'boolean',
    					dataIndex: 'visible'
    			     }
    			      ]
    			}],	
		        store: {
				    xtype: 'store',
				    autoLoad:true,
	                fields: [{name:'id'}, {name:'text'}, {name : 'sosp'}],
					data: [								    
    			     <?php echo acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'N', 'N', ''), ''); ?>	
    			    ] 					
									
					}, //store
				<?php $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
			
			      columns: [	
			       {
	                header   : '<?php echo $sosp; ?>',
	                dataIndex: 'sosp',
	                width : 30,
	                renderer: function(value, metaData, record){
		    			  if(record.get('sosp') == true){ 
                               metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			       return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=15>';
		    		 	   }}
	                },{
	                header   : 'Codice',
	                dataIndex: 'id',
	                width : 60,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Divisione',
	                dataIndex: 'text',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                } 
	                
	         	]
	         	
	         		, buttons : [
    	         		{
    	            		text: 'Aggiorna divisione',
    	            		//iconCls: 'icon-save-32',
    	            		scale: 'small',
    	            		handler: function() {
    	            		    var loc_win = this.up('window');
        	            		var g_divisione = this.up('grid');
	            				var id_selected_div = g_divisione.getSelectionModel().getSelection();
    	            			
    	            			var ar_div = [];
    	            			for (var i=0; i<id_selected_div.length; i++){
    			     				ar_div.push(id_selected_div[i].data.id)
    						 	}
    						 	if(ar_div.length > 0)
    	            				var t_div = 'DIV:' + ar_div.join(',');
    	            				
	            				loc_win.fireEvent('afterAggiorna', loc_win, 'DIV', t_div);	
    	            			         			 	            	            
    	            		} //handler
    	            	} //conferma
	         	]
	         	
	         	, listeners: {
	         		'afterrender': function(comp){   		  	 	    
    					comp.on('viewready', function(store, records, options) {
    					
    						var div_selected = [];

    					     <?php
            	  			  //recupero le divisioni selezionate in partenza
            	  			  $t_div = $m_params->bl_rows->f_text_0;
            	  			  $divs = explode(':', $t_div);
            	  			  if ($divs[0] == 'DIV'){
            	  			     $divs = explode(',', $divs[1]);
            	  			     echo 'var div_selected = ' . acs_je($divs) . ';';
            	  			  }
            	  			?>
            	  			comp.store.each(function(rec){            	  		    	        	  		    	
    	    					if (div_selected.includes(rec.data.id)){
    	    						comp.selModel.doSelect(rec, true);
    	    					}	    							    						    			
        					});            	  			
    		               
    					}, comp);
    				} //afterrender
				} //listeners
        		
			},
				
				
				
				
			{
				xtype: 'grid',
				itemId: 'g_modello',
				title: '',
				selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},
				flex:0.7,
		        loadMask: true,
		        features: [	
    				{
    					ftype: 'filters',
    					encode: false, 
    					local: true,   
    			   		 filters: [
    			       {
    			 		type: 'boolean',
    					dataIndex: 'visible'
    			     }
    			      ]
    			}],		
		        store: {
				    xtype: 'store',
				    autoLoad:true,
	                fields: [{name:'id'}, {name:'text'}, {name : 'sosp'}, {name : 'tarest'}],
					data: [								    
    			     <?php
    			         if (isset($cfg_mod_DeskArt['ACR_MOD']))
    			             $g_mod = $cfg_mod_DeskArt['ACR_MOD'];
    			         else
    			             $g_mod = 'MOD';
    			         echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $g_mod, null, null, 38, '', 'N', 'N', ''), '');
    			     ?>	
    			    ] 				
									
					}, //store
				<?php $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
			
			      columns: [	
			        {
	                header   : '<?php echo $sosp; ?>',
	                dataIndex: 'sosp',
	                width : 30,
	                renderer: function(value, metaData, record){
		    			  if(record.get('sosp') == true){ 
                               metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			       return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=15>';
		    		 	   }}
	                },{
	                header   : 'Codice',
	                dataIndex: 'id',
	                width : 60,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Modello',
	                dataIndex: 'text',
	                width : 120,
	                filter: {type: 'string'}, filterable: true,
	                },
	                {
	                header   : 'Tipologie',
	                dataIndex: 'tarest',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true,
	                renderer: function(value, metaData, record){
		    			var str = record.get('tarest');
		    			if(str != ''){
		    			var tipologie = str.split('');
                        for(var i=0; i < tipologie.length; i++){
                         if(i >= 2 && i%3 === 0){
                            tipologie[i] =  '*' + tipologie[i] ;
                          }
                        }
                        str = tipologie.join('').replace(/\*/g,', ');
                        return str; 
		    			}
		    		 }
	                }  
	                
	         ],
	         
	         	buttons : [
    	         		{
    	            		text: 'Aggiorna modello',
    	            		//iconCls: 'icon-save-32',
    	            		scale: 'small',
    	            		handler: function() {
    	            		    var loc_win = this.up('window');
        	            		var g_modello = this.up('grid');
	            				var id_selected_mod = g_modello.getSelectionModel().getSelection();
    	            			
    	            			var ar_mod = [];
    	            			var t_mods = [];
    	            			var c_in_row = 1;
    	            			for (var i=0; i<id_selected_mod.length; i++){
    			     				ar_mod.push(id_selected_mod[i].data.id)
    
    								//stacco ogni 19
    								if (c_in_row == 19 || i == id_selected_mod.length - 1){
    									var t_mod = 'MOD:' + ar_mod.join(',');
    									t_mods.push(t_mod);		
    									ar_mod = [];
    									c_in_row = 1;
    								} else {
    									c_in_row += 1;
    								} 								
    						 	}
	            				loc_win.fireEvent('afterAggiorna', loc_win, 'MOD', t_mods);	
    	            			         			 	            	            
    	            		} //handler
    	            	} //conferma
	         	]
	         
	         
	         	, listeners: {
	         		'afterrender': function(comp){   		  	 	    
    					comp.on('viewready', function(store, records, options) {
    					
    					var mod_selected = [];

						<?php for ($i = 1; $i < 15; $i++){ ?>	
    					     <?php
            	  			  //recupero i modelli selezionati in partenza
            	  			  $f_name = "f_text_{$i}";
            	  			  $t_div = $m_params->bl_rows->$f_name;
            	  			  $divs = explode(':', $t_div);
            	  			  if ($divs[0] == 'MOD'){
            	  			     $divs = explode(',', $divs[1]);
            	  			     echo 'var mod_selected = ' . acs_je($divs) . ';';
            	  			  }
            	  			?>
            	  			comp.store.each(function(rec){            	  		    	        	  		    	
    	    					if (mod_selected.includes(rec.data.id)){
    	    						console.log('preselezione: ' + rec.data.id);
    	    						comp.selModel.doSelect(rec, true);
    	    					}	    							    						    			
        					});
        				<?php } //for?>	
    		               
    					}, comp);
    				} //afterrender
				} //listeners
				
			} //grid modelli
				
				],
				
				//bottoni del panel
				buttons : [
				
					{
	            		text: 'Conferma',
	            		iconCls: 'icon-save-32',
	            		scale: 'large',
	            		handler: function() {
	            			//recupero i valori selezionati in divisione e modello.
	            			//preparo i text da ritornare al blocco note
	            			var loc_win = this.up('window'),
	            				g_divisione = this.up('panel').down('#g_divisione'),
	            				g_modello = this.up('panel').down('#g_modello'),	            			
	            				id_selected_div = g_divisione.getSelectionModel().getSelection(),
	            				id_selected_mod = g_modello.getSelectionModel().getSelection();
	            			
	            			
	            			//costruzione DIV, in prima riga blocco note
	            			var ar_div = [];
	            			for (var i=0; i<id_selected_div.length; i++){
			     				ar_div.push(id_selected_div[i].data.id)
						 	}
						 	if(ar_div.length > 0)
	            				var t_div = 'DIV:' + ar_div.join(',');

	            			//costruzione MOD, nelle righe blocco successive
	            			//(19 per ogni riga)
	            			var ar_mod = [];
	            			var t_mods = [];
	            			var c_in_row = 1;
	            			for (var i=0; i<id_selected_mod.length; i++){
			     				ar_mod.push(id_selected_mod[i].data.id)

								//stacco ogni 19
								if (c_in_row == 19 || i == id_selected_mod.length - 1){
									var t_mod = 'MOD:' + ar_mod.join(',');
									t_mods.push(t_mod);		
									ar_mod = [];
									c_in_row = 1;
								} else {
									c_in_row += 1;
								} 								
						 	}
						loc_win.fireEvent('afterOkSave', loc_win, t_div, t_mods);		            			 	            	            
	            		} //handler
	            	} //conferma
				]
				
				
				
				, listenersxxxxxxx: {
					afterrender: function(comp){
						console.log('bbb');
						
                            <?php
            	  			  //recupero le divisioni selezionate in partenza
            	  			  $t_div = $m_params->bl_rows->f_text_0;
            	  			  $divs = explode(':', $t_div);
            	  			  if ($divs[0] == 'DIV'){
            	  			     $divs = explode(',', $divs[1]);
            	  			     echo 'var div_selected = ' . acs_je($divs) . ';';
            	  			  }
            	  			?>			
            	  			
            	  		var g_div = comp.down('#g_divisione');
            	  		//g_div.getView().getSelectionModel().selectAll();				
            	  		
        	  		    g_div.store.each(function(rec){
        	  		    	console.log(rec);
        	  		    	//console.log(rec.data.id);
        	  		    	console.log(g_div.getView().getSelectionModel());
        	  		    	        	  		    	
	    					//if (div_selected.includes(rec.data.id))
	    					//	g_div.selModel.doSelect(1);
	    					//	g_div.getView().getSelectionModel().select(rec.index);	    							    						    				
    					});	
					}
				}
				
				}
			]}

<?php }

