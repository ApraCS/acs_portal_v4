<?php

require_once("../../config.inc.php");
require_once("acs_anag_artd_include.php");
require_once '_art_dett_include_f.php';

$main_module = new DeskUtility();
$m_DeskArt = new DeskArt();

function _imposta_campi_base_art_RI($ar){
  global $auth;
  $ar['RATIME'] = microtime(true);
  $ar['RARGES'] = 'UPD_ANAG_ART';
  $ar['RADTRI'] = oggi_AS_date();
  $ar['RAHMRI'] = oggi_AS_time();
  $ar['RAUSRI'] = substr($auth->get_user(), 0, 8);
  return $ar;
}


if ($_REQUEST['fn'] == 'exe_aggiorna_dett_art'){
    ini_set('max_execution_time', 6000);
    $ret = array();
    $m_params = acs_m_params_json_decode();
    
    $sql_where = "";
    
    
    if($m_params->g_multi == 'Y'){
        $sql_where .= sql_where_by_combo_value('ARART', (array)$m_params->multi_selected_id);
        
    }else{
        $cod_art       = $m_params->c_art;
        $sql_where     .= " AND ARART = '{$cod_art}'";
    }
    
    //$ret['success'] = false;
    //$ret['error_msg'] = 'Aggiornamento dati articolo non salvato';
    
    
    
    $ar_upd = array();      //campi da aggiornare direttamente su anag. art
    $ar_upd_RI = array();   //campi da aggiornare tramite RI anag art
    $ar_upd['ARUSUM'] 	= substr($auth->get_user(), 0, 8);
    $ar_upd['ARDTUM'] 	= oggi_AS_date();

    $update_values = $m_params->update_values;
  
    
    $gg = 0;
    for($i=1; $i <= 6; $i++){
        $ggi = "AXGGI{$i}";
        $ggc = "AXGGC{$i}";
        $ggr = "AXGGR{$i}";
        $ggri = "AXGCR{$i}";
        if(isset($update_values->$ggi))  $gg++;
        if(isset($update_values->$ggc))  $gg++;
        if(isset($update_values->$ggr))  $gg++;
        if(isset($update_values->$ggri)) $gg++;
        
    }
    
     if(isset($update_values->AXTAPO) || isset($update_values->AXTADP) || 
        isset($update_values->AXLMUR) || isset($update_values->AXLMIR) || 
        isset($update_values->AXNAZO) || isset($update_values->AXTAB1) || $gg > 0){
        
        $ar_ax = array();
        
        $ar_ax['AXUSUM'] 	= substr($auth->get_user(), 0, 8);;
        $ar_ax['AXDTUM']   = oggi_AS_date();
        $ar_ax['AXTMUM'] 	= oggi_AS_time();
        if(isset($update_values->AXTAPO))  $ar_ax['AXTAPO'] = $update_values->AXTAPO;
        if(isset($update_values->AXTADP))  $ar_ax['AXTADP'] = $update_values->AXTADP;
        if(isset($update_values->AXNAZO))  $ar_ax['AXNAZO'] = $update_values->AXNAZO;
        if(isset($update_values->AXTAB1))  $ar_ax['AXTAB1'] = $update_values->AXTAB1;
        if(isset($update_values->AXLMUR))  $ar_ax['AXLMUR'] = $update_values->AXLMUR;
        if(isset($update_values->AXLMIR))  $ar_ax['AXLMIR'] = $update_values->AXLMIR;
        for($i=1; $i <= 6; $i++){
            $ggi = "AXGGI{$i}";
            $ggc = "AXGGC{$i}";
            $ggr = "AXGGR{$i}";
            $ggri = "AXGCR{$i}";
            if(isset($update_values->$ggi))  $ar_ax["AXGGI{$i}"] = $update_values->$ggi;
            if(isset($update_values->$ggc))  $ar_ax["AXGGC{$i}"] = $update_values->$ggc;
            if(isset($update_values->$ggr))  $ar_ax["AXGGR{$i}"] = $update_values->$ggr;
            if(isset($update_values->$ggri)) $ar_ax["AXGCR{$i}"] = $update_values->$ggri;
            
        }
        
        if($m_params->g_multi == 'Y'){
             foreach($m_params->multi_selected_id as $k => $v){
                
                 $sql = "SELECT COUNT(*) AS C_ROW
                 FROM {$cfg_mod_DeskUtility['file_est_anag_art']} AX
                 WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$v}'";
                 
                 $stmt = db2_prepare($conn, $sql);
                 $result = db2_execute($stmt);
                 $row = db2_fetch_assoc($stmt);
                 
                 if($row['C_ROW'] > 0){
                     
                     $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art']} AX
                     SET " . create_name_field_by_ar_UPDATE($ar_ax) . "
                     WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$v}'";
                     
                 }else{
                     
                     $ar_ax['AXUSGE'] 	= substr($auth->get_user(), 0, 8);
                     $ar_ax['AXDTGE']   = oggi_AS_date();
                     $ar_ax['AXTMGE'] 	= oggi_AS_time();
                     $ar_ax['AXDT'] 	= $id_ditta_default;
                     $ar_ax['AXART'] 	= $v;
                     
                     $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art']} (" . create_name_field_by_ar($ar_ax) . ") VALUES (" . create_parameters_point_by_ar($ar_ax) . ")";
                     
                 }
                 
                 
                 $stmt = db2_prepare($conn, $sql);
                 echo db2_stmt_errormsg();
                 $result = db2_execute($stmt, $ar_ax);
                 echo db2_stmt_errormsg($stmt);
                
                
            }
            
        }else{
                     
            $sql = "SELECT COUNT(*) AS C_ROW
            FROM {$cfg_mod_DeskUtility['file_est_anag_art']} AX
            WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$m_params->c_art}'";
            
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);
            $row = db2_fetch_assoc($stmt);
            
            if($row['C_ROW'] > 0){
                $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art']} AX
                        SET " . create_name_field_by_ar_UPDATE($ar_ax) . "
                        WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$m_params->c_art}'";
                
            }else{
                
                $ar_ax['AXUSGE'] 	= substr($auth->get_user(), 0, 8);
                $ar_ax['AXDTGE']    = oggi_AS_date();
                $ar_ax['AXTMGE'] 	= oggi_AS_time();
                $ar_ax['AXDT'] 	    = $id_ditta_default;
                $ar_ax['AXART'] 	= $m_params->c_art;
                
                $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art']} (" . create_name_field_by_ar($ar_ax) . ") VALUES (" . create_parameters_point_by_ar($ar_ax) . ")";
                
            }
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ax);
            echo db2_stmt_errormsg($stmt);
            
        }
        
    }
   
    foreach($update_values as $ku=> $vu){
     if (substr("$ku", -3) != '_DE'){ //non faccio niente per i campi di tipo _DE (decodfiche)
        $fd = _diz_v0($ku);
        
        if($fd['su_RI']=='Y') {
          $ar_upd_RI[$fd['f_RI']] 	= $update_values->$ku;
          if(strlen(trim($ar_upd_RI[$fd['f_RI']])) == 0) {
              $ar_upd_RI[$fd['f_RI_A']] = 'A'; // A:Azzera
          } elseif ($fd['type'] =='nf' && $ar_upd_RI[$fd['f_RI']] == 0) {
              $ar_upd_RI[$fd['f_RI_A']] = 'A'; // A:Azzera
            }
          
          //  $ar_upd_RI['aaaa'] 	= $update_values->$ku;
        } else {
         
          if($fd['dt_composta']=='Y'){
              $ar_upd[$fd['anno']]   = substr($update_values->$ku, 0, 4);
              $ar_upd[$fd['mese']]   = substr($update_values->$ku, 4, 2);
              $ar_upd[$fd['giorno']] = substr($update_values->$ku, 6, 4);
          } 
          else{ 
              if($vu === "**azzera**")
                  $ar_upd[$ku] = '';
              else
                  $ar_upd[$ku] = $update_values->$ku;
          }   
          
              
              if(isset($ar_upd['U_M_cb'])){
                  $ar_upd['ARUMTE'] = $ar_upd['U_M_cb'];
                  $ar_upd['ARUMCO'] = $ar_upd['U_M_cb'];
                  $ar_upd['ARUMAL'] = $ar_upd['U_M_cb'];
                  unset($ar_upd['U_M_cb']);
              }
              
              if(isset($ar_upd['U_M'])) unset($ar_upd['U_M']);
              
             
              
            
        }
      } // != _DE  
    } //foreach
    

    /*
    //per fase di test *************
        $ret['success'] = false;
        $ret['error_msg'] = 'Funzione ancora non attiva';
        $ret['row_art'] = _get_data_articolo($cod_art);
        $ret['upd_data'] = $ar_upd;
        echo acs_je($ret);
        exit;
    //per fase di test *************
    */
    
    if(isset($update_values->AXTAPO))  unset($ar_upd['AXTAPO']);
    if(isset($update_values->AXTADP))  unset($ar_upd['AXTADP']);
    if(isset($update_values->AXNAZO))  unset($ar_upd['AXNAZO']);
    if(isset($update_values->AXTAB1))  unset($ar_upd['AXTAB1']);
    if(isset($update_values->AXLMIR))  unset($ar_upd['AXLMIR']);
    if(isset($update_values->AXLMUR))  unset($ar_upd['AXLMUR']);
    for($i=1; $i <= 6; $i++){
        $ggi = "AXGGI{$i}";
        $ggc = "AXGGC{$i}";
        $ggr = "AXGGR{$i}";
        $ggri = "AXGCR{$i}";
        if(isset($update_values->$ggi))  unset($ar_upd["AXGGI{$i}"]); 
        if(isset($update_values->$ggc))  unset($ar_upd["AXGGC{$i}"]); 
        if(isset($update_values->$ggr))  unset($ar_upd["AXGGR{$i}"]); 
        if(isset($update_values->$ggri))  unset($ar_upd["AXGCR{$i}"]); 
        
    }
    
    if (count($ar_upd) > 0){
                  
      $sql = "UPDATE {$cfg_mod_DeskUtility['file_anag_art']} AR
              SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
              WHERE ARDT = '{$id_ditta_default}' {$sql_where}";
      
           
      $stmt = db2_prepare($conn, $sql);
      echo db2_stmt_errormsg();
      $result = db2_execute($stmt, $ar_upd);
      echo db2_stmt_errormsg($stmt);
    }
    
    if (count($ar_upd_RI) > 0){
        
        if($m_params->g_multi == 'Y'){
            
            foreach($m_params->multi_selected_id as $cod){
                
                $ar_ins_copy = array_merge($ar_upd_RI, array());
                
                $ritime = microtime(true);
                $ar_ins_copy['RACDART']  = $cod;
                $ar_ins_copy['RATIME'] = $ritime;
                $ar_ins_copy['RADT'] = $id_ditta_default;
                $sql = "INSERT INTO {$cfg_mod_DeskArt['file_richieste_anag_art']} (" . create_name_field_by_ar($ar_ins_copy) . ") VALUES (" . create_parameters_point_by_ar($ar_ins_copy) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins_copy);
                echo db2_stmt_errormsg($stmt);
                
                $sh = new SpedHistory($m_DeskArt);
                $sh->crea(
                    'pers',
                    array(
                        "RITIME"    => $ritime,
                        "messaggio"	=> 'MOD_ANAG_ART',
                        "vals" => array(
                            "RICDNEW"     => $cod
                        )
                    )
                    );
                
                
            }
            
        }else{
        
            $ritime = microtime(true);
            $ar_upd_RI['RACDART']  = $m_params->c_art;
            $ar_upd_RI['RATIME'] = $ritime;
            $ar_upd_RI['RADT'] = $id_ditta_default;
            $sql = "INSERT INTO {$cfg_mod_DeskArt['file_richieste_anag_art']} (" . create_name_field_by_ar($ar_upd_RI) . ") VALUES (" . create_parameters_point_by_ar($ar_upd_RI) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd_RI);
            echo db2_stmt_errormsg($stmt);
            //print_r($sql); exit;
            
            $sh = new SpedHistory($m_DeskArt);
            $sh->crea(
                'pers',
                array(
                    "RITIME"    => $ritime,
                    "messaggio"	=> 'MOD_ANAG_ART',
                    "vals" => array(
                        "RICDNEW"     => $cod_art
                    )
                )
                );
        
        }
        
        
    }
    

    $ret['success'] = true;
    if($m_params->g_multi != 'Y')
        $ret['row_art'] = _get_data_articolo($cod_art);
    $ret['upd_data'] = $ar_upd;
  
    echo acs_je($ret);
    exit;            
}


// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
        	itemId: 'f_form',
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            items: [ { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						flex:0.5,
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data inser. dal'
						   , margin: '0 8 0 0'
						   , labelWidth :120
						   , name: 'f_data_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						},
						
						<?php for ($i = 1; $i<=9; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 13 0 0' },
						<?php }?>	
						<?php for ($i = 10; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 6 0 0' },
						<?php }?>	
						
						//sfondo
						{ xtype: 'displayfield', value : 'Codice articolo :', margin : '0 0 0 10' },
						   
						  
						 ]
					}, 
					
							
						{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},	
						defaults:{xtype: 'textfield'},					
						items: [
						{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , margin: '0 5 0 0'
						   , labelWidth :120
						   , name: 'f_data_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, 
						<?php for ($i = 1; $i<=15; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_<?php echo $i; ?>', 
						 // fieldLabel : '<?php echo $i?>',
						 // labelAlign: 'top', 
						  width: 20,
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
						
						{ fieldLabel: '', 
						name: 'f_cod_art',
						margin : '0 0 0 10',
						xtype: 'textfield', 
						anchor: '-5',
						flex : 0.1,
						plugins: [new Ux.InputTextMask('AAAAAAAAAAAAAAA', false)]
						 },
						
					/*	{
						
						flex: 1,
			            xtype: 'textfield',
						name: 'f_cod_art',
						margin : '0 0 0 10',
						fieldLabel: '',
						labelWidth :120, 
						anchor: '-5',	
					
						},*/
						
					 /*{
								name: 'f_sfondo',
								xtype: 'combo',
								flex: 2,
								margin : '0 5 0 10',
								fieldLabel: '',
								labelWidth : 70,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: 'select',
						   		//allowBlank: false,								
							    anchor: '-5',
							    value : <?php echo j($tipo); ?>,
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: '1', text : 'Rosso'},
								     	 {id: '2', text : 'Giallo'},
								     	 {id: '3', text : 'Verde'},
								     	 {id: 'N', text : 'Nessuno'},
								     	 
								     	
								    ]
								}
								
						   }*/
								
						]},
						
						/*{
						
						flex: 1,
			            xtype: 'textfield',
						name: 'f_desc_art',
						fieldLabel: 'Descrizione articolo',
						labelWidth :120, 
						anchor: '-5',	
					
						},*/
					{ 
	xtype: 'fieldcontainer',
	layout: { 	type: 'hbox',
				pack: 'start',
				align: 'stretch',
				frame: true},	
					
	items: [
	{
	
		xtype: 'textfield',
		name: 'f_desc_art',
		fieldLabel: 'Descrizione articolo',
		labelWidth :120, 
		anchor: '-5',	
		flex: 0.5,
	
		}, {
			name: 'f_sfg',
			xtype: 'combo',
			flex: 0.25,
			margin : '0 0 0 0',
			fieldLabel: 'Sf. generico',
			labelWidth : 80,
			labelAlign : 'right',
			multiSelect : true,
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',								
			emptyText: 'select',
			//allowBlank: false,								
			anchor: '-5',
			value : <?php echo j($tipo); ?>,
			store: {
				editable: false,
				autoDestroy: true,
				fields: [{name:'id'}, {name:'text'}],
				data: [								    
					 {id: '1', text : 'Rosso'},
					 {id: '2', text : 'Giallo'},
					 {id: '3', text : 'Verde'},
					 {id: 'N', text : 'Nessuno'},
					 
					
				]
			}
			
	   }, {
			name: 'f_sfp',
			xtype: 'combo',
			flex: 0.25,
			labelWidth : 80,
			fieldLabel: 'Sf. utente',
			labelAlign : 'right',
			multiSelect : true,
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',								
			emptyText: 'select',
			//allowBlank: false,								
			anchor: '-5',
			value : <?php echo j($tipo); ?>,
			store: {
				editable: false,
				autoDestroy: true,
				fields: [{name:'id'}, {name:'text'}],
				data: [								    
					 {id: '1', text : 'Rosso'},
					 {id: '2', text : 'Giallo'},
					 {id: '3', text : 'Verde'},
					 {id: 'N', text : 'Nessuno'},
					 
					
				]
			}
			
	   }
	
	]},
					                   
						
					
							 {
					xtype: 'fieldset',
	                title: 'Filtri',
	                layout: 'anchor',
	                flex:1,
	                items: [
						
								
						  { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
						
			            xtype: 'combo',
						name: 'f_fornitore',
						fieldLabel: 'Fornitore',
						flex:0.5,
						anchor: '-15',
						labelWidth : 80,
						minChars: 2,			
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '../base/acs_get_anagrafica.php?fn=get_data&cod=F',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}, {name:'loca'}, {name:'color'}],            	
			            },
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item" style="background-color:{color}">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {loca}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
        		},{
								name: 'f_classe',
								xtype: 'combo',
								flex:0.5,
								multiSelect : true,	
								labelWidth : 80,
								margin : '0 0 0 10',
								fieldLabel: 'Classe',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUCM'), ''); ?>	
								    ]
								}
								
						 }
						   
						]},
  { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
								name: 'f_tipo_parte',
								xtype: 'combo',
								flex:0.5,
								multiSelect : true,	
								labelWidth : 80,
								fieldLabel: 'Tipo/parte',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MTPA'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'f_gruppo',
								xtype: 'combo',
								fieldLabel: 'Gruppo',
								flex:0.5,
								multiSelect : true,	
								labelWidth : 80,
								margin : '0 0 0 10',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUGM'), ''); ?>	
								    ]
								}
								
						 }
						   
						]},
  { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ {
								name: 'f_pianificazione',
								xtype: 'combo',
								fieldLabel: 'Gr. pianificaz.',
								flex:0.5,
								multiSelect : true,	
								labelWidth : 80,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('GRPA'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'f_sottogruppo',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								flex:0.5,
								labelWidth : 80,
								multiSelect : true,
								margin : '0 0 0 10',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUSM'), ''); ?>	
								    ]
								}
								
						 }
						   
						]}						
					 
	             ]}, { 
						xtype: 'fieldcontainer',
						
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						
					  	{
		xtype: 'tabpanel',
		flex:0.6,
		items: [
				  { 	xtype: 'grid',
				  		grid_save_in_F8: 'liv_raggruppamenti',
				  		
				  		get_f8_data: function(){
				  			console.log('get_f8_data called');
				  			
				  			rows = this.getStore().data.items;
                            list_rows = [];
                            
                              for (var i=0; i<rows.length; i++) {
                             	 if(rows[i].get('disabled') != 'Y'){
				            		list_rows.push(rows[i].get('code'));
				            	   }
				            	}
				          
				  			return list_rows;
				  		},
				  		
				  		set_f8_data: function(values){
				  			console.log('set_f8_data called');
				  			if(typeof values != 'undefined' && values.length > 0){
				  					  				 
				  				this.store.each(function(rec){
				  				      if (values.includes(rec.get('code'))) {
									    rec.set('disabled', 'N');
									    rec.set('seq_ord', values.indexOf(rec.get('code')));
									 }else{
									    rec.set('seq_ord', 9);
									 }
								});
							
								this.getStore().sort('seq_ord','ASC');
				  			 
				  			}
				  			
				  		},
				  		
				  		
		 			    title : 'Raggrupamento', 
        				useArrows: true,
        		        rootVisible: false,
        		        loadMask: true,
        		        multiSelect: true,
        		        store: {
        						xtype: 'store',
        						autoLoad:true,
        					 data : [
        							{title: 'Tipo/Parte', disabled : 'N', code : 'ARTPAR'},
							        {title: 'Classe', disabled : 'Y', code : 'ARCLME'},
							        {title: 'Gruppo', disabled : 'Y', code : 'ARGRME'},
							        {title: 'Sottogruppo', disabled : 'Y', code : 'ARSGME'},
							        {title: 'Gruppo pianificazione', disabled : 'Y', code : 'ARTAB1'},
							        {title: 'Fornitore', disabled : 'Y', code : 'ARFOR1'}

							        ],
				   	fields: ['seq_ord', 'title', 'code', 'disabled']
						

				}, //store
		
				columns: [	
				 
					{
			                hidden : true,
			                dataIndex: 'disabled', 
			                flex: 1
			             },
		    		{
			                header   : 'Raggruppamento elenco articoli',
			                dataIndex: 'title', 
			                flex: 1
			             },
		    		{
			               text: 'Disabilita',
			               width: 100,
			               renderer: function(value, p, record){
			               
			               if (record.get('disabled') == 'N') return '<img src=<?php echo img_path("icone/16x16/button_yellow_play.png") ?>  width=15>';
			               if (record.get('disabled') == 'Y') return '<img src=<?php echo img_path("icone/16x16/button_red_play.png") ?>  width=15>';
			               
			               }   
			                
		             }
		    		
		    		
				]
				
				, listeners : {
				
				 itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  
						     if (rec.get('disabled') == 'N')
								rec.set('disabled', 'Y');
						   else
							    rec.set('disabled', 'N' );

						  }
					  }	 
				
				
				
				}
			
				,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
						       if (record.get('disabled')=='Y')
						           return ' segnala_riga_disabled';
						       			           		
						           return '';																
					         }, plugins: {
					                ptype: 'gridviewdragdrop',
					                dragGroup: 'sped_grid',
					                dropGroup: 'sped_grid'
				            }   
					    },
			
											    
				    		
                    }, //grid

		  
					{
				xtype: 'panel',
				title: 'Colonne', 
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				width: 165,
												
				items: [
							
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Attivit&agrave',
                            name: 'f_att',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Allegati',
                            name: 'f_allegati',	
                            labelWidth: 150,
                            checked : true,
                            xtype: 'checkboxfield',
                            inputValue: 'Y'         
                        }
						   
						
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Listini vendita',
                            name: 'f_list_ven',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Listini acquisto',
                            name: 'f_list_acq',	
                            labelWidth: 150,
                            checked : true,
                            xtype: 'checkboxfield',
                            inputValue: 'Y'         
                        }
						   
						
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Distinta componenti',
                            name: 'f_dist',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Elenco padri',
                            name: 'f_elenco',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            checked : true,
                            inputValue: 'Y'         
                        }
						   
						
						]}, 	{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Cicli/lavorazioni',
                            name: 'f_cicli',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Configuratore',
                            name: 'f_conf',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            checked : true,
                            inputValue: 'Y'         
                        }
						   
						
						]}, {
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Traduzione',
                            name: 'f_trad',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Varianti anagrafiche',
                            name: 'f_var',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            checked : true,
                            inputValue: 'Y'         
                        }
						   
						
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Regole abbinamento',
                            name: 'f_abb',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true, margin: '0 100 0 5',           
                        },
					
						   
						
						]}
				]
			},
		   	
		]
	} ,
               {
							 xtype: 'fieldcontainer',
							 flex:0.4, 
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    
						{
								name: 'f_lingua',
								xtype: 'combo',
								fieldLabel: 'Lingua',
								flex:1,
								labelWidth : 50,
								margin : '0 0 10 10',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($cfg_mod_DeskUtility['ar_lingue'], ''); ?>	
								    ]
								}
								
						 }, 	      
                   { 
						xtype: 'fieldset',
						title: 'Flags',
						//border : false,
						margin : '0 0 0 5',
	                    layout: 'anchor',
	                    items: [
						    {
							name: 'f_sos',
							xtype: 'radiogroup',
							fieldLabel: 'Sospesi',
							labelWidth: 45,
							flex: 1,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'Y'
		                          , width: 45
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'T'
		                          , width: 60                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Esclusi'	
		                          , inputValue: 'N'
		                          , width: 60
		                          , checked: true
		                          
		                        }]
						}	, {
                            fieldLabel: 'In esaurimento',
                            name: 'f_esau',	
                            labelWidth: 100,
                            xtype: 'checkboxfield',
                            inputValue: 'E',             
                        },{
                            fieldLabel: 'In avviamento',
                            name: 'f_avv',
                            labelWidth: 100,
                            xtype: 'checkboxfield',
                            inputValue: 'A',             
                        },
                        {
                            fieldLabel: 'Codifica in corso',
                            name: 'f_corso',	
                            labelWidth: 100,
                            xtype: 'checkboxfield',
                            inputValue: 'C'         
                        }
            						
						]}
						]}
						   
						]}
        		    		        
				
					        
				],
			buttons: [
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "ANAG_ART");  ?>
			{
	            text: 'Indici',
	            iconCls: 'icon-bookmark-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){
	            	
	            	rows = this.up('form').down('grid').getStore().data.items;
                            list_rows = [];
                            
                              for (var i=0; i<rows.length; i++) {
                             	 if(rows[i].get('disabled') != 'Y'){
				            		list_rows.push({
				            		livello : rows[i].get('code')
				            		});
				            	   }
				            	}
	            	
	            		acs_show_win_std('Indici di documentazione', 'acs_anag_artd_crea_nuovo.php?fn=open_indici', {
	            		form_open: form.getValues(),
	            		livelli : list_rows}, 
	            		800, 400, null, 'icon-bookmark-16');
	            		this.up('window').close();
		            
			        } 
	                      	                	                
	            }
	            },

			  {
	         	xtype: 'splitbutton',
	            text: 'Reports',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        		{ xtype: 'button',
		        		  text: 'Report anagrafica',
			              scale: 'medium',
			              handler: function() {
		                    form = this.up('form').getForm();
	                        this.up('form').submit({
	                        url: 'acs_anag_art_report.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',                        
	                        params: {
	                            form_values: Ext.encode(form.getValues())
						    }
                  			});            	                	     
            	         }
		        		} ,
		        		
		        		{ xtype: 'button',
		        		  text: 'Report cicli',
			              scale: 'medium',
			              handler: function() {
		                      form = this.up('form').getForm();
	                          this.up('form').submit({
	                          url: 'acs_anag_art_report.php',
                          	  target: '_blank', 
	                          standardSubmit: true,
	                          method: 'POST',                        
	                          params: {
	                              form_values: Ext.encode(form.getValues()),
	                              rp_cicli: 'Y'
  						      }
                  			});            	                	     
            	          }
			         },
			         { xtype: 'button',
		        		  text: 'Report formati',
			              scale: 'medium',
			                handler: function() {
    		            	    form = this.up('form').getForm();
    		                    if (form.isValid()){
            	                acs_show_win_std('Report formati', 'acs_anag_art_formati_report.php?fn=open_form', {
    	        				filter: form.getValues()
    	    					},		        				 
    	        				400, 200, {}, 'icon-print-16');	
		        			} 
		            
		                
		           		 }
			         }
		        	] <!-- end items -->
	        	} <!-- end men� -->
        	} <!-- end split button -->
			        
			         ,{
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){	            	
	            	
            	 	  var rows = this.up('form').down('grid').getStore().data.items;
                      var list_rows = [];
                            
                      for (var i=0; i<rows.length; i++) {
                     	 if(rows[i].get('disabled') != 'Y'){
		            		list_rows.push({
		            		livello : rows[i].get('code'),
		            		
		            		});
		            	   }
		            	}
		            	
		            	if(this.up('window').fromObjectId != null){
    		            	var tree_panel = Ext.getCmp(this.up('window').fromObjectId);
    		            	tree_panel.getStore().proxy.extraParams.form_ep = form.getValues();
    		            	tree_panel.getStore().proxy.extraParams.livelli = list_rows;
    		            	tree_panel.getStore().load();
		            	} else {
    		            	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', null, {
        		            	form_open: form.getValues(), 
        		            	livelli : list_rows,
        		            	win_id: win_id
    		            	}, null, null);
    		            }
		            	
			            this.up('window').hide();
			        }            	                	                
	            }
	        }	        
	        ], 
	        
	        
	        listeners: {
			    afterrender: function(comp) {
			        var win = comp.up('window');
			        win.tools.close.hide();
			        var myCloseTool = Ext.create('Ext.panel.Tool', {
			            type: 'close',
			            handler: function() {
			                if(win.fromObjectId != null){ //aperto da un tree
			                	win.hide();
			                }
			                else {						 //aperto da icona su menu principale
			                	win.close();
			                }
			            }
			        })
			        win.header.insert(3, myCloseTool); // number depends on other items in header
			    }
}
	        
	        
	        
				
        }
]}
<?php	
	exit;
} //open_form_filtri


function get_desc_livello($tipo_liv){
	
	global $main_module;
	
	switch ($tipo_liv){
		case 'ARTPAR' : 
			$task = 'D_TIPO_PARTE';
		break;
		case 'ARCLME' :
			$task = 'D_CLASSE';
		break;
		case 'ARGRME' : 
			$task = 'D_GRUPPO';
		break;
		case 'ARSGME':
			$task = 'D_SOTTOGRUPPO';
		break;
		case 'ARTAB1':
		    $task = 'D_GRUPPOPIANI';
		    break;
		case 'ARFOR1':
		    $task = 'D_FORNITORE';
		    break;
		
	}

	return $task;
}

function get_desc_liv_tree($tipo_liv){
    
    global $main_module;
    
    switch ($tipo_liv){
        case 'ARTPAR' :
             $desc = 'Tipo parte';
            break;
        case 'ARCLME' :
            $desc = 'Classe';
            break;
        case 'ARGRME' :
            $desc = 'Gruppo';
            break;
        case 'ARSGME':
            $desc = 'Sottogr.';
            break;
        case 'ARTAB1':
             $desc = 'Gr. pianific.';
            break;
        case 'ARFOR1':
             $desc = 'Forn.';
            break;
            
    }
    
    return $desc;
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data'){
####################################################	
	$ret = array();
	$ar =  array();
	$n_children = 'children';
	$m_params = acs_m_params_json_decode();
	$form_ep = $m_params->form_ep;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql_where = '';	
	$sql_join = '';
	$ar_where = array();
	$offset = "";

	
	$livelli = $m_params->livelli;
		
	$sql_where.= sql_where_by_combo_value('ARTPAR', $form_ep->f_tipo_parte);
	$sql_where.= sql_where_by_combo_value('ARCLME', $form_ep->f_classe);
	$sql_where.= sql_where_by_combo_value('ARGRME', $form_ep->f_gruppo);
	$sql_where.= sql_where_by_combo_value('ARSGME', $form_ep->f_sottogruppo);
	$sql_where.= sql_where_by_combo_value('ARTAB1', $form_ep->f_pianificazione);
	$sql_where.= sql_where_by_combo_value('ARFOR1', $form_ep->f_fornitore);
	
		
	if (strlen($form_ep->f_data_dal) > 0)
	    $sql_where.= " AND ARDTGE >= $form_ep->f_data_dal ";
	if (strlen($form_ep->f_data_al) > 0)
        $sql_where.= " AND ARDTGE <= $form_ep->f_data_al ";
	
        
    if($form_ep->f_cod_art != ''){
        $sql_where.= " AND UPPER(ARART) LIKE '{$form_ep->f_cod_art}'";
        
    }elseif($m_params->radice != ''){
        $sql_where.= artd_ARART_add_where($m_params->radice);
         
    }else{
	
	$text = '';
	for($i = 1; $i <= 15; $i++){
	    $filtro = 'f_'.$i;
	    if($form_ep->$filtro == '')
	        $form_ep->$filtro = '_';
	    $text .= $form_ep->$filtro;
     }
     
     if($text != "_______________")
        $sql_where.= " AND UPPER(ARART) LIKE '" . strtoupper($text) . "' ";
     
        }
  
        
	$ar_esau = array();
	if ($form_ep->f_esau == 'E')
	    array_push($ar_esau, $form_ep->f_esau);
	if ($form_ep->f_avv == 'A')
	    array_push($ar_esau, $form_ep->f_avv);
	if ($form_ep->f_corso == 'C')
	    array_push($ar_esau, $form_ep->f_corso);
    if (count($ar_esau) > 0)
        $sql_where.= sql_where_by_combo_value('ARESAU', $ar_esau);
    

	
	if (strlen($form_ep->f_desc_art) > 0)
		$sql_where.= " AND UPPER(ARDART) LIKE '%" . strtoupper($form_ep->f_desc_art) . "%' ";
	
		if($form_values->f_scaduti == 'N'){ //esclusi scaduti
		    $sql_where .= "AND BSDTSR >= ".oggi_AS_date();
		}
		if($form_values->f_scaduti == 'Y'){ //solo scaduti
		    $sql_where .= "AND BSDTSR < ".oggi_AS_date();
		}

	if ($form_ep->f_sospeso == 'Y')
		$sql_where.= " AND ARSOSP = 'S'";
	if ($form_ep->f_sospeso == 'N')
	    $sql_where.= " AND ARSOSP <> 'S'";
	
	 
	    if($form_ep->f_sfg[0] == 'N')
	        $sql_where .= " AND (AXSFON IS NULL  OR AXSFON = '')";
	     else  
	       $sql_where.= sql_where_by_combo_value('AXSFON', $form_ep->f_sfg);
	   
	   if($form_ep->f_sfp[0] == 'N')
	       $sql_where .= " AND (AUSFAR IS NULL  OR AUSFAR = '')";
	   else
	       $sql_where.= sql_where_by_combo_value('AUSFAR', $form_ep->f_sfp);
	
   /* if($form_ep->f_sfg == '1')
        $sql_where .= " AND AXSFON = '1'";
    elseif($form_ep->f_sfg == '2')
        $sql_where .= " AND AXSFON = '2'";
    elseif($form_ep->f_sfg == '3')
        $sql_where .= " AND AXSFON = '3'";
    elseif($form_ep->f_sfg == 'N')
        $sql_where .= " AND (AXSFON IS NULL  OR AXSFON = '')";*/
	
   /* if($form_ep->f_sfp == '1')
        $sql_where .= " AND AUSFAR = '1'";
    elseif($form_ep->f_sfp == '2')
        $sql_where .= " AND AUSFAR = '2'";
    elseif($form_ep->f_sfp == '3')
        $sql_where .= " AND AUSFAR = '3'";
    elseif($form_ep->f_sfp == 'N')
        $sql_where .= " AND (AXSFON IS NULL  OR AXSFON = '')";*/
	   
	
	 $where = "";
	if (isset($form_ep->f_lingua) && $form_ep->f_lingua != ''){
	    $where .= " WHERE ALLING = '{$form_ep->f_lingua}'";
	}else{
	    
	    foreach($cfg_mod_DeskUtility['ar_lingue'] as $k => $v){
	        $ar_lng[] = $v['id'];
	    }
	       
	    $where .= " WHERE ALLING IN (". sql_t_IN($ar_lng) . ")";

	
	}

	//if($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
	if($_REQUEST['node'] == 'root'){
		
		foreach($livelli as $value_ar){
			if($value_ar->livello == 'ARTPAR'){
				$ar_select .=  'ARTPAR, TA_TIPO_PARTE.TADESC AS D_TIPO_PARTE, ';
				$ar_group_by.= 'ARTPAR, TA_TIPO_PARTE.TADESC, ';
				$ar_order_by .= 'TA_TIPO_PARTE.TADESC, ';
				
			}
			if($value_ar->livello  == 'ARCLME'){
				$ar_select .=  'ARCLME, TA_CLASSE.TADESC AS D_CLASSE, ';
				$ar_group_by.= 'ARCLME, TA_CLASSE.TADESC, ';
				$ar_order_by .= 'TA_CLASSE.TADESC, ';
				
			}
			if($value_ar->livello ==  'ARGRME'){
				$ar_select .=  'ARGRME, TA_GRUPPO.TADESC AS D_GRUPPO, ';
				$ar_group_by.= 'ARGRME, TA_GRUPPO.TADESC, ';
				$ar_order_by .= 'TA_GRUPPO.TADESC, ';
				
			}	
			if($value_ar->livello ==  'ARSGME'){
				$ar_select .=  'ARSGME, TA_SOTTOGRUPPO.TADESC AS D_SOTTOGRUPPO, ';
				$ar_group_by.= 'ARSGME, TA_SOTTOGRUPPO.TADESC, ';
				$ar_order_by .= 'TA_SOTTOGRUPPO.TADESC, ';
				
			}	
			if($value_ar->livello ==  'ARTAB1'){
			    $ar_select .=  'ARTAB1, TA_GRUPPOPIANI.TADESC AS D_GRUPPOPIANI, ';
			    $ar_group_by.= 'ARTAB1, TA_GRUPPOPIANI.TADESC, ';
			    $ar_order_by .= 'TA_GRUPPOPIANI.TADESC, ';
			  
			}
			if($value_ar->livello ==  'ARFOR1'){
			    $ar_select .=  'ARFOR1, CF_FORNITORE.CFRGS1 AS D_FORNITORE, ';
			    $ar_group_by.= 'ARFOR1, CF_FORNITORE.CFRGS1, ';
			    $ar_order_by .= 'CF_FORNITORE.CFRGS1, ';
			  
			}
				
		}
	
		$sql_group_by ="GROUP BY $ar_group_by ARDT "; //  TA_ALLEGATI.NR_AL, L_VEN.NR_LV, L_ACQ.NR_LA, CL.NR_CL, DB.NR_DB";
		$sql_order_by ="ORDER BY $ar_order_by ARDT ";
		$sql_campi_select = "COUNT(*) AS ARTICOLI, $ar_select ARDT "; //, TA_ALLEGATI.NR_AL AS NR_AL, L_VEN.NR_LV AS NR_LV, L_ACQ.NR_LA AS NR_LA, CL.NR_CL AS NR_CL, DB.NR_DB AS NR_DB";
	
	}else{
	    
		$ar_node = explode("|", $_REQUEST['node']);
		
		$sql_campi_select = "AR.* , AXSFON, AUSFAR ";
		
	 if($form_ep->f_allegati == 'Y'){
	   $sql_campi_select .= " , TA_ALLEGATI.NR_AL AS NR_AL ";
	   $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_AL, TADT, TATAID, TAKEY1
                       FROM {$cfg_mod_DeskUtility['file_tabelle']}
                       GROUP BY TAKEY1, TADT, TATAID) TA_ALLEGATI
                 	ON TA_ALLEGATI.TADT = AR.ARDT AND TA_ALLEGATI.TATAID = 'ARTAL' AND TA_ALLEGATI.TAKEY1 = AR.ARART";
	 }
	 
	 if($form_ep->f_list_ven == 'Y'){
	     
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_listini']} LV WHERE LV.LIDT = AR.ARDT AND LV.LITPLI = 'V' AND LV.LIART = AR.ARART ) AS NR_LV ";
	  /* $sql_campi_select .= " , L_VEN.NR_LV AS NR_LV ";
	   $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_LV, LIDT, LITPLI, LIART
                       FROM {$cfg_mod_DeskUtility['file_listini']}
                       GROUP BY LIART, LIDT, LITPLI) L_VEN
                 	ON L_VEN.LIDT = AR.ARDT AND L_VEN.LITPLI = 'V' AND L_VEN.LIART = AR.ARART";*/
	 }
	 
	 if($form_ep->f_list_acq == 'Y'){
	     
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_listini']} LA WHERE LA.LIDT = AR.ARDT AND LA.LITPLI = 'A' AND LA.LIART = AR.ARART ) AS NR_LA";
	   /*$sql_campi_select .= " , L_ACQ.NR_LA AS NR_LA ";
	   $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_LA, LIDT, LITPLI, LIART
                       FROM {$cfg_mod_DeskUtility['file_listini']}
                       GROUP BY LIART, LIDT, LITPLI) L_ACQ
                 	ON L_ACQ.LIDT = AR.ARDT AND L_ACQ.LITPLI = 'A' AND L_ACQ.LIART = AR.ARART";*/
	 }
	 
	 if($form_ep->f_cicli == 'Y'){
	     
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_cicli']} CL WHERE CL.CIDT = AR.ARDT AND  CL.CIART = AR.ARART ) AS NR_CL";
	  /* $sql_campi_select .= " , CL.NR_CL AS NR_CL ";
	   $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_CL, CIDT, CIART
                       FROM {$cfg_mod_DeskUtility['file_cicli']}
                       GROUP BY CIDT, CIART) CL
                 	ON CL.CIDT = AR.ARDT AND CL.CIART = AR.ARART";*/
	 }
	 
	 if($form_ep->f_dist == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB WHERE DB.DBDT = AR.ARDT AND  DB.DBART = AR.ARART ) AS NR_DB";
	   /*$sql_campi_select .= " , DB.NR_DB AS NR_DB ";
	   $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_DB, DBDT, DBART
                       FROM {$cfg_mod_DeskUtility['file_distinta_base']}
                       GROUP BY DBDT, DBART) DB
                 	ON DB.DBDT = AR.ARDT AND DB.DBART = AR.ARART";*/
	 }
	 
	 if($form_ep->f_elenco == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_distinta_base']} DBF WHERE DBF.DBDT = AR.ARDT AND  DBF.DBCOMP = AR.ARART ) AS NR_DBF";
	   /*$sql_campi_select .= " , DBF.NR_DBF AS NR_DBF ";
	   $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_DBF, DBDT, DBCOMP
                       FROM {$cfg_mod_DeskUtility['file_distinta_base']}
                       GROUP BY DBDT, DBCOMP) DBF
                 	ON DBF.DBDT = AR.ARDT AND DBF.DBCOMP = AR.ARART";*/
	 }
	 
	 if($form_ep->f_trad == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_anag_art_trad']} AL WHERE AL.ALDT = AR.ARDT AND AL.ALART = AR.ARART ) AS N_TRAD";
	   /*$sql_campi_select .= " , AL_TRAD.N_TRAD AS N_TRAD ";
	   $sql_join .= " LEFT OUTER JOIN (
        	            SELECT DISTINCT COUNT(*) AS N_TRAD, ALDT, ALART 
        	            FROM {$cfg_mod_DeskUtility['file_anag_art_trad']} AL
                        {$where}
        	            GROUP BY ALDT, ALART) AL_TRAD
    	            ON AL_TRAD.ALDT = AR.ARDT AND AL_TRAD.ALART = AR.ARART  ";*/
	 }
	 
	 if($form_ep->f_att == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_assegna_ord']} ATT WHERE ATT.ASDT = AR.ARDT AND ATT.ASDOCU = AR.ARART ) AS N_ATT";
       /*$sql_campi_select .= " , ATT_OPEN.N_ATT AS N_ATT ";
       $sql_join .= " LEFT OUTER JOIN (
        	            SELECT COUNT(*) AS N_ATT, ASDOCU 
        	            FROM {$cfg_mod_DeskUtility['file_assegna_ord']}
                        GROUP BY ASDOCU) ATT_OPEN
    	            ON ATT_OPEN.ASDOCU = AR.ARART  ";*/
	 }
	 
	 if($form_ep->f_var == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_variabili']} AM WHERE AM.AMDT = AR.ARDT AND AM.AMART = AR.ARART AND (AM.AMVAR1 <> '' OR AM.AMVAR2 <> '' OR AM.AMVAR3 <> '' OR AM.AMVAR4 <> '' OR AM.AMVAR5 <> '' OR AM.AMVAR6 <> '' OR AM.AMVAR7 <> '' OR AM.AMVAR8 <> '' OR AM.AMVAR9 <> '' OR AM.AMVAR10 <> '')) AS NR_VAR";
       /*$sql_campi_select .= " ,AM.NR_VAR AS NR_VAR ";
       $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_VAR, AMDT, AMART
                       FROM {$cfg_mod_DeskUtility['file_variabili']}
                       WHERE AMDT = '{$id_ditta_default}' AND (AMVAR1 <> '' OR AMVAR2 <> '' OR AMVAR3 <> '' OR AMVAR4 <> '' OR AMVAR5 <> '' OR AMVAR6 <> '' OR AMVAR7 <> '' OR AMVAR8 <> '' OR AMVAR9 <> '' OR AMVAR10 <> '')
                       GROUP BY AMDT, AMART) AM
                 	ON AM.AMDT = AR.ARDT AND AM.AMART = AR.ARART";*/
	 }
	 
	 if($form_ep->f_abb == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA_AB WHERE TA_AB.TADT = AR.ARDT AND TA_AB.TAID = 'ABAR' AND TA_AB.TACOR1 = AR.ARART ) AS NR_AB";
      /* $sql_campi_select .= " , TA_AB.NR_AB AS NR_AB ";
       $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_AB, TADT, TAID, TACOR1
                       FROM {$cfg_mod_DeskUtility['file_tab_sys']}
                       GROUP BY TADT, TAID, TACOR1) TA_AB
                 	ON TA_AB.TADT = AR.ARDT AND TA_AB.TAID = 'ABAR' AND TA_AB.TACOR1 = AR.ARART
                 ";*/
	 }
		
		$sql_group_by = "";
		$sql_order_by = "ORDER BY ARART";
		
		
			
		$where_liv = '';
		$cliv = 0;
		foreach($livelli as $k => $v){
		    $where_liv .= " AND {$v->livello} = ?";
		    $ar_where[] = $ar_node[$cliv];
		    $cliv++;
		}
		
		/*$sql_count ="SELECT COUNT(*) AS ARTICOLI
		FROM {$cfg_mod['file_anag_art']} AR
		LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_TIPO_PARTE
		ON TA_TIPO_PARTE.TADT = AR.ARDT AND TA_TIPO_PARTE.TANR = AR.ARTPAR AND TA_TIPO_PARTE.TAID = 'MTPA'
		LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_CLASSE
		ON TA_CLASSE.TADT = AR.ARDT AND TA_CLASSE.TANR = AR.ARCLME AND TA_CLASSE.TAID = 'MUCM'
		LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_GRUPPO
		ON TA_GRUPPO.TADT = AR.ARDT AND TA_GRUPPO.TANR = AR.ARGRME AND TA_GRUPPO.TAID = 'MUGM'
		LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_SOTTOGRUPPO
		ON TA_SOTTOGRUPPO.TADT = AR.ARDT AND TA_SOTTOGRUPPO.TANR = AR.ARSGME AND TA_SOTTOGRUPPO.TAID = 'MUSM'
		LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_GRUPPOPIANI
		ON TA_GRUPPOPIANI.TADT = AR.ARDT AND TA_GRUPPOPIANI.TANR = AR.ARTAB1 AND TA_GRUPPOPIANI.TAID = 'GRPA'
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
		ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
		$sql_join
		WHERE ARDT = '{$id_ditta_default}' {$sql_where}  {$where_liv}
		";
		
		
		$stmt_count = db2_prepare($conn, $sql_count);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt_count, $ar_where);
		
		$row_count = db2_fetch_assoc($stmt_count);
		
		if($row_count['ARTICOLI'] > 500){
		    
		    if(substr($_REQUEST['node'], -1, 1) == '#'){
		       
		        switch (substr($_REQUEST['node'], -2, 1)){
		            case '1' :
		                $offset = "OFFSET 1";
		                break;
		            case '2' :
		                $offset = "OFFSET 501" ;
		                break;
		            case '3' :
		                $offset = "OFFSET 1001";
		                break;
		                
		        }
		    }
		        
		    
		    $ret = array(
		        array("id" => "{$_REQUEST['node']}#1#", "task" => "Da 1 a 500"),
		        array("id" => "{$_REQUEST['node']}#2#", "task" => "Da 501 a 1000")
		        );
		    
		    echo acs_je(array('success' => true, 'children' => $ret));
		   exit;
		    
		}*/
			
			
	}
	 	
            	
	$utente = trim($auth->get_user());
	$limit = "500";
	$sql = "SELECT $sql_campi_select 
			FROM {$cfg_mod['file_anag_art']} AR
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_TIPO_PARTE
				    ON TA_TIPO_PARTE.TADT = AR.ARDT AND TA_TIPO_PARTE.TAID = 'MTPA' AND TA_TIPO_PARTE.TANR = AR.ARTPAR 
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_CLASSE
				    ON TA_CLASSE.TADT = AR.ARDT AND TA_CLASSE.TAID = 'MUCM' AND TA_CLASSE.TANR = AR.ARCLME  
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_GRUPPO
				    ON TA_GRUPPO.TADT = AR.ARDT AND TA_GRUPPO.TAID = 'MUGM AND TA_GRUPPO.TANR = AR.ARGRME '
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_SOTTOGRUPPO
				    ON TA_SOTTOGRUPPO.TADT = AR.ARDT AND TA_SOTTOGRUPPO.TAID = 'MUSM' AND TA_SOTTOGRUPPO.TANR = AR.ARSGME
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_GRUPPOPIANI
                    ON TA_GRUPPOPIANI.TADT = AR.ARDT AND TA_GRUPPOPIANI.TAID = 'GRPA' AND TA_GRUPPOPIANI.TANR = AR.ARTAB1
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
                    ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art']} AX
		            ON AX.AXDT = AR.ARDT AND AR.ARART = AX.AXART
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
		            ON AU.AUDT = AR.ARDT AND AU.AUART = AR.ARART AND AU.AUUTEN = '{$utente}'
             $sql_join
	        WHERE ARDT = '{$id_ditta_default}' {$sql_where}  {$where_liv}
	        $sql_group_by $sql_order_by
            LIMIT {$limit} {$offset}";

	      
	foreach($livelli as $k => $v){	 
       	    $str_liv['f_liv'.$k] = $v->livello; 
    }
  
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_where);
	
	$count_rows = 0;
	$count = 0;
	
	while ($row = db2_fetch_assoc($stmt)) {
		
		$tmp_ar_id = array();
		
		
		//print_r($liv);
		if (isset($str_liv['f_liv0'])){
			$liv0_v = trim($row[$str_liv['f_liv0']]); 
			$task0 = $row[get_desc_livello($str_liv['f_liv0'])];
			$desc0 = get_desc_liv_tree($str_liv['f_liv0']);
		}
		if (isset($str_liv['f_liv1'])){
			$liv1_v = trim($row[$str_liv['f_liv1']]);
			$task1 = $row[get_desc_livello($str_liv['f_liv1'])];
			$desc1 = get_desc_liv_tree($str_liv['f_liv1']);
		}
		if (isset($str_liv['f_liv2'])){
			$liv2_v = trim($row[$str_liv['f_liv2']]);   
			$task2 = $row[get_desc_livello($str_liv['f_liv2'])];
			$desc2 = get_desc_liv_tree($str_liv['f_liv2']);
		}
		if (isset($str_liv['f_liv3'])){
			$liv3_v = trim($row[$str_liv['f_liv3']]);
			$task3 = $row[get_desc_livello($str_liv['f_liv3'])];
			$desc3 = get_desc_liv_tree($str_liv['f_liv3']);
		}
		if (isset($str_liv['f_liv4'])){
		    $liv4_v = trim($row[$str_liv['f_liv4']]);
		    $task4 = $row[get_desc_livello($str_liv['f_liv4'])];
		    $desc4 = get_desc_liv_tree($str_liv['f_liv4']);
		}
		if (isset($str_liv['f_liv5'])){
		    $liv5_v = trim($row[$str_liv['f_liv5']]);
		    $task5 = $row[get_desc_livello($str_liv['f_liv5'])];
		    $desc5 = get_desc_liv_tree($str_liv['f_liv5']);
		}
		
		// LIVELLO 0 (tipo parte)
		if (count($str_liv) > 0) {
		$s_ar = &$ar;
		$liv 	= $liv0_v;
		$tmp_ar_id[] = $liv;
		if (is_null($s_ar[$liv])){
			$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_0", "task"=> $liv);
			if($task0 == '')
			    $s_ar[$liv]["task"] = 'Non definito';
			else 
			    $s_ar[$liv]["task"]   = $task0; //$task0. " [" . $liv . "]";
			   
			if($liv == ''){ 
			    $s_ar[$liv]["codice"] = "[ND]";
			    $s_ar[$liv]["cod_nota"] = "{$desc0} [ND]";
			}else{ 
			     $s_ar[$liv]["codice"]   = " [" . $liv . "]";
			     $s_ar[$liv]["cod_nota"] = "{$desc0} [{$liv}] ";
			}
			$s_ar[$liv]["classe"]   = $str_liv['f_liv0'];

		}
		
		sum_values($s_ar[$liv], $row);
		$s_ar = &$s_ar[$liv][$n_children];
		
		}
		
		
		
		if (count($str_liv) > 1) {
			// LIVELLO 1 (classe)
			$liv 	= $liv1_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_1", "task"=> $liv);
				if($task1 == '')
				    $s_ar[$liv]["task"] = 'Non definito';
				 else
				     $s_ar[$liv]["task"]   = $task1; //$task0. " [" . $liv . "]";
				        
				     if($liv == ''){
				         $s_ar[$liv]["codice"] = "[ND]";
				         $s_ar[$liv]["cod_nota"] = "{$desc1} [ND]";
				     }else{
				         $s_ar[$liv]["codice"]   = " [" . $liv . "]";
				         $s_ar[$liv]["cod_nota"] = "{$desc1} [{$liv}]";
				     }
				  
				$s_ar[$liv]["classe"]   = $str_liv['f_liv1'];
				
			}
			sum_values($s_ar[$liv], $row);
			$s_ar = &$s_ar[$liv][$n_children];
		
		}		
		
		

		if (count($str_liv) > 2) {
			// LIVELLO 2 (GRUPPO)
			$liv 	= $liv2_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_2", "task"=> $liv);
				if($task2 == '')
				   $s_ar[$liv]["task"] = 'Non definito';
				else
				   $s_ar[$liv]["task"]   = $task2; //$task0. " [" . $liv . "]";
				        
				   if($liv == ''){
				       $s_ar[$liv]["codice"] = "[ND]";
				       $s_ar[$liv]["cod_nota"] = "{$desc2} [ND]";
				   }else{
				       $s_ar[$liv]["codice"]   = " [" . $liv . "]";
				       $s_ar[$liv]["cod_nota"] = "{$desc2} [{$liv}]";
				   }
			
				$s_ar[$liv]["classe"]   = $str_liv['f_liv2'];
			
			}
			
			sum_values($s_ar[$liv], $row);
			$s_ar = &$s_ar[$liv][$n_children];
			
		}

		
		if (count($str_liv) > 3) {
			// LIVELLO 3 (SOTTOGRUPPO)
			$liv 	= $liv3_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_3", "task"=> $liv);
				if($task3 == '')
				    $s_ar[$liv]["task"] = 'Non definito';
				 else
				    $s_ar[$liv]["task"]   = $task3; //$task0. " [" . $liv . "]";
				        
				    if($liv == ''){
				        $s_ar[$liv]["codice"] = "[ND]";
				        $s_ar[$liv]["cod_nota"] = "{$desc3} [ND]";
				    }else{
				        $s_ar[$liv]["codice"]   = " [" . $liv . "]";
				        $s_ar[$liv]["cod_nota"] = "{$desc3} [{$liv}]";
				    }
	
				$s_ar[$liv]["classe"]   = $str_liv['f_liv3'];
				
			}
				
			sum_values($s_ar[$liv], $row);
			$s_ar = &$s_ar[$liv][$n_children];
			
		}
		
		if (count($str_liv) > 4) {
		    // LIVELLO 4 
		    $liv 	= $liv4_v;
		    $tmp_ar_id[] = $liv;
		    if (is_null($s_ar[$liv])){
		        $s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_4", "task"=> $liv);
		        if($task4 == '')
		            $s_ar[$liv]["task"] = 'Non definito';
		        else
		            $s_ar[$liv]["task"]   = $task4; //$task0. " [" . $liv . "]";
		                
		            if($liv == ''){
		                $s_ar[$liv]["codice"] = "[ND]";
		                $s_ar[$liv]["cod_nota"] = "{$desc4} [ND]";
		            }else{
		                $s_ar[$liv]["codice"]   = " [" . $liv . "]";
		                $s_ar[$liv]["cod_nota"] = "{$desc4} [{$liv}]";
		            }
	
		         $s_ar[$liv]["classe"]   = $str_liv['f_liv4'];
		                        
		    }
		    
		    sum_values($s_ar[$liv], $row);
		    $s_ar = &$s_ar[$liv][$n_children];
		    
		}
		
		if (count($str_liv) > 5) {
		    // LIVELLO 5
		    $liv 	= $liv5_v;
		    $tmp_ar_id[] = $liv;
		    if (is_null($s_ar[$liv])){
		        $s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_5", "task"=> $liv);
		        if($task5 == '')
		            $s_ar[$liv]["task"] = 'Non definito';
		        else
		            $s_ar[$liv]["task"]   = $task5; //$task0. " [" . $liv . "]";
		                
		            if($liv == ''){
		                $s_ar[$liv]["codice"] = "[ND]";
		                $s_ar[$liv]["cod_nota"] = "{$desc5} [ND]";
		            }else{
		                $s_ar[$liv]["codice"]   = " [" . $liv . "]";
		                $s_ar[$liv]["cod_nota"] = "{$desc5} [{$liv}]";
		            }
		         
		         $s_ar[$liv]["classe"]   = $str_liv['f_liv5'];
		                        
		    }
		    
		    sum_values($s_ar[$liv], $row);
		    $s_ar = &$s_ar[$liv][$n_children];
		    
		}
		
		
		if($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
			continue;
		}
		
		$tmp_ar_id[] = trim($row['ARART']);
		
	   if(trim($row['ARUMCO']) == trim($row['ARUMTE']) && trim($row['ARUMCO']) == trim($row['ARUMAL'])){
		    $um = $row['ARUMTE'];
	   }elseif((trim($row['ARUMAL']) == trim($row['ARUMTE']) && trim($row['ARUMAL']) != trim($row['ARUMCO'])) ||
	       (trim($row['ARUMAL']) == trim($row['ARUMCO']) && trim($row['ARUMAL']) != trim($row['ARUMTE']))){
		   $um = $row['ARUMTE']. " - " . trim($row['ARUMCO']);
		}else{
		    $um = trim($row['ARUMTE']) . " - " . trim($row['ARUMCO']) . " - " . trim($row['ARUMAL']);
		}
		
		$codice = trim($row['ARART']);
		$ha_commenti = $main_module->has_commento_articolo($row['ARART']);
		
		if ($ha_commenti == FALSE)
		    $img_com_name = "icone/16x16/comment_light.png";
		else
		    $img_com_name = "icone/16x16/comment_edit_yellow.png";
		        
		    $codice .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_bl_articolo(\'' . $row['ARART'] . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
		
		$ar_row = array(
				'id' => implode("|", $tmp_ar_id),
		        'task' => trim($row['ARDART']),
		        'cod_nota'  => $codice,
		        'codice'  => trim($row['ARART']),
		        'um' => $um,
				'data_ins'   => trim($row['ARDTGE']),
				'config'   => trim($row['ARMODE']),
		        'dim1'   => $row['ARDIM1'],
		        'dim2'   => $row['ARDIM2'],
		        'dim3'   => $row['ARDIM3'],
		        'sosp'   => trim($row['ARSOSP']),
		        'esau'   => trim($row['ARESAU']),
		        'allegati' => trim($row['NR_AL']),
		        'l_ven' => trim($row['NR_LV']),
		        'l_acq' => trim($row['NR_LA']),
		        'd_base' =>trim($row['NR_DB']),
		        'd_base_f' =>trim($row['NR_DBF']),
    		    'cicli' => trim($row['NR_CL']),
		        'trad'   => trim($row['N_TRAD']),
		        'attav'   => trim($row['N_ATT']),
		        'var'   => trim($row['NR_VAR']),
		        'abar'   => trim($row['NR_AB']),
		        'sf_g' => trim($row['AXSFON']),
		        'sf_p' => trim($row['AUSFAR']),
		        'nr' => $count++,
		        'leaf' => true				
		);
		
		
		
		$count_rows++;
		
		$s_ar[] = $ar_row;
		
	}
	
	if($count_rows == $limit)
	    $s_ar[] = array(
	        'id' => "segue_".microtime(true),
	        'task' => "<b>*** LISTA INTERROTTA AI PRIMI {$limit} ARTICOLI ***</b>", 
	        'leaf' => true);
	
	
	//scrittura risultato
	$ret = array();
	
	foreach($ar as $kar => $v){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	//per sottoalbero
	if($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
	   
	    
	    $s_ar = &$ret;
	    foreach($livelli as $k => $v){
	        $s_ar = &$s_ar[0]['children'];
	    }
	    $ret = &$s_ar;
	}
	
	
	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
	
	
	
exit;
}


if ($_REQUEST['fn'] == 'open_tab'){
$m_params = acs_m_params_json_decode();
$lingua = $m_params->form_open->f_lingua;

?>
{
 "success":true, "items":
 { 
  		xtype: 'treepanel',
	    title: 'Anagrafica articoli',
	    <?php echo make_tab_closable(); ?>,
		stateful: true,
        stateId: 'panel-anag-art',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
	    tbar: new Ext.Toolbar({
	          items:['<b>Anagrafica articoli</b>', '->',
	            
	             {iconCls: 'icon-gear-16',
	             text: 'PARAMETRI', 
		           		handler: function(event, toolEl, panel){
			           		var m_form_win = Ext.getCmp('<?php echo $m_params->win_id; ?>');
			           		m_form_win.fromObjectId = this.up('panel').getId();
			           		m_form_win.show();
			          
		           		 
		           		 }
		           	 }
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', 
	           	   handler: function(event, toolEl, panel){ 
	           	   	this.up('panel').getStore().load();
	           	   		}
	           	   }
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	      
	        
	        //cls: 'elenco_ordini arrivi',
	        collapsible: true,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'liv', 'descrizione', 'um', 'data_ins', 'config',
				     'codice', 'c_art', 'dim1', 'dim2', 'dim3', 'sosp', 'esau', 'classe', 
				     'liv_cod', 'allegati', 'l_ven', 'l_acq', 'd_base', 'd_base_f', 'cicli', 
				     'trad', 'cod_nota', 'attav', 'var', 'abar', 'sf_g', 'nr', 'sf_p'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                      extraParams: {
	                      form_ep: <?php echo acs_je($m_params->form_open); ?>,
	                      livelli: <?php echo acs_je($m_params->livelli); ?>,
	                      radice : '<?php echo $m_params->radice;?>'
                      }
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),
	        multiSelect: true,
	        singleExpand: false,
	        
	        <?php $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>	
	        <?php $cf2 = "<img src=" . img_path("icone/48x48/camera.png") . " height=20>"; ?>
	        <?php $cl = "<img src=" . img_path("icone/48x48/tools.png") . " height=20>"; ?>
	        <?php $lv = "<img src=" . img_path("icone/48x48/currency_black_euro.png") . " height=20>"; ?>
	        <?php $la = "<img src=" . img_path("icone/48x48/currency_blue_dollar.png") . " height=20>"; ?>
	        <?php $db_p = "<img src=" . img_path("icone/48x48/folder_download.png") . " height=20>"; ?>
	        <?php $db_f = "<img src=" . img_path("icone/48x48/folder_upload.png") . " height=20>"; ?>
	        <?php $cfg = "<img src=" . img_path("icone/48x48/game_pad.png") . " height=20>"; ?>
	        <?php $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>"; ?>
	        <?php $attav = "<img src=" . img_path("icone/48x48/arrivi.png") . " height=20>"; ?>
	        <?php $var = "<img src=" . img_path("icone/48x48/sticker_blue.png") . " height=20>"; ?>
	        <?php $abar = "<img src=" . img_path("icone/48x48/exchange.png") . " height=20>"; ?>
	
			columns: [	
	    		{xtype: 'treecolumn', 
	    		text: 'Descrizione', 	
	    		flex: 1, 
	    		dataIndex: 'task', 
	    		tdCls: 'auto-height rif',
	    		renderer: function(value, metaData, record){
    				
      	  				if (record.get('sf_p') == '1') metaData.tdCls += ' sfondo_rosso'; 
    			    	if (record.get('sf_p') == '2') metaData.tdCls += ' sfondo_giallo';
    			    	if (record.get('sf_p') == '3') metaData.tdCls += ' sfondo_verde';						
    				
    				return value;	
				}
	    		},
	    		{text: '<?php echo $cf1; ?>', 	
				width: 30, 
				dataIndex: 'sosp',
				tooltip: 'Sospeso',		        			    	     
		    	renderer: function(value, metaData, record){
		    			  if(record.get('sosp') == 'S'){ 
		    			   metaData.tdCls += ' sfondo_rosso';
		    			   return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	}
		    		 
		    		  }
		        },
	    	    {text: 'Codice', 
	    	     width: 150, 
	    	     dataIndex: 'cod_nota',
	    	     renderer: function(value, metaData, record){
    				if (record.get('liv') == '')
      	  				 metaData.tdCls += ' auto-height';	
      	  				 
      	  				if (record.get('sf_g') == '1') metaData.tdCls += ' sfondo_rosso'; 
    			    	if (record.get('sf_g') == '2') metaData.tdCls += ' sfondo_giallo';
    			    	if (record.get('sf_g') == '3') metaData.tdCls += ' sfondo_verde';						
    				
    				return value;	
				}},
				 {text: '<?php echo $attav; ?>', 	
				width: 30,
				<?php if($m_params->form_open->f_att != 'Y'){?>
				hidden : true, 
				<?php }?>
				align: 'center', 
				dataIndex: 'attav',
				tooltip: 'Attivit&agrave;',		        			    	     
		    	renderer: function(value, p, record){
		    			  if(record.get('attav') > 0) return '<img src=<?php echo img_path("icone/48x48/arrivi.png") ?> width=15>';
		    		  }
		        },
	    	    {text: '<?php echo $cf2; ?>', 	
				width: 30,
				<?php if($m_params->form_open->f_allegati != 'Y'){?>
				hidden : true, 
				<?php }?>
				align: 'center', 
				dataIndex: 'allegati',
				 tdCls : ' sfondo_cl1',
				tooltip: 'Allegati',		        			    	     
		    	renderer: function(value, p, record){
		    			  if(record.get('allegati') > 0) return '<img src=<?php echo img_path("icone/48x48/camera.png") ?> width=15>';
		    		  }
		        },
				{text: '<?php echo $lv; ?>', 
				width: 30, 
				dataIndex: 'l_ven',
				<?php if($m_params->form_open->f_list_ven != 'Y'){?>
				hidden : true, 
				<?php }?>
				tooltip: 'Listini vendita',
				renderer: function(value, p, record){
	    			  if(record.get('l_ven') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_black_euro.png") ?> width=15>';
	    		  }},
				{text: '<?php echo $la; ?>', 
				<?php if($m_params->form_open->f_list_acq != 'Y'){?>
				hidden : true, 
				<?php }?>
				width: 30, 
				tdCls : ' sfondo_cl2',
				dataIndex: 'l_acq',
				tooltip: 'Listini acquisto',
				renderer: function(value, p, record){
	    			  if(record.get('l_acq') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_blue_dollar.png") ?> width=15>';
	    		  }},
				{text: '<?php echo $db_p; ?>',
				 width: 30, 
				
				 <?php if($m_params->form_open->f_dist != 'Y'){?>
				hidden : true, 
				<?php }?>
				 dataIndex: 'd_base',
				 tooltip: 'Distinta componenti',
				 renderer: function(value, p, record){
	    			  if(record.get('d_base') > 0) return '<img src=<?php echo img_path("icone/48x48/folder_download.png") ?> width=15>';
	    		  }
				 },{text: '<?php echo $db_f; ?>',
				 width: 30, 
				 <?php if($m_params->form_open->f_elenco != 'Y'){?>
				 hidden : true, 
				<?php }?>
				tdCls : ' sfondo_cl3',
				 dataIndex: 'd_base_f',
				 tooltip: 'Elenco padri',
				 renderer: function(value, p, record){
	    			  if(record.get('d_base_f') > 0) return '<img src=<?php echo img_path("icone/48x48/folder_upload.png") ?> width=15>';
	    		  }
				 },
		        {text: '<?php echo $cl; ?>', 
	        	width: 30, 
	        	 <?php if($m_params->form_open->f_cicli != 'Y'){?>
				 hidden : true, 
				<?php }?>
	        	dataIndex: 'cicli',
	        	tooltip: 'Cicli/lavorazioni',
	        	renderer: function(value, p, record){
	    			  if(record.get('cicli') > 0) return '<img src=<?php echo img_path("icone/48x48/tools.png") ?> width=15>';
	    		  }
		        	},
		        	{text: '<?php echo $cfg; ?>', 	
				 width: 30, 
				 <?php if($m_params->form_open->f_conf != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				dataIndex: 'config',
				tdCls : ' sfondo_cl2',
				tooltip: 'Configuratore',
				renderer: function(value, p, record){
	    			  if(record.get('config') != '') return '<img src=<?php echo img_path("icone/48x48/game_pad.png") ?> width=15>';
	    		  }
	    		},{text: '<?php echo $trad; ?>', 	
				width: 30, 
				 <?php if($m_params->form_open->f_trad != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				dataIndex: 'trad',
				tooltip: 'Traduzione',
				renderer: function(value, metaData, record){
				
					   <?php if(isset($lingua) && $lingua != ''){?>
        				   if(record.get('trad') > 0){
    	    			   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('[#' +record.get('trad')+']') + '"';
        				   return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=15>';
        				   }
    				   
    				   <?php }else{?>
    				   	  if(record.get('trad') > 0){
        	    			   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('[#' +record.get('trad')+']') + '"';
            				   return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=15>';
        				   }
        				   
    				   
    				   
    				   <?php }?>
    		
    				  
	    		  }
	    		},
	    	   {text: '<?php echo $var; ?>', 	
	    	    <?php if($m_params->form_open->f_var != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				width: 30, 
				dataIndex: 'var',
				tdCls : ' sfondo_cl1',
				tooltip: 'Varianti anagrafiche',
				renderer: function(value, p, record){
	    			  if(record.get('var') > 0) return '<img src=<?php echo img_path("icone/48x48/sticker_blue.png") ?> width=15>';
	    		  }
	    		},{text: '<?php echo $abar; ?>', 	
				width: 30, 
				 <?php if($m_params->form_open->f_abb != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				dataIndex: 'abar',
				tooltip: 'Regole abbinamento articoli',
				renderer: function(value, p, record){
	    			  if(record.get('abar') > 0) return '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=15>';
	    		  }
	    		},
	    	    {text: 'L', width: 50, dataIndex: 'dim1',
	    	     renderer: function(value, metaData, record){
					if (record.get('dim1') == 0) 
						return '';									
					return addSeparatorsNF(parseFloat(value), '.', ',', '.');
							}, 
	    	     align: 'right'},
	    	    {text: 'H', width: 50, dataIndex: 'dim2', align: 'right',
	    	     renderer: function(value, metaData, record){
					if (record.get('dim1') == 0) 
						return '';									
					return addSeparatorsNF(parseFloat(value), '.', ',', '.');
							}
	    	    },
	    	    {text: 'S', width: 50, dataIndex: 'dim3', align: 'right',
	    	     renderer: function(value, metaData, record){
					if (record.get('dim1') == 0) 
						return '';									
					return addSeparatorsNF(parseFloat(value), '.', ',', '.');
							} },
	    	    {text: 'UM', width: 80, dataIndex: 'um',
	    	      renderer: function(value, metaData, record){
					if (record.get('liv') != '') 
						value =  '[#' +record.get('c_art') + ']';									
					return value;	
							}
	    	   
	    	    },
				{text: 'Data ins.',  width: 70, dataIndex: 'data_ins', renderer: date_from_AS},
				
				{text: 'C', 
				tooltip: 'Ciclo di vita',	 	
				width: 30, 
				dataIndex: 'esau',
				renderer: function(value, metaData, record, row, col, store, gridView){
    		    	if (record.get('esau') == 'E'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In esaurimento') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/clessidra.png") ?> width=15>';}
					if (record.get('esau') == 'A'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In avviamento') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';}
				    if (record.get('esau') == 'C'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Codifica in corso') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=15>';}
				    
				    //return value;
				    }
				}
				],
			enableSort: false, // disable sorting

	        listeners: {
	                 beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },
                     load: function () {
                          Ext.getBody().unmask();
                        },
                     itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  		var grid = this;
				  													  
						  var voci_menu = [];
					      
					      id_selected = grid.getSelectionModel().getSelection();							
						  list_selected_id = [];
						  for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push(id_selected[i].data.codice);
									
					      
					      if(rec.get('liv') == ''){
						
					  	 voci_menu.push({
			         		text: 'Duplica articolo',
			        		iconCls : 'icon-comment_add-16',          		
			        		handler: function () {
					    		  acs_show_win_std('Duplica articolo', 
				                			'acs_anag_art_duplica.php?fn=open_form', {
				                             d_art: rec.get('task'),
				                             c_art: rec.get('codice'),
									}, 500, 390, {}, 'icon-comment_add-16');
				                }
			    		});	 
			    		
			    	my_listeners_inserimento = {
		        					afterInsertRecord: function(from_win){	
		        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
		        						//rec.store.treeStore.load({node: rec.parentNode});		        						
		        						from_win.close();
						        		}
				    				};	
			    		
			    		  voci_menu.push({
						         		text: 'Inserimento nuovo stato/attivit&agrave;',
						        		iconCls : 'icon-arrivi-16',          		
						        		handler: function() {
										acs_show_win_std('Nuovo stato/attivit&agrave;', 
											<?php echo j('acs_anag_art_create_attivita.php'); ?>, 
											{	tipo_op: 'ANART',
												rif : 'ART',
								  				list_selected_id: list_selected_id
								  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
								  							        		          		
						        		}
						    		});
						    		
						    		
						    		
						  		  voci_menu.push({
						         		text: 'Gestione stato/attivit&agrave; per articolo',
						        		iconCls : 'icon-arrivi-16',          		
						        		handler: function() {
										acs_show_panel_std( 
											<?php echo j('acs_anag_art_todolist.php?fn=open_tab'); ?>,
											null, {c_art: rec.get('codice'), rif : 'ART'} 
											);						        		          		
						        		}
						    		});	
						    		
			    			
					}
					
						
			    			voci_menu.push({
			         		text: 'Modifica descrizione/classificazione',
			        		iconCls : 'icon-pencil-16',          		
			        		handler: function () {
			        		
			        			my_listeners = {
		        					afterModCls: function(from_win){	
		        						grid.getStore().load();		        						
		        						from_win.close();
						        		}
				    				};
			        		
					    		  acs_show_win_std('Modifica descrizione/classificazione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_mod_cla', {c_art: rec.get('codice')}, 600, 300, my_listeners, 'icon-pencil-16');
				                }
			    		});	
			    		
			    		
			    		voci_menu.push({
			         		text: 'Evidenziazione generica',
			        		iconCls : 'icon-sub_blue_accept-16', 
			        		menu:{
                        	items:[
                                {  text: 'Rosso',
                                   iconCls : 'icon-sub_blue_accept-16', 
                                   handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '1'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
    									},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },
                            	{  text: 'Giallo',
                            	   iconCls : 'icon-sub_blue_accept-16', 
                            	        handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '2'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                    text: 'Verde',
                                    iconCls : 'icon-sub_blue_accept-16', 
                                         handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '3'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                	text: 'Nessuno',
                                	iconCls : 'icon-sub_blue_accept-16', 
                                	     handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : ''
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                }
                            ]
                        }        		
			        	
			    		});
			    		
			    		
			    		voci_menu.push({
			         		text: 'Evidenziazione per utente',
			        		iconCls : 'icon-sub_blue_accept-16', 
			        		menu:{
                        	items:[
                                {  text: 'Rosso',
                                   iconCls : 'icon-sub_blue_accept-16', 
                                   handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '1'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								        	for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },
                            	{  text: 'Giallo',
                            	   iconCls : 'icon-sub_blue_accept-16', 
                            	        handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '2'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
										for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                    text: 'Verde',
                                    iconCls : 'icon-sub_blue_accept-16', 
                                         handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '3'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                	text: 'Nessuno',
                                	iconCls : 'icon-sub_blue_accept-16', 
                                	     handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : ''
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								        for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                }
                            ]
                        }        		
			        	
			    		});
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
					
					 
			    	
			    	}
			 	,celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	
					  	if(rec.get('liv') == ''){
						iEvent.preventDefault();
						
    						 if(col_name == 'allegati')	
    							acs_show_win_std('Articolo ' +rec.get('codice') , 'acs_anag_art_allegati.php?fn=open_tab', {c_art: rec.get('codice')}, 600, 400, null, 'icon-camera-16');
    					   
            				 if(rec.get('l_ven') > 0 && col_name == 'l_ven')
           	  				  	acs_show_win_std('Listino di VENDITA [' +rec.get('codice') + '] ' +rec.get('task'), 'acs_anag_art_listini.php?fn=open_tab', {c_art: rec.get('codice'), tipo_list: 'V'}, 860, 400, null, 'icon-listino');
            				   
            				 if(rec.get('l_acq') > 0 && col_name == 'l_acq')
            				  	acs_show_win_std('Listino di ACQUISTO [' +rec.get('codice') + '] ' +rec.get('task') , 'acs_anag_art_listini.php?fn=open_tab', {c_art: rec.get('codice'), tipo_list: 'A'}, 1100, 400, null, 'icon-blog_add-16');
            				  	
            				 if(rec.get('d_base') > 0 && col_name == 'd_base')
            				  	acs_show_win_std('Componenti ' +rec.get('codice') + ' - ' +rec.get('task') , 'acs_anag_art_distinta_base.php?fn=open_tab', {c_art: rec.get('codice'), tipo_dist : 'P'}, 850, 400, null, 'icon-folder_download-16');
            				  	
            				 if(rec.get('d_base_f') > 0 && col_name == 'd_base_f')
            				  	acs_show_win_std('Articolo ' +rec.get('codice') + ' - ' +rec.get('task') , 'acs_anag_art_distinta_base.php?fn=open_tab', {c_art: rec.get('codice'), tipo_dist : 'F'}, 600, 400, null, 'icon-folder_upload-16');
            				  	
            				 if(col_name == 'cicli')
            				  	acs_show_win_std('Ciclo lavorazione ' +rec.get('codice')  + ' - ' +rec.get('task') , 'acs_anag_art_cicli.php?fn=open_tab', {c_art: rec.get('codice')}, 850, 400, null, 'icon-tools-16');
            				  	
            				 if(rec.get('config') != '' && col_name == 'config')
            				  	acs_show_win_std('Configuratore articolo ', 'acs_anag_art_config.php?fn=open_tab', {mode: rec.get('config'), c_art : rec.get('codice')}, 450, 400, null, 'icon-design-16');
					   
					    	 if(col_name == 'trad'){
					    	 
				        	  		acs_show_win_std('Traduzione articolo ' +rec.get('codice'), 
					    					'acs_anag_art_gest_trad.php?fn=open_al', 
					    					{c_art: rec.get('codice'), id_tree : grid.getId()}, 450, 500, null, 'icon-globe-16');
				        
					   		}
					   		
					   		 if(col_name == 'var'){
					   		 
					   			acs_show_win_std('Variante anagrafica per articolo ' +rec.get('codice'), 'acs_anag_art_variabili.php?fn=open_tab', {c_art: rec.get('codice'), new : 'Y'}, 600, 400, null, 'icon-sticker_blue-16');
					   		}
					   		
					   		 if(col_name == 'abar'){
					   		 
            				  	acs_show_win_std('[ABAR] Regole abbinamento articolo ' +rec.get('codice'), 'acs_anag_art_abbinamento.php?fn=open_tab', {c_art: rec.get('codice')}, 950, 400, null, 'iconScaricoIntermedio');
					   			
					   		}
					   		
					   		if(col_name == 'task'){			
					   			show_win_dett_art(rec.get('codice'), rec);
					   		}
					   	
					   		
					   		
					    }
	            	}
	          
	           }   	
			    	
	        ,itemclick: function(view,rec,item,index,eventObj) {
	
			     /*
			          if (rec.get('liv') == '')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	var wf = Ext.getCmp('OrdFormFormati');
			        		
						Ext.Ajax.request({
						   url: 'acs_anag_art_get_properties.php?m_art=' + rec.get('codice'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
						
						wf.loadArt(rec.get('codice'));							        	
					   }
				 */
					   
			         }
		        },
			

			viewConfig: {
		        getRowClass: function(record, index) {	
		        
		        if(record.get('liv') == ''){
		          if(record.get('nr') % 2 == 0)
        		     return ' segnala_riga_pari';
        		  
		        
		        }
		        		        	
		           v = record.get('liv');
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
		           	return v;																
		         }   
		    }			
			    		
 }
} 

<?php 

exit;
}
// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_mod_cla'){
    
    $m_params = acs_m_params_json_decode();
    
    $sql = "SELECT ARTPAR, ARCLME, ARGRME, ARSGME, ARTAB1, ARFOR1, ARDART,  
            CF_FORNITORE.CFCD AS C_FOR, CF_FORNITORE.CFRGS1 AS D_FOR
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
              ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
            WHERE ARART = '{$m_params->c_art}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
            
    
 
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
            title: '',
            items: [  {
                        xtype: 'textfield',
						name: 'ARDART',
						fieldLabel: 'Descrizione',
						labelWidth :120,
						anchor: '-15',
						value : '<?php echo trim($row['ARDART']); ?>'
               
             		  },{
					    xtype: 'combo',
						name: 'ARFOR1',
						fieldLabel: 'Fornitore',
						labelWidth :120,
						anchor: '-15',
						minChars: 2,			
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '../base/acs_get_anagrafica.php?fn=get_data&cod=F',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
								 fields: [{name:'id'}, {name:'text'}, {name:'loca'}, {name:'color'}],          	
			            },
						valueField: 'id',                        
			            displayField: 'text',
			            value : '<?php echo $row['D_FOR']; ?>',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item" style="background-color:{color}">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {loca}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
        		},{
								name: 'ARTPAR',
								xtype: 'combo',
								fieldLabel: 'Tipo/parte',
								labelWidth: 120,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								value : '<?php echo $row['ARTPAR'];  ?>',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MTPA'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'ARCLME',
								xtype: 'combo',
								labelWidth: 120,
								fieldLabel: 'Classe',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								value : '<?php echo $row['ARCLME'];  ?>',						
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUCM'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'ARGRME',
								xtype: 'combo',
								fieldLabel: 'Gruppo',
								labelWidth: 120,
								forceSelection: true,							
								displayField: 'text',
								valueField: 'id',	
								value : '<?php echo $row['ARGRME'];  ?>',							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUGM'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'ARSGME',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								labelWidth: 120,
								forceSelection: true,	
								displayField: 'text',
								valueField: 'id',
								value : '<?php echo $row['ARSGME'];  ?>',							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUSM'), ''); ?>	
								    ]
								}
								
						 }, {
								name: 'ARTAB1',
								xtype: 'combo',
								fieldLabel: 'Gruppo pianificazione',
								labelWidth: 120,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								value : '<?php echo $row['ARTAB1'];  ?>',						
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('GRPA'), ''); ?>	
								    ]
								}
								
						 }		        
				
					        
				],
			buttons: [
			
			{
	            text: 'Aggiorna',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
	            	var loc_win = this.up('window');
	            	if(form.isValid()){
	            	
	            		Ext.Ajax.request({
							   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna_art',
							   method: 'POST',
							   jsonData: {
							   form_values : form_values,
							   row_old : <?php echo acs_je($row); ?>,
							   c_art : '<?php echo $m_params->c_art; ?>'
							   }, 
							   
							   success: function(response, opts) {
							   	  Ext.getBody().unmask();
								  var jsonData = Ext.decode(response.responseText);
								  loc_win.fireEvent('afterModCls', loc_win)
								  					   	  
							   }, 
							   failure: function(response, opts) {
							      alert('error on save_manual');
							   }
							});	
    	
	            	 }
	        		}			
	        }
	        
	        ]
	        
	        
	        
	        
				
        }
]}
<?php	
	exit;
} //open_form_filtri


if ($_REQUEST['fn'] == 'exe_up_color_multi'){
    
    $m_params = acs_m_params_json_decode();
    
    foreach($m_params->list_selected_id as $v){
        
        $sql_a = "SELECT COUNT (*) AS C_ROW
        FROM {$cfg_mod_DeskUtility['file_est_anag_art']} AX
        WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$v}'";
        
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_a);
        
        $row = db2_fetch_assoc($stmt_a);
        
        
        $ar_ins = array();
        
        $ar_ins['AXUSGE'] 	= substr($auth->get_user(), 0, 8);
        $ar_ins['AXDTGE']   = oggi_AS_date();
        $ar_ins['AXTMGE'] 	= oggi_AS_time();
        $ar_ins['AXSFON'] 	= $m_params->flag;
        
        if($row['C_ROW'] == 0){
            
            $ar_ins['AXDT'] 	= $id_ditta_default;
            $ar_ins['AXART'] 	= $v;
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art']}(" . create_name_field_by_ar($ar_ins) . ")
			    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
            
            
        }else{
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art']} AX
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$v}' ";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
        }
    
        
    }
    
    $ret = array();
    $ret['success'] = true;
    $ret['flag'] = $m_params->flag;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_up_color_utente'){
    
    $m_params = acs_m_params_json_decode();
    
    foreach($m_params->list_selected_id as $v){
        
        $sql_a = "SELECT COUNT (*) AS C_ROW
        FROM {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
        WHERE AUDT = '{$id_ditta_default}' AND AUART = '{$v}'";
        
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_a);
        
        $row = db2_fetch_assoc($stmt_a);
        
        
        $ar_ins = array();
        
        $ar_ins['AUUSUM'] 	= substr($auth->get_user(), 0, 8);
        $ar_ins['AUDTUM']   = oggi_AS_date();
        $ar_ins['AUTMUM'] 	= oggi_AS_time();
        $ar_ins['AUSFAR'] 	= $m_params->flag;
        $ar_ins['AUUTEN'] 	= substr($auth->get_user(), 0, 8);
        
        if($row['C_ROW'] == 0){
            
            $ar_ins['AUUSGE'] 	= substr($auth->get_user(), 0, 8);
            $ar_ins['AUDTGE']   = oggi_AS_date();
            $ar_ins['AUTMGE'] 	= oggi_AS_time();
            $ar_ins['AUDT'] 	= $id_ditta_default;
            $ar_ins['AUART'] 	= $v;
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art_utente']}(" . create_name_field_by_ar($ar_ins) . ")
			    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
            
            
        }else{
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE AUDT = '{$id_ditta_default}' AND AUART = '{$v}' ";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
        }
        
        
    }
    
    $ret = array();
    $ret['success'] = true;
    $ret['flag'] = $m_params->flag;
    echo acs_je($ret);
    exit;
}
