<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $ar_ins['FADTUM'] = oggi_AS_date();
    $ar_ins['FAUSUM'] = substr($auth->get_user(), 0, 8);
    
    if(strlen($form_values->forn))
        $ar_ins['FAFORN'] = $form_values->forn;
    else 
        $ar_ins['FAFORN'] = '000000000';
    
    if(isset($form_values->t_elab))
        $ar_ins['FATPEL'] = $form_values->t_elab;
    if(isset($form_values->fami_prod))
        $ar_ins['FAFAMI'] = $form_values->fami_prod;
   // $ar_ins['FASGRU'] = $form_values->sgruppo;
    if(isset($form_values->categoria))
        $ar_ins['FAPERC'] = $form_values->categoria;
    if(isset($form_values->l_truc))
        $ar_ins['FATRUC'] = sql_f($form_values->l_truc);
    if(isset($form_values->l_mdf))
        $ar_ins['FAMDF']  = sql_f($form_values->l_mdf);
    if(isset($form_values->l_contr))
        $ar_ins['FALEGC'] = sql_f($form_values->l_contr);
    if(isset($form_values->l_mass))
        $ar_ins['FAMASS'] = sql_f($form_values->l_mass);
    if(isset($form_values->materiale))
        $ar_ins['FACOEFF']= sql_f($form_values->materiale);
    if(isset($form_values->data_val))
        $ar_ins['FAVALID']= sql_f($form_values->data_val);
    if(isset($form_values->var))
        $ar_ins['FAVR01']= rtrim($form_values->var);
    if(isset($form_values->van))
        $ar_ins['FAVN01']= rtrim($form_values->van);
    if(isset($form_values->tipo))
        $ar_ins['FATP01']= rtrim($form_values->tipo);

          
    //singola modifica, per RRN    
    if(isset($m_params->row->rrn) && $m_params->row->rrn != ''){
        $sql = "UPDATE {$cfg_mod_DeskArt['file_fsc']} FA
                SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE RRN(FA) = '{$m_params->row->rrn}'";        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg($stmt);        
        
    }else{
        
        //se ho c_art inserisco la FSC per l'articolo passato
        if (isset($m_params->c_art) && strlen(trim($m_params->c_art)) > 0){
            $ar_ins['FADTGE'] = oggi_AS_date();
            $ar_ins['FAUSGE'] = $auth->get_user();
            $ar_ins["FADT"] = $id_ditta_default;
            $ar_ins["FAART"] = $m_params->c_art;
        
            $sql = "INSERT INTO {$cfg_mod_DeskArt['file_fsc']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);        
        } // inserimento per singolo articolo
        


        //se ho list_art (array) devo modificare / inserire la FSC per ogni articolo passato
        if (isset($m_params->list_art) && is_array($m_params->list_art) && count($m_params->list_art) > 0){
            $ar_ins_copy = array_merge($ar_ins, array());
            $codice_fornitore = $ar_ins['FAFORN'];
            
            $ar_esito = array('INS' => 0, 'UPD' => 0);             
            
            foreach($m_params->list_art as $codice_articolo){
                
                //verifico esistenza in base a codice/fornitore
                $sql = "SELECT RRN(FA) AS RRN_FA FROM {$cfg_mod_DeskArt['file_fsc']} FA 
                         WHERE FADT = ? AND FAART = ? AND FAFORN = ?";        
                $stmtM = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmtM, array($id_ditta_default, $codice_articolo, $codice_fornitore));
                echo db2_stmt_errormsg($stmtM);
                $rowFA = db2_fetch_assoc($stmtM);

                
                if ($rowFA && $form_values->f_multiselect_update == 'Y'){
                    //aggiorno
                    $ar_ins_multi = array_merge($ar_ins_copy, array());                    
                    $sql = "UPDATE {$cfg_mod_DeskArt['file_fsc']} FA
                            SET " . create_name_field_by_ar_UPDATE($ar_ins_multi) . "
                            WHERE RRN(FA) = {$rowFA['RRN_FA']}";
                    $stmtMU = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmtMU, $ar_ins_multi);
                    echo db2_stmt_errormsg($stmtMU);  
                    $ar_esito['UPD']++;
                    

                }
                
                if (!$rowFA && $form_values->f_multiselect_insert == 'Y'){
                    //inserisco
                    $ar_ins_multi = array_merge($ar_ins_copy, array());
                    $ar_ins_multi['FADTGE'] = oggi_AS_date();
                    $ar_ins_multi['FAUSGE'] = $auth->get_user();
                    $ar_ins_multi["FADT"] = $id_ditta_default;
                    $ar_ins_multi["FAART"] = $codice_articolo;
                    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_fsc']} (" . create_name_field_by_ar($ar_ins_multi) . ") VALUES (" . create_parameters_point_by_ar($ar_ins_multi) . ")";
                    $stmtMU = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmtMU, $ar_ins_multi);
                    echo db2_stmt_errormsg($stmtMU);
                    $ar_esito['INS']++;
                }
                
            } //foreach articolo
            
          $ret['ar_esito'] = $ar_esito;  
            
        } //inserimento / modifica multipla        
        
    }
 
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc_scheda'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->row->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_fsc']} FA WHERE RRN(FA) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}





if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    $ar = array();
    
    $c_art = $m_params->open_request->c_art;
        
    $sql = " SELECT RRN(FA) AS RRN, FA.*,CF.CFRGS1 AS D_FOR
            FROM {$cfg_mod_DeskArt['file_fsc']} FA
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_cli']} CF
               ON CF.CFDT = FA.FADT AND CF.CFCD = FA.FAFORN
            WHERE FADT = '{$id_ditta_default}' AND FAART = '{$c_art}'";
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    while($row = db2_fetch_assoc($stmt)){
  
        $nr = array();
        $nr['rrn'] = $row['RRN'];
        $nr['forn'] = $row['FAFORN'];
        $nr['fornitore'] = "[".$row['FAFORN']."] ".trim($row['D_FOR']);
        $nr['t_elab'] = trim($row['FATPEL']);
        $fste = find_TA_sys('FSTE', trim($row['FATPEL']));
        $nr['d_elab'] = "[".trim($row['FATPEL'])."] ".$fste[0]['text'];
        $nr['fami_prod'] = trim($row['FAFAMI']);
        $fsct = find_TA_sys('FSCT', trim($row['FAFAMI']));
        $nr['d_fami'] = "[".trim($row['FAFAMI'])."] ".$fsct[0]['text'];
        $nr['sgruppo'] = trim($row['FASGRU']);
        $fscl = find_TA_sys('FSCL', trim($row['FASGRU']));
        $nr['d_sgruppo'] = "[".trim($row['FASGRU'])."] ".$fscl[0]['text'];
        $nr['categoria'] = trim($row['FAPERC']);
        $fscp = find_TA_sys('FSCP', trim($row['FAPERC']));
        $nr['d_categoria'] = "[".trim($row['FAPERC'])."] ".$fscp[0]['text'];
        
        $nr['l_truc'] = trim($row['FATRUC']);
        $nr['l_mdf'] = trim($row['FAMDF']);
        $nr['l_contr'] = trim($row['FALEGC']);
        $nr['l_mass'] = trim($row['FAMASS']);
        $nr['materiale'] = trim($row['FACOEFF']);
        $nr['data_val'] = trim($row['FAVALID']);
        
        $nr['var'] = trim($row['FAVR01']);
        $var = find_TA_sys('PUVR', trim($row['FAVR01']));
        $nr['d_var'] = "[".trim($row['FAVR01'])."] ".$var[0]['text'];
        
        $nr['van'] = trim($row['FAVN01']);
        $van = find_TA_sys('PUVN', trim($row['FAVN01']));
        $nr['d_van'] = "[".trim($row['FAVN01'])."] ".$van[0]['text'];
        
        $nr['tipo'] = trim($row['FATP01']);
        $tipo = find_TA_sys('PUTI', trim($row['FATP01']));
        $nr['d_tipo'] = "[".trim($row['FATP01'])."] ".$tipo[0]['text'];
        
        $nr['out_v'] = "{$nr['d_var']}<br>{$nr['d_van']}";
        
        $ar[] = $nr;
        
    }
    
    
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
				items: [
						{
						xtype: 'grid',
						title: '',
						itemId : 'm_grid',
						flex:0.7,
				        loadMask: true,	
				        tbar: new Ext.Toolbar({
	                    items:[ '->',
	                    
	            		{
	            		 //iconCls: 'icon-gear-16',
        	             text: 'AGGIUNGI', 
        		           		handler: function(event, toolEl, panel){
        		           		 var m_grid = this.up('window').down('#m_grid');
        		           		 var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    m_grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					  	 acs_show_win_std('Aggiungi FSC', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								 c_art : '<?php echo $m_params->c_art; ?>'  
							}, 500, 450, my_listeners, 'icon-pencil-16');
        			          
        		           		 
        		           		 }
        		           	 }
	           
	         ]            
	        }),	 
				   		store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['rrn', 'fornitore', 'forn', 't_elab', 'd_elab', 'fami_prod', 'd_fami', 
		        			         'sgruppo', 'd_sgruppo', 'categoria', 'd_categoria', 'l_truc', 'l_mdf', 
		        			         'l_contr', 'l_mass', 'materiale', 'data_val', 'var', 'd_var', 'van',
		        			         'd_van', 'tipo', 'd_tipo', 'out_v']
			}, //store
				
			      columns: [
			      
			      {
	                header   : 'Fornitore',
	                dataIndex: 'fornitore',
	                flex : 1
	                },
	                {
	                header   : 'Attivazione',
	                dataIndex: 'data_val',
	                width : 70,
	                renderer : date_from_AS
	                },
	                 {
	                header   : 'Tipo elaborazione',
	                dataIndex: 'd_elab',
	                flex : 1
	                },  {
	                header   : 'Famiglia prodotti',
	                dataIndex: 'd_fami',
	                flex : 1
	                },
	                {
	                header   : 'Sottogruppo',
	                dataIndex: 'd_sgruppo',
	                flex : 1
	                },{
	                header   : 'Categoria',
	                dataIndex: 'd_categoria',
	                flex : 1
	                },{
	                header   : 'Materiale',
	                dataIndex: 'materiale',
	                width : 60,
	                renderer : floatRenderer2
	                },{
	                header   : 'MDF',
	                dataIndex: 'l_mdf',
	                width : 50,
	                renderer : floatRenderer2
	                },{
	                header   : 'Truciolare',
	                dataIndex: 'l_truc',
	                width : 70,
	                renderer : floatRenderer2
	                },{
	                header   : 'Massello',
	                dataIndex: 'l_mass',
	                width : 60,
	                renderer : floatRenderer2
	                },{
	                header   : 'Legno controllato',
	                dataIndex: 'l_contr',
	                width : 110,
	                renderer : floatRenderer2
	                },
	                {
	                header   : 'Variabile',
	                dataIndex: 'd_var',
	                width : 110,
	                },
	                {
	                header   : 'Variante',
	                dataIndex: 'd_van',
	                width : 110,
	                },
			       {
	                header   : 'Tipologia',
	                dataIndex: 'd_tipo',
	                width : 110,
	                },
                 	 
	                
	         ], listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var row = rec.data;
					     var m_grid = this;
					     
					 	 voci_menu.push({
			         		text: 'Cancella',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_scheda',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								        
								   			m_grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    		});
			    		
			    		 voci_menu.push({
			         		text: 'Modifica',
			        		iconCls : 'icon-pencil-16',          		
			        		handler: function () {
			        		   	var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					  	 acs_show_win_std('Modifica scheda FSC articolo ' + <?php echo j($m_params->c_art); ?> , '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								row: row, c_art : '<?php echo $m_params->c_art; ?>' 
							}, 500, 450, my_listeners, 'icon-pencil-16');
				                }
			    		});		
			    	
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	  
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}


if ($_REQUEST['fn'] == 'open_mod'){?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            layout: {pack: 'start', align: 'stretch'},
	            items: [{ xtype: 'fieldcontainer'
                          , layout:
                              {	type: 'hbox'
                              , pack: 'start'
                        	  , align: 'stretch'
                                }
                          , items: [ 
	                    {
            				name: 'forn',
            				value : <?php echo j($m_params->row->forn); ?>,
            				xtype: 'hiddenfield',					
            			   }, 
	           		       {
            				name: 'fornitore',
            				fieldLabel : 'Fornitore',
            				value : <?php echo j($m_params->row->fornitore); ?>,
            				xtype: 'displayfield', 
            				anchor: '-15'							
            			   }, 
            			   {xtype: 'component',
                                flex : 1,
                            },
                          { xtype: 'button'
                         , margin: '0 15 0 0'
                         , anchor: '-15'	
                         , scale: 'small'
                         , iconCls: 'icon-search-16'
                         , iconAlign: 'top'
                         , width: 25			
                         , handler : function() {
                               var m_form = this.up('form').getForm();
                               var my_listeners = 
                                 { afterSel: function(from_win, row) {
                                     m_form.findField('forn').setValue(row.CFCD);
                                     m_form.findField('fornitore').setValue('['+row.CFCD+']' +row.CFRGS1);
                                     from_win.close();
                                   }
                                 }
                             
                               acs_show_win_std('Anagrafica clienti/fornitori'
                                               , 'acs_anag_ricerca_cli_for.php?fn=open_tab'
                                               , {cf: 'F'}, 600, 500, my_listeners
                                               , 'icon-search-16');
                             }
                                    
                        }
                        
                        ]},
	            	{
                	   xtype: 'datefield'
                	   , startDay: 1 //lun.
                	   , fieldLabel: 'Attivazione'
                	   , name: 'data_val'
                	   , format: 'd/m/Y'
                	   , value: '<?php echo print_date($m_params->row->data_val, "%d/%m/%Y"); ?>'							   
                	   , submitFormat: 'Ymd'
                	   , allowBlank: true
                	   , listeners: {
                	       invalid: function (field, msg) {
                	       Ext.Msg.alert('', msg);}
                		}
                	   , width: 250, anchor: 'none'
                	},
           			 {name: 't_elab',
					  xtype: 'combo',
					  allowBlank: false,
				      fieldLabel: 'Tipo elaborazione',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',
        			  value:  <?php echo j($m_params->row->t_elab); ?>,							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('FSTE'), ''); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 }, {name: 'fami_prod',
					  xtype: 'combo',
				      fieldLabel: 'Famiglia prodotti',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',
        			  value:  <?php echo j($m_params->row->fami_prod); ?>,							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('FSCT'), ''); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 }, {name: 'sgruppo',
					  xtype: 'combo',
				      fieldLabel: 'Sottogruppo',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',
        			  value:  <?php echo j($m_params->row->sgruppo); ?>,							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('FSCL'), ''); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 },{name: 'categoria',
					  xtype: 'combo',
				      fieldLabel: 'Categoria',
				      allowBlank: false,
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',
        			  value:  <?php echo j($m_params->row->categoria); ?>,							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('FSCP'), ''); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 },{xtype: 'numberfield',
                	name: 'materiale',
                	hideTrigger : true,
                	fieldLabel: 'Materiale',
                	width : '250',
                	value:  <?php echo j($m_params->row->materiale); ?>
                	},{xtype: 'numberfield',
                	name: 'l_mdf',
                	hideTrigger : true,
                	fieldLabel: 'MDF',
                	width : '250',
                	value:  <?php echo j($m_params->row->l_mdf); ?>
                	},{xtype: 'numberfield',
                	name: 'l_truc',
                	hideTrigger : true,
                	fieldLabel: 'Truciolare',
                	width : '250',
                	value:  <?php echo j($m_params->row->l_truc); ?>
                	},{xtype: 'numberfield',
                	name: 'l_mass',
                	hideTrigger : true,
                	fieldLabel: 'Massello',
                	width : '250',
                	value:  <?php echo j($m_params->row->l_mass); ?>
                	},{xtype: 'numberfield',
                	name: 'l_contr',
                	hideTrigger : true,
                	fieldLabel: 'Legno controllato',
                	width : '250',
                	value:  <?php echo j($m_params->row->l_contr); ?>
                	},
                	
                	<?php		      
                      $proc = new ApiProc();
                      echo $proc->get_json_response(
                          extjs_combo_dom_ris(array(
                              'initial_value' => array('dom_c'  => $m_params->row->var,
                                                       'ris_c'  => $m_params->row->van,
                                                       'tipo_c' => "",
                                                       'out_d'  => $m_params->row->out_v),
                              'risposta_a_capo' => true,
                              'label' =>'Variabile/<br>Variante', 
                              'file_TA' => 'sys',
                              'tipo_opz_risposta' => array('output_domanda' => true,  
                                 'tipo_cf' => '',
                                 /*'opzioni' => array(
                                      array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
                                  )*/),
                              'dom_cf'  => 'var',  'ris_cf' => 'van'))
                      );
                      echo ",";
                    ?>
                    
                    { name: 'tipo',
					  xtype: 'combo',
				      fieldLabel: 'Tipologia',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',
        			  value:  <?php echo j($m_params->row->tipo); ?>,							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUTI'), ''); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 }
				 
				 
				 <?php if(isset($m_params->list_art) && count($m_params->list_art) > 0){ ?>
				 	//articoli in multiselezione: chiedo se 'inserire' e/o 'modificare' le schede
				 	// (la chiave sara' articolo/fornitore)
				 	
				 	, {
    					xtype: 'fieldset',
    	                title: 'Modalit&agrave; multiselezione',
    	                items: [
        				 	 {
        						xtype: 'checkboxgroup',
        						fieldLabel: '',
        						labelWidth : 150,
        					    items: [{
                                    xtype: 'checkbox'
                                  , name: 'f_multiselect_insert' 
                                  , boxLabel: 'Inserisce se mancante'
                                  , checked : true
                                  , inputValue: 'Y'
                                }, {
                                    xtype: 'checkbox'
                                  , name: 'f_multiselect_update' 
                                  , boxLabel: 'Modifica se presente'
                                  , checked : true
                                  , inputValue: 'Y'
                                }]							
        					}, {
        						xtype: 'displayfield',
								value: 'La sostituzione avviene a livello di articolo/fornitore, sostituendo interamente la scheda con i valori qui inseriti.'
        					}
        				]
        			}	
				 <?php } ?>
                    
 		       	],
	            
				buttons: [					
					{
					
						<?php
						  if (is_array($m_params->list_art) && count($m_params->list_art) > 0)
						      $b_text = 'Inserisci/Modifica. Articoli selezionati: <b>' . count($m_params->list_art) . '</b>';
						  else
						      $b_text = 'Salva';
						?>
					
			            text: <?php echo j($b_text); ?>,
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			            	
			            	 if(form.getForm().isValid()){	
			                Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        jsonData: {
						        	form_values : form.getValues(),
						        	row : <?php echo acs_je($m_params->row) ?>,
						        	c_art : <?php echo j($m_params->c_art); ?>,
						        	list_art : <?php echo j($m_params->list_art); ?>,
						        	l_insert : <?php echo j($m_params->l_insert); ?>,
						        	
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						          var jsonData = Ext.decode(result.responseText);
						          
						          if(jsonData.success == false){
				      	    		acs_show_msg_error(jsonData.msg_error);
							        	return;
				        		  }else{
					          		loc_win.fireEvent('afterOkSave', loc_win, jsonData);
						          }		            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
						    }
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	exit;
   }
   

