<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

ini_set('max_execution_time', 90000);

$m_params = acs_m_params_json_decode();

function setup_articolo_consumi_out_mese($row, $sequneza_mese){
	$anno_inizio = $row['SKAIC0'];
	$mese_inizio = $row['SKMIC0'];
	
	$anno = $anno_inizio;
	$mese = $mese_inizio;
	for ($i = 1; $i < $sequneza_mese; $i++) {
		if ($mese == 1) {
			$anno--;
			$mese = 12;
		} else 
			$mese--;
	}
	return ucfirst(print_month($mese, '%b')) . " {$anno}";
}

// ******************************************************************************************
// RIPRISTINA TIPO GESTIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'ripristina_tipo_gestione'){
    
    $cod_art = $m_params->open_request->form_values->f_cod_art;
    $desc_art = $m_params->open_request->form_values->f_desc_art;
    $cod_forn = $m_params->open_request->form_values->f_fornitore;
    $magaz = $m_params->open_request->form_values->f_magazzino;
    
    if (strlen($cod_art) > 0)
        $sql_where.= " AND UPPER(SKCAR0) LIKE '%" . strtoupper($cod_art) . "%' ";
    
    if (strlen($desc_art) > 0)
        $sql_where.= " AND UPPER(SKDES0) LIKE '%" . strtoupper($desc_art) . "%' ";
        
    if (strlen($cod_forn) > 0)
        $sql_where.= " AND UPPER(SKCFO0) = '" . strtoupper($cod_forn) . "' ";
            
    if (strlen($magaz) > 0)
        $sql_where.= " AND SKCMA0 = '{$magaz}' ";
                
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_articoli']}
		SET SKFLG1 = SKFLA1
		WHERE SKDIT0 = '$id_ditta_default' AND SKDIV0 = '{$m_params->open_request->form_values->f_divisione}' {$sql_where} 
			AND SKTPR0 = ''";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);

    $ret = array('success' => true);
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// ELENCO SOSTITUITI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_sostituiti'){

	$m_params = acs_m_params_json_decode();
	?>

{
		success:true,
		items: [
		{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},

		items: [  
		
		{
			xtype: 'grid',
			flex:1,
	        loadMask: true,	
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
			
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_sost', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							         extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['SOCA20', 'SODE20']							
									
			}, //store
				

			      columns: [	
			      	{
	                header   : 'Articolo',
	                dataIndex: 'SOCA20',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'SODE20',
	                flex: 1,
					filter: {type: 'string'}, filterable: true,
	                }
	                
	         ], 
	         
	         listeners: {

	         }
	   
		},		//grid
		
		
	  		  
	  	]
	  }
	 ]
	}



<?php exit; }


// ******************************************************************************************
// FROM DETTAGLIO CONSUMI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_dettaglio_consumi'){
	$m_params = acs_m_params_json_decode();
	$r = get_by_rrn($m_params->rrn, $cfg_mod_DeskUtility['file_articoli']);
?>
{
	success:true,
	items: [
	
		{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
			items: [
			
				{
					xtype: 'panel', flex: 1,
					layout: {
						type: 'vbox',
						align: 'stretch',
						pack : 'start',
					},
					defaults: {xtype: 'textfield', fieldStyle: 'text-align:right;', padding: '4px'},
					items: [
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 1))?>, value: <?php echo j(n($r['SKC010'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 2))?>, value: <?php echo j(n($r['SKC020'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 3))?>, value: <?php echo j(n($r['SKC030'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 4))?>, value: <?php echo j(n($r['SKC040'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 5))?>, value: <?php echo j(n($r['SKC050'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 6))?>, value: <?php echo j(n($r['SKC060'], 3))?>},										
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 7))?>, value: <?php echo j(n($r['SKC070'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 8))?>, value: <?php echo j(n($r['SKC080'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 9))?>, value: <?php echo j(n($r['SKC090'], 3))?>},										
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 10))?>, value: <?php echo j(n($r['SKC100'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 11))?>, value: <?php echo j(n($r['SKC110'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 12))?>, value: <?php echo j(n($r['SKC120'], 3))?>},										
																	
					]
				}
				
				<?php if ($r['SKMEC0'] > 12){ ?>
				
				, {
					xtype: 'panel', flex: 1,
					layout: {
						type: 'vbox',
						align: 'stretch',
						pack : 'start',
					},
					defaults: {xtype: 'textfield', fieldStyle: 'text-align:right;', padding: '4px'},
					items: [
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 13))?>, value: <?php echo j(n($r['SKC130'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 14))?>, value: <?php echo j(n($r['SKC140'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 15))?>, value: <?php echo j(n($r['SKC150'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 16))?>, value: <?php echo j(n($r['SKC160'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 17))?>, value: <?php echo j(n($r['SKC170'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 18))?>, value: <?php echo j(n($r['SKC180'], 3))?>},										
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 19))?>, value: <?php echo j(n($r['SKC190'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 20))?>, value: <?php echo j(n($r['SKC200'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 21))?>, value: <?php echo j(n($r['SKC210'], 3))?>},										
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 22))?>, value: <?php echo j(n($r['SKC220'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 23))?>, value: <?php echo j(n($r['SKC230'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 24))?>, value: <?php echo j(n($r['SKC240'], 3))?>},										
																	
					]
				}
				<?php } ?>
				
				
				<?php if ($r['SKMEC0'] > 24){ ?>
				, {
					xtype: 'panel', flex: 1,
					layout: {
						type: 'vbox',
						align: 'stretch',
						pack : 'start',
					},
					defaults: {xtype: 'textfield', fieldStyle: 'text-align:right;', padding: '4px'},
					items: [
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 25))?>, value: <?php echo j(n($r['SKC250'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 26))?>, value: <?php echo j(n($r['SKC260'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 27))?>, value: <?php echo j(n($r['SKC270'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 28))?>, value: <?php echo j(n($r['SKC280'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 29))?>, value: <?php echo j(n($r['SKC290'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 30))?>, value: <?php echo j(n($r['SKC300'], 3))?>},										
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 31))?>, value: <?php echo j(n($r['SKC310'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 32))?>, value: <?php echo j(n($r['SKC320'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 33))?>, value: <?php echo j(n($r['SKC330'], 3))?>},										
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 34))?>, value: <?php echo j(n($r['SKC340'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 35))?>, value: <?php echo j(n($r['SKC350'], 3))?>},
							{fieldLabel: <?php echo j(setup_articolo_consumi_out_mese($r, 36))?>, value: <?php echo j(n($r['SKC360'], 3))?>},										
																	
					]
				}
				<?php } ?>
			
			]
		}
	
	
	
	]
}		
<?php	
	exit;
}



// ******************************************************************************************
// EXE SINCRO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_sincro'){
	$ret = array();
	$m_params = acs_m_params_json_decode();
    if(strlen($m_params->form_values->f_fornitore) > 0){
    	$forn = $m_params->form_values->f_fornitore;
    }else{
    	$forn = '';
    	}

	$sh = new SpedHistory($m_DeskAcq);
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> 'SCO_SAVE_BTS',
					"vals" => array("RICITI" =>$m_params->values->form_values->form_values->f_divisione,
					"RIART" =>$forn),
			)
			);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}

function options_select_fornitore($mostra_codice = 'N', $div){
	global $conn;
	global $cfg_mod_DeskUtility, $id_ditta_default;
	$ar = array();

	$sql = "SELECT SFCFO0, SFRAG0 FROM {$cfg_mod_DeskUtility['file_fornitori']} AF
	WHERE SFDIT0 = '$id_ditta_default' AND SFDIV0 = '{$div}'";
	

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		if ($mostra_codice == 'Y')
			$text = "[" . $row['SFCFO0'] . "] " . $row['SFRAG0'];
			else $text = $row['SFRAG0'];
	 	$ret[] = array( "id" 	=> $row['SFCFO0'],
	 			"text" 	=> $text );
	}
	return $ret;
}


// ******************************************************************************************
// EXE SAVE FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_forn'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();
	
	$form_values = $m_params->form_values;


	$ar_upd = array();
	$ar_upd['SKUTM0'] 	= $auth->get_user();
	$ar_upd['SKDTM0'] 	= oggi_AS_date();
	$ar_upd['SKORM0'] 	= oggi_AS_time();
	$ar_upd['SKGRI0'] 	= sql_f($form_values->SKGRI0);
	$ar_upd['SKLAE0'] 	= sql_f($form_values->SKLAE0);
	$ar_upd['SKLAM0'] 	= sql_f($form_values->SKLAM0);
	if(isset($form_values->SKFLG1_v) && $form_values->SKFLG1_v == 'S'){
		$ar_upd['SKFLG1'] 	= 'S';
	}else{
		$ar_upd['SKFLG1'] 	= '';
	}

	$sql = "UPDATE {$cfg_mod_DeskUtility['file_articoli']} KF
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE SKDIT0='$id_ditta_default' AND SKDIV0='{$form_values->SKDIV0}' AND RRN(KF) = '{$form_values->RRN}' ";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_upd);
	echo db2_stmt_errormsg($stmt);
	
	$sql_s = "SELECT *
				FROM {$cfg_mod_DeskUtility['file_articoli']} KF
				WHERE SKDIT0='$id_ditta_default' AND SKDIV0='{$form_values->SKDIV0}' AND RRN(KF) = '{$form_values->RRN}'";
	
	$stmt_s = db2_prepare($conn, $sql_s);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_s);
	echo db2_stmt_errormsg($stmt_s);
	

	
	$row = db2_fetch_assoc($stmt_s);
	
	if($row['SKFLG1'] == 'S'){
		$row['SKFLG1_v'] = true;
	}else{
		$row['SKFLG1_v'] = false;
	}


	$ret = array();
	$ret['success'] = true;
	$ret['record'] = $row;
	echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'grid_sost'){

	$m_params = acs_m_params_json_decode();

		$sql = "SELECT SOCA20, SODE20
		FROM {$cfg_mod_DeskUtility['file_art_sost']} SO
		WHERE SODIT0 = '$id_ditta_default' AND SODIV0 = '{$m_params->open_request->form_values->SKDIV0}'
		AND SOCAR0 = '{$m_params->open_request->cod_art}' ORDER BY SOCA20";
		
		//print_r($sql);
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);

		while($row = db2_fetch_assoc($stmt)){
			$nr = array();
			$nr['SOCA20'] = trim($row['SOCA20']);
			$nr['SODE20'] = trim($row['SODE20']);
			$ar[] = $nr;
		}

		echo acs_je($ar);
		exit;
}


if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();
	
	$cod_art = $m_params->open_request->form_values->f_cod_art;
	$desc_art = $m_params->open_request->form_values->f_desc_art;
	$cod_forn = $m_params->open_request->form_values->f_fornitore;
	$magaz = $m_params->open_request->form_values->f_magazzino;
	
	if (strlen($cod_art) > 0)
		$sql_where.= " AND UPPER(SKCAR0) LIKE '%" . strtoupper($cod_art) . "%' ";
	if (strlen($desc_art) > 0)
		$sql_where.= " AND UPPER(SKDES0) LIKE '%" . strtoupper($desc_art) . "%' ";
	

	if (strlen($cod_forn) > 0)
		$sql_where.= " AND UPPER(SKCFO0) = '" . strtoupper($cod_forn) . "' ";
	
	if (strlen($magaz) > 0)
        $sql_where.= " AND SKCMA0 = '{$magaz}' ";
		
	$sql = "SELECT RRN(KF) AS RRN, SOCA20, SKDIV0, SKAZI0, SKCMA0, SKCFO0, SKRAG0, SKCAR0,
			SKDES0, SKBTO0, SKBTP0, SKCOM0, SKCOT0, SKDEV0, SKFLG1, SKFLA1, SKCVP0, SKCMC0,
            SKGRI0, SKLAE0, SKLAM0
			FROM {$cfg_mod_DeskUtility['file_articoli']} KF
			
			LEFT OUTER JOIN (
	                SELECT COUNT(SOCA20) AS SOCA20, SODIT0, SODIV0, SOCAR0, SODES0
	           		 FROM {$cfg_mod_DeskUtility['file_art_sost']} SO
	           		 GROUP BY SODIT0, SODIV0, SOCAR0, SODES0
	            ) SO0
	           ON KF.SKDIT0 = SO0.SODIT0 AND KF.SKDIV0 = SO0.SODIV0 AND KF.SKCAR0 = SO0.SOCAR0 AND KF.SKDES0 = SO0.SODES0
	        WHERE SKDIT0 = '$id_ditta_default' AND SKDIV0 = '{$m_params->open_request->form_values->f_divisione}' {$sql_where} 
			AND SKTPR0 = '' ORDER BY SKRAG0, SKCAR0";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['SKDIV0'] = trim($row['SKDIV0']);
		$nr['SKAZI0'] = trim($row['SKAZI0']);
		$nr['SKCMA0'] = trim($row['SKCMA0']);
		$nr['SKCMA0'] = trim($row['SKCMA0']);
		$nr['SKCFO0'] = trim($row['SKCFO0']);
		$nr['SKRAG0'] = trim($row['SKRAG0'])." [".trim($row['SKCFO0'])."]";
		$nr['SKCAR0'] = trim($row['SKCAR0']);
		$nr['SKDES0'] = trim($row['SKDES0']);
		$nr['SKBTO0'] = trim($row['SKBTO0']);
		$nr['SKBTP0'] = trim($row['SKBTP0']);
		$nr['SKCOM0'] = trim($row['SKCOM0']);
		$nr['SKFLA1'] = trim($row['SKFLA1']);	
		$nr['SKCOT0'] = trim($row['SKCOT0']);
		$nr['SKDEV0'] = trim($row['SKDEV0']);
		$nr['SKCVP0'] = trim($row['SKCVP0']);
		$nr['SKFLG1'] = trim($row['SKFLG1']);
		$nr['SKCMC0'] = trim($row['SKCMC0']);
		$nr['SKGRI0'] = trim($row['SKGRI0']);
		$nr['SKLAE0'] = trim($row['SKLAE0']);
		$nr['SKLAM0'] = trim($row['SKLAM0']);
		$nr['SOCA20'] = trim($row['SOCA20']);
		$nr['RRN'] 	  = $row['RRN'];
		if ($nr['SKFLG1'] == 'S')
			$nr['SKFLG1_v'] = true;
		else
			$nr['SKFLG1_v'] = false;
		
		$nr = array_map('utf8_encode', $nr); //php7
		$ar[] = $nr;
	
		
	}
	
	echo acs_je($ar);
	exit;
}



if ($_REQUEST['fn'] == 'open_tab'){
	
	$m_params = acs_m_params_json_decode();
	
	$divisione = $m_params->form_values->f_divisione;
	$main_module->setup_scorte_save_divisione($divisione);
	?>
	
	{
		success:true,
		items: [
		{
			xtype: 'panel',
			title: 'Articoli [' + '<?php echo $divisione?>' + ']',
			<?php echo make_tab_closable(); ?>,
			   bbar: ['->', {
							xtype: 'button',
				            text: 'Sincronizza',
				            iconCls: 'icon-button_blue_play-32', 
				            scale: 'large',
				            handler: function() {
				            		acs_show_win_std('Sincronizzazione articoli', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_sincro', {form_values: <?php echo acs_je($m_params) ?>} , 350, 180, {}, 'icon-listino');
				            }
				        }], 
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
			
		items: [  
		
		{
            xtype: 'form',
            itemId : 'sx_form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            layout: 'fit',
            //autoScroll: true,
            title: '',
            flex:0.7,
            items: [				 	
					{
			xtype: 'grid',
			title: 'Lista articoli',
			autoScroll: true,
	        loadMask: true,	
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
			
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',
    							   timeout: 2400000,
							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['RRN', 'SKFLG1', 'SKFLG1_v', 'SKDIV0', 'SKAZI0', 'SKCMA0', 
		        			'SKCFO0', 'SKRAG0', 'SKCAR0', 'SKDES0', 'SKBTO0', 'SKBTP0', 
		        			{name: 'SKCOM0', type: 'float'}, {name: 'SKCOT0', type: 'float'}, 
		        			'SKDEV0', 'SKFLA1', 'SKCVP0', 'SKCMC0', 'SKGRI0', 'SKLAE0', 'SKLAM0',
		        			'SOCA20']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Fornitore',
	                dataIndex: 'SKRAG0',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Articolo',
	                dataIndex: 'SKCAR0',
	                width: 90,
					filter: {type: 'string'}, filterable: true	                
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'SKDES0',
	                flex: 1,
					filter: {type: 'string'}, filterable: true	                
	                },	                
	                {
	                header   : 'Magazzino',
	                dataIndex: 'SKCMA0',
	                 width: 80,
					filter: {type: 'string'}, filterable: true	               
	                },	                
	                {
		                header   : 'T.G.A.', tooltip: 'Tipo gestione Attuale',
		                dataIndex: 'SKFLA1',
		                width: 40,
						filter: {type: 'string'}, filterable: true	               
	                }, {
		                header   : 'T.G.', tooltip: 'Tipo gestione',
		                dataIndex: 'SKFLG1',
		                width: 40,
						filter: {type: 'string'}, filterable: true	               
	                }, {
		                header   : 'Cons.Tot', tooltip: 'Consumo totale',
		                dataIndex: 'SKCOT0', renderer: floatRenderer3,
		                width: 70,
						filter: {type: 'string'}, filterable: true, align: 'right'               
	                }, {
		                header   : 'Cons.Med.', tooltip: 'Consumo medio mensile',
		                dataIndex: 'SKCOM0',  renderer:  floatRenderer2,
		                width: 70,
						filter: {type: 'string'}, filterable: true, align: 'right'	               
	                }	                
	                
	                
	         ], 
	         
	         listeners: {
	         
	        		 
		                selectionchange: function(selModel, selected) {  
		                
		                 var form_a = this.up('panel').up('panel').down('#dx_form');           	
		                
		                 var b_sostituiti = form_a.down('#b_sos');
		                 
		                 if(!Ext.isEmpty(selected[0].data.SOCA20) && selected[0].data.SOCA20 > 0){
		                    b_sostituiti.enable();
		                 }else{
		                    b_sostituiti.disable();
		                 }
		                     
		                	//pulisco eventuali filtri
		                	form_a.getForm().reset();
		                	
		                	console.log('aaaa');
		                	console.log(selected[0]);
		                
		                	//ricarico i dati della form
		                    form_a.getForm().loadRecord(selected[0]);

		                }
	
		}
		
		}	
						 
				],
			buttons: [
			{
				text: 'Ripristino Tipo Gestiona da Anagrafica',
				iconCls: 'icon-button_blue_repeat-32',
				scale: 'large',
				  handler: function() {
				  		 var me = this,
				  		     loc_grid = me.up('form').down('grid');	//bottone       	                	     
            	         std_ajax_request(<?php echo j($_SERVER['PHP_SELF'] . "?fn=ripristina_tipo_gestione")?>, 
            	         {open_request: <?php echo acs_je($m_params); ?>},
                	         function(){
                	         	loc_grid.store.load();
                	         });
	            }
			},
			{ 
				xtype: 'tbfill'
			},	
			{text: 'Stampa',
			            iconCls: 'icon-print-32',
			            scale: 'large',
			            handler: function() {
		                    form = this.up('form').getForm();
	                        this.up('form').submit({
	                        url: 'acs_scorte_setup_articoli_report.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',                        
	                        params: {
	                            form_values: Ext.encode (<?php echo acs_je($m_params) ?>)
						    }
                  			});            	                	     
            	                	                
	            }
			         }
	        ]       
				
        }	//grid
		
		,{
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            title: 'Articolo',
 		            autoScroll: true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: false,
 		            buttons: [
 		            {
 			            text: 'Consumi',
 			            iconCls: 'icon-folder_search-32',
 			            scale: 'large',
 			            handler: function() {
							var form = this.up('form');
 			       			var grid = this.up('panel').up('panel').down('grid');  			            
 			            	var gridrecord = grid.getSelectionModel().getSelection(); 			            
 			            	acs_show_win_std('Dettaglio consumi', <?php echo j($_SERVER['PHP_SELF'] . "?fn=open_dettaglio_consumi") ?>, {rrn: gridrecord[0].get('RRN')}, 500, 520, {}, 'icon-filter-16');
 			            }
 			        },  {
 			            text: 'Sostituiti',
 			            itemId : 'b_sos',
 			            iconCls: 'icon-button_blue_repeat-32',
 			            scale: 'large',
 			            handler: function() {
							var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid');  			            
 			            	var gridrecord = grid.getSelectionModel().getSelection(); 	 	            
 			            	acs_show_win_std('Articoli sostituiti', <?php echo j($_SERVER['PHP_SELF'] . "?fn=open_sostituiti") ?>, {form_values: form_values, cod_art: gridrecord[0].get('SKCAR0')}, 500, 520, {}, 'icon-filter-16');
 			            }
 			        }, {xtype: 'tbfill'},{
 			            text: 'Salva',
 			            iconCls: 'icon-button_blue_play-32',
 			            scale: 'large',
 			            handler: function() {
 			                var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			grid = this.up('panel').up('panel').down('grid'); 
 			       			var gridrecord = grid.getSelectionModel().getSelection(); 	
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_forn',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
						          var jsonData = Ext.decode(result.responseText);
							        var new_rec = jsonData.record;
							       // var gridrecord = grid.getSelectionModel().getSelection();
							      //  console.log(gridrecord);
							       // console.log(new_rec);
							        gridrecord[0].set(new_rec);
			            			
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
 			            }
 			         }],   		            
 		            
 		            		items: [{
 										name: 'SKDIV0',
 										xtype: 'textfield',
 										hidden: true,
 									    anchor: '-15'							
 									 },{
 										name: 'SKCFO0',
 										xtype: 'textfield',
 										hidden: true,
 									    anchor: '-15'							
 									 },{
 										name: 'RRN',
 										xtype: 'textfield',
 										hidden: true,
 									    anchor: '-15'							
 									 },
 									 {
 										name: 'SKAZI0',
 										xtype: 'displayfield',
 										fieldLabel: 'Divisione',
 										value: '',
 										labelWidth: 130,
 										readOnly: true,
 									    anchor: '-15'							
 									 },{
 										name: 'SKRAG0',
 										xtype: 'displayfield',
 										fieldLabel: 'Fornitore',
 										value: '',
 										readOnly: true,
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKCFO0',
 										xtype: 'textfield',
 										hidden: true,
 									    anchor: '-15'							
 									 },{
 										name: 'SKCAR0',
 										fieldLabel: 'Articolo',
 										xtype: 'displayfield',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKDES0',
 										xtype: 'textfield',
 										fieldLabel: 'Descrizione',
 										xtype: 'displayfield',
 										value: '',
 										readOnly: true,
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKCMA0',
 										xtype: 'textfield',
 										fieldLabel: 'Magazzino',
 										xtype: 'displayfield',
 										value: '',
 										readOnly: true,
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKCMC0',
 										xtype: 'textfield',
 										fieldLabel: 'Magazzino centraliz.',
 										xtype: 'displayfield',
 										value: '',
 										readOnly: true,
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKBTO0',
 										xtype: 'textfield',
 										fieldLabel: 'Soglia BTO',
 										value: '',
 										xtype: 'displayfield',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKBTP0',
 										xtype: 'textfield',
 										fieldLabel: 'Soglia BTO CV%',
 										value: '',
 										xtype: 'displayfield',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKDEV0',
 										xtype: 'textfield',
 										fieldLabel: 'Deviazione media',
 										value: '',
 										xtype: 'displayfield',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKCVP0',
 										xtype: 'textfield',
 										fieldLabel: '% Coef.Var.',
 										value: '',
 										xtype: 'displayfield',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKCOT0',
 										fieldLabel: 'Consumo totale',
 										value: '',
 										xtype: 'displayfield',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 }, {
 										name: 'SKCOM0',
 										fieldLabel: 'Consumo medio mensile',
 										value: '',
 										xtype: 'displayfield',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 }, {
 										name: 'SKFLA1',
 										fieldLabel: 'Tipo gestione attuale',
 										value: '',
 										xtype: 'displayfield',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
														 
        								name: 'SKFLG1',
        								xtype: 'checkboxgroup',
        								fieldLabel: 'Tipo gestione',
        								allowBlank: true,
        							   	labelWidth: 130,
        							   	items: [{
        		                            xtype: 'checkbox'
        		                          , name: 'SKFLG1_v' 
        		                          , boxLabel: ''
        		                          , checked: false
        		                          , inputValue: 'S'
        		                       	  
        		                        }]														
						 			},{
 										name: 'SKGRI0',
 										fieldLabel: 'Lead time acquisto',
 										value: '',
 										xtype: 'numberfield',
 										decimalPrecision: 2,
 										hideTrigger  : true,
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKLAE0',
 										fieldLabel: 'Lotto econ acq.',
 										value: '',
 										xtype: 'numberfield',
 										hideTrigger  : true,
 										decimalPrecision: 3,
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SKLAM0',
 										fieldLabel: 'Mult. lotto ec. acq',
 										value: '',
 										xtype: 'numberfield',
 										decimalPrecision: 3,
 										hideTrigger  : true,
 										labelWidth: 130,
 									    anchor: '-15'							
 									 }
 									 
 									 
 									 
 													 
			 		           ]
			 		              }
				  		  
	  	]
	  }
	 ]
	}
<?php exit; 
}

if ($_REQUEST['fn'] == 'form_sincro'){
	$m_params = acs_m_params_json_decode();
	$fornitore = $m_params->form_values->form_values->f_fornitore;
	$divisione = $m_params->form_values->form_values->f_divisione;
	?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
				items: [
					{
						name: 'f_fornitore',
						xtype: 'combo',
						fieldLabel: 'Fornitore',
						forceSelection: true,
						allowBlank: false,									
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona -',
				   		value: '<?php echo $fornitore; ?>',							
					    anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json(options_select_fornitore('Y', $divisione), ''); ?>	
						    ]
						}
					 }
					 ],
					 
			buttons: [					
				{
		            text: 'Conferma',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		             var form = this.up('form').getForm();
		             var form_values = form.getValues();
		             var win = this.up('window');
		             
		           	if (Ext.isEmpty(form_values.f_fornitore)){  
		           	  forn = '';
		           	}else{
		           	  forn = form_values.f_fornitore;
		           	}
		               
		               if(form.isValid()){
		             acs_show_win_std('Sincronizzazione articoli', 'acs_submit_job.php?fn=open_form', { 
           				chiave : {RIRGES:'SCO_SAVE_BTS', RICITI :  <?php echo j($m_params->form_values->form_values->f_divisione) ?>, RIART : forn},
           				vals   : {RIRGES:'SCO_SAVE_BTS', RICITI :  <?php echo j($m_params->form_values->form_values->f_divisione) ?>, RIART : forn}
           				} , 650, 250, {}, 'icon-listino');
		       			}
		                
		            }
		        }
	        
	        
	        ]  
					
					
	}
	
]}


<?php 
}
