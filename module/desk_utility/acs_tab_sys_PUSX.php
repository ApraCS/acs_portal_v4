<?php

require_once("../../config.inc.php");
require_once("acs_gestione_tabelle.php");
$m_params = acs_m_params_json_decode();


$m_table_config = array(
    'TAID' => 'PUSX',
    'title_grid' => '',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
  
    ),
    'title_form' => '',
    'hidden_grid' => 'Y',
    'TAREST' => array(
        'car_i1' 	=> array(
            "start" => 0,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s1' 	=> array(
            "start" => 2,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n1' 	=> array(
            "start" => 3,
            "len"   => 1,
            "riempi_con" => ""
        ),
        //----------------------------//
        'car_i2' 	=> array(
            "start" => 9,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s2' 	=> array(
            "start" => 11,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n2' 	=> array(
            "start" => 12,
            "len"   => 1,
            "riempi_con" => ""
        ),
        //----------------------------//
        'car_i3' 	=> array(
            "start" => 18,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s3' 	=> array(
            "start" => 20,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n3' 	=> array(
            "start" => 21,
            "len"   => 1,
            "riempi_con" => ""
        ),
        //----------------------------//
        'car_i4' 	=> array(
            "start" => 27,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s4' 	=> array(
            "start" => 29,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n4' 	=> array(
            "start" => 30,
            "len"   => 1,
            "riempi_con" => ""
        ),
        //----------------------------//
        'car_i5' 	=> array(
            "start" => 36,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s5' 	=> array(
            "start" => 38,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n5' 	=> array(
            "start" => 39,
            "len"   => 1,
            "riempi_con" => ""
        ),
        //----------------------------//
        'car_i6' 	=> array(
            "start" => 45,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s6' 	=> array(
            "start" => 47,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n6' 	=> array(
            "start" => 48,
            "len"   => 1,
            "riempi_con" => ""
        ),
        //----------------------------//
        'car_i7' 	=> array(
            "start" => 54,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s7' 	=> array(
            "start" => 56,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n7' 	=> array(
            "start" => 57,
            "len"   => 1,
            "riempi_con" => ""
        ),
          //----------------------------//
        'car_i8' 	=> array(
            "start" => 63,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s8' 	=> array(
            "start" => 65,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n8' 	=> array(
            "start" => 66,
            "len"   => 1,
            "riempi_con" => ""
        ),
        //----------------------------//
        'car_i9' 	=> array(
            "start" => 72,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'car_s9' 	=> array(
            "start" => 74,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'car_n9' 	=> array(
            "start" => 75,
            "len"   => 1,
            "riempi_con" => ""
        )
        
    )

    
);


if ($_REQUEST['fn'] == 'exe_canc_sx'){
    
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_aggiorna'){
    
    $ret = array();
    $ar_ins = array();
     
   
    $ar_ins['TAUSUM'] 	= $auth->get_user();
    $ar_ins['TADTUM']   = oggi_AS_date();
    $ar_ins['TADT'] 	= $id_ditta_default;
    $ar_ins['TAID'] 	= $m_table_config['TAID'];
    
    if(isset($m_table_config['fields']['TAREST'])){
        if (!isset($m_params->form_values->TAREST) || !isset($m_table_config['TAREST'])){
            $ret['success'] = false;
            $ret['msg_error'] = "Gestione tabella non configurata correttamente(TAREST) <br> Contattare amministratore di sistema";
            echo acs_je($ret);
            return;
        }
    }
    
    if(isset($m_table_config['fields']['TAREST'])){
        for($i = 1; $i<=9 ; $i++){
            $num = "car_n{$i}";
            if(!isset($m_params->form_values->$num)){
                $m_params->form_values->$num = " ";
            }
        }
        
        $tarest = genera_TAREST($m_table_config, $m_params->form_values);
        $ar_ins['TAREST'] 	= $tarest;
    }
    
    
    if($m_params->form_values->RRN == ''){

        
       $ar_ins['TAUSGE'] 	= $auth->get_user();
       $ar_ins['TADTGE']   = oggi_AS_date();
       $ar_ins['TANR'] 	= $m_params->form_values->TANR;
       if(isset($m_params->form_values->TACINT))
          $ar_ins['TACINT'] 	= $m_params->form_values->TACINT;
       $ar_ins['TADESC'] 	= $m_params->form_values->TADESC;
       $ar_ins['TADES2'] 	= $m_params->form_values->TADES2;
                       
       $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
   
       $stmt = db2_prepare($conn, $sql);
       echo db2_stmt_errormsg();
       $result = db2_execute($stmt, $ar_ins);
       echo db2_stmt_errormsg();
       
    }else{
 
        
        $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
        WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
        
           
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
        
        
    }
     
            
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}



if ($_REQUEST['fn'] == 'open_detail'){
    
    $sql = "SELECT RRN (TA) AS RRN, TA.*
            FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUSX' AND TANR = '{$m_params->tanr}'
            ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if(is_array($row)){
        $rrn = $row['RRN'];
        $xtype = 'displayfield';
        $codice = $row['TANR'];
        $interfaccia = $row['TACINT'];
        $descrizione = $row['TADESC'];
        $note = $row['TADES2'];
        if(is_array($m_table_config['TAREST'])){
            foreach($m_table_config['TAREST'] as $k => $v){
                $row[$k] = trim(substr($row['TAREST'], $v['start'], $v['len']));
                
            }}
    }else{
        
        $sql_p = "SELECT * FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'PUMO' AND TANR = '{$m_params->tanr}'
        ";
        
        $stmt_p = db2_prepare($conn, $sql_p);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_p);
        $row_p = db2_fetch_assoc($stmt_p);
        
        $rrn = "";
        $xtype = 'textfield';
        $codice = trim($row_p['TANR']);
        $interfaccia = trim($row_p['TACINT']);
        $descrizione = trim($row_p['TADESC']);
        $note = trim($row_p['TADES2']);
        
    }


    
    ?>

				
{"success":true, "items": [

 
 		          {  xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [
 		            
 		            {
                    xtype: 'fieldcontainer',
                    flex:1,
                    layout: { 	type: 'hbox',
                    pack: 'start',
                    align: 'stretch'},
                    items: [
                        {
                        xtype : 'textfield',
            		 	name: 'RRN', 
            		 	value : <?php echo j($rrn); ?>,
            		 	hidden : true
                        } ,
                          {
                        xtype : 'textfield',
            		 	name: 'TAREST', 
            		 	value : <?php echo j($row['TAREST']); ?>,
            		 	hidden : true
                        } ,
                         {
            			
            			xtype : <?php echo j($xtype); ?>,
            		 	name: 'TANR', 
            		 	value : <?php echo j($codice); ?>,
            		 	fieldLabel: 'Codice',
            		    width : 140,
            		    fieldStyle: 'font-weight: bold;'
            			},	{
            			xtype : <?php echo j($xtype); ?>,
            		 	name: 'TACINT', 
            		 	fieldLabel: 'Interfaccia',
            		 	value : <?php echo j($interfaccia); ?>,
            		 	labelWidth : 60,
            		 	maxLength : 10,
            		 	margin : '0 0 0 8',
            		 	width : 180
            		  }		
                        
            			]},{
            			xtype : <?php echo j($xtype); ?>,
            		 	name: 'TADESC', 
            		 	value : <?php echo j(trim($descrizione)); ?>,
            		 	fieldLabel: 'Descrizione',
            		 	maxLength : 30,
            		 	width : 330,
            		 	fieldStyle: 'font-weight: bold;'
            			},
            			{
            			xtype : <?php echo j($xtype); ?>,
            		 	name: 'TADES2', 
            		 	value : <?php echo j(trim($note)); ?>,
            		 	fieldLabel: 'Note',
            		 	maxLength : 30,
            		 	width : 330,
            		 	margin : '0 0 20 0',
            			},
            			 <?php for($i = 1; $i <= 9; $i ++){?>
            			{
                    xtype: 'fieldcontainer',
                    flex:1,
                    layout: { 	type: 'hbox',
                    pack: 'start',
                    align: 'stretch'},
                    items: [
                       
            		    {
            			xtype : 'numberfield',
            			hideTrigger : true,
            			valueToRaw: function(value) {
        					return value;
        				},
            		 	name: 'car_i<?php echo $i; ?>', 
            		 	value : <?php echo j($row["car_i{$i}"]); ?>,
            		 	fieldLabel: '<?php echo $i ?>) Estrai da',
            		 	labelWidth : 70,
            		    width : 100,
            		    maxLength : 2,
            		    minValue: 1,
            			maxValue: 15,
            			keyNavEnabled : false,
         				mouseWheelEnabled : false,
            			listeners: {
            				'blur': function(field) {
            					var n_value = field.getValue();
            					if(n_value > 0){	
            						n_value = (n_value < 10 ? '0' : '') + n_value;
            						field.setValue(n_value);
            					}
            				}
            			}
            			},	{
            			xtype : 'numberfield',
            			hideTrigger : true,
            			minValue: 1,
            			maxValue: 5,
            		 	name: 'car_s<?php echo $i; ?>', 
            		 	fieldLabel: 'Nr',
            		 	value : <?php echo j($row["car_s{$i}"]); ?>,
            		 	labelWidth : 25,
            		 	margin : '0 0 0 5',
            		 	width : 55,
            		 	keyNavEnabled : false,
         				mouseWheelEnabled : false,
            		 	maxLength : 1
            		   }, 
            		    {
						xtype: 'checkboxgroup',
						fieldLabel: 'Numerica',
					    margin : '0 0 0 5',
						labelWidth : 60,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'car_n<?php echo $i; ?>' 
                          <?php if($row["car_n{$i}"] == 'N'){?>
                          , checked : true
                          <?php }?>
                          , inputValue : 'N'
                        }]							
					   }
            		
            			]},
            			
            			<?php }?>	
            		
 		            
 		            ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
		              var loc_win = this.up('window');
	       			  Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				     Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_sx',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
									},							        
							        success : function(response, opts){
							        	form.getForm().reset();
							        	loc_win.fireEvent('afterModifica', loc_win);
							      
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
							    
							    }
            	   		  });
				        }

			     },   '->',  
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var loc_win = this.up('window'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if(jsonData.success == false){
					      	    acs_show_msg_error(jsonData.msg_error);
					      	    return false;
					        }else{
					        	 loc_win.fireEvent('afterModifica', loc_win);
					        }
					       },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			     
			     ]
		  
			 	 }  
		 ],
					 
					
					
	}
]
}

<?php 

exit; 

}
