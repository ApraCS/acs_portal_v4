<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
$puvn = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";



$m_table_config = array(
    'TAID' => 'PUVR',
    'title_grid' => 'PUVR-Variabili',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TACINT'   => array('label'	=> 'Interfaccia', 'only_view' => 'F',  'maxLength' => 10),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'numerica' => array('label'	=> 'Numerica', 'short' =>'N', 'width' => 150,  'c_width' => 30, 'maxLength' => 1, 'maskRe' => '/N/', 'tooltip' => 'Variabile numerica'),
        't_var'    => array('label'	=> 'Tipo variabile', 'short' =>'Tv', 'c_width' => 30, 'maxLength' => 1, 'tooltip' => 'Tipo variabile', 'xtype' => 'combo_tipo', 'tab_std' => 'TTPVR'),
        'var_rif'  => array('label'	=> 'Variabile riferim.', 'short' =>'Var. rif.', 'maxLength' => 3, 'xtype' => 'combo_tipo', 'c_width' => 50),
        'var_mst'  => array('label'	=> 'Variabile master', 'xtype' => 'combo_tipo', 'short' =>'Master',  'c_width' => 50),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TRAD'     => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'PUVN'     => array('label'	=> $puvn, 'only_view' => 'C', 'c_width' => 40, 'tooltip' => 'Varianti/opzioni precodificate'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella variabili',
    'j_puvn' => 'Y',
    'ricerca_var' => 'Y',
    'tab_tipo' => 'PUVR',
    'TAREST' => array(
       'numerica' 	=> array(
           "start" => 0,
           "len"   => 1,
           //"format" => "decimal",
           "riempi_con" => ""
       ), 
        't_var' 	=> array(
            "start" => 4,
            "len"   => 1,
            //"format" => "decimal",
            "riempi_con" => ""
        ), 
       'var_rif' 	=> array(
           "start" => 24,
           "len"   => 3,
           //"format" => "decimal",
           "riempi_con" => ""
       ),
        'var_mst' 	=> array(
            "start" => 67,
            "len"   => 3,
            //"format" => "decimal",
            "riempi_con" => ""
        )
        
   
       
   )
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';
