<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>";
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'BITL',
    'title_grid' => 'BITL-Codice listino',
    'fields' => array(
        'TAID'     => array('label'	=> 'tabella', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 300),
        'tp_list'  => array('label'	=> 'TL', 'only_view' => 'C', 'c_width' => 50, 'tooltip' => 'Tipi listino'),
        'list_p' => array('label'	=> 'LP', 'only_view' => 'C','c_width' => 30, 'tooltip' => 'Listino a punti'),
        'list_sp1' => array('label'	=> 'L. Speciali 1', 'only_view' => 'C','c_width' => 90, 'tooltip' => 'Listino speciale 1'),
        'list_sp2' => array('label'	=> 'L. Speciali 2', 'only_view' => 'C','c_width' => 90, 'tooltip' => 'Listino speciale 2'),
        'tpli1'    => array('label'	=> 'Tipo listino 1', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'left', 'width' => 175, 'labelWidth' => 120, 'type' =>'combo', 'tab_std' => 'TTPLI'),
        'tpli2'    => array('label'	=> '2', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'right', 'width' => 73, 'labelWidth' => 20, 'type' =>'combo', 'tab_std' => 'TTPLI'),
        'tpli3'    => array('label'	=> '3', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'right', 'width' => 73, 'labelWidth' => 20, 'type' =>'combo', 'tab_std' => 'TTPLI'),
        'tpli4'    => array('label'	=> '4', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'right', 'width' => 73, 'labelWidth' => 20, 'type' =>'combo', 'tab_std' => 'TTPLI', 'close' => 'Y'),
        
        
        'lis1'    => array('label'	=> 'Listino speciali 1', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 300, 'labelWidth' => 120, 'type' =>'combo', 'tab_sys' => 'BITL'),
        'psp1'    => array('label'	=> '%', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 98, 'labelWidth' => 30, 'type' =>'number', 'close' => 'Y'),
        'vasp1'    => array('label'	=> '>1a Var. maggior.1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'tisp1'    => array('label'	=> '>Tipologia', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUTI'),
        'vasp2'    => array('label'	=> '>2a Var. maggior.1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'tisp2'    => array('label'	=> '>Tipologia', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUTI'),
        'vasp3'    => array('label'	=> '>3a Var. maggior.1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'tisp3'    => array('label'	=> '>Tipologia', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUTI'),
        
        'lis2'    => array('label'	=> 'Listino speciali 2', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'left', 'width' => 300, 'labelWidth' => 120, 'type' =>'combo', 'tab_sys' => 'BITL'),
        'psp2'    => array('label'	=> '%', 'only_view' => 'F',  'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'right', 'width' => 98, 'labelWidth' => 30, 'type' =>'number', 'close' => 'Y'),
        'vasp4'    => array('label'	=> '>1a Var. maggior.2', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'tisp4'    => array('label'	=> '>Tipologia', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUTI'),
        'vasp5'    => array('label'	=> '>2a Var. maggior.2', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'tisp5'    => array('label'	=> '>Tipologia', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUTI'),
        'vasp6'    => array('label'	=> '>3a Var. maggior.2', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUVR'),
        'tisp6'    => array('label'	=> '>Tipologia', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'PUTI'),
              
        'cos1'    => array('label'	=> 'Cond.comm.spec.1', 'only_view' => 'F'),
        'cos2'    => array('label'	=> 'Cond.comm.spec.2', 'only_view' => 'F'),
     
        'cusp1'    => array('label'	=> 'Tipo spedizione 1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'CUSP'),
        'vsp1'    => array('label'	=> 'Importo spese', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' => 'number', 'maxValue' => 99999999),
        'fsp1'    => array('label'	=> 'Tipo spesa', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'type' =>'combo', 'tab_std' => 'TTPSP', 'close' => 'Y'),
        'tpar1'    => array('label'	=> 'Tipo arrotondam.', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' =>'combo', 'tab_std' => 'TTPAR'),
        'deci1'    => array('label'	=> 'Decim.(0-5)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'type' => 'number', 'maxValue' => 5,'close' => 'Y'),
        
        
        'cusp2'    => array('label'	=> 'Tipo spedizione 2', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'CUSP'),
        'vsp2'    => array('label'	=> 'Importo spese', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' => 'number', 'maxValue' => 99999999),
        'fsp2'    => array('label'	=> 'Tipo spesa', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'type' =>'combo', 'tab_std' => 'TTPSP', 'close' => 'Y'),
        'tpar2'    => array('label'	=> 'Tipo arrotondam.', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' =>'combo', 'tab_std' => 'TTPAR'),
        'deci2'    => array('label'	=> 'Decim.(0-5)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'type' => 'number', 'maxValue' => 5,'close' => 'Y'),
        
        'cusp3'    => array('label'	=> 'Tipo spedizione 3', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'CUSP'),
        'vsp3'    => array('label'	=> 'Importo spese', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' => 'number', 'maxValue' => 99999999),
        'fsp3'    => array('label'	=> 'Tipo spesa', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'type' =>'combo', 'tab_std' => 'TTPSP', 'close' => 'Y'),
        'tpar3'    => array('label'	=> 'Tipo arrotondam.', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' =>'combo', 'tab_std' => 'TTPAR'),
        'deci3'    => array('label'	=> 'Decim.(0-5)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'type' => 'number', 'maxValue' => 5,'close' => 'Y'),
        
        'tlis'    => array('label'	=> 'List. prezzi netti', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' =>'combo', 'tab_std' => 'TLPNT'),
        'stag'    => array('label'	=> 'Condiz.elaboraz.', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'maxLength' => 3,'close' => 'Y'),
        
        'lpun'    => array('label'	=> 'Listino a punti', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
        'tar2'    => array('label'	=> 'Tipo arrotondam.', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 220, 'labelWidth' => 120, 'type' =>'combo', 'tab_std' => 'TTPAR'),
        'ndec'    => array('label'	=> 'Decim.(0-5)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 180, 'labelWidth' => 100, 'type' => 'number', 'maxValue' => 5,'close' => 'Y'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella intestazione codice listino',
    'title_tab' => 'Altro',
    'title_tab2' => 'Documenti',
    'TAREST' => array(
        'lis1' 	=> array(
            "start" => 0,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'lis2' 	=> array(
            "start" => 4,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'psp1' 	=> array(
            "start" => 8,
            "len"   => 5,
            "riempi_con" => "",
            "type" => 'decimal',
            "decimal_positions" => 2
        ),
        'psp2' 	=> array(
            "start" => 13,
            "len"   => 5,
            "riempi_con" => "",
            "type" => 'decimal',
            "decimal_positions" => 2
        ),
        'tpli1'	=> array(
            "start" => 18,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpli2'	=> array(
            "start" => 20,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpli3'	=> array(
            "start" => 22,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpli4'	=> array(
            "start" => 24,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpli5'	=> array(
            "start" => 26,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'cfor'	=> array(
            "start" => 28,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'rise1'	=> array(
            "start" => 29,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'rise2'	=> array(
            "start" => 33,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'rise3'	=> array(
            "start" => 37,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'rise4'	=> array(
            "start" => 41,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'rise5'	=> array(
            "start" => 45,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'vasp1'	=> array(
            "start" => 49,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vasp2'	=> array(
            "start" => 52,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vasp3'	=> array(
            "start" => 55,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vasp4'	=> array(
            "start" => 58,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vasp5'	=> array(
            "start" => 61,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vasp6'	=> array(
            "start" => 64,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tisp1'	=> array(
            "start" => 67,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tisp2'	=> array(
            "start" => 70,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tisp3'	=> array(
            "start" => 73,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tisp4'	=> array(
            "start" => 76,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tisp5'	=> array(
            "start" => 79,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tisp6'	=> array(
            "start" => 82,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'atti1'	=> array(
            "start" => 85,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'atti2'	=> array(
            "start" => 86,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'atti3'	=> array(
            "start" => 87,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'atti4'	=> array(
            "start" => 88,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'atti5'	=> array(
            "start" => 89,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'ckdt'	=> array(
            "start" => 90,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'tpdt'	=> array(
            "start" => 91,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'cos1'	=> array(
            "start" => 92,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cos2'	=> array(
            "start" => 95,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cusp1'	=> array(
            "start" => 98,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cusp2'	=> array(
            "start" => 117,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'cusp3'	=> array(
            "start" => 136,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'vsp1'	=> array(
            "start" => 101,
            "len"   => 13,
            "riempi_con" => ""
        ),
        'vsp2'	=> array(
            "start" => 120,
            "len"   => 13,
            "riempi_con" => ""
        ),
        'vsp3'	=> array(
            "start" => 139,
            "len"   => 13,
            "riempi_con" => ""
        ),
        'tpar1'	=> array(
            "start" => 114,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'tpar2'	=> array(
            "start" => 133,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'tpar3'	=> array(
            "start" => 152,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'deci1'	=> array(
            "start" => 115,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'deci2'	=> array(
            "start" => 134,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'deci3'	=> array(
            "start" => 153,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'fsp1'	=> array(
            "start" => 116,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'fsp2'	=> array(
            "start" => 135,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'fsp3'	=> array(
            "start" => 154,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'licp'	=> array(
            "start" => 155,
            "len"   => 4,
            "riempi_con" => ""
        ),
        'tico'	=> array(
            "start" => 159,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpco'	=> array(
            "start" => 161,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tlis'	=> array(
            "start" => 163,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'stco1'	=> array(
            "start" => 164,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'stco2'	=> array(
            "start" => 166,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'stco3'	=> array(
            "start" => 168,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'stag'	=> array(
            "start" => 170,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gdoc'	=> array(
            "start" => 173,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'lpun'	=> array(
            "start" => 174,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'tar2'	=> array(
            "start" => 175,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'ndec'	=> array(
            "start" => 176,
            "len"   => 1,
            "riempi_con" => ""
        )
        
        
        
    
    
    )
    
);


$m_table_config_altro = array(
    'TAID' => 'BITL',
    'fields' => array(
        'rise1'    => array('label'	=> 'Cod. riservatezza 1', 'only_view' => 'F', 'maxLength' => 4),
        'rise2'    => array('label'	=> 'Cod. riservatezza 2', 'only_view' => 'F', 'maxLength' => 4),
        'rise3'    => array('label'	=> 'Cod. riservatezza 3', 'only_view' => 'F', 'maxLength' => 4),
        'rise4'    => array('label'	=> 'Cod. riservatezza 4', 'only_view' => 'F', 'maxLength' => 4),
        'rise5'    => array('label'	=> 'Cod. riservatezza 5', 'only_view' => 'F', 'maxLength' => 4),
        'atti1'    => array('label'	=> 'Valorizzazione 1', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARN'),
        'atti2'    => array('label'	=> 'Valorizzazione 2', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARN'),
        'atti3'    => array('label'	=> 'Valorizzazione 3', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARN'),
        'atti4'    => array('label'	=> 'Valorizzazione 4', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARN'),
        'atti5'    => array('label'	=> 'Valorizzazione 5', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARN'),
        'cfor'    => array('label'	=> 'Contr.fornit.anagraf.', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
    )
    
);

$m_table_config_docu = array(
    'TAID' => 'BITL',
    'title' => 'Documenti',
    'fields' => array(
        'ckdt'    => array('label'	=> 'Controllo validit&agrave;', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'TCKDT'),
        'tpdt'    => array('label'	=> '> Testata/riga doc.', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'TTRDC'),
        'licp'    => array('label'	=> 'List.composiz.promo', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'BITL'),
        'aaa'    => array('label'	=> '[Composiz.promozion.]', 'only_view' => 'F', 'xtype' => 'display'),
        'tico'    => array('label'	=> '>Tipologia docum.', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'BTID', 'list_btid' => 'Y'),
        'tpco'    => array('label'	=> '>Tipo documento', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'BDOC', 'store_bdoc' => 'Y'),
        'bbb'    => array('label'	=> '[Contratti forn.promoz.]', 'only_view' => 'F', 'xtype' => 'display'),
        'stco1'    => array('label'	=> '>1� Stato valido', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'BSTA', 'tacor2' => 'AL'),
        'stco2'    => array('label'	=> '>2� Stato valido', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'BSTA', 'tacor2' => 'AL'),
        'stco3'    => array('label'	=> '>3� Stato valido', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'BSTA', 'tacor2' => 'AL'),
        'ccc'    => array('label'	=> '[Tab. sconti/provvig.]', 'only_view' => 'F', 'xtype' => 'display'),
        'gdoc'    => array('label'	=> 'Prezzi x gruppo doc.', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_std' => 'ARY'),
    ),
    
);


//ROOT_ABS_PATH.
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';
