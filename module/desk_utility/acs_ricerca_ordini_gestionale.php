<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

$o_orig = $s->get_ordine_by_k_docu($m_params->k_ordine);

if ($_REQUEST['fn'] == 'exe_ricom'){
    
    $ret = array();
    
    $anno = $m_params->form_values->f_anno;
    $anno_or = $m_params->form_values->f_anno_or;
    $numero = $m_params->form_values->f_numero;
    
    $sql = "SELECT RE2.RETIDO AS RETIDO, RE2.REINUM AS REINUM, RE2.REAADO AS REAADO, RE2.RENRDO AS RENRDO,
            RE2.RENREC AS RENREC
            FROM {$cfg_mod_DeskArt['file_ricom']} RE
            INNER JOIN {$cfg_mod_DeskArt['file_ricom']} RE2
            ON RE.REDT = RE2.REDT AND RE.RETIDO = RE2.RETPCO AND RE.REINUM = RE2.RETPSV
            AND RE.REAADO = RE2.REAACO AND RE.RENRDO = RE2.RENRCO AND RE.RENREC = RE2.RERIGA
            WHERE RE.REDT = '{$id_ditta_default}' AND RE.RETPCO = 'L1' AND RE.REAACO = {$anno} AND RE.RENRCO = '{$numero}'
            AND RE.RETPSV = 'FB' AND RE.RELIVE = 1";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if($row == false){
        $ret['success'] = false;
        $ret['msg_error'] = "Ordine di produzione non trovato";
        
    } else{
        
        $sql_rd = "SELECT RDRIGA, RDNRDO, RDNREC, RDAADO
                FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                WHERE RDDT = '{$id_ditta_default}' AND RDTIDO = '{$row['RETIDO']}'
                AND RDINUM = '{$row['REINUM']}' AND RDAADO = {$row['REAADO']}
                AND RDNRDO = '{$row['RENRDO']}' AND RDNREC = '{$row['RENREC']}'";
        
        
        $stmt_rd = db2_prepare($conn, $sql_rd);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_rd);
        $row_rd = db2_fetch_assoc($stmt_rd);
        
        $ret['success'] = true;
        $ret['riga'] = $row_rd['RDRIGA'];
        $ret['ordine'] = $row_rd['RDNRDO'];
        $ret['anno'] = $row_rd['RDAADO'];
        $ret['nrec'] = $row_rd['RDNREC'];
        
    }
    
    echo acs_je($ret);
    
    exit;
    
}




// ******************************************************************************************
// FORM principale
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verifica_ordine'){
    ini_set('max_execution_time', 300000);
    $ret = array();
    
    $oe = $s->k_ordine_td_decode_xx($m_params->k_ordine);
    
    //costruzione del parametro
    //ordine assistenza
    $cl_p = str_repeat(" ", 246);
    $cl_p .= sprintf("%-2s", $oe['TDDT']);
    $cl_p .= sprintf("%-2s", $oe['TDOTID']);
    $cl_p .= sprintf("%-3s", $oe['TDOINU']);
    $cl_p .= sprintf("%04d", $oe['TDOADO']);
    $cl_p .= sprintf("%06d", $oe['TDONDO']);
    
    //ordine da abbinare
    $cl_p .= sprintf("%-2s", $oe['TDOTID']);
    $cl_p .= sprintf("%-3s", $oe['TDOINU']);
    $cl_p .= sprintf("%04d", $m_params->rec_selected->anno);
    $cl_p .= sprintf("%06d", $m_params->rec_selected->numero);
    
    
    $k_ordine_orig = $s->k_ordine_to_anno_numero($m_params->k_ordine,
        sprintf("%04d", $m_params->rec_selected->anno),
        sprintf("%06d", $m_params->rec_selected->numero));
    
    if ($m_params->tipo == 'esegui')
        $cl_p .= "Y";
        else //verifica
            $cl_p .= " ";
            
            $cl_p .= str_repeat(" ", 100);
            
            
            $call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
            $call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
            $call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
            
            $cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
            $call_return = $tkObj->PgmCall('UR21JNC', $libreria_predefinita_EXE, $cl_in, null, null);
            
            $rets = $call_return['io_param']['LK-AREA'];
            
            $tkObj->disconnect();
            
            $ret['k_ordine']		= $m_params->k_ordine;
            $ret['k_ordine_orig']	= $k_ordine_orig;
            $ret['codice_cliente']  = substr($rets, 279, 9);
            $ret['ragione_sociale'] = substr($rets, 288, 30);
            $ret['riferimento']     = substr($rets, 318, 30);
            $ret['data_ordine']		= substr($rets, 348, 8);
            $ret['fattura']			= substr($rets, 356, 20);
            $ret['data_fattura']	= substr($rets, 376, 8);
            
            $out_ret = array();
            $out_ret[] = "Codice cliente ................... " . $ret['codice_cliente'];
            $out_ret[] = "Denominazione .................... " . $ret['ragione_sociale'];
            $out_ret[] = "Riferimento ...................... " . $ret['riferimento'];
            $out_ret[] = "Data ordine.... .................. " . print_date($ret['data_ordine']);
            $out_ret[] = "Fattura .......................... " . $ret['fattura'];
            $out_ret[] = "Data fattura ..................... " . print_date($ret['data_fattura']);
            
            $ret['out_esito'] = implode("\n", $out_ret);
            
            $ret['success'] = true;
            echo acs_je($ret);
            
            exit;
}


if ($_REQUEST['fn'] == 'get_json_data_ordini'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    
    $data_eva_i=$m_params->form_values->f_data_eva_i;
    $data_eva_f=$m_params->form_values->f_data_eva_f;
    
    //controllo data
    if (strlen($data_eva_i) > 0)
        $sql_where .= " AND TD0.TDDTEP >= '{$data_eva_i}'";
    if (strlen($data_eva_f) > 0)
        $sql_where .= " AND TD0.TDDTEP <= '{$data_eva_f}'";
            
    $data_reg_i=$m_params->form_values->f_data_reg_i;
    $data_reg_f=$m_params->form_values->f_data_reg_f;
            
    if (strlen($data_reg_i) > 0)
        $sql_where .= " AND TD0.TDDTRG >= '{$data_reg_i}'";
    if (strlen($data_reg_f) > 0)
        $sql_where .= " AND TD0.TDDTRG <= '{$data_reg_f}'";
                    
    $cliente = $m_params->form_values->f_cli;
                    
    if (strlen($cliente) > 0)
        $sql_where.= " AND TD0.TDCCON = '{$cliente}' ";
                        
    $nr_ord =  $m_params->form_values->f_nr_or;
                        
    if (strlen($nr_ord) > 0)
        $sql_where.= " AND TD0.TDNRDO = '{$nr_ord}' ";
                            
    $anno_ord =  $m_params->form_values->f_anno_or;
    if (strlen($anno_ord) > 0)
        $sql_where.= " AND TD0.TDAADO = {$anno_ord} ";
                                
    $sql_where.= sql_where_by_combo_value('TD0.TDTPDO', $m_params->form_values->f_tipo_ordine);
    $sql_where.= sql_where_by_combo_value('TD0.TDSTAT', $m_params->form_values->f_stato_ordine);
                                
                                
    if (strlen($m_params->form_values->k_ordine) > 0){
        $oe = $s->k_ordine_td_decode_xx($m_params->form_values->k_ordine);
        
        $sql_where .= " AND TD0.TDDT = '{$oe['TDDT']}' AND TD0.TDTIDO = '{$oe['TDOTID']}' AND TD0.TDINUM = '{$oe['TDOINU']}' AND TD0.TDAADO = {$oe['TDOADO']} AND TD0.TDNRDO = {$oe['TDONDO']}";
    }
                                
                                
    if (strlen($m_params->form_values->f_contract) > 0){
        $ar_contract = explode("_", $m_params->form_values->f_contract);
        $sql_where .= " AND TD0.TDANRF =  {$ar_contract[0]} AND TD0.TDNRRF = {$ar_contract[1]}";
    }
                                
                                
    if(strlen($m_params->form_values->f_riga) > 0){
        $riga = $m_params->form_values->f_riga;
        $anno = $m_params->form_values->f_anno;
        $anno_or = $m_params->form_values->f_anno_or;
        $nrec = $m_params->form_values->f_nrec;
        $join = "LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                ON TD0.TDDT = RD.RDDT AND TD0.TDTIDO = RD.RDTIDO AND TD0.TDINUM = RD.RDINUM AND
                TD0.TDAADO = RD.RDAADO AND TD0.TDNRDO = RD.RDNRDO";
        
        $select = ", RD.*";
        $sql_where .= " AND RDRIGA = '{$riga}'";
        if($nrec != '')
            $sql_where .= " AND RDNREC = '{$nrec}'";
            
    }
                                
    /* Se ricevo start/limit da plugin paginazione */                            
    if (isset($_REQUEST['start']) && isset($_REQUEST['limit'])){
        $limit_offset = " LIMIT {$_REQUEST['limit']} OFFSET {$_REQUEST['start']}";
    } else {
        $limit_offset = '';
    }
           
    
    $sql = "SELECT TD0.TDAADO, TD0.TDNRDO, TD0.TDDT, TD0.TDTIDO, TD0.TDINUM, TD0.TDTPDO, TD0.TDVSRF, TD0.TDDTEP, TD0.TDDTRG, TD0.TDSTAT,
            TD.TDDCON, TD.TDCLOR, TD.TDFMTO, TD.TDMTOO, TD.TDDTCF, TD.TDBLEV, TD.TDBLOC, TD.TDFMTS, TD.TDFN15, TD.TDDTOR
            {$select}
            FROM {$cfg_mod_Spedizioni['file_testate_doc_gest_cliente']} TD0
            {$join}
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
              /* ON TD.TDDT = '{$id_ditta_default}'
              AND TD.TDDTOR = CASE WHEN TD0.TDDT = '{$id_ditta_default}' THEN '' ELSE TD0.TDDT END
              */
              ON TD.TDDT = TD0.TDDT
              AND TD.TDOTID = TD0.TDTIDO  
              AND TD.TDOINU = TD0.TDINUM  
              AND TD.TDOADO = TD0.TDAADO 
              /* AND SUBSTR(TD.TDONDO, 1, 6) = TD0.TDNRDO */
              AND TD.TDONDO = TD0.TDNRDO
            WHERE TD0.TDDT = '{$id_ditta_default}' AND TDTIDO IN('VO', 'VC') {$sql_where}
            ORDER BY TDAADO DESC
            $limit_offset";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
            
            /* Conteggio record, per plugin paginazione */
            $sql_c = "SELECT COUNT(*) AS C_ROW
	          FROM {$cfg_mod_Spedizioni['file_testate_doc_gest_cliente']} TD0
	          {$join}
	          WHERE TDDT = '{$id_ditta_default}' AND TDTIDO IN('VO', 'VC')
	          {$sql_where}";
	          
	          $stmt_c = db2_prepare($conn, $sql_c);
	          echo db2_stmt_errormsg();
	          $result = db2_execute($stmt_c);
	          $row_c = db2_fetch_assoc($stmt_c);
	          
	          while($row = db2_fetch_assoc($stmt)){
	              $nr = array();
	              $nr['count']	    = $row_c['C_ROW'];
	              $nr['anno']	        = trim($row['TDAADO']);
	              $nr['numero']	    = sprintf("%06s", trim($row['TDNRDO']));
	              $nr['k_ordine']     = implode("_", array($row['TDDT'], $row['TDTIDO'], $row['TDINUM'], $row['TDAADO'], sprintf("%06s", trim($row['TDNRDO'])), " ", $row['TDDT'] ));
	              $nr['riferimento']	= trim($row['TDVSRF']);
	              $nr['ordine']	    = implode("_", array(trim($row['TDAADO']), sprintf("%06s", trim($row['TDNRDO'])), trim($row['TDTPDO']) ));
	              $nr['tipologia']	= trim($row['TDTPDO']);
	              
	              $nr['cliente']	    = acs_u8e(trim($row['TDDCON']));
	              $nr['raggr']        = trim($row['TDCLOR']);
	         
	              $nr['data_eva']	    = trim($row['TDDTEP']);
	              $nr['data_reg'] 	= trim($row['TDDTRG']);
	              $nr['stato']	    = trim($row['TDSTAT']);
	              $nr['riga']	        = $row['RDRIGA'];
	              $nr['RDDART'] 	    = trim($row['RDDART']);
	              $nr['RDART']	    = trim($row['RDART']);
	              $nr['RDQTA']	    = $row['RDQTA'];
	              $nr['RDNREC']	    = $row['RDNREC'];
	              $nr['fl_art_manc']  = $s->get_fl_art_manc($row);
	              $nr['fl_bloc']      = $s->get_ord_fl_bloc($row);
	              $nr['art_da_prog']  = $s->get_art_da_prog($row);
	              $nr['anno_l1']	    = $m_params->form_values->f_anno;
	              $nr['numero_l1']	= $m_params->form_values->f_numero;
	              $ar[] = $nr;
	              
	              //????????? $s->get_ordine_by_k_docu($nr['k_ordine']);
	              
	          }
	          
	          
	          echo acs_je(array(
	              'sql' => $sql,
	              'total' => $row_c['C_ROW'],  //prima serve conteggiare il numero totale di record
	              'root' => $ar)
	              );
	          
	          
	          exit;
	          
}



if ($_REQUEST['fn'] == 'ricerca_ordini_grid'){
    $m_params = acs_m_params_json_decode();
    
    
    ?>

{"success":true, "items": [

     { xtype: 'form'
     , bodyStyle: 'padding: 10px'
     , bodyPadding: '5 5 0'
     , autoScroll:true
     , frame: true
     , layout: 
         {  type: 'vbox',
           align: 'stretch'
    	 }
    
     , items: [
         , { xtype: 'fieldcontainer'
    	 , layout: 
    	     { type: 'hbox'
    		 ,	pack: 'start'
    	     , align: 'stretch'
    	     }
    	 , items: [
    	 <!-- filtro Nr. Ordine -->
    	 
	       { xtype: 'displayfield',
	         fieldLabel: 'Anno / Nr. Ordine',
	         labelWidth : 141,
	         margin: "0 10 0 9"	
	       },
	               
	       { 
	       
	       xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_anno_or'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },{ 
	       
	         xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_nr_or'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },
	       { 
	       
	       xtype: 'textfield'
           , name: 'f_riga'
           , fieldLabel: 'Riga'
           , width : 80
           , labelWidth : 30
           , anchor: '-15'
           , margin: "0 5 0 10"	
	       },
    	   <!-- filtro cliente -->
    	   , { flex: 1
    	   , xtype: 'combo'
    	   , name: 'f_cli'
    	   , fieldLabel: 'Cliente'
    	   , labelWidth : 50
    	   , anchor: '-15'
    	   , margin: "0 10 0 43"	
    	   , minChars: 2			
           , store: 
               { pageSize: 1000            	
    		   , proxy: 
    		       { type: 'ajax'
    		       , url : '../desk_vend/acs_get_select_json.php?select=search_cli_anag'
    		       , reader: 
    		           { type: 'json'
    		           , root: 'root'
    		           , totalProperty: 'totalCount'
    			       }
    			   }
    	       , fields: ['cod', 'descr', 'out_loc', 'ditta_orig'],		             	
    	       }
    	   , valueField: 'cod'
    	   , displayField: 'descr'
    	   , typeAhead: false
    	   , hideTrigger: true
    	   , anchor: '100%'
    	   , listConfig: 
    	       { loadingText: 'Searching...'
    	       , emptyText: 'Nessun cliente trovato'
    	       // Custom rendering template for each item
    	       , getInnerTpl: function() 
    	           { return '<div class="search-item">' +
    	                    '<h3><span>{descr}</span></h3>' +
    	                    '[{cod}] {out_loc} {ditta_orig}' + 
    	                    '</div>';
    	           }                
                    
               }
           , pageSize: 1000
    	   }   
    	   <!-- end cliente -->
    	   
		   ] <!-- end items field container -->
		 } <!-- end field container -->
		 
       , { xtype: 'fieldcontainer'
    	 , layout: 
    	     { type: 'hbox'
    		 ,	pack: 'start'
    	     , align: 'stretch'
    	     }
    	 , items: [

	       { xtype: 'displayfield',
	         fieldLabel: 'Anno / Nr. Ordine prod. ',
	         labelWidth : 141,
	         margin: "0 10 0 10"	
	       },
	       
	        { xtype: 'hiddenfield'
           , name: 'f_nrec'
	       },
	       
	       { 
	       
	       xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_anno'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },{ 
	       
	         xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_numero'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },
	       {
            fieldLabel: '',
            name: 'f_conferma',	
            margin : '0 0 0 10',
            xtype: 'checkboxfield',
            inputValue: 'Y',
            listeners: {
            	change: function(comp, check){
                     if(check == true){
                         var form = comp.up('form').getForm();
                         var form_values = form.getValues();
                         Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ricom',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},				
										        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        
							        if(jsonData.success == false){
					      	    		acs_show_msg_error(jsonData.msg_error);
							        }else{
							        	form.findField('f_riga').setValue(jsonData.riga);
							        	form.findField('f_nr_or').setValue(jsonData.ordine);
							        	form.findField('f_anno_or').setValue(jsonData.anno);
							        	form.findField('f_nrec').setValue(jsonData.nrec);
							        }
							        
							        
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
                         
                         
                     }
                    }
                 } 
                 
                 
                 
                 
                 
                         
        },
        
    		 {
    			name: 'f_contract',						
    			xtype: 'textfield',
    			fieldLabel: 'Contract',
    		    labelAlign : 'right',
    		    margin : '0 0 0 65',
    		    readOnly: true					
    		},
    		{										  
    			xtype: 'displayfield',
    			editable: false,
    			fieldLabel: '',
    			padding: '0 0 0 5',
    		    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
    		    listeners: {
		            render: function( component ) {
		            
		            	m_form = this.up('form').getForm();
		            
						component.getEl().on('dblclick', function( event, el ) {
						
					    var my_listeners = {
    	    		  			afterOkSave: function(from_win, k_ordine){
            						from_win.close();
            					    var ar_ordine = k_ordine.split("_");
            						m_form.findField('f_contract').setValue(ar_ordine[3] + '_' + ar_ordine[4]);
            						
    				        		}
    		    				};	
    		        		
    		        		 var cliente = m_form.findField('f_cli').getValue();
    		        		 if(Ext.isEmpty(cliente)){
	        		 			acs_show_msg_error('Selezionare prima un cliente');
				 			    return;
    		        		 }
    		        		 
    				         acs_show_win_std('Ricerca su archivio ordini', 
    				         	'../base/acs_seleziona_ordini_gest.php?fn=open', {
    				         		cod_cli: cliente,
    				         		doc_gest_search: 'CONTRACT_abbina',
    				         		show_parameters: false,
    				         		auto_load: true
    				         	}, 800, 550,  my_listeners, 'icon-clessidra-16');	    
						});										            
		             }
				}										    
    		    
    		    
    		} 
    	   
		   ] <!-- end items field container -->
		 } <!-- end field container -->         
       , { xtype: 'fieldcontainer'
	     , layout: 
	       { type: 'hbox'
	       , pack: 'start'
	       , align: 'stretch'
		   }
		 , items: [
    	       { name: 'f_data_reg_i'
    		   , margin: "0 10 0 10"   
    		   , flex: 1        
    		   , labelWidth :150   		
    		   , xtype: 'datefield'
    		   , startDay: 1 //lun.
    		   , fieldLabel: 'Data registrazione iniziale'
    		   , labelAlign: 'left'
    		   , format: 'd/m/Y'
    		   , submitFormat: 'Ymd'
    		   , allowBlank: true
    		   , anchor: '-15'
    		   }
    		 , { name: 'f_data_reg_f'
    		   , margin: "0 10 0 10"						     
    		   , flex: 1						                     		
    		   , xtype: 'datefield'
    		   , labelWidth :120
    		   , startDay: 1 //lun.
    		   , fieldLabel: 'finale'
    		   , labelAlign: 'right'						   
    		   , format: 'd/m/Y'
    		   , submitFormat: 'Ymd'
    		   , allowBlank: true
    		   , anchor: '-15'
    		   }
		   ]
		 }
		 
	   ,     		 { xtype: 'fieldcontainer'
					 , layout: { type: 'hbox'
							   , pack: 'start'
							   , align: 'stretch'
								}
					 , items: [
						       { name: 'f_data_eva_i'
							   , margin: "0 10 0 10"   
							   , flex: 1        
							   , labelWidth :150    		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data evasione iniziale'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   }
							    
							,  { name: 'f_data_eva_f'
							   , margin: "0 10 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , labelWidth :120 
							   , startDay: 1 //lun.
							   , fieldLabel: 'finale'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   }
						
					   ] 
					 },	 
					 
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ 
						
						{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ 
						 { 
        		 	xtype: 'fieldcontainer',
        		 	layout: { 	type: 'hbox',
        				    pack: 'start',
        				    align: 'stretch'},						
        		 	items: [
        		 		{
        					xtype: 'radiogroup',
        					fieldLabel: 'Tipologia',
        					margin: "0 10 0 10",
        					labelWidth : 150,
        				    items: [{
                                xtype: 'radio'
                              , name: 'f_tipol' 
                              , boxLabel: 'Ordini'
                              , checked : true
                              , width : 70
                              , inputValue: 'VO'
                            },{
                                xtype: 'radio'
                              , name: 'f_tipol' 
                              , boxLabel: 'Contract' 
                              , inputValue: 'VC'
                            }],
                            listeners: {
                            	change: function(field,newVal) {
                            	    var tipologia = newVal.f_tipol;	
                              		if (!Ext.isEmpty(tipologia)){
                                    	combo_tipo = this.up('form').down('#c_tipo');                      		 
                                     	combo_tipo.store.proxy.extraParams.tacor2 = tipologia;
                                    	combo_tipo.store.load(); 
                                    	
                                    	combo_stato = this.up('form').down('#c_stato');                      		 
                                     	combo_stato.store.proxy.extraParams.tacor2 = tipologia;
                                    	combo_stato.store.load();                              
                                     }
                                     
        
                                    }
                           }							
        				}
        		 	]
        		  },
						    {
            				name: 'f_tipo_ordine',
            				xtype: 'combo',
            				itemId : 'c_tipo',
                        	anchor: '-15',
					        margin: "0 43 5 10",	
					    	labelWidth : 150,  
            				fieldLabel: 'Tipi ordine',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            			    flex: 1,  
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				 store: {
        			        autoLoad: true,
        					proxy: {
        			            type: 'ajax',
        			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
        			            actionMethods: {
        				          read: 'POST'
        			        },
        	                	extraParams: {
        	                	    tabella : 'BDOC',
        	    		    		tacor2: 'VO',
        	    				},				            
        			            doRequest: personalizza_extraParams_to_jsonData, 
        			            reader: {
        			            type: 'json',
        						method: 'POST',						            
        				            root: 'root'						            
        				        }
        			        },       
        					fields: ['id', 'text'],		             	
        	            },listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
            			},{
							name: 'f_stato_ordine',
							xtype: 'combo',
							itemId : 'c_stato',
							multiSelect: true,
							fieldLabel: 'Stato ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "0 43 0 10",
						    labelWidth : 150,
						    flex : 1,
						    multiSelect: true,						   													
							 store: {
        			        autoLoad: true,
        					proxy: {
        			            type: 'ajax',
        			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
        			            actionMethods: {
        				          read: 'POST'
        			        },
        	                	extraParams: {
        	                	    tabella : 'BSTA',
        	    		    		tacor2: 'VO',
        	    				},				            
        			            doRequest: personalizza_extraParams_to_jsonData, 
        			            reader: {
        			            type: 'json',
        						method: 'POST',						            
        				            root: 'root'						            
        				        }
        			        },       
        					fields: ['id', 'text'],		             	
        	            }						 
						}]},
						{
							xtype: 'button',	
							text: '<b>Applica filtri</b>',
							margin: "0 5 30 100",
							width : 115,
							anchor: '-15',
							iconCls: 'icon-filter-32',
							scale: 'large',	
							handler: function() {
		            	
								l_form = this.up('form').getForm();
	            				l_grid = this.up('window').down('grid');
	            				var cliente = l_form.findField('f_cli').getValue();
	            				var nr_ord = l_form.findField('f_nr_or').getValue();
	            				
	            				if(!cliente && !nr_ord){
	            				
	            				acs_show_msg_error('Inserire cliente o nr ordine');
									  return false;
	            				
	            				}

	            						l_grid.store.proxy.extraParams.form_values = l_form.getValues();
	            						l_grid.store.load();
	            						l_grid.getView().refresh();	   
		                     
							 }
							  
							},
							
							 {
	            	xtype: 'button',	
		        	text: '<b>Nuovo ordine</b>',
		            iconCls: 'icon-blog_add-32',
		            scale: 'large',	
		            margin: "0 10 30 0",
					width : 120,
					anchor: '-15',
		       		handler: function() {
		       		
		       			var grid = this.up('window').down('grid');
						
						var my_listeners = {
							afterOkSave: function(from_win, k_ordine){
								var k_ordine_ar = k_ordine.split('_');
								acs_show_msg_info('Creato ordine ' + [k_ordine_ar[1], k_ordine_ar[3], k_ordine_ar[4]].join('_'));
								from_win.destroy();
								
								//eseguo ajax per recuperare testata nuovo ordine e caricarla su grid								
								grid.store.proxy.extraParams.form_values = {k_ordine: k_ordine};
		         				grid.store.load();								
							}
						};
						acs_show_win_std('Intestazione nuovo ordine', 
							'../base/acs_protocollazione_SV2.php?fn=open_form', 
							{
								protocollazione_config: 'base_dati'
							}, 450, 550, my_listeners, 'icon-shopping_cart-16');
				    }                  
		  
		        } 
						
						]
					 },				 	
					 {
			xtype: 'grid',
			flex: 1,
			name: 'f_ordini',
			margin: "0 10 10 10",
			multiSelect : true,	
	       features: [
	
				{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ]
			}],
			loadMask: true,	
			
			store: {
				xtype: 'store',
				autoLoad: false,
	            pageSize: 250, 
						proxy: {
						   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_ordini', 
						   timeout: 240000,
						   method: 'POST',								
						   type: 'ajax',

					       actionMethods: {
					          read: 'POST',
					         
					        },
					        
					        
					           extraParams: {
								 open_request: <?php echo acs_raw_post_data(); ?>,
								 form_values: ''
	        				},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
					
        			fields: ['count', 'art_da_prog', 'fl_bloc', 'fl_art_manc', 'raggr', 'ordine', 'anno', 'numero', 'riferimento', 'tipologia', 'stato', 'data_eva', 'data_reg', 'k_ordine', 'riga', 'RDNREC', 'RDDART', 'RDART', 'RDQTA', 'anno_l1', 'numero_l1', 'cliente']
									
			}, //store
				

			      columns: [	
			      <?php echo dx_mobile(); ?>,
			      {
	                header   : 'Anno',
	                dataIndex: 'anno',
	                width: 40,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
				{
	                header   : 'Numero',
	                dataIndex: 'numero',
	                width: 70,
	                filter: {type: 'string'}, 
	                filterable: true
	            }, 
	            {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',	  
	    			    renderer: function(value, metaData, record){
	    			    
	    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=15>';
	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=15>';			    	
	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=15>';
	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=15>';    			    				    	
	    			    	}},
	    			    			
    	         {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini MTO',  
    	    		dataIndex: 'fl_art_manc',
					renderer: function(value, p, record){
    			    	if (record.get('fl_art_manc')==20) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_red.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=15>';			    	
    			    	}},
    			    	
	    	     {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>',width: 30, tdCls: 'tdAction', tooltip: 'Articoli mancanti MTS',
	        			dataIndex: 'art_da_prog',
						renderer: function(value, p, record){
	    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=15>';
			    			if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=15>';			    	
			    			if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=15>';	    			    	
				}},
	            {
	                header   : 'Tipo',
	                dataIndex: 'tipologia',
	                width: 40,
	                filter: {type: 'string'}, 
	                filterable: true,
	                tdCls: 'tipoOrd',
	                renderer: function (value, metaData, record, row, col, store, gridView){						
				      metaData.tdCls += ' ' + record.get('raggr');										
			          return value;			    
					}
	            },
	            {
	                header   : 'Data',
	                dataIndex: 'data_reg',
	                renderer: date_from_AS,
	                width: 100,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Evasione',
	                dataIndex: 'data_eva',
	                renderer: date_from_AS,
	                width: 100
	            },
	            {
	                header   : 'Stato',
	                dataIndex: 'stato',
	                width: 50,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Cliente',
	                dataIndex: 'cliente',
	                width: 150,
	                filter: {type: 'string'},
	                filterable: true
	            },
				{
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                flex:1,
	                filter: {type: 'string'}, 
	                filterable: true
	            }
	            
	        
	         ] ,		
	         
	         		listeners:   { 
	         			 afterrender: function(comp){
        	  
    					//autocheck degli ordini che erano stati selezionati
            	  		comp.store.on('load', function(store, records, options) {
    						t_rows = comp.store.totalCount;	
    						l_win = comp.up('window');
    					    l_win.setTitle('Ricerca su archivio ordini di vendita [Nr ordini ' + records[0].data.count +']');
        										
    					}, comp);
        	 		 },
	         		  
	         		  
	         		    celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							 rec = iView.getRecord(iRowEl);							  
							 col_name = iView.getGridColumns()[iColIdx].dataIndex;
							 iEvent.stopEvent();
							  <?php if($m_params->from_ot == 'Y'){?>
							  acs_show_win_std('Duplica ordine cliente ' +rec.get('ordine'), 'acs_dup_righe_trasf.php?fn=open_form', {k_ord_dup : rec.get('k_ordine'), k_ordine_trasf : <?php echo j($m_params->k_ordine); ?>}, 900, 550, null, 'icon-leaf-16');
						     <?php }else{?>
						 	 if(col_name == 'art_da_prog')
								show_win_art_critici(rec.get('k_ordine'), 'MTS');	
						     if(col_name == 'fl_art_manc')
								show_win_art_critici(rec.get('k_ordine'), 'MTO');	
						     <?php }?>
						  }
			   		  	},
	         		  
	         		  
	         		<?php if($m_params->from_ot != 'Y'){ ?>
	         		  
	         		  itemcontextmenu : function(grid, rec, node, index, event) 
	         		      { event.stopEvent();
	         		        var voci_menu = [];
	         		      
					        voci_menu.push(
					          { text: 'Visualizza righe'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function() 
					              { acs_show_win_std( null
					                               , '../desk_utility/acs_get_order_rows_gest_order_entry.php'
					                               , { from_righe_info: 'Y'
					                                 , from_anag_art : 'Y'
					                                 , k_ordine: rec.get('k_ordine')
					                                 , riga : rec.get('riga')
					                                 , modificabile: 'Y'	//ToDo: solo per gli ordini creati da qui!
					                                 }); 
					                                 
					                                     		
		        		          }
		        		         
		    		          }
		    		        );	
		    		        
		    		        
	         		        if(rec.get('riga') > 0){
	         		        
	         		        	if(rec.get('RDQTA') > 0){
        	         		         voci_menu.push({
        				         		text: 'Visualizza distinta componenti',
        				        		iconCls : 'icon-folder_search-16',          		
        				        		handler: function() {
        				        		   acs_show_win_std('Distinta componenti', '../desk_utility/acs_get_riga_componenti.php', {k_ordine: rec.get('k_ordine'), nrec: rec.get('RDNREC'), d_art: rec.get('RDDART'), c_art : rec.get('RDART'), riga : rec.get('riga'), from_anag_art : 'Y'}, 1300, 450, {}, 'icon-folder_search-16');          		
        				        		}
        				    		});
    				    		}
    				    		
    				    	if(rec.get('ordine') != '' && rec.get('RDNREC') > 0){
    				    		voci_menu.push({
    				         		text: 'Visualizza distinta di lavorazione',
    				        		iconCls : 'icon-folder_search-16',          		
    				        		handler: function() {
    				        		
    				        		var row_l1 = {RETPCO: 'L1', RETPSV : 'FB', REAACO : rec.get('anno_l1'), RENRCO : rec.get('numero_l1')};
    				        		
    				        		
    				        		     acs_show_win_std( 'Distinta di lavorazione'
			   							   , '../desk_utility/acs_get_riga_componenti.php'
			   							   , {row : row_l1, from_lotto : 'Y', from_op : 'Y', k_ordine : rec.get('k_ordine'), riga_comp : rec.get('riga_comp'), desc_art: rec.get('RDDART'), cod_art : rec.get('RDART')}
			   							   , 1300, 450, null
			   							   , 'icon-folder_search-16'
			   							   ); 
    				        		}
    				    		});
    				    		}
	         		     
	         		        }
		    		        
		    		          voci_menu.push(
					          { text: 'Visualizza packing list'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function()  {
					               <?php if($m_params->filter_type == 'desk_prod'){?>
					                   acs_show_win_std(null, 'acs_win_colli_ord.php?fn=open_tab', 
	    			  				   {k_ordine : rec.get('k_ordine')});	 
					               <?php }else{?>
					               		show_win_colli_ord(rec, rec.get('k_ordine'));
					               <?php }?>
					          		           		
		        		          }
		    		          }
		    		        );	
		    		        
		    		            voci_menu.push(
					          { text: 'Visualizza dettagli ordine'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function()  {
					          		acs_show_win_std('Dettaglio ordini', '../desk_vend/acs_win_dettaglio_ordine.php?fn=open_win', 
	    			  				{k_ordine : rec.get('k_ordine')}, 400, 550, null, 'icon-leaf-16');	            		
		        		          }
		    		          }
		    		        );	
		    		        
		    		           if(rec.get('riga') == null){
		    		            voci_menu.push(
    					          { text: 'Elenco esploso distinta componenti'
    					          , iconCls : 'icon-leaf-16'
    					          , handler: function()  {
    					          		acs_show_win_std('Riepilogo articoli/componenti ', '../desk_utility/acs_elenco_riepilogo_art_comp.php?fn=open_elenco', {k_ordine: rec.get('k_ordine'), elenco : 'Y'}, 1350, 450, {}, 'icon-leaf-16');   	     	          		
    		        		          }
    		    		          }
    		    		        );	
    		    		        
    		    		        voci_menu.push({
				         		text: 'Riepilogo articoli/componenti',
				        		iconCls : 'icon-print-16',          		
				        		handler: function() {
				        			acs_show_win_std('Riepilogo articoli/componenti', '../desk_utility/acs_report_riepilogo_art_comp.php?fn=open_form', {k_ordine: rec.get('k_ordine')}, 400, 300, {}, 'icon-print-16');   	     	
				        		}
				    		  });
				    		  <?php if($m_params->filter_type != 'desk_prod'){ ?>
				    		  voci_menu.push(
    					          { text: 'Modifica testata'
    					          , iconCls : 'icon-leaf-16'
    					          , handler: function()  {
    					          		acs_show_win_std('Modifica testata ' + rec.get('ordine'), '../base/acs_panel_testata_ordine_gest_SV2.php?fn=open_form', {tddocu: rec.get('k_ordine')}, 900, 540, {}, 'icon-edit-16');   	     	          		
    		        		          }
    		    		          }
    		    		        );
    		    		        
    		    		        
				    		  voci_menu.push(
    					          { text: 'Sviluppo fabbisogno'
    					          , iconCls : 'icon-gear-16'
    					          , handler: function()  {
    					          		acs_show_win_std('Scrivi messaggio per ordine ' + rec.get('ordine'), '../base/acs_panel_messaggio_ordine_gest_SV2.php?fn=open_form', {
    					          			messaggio: 'SVIL_FAB_DOC',
    					          			tddocu: rec.get('k_ordine')}, 900, 500, {}, 'icon-gear-16');   	     	          		
    		        		          }
    		    		          }
    		    		        ); 
    		    		        
    		    		        <?php }?>
    		    		        
    		    		        
    					           id_selected = grid.getSelectionModel().getSelection();
            			     		 list_selected_id = [];
            			     		 for (var i=0; i<id_selected.length; i++){
            			     			list_selected_id.push(id_selected[i].get('k_ordine'));
            						 }
    		    		        
    		    		        if(list_selected_id.length == 1){
    		    		           voci_menu.push(
    					          { text: 'Valorizzazione distinta componenti'
    					          , iconCls : 'icon-folder_search-16'
    					          , handler: function()  {
    					          
    					             	acs_show_win_std(null, '../desk_utility/acs_export_ordini.php?fn=open_form', {
    					          	    list_selected_id: list_selected_id, tacor1 : 'BR24A0'});   	     	          		
    		        		          }
    		    		          }
    		    		        ); }	
    		    		       
    		    		            voci_menu.push(
    					          { text: 'Export distinta componenti valorizzata'
    					          , iconCls : 'icon-outbox-16'
    					          , handler: function()  {
    					              acs_show_win_std(null, '../desk_utility/acs_export_ordini.php?fn=open_form', {
    					              list_selected_id: list_selected_id, tacor1 : 'BS24A0', type : 'E'});   	     	          		
    		        		          }
    		    		          }
    		    		        );    
    		    		        
    		    		        
    		    		         
    		    		        	    	
    		    		        	    		        
		    		           
		    		           }
		    		        
					        var menu = new Ext.menu.Menu({ items: voci_menu }).showAt(event.xy);
						  }
						  
						 <?php }?> 
					  },
					  dockedItems:
                        [   
                            { xtype: 'pagingtoolbar',
                                dock: 'bottom',
                                displayMsg: '{0} - {1} of {2}',
                                emptyMsg: 'No data to display',
                                listeners: {
                                    afterrender: function(comp){    //associa lo store del paginazione a quello della grid
                                        var grid = comp.up('grid');
                                        comp.bindStore(grid.store);
                                    }
                                }
                            }
                        ]
	   		
			
		} <!-- end grid -->
		 
						 
				],	
				

			
	  /*  buttons: [
	      	         
	            {
	            	//Aggiunti (protocolla)
		        	text: 'Crea nuovo ordine',
		            iconCls: 'icon-sub_blue_add-32',
		            scale: 'large',	
		       		handler: function() {
		       		
		       			var grid = this.up('form').down('grid');

						var my_listeners = {
							afterOkSave: function(from_win, k_ordine){
								var k_ordine_ar = k_ordine.split('_');
								acs_show_msg_info('Creato ordine ' + [k_ordine_ar[1], k_ordine_ar[3], k_ordine_ar[4]].join('_'));
								from_win.destroy();
								
								//eseguo ajax per recuperare testata nuovo ordine e caricarla su grid								
								grid.store.proxy.extraParams.form_values = {k_ordine: k_ordine};
		         				grid.store.load();								
							}
						};
						acs_show_win_std('Intestazione nuovo ordine', 
							'../base/acs_protocollazione_SV2.php?fn=open_form', 
							{
								protocollazione_config: 'base_dati'
							}, 450, 550, my_listeners, 'icon-shopping_cart-16');
				    }                  
		  
		        } 
	          
	     ] //buttons
				*/
        }
		
 				
	
     ]
        
 }



<?php 
	exit;
	}
	
	if ($_REQUEST['fn'] == 'get_data_tab'){
	    
	    $where = "";
	    if(strlen($m_params->tacor2)>0)
	        $where .= " AND TACOR2 = '{$m_params->tacor2}'";
	        
	        
	            
        $sql = "SELECT *
                FROM {$cfg_mod_Admin['file_tab_sys']}
                WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_params->tabella}' 
                AND TATP <> 'S' AND TALINV='' {$where} 
                ORDER BY TANR, TADESC";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        $ret = array();
        while ($row = db2_fetch_assoc($stmt)) {
            
            $text = "[" . trim($row['TANR']) . "] " . trim($row['TADESC']);
            
            $ret[] = array( "id" 	=> trim($row['TANR']),
                "text" 	=> acs_u8e($text) );
        }
        
        
        
        
        echo acs_je($ret);
        exit;
	}
