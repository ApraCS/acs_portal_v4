<?php

//$f = field name
//$fd= field description
//$l = label
//$p = parametri
//$np = parametri numero
//$cp = parametri combo (tab di decodifica, ....)
//dp = decimal precision
function _diz_v0($f) {
    
    $ar_e=array();
    $ar_e['ARART'] =array('label' => 'Articolo', 'type' => 'DF');
    $ar_e['ARDART']=array('label' => 'Descrizione', 'type' => 'DF');
    // *** Classificazioni *** //
    $ar_e['ARTPAR']=array('label' => 'Tipo parte', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MTPA'));
    $ar_e['ARCLME']=array('label' => 'Classe merceologica', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUCM'));
    $ar_e['ARGRME']=array('label' => 'Gruppo merceologico', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUGM'));
    $ar_e['ARSGME']=array('label' => 'S/Gruppo merceologico', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUSM'));
    $ar_e['ARSTA1']=array('label' => 'Selezione statistica 1', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUS1'));
    $ar_e['ARST21']=array('label' => 'Selezione statistica 2a', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUS2'));
    $ar_e['ARST22']=array('label' => 'Selezione statistica 2b', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUS3'));
    $ar_e['ARST23']=array('label' => 'Selezione statistica 2c', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUS4'));
    $ar_e['ARPRE2']=array('label' => 'ABC assoluto', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BABC'));
    $ar_e['ARPRE3']=array('label' => 'ABC relativo', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BABC'));
    $ar_e['ARCDIV']=array('label' => 'Codice iva', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'CUAE'));
    $ar_e['ARCLFI']=array('label' => 'Classe fiscale', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUCF'));
    $ar_e['ARGRFI']=array('label' => 'Gruppo fiscale', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUGF'));
    $ar_e['ARCENC']=array('label' => 'Intra vendite', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'CENC'));
    $ar_e['ARCENA']=array('su_RI' => 'Y', 'f_RI' => 'RACENA', 'f_RI_A' => 'RAAZ10',
        'label' => 'Intra acquisti', 'type' => 'cb',
        'cp' => array('td' => 'sys', 'taid' => 'CENC'));
    $ar_e['ARTBVE']=array('label' => 'Categoria vendite', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUTV'));
    $ar_e['ARTBAC']=array('label' => 'Categoria acquisti', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUTA'));
    $ar_e['ARANAL']=array('label' => 'Conto analitico', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUAN'));
    $ar_e['ARROYA']=array('label' => 'Royalties', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'CUAG'));
    
    // *** Specifiche *** //
    $ar_e['ARARFO']=array('label' => 'Articolo di riferimento', 'ml' => 15, 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARGRAL']=array('label' => 'Articolo alternativo', 'ml' => 15, 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARRIF'] =array('label' => 'Riferimento tecnico', 'ml' => 15, 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARDIM1']=array('label' => 'Larghezza', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARDIM2']=array('label' => 'Altezza', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARDIM3']=array('label' => 'Profondit&agrave;', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARDIM4']=array('label' => 'Quota/Posizione 1', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARDIM5']=array('label' => 'Quota/Posizione 2', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARPNET']=array('label' => 'Peso netto', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARPLOR']=array('label' => 'Peso lordo', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARPSPE']=array('su_RI' => 'Y', 'f_RI' => 'RAPSPE', 'f_RI_A' => 'RAAZ17',
        'label' => 'Peso specifico', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARUMTE']= array('label' => '', 'type' => 'hd');
    $ar_e['ARUMAL']= array('label' => '', 'type' => 'hd');
    $ar_e['ARUMCO']= array('label' => '', 'type' => 'hd');
    $ar_e['U_M'] = array('label'  => 'Unit&agrave; di misura', 'lwidth' => 140, 'type' => 'tf');
    $ar_e['U_M_cb']=array('label' => 'Unit&agrave; di misura', 'type' => 'cb', 'lwidth' => 140, 'cp' => array('td' => 'sys', 'taid' => 'MUUM'));
    $ar_e['ARVOLU']=array('label' => 'Volume', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARCOLL']=array('label' => 'Colli unitari', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARFLG1']=array('label' => 'Collo solo su ordine', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR017'));
    $ar_e['ARRCOL']=array('label' => 'Gruppo collo', 'ml' => 3, 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'PUCI'), 'width'=> 315);
    $ar_e['ARVOLK']=array('label' => 'Volume kit', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARCOLK']=array('label' => 'Colli unitari kit', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARFLG2']=array('label' => 'Collo kit solo su ordine', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR017'));
    $ar_e['ARRCOK']=array('label' => 'Gruppo collo kit', 'ml' => 3, 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'PUCI'), 'width'=> 315);
    
    // *** Movimentazione *** //
    $ar_e['ARESAU']=array('label' => 'Ciclo di vita/In esaurimento', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR012'), 'width' =>317);
    
    $ar_e['data_val_ini'] = array('dt_composta' =>'Y', 'anno' => 'ARAAIG', 'mese' => 'ARMMIG', 'giorno' => 'ARGGIG',
        'label' => 'Validit&agrave; iniziale', 'type' => 'df', 'width' =>317);
    $ar_e['data_val_fin'] = array('dt_composta' =>'Y', 'anno' => 'ARAAFG', 'mese' => 'ARMMFG', 'giorno' => 'ARGGFG',
        'label' => 'Validit&agrave; finale', 'type' => 'df', 'width' =>317);
    $ar_e['ARAAIG']=array('label' => 'Anno codifica Validit&agrave; iniziale', 'type' => 'nf', 'np' => array('lm' => 2, 'dp' =>2), 'lwidth' => 140);
    $ar_e['ARMMIG']=array('label' => 'Mese codifica Validit&agrave; iniziale', 'type' => 'nf', 'np' => array('lm' => 2, 'dp' =>2), 'lwidth' => 140);
    $ar_e['ARGGIG']=array('label' => 'Giorno codifica Validit&agrave; iniziale', 'type' => 'nf', 'np' => array('lm' => 2, 'dp' =>2), 'lwidth' => 140);
    
    $ar_e['ARAAFG']=array('label' => 'Anno fine validit&agrave;', 'type' => 'nf', 'np' => array('dp' => 0), 'lwidth' => 140);
    $ar_e['ARMMFG']=array('label' => 'Mese fine validit&agrave;', 'type' => 'nf', 'np' => array('dp' => 0), 'lwidth' => 140);
    $ar_e['ARGGFG']=array('label' => 'Giorno fine validit&agrave;', 'type' => 'nf', 'np' => array('dp' => 0), 'lwidth' => 140);
    $ar_e['ARLTLO']=array('su_RI' => 'Y', 'f_RI' => 'RALTLO', 'f_RI_A' => 'RAAZ14',
        'label' => 'Lead time logistica', 'type' => 'nf', 'np' => array('dp' => 0), 'lwidth' => 140);
    $ar_e['ARLTPR']=array('label' => 'Lead time produzione', 'type' => 'nf', 'np' => array('dp' => 0), 'lwidth' => 140);
    $ar_e['ARFLR1']=array('label' => 'Obsoleto', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR002'), 'width' =>317);
    $ar_e['ARFLR2']=array('label' => 'Lenta movimentazione', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR001'));
    $ar_e['ARFLR7']=array(
        'su_RI' => 'Y', 'f_RI' => 'RAFLR7',  'f_RI_A' => 'RAAZ08',
        'label' => 'Trasferim. in logistica', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'ARYN'), 'width' =>317);
    $ar_e['ARFLR6']=array(
        'su_RI' => 'Y', 'f_RI' => 'RAFLR6',  'f_RI_A' => 'RAAZ07',
        'label' => 'Giac.da logistica', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR003'));
    $ar_e['ARFLR8']=array(
        'su_RI' => 'Y', 'f_RI' => 'RAFLR8',  'f_RI_A' => 'RAAZ09',
        'label' => 'Movim. da logistica', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR004'), 'width' =>317);
    $ar_e['ARMAUT']=array(
        'su_RI' => 'Y', 'f_RI' => 'RAMAUT',  'f_RI_A' => 'RAAZ04',
        'label' => 'Mag. automat.', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'ARYN'), 'width' =>317);
    $ar_e['ARFLR4']=array(
            'su_RI' => 'Y', 'f_RI' => 'RAFLR4',  'f_RI_A' => 'RAAZ05',
            'label' => 'Consistenza', 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARFLG3']=array('label' => 'Movim.ubicazione', 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARSWES']=array('label' => 'Ind.esplos.movim.',  'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR005'));
    $ar_e['ARSWE1']=array('label' => 'Ind.esplos.fabbis.', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR005'));
    $ar_e['ARSWE2']=array('label' => 'Ind.esplos.costi', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR005'));
    
    // *** Configurazione *** //
    $ar_e['ARMODE']=array('label' => 'Modello configurazione', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'PUMO'));
    $ar_e['ARMOCO']=array('label' => 'Modello commerciale', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'PUMC'));
    $ar_e['ARVAR1']=array('label' => '1a Variabile', 'type' => 'cb', 'width' => 450,
        'cp' => array('td' => 'sys', 'taid' => 'PUVR'));
    $ar_e['ARGVA1']=array('label' => 'Obblig.', 'type' => 'cb', 'width' => 180, 'lwidth' => 50,
        'cp' => array('td' => 'std', 'tataid' => 'AR006'));
    $ar_e['ARVAR2']=array('label' => '2a Variabile', 'type' => 'cb', 'width' => 450,
        'cp' => array('td' => 'sys', 'taid' => 'PUVR'));
    $ar_e['ARGVA2']=array('label' => 'Obblig', 'type' => 'cb', 'width' => 180, 'lwidth' => 50,
        'cp' => array('td' => 'std', 'tataid' => 'AR006'));
    $ar_e['ARVAR3']=array('label' => '3a Variabile', 'type' => 'cb', 'width' => 450,
        'cp' => array('td' => 'sys', 'taid' => 'PUVR'));
    $ar_e['ARGVA3']=array('label' => 'Obblig', 'type' => 'cb', 'width' => 180, 'lwidth' => 50,
        'cp' => array('td' => 'std', 'tataid' => 'AR006'));
    $ar_e['ARVDI1']=array('label' => 'Variabile larghezza', 'type' => 'cb', 'width' => 450,
        'cp' => array('td' => 'sys', 'taid' => 'PUVR'));
    $ar_e['ARGDI1']=array('label' => 'Obblig', 'type' => 'cb', 'width' => 180, 'lwidth' => 50,
        'cp' => array('td' => 'std', 'tataid' => 'AR007'));
    $ar_e['ARVDI2']=array('label' => 'Variabile lunghezza', 'type' => 'cb', 'width' => 450,
        'cp' => array('td' => 'sys', 'taid' => 'PUVR'));
    $ar_e['ARGDI2']=array('label' => 'Obblig', 'type' => 'cb', 'width' => 180, 'lwidth' => 50,
        'cp' => array('td' => 'std', 'tataid' => 'AR007'));
    $ar_e['ARVDI3']=array('label' => 'Variabile spessore', 'type' => 'cb', 'width' => 450,
        'cp' => array('td' => 'sys', 'taid' => 'PUVR'));
    $ar_e['ARGDI3']=array('label' => 'Obblig', 'type' => 'cb', 'width' => 180, 'lwidth' => 50,
        'cp' => array('td' => 'std', 'tataid' => 'AR007'));
    $ar_e['ARCLIP']=array('su_RI' => 'Y', 'f_RI' => 'RACLIP', 'f_RI_A' => 'RAAZ16',
        'label' => 'Listino punti', 'type' => 'cb', 'width' => 450,
        'cp' => array('td' => 'sys', 'taid' => 'CLIP'));
    $ar_e['ARTBRI']=array('label' => 'Ind.ricarico', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MURI'));
    $ar_e['ARCATS']=array('label' => 'Categoria sconti', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUCS'));
    $ar_e['ARCATP']=array('label' => 'Categoria provvigioni', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUCP'));
    
    // *** Approvvigionamento *** //
    $ar_e['ARFOR1'] = array('label'  => 'Fornitore'
                           , 'cf'    => 'F'
                           , 'type'  => 'anag_cf'
                           , 'width' => 500
                           );
    $ar_e['ARLTFO']=array('label' => 'Lead time fornitore', 'type' => 'nf', 'lwidth' => 140, 
        'np' => array('dp' => 0));
    $ar_e['ARBSCA']=array('su_RI' => 'Y', 'f_RI' => 'RABSCA',  'f_RI_A' => 'RAAZ18', 
        'label' => 'Baia di scarico', 'type' => 'tf');
    $ar_e['ARCKIN']=array('su_RI' => 'Y', 'f_RI' => 'RACKIN', 'f_RI_A' => 'RAAZ11',  'lwidth' => 100,
        'label' => 'Check entrata', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'ARY'));
    $ar_e['ARCKOU']=array('su_RI' => 'Y', 'f_RI' => 'RACKOU', 'f_RI_A' => 'RAAZ12', 'lwidth' => 100,
        'label' => 'Check consegna', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'ARY'));
    $ar_e['ARDEPO']=array('label' => 'Deposito', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'MUFD'));
    $ar_e['ARFLR3']=array('label' => 'Tipo spunta', 'type' => 'cb',
        'cp' => array('td' => 'sys', 'taid' => 'BTSP'));
    /*$ar_e['ARCEUU']=array('su_RI' => 'Y', 'f_RI' => 'RACEEU', 'f_RI_A' => 'RAAZ19',
        'label' => 'Certificazione', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BCEU'));*/
    $ar_e['AXTAB1']=array('label' => 'Materiale principale', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BCMP'));
    $ar_e['ARISO ']=array('label' => 'Categoria iso', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BQAR'));
    $ar_e['AXNAZO']=array('label' => 'Nazione origine', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BNAZ'));
    $ar_e['ARTAB2']=array('label' => 'Contributo RAEE', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BRAE'));
//    $ar_e['ARFOR2']=array('label' => 'Fornitore alternativo', 'type' => 'tf');
    $ar_e['ARFOR2'] = array('label'  => 'Fornitore alternativo'
                           , 'cf'    => 'F'
                           , 'type'  => 'anag_cf'
                           , 'width' => 500
                           );
    
//    $ar_e['ARTER ']=array('label' => 'Terzista', 'type' => 'tf');
    $ar_e['ARTER'] = array( 'label'  => 'Terzista'
                          , 'cf'    => 'F'
                          , 'type'  => 'anag_cf'
                          , 'width' => 500
                          );
    
    $ar_e['ARTAB4']=array(
            'su_RI' => 'Y', 'f_RI' => 'RATAB4',  'f_RI_A' => 'RAAZ02',
            'label' => 'Allegati grafici', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BALL'));
    $ar_e['ARTAB5']=array(
            'su_RI' => 'Y', 'f_RI' => 'RATAB5',  'f_RI_A' => 'RAAZ03',
            'label' => 'Var.analisi disponibilitÓ', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'VMAG'));
    $ar_e['ARPZCO']=array('label' => 'Pezzi per confezione', 'type' => 'nf', 'lwidth' => 140,
        'np' => array('dp' => 4));
    
    $ar_e['space'] = array('label' => '&nbsp;', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 140, 'lsep' => '');
    $ar_e['lun'] = array('label' => 'Lunedý', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 70);
    $ar_e['mar'] = array('label' => 'Martedý', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 70);
    $ar_e['mer'] = array('label' => 'Mercoledý', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 70);
    $ar_e['gio'] = array('label' => 'Giovedý', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 70);
    $ar_e['ven'] = array('label' => 'Venerdý', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 70);
    $ar_e['sab'] = array('label' => 'Sabato', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 70);
   
    
    
    $ar_e['gg1'] = array('label' => 'GG invio ordini acquisto', 'lalign' => 'end', 'type' => 'DF', 'lwidth' => 130);
    $ar_e['AXGGI1']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGI2']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGI3']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGI4']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGI5']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGI6']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    
    $ar_e['gg2'] = array('label' => 'GG ricezione fornitore', 'type' => 'DF', 'lwidth' => 130);
    
    $ar_e['AXGGR1']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGR2']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGR3']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGR4']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGR5']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGR6']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    
    $ar_e['gg3'] = array('label' => 'GG consegna fornitore', 'type' => 'DF', 'lwidth' => 130);
    $ar_e['AXGGC1']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGC2']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGC3']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGC4']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGC5']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGGC6']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    
    $ar_e['gg4'] = array('label' => 'GG consegna ritiri', 'type' => 'DF', 'lwidth' => 130);
    $ar_e['AXGCR1']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGCR2']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGCR3']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGCR4']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGCR5']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    $ar_e['AXGCR6']=array('label' => '&nbsp;', 'type' => 'cb', 'lwidth' => 5, 'margin'=>'0 2 0 0', 'lsep' => '', 'cp' => array('td' => 'std', 'tataid' => 'ARTGG'));
    
    
    // *** Programmazione *** //
    $ar_e['ARTPAP']=array('label' => 'Tipo approvv. principale', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR008'));
    $ar_e['AXTAPO']=array('label' => 'Tipo approvv. potenziale', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR008'));
    $ar_e['AXTADP']=array('label' => 'Tipo approvv. dim. personalizz.', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR010'));
    $ar_e['ARTAB1']=array('label' => 'Gr.di pianificazione', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'GRPA'), 'width' => 315);
    $ar_e['ARBGRM']=array('su_RI' => 'Y', 'f_RI' => 'RABGRM', 'f_RI_A' => 'RAAZ15',
        'label' => 'Elaborazione Background MRP', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR009'));
    $ar_e['ARSWTR']=array('label' => 'Tipo riordino (O/S/D..)', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'AR011'));
    $ar_e['ARFLR5']=array('label' => 'Mto per lotto', 'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'ARYN'));
    $ar_e['ARTAB3']=array(
            'su_RI' => 'Y', 'f_RI' => 'RATAB3',  'f_RI_A' => 'RAAZ01',
            'label' => 'Condizioni Mto', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'BMTO'), 'width' => 315);
    $ar_e['AREOAQ']=array('su_RI' => 'Y', 'f_RI' => 'RAEOAQ',  'f_RI_A' => 'RAAZ20',
        'label' => 'Emissione automatica ordine Mto',  'type' => 'cb',
        'cp' => array('td' => 'std', 'tataid' => 'ARYN'));
    $ar_e['ARLOTT']=array('label' => 'Lotto di riordino', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARSMIN']=array('label' => 'Scorta minima', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARSMAX']=array('label' => 'Scorta massima', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARRIOR']=array('label' => 'Punto riordino/Pick', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['ARPRE1']=array('label' => 'Tipo prelievo', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'PUPR'), 'width' => 315);
    $ar_e['ARDBFB']=array('label' => 'D.B.', 'ml' => 15, 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARDBCS']=array('label' => 'D.B. alternativa 1', 'ml' => 15, 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARDBPL']=array('label' => 'D.B. alternativa 2', 'ml' => 15, 'type' => 'tf', 'lwidth' => 140);
    $ar_e['ARDBCO']=array('label' => 'Distinta lavorazione', 'type' => 'tf', 'lwidth' => 140);
    $ar_e['AROPE1']=array('label' => 'Operazione 1', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'PUOP'), 'width' => 315);
    $ar_e['AROPE2']=array('label' => 'Operazione 2', 'type' => 'cb', 'cp' => array('td' => 'sys', 'taid' => 'PUOP'), 'width' => 315);
    
    $ar_e['AXLMIR']=array('label' => 'Lotto min.ritiri', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    $ar_e['AXLMUR']=array('label' => 'Lotto mult.ritiri', 'type' => 'nf', 'np' => array('dp' => 4), 'lwidth' => 140);
    
    if (strlen(trim($ar_e[$f]['label'])) == 0)
        $ar_e[$f]['label'] = $f;
    if ($ar_e[$f]['label'] == 'null')
        $ar_e[$f]['label'] = '';
            
    if (strlen(trim($ar_e[$f]['type'])) == 0)
        $ar_e[$f]['type'] = 'DF';
                
    if (isset($ar_e[$f]['ml']))
        $ar_e[$f]['maxLength'] = trim($ar_e[$f]['type']);
    
                      
    $ar_e[$f]['name'] = $f;
    return $ar_e[$f];
                    
       
}

/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _get_data_articolo($cod_art){
    global $conn, $id_ditta_default, $cfg_mod_DeskArt, $cfg_mod_DeskUtility;
    $sql = "SELECT *
            FROM {$cfg_mod_DeskArt['file_anag_art_sst']} AR
            WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$cod_art}' ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    $row = array_map('rtrim', $row);
    
    $row['data_val_ini'] = _componi_data('data_val_ini', $row);
    $row['data_val_ini_val'] = _componi_data('data_val_ini', $row, "%Y%m%d");
    $row['data_val_fin'] = _componi_data('data_val_fin', $row);
    $row['data_val_fin_val'] = _componi_data('data_val_fin', $row, "%Y%m%d");
    $row['ARFOR1_DE'] = _deco_ana_cf('ARFOR1', $row);
    $row['ARFOR2_DE'] = _deco_ana_cf('ARFOR2', $row);
    $row['ARTER_DE'] = _deco_ana_cf('ARTER', $row);
    $row['ARUMCO'] = trim($row['ARUMCO']);
    $row['ARUMTE'] = trim($row['ARUMTE']);
    $row['ARUMAL'] = trim($row['ARUMAL']);
    $row['ARDART'] = htmlentities($row['ARDART']);
    
    $sql_ax = "SELECT *
            FROM {$cfg_mod_DeskUtility['file_est_anag_art']} AX
            WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$cod_art}'";
    
    $stmt_ax = db2_prepare($conn, $sql_ax);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_ax);
    $row_ax = db2_fetch_assoc($stmt_ax);
    
    $row['AXTAPO'] = trim($row_ax['AXTAPO']);
    $row['AXTADP'] = trim($row_ax['AXTADP']);
    $row['AXNAZO'] = trim($row_ax['AXNAZO']);
    $row['AXTAB1'] = trim($row_ax['AXTAB1']);
    $row['AXLMIR'] = trim($row_ax['AXLMIR']);
    $row['AXLMUR'] = trim($row_ax['AXLMUR']);
    
    for($i=1; $i <= 6; $i++){
        $row["AXGGI{$i}"] = trim($row_ax["AXGGI{$i}"]);
        $row["AXGGC{$i}"] = trim($row_ax["AXGGC{$i}"]);
        $row["AXGGR{$i}"] = trim($row_ax["AXGGR{$i}"]);
        $row["AXGCR{$i}"] = trim($row_ax["AXGCR{$i}"]);
    }
   
    
  /*  if(trim($row['ARUMCO']) == trim($row['ARUMTE']) && trim($row['ARUMCO']) == trim($row['ARUMAL']))
        $row['um'] = trim($row['ARUMTE']);
    else
        $row['um'] = trim($row['ARUMTE']) . ", " . trim($row['ARUMCO']) . ", " . trim($row['ARUMAL']);
    */
    return $row;
}

/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _componi_data($f, $row, $to_format = "%d/%m/%Y"){
    $fd = _diz_v0($f);
    $data = print_date(sprintf("%04s", $row[$fd['anno']]) . sprintf("%02s", $row[$fd['mese']]) . sprintf("%02s", $row[$fd['giorno']]), $to_format);
    return $data;
}

/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _deco_ana_cf($f, $row){
    global $conn, $cfg_mod_DeskArt, $id_ditta_default;
    $fd = _diz_v0($f);
    $sql = "Select trim(CFRGS1) as CFRGS1 from {$cfg_mod_DeskArt['file_anag_cli']}
	   where CFDT = '{$id_ditta_default}' and CFTICF = '{$fd['cf']}' and
       CFCD = {$row[$f]}";

    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $r = db2_fetch_assoc($stmt);
    
    //print_r($r['CFRGS1']); exit;
    return "[{$row[$f]}] {$r['CFRGS1']}";
}
    
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _outfield($f, $multi = 'N'){
    $fd = _diz_v0($f);
   
    if (strlen(trim($fd['type'])) == 0)
        $fd['type'] = 'tf';
        //print_r($fd['type']); exit;
        switch(trim($fd['type'])){
            case 'DF': $ret = _displayf($f);
            break;
            case 'tf': $ret = _textf($f, $multi);
            break;
            case 'nf': $ret = _numberf($f, $multi);
            break;
            case 'cb': $ret = _combo($f);
            break;
            case 'df': $ret = _datef($f);
            break;
            case 'hd': $ret = _hiddenf($f);
            break;
            case 'anag_cf':                 
                $ret = _displayf($f);
                $ret .= ', ' . _bottone_ric_ana($f);
                $ret .= ', ' . _bottone_reset_ana($f);
                $ret .= ', ' . _hiddenf($f);
            break;
            
        }
        return $ret;
}
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _displayf($f){
    $fd = _diz_v0($f);

    $ret = array( 'xtype' => 'displayfield'
                , 'name'  => $f
                , 'margin' => '0 10 5 0'
                , 'itemId' => $f
                );
    if ($fd['type'] == 'anag_cf')
        $ret['name'] =$f .'_DE';
    if (isset($fd['label'])) $ret['fieldLabel'] = $fd['label'];
    if (isset($fd['width'])) $ret['width'] = $fd['width'];
    if (isset($fd['hidden'])) $ret['hidden']  =$fd['hidden'];
    if (isset($fd['lwidth'])) $ret['labelWidth'] = $fd['lwidth'];
    if (isset($fd['lalign'])) $ret['labelAlign'] = $fd['lalign'];
    if (isset($fd['lsep'])) $ret['labelSeparator'] = $fd['lsep'];
    return acs_je($ret);
}
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _hiddenf($f){
    $fd = _diz_v0($f);
    $ret = array( 'xtype'  => 'hiddenfield'
                , 'name'   => $f

                );
    return acs_je($ret);
}

/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _textf($f, $multi){
    $fd = _diz_v0($f);
   
   $ret = "{ xtype: 'fieldcontainer',
			flex:1,
			layout: { type: 'hbox', pack: 'start', align: 'stretch'},						
			items: [{ xtype : 'textfield',
            name : '{$f}',
            
            itemId : 'text_{$f}',
            fieldLabel : '{$fd['label']}', 
            //labelWidth : 140, 
            ";
   if (isset($fd['width'])) $ret.= "width : {$fd['width']}, ";
   else  $ret.= "flex : 1,";
   if (isset($fd['lwidth']))$ret .= "labelWidth : {$fd['lwidth']}, "; 
   if (isset($fd['ml'])) $ret .= "maxLength : {$fd['ml']}";

    $ret .= "} ";
    
    if($multi == 'Y'){
    $ret .= ",{
               xtype: 'checkbox'
              , name: 'clear_{$f}'
              , boxLabel: ''
              , margin : '0 0 0 5'
              , inputValue: 'Y'
              , listeners : {
                change: function(comp, check) {
                    var text = this.up('window').down('#text_{$f}');
                    if(check == true){
                        text.setValue('**azzera**');
                        text.setReadOnly(true);
                    }else{
                        text.setValue('');
                        text.setReadOnly(false);
                    }
                    
                }
            }}";
    }
    $ret .= "]}";
    
    return $ret;
}
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _numberf($f, $multi){
    $fd = _diz_v0($f);
  
    
    
    /*$ret = array('xtype' => 'numberfield'
        , 'name' => $f
        , 'hideTrigger' => true
        , 'keyNavEnabled' => false
        , 'mouseWheelEnabled' => false
        , 'fieldLabel' => $fd['label']
        , 'decimalPrecision' => $fd['np']['dp']
        , 'forcePrecision' => true
    );*/
    
    $ret = "{ xtype: 'fieldcontainer',
            flex:1,
            layout: { type: 'hbox', pack: 'start', align: 'stretch'},
            items: [{ xtype : 'numberfield',
            name : '{$f}',
            itemId : 'numb_{$f}',
            fieldLabel : '{$fd['label']}',
            hideTrigger : true,
            keyNavEnabled : false,
            mouseWheelEnabled : false,
            decimalPrecision : {$fd['np']['dp']},
            forcePrecision : true,
            ";
            if (isset($fd['width'])) $ret.= "width : {$fd['width']}, ";
            else  $ret.= "flex : 1,";
                
            if (isset($fd['lwidth']))$ret .= "labelWidth : {$fd['lwidth']}, ";
            if (isset($fd['ml'])) $ret .= "maxLength : {$fd['ml']}";
    
            $ret .= "} ";

        if($multi == 'Y'){
          $ret .= ", {
            xtype: 'checkbox'
            , name: 'clear_{$f}'
            , boxLabel: ''
            , margin : '0 0 0 5'
            , inputValue: 'Y'
            , listeners : {
            change: function(comp, check) {
                var text = this.up('window').down('#numb_{$f}');
                if(check == true){
                    text.setValue(0);
                    text.setReadOnly(true);
                }else{
                    text.setValue('');
                    text.setReadOnly(false);
                }
               }
                 }}";
        } 
          $ret .= "]}";
    
    return $ret;
    
    
    
}
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _combo($f){
    $fd = _diz_v0($f); //Recupero le specifiche dal campo dal "dizionario";
    $width = '';
    if ($fd['width'] > 0)
        $width = ", width : ".$fd['width'];
    else 
        $width = ", flex : 1";
    if (isset($fd['margin']))
        $margin = ", margin: '".$fd['margin']."'";
    if ($fd['lwidth'] > 0)
        $lwidth = ", labelWidth : ".$fd['lwidth'];
    if (isset($fd['lalign']))
        $lalign = ", labelAlign : '".$fd['lalign']."'";
    if (isset($fd['lsep']))
        $lsep = ", labelSeparator : '".$fd['lsep']."'";
        $ret = " { xtype: 'combo', name: " . j($f) . ", fieldLabel: " .  j($fd['label']) . "
                 , forceSelection: true
                 , displayField: 'text'
	             , valueField: 'id'
                 , queryMode: 'local'
	             , minChars: 1
	             , emptyText: '- seleziona -'
                 " . $width . $lwidth . $margin. $lalign. $lsep."
	             , store: { editable: false, autoDestroy: true, fields: ['id', 'text']
	                      , data: [" . _get_combo_data($fd['cp']) . "]}
	             , listeners: { beforequery: function (record) {
			         record.query = new RegExp(record.query, 'i');
			         record.forceAll = true;
		             }}
                 }";
        
        return $ret;
}
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _datef($f){
    $fd = _diz_v0($f);
    $width = ", width : ".$fd['width'];
    $ret = " { xtype: 'datefield'
             , name: '".$f."'
             , flex : 1
             , startDay: 1 //lun.
             , fieldLabel: '" . $fd['label'] ."'
             , format: 'd/m/Y'
             , submitFormat: 'Ymd'
             , allowBlank: true
             " . $width . "
	         , listeners: { invalid: function (field, msg) { Ext.Msg.alert('', msg);}}
             }";
    //print_r($ret); exit;
    return $ret;
}

/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _get_combo_data($cp){
    global $desk_art;
    if ($cp['td'] == 'sys')            
      return acs_ar_to_select_json(find_TA_sys($cp['taid'], null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R', 'N', 'Y');   
    else
      return acs_ar_to_select_json($desk_art->find_TA_std($cp['tataid'], null, 'N', 'N', null, null, null, 'N', 'Y'), '', 'R', 'N', 'Y');
}

/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _bottone_ric_ana($f){
    $fd = _diz_v0($f);
    $ret = " { xtype: 'button'
             , margin: '0 0 0 5'
             , scale: 'small'
             , iconCls: 'icon-search-16'
             , iconAlign: 'top'
             , width: 25
             , handler : function() {
                   var m_form = this.up('form').getForm();
                   var my_listeners = 
                     { afterSel: function(from_win, row) {
                         m_form.findField('{$f}').setValue(row.CFCD);
                         m_form.findField('{$f}_DE').setValue('['+row.CFCD+']'+' '+row.CFRGS1);
                         from_win.close();
                       }
                     }
                   acs_show_win_std('Anagrafica clienti/fornitori'
                                   , 'acs_anag_ricerca_cli_for.php?fn=open_tab'
                                   , {cf:'{$fd['cf']}'}, 600, 500, my_listeners
                                   , 'icon-search-16');
                 }
                        
            }";
    return $ret;
}

function _bottone_reset_ana($f){
    $fd = _diz_v0($f);
    $ret = " { xtype: 'button'
             , margin: '0 0 0 5'
             , scale: 'small'
             , iconCls: 'icon-sub_red_delete-16'
             , iconAlign: 'top'
             , width: 25
             , handler : function() {
                   var m_form = this.up('form').getForm();
                   m_form.findField('{$f}').setValue(0);
                   m_form.findField('{$f}_DE').setValue('');
                   
                 }
                 
            }";
    return $ret;
}



/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
function _intestazione() {
    $ar_art = array();
    $f = 'ARART';
    $ar_art = _diz_v0($f);
    $ar_art['label'] = '';
    $ar_art['width'] = '140';
    $ar_art = acs_je($ar_art);
    
    $ar_dart = array();
    $f = 'ARDART';
    $ar_dart = _diz_v0($f);
    $ar_dart['label'] = '';
    $ar_dart = acs_je($ar_dart);
    
    
    $ret = "{ xtype: 'fieldset'
             ,title: 'Articolo'
             ,style: 'padding: 0px 0px 5px 5px'
             ,bodyStyle: 'padding: 0px 0px 5px 5px'
             ,layout: 'hbox'
             ,defaults: {
        	   xtype: 'displayfield'
        	  ,fieldStyle: 'font-weight: bold;'
        	  }
	         ,flex:1
             ,items: [" . $ar_art  . ",
                      " . $ar_dart . "
              ]
            }";
    return $ret;
}
//--------------------------------------------------------------------------------------------------------------
// OLD
//--------------------------------------------------------------------------------------------------------------
function _number_OLD($f, $l, $p, $np){
    if (isset($np['decimal_precision']))
        $decimal_precision = $np['decimal_precision'];
        else
            $decimal_precision = 0;
            $ret = " {
                xtype: 'numberfield',
                name: " . j($f) . ",
                fieldLabel: " .  j($l) . ",
                hideTrigger  : true,
                decimalPrecision: " . j($decimal_precision) . ",
        }";
            return $ret;}
            
