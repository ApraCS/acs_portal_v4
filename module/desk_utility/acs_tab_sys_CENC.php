<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'CENC',
    'title_grid' => 'CENC-Nomenclatura cee',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'nom'      => array('label'	=> 'Nomenclatura', 'short' =>'Nomencl.', 'maxLength' => 8, 'c_width' => 60),
        'ncor'     => array('label'	=> 'Nomenclatura di raggruppamento', 'short' =>'Nom.raggr.', 'maxLength' => 8, 'c_width' => 70),
        'u_sup'    => array('label'	=> 'Unit� suppl.', 'short' =>'US', 'xtype' => 'combo_tipo', 'c_width' => 30, 'tab_std' => 'ARY'),
        'serv'     => array('label'	=> 'Servizio',  'xtype' => 'combo_tipo', 'c_width' => 60, 'tab_std' => 'TSERV'),
        'ums'      => array('label'	=> 'Unit� misura supl.', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'MUUM'),
        'qta1'     => array('label'	=> 'Calcolo quantit&agrave;', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'width' => 215, 'labelWidth' => 120, 'maxLength' => 1, 'type' =>'combo', 'tab_std' => 'TCQTA'),
        'qta2'     => array('label'	=> '&nbsp;', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 100, 'labelWidth' => 5, 'maxLength' => 1, 'type' =>'combo', 'labelSeparator' => '', 'tab_std' => 'TCQTA'),
        'qta3'     => array('label'	=> '&nbsp;', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'width' => 100, 'labelWidth' => 5, 'maxLength' => 1, 'type' =>'combo', 'close' => 'Y', 'labelSeparator' => '','tab_std' => 'TCQTA'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'     => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'   => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'   => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli nomenclatura cee',
    'tab_tipo' => 'PUSQ',
    'TAREST' => array(
        'nom' 	=> array(
            "start" => 0,
            "len"   => 8,
            "riempi_con" => ""
        ), 
        'u_sup'	=> array(
            "start" => 8,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'serv'	=> array(
            "start" => 9,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'ncor'	=> array(
            "start" => 10,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'ums'	=> array(
            "start" => 18,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'qta1'	=> array(
            "start" => 21,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'qta2'	=> array(
            "start" => 22,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'qta3'	=> array(
            "start" => 23,
            "len"   => 1,
            "riempi_con" => ""
        )
    )
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

