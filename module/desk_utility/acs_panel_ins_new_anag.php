<?php

require_once("../../config.inc.php");

ini_set('max_execution_time', 30000);

$main_module = new DeskUtility();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_UTIL'));



function get_gcrow_by_prog($id_prog){
	global $conn, $cfg_mod_Gest, $id_ditta_default;
	$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC WHERE GCDT='$id_ditta_default' AND GCPROG = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_prog));
	$row = db2_fetch_assoc($stmt);
	return $row;
}


function ar_standard_alternativa(){
	$ret = array();
	$ret[] = array('id' => 'S', 'text' => 'Standard');
	$ret[] = array('id' => 'A', 'text' => 'Alternativa');
	return $ret;
}


function ar_giorno_scarico(){
	$ret = array();
	$ret[] = array('id' => 'X', 'text' => 'Si');
	$ret[] = array('id' => 'M', 'text' => 'Mattino');
	$ret[] = array('id' => 'P', 'text' => 'Pomeriggio');
	return $ret;
}

function insert_parametro_in_db($stmt, $ar_ins, $parametro, $contenuto, $code, $seq, $tipo_contenuto = ''){
	$ar_ins['PARAMETRO'] = acs_u8e($parametro);
	$ar_ins['CONTENUTO'] = acs_u8e($contenuto);
	$ar_ins['CODICE_CONTENUTO'] = acs_u8e($code);
	$ar_ins['TIPO_CONTENUTO'] = $tipo_contenuto;
	$ar_ins['ICSEQU'] 	 = $seq;
	db2_execute($stmt, $ar_ins); echo db2_stmt_errormsg($stmt);
}



function write_parametri_cerved_base($resp_json){
	global $auth, $conn, $cfg_mod_Gest;

	$ci = $resp_json->companyInfo[0];

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'BASE';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'companyName', $ci->companyName, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'street', $ci->address[0]->street, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'postCode', $ci->address[0]->postCode, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'province', $ci->address[0]->province, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'municipality', $ci->address[0]->municipality, $ci->address[0]->municipalityCode, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'activityStatus', $ci->activityStatusDescription, $ci->activityStatus, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'companyType', $ci->companyType, $ci->companyType, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'companyForm ', $ci->companyForm->description, $ci->companyForm->code, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'registrationDate ', strftime('%Y%m%d', strtotime($ci->registrationDate)), '', ++$seq, 'D');
	insert_parametro_in_db($stmt, $ar_ins, 'ateco07  ', $ci->ateco07Description, $ci->ateco07, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'reaCode  ', implode(' ', array($ci->reaCode->coCProvinceCode, $ci->reaCode->reano)), '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'taxCode  ', $ci->taxCode, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'vatRegistrationNo  ', $ci->vatRegistrationNo, '', ++$seq);

	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}




function write_parametri_cerved_details($resp_json){
	global $auth, $conn, $cfg_mod_Gest;

	$ci = $resp_json->companyInfo;

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'DETAILS';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'telephone', 	$ci->telephone, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'email', 		$ci->email, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'certifiedEmail',$ci->certifiedEmail, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'activityStartDate', strftime('%Y%m%d', strtotime($ci->activityStartDate)), '', ++$seq, 'D');
	insert_parametro_in_db($stmt, $ar_ins, 'paidUpEquityCapital', $ci->paidUpEquityCapital, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'sales', $ci->balanceDetails->sales, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'grossProfit', $ci->balanceDetails->grossProfit, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'profitAndLoss', $ci->balanceDetails->profitAndLoss, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'numberOfEmployes', $ci->balanceDetails->numberOfEmployes, '', ++$seq);

	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}





function write_parametri_cerved_scores($resp_json){
	global $auth, $conn, $cfg_mod_Gest;

	$ci = $resp_json;

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'SCORES';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'negativeEventsGrading', 	implode(', ', array(
			$ci->negativeEventsGrading->grading,
			$ci->negativeEventsGrading->descriptiveGradingSynthesisCode,
			$ci->negativeEventsGrading->subScoreClass
	)), $ci->negativeEventsGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'economicalFinancialGrading', 	implode(', ', array(
			$ci->economicalFinancialGrading->grading,
			$ci->economicalFinancialGrading->descriptiveGradingSynthesisCode,
			$ci->economicalFinancialGrading->subScoreClass
	)), $ci->economicalFinancialGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'paylineGrading', 	implode(', ', array(
			$ci->paylineGrading->grading,
			$ci->paylineGrading->descriptiveGradingSynthesisCode,
			$ci->paylineGrading->subScoreClass
	)), $ci->paylineGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'cervedGroupScore', 	implode(', ', array(
			$ci->cervedGroupScore->grading,
			$ci->cervedGroupScore->descriptiveGradingSynthesisCode,
			$ci->cervedGroupScore->icon
	)), $ci->cervedGroupScore->available, ++$seq);


	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}




function get_request_data($key_request){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_cerved']}
	LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']}
	ON PARAMETRO = TAMAIL AND TATAID = 'CRVDS'
	WHERE CODICE_RICHIESTA = ?";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($key_request));

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		$n = array();
		$n['parametro'] = acs_u8e(trim($row['PARAMETRO']));
		$n['des_parametro'] = acs_u8e(trim($row['TADESC']));
		$n['contenuto'] = acs_u8e(trim($row['CONTENUTO']));
		$n['codice_contenuto'] = acs_u8e(trim($row['CODICE_CONTENUTO']));
		$n['tipo_contenuto'] = acs_u8e(trim($row['TIPO_CONTENUTO']));
		$ret[] = $n;
	}

	return $ret;
}





// ******************************************************************************************
// IMPORT ANAGRAFICA DA GEST
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_import_anag'){

	$m_params = acs_m_params_json_decode();

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $id_ditta_default);
	$cl_p .= sprintf("%09d", $m_params->form_values->f_cliente_cod);
	$cl_p .= sprintf("%-1s", '');
	$cl_p .= sprintf("%010d", '');
	$cl_p .= sprintf("%-1s", 'P'); //PUNTO VENDITA

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31O2('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31O2', $libreria_predefinita_EXE, $cl_in, null, null);
	}

	$ret = array();
	$ret['success'] = true;
	$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
	$ret['id_prog'] = (int)substr($ret['call_return'], 259, 10);
	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// IMPORT ANAGRAFICA DA GEST
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_import_anag_by_prog'){
	$m_params = acs_m_params_json_decode();
	$row = get_gcrow_by_prog($m_params->gcprog);

	if ($row['GCPRAB'] > 0){	//richiesta aggiornamento su destinazione
		$rowAbb = get_gcrow_by_prog($row['GCPRAB']);
		$cod_cli = $rowAbb['GCCDCF'];
		$cod_des = $row['GCCDCF'];
	} else {
		$cod_cli = $row['GCCDCF'];
		$cod_des = '';
	}

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $id_ditta_default);
	$cl_p .= sprintf("%09d", $cod_cli); //cliente
	$cl_p .= sprintf("%-1s", '');
	$cl_p .= sprintf("%010d", ''); //OUTPUT: progressivo ritornato che devo aprire
	$cl_p .= sprintf("%-1s", 'P'); //PUNTO VENDITA
	$cl_p .= sprintf("%-9s", $cod_des);  //destinazione

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31O2('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31O2', $libreria_predefinita_EXE, $cl_in, null, null);
	}

	$ret = array();
	$ret['success'] = true;
	$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
	$ret['id_prog'] = (int)substr($ret['call_return'], 259, 10);
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// SCELTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'search_anag_cli'){
	$m_params = acs_m_params_json_decode();
	?>

{"success":true, "items": [
  {
   xtype: 'form',
   bodyStyle: 'padding: 10px',
   bodyPadding: '5 5 0',
   frame: true,   
   items: [
		{
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,			
            margin: "0 20 0 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		echo acs_je('acs_get_select_json.php?select=search_cli_anag');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }   
   ]
   
	, buttons: [{
            text: 'Apri in modifica',
            iconCls: 'icon-save-24', scale: 'medium',            
            handler: function() {
            	t_win = this.up('window');
				Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_import_anag',
				        timeout: 2400000,
				        jsonData: {
				        	form_values: this.up('form').getValues()
				        },
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            t_win.close();
				            
				            Ext.getCmp(<?php  echo j($m_params->tree_id)?>).store.load();
				            
								acs_show_win_std('Modifica anagrafica', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				i_e: 'I', //ToDo
						  				rec_id: jsonData.id_prog
						  			}, 750, 550, {}, 'icon-globe-16');						  
				            
				            
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });                        
            } //handler
        }
      ]   
   
  }
 ]
}
<?php exit; }	



// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_ricalcola_iban'){
	
	require_once(dirname(__FILE__) . '/../../utility/php-iban-2.5.9/php-iban.php');
	
	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->form_values;
	
	//costruisco iban neutro
	$iban = "IT";
	$iban .= sprintf("%-2s", 'XX'); //CIN IBAN
	$iban .= sprintf("%-1s", 'Y'); //CIN BBAN
	$iban .= sprintf("%05d", $form_values->CCABI);
	$iban .= sprintf("%05d", $form_values->CCCAB);
	
	if (strlen(trim($form_values->CCCOCO)) < 12)
		$iban .= sprintf("%012d", $form_values->CCCOCO);
	else	
		$iban .= trim($form_values->CCCOCO);
	
	$iban = iban_set_nationalchecksum($iban);
	
	$ret = array();
	$ret['success'] = true;
	$ret['iban'] = $iban;
	
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verify_set_localita'){
	$m_params = acs_m_params_json_decode();

	$sql = "SELECT TA_COM.*, TA_REG.TADESC AS des_reg FROM {$cfg_mod_Gest['file_tabelle']} TA_COM			
				LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
					TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'	
				WHERE TA_COM.TATAID = 'ANCOM' AND TA_COM.TAKEY4 = '{$cfg_mod_Gest['cod_naz_italia']}' AND TA_COM.TAKEY2 = ? AND TA_COM.TADESC = ?";		
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->provincia, $m_params->comune));
	$row = db2_fetch_assoc($stmt);
	$ret = array();

	if ($row) {
		$ret['success'] = true;
		$ret['data'] = $row;
	}
	else
		$ret['success'] = false;
	
	echo acs_je($ret);
 exit;
}

// ******************************************************************************************
// VIEW GRID RESPONSE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'view_grid_response'){
	$m_params = acs_m_params_json_decode();	
	?>
	{"success":true, "items": [
			{
				xtype: 'grid',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
				store: {
						xtype: 'store',
						autoLoad:true,
						data: <?php echo acs_je(get_request_data($m_params->key_request)); ?>,
			        	fields: ['parametro', 'des_parametro', 'contenuto', 'codice_contenuto', 'tipo_contenuto']
				}, //store
		
				columns: [	
		    		  {text: 'Parametro', width: 90, 	dataIndex: 'des_parametro'}
		    		, {text: 'Contenuto', flex: 1, 		dataIndex: 'contenuto', renderer: function (value, metaData, rec){
		    		
		    				if (rec.get('tipo_contenuto') == 'D')
		    					return date_from_AS(value);
		    		
		    				if (Ext.isEmpty(rec.get('codice_contenuto')) == false){
		    					return '[' + rec.get('codice_contenuto') + '] ' + value;
		    				}
		    				 else return value;		    				
		    			}
		    		  }
				],
				enableSort: false

			, buttons: [
			
			<?php if ($m_params->view_type == 'base') { ?>		
							
						{
							xtype: 'button',
				            text: 'Dettagli', 
				            iconCls: 'icon-folder_open-32', scale: 'large',
				            handler: function() {
				            
								Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
										timeout: 2400000,
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved_by_company',
								        jsonData: {
								        	view_type: 'details',
								        	companyId: <?php echo $m_params->companyId; ?>
								        },	
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        						        
											Ext.getBody().unmask();
								            var jsonData = Ext.decode(result.responseText);
								            
											acs_show_win_std('Cerved response - Details', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
													{
														view_type: 'details',
										  				key_request: jsonData.key_request,
										  				companyId: jsonData.companyId
										  			}, 750, 550, {}, 'icon-globe-16');	
		
								        },
								        failure    : function(result, request){
											Ext.getBody().unmask();							        
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	
				            
				            }
				        }
				        
				        
				        
				        , {
							xtype: 'button',
				            text: 'Scores', 
				            iconCls: 'icon-certificate-32', scale: 'large',
				            handler: function() {
				            
								Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
										timeout: 2400000,
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved_by_company',
								        jsonData: {
								        	view_type: 'scores',
								        	companyId: <?php echo $m_params->companyId; ?>
								        },	
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        						        
											Ext.getBody().unmask();
								            var jsonData = Ext.decode(result.responseText);
								            
											acs_show_win_std('Cerved response - Scores', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
													{
														view_type: 'scores',
										  				key_request: jsonData.key_request,
										  				companyId: jsonData.companyId
										  			}, 750, 550, {}, 'icon-globe-16');	
		
								        },
								        failure    : function(result, request){
											Ext.getBody().unmask();							        
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	
				            
				            }
				        }
				        
				        , {
							xtype: 'button',
				            text: 'Copia intestazione', 
				            iconCls: 'icon-keyboard-32', scale: 'large',
				            handler: function() {
				            
				            	t_win = this.up('window');
				            	from_win = Ext.getCmp(<?php echo j($m_params->from_window_id); ?>);
				            	form = from_win.down('form').getForm();
				            
				            	this.up('grid').store.each(function(rec)  
								{
								  
								  if (rec.get('parametro') == 'taxCode') form.findField('GCCDFI').setValue(rec.get('contenuto'));
								  
								  if (rec.get('parametro') == 'companyName') form.findField('GCDCON').setValue(rec.get('contenuto'));  
								  if (rec.get('parametro') == 'street') form.findField('GCINDI').setValue(rec.get('contenuto'));
								  if (rec.get('parametro') == 'postCode') form.findField('GCCAP').setValue(rec.get('contenuto'));
								  
								  //if (rec.get('parametro') == 'municipality') form.findField('GCTEL').setValue(rec.get('contenuto'));								  								  						           
								  //if (rec.get('parametro') == 'municipality') form.findField('GCLOCA').setValue(rec.get('contenuto'));								  								  						            								  								  						           						            						            						            								  
								  
								  if (rec.get('parametro') == 'municipality') 	tmp_comune = rec.get('contenuto');
								  if (rec.get('parametro') == 'province') 		tmp_provincia = rec.get('contenuto');
								  
								},this);
								
								Ext.Ajax.request({
								   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=verify_set_localita',
								   method: 'POST',
								   jsonData: {
								   		comune: tmp_comune,
								   		provincia: tmp_provincia
								   	}, 
								   
								   success: function(response, opts) {
								   	  Ext.getBody().unmask();
									  var jsonData = Ext.decode(response.responseText);									   	  
     								  if (jsonData.success == true) {
     								  	form.findField('GCLOCA').setValue(tmp_comune);
     								  	form.findField('GCPROV').setValue(tmp_provincia);
     								  	form.findField('GCNAZI').setValue(<?php echo j($cfg_mod_Gest['cod_naz_italia']); ?>);
     								  	form.findField('v_regione').setValue(jsonData.data.DES_REG);
     								  	t_win.close();
     								  }	else {
     								  	acs_show_msg_error('Localit&agrave;/Provincia non presente in anagrafica');
     								  }
								   }, 
								   failure: function(response, opts) {
								      Ext.getBody().unmask();
								      alert('error');
								   }
								});									
									
				            }
				           }				        
				        
				        
				        
				 <?php } ?>
				        
				    ]
											    
				    		
	 	}
	]
   }
	
	<?php
	exit;
}





// ******************************************************************************************
// CERVED
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved'){
$m_params = acs_m_params_json_decode();

$service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";

$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $service_url,
		CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_USERAGENT => 'Codular Sample cURL Request'
));
// Send the request & save response to $resp
$resp = curl_exec($curl);

$resp_json = json_decode($resp);
$token = $resp_json->access_token;
$token_type = $resp_json->token_type;

// Close request to clear up some resources
curl_close($curl);

$taxCode = $m_params->form_values->GCPIVA; 

//RICHIEDO INFO AZIENDA
$service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies?&taxCode={$taxCode}&registered=N";

$headr = array();
$headr[] = 'Accept: application/json';
$headr[] = 'Authorization: '. $token_type . ' ' . $token;

$curl = curl_init();

// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $service_url,
		CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_USERAGENT => 'php curl',
		//CURLOPT_HEADER => true, //per debug
		CURLINFO_HEADER_OUT => true,
		CURLOPT_HTTPHEADER => $headr
));

// Send the request & save response to $resp
$resp = curl_exec($curl);

$headerSent = curl_getinfo($curl, CURLINFO_HEADER_OUT );

$resp_json = json_decode($resp);

//scrivo i parametri ricevuti su IC0
$key_request = write_parametri_cerved_base($resp_json);

// Close request to clear up some resources
curl_close($curl);

$ret['success'] = true;
$ret['key_request'] = $key_request;
$ret['companyId'] = $resp_json->companyInfo[0]->companyId;
echo acs_je($ret);
exit;
}





// ******************************************************************************************
// CERVED (by company)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved_by_company'){
	ini_set('max_execution_time', 300000);	
	$m_params = acs_m_params_json_decode();

	$service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";

	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'Codular Sample cURL Request'
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	$token = $resp_json->access_token;
	$token_type = $resp_json->token_type;

	// Close request to clear up some resources
	curl_close($curl);

	$taxCode = $m_params->form_values->GCPIVA;

	//RICHIEDO INFO AZIENDA
	$service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies/{$m_params->companyId}/{$m_params->view_type}";

	$headr = array();
	$headr[] = 'Accept: application/json';
	$headr[] = 'Authorization: '. $token_type . ' ' . $token;

	$curl = curl_init();

	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'php curl',
			//CURLOPT_HEADER => true, //per debug
			CURLINFO_HEADER_OUT => true,
			CURLOPT_HTTPHEADER => $headr
	));

	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	
	//scrivo i parametri ricevuti su IC0
	if ($m_params->view_type == 'details')
		$key_request = write_parametri_cerved_details($resp_json);
	if ($m_params->view_type == 'scores')
		$key_request = write_parametri_cerved_scores($resp_json);			
	
	// Close request to clear up some resources
	curl_close($curl);
	
	$ret['success'] = true;
	$ret['key_request'] = $key_request;
	$ret['companyId'] = $resp_json->companyInfo->companyId;	
	echo acs_je($ret);
	exit;
}
	






// ******************************************************************************************
// tree grid data
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	ini_set('max_execution_time', 300);	
	$m_params = acs_m_params_json_decode();
		
		$sql = "SELECT * 
				FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
				WHERE 
				  /*GCFG01 NOT IN ('I', 'U', 'H')*/
				  GCFG01 <> 'A'
				  AND GCDT = '$id_ditta_default'
				ORDER BY GCDTGE DESC, GCPRAB FETCH FIRST 50 ROWS ONLY";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();
		
		while ($r = db2_fetch_assoc($stmt)) {
						
			$tmp_ar_id = array();
			$ar_r = &$ar;			
									
			$cod_liv0 = trim($r['GCPROG']); 	//Progressivo				
			
			if (trim($r['GCTPAN']) == 'CLI'){
				//Cliente
				
				$liv = $r['GCPROG'];			
				$tmp_ar_id[] = $liv;
				
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new = $r;
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['liv'] = 'liv_1';
					$ar_new['liv_cod'] = $liv;
					$ar_new['liv_cod_out'] = $liv;				
					$ar_new['task'] = acs_u8e($r['GCDCON']);
					$ar_new['leaf'] = false;
					$ar_new['expanded'] = true;
					$ar_new["children"] = array();
					$ar_r["{$liv}"] = $ar_new;					
				}
				$ar_r = &$ar_r["{$liv}"];
				$ar_liv2 = &$ar_r;
			}

			if (in_array(trim($r['GCTPAN']), array('DES', 'PVEN'))) {
				//Destinazione
				
				$tmp_ar_id = array();
				$ar_r = &$ar;
				
				$liv = $r['GCPRAB'];	//anagrafica cliente collegata
				$tmp_ar_id[] = $liv;	
				$ar_r = &$ar_r["{$liv}"]["children"];
				
				$liv = $r['GCPROG'];
				$tmp_ar_id[] = $liv;
				
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new = $r;
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['liv'] = 'liv_2';
					$ar_new['liv_cod'] = $liv;
					$ar_new['liv_cod_out'] = $liv;
					$ar_new['task'] = "[" . trim($r['GCTPAN']) . "] " . acs_u8e($r['GCDCON']);
					$ar_new['leaf'] = true;
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r = &$ar_r["{$liv}"];
				$ar_liv2 = &$ar_r;
			}
			
/*		
			//LIVELLO 2  // destinazioni
			$liv = $cod_liv1;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_2';
				$ar_new['liv_cod'] = $liv;
				$ar_new['liv_cod_out'] = $liv;				
				$ar_new['task'] = acs_u8e($r['TFDCON']);
				$ar_new['leaf'] = false;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv3 = &$ar_r;
*/
				
	}
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
		$ret = $ret[0]['children'][0]['children'][0]['children'];
	}	
	
	 echo acs_je(array('success' => true, 'children' => $ret));	
	
	exit;
} //get_json_data



// ******************************************************************************************
// INVIO A GESTIONALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_send_to_gest'){
	$m_params = acs_m_params_json_decode();
	
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $m_params->rec_dt);
	$cl_p .= sprintf("%010s", $m_params->rec_prog);
	
	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31O1('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31O1', $libreria_predefinita_EXE, $cl_in, null, null);
		//$call_return = $tkObj->PgmCall('UTIBR31G5', $libreria_predefinita_EXE, '', null, null);
	}
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);	
	
 exit;	
}



// ******************************************************************************************
// EXE SAVE NEW ANAGRAGICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_new_anag'){
	
	$m_params = acs_m_params_json_decode();	
	
	$ar_ins = array();
	$ret = array();
	
	$r_comune  	= $s->get_TA_std('ANCOM', trim($m_params->form_values->GCLOCA));
	$r_regione 	= $s->get_TA_std_by_des('ANREG', trim($m_params->form_values->v_regione));
	$r_prov 	= $s->get_TA_std('ANPRO', trim($m_params->form_values->GCPROV));
	
	$ar_ins['GCUSGE'] = substr(trim($auth->get_user()), 0, 8);
	$ar_ins['GCDTGE'] = oggi_AS_date();
	$ar_ins['GCORGE'] = oggi_AS_time();	
	
	$ar_ins['GCDCON'] 	= utf8_decode(trim($m_params->form_values->GCDCON));
	$ar_ins['GCINDI'] 	= utf8_decode(trim($m_params->form_values->GCINDI));
	$ar_ins['GCCAP'] 	= trim($m_params->form_values->GCCAP);
	
	$ar_ins['GCKMIT'] 	= sql_f(trim($m_params->form_values->GCKMIT));
	
	//$ar_ins['GCLOCA'] 	= trim($r_comune['TADESC']);
	$ar_ins['GCLOCA']	= utf8_decode(trim($m_params->form_values->GCLOCA));
	$ar_ins['GCPROV'] 	= utf8_decode(trim($m_params->form_values->GCPROV)); //trim($r_comune['TAKEY2']);
	$ar_ins['GCNAZI'] 	= utf8_decode(trim($m_params->form_values->GCNAZI));
	$ar_ins['GCCISN'] 	= trim($r_comune['TAKEY4']);   //Nazione
	
	$ar_ins['GCZONA'] 	= trim($r_regione['TANAZI']);   //Zona
	$ar_ins['GCCITI'] 	= trim($r_prov['TANAZI']);   	//Itinerario
	
	$ar_ins['GCPIVA'] 	= utf8_decode(trim($m_params->form_values->GCPIVA));
	$ar_ins['GCCDFI'] 	= utf8_decode(trim($m_params->form_values->GCCDFI));
	
	$ar_ins['GCMAIL'] 	= utf8_decode(trim($m_params->form_values->GCMAIL));
	$ar_ins['GCWWW'] 	= utf8_decode(trim($m_params->form_values->GCWWW));
	$ar_ins['GCMAI1'] 	= utf8_decode(trim($m_params->form_values->GCMAI1));
	$ar_ins['GCMAI2'] 	= utf8_decode(trim($m_params->form_values->GCMAI2));
	$ar_ins['GCMAI3'] 	= utf8_decode(trim($m_params->form_values->GCMAI3));
	$ar_ins['GCMAI4'] 	= utf8_decode(trim($m_params->form_values->GCMAI4));
	
	$ar_ins['GCSEL3'] 	= utf8_decode(trim($m_params->form_values->GCSEL3));   //tipologia
	
	$ar_ins['GCTEL'] 	= utf8_decode(trim($m_params->form_values->GCTEL));	
	$ar_ins['GCTEL2'] 	= utf8_decode(trim($m_params->form_values->GCTEL2));
	$ar_ins['GCFAX'] 	= utf8_decode(trim($m_params->form_values->GCFAX));
	
	//destinazione standard/alternativa
	$ar_ins['GCTPDS'] 	= utf8_decode(trim($m_params->form_values->GCTPDS));


	if ($m_params->form_values->mode == 'NEW') {
		$ar_ins['GCDT'] 	= $id_ditta_default;
		$ar_ins['GCPROG'] 	= $s->next_num('ANAG_GC');
		
		if ($m_params->form_values->tipo_anagrafica == 'DES'){
			$ar_ins['GCTPAN'] 	= 'DES';
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else if ($m_params->form_values->tipo_anagrafica == 'PVEN'){
			$ar_ins['GCTPAN'] 	= 'PVEN';
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else {
			$ar_ins['GCTPAN'] 	= 'CLI';
			$ar_ins['GCDEFA'] 	= $m_params->open_request->modan;
		}
		
		
		//insert riga
		$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
		$ret['GCPROG'] = $ar_ins['GCPROG'];		
	}
	

	if ($m_params->form_values->mode == 'EDIT') {
		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_ins,
				array($m_params->form_values->GCPROG)
				));
		echo db2_stmt_errormsg($stmt);
		
		$ret['success'] = true;
		$ret['GCPROG'] = $m_params->form_values->GCPROG;
	}

	echo acs_je($ret);
 exit;
}




function exe_save_logis_add_field($ar, $m_params){
	$ar['GCLING'] 	= utf8_decode($m_params->form_values->GCLING);
	$ar['GCSEL3'] 	= utf8_decode($m_params->form_values->GCSEL3);
	$ar['GCCFGC'] 	= utf8_decode($m_params->form_values->GCCFGC);
	$ar['GCNAZO'] 	= utf8_decode($m_params->form_values->GCNAZO);
	$ar['GCFMNC'] 	= utf8_decode($m_params->form_values->GCFMNC);
	$ar['GCICON'] 	= utf8_decode($m_params->form_values->GCICON);
	$ar['GCIFAT'] 	= utf8_decode($m_params->form_values->GCIFAT);
	$ar['GCIPSP'] 	= utf8_decode($m_params->form_values->GCIPSP);
	$ar['GCGGS1'] 	= utf8_decode($m_params->form_values->GCGGS1);
	$ar['GCGGS2'] 	= utf8_decode($m_params->form_values->GCGGS2);
	$ar['GCGGS3'] 	= utf8_decode($m_params->form_values->GCGGS3);
	$ar['GCGGS4'] 	= utf8_decode($m_params->form_values->GCGGS4);
	$ar['GCGGS5'] 	= utf8_decode($m_params->form_values->GCGGS5);
	$ar['GCGGS6'] 	= utf8_decode($m_params->form_values->GCGGS6);
	$ar['GCNOSC'] 	= utf8_decode($m_params->form_values->GCNOSC);	
	return $ar;
}




function exe_save_cc_add_field($ar, $m_params){
	$ar['CCAG1'] 	= utf8_decode($m_params->form_values->CCAG1);
	$ar['CCAG2'] 	= utf8_decode($m_params->form_values->CCAG2);
	$ar['CCAG3'] 	= utf8_decode($m_params->form_values->CCAG3);
	$ar['CCPA1'] 	= sql_f($m_params->form_values->CCPA1);
	$ar['CCPA2'] 	= sql_f($m_params->form_values->CCPA2);
	$ar['CCPA3'] 	= sql_f($m_params->form_values->CCPA3);
	
	$ar['CCPAGA'] 	= utf8_decode(trim($m_params->form_values->CCPAGA));
	$ar['CCNGPA'] 	= sql_f($m_params->form_values->CCNGPA);
	$ar['CCMME1'] 	= sql_f($m_params->form_values->CCMME1);
	$ar['CCMME2'] 	= sql_f($m_params->form_values->CCMME2);
	$ar['CCGGE1'] 	= sql_f($m_params->form_values->CCGGE1);
	$ar['CCGGE2'] 	= sql_f($m_params->form_values->CCGGE2);
	$ar['CCTSIV'] 	= utf8_decode(trim($m_params->form_values->CCTSIV));
	
	$ar['CCREFE'] 	= utf8_decode(trim($m_params->form_values->CCREFE));
	$ar['CCLIST'] 	= utf8_decode(trim($m_params->form_values->CCLIST));
	$ar['CCCATR'] 	= utf8_decode(trim($m_params->form_values->CCCATR));
	$ar['CCSC1'] 	= sql_f($m_params->form_values->CCSC1);
	$ar['CCSC2'] 	= sql_f($m_params->form_values->CCSC2);
	$ar['CCSC3'] 	= sql_f($m_params->form_values->CCSC3);
	$ar['CCSC4'] 	= sql_f($m_params->form_values->CCSC4);
	
	if (strlen(trim($m_params->form_values->CCABI)) > 0)
		$ar['CCABI'] 	= sprintf("%05s", trim($m_params->form_values->CCABI));
	else
		$ar['CCABI'] = '';
	
	if (strlen(trim($m_params->form_values->CCCAB)) > 0)
		$ar['CCCAB'] 	= sprintf("%05s", trim($m_params->form_values->CCCAB));
	else
		$ar['CCCAB'] = '';

	if (strlen(trim($m_params->form_values->CCCOCO)) > 0)
		if (strlen(trim($m_params->form_values->CCCOCO)) < 12)	
			$ar['CCCOCO'] 	= sprintf("%012d", $m_params->form_values->CCCOCO);
		else
			$ar['CCCOCO'] = trim($m_params->form_values->CCCOCO);
	else	
		$ar['CCCOCO'] = '';
	
	$ar['CCIBAN'] 	= sql_f($m_params->form_values->CCIBAN);	
	
	
  return $ar;
}



function exe_save_fido_add_field($ar, $m_params){
	$ar['CCDTVI'] 	= $m_params->form_values->CCDTVI;
	$ar['CCDTVF'] 	= $m_params->form_values->CCDTVF;
	$ar['CCIMFD'] 	= sql_f($m_params->form_values->CCIMFD);

	return $ar;
}



// ******************************************************************************************
// EXE SAVE CONDIZIONE COMMERCIALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_cc'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

		
		
		//se non esiste la condizione standard la creo
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? AND CCSEZI='BCCO' AND CCCCON = 'STD'";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($id_ditta_default, $m_params->form_values->CCPROG));
		echo db2_stmt_errormsg($stmt);
		
		$row = db2_fetch_assoc($stmt);
		if (!$row){
			$ar_ins['CCUSGE'] = trim($auth->get_user());
			$ar_ins['CCDTGE'] = oggi_AS_date();
			$ar_ins['CCORGE'] = oggi_AS_time();
		
			$ar_ins['CCDT'] 	= $id_ditta_default;
			$ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
			$ar_ins['CCSEZI'] 	= 'BCCO';
			$ar_ins['CCCCON'] 	= 'STD';
		
			$ar_ins = exe_save_cc_add_field($ar_ins, $m_params);
		
			//insert riga
			$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
			echo db2_stmt_errormsg($stmt);
		}
		
		
		$ar_upd = exe_save_cc_add_field(array(), $m_params);
		
		$ar_upd['CCUSGE'] = trim($auth->get_user());
		$ar_upd['CCDTGE'] = oggi_AS_date();
		$ar_upd['CCORGE'] = oggi_AS_time();		
		
		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
				SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";		

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, 'BCCO', 'STD')
		));
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;

	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// EXE SAVE FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_fido'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();



	if ($m_params->open_request->mode == 'NEW') {

		$ar_ins['CCUSGE'] = trim($auth->get_user());
		$ar_ins['CCDTGE'] = oggi_AS_date();
		$ar_ins['CCORGE'] = oggi_AS_time();

		$ar_ins['CCDT'] 	= $id_ditta_default;
		$ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
		$ar_ins['CCSEZI'] 	= 'BFID';
		$ar_ins['CCCCON'] 	= utf8_decode(trim($m_params->form_values->CCCCON));
		$ar_ins['CCDCON'] 	= utf8_decode(trim($m_params->form_values->CCDCON));

		$ar_ins = exe_save_fido_add_field($ar_ins, $m_params);

		//insert riga
		$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}


	if ($m_params->open_request->mode == 'EDIT') {
		$ar_upd = exe_save_fido_add_field(array(), $m_params);

		$ar_upd['CCUSGE'] = trim($auth->get_user());
		$ar_upd['CCDTGE'] = oggi_AS_date();
		$ar_upd['CCORGE'] = oggi_AS_time();

		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
		SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, $m_params->form_values->CCSEZI, $m_params->form_values->CCCCON)
		));
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}

	echo acs_je($ret);
	exit;
}






// ******************************************************************************************
// EXE SAVE FORM LOGISTICA/CONFIGURAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_logis'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	$ar_upd = exe_save_logis_add_field(array(), $m_params);
	
	$ar_upd['GCUSGE'] = trim($auth->get_user());
	$ar_upd['GCDTGE'] = oggi_AS_date();
	$ar_upd['GCORGE'] = oggi_AS_time();
	
	
	//update riga
	$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE GCDT='$id_ditta_default' AND GCPROG=?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($m_params->form_values->GCPROG)
	));
	echo db2_stmt_errormsg($stmt);

	$ret['success'] = true;


	echo acs_je($ret);
	exit;
}







// ******************************************************************************************
// ELENCO CONDIZIONI COMMERCIALI PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_cc'){

	$m_params = acs_m_params_json_decode();
	
	$ar_ins = array();
	$ret = array();

	$sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID FROM {$cfg_mod_Gest['file_tabelle']} TA
			LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}  AN ON
				CCDT = ? AND CCPROG = ? AND CCSEZI = 'BCCO' AND CCCCON = TAKEY2 
			WHERE TATAID = 'ANCCO' AND TAKEY1 = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id, $m_params->GCDEFA ));
	echo db2_stmt_errormsg($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$ret[] = $row;
	}

	echo acs_je($ret);
	exit;
}







// ******************************************************************************************
// ELENCO CONDIZIONI AMMINISTRATIVE PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_fido'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	$sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID FROM {$cfg_mod_Gest['file_tabelle']} TA
			LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}  AN ON
			CCDT = ? AND CCPROG = ? AND CCSEZI = 'BFID' AND CCCCON = TAKEY1
			WHERE TATAID = 'ANC05'";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id));
	echo db2_stmt_errormsg($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$ret[] = $row;
	}

	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// OPEN TAB
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){ ?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'hbox',
	    align: 'stretch',
	    pack : 'start',
	},
    <?php echo make_tab_closable(); ?>,	
    title: 'Customer POS',
    tbar: new Ext.Toolbar({
      items:[
            '<b>Inserimento/Manutenzione anagrafica clienti punto vendita</b>', '->',
            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').expandAll();}}
            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').collapseAll();}}
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
      ]            
    }),  
	
	
	items: [  
  		  <?php write_main_tree(array()); ?>
  	    , <?php // write_main_form(array()); ?>
  	]
  }
 ]
}
<?php exit; } 
// ******************************************************************************************
// NUOVA ANAGRAFICA
// ******************************************************************************************
$m_params = acs_m_params_json_decode();
if ($_REQUEST['fn'] == 'new_anag_cli' && $m_params->i_e == 'I'){ ?>
{
 success:true,  
 items: [
  	{
		xtype: 'tabpanel',
		
		<?php
		$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
		
		//se rientro in modifica, ricarico il record		
		if ($all_params['mode'] == 'EDIT') {
			$sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG 
					FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
					LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
						TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
					LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
						TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
					WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($all_params['rec_id']));
			$row = db2_fetch_assoc($stmt);			
		}
		?>
			
		
		items: [
		
		   	<?php write_new_form_ITA(
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>, 	

		      	<?php write_new_form_COMM(
			   			array(),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())
			   	); ?>
		   	
		   	
		]
	}
 ]
}  	
<?php exit; } ?>
<?php
// ******************************************************************************************
// NUOVA DESTINAZIONE - ITALIA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'new_dest' && $m_params->i_e == 'I'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
		$sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG FROM {$cfg_mod_DeskUtility['ins_new_anag']['file_appoggio']} GC
				LEFT OUTER JOIN  {$cfg_mod_DeskUtility['file_tabelle']} TA_COM ON
				TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
				LEFT OUTER JOIN   {$cfg_mod_DeskUtility['file_tabelle']} TA_REG ON
				TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
				WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($all_params['rec_id']));
		$row = db2_fetch_assoc($stmt);
	}
	
	?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'DES'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } ?>

<?php
if ($_REQUEST['fn'] == 'new_pven' && $m_params->i_e == 'I'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
		$sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
		LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
		TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
		LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
		TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
		WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($all_params['rec_id']));
		$row = db2_fetch_assoc($stmt);
	}
	
	
?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'PVEN'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } ?>




<?php
// ******************************************************************************************
// OPEN FORM FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_FIDO'){ ?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'panel',
		layout: 'fit',
		items: [
				
			   	<?php write_new_form_FIDO(
			   			array(),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } ?>



<?php

/********************************************************************************** 
 WIDGET
 **********************************************************************************/
function write_main_tree($p){
	global $s;	
?>	
			{
				xtype: 'treepanel',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					    fields: ['trCls', 'task', 'liv', 'liv_data', 'liv_cod', 'liv_cod_out', 'liv_cod_qtip',
					    	'GCDT', 'GCPROG',
					    	'GCDCON', 'GCMAIL', 'GCWWW', 'GCMAI1', 'GCMAI2', 'GCMAI3', 'GCMAI4', 'GCTEL', 'GCTEL2', 'GCFAX',
					    	'GCINDI', 'GCCDCF', 'GCCDFI', 'GCPIVA', 'GCLOCA', 'GCTPAN', 'GCPROV', 'GCDTGE'
					    ],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							actionMethods: {read: 'POST'},
							timeout: 240000,
	                        
	                        reader: {
	                            root: 'children'
	                        },
	                        
							extraParams: {
							}
	                	    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		{text: 'Cliente', flex: 1, xtype: 'treecolumn', dataIndex: 'task', menuDisabled: true}
		    		, {text: 'Data', width: 70, dataIndex: 'GCDTGE', renderer: date_from_AS}
		    		, {text: 'Localit&agrave;', flex: 1, dataIndex: 'GCLOCA'}					
		    		, {text: 'Indirizzo', flex: 1, dataIndex: 'GCINDI'}
		    		, {text: 'Prov', width: 35, dataIndex: 'GCPROV'}
					, {text: 'Codice', width: 100, dataIndex: 'GCCDCF', 
							renderer: function(value, metaData, record){
								if (value.trim().length > 0)
									metaData.tdCls += ' sfondo_verde';
								return value;	
							}
					}		    			   	
					, {text: 'Cod.Fiscale', width: 150, dataIndex: 'GCCDFI'}
					, {text: 'P.IVA', width: 130, dataIndex: 'GCPIVA'}
				],
				enableSort: true,
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
					    celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  
						  	rec = iView.getRecord(iRowEl);
						  	
						  	iEvent.stopEvent();
						  
	            			my_tree = iView;
							my_listeners_upd_dest = {
	        					afterEditRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_tree.getTreeStore().load();		        						
	        						from_win.close();
					        		}
			    				};					  

						    if (rec.get('GCCDCF').trim().length > 0){
						    	console.log('Richiedo aggiornamento an. cliente');
						    	Ext.getBody().mask('Loading... ', 'loading').show();
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_import_anag_by_prog',
									        timeout: 2400000,
									        jsonData: {
									        	gcprog: rec.get('GCPROG')
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									        	Ext.getBody().unmask();
									            var jsonData = Ext.decode(result.responseText);
									            
									            //refresh dei record: altrimenti ha in linea i vecchi id prog
									            my_tree.getSelectionModel().deselectAll();
												my_tree.getTreeStore().load();										            
									            
												if (rec.get('GCTPAN').trim() == 'DES') {
													acs_show_win_std('Modifica destinazione', 
														'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
														{
											  				mode: 'EDIT',
											  				modan: 'ToDo',
											  				i_e: 'I', //ToDo
											  				rec_id: jsonData.id_prog
											  			}, 750, 550, my_listeners_upd_dest, 'icon-globe-16');							
												 	return;
												}
												
												if (rec.get('GCTPAN').trim() == 'PVEN') {
													acs_show_win_std('Modifica anagrafica', 
														'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
														{
											  				mode: 'EDIT',
											  				modan: 'ToDo',
											  				i_e: 'I', //ToDo
											  				rec_id: jsonData.id_prog
											  			}, 750, 550, my_listeners_upd_dest, 'icon-globe-16');							
												 	return;
												}								
																	  						  	
												acs_show_win_std('Modifica anagrafica', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
													{
										  				mode: 'EDIT',
										  				modan: 'ToDo',
										  				i_e: 'I', //ToDo
										  				rec_id: jsonData.id_prog
										  			}, 750, 550, {}, 'icon-globe-16');						  
										  	
										  	return false;									            
									            									            
									        }, //success
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });  						    	
						    	
						    	return;
						    }

							
								if (rec.get('GCTPAN').trim() == 'DES') {
									acs_show_win_std('Modifica destinazione', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 750, 550, my_listeners_upd_dest, 'icon-globe-16');							
								 	return;
								}
								
								if (rec.get('GCTPAN').trim() == 'PVEN') {
									acs_show_win_std('Modifica anagrafica', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 750, 550, my_listeners_upd_dest, 'icon-globe-16');							
								 	return;
								}								
													  						  	
								acs_show_win_std('Modifica anagrafica', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				i_e: 'I', //ToDo
						  				rec_id: rec.get('id')
						  			}, 750, 550, my_listeners_upd_dest, 'icon-globe-16');						  
						  	
						  	return false;
						  				
						  }
					    },
			            
						itemclick: function(view, record, item, index, e) {
								//form = this.up('panel').down('form');
								//console.log(record);
								//form.loadRecord(record);
						},			            
			          
			          
			          itemcontextmenu : function(grid, rec, node, index, event) {			          				                          
			          }
			            	        
			        } //listeners
				

		, buttons: [
		
		
				{
							xtype: 'button',
				            text: 'Interrogazione/manutenzione anagrafica cliente', 
				            iconCls: 'icon-clienti-32', scale: 'large',
				            handler: function() {				            			
								acs_show_win_std('Ricerca cliente', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=search_anag_cli', 
									{tree_id: this.up('treepanel').id}, 650, 350, {}, 'icon-globe-16');
				            }
				}, { xtype: 'tbfill' },		
		
			, {
	         	xtype: 'splitbutton',
	            text: 'Crea nuova anagrafica',    //privato azienda
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	
		        	<?php foreach ($s->find_TA_std('MODAP', null, 'N', 'Y') as $ec) { ?> 	
						{
							xtype: 'button',
				            text: <?php echo j(trim($ec['text'])) ?>,
				            //iconCls: 'icon-save-32', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_inserimento = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						//from_win.close();
								        		}
						    				};					            
										acs_show_win_std('Immissione nuova anagrafica cliente', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
											{
								  				mode: 'NEW',
								  				modan: <?php echo j(trim($ec['id'])) ?>,
								  				i_e: <?php echo j(trim($ec['TAFG01'])) ?>
								  			}, 750, 550, my_listeners_inserimento, 'icon-globe-16');
				            
				            }
				        },
				       <?php } ?>  
				            	        	
		        	]
		        }
	        }, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>destinazione',
	            iconCls: 'icon-button_black_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					  
						    				          	
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}
				            			
				            			parent_prog = selected_record[0].get('id');				            							            			
										acs_show_win_std('Nuova destinazione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				parent_prog: parent_prog
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					         
						    				
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
				            			parent_prog = selected_record[0].get('id');				            							            			
										acs_show_win_std('Nuova destinazione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				parent_prog: parent_prog
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				},  {
							xtype: 'button',
				            text: 'Conferma archiviazione anagrafica cliente', 
				            iconCls: 'icon-outbox-32', scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};					  
						    				          	
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			
				            			rec_prog = selected_record[0].get('id');
				            			
				            			Ext.getBody().mask('Loading... ', 'loading').show();
										Ext.Ajax.request({
										        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_send_to_gest',
										        timeout: 2400000,
										        jsonData: {
										        	rec_dt: selected_record[0].get('GCDT'),
										        	rec_prog: selected_record[0].get('GCPROG'),
										        },	
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){	            	
													Ext.getBody().unmask();
										            var jsonData = Ext.decode(result.responseText);
										            my_tree.store.load();
										        },
										        failure    : function(result, request){
										        	Ext.getBody().unmask();
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });				            							            			
										
				            
				            }
				        }        
         ]
			        
			        
			        
			, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');

			           if (record.get('trCls')=='grassetto')
					        return ret + ' grassetto';
			           
			           if (record.get('trCls')=='G')
					        return ret + ' colora_riga_grigio';
			           if (record.get('trCls')=='Y')
					        return ret + ' colora_riga_giallo';					        
			           		        	
			           return ret;																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree



function write_main_form($p){
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            flex: 0.3,
            
            defaults:{ anchor: '-10' , xtype: 'textfield', labelAlign: 'top'},
            
            items: [
            	{name: 'GCDCON',	fieldLabel: 'Denominazione'},
            	{name: 'GCWW', 		fieldLabel: 'Sito web'},
            	{name: 'GCMAIL', 	fieldLabel: 'Email azienda'},
            	{name: 'GCMAI1', 	fieldLabel: 'Email - Conf. ordine'},
            	{name: 'GCMAI2', 	fieldLabel: 'Email - Fatt. vendita'},
            	{name: 'GCMAI3', 	fieldLabel: 'Email - Piani sped.'},
            	{name: 'GCMAI4', 	fieldLabel: 'Email - Resi'},
            	{name: 'GCTEL', 	fieldLabel: 'Telefono'},
            	{name: 'GCTEL2', 	fieldLabel: 'Telefono 2'},
            	{name: 'GCFAX', 	fieldLabel: 'Fax'},
            ],
			buttons: [],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_main_form







function write_new_form_ITA($p = array(), $request = array(), $r = array()){
	global $s, $main_module;
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '0 0 0 3',
            frame: true,
            title: 'Intestazione',
            autoScroll: true,
            
            flex: 0.7,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'mode',
	                	value: <?php echo j($request['mode']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'tipo_anagrafica',
	                	value: <?php echo j($p['tipo_anagrafica']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'tipologia',
	                	value: <?php echo j($cfg_mod_Gest['cod_naz_italia']); ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'GCPROG',
	                	value: <?php echo j(trim($r['GCPROG'])); ?>	
                	}
                	
                	
<?php if (in_array($p['tipo_anagrafica'], array('DES'))) { ?>
					, <?php write_combo_std('GCTPDS', 'Tipo destinazione', $r['GCTPDS'], acs_ar_to_select_json(ar_standard_alternativa(), '', 'R'), array() ) ?>
<?php } ?>                	
                	
                	
                	
                	
                	
                	, {
						name: 'GCDCON',
						fieldLabel: 'Denominazione',
					    maxLength: 100,
					    value: <?php echo j(trim(acs_u8e($r['GCDCON']))); ?>								
					},
					{
									name: 'GCINDI',
									fieldLabel: 'Indirizzo',
									maxLength: 100,
									value: <?php echo j(trim(acs_u8e($r['GCINDI']))); ?>							
							},
					 {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							  
								{
									xtype: 'fieldcontainer', flex: 3,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'GCLOCA', flex: 1,
											fieldLabel: 'Localit&agrave;', labelWidth: 130,
										    value: <?php echo j(trim(acs_u8e($r['GCLOCA']))); ?>,
										    readOnly: true
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners_search_loc = {
									        					afterSelected: function(from_win, record_selected){
										        						m_form.findField('GCLOCA').setValue(record_selected.get('descr'));
										        						m_form.findField('GCPROV').setValue(record_selected.get('COD_PRO'));
										        						m_form.findField('v_regione').setValue(record_selected.get('DES_REG'));
										        						m_form.findField('GCNAZI').setValue(record_selected.get('COD_NAZ'));
										        						from_win.close();
														        		}										    
														    },
															
															
															acs_show_win_std('Seleziona localit&agrave;', 
																'search_loc.php?fn=open_search', 
																{}, 450, 150, my_listeners_search_loc, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								}							  
							  	
								
								
								
						        
						        , {
									name: 'GCCAP', width: 110, labelWidth: 70,
									fieldLabel: 'Cap', labelAlign: 'right',
								    maxLength: 5,
								    value: <?php echo j(trim(acs_u8e($r['GCCAP']))); ?>
								}								    	
							
						]
					 }, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							  
								{
									xtype: 'fieldcontainer', flex: 3,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'v_regione',  flex: 1,
											fieldLabel: 'Regione',
										    labelWidth: 130,
										    value: <?php echo j(trim(acs_u8e($r['DES_REG']))); ?>,
										    readOnly: true
										}
								  ]
								}							  
							  	
						        
						        , {
									name: 'GCPROV', width: 110, labelWidth: 70,
									fieldLabel: 'Prov.', labelAlign: 'right',
								    maxLength: 5, readOnly: true,
								    value: <?php echo j(trim(acs_u8e($r['GCPROV']))); ?>,
							    	readOnly: true	
								}	
							
						]
					 },
					 
					 		{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [					
							 {
								name: 'GCNAZI',
								fieldLabel: 'Nazione', labelWidth: 130,
							    maxLength: 2,
							    value: <?php echo j(trim(acs_u8e($r['GCNAZI']))); ?>,
							    readOnly: true							
							},{
								name: 'GCTEL',
								fieldLabel: 'Telefono 1',labelAlign: 'right',
								maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCTEL']))); ?>							
							}
						]
					},{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [

							<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>						
							 {
								name: 'GCCDFI',
								fieldLabel: 'Codice fiscale',
							    maxLength: 100,
							    allowBlank: false,
							    value: <?php echo j(trim(acs_u8e($r['GCCDFI']))); ?>							
							} ,
							<?php } ?>
							 {
								name: 'GCTEL2',
								fieldLabel: 'Telefono 2', labelAlign: 'right',
							    maxLength: 20,
							    value: <?php echo j(trim(acs_u8e($r['GCTEL2']))); ?>
							}
						]
					},{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [

							<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>						
								{
								name: 'GCPIVA',
								fieldLabel: 'P. IVA',
							    maxLength: 100,
							    value: <?php echo j(trim(acs_u8e($r['GCPIVA']))); ?>							
							} ,
							<?php } ?>
							 {
								name: 'GCFAX',
								fieldLabel: 'Fax', labelAlign: 'right',
							    maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCFAX']))); ?>
							}
						]
					}
					, {
						xtype: 'fieldset', title: 'E-mail',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								{
									vtype: 'email',
									name: 'GCMAIL',
									fieldLabel: 'Principale',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAIL']))); ?>							
								}
								
								
<?php if (!in_array($p['tipo_anagrafica'], array('PVEN'))) { ?>								
								, {
									vtype: 'email',
									name: 'GCMAI1',
									fieldLabel: 'Contratto/Ordine',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI1']))); ?>							
								}
					<?php if (!in_array($p['tipo_anagrafica'], array('DES'))) { ?>								
								, {
									vtype: 'email',
									name: 'GCMAI2',
									fieldLabel: 'Fattura',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI2']))); ?>							
								}
					<?php } ?>
								
							
<?php } ?>			

			
								
						]
					  }
					  
					  
					  <?php if (!in_array($p['tipo_anagrafica'], array('DES'))) { ?>								
						
						
                          , <?php write_combo_std('GCSEL3', 'Tipologia', $r['GCSEL3'], acs_ar_to_select_json($main_module->find_TA_std('ANC01'), '', 'R') ) ?>

						  , <?php write_textarea_std('GCNOTE', 'Note', trim($r['GCNOTE']), 240 ) ?>

								
							
					<?php } ?>
					  	
					
					
							
					
					
					
					],
					
					
					
					
		dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            fixed: true,
            items: [					
			{
		        xtype:'tbspacer',
		        flex:1
		    } 
	        , {
	            text: 'Gmap',
	            iconCls: 'icon-gmaps_logo-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	
	            	//costruisco indirizzo
	            	ind = 'VIA INSALA, 236 - 61122 - PESARO';
	            	
					//$ar[$liv1_v]["val"][$liv2_v]["gmap_ind"]	= implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));
					
					f = [];
					//f.push(form.findField('f_nazione').getValue());
					f.push(form.findField('GCCAP').getValue());
					f.push(form.findField('GCPROV').getValue());
					f.push(form.findField('GCLOCA').getValue());
					f.push(form.findField('GCINDI').getValue());
					
					km_field_id = form.findField('GCKMIT').id;	            	
	            	
					gmapPopup("../desk_vend/gmap_route.php?km_field_id=" + km_field_id + "&ind=" + f.join());	            	
	            	
	            }
	         }
/*	         
	         , {
	            text: 'Percorso',
	            iconCls: 'icon-pneumatico-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	
	            	//costruisco indirizzo
					f = [];
					//f.push(form.findField('f_nazione').getValue());
					f.push(form.findField('GCCAP').getValue());
					f.push(form.findField('GCPROV').getValue());
					f.push(form.findField('GCLOCA').getValue());
					f.push(form.findField('GCINDI').getValue());
					gmapPopup("../desk_vend/gmap_percorso_single.php?ind=" + f.join());	            	
	            	
	            }
	         }
*/	         
	         
			, {
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');

					if(form.isValid()){
						Ext.Ajax.request({
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_new_anag',
						        jsonData: {
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						            var jsonData = Ext.decode(result.responseText);
						            loc_win.fireEvent('afterInsertRecord', loc_win);
						            form.findField('GCPROG').setValue(jsonData.GCPROG);
						            form.findField('mode').setValue('EDIT');
						            
						            tabPanel = loc_win.down('tabpanel');
						            
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						            						            
						            
									var activeTab = tabPanel.getActiveTab();
									var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
						            
									//activeTab_idx = tab_panel.getActiveTab();
									//tab_panel.setActiveTab(1);

									tabPanel.items.each(function(panelItem, index, totalCount){
										panelItem.setDisabled(false);
										
										//per aggiornare le condizioni commerciali: MIGLIORARE: TODO
										if (panelItem.itemId == 'form_COMM'){
											panelItem.getForm().findField('CCPROG').setValue(jsonData.GCPROG);
											panelItem.disabled = false;
										}
										
								
										
										if (panelItem.itemId == 'form_LOGIS'){
											panelItem.getForm().findField('GCPROG').setValue(jsonData.GCPROG);
											panelItem.disabled = false;
										}
										
									});
									
									tabPanel.setActiveTab(activeTabIndex + 1);
											
															            
						            	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }            	                	                
	            }
	        }	         
	         
	        ]// buttons
	       }
	      ]             
			
			
			, listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_new_form_ITA



function write_new_form_COMM($p = array(), $request = array()){
	global $conn, $cfg_mod_Gest, $id_ditta_default;

	if ($request['mode'] == 'EDIT'){
		//recupero la riga da modificare
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? /*AND CCSEZI=? AND CCCCON=?*/";		
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($id_ditta_default, $request['rec_id']));
		//$result = db2_execute($stmt, array($id_ditta_default, $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));
		
		$ret = array();
		$r = db2_fetch_assoc($stmt);
		
		//des_banca
		if (strlen(trim($r['CCABI'])) > 0 && strlen(trim($r['CCCAB'])) > 0) {				
			$sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();		
			$result = db2_execute($stmt, array($r['CCABI'], $r['CCCAB']));
			$r_cab = db2_fetch_assoc($stmt);
			$des_banca = implode(" - ", array(trim($r_cab['XDSABI']), trim($r_cab['XDSCAB'])));
		}
		
		
	} else {
		
		
		//DALLA REGIONE PROVO A RECUPERARE IL REFERENTE (TATISP IN TATAID='ANREG')
		
			$sql = "SELECT TA_REG.TADESC AS DES_REG, TA_REG.TATISP AS REF_BY_REG 
					FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
					LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
						TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
					LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
						TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
					WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($request['CCPROG']));
			$r_regione = db2_fetch_assoc($stmt);
		
		//NUOVA CONDIZIONE COMMERCIALE
		$r = array(	'CCDT' => $request['CCDT'], 
					'CCPROG' => $request['CCPROG'], 
					'CCSEZI' => $request['CCSEZI'], 
					'CCCCON' => $request['CCCCON'],
					'CCDCON' => $request['CCDCON'],
					'CCREFE' => $r_regione['REF_BY_REG']
		);
	}
	
	global $s, $main_module, $id_ditta_default;
	?>
        {
            xtype: 'form',
     		itemId: 'form_COMM',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Modifica condizione commerciale',
            disabled: false, //true,
            autoScroll: true,
            
            flex: 1,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [
            		{xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($id_ditta_default); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j('BCCO'); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($request['rec_id']); ?>},
            		{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j('STD'); ?>},
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							    value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,
							    disabled: true
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    maxLength: 5,
							    value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    disabled: true							
							}
							]
						} ,
						{
						xtype: 'fieldset', title: 'Pagamento',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('CCPAGA', 'Pagamento', $r['CCPAGA'], acs_ar_to_select_json($main_module->find_sys_TA('CUCP'), ''), array('flex_width' => "flex: 2") ) ?>
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [										
										{
											name: 'CCNGPA',
											fieldLabel: 'GG inizio scad.',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCNGPA']))); ?>							
										}, {
											name: 'CCMME1',
											fieldLabel: '1&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCMME1']))); ?>							
										}, <?php write_combo_std('CCGGE1', '', $r['CCGGE1'], acs_ar_to_select_json($main_module->find_TA_std('ANC04'), ''), array('flex_width' => "flex: 1") ) ?>
										
										, {
											name: 'CCMME2',
											fieldLabel: '2&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCMME2']))); ?>							
										}, <?php write_combo_std('CCGGE2', '', $r['CCGGE2'], acs_ar_to_select_json($main_module->find_TA_std('ANC04'), ''), array('flex_width' => "flex: 1") ) ?>
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [										
										<?php write_combo_std('CCTSIV', 'Tipo scad. iva', $r['CCTSIV'], acs_ar_to_select_json($main_module->find_TA_std('ANC03'), ''), array('flex_width' => "flex: 2") ) ?>
									]
								}
						]
					}	           
            
            
					, {
						xtype: 'fieldset', title: 'Coordinate bancarie',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'des_banca',
											fieldLabel: 'Banca', labelWidth: 120, flex: 1,
										    maxLength: 160, 
										    value: <?php echo j(trim(acs_u8e($des_banca))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners= {
									        					afterSelected: function(from_win, form_values){
										        						m_form.findField('CCABI').setValue(form_values.f_abi);
										        						m_form.findField('CCCAB').setValue(form_values.f_cab);
										        						m_form.findField('des_banca').setValue(form_values.des_banca);
										        						from_win.close();
														        		}										    
														    },
															
															
															acs_show_win_std('Ricerca banca', 
																'search_bank.php?fn=open_search', 
																{}, 450, 300, my_listeners, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								}		
								
								, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [								
										{
											name: 'CCABI',
											fieldLabel: 'ABI / CAB / CC',
										    maxLength: 5,
										    labelWidth: 120, width: 180,
										    value: <?php echo j(trim(acs_u8e($r['CCABI']))); ?>							
										}, {
											name: 'CCCAB',
											fieldLabel: '', labelWidth: 0, width: 60,
										    maxLength: 5,
										    value: <?php echo j(trim(acs_u8e($r['CCCAB']))); ?>							
										}, {
											name: 'CCCOCO',
											fieldLabel: '',
										    maxLength: 20, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCCOCO']))); ?>							
										}
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [{
											name: 'CCIBAN',
											fieldLabel: 'Iban',
										    labelWidth: 120, maxLength: 34, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCIBAN']))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/button_black_repeat_dx.png") . " width=16>"); ?>,
										    
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
															Ext.getBody().mask('Loading... ', 'loading').show();
															
															Ext.Ajax.request({
																	timeout: 2400000,
															        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ricalcola_iban',
															        jsonData: {
															        	form_values: m_form.getValues()
															        },	
															        method     : 'POST',
															        waitMsg    : 'Data loading',
															        success : function(result, request){
															        						        
																		Ext.getBody().unmask();
															            var jsonData = Ext.decode(result.responseText);
																		
																		m_form.findField('CCIBAN').setValue(jsonData.iban);
									
															        },
															        failure    : function(result, request){
																		Ext.getBody().unmask();							        
															            Ext.Msg.alert('Message', 'No data to be loaded');
															        }
															    });																
															
															
															
														
															});
											             }
													}										    
										    
										    
										}
									]
								}
						]
					}
					
				
					, {
						xtype: 'fieldset', title: 'Vendita',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('CCREFE', 'Referente', $r['CCREFE'], acs_ar_to_select_json($main_module->find_sys_TA('BREF'), ''), array('flex_width' => "flex: 2") ) ?>,
									]
								},									
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('CCCATR', 'Causale vendita', $r['CCCATR'], acs_ar_to_select_json($main_module->find_sys_TA('VUCT'), ''), array('flex_width' => "flex: 2") ) ?>,
										<?php write_combo_std('CCLIST', 'Listino', $r['CCLIST'], acs_ar_to_select_json($main_module->find_sys_TA('BITL'), ''), array('flex_width' => "flex: 2", 'labelAlign' => 'right') ) ?>
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [										
										{
											name: 'CCSC1',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: 'Sconto comm. 1',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC1']))); ?>							
										}, {
											name: 'CCSC2',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: '2', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC2']))); ?>							
										}, {
											name: 'CCSC3',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: '3', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC3']))); ?>							
										}, {
											name: 'CCSC4',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: '4', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC4']))); ?>							
										}
									]
								}
						]
					}						
					
					
            
					
			],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');	            	
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_cc',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            loc_win.close();
						            //loc_win.fireEvent('afterEditRecord', loc_win);
						            
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_new_form_COMM







function write_new_form_FIDO($p = array(), $request = array()){
	global $conn, $cfg_mod_Gest;

	if ($request['mode'] == 'EDIT'){
		//recupero la riga da modificare
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($request['CCDT'], $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));

		$ret = array();
		$r = db2_fetch_assoc($stmt);

	} else {
		//NUOVA CONDIZIONE COMMERCIALE
		$r = array(	'CCDT' => $request['CCDT'],
				'CCPROG' => $request['CCPROG'],
				'CCSEZI' => $request['CCSEZI'],
				'CCCCON' => $request['CCCCON'],
				'CCDCON' => $request['CCDCON'],
		);
	}

	global $s, $main_module;
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            disabled: false, //true,
            autoScroll: true,
            
            flex: 1,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [
            		{xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($r['CCDT']); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j($r['CCSEZI']); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($r['CCPROG']); ?>},
            		{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j($r['CCCCON']); ?>},
            		{xtype: 'hiddenfield', name: 'CCDCON', value: <?php echo j($r['CCDCON']); ?>},
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							    value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,
							    disabled: true
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    maxLength: 5,
							    value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    disabled: true							
							}
							]
						}            
            
						, <?php write_numberfield_std('CCIMFD', 'Importo fido', trim($r['CCIMFD'])) ?>		
						, <?php write_datefield_std('CCDTVI', 'Data validit&agrave; iniziale', trim($r['CCDTVI'])) ?>
						, <?php write_datefield_std('CCDTVF', 'Data validit&agrave; finale', trim($r['CCDTVF'])) ?>
					
									
            
					
			],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');	            	
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_fido',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            
						            loc_win.fireEvent('afterEditRecord', loc_win);
						            
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_new_form_FIDO




 
?>


