<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));


function options_select_divisioni($mostra_codice = 'N'){
	global $conn;
	global $cfg_mod_DeskUtility, $cfg_mod_DeskAcq, $id_ditta_default;
	$ar = array();
	$sql_filtra_divisioni = '';
	//Verifico se per l'utente attuale ho dei filtri sulla divisione
	global $auth, $m_DeskAcq;
	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_tabelle']} WHERE TADT='{$id_ditta_default}' AND TATAID = 'SCUTD'	AND TAKEY1 = ?";	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($auth->get_user()));	
	$row = db2_fetch_assoc($stmt);
	if ($row != false){
		$sql_filtra_divisioni = " AND SADIV0 IN(" . sql_t_IN(explode(",", $row['TADESC'])) . ")";
	}

	$sql = "SELECT SADIV0, SAAZI0 
			FROM {$cfg_mod_DeskUtility['file_fornitori_esistenti']} AF
			WHERE SADIT0 = '$id_ditta_default'
			 $sql_filtra_divisioni
			GROUP BY SADIV0, SAAZI0";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		if ($mostra_codice == 'Y')
			$text = "[" . $row['SADIV0'] . "] " . $row['SAAZI0'];
			else $text = $row['SAAZI0'];
	 	$ret[] = array( "id" 	=> $row['SADIV0'],
	 			"text" 	=> $text );
	}
	return $ret;
}

if ($_REQUEST['fn'] == 'combo_fornitori'){
	
	$ar = array();
	
	if(strlen($_REQUEST['divisione']) > 0){
		$sql = "SELECT SFCFO0, SFRAG0 FROM {$cfg_mod_DeskUtility['file_fornitori']} AF
		WHERE SFDIT0 = '$id_ditta_default' AND SFDIV0 = '{$_REQUEST['divisione']}'
		ORDER BY SFRAG0";
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
		
			$text = "[" . $row['SFCFO0'] . "] " . $row['SFRAG0'];
				
			$ret[] = array( "id" 	=> $row['SFCFO0'],
							"text" 	=> acs_u8e($text) );
		}

	
	}else{
		$ret = array();
	}
	
	echo acs_je($ret);
	exit;
}

if ($_REQUEST['fn'] == 'combo_magazzini'){
    
    $ar = array();
    
    if(strlen($_REQUEST['divisione']) > 0){
        $sql = "SELECT *
		 		FROM {$cfg_mod_DeskAcq['file_tabelle']}
		 		WHERE TADT='$id_ditta_default' AND TATAID = 'SCMAG' AND TAKEY2 = '{$_REQUEST['divisione']}' ";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        $ret = array();
        while ($row = db2_fetch_assoc($stmt)) {
            
            $text = "[" . trim($row['TAKEY1']) . "] " .trim($row['TADESC']);
            
            $ret[] = array( "id" 	=> trim($row['TAKEY1']),
                "text" 	=> acs_u8e($text) );
        }
        
        
    }else{
        $ret = array();
    }
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_ric_sc'){
	$ret = array();
	$m_params = acs_m_params_json_decode();


	$sh = new SpedHistory($m_DeskAcq);
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> 'SCO_RIC_ROP',
					"vals" => array("RICITI" => $m_params->divisione),
			)
			);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}

if ($_REQUEST['fn'] == 'open_form'){
	$m_params = acs_m_params_json_decode();
	?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
				items: [
					 {
						name: 'f_divisione',
						itemId: 'id_div',
						xtype: 'combo',
						fieldLabel: 'Divisione',
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona -',
				   		allowBlank: false,								
					    anchor: '-15',
					    value: <?php echo j($main_module->setup_scorte_get_divisione()) ?>,
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json(options_select_divisioni('Y'), ''); ?>	
						    ]
						},     listeners: {
                    	change: function(field,newVal) {	
                      		if (!Ext.isEmpty(newVal)){
                            	combo_forn = this.up('form').down('#id_forn');                      		 
                             	combo_forn.store.proxy.extraParams.divisione = newVal;
                            	combo_forn.store.load();
                            	combo_maga = this.up('form').down('#id_magaz');                      		 
                             	combo_maga.store.proxy.extraParams.divisione = newVal;
                            	combo_maga.store.load();
                            	                           
                             }
                             

                    }
                }
					 },{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   			 {
 									xtype: 'fieldset',
 									flex:1,
 					                title: 'Articolo',
 					                defaultType: 'textfield',
 					                layout: 'anchor',
 					              
 					             	items: [
 					             	{
						            xtype: 'textfield',
									name: 'f_cod_art',
									fieldLabel: 'Codice',
									anchor: '-15',
									},{
						            xtype: 'textfield',
									name: 'f_desc_art',
									fieldLabel: 'Descrizione',
									anchor: '-15',	
								    }
 									 ]
 									 }, {
 									xtype: 'fieldset',
 					                title: 'Filtro ricerca',
 					                flex:1,
 					                defaultType: 'textfield',
 					                layout: 'anchor',
 					              
 					             	items: [
 					             	{
									name: 'f_fornitore',
									xtype: 'combo',
									itemId: 'id_forn',
									fieldLabel: 'Fornitore',
									labelWidth: 60,							
									displayField: 'text',
									valueField: 'id',								
									emptyText: '- seleziona -',
									forceSelection: true,
							   		anchor: '-15',
								    store: {
							                autoLoad: true,
											proxy: {
									            type: 'ajax',
									            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=combo_fornitori',
									            
							                	extraParams: {
							    		    		divisione: <?php echo j($main_module->setup_scorte_get_divisione()) ?>,
							    				},				            
									            
									            reader: {
									                type: 'json',
									                root: 'options'
									            }
									        },       
											fields: ['id', 'text'],		             	
							            }
								
					 }, {
									name: 'f_magazzino',
									xtype: 'combo',
									itemId: 'id_magaz',
									fieldLabel: 'Magazzino',
									displayField: 'text',
									valueField: 'id',								
									emptyText: '- seleziona -',
							   		forceSelection: true,								
								    anchor: '-15',
								    labelWidth: 60,
								    store: {
							                autoLoad: true,
											proxy: {
									            type: 'ajax',
									            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=combo_magazzini',
									            
							                	extraParams: {
							    		    		divisione: <?php echo j($main_module->setup_scorte_get_divisione()) ?>,
							    				},				            
									            
									            reader: {
									                type: 'json',
									                root: 'options'
									            }
									        },       
											fields: ['id', 'text'],		             	
							            }
								 }
 									 ]
 									 }
						
						]} 
				/*	  {
						name: 'f_livelli',
						xtype: 'combo',
						fieldLabel: 'Livelli',
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona -',
				   		allowBlank: false,								
					    anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('XUDT'), ''); ?>	
						    ]
						}
					 } 	*/
					 ],
					 
			buttons: [					
				{
		            text: 'Stagionalit&agrave;',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		               var form = this.up('form').getForm();
		               if(form.isValid()){
			               var form_values = form.getValues();
			                acs_show_panel_std('acs_scorte_stagionalita.php?fn=open_tab', 'panel_stato_ordini', {form_values: form_values}); 
			             	this.up('window').close();
			               
	                   }
		                
		            }
		        },{
		            text: 'Setup fornitori',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		             var form = this.up('form').getForm();
		               if(form.isValid()){
			               var form_values = form.getValues();
			               acs_show_panel_std('acs_scorte_setup_fornitori.php?fn=open_tab', 'panel_stato_ordini', {form_values: form_values}); 
			               this.up('window').close();
		               }
		            }
		        }, {
		            text: 'Setup articoli',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		               var form = this.up('form').getForm();
		               if(form.isValid()){
			               var form_values = form.getValues();
			               acs_show_panel_std('acs_scorte_setup_articoli.php?fn=open_tab', 'panel_stato_ordini', {form_values: form_values}); 
			              this.up('window').close();
	                   }
		                
		            }
		        }, {
		            text: 'Avvio ricalcolo scorte',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		            	 var form = this.up('form').getForm();
		            	 if(!form.isValid()){
		            	 	return false;
		            	 }
		            
		                 var div = this.up('form').down('#id_div').getValue();
		      
				         	acs_show_win_std('Ricalcolo scorte', 'acs_submit_job.php?fn=open_form', { 
           						chiave : {RIRGES:'SCO_RIC_ROP', RICITI : div },
           						vals   : {RIRGES:'SCO_RIC_ROP', RICITI : div }
           						} , 650, 250, {}, 'icon-listino');

		                
		            }
		        },{
		            text: 'Setup scorte',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		               var form = this.up('form').getForm();
		               if(form.isValid()){
			               var form_values = form.getValues();
			                acs_show_panel_std('acs_scorte_setup_scorte.php?fn=open_tab', 'panel_stato_ordini', {form_values: form_values}); 
			             	this.up('window').close();
			              
	                   }
		                
		            }
		        }
	        
	        
	        ]  
					
					
	}
	
]}


<?php 
}
