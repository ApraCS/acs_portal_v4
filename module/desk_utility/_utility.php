<?php

function find_ART_like($like_cod, $dt = null, $add_cod_in_des = 'N'){
	global $conn, $libreria_predefinita;
	$ar = array();
	
	$like_cod = strtoupper($like_cod);

	$sql = "SELECT * FROM {$libreria_predefinita}.XX0S2AR0 WHERE ARDT = ? 
            /*AND UPPER(ARART) = ? */
            AND UPPER(ARART) LIKE '%" . strtoupper($like_cod) . "%'
            ORDER BY ARDART";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($dt));

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
			
		$id 	= trim($row['ARART']);
		$text	= trim($row['ARDART']);
		if ($add_cod_in_des == 'B')
			$text = "[{$id}] {$text}";
		if ($add_cod_in_des == 'A')
		$text = "{$text} [{$id}]";
			
		$ret[] = array( "id" 		=> $id,
						"text" 		=> acs_u8e($text),
						"ARDART"	=> acs_u8e(trim($row['ARDART']))
		);
	}

	return $ret;
}


 function find_PUVN_std($tab, $dt = null, $add_cod_in_des = 'N', $field_cod = 'TANR', $tab_id = 'PUVN'){
 		global $conn, $libreria_predefinita;
		$ar = array(); 
		
		$sql = "SELECT * FROM {$libreria_predefinita}.XX0S2TA0 WHERE TADT = ? AND TAID=? AND TACOR2=? ORDER BY TADESC";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt, array($dt, $tab_id,  $tab));	
		
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
			
			 $id 	= trim($row[$field_cod]);
			 $text	= trim($row['TADESC']);
			 if ($add_cod_in_des == 'B')
			 	$text = "[{$id}] {$text}"; 
			 if ($add_cod_in_des == 'A')
			 	$text = "{$text} [{$id}]";			 
			
			 $ret[] = array("id" 		=> $id,
		 		        	"text" 		=> $text			 				 				 		
		  );	
		}		

	return $ret;	
 }
?>