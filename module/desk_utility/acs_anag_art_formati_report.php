<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          
                  		{
							xtype: 'radiogroup',
							fieldLabel: '',
							labelWidth: 110,
							labelAlign: 'left',
						   	allowBlank: true,
						   	vertical: true,
						   	columns: 1,
						   	anchor: '-15',
						  //  margin: "0 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_formati' 
		                          , boxLabel: 'Solo articoli con formati'
		                          , inputValue: 'F'
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_formati' 
		                          , boxLabel: 'Articoli senza formati'
		                          , inputValue: 'A'
		                         
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_formati' 
		                          , boxLabel: 'Tutto'
		                          , inputValue: ''
		                          
		                        }]
						}
	            ],
	            
				buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	
if ($_REQUEST['fn'] == 'open_report'){

    $users = new Users;
    $ar_users = $users->find_all();
    
    
    foreach ($ar_users as $ku=>$u){
        $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }
    
    $ar_email_json = acs_je($ar_email_to);



?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$formati =	strtr($formati_per_sottogruppo, array('\"' => '"'));
$formati = json_decode($formati);

$form_values_f =	strtr($_REQUEST['filter'], array('\"' => '"'));
$form_values_f = json_decode($form_values_f);

foreach($formati as $k => $v){
    
    foreach($v as $k1 =>$v1){
        $ar_bp[$k] = $v1;
    }
     $sg[] = $k;
    
}



$sql_where = "";

if(isset($form_values_f->f_tipo_parte) && $form_values_f->f_tipo_parte !='')
    $sql_where.= sql_where_by_combo_value('ARTPAR', $form_values_f->f_tipo_parte);
if(isset($form_values_f->f_classe) && $form_values_f->f_classe !='')
    $sql_where.= sql_where_by_combo_value('ARCLME', $form_values_f->f_classe);
if(isset($form_values_f->f_gruppo) && $form_values_f->f_gruppo !='')
    $sql_where.= sql_where_by_combo_value('ARGRME', $form_values_f->f_gruppo);
if(isset($form_values_f->f_sottogruppo) && $form_values_f->f_sottogruppo !='')
    $sql_where.= sql_where_by_combo_value('ARSGME', $form_values_f->f_sottogruppo);
if(isset($form_values_f->f_pianificazione) && $form_values_f->f_pianificazione !='')
    $sql_where.= sql_where_by_combo_value('ARTAB1', $form_values_f->f_pianificazione);
if(isset($form_values_f->f_fornitore) && $form_values_f->f_fornitore !='')
    $sql_where.= sql_where_by_combo_value('ARFOR1', $form_values_f->f_fornitore);
 
if (strlen($form_values_f->f_data_dal) > 0)
    $sql_where.= " AND ARDTGE >= $form_values_f->f_data_dal ";
if (strlen($form_values_f->f_data_al) > 0)
    $sql_where.= " AND ARDTGE <= $form_values_f->f_data_al ";
  
    if($form_values_f->f_cod_art != ''){
        $sql_where.= " AND UPPER(ARART) = '{$form_values_f->f_cod_art}'";
        
    }else{
        $text_a = '';
            for($i = 1; $i <= 15; $i++){
             $filtro = 'f_'.$i;
             if($form_values_f->$filtro == '')
                 $form_values_f->$filtro = '_';
                 $text_a .= $form_values_f->$filtro;
            }
             
            $sql_where.= " AND UPPER(ARART) LIKE '" . strtoupper($text_a) . "' ";
    }
         
$ar_esau = array();
if ($form_values_f->f_esau == 'E')
    array_push($ar_esau, $form_values_f->f_esau);
if ($form_values_f->f_avv == 'A')
    array_push($ar_esau, $form_values_f->f_avv);
if ($form_values_f->f_corso == 'C')
    array_push($ar_esau, $form_values_f->f_corso);
if (count($ar_esau) > 0)
    $sql_where.= sql_where_by_combo_value('ARESAU', $ar_esau);
                         
if (strlen($form_values_f->f_desc_art) > 0)
    $sql_where.= " AND UPPER(ARDART) LIKE '%" . strtoupper($form_values_f->f_desc_art) . "%' ";

if ($form_values_f->f_sospeso == 'S')
    $sql_where.= " AND ARSOSP = '$form_values_f->f_sospeso'";

if($form_values_f->f_sfg == '1')
    $sql_where .= " AND AXSFON = '1'";
elseif($form_values_f->f_sfg == '2')
    $sql_where .= " AND AXSFON = '2'";
elseif($form_values_f->f_sfg == '3')
    $sql_where .= " AND AXSFON = '3'";
elseif($form_values_f->f_sfg == 'N')
    $sql_where .= " AND (AXSFON IS NULL  OR AXSFON = '')";
        
if($form_values_f->f_sfp == '1')
    $sql_where .= " AND AUSFAR = '1'";
elseif($form_values_f->f_sfp == '2')
    $sql_where .= " AND AUSFAR = '2'";
elseif($form_values_f->f_sfp == '3')
    $sql_where .= " AND AUSFAR = '3'";
elseif($form_values_f->f_sfp == 'N')
    $sql_where .= " AND (AUSFAR IS NULL  OR AUSFAR = '')";

$sql_where.= " AND ARSGME  IN (" . sql_t_IN($sg) . ")"; 
         
   if($form_values->f_formati == 'F'){
       //solo articoli con formati
       $select = "AF.*, ";
       $join = "INNER JOIN {$cfg_mod_DeskArt['file_anag_art_formati']} AF  
                 ON AR.ARDT = AF.AFDT AND AR.ARART = AF.AFCMAT";
   }elseif($form_values->f_formati == 'A'){
       //articoli senza formati
       $select = "AF.AFDT";
       $join = "LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art_formati']} AF
       ON AR.ARDT = AF.AFDT AND AR.ARART = AF.AFCMAT";
       $sql_where .= " AND AF.AFDT IS NULL";
      
   }else{
       //tutto
       $select = "AF.*, ";
       $join = "LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art_formati']} AF
       ON AR.ARDT = AF.AFDT AND AR.ARART = AF.AFCMAT";
       
   }
 
    $utente = trim($auth->get_user());
   
    $sql = "SELECT {$select} ARART, ARDART, ARSGME, ARUMCO, ARUMTE, ARUMAL,
      ARDIM1, ARDIM2, ARDIM3, ARMOCO, AXSFON
    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
    LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art']} AX
	   ON AX.AXDT = AR.ARDT AND AR.ARART = AX.AXART
    LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
	   ON AU.AUDT = AR.ARDT AND AU.AUART = AR.ARART AND AU.AUUTEN = '{$utente}'
    {$join}
    WHERE ARDT = '{$id_ditta_default}' {$sql_where}
    ORDER BY ARART
    FETCH FIRST 2000 ROWS ONLY";
 
 
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr = $row;
   
        $ar[] = $nr;
    }
    
    
echo "<div id='my_content'>";
echo "<div class=header_page>";
echo "<H2>Anagrafica articoli - Formati</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";
echo "<tr class='liv_data'>";

echo "
	<th>Articolo</th>
	<th>Descrizione</th>
    <th class=number>L</th>
    <th class=number>H</th>
    <th class=number>S</th>
    <th>U.M.</th>
    <th>Parametro</th>
    <th>Valore</th>
    <th>Dimensioni</th>";


echo "</tr>";  


if(is_array($ar))
 foreach ($ar as $kar => $r){
    
 
    if(trim($r['ARUMCO']) == trim($r['ARUMTE']) && trim($r['ARUMCO']) == trim($r['ARUMAL'])){
        $um = $r['ARUMTE'];
    }elseif((trim($r['ARUMAL']) == trim($r['ARUMTE']) && trim($r['ARUMAL']) != trim($r['ARUMCO'])) ||
        (trim($r['ARUMAL']) == trim($r['ARUMCO']) && trim($r['ARUMAL']) != trim($r['ARUMTE']))){
            $um = $r['ARUMTE']. " - " . trim($r['ARUMCO']);
    }else{
        $um = trim($r['ARUMTE']) . " - " . trim($r['ARUMCO']) . " - " . trim($r['ARUMAL']);
    }
    
    echo "<tr>
         <td valign = top>".$r['ARART']."</td>
         <td valign = top>".$r['ARDART']."</td>";
    
    if($r['ARDIM1'] > 0)
        echo "<td class=number valign='top'>".n_auto($r['ARDIM1'])."</td>";
    else
        echo"<td>&nbsp;</td>";
    if($r['ARDIM2'] > 0)
        echo "<td class=number valign='top'>".n_auto($r['ARDIM2'])."</td>";
     else
        echo"<td>&nbsp;</td>";
     if($r['ARDIM3'] > 0)
        echo "<td class=number valign='top'>".n_auto($r['ARDIM3'])."</td>";
     else
        echo"<td>&nbsp;</td>";
                            
     echo"<td valign='top'>".$um."</td>";
     
     
   $text = "";  
   $value = "";
   $dim = "";


       if($ar_bp[trim($r['ARSGME'])] == 'pannello'){
         if(trim($r['AFCDEC']) != ''){
             $text =  "Codice decoro";
             $ta0 = $desk_art->find_TA_std('DECAF', trim($r['AFCDEC']));
             $value = "[".trim($r['AFCDEC'])."] ".$ta0[0]['text'];
         }
       
        
         if(trim($r['AFVENA']) != ''){
             $text .=  "<br>Venatura";
             $ta0 = $desk_art->find_TA_std('ANFVE', trim($r['AFVENA']));
             $value .= "<br>[".trim($r['AFVENA'])."] ".$ta0[0]['text'];
         }
         
         if(trim($r['AFTMAT']) != ''){
             $text .=  "<br>Tipo materiale";
             $ta0 = $desk_art->find_TA_std('ANFTM', trim($r['AFTMAT']));
             $value .= "<br>[".trim($r['AFTMAT'])."] ".$ta0[0]['text'];
         }
         
         if(trim($r['AFTPIL']) != ''){
             $text .=  "<br>Tipo pila";
             $ta0 = $desk_art->find_TA_std('ANFTP', trim($r['AFTPIL']));
             $value .= "<br>[".trim($r['AFTPIL'])."] ".$ta0[0]['text'];
         }
         
        if(trim($r['AFFUTI']) != ''){
            $text .=  "<br>Frequenza utilizzo";
            $ta0 = $desk_art->find_TA_std('ANFFU', trim($r['AFFUTI']));
            $value .= "<br>[".trim($r['AFFUTI'])."] ".$ta0[0]['text'];
        }
        
        if(trim($r['AFDIMM']) != ''){
            $text .=  "<br>Da immagazzinare";
            $ta0 = $desk_art->find_TA_std('ANFIM', trim($r['AFDIMM']));
            $value .= "<br>[".trim($r['AFDIMM'])."] ".$ta0[0]['text'];
        }
        
        if(trim($r['AFPELL']) != ''){
            $text .=  "<br>Pellicola";
            $ta0 = $desk_art->find_TA_std('ANFPL', trim($r['AFPELL']));
            $value .= "<br>[".trim($r['AFPELL'])."] ".$ta0[0]['text'];
        }

       if(trim($r['AFBCOL']) != ''){
            $text .=  "<br>Colla";
            $ta0 = $desk_art->find_TA_std('ANFCL', trim($r['AFBCOL']));
            $value .= "<br>[".trim($r['AFBCOL'])."] ".$ta0[0]['text'];
        }
           
       if(trim($r['AFTSUP']) != ''){
           $text .=  "<br>Tipo superficie";
           $ta0 = $desk_art->find_TA_std('ANFTS', trim($r['AFTSUP']));
           $value .= "<br>[".trim($r['AFTSUP'])."] ".$ta0[0]['text'];
       }
       
       if(trim($r['AFBTON']) != ''){
           $text .=  "<br>Tonalit&agrave;";
           $ta0 = $desk_art->find_TA_std('ANFTN', trim($r['AFBTON']));
           $value .= "<br>[".trim($r['AFBTON'])."] ".$ta0[0]['text'];
       }
       
}else{
    
    if(trim($r['AFBSPE']) != ''){
        $text .=  "Spessore";
        $ta0 = $desk_art->find_TA_std('ANFSP', trim($r['AFBSPE']));
        $value .= "[".trim($r['AFBSPE'])."] ".$ta0[0]['text'];
    }
            
    if(trim($r['AFBRET']) != ''){
        $text .=  "<br>Rettifica";
        $ta0 = $desk_art->find_TA_std('ANFRT', trim($r['AFBRET']));
        $value .= "<br>[".trim($r['AFBRET'])."] ".$ta0[0]['text'];
    }
                    
                 
    if(trim($r['AFBTIP']) != ''){
        $text .=  "<br>Tipo";
        $ta0 = $desk_art->find_TA_std('ANFTI', trim($r['AFBTIP']));
        $value .= "<br>[".trim($r['AFBTIP'])."] ".$ta0[0]['text'];
    }
                                    
    if(trim($r['AFBPEL']) != ''){
        $text .=  "<br>Pellicola";
        $ta0 = $desk_art->find_TA_std('ANFBP', trim($r['AFBPEL']));
        $value .= "<br>[".trim($r['AFBPEL'])."] ".$ta0[0]['text'];
    }
    
    }     
    
    if(trim($r['ARMOCO']) != ''){
        $ta_sys = find_TA_sys('PUMC', trim($r['ARMOCO']));
        if($text == ""){
            $text .=  "Modello commerciale";
            $value .= "[".trim($r['ARMOCO'])."] ".$ta_sys[0]['text'];
        }else{
           
            $text .=  "<br>Modello commerciale";
            $value .= "<br>[".trim($r['ARMOCO'])."] ".$ta_sys[0]['text'];
        }
    }


    echo "<td>".$text."</td>";
    echo "<td>".$value."</td>";
    
    if(trim($r['AFQSTO']) != '' && $ar_bp[trim($r['ARSGME'])] == 'pannello'){
        $dim .=  "Quantit&agrave; di stock: " .trim($r['AFQSTO']) ;
        
    }
   
    if(trim($r['AFLUNG']) != 0){
         $dim .=  "<br> Materiale: ".n_auto(trim($r['AFLUNG']))."x".n_auto(trim($r['AFLARG']))."x".n_auto(trim($r['AFSPES']))." - Peso: ".n_auto(trim($r['AFPSPE']));
    }
    
    
    if(trim($r['AFMDES']) != 0){
        $dim .= "<br>Margini: ".n_auto(trim($r['AFMDES']))."x".n_auto(trim($r['AFMSOP']))."x".n_auto(trim($r['AFMSIN']))."x".n_auto(trim($r['AFMSOT']))." - Resto: ".n_auto(trim($r['AFRMLU']))."x".n_auto(trim($r['AFRMLA']));
    }
 
    if($dim == '')
        echo "<td valign = top><i> Agg. al ".print_date($r['AFDTUM']).", ".print_ora($r['AFORUM'])." [".trim($r['AFUSUM'])."]</i></td>";
    else
        echo "<td valign = top>".$dim."<br><i> Agg. al ".print_date($r['AFDTUM']).", ".print_ora($r['AFORUM'])." [".trim($r['AFUSUM'])."]</i></td>";
    
    echo "</tr>";

            
}




?>
</div>
</body>
</html>	

<?php 
exit;
}
