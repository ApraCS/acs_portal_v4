<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $list_art = $m_params->open_request->list_art;
    $ar_articoli = explode('_', $list_art);
   
    
    $sql_where.= sql_where_by_combo_value('ARART', $ar_articoli);
    
    $sql = "SELECT ARART, ARDART
    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
    WHERE ARDT = '{$id_ditta_default}' {$sql_where}";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while($row = db2_fetch_assoc($stmt)){
        $ar[] = $row;
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_win'){
?>

{"success":true, "items": [
			 {
			xtype: 'grid',
			loadMask: true,	
			autoScroll: true,
			store: {
						xtype: 'store',
						autoLoad: true,
			             	proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>,
									},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['ARART', 'ARDART']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Codice',
	                dataIndex: 'ARART',
	                width: 70,
	            },
				{
	                header   : 'Descrizione',
	                dataIndex: 'ARDART',
	                flex : 1
	            }
	         ] 	
	         
	         	
		} <!-- end grid -->
]}
<?php 

exit;
}