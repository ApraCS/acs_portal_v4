<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    //$nr['descrizione3'] = substr($row['TAREST'], 0, 30);
    //$nr['causale'] = substr($row['TAREST'], 30, 4);
    $nr['data_reg'] = substr($row['TAREST'], 38, 4).substr($row['TAREST'], 36, 2).substr($row['TAREST'], 34, 2);
   // $nr['trattamento'] = substr($row['TAREST'], 42, 4);
   // $nr['cls_anz'] = substr($row['TAREST'], 46, 4);
    $nr['svalutazione'] = substr($row['TAREST'], 50, 3).",".substr($row['TAREST'], 53, 2);
   // $nr['trattamento2'] = substr($row['TAREST'], 55, 4);
    $nr['svalutazione2'] = substr($row['TAREST'], 59, 3).",".substr($row['TAREST'], 62, 2);

    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'data_reg', 'svalutazione', 'svalutazione2');
    
    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start();
    
    ?>
    
     	   {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            },{
            header   : 'Data reg.',
            dataIndex: 'data_reg',
            flex : 1,
            renderer: date_from_AS
            },
            {
            header   : 'Svalutazione%',
            dataIndex: 'svalutazione',
            flex : 1
            },
            {
            header   : 'Svalutazione2%',
            dataIndex: 'svalutazione2',
            flex : 1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $descrizione =  trim($m_params->row->tadesc);
    $data_reg =  trim($m_params->row->data_reg);
    $svalutazione =  trim($m_params->row->svalutazione);
    $svalutazione2 =  trim($m_params->row->svalutazione2);
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "data_reg" => trim($row->data_reg),
        "svalutazione" => trim($row->svalutazione),
        "svalutazione2" => trim($row->svalutazione2),
        
    );
 
    return $ar_values;
    
}

function get_values_from_array($row){
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "data_reg" => trim($row['data_reg']),
        "svalutazione" => trim($row['svalutazione']),
        "svalutazione2" => trim($row['svalutazione2']),
    );
   return $ar_values;
    
}

function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		labelWidth : 110,
		anchor: '-15',	
		maxLength : 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
    

	{xtype: 'textfield',
	name: 'descrizione',
	fieldLabel: 'Descrizione',
	maxLength : 30,
	labelWidth : 110,
	anchor: '-15',	
	value:  <?php echo j($ar_values['descrizione']); ?>
	}, {
   xtype: 'datefield'
   , startDay: 1 //lun.
   , fieldLabel: 'Data reg.'
   , labelWidth :110
   , name: 'data_reg'
   , format: 'd/m/Y'
   , submitFormat: 'dmY'
   , allowBlank: true
   , anchor: '-15'
   ,value: '<?php echo print_date($ar_values['data_reg'], "%d/%m/%Y"); ?>'
   , listeners: {
       invalid: function (field, msg) {
       Ext.Msg.alert('', msg);}
		}
	},{xtype: 'numberfield',
	name: 'svalutazione',
	fieldLabel: 'Svalutazione',
	labelWidth : 110,
	anchor: '-15',
	maxValue : 999.99,
	decimalPrecision : 2,
	value:  <?php echo j($ar_values['svalutazione']); ?>,
	hideTrigger : true
	}, {xtype: 'numberfield',
	name: 'svalutazione2',
	fieldLabel: 'Svalutazione2',
	labelWidth : 110,
	anchor: '-15',
	maxValue : 999.99,
	decimalPrecision : 2,
	value:  <?php echo j($ar_values['svalutazione2']); ?>,
	hideTrigger : true
	}
    
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-30s", " ");
    $ar_ins['TAREST'] .= sprintf("%-4s", " ");
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_reg);
    $ar_ins['TAREST'] .= sprintf("%-4s", " ");
    $ar_ins['TAREST'] .= sprintf("%-4s", " ");
    $ar_ins['TAREST'] .= sprintf("%-5s", str_replace(",", "", $form_values->svalutazione));
    $ar_ins['TAREST'] .= sprintf("%-4s", " ");
    $ar_ins['TAREST'] .= sprintf("%-5s", str_replace(",", "", $form_values->svalutazione2));
    
    $ar_ins['TANR']   .= sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] .= $form_values->descrizione;
    
    return $ar_ins;
    
}