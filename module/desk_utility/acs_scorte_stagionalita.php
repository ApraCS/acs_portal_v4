<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));

$s = new Spedizioni(array('no_verify' => 'Y'));

ini_set('max_execution_time', 90000);


if ($_REQUEST['fn'] == 'data_chart'){

	$m_params = acs_m_params_json_decode();

	$sql = "SELECT SSMES0, SSFAT0
	FROM {$cfg_mod_DeskUtility['file_stag_mese']} MF
	WHERE SSDIT0 = '$id_ditta_default' AND SSDIV0 = '{$m_params->open_request->form_values->f_divisione}'
    ORDER BY SSMES0";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$nr = array();
	while($row = db2_fetch_assoc($stmt)){
		
		setlocale(LC_TIME, 'it_IT');
		$monthNum = sprintf("%02s", $row['SSMES0']);
		$nr['mesi'] = ucfirst(acs_u8e(strftime("%B", strtotime("2017{$monthNum}01"))));
		$nr['totale'] = trim($row['SSFAT0']);
		
		$ar_records[] = $nr;
	}

	
	echo acs_je($ar_records);
	exit;
}

if ($_REQUEST['fn'] == 'grid_stag_mese'){

	$m_params = acs_m_params_json_decode();

	$sql = "SELECT SSNRA0, SSMES0, SSFAT0, SSSTM0
	FROM {$cfg_mod_DeskUtility['file_stag_mese']} MF
	WHERE SSDIT0 = '$id_ditta_default' AND SSDIV0 = '{$m_params->open_request->form_values->f_divisione}'
	ORDER BY SSMES0";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['SSNRA0'] = trim($row['SSNRA0']);
		setlocale(LC_TIME, 'it_IT');
		$monthNum = sprintf("%02s", $row['SSMES0']);
		$nr['SSMES0'] = ucfirst(acs_u8e(strftime("%B", strtotime("2017{$monthNum}01")))); 
		$nr['SSFAT0'] = trim($row['SSFAT0']);
		$nr['SSSTM0'] = trim($row['SSSTM0']);
		$tot_fatt +=$nr['SSFAT0'];
		$tot_fatt_perc +=$nr['SSSTM0'];
		$ar[] = $nr;
			}
	
		$anni = $nr['SSNRA0'];
	$nr = array();
	
	$nr['SSMES0'] =  "<b>Totale fatturato</b>";
	$nr['SSFAT0'] = $tot_fatt;
	$nr['SSSTM0'] = $tot_fatt_perc/12;
	$ar[] = $nr;
	
	$nr['SSMES0'] =  "<b>Media mensile</b>";
	$nr['SSFAT0'] = $tot_fatt/($anni*12);
	$nr['SSSTM0'] = $tot_fatt_perc/($anni*12);
	$ar[] = $nr;

	echo acs_je($ar);
	exit;
}


if ($_REQUEST['fn'] == 'open_tab'){

	$m_params = acs_m_params_json_decode();
	
	$divisione = $m_params->form_values->f_divisione;
	$main_module->setup_scorte_save_divisione($divisione);
	
	?>
	
	{
		success:true,
		items: [{
			xtype: 'panel',
			title: 'Stagionalit&agrave; [' + '<?php echo $divisione?>' + ']',
			<?php echo make_tab_closable(); ?>,
			 bbar: ['->', {
							xtype: 'button',
				            text: 'Ricalcola',
				            iconCls: 'icon-button_blue_play-32', 
				            scale: 'large',
				            handler: function() {
				            	var panel = this.up('panel');
				           
				           		acs_show_win_std('Ricalcolo stagionalit&agrave;', 'acs_submit_job.php?fn=open_form', { 
				           						chiave : {RIRGES:'SCO_RIC_STAG', RICITI :  <?php echo j($m_params->form_values->f_divisione) ?> },
				           						vals   : {RIRGES:'SCO_RIC_STAG', RICITI :  <?php echo j($m_params->form_values->f_divisione) ?> }
				           						} , 650, 250, {}, 'icon-listino');
				           
				      
				            }
				        }], 
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
			
		items: [  
		
		{
			xtype: 'grid',
			flex:0.5,
			loadMask: true,	
	      	store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_stag_mese', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							}, 
		        			fields: ['SSNRA0', 'SSMES0', 'SSFAT0', 'SSSTM0']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Mese',
	                dataIndex: 'SSMES0',
	                flex: 1
	                },{
	                header   : 'Fatturato',
	                dataIndex: 'SSFAT0',
	                flex: 1,
	                renderer: floatRenderer2,
	                align: 'right',
	                },{
	                header   : 'Stagionalit&agrave;',
	                dataIndex: 'SSSTM0',
	                flex: 1,
	                renderer: floatRenderer0N,
	                align: 'right',
	                
	                renderer: function(value, metaData, record, row, col, store, gridView){
				       return floatRenderer2(value) + '%';
				                }
	                }
	         ], 
	         
	         listeners: {
	         
	         		afterrender: function (comp) {
	         	
				
				comp.store.on('load', function(store, records, options) {
						var anni = records[0].data.SSNRA0;
							  comp.columns[1].setText('Fatturato ultimi '+anni+ ' anni');
							}, comp);
								
	 			}
	         
			
				}
		
		
		}//grid
		
		,{
		xtype: 'chart',
        style: 'background:#fff',
       	width: 600,
        height: 600,
        animate: true,
        store: {
					xtype: 'store',
					autoLoad:true,
					editable: false,
                    autoDestroy: true, 
			 		proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=data_chart', 
								method: 'POST',								
								type: 'ajax',
							
								 extraParams: {
                                   open_request: <?php echo acs_je($m_params) ?>
                            
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData,

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: ['totale', 'mesi'],
		        								
									
			},
        shadow: true,
        theme: 'Category1',
        /*legend: {
            position: 'right'
        },*/
        axes: [{
            type: 'Numeric',
            minimum: 0,
            position: 'left',
            fields: ['totale'],
            title: 'Fatturato',
            minorTickSteps: 1,
            grid: {
                odd: {
                    opacity: 1,
                    fill: '#ddd',
                    stroke: '#bbb',
                    'stroke-width': 0.5
                }
            }
        }, {
            type: 'Category',
            position: 'bottom',
            fields: ['mesi'],
            title: 'Mesi anno'
        }],
            series: [{
        type: 'column',
        axis: 'left',
        highlight: true,
        tips: {
          trackMouse: true,
          width: 140,
          height: 28,
          renderer: function(storeItem, item) {
            this.setTitle(storeItem.get('mesi') + ': ' + storeItem.get('totale'));
          }
        },
        label: {
          display: 'insideEnd',
            field: 'totale',
            renderer: Ext.util.Format.numberRenderer('0'),
            orientation: 'vertical',
            color: '#333',
            'text-anchor': 'middle'
        },
        xField: 'mesi',
        yField: ['totale']
    }]
    }//chart
		]}
	 ]
	}
<?php exit; 
}