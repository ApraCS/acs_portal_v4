<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility(array('abilita_su_modulo' => 'DESK_ACQ'));

$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

$m_table_config = array(
    
    'FILLER' => array(
        
        'sigla' => array(
            "start" => 3,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'LIFLG2' => array(
            "start" => 4,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'f_imp' => array(
            "start" => 5,
            "len"   => 1,
            "riempi_con" => ""
        )
    )
);


function genera_filler($m_table_config, $values){
    
    $value_attuale = $values->filler;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-50s", "");
        
        //$new_value = "";
        foreach($m_table_config['FILLER'] as $k => $v){
            if(isset($values->$k)){
                $chars = $values->$k;
                $len = "%-{$v['len']}s";
                $chars = substr(sprintf($len, $chars), 0, $v['len']);
                $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                $value_attuale = $new_value;
                
            }
            
        }
        
        
        return $new_value;
}


// ******************************************************************************************
// EXE SAVE LISTINO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_listino'){
    ini_set('max_execution_time', 3000);
   
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
    
    $ar_upd['LIUSUM'] 	= SV2_db_user($auth->get_user());
    $ar_upd['LIDTUM'] 	= oggi_AS_date();
    
    //if($form_values->LITPLI == 'A'){
    if($m_params->tipo_listino == 'A'){
        if(strlen($form_values->LICCON) > 0){
            $ar_ins['LICCON'] 	= $form_values->LICCON;
            $fornitore = $form_values->LICCON;
        }else
            $fornitore = 0;
    }else
        $fornitore = 0;
        
        if(strlen($form_values->listino) > 0)
            $ar_upd['LILIST'] 	= $form_values->listino;
            
            if(strlen($form_values->valuta) > 0)
                $ar_upd['LIVALU'] 	= $form_values->valuta;
                $ar_upd['LIPRZ'] 	= sql_f($form_values->prezzo);
               
               
                if(is_null($form_values->LIVAR1)){
                    $ar_upd['LIVAR1'] 	= "";
                    $var1 = "";
                }else{
                    $ar_upd['LIVAR1'] 	= $form_values->LIVAR1;
                    $var1 = $form_values->LIVAR1;
                }
                if(is_null($form_values->LIVAR2)){
                    $ar_upd['LIVAR2'] 	= "";
                    $var2 = "";
                }else{
                    $ar_upd['LIVAR2'] 	= $form_values->LIVAR2;
                    $var2 = $form_values->LIVAR2;
                }
                if(is_null($form_values->LIVAR3)){
                    $ar_upd['LIVAR3'] 	= "";
                    $var3 = "";
                }else{
                    $ar_upd['LIVAR3'] 	= $form_values->LIVAR3;
                    $var3 = $form_values->LIVAR3;
                }
                
                
                
                            if(strlen($form_values->data_val_ini_df) > 0)
                                $ar_upd['LIDTDE'] 	= $form_values->data_val_ini_df;
                                if(strlen($form_values->data_val_fin_df) > 0)
                                    $ar_upd['LIDTVA'] 	= $form_values->data_val_fin_df;
                                    $ar_upd['LIPRZP'] 	= sql_f($form_values->LIPRZP);
                                    $ar_upd['LIPRZF'] 	= sql_f($form_values->LIPRZF);
                                    if(strlen($form_values->decorr) > 0)
                                        $ar_upd['LIDTDP'] 	= $form_values->decorr;
                                        
                                        
                                        if($cfg_mod_DeskArt['filler_listini'] != ''){
                                            $filler = genera_filler($m_table_config, $form_values);
                                            $ar_upd['LIFIL1'] 	= $filler;
                                        }else{
                                            if(strlen($form_values->LIFLG2) > 0)
                                                $ar_upd['LIFLG2'] 	= $form_values->LIFLG2;
                                            if(strlen($form_values->f_imp) > 0)
                                                $ar_upd['LIFLG3'] 	= $form_values->f_imp;
                                            $ar_upd['LIFLG1'] 	= $form_values->sigla;
                                        }
                                        
                                        
                                        
                                        $ar_upd['LISC1'] 	= sql_f($form_values->LISC1);
                                        $ar_upd['LISC2'] 	= sql_f($form_values->LISC2);
                                        $ar_upd['LISC3'] 	= sql_f($form_values->LISC3);
                                        $ar_upd['LISC4'] 	= sql_f($form_values->LISC4);
                                        $ar_upd['LIMAGG'] 	= sql_f($form_values->LIMAGG);
                                        $ar_upd['LINOTE'] 	= $form_values->LINOTE;
                                        if(strlen($form_values->u_m) > 0)
                                            $ar_upd['LIUM'] 	= $form_values->u_m;
                                            
                                            $ar_upd['LIMF'] 	= sql_f($form_values->min_fatt);
                                            
                                            $sql = "UPDATE {$cfg_mod_DeskUtility['file_listini']} LI
                                            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                                            WHERE RRN(LI) = '{$form_values->rrn}' ";
                                            
                                            /* print_r($ar_upd);
                                             print_r($sql);
                                             exit;
                                             */
                                            
                                            $stmt = db2_prepare($conn, $sql);
                                            echo db2_stmt_errormsg();
                                            $result = db2_execute($stmt, $ar_upd);
                                            echo db2_stmt_errormsg($stmt);
                                            
                                            
                                            $sh = new SpedHistory($deskArt);
                                            $sh->crea(
                                                'pers',
                                                array(
                                                    "messaggio"	=> 'LIST_AZZ_SC',
                                                    "vals" => array(
                                                        "RICDNEW" => trim($form_values->LIART),
                                                        "RIFG02" => sprintf("%-1s", $form_values->azzera),
                                                        "RIFG01" => $form_values->LITPLI,
                                                        "RIFOR1" => $fornitore,
                                                        "RICLME" => $form_values->listino,
                                                        "RIGRME" => $form_values->LIZONA,
                                                        "RIRCOL" => $form_values->valuta,
                                                        "RISGME" => $var1,
                                                        "RICLFI" => $var2,
                                                        "RIGRFI" => $var3)
                                                )
                                                );
                                            
                                            
                                            $ret = array();
                                            $ret['success'] = true;
                                            echo acs_je($ret);
                                            exit;
}
// ******************************************************************************************
// EXE AGGIUNGI LISTINO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi_listino'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_ins = array();
    $ar_ins['LIUSUM'] 	= SV2_db_user($auth->get_user());
    $ar_ins['LIDTUM'] 	= oggi_AS_date();
    $ar_ins['LIUSGE'] 	= $auth->get_user();
    $ar_ins['LIDTGE'] 	= oggi_AS_date();
    $ar_ins['LIDT'] 	= $id_ditta_default;
    
    //if($form_values->LITPLI == 'A'){
    if($m_params->tipo_listino == 'A'){
        if(strlen($form_values->LICCON) > 0){
            $ar_ins['LICCON'] 	= $form_values->LICCON;
            $fornitore = $form_values->LICCON;
        }else
            $fornitore = 0;
    }else
        $fornitore = 0;
        
        $ar_ins['LIART'] 	= $m_params->c_art;
        $ar_ins['LILIST'] 	= $form_values->listino;
        $ar_ins['LINOTE'] 	= $form_values->LINOTE;
        if(strlen($form_values->valuta) > 0)
            $ar_ins['LIVALU'] 	= $form_values->valuta;
            $ar_ins['LIPRZ'] 	= sql_f($form_values->prezzo);
            
            if(is_null($form_values->LIVAR1)){
                $ar_ins['LIVAR1'] 	= "";
                $var1 = "";
            }else{
                $ar_ins['LIVAR1'] 	= $form_values->LIVAR1;
                $var1 = $form_values->LIVAR1;
            }
            if(is_null($form_values->LIVAR2)){
                $ar_ins['LIVAR2'] 	= "";
                $var2 = "";
            }else{
                $ar_ins['LIVAR2'] 	= $form_values->LIVAR2;
                $var2 = $form_values->LIVAR2;
            }
            if(is_null($form_values->LIVAR3)){
                $ar_ins['LIVAR3'] 	= "";
                $var3 = "";
            }else{
                $ar_ins['LIVAR3'] 	= $form_values->LIVAR3;
                $var3 = $form_values->LIVAR3;
            }
                        
                        if(strlen($form_values->data_val_ini_df) > 0)
                            $ar_ins['LIDTDE'] 	= $form_values->data_val_ini_df;
                            if(strlen($form_values->data_val_fin_df) > 0)
                                $ar_ins['LIDTVA'] 	= $form_values->data_val_fin_df;
                                $ar_ins['LITPLI'] 	= $m_params->tipo_listino;
                                $ar_ins['LIPRZP'] 	= sql_f($form_values->LIPRZP);
                                $ar_ins['LIPRZF'] 	= sql_f($form_values->LIPRZF);
                                if(strlen($form_values->decorr) > 0)
                                    $ar_ins['LIDTDP'] 	= $form_values->decorr;
                                    
                                    if($cfg_mod_DeskArt['filler_listini'] != ''){
                                        $filler = genera_filler($m_table_config, $form_values);
                                        $ar_ins['LIFIL1'] 	= $filler;
                                    }else{
                                        if(strlen($form_values->LIFLG2) > 0)
                                            $ar_ins['LIFLG2'] 	= $form_values->LIFLG2;
                                        if(strlen($form_values->f_imp) > 0)
                                            $ar_ins['LIFLG3'] 	= $form_values->f_imp;
                                        $ar_ins['LIFLG1'] 	= $form_values->sigla;
                                    }
                                    
                                    $ar_ins['LISC1'] 	= sql_f($form_values->LISC1);
                                    $ar_ins['LISC2'] 	= sql_f($form_values->LISC2);
                                    $ar_ins['LISC3'] 	= sql_f($form_values->LISC3);
                                    $ar_ins['LISC4'] 	= sql_f($form_values->LISC4);
                                    $ar_ins['LIMAGG'] 	= sql_f($form_values->LIMAGG);
                                    if(strlen($form_values->u_m) > 0)
                                        $ar_ins['LIUM'] 	= $form_values->u_m;
                                        $ar_ins['LIMF'] 	= sql_f($form_values->min_fatt);
                                        
                                        $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_listini']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                                        $stmt = db2_prepare($conn, $sql);
                                        echo db2_stmt_errormsg();
                                        $result = db2_execute($stmt, $ar_ins);
                                        echo db2_stmt_errormsg($stmt);
                                        
                                        
                                        $sh = new SpedHistory($deskArt);
                                        $sh->crea(
                                            'pers',
                                            array(
                                                "messaggio"	=> 'LIST_AZZ_SC',
                                                "vals" => array(
                                                    "RICDNEW" => trim($m_params->c_art),
                                                    "RIFG02" => sprintf("%-1s", $form_values->azzera),
                                                    "RIFG01" => $m_params->tipo_listino,
                                                    "RIFOR1" => $fornitore,
                                                    "RICLME" => $form_values->listino,
                                                    "RIGRME" => $form_values->LIZONA,
                                                    "RIRCOL" => $form_values->valuta,
                                                    "RISGME" => $var1,
                                                    "RICLFI" => $var2,
                                                    "RIGRFI" => $var3)
                                            )
                                            );
                                        
                                        
                                        
                                        $ret = array();
                                        $ret['success'] = true;
                                        echo acs_je($ret);
                                        exit;
}
// ******************************************************************************************
// EXE DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    //LETTURA RECORD LISTINO
    $sql = "SELECT * FROM {$cfg_mod_DeskUtility['file_listini']} LI WHERE RRN(LI) = '{$form_values->rrn}' ";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $error_msg =  db2_stmt_errormsg($stmt);
    $row_li = db2_fetch_assoc($stmt);
    
    //licon
    $sql_licon = "DELETE FROM {$cfg_mod_DeskUtility['file_listini_licon']}
                    WHERE LCDT = '{$row_li['LIDT']}' 
                      AND LCSZLI = '{$row_li['LITPLI']}'
                      AND LCCCON = '{$row_li['LICCON']}'
                      AND LCLIST = '{$row_li['LILIST']}'
                      AND LCZONA = '{$row_li['LIZONA']}'
                      AND LCVALU = '{$row_li['LIVALU']}'
                      AND LCART  = '{$row_li['LIART']}'
                ";
    
    $stmt_licon = db2_prepare($conn, $sql_licon);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_licon);
    
    //liraq
    $sql_liraq = "DELETE FROM {$cfg_mod_DeskUtility['file_listini_liraq']}
                    WHERE LRDT = '{$id_ditta_default}' AND LRART = '{$form_values->LIART}' AND
                    LRTPLI = '{$form_values->LITPLI}' AND LRLIST = '{$form_values->listino}' AND LRCCON = '{$form_values->LICCON}'
                    ";
    
    $stmt_liraq = db2_prepare($conn, $sql_liraq);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_liraq);
    
    //DELETE RECORD LISTINO
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_listini']} LI WHERE RRN(LI) = '{$form_values->rrn}' ";    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $error_msg =  db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = $result;
    if (strlen($error_msg) > 0) {
        $ret['success'] = false;
        $ret['message'] = $error_msg;
    }
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    
    $c_art = $m_params->open_request->c_art;
    $tipo_listino = $m_params->open_request->tipo_list;
    
    if($tipo_listino == 'V')
        $sql_where = " AND LITPLI = 'V'";
    else
        $sql_where = " AND LITPLI = 'A'";
            
            $sql = "SELECT LI.*, RRN(LI) AS RRN, ARVAR1, ARVAR2, ARVAR3, ARFOR1, CF_FORNITORE.CFRGS1 AS D_FORNITORE,
            LIRAQ.NR_LIRAQ AS NR_LIRAQ, LICON.NR_LICON AS NR_LICON
            FROM {$cfg_mod_DeskUtility['file_listini']} LI
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR
            ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
            ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = LI.LICCON AND CFTICF = 'F'
            LEFT OUTER JOIN (
            SELECT COUNT(*) AS NR_LIRAQ, LRDT, LRART, LRTPLI, LRCCON, LRLIST
            FROM {$cfg_mod_DeskUtility['file_listini_liraq']}
            GROUP BY LRDT, LRART, LRTPLI, LRCCON, LRLIST) LIRAQ
            ON LIRAQ.LRDT = LI.LIDT AND LIRAQ.LRART = LI.LIART AND LIRAQ.LRTPLI = LI.LITPLI
            AND LIRAQ.LRCCON = LI.LICCON AND LIRAQ.LRLIST = LI.LILIST
            LEFT OUTER JOIN (
                SELECT COUNT(*) AS NR_LICON, LCDT, LCSZLI, LCCCON, LCLIST, LCZONA, LCVALU, LCART
                FROM {$cfg_mod_DeskUtility['file_listini_licon']}
                GROUP BY LCDT, LCSZLI, LCCCON, LCLIST, LCZONA, LCVALU, LCART) LICON
                ON LICON.LCDT = LI.LIDT
                    AND LICON.LCSZLI = LI.LITPLI
                    AND LICON.LCCCON = LI.LICCON
                    AND LICON.LCLIST = LI.LILIST
                    AND LICON.LCZONA = LI.LIZONA
                    AND LICON.LCVALU = LI.LIVALU
                    AND LICON.LCART = LI.LIART
            WHERE LIDT = '{$id_ditta_default}' AND LIART = '{$c_art}' $sql_where
            ORDER BY LILIST, LIDTDE DESC";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            $count = 0;
            
            while($row = db2_fetch_assoc($stmt)){
                $count++;
                $nr = array();
                
                $nr['rrn'] = $row['RRN'];
                $nr['fornitore'] = "[".$row['LICCON']."] ".$row['D_FORNITORE'];
                $nr['c_forn'] = "[".$row['LICCON']."]";
                $nr['d_forn'] = "<b>".$row['D_FORNITORE']."</b>";
                
                $nr['LICCON'] = $row['LICCON'];
                $nr['listino']   = trim($row['LILIST']);
                $nr['valuta']    = trim($row['LIVALU']);
                $nr['data_val_ini']  = $row['LIDTDE'];
                $nr['data_val_fin']  = $row['LIDTVA'];
                $nr['data_val_ini_df']  = print_date($row['LIDTDE']);
                $nr['data_val_fin_df']  = print_date($row['LIDTVA']);
                $nr['decorr'] 	 = print_date($row['LIDTDP']); //data decorrenza precedente
                $nr['data_gen']  = $row['LIDTGE'];
                $nr['utente_gen'] 	 = $row['LIUSGE'];
                $nr['prezzo'] = $row['LIPRZ'];
                $nr['u_m'] 	  = trim($row['LIUM']);
                
                $varianti = "";
                
                if(trim($row['ARVAR1']) == '' && isset($cfg_mod_DeskArt["var_def_listino_1"]))
                    $dom1 = $cfg_mod_DeskArt["var_def_listino_1"];
                else
                    $dom1 = trim($row['ARVAR1']);
                        
                if(trim($row['ARVAR2']) == '' && isset($cfg_mod_DeskArt["var_def_listino_2"]))
                    $dom2 = $cfg_mod_DeskArt["var_def_listino_2"];
                else
                    $dom2 = trim($row['ARVAR2']);
                                
                if(trim($row['ARVAR3']) == '' && isset($cfg_mod_DeskArt["var_def_listino_3"]))
                    $dom3 = $cfg_mod_DeskArt["var_def_listino_3"];
                else
                    $dom3 = trim($row['ARVAR3']);
                        
                $val_risp = find_TA_sys('PUVN', trim($row['LIVAR1']), null, $dom1, null, null, 0, '', 'N', 'Y');
                if(trim($row['LIVAR1']) != '')
                    $varianti .= "[".trim($row['LIVAR1'])."] ".$val_risp[0]['text'];
                $val_risp = find_TA_sys('PUVN', trim($row['LIVAR2']), null, $dom2, null, null, 0, '', 'N', 'Y');
                if(trim($row['LIVAR2']) != '')
                    $varianti .= "<br>[".trim($row['LIVAR2'])."] ".$val_risp[0]['text'];
                $val_risp = find_TA_sys('PUVN', trim($row['LIVAR3']), null, $dom3, null, null, 0, '', 'N', 'Y');
                if(trim($row['LIVAR3']) != '')
                    $varianti .= "<br>[".trim($row['LIVAR3'])."] ".$val_risp[0]['text'];
                            
                                    
                $nr['ARVAR1'] 	 = $row['ARVAR1'];
                $nr['ARVAR2'] 	 = $row['ARVAR2'];
                $nr['ARVAR3'] 	 = $row['ARVAR3'];
                $nr['liraq'] 	 = $row['NR_LIRAQ'];
                $nr['licon'] 	 = $row['NR_LICON'];
                $nr['LIART'] 	 = $row['LIART'];
                $nr['LITPLI'] 	 = trim($row['LITPLI']);
                $nr['LIPRZP'] 	 = $row['LIPRZP'];
                $nr['LIPRZF'] 	 = $row['LIPRZF'];
                
                $nr['LISC1'] 	 = $row['LISC1'];
                $nr['LISC2'] 	 = $row['LISC2'];
                $nr['LISC3'] 	 = $row['LISC3'];
                $nr['LISC4'] 	 = $row['LISC4'];
                $nr['LIMAGG'] 	 = $row['LIMAGG'];
                $nr['LIVAR1'] 	 = trim($row['LIVAR1']);
                $nr['LIVAR2'] 	 = trim($row['LIVAR2']);
                $nr['LIVAR3'] 	 = trim($row['LIVAR3']);
                $nr['LINOTE'] 	 = trim($row['LINOTE']);
                $nr['filler'] 	 = $row['LIFIL1'];
                if($cfg_mod_DeskArt['filler_listini'] != ''){
                    $nr['LIFLG2'] 	 = substr($row['LIFIL1'], 4, 1);
                    $nr['sigla']     = substr($row['LIFIL1'], 3, 1);
                    $nr['f_imp']     = substr($row['LIFIL1'], 5, 1);
                    $nr['azzera']    = substr($row['LIFIL1'], 2, 1);
                }else{
                    $nr['sigla']     = trim($row['LIFLG1']);
                    $nr['LIFLG2'] 	 = trim($row['LIFLG2']);
                    $nr['f_imp']     = trim($row['LIFLG3']);
                    $nr['azzera']    = trim($row['LIAZZS']);
                }
                $nr['LIZONA'] 	 = trim($row['LIZONA']);
                $nr['varianti']  = $varianti;
                $nr['min_fatt']  = $row['LIMF'];
                
                if((trim($row['LICCON']) == trim($row['ARFOR1'])))
                    $nr['f_abi'] 	 = 'Y';
          
                    $ar[] = $nr;
                    
                   
                    
            }
                        
            echo acs_je($ar);
            exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
							{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            autoScroll: true,
            title: '',
            flex:0.7,
            items: [
						
							{
						xtype: 'grid',
						title: '',
						flex:0.75,
						autoScroll: true,
				        loadMask: true,
				        stateful: true,
				         <?php if($m_params->tipo_list == 'A'){?>
				         stateId: 'seleziona-listini-acq',
				         <?php }else{?>
				         stateId: 'seleziona-listini-ven',
				         <?php }?>
    					stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],	
				        store: {
						//xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['fornitore', 'c_forn', 'd_forn', 'listino', 'valuta', 'data_val_ini', 'data_val_fin', 'data_gen', 'utente_gen', {name: 'prezzo', type: 'float'}, 
		        			         'u_m', 'liraq', 'licon', 'rrn', 'LICCON', 'LIART', 'varianti', 'LIFLG2', 'LINOTE', 'azzera', 'LIZONA', 'filler',
		        			         'LITPLI', 'LIPRZP', 'LIPRZF', 'LIDTDP', 'LISC1', 'LISC2', 'LISC3', 'LISC4', 'LIMAGG', 'f_abi', 'min_fatt', 'f_imp',
		        			         'data_val_ini_df', 'data_val_fin_df', 'LIVAR1', 'LIVAR2', 'LIVAR3', 'ARVAR1', 'ARVAR2', 'ARVAR3', 'decorr', 'sigla']							
									
			}, //store
				
		    <?php $lrq = "<img src=" . img_path("icone/48x48/currency_black_pound.png") . " height=20>"; ?>
	        <?php $lcn = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>

			      columns: [	
			      
			      <?php if($m_params->tipo_list == 'A'){?>
			       {
	                header   : 'Fornitore',
	                dataIndex: 'fornitore',
	                width: 200,
	                filter: {type: 'string'}, filterable: true,
	                renderer: function(value, metaData, record){
    					if (record.get('f_abi') == 'Y')  metaData.tdCls += ' grassetto'; 						
    				
    				return value;	
					}
	                },
			      
			      <?php }?>
			      
			      {
	                header   : 'Listino',
	                dataIndex: 'listino',
	                filter: {type: 'string'}, filterable: true,
	                width: 50,
	                },
	                {
	                header   : 'Val.',
	                dataIndex: 'valuta',
	                filter: {type: 'string'}, filterable: true,
	                width: 40
	                },
	                {
	                header   : 'Prezzo',
	                dataIndex: 'prezzo',
	                width: 60,
	                align : 'right',
	                filter: {type: 'numeric'}, filterable: true,
	                renderer : floatRenderer2
	                },{
	                header   : 'UM',
	                dataIndex: 'u_m',
	                filter: {type: 'string'}, filterable: true,
	                width: 30
	                },{
	                header   : 'Varianti',
	                dataIndex: 'varianti',
	                filter: {type: 'string'}, filterable: true,
	                flex: 1
	                },
	                
	                <?php if($m_params->tipo_list == 'A'){?>
	                 {text: '<?php echo $lrq; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'liraq',
    			    filter: {type: 'string'}, filterable: true,
    				tooltip: 'Condizioni per quantit�',     			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('liraq') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_black_pound.png") ?> width=15>';
    		    		  }
    		        },
    		        <?php }?>
    		        
    		         {text: '<?php echo $lcn; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'licon',
    			    filter: {type: 'string'}, filterable: true,
    				tooltip: 'Maggiorazioni condizionate',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('licon') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_blue_yuan.png") ?> width=15>';
    		    		  }
    		        },
	                
	                {header: 'Data validit&agrave;',
                    columns: [
                      {header: 'Iniziale', dataIndex: 'data_val_ini', renderer: date_from_AS, width: 60, sortable: true}
                     ,{header: 'Finale', dataIndex: 'data_val_fin', renderer: date_from_AS, width: 60, sortable: true}
                 	 ]},
	                {header: 'Immissione',
                    columns: [
                      {header: 'Data', dataIndex: 'data_gen', renderer: date_from_AS, width: 60, sortable: true}
                     ,{header: 'Utente', dataIndex: 'utente_gen', width: 70}
                 	 ]}
	                     
	                
	                
	         ], listeners: {
	         
	         
	         selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('form').up('panel').down('#dx_form');
		               //pulisco eventuali filtri
		               form_dx.getForm().reset();
		               //ricarico i dati della form
	                   form_dx.getForm().setValues(selected[0].data);
	                   
	                   }
		          },
		          celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	var title_liraq = ' [' + rec.get('LIART') + ', '+ rec.get('LICCON') + ', ' +rec.get('listino') + ', ' + rec.get('valuta') + ']';
					  	var title_licon = ' [' + rec.get('LIART') + ', ' +rec.get('listino') + ', ' + rec.get('valuta') + ']';
					  	
					  	var my_listeners = {
    			    		  			afterModifica: function(from_win){
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
					  	
					  	 if(col_name == 'liraq')
            				acs_show_win_std('Prezzi/Condizioni per quantit&agrave;' + title_liraq, 'acs_anag_art_liraq.php?fn=open_tab', {c_art: rec.get('LIART'), list : rec.get('listino'), forn : rec.get('LICCON'), tipo : rec.get('LITPLI')}, 700, 400, my_listeners, 'icon-currency_black_pound-16');
					  	
					  	 if(col_name == 'licon')
            				acs_show_win_std('Maggiorazioni condizionate' + title_licon , 'acs_anag_art_licon.php?fn=open_tab', {c_art: rec.get('LIART'), list : rec.get('listino'), forn : rec.get('LICCON'), tipo : rec.get('LITPLI')}, 1200, 400, null, 'icon-currency_blue_yuan-16');
					  	
					  	}
					  	},
					  	
					  	  itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var row = rec.data;
					     var m_grid = this;
					     
					 	 
			    		
			    		 voci_menu.push({
			         		text: 'Riepilogo prezzi di vendita per variante/listino',
			        		iconCls : 'icon-print-16',          		
			        		  handler: function() {
			                 	 window.open('acs_anag_art_report_variante_listino.php?articolo='+ rec.get('LIART'));
		                  }
			    		});		
			    	
			    	<?php if($m_params->tipo_list == 'V'){?>
			    	 	 voci_menu.push({
			         		text: 'Duplica prezzo',
			        		iconCls : 'icon-copy-16',          		
			        		  handler: function() {
			        		  
			        		  var my_listeners = 
                                 { afterDuplica: function(from_win) {
                                     grid.getStore().load();
                                     from_win.close();
                                   }
                                 }
			        		  
			                 	acs_show_win_std('Duplica prezzo', 'acs_anag_art_listini.php?fn=open_duplica', {rrn: rec.get('rrn'), c_art: rec.get('LIART')}, 400, 300, my_listeners, 'icon-copy-16');
		                  }
			    		});		
			    	<?php }?>
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	         
	         
				  
				 }
				 
			, viewConfig: {
		        getRowClass: function(record, index) {
		        
		           if(new Date() < record.get('data_val_ini') && new Date() > record.get('data_val_fin'))
		          		 return ' colora_riga_rosso';
		         		        
		           /*if (record.get('data_val_fin') <=  new Date())
		            return ' colora_riga_rosso';
		           		
		           if (record.get('data_val_ini') >  new Date())
		           		return ' colora_riga_giallo';	*/	           		
		        		           		
		           return '';																
		         }   
		    }		
					       
		
		
		}
		]}, 
		
		<?php if($m_params->tipo_list == 'V'){
    		    $title = "Dettagli listino";
    		    $where = " AND (SUBSTRING(TAREST, 19, 2) = 'V'
                          OR SUBSTRING(TAREST, 21, 2) = 'V'
                          OR SUBSTRING(TAREST, 23, 2) = 'V'
                          OR SUBSTRING(TAREST, 25, 2) = 'V'
                          OR SUBSTRING(TAREST, 27, 2) = 'V')";
		}else{
		    
		    /*if(trim($m_params->rif_for) != '')
		        $title = "Dettagli listino - Riferim.fornitore {$m_params->rif_for}";
		    else*/
		        $title = "Dettagli listino ACQUISTO";
		        $where = " AND (SUBSTRING(TAREST, 19, 2) = 'A'
                          OR SUBSTRING(TAREST, 21, 2) = 'A'
                          OR SUBSTRING(TAREST, 23, 2) = 'A'
                          OR SUBSTRING(TAREST, 25, 2) = 'A'
                          OR SUBSTRING(TAREST, 27, 2) = 'A')";
			}
			
	?>
		    
		
		   {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: '<?php echo $title; ?>',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [
 		          {
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					hidden : true							
				   },  {
					name: 'filler',
					fieldLabel : 'filler',
					xtype: 'textfield',
					hidden : true				
				   }, {
					name: 'LITPLI',
					fieldLabel : 'litpli',
					xtype: 'textfield',
					hidden : true							
				   }, {
					name: 'LIZONA',
					fieldLabel : 'LIZONA',
					xtype: 'textfield',
					hidden : true							
				   }, 
				  {
					name: 'LIART',
					fieldLabel : 'LIART',
					xtype: 'textfield',
					hidden : true							
				   },
				   
				    <?php if($m_params->tipo_list == 'A'){
				        
				        if($m_params->nr_la == 0){
				   
				          $sql = "SELECT ARFOR1, CF_FORNITORE.CFRGS1 AS D_FOR
    				                FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
                                    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
                                        ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1 AND CFTICF = 'F'
    				                WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$m_params->c_art}'";
            				        $stmt = db2_prepare($conn, $sql);
            				        echo db2_stmt_errormsg();
            				        $result = db2_execute($stmt);
            				        $row = db2_fetch_assoc($stmt);
            				        $fornitore = trim($row['ARFOR1']);
            				        $d_fornitore = trim($row['D_FOR']);
				        }
				        
				        ?>
				        
				        {
    					name: 'LICCON',
    					fieldLabel : 'LICCON',
    					xtype: 'textfield',
    					hidden : true,
    					<?php if($m_params->nr_la == 0){?>
    						value : <?php echo j($fornitore) ?>
    					<?php }?>						
    				   },
    				    
    				    { xtype: 'fieldcontainer'
                          , layout:
                              {	type: 'hbox'
                              , pack: 'start'
                        	  , align: 'stretch'
                                }
                          , items: [ 
                             {
            				name: 'c_forn',
            				fieldLabel : 'Fornitore',
            				xtype: 'displayfield',
            				<?php if($m_params->nr_la == 0){?>
            					value : <?php echo j("[{$fornitore}]") ?>,
            				<?php }?>
            				labelWidth : 80, 
            				anchor: '-15',
            				listeners : {
            				   
            				
            				}							
            			   }, 
            			   {xtype: 'component',
                                flex : 1,
                            },
                          { xtype: 'button'
                         , margin: '0 15 0 0'
                         , anchor: '-15'	
                         , scale: 'small'
                         , iconCls: 'icon-search-16'
                         , iconAlign: 'top'
                         , width: 25			
                         , handler : function() {
                               var m_form = this.up('form').getForm();
                               var my_listeners = 
                                 { afterSel: function(from_win, row) {
                                     m_form.findField('LICCON').setValue(row.CFCD);
                                     m_form.findField('c_forn').setValue('['+row.CFCD+']');
                                     m_form.findField('d_forn').show();
                                     m_form.findField('d_forn').setValue('<b>'+row.CFRGS1+'</b>');
                                     from_win.close();
                                   }
                                 }
                             
                               acs_show_win_std('Anagrafica clienti/fornitori'
                                               , 'acs_anag_ricerca_cli_for.php?fn=open_tab'
                                               , {cf: 'F'}, 600, 500, my_listeners
                                               , 'icon-search-16');
                             }
                                    
                        }
                          
                          ]
                         },
				   
			            {
            				name: 'd_forn',
            				fieldLabel : '&nbsp;',
            				labelSeparator : '',
            				labelWidth : 80,
            				xtype: 'displayfield',
            				<?php if($m_params->nr_la == 0){?>
            				value : <?php echo j($d_fornitore) ?>,	
            				<?php }?>
            			//	hidden : true,
            				anchor: '-15'							
            			   },
				   
				   <?php }else{?>
				       {
    					name: 'LICCON',
    					fieldLabel : 'LICCON',
    					xtype: 'textfield',
    					hidden : true							
    				   },
				   <?php }?>
				       {
								name: 'listino',
								xtype: 'combo',
								fieldLabel: 'Listino',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		allowBlank: false,	
						   		queryMode: 'local',
						   		<?php if($m_params->tipo_list == 'A' && isset($cfg_mod_DeskArt["d_list_acq"])){?>
						   			value : <?php echo j($cfg_mod_DeskArt["d_list_acq"]) ?>,	
						   		<?php }?>
	                 		    minChars: 1, 											
							  	anchor: '-15',
                			    labelWidth : 80,
                				width : 200, 		
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, $where, 'Y'), ''); ?>	
								    ]
								}
								,listeners: { 
                			 		beforequery: function (record) {
                    	         		record.query = new RegExp(record.query, 'i');
                    	         		record.forceAll = true;
		            				 }
		         				 }
								
						 },
				   
						 {
								name: 'valuta',
								xtype: 'combo',
								fieldLabel: 'Valuta',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		allowBlank: false,	
						   		queryMode: 'local',
	                 		    minChars: 1, 	
                				labelWidth : 80,
                				anchor: '-15',
                				width : 150	,				
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('VUVL'), ''); ?>	
								    ]
								}
								,listeners: { 
                			 		beforequery: function (record) {
                    	         		record.query = new RegExp(record.query, 'i');
                    	         		record.forceAll = true;
		            				 }
		         				 }
								
						 },
						
				   
				   
				   	 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
        				   {
        					name: 'prezzo',
        					labelWidth : 80,
        					fieldLabel : 'Prezzo',
        					xtype: 'numberfield',
        					anchor: '-15',
        					width : 200,	
        					hideTrigger : true							
        				   },  
        				   
        				     <?php 
                			   
                			   $sql = "SELECT ARUMCO, ARUMAL, ARUMTE FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
                                          WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$m_params->c_art}' ";
                
                			   $stmt = db2_prepare($conn, $sql);
                			   echo db2_stmt_errormsg();
                			   $result = db2_execute($stmt);
                			   $row = db2_fetch_assoc($stmt);
                			   $arumte = trim($row['ARUMTE']);
                			   $arumco = trim($row['ARUMCO']);
                			   $arumal = trim($row['ARUMAL']);
                			   
                			   
                			   if($arumco == $arumte && $arumco == $arumal){
                			       $ar_um = "{id: '{$arumte}'}";
                			   }elseif(($arumal == $arumte && $arumal != $arumco) ||
                			       ($arumal == $arumco && $arumal != $arumte)){
                			       $ar_um = "{id: '{$arumte}'}, {id : '{$arumco}'}";
                			   }else{
                			       $ar_um = "{id: '{$arumte}'}, {id : '{$arumco}'}, {id : '{$arumal}'}";
                			   }
                			 
                			  ?>
        				   
        				    {
                			name: 'u_m',
                			xtype: 'combo',
                			fieldLabel: 'UM',
                			labelAlign : 'right',
                			width : 135,
                			labelWidth : 50,
                			forceSelection: true,								
                			displayField: 'id',
                			valueField: 'id',								
                			emptyText: 'select',
                	   		allowBlank: true,								
                		    anchor: '-15',
                		    queryMode: 'local',
                            minChars: 1,
                            value :  '<?php echo $arumte; ?>',
                			store: {
                				editable: false,
                				autoDestroy: true,
                			    fields: [{name:'id'}],
                			    data: [	<?php echo $ar_um; ?> ]
                			},
                			listeners: { 
                			 	beforequery: function (record) {
                	         	record.query = new RegExp(record.query, 'i');
                	         	record.forceAll = true;
                             }
                          }
                		 } ,
						
						   
						]},
				   
				  
				   
				   <?php 
				   
				   $sql_a = "SELECT ARVAR1, ARVAR2, ARVAR3, ARGVA1, ARGVA2, ARGVA3 
                             FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
                             WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$m_params->c_art}' ";
			
				   $stmt_a = db2_prepare($conn, $sql_a);
				   echo db2_stmt_errormsg();
				   $result = db2_execute($stmt_a);
				   $row_a = db2_fetch_assoc($stmt_a);
				   
				   
				   if(trim($row_a['ARVAR1']) == '' && isset($cfg_mod_DeskArt["var_def_listino_1"])){
				       $arvar1 = $cfg_mod_DeskArt["var_def_listino_1"];
				   }else{ 
				       $arvar1 = trim($row_a['ARVAR1']);
				   }
				   
				   if(trim($row_a['ARVAR1']) == '' /*&& isset($cfg_mod_DeskArt["var_def_listino_1"])*/){
				       $allowBlank_var1 = "true";
				   } else {
				       if(trim($row_a['ARGVA1']) == 'L')
				          $allowBlank_var1 = "false";
				       else
				          $allowBlank_var1 = "true";
				   }				   
				   
				   
			       if(trim($row_a['ARVAR2']) == '' /*&& isset($cfg_mod_DeskArt["var_def_listino_2"])*/){
			           $arvar2 = "";//$cfg_mod_DeskArt["var_def_listino_2"];
			           $allowBlank_var2 = "true";
                   }else{
		               $arvar2 = trim($row_a['ARVAR2']);
		               if(trim($row_a['ARGVA2']) == 'L')
		                   $allowBlank_var2 = "false";
	                   else
	                       $allowBlank_var2 = "true";
                   }        
	               if(trim($row_a['ARVAR3']) == '' /*&& isset($cfg_mod_DeskArt["var_def_listino_3"])*/){
	                   $arvar3 = "";//$cfg_mod_DeskArt["var_def_listino_3"];
	                   $allowBlank_var3 = "true";
	               }else{
                       $arvar3 = trim($row_a['ARVAR3']);
                       if(trim($row_a['ARGVA3']) == 'L')
                           $allowBlank_var3 = "false";
                       else
                           $allowBlank_var3 = "true";
	               }
				                   
				
				   ?>
				   
				   {
					name: 'LIVAR1',
					xtype: 'combo',
					id : 'id_class',
					fieldLabel: 'Variante 1',
					labelWidth : 80,
					forceSelection: true,								
					displayField: 'text',
					valueField: 'id',								
					emptyText: '- seleziona -',
					allowBlank: <?php echo $allowBlank_var1; ?>,			
			   		anchor: '-15',
				    queryMode: 'local',
	                minChars: 1, 
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'},  {name:'color'}, {name:'sosp'}],
					    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $arvar1, null, null, 0, '', 'Y', 'Y'), ""); ?>	
					    ]
					},
    				tpl: [
    				 '<ul class="x-list-plain">',
                        '<tpl for=".">',
                        '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                        '</tpl>',
                        '</ul>'
    				 ],
                       // template for the content inside text field
                    displayTpl: Ext.create('Ext.XTemplate',
                           '<tpl for=".">',

	                            '{text}',

	                        '</tpl>'
                     
                    ),
					listeners: { 
					 	beforequery: function (record) {
			         	record.query = new RegExp(record.query, 'i');
			         	record.forceAll = true;
		             },
		             
		          }
				 } ,
				 {
					name: 'LIVAR2',
					xtype: 'combo',
					fieldLabel: 'Variante 2',
					forceSelection: true,	
					labelWidth : 80,							
					displayField: 'text',
					valueField: 'id',								
					emptyText: '- seleziona -',
			   		allowBlank: <?php echo $allowBlank_var2; ?>,										
				    anchor: '-15',
				    queryMode: 'local',
	                minChars: 1, 
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $arvar2, null, null, 0, '', 'Y', 'Y'), ""); ?>	
					    ]
					},
					listeners: { 
					 	beforequery: function (record) {
			         	record.query = new RegExp(record.query, 'i');
			         	record.forceAll = true;
		             }
		            
		          }
				 } ,
				 {
					name: 'LIVAR3',
					xtype: 'combo',
					fieldLabel: 'Variante 3',
					forceSelection: true,	
					labelWidth : 80,							
					displayField: 'text',
					valueField: 'id',								
					emptyText: '- seleziona -',
		   			allowBlank: <?php echo $allowBlank_var3; ?>,									
				    anchor: '-15',
				    queryMode: 'local',
	                minChars: 1, 
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
					      <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $arvar3, null, null, 0, '', 'Y', 'Y'), ""); ?>		
					    ]
					}, listeners: { 
					 	beforequery: function (record) {
			         	record.query = new RegExp(record.query, 'i');
			         	record.forceAll = true;
		             }
		             
		          }
				 },
				 
				 	 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						   {
        				   xtype: 'datefield'
        				   , startDay: 1 //lun.
        				   , fieldLabel: 'Validit&agrave; dal'
        				   , labelWidth : 80			
        				   , name: 'data_val_ini_df'
        				   , width : 200			
        				   , format: 'd/m/y'
        				   , submitFormat: 'Ymd'
        				   , allowBlank: true
        				   , anchor: '-15'
        				   , listeners: {
        				       invalid: function (field, msg) {
        				       Ext.Msg.alert('', msg);}
        						}
        				},  {
        				   xtype: 'datefield'
        				   , startDay: 1 //lun.
        				   , fieldLabel: 'al'
        				   , labelAlign : 'right'
                		   , width : 135
                		   , labelWidth : 50
        				   , name: 'data_val_fin_df'
        				   , format: 'd/m/y'
        				   , submitFormat: 'Ymd'
        				   , allowBlank: true
        				   , anchor: '-15'
        				   , maxValue: '20991231'
        				   , listeners: {
        				       invalid: function (field, msg) {
        				       Ext.Msg.alert('', msg);}
        						}
        				},
						   
						]},
						
        					{
                			name: 'LIFLG2',
                			xtype: 'combo',
                			fieldLabel: 'Check data',
                			labelWidth : 80,
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    anchor: '-15',
                		    queryMode: 'local',
                		    width : 200,
                            minChars: 1, 
                			store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								    <?php echo acs_ar_to_select_json($deskArt->find_TA_std('CHKDT', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>
								    ]
								},
                			listeners: { 
                			 	beforequery: function (record) {
                	         	record.query = new RegExp(record.query, 'i');
                	         	record.forceAll = true;
                             }
                          }
                		 },
						
				
				 
    				  {
        				name: 'LINOTE',
        				fieldLabel : 'Rifer. listino',
        				xtype: 'textfield',
        				labelWidth : 80,			
        				anchor: '-15'							
        			   },
        			     {
        				name: 'rifer_forn',
        				fieldLabel : 'Rif. Fornitore',
        				xtype: 'textfield',
        				value: <?php echo j($m_params->rif_for); ?>,
        				labelWidth : 80,			
        				anchor: '-15',
        				readOnly : true							
        			   },
        			  
    			   
    			   { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
        				name: 'LIPRZP',
        				fieldLabel : 'Prezzo prec.',
        				xtype: 'numberfield',
        				hideTrigger : true,	
        				labelWidth : 80,
        				width : 200,
        				anchor: '-15'							
        			   }, {
        				   xtype: 'datefield'
        				   , startDay: 1 //lun.
        				   , fieldLabel: 'Decorr.'
        				   , labelWidth : 50
        				   , labelAlign : 'right'			
        				   , name: 'decorr'
        				   , width : 135			
        				   , format: 'd/m/y'
        				   , submitFormat: 'Ymd'
        				   , allowBlank: true
        				   , anchor: '-15'
        				   , listeners: {
        				       invalid: function (field, msg) {
        				       Ext.Msg.alert('', msg);}
        						}
        				}
						
						   
						]},
						
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
        					{
                				name: 'LIPRZF',
                				fieldLabel : 'Prezzo futuro',
                				hideTrigger : true,	
                				xtype: 'numberfield',
                				labelWidth : 80,	
                				width : 200,		
                				anchor: '-15'							
                			   }, 
        						{
                				name: 'sigla',
                				fieldLabel : 'Sigla',
                				xtype: 'textfield',
                				labelWidth : 80,			
                				anchor: '-15', 
                				labelAlign : 'right', 
                				labelWidth : 50,
                				maxLength : 1,
                				width : 135,
                				listeners:{
                                   change:function(field){
                                        field.setValue(field.getValue().toUpperCase());
                                   }
                               }							
                			   },
						   
						]},
    			   
    			  
    			 
    			   
    			   
    			   
    			   { 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					  {
    				name: 'LISC1',
    				fieldLabel : 'Sconti %',
    				xtype: 'numberfield',
    				anchor: '-15',
    				hideTrigger : true,	
    				width : 132	,
    				labelWidth : 80					
    			   },{
    				name: 'LISC2',
    				fieldLabel : '%',
    				xtype: 'numberfield',
    				labelSeparator : '',
    				hideTrigger : true,	
    				labelAlign : 'right',
    				labelWidth : 20,
    				width : 68,
    											
    			   },{
    				name: 'LISC3',
    				fieldLabel : '%',
    				xtype: 'numberfield',
    				hideTrigger : true,	
    				labelSeparator : '',
    				labelAlign : 'right',
    				labelWidth : 20,
    				width : 68,							
    			   },{
    				name: 'LISC4',
    				fieldLabel : '%',
    				labelWidth : 20,
    				labelAlign : 'right',
    				hideTrigger : true,	
    				labelSeparator : '',
    				width : 68,
    				xtype: 'numberfield',
    				anchor: '-15'							
    			   }
					   
					]},
					
					 
    			   { 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					  {
    				name: 'LIMAGG',
    				fieldLabel : 'Maggioraz. %',
    				xtype: 'numberfield',
    				hideTrigger : true,	
    				width : 132	,
    				labelWidth : 80,	
    				anchor: '-15'							
    			   },{ 
    				name: 'azzera',
    				fieldLabel : 'Azzera sconti',
    				xtype: 'combo',
    				margin : '0 0 0 22',
    				width : 180	,
    				labelWidth : 80,	
    				anchor: '-15', 
    				forceSelection: true,								
        			displayField: 'text',
        			valueField: 'id',								
        			emptyText: '-seleziona-',
					store: {
    				editable: false,
    				autoDestroy: true,
    				fields: [{name:'id'}, {name:'text'}, {name:'color'}],
    				data: [	 {id: 'Y', text : '[Y] Si'},]
        			}
    				}
					   
					]} ,
					
				{ 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					  {
    				name: 'min_fatt',
    				fieldLabel : 'Minimo fatt.',
    				xtype: 'numberfield',
    				hideTrigger : true,	
    				width : 132	,
    				labelWidth : 80,	
    				anchor: '-15'							
    			   }, 	{ 
    				name: 'f_imp',
    				fieldLabel : 'Ad importo',
    				xtype: 'combo',
    				margin : '0 0 0 22',
    				width : 180	,
    				labelWidth : 80,	
    				anchor: '-15', 
    				forceSelection: true,								
        			displayField: 'text',
        			valueField: 'id',								
        			emptyText: '-seleziona-',
					store: {
    				editable: false,
    				autoDestroy: true,
    				fields: [{name:'id'}, {name:'text'}, {name:'color'}],
    				data: [	 {id: 'I', text : '[I] Si'},]
        			}
    				}
					   
					]},
									 
			 		           ],
			 		           
	<?php if(!isset($m_params->solo_visualizzazione) || $m_params->solo_visualizzazione != 'Y'){?>	           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     }, {
                     xtype: 'button',
                    text: 'Refresh',
		            scale: 'small',	               
		            iconCls : 'icon-button_black_repeat_dx-16',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			  	  form = this.up('form'); 
 			          grid.getStore().load();
 			          form.getForm().reset();
 			        }
			     }
			     
			    <?php if($m_params->tipo_list == 'A'){ ?>
			     ,{
                     xtype: 'button',
                    text: 'Costi',
		            scale: 'small',	               
		            iconCls : 'icon-listino',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			 	  form = this.up('form'); 
	       			 	  
	       			 	  acs_show_win_std('Costi articolo [' + <?php echo j($m_params->c_art); ?> + '] ' + <?php echo j($m_params->d_art); ?>, 'acs_anag_art_costi.php?fn=open_tab', {c_art: <?php echo j($m_params->c_art); ?> , solo_visualizzazione : 'Y'}, 1000, 530, null, 'icon-listino');
 			        
 			        }
			     }
			     <?php }?>
			     
			     , '->',
                 
                  
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
	       			  
	       			  if(typeof(form_values.LIVAR1) !== 'undefined'){
 			       			   <?php if(trim($row_a['ARGVA1']) != 'L'){?>
			 		       		form.getForm().findField('LIVAR1').setValue('');
			 		       		acs_show_msg_error('Errore valorizzazione 1a variante: non prevista a listino'); 
                	       		return;
			 		     	   <?php }?>
 			       			}
 			       			
 			       			if(typeof(form_values.LIVAR2) !== 'undefined'){
 			       			   <?php if(trim($row_a['ARGVA2']) != 'L'){?>
			 		       		form.getForm().findField('LIVAR2').setValue('');
			 		       		acs_show_msg_error('Errore valorizzazione 2a variante: non prevista a listino'); 
                	       		return;
			 		     	   <?php }?>
 			       			}
 			       			
 			       			if(typeof(form_values.LIVAR3) !== 'undefined'){
 			       			   <?php if(trim($row_a['ARGVA3']) != 'L'){?>
			 		       		form.getForm().findField('LIVAR3').setValue('');
			 		       		acs_show_msg_error('Errore valorizzazione 3a variante: non prevista a listino'); 
                	       		return;
			 		     	   <?php }?>
 			       			}
	       			  
	       			  
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_listino',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				c_art : '<?php echo $m_params->c_art; ?>',
	        				tipo_listino : '<?php echo$m_params->tipo_list; ?>'
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		           	
			 		     
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			
 			       			if(typeof(form_values.LIVAR1) !== 'undefined'){
 			       			   <?php if(trim($row_a['ARGVA1']) != 'L'){?>
			 		       		form.getForm().findField('LIVAR1').setValue('');
			 		       		acs_show_msg_error('Errore valorizzazione 1a variante: non prevista a listino'); 
                	       		return;
			 		     	   <?php }?>
 			       			}
 			       			
 			       			if(typeof(form_values.LIVAR2) !== 'undefined'){
 			       			   <?php if(trim($row_a['ARGVA2']) != 'L'){?>
			 		       		form.getForm().findField('LIVAR2').setValue('');
			 		       		acs_show_msg_error('Errore valorizzazione 2a variante: non prevista a listino'); 
                	       		return;
			 		     	   <?php }?>
 			       			}
 			       			
 			       			if(typeof(form_values.LIVAR3) !== 'undefined'){
 			       			   <?php if(trim($row_a['ARGVA3']) != 'L'){?>
			 		       		form.getForm().findField('LIVAR3').setValue('');
			 		       		acs_show_msg_error('Errore valorizzazione 3a variante: non prevista a listino'); 
                	       		return;
			 		     	   <?php }?>
 			       			}
 			       			
 			       			
 			       			
 			       			
 			       			
 			       			
 			       			
 			       			
 			       			
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_listino',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
		   <?php }?>
			 	 }
					
						   
						
						
				
						

								
					
					
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}



if ($_REQUEST['fn'] == 'exe_duplica'){
    $ret = array();
    $form_values = $m_params->form_values;
    
    //LETTURA RECORD LISTINO
    $sql_li = "SELECT * FROM {$cfg_mod_DeskUtility['file_listini']} LI WHERE RRN(LI) = '{$m_params->rrn}' ";
   
    $stmt_li = db2_prepare($conn, $sql_li);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_li);
    $error_msg =  db2_stmt_errormsg($stmt_li);
    $row_li = db2_fetch_assoc($stmt_li);
    
    $conteggio = 0;
    $ar_listino = array();
    for($i =1; $i <=5; $i++){
        $listino = "listino_{$i}";
        if(isset($form_values->$listino) && strlen($form_values->$listino) > 0)
            $ar_listino[] = $form_values->$listino;
        
    }    
 
    $where = "";
    if(count($ar_listino) > 0)
        $where .= sql_where_by_combo_value('LILIST', $ar_listino);
    else 
        $where .= sql_where_by_combo_value('LILIST', $row_li['LILIST']);
   
    if(isset($form_values->articolo) && strlen($form_values->articolo) > 0)
        $where .= " AND LIART = '{$form_values->articolo}' ";
    else
        $where .= " AND LIART = '{$m_params->c_art}' ";
    
    
    $sql = "SELECT COUNT(*) C_ROW FROM {$cfg_mod_DeskUtility['file_listini']} LI 
            WHERE LIDT = '{$id_ditta_default}' AND LITPLI = 'V'
            {$where}";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
  
    if($row['C_ROW'] > 0){
        $conteggio++;
    }
     
    
    if($conteggio == 0){
        
        $row_li['LIUSUM'] 	= SV2_db_user($auth->get_user());
        $row_li['LIDTUM'] 	= oggi_AS_date();
        $row_li['LIUSGE'] 	= $auth->get_user();
        $row_li['LIDTGE'] 	= oggi_AS_date();
        if(isset($form_values->articolo) && strlen($form_values->articolo) > 0)
            $row_li['LIART'] = $form_values->articolo;
        
        for($i =1; $i <=5; $i++){
            $listino = "listino_{$i}";
            if(isset($form_values->$listino) && strlen($form_values->$listino) > 0){
                $row_li['LILIST'] = $form_values->$listino;
                
                $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_listini']}(" . create_name_field_by_ar($row_li) . ") VALUES (" . create_parameters_point_by_ar($row_li) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $row_li);
                echo db2_stmt_errormsg($stmt);
                
            }
        }
        
        if(count($ar_listino) == 0 && 
            isset($form_values->articolo) && strlen($form_values->articolo) > 0){
            
                
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_listini']}(" . create_name_field_by_ar($row_li) . ") VALUES (" . create_parameters_point_by_ar($row_li) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $row_li);
            echo db2_stmt_errormsg($stmt);
            
            
        }
        

    }else{
        
        $ret['success'] = false;
        $ret['msg_error'] = "Righe listino da generare gi� esistenti!";
        echo acs_je($ret);
        return;
    }
   
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'open_duplica'){ 

    $where = " AND (SUBSTRING(TAREST, 19, 2) = 'V'
              OR SUBSTRING(TAREST, 21, 2) = 'V'
              OR SUBSTRING(TAREST, 23, 2) = 'V'
              OR SUBSTRING(TAREST, 25, 2) = 'V'
              OR SUBSTRING(TAREST, 27, 2) = 'V')";
    ?>
    
 {"success":true, 
	m_win: {
		title: 'Duplica prezzo',
		width: 500, height: 200,
		iconCls: 'icon-copy-16'
	}, 
	"items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [
            <?php for($i = 1; $i <= 5; $i++){?>
        		 {
					name: '<?php echo "listino_{$i}"; ?>',
					xtype: 'combo',
					fieldLabel: 'Listino',
					forceSelection: true,								
					displayField: 'text',
					valueField: 'id',								
					emptyText: '- seleziona -',
			   		//allowBlank: false,	
			   		queryMode: 'local',
         		    minChars: 1, 											
				  	anchor: '-15',
    			    labelWidth : 80,
    				width : 200, 		
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, $where, 'Y'), ''); ?>	
					    ]
					}
					,listeners: { 
    			 		beforequery: function (record) {
        	         		record.query = new RegExp(record.query, 'i');
        	         		record.forceAll = true;
        				 }
     				 }
					
			 },
			 <?php }?>
			   {
						flex: 1,
			            xtype: 'combo',
						name: 'articolo',
						fieldLabel: 'Articolo',
						labelWidth : 80,
						itemId: 'cod_art',
						anchor: '-15',
						minChars: 2,			
						store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_anag_art_abbinamento.php?fn=get_json_data_articoli',
					            actionMethods: {
						          read: 'POST'
						        },
					            extraParams: {
				    		    		gruppo: '',
				    				},				            
						        doRequest: personalizza_extraParams_to_jsonData, 
					            reader: {
					                type: 'json',
					                root: 'root',
					                method: 'POST',	
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            },
                      
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun articolo trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {text}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
        		}
            ],
            
			buttons: [{
		            text: 'Conferma duplica',
		            iconCls: 'icon-button_blue_play-32',
			       	scale: 'large',		            
		            handler: function() {
		            	var form = this.up('form').getForm(),
		            	    form_values = form.getValues(),
			                loc_win = this.up('window');
			                
			           if(Object.keys(form_values).length === 0 && form_values.constructor === Object){
			              acs_show_msg_error('Inserire o un listino o un articolo');
			              return;
			           }
			           
			                
		              if (form.isValid()){
							Ext.Ajax.request({
 						        url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_duplica',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   form_values: form_values,
 			        			   c_art: <?php echo j($m_params->c_art); ?>,
 			        			   rrn: <?php echo j($m_params->rrn); ?>,
 								},
 								
 								success: function(response, opts) {
						        	 var jsonData = Ext.decode(response.responseText);
						        	 if(jsonData.success == false){
						        	 	acs_show_msg_error(jsonData.msg_error);
						        	 }else{
									 	loc_win.fireEvent('afterDuplica', loc_win);
			            		     }
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
			            }
		            }
		        }
	        ]
	  }
]}
 
 <?php 
    
}
