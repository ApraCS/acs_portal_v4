<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'exe_canc_allegato'){
 
    $m_params = acs_m_params_json_decode();
    $rrn= $m_params->row->RRN;
    
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle']} TA WHERE RRN(TA) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    $file_src = $cfg_mod_DeskArt['root_path']."PROJECTTODO/".trim($id_ditta_default) ."/".trim($m_params->row->n_img);
    
    unlink($file_src);
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    $ar = array(); 
    $progetto = $m_params->open_request->progetto;
   
    
    $sql = "SELECT RRN(TA) AS RRN, TA.*
    FROM {$cfg_mod_DeskArt['file_tabelle']} TA
    WHERE  TATAID = 'TDPAL' AND TAKEY1 = '{$progetto}'";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $tab_cat = $desk_art->get_TA_std('TDPCA', $row['TASITI']);
        $nr['utente']   = trim($row['TAUSGE']);
        $nr['categoria']   = $tab_cat['TADESC'];
        $nr['data_ins']    = print_date(oggi_AS_date());
        $nr['descrizione'] = $row['TADESC'];
        $nr['tipo_img']    = $row['TAFG01'];
        $nr['n_img']       = trim($row['TAMAIL']);
        $nr['RRN'] 	       = $row['RRN'];
        
        
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
      
    ?>

{"success":true, "items": [

        {
				xtype: 'form',
				flex: 1, layout: 'fit',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
				items: [
						{
						xtype: 'grid',
						title: '',					
				        loadMask: true,	
				        tbar: new Ext.Toolbar({
            	            items:['->',
            	            {text: 'AGGIUNGI', 
            	             iconCls: 'icon-search-16',
            	             handler: function(event, toolEl, panel){
            	                    var m_grid = this.up('grid');
            			          	acs_show_win_std('Aggiungi', '../desk_utility/acs_todo_aggiungi_img.php?fn=open_form', {
            			          			from_grid: m_grid.id,
                			          		progetto: '<?php echo $m_params->progetto; ?>'
                			          	} , 600, 220, {
             		            		afterUpload: function(from_win){
             		            			m_grid.store.load();
             		            			from_win.close()
                 		            	}
             		            	   }, 'icon-search-16');
            			          
            		           		 
            		           		 }
            		           	 }
            	           
            	         ]            
	      			  }),
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['utente', 'categoria', 'data_ins', 'descrizione', 'tipo_img', 'n_img', 'RRN']							
									
			}, //store
				
				<?php $cf1 = "<img src=" . img_path("icone/48x48/camera.png") . " height=18>"; ?>

			      columns: [
			      {
	                header   : 'Utente',
	                dataIndex: 'utente',
	                flex: 1
	                },	
			      {
	                header   : 'Categoria',
	                dataIndex: 'categoria',
	                flex: 1
	                },
	                {
	                header   : 'Data',
	                dataIndex: 'data_ins',
	                width: 60
	                },
	                {
	                header   : 'Descrizione',
	                dataIndex: 'descrizione',
	                flex: 2
	                },{
	                text: '<?php echo $cf1; ?>',
	                dataIndex: 'tipo_img',
	                width: 40,
	                renderer: function(value, p, record){
		    			  if(record.get('tipo_img') == 'L') return '<img src=<?php echo img_path("icone/48x48/attachment.png") ?> width=15>';
		    			  if(record.get('tipo_img') == 'U') return '<img src=<?php echo img_path("icone/48x48/folder_upload.png") ?> width=15>';
		    		  }
	                }               
	                
	                
	         ], listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					  	 voci_menu.push({
			         		text: 'Cancella',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_allegato',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				progetto : <?php echo j($m_params->progetto); ?>
										},							        
								        success : function(result, request){
					            			grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    		});	 
			    	
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	         
	           , celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);
							  
							  if(rec.get('tipo_img') == 'L'){
							  
    							  var submitForm = new Ext.form.FormPanel();
    								submitForm.submit({
    								        target: '_blank', 
    			                        	standardSubmit: true,
    			                        	method: 'POST',
    								        url: rec.get('n_img'),
    								        params: {}
    								        });	  
						
							  }else{
							     allegatiPopup('../desk_utility/acs_get_allegati_images.php?function=view_image&RRN=' + rec.get('RRN') + '&p_todo=Y');
							  }
							  							  
							 
						  }
			   		  }
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           /*if (record.get('stato') == 'C')
					           		return 'colora_riga_grigio';*/
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					
					
	}
	
]}


<?php 
}
