<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];  //descrizione
    $nr['tades2'] = $row['TADES2'];  //marca
    
    $nr['tipo'] = substr($row['TAREST'], 0, 15);
    $nr['dim1'] = substr($row['TAREST'], 15, 8);
    $nr['dim2'] = substr($row['TAREST'], 23, 8);
    $nr['dim3'] = substr($row['TAREST'], 31, 8);
    $nr['gruppo'] = substr($row['TAREST'], 39, 4);
    $nr['disegno'] = substr($row['TAREST'], 43, 30);
    $nr['parametri'] = substr($row['TAREST'], 73, 100);
    
    
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'tipo', 'parametri' , 'dim1', 'dim2', 'dim3', 'gruppo', 'disegno');
    
    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start();
    
    ?>
    
			        {
	                header   : 'Descrizione',
	                dataIndex: 'tadesc',
	                width : 200
	                },
	                
	                {
	                header   : 'Marca',
	                dataIndex: 'tades2',
	                width : 100
	                },    {
	                header   : 'Tipo articolo',
	                dataIndex: 'tipo',
	                width : 150
	                },
	                {
	                header   : 'Dim 1',
	                dataIndex: 'dim1',
	                width : 50
	                },
	                {
	                header   : 'Dim 2',
	                dataIndex: 'dim2',
	                width : 50
	                },{
	                header   : 'Dim 3',
	                dataIndex: 'dim3',
	                width : 50
	                },{
	                header   : 'Parametri',
	                dataIndex: 'parametri',
	                flex: 1
	                }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "tades2" => trim($row->tades2),
        "tipo" => trim($row->tipo),
        "parametri" => trim($row->parametri),
        "dim1" => trim($row->dim1),
        "dim2" => trim($row->dim2),
        "dim3" => trim($row->dim3),
        "gruppo" => trim($row->gruppo),
        "disegno" => trim($row->disegno)
        
    );
 
    return $ar_values;
    
}

function get_values_from_array($row){
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "tades2" => trim($row['tades2']),
        "tipo" => trim($row['tipo']),
        "parametri" => trim($row['parametri']),
        "dim1" => trim($row['dim1']),
        "dim2" => trim($row['dim2']),
        "dim3" => trim($row['dim3']),
        "gruppo" => trim($row['gruppo']),
        "disegno" => trim($row['disegno'])
        
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    
    global $main_module;
    
    ob_start();
    
    ?>
      {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		labelWidth : 110,
		//anchor: '-15',	
		width : 150,
		maxLength : 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
        {xtype: 'textfield',
        name: 'descrizione',
        fieldLabel: 'Descrizione',
        labelWidth : 110,
        maxLength: 30,
        anchor: '-15',
        flex : 1,	
        value:  <?php echo j($ar_values['descrizione']); ?>
        },
        {xtype: 'textfield',
        name: 'tades2',
        fieldLabel: 'Marca',
        labelWidth : 110,
        maxLength: 30,
        anchor: '-15',	
        value:  <?php echo j($ar_values['tades2']); ?>
        },
        {xtype: 'textfield',
        name: 'tipo',
        fieldLabel: 'Tipo articolo',
        labelWidth : 110,
        maxLenght: 15,
       // anchor: '-15',	
        value:  <?php echo j($ar_values['tipo']); ?>
        },

		{
	    xtype: 'fieldcontainer',
	    title : 'Dimensioni',
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},
		 						
		items: [
		    { xtype: 'displayfield', value : 'Dimens.nominali', margin : '0 30 0 0'},
			{
			xtype: 'numberfield',
		    hideTrigger : true,
			width : 113,
			labelWidth : 30,
			labelAlign : 'right',
			name: 'dim1',
			fieldLabel: 'L',
			anchor: '-15',	
			maxLength : 8,
			value:  <?php echo j($ar_values['dim1']); ?>
			},{
			xtype: 'numberfield',
			hideTrigger : true,
			width : 113,
			name: 'dim2',
			fieldLabel: 'H',
			labelWidth : 30,
			labelAlign : 'right',
			anchor: '-15',
			maxLength : 8,	
			value:  <?php echo j($ar_values['dim2']); ?>
			},{
			xtype: 'numberfield',
			hideTrigger : true,
			width : 113,
			labelWidth : 30,
			name: 'dim3',
			labelAlign : 'right',
			fieldLabel: 'S',
			anchor: '-15',	
			maxLength : 8,
			value:  <?php echo j($ar_values['dim3']); ?>
			},
		   
		
		]},
		
		   {name: 'gruppo',
			xtype: 'combo',
			fieldLabel: 'Gruppo incasso',
			labelWidth: 110,
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['gruppo']); ?>,							
			emptyText: '- seleziona -',
	   		//allowBlank: false,								
		    anchor: '-15',
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('GRIE'), ''); ?>	
			    ]
			}
			
	 },
	
	{xtype: 'textfield',
	name: 'disegno',
	fieldLabel: 'Disegno/SAT',
	labelWidth : 110,
	maxLength: 30,
	anchor: '-15',	
	value:  <?php echo j($ar_values['disegno']); ?>
	},
	{xtype: 'textfield',
	name: 'parametri',
	fieldLabel: 'Parametri',
	labelWidth : 110,
	maxLength: 100,
	anchor: '-15',	
	value:  <?php echo j($ar_values['parametri']); ?>
	}
	
    
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
  
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-15s", $form_values->tipo);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->dim1);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->dim2);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->dim3);
    $ar_ins['TAREST'] .= sprintf("%-4s", $form_values->gruppo);
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->disegno);
    $ar_ins['TAREST'] .= sprintf("%-100s", $form_values->parametri);
    $ar_ins['TANR']   .= sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] .= $form_values->descrizione;
    $ar_ins['TADES2'] .= $form_values->tades2;
    
    return $ar_ins;
    
}