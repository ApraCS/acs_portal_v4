<?php

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$cfg_mod = $desk_art->get_cfg_mod();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 

$m_table_config = array(
    'module'      => $desk_art,
    'tab_name' =>  $cfg_mod['file_tabelle'],
    't_panel' =>  "DTIMP - Dati import",
    'descrizione' => "Tipologia articoli",
    'form_title' => "Dettagli tabella dati import",
    
    'fields_preset' => array(
        'TATAID' => 'DTIMP'
    ),
   
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
    
    'fields_key' => array('TAKEY1', 'TADESC'),

    'fields_grid' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TATELE', 'TIPOL_DA', 'TAINDI', 'TAB_ABB', 'immissione'),
    'fields_form' => array('TAKEY1', 'TADESC', 'TAMAIL', 'TATELE', 'TAASPE', 'TATISP', 'TATITR', 'TARIF1', 'TARIF2', 
                           'TALOCA', 'TACAP', 'TAINDI', 'TAPROV', 'TASITI'),

    
    'fields' => array(
        //'TASTAL'     => array('label'	=> $divieto, 'fw'=>'width: 40'),
        'TAKEY1' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione',  'maxLength' => 100),
        'TAMAIL' => array('label'	=> 'Note', 'maxLength' => 100),
        'TATELE' => array('label'	=> 'Seq.', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 5), //ALFANUMERICO
        
        'TAASPE' => array('label'	=> 'Tipologia', 'type' => 'from_TA', 'TAID' => 'TPART', 'allowBlank' => true),
        'TATISP' => array('label'	=> 'Tipologia', 'type' => 'from_TA', 'TAID' => 'TPART', 'allowBlank' => true),
        'TATITR' => array('label'	=> 'Tipologia', 'type' => 'from_TA', 'TAID' => 'TPART', 'allowBlank' => true),
        'TARIF1' => array('label'	=> 'Tipologia', 'type' => 'from_TA', 'TAID' => 'TPART', 'allowBlank' => true),
        'TARIF2' => array('label'	=> 'Tipologia', 'type' => 'from_TA', 'TAID' => 'TPART', 'allowBlank' => true),

        'TALOCA' => array('label'	=> 'Tipologia da', 'maxLength' => 10, 'fw'=>'width: 190'),
        'TACAP'  => array('label'	=> 'Tipologia a', 'maxLength' => 10, 'fw'=>'width: 190'),
        'TIPOL_DA'  => array('label'	=> 'Tipologia da-a', 'c_fw' => 'width: 100'),
        'TAINDI'    => array('label'	=> 'Etichetta colonna', 'maxLength' => 60),
        'TAPROV'    => array('label'	=> 'Formato', 'type' => 'radio_tn'),

        'TASITI' => array('label'	=> 'Tabella opzioni',  'maxLength' => 5, 'fw'=>'width: 190'),
        //TABELLA ABBINATA
        'TAB_ABB' =>  array('label' => 'opzioni', 'type' => 'TA',
            'iconCls' => 'search',
            'tooltip' => 'Tabella opzioni',
            'select' => "TA_OP.C_ROW AS TAB_ABB",
            'join' => "LEFT OUTER JOIN (
            SELECT COUNT(*) AS C_ROW, TA2.TATAID
            FROM {$cfg_mod['file_tabelle']} TA2
            WHERE TA2.TADT = '{$id_ditta_default}' AND SUBSTRING(TA2.TATAID, 1, 2) ='A#'
            GROUP BY TA2.TATAID) TA_OP
            ON TA.TASITI = TA_OP.TATAID",
            'ta_config' => array(
                'only_opz' => 'Y',
                'file_acs'     => '../desk_utility/acs_gest_TAB_OPZ_art.php?fn=open_tab',
                
            )
            ),
          //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
        'TADTGE' => array('label'	=> 'Data generazione'),
        'TAUSGE' => array('label'	=> 'Utente generazione'),
                
    
        ),
    'buttons' => array(
        array(
            'text'  => '',
            'iconCls' => 'icon-print-16',
            'tooltip' => 'Riepilogo codifica dati import',
            'scale' => 'small',
            'style' => 'border: 1px solid gray;',
            'handler' =>  new ApiProcRaw("function(){
                    window.open('acs_gest_DTIMP_report.php?fn=open_report');
             }")
        )
    )

        );


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
