<?php
require_once("../../config.inc.php");

$main_module = new DeskArt(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();
$cfg_mod = $main_module->get_cfg_mod();

//ART
$row_attav = find_TA_std('ATTAV', $m_params->TAKEY1, 1, null, null, null, $main_module);
$descrizione = $row_attav[0]['text'];

$m_table_config = array(
    'module'      => $main_module,
    'tab_name'    => $cfg_mod_DeskArt['file_tabelle'],
    'descrizione' => "To Do Entry",
    't_panel' =>  "RILAV - {$m_params->TAKEY1}",
    'form_title' => "Dettagli opzioni [{$m_params->TAKEY1}] {$descrizione}",
    'maximize'    => 'Y',
    'conferma_elimina'    => 'Y',
    'fields_preset' => array(
        'TATAID' => 'RILAV',
        'TAKEY1' => $m_params->open_request->TAKEY1
    ),
    
    'immissione' => array(
        'data_gen'   => 'TADTGE',
        'user_gen'   => 'TAUSGE',
        
    ),
   
    'fields_key' => array('TAKEY1', 'TADESC'),
    
    'fields_grid' => array('TAPESO', 'TAKEY2', 'TADESC', 'TASTAL', 'TARIF1', 'TAINDI', 'TAPROV', 'immissione'),
    'fields_form' => array('TAPESO', 'TAKEY2', 'TADESC', 'TAFG02', 'TAFG01', 'TACOGE', 'TAMAIL', 'TASTAL', 'TARIF1', 'TAINDI', 'TAPROV'),

    
    
    'fields' => array(
        'TAPESO' => array('label'	=> 'Seq.', 'c_fw'=>'width: 70','fw'=>'width: 170', 'type'=> 'numeric'),
        'TAKEY2' => array('label'	=> 'Codice', 'c_fw' => 'width: 80', 'fw'=>'width: 190', 'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione', 'fw'=>'flex: 1'),
        'TARIF1' => array('label'	=> 'Riferimento', 'fw'=>'width: 180'),
        'TAFG01' => array('label'	=> 'Notifica RI', 'fw'=>'width: 170', 'tooltip' => 'TAFG01', 'type' => 'from_TA', 'TAID' => 'ARY', 'allowBlank' => true),
        'TAFG02' => array('label'	=> 'Note obbligatorie', 'fw'=>'width: 170', 'tooltip' => 'TAFG02', 'type' => 'from_TA', 'TAID' => 'ARY', 'allowBlank' => true),
        'TATELE' => array('label'	=> 'Messaggio RI', 'fw'=>'width: 170', 'tooltip' => 'TATELE'),
        'TACOGE' => array('label'	=> 'Call programma', 'tooltip' => 'TACOGE'),
        'TAMAIL' => array('label'	=> 'Vincolo su stati', 'fw'=>'width: 170', 'tooltip' => 'TAMAIL'),
        'TASTAL' => array('label'	=> 'E/A', 'c_fw'=>'width: 40', 'fw'=>'width: 170', 'tooltip' => 'Evasione (blank o E) o Avanzamento (A)'),
        'TARIF1' => array('label'	=> 'Raggruppamento', 'short'=> 'Raggr.', 'c_fw'=>'width: 50', 'fw'=>'width: 170', 'tooltip' => 'Raggruppamento (per causali di Avanzamento)'),
        'TAINDI' => array('label'	=> 'Icona', 'tooltip' => 'Icona', 'c_fw'=>'width: 150'),
        'TAPROV' => array('label'	=> 'Formato grafico', 'short'=> 'FG', 'c_fw'=>'width: 40', 'fw'=>'width: 170', 'tooltip' => 'Formato grafico (G = sfondo giallo, R = sfondo rosso, ...)'),
        //immissione
        'immissione' => array(
            'type' => 'immissione', 'fw'=>'width: 70',
            'config' => array(
                'data_gen'   => 'TADTGE',
                'user_gen'   => 'TAUSGE'
            )
            
        ),
   
            'TADTGE' => array('label'	=> 'Data generazione'),
            'TAUSGE' => array('label'	=> 'Utente generazione'),
        
    )
);


require ROOT_ABS_PATH . 'module/base/_gest_table_std.php';
