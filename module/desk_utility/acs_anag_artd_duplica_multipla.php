<?php

require_once "../../config.inc.php";
require_once("acs_anag_artd_include.php");

$main_module = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_dup_multi'){
    
    $form_values = $m_params->form_values;
    $p_gruppo = $form_values->f_gruppo;
    $p_radice = $form_values->f_radice;
    $p_voce = $m_params->list_rows[0]->CDSLAV;
    
  
    
    $new_gruppo = '';
    for($i = 0; $i <= 19; $i++){
        $filtro = 'f_g_'.$i;
        if(trim($form_values->$filtro) != ''){
            $new_gruppo .= $form_values->$filtro;
        } else {
            $new_gruppo .= substr($p_gruppo, $i, 1);
        }
    }
    
    $new_radice = '';
    for($i = 0; $i <= 39; $i++){
        $filtro = 'f_r_'.$i;
        if(trim($form_values->$filtro) != ''){
            $new_radice .= $form_values->$filtro;
        } else {
            $new_radice .= substr($p_radice, $i, 1);
        }
    }
    
   
    

    
    foreach($m_params->list_rows as $k =>$v){
        
        $radice = "";
        
        $sql = "SELECT COUNT(*) AS C_ROW
        FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
        WHERE CDDT= '$id_ditta_default' AND CDDICL = '{$m_params->indice}'
        AND CDCMAS = '{$new_gruppo}' AND CDSLAV = '{$v->CDSLAV}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if($row['C_ROW'] > 0){
            $err_msg .= "Configurazione con gruppo {$new_gruppo} e voce {$v->CDSLAV} esistente <br>";
        }else{
            
            $ar_ins = array();
            $ar_ins['CDUSGE'] 	= $auth->get_user();
            $ar_ins['CDORGE'] 	= oggi_AS_time();
            $ar_ins['CDDTGE'] 	= oggi_AS_date();
            $ar_ins['CDUSUM'] 	= $auth->get_user();
            $ar_ins['CDORUM'] 	= oggi_AS_time();
            $ar_ins['CDDTUM'] 	= oggi_AS_date();
            $ar_ins['CDDT'] 	= $id_ditta_default;
            $ar_ins['CDDICL'] 	= $m_params->indice;
            $ar_ins['CDSEQI'] 	= $v->CDSEQI;
            $ar_ins['CDSEQU'] 	= $v->CDSEQU;
            $ar_ins['CDSTIN'] 	= $v->CDSTIN;
            $ar_ins['CDSTFI'] 	= $v->CDSTFI;
            $ar_ins['CDFLGA'] 	= $v->CDFLGA;
            $ar_ins['CDFLLG'] 	= $v->CDFLLG;
            $ar_ins['CDCDLI'] 	= $v->CDCDLI;
            $ar_ins['CDNEWC'] 	= $v->CDNEWC;
            $ar_ins['CDPROG'] 	= $v->CDPROG;
            $ar_ins['CDNCAR'] 	= $v->CDNCAR;
            $ar_ins['CDARDU'] 	= $v->CDARDU;
            $ar_ins['CDSLAV']   = $v->CDSLAV;;
            $ar_ins['CDDESC'] 	= $v->CDDESC;
            $ar_ins['CDCMAS'] 	= $new_gruppo;
            $radice = str_replace($p_voce, $v->CDSLAV, $new_radice);
            $ar_ins['CDRSTR'] 	= $radice;  //radice
            
            $err_msg = "";
            
           
         
                       
            $sql_i = "INSERT INTO {$cfg_mod_DeskArt['file_cfg_distinte']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt_i = db2_prepare($conn, $sql_i);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_i, $ar_ins);
            echo db2_stmt_errormsg($stmt_i);
        }
    }
   
    $ret = array();
    $ret['success'] = true;
    $ret['error_msg'] = $err_msg;
    echo acs_je($ret);
    exit; 
}

if ($_REQUEST['fn'] == 'open_duplica'){
 
    $p_gruppo = $m_params->list_rows[0]->CDCMAS;
    $p_radice = $m_params->list_rows[0]->CDRSTR;
           
    ?>
    
    {"success":true, "items": [

        {
            xtype: 'form',
            flex : 1,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
     		items: [
     		
     				{ 
						xtype: 'fieldcontainer',
						margin : '0 0 0 68',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
							<?php for ($i = 1; $i<=9; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 13 0 0' },
						<?php }?>	
						<?php for ($i = 10; $i<=20; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 6 0 0' },
						<?php }?>	
						
						]
						
						},
						
							{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
						
							{ 
						  xtype : 'textfield',
						  name: 'f_gruppo', 
						  hidden : true,
						  value : '<?php echo $p_gruppo; ?>',
						 },
 						 	{ 
						  xtype : 'textfield',
						  name: 'f_radice',
						  hidden : true, 
						  value : '<?php echo $p_radice; ?>',
						  },
						
						
							{ xtype: 'displayfield', value : 'Gruppo:', margin : '0 20 0 0' },
						
						<?php for ($i = 0; $i<=19; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_gruppo_<?php echo $i; ?>', 
						  width: 20,
						  readOnly : true,
						  value : '<?php echo $p_gruppo[$i]; ?>',
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
					
								
						]},
     		
     		
						{ 
						xtype: 'fieldcontainer',
						margin : '0 0 0 64',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
						//	{ xtype: 'displayfield', value : 'Gruppo:', margin : '0 20 0 0' },
						
						<?php for ($i = 0; $i<=19; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_g_<?php echo $i; ?>', 
						  width: 20,
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
					
								
						]},
						
						{ 
						xtype: 'fieldcontainer',
						margin : '0 0 0 68',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
							<?php for ($i = 1; $i<=9; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 13 0 0' },
						<?php }?>	
						<?php for ($i = 10; $i<=40; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 6 0 0' },
						<?php }?>	
						
						]
						
						},
						
							{ 
						xtype: 'fieldcontainer',
						
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
							{ xtype: 'displayfield', value : 'Radice:', margin : '0 20 0 0', width : 45 },
						
						<?php for ($i = 0; $i<=39; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_radice_<?php echo $i; ?>', 
						  readOnly : true,
						  value : '<?php echo $p_radice[$i]; ?>',
						  width: 20,
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
					
								
						]},
						
					{ 
						xtype: 'fieldcontainer',
						margin : '0 0 0 64',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
							// { xtype: 'displayfield', value : 'Radice:', margin : '0 20 0 0', width : 45 },
						
						<?php for ($i = 0; $i<=39; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_r_<?php echo $i; ?>', 
						  value : '<?php echo $radice[$i]; ?>',
						  width: 20,
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
					
								
						]},
            		
					
						
						
            ],	buttons: [
			
			{
	            text: 'Duplica',
	            iconCls: 'icon-button_black_play-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	var loc_win = this.up('window');
	            	
	            	if(form.isValid()){
	            	
	            	   	Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_dup_multi',
					        jsonData: {
					        	list_rows : <?php echo acs_je($m_params->list_rows); ?>, 
					        	indice : '<?php echo $m_params->indice; ?>'	,
					        	form_values : form.getValues(),						        	
					         },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	
					            jsonData = Ext.decode(result.responseText);
					            
					            if(jsonData.error_msg != ''){
            	 					acs_show_msg_error(jsonData.error_msg);
            	 					return;
            	 				 }else{
            	 				 	loc_win.fireEvent('afterDup', loc_win);	
            	 				 }
		        					
					            	
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					            console.log('errorrrrr');
					        }
					    });
	            	
	            		
	            	
			        } 
	                      	                	                
	            }
	        }			
	        
	        ]
            
            
            
            }
            
            
       ]}
    
    
<?php 
    exit;
}