<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$desk_art = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_conferma_mod'){
     
    $list_rows = $m_params->list_rows_modified;
    
    foreach($list_rows as $v){
        
        $ar_upd = array();
        $ar_upd['ARUSUM'] 	= $auth->get_user();
        $ar_upd['ARDTUM'] 	= oggi_AS_date();
        $ar_upd['ARDART'] = utf8_decode($v->n_desc);
                
        $sql = "UPDATE {$cfg_mod_DeskUtility['file_anag_art']} AR
        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
        WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$v->cod}'";
                
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg($stmt);
        
    }
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'open_mod'){
    
    ?>
    {"success":true, "items": [
    
 {
				xtype: 'grid',
				title: '',
		        loadMask: true,	
		        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1
		          })
		      	],
		        store: {
				xtype: 'store',
				autoLoad:true,
				
				<?php if (1==2){?>
	
						proxy: {
						   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
						   method: 'POST',								
						   type: 'ajax',

					       actionMethods: {
					          read: 'POST'
					        },
					        
					        
					           extraParams: {
								 open_request: <?php echo acs_je($m_params) ?>
	        				},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
					
					<?php }else{ ?>
					
					data : [
					
				    <?php foreach($m_params->list_selected_art as $v){
				        //ricevendo codice e descrizione dai params,
				        // i caratteri speciali (es: < o &) sono espressi in html (es: &lt;)
				        // quindi qui deve eseguire un decode.
				        // ToDo: sarebbe meglio passare il solo codice articolo e recuperare la descr da db
				        $desc = html_entity_decode(utf8_decode($v->d_art));
				         ?>
				                      
		 			 
							{cod : '<?php echo $v->c_art; ?>', 
							desc : <?php echo j($desc); ?>, 
							len : '<?php echo strlen($desc); ?>',
							n_desc : <?php echo j($desc); ?>, 
							n_len : '<?php echo strlen($desc); ?>'},

			  		<?php } ?>
			  		
			  		   ],
					
					<?php  }?>
					
        			fields: [ 'cod', 'desc' , 'len', 'n_desc' , 'n_len']							
							
	}, //store
		

	      columns: [
	
            {
            header   : 'Codice',
            dataIndex: 'cod',
            width : 120
            },
            {
             header   : 'Descrizione',
             dataIndex: 'desc',
             flex: 1,
             tdCls: 'white-space-pre font-monospace',
             renderer: function(value, p, record){             	   
             	   return Ext.util.Format.htmlEncode(value);
    		 },
            },
         	{
             header   : 'Lung.',
             dataIndex: 'len',
             width : 40
            },   {
             header   : 'Nuova descrizione',
             dataIndex: 'n_desc',
             flex: 1,
             tdCls: 'white-space-pre font-monospace',
             renderer: function(value, p, record){
             	   if (record.get('n_len') > 50)
             	   		p.tdCls += ' sfondo_rosso';
             	   return Ext.util.Format.htmlEncode(value);
    		 },
             editor: {
	                xtype: 'textfield',
	                allowBlank: true,
	                fieldCls: 'x-form-field font-monospace'	
	            }
	            
            }, 
              {
            header   : 'Lung.',
            dataIndex: 'n_len',
            width : 40
            }
            
     ], listeners: {
     
     	edit: function(editor, e, a, b){
                 
          	grid = editor.grid;
        	record = e.record;	
        	new_value =  record.data.n_desc;
        	record.set('n_len', record.data.n_desc.length);
	        if (record.get('n_len') > 50){ 
     	   				acs_show_msg_error('Presenti descrizioni oltre i 50 caratteri!');
						return value;
     	   			}

         }
     
		 }, 
		 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                    {
        			name: 'f_sos',
        			xtype: 'textfield',
        			fieldLabel: 'Sostituisci',
        			itemId : 'w_sos',
        			labelWidth : 60,
        			width : 170
    			   },  {
        			name: 'f_con',
        			xtype: 'textfield',
        			fieldLabel: 'Con',
        			itemId : 'w_con',
        			labelWidth : 30,
        			width : 150
        									
        		 	},{
                     xtype: 'button',
                    text: 'Applica',
                    iconCls: 'icon-button_black_play-32',
		            scale: 'large',	      
		            handler: function() {
		             
		               var grid = this.up('grid');
		               var rows = grid.getStore().data.items;
		               var sos = this.up('window').down('#w_sos').getValue();
		               var con = this.up('window').down('#w_con').getValue();
		              
		               id_selected = grid.getSelectionModel().getSelection();	
		                  						
					   list_desc = [];             
		                 for (var i=0; i<rows.length; i++){
		                     if(rows[i].data.n_desc.indexOf(sos) > -1){
		                        var old_desc = rows[i].data.n_desc;
		                        var new_desc = old_desc.replace(sos, con);
		                        var new_len = new_desc.length;
		                        rows[i].set('n_desc', new_desc);
		                        rows[i].set('n_len', new_len);
		                     }
		               
                             }
                         
                             
                             
                        }
		            }
		            
		            , '->',
		            {
                     xtype: 'button',
                    text: 'Reset',
		            iconCls: 'icon-button_blue_repeat-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                   		var grid = this.up('grid');
                   		this.up('window').down('#w_sos').setValue('');
                   		this.up('window').down('#w_con').setValue('');
                   		grid.getStore().load();
                	
			
			            }

			     },  {
                     xtype: 'button',
                    text: 'Conferma',
                    itemId : 'b_confirm',
		            iconCls: 'icon-save-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                      var grid = this.up('grid');
                      var loc_win = this.up('window');
                            
                      rows_modified = grid.getStore().getUpdatedRecords();
                      list_rows_modified = [];
                            
                      for (var i=0; i<rows_modified.length; i++) {
		              	list_rows_modified.push(rows_modified[i].data);
		              }
		              
		              for (var i=0; i<rows_modified.length; i++) {
		              	if(rows_modified[i].get('n_len') > 50){
		              	 acs_show_msg_error('Presenti descrizioni oltre i 50 caratteri!');
	 					 return;
	 					} 
		              }
		              
		    	       loc_win.fireEvent('afterConfirm', list_rows_modified, loc_win);
	            
					   Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_mod',
						        method     : 'POST',
			        			jsonData: {
			        				list_rows_modified : list_rows_modified
								},							        
						        success : function(result, request){
						           var jsonData = Ext.decode(result.responseText);
						           loc_win.fireEvent('afterConfirm', list_rows_modified, loc_win);								        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			
			            }

			     }
			     ]
		   }]
		 
		  ,viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			           
			           return '';																
			         }   
			    }
			       


}
    
    ]}

<?php 
exit;
}