<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_salva_traduzione'){
    
    $m_params = acs_m_params_json_decode();
    
    if($m_params->multi != 'Y'){
        
        if(isset($m_params->rrn) && trim($m_params->rrn) != ''){
               
                $ar_ins = array();
                
                /*$ar_ins['BSUSAL']   = trim($auth->get_user());
                 $ar_ins['BSDTAL']   = oggi_AS_date();
                 $ar_ins['BSORAL']   = oggi_AS_time();*/
                $ar_ins["ALDAR1"] = acs_toDb($m_params->form_values->f_text_1);
                $ar_ins["ALDAR2"] = acs_toDb($m_params->form_values->f_text_2);
                $ar_ins["ALDAR3"] = acs_toDb($m_params->form_values->f_text_3);
                $ar_ins["ALDAR4"] = acs_toDb($m_params->form_values->f_text_4);
                
                $sql = "UPDATE {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
                SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE RRN(AL) = '{$m_params->rrn}'
                ";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
            
        }else{
            
                $ar_ins = array();
                
                /*$ar_ins['BSUSAL']   = trim($auth->get_user());
                 $ar_ins['BSDTAL']   = oggi_AS_date();
                 $ar_ins['BSORAL']   = oggi_AS_time();*/
                $ar_ins["ALDAR1"] = acs_toDb($m_params->form_values->f_text_1);
                $ar_ins["ALDAR2"] = acs_toDb($m_params->form_values->f_text_2);
                $ar_ins["ALDAR3"] = acs_toDb($m_params->form_values->f_text_3);
                $ar_ins["ALDAR4"] = acs_toDb($m_params->form_values->f_text_4);
                $ar_ins["ALDT"]   = $id_ditta_default;
                $ar_ins["ALART"]  = $m_params->c_art;
                $ar_ins["ALLING"] = acs_toDb($m_params->lng);
                
                
                $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_art_trad']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
           
            
        }
    
    }else{
        
        foreach($m_params->list_selected_id as $v){
            $sql_s = "SELECT RRN(AL) AS RRN
                    FROM {$cfg_mod_DeskUtility['file_anag_art_trad']} AL
                    WHERE ALDT = '{$id_ditta_default}'
                    AND ALART = '{$v}'
                    AND ALLING = '{$m_params->lng}'";
            $stmt_s = db2_prepare($conn, $sql_s);
            $result = db2_execute($stmt_s);
            $row_s = db2_fetch_assoc($stmt_s);
           
            if($row_s['RRN'] != false && trim($row_s['RRN']) != ''){
                $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
                        WHERE RRN(AL) = '{$row_s['RRN']}'";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt);
                
            }
                $ar_ins = array();
                $ar_ins["ALDT"]   = $id_ditta_default;
                $ar_ins["ALART"]  = $v;
                $ar_ins["ALLING"] = trim($m_params->lng);
                $ar_ins["ALDAR1"] = acs_toDb($m_params->form_values->f_text_1);
                $ar_ins["ALDAR2"] = acs_toDb($m_params->form_values->f_text_2);
                $ar_ins["ALDAR3"] = acs_toDb($m_params->form_values->f_text_3);
                $ar_ins["ALDAR4"] = acs_toDb($m_params->form_values->f_text_4);
                
                
                
                $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_art_trad']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
                
            
            
            
        }
        
        
        
    }

    
    $ret = array();
    $ret['success'] = true;
    $ret['row'] = "{$m_params->form_values->f_text_1}<br>
    {$m_params->form_values->f_text_2}<br>
    {$m_params->form_values->f_text_3}<br>
    {$m_params->form_values->f_text_4}";
    echo acs_je($ret);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
  if($m_params->multi != 'Y'){
        $sql = "SELECT RRN(AL) AS RRN, ALDAR1, ALDAR2, ALDAR3, ALDAR4
        FROM {$cfg_mod_DeskUtility['file_anag_art_trad']} AL
        WHERE ALDT = '{$id_ditta_default}'
        AND ALART = '{$m_params->c_art}'
        AND ALLING = '{$m_params->lng}'";
        
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
  }else{
      $row['ALDAR1'] = "";
      $row['ALDAR2'] = "";
      $row['ALDAR4'] = "";
      $row['ALDAR5'] = "";
      
  }     
      

    
    ?>
 {"success":true, "items": [
        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            open_ajax: function(){
		            console.log(this);
		             var me = this,
		            	m_win = this.up('window');
		                var f = Ext.Ajax.request({
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_salva_traduzione',
						        jsonData: {
						        	form_values: me.getForm().getValues(),
						        	c_art : '<?php echo $m_params->c_art; ?>',
						        	lng : '<?php echo $m_params->lng; ?>',
						        	rrn : '<?php echo $row['RRN']; ?>',
						        	list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>,
						        	multi : <?php echo j($m_params->multi) ?>
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						            var jsonData = Ext.decode(result.responseText);
						          	m_win.fireEvent('afterSave', m_win, jsonData);
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						            console.log('errorrrrr');
						        }
						    });	
		            
		            },
					buttons: [{
			            text: 'Salva',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			                var m_form = this.up('form');
			            	var form = this.up('form').getForm();
			            	var form_values = form.getValues();
							var loc_win = this.up('window');
							 
							<?php if($m_params->multi == 'Y'){?>
							var trad = form_values.f_text_1.trim() + form_values.f_text_2.trim() + form_values.f_text_3.trim() + form_values.f_text_4.trim();
							var testo = 'Attenzione, nessun testo inserito!<br>Questo comporterÓ la cancellazione della traduzione degli articoli selezionati<br>Continuare?';
								if(trad == ''){
								 	Ext.Msg.confirm('Richiesta conferma', testo, function(btn, text){
                	   				if (btn == 'yes'){
                	   				    if(form.isValid()) m_form.open_ajax.call(m_form);
                	   				}
                	   				
                	   				});
            	   				}else{
            	   				    if(form.isValid()) m_form.open_ajax.call(m_form);
            	   				}
						
							<?php }else{?>
								if(form.isValid()) m_form.open_ajax.call(m_form);
							<?php }?>
					
			            }
			         }],   		            
		
		            items: [{
    					xtype: 'fieldset',
    	                title: 'Lingua : <?php echo $m_params->lng; ?>',
    	                layout: 'anchor',
    	                flex:1,
    	                items: [
    	                 
    	                <?php  for($i=1; $i<= 4;$i++){ ?>
                    	
                    	{
    						name: 'f_text_<?php echo $i ?>',
    						xtype: 'textfield',
    						fieldLabel: '',
    						anchor: '-15',
    					    maxLength: 80,					    
    					    value: '<?php echo trim(acs_u8e($row["ALDAR{$i}"])); ?>',							
    					},	
    					
    									
					<?php   }?>	
								
									
					 
	             ]}
								 
		           			]
		              }
		
			]
}	
	
	
	
<?php 
exit;
}

if ($_REQUEST['fn'] == 'open_al'){
    $m_params = acs_m_params_json_decode();
    $lingue = $cfg_mod_DeskUtility['ar_lingue'];

    if($m_params->multi != 'Y'){
        $sql = "SELECT RRN(AL) AS RRN, ALLING, ALDAR1, ALDAR2, ALDAR3, ALDAR4
        FROM {$cfg_mod_DeskUtility['file_anag_art_trad']} AL
        WHERE ALDT = '{$id_ditta_default}'
        AND ALART = '{$m_params->c_art}'
        AND ALLING = ?";
        
           
        $stmt = db2_prepare($conn, $sql);
    }
      
?>

{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: false,
	            autoScroll : true,
	            title: '',
	            
	            items: [
	          
	 	
	 	<?php 
	 	
	 	foreach($lingue as $k => $v){
	 	    
	 	    if($m_params->multi != 'Y'){
    	 	    $result = db2_execute($stmt, array($v['id']));
    	 	    $row = db2_fetch_assoc($stmt);
    	 	
    	 	    $trad = " {$row['ALDAR1']}<br>
                          {$row['ALDAR2']}<br>
                          {$row['ALDAR3']}<br>
                          {$row['ALDAR4']}";
	 	    }else $trad = "";
	 	        
	 	
	 	?>
	 	
	 			
							 {
					xtype: 'fieldset',
	                title: <?php echo j($v['text'])?>,
	                layout: 'anchor',
	                flex:1,
	                items: [
						
							 {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox'},						
						items: [
            	          		{xtype : 'displayfield',
            	          		 name : 'f_ling_<?php echo $v['id']?>',
            	          		 flex: 1,
            	          		 labelWidth : 60,
            	          		 value : <?php echo j($trad); ?>
            	          		
            	          		},
						{
								xtype: 'button',
								text: 'Modifica',
								scale: 'small',
								handler : function(){
								
								var form = this.up('form').getForm();
								
								 <?php  if($m_params->multi != 'Y'){?>
								   acs_show_win_std('Traduzione articolo ' +'<?php echo $m_params->c_art; ?>', 
				    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
				    					{c_art: '<?php echo $m_params->c_art; ?>', lng: '<?php echo $v['id']; ?>'}, 400, 270,  {
			        					'afterSave': function(from_win, new_trad){
			        					    form.findField('f_ling_<?php echo $v['id']?>').setValue(new_trad.row);
			        						from_win.close();
			        					}
			        				}, 'icon-globe-16');
								 <?php }else{?>
								   acs_show_win_std('Traduzione articoli', 
				    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
				    					{list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>, lng: '<?php echo $v['id']; ?>', multi : 'Y'}, 400, 270,  {
			        					'afterSave': function(from_win, new_trad){
			        					    form.findField('f_ling_<?php echo $v['id']?>').setValue(new_trad.row);
			        						from_win.close();
			        					}
			        				}, 'icon-globe-16');
								 
								 <?php }?>
			        			
								 
			        		
									
								} 
							}
						    
						
						]}	   
 					
					 
	             ]},
						
						<?php }	?>
						    
						    
	           
	          		
	            ]
	                     
	            
	           
	}
		
	]}

<?php 
exit;
}