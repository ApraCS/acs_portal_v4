<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

ini_set('max_execution_time', 90000);

function options_select_fornitore($mostra_codice = 'N', $div){
	global $conn;
	global $cfg_mod_DeskUtility, $id_ditta_default;
	$ar = array();

	$sql = "SELECT SFCFO0, SFRAG0 FROM {$cfg_mod_DeskUtility['file_fornitori']} AF
	WHERE SFDIT0 = '$id_ditta_default' AND SFDIV0 = '{$div}'";


	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		if ($mostra_codice == 'Y')
			$text = "[" . $row['SFCFO0'] . "] " . $row['SFRAG0'];
			else $text = $row['SFRAG0'];
	 	$ret[] = array( "id" 	=> $row['SFCFO0'],
	 			"text" 	=> $text );
	}
	return $ret;
}

if ($_REQUEST['fn'] == 'exe_ric_bts'){
	$ret = array();
	$m_params = acs_m_params_json_decode();


	$sh = new SpedHistory($m_DeskAcq);
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> 'SCO_RIC_BTS',
					"vals" => array("RICITI" =>$m_params->values->form_values->f_divisione),
					
			)
			);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}




if ($_REQUEST['fn'] == 'exe_sincronizza'){
	$ret = array();
	$m_params = acs_m_params_json_decode();


	$sh = new SpedHistory($m_DeskAcq);
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> 'SCO_SAVE_FOR',
					"vals" => array("RICITI" =>$m_params->values->form_values->f_divisione),
			)
			);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// EXE DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->form_values;
	
	$sql = "DELETE FROM {$cfg_mod_DeskUtility['file_fornitori']}
			WHERE SFDIT0='$id_ditta_default' AND SFDIV0='{$form_values->SFDIV0}' AND SFCFO0 = '{$form_values->SFCFO0}' ";	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$error_msg =  db2_stmt_errormsg($stmt);
	
	$sh = new SpedHistory($m_DeskAcq);
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> 'SCO_DEL_FOR',
					"vals" => array("RICITI" =>$form_values->SFDIV0, "RIART" =>$form_values->SFCFO0)
				
			)
			);
	
	$ret = array();
	$ret['success'] = $result;
	if (strlen($error_msg) > 0) {
		$ret['success'] = false;
		$ret['message'] = $error_msg;
	}
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// EXE SAVE FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_forn'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->form_values; 

	$ar_upd = array();
	$ar_upd['SFUTM0'] 	= $auth->get_user();
	$ar_upd['SFDTM0'] 	= oggi_AS_date();
	$ar_upd['SFORM0'] 	= oggi_AS_time();
	$ar_upd['SFGGT0'] 	= (float)$form_values->SFGGT0;
	$ar_upd['SFCST0'] 	= (float)$form_values->SFCST0;
	$ar_upd['SFLSH0'] 	= (float)$form_values->SFLSH0;
	$ar_upd['SFLSL0'] 	= (float)$form_values->SFLSL0;
	$ar_upd['SFWLR0'] 	= (float)$form_values->SFWLR0;
	if(!isset($form_values->SFTGE0_cb)){
		$ar_upd['SFTGE0'] = 'D';
	}elseif(isset($form_values->SFTGE0_cb) && $form_values->SFTGE0_cb == 'C'){
		$ar_upd['SFTGE0'] = 'C';
	}else{
		$ar_upd['SFTGE0'] = 'D';
	}

	if(isset($form_values->SFCMA0)  && strlen($form_values->SFCMA0) > 0 && $form_values->SFTGE0_cb == 'C'){
		$ar_upd['SFCMA0'] 	= $form_values->SFCMA0;
	}else{
		$ar_upd['SFCMA0'] 	= '';
	}
	$ar_upd['SFBTO0'] 	= (float)sql_f($form_values->SFBTO0);
	
	$ar_upd['SFBTP0'] 	= (float)$form_values->SFBTP0;
	$ar_upd['SFTRF0'] 	= $form_values->SFTRF0;
    $ar_upd['SFTRS0'] 	= (float)$form_values->SFTRS0;
    
	$sql = "UPDATE {$cfg_mod_DeskUtility['file_fornitori']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE SFDIT0='$id_ditta_default' AND SFDIV0='{$form_values->SFDIV0}' AND SFCFO0 = '{$form_values->SFCFO0}' ";

	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_upd);
	echo db2_stmt_errormsg($stmt);


	$sql_s = "SELECT RF.*, TA_HR.TADESC AS HR_DESC, TA_LR.TADESC AS LR_DESC
				FROM {$cfg_mod_DeskUtility['file_fornitori']} RF
					LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tabelle_acq']} TA_HR
						ON SFDIT0 = TA_HR.TADT AND SFLSH0 = TA_HR.TAKEY1  AND TA_HR.TATAID = 'LIVSR'
					LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tabelle_acq']} TA_LR
						ON SFDIT0 = TA_LR.TADT AND SFLSL0 = TA_LR.TAKEY1  AND TA_LR.TATAID = 'LIVSR'
	 			WHERE SFDIT0='$id_ditta_default' AND SFDIV0='{$form_values->SFDIV0}' AND SFCFO0 = '{$form_values->SFCFO0}' ";
	
	$stmt_s = db2_prepare($conn, $sql_s);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_s);
	echo db2_stmt_errormsg($stmt_s);
	
	$row = db2_fetch_assoc($stmt_s);
	
	if($row['SFTGE0'] == 'C'){
		$row['SFTGE0_cb'] = true;
	}else{
		$row['SFTGE0_cb'] = false;
	}
	
	if($row['HR_DESC'] != 0)
		$row['HR_DESC'] = substr(trim($row['HR_DESC']), 0, 4);
	if($row['LR_DESC'] != 0)
		$row['LR_DESC'] = substr(trim($row['LR_DESC']), 0, 4);

	$ret = array();
	$ret['success'] = true;
	$ret['record'] = $row;
	echo acs_je($ret);
	exit;
}

// ******************************************************************************************
// EXE ADD FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_add_forn'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();
	
	$record = $m_params->record;

	$ar_ins = array();
	$ar_ins['SFUTM0'] 	= $auth->get_user();
	$ar_ins['SFDTM0'] 	= oggi_AS_date();
	$ar_ins['SFORM0'] 	= oggi_AS_time();
	$ar_ins['SFDIT0'] 	= $id_ditta_default;
	$ar_ins['SFDIV0'] 	= $record->SADIV0;
	$ar_ins['SFAZI0'] 	= $record->SAAZI0;
	$ar_ins['SFCFO0'] 	= $record->SACFO0;
	$ar_ins['SFRAG0'] 	= $record->SARAG0;
		
	
	$sql = "INSERT INTO {$cfg_mod_DeskUtility['file_fornitori']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	echo db2_stmt_errormsg($stmt);
		

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}

if ($_REQUEST['fn'] == 'grid_fornitori'){
	
	$m_params = acs_m_params_json_decode();
	
	
	$forn = $m_params->open_request->form_values->f_fornitore;
	$magaz = $m_params->open_request->form_values->f_magazzino;
	
	if (strlen($forn) > 0)
		$sql_where.= " AND UPPER(SFCFO0) LIKE '%" . strtoupper($forn) . "%' ";
	
	if (strlen($magaz) > 0)
	$sql_where.= " AND SFCMA0 = '{$magaz}' ";

	
	$sql = "SELECT SFDIV0, SFAZI0, SFCFO0, SFRAG0, SFGGT0, SFCST0, SFLSH0, SFLSL0, SFWLR0, SFTGE0,
			SFCMA0, SFBTO0, SFBTP0, SFTRF0, SFTRS0, TA_HR.TADESC AS HR_DESC, TA_LR.TADESC AS LR_DESC
			FROM {$cfg_mod_DeskUtility['file_fornitori']} RF
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tabelle_acq']} TA_HR
					ON SFDIT0 = TA_HR.TADT AND SFLSH0 = TA_HR.TAKEY1  AND TA_HR.TATAID = 'LIVSR'
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tabelle_acq']} TA_LR
					ON SFDIT0 = TA_LR.TADT AND SFLSL0 = TA_LR.TAKEY1  AND TA_LR.TATAID = 'LIVSR'
	        WHERE SFDIT0 = '$id_ditta_default' AND SFDIV0 = '{$m_params->open_request->form_values->f_divisione}' {$sql_where}
			ORDER BY SFRAG0";


	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['SFCFO0'] = trim($row['SFCFO0']);
		$nr['SFRAG0'] = trim($row['SFRAG0']);
		$nr['SFDIV0'] = trim($row['SFDIV0']);
		$nr['SFAZI0'] = trim($row['SFAZI0']);
		$nr['SFGGT0'] = trim($row['SFGGT0']);
		$nr['SFCST0'] = trim($row['SFCST0']);
		
		if($row['HR_DESC'] != 0)
			$nr['HR_DESC'] = substr(trim($row['HR_DESC']), 0, 4);
		if($row['LR_DESC'] != 0)
			$nr['LR_DESC'] = substr(trim($row['LR_DESC']), 0, 4);
		
		$nr['SFLSH0'] = trim($row['SFLSH0']);
		$nr['SFLSL0'] = trim($row['SFLSL0']);
		$nr['SFWLR0'] = trim($row['SFWLR0']);
		$nr['SFTGE0'] = trim($row['SFTGE0']);
		if ($nr['SFTGE0'] == 'C')
			$nr['SFTGE0_cb'] = true;
		else
			$nr['SFTGE0_cb'] = false;
		$nr['SFCMA0'] = trim($row['SFCMA0']);
		$nr['SFBTO0'] = trim($row['SFBTO0']);
		$nr['SFBTP0'] = trim($row['SFBTP0']);
		$nr['SFTRF0'] = trim($row['SFTRF0']);
		$nr['SFTRS0'] = trim($row['SFTRS0']);
		$ar[] = $nr;
	
	
	}
	
	
	
	echo acs_je($ar);
	exit;
}

if ($_REQUEST['fn'] == 'grid_add_fornitori'){

	$m_params = acs_m_params_json_decode();
	$divisione = $m_params->open_request->divisione;  

	$sql = "SELECT SADIV0, SAAZI0, SACFO0, SARAG0 FROM {$cfg_mod_DeskUtility['file_fornitori_esistenti']} AF
	WHERE SADIT0 = '$id_ditta_default' AND SADIV0 = '{$divisione}'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['SACFO0'] = trim($row['SACFO0']);
		$nr['SARAG0'] = trim($row['SARAG0']);
		$nr['SAAZI0'] = trim($row['SAAZI0']);
		$nr['SADIV0'] = trim($row['SADIV0']);
	
		$ar[] = $nr;

	}

	echo acs_je($ar);
	exit;
}


if ($_REQUEST['fn'] == 'open_tab'){
	
	$m_params = acs_m_params_json_decode();
	
	$divisione = $m_params->form_values->f_divisione;
	$main_module->setup_scorte_save_divisione($divisione);

	?>
	
	{
		success:true,
		items: [
		{
			xtype: 'panel',
			title: 'Fornitori [' + '<?php echo $divisione?>' + ']',
			<?php echo make_tab_closable(); ?>,
			   bbar: ['->', {
							xtype: 'button',
				            text: 'Sincronizza',
				            iconCls: 'icon-button_blue_play-32', 
				            scale: 'large',
				            handler: function() {
				            	var panel = this.up('panel');
				            	
				            	acs_show_win_std('Sincronizzazione', 'acs_submit_job.php?fn=open_form', { 
	           						chiave : {RIRGES:'SCO_SAVE_FOR', RICITI :  <?php echo j($m_params->form_values->f_divisione) ?> },
	           						vals   : {RIRGES:'SCO_SAVE_FOR', RICITI :  <?php echo j($m_params->form_values->f_divisione) ?> }
	           						} , 650, 250, {}, 'icon-listino');
				          
				            }
				        }, {
							xtype: 'button',
				            text: 'Ricalcola buy to stock',
				            iconCls: 'icon-button_blue_play-32', 
				            scale: 'large',
				            handler: function() {
				            	var panel = this.up('panel');
				            	
				            	acs_show_win_std('Ricalcola buy to stock', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_sincro', {form_values: <?php echo acs_je($m_params) ?>} , 350, 180, {}, 'icon-listino');
				            	
				            	  /*acs_show_win_std('Ricalcolo buy to stock', 'acs_submit_job.php?fn=open_form', { 
	           						chiave : {RIRGES:'SCO_RIC_BTS', RICITI :  <?php echo j($m_params->form_values->f_divisione) ?> },
	           						vals   : {RIRGES:'SCO_RIC_BTS', RICITI :  <?php echo j($m_params->form_values->f_divisione) ?> }
	           						} , 650, 250, {}, 'icon-listino');
				            	*/
				            }
				        }], 
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
			
		items: [  
		
		{
			xtype: 'grid',
			title: 'Lista fornitori',
			flex: 0.7,
	        loadMask: true,	
	      
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
			
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_fornitori', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['SFCFO0', 'SFRAG0', 'SFDIV0', 'SFAZI0', 'SFGGT0', 'SFCST0', 'SFLSH0', 'SFLSL0', 'SFWLR0', 
		        			'SFTGE0', 'SFTGE0_cb', 'SFCMA0', 'SFBTO0', 'SFBTP0', 'SFTRF0', 'SFTRS0', 'HR_DESC', 'LR_DESC']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Fornitore',
	                dataIndex: 'SFRAG0',
	                flex:1,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'GG <br>Trasp.',
	                dataIndex: 'SFGGT0',
	                width: 50,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Cons. <br> settim.',
	                dataIndex: 'SFCST0',
	                width: 50,
	                renderer: floatRenderer2,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'LS% <br>HR',
	                dataIndex: 'HR_DESC',
	                width: 50,
	                filter: {type: 'string'}, filterable: true
	              //  renderer: floatRenderer2
	                }, {
	                header   : 'LS% <br>LR',
	                dataIndex: 'LR_DESC',
	                width: 40,
	                filter: {type: 'string'}, filterable: true
	               // renderer: floatRenderer2
	                },{
	                header   : 'Week <br> lotto',
	                dataIndex: 'SFWLR0',
	                width: 40,
	                renderer: floatRenderer0N,
	                filter: {type: 'string'}, filterable: true
	                },{
	                header   : 'Gest.',
	                dataIndex: 'SFTGE0',
	                width: 40,
	                filter: {type: 'string'}, filterable: true
	                },{
	                header   : 'Magaz.',
	                dataIndex: 'SFCMA0',
	                width: 50,
	                filter: {type: 'string'}, filterable: true
	                },{
	                header   : 'Soglia <br> BTO',
	                dataIndex: 'SFBTO0',
	                width: 50,
	                renderer: floatRenderer3,
	                filter: {type: 'string'}, filterable: true
	                }, {
	                header   : 'Soglia <br> BTO CV%',
	                dataIndex: 'SFBTP0',
	                width: 60,
	                filter: {type: 'string'}, filterable: true
	                },
	                 {
	                header   : 'Trasfer.',
	                dataIndex: 'SFTRF0',
	                width: 50,
	                filter: {type: 'string'}, filterable: true
	                },
	                 {
	                header   : 'Trasf <br> sett.',
	                dataIndex: 'SFTRS0',
	                width: 50,
	                filter: {type: 'string'}, filterable: true
	                }
	         ], 
	         
	         listeners: {
	         
	        		 
		                selectionchange: function(selModel, selected) {     
		                
		                    form = this.up('panel').down('form');     
		                 
		                	//pulisco eventuali filtri
		                	form.getForm().reset();
		                
		                	//ricarico i dati della form
		                    form.getForm().loadRecord(selected[0]);


		                }
	
			
		}
		
		
		},		//grid
		
		{
 		            xtype: 'form',
 		            title: 'Fornitore',
 		        	autoScroll: true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: false,
 		            buttons: [
 		            
 		            {
 			            text: 'Elimina',
 			            iconCls: 'icon-sub_red_delete-16',
 			            scale: 'small',
 			            handler: function() {
 			                var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        if (jsonData.success == true){
							        	var gridrecord = grid.getSelectionModel().getSelection();
							        	grid.store.remove(gridrecord);							        
							        }
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    }); 		            
 			            }
 			        }, {xtype: 'tbfill'},
 		            
 		            {
 			            text: 'Aggiungi',
 			            iconCls: 'icon-button_blue_play-16',
 			            scale: 'small',
 			            handler: function() {
 			            	grid = this.up('panel').up('panel').down('grid'); 
 			            	var last_row = grid.getStore().getCount()-1;
 			            	my_listeners = {
				        			    afterAdd: function(record, from_win){
				        				var rec = record.data;
				        				var values = {SFDIV0: rec.SADIV0, SFAZI0: rec.SAAZI0, SFCFO0: rec.SACFO0, SFRAG0: rec.SARAG0};
		        						grid.getStore().add(values);
		        						var last_row = grid.getStore().getCount()-1;
		        						grid.getSelectionModel().select(last_row);
	 									from_win.close();
	 											
								        	}
										}
 			            
 			      			acs_show_win_std('Tabella fornitori', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_forn', {divisione : '<?php echo $divisione; ?>'}, 500, 600, my_listeners, 'icon-listino');
 			            }
 			         },{
 			            text: 'Salva',
 			            iconCls: 'icon-button_blue_play-16',
 			            scale: 'small',
 			            handler: function() {
 			                var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_forn',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        var new_rec = jsonData.record;
							        var gridrecord = grid.getSelectionModel().getSelection();
							        console.log(gridrecord);
							        console.log(new_rec);
							        gridrecord[0].set(new_rec);
							        
							        
			            			
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
 			            }
 			         }],   		            
 		            
 		            items: [
 									 {
 									xtype: 'fieldset',
 					                title: 'Riferimenti',
 					                defaultType: 'textfield',
 					                layout: 'anchor',
 					              
 					             	items: [{
 										name: 'SFDIV0',
 										xtype: 'textfield',
 										fieldLabel: 'Divisione',
 										value: '',
 										readOnly: true,
 									    anchor: '-15'							
 									 },{
 										name: 'SFAZI0',
 										xtype: 'textfield',
 										fieldLabel: 'Azienda',
 										value: '',
 										readOnly: true,
 									    anchor: '-15'							
 									 },{
 										name: 'SFCFO0',
 										xtype: 'textfield',
 										fieldLabel: 'Fornitore',
 										value: '',
 										readOnly: true,
 									    anchor: '-15'							
 									 },{
 										name: 'SFRAG0',
 										xtype: 'textfield',
 										fieldLabel: 'Ragione sociale',
 										value: '',
 										readOnly: true,
 									    anchor: '-15'							
 									 }
 									 ]
 									 },{
 										name: 'SFGGT0',
 										xtype: 'textfield',
 										fieldLabel: 'GG Trasporto',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SFCST0',
 										xtype: 'textfield',
 										fieldLabel: 'Consegne settimanali',
 										labelWidth: 130,
 										value: '',
 									    anchor: '-15'							
 									 }
 
 
 
								 
								, {
									name: 'SFLSH0',
									xtype: 'combo',
									fieldLabel: 'Liv. di servizio HR',
									forceSelection: true,								
									displayField: 'text',
									valueField: 'id',								
									emptyText: '- seleziona -',
							   		allowBlank: false,								
								    anchor: '-15',
								    labelWidth: 130,
									store: {
										editable: false,
										autoDestroy: true,
									    fields: [{name:'id'}, {name:'text'}],
									    data: [								    
									     <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('LIVSR'), ""); ?>	
									    ]
									}
								 }
 
								, {
									name: 'SFLSL0',
									xtype: 'combo',
									fieldLabel: 'Liv. di servizio LR',
									forceSelection: true,								
									displayField: 'text',
									valueField: 'id',								
									emptyText: '- seleziona -',
							   		allowBlank: false,								
								    anchor: '-15',
								    labelWidth: 130,
									store: {
										editable: false,
										autoDestroy: true,
									    fields: [{name:'id'}, {name:'text'}],
									    data: [								    
									     <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('LIVSR'), ""); ?>	
									    ]
									}
								 } 
 
 
 
 
 									 
/* 									 
 									 ,{
 										name: 'SFLSH0',
 										xtype: 'textfield',
 										fieldLabel: 'Liv. di servizio HR',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SFLSL0',
 										xtype: 'textfield',
 										fieldLabel: 'Liv. di servizio LR',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 }
*/ 									 
 									 
 									 
 									 
 									 
 									 
 									 
 									 ,{
 										name: 'SFWLR0',
 										xtype: 'textfield',
 										fieldLabel: 'Week lotto riordino',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 }, {
														 
								name: 'SFTGE0',
								xtype: 'checkboxgroup',
								fieldLabel: 'Tipo gestione',
								allowBlank: true,
							   	labelWidth: 130,
							   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'SFTGE0_cb' 
		                          , boxLabel: 'centralizzato'
		                          , checked: false
		                          , inputValue: 'C'
		                       	  , listeners: {
								    change: function(checkbox, newValue, oldValue, eOpts) {
								    	var m_form = this.up('form');
								   
								    	var combo_magaz = m_form.down('#magaz_c');
								    	
			                    	    if (newValue == false)
			                    	    	combo_magaz.disable();
			                    	    else
			                    	    	combo_magaz.enable();	
			                    	  
								    }
								}
		                        }]														
						 },
						  {
									name: 'SFCMA0',
									xtype: 'combo',
									itemId: 'magaz_c',
									fieldLabel: 'Magazzino centraliz.',
									forceSelection: true,								
									displayField: 'text',
									valueField: 'id',								
									emptyText: '- seleziona -',
							   		allowBlank: false,								
								    anchor: '-15',
								    labelWidth: 130,
									store: {
										editable: false,
										autoDestroy: true,
									    fields: [{name:'id'}, {name:'text'}],
									    data: [								    
									     <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('SCMAG', null, 'N', 'N', $divisione), ''); ?>	
									    ]
									}
								 }
								 
								 
								,	{
										name: 'SFBTO0',
										xtype: 'numberfield', 
										hideTrigger:true,
										fieldLabel: 'Soglia BTO',
										decimalPrecision: 3,
 										labelWidth: 130,
 									    anchor: '-15'
									}								 
								 
								 								 
								 
								 
								 ,{
 										name: 'SFBTP0',
 										xtype: 'textfield',
 										fieldLabel: 'Soglia BTO CV%',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SFTRF0',
 										xtype: 'textfield',
 										fieldLabel: 'Trasferimento',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 },{
 										name: 'SFTRS0',
 										xtype: 'textfield',
 										fieldLabel: 'Settimana trasferim.',
 										value: '',
 										labelWidth: 130,
 									    anchor: '-15'							
 									 }
 									 
 									 

 										 
 		           ]
 		              }
	  		  
	  	]
	  }
	 ]
	}
<?php exit; 
}

if ($_REQUEST['fn'] == 'open_forn'){ 

	$m_params = acs_m_params_json_decode();
?>

{
		success:true,
		items: [
		{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
			 tbar: ['->', {
							xtype: 'button',
				            text: 'Aggiorna',
				           // iconCls: 'icon-button_blue_play-16', 
				            scale: 'small',
				            handler: function() {
				            	//this.up('panel').down('grid').getStore().load();
				            	acs_show_win_std('Aggiornamento elenco fornitori', 'acs_submit_job.php?fn=open_form', { 
				           						chiave : {RIRGES:'SCO_RIC_FOR', RICITI :  <?php echo j($m_params->divisione) ?> },
				           						vals   : {RIRGES:'SCO_RIC_FOR', RICITI :  <?php echo j($m_params->divisione) ?> }
				           						} , 650, 250, {}, 'icon-listino');
				            }
				        }], 
		items: [  
		
		{
			xtype: 'grid',
			flex:1,
	        loadMask: true,	
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
			
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_add_fornitori', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['SACFO0', 'SARAG0', 'SAAZI0', 'SADIV0']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Divisione',
	                dataIndex: 'SADIV0',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Azienda',
	                dataIndex: 'SAAZI0',
	                flex: 1,
					filter: {type: 'string'}, filterable: true,
	                
	                },
	                {
	                header   : 'Fornitore',
	                dataIndex: 'SACFO0',
	                flex: 1,
					filter: {type: 'string'}, filterable: true,
	                
	                },
	                {
	                header   : 'Ragione sociale',
	                dataIndex: 'SARAG0',
	                flex: 1,
					filter: {type: 'string'}, filterable: true,
	                
	                }
	         ], 
	         
	         listeners: {
	         
	          celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var loc_win = this.up('window');
					  	
					  	Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_add_forn',
						     	timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				record : rec.data
								},							        
						        success : function(result, request){
			            			loc_win.fireEvent('afterAdd', rec, loc_win);	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
	           
	            	}
	          
	           }
	         

	         }
	   
		},		//grid
		
		
	  		  
	  	]
	  }
	 ]
	}



<?php exit; }


if ($_REQUEST['fn'] == 'form_sincro'){
	$m_params = acs_m_params_json_decode();
	$fornitore = $m_params->form_values->form_values->f_fornitore;
	$divisione = $m_params->form_values->form_values->f_divisione;
	?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
				items: [
					 {
							name: 'f_hold',
							xtype: 'radiogroup',
							//fieldLabel: 'Hold',
							itemId: 's_check',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: 'A'
		                          , width: 50
		                          <?php if(strlen($fornitore)>0){?>
		                          , checked: false
		                          <?php }else{?>
		                          , checked: true
		                          <?php } ?>
		                          , listeners :{
		                           change: function(comp) {
		                          			var s_forn = this.up('form').down('#s_forn');
		                          			
		                          			if( comp.inputValue == 'A')
		                          				s_forn.enable();
		                          		    else
		                          		        s_forn.disable();
		                          		}
		                           
		                          }	
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Fornitore'
		                          , inputValue: 'F'
		                          <?php if(strlen($fornitore)>0){?>
		                          , checked: true
		                          <?php } ?>
		                         , listeners :{
		                           change: function(comp) {
		                          			var s_forn = this.up('form').down('#s_forn');
		                          			
		                          			if( comp.inputValue == 'F')
		                          				s_forn.disable();
		                          		    else
		                          		        s_forn.enable();
		                          		}
		                           
		                          }	
		                   
		                        }]
						}, {
						name: 'f_fornitore',
						xtype: 'combo',
						itemId: 's_forn',
						fieldLabel: 'Fornitore',
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona -',
		                value: '<?php echo $fornitore; ?>',							
					    anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json(options_select_fornitore('Y', $divisione), ''); ?>	
						    ]
						}, listeners :{
		                          afterrender: function(comp) {
		                          			var s_check = this.up('form').down('#s_check');
		                          			console.log(s_check.getValue().f_hold);
											if(s_check.getValue().f_hold == 'A')
		                          				comp.disable();
		                          		    
		                          		}
		                           
		                          }	
					     },{						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Solo articoli nuovi',
							labelAlign: 'left',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_art_n' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'N'
		                          
		                        }]														
						 }
					 ],
					 
			buttons: [					
				{
		            text: 'Conferma',
			        iconCls: 'icon-folder_search-32',		            
			        scale: 'large',            
		            handler: function() {
		             var form = this.up('form').getForm();
		             var form_values = form.getValues();
		             var win = this.up('window');
		             
		             console.log(form_values.f_fornitore);
		           	if (Ext.isEmpty(form_values.f_fornitore)){  
		           	  forn = '';
		           	}else{
		           	  forn = form_values.f_fornitore;
		           	}
		           	
		           	
		           	if (Ext.isEmpty(form_values.f_art_n)){  
		           	  v_art = '';
		           	}else{
		           	  v_art = form_values.f_art_n;
		           	}
		           	
		           	if(form_values.f_hold == 'F' && Ext.isEmpty(form_values.f_fornitore)){
		           		acs_show_msg_error('Selezionare un fornitore per procedere');
		              	return false;
		           	}
		          
		               
		             acs_show_win_std('Ricalcola buy to stock', 'acs_submit_job.php?fn=open_form', { 
           				chiave : {RIRGES:'SCO_RIC_BTS', RICITI :  <?php echo j($m_params->form_values->form_values->f_divisione) ?>},
           				vals   : {RIRGES:'SCO_RIC_BTS', RICITI :  <?php echo j($m_params->form_values->form_values->f_divisione) ?>, RIART : forn, RIFG01 : v_art}
           				} , 650, 250, {}, 'icon-listino');
		       
		                
		            }
		        }
	        
	        
	        ]  
					
					
	}
	
]}


<?php 
}