<?php

require_once "../../config.inc.php";

$m_DeskArt = new DeskArt(array('no_verify' => 'Y'));
$main_module = new DeskUtility();
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'open_form'){
    
    ?>
	    {"success":true, 
    	    m_win: {
    		title: 'Sostituzione avanzata',
    		iconCls: 'icon-leaf-16'
    	    },
	    items: 
	    {
        xtype: 'form',
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        buttons: [
        		{
	            text: 'Rimuovi regola',
	            iconCls: 'icon-sub_red_delete-32',
	            scale: 'large',
	            handler: function() {
	                var loc_win = this.up('window');
	            	var form = this.up('form').getForm();	            	
					loc_win.fireEvent('afterConfirm', loc_win, []);   
					 
			      }
			     }
        	  , '->', {
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',
	            handler: function() {
	                var loc_win = this.up('window');
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
	            	
	            	var n_selezioni = 0;
	            	if (!Ext.isEmpty(form_values.f_char)) n_selezioni +=1;
	            	if (!Ext.isEmpty(form_values.f_stringa)) n_selezioni +=1;
	            	if (!Ext.isEmpty(form_values.f_pos)) n_selezioni +=1;
	            	if (!Ext.isEmpty(form_values.f_rimuovi)) n_selezioni +=1;
	            	if (n_selezioni != 1) {
	            		acs_show_msg_error('Indicare una sola regola di sostituzione');
	            		return false;
	            	}	            	
					loc_win.fireEvent('afterConfirm', loc_win, form_values);   					
			      }
			     }],   		
			  items: [
                       {
                           xtype:'textfield'
                    	   , fieldLabel: 'Sostituisci con carattere'
                    	   , labelWidth : 150
                    	   , width : 180
                    	   , name: 'f_char'
                    	   , maxLength : 1
                    	   , value : <?php echo j($m_params->m_values->f_char)?>
                    	   , listeners: {
        						'change': function(field){
        						
        						 value = this.getValue().toString();
        						if (value.length == 1) {
                                    var nextfield = field.nextSibling();
                                    nextfield .focus(true, 0);
                                  } 
                                  
                                  field.setValue(field.getValue().toUpperCase());
          							
        					  }}
                       },{
                           xtype:'textfield'
                    	   , fieldLabel: 'Sostituisci con stringa'
                    	   , labelWidth : 150
                    	   , name: 'f_stringa'
                    	   , anchor : '-15'
                    	   , maxLength : 10 
                    	   , value : <?php echo j($m_params->m_values->f_stringa)?>
                    	   , listeners: {
        						'change': function(field){
        						
                                  field.setValue(field.getValue().toUpperCase());
          							
        					  }}
                       },
                        {
						xtype: 'checkboxgroup',
						flex: 1,
						fieldLabel: 'Rimuovi',
						labelWidth : 150,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_rimuovi' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                          , checked : <?php if($m_params->m_values->f_rimuovi == 'Y') echo j(true); else echo j(false)?>
                        }]							
					   },
					   {
                           xtype:'textfield'
                    	   , fieldLabel: 'Sostituisci con il carattere alla posizione'
                    	   , labelWidth : 150
                    	   , name: 'f_pos'
                    	   , width : 180
                    	   , maxLength : 1
                    	   , value : <?php echo j($m_params->m_values->f_pos)?>
                       },
	             	  ]
					
			    	
        
        }
	    
	    }
<?php 
exit;
}