<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$deskArt = new DeskArt();

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_aggiorna'){
    $ret = array();
    
    $ar_upd = array();
    $ar_upd['ERFG01'] 	= 'A';
    
    foreach($m_params->list_selected_id as $v){
        
        $sql = "UPDATE {$cfg_mod_DeskArt['file_esito_raffronto']} ER
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE RRN(ER) = '{$v->RRN}' ";
    
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg($stmt);
     
    }
    
    $sh = new SpedHistory($deskArt);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_RAFFRONTO',
            "vals" => array(
                "RICDOLD" => $m_params->cod_art_raffronto,
                "RINOTR" => $m_params->ritime,
                "RIPRZ" => $m_params->ritime)
            
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

if ($_REQUEST['fn'] == 'exe_raffronta'){
    $ret = array();
    $ritime = microtime(true);
    foreach($m_params->list_selected_id as $v){
        
        $sh = new SpedHistory($deskArt);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'RAFF_ARTI',
                "use_session_history" => $ritime,
                "vals" => array("RICDOLD" => $m_params->cod_art_raffronto,
                                "RICDNEW" => $v)
                 
            )
            );
    }
    
    $sh = new SpedHistory($deskArt);
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'RAFF_ARTI',
            "end_session_history" => $ritime
        )
        );
    
    $ret['success'] = true;
    $ret['RITIME'] = trim($ret_RI['RITIME']);
    echo acs_je($ret);
    exit;
    
}

//----------------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//----------------------------------------------------------------------------

    $sql= "SELECT RRN(ER) AS RRN, ER.* FROM {$cfg_mod_DeskArt['file_esito_raffronto']} ER
           WHERE ERDT = '{$id_ditta_default}' AND ERTIME = '{$m_params->ritime}'";
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while ($row= db2_fetch_assoc($stmt)) {
        $ar[] = $row;
    }
    
    echo acs_je($ar);
    exit;
}

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_grid'){
//----------------------------------------------------------------------
   ?>
{"success":true, 
	m_win: {
		title: 'Confronto con dati anagrafici articolo ' + <?php echo j($m_params->cod_art_raffronto); ?>,
		width: 1000, height: 500,
		iconCls: 'icon-folder_search-16'
	}, 
	items: 
	{
				xtype: 'grid',
				flex: 1,
		        useArrows: true,
		        autoScroll : true,
		        rootVisible: false,
		        loadMask: true,
		        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
		   		store: {
					xtype: 'store',
					autoLoad:true,
					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
		                        extraParams: {
									ritime : <?php echo j($m_params->ritime); ?>
									
		        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['RRN', 'ERCARF', 'ERCARA', 'ERFILE', 'ERCAMP','ERDCAM', 'ERVAL1', 'ERVAL2'],
			
			}, //store
	
	           
				columns: [ 
				         {
			                header   : 'Articolo',
			                dataIndex: 'ERCARA', 
			                width: 100
			             },
			              {
			                header   : 'File',
			                dataIndex: 'ERFILE', 
			                width: 100
			             },{
			                header   : 'Campo',
			                dataIndex: 'ERCAMP', 
			                width: 100
			             },
			              {
			                header   : 'Descrizione campo',
			                dataIndex: 'ERDCAM', 
			                 flex: 1
			             },{
			                header   : 'Articolo ' + <?php echo j($m_params->cod_art_raffronto)?>,
			                dataIndex: 'ERVAL1', 
			                flex: 1
			             },{
			                header   : 'Dato raffrontato diverso',
			                dataIndex: 'ERVAL2', 
			                flex: 1
			             }
			
				
				],
				  dockedItems: [{
                    dock: 'bottom',
                    xtype: 'toolbar',
                    scale: 'large',
                    items: [
                        { xtype: 'button',
		        		  text: 'Report',
		        		  iconCls: 'icon-print-24',
			              scale: 'medium',
			              handler: function() {
			              	 window.open('acs_raffronta_articoli_report.php?fn=open_report&ritime='+ <?php echo j($m_params->ritime); ?>);
    		                             	                	     
            	         }
		        		} , '->',
                        {
                         xtype: 'button',
                        text: 'Conferma aggiornamento',
    		            scale: 'medium',	                     
    					iconCls: 'icon-button_blue_play-24',
    		          	handler: function() {
    		          	
    		          	     var grid = this.up('grid');
    	       			     var loc_win = this.up('window');
				                 var records =  grid.getSelectionModel().getSelection();  
				           		 list_selected_id = [];                           
                                 for (var i=0; i<records.length; i++) 
                            	 list_selected_id.push(records[i].data);
    	       			    
         			          Ext.Ajax.request({
        				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				list_selected_id : list_selected_id,
        	        				ritime : <?php echo j($m_params->ritime); ?>,
									cod_art_raffronto : <?php echo j($m_params->cod_art_raffronto)?>
        						},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					         loc_win.destroy();
        					         Ext.Ajax.request({
             						        url     : 'acs_raffronta_articoli.php?fn=exe_raffronta',
             						        method  : 'POST',
             			        			jsonData: {
             			        			   list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>,
             			        			   cod_art_raffronto : <?php echo j($m_params->cod_art_raffronto)?>
             								},
             								
             								success: function(response, opts) {
            						        	 var jsonData = Ext.decode(response.responseText);
            						        	
            						        	  acs_show_win_std(null,
                        			    				'acs_raffronta_articoli.php?fn=open_grid', {
                        			    					ritime : jsonData.RITIME,
                        			    					cod_art_raffronto : <?php echo j($m_params->cod_art_raffronto)?>,
                        			    					list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>
                        		    				 });	       	 
            						        	      		   
            			            		},
            						        failure    : function(result, request){
            						            Ext.Msg.alert('Message', 'No data to be loaded');
            						        }
             								
             						    });
        					       },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				    }); 
    		           
    			
    			            }
    
    			     },
                    ]}],
				viewConfig: {
    				getRowClass: function(record, index) {	
    		           var v = '';
    		           if (record.get('ERCAMP').trim() == 'ARDART') 
    		           	v = v + ' grassetto';
    		           	
    		           	return v;																
    		         }  
				
				}
				
			
	    		
	 	}
       
}
<?php 
	exit;
}