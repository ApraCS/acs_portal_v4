<?php
require_once("../../config.inc.php");

require_once("acs_anag_artd_include.php");

/* TODO: verificare permessi Admin */

$main_module = new DeskUtility();

$m_params = acs_m_params_json_decode();

/*
   Recupero articoli   
 */
if ($_REQUEST['fn'] == 'get_json_data_art'){
    
    $sql_where_ar = gest_form_values($m_params->open_request->form_values);
    
    $radice = $m_params->open_request->radice;
 
    //posizione 1 variabile?
    $sql = "SELECT ARART, ARDART, ARSOSP, ARESAU, ARDTGE
            FROM {$cfg_mod_DeskArt['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' " . artd_ARART_add_where($radice) . "
            {$sql_where_ar}
            ORDER BY ARART";
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
    
        $nr['codice'] = trim($row['ARART']);
        $nr['desc'] = trim($row['ARDART']);
        $nr['sosp'] = trim($row['ARSOSP']);
        $nr['cicli'] = trim($row['ARESAU']);
        $nr['data_ins'] = trim($row['ARDTGE']);
        $nr['new'] = $m_params->open_request->rows->new;
    
            $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
    
}

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $radice = $m_params->open_request->radice;
    $sql_where = "";
  

    if($m_params->open_request->from_dist == 'Y'){
 
       
        $sql = "SELECT CDSTIN, CDSTFI
                FROM {$cfg_mod_DeskArt['file_cfg_distinte']}
                WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$m_params->open_request->distinta}'
                AND CDCMAS = '{$m_params->open_request->gruppo}'
                FETCH FIRST 1 ROWS ONLY";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        $da = $row['CDSTIN'];
        $a = $row['CDSTFI'];
       
        
        $where = artd_ARART_add_where(trim($radice));
        
       
    } else {    
        
        $form_ep = $m_params->open_request->form_values;
        $cod_lista = $form_ep->f_lista;
        $da = $form_ep->f_da;
        $a = $form_ep->f_a;
        
        $art = "";
        for($i = 0; $i <= 14; $i++){
            $filtro = 'f_'.$i;
            /*if($form_ep->$filtro != '')
                $art .= $form_ep->$filtro;*/
            
            if($form_ep->$filtro == '?')
                $form_ep->$filtro = '_';
                $art .= $form_ep->$filtro;;
        }
        
        $where = " AND ARART LIKE '{$art}%' ";
    
    }
    
    if($da > 0 && $a > 0){
        $n_c =  $a - $da + 1;
        $select = " SUBSTR(ARART, $da, {$n_c}) AS ART, MAX(ARDART) AS DESC";
        $group = " GROUP BY SUBSTR(ARART, $da, {$n_c})";
        $order = " ORDER BY SUBSTR(ARART, $da, {$n_c})";
       // $where = " AND ARART LIKE '{$art}' ";
    }else{
       
        $select = " ARART AS ART, ARDART AS DESC";
        $group = "";
        $order = " ORDER BY ARART"; 
      //  $where = " AND ARART LIKE '{$art}'";
        
    }
    
    if(isset($cod_lista) && strlen($cod_lista) > 0){
        
        $sql_cl = "SELECT * FROM {$cfg_mod_DeskArt['file_cfg_distinte_liste']} CL
                   WHERE CLDT = '{$id_ditta_default}' AND CLCDLI = '{$cod_lista}'
                   AND CLCELE = ?";
        
        $stmt_cl = db2_prepare($conn, $sql_cl);
        echo db2_stmt_errormsg();
    }
    
    
    $sql_cd = "SELECT COUNT(*) AS C_ROW
               FROM {$cfg_mod_DeskArt['file_cfg_distinte']} CD
               WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$m_params->open_request->distinta}'
               AND CDRSTR = ?";
    
    $stmt_cd = db2_prepare($conn, $sql_cd);
    echo db2_stmt_errormsg();
       
    //posizione 1 variabile?
    $sql = "SELECT {$select}
    FROM {$cfg_mod_DeskArt['file_anag_art']} AR
    WHERE ARDT = '{$id_ditta_default}' {$where}
    /*AND ARSOSP <> 'S'*/
    {$group} {$order}";
    
    /*print_r($sql);
    exit;*/
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        if(trim($row['ART']) != '')
            $nr['codice'] = trim($row['ART']);
            $nr['gruppo'] = $m_params->open_request->gruppo . trim($row['ART']);
            $nr['desc'] = trim($row['DESC']);  
                       
            $result = db2_execute($stmt_cd, array(trim($row['ART'])));
            $row_cd = db2_fetch_assoc($stmt_cd);
            
            $nr['config'] = $row_cd['C_ROW'];
            
            if(isset($cod_lista) && strlen($cod_lista) > 0){
                
                $result = db2_execute($stmt_cl, array(trim($row['ART'])));
                $row_cl = db2_fetch_assoc($stmt_cl);
                
                $nr['d_voce'] = trim($row_cl['CLDESC']);
                
            }
            
            $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_codice'){
    
    if($m_params->from_cl == 'Y'){
        
        $radice = $m_params->row->CLRSTR;
        $rad_da = $m_params->row->CLSTIN;
        $rad_a = $m_params->row->CLSTFI;
       
        
    }else if($m_params->from_cugino == 'Y'){
        
        $radice = $m_params->row_p->radice;
        $rad_da = $m_params->row_c->p_da;
        $rad_a = $m_params->row_c->p_a;
       // $cod_lista = $m_params->row->CDCDLI;
        $gruppo = $m_params->row_p->gruppo;
        
    }else{
        $radice = $m_params->row->CDRSTR;
        $rad_da = $m_params->row->CDSTIN;
        $rad_a = $m_params->row->CDSTFI;
        $cod_lista = $m_params->row->CDCDLI;
        $gruppo = $m_params->row->CDCMAS;
        
    }
    
    $rad_da = strlen($radice) + 1;
    $rad_a = strlen($radice) + 2;
  
  
    
    ?>
    
    {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
     		items: [
     		
     			{ 
						xtype: 'fieldcontainer',
						margin : '0 0 0 105',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
							<?php for ($i = 1; $i<=9; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 13 0 0' },
						<?php }?>	
						<?php for ($i = 10; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 6 0 0' },
						<?php }?>	
						
						]
						
						},
     			               			
						{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},
									
						items: [
						
							{ xtype: 'displayfield', value : 'Codice articolo:', margin : '0 20 0 0' },
						
						<?php for ($i = 0; $i<=14; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_<?php echo $i; ?>', 
						  value : '<?php echo $radice[$i]; ?>',
						  width: 20,
						  maxLength: 1,
						  listeners: {
    						'change': function(field){
    						   var c_da = this.up('window').down('#c_da');
    						   var c_a = this.up('window').down('#c_a');
    						   console.log(c_da);
    						
    						 value = this.getValue().toString();
    						 if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                                var value = <?php echo j($i); ?>;
                                c_da.setValue(value + 2);
                                <?php if($i == 13){?>
                                    c_a.setValue(value + 2);
                                	
                               <?php }else{?>
                                    c_a.setValue(value + 3);
                               <?php }?>
                              } 
      							
    					  }
 						 }},
						
						<?php }?>
					
								
						]},
						
						{ 
						  xtype : 'numberfield',
						  fieldLabel : 'Da',
						  itemId : 'c_da',
						  name: 'f_da',
						  hideTrigger : true, 
						  value : '<?php echo $rad_da ?>',
						  labelWidth: 20,
						  width: 50,
					
						  },
						  { 
						  xtype : 'numberfield',
						  fieldLabel : 'A',
						  hideTrigger : true,
						  itemId : 'c_a',
						  name: 'f_a', 
						  value : '<?php echo $rad_a; ?>',
						  labelWidth: 20,
						  width: 50,
						  
						  }, 
						
							{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},					
						items: [
						
							{ 
    					  xtype : 'textfield',
    					  fieldLabel : 'Gruppo',
    					  hideTrigger : true, 
    					  name: 'f_gruppo', 
    					  value : '<?php echo $gruppo; ?>',
    					  labelWidth: 50,
    					  width: 150						  
						}, { 
						  xtype : 'textfield',
						  fieldLabel : 'Codice lista',
						  hideTrigger : true, 
						  labelAlign : 'right',
						  name: 'f_lista', 
						  value : '<?php echo $cod_lista; ?>',
						  labelWidth: 80,
						  width: 200,
						  
						  },
					
						
						]},
						
							
						
						
						
            ],	buttons: [
			
			{
	            text: 'Elenco stringhe',
	            iconCls: 'icon-search-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	                var form_values = form.getValues();
	            	var win_id = this.up('window').getId();
	            	var loc_win = this.up('window');
	            	
	            	if(form.isValid()){
	            	
	            	  var my_listeners = {						        			
		        					afterDuplica: function(from_win){
		        						from_win.close();
		        						loc_win.close();
						        	}
								}
	
	            	
	            		acs_show_win_std('Elenco stringhe', 'acs_anag_artd_elenco_stringhe.php?fn=open_elenco', {
	            			form_values: form.getValues(),
	            			radice : '<?php echo $radice; ?>', 
	            			gruppo : '<?php echo $gruppo; ?>',
	            			distinta : '<?php echo $m_params->distinta; ?>',
	            			or_row: <?php echo acs_je($m_params->row); ?>
	            		},800, 400, my_listeners, 'icon-search-16');
	            	
		            
			        } 
	                      	                	                
	            }
	        }			
	        
	        ]
            
            
            
            }
            
            
       ]}
    
    
<?php 
    exit;
}


if ($_REQUEST['fn'] == 'open_elenco'){
    

    ?>
    
    {"success":true, "items": [
	{	xtype: 'grid',
        				flex:1,
        		        useArrows: true,
        		        rootVisible: false,
        		        loadMask: true,
        		        multiSelect: true,
        		        plugins: [
                          Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 2,
                          })
                      ],
        		        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['codice', 'desc', 'd_voce', 'config', 'gruppo']							
									
			}, //store
		
			    <?php $gen = "<img src=" . img_path("icone/48x48/check_green.png") . " height=18>"; ?>
				columns: [	
					
		    			{
			                header   : 'Gruppo',
			                dataIndex: 'gruppo', 
			                flex: 0.5
			             },{
			                header   : 'Stringa',
			                dataIndex: 'codice', 
			                flex: 0.5
			             },
			             {
			                text   : '1� Articolo',
			                dataIndex: 'desc', 
			                flex: 1
			             },  {
			                text   : 'Descrizione voce',
			                dataIndex: 'd_voce', 
			                flex: 1,
			                editor: {
            				    xtype: 'textfield',
            				    allowBlank: true
            				}
			             }, {text: '<?php echo $gen; ?>', 	
            				width: 30, 
            				dataIndex: 'config',
            				tooltip: 'Configurazione esistente',		        			    	     
            		    	renderer: function(value, p, record){
            		    	       if(record.get('config') > 0)
            		    			 return '<img src=<?php echo img_path("icone/48x48/check_green.png") ?> width=15>';
            		    		  }
            		        }
		    
		    
		    		
		    		
				]
				
				, listeners : {
				
				afterrender: function (comp) {
	                   
						<?php 
						$cod = '';
						$form_values = $m_params->form_values;
						 for($i = 0; $i <= 14; $i++){
						     $name = "f_{$i}";
						     $cod .= $form_values->$name;
						  }
						 
						  $title = " [{$cod}, {$form_values->f_da}, {$form_values->f_a}]"
						 ?>
						
						
						comp.up('window').setTitle('<?php echo "Elenco stringhe {$title}";?>');	 				
		 	    } ,
				
				itemcontextmenu : function(grid, rec, node, index, event) {	
					event.stopEvent();
					var voci_menu = [];
					
					voci_menu.push({
				         		text: 'Elenco articoli',
				        		iconCls : 'icon-leaf-16',          		
				        		handler: function() {

									acs_show_win_std('Elenco articoli', 'acs_anag_artd_elenco_stringhe.php?fn=open_art', {
            	            		c_art : rec.get('codice'), radice : '<?php echo $m_params->radice; ?>' + rec.get('codice')}, 
            	            		450, 400, null, 'icon-leaf-16');
				        		
				        		}
				    		});
				    		
			    	   var menu = new Ext.menu.Menu({
			           items: voci_menu
				       }).showAt(event.xy);	
				    		
					
					}
				
				
				}
				
				
				, dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                <?php if($m_params->from_cugino == 'Y'){?>
                   {
                    xtype : 'displayfield',
                    value : '<?php echo "Duplica da cugino {$m_params->cugino->voce} {$m_params->cugino->desc}"?>'
                
                	},
                <?php }?>	
                	 '->',{
                     xtype: 'button',
	            	 scale: 'medium',
	            	 iconCls: 'icon-comment_add-24 ',
                     text: 'Duplica',
			            handler: function() {
			        
			            var loc_win = this.up('window');
			            var grid = this.up('grid');
			            all_rows =   grid.getStore().getRange();
			            id_selected = grid.getSelectionModel().getSelection();
			            var voce = id_selected[0].data.codice;
			  			list_rows = [];
                    
                        Ext.each(all_rows, function(item) {
    						
    						if(item.get('d_voce').trim() != '')	{
    						
    						var Obj = {	
       							 c_art:   item.get('codice'),
       							 d_art:    item.get('d_voce')
    							};
    								
    							list_rows.push(Obj); // push this to the array	
    						 }		
    					
   						
						}, this);
						
					      ////////loc_win.fireEvent('afterDuplica2', loc_win, list_rows);
    					    Ext.Ajax.request({
						        url: 'acs_anag_art_duplica_indici.php?fn=exe_ins_dup',
						        jsonData: {
						            form_values : <?php echo acs_je($m_params->form_values); ?>,
						        	rows : list_rows,
						        	voce : voce,
						        	or_row : <?php echo acs_je($m_params->or_row); ?>,
						        	indice : <?php echo acs_je($m_params->distinta) ?>,
						        	from_cl : <?php echo acs_je($m_params->from_cl) ?>						        	
						         },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        	jsonData = Ext.decode(result.responseText);
						        	
 						        	if(jsonData.error_msg != ''){
	 						        	acs_show_msg_error(jsonData.error_msg);
	 						        	return;
	 						        }
	 						        
	 						        loc_win.fireEvent('afterDuplica', loc_win);
 						        	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						            console.log('errorrrrr');
						        }
						    });					      	
					   
			
			            }
			     }
			     
			     
			     ]
		   }]
				
		
			
				,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
						         if(record.get('config') > 0)
						           return ' segnala_riga_grigio_nb';
						       			           		
						           return '';																
					         }  
					    },
			
											    
				    		
                    }
      
            
            
       ]}
    
    
<?php 
    exit;
}

if ($_REQUEST['fn'] == 'open_duplica'){
    
    if($m_params->from_cl == 'Y'){
        
        $radice = $m_params->row->CLRSTR;
        $rad_da = $m_params->row->CLSTIN;
        $rad_a = $m_params->row->CLSTFI;
        
        
    }else{
        $gruppo = $m_params->row->CDCMAS;
        $voce = $m_params->row->CDSLAV;
        
    }
    
    
    
    
    ?>
    
    {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
     		items: [
            		
							{ 
						  xtype : 'textfield',
						  fieldLabel : 'Gruppo',
						  name: 'f_gruppo', 
						  hideTrigger : true, 
						  value : '<?php echo $gruppo ?>',
						  labelWidth: 50,
						  width: 150,
						  
						  },
						  { 
						  xtype : 'textfield',
						  fieldLabel : 'Voce',
						  hideTrigger : true, 
						  name: 'f_voce', 
						  value : '<?php echo $voce; ?>',
						  labelWidth: 50,
						  width: 150,
						  
						  }
						
						
            ],	buttons: [
			
			{
	            text: 'Duplica',
	            iconCls: 'icon-button_black_play-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	var loc_win = this.up('window');
	            	
	            	if(form.isValid()){
	            	
	            	   	Ext.Ajax.request({
					        url: 'acs_anag_art_duplica_indici.php?fn=exe_ins_dup',
					        jsonData: {
					        	or_row : <?php echo acs_je($m_params->row); ?>, 
					        	indice : '<?php echo $m_params->indice; ?>'	,
					        	form_values : form.getValues(),
					        	from_cl : '<?php echo $m_params->from_cl; ?>'						        	
					         },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	
					            jsonData = Ext.decode(result.responseText);
					            loc_win.fireEvent('afterDup', loc_win, jsonData);	
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					            console.log('errorrrrr');
					        }
					    });	
	            	
	            		
	            	
			        } 
	                      	                	                
	            }
	        }			
	        
	        ]
            
            
            
            }
            
            
       ]}
    
    
<?php 
    exit;
}


if ($_REQUEST['fn'] == 'open_art'){
    
    
    ?>
    
    {"success":true, "items": [
	{	xtype: 'grid',
        				flex:1,
        		        useArrows: true,
        		        rootVisible: false,
        		        loadMask: true,
        		        multiSelect: true,
        		        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_art', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['codice', 'desc', 'sosp', 'cicli', 'data_ins', 'new']							
									
			}, //store
			
			    <?php $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
			    <?php $gen = "<img src=" . img_path("icone/48x48/exchange.png") . " height=20>"; ?>
		
				columns: [	
					
		    		{
			                header   : 'Articolo',
			                dataIndex: 'codice', 
			                flex: 0.5
			             },
			             {
			                text   : 'Descrizione',
			                dataIndex: 'desc', 
			                flex: 1
			             }, {text: '<?php echo $cf1; ?>', 	
            				width: 30, 
            				dataIndex: 'sosp',
            				tooltip: 'Sospeso',		        			    	     
            		    	renderer: function(value, p, record){
            		    			  if(record.get('sosp') == 'S') return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
            		    		  }
            		        }, {text: 'C', 
            				tooltip: 'Ciclo di vita',	 	
            				width: 30, 
            				dataIndex: 'esau',
            				renderer: function(value, metaData, record, row, col, store, gridView){
                		    	if (record.get('esau') == 'E'){	    			    	
            					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In esaurimento') + '"';
            					   return '<img src=<?php echo img_path("icone/48x48/clessidra.png") ?> width=15>';}
            					if (record.get('esau') == 'A'){	    			    	
            					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In avviamento') + '"';
            					   return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';}
            				    if (record.get('esau') == 'C'){	    			    	
            					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Codifica in corso') + '"';
            					   return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=15>';}
            				    
            				    //return value;
            				    }
            				},   {
			                text   : 'Immissione',
			                dataIndex: 'data_ins', 
			                width: 80, 
			                renderer: date_from_AS
			             }, 
            		    <?php if($m_params->from_ant == 'Y'){?>
            		    
            		    	{text: '<?php echo $gen; ?>', 	
            				width: 30, 
            				dataIndex: 'new',
            				tooltip: 'Duplica',		        			    	     
            		    	renderer: function(value, p, record){
            		    	       if(record.get('new') == 'Y')
            		    			 return '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=18>';
            		    		  }
            		        },
            		    
            		    <?php }?>
		    
		    		
		    		
				],  listeners: {
				
	            celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var rec = iView.getRecord(iRowEl);
							  col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  
							  var grid = this;
							 
							  if(col_name == 'new' && rec.get('new') == 'Y'){
				     
							     acs_show_win_std('Duplica articolo ' + rec.get('codice'), 
				                			'acs_anag_art_duplica.php?fn=open_form', {
				                             d_art_new: rec.get('desc'),
				                             c_art: rec.get('codice'),
				                             from_indici : 'Y',
				                             radice : '<?php echo $m_params->rows->gruppo; ?>',
				                             nr_id : '<?php echo $m_params->rows->nr_id; ?>',
				                             prog : '<?php echo $m_params->rows->prog; ?>',
				                             voce : '<?php echo $m_params->rows->voce; ?>',
				                             master : '<?php echo $m_params->rows->master; ?>',
				                             distinta : '<?php echo $m_params->rows->distinta; ?>',
									       }, 500, 450, {
				        					'afterOkSave': function(from_win){
				        						grid.getStore().load();
				        						from_win.close();
	 										}
				        				}, 'icon-comment_add-16');
							  
							  
							  }
							 
						  }
			   		  }
				
				
				}
			
				,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
						       if (record.get('disable')=='Y')
						           return ' segnala_riga_disabled';
						       			           		
						           return '';																
					         }  
					    },
			
											    
				    		
                    }
      
            
            
       ]}
    
    
<?php 
    exit;
}
