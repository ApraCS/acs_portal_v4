<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));
$desk_art = new DeskArt();

$m_params = acs_m_params_json_decode();


if($_REQUEST['fn'] == 'exe_save_nota'){
    
    $ar_ins = array();
    $ar_ins['NTMEMO'] 	= $m_params->form_values->f_nota;
    
    if(trim($m_params->rrn) != ''){
        if(trim($m_params->form_values->f_nota) == ''){
            $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_note']} NT
                    WHERE RRN(NT) = '{$m_params->rrn}'";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
        }else{
        
        $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
                SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE RRN(NT) = '{$m_params->rrn}'";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
        }
        
    }else{
        $ar_ins['NTDT'] 	= $id_ditta_default;
        $ar_ins['NTKEY1'] 	= $m_params->prog;
        $ar_ins['NTSEQU'] 	= 0;
        $ar_ins['NTTPNO'] 	= 'ASPRS';
         
        $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}
               (" . create_name_field_by_ar($ar_ins) . ")
               VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
  
    }
    
    $ha_nota = $desk_art->has_nota_progetto($m_params->prog);
    $row_nt = $desk_art->get_note_progetto($m_params->prog);
            
    $ret = array();
    $ret['success'] = true;
    $ret['note'] = $row_nt['NTMEMO'];
    $ret['icon'] = $ha_nota;
    echo acs_je($ret);
    exit;
}


if($_REQUEST['fn'] == 'open_panel'){
    
    $row_nt = $desk_art->get_note_progetto($m_params->prog);
  
    ?>
    
 {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
            frame: true,
            flex:1,
            title: '',
            items: [ 
              	   {
						name: 'f_nota',
						xtype: 'textarea',
						fieldLabel: '',
					    anchor: '-15',		
					    height : 200,		    
					    value: <?php echo j(trim($row_nt['NTMEMO'])); ?>,							
					}
				                	
				], buttons: [
		
				{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	  
					
						Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_nota',
					        jsonData: {
					        	form_values: form.getValues(),
					        	prog : <?php echo j($m_params->prog); ?>,
					        	rrn : <?php echo j($row_nt['RRN']); ?>
					        	
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	
					        try {
                               var jsonData = Ext.decode(result.responseText);
							   loc_win.fireEvent('afterSave', loc_win, jsonData);
                            }
                            catch(err) {
                                Ext.Msg.alert('Try&catch error', err.message);
                            }
					       
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					            console.log('errorrrrr');
					        }
					    });	  
					
	

				    }            	                	                
	            }
	        }
   
	        ]              
				
        }
]}   
    
    
    
    
<?php  
exit;
}
