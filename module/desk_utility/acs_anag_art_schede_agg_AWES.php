<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];  
    $nr['tades2'] = $row['TADES2'];  
    
  //  $nr['svalutazione2'] = substr($row['TAREST'], 59, 3).",".substr($row['TAREST'], 62, 2);
    
    $nr['dimensioni1'] = substr($row['TAREST'], 0, 5).",".substr($row['TAREST'], 5, 2);
    $nr['dimensioni2'] = substr($row['TAREST'], 7, 5).",".substr($row['TAREST'], 12, 2);
    $nr['dimensioni3'] = substr($row['TAREST'], 14, 5).",".substr($row['TAREST'], 19, 2);
    
    $nr['um'] = substr($row['TAREST'], 21, 3);
    $nr['magg_dim_L'] = substr($row['TAREST'], 24, 8);
    $nr['magg_dim_H'] = substr($row['TAREST'], 32, 8);
    $nr['magg_dim_P'] = substr($row['TAREST'], 40, 8);
    
    $val_dom = find_TA_sys('PUVR', substr($row['TAREST'], 48, 3));
    if(trim(substr($row['TAREST'], 48, 3)) != ''){
        $nr['variabile1'] = " [".trim(substr($row['TAREST'], 48, 3))."] ".$val_dom[0]['text'];
        $nr['val_variabile1'] = trim(substr($row['TAREST'], 48, 3));
    }else{
        $nr['variabile1'] = "";
    }
    $val_risp = find_TA_sys('PUVN', substr($row['TAREST'], 51, 3), null, substr($row['TAREST'], 48, 3));
    if(trim(substr($row['TAREST'], 21, 3)) != ""){
        $nr['variante1']= "[".trim(substr($row['TAREST'], 51, 3))."] ".$val_risp[0]['text'];
        $nr['val_variante1'] = trim(substr($row['TAREST'], 51, 3));
    }else{
        $nr['variante1'] = "";
    }
    
    $nr['magg_dim1_L'] = substr($row['TAREST'], 54, 8);
    $nr['magg_dim1_H'] = substr($row['TAREST'], 62, 8);
    $nr['magg_dim1_P'] = substr($row['TAREST'], 70, 8);
    
  /*  $val_dom = find_TA_sys('PUVR', substr($row['TAREST'], 78, 3));
    if(trim(substr($row['TAREST'], 78, 3)) != ""){
        $nr['variabile2'] = " [".trim(substr($row['TAREST'], 78, 3))."] ".$val_dom[0]['text'];
        $nr['val_variabile2'] = trim(substr($row['TAREST'], 78, 3));
    }else{
        $nr['variabile2'] = "";
    }
    $val_risp = find_TA_sys('PUVN', substr($row['TAREST'], 81, 3), null, substr($row['TAREST'], 78, 3));
    if(trim(substr($row['TAREST'], 81, 3)) != ''){
        $nr['variante2']= "[".trim(substr($row['TAREST'], 81, 3))."] ".$val_risp[0]['text'];
        $nr['val_variante2'] = trim(substr($row['TAREST'], 81, 3));
    }else{
        $nr['variante2'] = "";
    }
    
    $nr['magg_dim2_L'] = substr($row['TAREST'], 84, 8);
    $nr['magg_dim2_H'] = substr($row['TAREST'], 92, 8);
    $nr['magg_dim2_P'] = substr($row['TAREST'], 100, 8);
    
    $val_ubi = find_TA_sys('TUBI', substr($row['TAREST'], 108, 3));
    if(trim(substr($row['TAREST'], 108, 3)) != ""){
        $nr['tipo_ubi']= "[".trim(substr($row['TAREST'], 108, 3))."] ".$val_ubi[0]['text'];
        $nr['val_tipo_ubi'] = trim(substr($row['TAREST'], 108, 3));
    }else{
        $nr['tipo_ubi'] = "";
    }
    
    $nr['dim_max_L'] = substr($row['TAREST'], 111, 8);
    $nr['dim_max_H'] = substr($row['TAREST'], 119, 8);
    $nr['dim_max_P'] = substr($row['TAREST'], 127, 8);
    
    $nr['num_cataste'] = substr($row['TAREST'], 135, 3);
    */
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 
        'dimensioni1', 'dimensioni2', 'dimensioni3',
        'um', 'magg_dim_L', 'magg_dim_H', 'magg_dim_P',
        'variabile1', 'val_variabile1', 'variante1', 'val_variante1', 
        'magg_dim1_L', 'magg_dim1_H', 'magg_dim1_P');

    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
     				{
	                header   : 'Descrizione',
	                dataIndex: 'tadesc',
	                flex:1
	                },
	                
	               { header   : 'Interfaccia',
	                dataIndex: 'tades2',
	                flex: 1
	                },
			       
	                {
	                header   : 'Dimensioni',
	                dataIndex: 'dimensioni1',
	                flex:1,
	                renderer: function(value, p, record){
		    			
		    			  return value + 'x' +record.get('dimensioni2') + 'x' +record.get('dimensioni3');
		    		  }
	                },   {
	                header   : 'Um',
	                dataIndex: 'um',
	                width : 30
	                },
	                {
	                header   : 'Magg.dim.',
	                dataIndex: 'magg_dim_L',
	                 flex:1,
	                renderer: function(value, p, record){
		    			
		    			  return value + 'x' +record.get('magg_dim_H') + 'x' +record.get('magg_dim_P');
		    		  }
	                },
	                {
	                header   : 'Variabile 1',
	                dataIndex: 'variabile1',
	                width:80
	                },{
	                header   : 'Variante 1',
	                dataIndex: 'variante1',
	                 width:80
	                },{
	                header   : 'Magg.dim.1',
	                dataIndex: 'magg_dim1_L',
	                flex:1,
	                renderer: function(value, p, record){
		    			
		    			  return value + 'x' +record.get('magg_dim1_H') + 'x' +record.get('magg_dim1_P');
		    		  }
	                }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
   
    $ar_values = array( 
        "riga" => trim($row->riga),
        "interfaccia" => trim($row->tades2),
        "descrizione" => trim($row->tadesc),
        "dimensioni1" => trim($row->dimensioni1),
        "dimensioni2" => trim($row->dimensioni2),
        "dimensioni3" => trim($row->dimensioni3),
        "um" => trim($row->um),
        "magg_dim_L" => trim($row->magg_dim_L),
        "magg_dim_H" => trim($row->magg_dim_H),
        "magg_dim_P" => trim($row->magg_dim_P),
        "val_variabile1" => trim($row->val_variabile1),
        "val_variante1" => trim($row->val_variante1),
        "variante1" => trim($row->variante1),
        "magg_dim1_L" => trim($row->magg_dim1_L),
        "magg_dim1_H" => trim($row->magg_dim1_H),
        "magg_dim1_P" => trim($row->magg_dim1_P),
       
        
    );
 
    return $ar_values;
    
}

function get_values_from_array($row){
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "interfaccia" => trim($row['tades2']),
        "descrizione" => trim($row['tadesc']),
        "dimensioni1" => trim($row['dimensioni1']),
        "dimensioni2" => trim($row['dimensioni2']),
        "dimensioni3" => trim($row['dimensioni3']),
        "um" => trim($row['um']),
        "magg_dim_L" => trim($row['magg_dim_L']),
        "magg_dim_H" => trim($row['magg_dim_H']),
        "magg_dim_P" => trim($row['magg_dim_P']),
        "val_variabile1" => trim($row['val_variabile1']),
        "val_variante1" => trim($row['val_variante1']),
        "variante1" => trim($row['variante1']),
        "magg_dim1_L" => trim($row['magg_dim1_L']),
        "magg_dim1_H" => trim($row['magg_dim1_H']),
        "magg_dim1_P" => trim($row['magg_dim1_P']),
        
        
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		labelWidth : 110,
		anchor: '-15',	
		maxLength: 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
    	{xtype: 'textfield',
		name: 'descrizione',
		fieldLabel: 'Descrizione',
		labelWidth : 110,
		maxLength: 30,
		anchor: '-15',	
		value:  <?php echo j($ar_values['descrizione']); ?>
		},
       {xtype: 'textfield',
		name: 'interfaccia',
		fieldLabel: 'Interfaccia',
		labelWidth : 110,
		maxLength: 10,
		anchor: '-15',	
		value:  <?php echo j($ar_values['interfaccia']); ?>
		},
		
		
			{
	    xtype: 'fieldcontainer',
	    title : 'Dimensioni',
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},
		 						
		items: [
		    { xtype: 'displayfield', value : 'Dimensioni : ', margin : '0 30 0 0'},
			{
			xtype: 'textfield',
			width : 113,
			labelWidth : 30,
			labelAlign : 'right',
			name: 'dimensioni1',
			fieldLabel: 'L',
			anchor: '-15',	
			maxLength: 7,
			value:  <?php echo j($ar_values['dimensioni1']); ?>
			},{
			xtype: 'textfield',
			width : 114,
			name: 'dimensioni2',
			fieldLabel: 'H',
			labelWidth : 30,
			labelAlign : 'right',
			anchor: '-15',	
			maxLength: 7,
			value:  <?php echo j($ar_values['dimensioni2']); ?>
			},{
			xtype: 'textfield',
			width : 114,
			labelWidth : 30,
			name: 'dimensioni3',
			labelAlign : 'right',
			fieldLabel: 'P',
			anchor: '-15',	
			maxLength: 7,
			value:  <?php echo j($ar_values['dimensioni3']); ?>
			},
		   
		
		]},
	 
	    {xtype: 'textfield',
		name: 'um',
		fieldLabel: 'UM',
		maxLength : 3,
		labelWidth : 110,
		anchor: '-15',	
		value:  <?php echo j($ar_values['um']); ?>
		}, 
		{
	    xtype: 'fieldcontainer',
	    title : 'Dimensioni',
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},
		 						
		items: [
		    { xtype: 'displayfield', value : 'Magg. dimens.: ', margin : '0 10 0 0'},
			{
			xtype: 'textfield',
			width : 113,
			labelWidth : 30,
			labelAlign : 'right',
			name: 'magg_dim_L',
			fieldLabel: 'L',
			anchor: '-15',	
			maxLength : 8,
			value:  <?php echo j($ar_values['magg_dim_L']); ?>
			},{
			xtype: 'textfield',
			width : 114,
			name: 'magg_dim_H',
			fieldLabel: 'H',
			labelWidth : 30,
			labelAlign : 'right',
			anchor: '-15',	
			maxLength : 8,
			value:  <?php echo j($ar_values['magg_dim_H']); ?>
			},{
			xtype: 'textfield',
			width : 114,
			labelWidth : 30,
			name: 'magg_dim_P',
			labelAlign : 'right',
			fieldLabel: 'P',
			anchor: '-15',	
			maxLength : 8,
			value:  <?php echo j($ar_values['magg_dim_P']); ?>
			},
		   
		
		]},
		
		
		{name: 'val_variabile1',
		xtype: 'combo',
		flex: 1,
		labelWidth : 110,
		fieldLabel: 'Variabile1',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		queryMode: 'local',
		minChars: 1,							
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	    value:  <?php echo j($ar_values['val_variabile1']); ?>,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		    data: [								    
		     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
		    ]
		}, listeners: {
        	change: function(field,newVal) {	
          		if (!Ext.isEmpty(newVal)){
          		
                	combo_risp = this.up('form').down('#c_risp');                      		 
                 	combo_risp.store.proxy.extraParams.domanda = newVal;
                	combo_risp.store.load();                          
                 }
                 

                }, beforequery: function (record) {
                record.query = new RegExp(record.query, 'i');
                record.forceAll = true;
            }
   }
		
   } ,
   
   {
		name: 'val_variante1',
		xtype: 'combo',
		flex: 1,
		labelWidth : 110,
		itemId: 'c_risp',
		fieldLabel: 'Variante1',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',								
		emptyText: '- seleziona -',
   		//allowBlank: false,								
	    anchor: '-15',
	    value:  <?php echo j($ar_values['val_variante1']); ?>,
	    store: {
			        autoLoad: true,
					proxy: {
			            type: 'ajax',
			            url : 'acs_anag_art_variabili.php?fn=get_data_puvn',
			            actionMethods: {
				          read: 'POST'
			        },
	                	extraParams: {
	    		    		domanda: <?php echo j($ar_values['val_variabile1']); ?>,
	    				},				            
			            doRequest: personalizza_extraParams_to_jsonData, 
			            reader: {
			            type: 'json',
						method: 'POST',						            
				            root: 'root'						            
				        }
			        },       
					fields: ['id', 'text'],		             	
	            },
	          listeners: {
                        afterrender: function(comp){
                            data = [
                                {id: <?php echo j($ar_values['val_variante1'])?>, text: <?php echo j($ar_values['variante1']) ?>}
                            ];
                            comp.store.loadData(data);
                            comp.setValue(<?php echo j($ar_values['val_variante1'])?>);
                        }
         }
			
   		}, 		{
	    xtype: 'fieldcontainer',
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},
		 						
		items: [
		    { xtype: 'displayfield', value : 'Magg. dimens.: ', margin : '0 10 0 0'},
			{
			xtype: 'textfield',
			width : 113,
			labelWidth : 30,
			labelAlign : 'right',
			name: 'magg_dim1_L',
			fieldLabel: 'L',
			anchor: '-15',
			maxLength : 8,	
			value:  <?php echo j($ar_values['magg_dim1_L']); ?>
			},{
			xtype: 'textfield',
			width : 114,
			name: 'magg_dim1_H',
			fieldLabel: 'H',
			labelWidth : 30,
			labelAlign : 'right',
			anchor: '-15',
			maxLength : 8,	
			value:  <?php echo j($ar_values['magg_dim1_H']); ?>
			},{
			xtype: 'textfield',
			width : 114,
			labelWidth : 30,
			name: 'magg_dim1_P',
			labelAlign : 'right',
			fieldLabel: 'P',
			anchor: '-15',	
			maxLength : 8,
			value:  <?php echo j($ar_values['magg_dim1_P']); ?>
			},
		   
		
		]}

    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-7s", str_replace(",", "", $form_values->dimensioni1));
    $ar_ins['TAREST'] .= sprintf("%-7s", str_replace(",", "", $form_values->dimensioni2));
    $ar_ins['TAREST'] .= sprintf("%-7s", str_replace(",", "", $form_values->dimensioni3));

    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->um);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->magg_dim_L);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->magg_dim_H);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->magg_dim_P);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->val_variabile1);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->val_variante1);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->magg_dim1_L);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->magg_dim1_H);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->magg_dim1_P);
    
    $ar_ins['TANR']   .= sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] .= $form_values->descrizione;
    $ar_ins['TADES2'] .= $form_values->interfaccia;
    
    return $ar_ins;
    
}