<?php

require_once "../../config.inc.php";

$main_module = new DeskUtility();
$deskArt = new DeskArt();

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
    $ar_email_to = array();
    
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u){
        $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }
    
    $ar_email_json = acs_je($ar_email_to);
    ?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   tr.grassetto td{font-weight: bold;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff; font-weight: bold;}   
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
    
   
	@media print 
	{
	    .noPrint{display:none;}
	     table.int1 {
        border-collapse: unset;
	     	
    }
        .page-break  { display: block; page-break-before: always; }
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 


<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php


$ritime = $_REQUEST['ritime']; //TDDTSP??


$sql= "SELECT RRN(ER) AS RRN, ER.* FROM {$cfg_mod_DeskArt['file_esito_raffronto']} ER
       WHERE ERDT = '{$id_ditta_default}' AND ERTIME = '{$ritime}'";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
while ($row= db2_fetch_assoc($stmt)) {
    $ar[] = $row;
}


echo "<div id='my_content'>"; 
echo "<div class=header_page>";
echo "<H2>Confronto con dati anagrafici articolo {$ar[0]['ERCARF']}</H2>";
echo "</div>";

echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
echo "<table class=int1>";

echo "<tr class='liv_data'>
              <th>Articolo</th>
              <th>File</th>
              <th>Campo</th>
              <th>Descrizione campo</th>
              <th>Articolo {$ar[0]['ERCARF']}</th>
              <th>Dato raffrontato diverso</th>";
echo "</tr>";



foreach($ar as $k => $v){
    
    if(trim($v['ERCAMP']) == 'ARDART')
        echo "<tr class = 'grassetto'>";
    else 
        echo "<tr>";
    
    echo "<td>{$v['ERCARA']}</td>
            <td>{$v['ERFILE']}</td>
            <td>{$v['ERCAMP']}</td>
            <td>{$v['ERDCAM']}</td>
            <td>{$v['ERVAL1']}</td>
            <td>{$v['ERVAL2']}</td>";
    echo "</tr>";
   
   
}


echo "</table>";
echo "</div>";

 		
?>

 </body>
</html>


<?php
exit;
}