<?php

require_once "../../config.inc.php";

$main_module = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data_righe'){
    
    $ar = array();
    
    $list_art = $m_params->open_request->list_selected_id;
    $form_values = $m_params->open_request->form_values;
    $tipologia = $m_params->open_request->tipologia;
    
    $ar=crea_ar_righe_documento($list_art, $tipologia, $form_values);
  
    echo acs_je($ar);
    exit;
    
}

if ($_REQUEST['fn'] == 'grid_righe'){ ?>

{"success":true, "items": [

       	{
		   xtype: 'grid',
		   loadMask: true,	
		   features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
			
		   store: {
				xtype: 'store',
				autoLoad: true,
	
						proxy: {
						   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_righe', 
						   method: 'POST',								
						   type: 'ajax',

					       actionMethods: {
					          read: 'POST'
					        },
					          extraParams: {
								 open_request: <?php echo acs_raw_post_data(); ?>,
								
	        				},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
					
        			fields: ['data_eva', 'k_ordine', 'cod_cf', 'den_cf', 'documento', 'documento_aa', 'documento_nr', 'riga', 's_riga', 'qta', 'u_m', 'c_art', 'd_art', 'data_reg', 'stato', 'tipo']							
							
			}, //store
				

			columns: [
			    {
	                header   : 'Codice C/F',
	                dataIndex: 'cod_cf',
	                width : 70,
	                filter: {type: 'string'}, filterable: true,
	            },{
	                header   : 'Denominazione C/F',
	                dataIndex: 'den_cf',
	                flex : 1,
	                filter: {type: 'string'}, filterable: true,
	            },
			      {
	                header   : 'Anno',
	                dataIndex: 'documento_aa',
	                width : 40,
	                filter: {type: 'string'}, filterable: true,
	            },
			      {
	                header   : 'Numero',
	                dataIndex: 'documento_nr',
	                width : 60,
	                filter: {type: 'string'}, filterable: true,
	            },{
	                header   : 'Tp',
	                dataIndex: 'tipo',
	                tooltip : 'tipo',
	                width : 30,
	                filter: {type: 'string'}, filterable: true,
	            },
	            {
	                header   : 'St',
	                dataIndex: 'stato',
	                tooltip : 'stato',
	                width: 30,
	                filter: {type: 'string'}, filterable: true,
	            },	
			     {
	                header   : 'Data',
	                dataIndex: 'data_reg',
	                width: 60,
	                filter: {type: 'string'}, filterable: true,
	                renderer: date_from_AS
	            }, {
	                header   : 'Data evasione',
	                dataIndex: 'data_eva',
	                width: 90,
	                filter: {type: 'string'}, filterable: true,
	                renderer: date_from_AS
	            }, {
	                header   : 'Righe',
	                dataIndex: 'riga',
	                width: 50,
	                filter: {type: 'string'}, filterable: true,
	            }, {
	                header   : 'Sottorighe',
	                dataIndex: 's_riga',
	                width: 70,
	                filter: {type: 'string'}, filterable: true,
	            },
				{
	                header   : 'Articolo',
	                dataIndex: 'c_art',
	                width: 80,
	                filter: {type: 'string'}, filterable: true,
	            },
	            
	            {
	                header   : 'Descrizione',
	                dataIndex: 'd_art',
	                flex : 1,
	                filter: {type: 'string'}, filterable: true  
	            },
	           
	            {
	                header   : 'UM',
	                dataIndex: 'u_m',
	                width: 30,
	                filter: {type: 'string'}, filterable: true,
	            },
	             {
	                header   : 'Q.t&agrave;',
	                dataIndex: 'qta',
	                width: 40,
	                filter: {type: 'string'}, filterable: true,
                    renderer: floatRenderer2	    
					}	
	         ],
	         
	         listeners: {
				itemcontextmenu : function(grid, rec, node, index, event) 
	         		      { event.stopEvent();
	         		        var voci_menu = [];	         		       	         		        
	         		        
					        voci_menu.push(
					          { text: 'Visualizza righe'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function() 
					              { acs_show_win_std(null
					                               , 'acs_get_order_rows_gest_order_entry.php'
					                               , { from_righe_info: 'Y'
					                                 , from_anag_art : 'Y'
					                                 , k_ordine: rec.get('k_ordine')
					                                 , riga : rec.get('riga')
					                                 });            		
		        		          }
		    		          }
		    		        );	
		    		        
		    		        
	         		        if(rec.get('riga') > 0){
	         		        
	         		        	if(rec.get('RDQTA') > 0){
        	         		         voci_menu.push({
        				         		text: 'Visualizza distinta componenti',
        				        		iconCls : 'icon-folder_search-16',          		
        				        		handler: function() {
        				        		   acs_show_win_std('Distinta componenti', 'acs_get_riga_componenti.php', {k_ordine: rec.get('k_ordine'), nrec: rec.get('RDNREC'), d_art: rec.get('RDDART'), c_art : rec.get('RDART'), riga : rec.get('riga'), from_anag_art : 'Y'}, 1300, 450, {}, 'icon-folder_search-16');          		
        				        		}
        				    		});
    				    		}
    				    		
    				    	if(rec.get('ordine') != '' && rec.get('RDNREC') > 0){
    				    		voci_menu.push({
    				         		text: 'Visualizza distinta di lavorazione',
    				        		iconCls : 'icon-folder_search-16',          		
    				        		handler: function() {
    				        		
    				        		var row_l1 = {RETPCO: 'L1', RETPSV : 'FB', REAACO : rec.get('anno_l1'), RENRCO : rec.get('numero_l1')};
    				        		
    				        		console.log(row_l1);
    				        		     acs_show_win_std( 'Distinta di lavorazione'
			   							   , 'acs_get_riga_componenti.php'
			   							   , {row : row_l1, from_lotto : 'Y', from_op : 'Y', k_ordine : rec.get('k_ordine'), riga_comp : rec.get('riga_comp'), desc_art: rec.get('RDDART'), cod_art : rec.get('RDART')}
			   							   , 1300, 450, null
			   							   , 'icon-folder_search-16'
			   							   ); 
    				        		}
    				    		});
    				    		}
	         		     
	         		        }
		    		        
		    		          voci_menu.push(
					          { text: 'Visualizza packing list'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function()  {
					          		show_win_colli_ord(rec, rec.get('k_ordine'));           		
		        		          }
		    		          }
		    		        );	
		    		        
		    		            voci_menu.push(
					          { text: 'Visualizza dettagli ordine'
					          , iconCls : 'icon-folder_search-16'
					          , handler: function()  {
					          		acs_show_win_std('Dettaglio ordini', '../desk_vend/acs_win_dettaglio_ordine.php?fn=open_win', 
	    			  				{k_ordine : rec.get('k_ordine')}, 400, 550, null, 'icon-leaf-16');	            		
		        		          }
		    		          }
		    		        );	
		    		        
		    		           if(rec.get('riga') == null){
		    		            voci_menu.push(
    					          { text: 'Elenco esploso distinta componenti'
    					          , iconCls : 'icon-leaf-16'
    					          , handler: function()  {
    					          		acs_show_win_std('Riepilogo articoli/componenti '+rec.get('ordine'), 'acs_elenco_riepilogo_art_comp.php?fn=open_elenco', {k_ordine: rec.get('k_ordine')}, 1300, 450, {}, 'icon-leaf-16');   	     	          		
    		        		          }
    		    		          }
    		    		        );	
    		    		        
    		    		        voci_menu.push({
				         		text: 'Riepilogo articoli/componenti',
				        		iconCls : 'icon-print-16',          		
				        		handler: function() {
				        			acs_show_win_std('Riepilogo articoli/componenti', 'acs_report_riepilogo_art_comp.php?fn=open_form', {k_ordine: rec.get('k_ordine')}, 330, 200, {}, 'icon-print-16');   	     	
				        		}
				    		  });
		    		           
		    		           }
		    		        
					        var menu = new Ext.menu.Menu({ items: voci_menu }).showAt(event.xy);
						  }	         
	         }
				
				
		} <!-- end grid -->
	   ]
        
 }



<?php 
	exit;
	}
	
	if ($_REQUEST['fn'] == 'open_parameters'){
	    $m_params = acs_m_params_json_decode();
	    $ta_sys = find_TA_sys('BTID', $m_params->tipologia);
	    $title = "[".$m_params->tipologia."] ".trim($ta_sys[0]['text']);
	    //descrizione
	    
	    
	    ?>

{"success":true, "items": [

     { xtype: 'form'
     , bodyStyle: 'padding: 10px'
     , bodyPadding: '5 5 0'
     , autoScroll:true
     , frame: true
     , layout: 
         {  type: 'vbox',
           align: 'stretch'
    	 }
    
     , items: [
       { 
		 xtype: 'fieldcontainer',
		 layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		 items: [	
	 {
	   xtype: 'datefield'
	   , flex : 1
	   , startDay: 1 //lun.
	   , fieldLabel: 'Data registrazione da'
	   , name: 'f_registrazione_da'
	   , labelWidth : 130
	   , format: 'd/m/Y'
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	   , anchor: '-15'
	 

	},{
	   xtype: 'datefield'
	   , flex : 1
	   , startDay: 1 //lun.
	   , fieldLabel: 'Data registrazione a'
	   , name: 'f_registrazione_a'
	   , format: 'd/m/Y'
	   , labelAlign: 'right'	
	   , labelWidth : 130
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	
	}
			
		   ]
		  }	
		 
	   
	 , { 
		 xtype: 'fieldcontainer',
		 layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		 items: [	
			 {
			   xtype: 'datefield'
			   , flex : 1
			   , startDay: 1 //lun.
			   , labelWidth : 130
			   , fieldLabel: 'Data evasione da'
			   , name: 'f_evasione_da'
			   , format: 'd/m/Y'
			   , submitFormat: 'Ymd'
			   , allowBlank: true
			   , anchor: '-15'
			 

			},{
			   xtype: 'datefield'
			   , flex : 1
			   , startDay: 1 //lun.
			   , labelWidth : 130
			   , fieldLabel: 'Data evasione a'
			   , name: 'f_evasione_a'
			   , format: 'd/m/Y'
			   , labelAlign: 'right'	
			   , submitFormat: 'Ymd'
			   , allowBlank: true
			
			}						   
	   	]
	  },
		{ 
		 xtype: 'fieldcontainer',
		 layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		 items: [	

		   {
			name: 'f_stato',
			flex: 1,
			xtype: 'combo',
			fieldLabel: 'Stati documento',
			labelAlign: 'right',
			displayField: 'text',
			labelAlign:'left',
			labelWidth : 130,							
			valueField: 'id',
			emptyText: '- seleziona -',
			forceSelection:true,
			allowBlank: true,
			multiSelect: true,
			queryMode: 'local',
            minChars: 1,
			store: {
				autoLoad: true,
				editable: false,
				autoDestroy: true,	 
				fields: [{name:'id'}, {name:'text'}],
				 data: [
                          <?php echo acs_ar_to_select_json(find_TA_sys('BSTA', null, null, $m_params->tipologia, null, null, 0, "", 'Y'), ""); ?>
				    ]
				},
			listeners: { beforequery: function (record) {
	        	 record.query = new RegExp(record.query, 'i');
	        	 record.forceAll = true;
             }}						 
			},{
			name: 'f_tipo',
			xtype: 'combo',
			fieldLabel: 'Tipi documento',
			flex: 1,
			labelWidth : 130,
			displayField: 'text',
			labelAlign: 'right',								
			valueField: 'id',
			emptyText: '- seleziona -',
			forceSelection:true,
			allowBlank: true,
		  	multiSelect: true,	
		  	queryMode: 'local',
            minChars: 1,													
			store: {
				autoLoad: true,
				editable: false,
				autoDestroy: true,	 
				fields: [{name:'id'}, {name:'text'}],
				 data: [
                          <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, $m_params->tipologia, null, null, 0, "", 'Y'), ""); ?>
				    ]
				},
			listeners: { beforequery: function (record) {
		         record.query = new RegExp(record.query, 'i');
		         record.forceAll = true;
	             }}							 
			}
			
		   ]
		  }, 
		  
		  
		  { 
		 	xtype: 'fieldcontainer',
		 	layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		 	items: [
		 		{
					xtype: 'checkboxgroup',
					fieldLabel: 'Includi sottorighe',
					margin : '0 0 0 0',
					labelWidth : 150,
				    items: [{
                        xtype: 'checkbox'
                      , name: 'f_sottorighe' 
                      , boxLabel: ''
                      , checked : true
                      , inputValue: 'Y'
                    }]							
				}
		 	]
		  }
		  	
											 	
		],	
		buttons: [
		<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "AN_DOC_{$m_params->tipologia}");  ?>
		'->',
		{
			xtype: 'button',
            scale: 'large',
            iconCls: 'icon-print-32',
            text: 'Report',	            
            handler: function() {
            	form = this.up('form').getForm();
	            this.up('form').submit({
                    url: 'acs_documenti_btid_report.php',
                    target: '_blank', 
                    standardSubmit: true,
                    method: 'POST',                        
                    params: {
                        form_values: Ext.encode(form.getValues()),
                        tipologia : <?php echo j($m_params->tipologia)?>,
    				    list_selected_id : Ext.encode(<?php echo acs_je($m_params->list_selected_id)?>)
				    }
          			});  
        	
            
                
            }
        },{
			xtype: 'button',
            scale: 'large',
            iconCls: 'icon-windows-32',
            text: 'Visualizza',	            
            handler: function() {
            	form = this.up('form').getForm();
            	   acs_show_win_std('Interrogazione articoli su righe documenti ' + <?php echo j($title)?> , 'acs_documenti_btid.php?fn=grid_righe', {
    				form_values: form.getValues(),
    				tipologia : <?php echo j($m_params->tipologia)?>,
    				list_selected_id : <?php echo acs_je($m_params->list_selected_id)?>
					},		        				 
    				900, 500, {}, 'icon-print-16');	
                   
        	
            }
        }
		
		]
        }
		
 				
	
     ]
        
 }



<?php 
	exit;
	}

if ($_REQUEST['fn'] == 'open_form'){

    ?>
	    {"success":true, "items": [
	    {
        xtype: 'form',
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        buttons: [{
	            text: 'Conferma',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
					 var loc_win = this.up('window');
					 if(form.isValid()){
					 
					  var title = form_values.f_d_tipologia;
			
					 	acs_show_win_std('Filtri testata documento ' + title, 'acs_documenti_btid.php?fn=open_parameters', {tipologia: form_values.f_tipologia, list_selected_id : <?php echo acs_je($m_params->list_selected_id)?>}, 750, 250, null, 'icon-folder_open-16');	
			         	loc_win.close();
			         }
			         }
			     }],   		
			  items: [
			    	{name: 'f_d_tipologia',
			  	     itemId : 'd_tipo',
				     xtype: 'textfield',
				     hidden : true
			        },
			  		{
					name: 'f_tipologia',
					xtype: 'combo',			
				 	flex: 1,
				 	anchor: '100%',
    				fieldLabel: 'Tipologia',
    				labelWidth : 80,
    				displayField: 'text',
    				valueField: 'id',
    				emptyText: '- seleziona -',
    				forceSelection: true,
    			   	allowBlank: false,
    				store: {
    					autoLoad: true,
    					editable: false,
    					autoDestroy: true,
    				    fields: [{name:'id'}, {name:'text'}],
    				    data: [
    					    <?php echo acs_ar_to_select_json($main_module->find_TA_std('BTID', null, 'N', 'N', null, null, null, 'N', 'Y'), '') ?>
    					    ]
				    }, listeners: {
                    	change: function(field,newVal) {	
                      		if (!Ext.isEmpty(newVal)){
                            	var value = field.rawValue;
                            	var d_tipo = this.up('form').down('#d_tipo');
                            	d_tipo.setValue(value);
                            	                          
                             }
                             
            
                            }, beforequery: function (record) {
                            record.query = new RegExp(record.query, 'i');
                            record.forceAll = true;
                        }
   }
				
					},
			    	
					 
	             	  ]
					
			    	
        
        }
	    
	    
	    
	    
	    ]}
<?php 
exit;
}



function crea_ar_righe_documento($list_art, $tipologia, $form_values){
    
    global $cfg_mod_DeskArt, $conn, $id_ditta_default;
    
    $sql_where = sql_where_by_params($form_values);
    $sql_where .= " AND RDART IN (" . sql_t_IN($list_art) . ")";
    $sql_where .= " AND TDTIDO = '{$tipologia}'";
    
   
    $sql = "SELECT TD.*, RD.*,  CF.CFRGS1 AS D_CF, RTPRZ
            FROM {$cfg_mod_DeskArt['file_testate_doc_gest']} TD
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_righe_doc_gest']} RD
              ON TD.TDDT = RD.RDDT AND TD.TDTIDO = RD.RDTIDO AND TD.TDINUM= RD.RDINUM AND TD.TDAADO = RD.RDAADO AND TD.TDNRDO = RD.RDNRDO 
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_righe_doc_gest_valuta']} RT
              ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_cli']} CF
              ON CF.CFDT = TD.TDDT AND CF.CFCD = TD.TDCCON
            WHERE TDDT= '{$id_ditta_default}' {$sql_where}
            ORDER BY TDDTRG";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $count_righe = 0;
    $count_docu = 0;
    $ar_docu = array();
    $ar = array();
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        $nr['documento']  = implode("_", array(trim($row['TDAADO']), sprintf("%06s", trim($row['TDNRDO']))));
        $nr['documento_aa']  = $row['TDAADO'];
        $nr['documento_nr']  = sprintf("%06s", trim($row['TDNRDO']));
        $nr['cod_cf']     = $row['TDCCON'];
        $nr['den_cf']     = $row['D_CF'];
                
        $nr['k_ordine']     = implode("_", array($row['TDDT'], $row['TDTIDO'], $row['TDINUM'], $row['TDAADO'], sprintf("%06s", trim($row['TDNRDO'])), " ", $row['TDDT'] ));        
        
        $nr['stato']	  = trim($row['TDSTAT']);
        $nr['tipo']	      = trim($row['TDTPDO']);
        $nr['riga']	      = trim($row['RDRIGA']);
        $nr['s_riga']	  = trim($row['RDSRIG']);
        $nr['c_art'] 	  = trim($row['RDART']);
        $nr['d_art']      = acs_toDb(trim($row['RDDART']));
        $nr['dim1']       = trim($row['RDDIM1']);
        $nr['dim2']       = trim($row['RDDIM2']);
        $nr['dim3']       = trim($row['RDDIM3']);
        $nr['u_m']        = trim($row['RDUM']);
        $nr['qta']        = trim($row['RDQTA']);
        $nr['data_reg']   = $row['TDDTRG'];
        $nr['data_eva']   = $row['TDDTEP'];
        $ar[] = $nr;
      
    }
  
    return $ar;
    
}



function sql_where_by_params($form_values){
    
    $reg_i = $form_values->f_registrazione_da;
    $reg_f = $form_values->f_registrazione_a;
    
    if ($form_values->f_sottorighe != 'Y'){
        $sql_where .= " AND RDSRIG = 0 ";
    }
    
    if (strlen($reg_i) > 0)
        $sql_where .= " AND TDDTRG >= '{$reg_i}'";
    if (strlen($reg_f) > 0)
        $sql_where .= " AND TDDTRG <= '{$reg_f}'";
    
    $eva_i = $form_values->f_evasione_da;
    $eva_f = $form_values->f_evasione_a;
    
    if (strlen($eva_i) > 0)
        $sql_where .= " AND TDDTEP >= '{$eva_i}'";
    if (strlen($eva_f) > 0)
        $sql_where .= " AND TDDTEP <= '{$eva_f}'";
                               
    $sql_where.= sql_where_by_combo_value('TDTPDO', $form_values->f_tipo);
    $sql_where.= sql_where_by_combo_value('TDSTAT', $form_values->f_stato);
                                
    
    return $sql_where;
}

