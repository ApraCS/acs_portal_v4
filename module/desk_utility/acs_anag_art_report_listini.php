<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$rp_listini =	$_REQUEST['rp_listini'];
$sql_where = sql_where_params($form_values);

if(strlen($form_values->f_utente) > 0)
    $utente = $form_ep->f_utente;
else
    $utente = trim($auth->get_user());

if(isset($form_values->f_todo) && strlen($form_values->f_todo) > 0){
    
    $sql_join .= "LEFT OUTER JOIN (
    SELECT *
    FROM {$cfg_mod_DeskUtility['file_assegna_ord']}
    ) AS0
    ON AS0.ASDT = AR.ARDT AND AS0.ASDOCU = AR.ARART";
}


if($rp_listini == 'A' && strlen(trim($form_values->f_forn_list)) > 0)
    $sql_where.= sql_where_by_combo_value('LI.LICCON', $form_values->f_forn_list);

if($rp_listini == 'V' && strlen(trim($form_values->f_forn_list)) > 0)
    $sql_where.= sql_where_by_combo_value('AR.ARFOR1', $form_values->f_forn_list);

if($rp_listini == 'A' && count($form_values->f_la) > 0)
    $sql_where.= sql_where_by_combo_value('LI.LILIST', $form_values->f_la);

if($rp_listini == 'V' && count($form_values->f_lv) > 0)
    $sql_where.= sql_where_by_combo_value('LI.LILIST', $form_values->f_lv);

    $sql = "SELECT AR.*, LI.*, CF_FORNITORE.CFRGS1 AS D_FORNITORE
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
                ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
                ON AU.AUDT = AR.ARDT AND AU.AUART = AR.ARART AND AU.AUUTEN = '{$utente}'
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_listini']} LI
                ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART AND LI.LITPLI = '{$rp_listini}'
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art']} AX
		        ON AX.AXDT = AR.ARDT AND AR.ARART = AX.AXART
            {$sql_join}
            WHERE ARDT = '{$id_ditta_default}' {$sql_where}
            ORDER BY LIDTGE, ARART
            FETCH FIRST 2000 ROWS ONLY";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr = $row;
        $ar[] = $nr;
    }

    

$c_forn = $form_values->f_forn_list;

$sql_f = "SELECT CFCD, CFRGS1, CFSC1, CFSC2, CFSC3, CFSC4, CFSC5, CFSC6, CFSC7, CFSC8
          FROM {$cfg_mod_Spedizioni['file_anag_cli']}
          WHERE digits(CFCD)= '{$c_forn}'
          AND CFDT = '{$id_ditta_default}' AND CFTICF = 'F'";

$stmt_f = db2_prepare($conn, $sql_f);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_f);
$row_f = db2_fetch_assoc($stmt_f);
$fornitore = trim($row_f['CFRGS1']);

$sc_art = n($row_f['CFSC1'],2)."%";
if($r['CFSC2'] > 0)
    $sc_art .= ", ". n($row_f['CFSC2'],2)."%";
if($r['CFSC3'] > 0)
    $sc_art .=  ", ". n($row_f['CFSC3'],2)."%";
if($r['CFSC4'] > 0)
    $sc_art .=  ", ". n($row_f['CFSC4'],2)."%";

$sc_amm = n($row_f['CFSC5'],2)."%";
if($r['CFSC6'] > 0)
    $sc_amm .= ", ". n($row_f['CFSC6'],2)."%";
if($r['CFSC7'] > 0)
    $sc_amm .=  ", ". n($row_f['CFSC7'],2)."%";
if($r['CFSC8'] > 0)
    $sc_amm .=  ", ". n($row_f['CFSC8'],2)."%";
            
            
   

echo "<div id='my_content'>";
echo "<div class=header_page>";
if ($rp_listini == 'A'){
    echo "<H2>Riepilogo listini di acquisto</H2>";
    echo "<b>[{$c_forn}] {$fornitore} </b>";
    echo " <span style = 'font-size: 15px;'> (Sconti anagrafici amm. {$sc_amm}, art. {$sc_art})</span>";
}else
	echo "<H2>Riepilogo listini di vendita</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";
echo "<tr class='liv_data'>";

echo "
	<th rowspan=2>Articolo</th>
	<th rowspan=2>Descrizione</th>
    <th rowspan=2>Listino</th>
    <th rowspan=2>U.M.</th>
	<th rowspan=2>Prezzo</th>
    <th rowspan=2>Val.</th>
    <th rowspan=2>Sconti</th>
    <th rowspan=2>Magg.</th>
    <th rowspan=2>Varianti</th>
    <th rowspan=2>Sigla</th>
    <th rowspan=2>Inserimento</th>
    <th colspan = 2 class='center'> Validit&agrave; </th>
      <tr class='liv_data'>
          <th>Iniziale</th>
          <th>Finale</th>
      </tr>
      ";

    
echo "</tr>";  

$ultimo_art='';

if(is_array($ar)){
foreach ($ar as $kar => $r){

    if(trim($r['ARART']) != $ultimo_art){
        $c_art = trim($r['ARART']);
        $d_art = trim($r['ARDART']);
       
        
    }else {
        $c_art = "";
        $d_art = "";
    }
    
    $varianti = "";
    
    $val_risp = find_TA_sys('PUVN', trim($r['LIVAR1']), null, trim($r['ARVAR1']), null, null, 0, '', 'N', 'Y');
    if(trim($r['LIVAR1']) != '')
        $varianti .= "[".trim($r['LIVAR1'])."] ".$val_risp[0]['text'];
        $val_risp = find_TA_sys('PUVN', trim($r['LIVAR2']), null, trim($r['ARVAR2']), null, null, 0, '', 'N', 'Y');
    if(trim($r['LIVAR2']) != '')
        $varianti .= "<br>[".trim($r['LIVAR2'])."] ".$val_risp[0]['text'];
        $val_risp = find_TA_sys('PUVN', trim($r['LIVAR3']), null, trim($r['ARVAR3']), null, null, 0, '', 'N', 'Y');
    if(trim($r['LIVAR3']) != '')
        $varianti .= "<br>[".trim($r['LIVAR3'])."] ".$val_risp[0]['text'];
    
    if(trim($r['LINOTE']) != '' ){
        if($varianti == "")
            $varianti .= "Rif.listino: ".trim($r['LINOTE']);
        else  
            $varianti .= "<br>Rif.listino: ".trim($r['LINOTE']);
    }
     
        $sconti = n($r['LISC1'],2)."%";
        if($r['LISC2'] > 0)
            $sconti .= " - ". n($r['LISC2'],2)."%";
        if($r['LISC3'] > 0)
            $sconti .=  " - ". n($r['LISC3'],2)."%";
        if($r['LISC4'] > 0)
            $sconti .=  " - ". n($r['LISC4'],2)."%";
       
        if($r['LIMAGG'] > 0)
            $magg = "<b>+</b>".n($r['LIMAGG'],2)."%";
        else 
            $magg = "&nbsp;";
 
    
	echo "<tr>";
	echo "  <td>".$c_art."</td>
   			<td>".$d_art."</td>
            <td>".$r['LILIST']."</td>
            <td>".$r['LIUM']."</td>
           	<td class = 'number'>".n($r['LIPRZ'])."</td>
            <td>".$r['LIVALU']."</td>
            <td>".$sconti."</td>
            <td>".$magg."</td>
            <td>".$varianti."</td>
            <td>".$r['LIFLG1']."</td>
            <td>".print_date($r['LIDTGE'])."</td>
            <td>".print_date($r['LIDTDE'])."</td>
            <td>".print_date($r['LIDTVA'])."</td>";
    echo "</tr>";
            
            $ultimo_art =  trim($r['ARART']);

            
}

}


?>
</div>
</body>
</html>	

