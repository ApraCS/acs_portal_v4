<?php

require_once "../../config.inc.php";
require_once("acs_gestione_tabelle_include.php");

$s = new Spedizioni(array('no_verify' => 'Y'));
$main_module = new DeskUtility();
$desk_art = new DeskArt();
$m_params = acs_m_params_json_decode();

function m_get_fields($m_table_config, $m_table_config_altro = array(), $m_table_config_docu = array(), $m_table_config_altro_cv = array()){
    $r = array();
    foreach ($m_table_config['fields'] as $kf=>$f)
        $r[] = $kf;
    
     if($m_table_config['TAID'] == 'PUVN'){
        $r[] = 'esau';
        $r[] = 'red';
        $r[] = 'green';
        $r[] = 'blue';
     }

     if(isset($m_table_config_altro) && count($m_table_config_altro) > 0){
         foreach ($m_table_config_altro['fields'] as $kf=>$f)
             $r[] = $kf;
     }
     
     if(isset($m_table_config_docu) && count($m_table_config_docu) > 0){
         foreach ($m_table_config_docu['fields'] as $kf=>$f)
             $r[] = $kf;
     }
     
     if(isset($m_table_config_altro_cv) && count($m_table_config_altro_cv) > 0){
         foreach ($m_table_config_altro_cv['fields'] as $kf=>$f)
             $r[] = $kf;
     }
     
     
     if($m_table_config['TAID'] == 'MURC'){
         for($i=0; $i <= 14; $i++){
             $r[] = "frz_{$i}";
             $r[] = "chr_ric_{$i}"; 
             
         }
     
     }
     
     if($m_table_config['TAID'] == 'PUFD'){
         for($i=0; $i <= 14; $i++)
             $r[] = "coaf_{$i}";
     }
     
     $r[] = 'c_var';
     $r[] = 'c_seq';
     $r[] = 'descrizione';
     $r[] = 'is_master';
   
     return acs_je($r);
}


function m_get_columns($m_table_config){
      
    foreach ($m_table_config['fields'] as $kf=>$f) {
        if(!isset($f['hidden']))
            $hidden = 'false';
        else 
            $hidden = $f['hidden'];
        
        if(!isset($f['c_width']))
            $width = "flex : 1";
        else
            $width = "width : {$f['c_width']}";
        
        if(isset($f['short']))
            $header = $f['short'];
        else
            $header = $f['label'];
        
        $renderer = "";
        if($kf == 'TATP'){
            $renderer = "renderer: function(value, metaData, record){
		    			  if(record.get('TATP') == 'S'){ 
                               metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			       return '<img src=" .  img_path("icone/48x48/divieto.png") . " width=15>';
		    		 	   }

                            if (record.get('esau') == 'E'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('In esaurimento') + '\"';
					   			return '<img src=". img_path("icone/48x48/clessidra.png") ." width=15>';}
							if (record.get('esau') == 'A'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('In avviamento') + '\"';
					   			return '<img src=".img_path("icone/48x48/label_blue_new.png") ."width=18>';}
				    		if (record.get('esau') == 'C'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('In codifica') + '\"';
					   			return '<img src= ". img_path("icone/48x48/design.png")." width=15>';}
                            if (record.get('esau') == 'R'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Riservato') + '\"';
					   			return '<img src= ". img_path("icone/48x48/folder_private.png")." width=15>';}
                            if (record.get('esau') == 'F'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Fine codifica') + '\"';
					   			return '<img src= ". img_path("icone/48x48/button_blue_play.png")." width=15>';}
		    		 
		    		  }";
        }
            
        if($kf == 'MODE'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('MODE') > 0){
		    			   return '<img src=" .  img_path("icone/48x48/game_pad.png") . " width=15>';
		    		 	}
		    			       
		    		  }";
            
        }
        
        if($kf == 'TRAD'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('TRAD') > 0){
		    			   return '<img src=" .  img_path("icone/48x48/globe.png") . " width=15>';
		    		 	}

                       
		    		  }";
            
        }
        
        if($kf == 'PUSX'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('PUSX') > 0){
		    			   return '<img src=" .  img_path("icone/48x48/barcode.png") . " width=15>';
		    		 	}
		    			       
		    		  }";
            
        }
        
        if($kf == 'PUSQ'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('PUSQ') > 0){
		    			   return '<img src=" .  img_path("icone/48x48/button_black_play.png") . " width=15>';
		    		 	}
		    			       
		    		  }";
            
        }
        
        if($kf == 'PUVN'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('PUVN') > 0){
		    			   return '<img src=" .  img_path("icone/48x48/search.png") . " width=15>';
		    		 	}
		    			       
		    		  }";
            
        }
        
        if($kf == 'PUFD'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('PUFD') > 0){
		    			   return '<img src=" .  img_path("icone/48x48/search.png") . " width=15>';
		    		 	}
		    			       
		    		  }";
            
        }
        
        if($kf == 'TANR'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('esau') == 'E'){
		    			    metaData.tdCls += ' sfondo_giallo';
		    		 	 }

                         return value;     
		    		  }";
            
        }
        
        if($kf == 'RGB'){
            
            $renderer = "renderer: function(value, metaData, record){
   
                         if(typeof record.get('rgb') !== 'undefined' && record.get('rgb') != ''){
                            var red = record.get('rgb').substring(0, 3);
                            var green = record.get('rgb').substring(3);
                            var blue = record.get('rgb').substring(6);
                            green = green.substring(0,3);
                            metaData.style += 'background-color: rgb(' + red + ', '+ green + ', ' + blue + ');';
                            //if(parseInt(red) < 50 && parseInt(green) < 50 && parseInt(blue) < 50) metaData.style += 'color:white;';
		    		 	  }
		    			  return '';
		    		  }";
            
            
        }
        
        if($kf == 'TADESC'){
            
            $renderer = "renderer: function(value, metaData, record){
                           return record.get('descrizione');
		    		  }";
            
            
        }
      
        
        $view = true;
        if($f['only_view'] == 'F')
            $view = false;
            
        if($view == true){
        
        ?>
		 {
			header: <?php echo j($header)?>,
		 	dataIndex: <?php echo j($kf)?>, 
		 	hidden: <?php echo $hidden ?>, 
		 	<?php if(isset($f['tooltip'])){?>
		 		tooltip: <?php echo j($f['tooltip']); ?>,
		 	<?php }?>	 
		    <?php echo $width ?>,
		 	filter: {type: 'string'}, filterable: true,
		 	<?php echo $renderer ?>
		 },		
		<?php
		
            }
	}
	
	?>
	
	  {header: 'Immissione',
	 columns: [
	 {header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
	 renderer: function(value, metaData, record){
	         if (record.get('TADTGE') != record.get('TADTUM')) 
	          metaData.tdCls += ' grassetto';
	 
             q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		     return date_from_AS(value);	
	}
	 
	 }
	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
	  renderer: function(value, metaData, record){
	  
	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
	          metaData.tdCls += ' grassetto';
	  
             q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		     return value;	
	} 
	 }
	 ]}
	 
	 <?php 
}


function m_get_comp_form($m_table_config, $m_params){
    global $main_module, $desk_art, $s;
    
   
    $f_combo = "";
    
    if($m_table_config['TAID'] == 'PUOP'){
        for($i = 0; $i<=20; $i++)
            unset($m_table_config['fields']["var{$i}"]);
    }  
    
    if($m_table_config['TAID'] == 'PUVN'){
        unset($m_table_config['fields']['var_or']);
        unset($m_table_config['fields']['var_dp']);
        unset($m_table_config['fields']['agente']);
        unset($m_table_config['fields']['royal']);
    }  
    
    
    if($m_table_config['TAID'] == 'PUVN' || $m_table_config['TAID'] == 'PUVR'){
        unset($m_table_config['fields']['TANR']);
        unset($m_table_config['fields']['TACINT']);
       
       
        
        ?>
        {
            xtype: 'fieldcontainer',
            flex:1,
            layout: { 	type: 'hbox',
            pack: 'start',
            align: 'stretch'},
            items: [
              {
    			xtype : 'textfield',
    		 	name: 'TANR', 
    		 	maxLength : 4,
    		 	fieldLabel: 'Codice',
    		 	labelWidth : 120,
    		 	width : 160,
    		 	listeners: {
    				'change': function(field){
    					 field.setValue(field.getValue().toUpperCase());
      				},
      				'blur': function(field) {
      				    var form = this.up('form').getForm();
      				    var grid = this.up('form').up('panel').down('grid');
      				    
      					Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				codice : field.getValue(),
			        				tacor2 : <?php echo j($m_params->tacor2); ?>,
			        				tacor1 : <?php echo j($m_params->c_art); ?>, 
			        				
								},				
										        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        
							        if(jsonData.success == false){
					      	    		 acs_show_msg_error(jsonData.msg_error);
							        	 form.setValues(jsonData.record);
			        			   		 var rec_index = grid.getStore().findRecord('TANR', jsonData.record.TANR);
		  					   			 grid.getView().select(rec_index);
			  					   	     grid.getView().focusRow(rec_index);
				        			}
						
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
                	 		

            		}
 				}
    		  },	{
    			xtype : 'textfield',
    		 	name: 'TACINT', 
    		 	fieldLabel: 'Interfaccia',
    		 	labelWidth : 60,
    		 	maxLength : 10,
    		 	<?php if($m_table_config['TAID'] == 'PUVR'){?>
    		 	margin : '0 0 0 24',
    		 	<?php }else{?>
    		 	margin : '0 0 0 8',
    		 	<?php }?>
    		 	width : 180
    		  }		
                
            ]},
        <?php 
        
    }
    
    $n_fc = '';
    
    foreach ($m_table_config['fields'] as $kf=>$f) {
        
        //hidden
        if(!isset($f['hidden']))
            $hidden = 'false';
        else
            $hidden = $f['hidden'];
        
        
        //width
        if(!isset($f['width']))
            $width = "flex : 1, anchor: '-15'";
        else
            $width = "width : {$f['width']}";
        
            
        //maxLength
        if(!isset($f['maxLength']))
            $maxlen = 1024;
        else
            $maxlen = $f['maxLength'];
        
        if(!isset($f['maskRe']))
            $maskRe = "";
        else
            $maskRe = "maskRe : {$f['maskRe']}, ";
        
        
        if(isset($f['upper'])){
            $upper = " 'change': function(field){
        				  field.setValue(field.getValue().toUpperCase());
      					}
 						 ";
        }else{
            $upper = "";
        }
        
        
        if(isset($f['c_num'])){
            $c_num = " 'blur': function(field) {
                	      console.log('aaa');
        					var n_value = field.getValue();
        					if(n_value > 0){	
        						n_value = (n_value < 10 ? '0' : '') + n_value;
        						field.setValue(n_value);
        					}
        				}
 						 ";
        }else{
            $c_num = "";
        }
                
        if(isset($f['check'])){
            $check = " , 'blur': function(field) {
      				    var form = this.up('form').getForm();
                        var grid = this.up('form').up('panel').down('grid');
                      
      					Ext.Ajax.request({
						        url        : '".$_SERVER['PHP_SELF']."?fn=exe_check',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				codice : field.getValue(),
                                    tacor2 : '".$m_params->tacor2."',
                                    tacor1 : '".$m_params->c_art."'      

								},							        
						        success : function(result, request){
                                    var jsonData = Ext.decode(result.responseText);
							        if(jsonData.success == false){
					      	    		 acs_show_msg_error(jsonData.msg_error);
							        	 form.setValues(jsonData.record);
			        			   		 var rec_index = grid.getStore().findRecord('TANR', jsonData.record.TANR);
		  					   			 grid.getView().select(rec_index);
			  					   	     grid.getView().focusRow(rec_index);
				        			}
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
                	 		

            		}
 						 ";
        }else{
            $check = "";
        }
        
        
        
        //view
        $view = true;    
        if($f['only_view'] == 'C')
            $view = false;
        
        if($view == true){
     
            //xtype
            if(!isset($f['xtype'])){
                
                if($f['fieldcontainer'] == 'Y'){?>
          
  		 	  { 
				xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox',
						    pack: 'start',
						    align: 'stretch'},		
				<?php if(trim($m_params->master) != '' && $m_params->is_master != 'Y'){ ?>
				  defaults: {readOnly: true},
				<?php }?>		
				items: [
				
				   {
                    xtype : 'textfield',
        		 	name: 'red',
        		 	fieldLabel: 'RGB',
        		 	labelWidth : 120,
        		 	width : 170,
        		 	maxLength : 3,
        		 	listeners: {
        				'blur': function(field) {
        					var n_value = field.getValue();
        					if(parseInt(n_value) > 255){
        						field.setValue('');	 
        						acs_show_msg_error('Impossibile inserire un valore maggiore di 255');
					      	    return false;
					      	}
					      	
        				}
        			}
         				
        		   }, {
                    xtype : 'textfield',
        		 	name: 'green',
        		 	fieldLabel: '',
        		 	width : 45,
        		 	maxLength : 3,
        		 	listeners: {
        				'blur': function(field) {
        					var n_value = field.getValue();
        					if(parseInt(n_value) > 255){
        						field.setValue('');	 
        						acs_show_msg_error('Impossibile inserire un valore maggiore di 255');
					      	    return false;
					      	}
					      	
					      	
        				}
        			}
        	
        		   }, {
                    xtype : 'textfield',
        		 	name: 'blue',
        		 	fieldLabel: '',
        		 	width : 45,
        		 	maxLength : 3,
        		 	listeners: {
        				'blur': function(field) {
        					var n_value = field.getValue();
        					if(parseInt(n_value) > 255){
 								field.setValue('');	       					
        						acs_show_msg_error('Impossibile inserire un valore maggiore di 255');
					      	    return false;
					      	}
					      	
        				}
        			}
        	
        		  }
				
				   
				]},
							
              
         <?php }else{ ?>
           {
            xtype : 'textfield',
		 	name: <?php echo j($kf)?>,
		 	fieldLabel: <?php echo j($f['label'])?>,
		 	labelWidth : 120,
		 	<?php echo $width ?>,
		 	maxLength : <?php echo $maxlen ?>,
		 	<?php echo $maskRe ?>
		 	hidden : <?php echo $hidden ?>,
		 	listeners: {
		 	  <?php echo $upper ?>
		 	  <?php echo $check ?>
		 	  <?php echo $c_num ?>
		 	}
		 	
		  },
              
              
       <?php   }
          
      
            }elseif($f['xtype'] == 'combo_tipo'){
                ?>
		     {
			xtype : 'combo',
		 	name: <?php echo j($kf)?>, 
		 	fieldLabel: <?php echo j($f['label'])?>,
		 	labelWidth : 120,
		 	<?php echo $width ?>,
		 	hidden : <?php echo $hidden ?>,
			displayField: 'text',
		    valueField: 'id',	
		    value : '',						
			emptyText: '- seleziona -',
		   	queryMode: 'local',
     		minChars: 1,
     		 <?php if(isset($f['store_bdoc']) == 'Y'){?>
     		 
     		  store: {
			        autoLoad: true,
					proxy: {
			            type: 'ajax',
			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
			            actionMethods: {
				          read: 'POST'
			        },
	                	extraParams: {
	                	    tabella : <?php echo j($f['tab_sys']); ?>,
	    		    		domanda: '',
	    				},				            
			            doRequest: personalizza_extraParams_to_jsonData, 
			            reader: {
			            type: 'json',
						method: 'POST',						            
				            root: 'root'						            
				        }
			        },       
					fields: ['id', 'text'],		             	
	            }
     		 
     		 <?php }else{?>
     		 store: {
			
    			editable: false,
    			autoDestroy: true,
    			fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		     	<?php 
    		     	      if(isset($f['tab_sys']) && strlen($f['tab_sys']) > 0){
    		                 if(isset($f['tacor2'])) $tacor2 = $f['tacor2'];
    		                 else $tacor2 = '';
    		                 echo acs_ar_to_select_json(find_TA_sys($f['tab_sys'], null, null, $tacor2, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); 
            		      }else if(isset($f['tab_std']) && strlen($f['tab_std']) > 0){
            		          echo acs_ar_to_select_json($desk_art->find_TA_std($f['tab_std'], null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y');
            		      }else{
    		                  echo acs_ar_to_select_json(find_TA_sys($m_table_config['tab_tipo'], null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y'); 
    		     	   }
    		     	
    		     	?>	
    		    ]
			}
     		 
     		 <?php }?>
			, listeners: { 
			 		beforequery: function (record) {
    	         		record.query = new RegExp(record.query, 'i');
    	         		record.forceAll = true;
    				 },
                  change: function(field,newVal) {
                      if (this.getValue() === null) {
                        this.setValue('');
                      }
                      
                      <?php if(isset($f['list_btid']) == 'Y'){?>
                      
                      		if (!Ext.isEmpty(newVal)){
                  				combo_risp = this.up('form').getForm().findField('tpco');                      		 
                         		combo_risp.store.proxy.extraParams.domanda = newVal;
                        		combo_risp.store.load();                          
                     		}
                      
                      <?php }?>
                }
    				 
   
    			
 			},	
		 	
		  },		
		  <?php }elseif($f['xtype'] == 'fieldcontainer'){ 
		      if($n_fc != $f['n_fc']) {?>
		       { 
				xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox',
						    pack: 'start',
						    align: 'stretch'},						
				items: [
				<?php } 
				    if($f['type'] == 'combo'){
				    
				        if (isset($f['labelSeparator'])) $lsep = "labelSeparator : '', ";
				        ?>
				  {
        			xtype : 'combo',
        		 	name: <?php echo j($kf)?>, 
        		 	fieldLabel: <?php echo j($f['label'])?>,
        		 	labelAlign : <?php echo j($f['labelAlign'])?>,
        		 	labelWidth : <?php echo j($f['labelWidth'])?>,
        		 	<?php echo $width ?>,
        		 	<?php echo $lsep; ?>
        		 	<?php if($kf == 'a_dim'){ ?>
        		 	itemId : 'a_dim',
        		 	<?php }?>
        		 	forceSelection: true,  							
        			displayField: 'text',
        		    valueField: 'id',							
        			emptyText: '- seleziona -',
        		   	queryMode: 'local',
             		minChars: 1,
         			<?php if($kf == 'a_dim'){ ?>
         			store: {
						xtype: 'store',
						autoLoad:true,
			            proxy: {
						   url: 'acs_tab_sys_PUCI.php?fn=get_json_data', 
						   method: 'POST',								
						   type: 'ajax',
						   actionMethods: {
					          read: 'POST'
					        },
						   extraParams: {
								 cgs: ''
	        				},
			        		doRequest: personalizza_extraParams_to_jsonData, 
						    reader: {
					            type: 'json',
								method: 'POST',						            
					            root: 'root'						            
					        }
							},
							fields: [{name:'id'}, {name:'text'}]							
									
					}
         			
         			<?php }else{?>
        			store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}],
            		    data: [	
            		    <?php if(isset($f['tab_sys']) && strlen($f['tab_sys']) > 0)
            		        echo acs_ar_to_select_json(find_TA_sys($f['tab_sys'], null, null, null, null, null, 0, '', 'Y'), '', true, 'N', 'Y');
            		      
            		          if(isset($f['tab_std']) && strlen($f['tab_std']) > 0)
            		              echo acs_ar_to_select_json($desk_art->find_TA_std($f['tab_std'], null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y');
            		      ?> 	    
            		    ]
        			}
        			<?php }?>
        			
        			
        			, listeners: { 
        			 		beforequery: function (record) {
            	         		record.query = new RegExp(record.query, 'i');
            	         		record.forceAll = true;
            	         		
            	         		}
            	         		<?php if($kf == 'cgs'){?>
            	         		,  change: function(ele, newValue, oldValue) {
            	         		    var ep_adim = this.up('panel').down('#a_dim').store.proxy.extraParams;
            	         		    ep_adim.cgs = newValue;
            	         		    this.up('panel').down('#a_dim').getStore().load();
            	         		     
            	         		 }
            	         		<?php }?>				        	         		
            	         		
         			}	
        		 	
        		  },
				
				
				<?php }elseif($f['type'] == 'number'){ 
				    if(isset($f['maxValue']))
				        $maxValue = "maxValue : {$f['maxValue']}, ";
				   
				    ?>
				
				   {
                    xtype : 'numberfield',
        		 	name: <?php echo j($kf)?>, 
        		 	labelWidth : 120,
    	 	        fieldLabel: <?php echo j($f['label'])?>,
    	 	        <?php echo $width ?>,
    	 	        <?php echo $maxValue ?>
    	 	        hideTrigger:true,
    	 	        decimalPrecision : 2,
    	 	        keyNavEnabled : false,
    				mouseWheelEnabled : false,
    	 	        labelAlign : <?php echo j($f['labelAlign'])?>,
        		 	labelWidth : <?php echo $f['labelWidth']; ?>  		 	
        		   },
				
				<?php }else{?>
			
				   {
                    xtype : 'textfield',
        		 	name: <?php echo j($kf)?>, 
		 	        fieldLabel: <?php echo j($f['label'])?>,
		 	        <?php echo $width ?>,
		 	        <?php echo $maskRe ?>
		 	        maxLength : <?php echo $maxlen ?>, 
		 	        labelAlign : <?php echo j($f['labelAlign'])?>,
        		 	labelWidth : <?php echo $f['labelWidth']; ?>,
        		 	<?php if($f['listener'] == 'Y'){?>
        		 	 listeners: {
        				'blur': function(field) {
        					var n_value = field.getValue();
        					if(n_value > 0){	
        						n_value = (n_value < 10 ? '0' : '') + n_value;
        						field.setValue(n_value);
        					}
        				}
            		}
        		 	<?php }?>
        		   },
				
			
	          <?php  
				}
	          if($f['close'] == 'Y') {  ?> ]},  <?php }
		
		  }elseif($f['xtype'] == 'datefield'){
		      
		      if($f['submit'] != '')
		          $submitFormat = $f['submit'];
		      else
		          $submitFormat = 'Ymd'
		      
		      ?>
		      {
			   xtype: 'datefield'
			   , startDay: 1 //lun.
			   , name: <?php echo j($kf)?>
		 	   , fieldLabel: <?php echo j($f['label'])?>
		 	   ,<?php echo $width ?>
		 	   , labelWidth : 120
			   , format: 'd/m/y'
        	   , submitFormat: <?php echo j($submitFormat); ?>
			   , allowBlank: true
			   , anchor: '-15'
			   , listeners: {
			       invalid: function (field, msg) {
			       Ext.Msg.alert('', msg);}
					}
			},
		   
		      
		      
		      <?php 
		      
		  }elseif($f['xtype'] == 'articolo'){?>
		      
		      {   xtype: 'fieldcontainer',
		          layout: 'hbox',
		      	  margin : '0 0 0 128',
		          items: [
						<?php for ($i = 1; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', width : 18, fieldStyle:'font-size:10px;'},
						<?php }?>	
						 ]
				}, { 
						xtype: 'fieldcontainer',
						margin : '0 0 5 0',
						layout: { 	type: 'hbox',
									pack: 'start',
									align: 'stretch',
									frame: true},
						defaults: {xtype: 'textfield', width: 18, margin : '0 0 0 0', maxLength: 1},
						items: [
	                        {xtype: 'label', text: <?php echo j($f['label'])?>, width: 125},
							<?php for ($i = 0; $i <= 14; $i++){?>
								{ name: '<?php echo "{$kf}_{$i}"; ?>',
								  listeners: {
								    
									'change': function(field){
									 value = this.getValue().toString();
									if (value.length == 1) {
										var nextfield = field.nextSibling();
										if (!Ext.isEmpty(nextfield))
											nextfield.focus(true, 0);
									  } 
									  
									  field.setValue(field.getValue().toUpperCase());
										
								  }
								}
							  },
							<?php }?>
							
						]						
					}, <?php 
		      
		  }elseif($f['xtype'] == 'fornitore'){?>
		      
		       {
                    xtype: 'combo',
        			name: <?php echo j($kf)?>,
		 	        fieldLabel: <?php echo j($f['label'])?>,
        			labelWidth : 120,
        			minChars: 1,
        			allowBlank : true,	
                    store: {
                    	pageSize: 1000,
                    	proxy: {
        		            type: 'ajax',
        		            url : <?php echo acs_je('../desk_gest/acs_get_select_json.php?select=search_fornitori'); ?>,
        		            reader: {
        		                type: 'json',
        		                root: 'root',
        		                totalProperty: 'totalCount'
        		            },
        		              actionMethods: { read: 'POST'},
        		              doRequest: personalizza_extraParams_to_jsonData
        		        }, 
        		        fields: ['cod', 'descr', 'out_loc'],		             	
                    },
                    valueField: 'cod',                        
                    displayField: 'descr',
                    typeAhead: false,
                    hideTrigger: true,
                    anchor: '-15',
                    listConfig: {
                        loadingText: 'Searching...',
                        emptyText: 'Nessun cliente trovato',
                       //Custom rendering template for each item
                        getInnerTpl: function() {
                            return '<div class="search-item">' +
                                '<h3><span>{descr}</span></h3>' +
                                '[{cod}] {out_loc}' + 
                            '</div>';
                        }                
                        
                    },
                    pageSize: 1000
                }, 
		      
		  <?php 
		  }elseif($f['xtype'] == 'number'){?>
		      {
                xtype : 'numberfield',
    		 	name: <?php echo j($kf)?>, 
    		 	labelWidth : 120,
	 	        fieldLabel: <?php echo j($f['label'])?>,
	 	        <?php echo $width ?>,
	 	        hideTrigger:true,
	 	        decimalPrecision : 2,
	 	        keyNavEnabled : false,
				mouseWheelEnabled : false,
	 	       // maxLength : <?php echo $maxlen ?>, 
	 	       // labelAlign : <?php echo j($f['labelAlign'])?>,
    		  
    		 	
    		   },
		  
		  <?php }elseif($f['xtype'] == 'display'){ ?>
		      
		       {xtype : 'displayfield',
    		 	name: <?php echo j($kf)?>, 
    		 	labelWidth : 200,
    		 	labelSeparator : '',
	 	        fieldLabel: <?php echo j($f['label'])?>,
	 	        <?php echo $width ?>,
	 	      
    		   },
		      
		 <?php  }
 
        }
        
            $n_fc = $f['n_fc'];
		
        
	}
}



if ($_REQUEST['fn'] == 'exe_check'){
    
    $ret = array();
    
    if(strlen(trim($m_params->tacor2)) > 0)
        $tacor2 = trim($m_params->tacor2);
    
    if(isset($m_params->tacor1) && strlen(trim($m_params->tacor1)) > 0)
        $tacor1 = trim($m_params->tacor1);
   
    
    if($m_table_config['TAID'] == 'PUVN' || $m_table_config['TAID'] == 'PUSQ')
        $row = get_TA_sys($m_table_config['TAID'], $m_params->codice, null, $tacor2);
    else if($m_table_config['TAID'] == 'BCER' || $m_table_config['TAID'] == 'PUFD')
        $row = get_TA_sys($m_table_config['TAID'], $m_params->codice, $tacor1);
    else
        $row = get_TA_sys($m_table_config['TAID'], $m_params->codice);
    
    
    if($row != false && count($row) > 0){
        $ret['msg_error'] = "Codice esistente";
        $ret['success'] = false;
        
        $sql_s = "SELECT RRN(TA) AS RRN, TA.*
        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_table_config['TAID']}'
        AND TANR = '{$m_params->codice}'";
        
        $stmt_s = db2_prepare($conn, $sql_s);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_s);
        echo db2_stmt_errormsg($stmt_s);
        $row_s = db2_fetch_assoc($stmt_s);
        $row_s = get_m_row($row_s, $m_table_config);
        
            
        $ret['record'] = $row_s;
    }else{
        $ret['success'] = true;
    }
  
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_sos_att'){
    
    $ar_upd = array();
    
    $tatp = trim($m_params->row->TATP);
  
    if($tatp == "")
        $new_value = "S";
    else
        $new_value = "";
    
        
    $ar_upd['TAUSUM'] = $auth->get_user();
    $ar_upd['TADTUM'] = oggi_AS_date();
    $ar_upd['TATP']   = $new_value;
    
    $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(TA) = '{$m_params->row->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'exe_canc'){
    
    $tab = $m_params->form_values->TAID;
    
    if($tab == 'PUVR'){
        
        $row = get_TA_sys('PUVN', null, null, $m_params->form_values->TANR);
        
        if($row != false && count($row) > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Richiesta non disponibile! <br> (Variabile con Varianti abbinate)";
            echo acs_je($ret);
            return;
        }else{
            $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                    WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
          }
        
    }else if($tab == 'PUFI'){
        
        $row = get_TA_sys('PUFD', null, $m_params->form_values->TANR);
        
        if($row != false && count($row) > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Richiesta non disponibile! <br> (Scheda ferramenta con dettagli abbinati)";
            echo acs_je($ret);
            return;
        }else{
            $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
        }
        
    }else if($tab == 'PUVN'){
        
        $var = $m_params->form_values->TACOR2;
        $van = $m_params->form_values->TANR;
        $count = 0;
        //VALORI AMMESSI
        $sql = "SELECT COUNT(*) AS C_ROW 
                FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                WHERE MODT = '{$id_ditta_default}' AND MOVARI = '{$var}' AND MOVALO = '{$van}'
                ";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        $count += $row['C_ROW'];
        
        //VALORI ASSUNTI
        $sql_a = "SELECT COUNT(*) AS C_ROW
                  FROM {$cfg_mod_DeskUtility['file_config']} MD
                  WHERE MDDT = '{$id_ditta_default}' AND MDVARI = '{$var}' AND MDVAL1 = '{$van}'
                  AND MDSEQC='999'";
        
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_a);
        $row_a = db2_fetch_assoc($stmt_a);
        $count += $row_a['C_ROW'];
        
        $sql_where = " AND (";
        for($i = 1; $i <= 12 ; $i++)
            $sql_where .= " (MDVAR{$i} = '{$var}' AND MDVAL{$i} = '{$van}' AND MDTPV{$i} <> 'T') OR";
         $sql_where .= " 1=2 )";
        
        //CONDIZIONI
        $sql_c = "SELECT COUNT(*) AS C_ROW
                  FROM {$cfg_mod_DeskUtility['file_config']} MD
                  WHERE MDDT = '{$id_ditta_default}' {$sql_where}
                  AND MDSEQC <> '999'";
        
        $stmt_c = db2_prepare($conn, $sql_c);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_c);
        $row_c = db2_fetch_assoc($stmt_c);
        $count += $row_c['C_ROW'];
        
        //VARIABILI COLLEGATE
        if($m_params->is_master == 'Y'){
            $master = $m_params->form_values->TACOR2;
            $tanr = $m_params->form_values->TANR;
            
            $sql_vc = "SELECT COUNT(*) AS C_ROW
                      FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                      INNER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA2
                      ON TA.TADT = TA2.TADT AND TA2.TANR = '{$tanr}' AND TA2.TACOR2 = TA.TANR AND TA2.TAID = 'PUVN'
                      WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'PUVR'
                      AND SUBSTRING(TA.TAREST, 68, 3) = '{$master}' AND TA.TANR <> '{$master}'
                        ";
                        
            $stmt_vc = db2_prepare($conn, $sql_vc);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_vc);
            $row_vc = db2_fetch_assoc($stmt_vc);
            $count += $row_vc['C_ROW'];
        }
        
        if($count > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Richiesta non disponibile! <br> (Variante presente in configurazione o in variabili collegate)";
            echo acs_je($ret);
            return;
        }else{
            $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                    WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
        }
        
    }else if($tab == 'PUMO'){
        $count_pusx = 0;
        $count_pusq = 0;
        $count = 0;
        $row = get_TA_sys('PUSX', $m_params->form_values->TANR);
        if($row != false && count($row) > 0)
            $count_pusx ++;
  
        $row_q = get_TA_sys('PUSQ', null, null, $m_params->form_values->TANR);
        if($row_q != false && count($row_q) > 0)
            $count_pusq ++;
        
        $sql = "SELECT COUNT(*) AS C_ROW
                FROM {$cfg_mod_DeskUtility['file_config']} MD
                WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$m_params->form_values->TANR}'
                AND MDSEQC = '999' AND MDVARI <> ''"; 
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        $count += $row['C_ROW'];
        
        if($count > 0){
            $ret['success'] = false;
            $ret['msg_error'] = "Richiesta non disponibile! <br> (Modello di configurazione con dati abbinati)";
            echo acs_je($ret);
            return;
        }elseif($count == 0 && ($count_pusx > 0 || $count_pusq > 0)) {
            
            $ret['success'] = false;
            $ret['canc_dati'] = 'Y';
            echo acs_je($ret);
            return;
        }else{
            $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                    WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
        }
        
    }else{
        
        $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
    }
 

	$ret = array();
    $ret['success'] = true;
	echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ret = array();
    
    $forza_articolo = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 'frz_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
            $forza_articolo .= $m_params->form_values->$filtro;
            else
                $forza_articolo .= sprintf("%-1s", '');
                
    }
    
    $sostituzione_articolo = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 'chr_ric_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
            $sostituzione_articolo .= $m_params->form_values->$filtro;
            else
                $sostituzione_articolo .= sprintf("%-1s", '');
    }
    
    $c_art_forn = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 'coaf_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
            $c_art_forn .= $m_params->form_values->$filtro;
        else
            $c_art_forn .= sprintf("%-1s", '');
    }
    
    $m_params->form_values->frz = $forza_articolo;
    $m_params->form_values->chr_ric = $sostituzione_articolo;
    $m_params->form_values->coaf = $c_art_forn;
    
    //se ho passato tacor2 il controllo dell'esistenza devo farlo anche in base a TACOR2
    if(isset($m_params->tacor2) && strlen($m_params->tacor2) > 0){
        $row = get_TA_sys($m_table_config['TAID'], $m_params->form_values->TANR, null, $m_params->tacor2);
    }elseif(isset($m_params->tacor1) && strlen($m_params->tacor1) > 0){ 
        $row = get_TA_sys($m_table_config['TAID'], $m_params->form_values->TANR, $m_params->tacor1);
    }else {
        $row = get_TA_sys($m_table_config['TAID'], $m_params->form_values->TANR);
    }
  
    if($row != false && count($row) > 0){
        $ret['success'] = false;
        $ret['msg_error'] = "Codice esistente";
        echo acs_je($ret);
        return;
    }

    
	$ar_ins = array();
	
	if(isset($m_table_config['fields']['TAREST'])){
	    if (!isset($m_params->form_values->TAREST) || !isset($m_table_config['TAREST'])){
	        $ret['success'] = false;
	        $ret['msg_error'] = "Gestione tabella non configurata correttamente(TAREST) <br> Contattare amministratore di sistema";
	        echo acs_je($ret);
	        return;
	    }
	}
	
	
	if(trim($m_params->form_values->red) != '' || trim($m_params->form_values->green) != '' || trim($m_params->form_values->blue) != ''){
	    if(trim($m_params->form_values->red) == '')
	        $m_params->form_values->red = 0;
        if(trim($m_params->form_values->green) == '')
            $m_params->form_values->green = 0;
        if(trim($m_params->form_values->blue) == '')
            $m_params->form_values->blue = 0;
	                
	}
	
	if(isset($m_table_config['fields']['TAREST'])){
	    $tarest = genera_TAREST($m_table_config, $m_params->form_values);
	    if($m_table_config['TAID'] == 'PUFD'){
	        $ar_ins['TADES2'] 	= substr($tarest, 0, 30);
	        $ar_ins['TAREST']   = substr($tarest, 30);
	    }else
	        $ar_ins['TAREST'] 	= $tarest;
	}
	   
	

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAUSUM'] 	= $auth->get_user();
	$ar_ins['TADTUM']   = oggi_AS_date();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TAID'] 	= $m_table_config['TAID'];
	$ar_ins['TANR'] 	= $m_params->form_values->TANR;
	if(isset($m_params->form_values->TACINT))
        $ar_ins['TACINT'] 	= $m_params->form_values->TACINT;
	if(isset($m_params->tacor2))
        $ar_ins['TACOR2'] 	= $m_params->tacor2;
    if(isset($m_params->tacor1))
        $ar_ins['TACOR1'] 	= $m_params->tacor1;
    $ar_ins['TADESC'] 	= acs_toDb(trim($m_params->form_values->TADESC));
    if($m_table_config['TAID'] != 'PUFD')
        $ar_ins['TADES2'] 	= acs_toDb(trim($m_params->form_values->TADES2));
   

	$sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	echo db2_stmt_errormsg();
	
	
	if(isset($m_params->tacor2))
	    $sql_where = " AND TACOR2 = '{$m_params->tacor2}'";
	
	$sql_s = "SELECT TA.*, RRN(TA) AS RRN
	          FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
	          WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_table_config['TAID']}' AND TANR = '{$m_params->form_values->TANR}'
              {$sql_where}";
	
	$stmt_s = db2_prepare($conn, $sql_s);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_s);
	echo db2_stmt_errormsg($stmt_s);
	$row = db2_fetch_assoc($stmt_s);
	$row = get_m_row($row, $m_table_config);
	
	$ret['success'] = true;
	$ret['record'] = $row;
	echo acs_je($ret);
    exit;
}




if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $ar_upd = array();
   
    $forza_articolo = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 'frz_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
           $forza_articolo .= $m_params->form_values->$filtro;
        else
           $forza_articolo .= sprintf("%-1s", '');
        
    }
    
    $sostituzione_articolo = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 'chr_ric_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
            $sostituzione_articolo .= $m_params->form_values->$filtro;
        else
            $sostituzione_articolo .= sprintf("%-1s", '');
        
    }
    
    $c_art_forn = '';
    for($i = 0; $i <= 14; $i++){
        $filtro = 'coaf_'.$i;
        if(trim($m_params->form_values->$filtro) != '')
            $c_art_forn .= $m_params->form_values->$filtro;
        else
            $c_art_forn .= sprintf("%-1s", '');
    }
    
    $m_params->form_values->coaf = $c_art_forn;
    $m_params->form_values->frz = $forza_articolo;
    $m_params->form_values->chr_ric = $sostituzione_articolo;
    
    if(isset($m_table_config['fields']['TAREST'])){
        if (!isset($m_params->form_values->TAREST) || !isset($m_table_config['TAREST'])){
            $ret['success'] = false;
            $ret['msg_error'] = "Gestione tabella non configurata correttamente(TAREST) <br> Contattare amministratore di sistema";
            echo acs_je($ret);
            return;
        }
    }
    
    if(trim($m_params->form_values->red) != '' || trim($m_params->form_values->green) != '' || trim($m_params->form_values->blue) != ''){
        if(trim($m_params->form_values->red) == '')
            $m_params->form_values->red = 0;
        if(trim($m_params->form_values->green) == '')
            $m_params->form_values->green = 0;
        if(trim($m_params->form_values->blue) == '')
            $m_params->form_values->blue = 0;
        
    }
    
    
    if(isset($m_table_config['fields']['TAREST'])){
        $tarest = genera_TAREST($m_table_config, $m_params->form_values);
        if($m_table_config['TAID'] == 'PUFD'){
            $ar_upd['TADES2'] 	= substr($tarest, 0, 30);
            $ar_upd['TAREST']   = substr($tarest, 30);
        }else
            $ar_upd['TAREST'] 	= $tarest;
    }

	$ar_upd['TAUSUM'] 	= $auth->get_user();
	$ar_upd['TADTUM']   = oggi_AS_date();
	//$ar_upd['TANR'] 	= utf8_decode($m_params->form_values->TANR);
	if(isset($m_params->form_values->TACINT))
	   $ar_upd['TACINT'] 	= $m_params->form_values->TACINT;
   $ar_upd['TADESC'] 	= acs_toDb(trim($m_params->form_values->TADESC));
   if($m_table_config['TAID'] != 'PUFD')
        $ar_upd['TADES2'] 	= acs_toDb(trim($m_params->form_values->TADES2));

           
	$sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
	SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
	WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_upd);
	echo db2_stmt_errormsg();
	
	$sql_s = "SELECT RRN(TA) AS RRN, TA.*
	          FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
	          WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
	
	$stmt_s = db2_prepare($conn, $sql_s);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_s);
	echo db2_stmt_errormsg($stmt_s);
	$row = db2_fetch_assoc($stmt_s);
	$row = get_m_row($row, $m_table_config);
	
	$ret['success'] = true;
	$ret['record'] = $row;
	
	echo acs_je($ret);
    exit;
}



if ($_REQUEST['fn'] == 'get_data_grid'){

    $sql_where = "";
    $sql_join = "";
    $sql_sel = "";
    $ar_lin = array();
    foreach($cfg_mod_DeskUtility['ar_lingue'] as $v){
        $ar_lin[] = $v['id'];
    }    
    
    if($m_params->tacor2 != ''){
        $sql_where .= " AND TACOR2 = '".trim($m_params->tacor2)."'";
        $sql_where_join = " AND TACOR2 = '".trim($m_params->tacor2)."'";
    }
    
    if(isset($m_params->tacor1) && $m_params->tacor1 != ''){
        $sql_where .= " AND TACOR1 = '".trim($m_params->tacor1)."'";
        $sql_where_join = " AND TACOR1 = '".trim($m_params->tacor1)."'";
    }
    
    if(isset($m_params->tanr) && $m_params->tanr != ''){
        $sql_where .= " AND TA.TANR = '".$m_params->tanr."'";
        $sql_where_join = " AND TANR = '".$m_params->tanr."'";
    }
    
    if($m_table_config['j_mode'] == 'Y'){
        $sql_sel .= ", MD.C_ROW AS MODE";
        $sql_join .= "  LEFT OUTER JOIN (
                            SELECT COUNT(*) AS C_ROW, MDMODE
                            FROM {$cfg_mod_DeskUtility['file_config']}
                            WHERE MDDT = '{$id_ditta_default}' AND MDSEQC = '999' AND MDVARI <> ''
                            GROUP BY MDMODE) MD
                        ON MD.MDMODE = TA.TANR";
    }
    
    if($m_table_config['j_pusx'] == 'Y'){
        $sql_sel .= ", SX.C_ROW AS PUSX";
        $sql_join .= "  LEFT OUTER JOIN (
                        SELECT COUNT(*) AS C_ROW, TANR
                        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} 
                        WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUSX'
                        GROUP BY TANR) SX
                        ON SX.TANR = TA.TANR";
    }
    
    if($m_table_config['j_pusq'] == 'Y'){
        $sql_sel .= ", SQ.C_ROW AS PUSQ";
        $sql_join .= "  LEFT OUTER JOIN (
                        SELECT COUNT(*) AS C_ROW, TACOR2
                        FROM {$cfg_mod_DeskArt['file_tabelle_sys']}
                        WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUSQ'
                        GROUP BY TACOR2) SQ
                        ON SQ.TACOR2 = TA.TANR";
    }
    
    if($m_table_config['j_puvn'] == 'Y'){
        $sql_sel .= ", VN.C_ROW AS PUVN";
        $sql_join .= "  LEFT OUTER JOIN (
                        SELECT COUNT(*) AS C_ROW, TACOR2
                        FROM {$cfg_mod_DeskArt['file_tabelle_sys']}
                        WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN'
                        GROUP BY TACOR2) VN
                        ON VN.TACOR2 = TA.TANR";
    }
    
    if($m_table_config['j_pufd'] == 'Y'){
        $sql_sel .= ", FD.C_ROW AS PUFD";
        $sql_join .= "  LEFT OUTER JOIN (
                        SELECT COUNT(*) AS C_ROW, TACOR1
                        FROM {$cfg_mod_DeskArt['file_tabelle_sys']}
                        WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUFD'
                        GROUP BY TACOR1) FD
                        ON FD.TACOR1 = TA.TANR";
    }
    
	$sql = "SELECT RRN (TA) AS RRN, TA.*, TA_TRAD.C_ROW AS TRAD {$sql_sel}
	        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
	        {$sql_join}
	        LEFT  OUTER JOIN (
                SELECT COUNT(*) AS C_ROW, TANR
                FROM  {$cfg_mod_DeskArt['file_tabelle_sys']} 
                WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_table_config['TAID']}' {$sql_where_join} AND TALINV IN (" . sql_t_IN($ar_lin) . ")
                GROUP BY TANR) TA_TRAD
            ON TA.TANR = TA_TRAD.TANR 
	        WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = ?
            {$sql_where} ORDER BY TANR, TADESC";
	        
   	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_table_config['TAID']));
	
	//sostituire il ? con il tanr 
	$sql_t = "SELECT COUNT(DISTINCT MDVARI) AS C_VAR, COUNT(DISTINCT CONCAT(MDSEQ3, MDSEQ4)) AS C_SEQ
	          FROM {$cfg_mod_DeskUtility['file_config']} MD
	          WHERE MDDT = '{$id_ditta_default}' AND MDMODE = ? AND MDSEQC='999'";
	
	$stmt_t = db2_prepare($conn, $sql_t);
	echo db2_stmt_errormsg();

	$data = array();
	
	while ($row = db2_fetch_assoc($stmt)) {
	    
	    $result = db2_execute($stmt_t, array($row['TANR']));
	    $row_t = db2_fetch_assoc($stmt_t);
	    $row['c_var'] = trim($row_t['C_VAR']);
	    $row['c_seq'] = trim($row_t['C_SEQ']);
	    
	    $row = get_m_row($row, $m_table_config);
	 
		$data[] = $row;		
	}

	echo acs_je($data);
	exit();
}


if ($_REQUEST['fn'] == 'open_panel'){
    
    $cod = "";
        
    if(trim($m_params->tacor2) != ''){
        $cod = " [".trim($m_params->tacor2)."] ";
        $desc = $cod.trim($m_params->d_cod);
    }
    
    if($m_table_config['TAID'] == 'PUFD'){
        $cod = " [".trim($m_params->tacor1)."] Righe scheda";
        $desc = " [".trim($m_params->tacor1)."] ".trim($m_params->d_cod);
    }
    
	?>

				
{"success":true, "items": [

   {
			xtype: 'panel',
			<?php if($m_table_config['TAID'] != 'BCER'){
			    if($m_params->hide_grid != 'Y'){?>
				title: '<?php echo "{$m_table_config['title_grid']}{$cod}"; ?>',
        		<?php echo make_tab_closable(); ?>,
        	<?php }}?>
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
	{
        
        xtype: 'grid',
        multiSelect : true,
        itemId : 'my_grid',
        <?php if($m_params->hide_grid == 'Y'){?>	
			hidden : true,
		<?php } ?>
        flex : 0.7,
	    features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}]
			,store: {
						
						xtype: 'store',
						autoLoad: true,
					   	proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },							        
							        
							           extraParams: {
										 tacor2 : <?php echo j($m_params->tacor2); ?>,
										 tacor1 : <?php echo j($m_params->c_art); ?>,
										 tanr : <?php echo j($m_params->tanr);?>
										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        		 fields: <?php 
		        		 if($m_table_config['TAID'] == 'PUCI' || $m_table_config['TAID'] == 'MURC' || $m_table_config['TAID'] == 'BITL')
		        		     echo m_get_fields($m_table_config, $m_table_config_altro, $m_table_config_docu, $m_table_config_altro_cv);
		        		 else
		        		     echo m_get_fields($m_table_config);
		        		 ?>,
		        		
		        		},
		    		
			        columns: [ <?php m_get_columns($m_table_config) ?> ],  
					
						listeners: {
						
					      	 selectionchange: function(selModel, selected) { 
	                           if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            		               rec_index = selected[0].index;
            		               form_dx.getForm().findField('rec_index').setValue(rec_index);
            		               form_dx.getForm().setValues(selected[0].data);            		                           	                
            	                }
            	                
		       		  	 	 }
		       		  	 	 <?php if($m_params->autoload == 'firstRec'){?>
		       		  	 	 ,'afterrender': function(comp){
		       		  	 	    var win = comp.up('window');
								comp.store.on('load', function(store, records, options) {
								   var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().setValues(records[0].data);
								}, comp);
    						}
    						<?php }?>
		       		  	 	,itemcontextmenu : function(grid, rec, node, index, event) {
		       		  	 	    event.stopEvent();
				  											  
						 		var voci_menu = [];
					     		var row = rec.data;
					     		var m_grid = this;
					     		
					     		id_selected = grid.getSelectionModel().getSelection();
					     		list_selected_id = [];
					     		for (var i=0; i<id_selected.length; i++){
					     			list_selected_id.push({
					     			   RRN : id_selected[i].get('RRN'),
					     			   TAREST : id_selected[i].get('TAREST')});
								}
					     
					 	 voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          rec.set('TATP', jsonData.new_value);	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
			    				
			    				if(rec.get('TAID') == 'PUMO'){
			    				
			    				if(rec.get('MODE') > 0){
    			    				 voci_menu.push({
    			         			text: 'Ricorrenza variabili',
    			        			iconCls : 'icon-leaf-16',             		
    			        			handler: function () {
    			        		  	acs_show_win_std('Configuratore [' +rec.get('TANR') + '] - RIEPILOGO VARIABILI'+ ' [' + rec.get('c_var') + ', ' + rec.get('c_seq') +']', 'acs_anag_art_config.php?fn=open_tab', {mode: rec.get('TANR'), totalizza : 'Y'}, 600, 400, null, 'icon-design-16');   	     
    				                }
    			    				});
			    				}
			    				
			    					 voci_menu.push({
			         				text: 'Duplica modello',
			        				iconCls : 'iconScaricoIntermedio',             		
			        				handler: function () {
			        				
			        				 var my_listeners = {
    			    		  			afterDuplica: function(from_win){
    			    		  			 //focus??
    			    		  			   grid.getStore().load();
    			    		  			   from_win.close();
    			    		  			   }
    				    				};
			        				
			        		  		acs_show_win_std('Duplica modello configurazione', 'acs_anag_art_config_duplica.php?fn=open_form', {mode: rec.get('TANR'), d_mode : rec.get('TADESC'), note : rec.get('TADES2')}, 450, 220, my_listeners, 'iconScaricoIntermedio');   	     
    				                }
    			    				});
    			    				
    			    				 voci_menu.push({
			         				text: 'Duplica configuratore',
			        				iconCls : 'icon-game_pad-16',             		
			        				handler: function () {
			        				
			        				 var my_listeners = {
    			    		  			afterSelect: function(from_win, new_mode){
    			    		  			   
    			    		  			   from_win.close();
    			    		  			   
			    		  			   var config_title = 'Duplica da configuratore [' + rec.get('TANR')+ '] ' +  rec.get('TADESC') +' a [' +  new_mode + '] '; 
    			    		  			   
    			    		  			   acs_show_win_std(config_title, 'acs_anag_art_config.php?fn=open_tab', {mode: rec.get('TANR'), d_cfg : 'Y', new_mode : new_mode, d_mode : rec.get('TADESC'), from_duplica : 'Y'}, 1200, 400
    			    		  			   , {afterDupCfg: function(from_win){
            			    		  			 grid.getStore().load();
            			    		  			  acs_show_panel_std('acs_anag_art_config.php?fn=open_tab', 'panel_configuratore', {mode: new_mode, d_mode : ''});
            			    		  			   from_win.close();
            			    		  			 }}
    			    		  			   , 'icon-game_pad-16');
    			    		  			   }
    				    				};
			        				
			        		  		acs_show_win_std('Duplica distinta configurazione varianti', 'acs_anag_art_config_set_values.php?fn=open_form', {mode: rec.get('TANR'), d_mode : rec.get('TADESC'), d_cfg : 'Y'}, 450, 150, my_listeners, 'icon-game_pad-16');   	     
    				                }
    			    				});
    			    				
    			    				
    			    			voci_menu.push({
			         				text: 'Report configuratore',
			        				iconCls : 'icon-print-16',             		
			        				handler: function () {
			        					
			        					acs_show_win_std('Report configuratore', 'acs_anag_art_report_configuratore.php?fn=open_form', {
            	        				mode: rec.get('TANR')
            	    					},		        				 
            	        				250, 200, {}, 'icon-print-16');
			        					
			            		    }
    			    				});
    			    				
    			    				voci_menu.push({
			         				text: 'Elenco articoli',
			        				iconCls : 'icon-leaf-16',             		
			        				handler: function () {
			        					    livelli = [];
			        						acs_show_panel_std('acs_panel_anag_art.php?fn=open_tab', null, {
                        		            	form_open: {f_configuratore :  rec.get('TANR')},
                        		            	livelli : livelli
                        		           }, null, null);
			        					
			            		    }
    			    				});
			    				
			    				}
			    				
			    				if(rec.get('TAID') == 'PUVR'){
			    				if(id_selected.length == 1){
			    				 voci_menu.push({
    			         					text: 'Configurazione variabile',
    			        					iconCls : 'icon-game_pad-16',             		
    			        					handler: function () {
    			        					
    			        					 var title = 'Configurazione [' + rec.get('TANR') + '] ';
    			        				
    			        				    	acs_show_panel_std('acs_anag_art_config.php?fn=open_tab', 'panel_configuratore_puvr', {variabile : rec.get('TANR'), title: title, from_var : 'Y'});
    			        				
    			        					
    			            		    }
        			    				});
			    				}
			    				
			    				}
			    				
			    				
			    				if(rec.get('TAID') == 'PUVN'){
			    				if(id_selected.length == 1){
			    				 voci_menu.push({
    			         					text: 'Configurazione variante',
    			        					iconCls : 'icon-game_pad-16',             		
    			        					handler: function () {
    			        					
    			        					 var title = 'Configurazione [' +rec.get('TACOR2') + ': ' + rec.get('TANR') + '] ';
    			        				
    			        				    	acs_show_panel_std('acs_anag_art_config.php?fn=open_tab', 'panel_configuratore_puvn', {variabile :  rec.get('TACOR2'), variante : rec.get('TANR'), title: title, from_puvn : 'Y'});
    			        				
    			        					
    			            		    }
        			    				});
			    				}
			    				
			    				
			    				    voci_menu.push({
    			         				text: 'Assegna TAG',
    			        				iconCls : 'icon-blog_compose-16',             		
    			        				handler: function () {
    			        				
    			        				var my_listeners = {
    			    		  				afterAssegna: function(from_win, tag){
    			    		  			
    			    		  			   	for (var i=0; i<id_selected.length; i++)
					     						id_selected[i].set('tag', tag);
					     						
					     						from_win.close();
					     					}	
					     					};
    			        				
    			        					acs_show_win_std('Assegna TAG varianti', 'acs_tab_sys_ricerca_PUVN.php?fn=open_form_tag', {
                	        				list_selected_id: list_selected_id
                	        				},		        				 
                	        				350, 150, my_listeners, 'icon-blog_compose-16');
    			        					
    			            		    }
        			    				});
        			    				
        			    				var count_sosp = 0
    			    					for (var i=0; i<id_selected.length; i++){
    			    						if(id_selected[i].get('TATP') == 'S')
    			    						    count_sosp++;
    			    					}
			    					
			    						voci_menu.push({
        	         				 			text: 'Assegna ciclo di vita',
        	        				 			iconCls : 'icon-blog_compose-16',             		
        	        				 			handler: function () {
        	        				 				
            			        				var my_listeners = {
            			    		  				afterAssegna: function(from_win, ciclo){
            			    		  			
            			    		  			   	for (var i=0; i<id_selected.length; i++)
        					     						id_selected[i].set('esau', ciclo);
        					     						
        					     						from_win.close();
        					     					}	
        					     					};
            			        				
            			        					if(count_sosp == 0){
                			        					acs_show_win_std('Assegna ciclo di vita', 'acs_tab_sys_ricerca_PUVN.php?fn=open_form_ciclo', {
                            	        				list_selected_id: list_selected_id
                            	        				},		        				 
                            	        				350, 150, my_listeners, 'icon-blog_compose-16');  	     
                    			                	}else{
                    			                	 	acs_show_msg_error('Selezionare solo varianti non sospese');
        					      	       				return false;
                    			                	}
                    			                	
                    			                	
                    			                	}
                    		    				 });
    			    				    
        			    				
        			    				id_selected = grid.getSelectionModel().getSelection();
			     		 				list_selected_id2 = [];
			     		 				for (var i=0; i<id_selected.length; i++){
			     						list_selected_id2.push({
			     						  cod : id_selected[i].get('TANR'),
			     						  desc : id_selected[i].get('TADESC')});
						 				}
        			    				
        			    				 voci_menu.push({
        	         				 	text: 'Riepilogo codici articolo',
        	        				 	iconCls : 'icon-print-16',             		
        	        				 	handler: function () {
        	        				 		acs_show_win_std('Riepilogo codici articolo', 'acs_report_riepilogo_art.php?fn=open_form', {
        	        				 				cod_variabile: rec.get('TACOR2'),
        	        				 				list_selected_id : list_selected_id2
        	        				 			}, 400, 380, {}, 'icon-print-16');   	     
        			                	}
        		    				 });
			    					
			    						
			    				}
			    		
        			    
        				      var menu = new Ext.menu.Menu({
        				            items: voci_menu
        					}).showAt(event.xy);	
			    	
			    	},celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	
					  	if(rec.get('TAID') == 'PUVN')
					  	   var titolo = 'Traduzione [' +rec.get('TACOR2') + ', ' + rec.get('TANR') + '] ';
					  	else
					  	   var titolo = 'Traduzione [' +rec.get('TANR') + '] ';
					  	
					  	 if(col_name == 'MODE')
					  	 		acs_show_panel_std('acs_anag_art_config.php?fn=open_tab', 'panel_configuratore', {mode: rec.get('TANR'), d_mode : rec.get('TADESC')});
					  	
					  	 if(col_name == 'TRAD')
            				  	acs_show_win_std( titolo + rec.get('TADESC'), 'acs_anag_art_gest_trad_sys.php?fn=open_al', {tanr: rec.get('TANR'), taid : rec.get('TAID'), tacor2 : rec.get('TACOR2')}, 450, 500, null, 'icon-globe-16');
            				  	
            		     if(col_name == 'PUSX'){
            		     
            		      var my_listeners = {
    			    		  			afterModifica: function(from_win){
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
            		     
            				  acs_show_win_std('Stringhe analisi codice articolo - Variabili XA1-XA9', 'acs_tab_sys_PUSX.php?fn=open_detail', {tanr: rec.get('TANR'), taid : rec.get('TAID')}, 380, 410, my_listeners, 'icon-barcode');
					  		}
					  		
					  	if(col_name == 'PUSQ')
					  		acs_show_panel_std('acs_tab_sys_PUSQ.php?fn=open_panel', 'panel_gestione_tabelle', {tacor2: rec.get('TANR'), d_cod : rec.get('TADESC') });
					  		
					  	if(col_name == 'PUVN')
					  		acs_show_panel_std('acs_tab_sys_PUVN.php?fn=open_panel', 'panel_gestione_tabelle', {tacor2: rec.get('TANR'), d_cod : rec.get('TADESC'), is_master : rec.get('is_master'), master : rec.get('var_mst') });
					  		
					  	if(col_name == 'PUFD')
					  		acs_show_panel_std('acs_tab_sys_PUFD.php?fn=open_panel', 'panel_gestione_tabelle', {tacor1: rec.get('TANR'), c_art : rec.get('TANR'), d_cod : rec.get('TADESC'), var_rif1 : rec.get('vras'), var_rif2 : rec.get('vrs2'), var_rif3 : rec.get('vrs3')});	          	        	        	          	        	          	               
					  		
					  	}
					  	}
					
					
					},
						viewConfig: {
        		        getRowClass: function(record, index) {	
        		        
        		        if(record.get('TATP') == 'S'){
        		           return ' colora_riga_rosso';
                		 }
		        														
		         }   
		    }
       
        }  
						
        <?php if(trim($m_params->master) != ''){
                $title = $m_table_config['title_form'].$desc." - Master [".$m_params->master."]";
            }else{
                $title = $m_table_config['title_form'].$desc;
            }
        ?>
		
		
		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            <?php if($m_params->hide_grid != 'Y'){?>
 		            	title: <?php echo j($title); ?>,
 		            <?php }?>
 		          //  bodyStyle: 'padding: 10px',
		            bodyPadding: '2 2 0',
 		            flex:0.35,
 		            frame: true,
 		            items: [ 
 		            
 		            	
 		            <?php if($m_table_config['TAID'] == 'PUOP' || $m_table_config['TAID'] == 'PUVN' || $m_table_config['TAID'] == 'PUCI' || $m_table_config['TAID'] == 'MURC' || $m_table_config['TAID'] == 'BITL'){ ?>
        	{
        		xtype: 'tabpanel',
        		items: [
        		
        		{
				xtype: 'panel',
				title: 'Dettagli', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
        <?php }?>
 		            
 		            {xtype : 'textfield',name : 'rec_index',hidden : true},
                    <?php m_get_comp_form($m_table_config, $m_params) ?>
                    <?php if($m_table_config['TAID'] == 'PUOP' || $m_table_config['TAID'] == 'PUVN' || $m_table_config['TAID'] == 'PUCI' || $m_table_config['TAID'] == 'MURC' || $m_table_config['TAID'] == 'BITL'){?>
		    ]},
		    
		    <?php if(isset($m_table_config_docu) && count($m_table_config_docu) > 0){?>
				
				 {
				xtype: 'panel',
				title: <?php echo j($m_table_config_docu['title']) ?>, 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				<?php m_get_comp_form($m_table_config_docu, $m_params); ?>
				
				]},
				<?php } ?>
		    
		    {
				xtype: 'panel',
				title: <?php echo j($m_table_config['title_tab']); ?>, 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				
				if($m_table_config['TAID'] == 'PUOP'){
                    
				for($i=1; $i <= 20 ; $i ++){?>
				
				 {
					name: 'var<?php echo $i; ?>',
					xtype: 'combo',
					flex: 1,
					fieldLabel: 'Variabile <?php echo $i; ?>',
					forceSelection: true,								
					displayField: 'text',
					valueField: 'id',								
					emptyText: '- seleziona -',
			   		//allowBlank: false,								
				    anchor: '-15',
				    queryMode: 'local',
             		minChars: 1,	
					store: {
						editable: false,
						autoDestroy: true,
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
					     <?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
					    ]
					},listeners: { 
        			 		beforequery: function (record) {
        	         		record.query = new RegExp(record.query, 'i');
        	         		record.forceAll = true;
    				 }
         			}
					
			   },
				
				<?php }
				
                  }elseif($m_table_config['TAID'] == 'PUCI' || $m_table_config['TAID'] == 'MURC' || $m_table_config['TAID'] == 'BITL'){ 
                        
                        m_get_comp_form($m_table_config_altro, $m_params);
                
                    }else{ ?>
                    {
            		name: 'var_or',
            		xtype: 'combo',
            		labelWidth : 120,
            		flex: 1,
            		fieldLabel: 'Variante origine',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
            		//allowBlank: false,								
            		anchor: '-15',
            		queryMode: 'local',
            		minChars: 1,	
            		store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}],
            			data: [								    
            			 <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $m_params->tacor2, null, null, 0, '', 'Y'), ''); ?>	
            			]
            		},listeners: { 
            				beforequery: function (record) {
            				record.query = new RegExp(record.query, 'i');
            				record.forceAll = true;
            		 }
            		}
            		
                 }, {
            		name: 'agente',
            		xtype: 'combo',
            		labelWidth : 120,
            		flex: 1,
            		fieldLabel: 'Agente',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
            		//allowBlank: false,								
            		anchor: '-15',
            		queryMode: 'local',
            		minChars: 1,	
            		store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}],
            			data: [								    
            			 <?php echo acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
            			]
            		},listeners: { 
            				beforequery: function (record) {
            				record.query = new RegExp(record.query, 'i');
            				record.forceAll = true;
            		 }
            		}
            		
                 },{
                    name: 'royal',
            		xtype: 'textfield',
            		labelWidth : 120,
            		flex: 1,
            		fieldLabel: '% Royalties',
            		maxLength : 5
                 },
                    {
            		name: 'var_dp',
            		xtype: 'combo',
            		labelWidth : 120,
            		flex: 1,
            		fieldLabel: 'Variante duplica',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
            		//allowBlank: false,								
            		anchor: '-15',
            		queryMode: 'local',
            		minChars: 1,	
            		store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}],
            			data: [								    
            			 <?php echo acs_ar_to_select_json(find_TA_sys('PUVN', null, null, $m_params->tacor2, null, null, 0, '', 'Y'), ''); ?>	
            			]
            		},listeners: { 
            				beforequery: function (record) {
            				record.query = new RegExp(record.query, 'i');
            				record.forceAll = true;
            		 }
            		}
            		
                 }
                    
                    
                <?php }  ?>
				
				
				]}
				
				  
				<?php if(isset($m_table_config_altro_cv) && count($m_table_config_altro_cv) > 0){?>
				
				 , {
				xtype: 'panel',
				title: <?php echo j($m_table_config_altro_cv['title']) ?>, 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				<?php m_get_comp_form($m_table_config_altro_cv, $m_params); ?>
				
				]},
				<?php } ?>
				
		    ]}
		    
		    
		    
		    ],
		    <?php }else{?>
		    ],
		<?php }?>
		
			 <?php if($m_params->only_view != 'Y'){?>		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                {
                    xtype: 'button',
                    tooltip: 'Opzioni disponibili',
                    scale: 'small',	                  
        			iconCls : 'icon-button_grey_play-16',
        			style:'border: 1px solid gray;',    
                    handler: function(a , event) {
                      
                         var form = this.up('form');
		                 var form_values = form.getValues();
                    	 var grid = this.up('form').up('panel').down('grid');
                    	 var index = grid.getStore().findExact('RRN', form_values.RRN); 			       		
 			       		 var record = grid.getStore().getAt(index);
	                	 grid.fireEvent('itemcontextmenu', grid, record, null, index, event);
        		
        		       }
        
        	     },
                
                
                
                
                <?php if($m_table_config['ricerca_var']  == 'Y'){?>
                
						{
                            xtype: 'button',
                            text: 'Ricerca per denominazione variante',
        		            scale: 'small',	   
        		            maxWidth : 300,                  
        					//iconCls : 'icon-search-16',
        					style:'border: 1px solid gray;',    
        		            handler: function() {
        		            	 var grid = this.up('form').up('panel').down('grid');
        		            	acs_show_win_std('Ricerca variabile per denominazione variante', 'acs_tab_sys_ricerca_PUVN.php?fn=open_form', {grid_id: grid.id}, 400, 150, null, 'icon-search-16');
                		
        				       }
        
        			     },
        			     
        			     <?php } 
        			     
        			     if($m_table_config['elenco_var']  == 'Y'){?>   
        			     {
                            xtype: 'button',
                            text: 'Elenco varianti abbinate',
        		            scale: 'small',	   
        		            maxWidth : 300,                  
        					//iconCls : 'icon-search-16',
        					style:'border: 1px solid gray;',    
        		            handler: function() {
        		            	 var grid = this.up('form').up('panel').down('grid');
        		            	 var form = this.up('form');
		                         var form_values = form.getValues();
		                         acs_show_win_std('Elenco varianti abbinate [' +form_values.TANR + '] ' +form_values.TADESC, 'acs_tab_sys_ricerca_PUVN.php?fn=open_tab', {grid_id: grid.id, from_tipo : 'Y', tanr : form_values.TANR}, 600, 500, null, 'icon-search-16');
                		
        				       }
        
        			     },
						
			     <?php }?>
			     
			     '->',
			     <?php if(trim($m_params->master) == '' || $m_params->is_master == 'Y'){ ?>
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
	       			 
	       			  if(form_values.RRN == ''){
							  acs_show_msg_error('Selezionare una riga dalla tabella di sinistra');
							  return false;
						 }
        		
        		      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				       Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values,
										is_master : <?php echo j($m_params->is_master); ?>
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
        					         	   if(jsonData.canc_dati == 'Y'){
        					         	   
        					         	   
        			        				 var my_listeners = {
            			    		  			afterConferma: function(from_win){
            			    		  			    from_win.close();
            			    		  			    form.getForm().reset();
							        	            var rec_index = grid.getStore().findRecord('TANR', form_values.TANR);
							       		            grid.getStore().remove(rec_index);
            			    		  			   
            			    		  			 
            			    		  			   }
            				    				};
        					         	   
        					         	   		acs_show_win_std('Conferma cancellazione dati abbinati', 'acs_anag_delete_dati_abbinati.php?fn=open_tab', {
                    								codice : form_values.TANR
                    							}, 500, 300, my_listeners, 'icon-sub_red_delete-16');
        					         	   
        					         	   }else{
        					         	     acs_show_msg_error(jsonData.msg_error);
							        	 	 return;
        					         	   }
 			       						
				        			     }else{
        					         		form.getForm().reset();
							        	    var rec_index = grid.getStore().findRecord('TANR', form_values.TANR);
							       		    grid.getStore().remove(rec_index);	
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
					
					    	}
            	   		  });		    
							    
				        }

			     },  
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form_values = this.up('form').getValues();
 			          var form = this.up('form').getForm();
	       			  var grid = this.up('form').up('panel').down('grid'); 
	       			  
       			   	  if(form_values.TANR.trim() == ''){
			      	    acs_show_msg_error('Inserire un codice');
			      	    return false;
				      }
 			        
 			         if (form.isValid()){
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				tacor2 : <?php echo j(trim($m_params->tacor2)); ?>,
	        				tacor1 : <?php echo j(trim($m_params->c_art)); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if(jsonData.success == false){
					      	    acs_show_msg_error(jsonData.msg_error);
					      	    return false;
					        }else{
					        	//grid.getStore().load();
					        	
					        	 var new_rec = jsonData.record;
					        	 grid.getStore().add(new_rec);
					        	 var rec_index = grid.getStore().findRecord('TANR', new_rec.TANR);
			  					 grid.getView().select(rec_index);
			  					 grid.getView().focusRow(rec_index);
			  					 
			  					 <?php if($m_params->is_master == 'Y'){?>
			  					 
			  					     acs_show_win_std('Conferma generazione varianti collegate', 'acs_varianti_collegate.php?fn=open_grid', {
	        				 			master : <?php echo j($m_params->tacor2); ?>,
	        				 			variante : new_rec
	        				 		 }, 800, 300, {}, 'icon-leaf-16'); 
			  					 
			  					 <?php }?>
			  					 
					           
					        }
					       },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		          }
			
			            }

			     },
			     <?php }else{?>
			     
			        {
                     xtype: 'button',
                    text: 'Inserisci da master',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form_values = this.up('form').getValues();
 			          var form = this.up('form').getForm();
	       			  var grid = this.up('form').up('panel').down('grid'); 
	       			  
       			            var my_listeners = {
			    		  			afterInsert: function(from_win){
			    		  			     grid.getStore().load();
        					        	 from_win.close();
			    		  			  }
				    		   };
	       			  
	       			        acs_show_win_std('Aggiungi variante da master', 'acs_varianti_collegate.php?fn=open_grid_master', {
    				 			master : <?php echo j($m_params->master); ?>,
    				 			variabile : <?php echo j($m_params->tacor2); ?>
    				 		}, 800, 300, my_listeners, 'icon-leaf-16'); 
			
			            }

			     },
			     <?php }?>
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       		var form_values = this.up('form').getValues();
 			       		var form = this.up('form').getForm();
 			       		var grid = this.up('form').up('panel').down('grid'); 
 			       		
 			       		///var index = form_values.rec_index;
 			       		var index = grid.getStore().findExact('RRN', form_values.RRN); 			       		
 			       		var record_grid = grid.getStore().getAt(index);
 			       		
 			       		if(form_values.RRN == ''){
						  acs_show_msg_error('Selezionare una riga dalla tabella di sinistra');
						  return false;
						}			
 			       						       		 
 			       		 if (form.isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error(jsonData.msg_error);
					      	    		return false;
					        			}else{
					        			   var new_rec = jsonData.record;
					        			   record_grid.set(new_rec);
					        			   
					        			   <?php if($m_params->is_master){?>
					        			   
    					        			   Ext.Ajax.request({
                    						        url        : 'acs_varianti_collegate.php?fn=exe_modifica_van',
                    						        timeout: 2400000,
                    						        method     : 'POST',
                    			        			jsonData: {
                    			        				form_values : form_values
                    								},							        
                    						        success : function(result, request){
                    							        var jsonData = Ext.decode(result.responseText);
                    							           
                    							    },
                    						        failure    : function(result, request){
                    						            Ext.Msg.alert('Message', 'No data to be loaded');
                    						        }
                    						    });
					        			   
					        			   
					        			   <?php }?>
					        			   
					        			}
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               }
			            }

			     }
			     ]
		   }]
		   
		   <?php }?>
		   
			 	 }  
		 ],
					 
					
					
	}
]
}

<?php 

exit; 

}






