<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_DeskArt['file_cfg_distinte'],
    'descrizione' => "Distinta indice {$m_params->title}",
    'f_ditta' => 'CDDT',
    'f_desc'  => 'CDDESC',
   
    'fields_key' => array('CDCMAS', 'CDSLAV'),
    
    'fields' => array(
        'CDSEQI' => array('label'	=> 'Seq. Indice',  'fw'=>'width: 70'),
        //'CDDICL' => array('label'	=> 'Distinta',       'fw'=>'width: 60'),
        'CDCMAS' => array('label'	=> 'Gruppo',       'fw'=>'width: 60'),
        'CDSEQU' => array('label'	=> 'Seq.voce',     'fw'=>'width: 60', 'tooltip'=>'Sequenza'),
        'CDSLAV' => array('label'	=> 'Voce',         'fw'=>'width: 70'),
        'CDDESC' => array('label'	=> 'Descriz. voce', 'fw'=>'width: 150'),
        'CDSTIN' => array('label'	=> 'Da',           'fw'=>'width: 30'),
        'CDSTFI' => array('label'	=> 'A',            'fw'=>'width: 30'),
        'CDRSTR' => array('label'	=> 'Radice',       'fw'=>'width: 50', 'tooltip'=>'Radice stringa codice'),
        'CDFLGA' => array('label'	=> 'Segue',        'fw'=>'width: 50', 'tooltip'=>'(G, A) Gruppo o elenco Articoli'),
        'CDFLLG' => array('label'	=> 'Tipo lista',   'fw'=>'width: 60', 'tooltip'=>'Lista ulteriori opzioni da selezionare (L, G, A, N) Lista opzioni, Genera in automatico, lista Articoli, Non presente'),
        'CDCDLI' => array('label'	=> 'Codice lista', 'fw'=>'width: 80'),
        'CDNEWC' => array('label'	=> 'Nuovo',        'fw'=>'width: 50', 'tooltip'=>'Y: Attiva la generazione di un nuovo codice articolo'),
        'CDPROG' => array('label'	=> 'Ultimo ID',    'fw'=>'width: 70', 'tooltip'=>'Ultimo suffisso automatico assegnato'),
        'CDNCAR' => array('label'	=> 'Nr ID',        'fw'=>'width: 40', 'tooltip'=>'Nr caratteri suffisso automatico'),
        'CDARDU' => array('label'	=> 'Duplica',      'fw'=>'width: 50', 'tooltip'=>'Articolo da duplicare per nuovo codice'),
        'CDIDME' => array('label'	=> 'ID Memorizza', 'fw'=>'width: 90', 'tooltip'=>'ID Memorizza'),
        'CDTODO' => array('label'	=> 'Cod. ToDo',    'fw'=>'width: 100', 'tooltip'=>'Codice ToDo'),
        'CDPARM' => array('label'	=> 'Param. ToDo', 'fw'=>'width: 110', 'tooltip'=>'Parametri ToDo'),
        'note' => array('label'	=> 'Note', 'fw'=>'width: 40', 'tooltip'=>'Note'),
    ),
    
    'tasto_dx' => array('elenco_stringe' => 'Y', 'duplica' => 'Y', 'lista' => 'Y'),
    'report' => 'Y',
    'note' => 'Y',
    'cod_indice' => $m_params->cod,
    'des_indice' => $m_params->des,
    'form_values' => $m_params->form_values,
    'flt_adv' => array(
        'CDCMAS' => array($m_params->gruppo)
        
    )
    
    
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
