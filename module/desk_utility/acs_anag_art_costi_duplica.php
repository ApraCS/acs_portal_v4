<?php

require_once "../../config.inc.php";

$main_module = new DeskArt(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// EXE DUPLICA COSTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_duplica_costo'){
    
    $articolo  = trim($m_params->form_values->articolo);
    $anno = trim($m_params->form_values->ACANNO);
    $mese = trim($m_params->form_values->ACMESE);
    $tipo = trim($m_params->form_values->ACTPCS);
    
    $sql = "SELECT AC.*
            FROM {$cfg_mod_DeskArt['file_costi']} AC
            WHERE RRN(AC) = '{$m_params->row->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row_ac = db2_fetch_assoc($stmt);
    
    $row_ac['ACDTUM'] =  oggi_AS_date();
    $row_ac['ACUSUM'] = $auth->get_user();
    $row_ac['ACART']  = $articolo;
    $row_ac['ACANNO'] = $anno;
    $row_ac['ACMESE'] = $mese;
    $row_ac['ACTPCS'] = $tipo;
    //AZZERA CAMPI
    $row_ac['ACAAIC'] = 0;
    $row_ac['ACMMIC'] = 0;
    $row_ac['ACGGIC'] = 0;
    $row_ac['ACAACU'] = 0;
    $row_ac['ACMMCU'] = 0;
    $row_ac['ACGGCU'] = 0;
    $row_ac['ACPRZC'] = '';
    $row_ac['ACPCST'] = 0;
    $row_ac['ACPSPE'] = 0;
    //$row_ac['ACMCST'] = 0;
    $row_ac['ACPQTA'] = 0;
    $row_ac['ACTIOR'] = '';
    $row_ac['ACINOR'] = '';
    $row_ac['ACANOR'] = 0;
    $row_ac['ACNROR'] = 0;
    $row_ac['ACREOR'] = 0;
    $row_ac['ACRGOR'] = 0;
    $row_ac['ACTPOR'] = '';
    $row_ac['ACUMTE'] = '';
    $row_ac['ACPQTT'] = 0;
    $row_ac['ACUMAL'] = '';
    $row_ac['ACPQTL'] = 0;
    $row_ac['ACFIL3'] = '';
    
    
    $sql_ac = "INSERT INTO {$cfg_mod_DeskArt['file_costi']}(" . create_name_field_by_ar($row_ac) . ") VALUES (" . create_parameters_point_by_ar($row_ac) . ")";
    $stmt_ac = db2_prepare($conn, $sql_ac);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_ac, $row_ac);
    echo db2_stmt_errormsg($stmt_ac);
    
    $ar_ins = array();
    $ar_ins['RLDT']   = $id_ditta_default;
    $ar_ins['RLTPNO']   = 'AC';
    $ar_ins['RLRIFE1'] 	= $articolo.trim($row_ac['ACVALU']).$anno.sprintf("%02s", $mese).$tipo;
    $ar_ins['RLRIFE2'] 	= 'DCS'; 
    $ar_ins['RLREST1'] 	= $m_params->row->ACART; 
  
    $sql_rl = "INSERT INTO {$cfg_mod_DeskUtility['file_note_anag']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt_rl = db2_prepare($conn, $sql_rl);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_rl, $ar_ins);
    echo db2_stmt_errormsg();
    
   
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'open_form'){?>
	
	{"success":true,
	 "items": [
	    {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	         
						   {
						flex: 1,
			            xtype: 'combo',
						name: 'articolo',
						fieldLabel: 'Articolo',
						itemId: 'cod_art',
						anchor: '-15',
						minChars: 2,
						allowBlank : false,
						labelWidth : 80,
						store: {
			            	pageSize: 1000,           	
							proxy: {
					            type: 'ajax',
					            url : 'acs_anag_art_abbinamento.php?fn=get_json_data_articoli',
					            actionMethods: {
						          read: 'POST'
						        },
					            extraParams: {
				    		    		gruppo: '',
				    				},				            
						        doRequest: personalizza_extraParams_to_jsonData, 
					            reader: {
					                type: 'json',
					                root: 'root',
					                method: 'POST',	
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            },
                      
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun articolo trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {text}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
        		},
        		{
    					name: 'ACTPCS',
    					xtype: 'combo',
    					fieldLabel: 'Tipo costo',
    					forceSelection: true,								
    					displayField: 'text',
    					valueField: 'id',								
    					emptyText: '- seleziona -',
    			   		allowBlank: false,	
    			   		queryMode: 'local',
             		    minChars: 1, 	
        				labelWidth : 80,
        				anchor: '-15',			
    					store: {
    						editable: false,
    						autoDestroy: true,
    					    fields: [{name:'id'}, {name:'text'}],
    					    data: [								    
    					     <?php echo acs_ar_to_select_json(find_TA_sys('BVCS', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
    					    ]
    					}
    					,listeners: { 
        			 		beforequery: function (record) {
            	         		record.query = new RegExp(record.query, 'i');
            	         		record.forceAll = true;
            				 }
         				 }
    					
    			 },
					 
					 {xtype: 'fieldcontainer',
				     layout: { 	type: 'hbox'},						
						items: [
						 {
                			name: 'ACANNO',
                			xtype: 'numberfield',
                			fieldLabel: 'Anno',
                			labelWidth : 80,
                			hideTrigger : true,
                			maxValue : 2999,
                			width : 150,
                			value : <?php echo j($m_params->row->ACANNO); ?>
                			},
                			{
                			name: 'ACMESE',
                			xtype: 'numberfield',
                			fieldLabel: 'Mese',
                			labelWidth : 40,
                			hideTrigger : true,
                			labelAlign : 'right',
                			maxValue : 31,
                			width : 100,
                			value : <?php echo j($m_params->row->ACMESE); ?>
                			}
					
						
						]}
				
	            ],
	            
				buttons: [	
						{
		        		  text: 'Conferma',
			              scale: 'large',
			              iconCls: 'icon-button_blue_play-32',
			              handler: function() {
			              
			                  var form = this.up('form');
        	       			  var form_values = form.getValues(); 
        	       			  var loc_win = this.up('window');
        	       			  
         			        if (form.getForm().isValid()){	
         			          Ext.Ajax.request({
        				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_duplica_costo',
        				        timeout: 2400000,
        				        method     : 'POST',
        	        			jsonData: {
        	        				form_values : form_values,
        	        				row : <?php echo acs_je($m_params->row); ?>
        						},							        
        				        success : function(result, request){
        					        var jsonData = Ext.decode(result.responseText);
        					   		loc_win.destroy();     
        					    },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				    }); 
        		           }
			              
			                
			            }
		        		}
		       ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}