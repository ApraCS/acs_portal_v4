<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_check'){
    
    $ret = array();
    $row = get_TA_sys($m_table_config['TAID'], 'LIPC');
                        
                        
    if($row != false && count($row) > 0){
        $ret['msg_error'] = "Codice esistente";
        $ret['success'] = false;
        
        $sql_s = "SELECT RRN (TA) AS RRN, TA.*,
                  P2.TAORDI AS TAORDI2, P2.TADESC AS TADESC2, P2.TADES2 AS TADES22, P2.TAREST AS TAREST2
                  FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
                  LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} P2
                  ON TA.TADT = P2.TADT AND P2.TAID = 'LIP2' AND TA.TANR = P2.TANR
                  WHERE TADT='{$id_ditta_default}' AND TALINV = '' AND TAID = 'LIPC'
                  AND TANR = '{$m_params->codice}'";
        
        $stmt_s = db2_prepare($conn, $sql_s);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_s);
        $row = db2_fetch_assoc($stmt_s);
        $row['TADESC'] = acs_u8e(trim($row['TADESC']));
        $row['TADES2'] = trim($row['TADES2']);
        $row['TACINT'] = trim($row['TACINT']);
        $row['TANR'] = trim($row['TANR']);
        
        for($i = 0; $i <=27 ; $i+=3)
            $row["OR1_{$i}"] = trim(substr($row['TAREST'], $i, 3));
            $row["IE_OR1"] = trim(substr($row['TAREST'], 30, 1));
            
        for($i = 31; $i <=58 ; $i+=3)
            $row["OR2_{$i}"] = trim(substr($row['TAREST'], $i, 3));
            $row["IE_OR2"] = trim(substr($row['TAREST'], 61, 1));
                
        for($i = 62; $i <=89 ; $i+=3)
            $row["CLS_{$i}"] = trim(substr($row['TAREST'], $i, 3));
            $row["IE_CLS"] = trim(substr($row['TAREST'], 92, 1));
            
        for($i = 93; $i <=120 ; $i+=3)
            $row["GRP_{$i}"] = trim(substr($row['TAREST'], $i, 3));
            $row["IE_GRP"] = trim(substr($row['TAREST'], 123, 1));
                
        for($i = 124; $i <=151 ; $i+=3)
            $row["SGR_{$i}"] = trim(substr($row['TAREST'], $i, 3));
            $row["IE_SGR"] = trim(substr($row['TAREST'], 154, 1));
            
        for($i = 155; $i <=230 ; $i+=15)
            $row["ART_{$i}"] = trim(substr($row['TAREST'], $i, 15));
            $row["IE_ART"] = trim(substr($row['TAREST'], 245, 1));
            
        $tarest2 = sprintf("%-5s", $row['TAORDI2']);
        $tarest2 .= sprintf("%-30s", $row['TADESC2']);
        $tarest2 .= sprintf("%-30s", $row['TADES22']);
        $tarest2 .= sprintf("%-248s", $row['TAREST2']);
        $row['TAREST2'] = $tarest2;
                        
        $n = 0;
        for($a = 0; $a <=67 ; $a+=7){
            $n++;
            $row["var_collo{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_collo{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_collo{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
                        
        $n = 0;
        for($a = 70; $a <=137 ; $a+=7){
            $n++;
            $row["var_riga{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_riga{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_riga{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
        $n = 0;
        for($a = 140; $a <=207 ; $a+=7){
            $n++;
            $row["var_srig{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_srig{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_srig{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
        $row["gr_srig1"] = trim(substr($row['TAREST2'], 210, 3));
        $row["gr_srig2"] = trim(substr($row['TAREST2'], 213, 3));
        $row["gr_srig3"] = trim(substr($row['TAREST2'], 216, 3));
        $row["tempo_s"]  = trim(substr($row['TAREST2'], 219, 10));
        $row["um_s"]  = trim(substr($row['TAREST2'], 229, 1));
        
        $ret['record'] = $row;
    }else{
        $ret['success'] = true;
    }
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_sos_att'){
    
    $ar_upd = array();
    
    $tatp = trim($m_params->row->TATP);
    
    if($tatp == "")
        $new_value = "S";
    else
        $new_value = "";
            
            
    $ar_upd['TAUSUM'] = $auth->get_user();
    $ar_upd['TADTUM'] = oggi_AS_date();
    $ar_upd['TATP']   = $new_value;
    
    $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    WHERE RRN(TA) = '{$m_params->row->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}


function genera_TAREST($values){

    $value_attuale = $values->TAREST;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
        for($i = 0; $i <=27 ; $i+=3){
            $name = "OR1_{$i}";
            $chars = $values->$name;
            $len = "%-3s";
            $chars = substr(sprintf($len, $chars), 0, 3);
            $new_value = substr_replace($value_attuale, $chars, $i, 3);
            $value_attuale = $new_value;
            
        }
        $name = "IE_OR1";
        $chars1 = $values->$name;
        $len1 = "%-1s";
        $chars1 = substr(sprintf($len1, $chars1), 0, 1);
        $new_value = substr_replace($value_attuale, $chars1, 30, 1);
        $value_attuale = $new_value;
     
        for($i = 31; $i <=58 ; $i+=3){
            $name = "OR2_{$i}";
            $chars = $values->$name;
            $len = "%-3s";
            $chars = substr(sprintf($len, $chars), 0, 3);
            $new_value = substr_replace($value_attuale, $chars, $i, 3);
            $value_attuale = $new_value;
            
        }
        $name = "IE_OR2";
        $chars2 = $values->$name;
        $len2 = "%-1s";
        $chars2 = substr(sprintf($len2, $chars2), 0, 1);
        $new_value = substr_replace($value_attuale, $chars2, 61, 1);
        $value_attuale = $new_value;
        //CONTINUARE....        
        
        for($i = 62; $i <=89 ; $i+=3){
            
            $name = "CLS_{$i}";
            $chars = $values->$name;
            $len = "%-3s";
            $chars = substr(sprintf($len, $chars), 0, 3);
            $new_value = substr_replace($value_attuale, $chars, $i, 3);
            $value_attuale = $new_value;
            
        }
        
      
        
        $name = "IE_CLS";
        $chars3 = $values->$name;
        $len3 = "%-1s";
        $chars3 = substr(sprintf($len3, $chars3), 0, 1);
        $new_value = substr_replace($value_attuale, $chars3, 92, 1);
        $value_attuale = $new_value;
        
        for($i = 93; $i <=120 ; $i+=3){
            $name = "GRP_{$i}";
            $chars = $values->$name;
            $len = "%-3s";
            $chars = substr(sprintf($len, $chars), 0, 3);
            $new_value = substr_replace($value_attuale, $chars, $i, 3);
            $value_attuale = $new_value;
            
        }
        $name = "IE_GRP";
        $chars4 = $values->$name;
        $len4 = "%-1s";
        $chars4 = substr(sprintf($len4, $chars4), 0, 1);
        $new_value = substr_replace($value_attuale, $chars4, 123, 1);
        $value_attuale = $new_value;
        
        for($i = 124; $i <=151 ; $i+=3){
            $name = "SGR_{$i}";
            $chars = $values->$name;
            $len = "%-3s";
            $chars = substr(sprintf($len, $chars), 0, 3);
            $new_value = substr_replace($value_attuale, $chars, $i, 3);
            $value_attuale = $new_value;
            
        }
        $name = "IE_SGR";
        $chars5 = $values->$name;
        $len5 = "%-1s";
        $chars5 = substr(sprintf($len5, $chars5), 0, 1);
        $new_value = substr_replace($value_attuale, $chars5, 154, 1);
        $value_attuale = $new_value;
        
        for($i = 155; $i <=230 ; $i+=15){
            $name = "ART_{$i}";
            $chars = $values->$name;
            $len = "%-15s";
            $chars = substr(sprintf($len, $chars), 0, 15);
            $new_value = substr_replace($value_attuale, $chars, $i, 15);
            $value_attuale = $new_value;
            
        }
        $name = "IE_ART";
        $chars6 = $values->$name;
        $len6 = "%-1s";
        $chars6 = substr(sprintf($len6, $chars6), 0, 1);
        $new_value = substr_replace($value_attuale, $chars6, 245, 1);
        $value_attuale = $new_value;
        
        
    
        return $value_attuale;
}

function genera_TAREST2($values){
    $value_attuale = $values->TAREST2;

    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-313s", "");
    
    $n = 0;
    for($a = 0; $a <=67 ; $a+=7){
        $n++;
        
        $name = "var_collo{$n}";
        $chars = $values->$name;
        $len = "%-3s";
        $chars = substr(sprintf($len, $chars), 0, 3);
        $new_value = substr_replace($value_attuale, $chars, $a, 3);
        $value_attuale = $new_value;
        
        $name = "t_collo{$n}";
        $chars = $values->$name;
        $len = "%-1s";
        $chars = substr(sprintf($len, $chars), 0, 1);
        $new_value = substr_replace($value_attuale, $chars, $a + 3, 1);
        $value_attuale = $new_value;
        
        $name = "van_collo{$n}";
        $chars = $values->$name;
        $len = "%-3s";
        $chars = substr(sprintf($len, $chars), 0, 3);
        $new_value = substr_replace($value_attuale, $chars, $a + 4, 3);
        $value_attuale = $new_value;
    }
    
    $n = 0;
    for($a = 70; $a <=137 ; $a+=7){
        $n++;
        
        $name = "var_riga{$n}";
        $chars = $values->$name;
        $len = "%-3s";
        $chars = substr(sprintf($len, $chars), 0, 3);
        $new_value = substr_replace($value_attuale, $chars, $a, 3);
        $value_attuale = $new_value;
        
        $name = "t_riga{$n}";
        $chars = $values->$name;
        $len = "%-1s";
        $chars = substr(sprintf($len, $chars), 0, 1);
        $new_value = substr_replace($value_attuale, $chars, $a + 3, 1);
        $value_attuale = $new_value;
        
        $name = "van_riga{$n}";
        $chars = $values->$name;
        $len = "%-3s";
        $chars = substr(sprintf($len, $chars), 0, 3);
        $new_value = substr_replace($value_attuale, $chars, $a + 4, 3);
        $value_attuale = $new_value;
    }
    
    $n = 0;
    for($a = 140; $a <=207 ; $a+=7){
        $n++;
        
        $name = "var_srig{$n}";
        $chars = $values->$name;
        $len = "%-3s";
        $chars = substr(sprintf($len, $chars), 0, 3);
        $new_value = substr_replace($value_attuale, $chars, $a, 3);
        $value_attuale = $new_value;
        
        $name = "t_srig{$n}";
        $chars = $values->$name;
        $len = "%-1s";
        $chars = substr(sprintf($len, $chars), 0, 1);
        $new_value = substr_replace($value_attuale, $chars, $a + 3, 1);
        $value_attuale = $new_value;
        
        $name = "van_srig{$n}";
        $chars = $values->$name;
        $len = "%-3s";
        $chars = substr(sprintf($len, $chars), 0, 3);
        $new_value = substr_replace($value_attuale, $chars, $a + 4, 3);
        $value_attuale = $new_value;
    }
    
   
    $chars = substr(sprintf("%-3s", $values->gr_srig1), 0, 3);
    $new_value = substr_replace($value_attuale, $chars, 210, 3);
    $value_attuale = $new_value;
   
    $chars = substr(sprintf("%-3s", $values->gr_srig2), 0, 3);
    $new_value = substr_replace($value_attuale, $chars, 213, 3);
    $value_attuale = $new_value;
    
    $chars = substr(sprintf("%-3s", $values->gr_srig3), 0, 3);
    $new_value = substr_replace($value_attuale, $chars, 216, 3);
    $value_attuale = $new_value;
    
    
   
    $chars = sql_f(substr(sprintf("%-10s", $values->tempo_s), 0, 10));
    $new_value = substr_replace($value_attuale, $chars, 219, 10);
    $value_attuale = $new_value;
    
    $chars = substr(sprintf("%-1s", $values->um_s), 0, 1);
    $new_value = substr_replace($value_attuale, $chars, 229, 1);
    $value_attuale = $new_value;
    
    return $value_attuale;
}

if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $sql_2 = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
              WHERE TADT = '{$id_ditta_default}' AND TAID = 'LIP2'
              AND TANR = '{$m_params->form_values->TANR}'";
    
    $stmt_2 = db2_prepare($conn, $sql_2);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_2);
 
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ret = array();
    
    $row = get_TA_sys('LIPC', $m_params->form_values->TANR);
    
    if($row != false && count($row) > 0){
        $ret['success'] = false;
        $ret['msg_error'] = "Codice esistente in LIPC";
        echo acs_je($ret);
        return;
    }
    
    $row2 = get_TA_sys('LIP2', $m_params->form_values->TANR);
    
    if($row2 != false && count($row2) > 0){
        $ret['success'] = false;
        $ret['msg_error'] = "Codice esistente in LIP2";
        echo acs_je($ret);
        return;
    }
    
          
    
    $ar_ins['TAUSGE'] 	= $auth->get_user();
    $ar_ins['TADTGE']   = oggi_AS_date();
    $ar_ins['TAUSUM'] 	= $auth->get_user();
    $ar_ins['TADTUM']   = oggi_AS_date();
    $ar_ins['TADT'] 	= $id_ditta_default;
    $ar_ins['TAID'] 	= 'LIPC';
    $ar_ins['TANR'] 	= $m_params->form_values->TANR;
    $ar_ins['TADESC'] 	= trim($m_params->form_values->TADESC);
    $ar_ins['TADES2'] 	= trim($m_params->form_values->TADES2);
    if(isset($m_params->form_values->TACINT))
        $ar_ins['TACINT'] 	= $m_params->form_values->TACINT;
    $tarest = genera_TAREST($m_params->form_values);
    $ar_ins['TAREST'] 	= $tarest;
         
    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg();
    
    $ar_ins2['TAUSGE'] 	= $auth->get_user();
    $ar_ins2['TADTGE']  = oggi_AS_date();
    $ar_ins2['TAUSUM'] 	= $auth->get_user();
    $ar_ins2['TADTUM']  = oggi_AS_date();
    $ar_ins2['TADT'] 	= $id_ditta_default;
    $ar_ins2['TAID']    = 'LIP2';
    $ar_ins2['TANR'] 	= $m_params->form_values->TANR;
    $tarest2 = genera_TAREST2($m_params->form_values);
    $ar_ins2['TAORDI'] 	= substr($tarest2, 0, 5);
    $ar_ins2['TADESC'] 	= substr($tarest2, 5, 30);
    $ar_ins2['TADES2'] 	= substr($tarest2, 35, 30);
    $ar_ins2['TAREST']  = substr($tarest2, 65);
    
    
    $sql_2 = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($ar_ins2) . ") VALUES (" . create_parameters_point_by_ar($ar_ins2) . ")";
    
    $stmt_2 = db2_prepare($conn, $sql_2);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_2, $ar_ins2);
    echo db2_stmt_errormsg();
    
    $sql_s = "SELECT RRN (TA) AS RRN, TA.*,
              P2.TAORDI AS TAORDI2, P2.TADESC AS TADESC2, P2.TADES2 AS TADES22, P2.TAREST AS TAREST2
              FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
              LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} P2
              ON TA.TADT = P2.TADT AND P2.TAID = 'LIP2' AND TA.TANR = P2.TANR
              WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'LIPC'
              AND TA.TANR = '{$m_params->form_values->TANR}'";
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_s);
    $row = db2_fetch_assoc($stmt_s); 
    $row['TADESC'] = acs_u8e(trim($row['TADESC']));
    $row['TADES2'] = trim($row['TADES2']);
    $row['TACINT'] = trim($row['TACINT']);
    $row['TANR'] = trim($row['TANR']);
        
    for($i = 0; $i <=27 ; $i+=3)
        $row["OR1_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_OR1"] = trim(substr($row['TAREST'], 30, 1));
            
    for($i = 31; $i <=58 ; $i+=3)
        $row["OR2_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_OR2"] = trim(substr($row['TAREST'], 61, 1));
                
    for($i = 62; $i <=89 ; $i+=3)
        $row["CLS_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_CLS"] = trim(substr($row['TAREST'], 92, 1));
                    
    for($i = 93; $i <=120 ; $i+=3)
        $row["GRP_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_GRP"] = trim(substr($row['TAREST'], 123, 1));
                        
    for($i = 124; $i <=151 ; $i+=3)
        $row["SGR_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_SGR"] = trim(substr($row['TAREST'], 154, 1));
                            
    for($i = 155; $i <=230 ; $i+=15)
        $row["ART_{$i}"] = trim(substr($row['TAREST'], $i, 15));
        $row["IE_ART"] = trim(substr($row['TAREST'], 245, 1));
       
    $tarest2 = sprintf("%-5s", $row['TAORDI2']);
    $tarest2 .= sprintf("%-30s", $row['TADESC2']);
    $tarest2 .= sprintf("%-30s", $row['TADES22']);
    $tarest2 .= sprintf("%-248s", $row['TAREST2']);
    $row['TAREST2'] = $tarest2;
    
        $n = 0;
        for($a = 0; $a <=67 ; $a+=7){
            $n++;
            $row["var_collo{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_collo{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_collo{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
        
        $n = 0;
        for($a = 70; $a <=137 ; $a+=7){
            $n++;
            $row["var_riga{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_riga{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_riga{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
    $n = 0;
    for($a = 140; $a <=207 ; $a+=7){
        $n++;
        $row["var_srig{$n}"] = trim(substr($row['TAREST2'], $a, 3));
        $row["t_srig{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
        $row["van_srig{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
        
    }
    $row["gr_srig1"] = trim(substr($row['TAREST2'], 210, 3));
    $row["gr_srig2"] = trim(substr($row['TAREST2'], 213, 3));
    $row["gr_srig3"] = trim(substr($row['TAREST2'], 216, 3));
    $row["tempo_s"]  = trim(substr($row['TAREST2'], 219, 10));
    $row["um_s"]  = trim(substr($row['TAREST2'], 229, 1));
         
    $ret['success'] = true;
    $ret['record'] = $row;
    
    
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $ar_upd = array();
    
    $ar_upd['TAUSUM'] 	= $auth->get_user();
    $ar_upd['TADTUM']   = oggi_AS_date();
    if(isset($m_params->form_values->TACINT))
        $ar_upd['TACINT'] 	= $m_params->form_values->TACINT;
    $ar_upd['TADESC'] 	= utf8_decode(trim($m_params->form_values->TADESC));
    $ar_upd['TADES2'] 	= trim($m_params->form_values->TADES2);
 
    $tarest = genera_TAREST($m_params->form_values);
    $ar_upd['TAREST'] 	= $tarest;
        
    $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
        
        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg();
    
    $ar_upd2['TAUSUM'] 	= $auth->get_user();
    $ar_upd2['TADTUM']   = oggi_AS_date();
    $tarest2 = genera_TAREST2($m_params->form_values);
    $ar_upd2['TAORDI'] 	= substr($tarest2, 0, 5);
    $ar_upd2['TADESC'] 	= substr($tarest2, 5, 30);
    $ar_upd2['TADES2'] 	= substr($tarest2, 35, 30);
    $ar_upd2['TAREST']  = substr($tarest2, 65);

    
    $sql_2 = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
             SET " . create_name_field_by_ar_UPDATE($ar_upd2) . "
             WHERE TADT = '{$id_ditta_default}' AND TAID = 'LIP2'
             AND TANR = '{$m_params->form_values->TANR}'";
    
    $stmt_2 = db2_prepare($conn, $sql_2);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_2, $ar_upd2);
    echo db2_stmt_errormsg();
        
        
    $sql_s = "SELECT RRN (TA) AS RRN, TA.*,
              P2.TAORDI AS TAORDI2, P2.TADESC AS TADESC2, P2.TADES2 AS TADES22, P2.TAREST AS TAREST2
              FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
              LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} P2
              ON TA.TADT = P2.TADT AND P2.TAID = 'LIP2' AND TA.TANR = P2.TANR
              WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'LIPC'
              AND TA.TANR = '{$m_params->form_values->TANR}'";
 
    
    $stmt_s = db2_prepare($conn, $sql_s);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_s);
    echo db2_stmt_errormsg($stmt_s);
    $row = db2_fetch_assoc($stmt_s);
    $row['TADESC'] = acs_u8e(trim($row['TADESC']));
    $row['TADES2'] = trim($row['TADES2']);
    $row['TACINT'] = trim($row['TACINT']);
    $row['TANR'] = trim($row['TANR']);
    for($i = 0; $i <=27 ; $i+=3)
        $row["OR1_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_OR1"] = trim(substr($row['TAREST'], 30, 1));
            
    for($i = 31; $i <=58 ; $i+=3)
        $row["OR2_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_OR2"] = trim(substr($row['TAREST'], 61, 1));
                
    for($i = 62; $i <=89 ; $i+=3)
        $row["CLS_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_CLS"] = trim(substr($row['TAREST'], 92, 1));
                    
    for($i = 93; $i <=120 ; $i+=3)
        $row["GRP_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_GRP"] = trim(substr($row['TAREST'], 123, 1));
                        
    for($i = 124; $i <=151 ; $i+=3)
        $row["SGR_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_SGR"] = trim(substr($row['TAREST'], 154, 1));
                            
    for($i = 155; $i <=230 ; $i+=15)
        $row["ART_{$i}"] = trim(substr($row['TAREST'], $i, 15));
        $row["IE_ART"] = trim(substr($row['TAREST'], 245, 1));
        
    $tarest2 = sprintf("%-5s", $row['TAORDI2']);
    $tarest2 .= sprintf("%-30s", $row['TADESC2']);
    $tarest2 .= sprintf("%-30s", $row['TADES22']);
    $tarest2 .= sprintf("%-248s", $row['TAREST2']);
    $row['TAREST2'] = $tarest2;
    
    $n = 0;
    for($a = 0; $a <=67 ; $a+=7){
        $n++;
        $row["var_collo{$n}"] = trim(substr($row['TAREST2'], $a, 3));
        $row["t_collo{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
        $row["van_collo{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
        
    }
    
    $n = 0;
    for($a = 70; $a <=137 ; $a+=7){
        $n++;
        $row["var_riga{$n}"] = trim(substr($row['TAREST2'], $a, 3));
        $row["t_riga{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
        $row["van_riga{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
        
    }
    $n = 0;
    for($a = 140; $a <=207 ; $a+=7){
        $n++;
        $row["var_srig{$n}"] = trim(substr($row['TAREST2'], $a, 3));
        $row["t_srig{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
        $row["van_srig{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
        
    }
    $row["gr_srig1"] = trim(substr($row['TAREST2'], 210, 3));
    $row["gr_srig2"] = trim(substr($row['TAREST2'], 213, 3));
    $row["gr_srig3"] = trim(substr($row['TAREST2'], 216, 3));
    $row["tempo_s"]  = trim(substr($row['TAREST2'], 219, 10));
    $row["um_s"]  = trim(substr($row['TAREST2'], 229, 1));
        
   
    $ret['success'] = true;
    $ret['record'] = $row;
    echo acs_je($ret);
    exit;
}




if ($_REQUEST['fn'] == 'get_data_grid'){
    
    $ar_lin = array();
    foreach($cfg_mod_DeskUtility['ar_lingue'] as $v){
        $ar_lin[] = $v['id'];
    }
    
 
    $sql = "SELECT RRN (TA) AS RRN, TA.*, TA_TRAD.C_ROW AS TRAD, 
            P2.TAORDI AS TAORDI2, P2.TADESC AS TADESC2, P2.TADES2 AS TADES22, P2.TAREST AS TAREST2
            FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            {$sql_join}
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} P2
            ON TA.TADT = P2.TADT AND P2.TAID = 'LIP2' AND TA.TANR = P2.TANR
            LEFT  OUTER JOIN (
                SELECT COUNT(*) AS C_ROW, TANR
                FROM  {$cfg_mod_DeskArt['file_tabelle_sys']}
                WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_table_config['TAID']}' AND TALINV IN (" . sql_t_IN($ar_lin) . ")
                GROUP BY TANR) TA_TRAD
            ON TA.TANR = TA_TRAD.TANR
            WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = 'LIPC'
            ORDER BY TA.TANR, TA.TADESC";
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $data = array();
    
    while ($row = db2_fetch_assoc($stmt)) {
        
        $row['RRN'] = acs_u8e(trim($row['RRN']));
        $row['TAID'] = trim($row['TAID']);
        $row['TRAD'] = trim($row['TRAD']);
        $row['TADESC'] = acs_u8e(trim($row['TADESC']));
        $row['TADES2'] = trim($row['TADES2']);
        $row['TACINT'] = trim($row['TACINT']);
        $row['TATP'] = trim($row['TATP']);
        $row['TANR'] = trim($row['TANR']);
        $row['TADTGE'] = trim($row['TADTGE']);
        $row['TAUSGE'] = trim($row['TAUSGE']);
        $row['TADTUM'] = trim($row['TADTUM']);
        $row['TAUSUM'] = trim($row['TAUSUM']);
       
        for($i = 0; $i <=27 ; $i+=3)
            $row["OR1_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_OR1"] = trim(substr($row['TAREST'], 30, 1));
        
        for($i = 31; $i <=58 ; $i+=3)
            $row["OR2_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_OR2"] = trim(substr($row['TAREST'], 61, 1));
        
        for($i = 62; $i <=89 ; $i+=3)
            $row["CLS_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_CLS"] = trim(substr($row['TAREST'], 92, 1));
        
        for($i = 93; $i <=120 ; $i+=3)
            $row["GRP_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_GRP"] = trim(substr($row['TAREST'], 123, 1));
        
        for($i = 124; $i <=151 ; $i+=3)
            $row["SGR_{$i}"] = trim(substr($row['TAREST'], $i, 3));
        $row["IE_SGR"] = trim(substr($row['TAREST'], 154, 1));
        
        for($i = 155; $i <=230 ; $i+=15)
            $row["ART_{$i}"] = trim(substr($row['TAREST'], $i, 15));
        $row["IE_ART"] = trim(substr($row['TAREST'], 245, 1));
        
        $tarest2 = sprintf("%-5s", $row['TAORDI2']);
        $tarest2 .= sprintf("%-30s", $row['TADESC2']);
        $tarest2 .= sprintf("%-30s", $row['TADES22']);
        $tarest2 .= $row['TAREST2'];
        $row['TAREST2'] = $tarest2;
        
        $n = 0;
        for($a = 0; $a <=67 ; $a+=7){
            $n++;
            $row["var_collo{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_collo{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_collo{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
                                                                                                                                                  
        $n = 0;
        for($a = 70; $a <=137 ; $a+=7){
            $n++;
            $row["var_riga{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_riga{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_riga{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
        $n = 0;
        for($a = 140; $a <=207 ; $a+=7){
            $n++;
            $row["var_srig{$n}"] = trim(substr($row['TAREST2'], $a, 3));
            $row["t_srig{$n}"] = trim(substr($row['TAREST2'], $a + 3, 1));
            $row["van_srig{$n}"] = trim(substr($row['TAREST2'], $a + 4, 3));
            
        }
        $row["gr_srig1"] = trim(substr($row['TAREST2'], 210, 3));
        $row["gr_srig2"] = trim(substr($row['TAREST2'], 213, 3));
        $row["gr_srig3"] = trim(substr($row['TAREST2'], 216, 3));
        $row["tempo_s"]  = trim(substr($row['TAREST2'], 219, 10));
        $row["um_s"]  = trim(substr($row['TAREST2'], 229, 1));
       
        
        
        $data[] = $row;
        
    }
 
    echo acs_je($data);
    
    exit();
    
}




if ($_REQUEST['fn'] == 'open_panel'){ ?>
{"success":true, "items": [

   {
			xtype: 'panel',
			title: 'LIPC-Linee ProLog',
    		<?php echo make_tab_closable(); ?>,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
        		{ xtype: 'grid',
                multiSelect : true,
                itemId : 'my_grid',
                flex : 0.7,
        	    features: [{
        				ftype: 'filters',
        				encode: false, 
        				local: true,   
        		   		 filters: [
        		       {
        		 		type: 'boolean',
        				dataIndex: 'visible'
        		     }
        		      ]
        			}]
				,store: {
						
						xtype: 'store',
						autoLoad: true,
					   	proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },							        
							     extraParams: {},
			        			 doRequest: personalizza_extraParams_to_jsonData, 
						         reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        		 fields: ['TRAD', 'RRN', 'TAID', 'TANR', 'TADESC', 'TADES2', 'TACINT', 
		        		        'TAREST', 'TADTGE', 'TAUSGE', 'TADTUM', 'TAUSUM', 'TATP', 'TAREST2',
		        		 		'IE_OR1', 'IE_OR2', 'IE_CLS', 'IE_GRP', 'IE_SGR', 'IE_ART',
		        		 		'gr_srig1', 'gr_srig2', 'gr_srig3', 'tempo_s', 'um_s'
		        		 		<?php for($i = 0; $i <=27 ; $i+=3){?>
		        		 		, 'OR1_<?php echo $i; ?>'
		        		 		<?php }?>
		        		 		<?php for($i = 31; $i <=58 ; $i+=3){?>
		        		 		, 'OR2_<?php echo $i; ?>'
		        		 		<?php }?>
		        		 		<?php for($i = 62; $i <=89 ; $i+=3){?>
		        		 		, 'CLS_<?php echo $i; ?>'
		        		 		<?php }?>
		        		 		<?php for($i = 93; $i <=120 ; $i+=3){?>
		        		 		, 'GRP_<?php echo $i; ?>'
		        		 		<?php }?>
		        		 		<?php for($i = 124; $i <=151 ; $i+=3){?>
		        		 		, 'SGR_<?php echo $i; ?>'
		        		 		<?php }?>
		        		 		<?php for($i = 155; $i <=230 ; $i+=15){?>
		        		 		, 'ART_<?php echo $i; ?>'
		        		 		<?php }?>
		        		 		<?php 
		        		 		for($a = 1; $a <=10 ; $a++){?>
		        		 		    , 'var_collo<?php echo $a; ?>'
		        		 		    , 't_collo<?php echo $a; ?>'
		        		 		    , 'van_collo<?php echo $a; ?>'
		        		 		    
		        		 		    , 'var_riga<?php echo $a; ?>'
		        		 		    , 't_riga<?php echo $a; ?>'
		        		 		    , 'van_riga<?php echo $a; ?>'
		        		 		    
		        		 		    , 'var_srig<?php echo $a; ?>'
		        		 		    , 't_srig<?php echo $a; ?>'
		        		 		    , 'van_srig<?php echo $a; ?>'
		        		 		<?php }?>
		        		 		   
		        		 		   
		        		 		],
		        		
		        		},
		    		<?php 
		    		      $divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
		    		      $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
		    		?>
					columns: [ 
			           {
            			text: '<?php echo $divieto; ?>',
            		 	dataIndex: 'TATP', 
            		 	width : 40,
            		 	filter: {type: 'string'}, filterable: true,
            		 	renderer: function(value, metaData, record){
		    			  if(record.get('TATP') == 'S'){ 
                               metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			        return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	   }
						}},
			        	{
            			header: 'Cod.',
            		 	dataIndex: 'TANR', 
            		 	width : 50,
            		 	filter: {type: 'string'}, filterable: true,
            		    },{
            			header: 'Interfaccia',
            		 	dataIndex: 'TACINT', 
            		 	width : 150,
            		 	filter: {type: 'string'}, filterable: true,
            		    },{
            			header: 'Descrizione',
            		 	dataIndex: 'TADESC', 
            		 	flex : 1,
            		 	filter: {type: 'string'}, filterable: true,
            		    },{
            			header: 'Note',
            		 	dataIndex: 'TADES2', 
            		 	flex : 1,
            		 	filter: {type: 'string'}, filterable: true,
            		    },{
            			text: '<?php echo $trad; ?>',
            		 	dataIndex: 'TRAD', 
            		 	width : 40,
            		 	filter: {type: 'string'}, filterable: true,
            		 	renderer: function(value, metaData, record){
                         if(record.get('TRAD') > 0){
		    			   return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=18>';
		    		 	}
 						}},
 						{header: 'Immissione',
	 					columns: [
	 						{header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
	 						renderer: function(value, metaData, record){
	         					if (record.get('TADTGE') != record.get('TADTUM')) 
	          						metaData.tdCls += ' grassetto';
	 
             					q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
			 					metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		     					return date_from_AS(value);	
							}
	 
	 						}
                    	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
                    	  renderer: function(value, metaData, record){
                    	  
                    	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
                    	          metaData.tdCls += ' grassetto';
                    	  
                                 q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
                    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
                    		     return value;	
                    	} 
                    	 }
                    	 ]}
			        
			        ],  
					
						listeners: {
						
					      	 selectionchange: function(selModel, selected) { 
	                           if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            		               rec_index = selected[0].index;
            		               form_dx.getForm().findField('rec_index').setValue(rec_index);
            	                   acs_form_setValues(form_dx, selected[0].data);
            	                 //  form_dx.getForm().setValues(selected[0].data);
            	                }
            	                
		       		  	 	 }
		       		  	 	 
		       		  	 	,itemcontextmenu : function(grid, rec, node, index, event) {
				  				event.stopEvent();
				  													  
						 		var voci_menu = [];
					     		var row = rec.data;
					     		var m_grid = this;
					     		
					     		id_selected = grid.getSelectionModel().getSelection();
					     		list_selected_id = [];
					     		for (var i=0; i<id_selected.length; i++){
					     			list_selected_id.push({
					     			   RRN : id_selected[i].get('RRN'),
					     			   TAREST : id_selected[i].get('TAREST')});
								}
					     
					 	 voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          rec.set('TATP', jsonData.new_value);	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
			    		
        			    
        				      var menu = new Ext.menu.Menu({
        				            items: voci_menu
        					}).showAt(event.xy);	
			    	
			    	},celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	var titolo = 'Traduzione [' +rec.get('TANR') + '] ';
					  	
					  	 if(col_name == 'TRAD')
            				  	acs_show_win_std( titolo + rec.get('TADESC'), 'acs_anag_art_gest_trad_sys.php?fn=open_al', {tanr: rec.get('TANR'), taid : rec.get('TAID'), tacor2 : rec.get('TACOR2')}, 450, 500, null, 'icon-globe-16');
            			
					  	}
					  	}
					},
						viewConfig: {
        		        getRowClass: function(record, index) {	
        		        
        		        if(record.get('TATP') == 'S'){
        		           return ' colora_riga_rosso';
                		 }
		        														
		         }   
		    }
       
        }  
	, {		   xtype: 'form',
	            itemId: 'dx_form',
	            autoScroll : true,
	            title: 'Dettagli linee/gruppi logistico/produttivi',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            flex:0.3,
	            frame: true,
	            items: [ 
 		      	{
        		xtype: 'tabpanel',
        		items: [
        		
        		{
				xtype: 'panel',
				title: 'Dettagli', 
				autoScroll : true,
				frame : true,
				 layout: 'anchor',
				//layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
		    		{xtype : 'textfield',name : 'rec_index', hidden : true},
		    		{xtype : 'textfield',name : 'RRN', hidden : true},
		    		{xtype : 'textfield',name : 'TAREST', hidden : true},
		    		{xtype : 'textfield',name : 'TAREST2', hidden : true},
		    	    {
    				xtype : 'textfield',
    		 		name: 'TANR', 
    		 		maxLength : 4,
    		 		fieldLabel: 'Codice',
    		 		width : 140,
    		 		listeners: {
    					'change': function(field){
    						 field.setValue(field.getValue().toUpperCase());
      				},
      				'blur': function(field) {
      				    var form = this.up('form').getForm();
      				    var grid = this.up('form').up('panel').down('grid');
      				    
      					Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				codice : field.getValue(),
			        			},				
							  success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        
							        if(jsonData.success == false){
					      	    		 acs_show_msg_error(jsonData.msg_error);
							        	 form.setValues(jsonData.record);
			        			   		 var rec_index = grid.getStore().findRecord('TANR', jsonData.record.TANR);
		  					   			 grid.getView().select(rec_index);
			  					   	     grid.getView().focusRow(rec_index);
				        			}
						
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
                    	 		
    
                		}
     				}
        		  },{
        			xtype : 'textfield',
        		 	name: 'TACINT', 
        		 	fieldLabel: 'Interfaccia',
        		 	maxLength : 10,
        		 	flex : 1,
        		 	anchor: '-15'
        		  }	,{
        			xtype : 'textfield',
        		 	name: 'TADESC', 
        		 	fieldLabel: 'Descrizione',
        		 	maxLength : 30,
        		 	anchor: '-15'
        		  }	,{
        			xtype : 'textfield',
        		 	name: 'TADES2', 
        		 	fieldLabel: 'Note',
        		 	maxLength : 30,
        		 	anchor: '-15'
        		  },
        		  
        		   {xtype: 'fieldcontainer',
        		   anchor: '-15',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {xtype: 'numberfield',
            			    name: 'tempo_s',
            			    flex: 1.5,
    						fieldLabel: 'Tempo stimato',
    						hideTrigger : true,
    						maxValue: 9999999.999,
    						decimalPrecision : 3
    						},
								{
								name: 'um_s',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'UM',
								labelWidth : 30,
								labelAlign : 'right',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
							    store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     	 {id: 'M', text : '[M] Minuti'},
								     	 {id: 'S', text : '[S] Secondi'},
								     	 {id: 'H', text : '[H] Ore'}
								     	
								    ]
								}
								
						   }
						
						]}
                
		    		]},
		    
    		      {
    				xtype: 'panel',
    				title: 'Oper.1', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    					{
                			name: 'IE_OR1',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
    				    <?php  
    				    $a = 0;
    				    for($i = 0; $i <=27 ; $i+=3){
    				        $a++;  ?>
                        
                         {
                    		name: 'OR1_<?php echo $i; ?>',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '<?php echo $a; ?>)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('PUOP', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         },
            				
                      <?php }?>
    				    				
    					]},
    					
    					{
    				xtype: 'panel',
    				title: 'Oper.2', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    					{
                			name: 'IE_OR2',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
    				    <?php  
    				    $a = 0;
    				    for($i = 31; $i <=58 ; $i+=3){
    				        $a++;  ?>
                        
                         {
                    		name: 'OR2_<?php echo $i; ?>',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '<?php echo $a; ?>)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('PUOP', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         },
            				
                      <?php }?>
    				
    					]},
    					{
    				xtype: 'panel',
    				title: 'Classi', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    						{
                			name: 'IE_CLS',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
    				    <?php  
    				    $a = 0;
    				    for($i = 62; $i <=89 ; $i+=3){
    				        $a++;  ?>
                        
                         {
                    		name: 'CLS_<?php echo $i; ?>',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '<?php echo $a; ?>)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('MUCM', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         },
            				
                      <?php }?>
    					]},
    				{
    				xtype: 'panel',
    				title: 'Gruppi', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    						{
                			name: 'IE_GRP',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
    				    <?php  
    				    $a = 0;
    				    for($i = 93; $i <=120 ; $i+=3){
    				        $a++;  ?>
                        
                         {
                    		name: 'GRP_<?php echo $i; ?>',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '<?php echo $a; ?>)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('MUGM', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         },
            				
                      <?php }?>
    					]},
    					{
    				xtype: 'panel',
    				title: 'Sottogr.', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    					{
                			name: 'IE_SGR',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
    				    <?php  
    				    $a = 0;
    				    for($i = 124; $i <=151 ; $i+=3){
    				        $a++;  ?>
                        
                         {
                    		name: 'SGR_<?php echo $i; ?>',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '<?php echo $a; ?>)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('MUSM', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         },
            				
                      <?php }?>
    					]},
    				{
    				xtype: 'panel',
    				title: 'Stringhe', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    				{
                			name: 'IE_ART',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
    				    <?php  
    				    $a = 0;
    				    for($i = 155; $i <=230 ; $i+=15){
    				        $a++;  ?>
                        	{ xtype : 'textfield',
                        	  maxLength : 15,
                        	  fieldLabel: '<?php echo $a; ?>)',
							  name: 'ART_<?php echo $i; ?>',
						      anchor: '-15',
						      listeners: {
    						    'change': function(field){
    					        field.setValue(field.getValue().toUpperCase());
      					 }
 						 }
						
						 },
                       <?php }?>
    					]},
    						{
    				xtype: 'panel',
    				title: 'Collo', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    				            <?php 
	        		 		for($a = 1; $a <=10 ; $a++){
	        		 		    
	        		 		    $var  = "var_collo{$a}";
	        		 		    $tipo = "t_collo{$a}";
	        		 		    $van  = "van_collo{$a}";
	        		 		    
		        		 	
	        		 		    $proc = new ApiProc();
	        		 		    echo $proc->get_json_response(
	        		 		        extjs_combo_dom_ris(array(
	        		 		            'initial_value' => array('dom_c'  => $m_params->$var,
                        	        		 		             'ris_c'  =>  $m_params->$van,
                        	        		 		             'tipo_c' =>  $m_params->$tipo,
                    	        		 		                 'out_d'  => $m_params->out_v1),
	        		 		            'risposta_a_capo' => true,
	        		 		            'label' => "Variabile ({$a})<br>Variante",
	        		 		            'file_TA' => 'sys',
	        		 		            'tipo_opz_risposta' => array('output_domanda' => true,
	        		 		                'tipo_cf' => $tipo,
	        		 		                'opzioni' => array(
	        		 		                    array('id' => '',   'text' => 'Variante', 'taid' => '*RIS*'),
	        		 		                    array('id' => 'T',  'text' => 'Tipologia', 'taid' => 'PUTI')
	        		 		                )),
	        		 		                'dom_cf'  => $var,  'ris_cf' => $van))
	        		 		        );
	        		 		    echo ",";
		        		 		 
		        		  }?>
    					
            				
    					]},		{
    				xtype: 'panel',
    				title: 'Riga', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    				<?php
    				for($a = 1; $a <=10 ; $a++){
    				    
    				    $var  = "var_riga{$a}";
    				    $tipo = "t_riga{$a}";
    				    $van  = "van_riga{$a}";
    				    
    				    
    				    $proc = new ApiProc();
    				    echo $proc->get_json_response(
    				        extjs_combo_dom_ris(array(
    				            'initial_value' => array('dom_c'  => $m_params->$var,
    				                'ris_c'  =>  $m_params->$van,
    				                'tipo_c' =>  $m_params->$tipo,
    				                'out_d'  => $m_params->out_v1),
    				            'risposta_a_capo' => true,
    				            'label' => "Variabile ({$a})<br>Variante",
    				            'file_TA' => 'sys',
    				            'tipo_opz_risposta' => array('output_domanda' => true,
    				                'tipo_cf' => $tipo,
    				                'opzioni' => array(
    				                    array('id' => '',   'text' => 'Variante', 'taid' => '*RIS*'),
    				                    array('id' => 'T',  'text' => 'Tipologia', 'taid' => 'PUTI')
    				                )),
    				                'dom_cf'  => $var,  'ris_cf' => $van))
    				        );
    				    echo ",";
    				    
		        		  }?>
    					
            				
    					]},
    						{
    				xtype: 'panel',
    				title: 'Sottoriga', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
		        		  <?php
		        		  for($a = 1; $a <=10 ; $a++){
		        		      
		        		      $var  = "var_srig{$a}";
		        		      $tipo = "t_srig{$a}";
		        		      $van  = "van_srig{$a}";
		        		      
		        		      
		        		      $proc = new ApiProc();
		        		      echo $proc->get_json_response(
		        		          extjs_combo_dom_ris(array(
		        		              'initial_value' => array('dom_c'  => $m_params->$var,
		        		                  'ris_c'  =>  $m_params->$van,
		        		                  'tipo_c' =>  $m_params->$tipo,
		        		                  'out_d'  => $m_params->out_v1),
		        		              'risposta_a_capo' => true,
		        		              'label' => "Variabile ({$a})<br>Variante",
		        		              'file_TA' => 'sys',
		        		              'tipo_opz_risposta' => array('output_domanda' => true,
		        		                  'tipo_cf' => $tipo,
		        		                  'opzioni' => array(
		        		                      array('id' => '',   'text' => 'Variante', 'taid' => '*RIS*'),
		        		                      array('id' => 'T',  'text' => 'Tipologia', 'taid' => 'PUTI')
		        		                  )),
		        		                  'dom_cf'  => $var,  'ris_cf' => $van))
		        		          );
		        		      echo ",";
		        		      
		        		  }?>
    					
            				
    					]},
		           {
    				xtype: 'panel',
    				title: 'Gr. sottor.', 
    				autoScroll : true,
    				frame : true,
    				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
    				items: [
    				
    				                       {
                    		name: 'gr_srig1',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '1)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('MUSM', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         },{
                    		name: 'gr_srig2',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '2)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('MUSM', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         },{
                    		name: 'gr_srig3',
                    		xtype: 'combo',
                    		flex: 1,
                    		fieldLabel: '3)',
                    		//labelWidth : 50,
                    		forceSelection: true,								
                    		displayField: 'text',
                    		valueField: 'id',								
                    		emptyText: '- seleziona -',
                    		//allowBlank: false,								
                    		anchor: '-15',
                    		queryMode: 'local',
                    		minChars: 1,	
                    		store: {
                    			editable: false,
                    			autoDestroy: true,
                    			fields: [{name:'id'}, {name:'text'}],
                    			data: [								    
                    			 <?php echo acs_ar_to_select_json(find_TA_sys('MUSM', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                    			]
                    		},listeners: { 
                    				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                    		}
                    		
                         }
    				
    				]}
		    ]}
		    ],
		    		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [ '->',
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
	       			  
	       			  if(form_values.RRN == ''){
							  acs_show_msg_error('Selezionare una riga dalla tabella di sinistra');
							  return false;
						 }
        		
        		      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				       Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
					      	    		 	acs_show_msg_error(jsonData.msg_error);
							        	 	return;
				        			     }else{
        					         		form.getForm().reset();
							        	    var rec_index = grid.getStore().findRecord('TANR', form_values.TANR);
							       		    grid.getStore().remove(rec_index);	
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
					
					    	}
            	   		  });		    
							    
				        }

			     },  
			      {
                     xtype: 'button',
                    text: 'Crea',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	 var form_values = this.up('form').getValues();
 			          var form = this.up('form').getForm();
	       			  var grid = this.up('form').up('panel').down('grid'); 
 			        
 			         //if (form.isValid()){
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if(jsonData.success == false){
					      	    acs_show_msg_error(jsonData.msg_error);
					      	    return false;
					        }else{
					        	//grid.getStore().load();
					        	
					        	 var new_rec = jsonData.record;
					        	 grid.getStore().add(new_rec);
					        	 var rec_index = grid.getStore().findRecord('TANR', new_rec.TANR);
			  					 grid.getView().select(rec_index);
			  					 grid.getView().focusRow(rec_index);
					           
					        }
					       },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		          // }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			var index = form_values.rec_index;
 			       			//var record_grid = grid.getStore().getAt(index);
 			       			var record_grid = grid.getSelectionModel().getSelection()[0];
 			       			
 			       			if(form_values.RRN == ''){
								  acs_show_msg_error('Selezionare una riga dalla tabella di sinistra');
								  return false;
							 }			
 			       						       		 
 			       		 
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error(jsonData.msg_error);
					      	    		return false;
					        			}else{
					        			   var new_rec = jsonData.record;
					        			    record_grid.set(new_rec);
					        			   
					        			   
					        			}
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               
			            }

			     }
			     ]
		   }]
		   
			 	 }  
		 ],
					 
					
					
	}
]
}

<?php 

exit; 

}




