<?php

require_once("../../config.inc.php");
require_once("./_utility.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$m_params = acs_m_params_json_decode();



if ($_REQUEST['fn'] == 'exe_duplica'){
    
    $list_rows = $m_params->list_selected_art;
    
    foreach($list_rows as $v){
        
        $sh = new SpedHistory($deskArt);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'DUP_COND_LISV',
                "vals" => array(
                    "RICDOLD" 	=> $m_params->form_values->f_articolo,
                    "RICDNEW" 	=> $v->c_art,
                    "RICLME"    => $m_params->form_values->f_listino,
                    "RIFG01"    => $m_params->form_values->f_canc,
                    "RITAB1"    => $m_params->form_values->f_riga_da,
                    "RITAB2"    => $m_params->form_values->f_riga_a,
                    "RIDT"  	=> $id_ditta_default,
                ),
            )
            );
        
     
        
    }
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_data_articolo'){
    global $id_ditta_default;
    $ar_art = find_ART_like($_REQUEST['query'],  $id_ditta_default, 'B');
    echo acs_je(array("root" => $ar_art));
    exit;
} //get_json_data


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_data_righe'){
   
    
    $sql = "SELECT LCRIGA, LCNOTE
            FROM {$cfg_mod_DeskUtility['file_listini_licon']} LC
            WHERE LCDT = '{$id_ditta_default}' AND LCSZLI='V' AND LCCCON=0
            AND LCLIST = '{$m_params->listino}' AND LCZONA = '' AND LCVALU='EUR' 
            AND LCART= '{$m_params->articolo}'
            ORDER BY LCRIGA";
    
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $ret[] = array( "id" 	=> $row['LCRIGA'],
                        "text" 	=> "[".trim($row['LCRIGA'])."] ".trim($row['LCNOTE']),
            
        );
        
    }
    
    
    echo acs_je($ret);
    exit;
} //get_json_data

if ($_REQUEST['fn'] == 'open_form'){
    
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	         
						 {
							name: 'f_listino',
							xtype: 'combo',
							fieldLabel: 'Listino da elaborare',
							forceSelection: true,								
							displayField: 'text',
							valueField: 'id',								
							emptyText: '- seleziona -',
					   		queryMode: 'local',
                 		    minChars: 1, 	
            				labelWidth : 120,
            				anchor: '-15',
            				width : 150	,
            				allowBlank : false,				
							store: {
								editable: false,
								autoDestroy: true,
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
							     <?php 
							     $where = " AND (SUBSTRING(TAREST, 19, 2) = 'V'
                                              OR SUBSTRING(TAREST, 21, 2) = 'V'
                                              OR SUBSTRING(TAREST, 23, 2) = 'V'
                                              OR SUBSTRING(TAREST, 25, 2) = 'V'
                                              OR SUBSTRING(TAREST, 27, 2) = 'V')";
							     
							     echo acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, $where, 'Y'), ''); 
							     
							     
							     ?>	
							    ]
							}
							,listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
	            				 },
	            				 change: function(field,newVal) {	
                              		if (!Ext.isEmpty(newVal)){
                                    	combo_riga = this.up('form').down('#c_riga');                      		 
                                     	combo_riga.store.proxy.extraParams.listino = newVal;
                                    	combo_riga.store.load();    
                                    	
                                    	combo_riga2 = this.up('form').down('#c_riga2');                      		 
                                     	combo_riga2.store.proxy.extraParams.listino = newVal;
                                    	combo_riga2.store.load();                             
                                     }
                                 
    
                                }
	            				 
	            		
	         				 }
							
					 },
					  {
						flex: 1,
			            xtype: 'combo',
						name: 'f_articolo',
						fieldLabel: 'Articolo',
						labelWidth : 120,
						anchor: '-15',
						minChars: 2,	
						allowBlank : false,			
						store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_articolo',
					            actionMethods: {
						          read: 'POST'
						        },
					            extraParams: {
				    		    		gruppo: '',
				    				},				            
						        doRequest: personalizza_extraParams_to_jsonData, 
					            reader: {
					                type: 'json',
					                root: 'root',
					                method: 'POST',	
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            },
                      
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun articolo trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {text}' + 
			                    '</div>';
			                	}                
		                
		            	}
		            	,listeners: { 
            			 		
	            				 change: function(field,newVal) {	
                              		if (!Ext.isEmpty(newVal)){
                                    	combo_riga = this.up('form').down('#c_riga');                      		 
                                     	combo_riga.store.proxy.extraParams.articolo = newVal;
                                    	combo_riga.store.load();   
                                    	
                                    	combo_riga2 = this.up('form').down('#c_riga2');                      		 
                                     	combo_riga2.store.proxy.extraParams.articolo = newVal;
                                    	combo_riga2.store.load();                           
                                     }
                                 
    
                                }
	            				 
	            		
	         				 }
        		},
					 
					 	{
						xtype: 'checkboxgroup',
						fieldLabel: 'Cancella preesistenti',
						labelWidth : 120,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_canc' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					},
					
					{ xtype: 'fieldcontainer'
              			  , flex: 1
              			  , anchor: '-15'
                          , layout:
                              {	type: 'hbox'
                              , pack: 'start'
                        	  , align: 'stretch'
                                }
                          , items: [ 
                        
                        
                          {
						flex : 1.5,
			            xtype: 'combo',
						name: 'f_riga_da',
						itemId : 'c_riga',
						fieldLabel: 'Riga condizioni da',
						labelWidth : 120,
						anchor: '-15',
						minChars: 2,	
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,		
						store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_righe',
					            actionMethods: {
						          read: 'POST'
						        },
					            extraParams: {
					            	listino : '',
					            	articolo : ''
					            },				            
						        doRequest: personalizza_extraParams_to_jsonData, 
					            reader: {
					                type: 'json',
					                root: 'root',
					                method: 'POST',	
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            }
        		},{
					    flex : 1,
			            xtype: 'combo',
						name: 'f_riga_a',
						itemId : 'c_riga2',
						fieldLabel: 'a',
						labelAlign : 'right',
						labelWidth : 20,
						minChars: 2,	
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,		
						store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_righe',
					            actionMethods: {
						          read: 'POST'
						        },
					            extraParams: {
					            	listino : '',
					            	articolo : ''
					            },				            
						        doRequest: personalizza_extraParams_to_jsonData, 
					            reader: {
					                type: 'json',
					                root: 'root',
					                method: 'POST',	
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            }
        		}
                          ]}
	            ],
	            
				buttons: [	
						{
		        		  text: 'Conferma',
			              scale: 'large',
			              iconCls: 'icon-save-32',
			              handler: function() {
			                var form = this.up('form').getForm();
        	            	var form_values = form.getValues();
        	            	var loc_win = this.up('window');
			                if(form.isValid()){
			                		Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_duplica',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				form_values : form_values,
					        				list_selected_art : <?php echo acs_je($m_params->list_selected_art); ?>
										},							        
								        success : function(result, request){
								         	jsonData = Ext.decode(result.responseText);
        				            	    acs_show_msg_info('Duplica eseguita con successo');
        				            		loc_win.fireEvent('afterConfirm', loc_win);
    									},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
			               
			                }
			            }
		        		}
		       ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}