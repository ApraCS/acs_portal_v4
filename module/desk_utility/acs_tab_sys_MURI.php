<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'MURI',
    'title_grid' => 'MURI-Indici di ricarico ',
    'fields' => array(
        'TAID'   => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'     => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'tp_ind'   => array('label'	=> 'Tipo indice', 'short' =>'TI', 'tooltip' => 'Tipi indice',  'c_width' => 50, 'xtype' => 'combo_tipo', 'tab_std' => 'TTPIN'),
        'ricarico'   => array('label'	=> 'Ricarico', 'c_width' => 100, 'xtype' => 'number'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli indici di ricarico ',
   // 'tab_tipo' => 'PUSQ',
    'TAREST' => array(
        'ricarico' 	=> array(
            "start" => 0,
            "len"   => 6,
            "riempi_con" => "",
            "type" => 'decimal',
            "decimal_positions" => 3
        ),
        'tp_ind' 	=> array(
            "start" => 6,
            "len"   => 1,
            "riempi_con" => ""
        )
    )

);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

