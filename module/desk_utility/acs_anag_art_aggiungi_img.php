<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// UPLOAD FILE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_upload_file'){
	$ret = array();
	
	$c_art =	trim(strtr($_REQUEST['c_art'], array('\"' => '"')));
	$c_art = json_decode($c_art);
	
	$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
	$form_values = json_decode($form_values);
	
	//progressivo
	$artprg = sprintf("%09s", $s->next_num('ARTPRG'));
	

	if(strlen($_FILES['f_upload']['name'])>0){
	    
	    $save_in_dir = $cfg_mod_DeskArt['root_path'];      //"/SV2/ARTICOLI/";	    
	    if (!is_dir($save_in_dir)) mkdir($save_in_dir);
	    
	    $save_in_dir = $save_in_dir . trim($id_ditta_default); //Salvo in base alla ditta
	    if (!is_dir($save_in_dir)) mkdir($save_in_dir);
	    	   
	        
	    $file_src = $save_in_dir . "/" .$artprg."_".$_FILES['f_upload']['name'];

	        //unlink($file_src);
	        move_uploaded_file($_FILES['f_upload']['tmp_name'], $file_src);
	        
	        $ar_ins = array();
	        $ar_ins['TAUSGE'] 	= $auth->get_user();
	        $ar_ins['TADTGE'] 	= oggi_AS_date();
	        $ar_ins['TAORGE'] 	= oggi_AS_time();
	        $ar_ins['TADT'] 	= $id_ditta_default;
	        $ar_ins['TATAID'] 	= 'ARTAL';
	        $ar_ins['TAKEY1'] 	= $c_art;
	        $ar_ins['TAKEY2']   = $artprg;
	        $ar_ins['TAFG01'] 	= 'U';
	        $ar_ins['TASITI'] 	= $form_values->f_categoria;
	        $ar_ins['TAMAIL'] 	= $artprg."_".$_FILES['f_upload']['name'];
	        $ar_ins['TALOCA'] 	= $_FILES['f_upload']['name'];
	        $ar_ins['TADESC']   = $form_values->f_desc_al;
	        
	        
	        $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	        $stmt = db2_prepare($conn, $sql);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt, $ar_ins);
	        echo db2_stmt_errormsg($stmt);
	    
	}

	if(strlen($form_values->f_link)>0){
	   
	    $ar_ins = array();
	    $ar_ins['TAUSGE'] 	= $auth->get_user();
	    $ar_ins['TADTGE'] 	= oggi_AS_date();
	    $ar_ins['TAORGE'] 	= oggi_AS_time();
	    $ar_ins['TADT'] 	= $id_ditta_default;
	    $ar_ins['TATAID'] 	= 'ARTAL';
	    $ar_ins['TAKEY1'] 	= $c_art;
	    $ar_ins['TAKEY2']   = $artprg;
	    $ar_ins['TAFG01'] 	= 'L';
	    $ar_ins['TASITI'] 	= $form_values->f_categoria;
	    $ar_ins['TAMAIL'] 	= $form_values->f_link;
	    $ar_ins['TADESC']   = $form_values->f_desc_al;
	    
	    
	    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, $ar_ins);
	    echo db2_stmt_errormsg($stmt);
	    
	}
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;	
}

// ******************************************************************************************
// OPEN FORM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
	?>
 
 {"success":true, "items": [
        {
           
			xtype: 'form',
                        border: false,
                        bodyStyle: {
                            padding: '10px',
                            align: 'stretch'
                        },
                        items: [
                       { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    {
						name: 'f_categoria',
						xtype: 'combo',
						fieldLabel: 'Categoria',
						labelWidth: 130,
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',							
						emptyText: '- seleziona -',
				   		allowBlank: false,
				   		width: 300,								
					    //anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json($main_module->find_TA_std('ARTCA'), ''); ?>	
						    ]
						}
								
						 }, {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
									acs_show_win_std('Categorie articoli', 'acs_anag_art_gest_categorie.php?fn=open_tab', {}, 950, 500, null, 'icon-gear-16');
								} //handler function()
						 
						 }
						]}, {
			                    xtype: 'radiogroup',
			                    width: 150,
			                    fieldLabel: '',
			                    allowBlank: false,
			                    items: [{
			                            xtype: 'radio'
			                          , name: 'scelta_img'
			                          , inputValue: 'U' 
			                          , boxLabel: 'Upload'
			                          , checked: true
			                          , listeners: {
        								    change: function(radio, newValue, oldValue, eOpts) {
        								    	var m_form = this.up('form');
        								   		var link = m_form.down('#link');
        								    	if (newValue == false)
        			                    	    	link.enable();
        			                    	    else
        			                    	    	link.disable();	
        			                    	  
        								    }
        								}
			                          },{
			                            xtype: 'radio'
			                          , name: 'scelta_img'
			                          , inputValue: 'L' 
			                          , boxLabel: 'Link'
			                          , listeners: {
        								    change: function(radio, newValue, oldValue, eOpts) {
        								    	var m_form = this.up('form');
        								   		var upload = m_form.down('#upload');
        								    	if (newValue == false)
        			                    	    	upload.enable();
        			                    	    else
        			                    	    	upload.disable();	
        			                    	  
        								    }
        								}		                         
			                        }
			                    ]
			                },    
							{
                        	name: 'f_upload',
                            xtype: 'filefield',
                            itemId: 'upload',
                            labelWidth: 130,
                            fieldLabel: 'Seleziona immagine',
                            anchor: '100%',
                            disabled : false,
                           // allowBlank: false,
                            buttonText: 'Sfoglia',
                            
							}, {
                        	name: 'f_link',
                            xtype: 'textfield',
                            itemId: 'link',
                            labelWidth: 130,
                            anchor: '100%',
                            fieldLabel: 'Inserisci link',
                            disabled : true,
                            allowBlank: false,
                            
							}, {
                        	name: 'f_desc_al',
                            xtype: 'textfield',
                            labelWidth: 130,
                            anchor: '100%',
                            fieldLabel: 'Descrizione allegato',
                            allowBlank: false                           
							}
						],
							
    					buttons: [{
    					 text: 'Abbina da altro articolo',
    					 scale: 'medium',
    					 handler: function () {
    					 	var m_win = this.up('window');
    					 	m_win.close();
            			    acs_show_win_std('Abbina', '../desk_utility/acs_anag_art_abbina_img.php?fn=open', {
            			    		c_art: '<?php echo $m_params->c_art; ?>',
            			    		from_grid: '<?php echo $m_params->from_grid; ?>'} , 900, 600, {
         		            		afterUpload: function(from_win){
         		            			//ToDoooo
         		            			//m_grid.store.load();
         		            			//from_win.close()
             		            	}
         		            	}, 'icon-search-16');    					 	
    					 }
    					}, {xtype: 'tbfill'}, 
    					{
                                text: 'Crea',
                                scale: 'medium',
                                handler: function () {
                               
                                    var m_win = this.up('window');
                                    var form = this.up('form').getForm();
                	            	var form_values = form.getValues();
                	            	
                	            	if (!form.isValid()) return false;
         
                                    form.submit({
                                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upload_file',
                                        //waitMsg: 'Uploading your file(s)...',
                                        method: 'POST',
                                        params: {
                                            form_values : Ext.encode(form_values),
                                            c_art: Ext.encode (<?php echo acs_je( $m_params->c_art) ?>)
                                        },
                                        success: function (f, a) {
                                            var data = a.result;
                                            m_win.fireEvent('afterUpload', m_win);
                                        },
                                        failure: function (f, a) {
                                            Ext.Msg.alert('Failure', a.result.msg || 'server error', function () {
                                                //win.close();
                                            });
                                        }
                                    });     
                                }
                        	}]
          
			}
			]}


	
	<?php 
}