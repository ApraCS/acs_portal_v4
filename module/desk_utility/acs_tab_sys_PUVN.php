<?php

require_once("../../config.inc.php");
$desk_art = new DeskArt();
$m_params = acs_m_params_json_decode();
$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'PUVN',
    'title_grid' => 'PUVN-Varianti',
    'fields' => array(
        'TAID'   => array('label'	=> 'taid', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 40, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'RGB'   => array('label'	=> 'RGB', 'c_width' => 35, 'only_view' => 'C'),
        'TACINT'   => array('label'	=> 'Interfaccia', 'only_view' => 'F',  'maxLength' => 10),
        'TADESC' => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'TACOR2'   => array('label'	=> 'tacor2', 'hidden' => 'true'),
        'ref_anag' => array('label'	=> 'Referenza anagrafica', 'only_view' => 'F', 'maxLength' => 15),
        'TAREST'    => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TIPOLOGIE' => array('label'	=> 'Tipologie', 'only_view' => 'C'),
        'tipo1'    => array('label'	=> 'Tipologia 1', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo2'   => array('label'	=> 'Tipologia 2', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo3'   => array('label'	=> 'Tipologia 3', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo4'   => array('label'	=> 'Tipologia 4', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo5'   => array('label'	=> 'Tipologia 5', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo6'   => array('label'	=> 'Tipologia 6', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo7'   => array('label'	=> 'Tipologia 7', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo8'   => array('label'	=> 'Tipologia 8', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo9'   => array('label'	=> 'Tipologia 9', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'tipo10'   => array('label'	=> 'Tipologia 10', 'xtype' => 'combo_tipo', 'only_view' => 'F'),
        'esau'   => array('label'	=> 'Ciclo di vita','xtype' => 'combo_tipo', 'only_view' => 'F', 'tab_std' => 'AR012'),
        'var_or'   => array('label'	=> 'Variante origine', 'hidden' => 'true'),
        'var_dp'   => array('label'	=> 'Variante duplica', 'hidden' => 'true'),
        'stringa_cod'   => array('label'	=> 'Stringa codice DB','short' =>'S.DB', 'c_width' => 50, 'maxLength' => 9),
        'tag' => array('label'	=> 'Tag', 'c_width' => 50),
        'agente'   => array('label'	=> 'Agente', 'hidden' => 'true'),
        'royal'   => array('label'	=> '% Royalties', 'hidden' => 'true'),
        'rgb'   => array('label'	=> 'RGB',  'only_view' => 'F', 'fieldcontainer' => 'Y'),
        'RRN'    => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'   => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Tabella varianti',
    'tab_tipo' => 'PUTI',
    'title_tab' => 'Altro',
    'TAREST' => array(
        'stringa_cod' 	=> array(
            "start" => 0,
            "len"   => 9,
            "riempi_con" => ""
        ),
        'tipo1' 	=> array(
            "start" => 9,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tipo2' 	=> array(
            "start" => 12,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tipo3' 	=> array(
            "start" => 15,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tipo4' 	=> array(
            "start" => 18,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tipo5' 	=> array(
            "start" => 21,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'tipo6' 	=> array(
            "start" => 24,
            "len"   => 3,
            "riempi_con" => ""
        ),'tipo7' 	=> array(
            "start" => 27,
            "len"   => 3,
            "riempi_con" => ""
        ),'tipo8' 	=> array(
            "start" => 30,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'tipo9' 	=> array(
            "start" => 33,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'tipo10' 	=> array(
            "start" => 36,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'ref_anag'	=> array(
            "start" => 42,
            "len"   => 15,
            "riempi_con" => ""
        )
        ,'rgb'	=> array(
            "start" => 84,
            "len"   => 9,
            "riempi_con" => ""
        )
        ,'red'	=> array(
            "start" => 84,
            "len"   => 3,
            "riempi_con" => "0"
        )
        ,'green'	=> array(
            "start" => 87,
            "len"   => 3,
            "riempi_con" => "0"
        )
        ,'blue'	=> array(
            "start" => 90,
            "len"   => 3,
            "riempi_con" => "0"
        )
        ,'tag'  => array(
            "start" => 97,
            "len"   => 5,
            "riempi_con" => ""
        )
        ,'royal' 	=> array(
            "start" => 104,
            "len"   => 5,
            "riempi_con" => ""
        )
        ,'agente' 	=> array(
            "start" => 109,
            "len"   => 3,
            "riempi_con" => ""
        ) 
        ,'esau' 	=> array(
            "start" => 127,
            "len"   => 1,
            "riempi_con" => ""
        )  
        , 'var_or' 	=> array(
            "start" => 138,
            "len"   => 3,
            "riempi_con" => ""
        )
        , 'var_dp' 	=> array(
            "start" => 141,
            "len"   => 3,
            "riempi_con" => ""
        )
        
    )

    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_utility/acs_gestione_tabelle.php';

