<?php

require_once "../../config.inc.php";
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$s = new Spedizioni(array('no_verify' => 'Y'));

function out_IE($v){
    if (trim($v) == 'I')   return 'Incl.se:';
    if (trim($v) == 'E')   return 'Escl.se:';
    return $v;
}

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          		{
						xtype: 'checkboxgroup',
						labelWidth : 180,
						fieldLabel: 'Elenco valori ammessi',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_va' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					},{
						xtype: 'checkboxgroup',
						labelWidth : 180,
						fieldLabel: 'Valori ammessi indiretti',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_va_in' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					},{
						xtype: 'checkboxgroup',
						labelWidth : 180,
						fieldLabel: 'Elenco condizioni',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_cond' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					},  {
        			xtype : 'combo',
        		 	name: 'f_variabile',
		 			fieldLabel: 'Variabile',
		 			labelWidth : 50,
    		 		forceSelection: true,  							
					displayField: 'text',
		    		valueField: 'id',						
        			emptyText: '- seleziona -',
        		   	queryMode: 'local',
             		minChars: 1,	
        			store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}],
            		    data: [								    
    		     			<?php echo acs_ar_to_select_json(find_TA_sys('PUVR', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
            		    ]
        			}, listeners: { 
        			 		beforequery: function (record) {
        	         		record.query = new RegExp(record.query, 'i');
        	         		record.forceAll = true;
    				 }
         			},	
        		 	
        		  } 
	            ],
	            
				buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	mode : '<?php echo $m_params->mode; ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	
exit;	
}

if ($_REQUEST['fn'] == 'open_report'){

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   tr.liv_compo td{font-weight: normal; background-color: #eeeeee;}
   table.int1 td.no_border {border: 0px solid white;}
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$mode =	$_REQUEST['mode'];

if(strlen($form_values->f_variabile) > 0)
    $sql_where = " AND MD.MDVARI = '{$form_values->f_variabile}'";
else
    $sql_where = " AND MD.MDVARI <> ''";

$sql = "SELECT MD.*, TA.TADESC AS D_MODE, MD_VA.MDVAL1 AS V_ASS, MD_VA.MDTPV1 AS T_ASS, MD_VA.MDDTGE AS I_DATA, MD.MDUSGE AS I_US
        FROM {$cfg_mod_DeskUtility['file_config']} MD
        LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_config']} MD_VA
        ON MD_VA.MDDT=MD.MDDT AND MD_VA.MDMODE=MD.MDMODE AND MD_VA.MDSEQ3 = MD.MDSEQ3 AND  MD_VA.MDSEQ4 = MD.MDSEQ4 AND MD_VA.MDVARI = MD.MDVARI AND MD_VA.MDSEQC= '999'
        LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA
        ON TA.TADT = MD.MDDT AND TA.TANR = MD.MDMODE AND TA.TAID = 'PUMO'
        WHERE MD.MDDT = '{$id_ditta_default}' AND MD.MDMODE = '{$mode}' {$sql_where}
        ORDER BY MD.MDSEQ3, MD.MDSEQ4, MD.MDVARI, MD.MDSEQC ";

    
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        
        $sql_mo = "SELECT RRN(MO) AS RRN, MO.*
                   FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
                   WHERE MODT = '{$id_ditta_default}' AND MOMODE = ?
                   AND MOSEQU= ? AND MOVARI = ?
                   ORDER BY MOSEQS, MOVALO";
        
        $stmt_mo = db2_prepare($conn, $sql_mo);
        echo db2_stmt_errormsg();
    
        $sql_tag = "SELECT TAREST
                   FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
                   WHERE TADT = '{$id_ditta_default}' AND TAID = 'PUVN'
                   AND TANR = ? AND TACOR2 = ? AND TALINV <> ''";
        
        $stmt_tag = db2_prepare($conn, $sql_tag);
        echo db2_stmt_errormsg();
    
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr = $row;
        
        $val_dom = find_TA_sys('PUVR', trim($row['MDVARI']));
        $var = "[".trim($row['MDVARI'])."]";
        $nr['var']    = $var;
        $nr['denominazione']  = $val_dom[0]['text'];
        $nr['d_mode'] = $row['D_MODE'];
       // if($row['MDSEQC'] == '999'){
        $nr['data'] = trim($row['I_DATA']);
        $nr['user'] = trim($row['I_US']);
        $nr['valore'] = trim($row['V_ASS']); //$nr['valore'] = trim($row['MDASSU']);
        $nr['t_val'] = trim($row['T_ASS']); //$nr['t_val'] = trim($row['MDTPAS']);
        
        if(trim($row['T_ASS']) == '?'){
            $val_risp = find_TA_sys('PUVR', trim($row['V_ASS']));
            $nr['d_valore'] = $val_risp[0]['text'];
        }else{
            $val_risp = find_TA_sys('PUVN', trim($row['V_ASS']), null, trim($row['MDVARI']), null, null, 0, '', 'N', 'Y');
            $nr['d_valore'] = $val_risp[0]['text'];
        }
            
       // }
       
        $result = db2_execute($stmt_tag, array(trim($row['V_ASS']), trim($row['MDVARI'])));
        $row_tag = db2_fetch_assoc($stmt_tag);
        $nr['tag'] = substr($row_tag['TAREST'], 97, 5);
        
            
        $nr['valore_c'] = trim($row['MDASSU']);
        $nr['t_val_c'] = trim($row['MDTPAS']); 
        
        if(trim($row['MDTPAS']) == '?'){
            $val_risp = find_TA_sys('PUVR', trim($row['MDASSU']));
            $nr['d_valore_c'] = $val_risp[0]['text'];
        }else{
            $val_risp = find_TA_sys('PUVN', trim($row['MDASSU']), null, trim($row['MDVARI']), null, null, 0, '', 'N', 'Y');
            $nr['d_valore_c'] = $val_risp[0]['text'];
        }
        
        
        //valori ammessi
        $seq = trim($row['MDSEQ3']).trim($row['MDSEQ4']);
        $valori_ammessi = "";
        $result = db2_execute($stmt_mo, array($mode, $seq, trim($row['MDVARI'])));
        while($row_mo = db2_fetch_assoc($stmt_mo)){
            
            $val_risp = find_TA_sys('PUVN', trim($row_mo['MOVALO']), null, trim($row_mo['MOVARI']), null, null, 0, '', 'N', 'Y');
            if($val_risp[0]['sosp'] == true){
                $text = str_replace('(*S*)', '', $val_risp[0]['text']);
            }else{
                $text = $val_risp[0]['text'];
            }
            
            $val = "[".trim($row_mo['MOVALO'])."] ".$text;
            $valori_ammessi  .= $val."<br>";
            
            
        }
        
        $nr['val_am']    = $valori_ammessi;
        $ar[] = $nr;
    }

echo "<div id='my_content'>";
echo "<div class=header_page>";
echo "<H2>Distinta configuratore varianti</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";


echo "
  <tr class='liv_data'>
    <th rowspan=2 >Sequenza</th>
	<th rowspan=2>Cod.</th>
	<th rowspan=2>Descrizione variabile</th>

    <th rowspan=2>Valore assunto/Valori ammessi</th> 
    <th rowspan=2>Tag</th>   
    <th colspan = 7 class='center'> Condizioni </th>
    <th colspan = 2 class='center'> Immissione </th>
    </tr> 
    <tr class='liv_data'>

              
          <th>Cond.</th>
          <th>I/E</th>
          <th>Variabile</th>
          <th>N</th>
          <th>Test</th>
          <th>Valore</th>          
          <th>O/S</th>

          <th>Data</th>
          <th>Utente</th>

   </tr>";
echo "</tr>";  

$ultimo_config='';
$ultimo_liv_compo = '';
foreach ($ar as $kar => $r){
    
    $seqc = "";
    if($r['MDSEQC'] == '999')  $seqc = 'A';
    else  $seqc = 'B';
    
    $liv_compo = implode("_", array($r['MDMODE'], $r['MDSEQ3'], $r['MDSEQ4'], $r['MDVARI'], $seqc));  // 
    $chiave_va = array($r['MDMODE'], $r['MDSEQ3'], $r['MDSEQ4'], $r['MDVARI']);
    //CONFIGURATORE
    if(trim($r['MDMODE']) != $ultimo_config){
        
        $c_mode = "<b>".trim($r['MDMODE'])."</b><br>";
        $d_mode = "<b>".trim($r['d_mode'])."</b><br>";
      
        if($ultimo_art != '')
            echo "<tr><td colspan = 14 class=no_border>&nbsp;</td></tr>";
        echo "<tr class = 'liv1'>
           <td >&nbsp;</td>
           <td valign = top style = 'font-size : 13px;'>".$c_mode."</td>
           <td valign = top style = 'font-size : 13px;'>".$d_mode."</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td colspan=3>&nbsp;</td>
           <td valign = top colspan=4>&nbsp;</td>
           <td valign = top>".print_date($r['MDDTGE'])."</td>
           <td valign = top>".$r['MDUSGE']."</td>
          </tr>";  
        
    }
   
    $chiave = array($r['MDMODE'], $r['MDSEQ3'], $r['MDSEQ4'], $r['MDVARI']);
    
    if($chiave != $chiave_ultima_condizione_stampata){
    
        if ($liv_compo != $ultimo_liv_compo){
         
    	   echo "
              <TR>
                <td valign = top>".$r['MDSEQ3']." - ".$r['MDSEQ4']."</td>
    	        <td valign = top><b>".$r['var']."</b></td>
       			<td valign = top><b>".$r['denominazione']."</b></td>";
               if(trim($r['valore']) != '')
                   echo "<td><b>[".$r['valore']."] ".$r['d_valore']. "</td>";
               else
                   echo "<td>&nbsp;</td>";
               echo "<td>&nbsp;</td>";
               echo "<td valign = top colspan=7>".$r['MDNOTE']."</td>
                <td valign = top>".print_date($r['data'])."</td>
                <td valign = top>".$r['user']."</td>
              </TR>
             ";
        }
    	

    }
    
  	
	$ar_cond = array();
	
	
//	if ($liv_compo != $ultimo_liv_compo){
	    for($i = 1; $i <= 12 ; $i++){
	        
	        $cond = "";
	        $variabile = "";
	        $var_num = "";
	        $variante = "";
	        $tipologia = "";
	        $tipologia2 = "";
	        $obb_scelta = "";
	        
	        //FILE MD0
	        if (strlen(trim($r["MDVAR{$i}"])) > 0){
	            $val_variabile = find_TA_sys('PUVR', trim($r["MDVAR{$i}"]));
	            $variabile .= "[".trim($r["MDVAR{$i}"])."] ".$val_variabile[0]['text']. "<br>";
	            
	            	            
	            if(trim($r["MDTPV{$i}"]) != '')
	                $tipologia .= trim($r["MDTPV{$i}"]). "<br>";
	            
	            if(trim($r["MDTPV{$i}"]) == 'T'){
	                $val_variante = find_TA_sys('PUTI', trim($r["MDVAL{$i}"]));
	                if(trim($r["MDVAL{$i}"]) != '')
	                    $variante .= "[".trim($r["MDVAL{$i}"])."] ".$val_variante[0]['text']. "<br>";
	            } else {
	                // =
	                $val_variante = find_TA_sys('PUVN', trim($r["MDVAL{$i}"]), null, trim($r["MDVAR{$i}"]), null, null, 0, '', 'N', 'Y');
	                if(trim($r["MDVAL{$i}"]) != '')
	                    $variante .= "[".trim($r["MDVAL{$i}"])."] ".$val_variante[0]['text']. "<br>";
	            }
	                
	                


                
                if(trim($r["DBSWS{$i}"]) != '')
                    $tipologia2 .= trim($r["DBSWS{$i}"]). "<br>";
                
                if(trim($r["MDOPV{$i}"]) != '')
                    $obb_scelta .= trim($r["MDOPV{$i}"]). "<br>";
                
                $ar_cond[] = array(
                    'cond' => $r['MDSEQC'],
                    'ie'        => out_IE($r['MDSWIE']),
                    'variabile' => $variabile,
                    'var_num'   => '',
                    'tipologia' => $tipologia,
                    'variante'  => $variante,
                    'obb_sce'   => $obb_scelta,
                    'data'   =>   $r['MDDTGE'],
                    'utente'   => $r['MDUSGE']
                );
	        }
	        
	                       

	    }
	    
	   
	//}
	

  
    $n_cond = count($ar_cond);
    
   
    $old_cond = '';
    $old_ie   = '';
    //if($form_values->f_cond == 'Y'){
                
        foreach($ar_cond as $k_cond => $cond){
            
            $chiave_ultima_condizione_stampata = array($r['MDMODE'], $r['MDSEQ3'], $r['MDSEQ4'], $r['MDVARI']);
            
            if($form_values->f_cond == 'Y'){
            echo "<tr>";
            
            //if ($k_cond == 0)
            echo "<td colspan=3 class=no_border>&nbsp;</td>";
            
           // echo "<td>".$r['MDASSU']."</td>";
            if ($old_cond != $cond['cond']){
                if(trim($r['valore_c']) != '')
                    echo "<td>[".$r['valore_c']."] ".$r['d_valore_c']."</td>";
                else 
                    echo "<td>&nbsp;</td>";
                
                echo "<td>&nbsp;</td>";
                echo "<td valign = top>".$cond['cond']."</td>";
            }else{
                echo "<td>&nbsp;</td>";
                echo "<td>&nbsp;</td>";
                echo "<td>&nbsp;</td>";
            }
            
            if ($old_ie != $cond['ie'])
                echo "<td valign = top>".$cond['ie']."</td>";
            else
                echo "<td valign = top>&nbsp;</td>";
                
            echo   "
                <td valign = top>".$cond['variabile']."</td>
                <td valign = top>".$cond['var_num']."</td>
    	        <td valign = top>".$cond['tipologia']."</td>
                <td valign = top>".$cond['variante']."</td>
                <td valign = top>".$cond['obb_sce']."</td>";
            if ($old_cond != $cond['cond']){
             echo "<td valign = top>".print_date($cond['data'])."</td>
                    <td valign = top>".$cond['utente']."</td>";
            }else{
                echo "<td>&nbsp;</td>";
                echo "<td>&nbsp;</td>";
                
            }
             echo "</tr>";
             
            $old_cond = $cond['cond'];
            $old_ie = $cond['ie'];
            
            
        }
        }
    
    
    
    if($r['MDSEQC'] == '999' && $liv_compo != $ultimo_liv_compo){
        if($form_values->f_va == 'Y'){
                if(trim($r['val_am']) != ''){
                    echo "<tr>";
                    echo "<td colspan=3 class=no_border>&nbsp;</td>";
                    echo "<td>".$r['val_am']."</td>";
                    echo "<td colspan=9 class=no_border>&nbsp;</td>";
                    echo "</tr>";
                }
                
            }
     }
    
    
   
    
    $ultimo_config =  trim($r['MDMODE']);
    $ultimo_liv_compo = $liv_compo;
    $ultima_chiave_va = $chiave_va;
   
 }



?>
</div>
</body>
</html>	


<?php 

exit;

}




