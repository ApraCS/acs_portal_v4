<?php

require_once("../../config.inc.php");
require_once("acs_anag_artd_include.php");
require_once("acs_panel_anag_art_include.php");

$main_module = new DeskUtility();
$deskArt = new DeskArt();
$m_params = acs_m_params_json_decode();

function sum_values(&$ar, $r){
    
    $ar['c_art'] += $r['ARTICOLI'];
    
}
if ($_REQUEST['fn'] == 'exe_save_nota'){
    
    $ar_ins = array();
    $nota = $m_params->nota;
    $flag = 'A';
    
    $ar_ins['RLSWST'] 	= acs_toDb(mb_substr($nota, 0, 1));    //1
    $ar_ins['RLREST1'] 	= acs_toDb(mb_substr($nota, 1, 15));   //15
    $ar_ins['RLFIL1'] 	= acs_toDb(mb_substr($nota, 16, 64));  //64
    $ar_ins['RLDTUM'] 	= oggi_AS_date();
    $ar_ins['RLUSUM'] 	= $auth->get_user();
    $ar_ins['RLFEXP']   = $flag;
    
    if(trim($nota) == '' ){
        
        if(trim($m_params->rec->rrn_appunti) != ''){
             $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_note_anag']} RL
             WHERE RRN(RL) = '{$m_params->rec->rrn_appunti}'";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
        }
        
        
    }else{
        
        if(trim($m_params->rec->rrn_appunti) == ''){
            
            $ar_ins['RLDTGE'] 	= oggi_AS_date();
            $ar_ins['RLUSGE'] 	= $auth->get_user();
            $ar_ins['RLDT'] 	= $id_ditta_default;
            $ar_ins['RLRIFE2'] 	= $m_params->blocco;
            $ar_ins['RLRIFE1'] 	= $m_params->rec->codice;
            $ar_ins['RLTPNO'] 	= 'AR';
            $ar_ins['RLRIGA']   = 1;
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_note_anag']}(" . create_name_field_by_ar($ar_ins) . ")
			                VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        }else{
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_note_anag']} RL
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(RL) = '{$m_params->rec->rrn_appunti}'";
            
        }
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
        
    }

    /*$ha_commenti = $main_module->has_commento_articolo($m_params->rec->codice, $m_params->blocco);
    if ($ha_commenti == FALSE)
        $img_com_name = "iconCommGray";
    else
        $img_com_name = "iconCommYellow";*/
            
    $ret = array();
    $ret['success'] = true;
   // $ret['icon'] = $img_com_name;
   // $ret['note'] = $ha_commenti;
    echo acs_je($ret);
    exit;
    
}
if ($_REQUEST['fn'] == 'exe_cancella'){
        
    $sh = new SpedHistory($deskArt);
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DEL_ANAG_ART',
            "vals" => array(
                "RICDOLD" 	=> $m_params->c_art,
                "RIDT"  	=> $id_ditta_default
            ),
        )
        );
    
    
    $ret = array();
    $ret['success'] = true;
    $ret['ret_RI'] = $ret_RI;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'exe_sos_att'){
    
    $ar_new = array();

    foreach($m_params->list_rows as $v){
        
        if($v->flag == "")
            $new_value = "S";
        else
            $new_value = "";
                
        $ar_upd = array();
        $ar_upd['ARUSUM'] = $auth->get_user();
        $ar_upd['ARDTUM'] = oggi_AS_date();
        $ar_upd['ARSOSP']   = $new_value;
        if($new_value == "")
            $ar_upd['ARTP']   = $new_value;
                    
        $sql = "UPDATE {$cfg_mod_DeskUtility['file_anag_art']} AR
        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
        WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$v->c_art}'";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        
        $ar_new['codice'] = $v->c_art;
        $ar_new['flag'] = $new_value;
        $ar[] = $ar_new;
    }
       
        
    $ret = array();
    $ret['new_value'] = $ar;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}


if ($_REQUEST['fn'] == 'exe_aggiorna_art'){
    ini_set('max_execution_time', 6000);
    $ret = array();
    $m_params = acs_m_params_json_decode();
    
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
    $ar_upd['ARUSUM'] 	= $auth->get_user();
    $ar_upd['ARDTUM'] 	= oggi_AS_date();
    
    if(isset($form_values->ARTPAR) && $form_values->ARTPAR != '')
        $ar_upd['ARTPAR'] 	= $form_values->ARTPAR;
    if(isset($form_values->ARCLME) && $form_values->ARCLME != '')
        $ar_upd['ARCLME'] 	= $form_values->ARCLME;
    if(isset($form_values->ARGRME) && $form_values->ARGRME != '')
        $ar_upd['ARGRME'] 	= $form_values->ARGRME;
    if(isset($form_values->ARSGME) && $form_values->ARSGME != '')
        $ar_upd['ARSGME'] 	= $form_values->ARSGME;
    if(isset($form_values->ARTAB1) && $form_values->ARTAB1 != '')
        $ar_upd['ARTAB1'] 	= $form_values->ARTAB1;
    if(isset($form_values->ARDART) && $form_values->ARDART != '')
        $ar_upd['ARDART'] 	= $form_values->ARDART;
                            
                            
    foreach($m_params->list_art as $k=>$v){
        
        if(isset($form_values->ARFOR1) && $form_values->ARFOR1 != ''){
            if($form_values->ARFOR1 == $v->D_FOR)
                $ar_upd['ARFOR1'] = sql_f($v->ARFOR1);
                else
                    $ar_upd['ARFOR1'] = sql_f($form_values->ARFOR1);
        }
        
        
        $sql = "UPDATE {$cfg_mod_DeskUtility['file_anag_art']} AR
        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
        WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$v->codice}'";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg($stmt);
        
        foreach($form_values as $k1=> $v1){
            
            $old_value = $v->$k1;//$m_params->row_old->$k;
            $new_value = $v1;
            $campo = $k1;
            $codice = $v->codice;
            
            $ar_ins = array();
            $ar_ins['JMDATE'] 	= oggi_AS_date();
            $ar_ins['JMTIME'] 	= oggi_AS_time();
            $ar_ins['JMUSER'] 	= $auth->get_user();
            $ar_ins['JMAZIO'] 	= "$libreria_predefinita/XX0S2AR0/$codice,$campo,$old_value,$new_value";
            
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_cronologia']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
            
        }
        
        if(isset($form_values->f_valore) && strlen($form_values->f_valore) > 0){
            $sql_af = "SELECT COUNT(*) AS C_ROW 
                       FROM {$cfg_mod_DeskArt['file_proprieta']} AF
                       WHERE AFDT = '{$id_ditta_default}' AND AFART = '{$v->codice}'
                       AND AFTDAT = '{$form_values->f_proprieta}'
                       /*AND AFVALO = '{$form_values->f_valore}*/";
            
            $stmt_af = db2_prepare($conn, $sql_af);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_af);
            echo db2_stmt_errormsg();
            $row_af = db2_fetch_assoc($stmt_af);
            
            $ar_ins = array();
            $ar_ins['AFVALO'] = $form_values->f_valore;
            
            
            if($row_af['C_ROW'] > 0){
                $sql_ins = "UPDATE {$cfg_mod_DeskArt['file_proprieta']} PROP
                           SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                           WHERE AFDT = '{$id_ditta_default}' AND AFART = '{$v->codice}'
                           AND AFTDAT = '{$form_values->f_proprieta}'";
            }else{
                
                $ar_ins['AFUSGE'] 	= trim($auth->get_user());
                $ar_ins['AFDTGE']   = oggi_AS_date();
                $ar_ins['AFDT']     = $id_ditta_default;
                $ar_ins['AFART']    = $v->codice;
                $ar_ins['AFTDAT']   = $form_values->f_proprieta;   
                
                $sql_ins = "INSERT INTO {$cfg_mod_DeskArt['file_proprieta']}
                        (" . create_name_field_by_ar($ar_ins) . ")
                        VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                
            }
            
            $stmt_ins = db2_prepare($conn, $sql_ins);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_ins, $ar_ins);
            echo db2_stmt_errormsg();
            
            
        }
        
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
    
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
        $ar_users_json = acs_je($user_to);
        
        ?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
        	itemId: 'f_form',
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            get_values_for_submit: function(){
            	//bug lentezza su invio con tutti i parametri (anche quelli a blank)
			    // Invio solo i parametri con un valore                       
			    // ToDo: la delete sotto rimuove anche gli array vuoti. E' giusto?
			   
               var  me = this, //form
               		submit_form_values = me.getValues();                      
               for (var rf in submit_form_values) {
              	 if (submit_form_values[rf] == ''){
              	 	//a meno che non sia un combo con il valore blank realmente selezionato
              	 	var m_mf = me.getForm().findField(rf);
              	 	
              	 	if(m_mf.xtype == 'combo') {
              	 		if (m_mf.value.length == 0)
              	 			delete submit_form_values[rf];
              	 	} else {              	 	
              			delete submit_form_values[rf];
              		}
              	 }
               }
             return submit_form_values;      
            },
            
            
            items: [ 
            
                { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						flex:0.5,
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data inser. dal'
						   , margin: '0 8 0 0'
						   , labelWidth :120
						   , name: 'f_data_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						},
						
						<?php for ($i = 1; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', width : 20, fieldStyle:'font-size:10px;'},
						<?php }?>	
						
						//sfondo
						{xtype: 'displayfield', value : 'Codice articolo :', margin : '0 0 0 10' },
						   
						  
						 ]
					}, 
					
							
						{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch',
								    frame: true},	
						defaults:{xtype: 'textfield'},					
						items: [
						{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , margin: '0 5 0 0'
						   , labelWidth :120
						   , name: 'f_data_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, 
						<?php for ($i = 1; $i<=15; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_<?php echo $i; ?>', 
						 // fieldLabel : '<?php echo $i?>',
						 // labelAlign: 'top', 
						  width: 20,
						  maxLength: 1,
						  enforceMaxLength : true,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
                              
                              field.setValue(field.getValue().toUpperCase());
      							
    					  }
 						 }},
						
						<?php }?>
						
						{ fieldLabel: '', 
						name: 'f_cod_art',
						margin : '0 0 0 10',
						anchor: '-5',
						flex : 0.1,
						  listeners: {
    						'change': function(field){
    					
                              field.setValue(field.getValue().toUpperCase());
      							
    					  }
 						 }
						//plugins: [new Ux.InputTextMask('AAAAAAAAAAAAAAA', false)]
						 },
				
						]
						
						
						},
						
						{						
							flex: 1,
			            	xtype: 'textfield',
			            	margin : '0 0 5 0',
							name: 'f_desc_art',
							fieldLabel: 'Descrizione articolo',
							labelWidth :120, 
							anchor: '-1'						
						},
						{ 
						xtype: 'fieldcontainer',
						
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						
					  	{
        		xtype: 'tabpanel',
        		height : 225,
        		flex:0.6,
        		items: [
        				  { 	xtype: 'grid',
				  		grid_save_in_F8: 'liv_raggruppamenti',
				  		
				  		get_f8_data: function(){
				  		
				  			rows = this.getStore().data.items;
                            list_rows = [];
                            
                              for (var i=0; i<rows.length; i++) {
                             	 if(rows[i].get('disabled') != 'Y'){
				            		list_rows.push(rows[i].get('code'));
				            	   }
				            	}
				          
				  			return list_rows;
				  		},
				  		
				  		set_f8_data: function(values){
				  			console.log('set_f8_data called');
				  			if(typeof values != 'undefined' && values.length > 0){
				  					  				 
				  				this.store.each(function(rec){
				  				      if (values.includes(rec.get('code'))) {
									    rec.set('disabled', 'N');
									    rec.set('seq_ord', values.indexOf(rec.get('code')));
									 }else{
									    rec.set('seq_ord', 9);
									 }
								});
							
								this.getStore().sort('seq_ord','ASC');
				  			 
				  			}
				  			
				  		},
				  		
				  		
		 			    title : 'Raggruppamento', 
        				useArrows: true,
        		        rootVisible: false,
        		        loadMask: true,
        		        multiSelect: true,
        		        store: {
        						xtype: 'store',
        						autoLoad:true,
        					 data : [
        							{title: 'Tipo/Parte', disabled : 'N', code : 'ARTPAR'},
							        {title: 'Classe', disabled : 'Y', code : 'ARCLME'},
							        {title: 'Gruppo', disabled : 'Y', code : 'ARGRME'},
							        {title: 'Sottogruppo', disabled : 'Y', code : 'ARSGME'},
							        {title: 'Gruppo pianificazione', disabled : 'Y', code : 'ARTAB1'},
							        {title: 'Fornitore', disabled : 'Y', code : 'ARFOR1'},
							        {title: 'Configuratore', disabled : 'Y', code : 'ARMODE'},
							        {title: 'Tipo riordino', disabled : 'Y', code : 'ARSWTR'}

							        ],
				   	fields: ['seq_ord', 'title', 'code', 'disabled']
						

				}, //store
		
				columns: [	
				 
					{
			                hidden : true,
			                dataIndex: 'disabled', 
			                flex: 1
			             },
		    		{
			                header   : 'Raggruppamento elenco articoli',
			                dataIndex: 'title', 
			                flex: 1
			             },
		    		{
			               text: 'Disabilita',
			               width: 100,
			               renderer: function(value, p, record){
			               
			               if (record.get('disabled') == 'N') return '<img src=<?php echo img_path("icone/16x16/button_yellow_play.png") ?>  width=15>';
			               if (record.get('disabled') == 'Y') return '<img src=<?php echo img_path("icone/16x16/button_red_play.png") ?>  width=15>';
			               
			               }   
			                
		             }
		    		
		    		
				]
				
				, listeners : {
				
				 itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  
						     if (rec.get('disabled') == 'N')
								rec.set('disabled', 'Y');
						   else
							    rec.set('disabled', 'N' );

						  }
					  }	 
				
				
				
				}
			
				,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
						       if (record.get('disabled')=='Y')
						           return ' segnala_riga_disabled';
						       			           		
						           return '';																
					         }, plugins: {
					                ptype: 'gridviewdragdrop',
					                dragGroup: 'sped_grid',
					                dropGroup: 'sped_grid'
				            }   
					    },
			
											    
				    		
                    }, //grid

		  
					{
				xtype: 'panel',
				title: 'Colonne', 
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				width: 165,
												
				items: [
							
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Attivit&agrave',
                            name: 'f_att',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Allegati',
                            name: 'f_allegati',	
                            labelWidth: 150,
                            checked : true,
                            xtype: 'checkboxfield',
                            inputValue: 'Y'         
                        }
						   
						
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Listini vendita',
                            name: 'f_list_ven',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Listini acquisto',
                            name: 'f_list_acq',	
                            labelWidth: 150,
                            checked : true,
                            xtype: 'checkboxfield',
                            inputValue: 'Y'         
                        }
						   
						
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Distinta componenti',
                            name: 'f_dist',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Elenco padri',
                            name: 'f_elenco',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            checked : true,
                            inputValue: 'Y'         
                        }
						   
						
						]}, 	{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Cicli/lavorazioni',
                            name: 'f_cicli',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Configuratore',
                            name: 'f_conf',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            checked : true,
                            inputValue: 'Y'         
                        }
						   
						
						]}, {
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Traduzione',
                            name: 'f_trad',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true,
                            margin: '0 100 0 5',         
                        },
						 {
                            fieldLabel: 'Varianti anagrafiche',
                            name: 'f_var',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            checked : true,
                            inputValue: 'Y'         
                        }
						   
						
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Regole abbinamento',
                            name: 'f_abb',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            checked : true, margin: '0 100 0 5',           
                        },
                        {
                            fieldLabel: 'Riferimento tecnico',
                            name: 'f_rif_tec',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y',            
                        }
                        
						   
						
						]},
						
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
                            fieldLabel: 'Codice alternativo',
                            name: 'f_cod_alt',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y', 
                            margin: '0 100 0 5'        
                        }, {
                            fieldLabel: 'Codice riferimento',
                            name: 'f_cod_rife',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y'         
                        }
                        
						   
						
						]},
						{
						 xtype: 'fieldcontainer',
						 layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
                            fieldLabel: 'Tipo riordino',
                            name: 'f_riordino',	
                            labelWidth: 150,
                            xtype: 'checkboxfield',
                            inputValue: 'Y',
                            margin: '0 0 0 5'             
                        },
						{xtype : 'component', flex : 0.5}
						]},
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
						name: 'f_appunti',
						xtype: 'combo',
						fieldLabel: 'Blocco note appunti',
						labelWidth : 150,
						flex : 2,
						margin : '0 0 5 5',
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona blocco-',
				   		//allowBlank: false,								
					    anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json(find_TA_sys('NUSN', null, 'AR', '*ST', null, null, 0, '', 'Y'), ''); ?>	
						    ]
						}
						
				       }, {xtype : 'component', flex : 0.5}
				       
				       ]}
				]
			},{
				xtype: 'panel',
				title: 'Filtri', 
				autoScroll : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
				 { 
						xtype: 'fieldcontainer',
						margin : '5 5 5 5',
						flex:1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
						
			            xtype: 'combo',
						name: 'f_fornitore',
						fieldLabel: 'Fornitore',
						anchor: '-15',
						labelWidth : 80,
						minChars: 2,			
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '../base/acs_get_anagrafica.php?fn=get_data&cod=F',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}, {name:'loca'}, {name:'color'}],            	
			            },
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            triggerAction: 'all',
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item" style="background-color:{color}">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {loca}' + 
			                    '</div>';
			                	}                
		                
		            	},
		            	pageSize: 1000,
		                 listeners: {
                            beforequery: function(qeb){
                           
                               qeb.combo.getStore().removeAll();
								
							}
                        }
						},{
								name: 'f_classe',
								xtype: 'combo',
								//flex:0.5,
								multiSelect : true,	
								labelWidth : 80,
								fieldLabel: 'Classe',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   	   anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUCM'), ''); ?>	
								    ]
								},
								
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }, 	{
								name: 'f_tipo_parte',
								xtype: 'combo',
								multiSelect : true,	
								labelWidth : 80,
								fieldLabel: 'Tipo/parte',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MTPA'), ''); ?>	
								    ]
								},
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }, {
								name: 'f_gruppo',
								xtype: 'combo',
								fieldLabel: 'Gruppo',
								multiSelect : true,	
								labelWidth : 80,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUGM'), ''); ?>	
								    ]
								},
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }, {
								name: 'f_pianificazione',
								xtype: 'combo',
								fieldLabel: 'Gr. pianificaz.',
								multiSelect : true,	
								labelWidth : 80,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('GRPA'), ''); ?>	
								    ]
								},
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }, {
								name: 'f_sottogruppo',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								labelWidth : 80,
								multiSelect : true,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUSM'), ''); ?>	
								    ]
								},
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 },{
								name: 'f_configuratore',
								xtype: 'combo',
								fieldLabel: 'Configuratore',
								labelWidth : 80,
								multiSelect : true,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUMO'), ''); ?>	
								    ]
								},
								queryMode: 'local',
	                			minChars: 1, 	
								listeners: { 
            					 	beforequery: function (record) {
            			         	record.query = new RegExp(record.query, 'i');
            			         	record.forceAll = true;
            		             }
            		          }
								
						 }
						   
						]}
							
					
				]
			},
			
			
		{
				xtype: 'panel',
				title: 'Evidenza', 
				autoScroll : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
				 { 
						xtype: 'fieldcontainer',
						margin : '5 5 5 5',
						flex:1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
        			name: 'f_sfg',
        			xtype: 'combo',
        	   		labelWidth : 110,
        			fieldLabel: 'Generica',
        		//	labelWidth : 120,
        			multiSelect : true,
        			forceSelection: true,								
        			displayField: 'text',
        			valueField: 'id',								
        			emptyText: '-seleziona-',
        			//allowBlank: false,								
        			anchor: '-5',
        			value : <?php echo j($tipo); ?>,
        			tpl: [
                        '<ul class="x-list-plain">',
                        '<tpl for=".">',
                        '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                        '</tpl>',
                        '</ul>'
                    ],
        			store: {
        				editable: false,
        				autoDestroy: true,
        				fields: [{name:'id'}, {name:'text'}, {name:'color'}],
        				data: [								    
        					 {id: '1', text : 'Rosso',  color: '#F9BFC1'},
        					 {id: '2', text : 'Giallo', color : '#F4E287'},
        					 {id: '3', text : 'Verde', color : '#A0DB8E'},
        					 {id: 'N', text : 'Nessuno'},
        					 
        					
        				]
        			}
                       
			
	   }, {
			name: 'f_sfp',
			xtype: 'combo',
			fieldLabel: 'Personale',
			labelWidth : 110,
			multiSelect : true,
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',								
			emptyText: '-seleziona-',
			//allowBlank: false,								
			anchor: '-5',
			value : <?php echo j($tipo); ?>,
			tpl: [
                    '<ul class="x-list-plain">',
                    '<tpl for=".">',
                    '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                    '</tpl>',
                    '</ul>'
                ],
			store: {
				editable: false,
				autoDestroy: true,
				fields: [{name:'id'}, {name:'text'}, {name:'color'}],
				data: [								    
					 {id: '1', text : 'Rosso',   color: '#F9BFC1'},
					 {id: '2', text : 'Giallo', color : '#F4E287'},
					 {id: '3', text : 'Verde', color : '#A0DB8E'},
					 {id: 'N', text : 'Nessuno'},
					 
					
				]
			}
			
	   		} ,
    						{
    			            xtype: 'combo',
    			            name: 'f_utente',
    			            store: Ext.create('Ext.data.ArrayStore', {
    			                fields: [ 'cod', 'descr' ],
    			                data: <?php echo $ar_users_json ?>
    			            }),
    			            displayField: 'descr',
    			            valueField: 'cod',
    			            fieldLabel: 'Utente',
    			            labelWidth : 110,
							//margin : '0 0 5 10',
    			            queryMode: 'local',
    			            selectOnTab: false,
    			            allowBlank: true,
    						forceSelection: true		          
    			        },
    			        
    			        	{
    			            xtype: 'combo',
    			            name: 'f_ut_imm',
    			            store: Ext.create('Ext.data.ArrayStore', {
    			                fields: [ 'cod', 'descr' ],
    			                data: <?php echo $ar_users_json ?>
    			            }),
    			            displayField: 'descr',
    			            valueField: 'cod',
    			            fieldLabel: 'Utente immissione',
    			            labelWidth : 110,
							//margin : '0 0 5 10',
    			            queryMode: 'local',
    			            selectOnTab: false,
    			            allowBlank: true,
    						forceSelection: true		          
    			        },
    			        {
						name: 'f_todo',
						xtype: 'combo',
						fieldLabel: 'To Do',
						labelWidth : 110,
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: true,													
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php
							     // echo acs_ar_to_select_json($s->find_TA_std('ATTAV', N, 'Y'), "");
							     echo acs_ar_to_select_json($deskArt->find_TA_std('ATTAV', null, 'Y', 'N', null, null, 'ART'), "");
							       ?>	
							    ] 
						},listeners: {
                    				change: function(field,newVal) {
                    				
                                    	 combo_todo_ev = this.up('form').down('#todo_ev');    
                                         combo_todo_ev.store.proxy.extraParams.takey1 = newVal;
                                    	 combo_todo_ev.store.load();                             
                             			
                             

                            }
                        }						 
					}, {
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente',
			            queryMode: 'local',
			            selectOnTab: false,
			            name: 'f_utente_assegnato',
			            allowBlank: true,
						forceSelection: true,			            
						labelWidth : 110
			        }, {
						name: 'f_todo_ev',
						xtype: 'combo',
						itemId: 'todo_ev',
						fieldLabel: 'To Do evase',
						multiSelect : true, 
						labelWidth : 110,
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: true,	
					   	store: {
                            autoLoad: true,
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
                                url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_rilav',
					            reader: {
					                type: 'json',
	                                method: 'POST',		
					                root: 'root',
					            },
                                actionMethods: {
    							          read: 'POST'
    							        },
                                extraParams: {
               		    		    		takey1: ''
               		    		       },               						        
				        		doRequest: personalizza_extraParams_to_jsonData
					        },       
								fields: ['id', 'text'],            	
			            }												
							 
					}, 
						   
						]}
							
					
				]
			}, 
				{
				xtype: 'panel',
				title: 'Listini', 
				autoScroll : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
				 { 
						xtype: 'fieldcontainer',
						margin : '5 5 5 5',
						flex:1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								name: 'f_lv',
								xtype: 'combo',
								multiSelect : true,	
								fieldLabel: 'Listini di vendita',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,
						   		queryMode: 'local',
	                 		    minChars: 1, 								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php 
  								     $where = " AND (SUBSTRING(TAREST, 19, 2) = 'V'
                                                  OR SUBSTRING(TAREST, 21, 2) = 'V'
                                                  OR SUBSTRING(TAREST, 23, 2) = 'V'
                                                  OR SUBSTRING(TAREST, 25, 2) = 'V'
                                                  OR SUBSTRING(TAREST, 27, 2) = 'V')";
								     
								     echo acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, $where, 'Y'), ''); 
								     
								     
								     ?>	
								    ]
								}
								,listeners: { 
                			 		beforequery: function (record) {
                    	         		record.query = new RegExp(record.query, 'i');
                    	         		record.forceAll = true;
		            				 }
		         				 }
								
						 },
						 {
								name: 'f_la',
								xtype: 'combo',
								multiSelect : true,	
								fieldLabel: 'Listini di acquisto',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,	
						   		queryMode: 'local',
	                 		    minChars: 1, 											
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php 
  								     $where = " AND (SUBSTRING(TAREST, 19, 2) = 'A'
                                                  OR SUBSTRING(TAREST, 21, 2) = 'A'
                                                  OR SUBSTRING(TAREST, 23, 2) = 'A'
                                                  OR SUBSTRING(TAREST, 25, 2) = 'A'
                                                  OR SUBSTRING(TAREST, 27, 2) = 'A')";
								     
								     echo acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, $where, 'Y'), ''); 
								     
								     
								     ?>	
								    ]
								}
								,listeners: { 
                			 		beforequery: function (record) {
                    	         		record.query = new RegExp(record.query, 'i');
                    	         		record.forceAll = true;
		            				 }
		         				 }
								
						 }, 
						 	{
						
			            xtype: 'combo',
						name: 'f_forn_list',
						fieldLabel: 'Fornitore',
						anchor: '-15',
						minChars: 2,			
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '../base/acs_get_anagrafica.php?fn=get_data&cod=F',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'},  {name:'loca'}, {name:'color'}],            	
			            },
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item" style="background-color:{color}">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {loca}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
						},
						
						 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
						xtype : 'button',
			            text: 'Report listini vendita',
			            margin : '5 5 5 5',
			            //iconCls: 'icon-print-32',
			            scale: 'medium',
		                 handler: function() {
	                      form = this.up('form').getForm();
                          this.up('form').submit({
                          url: 'acs_anag_art_report_listini.php',
                      	  target: '_blank', 
                          standardSubmit: true,
                          method: 'POST',                        
                          params: {
                              form_values: Ext.encode(form.getValues()),
                              rp_listini: 'V'
					      }
              			});            	                	     
        	          }
			         }, 		{
						xtype : 'button',
			            text: 'Report listini acquisto',
			            margin : '5 5 5 5',
			            //iconCls: 'icon-print-32',
			            scale: 'medium',
			           	handler: function() {
	                      var form = this.up('form').getForm();
	                      var form_values = form.getValues();
	                      
	                      if(typeof(form_values.f_forn_list) === 'undefined'){
							 acs_show_msg_error('Inserire un fornitore');
	                      	 return;
	                      }
	                      
	                
	                          this.up('form').submit({
                              url: 'acs_anag_art_report_listini.php',
                          	  target: '_blank', 
                              standardSubmit: true,
                              method: 'POST',                        
                              params: {
                                  form_values: Ext.encode(form.getValues()),
                                  rp_listini: 'A'
    					      }
                  			}); 
	                            	                	     
        	          }
			         }
						
						
						]}
    					
						   
						]}
							
					
				]
			},
			
			{
				xtype: 'panel',
				title: 'Codici', 
				autoScroll : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
					{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						margin : '0 0 0 62',
						items: [
						
						<?php for ($i = 1; $i<=9; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 13 0 0' },
						<?php }?>	
						<?php for ($i = 10; $i<=15; $i++){?>
						{ xtype: 'displayfield', value : '<?php echo $i; ?>', margin : '0 6 0 0' },
						<?php }?>	
				
						  
						 ]
					},
						
					<?php for ($n = 1; $n<= 20; $n++){?>	
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{ xtype: 'displayfield', value : '<?php echo "cod. {$n}) ";?>', margin : '0 15 0 5' },
						
					<?php for ($i = 1; $i<=15; $i++){?>
						
						{ 
						  xtype : 'textfield',
						  name: 'f_<?php echo "[{$n}]{$i}"; ?>', 
						  width: 20,
						  maxLength: 1,
						  enforceMaxLength : true,
						  listeners: {
    						'change': function(field){
    						
    						 value = this.getValue().toString();
    						if (value.length == 1) {
                                var nextfield = field.nextSibling();
                                nextfield .focus(true, 0);
                              } 
                              
                              field.setValue(field.getValue().toUpperCase());
      							
    					  }
 						 }},
						
						<?php }?>
					
						]},		
						<?php }?>
					
				]
			},
			
			{
				xtype: 'panel',
				title: 'Altri filtri', 
				autoScroll : true,
				layout: {type: 'vbox', border: false, pack: 'start', /*align: 'stretch'*/},
				items: [
			
						{
						xtype: 'textfield',
						name: 'f_desc_nota',
						xtype: 'textfield',
					    margin : '5 0 5 5',
					    width : 440,
						fieldLabel: 'Nota',
						anchor: '-1',
						maxLength : 80
						
						},{
						name: 'f_blocco',
						xtype: 'combo',
						fieldLabel: 'Blocco',
						width : 440,
						margin : '0 0 5 5',
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',								
						emptyText: '- seleziona blocco-',
				   		//allowBlank: false,								
					    anchor: '-15',
						store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
						     <?php echo acs_ar_to_select_json(find_TA_sys('NUSN', null, 'AR', '*ST', null, null, 0, '', 'Y'), ''); ?>	
						    ]
						}
						
				 }, { 
						xtype: 'textfield',		 
				        fieldLabel: 'Riferimento', 
						name: 'f_riferimento',
						width : 440,
						margin : '0 0 5 5',
						anchor: '-15',
						maxLength : 15,
						listeners: {
    						'change': function(field){
    							field.setValue(field.getValue().toUpperCase());
      						}
 						 }
						},
					 { 
						xtype: 'textfield',		 
				        fieldLabel: 'Alternativo', 
						name: 'f_alternativo',
						margin : '0 0 5 5',
						width : 440,
						anchor: '-15',
						maxLength : 15,
						listeners: {
    						'change': function(field){
    							field.setValue(field.getValue().toUpperCase());
      						}
 						 }
						},
						 { 
						xtype: 'textfield',		 
				        fieldLabel: 'Rif. tecnico', 
						name: 'f_rif_tecnico',
						margin : '0 0 5 5',
						width : 440,
						anchor: '-15',
						maxLength : 15,
						listeners: {
    						'change': function(field){
    							field.setValue(field.getValue().toUpperCase());
      						}
 						 }
						},
						
						//Tipo di riordino MTO/MTS o Discrieto (ARSWTR)
						, {
								name: 'f_tipo_riordino',
								xtype: 'combo',
								fieldLabel: 'Tipo riordino',
								margin : '0 0 5 5',
								forceSelection: true,
								multiSelect: true,
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json(
								            $deskArt->find_TA_std('AR011', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								}
								
						 },
						 
						{
				 	xtype: 'fieldcontainer',
				 	anchor: '-15',
				 	margin : '0 0 5 5',
				 	flex : 1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					{
								name: 'f_proprieta',
								xtype: 'combo',
								flex: 1,
								fieldLabel: 'Propriet&agrave;/Valore',
							    margin : '0 5 0 0',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								queryMode: 'local',
								minChars: 1,							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',	
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, 'TAASPE'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($deskArt->find_TA_std('ARPRO', null, 'N',  'Y', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								}, listeners: {
                                	change: function(field,newVal, a) {	
                                  		if (!Ext.isEmpty(newVal)){
                                  		    tab = field.displayTplData[0].TAASPE;
                                  		    combo_risp = this.up('form').down('#c_risp');                      		 
                                         	combo_risp.store.proxy.extraParams.tab = tab;
                                        	combo_risp.store.load();                             
                                         }
                                         
            
                                        }, beforequery: function (record) {
                                        record.query = new RegExp(record.query, 'i');
                                        record.forceAll = true;
                                    }
                           }
								
						   },
						   
						   {
								name: 'f_valore',
								xtype: 'combo',
								flex: 1,
								itemId: 'c_risp',
								fieldLabel: '',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
								queryMode: 'local',
								minChars: 1,	
								multiSelect : true,
						   		//allowBlank: false,								
							    anchor: '-15',	
								store: {
									        autoLoad: true,
											proxy: {
									            type: 'ajax',
									            url : 'acs_anag_art_variabili.php?fn=get_tab_risposte',
									            actionMethods: {
            							          read: 'POST'
           							        },
							                	extraParams: {
							    		    		tab: '',
							    		    		codice : 'Y'
							    				},				            
									            doRequest: personalizza_extraParams_to_jsonData, 
									            reader: {
               						            type: 'json',
               									method: 'POST',						            
                						            root: 'root'						            
                						        }
									        },       
											fields: [{name:'id'}, {name:'text'}],		             	
							            }
								
								, listeners: {
                                	    beforequery: function (record) {
                                        record.query = new RegExp(record.query, 'i');
                                        record.forceAll = true;
                                    }
                           }
						   }
					
					
					
					]}
				
		   	
		]
	}
		   	
		]
	} ,
               {
							 xtype: 'fieldcontainer',
							 flex:0.4, 
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						      {
								name: 'f_schede',
								xtype: 'combo',
								fieldLabel: 'Schede',
								labelWidth : 50,
								margin : '0 0 10 10',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								    <?php echo acs_ar_to_select_json($deskArt->find_TA_std('SCHED', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>
								    ]
								}
								
						 }, 
						       {
								name: 'f_lingua',
								xtype: 'combo',
								fieldLabel: 'Lingua',
								labelWidth : 50,
								margin : '0 0 10 10',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($cfg_mod_DeskUtility['ar_lingue'], ''); ?>	
								    ]
								}
								
						 }, 	      
                   { 
						xtype: 'fieldset',
						title: 'Flags',
						//border : false,
						margin : '5 0 0 5',
	                    layout: 'anchor',
	                    items: [
						    {
							name: 'f_sos',
							xtype: 'radiogroup',
							fieldLabel: 'Sospesi',
							labelWidth: 45,
							flex: 1,
							labelAlign: 'left',
						   	allowBlank: true,
						   	
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'Y'
		                          , width: 45
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'T'
		                          , width: 60   
								  , checked: true                       
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_sospeso' 
		                          , boxLabel: 'Esclusi'	
		                          , inputValue: 'N'
		                          , width: 60
		                         
		                          
		                        }]
						}, 
						
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
                            fieldLabel: 'In esaurimento',
                            name: 'f_esau',	
                            labelWidth: 100,
                            margin : '0 10 0 0',
                            xtype: 'checkboxfield',
                            inputValue: 'E',             
                        },{
                            fieldLabel: 'Attivo',
                            name: 'f_attivo',	
                            labelWidth: 100,
                            xtype: 'checkboxfield',
                            inputValue: 'AT',             
                        }
						
						   
						]},
						
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
                            fieldLabel: 'In avviamento',
                            name: 'f_avv',
                            labelWidth: 100,
                            margin : '0 10 0 0',
                            xtype: 'checkboxfield',
                            inputValue: 'A',             
                        },{
                            fieldLabel: 'Riservato',
                            name: 'f_riser',	
                            labelWidth: 100,
                            xtype: 'checkboxfield',
                            inputValue: 'R',             
                        }
						
						   
						]},
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
                            fieldLabel: 'In codifica',
                            name: 'f_corso',	
                            labelWidth: 100,
                            margin : '0 10 0 0',
                            xtype: 'checkboxfield',
                            inputValue: 'C'         
                        },{
                            fieldLabel: 'Fine codifica',
                            name: 'f_fine',	
                            labelWidth: 100,
                            xtype: 'checkboxfield',
                            inputValue: 'F',             
                        }
						
						   
						]},
                        
                         { 
						xtype: 'fieldcontainer',
						
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {
                            fieldLabel: 'Obsoleti',
                            name: 'f_obs',	
                            labelWidth: 100,
                            margin : '0 10 0 0',
                            xtype: 'checkboxfield',
                            inputValue: 'Y'         
                        },   {
                            fieldLabel: 'Lenta movim.',
                            name: 'f_l_mov',	
                            labelWidth: 100,
                            xtype: 'checkboxfield',
                            inputValue: 'Y'         
                        }
						
						   
						]}
            						
						]}
						]}
						   
						]}
        		    		        
				
					        
				],
			buttons: [
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "ANAG_ART");  ?>
			{
	            text: 'Recenti [200]',
	            iconCls: 'icon-news-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){	            	
	            	
            	 	  var rows = this.up('form').down('grid').getStore().data.items;
                      var list_rows = [];
                            
                      for (var i=0; i<rows.length; i++) {
                     	 if(rows[i].get('disabled') != 'Y'){
		            		list_rows.push({
		            		livello : rows[i].get('code'),
		            		
		            		});
		            	   }
		            	}
		            	
		            	if(this.up('window').fromObjectId != null){
    		            	var tree_panel = Ext.getCmp(this.up('window').fromObjectId);
    		            	tree_panel.getStore().proxy.extraParams.form_ep = form.getValues();
    		            	tree_panel.getStore().proxy.extraParams.livelli = list_rows;
    		            	tree_panel.getStore().proxy.extraParams.recenti = 200;
    		            	tree_panel.getStore().load();
		            	} else {
    		            	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', null, {
        		            	form_open: this.up('form').get_values_for_submit(), 
        		            	livelli : list_rows,
        		            	win_id: win_id,
        		            	recenti : 200
    		            	}, null, null);
    		            }
		            	
			            this.up('window').hide();
			        }            	                	                
	            }
	        },	{
	            text: 'Grafici',
	            iconCls: 'icon-grafici-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){
	            	
	            	rows = this.up('form').down('grid').getStore().data.items;
                            list_rows = [];
                            
                              for (var i=0; i<rows.length; i++) {
                             	 if(rows[i].get('disabled') != 'Y'){
				            		list_rows.push({
				            		livello : rows[i].get('code')
				            		});
				            	   }
				            	}
	            	
	            		acs_show_win_std('Anagrafica articoli', 'acs_grafici_art.php?fn=open', {
	            		form_open: this.up('form').get_values_for_submit(),
	            		livelli : list_rows}, 
	            		800, 400, null, 'icon-grafici-16', 'Y');		            
			        }
	                      	                	                
	            }
	            }, {
	            text: 'Indici',
	            iconCls: 'icon-bookmark-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){
	            	
	            	rows = this.up('form').down('grid').getStore().data.items;
                            list_rows = [];
                            
                              for (var i=0; i<rows.length; i++) {
                             	 if(rows[i].get('disabled') != 'Y'){
				            		list_rows.push({
				            		livello : rows[i].get('code')
				            		});
				            	   }
				            	}
	            	
	            		acs_show_win_std('Indici di classificazione', '../desk_utility/acs_anag_artd_crea_nuovo.php?fn=open_indici', {
	            		form_open: this.up('form').get_values_for_submit(),
	            		livelli : list_rows}, 
	            		400, 400, null, 'icon-bookmark-16');
	            		this.up('window').close();
		            
			        } 
	                      	                	                
	            }
	            },

			  {
	         	xtype: 'splitbutton',
	            text: 'Reports',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        		{ xtype: 'button',
		        		  text: 'Report anagrafica',
			              scale: 'medium',
			              handler: function() {
		                    form = this.up('form').getForm();
		                    
		                     var rows = this.up('form').down('grid').getStore().data.items;
                      		 var list_rows = [];
                            
                              for (var i=0; i<rows.length; i++) {
                             	 if(rows[i].get('disabled') != 'Y'){
        		            		list_rows.push({
        		            		livello : rows[i].get('code'),
        		            		
        		            		});
        		            	   }
        		            	}
		                    
		                    if(form.getValues().f_schede !== undefined){
		                        acs_show_win_std('Filtri report', 'acs_anag_art_report.php?fn=open_form', {form_values: this.up('form').get_values_for_submit(), livelli : list_rows, schede : 'Y'}, 440, 370, null, 'icon-barcode');
		                    }else{
		                        acs_show_win_std('Filtri report', 'acs_anag_art_report.php?fn=open_form', {form_values: this.up('form').get_values_for_submit(), livelli : list_rows }, 440, 280, null, 'icon-barcode');
		                    }
		                      	                	     
            	         }
		        		} ,
		        		
		        	<?php if($m_params->from_pv != 'Y'){?>
		        	
		        	{ xtype: 'button',
		        		  text: 'Report distinta',
			              scale: 'medium',
			              handler: function() {
		                      form = this.up('form').getForm();
	                          this.up('form').submit({
	                          url: 'acs_anag_art_report_distinta.php',
                          	  target: '_blank', 
	                          standardSubmit: true,
	                          method: 'POST',                        
	                          params: {
	                              form_values: Ext.encode(this.up('form').get_values_for_submit())
	                          }
                  			});            	                	     
            	          }
			         },
		        		
		        		{ xtype: 'button',
		        		  text: 'Report cicli',
			              scale: 'medium',
			              handler: function() {
		                      form = this.up('form').getForm();
	                          this.up('form').submit({
	                          url: 'acs_anag_art_report.php?fn=open_report',
                          	  target: '_blank', 
	                          standardSubmit: true,
	                          method: 'POST',                        
	                          params: {
	                              form_values: Ext.encode(this.up('form').get_values_for_submit()),
	                              rp_cicli: 'Y'
  						      }
                  			});            	                	     
            	          }
			         },
			         { xtype: 'button',
		        		  text: 'Report formati',
			              scale: 'medium',
			                handler: function() {
    		            	    form = this.up('form').getForm();
    		                    if (form.isValid()){
            	                acs_show_win_std('Report formati', 'acs_anag_art_formati_report.php?fn=open_form', {
    	        				filter: this.up('form').get_values_for_submit()
    	    					},		        				 
    	        				400, 200, {}, 'icon-print-16');	
		        			} 
		            
		                
		           		 }
			         }
			         
			         <?php }?>
		        	] <!-- end items -->
	        	} <!-- end men� -->
        	} <!-- end split button -->
			        
			         ,{
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){	            	
	            	
            	 	  var rows = this.up('form').down('grid').getStore().data.items;
                      var list_rows = [];
                      
                      var submit_form_values = this.up('form').get_values_for_submit();
                            
                      for (var i=0; i<rows.length; i++) {
                     	 if(rows[i].get('disabled') != 'Y'){
		            		list_rows.push({
		            		livello : rows[i].get('code'),
		            		
		            		});
		            	   }
		            	}
		            	
		            	if(this.up('window').fromObjectId != null){
    		            	var tree_panel = Ext.getCmp(this.up('window').fromObjectId);
    		            	tree_panel.getStore().proxy.extraParams.form_ep = form.getValues();
    		            	tree_panel.getStore().proxy.extraParams.livelli = list_rows;
    		            	tree_panel.getStore().load();
		            	} else {
    		            	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', null, {
        		            	form_open: submit_form_values,
        		            	livelli : list_rows,
        		            	win_id: win_id,
        		            	from_pv : '<?php echo $m_params->from_pv; ?>'
    		            	}, null, null);
    		            }
		            	
			            this.up('window').hide();
			        }            	                	                
	            }
	        }	        
	        ], 
	        
	        
	        listeners: {
			    afterrender: function(comp) {
			        var win = comp.up('window');
			        win.tools.close.hide();
			        var myCloseTool = Ext.create('Ext.panel.Tool', {
			            type: 'close',
			            handler: function() {
			                if(win.fromObjectId != null){ //aperto da un tree
			                	win.hide();
			                }
			                else {						 //aperto da icona su menu principale
			                	win.close();
			                }
			            }
			        })
			        win.header.insert(3, myCloseTool); // number depends on other items in header
			    }
}
	        
	        
	        
				
        }
]}
<?php	
	exit;
} //open_form_filtri


function get_desc_livello($tipo_liv){
	
	global $main_module;
	
	switch ($tipo_liv){
		case 'ARTPAR' : 
			$task = 'D_TIPO_PARTE';
		break;
		case 'ARCLME' :
			$task = 'D_CLASSE';
		break;
		case 'ARGRME' : 
			$task = 'D_GRUPPO';
		break;
		case 'ARSGME':
			$task = 'D_SOTTOGRUPPO';
		break;
		case 'ARTAB1':
		    $task = 'D_GRUPPOPIANI';
		    break;
		case 'ARFOR1':
		    $task = 'D_FORNITORE';
		    break;
		case 'ARMODE':
		    $task = 'D_CONFIG';
		    break;
		case 'ARSWTR':
		    $task = 'D_RIORDINO';
		    break;
	}

	return $task;
}

function get_desc_liv_tree($tipo_liv){
    
    global $main_module;
    
    switch ($tipo_liv){
        case 'ARTPAR' :
             $desc = 'Tipo parte';
            break;
        case 'ARCLME' :
            $desc = 'Classe';
            break;
        case 'ARGRME' :
            $desc = 'Gruppo';
            break;
        case 'ARSGME':
            $desc = 'Sottogr.';
            break;
        case 'ARTAB1':
             $desc = 'Gr. pianific.';
            break;
        case 'ARFOR1':
             $desc = 'Forn.';
            break;
        case 'ARMODE':
            $desc = 'Config.';
            break;
        case 'ARSWTR':
            $desc = 'Riordino';
            break;
            
    }
    
    return $desc;
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data'){
####################################################
    ini_set('max_execution_time', 6000);
    $sa = new SpedAssegnazioneArticoli();
        
    //per problema in fase di paginazione (sembra che mi fa il refresh dell'interno tree)
    if(!isset($_REQUEST['node'])){
        echo acs_je(array('success' => true));
        exit;
    }
    
    
	$ret = array();
	$ar =  array();
	$n_children = 'children';
	$m_params = acs_m_params_json_decode();
	$form_ep = $m_params->form_ep;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql_join = '';
	$ar_where = array();
	$str_offset = "";
	$str_limit  = "";
	$attav_chiuse = "";

	
	if(count($m_params->livelli) == 0  || $m_params->recenti > 0 ){
	    $str_liv['f_liv0'] = 'TOTALE';
	    $liv0_v = " ";
	    $task0 = "TOTALE";
	    $desc0 = " ";
	    $livelli = array();
	    $livelli[0] = (object)array('livello' =>'totale');
	    $liv_0_expanded = true;
	    $liv_totale = true;
	}else{	
	   $livelli = $m_params->livelli;
	   $liv_0_expanded = false;	
	   $liv_totale = false;
	}
	
	
	
	

    if(isset($form_ep->f_lv) && count($form_ep->f_lv) > 0){
        
        $sql_where_lv = sql_where_by_combo_value('LIV.LILIST', $form_ep->f_lv);
      
        $sql_join .= "INNER JOIN(
                    SELECT DISTINCT LIART, LIDT
                    FROM {$cfg_mod_DeskUtility['file_listini']} LIV
                    WHERE LITPLI = 'V' {$sql_where_lv}
                    ) LI
                    ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART";

	}elseif((isset($form_ep->f_la) && count($form_ep->f_la) > 0) || strlen(trim($form_ep->f_forn_list)) > 0){
	     
	    $sql_where_la = sql_where_by_combo_value('LIA.LILIST', $form_ep->f_la);
	    
	    $sql_join .= "INNER JOIN(
                    SELECT DISTINCT LIART, LIDT
                    FROM {$cfg_mod_DeskUtility['file_listini']} LIA
                    WHERE LITPLI = 'A' {$sql_where_la}
                    ) LI
                    ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART";
	   
	}
	
	if(isset($form_ep->f_todo) && strlen($form_ep->f_todo) > 0){
	    
	    if(isset($form_ep->f_utente_assegnato) && strlen($form_ep->f_utente_assegnato) > 0)
	       $as_where = " AND ASUSAT = '{$form_ep->f_utente_assegnato}'";
	    
       if(count($form_ep->f_todo_ev) > 0){
           $as_where .= " AND ASFLRI = 'Y'";
           $as_where .= sql_where_by_combo_value('ASCARI', $form_ep->f_todo_ev);
       }else{
           $as_where .= " AND ASFLRI <> 'Y'";
       }
       
       
	    $sql_join .= "INNER JOIN (
	                   SELECT DISTINCT ASDOCU
	                   FROM {$cfg_mod_DeskArt['file_assegna_ord']}
                       WHERE ASDT = '{$id_ditta_default}'  AND ASCAAS = '{$form_ep->f_todo}' {$as_where}
	                    ) AS0
	                  ON AS0.ASDOCU = AR.ARART";
	}
	
	if(isset($form_ep->f_desc_nota) && strlen($form_ep->f_desc_nota) > 0){
	    
	    if(isset($form_ep->f_blocco) && strlen($form_ep->f_blocco) > 0)
	        $bl_where = " AND RLRIFE2 = '{$form_ep->f_blocco}'";
	    
	      $sql_join .= " INNER JOIN (
	      SELECT DISTINCT RLDT, RLRIFE1
	      FROM {$cfg_mod_DeskUtility['file_note_anag']} 
	      WHERE RLTPNO = 'AR' {$bl_where}
            AND UPPER(CONCAT(RLSWST, CONCAT(RLREST1, RLFIL1))) LIKE '%" . strtoupper($form_ep->f_desc_nota) . "%'
	      ) RL
	       ON RL.RLDT = AR.ARDT AND RL.RLRIFE1 = AR.ARART";
	    
	}
	
	if(isset($form_ep->f_proprieta) && strlen($form_ep->f_proprieta) > 0){
	    
	    $af_where = " AFTDAT = '{$form_ep->f_proprieta}'"; 
	    if(isset($form_ep->f_valore) && count($form_ep->f_valore) > 0)
	        $af_where .= sql_where_by_combo_value('AFVALO', $form_ep->f_valore);
	    
	    
	    $sql_join .= "INNER JOIN(
	           SELECT DISTINCT AFART, AFDT
	           FROM {$cfg_mod_DeskArt['file_proprieta']}
	           WHERE {$af_where}
	             ) AF
	    ON AF.AFDT = AR.ARDT AND AF.AFART = AR.ARART";
	    
	    
	}
	
	
	        
	 if($m_params->recenti > 0)
	   $limit = "200";
	 else
	   $limit = "500";
	 
	   
   if(strlen($form_ep->f_utente) > 0)
       $utente = $form_ep->f_utente;
   else
       $utente = trim($auth->get_user());
	   
	$sql_where = sql_where_params($form_ep);

	
	
	//********************************************* root
	if($_REQUEST['node'] == 'root'){
	    
	    
	    //Per Recenti 200: torno direttamente il livello "Totale"
	    if($m_params->recenti > 0){
	        echo acs_je(array('success' => true, 
	            'children' => array(
	                'id' => 'totale',
	                'liv_cod' => '',
	                'liv' => 'liv_0',
	                'task' => 'TOTALE',
	                'codice' => '',
	                'classe' => 'totale',
	                'expanded' => true,
	                'c_art' => 200,
	                'children' => null
	            )));
	        exit;
	    }
	        
	    
		
		foreach($livelli as $value_ar){

			if($value_ar->livello == 'ARTPAR'){
				$ar_select .=  'ARTPAR, TA_TIPO_PARTE.TADESC AS D_TIPO_PARTE, ';
				$ar_group_by.= 'ARTPAR, TA_TIPO_PARTE.TADESC, ';
				$ar_order_by .= 'TA_TIPO_PARTE.TADESC, ';
				
			}
			if($value_ar->livello  == 'ARCLME'){
				$ar_select .=  'ARCLME, TA_CLASSE.TADESC AS D_CLASSE, ';
				$ar_group_by.= 'ARCLME, TA_CLASSE.TADESC, ';
				$ar_order_by .= 'TA_CLASSE.TADESC, ';
				
			}
			if($value_ar->livello ==  'ARGRME'){
				$ar_select .=  'ARGRME, TA_GRUPPO.TADESC AS D_GRUPPO, ';
				$ar_group_by.= 'ARGRME, TA_GRUPPO.TADESC, ';
				$ar_order_by .= 'TA_GRUPPO.TADESC, ';
				
			}	
			if($value_ar->livello ==  'ARSGME'){
				$ar_select .=  'ARSGME, TA_SOTTOGRUPPO.TADESC AS D_SOTTOGRUPPO, ';
				$ar_group_by.= 'ARSGME, TA_SOTTOGRUPPO.TADESC, ';
				$ar_order_by .= 'TA_SOTTOGRUPPO.TADESC, ';
				
			}	
			if($value_ar->livello ==  'ARTAB1'){
			    $ar_select .=  'ARTAB1, TA_GRUPPOPIANI.TADESC AS D_GRUPPOPIANI, ';
			    $ar_group_by.= 'ARTAB1, TA_GRUPPOPIANI.TADESC, ';
			    $ar_order_by .= 'TA_GRUPPOPIANI.TADESC, ';
			  
			}
			if($value_ar->livello ==  'ARFOR1'){
			    $ar_select .=  'ARFOR1, CF_FORNITORE.CFRGS1 AS D_FORNITORE, ';
			    $ar_group_by.= 'ARFOR1, CF_FORNITORE.CFRGS1, ';
			    $ar_order_by .= 'CF_FORNITORE.CFRGS1, ';
			  
			}
			if($value_ar->livello ==  'ARMODE'){
			    $ar_select .=  'ARMODE, TA_CONFIG.TADESC AS D_CONFIG, ';
			    $ar_group_by.= 'ARMODE, TA_CONFIG.TADESC, ';
			    $ar_order_by .= 'TA_CONFIG.TADESC, ';
			    
			}
			
			if($value_ar->livello ==  'ARSWTR'){
			    $ar_select .=  'ARSWTR, TA_RIORDINO.TADESC AS D_RIORDINO, ';
			    $ar_group_by.= 'ARSWTR, TA_RIORDINO.TADESC, ';
			    $ar_order_by .= 'TA_RIORDINO.TADESC, ';
			    
			}
				
		}
	
		if(trim($ar_order_by) == ''){
		    //$sql_order_by ="ORDER BY ARART ";
		    //$sql_group_by ="GROUP BY ARART";
		    //$sql_campi_select = "COUNT(*) AS ARTICOLI, ARART"; 
		    
		    $sql_order_by = "ORDER BY ARDT ";
		    $sql_group_by ="GROUP BY ARDT";
		    $sql_campi_select = "COUNT(*) AS ARTICOLI, 'aaaa' as ARART"; 
		    
		}else{
	        $sql_order_by ="ORDER BY $ar_order_by ARDT ";
	    
		    $sql_group_by ="GROUP BY $ar_group_by ARDT ";
		    $sql_campi_select = "COUNT(*) AS ARTICOLI, $ar_select ARDT "; 
		}
		
		

	
	} else {    	//********************************************* NO root (livello articoli)	    
	    
		$ar_node = explode("|", $_REQUEST['node']);
			
		$sql_campi_select = "AR.* , AXSFON, AUSFAR, TA_CONFIG.TADESC AS ARMODE_D ";
		
		
		if(isset($form_ep->f_appunti) && strlen($form_ep->f_appunti) > 0){
		    
		    $sql_campi_select .= " ,CONCAT(RLSWST, CONCAT(RLREST1, RLFIL1)) AS APPUNTI, RRN(RL) AS RRN_APPUNTI";
		    $sql_join .= " LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_note_anag']} RL
		    ON RL.RLDT = AR.ARDT AND RL.RLRIFE1 = AR.ARART AND RL.RLTPNO = 'AR' AND RL.RLRIFE2 = '{$form_ep->f_appunti}' AND RL.RLRIGA = 1
		    ";
		    
		}
		
	 if($form_ep->f_allegati == 'Y'){
	   $sql_campi_select .= " , TA_ALLEGATI.NR_AL AS NR_AL ";
	   $sql_join .= " LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_AL, TADT, TATAID, TAKEY1
                       FROM {$cfg_mod_DeskArt['file_tabelle']}
                       GROUP BY TAKEY1, TADT, TATAID) TA_ALLEGATI
                 	ON TA_ALLEGATI.TADT = AR.ARDT AND TA_ALLEGATI.TATAID = 'ARTAL' AND TA_ALLEGATI.TAKEY1 = AR.ARART";
	 }
	 
	 $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_note_anag']} RL WHERE RL.RLDT = AR.ARDT AND RL.RLRIFE1 = AR.ARART AND RL.RLTPNO = 'RA' AND RL.RLRIFE2 = 0001) AS NR_RL ";

	 $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskArt['file_ubicazioni']} BC WHERE BC.BCDT = AR.ARDT AND BC.BCART = AR.ARART AND BC.BCTABB = 'BC') AS NR_BC ";
	 
	 if($form_ep->f_list_ven == 'Y'){
	     
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_listini']} LV WHERE LV.LIDT = AR.ARDT AND LV.LITPLI = 'V' AND LV.LIART = AR.ARART ) AS NR_LV ";
	  
	 }
	 
	 if($form_ep->f_list_acq == 'Y'){
	     
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_listini']} LA WHERE LA.LIDT = AR.ARDT AND LA.LITPLI = 'A' AND LA.LIART = AR.ARART ) AS NR_LA";
	  
	 }
	 
	 if($form_ep->f_cicli == 'Y'){
	     
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_cicli']} CL 
               WHERE CL.CIDT = AR.ARDT 
                 AND CL.CIART = CASE WHEN AR.ARDBCO = '' 
                                THEN AR.ARART
                                ELSE AR.ARDBCO END) AS NR_CL";
	 
	 }
	 
	 if($form_ep->f_dist == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_distinta_base']} DB 
            WHERE DB.DBDT = AR.ARDT 
              AND DB.DBART = CASE WHEN AR.ARDBFB = '' OR ARTPAR IN ('M', 'R')
                             THEN AR.ARART 
                             ELSE AR.ARDBFB END) AS NR_DB";
	 }
	 
	 if($form_ep->f_elenco == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_distinta_base']} DBF WHERE DBF.DBDT = AR.ARDT AND  DBF.DBCOMP = AR.ARART ) AS NR_DBF";
	 }
	 
	 if($form_ep->f_trad == 'Y'){
	     $ar_lin = array();
	     if(isset($form_ep->f_lingua) && $form_ep->f_lingua != ''){
	         $where_trad = " AND ALLING = '{$form_ep->f_lingua}'";
	     }else{
	         foreach($cfg_mod_DeskUtility['ar_lingue'] as $v)
	             $ar_lin[] = $v['id'];
	         
	         $where_trad = " AND ALLING IN (" . sql_t_IN($ar_lin) . ")";
	     }
	      
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_anag_art_trad']} AL WHERE AL.ALDT = AR.ARDT AND AL.ALART = AR.ARART 
                                   {$where_trad} AND ALDAR1 <> '') AS N_TRAD";
	  
	 }
	 
	 if($form_ep->f_att == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT WHERE ATT.ASDT = AR.ARDT AND ATT.ASDOCU = AR.ARART ) AS N_ATT";
       
	 }
	 
	 if($form_ep->f_var == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_variabili']} AM WHERE AM.AMDT = AR.ARDT AND AM.AMART = AR.ARART AND (AM.AMVAR1 <> '' OR AM.AMVAR2 <> '' OR AM.AMVAR3 <> '' OR AM.AMVAR4 <> '' OR AM.AMVAR5 <> '' OR AM.AMVAR6 <> '' OR AM.AMVAR7 <> '' OR AM.AMVAR8 <> '' OR AM.AMVAR9 <> '' OR AM.AMVAR10 <> '')) AS NR_VAR";
      
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskArt['file_proprieta']} AF
                                    WHERE AF.AFDT = AR.ARDT AND AF.AFART = AR.ARART AND AF.AFVALO <> ''
                          ) AS NR_PROP";
	 }
	 
	 if($form_ep->f_abb == 'Y'){
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA_AB WHERE TA_AB.TADT = AR.ARDT AND TA_AB.TAID = 'ABAR' AND TA_AB.TACOR1 = AR.ARART AND TA_AB.TALINV='' AND TA_AB.TACOR2='') AS NR_AB";
	 }
	 

	 if(isset($form_ep->f_schede) && strlen($form_ep->f_schede) > 0){
	     
	     if(trim($form_ep->f_schede) == 'BBPR'){
	         $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA_SCHEDE
	         WHERE TA_SCHEDE.TADT = AR.ARDT AND TA_SCHEDE.TAID = 'BBPR' AND SUBSTR(TA_SCHEDE.TAREST, 25, 15) = AR.ARART ) AS NR_SCHEDE";
	         
	     }elseif(trim($form_ep->f_schede) == 'FSC'){
	         $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskArt['file_fsc']} FA
	         WHERE FA.FADT = AR.ARDT AND FA.FAART = AR.ARART) AS NR_SCHEDE";
	     }else{
	         $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA_SCHEDE
	         WHERE TA_SCHEDE.TADT = AR.ARDT AND TA_SCHEDE.TAID = '{$form_ep->f_schede}' AND TA_SCHEDE.TACOR1 = AR.ARART ) AS NR_SCHEDE";
	     }
	
	 }else{
	     
	     $tab_schede = $deskArt->find_TA_std('SCHED');
	     $ar_schede = array();
	     foreach($tab_schede as $v){
	         if($v['id'] != 'FSC')
	              $ar_schede[] = $v['id'];
	            
	     }    
	     $where = sql_where_by_combo_value('TA_SCHEDE.TAID' , $ar_schede);
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA_SCHEDE
	                           WHERE TA_SCHEDE.TADT = AR.ARDT AND TA_SCHEDE.TACOR1 = AR.ARART {$where}) AS NR_SCHEDE";
	     
	     $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskArt['file_fsc']} FA
	                               WHERE FA.FADT = AR.ARDT AND FA.FAART = AR.ARART) AS FSC";
	 
	 
	 }
		
		$sql_group_by = "";
		if($m_params->recenti > 0){
		    $sql_campi_select .= ", RRN(AR) AS RRN";  //ARDTGE
		    $sql_order_by = "ORDER BY RRN DESC";  //ARDTGE DESC, ARART
		}else{
		    $sql_order_by = "ORDER BY ARART";
		}
	
		
		$where_liv = '';
		$cliv = 0;
		foreach($livelli as $k => $v){
		    if($v->livello != 'totale'){
        	    $where_liv .= " AND {$v->livello} = ?";
        	    
        	    //Bug: se uso blank poi mi corrompe ar_where dopo la query conteggio totale (scrive C_ROW in ar_where)
        	    if ($ar_node[$cliv] == '')
        	      $ar_where[] = ' ';
        	    else
        	      $ar_where[] = $ar_node[$cliv];
        	    
        	    $cliv++;
		    }
		}

		
		if($m_params->recenti > 0)
		  $str_limit = "LIMIT 200";
		else {
		  $art_per_pagination = 500;
		  $str_limit = "LIMIT {$art_per_pagination}";
		}

		if (isset($_REQUEST['m_offset']))
		  $str_offset = " OFFSET " . ($art_per_pagination * $_REQUEST['m_offset']);
		
		
		//articolo con commenti?
		$sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_note_anag']} ART_COMM
              WHERE ART_COMM.RLDT = AR.ARDT AND ART_COMM.RLTPNO = 'AR' AND ART_COMM.RLRIFE2 <> 'APV' AND ART_COMM.RLRIFE1 = AR.ARART AND TRIM(CONCAT(RLSWST, CONCAT(RLREST1, RLFIL1))) <> '') AS NR_ART_COMM";
		  
		//articolo con ToDo aperte?
		$sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_DeskArt['file_assegna_ord']} OPEN_TODO
               WHERE OPEN_TODO.ASDT = AR.ARDT AND OPEN_TODO.ASDOCU = AR.ARART AND OPEN_TODO.ASFLRI = '') AS NR_OPEN_TODO";
		

	} //non root node

	$sql_main_body = " 
			FROM {$cfg_mod['file_anag_art']} AR
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_TIPO_PARTE
				    ON  TA_TIPO_PARTE.TADT = AR.ARDT AND TA_TIPO_PARTE.TAID = 'MTPA' AND TA_TIPO_PARTE.TANR = AR.ARTPAR
                    AND TA_TIPO_PARTE.TALINV = '' AND TA_TIPO_PARTE.TACOR1 = '' AND TA_TIPO_PARTE.TACOR2 = ''  
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_CLASSE
				    ON  TA_CLASSE.TADT = AR.ARDT AND TA_CLASSE.TAID = 'MUCM' AND TA_CLASSE.TANR = AR.ARCLME
                    AND TA_CLASSE.TALINV = '' AND TA_CLASSE.TACOR1 = '' AND TA_CLASSE.TACOR2 = ''
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_GRUPPO
				    ON  TA_GRUPPO.TADT = AR.ARDT AND TA_GRUPPO.TAID = 'MUGM' AND TA_GRUPPO.TANR = AR.ARGRME
                    AND TA_GRUPPO.TALINV = '' AND TA_GRUPPO.TACOR1 = '' AND TA_GRUPPO.TACOR2 = ''
				LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_SOTTOGRUPPO
				    ON  TA_SOTTOGRUPPO.TADT = AR.ARDT AND TA_SOTTOGRUPPO.TAID = 'MUSM' AND TA_SOTTOGRUPPO.TANR = AR.ARSGME
                    AND TA_SOTTOGRUPPO.TALINV = '' AND TA_SOTTOGRUPPO.TACOR1 = '' AND TA_SOTTOGRUPPO.TACOR2 = ''
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_GRUPPOPIANI
                    ON  TA_GRUPPOPIANI.TADT = AR.ARDT AND TA_GRUPPOPIANI.TAID = 'GRPA' AND TA_GRUPPOPIANI.TANR = AR.ARTAB1
                    AND TA_GRUPPOPIANI.TALINV = '' AND TA_GRUPPOPIANI.TACOR1 = '' AND TA_GRUPPOPIANI.TACOR2 = ''
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA_CONFIG
                    ON  TA_CONFIG.TADT = AR.ARDT AND TA_CONFIG.TAID = 'PUMO' AND TA_CONFIG.TANR = AR.ARMODE
                    AND TA_CONFIG.TALINV = '' AND TA_CONFIG.TACOR1 = '' AND TA_CONFIG.TACOR2 = ''
                LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_RIORDINO
                    ON  TA_RIORDINO.TADT = AR.ARDT AND TA_RIORDINO.TATAID = 'AR011' AND TA_RIORDINO.TAKEY1 = AR.ARSWTR 
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
                    ON  CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art']} AX
		            ON AX.AXDT = AR.ARDT AND AR.ARART = AX.AXART
                LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
		            ON AU.AUDT = AR.ARDT AND AU.AUART = AR.ARART AND AU.AUUTEN = '{$utente}' ";
           
	
	$sql = "SELECT $sql_campi_select 
             $sql_main_body
             $sql_join
	        WHERE ARDT = '{$id_ditta_default}' {$sql_where}  {$where_liv}
	        $sql_group_by $sql_order_by
            {$str_limit} {$str_offset}";
	      
	foreach($livelli as $k => $v){	 
       	    $str_liv['f_liv'.$k] = $v->livello; 
    }
  
   
    //* conto quanti sono (per paginazione)
    if (!$m_params->recenti > 0){
        $sqlCount = "SELECT COUNT(*) AS C_ROW $sql_main_body $sql_join     WHERE ARDT = '{$id_ditta_default}' {$sql_where}  {$where_liv}";
        $stmtCount = db2_prepare($conn, $sqlCount);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmtCount, $ar_where);
        $rowCount = db2_fetch_assoc($stmtCount);
    }

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_where);
	
	$count_rows = 0;
	$count = 0;
	
	while ($row = db2_fetch_assoc($stmt)) {
		
		$tmp_ar_id = array();
		
		
	if($liv_totale == false){
	    
	    if (isset($str_liv['f_liv0'])){
	        $liv0_v = trim($row[$str_liv['f_liv0']]);
	        $task0 = $row[get_desc_livello($str_liv['f_liv0'])];
	        $desc0 = get_desc_liv_tree($str_liv['f_liv0']);
	    }
	    if (isset($str_liv['f_liv1'])){
	        $liv1_v = trim($row[$str_liv['f_liv1']]);
	        $task1 = $row[get_desc_livello($str_liv['f_liv1'])];
	        $desc1 = get_desc_liv_tree($str_liv['f_liv1']);
	    }
	    if (isset($str_liv['f_liv2'])){
	        $liv2_v = trim($row[$str_liv['f_liv2']]);
	        $task2 = $row[get_desc_livello($str_liv['f_liv2'])];
	        $desc2 = get_desc_liv_tree($str_liv['f_liv2']);
	    }
	    if (isset($str_liv['f_liv3'])){
	        $liv3_v = trim($row[$str_liv['f_liv3']]);
	        $task3 = $row[get_desc_livello($str_liv['f_liv3'])];
	        $desc3 = get_desc_liv_tree($str_liv['f_liv3']);
	    }
	    if (isset($str_liv['f_liv4'])){
	        $liv4_v = trim($row[$str_liv['f_liv4']]);
	        $task4 = $row[get_desc_livello($str_liv['f_liv4'])];
	        $desc4 = get_desc_liv_tree($str_liv['f_liv4']);
	    }
	    if (isset($str_liv['f_liv5'])){
	        $liv5_v = trim($row[$str_liv['f_liv5']]);
	        $task5 = $row[get_desc_livello($str_liv['f_liv5'])];
	        $desc5 = get_desc_liv_tree($str_liv['f_liv5']);
	    }
	    if (isset($str_liv['f_liv6'])){
	        $liv6_v = trim($row[$str_liv['f_liv6']]);
	        $task6 = $row[get_desc_livello($str_liv['f_liv6'])];
	        $desc6 = get_desc_liv_tree($str_liv['f_liv6']);
	    }
	    if (isset($str_liv['f_liv7'])){
	        $liv7_v = trim($row[$str_liv['f_liv7']]);
	        $task7 = $row[get_desc_livello($str_liv['f_liv7'])];
	        $desc7 = get_desc_liv_tree($str_liv['f_liv7']);
	    }
	}
		// LIVELLO 0 (tipo parte)
		if (count($str_liv) > 0) {
		$s_ar = &$ar;
		$liv 	= $liv0_v;
		$tmp_ar_id[] = $liv;
		if (is_null($s_ar[$liv])){
			$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_0", "task"=> $liv);
			if($task0 == '')
			    $s_ar[$liv]["task"] = 'Non definito';
			else 
			    $s_ar[$liv]["task"]   = $task0; //$task0. " [" . $liv . "]";
			   
			if($liv == ''){ 
			    $s_ar[$liv]["codice"] = "[ND]";
			    $s_ar[$liv]["cod_nota"] = "{$desc0} [ND]";
			}else{ 
			     $s_ar[$liv]["codice"]   = " [" . $liv . "]";
			     $s_ar[$liv]["cod_nota"] = "{$desc0} [{$liv}] ";
			}
			$s_ar[$liv]["classe"]   = $str_liv['f_liv0'];
			
			 $s_ar[$liv]['expanded'] = $liv_0_expanded;

		}
		
		if($m_params->recenti > 0)
		    $s_ar[$liv]['c_art'] = $limit;
		else
		    sum_values($s_ar[$liv], $row);
		
		$s_ar = &$s_ar[$liv][$n_children];
		
		}
		
		
		
		if (count($str_liv) > 1) {
			// LIVELLO 1 (classe)
			$liv 	= $liv1_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_1", "task"=> $liv);
				if($task1 == '')
				    $s_ar[$liv]["task"] = 'Non definito';
				 else
				     $s_ar[$liv]["task"]   = $task1;
				        
				     if($liv == ''){
				         $s_ar[$liv]["codice"] = "[ND]";
				         $s_ar[$liv]["cod_nota"] = "{$desc1} [ND]";
				     }else{
				         $s_ar[$liv]["codice"]   = " [" . $liv . "]";
				         $s_ar[$liv]["cod_nota"] = "{$desc1} [{$liv}]";
				     }
				  
				$s_ar[$liv]["classe"]   = $str_liv['f_liv1'];
				
			}
			sum_values($s_ar[$liv], $row);
			$s_ar = &$s_ar[$liv][$n_children];
		
		}		
		
		

		if (count($str_liv) > 2) {
			// LIVELLO 2 (GRUPPO)
			$liv 	= $liv2_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_2", "task"=> $liv);
				if($task2 == '')
				   $s_ar[$liv]["task"] = 'Non definito';
				else
				   $s_ar[$liv]["task"]   = $task2; //$task0. " [" . $liv . "]";
				        
				   if($liv == ''){
				       $s_ar[$liv]["codice"] = "[ND]";
				       $s_ar[$liv]["cod_nota"] = "{$desc2} [ND]";
				   }else{
				       $s_ar[$liv]["codice"]   = " [" . $liv . "]";
				       $s_ar[$liv]["cod_nota"] = "{$desc2} [{$liv}]";
				   }
			
				$s_ar[$liv]["classe"]   = $str_liv['f_liv2'];
			
			}
			
			sum_values($s_ar[$liv], $row);
			$s_ar = &$s_ar[$liv][$n_children];
			
		}

		
		if (count($str_liv) > 3) {
			// LIVELLO 3 (SOTTOGRUPPO)
			$liv 	= $liv3_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_3", "task"=> $liv);
				if($task3 == '')
				    $s_ar[$liv]["task"] = 'Non definito';
				 else
				    $s_ar[$liv]["task"]   = $task3; //$task0. " [" . $liv . "]";
				        
				    if($liv == ''){
				        $s_ar[$liv]["codice"] = "[ND]";
				        $s_ar[$liv]["cod_nota"] = "{$desc3} [ND]";
				    }else{
				        $s_ar[$liv]["codice"]   = " [" . $liv . "]";
				        $s_ar[$liv]["cod_nota"] = "{$desc3} [{$liv}]";
				    }
	
				$s_ar[$liv]["classe"]   = $str_liv['f_liv3'];
				
			}
				
			sum_values($s_ar[$liv], $row);
			$s_ar = &$s_ar[$liv][$n_children];
			
		}
		
		if (count($str_liv) > 4) {
		    // LIVELLO 4 
		    $liv 	= $liv4_v;
		    $tmp_ar_id[] = $liv;
		    if (is_null($s_ar[$liv])){
		        $s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_4", "task"=> $liv);
		        if($task4 == '')
		            $s_ar[$liv]["task"] = 'Non definito';
		        else
		            $s_ar[$liv]["task"]   = $task4; //$task0. " [" . $liv . "]";
		                
		            if($liv == ''){
		                $s_ar[$liv]["codice"] = "[ND]";
		                $s_ar[$liv]["cod_nota"] = "{$desc4} [ND]";
		            }else{
		                $s_ar[$liv]["codice"]   = " [" . $liv . "]";
		                $s_ar[$liv]["cod_nota"] = "{$desc4} [{$liv}]";
		            }
	
		         $s_ar[$liv]["classe"]   = $str_liv['f_liv4'];
		                        
		    }
		    
		    sum_values($s_ar[$liv], $row);
		    $s_ar = &$s_ar[$liv][$n_children];
		    
		}
		
		if (count($str_liv) > 5) {
		    // LIVELLO 5
		    $liv 	= $liv5_v;
		    $tmp_ar_id[] = $liv;
		    if (is_null($s_ar[$liv])){
		        $s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_5", "task"=> $liv);
		        if($task5 == '')
		            $s_ar[$liv]["task"] = 'Non definito';
		        else
		            $s_ar[$liv]["task"]   = $task5; //$task0. " [" . $liv . "]";
		                
		            if($liv == ''){
		                $s_ar[$liv]["codice"] = "[ND]";
		                $s_ar[$liv]["cod_nota"] = "{$desc5} [ND]";
		            }else{
		                $s_ar[$liv]["codice"]   = " [" . $liv . "]";
		                $s_ar[$liv]["cod_nota"] = "{$desc5} [{$liv}]";
		            }
		         
		         $s_ar[$liv]["classe"]   = $str_liv['f_liv5'];
		                        
		    }
		    
		    sum_values($s_ar[$liv], $row);
		    $s_ar = &$s_ar[$liv][$n_children];
		    
		}
		
		if (count($str_liv) > 6) {
		    // LIVELLO 6
		    $liv 	= $liv6_v;
		    $tmp_ar_id[] = $liv;
		    if (is_null($s_ar[$liv])){
		        $s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_6", "task"=> $liv);
		        if($task6 == '')
		            $s_ar[$liv]["task"] = 'Non definito';
		            else
		                $s_ar[$liv]["task"]   = $task6; //$task0. " [" . $liv . "]";
		                
		                if($liv == ''){
		                    $s_ar[$liv]["codice"] = "[ND]";
		                    $s_ar[$liv]["cod_nota"] = "{$desc6} [ND]";
		                }else{
		                    $s_ar[$liv]["codice"]   = " [" . $liv . "]";
		                    $s_ar[$liv]["cod_nota"] = "{$desc6} [{$liv}]";
		                }
		                
		                $s_ar[$liv]["classe"]   = $str_liv['f_liv6'];
		                
		    }
		    
		    sum_values($s_ar[$liv], $row);
		    $s_ar = &$s_ar[$liv][$n_children];
		    
		}
		
		if (count($str_liv) > 7) {
		    // LIVELLO 7
		    $liv 	= $liv7_v;
		    $tmp_ar_id[] = $liv;
		    if (is_null($s_ar[$liv])){
		        $s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_7", "task"=> $liv);
		        if(trim($task7) == '')
		            $s_ar[$liv]["task"] = 'Non definito';
	            else
	                $s_ar[$liv]["task"]   = $task7; //$task0. " [" . $liv . "]";
		                
                if($liv == ''){
                    $s_ar[$liv]["codice"] = "[ND]";
                    $s_ar[$liv]["cod_nota"] = "{$desc7} [ND]";
                }else{
                    $s_ar[$liv]["codice"]   = " [" . $liv . "]";
                    $s_ar[$liv]["cod_nota"] = "{$desc7} [{$liv}]";
                }
                
                $s_ar[$liv]["classe"]   = $str_liv['f_liv7'];
		                
		    }
		    
		    sum_values($s_ar[$liv], $row);
		    $s_ar = &$s_ar[$liv][$n_children];
		    
		}
		
		if($_REQUEST['node'] == 'root'){
			continue;
		}
		
		$tmp_ar_id[] = trim($row['ARART']);
		
	    if(trim($row['ARUMCO']) == trim($row['ARUMTE']) && trim($row['ARUMCO']) == trim($row['ARUMAL'])){
		    $um = $row['ARUMTE'];
	    } elseif((trim($row['ARUMAL']) == trim($row['ARUMTE']) && trim($row['ARUMAL']) != trim($row['ARUMCO'])) ||
	       (trim($row['ARUMAL']) == trim($row['ARUMCO']) && trim($row['ARUMAL']) != trim($row['ARUMTE']))){
		   $um = $row['ARUMTE']. " - " . trim($row['ARUMCO']);
		} else{
		    $um = trim($row['ARUMTE']) . " - " . trim($row['ARUMCO']) . " - " . trim($row['ARUMAL']);
		}
		
		$codice = trim($row['ARART']);
		$ha_commenti = $row['NR_ART_COMM'] > 0 ? true : false;
		
		
		if ($ha_commenti == FALSE)
		    $img_com_name = "icone/16x16/comment_light.png";
		else
		    $img_com_name = "icone/16x16/comment_edit_yellow.png";
		        
		
	   //spostato in colonna dedicata    
	   ////$codice .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_bl_articolo(\'' . trim($row['ARART']) . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
		    
		
		//il codice per la distinta base puo' essere in ARDBFB, altrimenti e' lo stesso codice articolo
		if (trim($row['ARDBFB']) == ''){
		    $d_base_art = trim($row['ARART']);
		}else{
		    if(in_array(trim($row['ARTPAR']), array('M', 'R')))
		      $d_base_art = trim($row['ARART']);
	        else
		      $d_base_art = trim($row['ARDBFB']);
		}
		
		//il codice per il ciclo puo' essere in ARDBCO, altrimenti e' lo stesso codice articolo
		if (trim($row['ARDBCO']) == '')
		    $ciclo_art = trim($row['ARART']);
		else
		    $ciclo_art = trim($row['ARDBCO']);
		
		
		    
		$qtip_cfg 	=  "[".trim($row['ARMODE'])."] ". trim($row['ARMODE_D']);
		   		
		$attav_chiuse = $row['NR_OPEN_TODO'] > 0 ? '' : 'Y';
		
		if ($row['N_ATT'] > 0){
		  $tooltip = $sa->stato_tooltip_entry_per_ordine(trim($row['ARART']), null, 'N', 'ART', 'Y');
		} else {
		  $tooltip = array('tooltip' => array());
		}
		   
		
		$ar_row = array(
				'id' => implode("|", $tmp_ar_id),
		        'task' => htmlentities(trim($row['ARDART'])),
		        'cod_nota'  => $codice,
		        'codice'  => trim($row['ARART']),
		        'um' => $um,
				'data_ins'   => trim($row['ARDTGE']),
				'config'   => trim($row['ARMODE']),
		        'qtip_cfg'   => $qtip_cfg,
		        'dim1'   => $row['ARDIM1'],
		        'dim2'   => $row['ARDIM2'],
		        'dim3'   => $row['ARDIM3'],
		        'sosp'   => trim($row['ARSOSP']),
		        'esau'   => trim($row['ARESAU']),
		        'note_ra'   => trim($row['NR_RL']),
		        'barcode'   => trim($row['NR_BC']),
		        'note'   => $ha_commenti,
		        'allegati' => trim($row['NR_AL']),
		        'l_ven' => trim($row['NR_LV']),
		        'l_acq' => trim($row['NR_LA']),
		        'd_base' => trim($row['NR_DB']),
		        'd_base_art' => $d_base_art,
		        'ciclo_art' => $ciclo_art,
		        'd_base_f' =>trim($row['NR_DBF']),
    		    'cicli' => trim($row['NR_CL']),
		        'trad'   => trim($row['N_TRAD']),
		        'attav'   => $row['N_ATT'],
		        'attav_chiuse'  => $attav_chiuse,
		        'tooltip_attav'  => $tooltip['tooltip'],
		        'var'   => trim($row['NR_VAR']) + $row['NR_PROP'],
		        'abar'   => trim($row['NR_AB']),
		        'schede' => $row['NR_SCHEDE'] + $row['FSC'],
		        'tipo_scheda' => $form_ep->f_schede,
		        'sf_g' => trim($row['AXSFON']),
		        'sf_p' => trim($row['AUSFAR']),
        	    'ARTPAR' => trim($row['ARTPAR']),
        	    'ARCLME' => trim($row['ARCLME']),
        	    'ARGRME' => trim($row['ARGRME']),
        	    'ARSGME' => trim($row['ARSGME']),
        	    'ARTAB1' => trim($row['ARTAB1']),
        	    'ARFOR1' => trim($row['ARFOR1']),
		        'ARARFO' => trim($row['ARARFO']),
		        'ARGRAL' => trim($row['ARGRAL']),
		        'ARSWTR' => trim($row['ARSWTR']),
		        'ARRIF' => trim($row['ARRIF']),
		        'rif_for' => trim($row['ARARFO']),
		        'D_FOR'  => trim($row['D_FORNITORE']), 
		        'appunti'  => trim($row['APPUNTI']), 
		        'rrn_appunti'  => trim($row['RRN_APPUNTI']), 
		        'nr' => $count++,
		        'leaf' => true
		);
		
		
		$count_rows++;
		
		$s_ar[] = $ar_row;
		
	}
	
	if (isset($art_per_pagination))	    
	if (isset($_REQUEST['m_offset']))
	  $m_offset = (int)$_REQUEST['m_offset'] +1;
	else
	  $m_offset = 1;
	            
	  
	if($_REQUEST['node'] != 'root' && !$m_params->recenti > 0)
	if ($rowCount['C_ROW'] > ($art_per_pagination * $m_offset)){
	   $count_visualizzati = $art_per_pagination * $m_offset;
	   $s_ar[] = array(
	     'id' => "carica_successivi_".microtime(true),
	     'liv' => 'liv_succ',
	     'm_offset' => $m_offset,
	     'task' => "<b><FONT color=red>** LISTA NON COMPLETA. CARICA SUCCESSIVI **<br><br><center>(Visualizzati {$count_visualizzati} su {$rowCount['C_ROW']})<br>&nbsp;</center></font></b>",
	     'leaf' => true);
	}
	
	
	//scrittura risultato
	$ret = array();
	
	foreach($ar as $kar => $v){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	//per sottoalbero
	//if($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
	if($_REQUEST['node'] != 'root'){
	    
	    $s_ar = &$ret;
	    foreach($livelli as $k => $v){
	        $s_ar = &$s_ar[0]['children'];
	    }
	    $ret = &$s_ar;
	}
	
	
	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
	
	
	
exit;
}


if ($_REQUEST['fn'] == 'open_tab'){
$m_params = acs_m_params_json_decode();
?>
{
 "success":true, "items":
 { 
  		xtype: 'treepanel',
	    title: 'Anagrafica articoli',
	    <?php echo make_tab_closable(); ?>,
		stateful: true,
        stateId: 'panel-anag-art',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
       /* selModel	: 'spreadsheet',	
        plugins: [
		          Ext.create('Ext.grid.plugin.Clipboard', {
		          //  clicksToEdit: 1,
		          })
		      	],*/
      	plugins: [
          Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            listeners:{
	        	afteredit: function(cellEditor, context, eOpts){
		        	var value = context.value;
		        	var grid = context.grid;
		        	var record = context.record;			        	
	        	   	Ext.Ajax.request({
			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_nota',
			            method: 'POST',
	        		    jsonData: {
			      			   nota : value,
							   rec : record.data,
							   blocco : <?php echo j($m_params->form_open->f_appunti)?>					      			  
			      			   }, 					            
			            success: function ( result, request) {
			                var jsonData = Ext.decode(result.responseText);
			                record.set('appunti', value);
			   				record.commit();		                															
			            },
			            failure: function (result, request) {
			            }
			        });
	        	}
			 }
          })
      	],
	    tbar: new Ext.Toolbar({
	          items:['<b>Anagrafica articoli</b>', '->',
	            
	             {iconCls: 'icon-gear-16',
	             text: 'PARAMETRI', 
		           		handler: function(event, toolEl, panel){
			           		var m_form_win = Ext.getCmp('<?php echo $m_params->win_id; ?>');
			           		m_form_win.fromObjectId = this.up('panel').getId();
			           		m_form_win.show();
			          
		           		 
		           		 }
		           	 }
		           	 
					, {iconCls: 'tbar-x-tool x-tool-prev',
		            	tooltip: 'reset impostazioni di visualizzazione', 
		            	handler: function(event, toolEl, panel){
		            	var m_comp = this.up('panel');
		            	Ext.Msg.confirm('Richiesta conferma', 'Confermi il reset delle impostazioni di visualizzazione attualmente impostate?<br/>(la visualizzazione attuale verr&agrave; chiusa)', function(btn, text){
							if (btn == 'yes'){
            		            Ext.state.Manager.clear(m_comp.stateId);
            					m_comp.destroy();
            				}
            			});		
		            }},		           	 
		           	 
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', 
	           	   handler: function(event, toolEl, panel){ 
	           	   	this.up('panel').getStore().load();
	           	   		}
	           	   }
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	      
	        
	        //cls: 'elenco_ordini arrivi',
	        collapsible: true,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'liv', 'm_offset', 'descrizione', 'um', 'data_ins', 'config', 'qtip_cfg' ,
				     'codice', 'c_art', {name : 'dim1', type : 'float'}, {name : 'dim2', type : 'float'}, {name : 'dim3', type : 'float'},
				      'sosp', 'esau', 'classe', 'rrn_appunti',
				     'liv_cod', 'allegati', 'l_ven', 'l_acq', 'd_base', 'd_base_art', 'ciclo_art', 'd_base_f', 'cicli', 'appunti', 'barcode',
				     'trad', 'cod_nota', 'attav', 'attav_chiuse', 'tooltip_attav', 'var', 'abar', 'sf_g', 'nr', 'sf_p', 'schede', 'tipo_scheda',
				      'ARTPAR', 'ARCLME', 'ARGRME', 'ARSGME', 'ARTAB1', 'ARFOR1', 'D_FOR', 'note', 'note_ra', 'ARARFO', 'ARGRAL', 'ARSWTR', 'ARRIF', 'rif_for'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
                        timeout: 2400000,
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                      extraParams: {
	                      form_ep: <?php echo acs_je($m_params->form_open); ?>,
	                      livelli: <?php echo acs_je($m_params->livelli); ?>,
	                      radice : '<?php echo $m_params->radice;?>',
	                      recenti : '<?php echo $m_params->recenti; ?>'
                      }
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),
	        multiSelect: true,
	        singleExpand: false,
	        
	        <?php $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>	
	        <?php $cf2 = "<img src=" . img_path("icone/48x48/camera.png") . " height=20>"; ?>
	        <?php $cl = "<img src=" . img_path("icone/48x48/tools.png") . " height=20>"; ?>
	        <?php $lv = "<img src=" . img_path("icone/48x48/currency_black_euro.png") . " height=20>"; ?>
	        <?php $la = "<img src=" . img_path("icone/48x48/currency_blue_dollar.png") . " height=20>"; ?>
	        <?php $db_p = "<img src=" . img_path("icone/48x48/folder_download.png") . " height=20>"; ?>
	        <?php $db_f = "<img src=" . img_path("icone/48x48/folder_upload.png") . " height=20>"; ?>
	        <?php $cfg = "<img src=" . img_path("icone/48x48/game_pad.png") . " height=20>"; ?>
	        <?php $trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>"; ?>
	        <?php $attav = "<img src=" . img_path("icone/48x48/arrivi.png") . " height=20>"; ?>
	        <?php $var = "<img src=" . img_path("icone/48x48/sticker_blue.png") . " height=20>"; ?>
	        <?php $abar = "<img src=" . img_path("icone/48x48/exchange.png") . " height=20>"; ?>
	        <?php $schede = "<img src=" . img_path("icone/48x48/tag.png") . " height=20>"; ?>
	        <?php  $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>"; ?>
	
			columns: [	
	    		{xtype: 'treecolumn', 
	    		text: 'Descrizione', 	
	    		flex: 1, 
	    		dataIndex: 'task', 
	    		tdCls: 'auto-height rif white-space-pre font-monospace',
	    		renderer: function(value, metaData, record){
    				
      	  				if (record.get('sf_p') == '1') metaData.tdCls += ' sfondo_rosso'; 
    			    	if (record.get('sf_p') == '2') metaData.tdCls += ' sfondo_giallo';
    			    	if (record.get('sf_p') == '3') metaData.tdCls += ' sfondo_verde';						
    				
    				return value;	
				}
	    		},
	    		{text: '<?php echo $cf1; ?>', 	
				width: 30, 
				dataIndex: 'sosp',
				tooltip: 'Sospeso',		        			    	     
		    	renderer: function(value, metaData, record){
		    			  if(record.get('sosp') == 'S'){ 
		    			   metaData.tdCls += ' sfondo_rosso';
		    			   return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	}
		    		 	
		    		 	if(record.get('note_ra') > 0)  metaData.tdCls += ' sfondo_grigio_scuro';
		    		 
		    		  }
		        },
	    	    {text: 'Codice', 
	    	     width: 150, 
	    	     dataIndex: 'cod_nota',
	    	     tdCls: 'auto-height rif white-space-pre font-monospace',
	    	     renderer: function(value, metaData, record){
    				if (record.get('liv') == '')
      	  				 
      	  				if (record.get('sf_g') == '1') metaData.tdCls += ' sfondo_rosso'; 
    			    	if (record.get('sf_g') == '2') metaData.tdCls += ' sfondo_giallo';
    			    	if (record.get('sf_g') == '3') metaData.tdCls += ' sfondo_verde';						
    				
    				return value;	
				}},
				 
				{
				    text: '<?php echo $note; ?>',
				    width: 32, 
				    tooltip: 'Blocco note',
				    tdCls: 'tdAction',  
				    dataIndex : 'note',       		       		        
					renderer: function(value, metaData, record){
    					if(record.get('leaf')){
    						if (record.get('note') == true) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
    			    		if (record.get('note') == false) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
    			    	    if (record.get('note') == 'N') return '';    			    	   
    			    	}    		
			    	}
			    },				
				
				
				<?php if($m_params->form_open->f_appunti != ''){?>
				{
				    text: 'Appunti [' + <?php echo j($m_params->form_open->f_appunti); ?> + ']',
				    width: 90, 
				    dataIndex : 'appunti',  
				    tdCls: 'auto-height rif white-space-pre font-monospace',
				     editor: {
    	                xtype: 'textfield',
    	                allowBlank: true,
    	                maxLength : 80
	               }     		       		        
		 		
			    },	
				<?php }?>
				
				{
				 text: 'Riferimento', 
	    	     width: 75, 
	    	     dataIndex: 'ARARFO',
				 <?php if($m_params->form_open->f_cod_rife != 'Y'){?>
				 hidden : true
				 <?php }?>
				},{
				 text: 'Rif.tecnico', 
	    	     width: 75, 
	    	     dataIndex: 'ARRIF',
				 <?php if($m_params->form_open->f_rif_tec != 'Y'){?>
				 hidden : true
				 <?php }?>
				},{
				 text: 'Alternativo', 
	    	     width: 75, 
	    	     dataIndex: 'ARGRAL',
				 <?php if($m_params->form_open->f_cod_alt != 'Y'){?>
				 hidden : true
				 <?php }?>
				},
				
				{
				 text: 'Riordino', 
	    	     width: 75, 
	    	     dataIndex: 'ARSWTR',
				 <?php if($m_params->form_open->f_riordino != 'Y'){?>
				 hidden : true
				 <?php }?>
				},
				
				 {text: '<?php echo $attav; ?>', 	
				width: 30,
				<?php if($m_params->form_open->f_att != 'Y'){?>
				hidden : true, 
				<?php }?>
				align: 'center', 
				dataIndex: 'attav',
				tooltip: 'Attivit&agrave;',		        			    	     
		    	renderer: function(value, metaData, record){
		    	   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_attav')) + '"';
		    	   if(record.get('attav') > 0){ 
		    	       if(record.get('attav_chiuse') == 'Y') return '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=15>';
		    	   	   else return '<img src=<?php echo img_path("icone/48x48/arrivi.png") ?> width=15>';
		    	   } 
		    	  }
		        },
	    	    {text: '<?php echo $cf2; ?>', 	
				width: 30,
				<?php if($m_params->form_open->f_allegati != 'Y'){?>
				hidden : true, 
				<?php }?>
				align: 'center', 
				dataIndex: 'allegati',
				 tdCls : ' sfondo_cl1',
				tooltip: 'Allegati',		        			    	     
		    	renderer: function(value, p, record){
		    			  if(record.get('allegati') > 0) return '<img src=<?php echo img_path("icone/48x48/camera.png") ?> width=15>';
		    		  }
		        },
				{text: '<?php echo $lv; ?>', 
				width: 30, 
				dataIndex: 'l_ven',
				<?php if($m_params->form_open->f_list_ven != 'Y'){?>
				hidden : true, 
				<?php }?>
				tooltip: 'Listini vendita',
				renderer: function(value, p, record){
	    			  if(record.get('l_ven') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_black_euro.png") ?> width=15>';
	    		  }},
				{text: '<?php echo $la; ?>', 
				<?php if($m_params->form_open->f_list_acq != 'Y'){?>
				hidden : true, 
				<?php }?>
				width: 30, 
				tdCls : ' sfondo_cl2',
				dataIndex: 'l_acq',
				tooltip: 'Listini acquisto',
				renderer: function(value, p, record){
	    			  if(record.get('l_acq') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_blue_dollar.png") ?> width=15>';
	    		  }},
				
				<?php if($m_params->from_pv != 'Y'){?>
				{text: '<?php echo $db_p; ?>',
				 width: 30, 
				
				 <?php if($m_params->form_open->f_dist != 'Y'){?>
				hidden : true, 
				<?php }?>
				 dataIndex: 'd_base',
				 tooltip: 'Distinta componenti',
				 renderer: function(value, p, record){
	    			  if(record.get('d_base') > 0) return '<img src=<?php echo img_path("icone/48x48/folder_download.png") ?> width=15>';
	    		  }
				 },{text: '<?php echo $db_f; ?>',
				 width: 30, 
				 <?php if($m_params->form_open->f_elenco != 'Y'){?>
				 hidden : true, 
				<?php }?>
				tdCls : ' sfondo_cl3',
				 dataIndex: 'd_base_f',
				 tooltip: 'Elenco padri',
				 renderer: function(value, p, record){
	    			  if(record.get('d_base_f') > 0) return '<img src=<?php echo img_path("icone/48x48/folder_upload.png") ?> width=15>';
	    		  }
				 },
		        {text: '<?php echo $cl; ?>', 
	        	width: 30, 
	        	 <?php if($m_params->form_open->f_cicli != 'Y'){?>
				 hidden : true, 
				<?php }?>
	        	dataIndex: 'cicli',
	        	tooltip: 'Cicli/lavorazioni',
	        	renderer: function(value, p, record){
	    			  if(record.get('cicli') > 0) return '<img src=<?php echo img_path("icone/48x48/tools.png") ?> width=15>';
	    		  }
		        	},
		        <?php }?>	
		        	
		        	{text: '<?php echo $cfg; ?>', 	
				 width: 30, 
				 <?php if($m_params->form_open->f_conf != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				dataIndex: 'config',
				tdCls : ' sfondo_cl2',
				tooltip: 'Configuratore',
				renderer: function(value, metaData, record, row, col, store, gridView){
				      if(record.get('qtip_cfg') != "")	    			    	
							metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_cfg')) + '"';
	    			  if(record.get('config') != "") return '<img src=<?php echo img_path("icone/48x48/game_pad.png") ?> width=15>';
	    		  
	    		      
	    		  
	    		  
	    		  }
	    		},{text: '<?php echo $trad; ?>', 	
				width: 30, 
				 <?php if($m_params->form_open->f_trad != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				dataIndex: 'trad',
				tooltip: 'Traduzione',
				renderer: function(value, metaData, record){
				  if(record.get('trad') > 0){
	    			   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('[#' +record.get('trad')+']') + '"';
    				   return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=15>';
        		  }
        	  
	    		  }
	    		},
	    	   {text: '<?php echo $var; ?>', 	
	    	    <?php if($m_params->form_open->f_var != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				width: 30, 
				dataIndex: 'var',
				tdCls : ' sfondo_cl1',
				tooltip: 'Varianti anagrafiche',
				renderer: function(value, p, record){
	    			  if(record.get('var') > 0) return '<img src=<?php echo img_path("icone/48x48/sticker_blue.png") ?> width=15>';
	    		  }
	    		},{text: '<?php echo $abar; ?>', 	
				width: 30, 
				 <?php if($m_params->form_open->f_abb != 'Y'){?>
				 hidden : true, 
				 <?php }?>
				dataIndex: 'abar',
				tooltip: 'Regole abbinamento articoli',
				renderer: function(value, p, record){
				      if(record.get('barcode') > 0) p.tdCls += ' sfondo_giallo';
	    			  if(record.get('abar') > 0) return '<img src=<?php echo img_path("icone/48x48/exchange.png") ?> width=15>';
	    		  }
	    		},
	    		{text: '<?php echo $schede; ?>', 	
				width: 30, 
				dataIndex: 'schede',
				tooltip: 'Scheda',
				renderer: function(value, p, record){
				      var p_schede = this.store.proxy.extraParams.form_ep.f_schede;
				      if(record.get('liv') == ''){
	    			   if(record.get('schede') > 0){
	    			       if(!Ext.isEmpty(p_schede))
	    			       	return '<img src=<?php echo img_path("icone/48x48/tag.png") ?> width=15>';
	    			       else
	    			        return '<img src=<?php echo img_path("icone/16x16/tag_grey.png") ?> width=15>';
	    			 	}	
	    		 	}
	    		  }
	    		},
	    	    {text: 'L', width: 50, dataIndex: 'dim1',
	    	     renderer: function(value, metaData, record){
					if (record.get('dim1') == 0) 
						return '';									
					return addSeparatorsNF(parseFloat(value), '.', ',', '.');
							}, 
	    	     align: 'right'},
	    	    {text: 'H', width: 50, dataIndex: 'dim2', align: 'right',
	    	     renderer: function(value, metaData, record){
					if (record.get('dim1') == 0) 
						return '';									
					return addSeparatorsNF(parseFloat(value), '.', ',', '.');
							}
	    	    },
	    	    {text: 'S', width: 50, dataIndex: 'dim3', align: 'right',
	    	     renderer: function(value, metaData, record){
					if (record.get('dim1') == 0) 
						return '';									
					return addSeparatorsNF(parseFloat(value), '.', ',', '.');
							} },
	    	    {text: 'UM', width: 80, dataIndex: 'um',
	    	      renderer: function(value, metaData, record){
					if (record.get('liv') != '') 
						value =  '[#' +record.get('c_art') + ']';									
					return value;	
							}
	    	   
	    	    },
				{text: 'Data ins.',  width: 70, dataIndex: 'data_ins', renderer: date_from_AS},
				
				{text: 'C', 
				tooltip: 'Ciclo di vita',	 	
				width: 30, 
				dataIndex: 'esau',
				renderer: function(value, metaData, record, row, col, store, gridView){
    		    	if (record.get('esau') == 'E'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In esaurimento') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/clessidra.png") ?> width=15>';}
					if (record.get('esau') == 'A'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In avviamento') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';}
				    if (record.get('esau') == 'C'){	    			    	
					   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('In codifica') + '"';
					   return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=15>';}
				   if (record.get('esau') == 'R'){	    			    	
			   		   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Riservato') + '"';
			   		   return '<img src= <?php echo img_path("icone/48x48/folder_private.png") ?> width=15>';}
				   if (record.get('esau') == 'F'){	    			    	
			   		   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Fine codifica') + '"';
			   		   return '<img src= <?php echo img_path("icone/48x48/button_blue_play.png") ?> width=15>';}
				    
				    //return value;
				    }
				}

				, <?php echo dx_mobile() ?>

				],
			enableSort: false, // disable sorting

	        listeners: {
        	     /*sortchange : function(ct, column, direction){
        	           var allChildNodes = this.store.getRootNode().childNodes;
        	           for (var i=0; i<allChildNodes.length; i++){
        	               console.log(allChildNodes[i]);
        	           }
  			 
                  },*/
                  
        			beforestaterestore: function(comp, state){
        				//ESCLUDO SEMPRE LA MEMORIZZAZIONE DALLA STATEEVENTS
        				delete state.sort;
        			},
        	        
	                 beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },
                     load: function () {
                          Ext.getBody().unmask();
                        },
                     itemcontextmenu : function(grid, rec, node, index, event) {
                        console.log('on itemcontextmenu');
				  		event.stopEvent();
				  		var grid = this;
				  		//articolo = '';
				  													  
						  var voci_menu = [];
					      
					      id_selected = grid.getSelectionModel().getSelection();							
						  list_selected_id = [];
						  for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push(id_selected[i].data.codice);
									
					      
					      if(rec.get('liv') == ''){
					      
					       voci_menu.push({  
					        iconCls : 'icon-barcode',     
					       	text: 'Articoli selezionati: ' + id_selected.length
					       });
					      
					       voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		
			        		 list_rows = [];
						      for (var i=0; i<id_selected.length; i++) 
							    list_rows.push({c_art : id_selected[i].data.codice, 
							    			   flag : id_selected[i].data.sosp});
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				list_rows : list_rows
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          var ar = jsonData.new_value;
								             
								             for (var i=0; i<id_selected.length; i++){
								                
								                if(id_selected[i].data.codice == ar[i].codice)
								                 id_selected[i].set('sosp', ar[i].flag);
								             }
								          
								          
					            		  //grid.getStore().load();
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
			    				
			    				var voci_menu_raffronta = [];
			    				voci_menu_raffronta.push(
			    				       {  text: 'Seleziona per raffronto',
                                              iconCls : 'icon-sub_blue_accept-16', 
                                               handler : function(){
                                                   cod_art_raffronto = rec.get('codice');
                                              }
                                         }
			    				 );
			    				  if(typeof(cod_art_raffronto) !== 'undefined' && !Ext.isEmpty(cod_art_raffronto)){
                                           voci_menu_raffronta.push(
        			    				       {  text: 'Raffronta con articolo ' + cod_art_raffronto,
                                               iconCls : 'icon-sub_blue_accept-16', 
                                               handler : function(){
                                                 list_selected_id
                                                 	Ext.Ajax.request({
                         						        url     : 'acs_raffronta_articoli.php?fn=exe_raffronta',
                         						        method  : 'POST',
                         			        			jsonData: {
                         			        			   list_selected_id : list_selected_id,
                         			        			   cod_art_raffronto : cod_art_raffronto
                         								},
                         								
                         								success: function(response, opts) {
                        						        	 var jsonData = Ext.decode(response.responseText);
                        						        	
                        						        	  acs_show_win_std(null,
                                    			    				'acs_raffronta_articoli.php?fn=open_grid', {
                                    			    					ritime : jsonData.RITIME,
                                    			    					cod_art_raffronto : cod_art_raffronto,
                                    			    					list_selected_id : list_selected_id
                                    		    				 });	       	 
                        						        	      		   
                        			            		},
                        						        failure    : function(result, request){
                        						            Ext.Msg.alert('Message', 'No data to be loaded');
                        						        }
                         								
                         						    });
                                               }
                                            }
        			    				 );
                                         
                                     }
			    			
        			    		voci_menu.push({
        			         		text: 'Raffronta',
        			        		iconCls : 'icon-folder_search-16',  
        			        		menu:{
                                    	items: voci_menu_raffronta
                       				 }
        			        		
        			    		});
			    		    
			    				
			    				
			    				
			    				
			    				
					      
					      if(list_selected_id.length > 1){
					      	 voci_menu.push({
			         		text: 'Modifica attributi',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        			acs_show_win_std('Dettaglio articolo', 'acs_win_dettaglio_articoli.php?fn=open_win', 
		  				    	 {g_multi : 'Y', multi_selected_id : list_selected_id}, 700, 600, null, 'icon-leaf-16');        	
					        }
			    		});
			    		
			    		}
			    		
			    		
			    		
			    		
			    		/* TEST : ToDo da indice */
			    		/*
    			    		voci_menu.push({
    			         		text: 'Test: assegna ToDo da indice',
    			        		iconCls : 'icon-blog_compose-16',          		
    			        		handler: function () {
    			        		       		     
            			        	var list_selected_art = [];
            			        	
                                  	for (var i=0; i<id_selected.length; i++){ 
        				              list_selected_art.push(id_selected[i].get('codice'));            				        	
        				            }
    			        					        		
    			        			var my_listeners = {
    		        					afterConfirm: function(from_win){	    		        					    
    		        						from_win.close();
    		        						}
    				    				};    				    			
    				    		
    				    			acs_show_win_std('Attivit&agrave; da assegnare agli articoli generati', 
    				    				'acs_anag_art_create_todo_from_indici.php?fn=open_filtri', {
    				    					cod: 'WIZDUPART',
    				    					list_selected_art : list_selected_art
    				    			     }, 1200, 300, my_listeners, 'icon-blog_compose-16');
    				            } //handler
    			    		});
    			    	*/					    		
			    		
			    		
			    		
			    		
			    		
			    		
			    		
			    		if(list_selected_id.length > 1){
			    		
    			    		voci_menu.push({
    			         		text: 'Duplica assistita articoli',
    			        		iconCls : 'icon-blog_compose-16',          		
    			        		handler: function () {
    			        		       		     
            			        	list_selected_art = [];
            			        	
                                      for (var i=0; i<id_selected.length; i++){ 
            				            list_selected_art.push({
            				            	c_art: id_selected[i].get('codice'),
            				            	d_art: id_selected[i].get('task'),
            				            	sosp : id_selected[i].get('sosp'),
            				            	esau : id_selected[i].get('esau')            
            				            	});
            				        	
            				           }
    			        					        		
    			        			my_listeners = {
    		        					afterConfirm: function(from_win){	
    		        					    
            		        				/*grid.getStore().load();
    		        						from_win.close();*/
    		        						}
    				    				};
    				    				
    				    		
    				    	acs_show_win_std('Duplica assistita articoli', 'acs_anag_art_duplica_ass.php?fn=open_mod', {list_selected_art : list_selected_art}, 1200, 300, my_listeners, 'icon-blog_compose-16');
    				    			
    				    				
    			        		
    					    		 
    				                }
    			    		});	
    			    		
    			    			
			    		
        			  		voci_menu.push({
    			         		text: 'Inserisci/modifica scheda',
    			        		iconCls : 'icon-tag-16',          		
    			        		handler: function () {
    			        		    list_selected_art = [];
            			        	for (var i=0; i<id_selected.length; i++)
            				            list_selected_art.push(id_selected[i].get('codice'));
            				   	        		
    			        			my_listeners = {
    		        					afterConfirm: function(from_win){	
    		        					    grid.getStore().load();
    		        						from_win.close();
    		        						}
    				    				};
    				    			
    				    	var title = '[Articoli selezionati: '+list_selected_art.length +']';			
    				    	acs_show_win_std('Seleziona scheda ' + title, 'acs_anag_art_schede_aggiuntive_new.php?fn=open_form', {list_selected_art : list_selected_art, l_insert: 'Y'}, 500, 400, my_listeners, 'icon-tag-16');	
    				            }
    			    		});
    			    		
    			    		
    			    		voci_menu.push({
    			         		text: 'Inserisci blocco note',
    			        		iconCls : 'iconCommYellow',          		
    			        		handler: function () {
    			        		    list_selected_art = [];
            			        	for (var i=0; i<id_selected.length; i++)
            				            list_selected_art.push(id_selected[i].get('codice'));
            				   	        		
    			        			my_listeners = {
    		        					afterConfirm: function(from_win){	
    		        					    grid.getStore().load();
    		        						from_win.close();
    		        						}
    				    				};
    				    			
    				    	var title = '[' + list_selected_art.length + ']';			
    				    	acs_show_win_std('Inserisci/Modifica blocco note sugli articoli selezionati ' + title , 
    				    		'acs_anag_art_note.php?fn=open_bl', {
    				    				multi: 'Y',
    				    				list_selected_art : list_selected_art, 
    				    				l_insert: 'Y'}, 600, 500, my_listeners, 'iconCommYellow');	
    				            }
    			    		});
			    		
			    		
			    		
			    		} else {
			    		
			    		//selezionato un singolo articolo
			    		
			    		
			    		 voci_menu.push({
			         		text: 'Copia codice articolo',
			        		iconCls : 'icon-copy-16',          		
			        		handler: function () {
			        		   cod_art_copiato = rec.get('codice');
			        		
			        		   const el = document.createElement('textarea');
                    	  		el.value = rec.get('codice');
                    	  		document.body.appendChild(el);
                    	  		el.select();
                    	  		document.execCommand('copy');
                    	  		document.body.removeChild(el);
                    	  		
					        }
			    		});
			    		
			    	
			    		
			    			 voci_menu.push({
			         		text: 'Duplica articolo',
			        		iconCls : 'icon-comment_add-16',          		
			        		handler: function () {
			        		
			        				if(rec.get('sosp') == 'S'){
			        				Ext.Msg.confirm('Richiesta conferma', 'Attenzione articolo sospeso! <br> Confermi duplica articolo come Attivo?' , function(btn, text){
            	   				if (btn == 'yes'){
     			           		    acs_show_win_std('Duplica articolo', 
				                			'acs_anag_art_duplica.php?fn=open_form', {
				                             d_art: rec.get('task'),
				                             c_art: rec.get('codice'),
									}, 500, 390, {}, 'icon-comment_add-16');
		           
				    		}
				    		 });
			        				
			        				}else{
			        				
			        				  acs_show_win_std('Duplica articolo', 
				                			'acs_anag_art_duplica.php?fn=open_form', {
				                             d_art: rec.get('task'),
				                             c_art: rec.get('codice'),
									}, 500, 390, {}, 'icon-comment_add-16');
			        				
			        				}
			        		
					    		
				                }
			    		});	 
			    	
			    		
			    		} //singolo articolo selezionato
			    		
			    		voci_menu.push({
    			         		text: 'Duplica cond.magg. vendite',
    			        		iconCls : 'icon-sub_blue_add-16',          		
    			        		handler: function () {
    			        		       		     
            			        	list_selected_art = [];
            			        	
                                      for (var i=0; i<id_selected.length; i++){ 
            				            list_selected_art.push({
            				            	c_art: id_selected[i].get('codice'),
            				            	d_art: id_selected[i].get('task')           
            				            	});
            				        	
            				           }
    			        					        		
    			        			my_listeners = {
    		        					afterConfirm: function(from_win){	
    		        					   from_win.close();
    		        						}
    				    				};
    				    				
    				    		
    				    	acs_show_win_std('Duplica condizioni', 'acs_anag_art_duplica_condizioni.php?fn=open_form', {list_selected_art : list_selected_art}, 500, 250, my_listeners, 'icon-sub_blue_add-16');
    				    			
    				    				
    			        		
    					    		 
    				                }
    			    		});
		
			    		
			    		<?php if($m_params->from_pv != 'Y'){?>
			    		
			    	my_listeners_inserimento = {
		        					afterInsertRecord: function(from_win){	
		        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
		        						//rec.store.treeStore.load({node: rec.parentNode});		        						
		        						from_win.close();
						        		}
				    				};	
			    		
			    		  voci_menu.push({
						         		text: 'Inserimento nuovo stato/attivit&agrave;',
						        		iconCls : 'icon-arrivi-16',          		
						        		handler: function() {
										acs_show_win_std('Nuovo stato/attivit&agrave;', 
											<?php echo j('acs_anag_art_create_attivita.php'); ?>, 
											{	tipo_op: 'ANART',
												rif : 'ART',
								  				list_selected_id: list_selected_id
								  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
								  							        		          		
						        		}
						    		});
						    		
						    		
						    	if(list_selected_id.length == 1){
						  		  voci_menu.push({
						         		text: 'Gestione stato/attivit&agrave; per articolo',
						        		iconCls : 'icon-arrivi_gray-16',          		
						        		handler: function() {
										acs_show_panel_std( 
											<?php echo j('acs_panel_todolist.php?fn=open_panel'); ?>,
											null, {c_art: rec.get('codice')} 
											);						        		          		
						        		}
						    		});
						    	}	
						    		
						    		<?php }?>
						    		
						   voci_menu.push({
			         		text: 'Aggiornamento classificazioni',
			        		iconCls : 'icon-pencil-16',          		
			        		handler: function () {
			        		
			        		    
			        		     list_selected_art = [];
                                   
                                  for (var i=0; i<id_selected.length; i++){ 
        				            list_selected_art.push(id_selected[i].data);
        				           }
			        		
			        			my_listeners = {
		        					afterModCls: function(from_win){	
		        						grid.getStore().load();		        						
		        						from_win.close();
						        		}
				    				};
			        		
					    		  acs_show_win_std('Aggiornamento classificazioni', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_mod_cla', {list_selected_art : list_selected_art}, 600, 300, my_listeners, 'icon-pencil-16');
				                }
			    		});	
						    		
			    	voci_menu.push({
			         		text: 'Modifica descrizione',
			        		iconCls : 'icon-blog_compose-16',          		
			        		handler: function () {
			        		       		     
        			        	list_selected_art = [];
                                   
                                  for (var i=0; i<id_selected.length; i++){ 
        				            list_selected_art.push({
        				            	c_art: id_selected[i].get('codice'),
        				            	d_art: id_selected[i].get('task')       				            
        				            	});
        				           }
			        					        		
			        			my_listeners = {
		        					afterConfirm: function(list, from_win){	
		        						        					
		        					id_selected = grid.getSelectionModel().getSelection();	
		        					
		        					for (var i=0; i<id_selected.length; i++){
		        					 for(var a=0; a<list.length; a++) {
		        					 	if(id_selected[i].data.codice == list[a].cod)
		        					       id_selected[i].set('task', list[a].n_desc);  
								         }
		        					} 
		        					  
		        						from_win.close();
		        						
						        		}
				    				};
			        		
					    		  acs_show_win_std('Modifica descrizione', 'acs_anag_art_mod_desc_art.php?fn=open_mod', {list_selected_art : list_selected_art}, 1000, 300, my_listeners, 'icon-blog_compose-16');
				                }
			    		});			
						voci_menu.push({
			         		text: 'Evidenza generica',
			        		iconCls : 'icon-sub_blue_accept-16', 
			        		menu:{
                        	items:[
                                {  text: 'Rosso',
                                   iconCls : 'icon-sub_blue_accept-16',
                                   style: 'background-color: #F9BFC1;', 
                                   handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '1'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
    									},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },
                            	{  text: 'Giallo',
                            	   iconCls : 'icon-sub_blue_accept-16', 
                            	   style: 'background-color: #F4E287;',
                            	        handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '2'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                    text: 'Verde',
                                    iconCls : 'icon-sub_blue_accept-16', 
                                    style: 'background-color: #A0DB8E;',
                                         handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '3'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                	text: 'Nessuno',
                                	iconCls : 'icon-sub_blue_accept-16', 
                                	     handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_multi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : ''
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_g', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                }
                            ]
                        }        		
			        	
			    		});
			    		
			    		
			    		voci_menu.push({
			         		text: 'Evidenza per utente',
			        		iconCls : 'icon-user_only-16', 
			        		menu:{
                        	items:[
                                {  text: 'Rosso',
                                   iconCls : 'icon-user_only-16', 
                                   style: 'background-color: #F9BFC1;', 
                                   handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '1'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								        	for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },
                            	{  text: 'Giallo',
                            	   iconCls : 'icon-user_only-16',
                            	   style: 'background-color: #F4E287;', 
                            	        handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '2'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
										for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                    text: 'Verde',
                                    iconCls : 'icon-user_only-16', 
                                     style: 'background-color: #A0DB8E;',
                                         handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : '3'
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								         for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                },{
                                	text: 'Nessuno',
                                	iconCls : 'icon-user_only-16', 
                                	     handler : function(){
                                     Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_up_color_utente',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				flag : ''
										},							        
								        success : function(result, request){
								         var jsonData = Ext.decode(result.responseText);
								        for (var i=0; i<id_selected.length; i++) 
								           id_selected[i].set('sf_p', jsonData.flag);
								         
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
                                   }
                                }
                            ]
                        }        		
			        	
			    		});
			    		
			    		 voci_menu.push({
			         		text: 'Analisi righe documenti',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
			        		     acs_show_win_std('Seleziona tipologia documenti', 'acs_documenti_btid.php?fn=open_form', {list_selected_id: list_selected_id}, 350, 150, null, 'icon-folder_open-16');		
 					   
				                }
			    		});	
			    		
			    		
			    		if(list_selected_id.length == 1){
			    		 voci_menu.push({
			         		text: 'Cancella articolo',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		
			        		Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   				if (btn == 'yes'){
					    		  Ext.Ajax.request({
 						          url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cancella',
 						          timeout: 2400000,
 						          method     : 'POST',
 			        			  jsonData: {
 			        			    c_art: rec.get('codice')
 								  },							        
 						         success : function(result, request){
 						    		jsonData = Ext.decode(result.responseText);
 						    		 if (jsonData.ret_RI.RIESIT == 'W'){
									 	acs_show_msg_error(jsonData.ret_RI.RINOTE);
									 	return;
									 }else{
									  rec.destroy();	
									 }
     						       
 						    						            			
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
 						  	}
            	   		  });
 						    
 						    
				                }
			    		});	
			    		
			    		if(rec.get('config') != ''){
        			    		voci_menu.push({
                		      		text: 'Configura articolo',
                		    		iconCls: 'icon-game_pad-16',
                		    		handler: function() {
                		    		
                		    		
                		    		var my_listeners = {
                        					afterConferma: function(from_win, idsc){
                        					    //acs_show_msg_info('Fine Elaborazione!');
                        					    Ext.Ajax.request({
                							       url : '../base/acs_configuratore_articolo.php?fn=get_dom_risp',
        						                   method: 'POST',
        				        			       jsonData: {
        				        			       		idsc : idsc
        				        			       },					
                 		                           timeout: 2400000,
                							       method     : 'POST',
                							       waitMsg    : 'Data loading',  
                							       success : function(result, request){
                										jsonData = Ext.decode(result.responseText);
                										 acs_show_win_std('Fine configurazione articolo', 
                                		  				'../base/acs_configuratore_articolo.php?fn=open_form', 
                                		  				{	ar_config_row : jsonData.ar_config_row
                                		  				}, 600, 400, {}, 'icon-folder_search-16');
                 		                                
                							        },
                							        failure    : function(result, request){
                							            Ext.Msg.alert('Message', 'No data to be loaded');
                							        }
            							    });
                        						 from_win.destroy();                      						 						        									            
                				        		}
                		    				};	
                		    				
                		    				 acs_show_win_std('Configuratore articolo', 
                    		  				'../base/acs_configuratore_articolo.php?fn=open_tab', 
                    		  				{	
                    		  					c_art :  rec.get('codice'), 
                    		  					mode  : rec.get('config'),
                    		  					gestione_dim: 'Y'
                    		  				}, 600, 400, my_listeners, 'icon-copy-16');  
                		    		
                		    		
                			    	}
                				  });
        				  }
			    		
			    		}else{
			    		/* voci_menu.push({
			         		text: 'Traduzione articoli',
			        		iconCls : 'icon-globe-16',          		
			        		handler: function () {
			        		    acs_show_win_std('Traduzione articoli', 
		    				   'acs_anag_art_gest_trad.php?fn=open_al', 
					    	   {list_selected_id: list_selected_id, id_tree : grid.getId(), multi : 'Y'}, 450, 500, null, 'icon-globe-16');
 						    
				            }
			    		});*/
			    		
			    		
			    		}
			    	}
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
					
					 
			    	
			    	}
			 	,celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		var col_name = iView.getGridColumns()[iColIdx].dataIndex,				  	
					  		rec = iView.getRecord(iRowEl),
					  		grid = this;
					  		  
            				
					  		
					  	if (rec.get('liv') == 'liv_succ'){
					  	
					  		Ext.suspendLayouts();
            				  	//paginazione           
            				  	rec.store.treeStore.clearOnLoad = false;
            				  	rec.store.treeStore.load({
            					  		node: rec.parentNode,					  		
            					  		params: {m_offset: rec.get('m_offset')},
            					  		callback: function(records, operation, success) {
            				  				rec.store.treeStore.clearOnLoad = true;
            				  				rec.destroy();
            				 				Ext.resumeLayouts();            					  		
            					  		}    					  		
            					  	});
            					  	
					  		return;
					  	}					  		
					  	
					  	if(rec.get('liv') == ''){
						iEvent.preventDefault();
						
    						 if(col_name == 'allegati')	
    							acs_show_win_std('Articolo ' +rec.get('codice') , 'acs_anag_art_allegati.php?fn=open_tab', {c_art: rec.get('codice')}, 600, 400, null, 'icon-camera-16');
    					   
            				 if(col_name == 'l_ven')
           	  				  	acs_show_win_std('Listino di VENDITA [' +rec.get('codice') + '] ' +rec.get('task'), 'acs_anag_art_listini.php?fn=open_tab', {c_art: rec.get('codice'), tipo_list: 'V', rif_for : rec.get('rif_for')}, 1300, 500, null, 'icon-listino');
            				   
            				 if(col_name == 'l_acq')
            				  	acs_show_win_std('Listino di ACQUISTO [' +rec.get('codice') + '] ' +rec.get('task') , 'acs_anag_art_listini.php?fn=open_tab', {c_art: rec.get('codice'), d_art : rec.get('task'), tipo_list: 'A', rif_for : rec.get('rif_for'), nr_la : rec.get('l_acq'), forn: rec.get('ARFOR1')}, 1300, 530, null, 'icon-blog_add-16');
            				  	
            				 if(col_name == 'd_base')
            				  	acs_show_win_std('Componenti ' +rec.get('d_base_art') + ' - ' +rec.get('task') , 'acs_anag_art_distinta_base.php?fn=open_tab', {c_art: rec.get('d_base_art'), d_art : rec.get('task'), tipo_dist : 'P', nr_db : rec.get('d_base')}, 1300, 500, null, 'icon-folder_download-16');
            				  	
            				 if(col_name == 'd_base_f')
            				  	acs_show_win_std('Articolo ' +rec.get('codice') + ' - ' +rec.get('task') , 'acs_anag_art_distinta_base.php?fn=open_tab', {c_art: rec.get('codice'), tipo_dist : 'F'}, 1200, 500, null, 'icon-folder_upload-16');
            				  	
            				 if(col_name == 'cicli')
            				  	acs_show_win_std('Ciclo lavorazione ' +rec.get('ciclo_art')  + ' - ' +rec.get('task') , 'acs_anag_art_cicli.php?fn=open_tab', {c_art: rec.get('ciclo_art')}, 1300, 500, null, 'icon-tools-16');
            				  	
            				 if(col_name == 'config')
            				  	acs_show_win_std('Configuratore articolo ' +rec.get('codice') , 'acs_anag_art_config.php?fn=open_tab', {mode: rec.get('config'), c_art : rec.get('codice')}, 1200, 400, null, 'icon-design-16');
					   
					    	 if(col_name == 'trad'){
					    	 
				        	  		acs_show_win_std('Traduzione articolo ' +rec.get('codice'), 
					    					'acs_anag_art_gest_trad.php?fn=open_al', 
					    					{c_art: rec.get('codice'), id_tree : grid.getId()}, 450, 500, null, 'icon-globe-16');
				        
					   		}
					   		
					   		 if(col_name == 'var'){
					   		 
					   			acs_show_win_std('Variante anagrafica per articolo ' +rec.get('codice'), 'acs_anag_art_variabili.php?fn=open_tab', {c_art: rec.get('codice'), new : 'Y'}, 600, 500, null, 'icon-sticker_blue-16');
					   		}
					   		
					   		 if(col_name == 'abar'){
					   		 
            				  	acs_show_win_std('[ABAR] Regole abbinamento articolo [' +rec.get('codice') + '] ' + rec.get('task'), 'acs_anag_art_abbinamento.php?fn=open_tab', {c_art: rec.get('codice')}, 1050, 600, null, 'iconScaricoIntermedio');
					   			
					   		}
					   		
					   	
					   	     if(col_name == 'schede'){
					   	     
					   	     var p_schede = this.store.proxy.extraParams.form_ep.f_schede;
					   	     if(!Ext.isEmpty(p_schede)){
					   	     
					   	     <?php 
					   	       $scheda = $deskArt->find_TA_std('SCHED', $m_params->form_open->f_schede);
					   	       $desc_scheda = $scheda[0]['text'];
					   	     ?>	
					   		 
					   		 	if(rec.get('tipo_scheda') == 'BBPR')
					   		 		acs_show_win_std(rec.get('tipo_scheda') + ' ' + rec.get('codice'), 'acs_anag_art_schede_aggiuntive_sys.php?fn=open_tab', {c_art: rec.get('codice'), tipo_scheda : rec.get('tipo_scheda')}, 1200, 400, null, 'icon-tag-16');
					   		 	else if(rec.get('tipo_scheda') == 'FSC')
					   		 	    acs_show_win_std(rec.get('tipo_scheda') + ' ' + rec.get('codice'), 'acs_anag_art_fsc.php?fn=open_tab',{c_art: rec.get('codice')}, 1200, 400, null, 'icon-tag-16');
					   		 	else
            				  		acs_show_win_std(rec.get('tipo_scheda') + ' ' + rec.get('codice'), 'acs_anag_art_schede_aggiuntive.php?fn=open_tab', {c_art: rec.get('codice'), tipo_scheda : rec.get('tipo_scheda')}, 1200, 400, null, 'icon-tag-16');
					   			
    					   	  }else{
    					   	  
    					   	  	acs_show_win_std('Seleziona scheda', 'acs_anag_art_schede_aggiuntive_new.php?fn=open_form', {c_art: rec.get('codice')}, 500, 400, null, 'icon-tag-16');
    					   		 
    					   	  }
					   			
					   		 }
					   		
					   		if(col_name == 'task'){			
					   			show_win_dett_art(rec.get('codice'), rec);
					   		}
					   		
					   		
							if(col_name == 'sosp'){			
					   			  acs_show_win_std('Indici di revisione/modifica: ' + rec.get('task'), 
					   			  		'acs_indici_revisione_e_modifica.php?fn=open_win', 
	    			  			  		{c_art : rec.get('codice'), type: 'articolo'}, 820, 560, null, 'icon-leaf-16');  					   		
					   		}					   		
					   		
					   	
					   		if(col_name == 'note'){
								/////show_win_bl_articolo(rec.get('codice'));
								acs_show_win_std('Blocco note articolo ' + rec.get('codice'), 
					   			  		'acs_anag_art_note.php?fn=open_bl&c_art=' + rec.get('codice'), 
	    			  			  		{}, 600, 400, {
	    			  			  			afterSave: function(from_win, src){
	    			  			  				console.log('aaaaaa');
			        				     		rec.set('note', src.note);
 									    	}
 									    }, 'icon-leaf-16');													   		
					   		}					   		
					   		
					    }
	            	}
	          
	           }   	
			    	

		        },
			
		viewConfig: {
			
			   // enableTextSelection: true,
			
		        getRowClass: function(record, index) {	
		        
		        if(record.get('liv') == ''){
		          if(record.get('nr') % 2 == 0)
        		     return ' segnala_riga_pari';
        		  
		        
		        }
		        		        	
		           v = record.get('liv');
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
		           	return v;																
		         }   
		    }			
			    		
 }
} 

<?php 

exit;
}
// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_mod_cla'){
    
    $m_params = acs_m_params_json_decode();
   
    if(count($m_params->list_selected_art) == 1){
 
    
    $sql = "SELECT ARTPAR, ARCLME, ARGRME, ARSGME, ARTAB1, ARFOR1, ARDART,  
            CF_FORNITORE.CFCD AS C_FOR, CF_FORNITORE.CFRGS1 AS D_FOR/*, AFTDAT, AFVALO*/
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
              ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
           /* LEFT OUTER JOIN {$cfg_mod_DeskArt['file_proprieta']} AF
              ON AF.AFDT = AR.ARDT AND AF.AFART = AR.ARART AND AFVALO <> ''*/
            WHERE ARART = '{$m_params->list_selected_art[0]->codice}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
   /* print_r($row);
    exit;*/
    }
    
            
    
 
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
            title: '',
            items: [  	    
            	/*{
    					name: 'ARFOR1',
    					fieldLabel : 'ARFOR1',
    					xtype: 'textfield',
    					hidden : true							
    				   },
              		{ xtype: 'fieldcontainer'
              			  , flex: 1
                          , layout:
                              {	type: 'hbox'
                              , pack: 'start'
                        	  , align: 'stretch'
                                }
                          , items: [ 
                             {
            				name: 'f_fornitore',
            				value : '<?php echo $row['D_FOR']; ?>',
            				fieldLabel : 'Fornitore',
            				xtype: 'displayfield',
            				labelWidth : 80, 
            				anchor: '-15'							
            			   }, 
            			   {xtype: 'component',
                                flex : 1,
                            },
                          { xtype: 'button'
                         , margin: '0 15 0 0'
                         , anchor: '-15'	
                         , scale: 'small'
                         , iconCls: 'icon-search-16'
                         , iconAlign: 'top'
                         , width: 25			
                         , handler : function() {
                               var m_form = this.up('form').getForm();
                               var my_listeners = 
                                 { afterSel: function(from_win, row) {
                                     m_form.findField('ARFOR1').setValue(row.CFCD);
                                     m_form.findField('f_fornitore').setValue(row.CFRGS1);
                                     from_win.close();
                                   }
                                 }
                             
                               acs_show_win_std('Anagrafica clienti/fornitori'
                                               , 'acs_anag_ricerca_cli_for.php?fn=open_tab'
                                               , {cf: 'F'}, 600, 500, my_listeners
                                               , 'icon-search-16');
                             }
                                    
                        }
                          
                          ]
                         },
				   
			            {
            				name: 'd_forn',
            				fieldLabel : '&nbsp;',
            				labelSeparator : '',
            				labelWidth : 80,
            				xtype: 'displayfield',
            			//	hidden : true,
            				anchor: '-15'							
            			   },*/
				      {
								name: 'ARTPAR',
								xtype: 'combo',
								fieldLabel: 'Tipo/parte',
								labelWidth: 120,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								value : '<?php echo $row['ARTPAR'];  ?>',								
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MTPA'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'ARCLME',
								xtype: 'combo',
								labelWidth: 120,
								fieldLabel: 'Classe',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								value : '<?php echo $row['ARCLME'];  ?>',						
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUCM'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'ARGRME',
								xtype: 'combo',
								fieldLabel: 'Gruppo',
								labelWidth: 120,
								forceSelection: true,							
								displayField: 'text',
								valueField: 'id',	
								value : '<?php echo $row['ARGRME'];  ?>',							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUGM'), ''); ?>	
								    ]
								}
								
						 },{
								name: 'ARSGME',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								labelWidth: 120,
								forceSelection: true,	
								displayField: 'text',
								valueField: 'id',
								value : '<?php echo $row['ARSGME'];  ?>',							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('MUSM'), ''); ?>	
								    ]
								}
								
						 }, {
								name: 'ARTAB1',
								xtype: 'combo',
								fieldLabel: 'Gruppo pianificazione',
								labelWidth: 120,
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',		
								value : '<?php echo $row['ARTAB1'];  ?>',						
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('GRPA'), ''); ?>	
								    ]
								}
								
						 },
						 
						 {
								name: 'f_proprieta',
								xtype: 'combo',
								flex: 1,
								labelWidth: 120,
								fieldLabel: 'Propriet&agrave;',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',
								queryMode: 'local',
								minChars: 1,							
								emptyText: '- seleziona -',
						   		//allowBlank: false,								
							    anchor: '-15',
							    value : <?php echo j(trim($row['AFTDAT']));  ?>,	
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, 'TAASPE'],
								    data: [								    
								     <?php echo acs_ar_to_select_json($deskArt->find_TA_std('ARPRO', null, 'N',  'Y', null, null, null, 'N', 'Y'), ''); ?>	
								    ]
								}, listeners: {
                                	change: function(field,newVal, a) {	
                                  		if (!Ext.isEmpty(newVal)){
                                  		    tab = field.displayTplData[0].TAASPE;
                                  		    combo_risp = this.up('form').down('#c_risp');                      		 
                                         	combo_risp.store.proxy.extraParams.tab = tab;
                                        	combo_risp.store.load();                             
                                         }
                                         
            
                                        }, beforequery: function (record) {
                                        record.query = new RegExp(record.query, 'i');
                                        record.forceAll = true;
                                    }
                           }
								
						   },
						   
						   {
								name: 'f_valore',
								xtype: 'combo',
								flex: 1,
								labelWidth: 120,
								itemId: 'c_risp',
								fieldLabel: 'Valore assegnato',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
								queryMode: 'local',
								minChars: 1,	
						   		//allowBlank: false,								
							    anchor: '-15',
							    value : <?php echo j(trim($row['AFVALO']));  ?>,	
								store: {
									        autoLoad: true,
											proxy: {
									            type: 'ajax',
									            url : 'acs_anag_art_variabili.php?fn=get_tab_risposte',
									            actionMethods: {
            							          read: 'POST'
           							        },
							                	extraParams: {
							    		    		tab: '',
							    		    		codice : 'Y'
							    				},				            
									            doRequest: personalizza_extraParams_to_jsonData, 
									            reader: {
               						            type: 'json',
               									method: 'POST',						            
                						            root: 'root'						            
                						        }
									        },       
											fields: [{name:'id'}, {name:'text'}],		             	
							            }
								
								, listeners: {
                                	    beforequery: function (record) {
                                        record.query = new RegExp(record.query, 'i');
                                        record.forceAll = true;
                                    }
                           }
						   }	        
				
					        
				],
			buttons: [
			
			{
	            text: 'Conferma modifica dati',
	            iconCls: 'icon-save-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var form_values = form.getValues();
	            	var loc_win = this.up('window');
	            	if(form.isValid()){
	            	
	            		Ext.Ajax.request({
							   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna_art',
							   method: 'POST',
							   jsonData: {
							   form_values : form_values,
							   row_old : <?php echo acs_je($row); ?>,
							   c_art : '<?php echo $m_params->c_art; ?>', 
							   list_art : <?php echo acs_je($m_params->list_selected_art); ?>
							   }, 
							   
							   success: function(response, opts) {
							   	  Ext.getBody().unmask();
								  var jsonData = Ext.decode(response.responseText);
								  loc_win.fireEvent('afterModCls', loc_win)
								  					   	  
							   }, 
							   failure: function(response, opts) {
							      alert('error on save_manual');
							   }
							});	
    	
	            	 }
	        		}			
	        }
	        
	        ]
	        
	        
	        
	        
				
        }
]}
<?php	
	exit;
} //open_form_filtri


if ($_REQUEST['fn'] == 'exe_up_color_multi'){
    
    $m_params = acs_m_params_json_decode();
    
    foreach($m_params->list_selected_id as $v){
        
        $sql_a = "SELECT COUNT (*) AS C_ROW
        FROM {$cfg_mod_DeskUtility['file_est_anag_art']} AX
        WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$v}'";
        
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_a);
        
        $row = db2_fetch_assoc($stmt_a);
        
        
        $ar_ins = array();
        
        $ar_ins['AXUSGE'] 	= $auth->get_user();
        $ar_ins['AXDTGE']   = oggi_AS_date();
        $ar_ins['AXTMGE'] 	= oggi_AS_time();
        $ar_ins['AXSFON'] 	= $m_params->flag;
        
        if($row['C_ROW'] == 0){
            
            $ar_ins['AXDT'] 	= $id_ditta_default;
            $ar_ins['AXART'] 	= $v;
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art']}(" . create_name_field_by_ar($ar_ins) . ")
			    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
            
            
        }else{
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art']} AX
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE AXDT = '{$id_ditta_default}' AND AXART = '{$v}' ";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
        }
    
        
    }
    
    $ret = array();
    $ret['success'] = true;
    $ret['flag'] = $m_params->flag;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_up_color_utente'){
    
    $m_params = acs_m_params_json_decode();
    
    foreach($m_params->list_selected_id as $v){
        
        $sql_a = "SELECT COUNT (*) AS C_ROW
        FROM {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
        WHERE AUDT = '{$id_ditta_default}' AND AUART = '{$v}'";
        
        $stmt_a = db2_prepare($conn, $sql_a);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_a);
        
        $row = db2_fetch_assoc($stmt_a);
        
        
        $ar_ins = array();
        
        $ar_ins['AUUSUM'] 	= $auth->get_user();
        $ar_ins['AUDTUM']   = oggi_AS_date();
        $ar_ins['AUTMUM'] 	= oggi_AS_time();
        $ar_ins['AUSFAR'] 	= $m_params->flag;
        $ar_ins['AUUTEN'] 	= $auth->get_user();
        
        if($row['C_ROW'] == 0){
            
            $ar_ins['AUUSGE'] 	= $auth->get_user();
            $ar_ins['AUDTGE']   = oggi_AS_date();
            $ar_ins['AUTMGE'] 	= oggi_AS_time();
            $ar_ins['AUDT'] 	= $id_ditta_default;
            $ar_ins['AUART'] 	= $v;
            
            $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_est_anag_art_utente']}(" . create_name_field_by_ar($ar_ins) . ")
			    VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
            
            
        }else{
            
            $sql = "UPDATE {$cfg_mod_DeskUtility['file_est_anag_art_utente']} AU
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE AUDT = '{$id_ditta_default}' AND AUART = '{$v}' ";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
        }
        
        
    }
    
    $ret = array();
    $ret['success'] = true;
    $ret['flag'] = $m_params->flag;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data_rilav'){
    
    $m_params = acs_m_params_json_decode();
    $where = "";
    $ar = array();
    
    //if(strlen($m_params->takey1) > 0)
        $where .= " AND TAKEY1 = '{$m_params->takey1}' ";
    
    $sql = "SELECT *
    FROM {$cfg_mod_DeskArt['file_tabelle']}
    WHERE TADT='{$id_ditta_default}' AND TATAID = 'RILAV'  {$where}";

 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while ($row = db2_fetch_assoc($stmt)) {
         $r = array();
         $r['id'] 	= trim($row['TAKEY2']);
         $r['text'] = trim($row['TADESC']);
         $ar[] = $r;
    }
    
  
    echo acs_je($ar);
    exit;

}


    
    