<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_new_art'){
    $m_params = acs_m_params_json_decode();
    $ret = array();
    
    $sql = "SELECT ARUMTE, ARDART FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
    WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$m_params->codice}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    $ret['success'] = true;
    $ret['rec'] = $row;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_save'){
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
    
    
    $ar_upd['LCRIGA'] 	=  trim($form_values->LCRIGA);
    $ar_upd['LCPRZ'] 	=  sql_f($form_values->LCPRZ);
    $ar_upd['LCSC1'] 	=  sql_f($form_values->LCSC1);
    $ar_upd['LCSC2'] 	=  sql_f($form_values->LCSC2);
    $ar_upd['LCQMIN'] 	=  sql_f($form_values->LCQMIN);
    $ar_upd['LCQMAX'] 	=  sql_f($form_values->LCQMAX);
    $ar_upd['LCNOTE'] 	=  $form_values->note; 
    if(strlen($form_values->LCINE1) > 0)
      $ar_upd['LCINE1'] 	=  $form_values->LCINE1;
    $ar_upd['LCARTM'] 	=  $form_values->LCARTM;
    
       
	for($i = 1; $i <= 5; $i++){
	    $v = "LCVAR{$i}";
	    if (isset($form_values->$v)){
	        $v = "LCVAR{$i}"; $ar_upd["LCVAR{$i}"] =  $form_values->$v;
	        $v = "LCTPS{$i}"; $ar_upd["LCTPS{$i}"] =  $form_values->$v;
	        $v = "LCVAN{$i}"; $ar_upd["LCVAN{$i}"] =  $form_values->$v;
	        $v = "LCOSS{$i}"; $ar_upd["LCOSS{$i}"] =  $form_values->$v;
	    }
	}
    
	
    $sql = "UPDATE {$cfg_mod_DeskUtility['file_listini_licon']} LC
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(LC) = '{$form_values->RRN}' ";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
// ******************************************************************************************
// EXE AGGIUNGI 
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiungi'){
    ini_set('max_execution_time', 3000);
    $form_values = $m_params->form_values;
    $params = $m_params->params;
    
    $ar_ins = array();
    $ar_ins['LCDT'] 	=  $id_ditta_default;
    $ar_ins['LCSZLI'] 	=  $params->tipo;
    $ar_ins['LCLIST'] 	=  $params->list;
    $ar_ins['LCCCON'] 	=  $params->forn;
    $ar_ins['LCVALU'] 	=  'EUR';
    $ar_ins['LCART'] 	=  $params->c_art;
   
    $ar_ins['LCRIGA'] 	=  trim($form_values->LCRIGA);
    $ar_ins['LCPRZ'] 	=  sql_f($form_values->LCPRZ);
    $ar_ins['LCSC1'] 	=  sql_f($form_values->LCSC1);
    $ar_ins['LCSC2'] 	=  sql_f($form_values->LCSC2);
    $ar_ins['LCQMIN'] 	=  sql_f($form_values->LCQMIN);
    $ar_ins['LCQMAX'] 	=  sql_f($form_values->LCQMAX);
    $ar_ins['LCNOTE'] 	=  $form_values->note;
    if(strlen($form_values->LCINE1) > 0)
        $ar_ins['LCINE1'] 	=  $form_values->LCINE1;
    $ar_ins['LCARTM'] 	=  $form_values->LCARTM;
    
    for($i = 1; $i <= 5; $i++){
        $v = "LCVAR{$i}";
        if (isset($form_values->$v)){
            $v = "LCVAR{$i}"; $ar_ins["LCVAR{$i}"] =  $form_values->$v;
            $v = "LCTPS{$i}"; $ar_ins["LCTPS{$i}"] =  $form_values->$v;
            $v = "LCVAN{$i}"; $ar_ins["LCVAN{$i}"] =  $form_values->$v;
            $v = "LCOSS{$i}"; $ar_ins["LCOSS{$i}"] =  $form_values->$v;
        }
    }

    
    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_listini_licon']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
// ******************************************************************************************
// EXE DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    
    $sql = "DELETE FROM {$cfg_mod_DeskUtility['file_listini_licon']} LC
            WHERE RRN(LC) = '{$form_values->RRN}' ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $error_msg =  db2_stmt_errormsg($stmt);
    
    
    
    $ret = array();
    $ret['success'] = $result;
    if (strlen($error_msg) > 0) {
        $ret['success'] = false;
        $ret['message'] = $error_msg;
    }
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    
    $m_params = $m_params->open_request;
   
    
    $sql = "SELECT RRN(LC) AS RRN, LC.*, AR.ARDART AS D_ARTR
            FROM {$cfg_mod_DeskUtility['file_listini_licon']} LC
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR
            ON AR.ARDT = LC.LCDT AND AR.ARART = LC.LCARTM
            WHERE LCDT = '{$id_ditta_default}' AND LCART = '{$m_params->c_art}'
            AND LCCCON = '{$m_params->forn}' AND LCLIST = '{$m_params->list}' AND LCSZLI = '{$m_params->tipo}'
            ";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
       $row['percentuale'] = n($row['LCPRZP'],2) ."/".n($row['LCPRZF'],2);
       $row['articolo'] = "[".trim($row['LCARTM'])."] ".$row['D_ARTR'];
       $row['descrizione'] = $row['D_ARTR'];
       $row['note'] = trim($row['LCNOTE']);
       $row['LCARTM'] = trim($row['LCARTM']);
       $ar[] = $row;
    }
    
    echo acs_je($ar);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'open_tab'){
  

       
    ?>
{"success":true, "items": [

        {
			xtype: 'panel',
			title: '',
		
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				
					{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],		
			        	store: {
						//xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['percentuale', 'articolo', 'descrizione', 'RRN', 'LCRIGA', 'LCPRZ', 'note', 'LCARTM', 'LCINE1',
		        					  'LCPRZP', 'LCPRZF', 'LCQMIN', 'LCQMAX', 'LCSC1', 'LCSC2',
    						<?php for($i = 1; $i <= 5; $i++){?>
		        				'LCVAR<?php echo $i?>',
		        				'LCTPS<?php echo $i?>',
		        				'LCVAN<?php echo $i?>',
		        				'LCOSS<?php echo $i?>',
		        			<?php }?>
		        			
		        			]							
									
			}, //store
			
			 <?php $lcn = "<img src=" . img_path("icone/48x48/currency_blue_yuan.png") . " height=20>"; ?>
		
			      columns: [	
			
	                {
	                header   : 'Riga',
	                dataIndex: 'LCRIGA',
	                width : 50
	                },
	                {
	                header   : 'Prezzo',
	                dataIndex: 'LCPRZ',
	                align : 'right',
	                renderer : floatRenderer2,
	                 width : 70
	                },{
	                header   : 'Note',
	                dataIndex: 'note',
	                flex : 1
	                },{
	                header   : 'Articolo rif.',
	                dataIndex: 'articolo',
	                flex : 1
	                },
	                
	                {text: '<?php echo $lcn; ?>', 	
    				width: 30,
    				align: 'center', 
    				dataIndex: 'LCINE1',
    			    filter: {type: 'string'}, filterable: true,
    				tooltip: 'Maggiorazioni condizionate',		        			    	     
    		    	renderer: function(value, p, record){
    		    			  if(record.get('LCINE1') == 'I') return '<img src=<?php echo img_path("icone/48x48/currency_blue_yuan.png") ?> width=15>';
    		    		  }
    		        },
	                
	         ], listeners: {
	         
	     
	      		 selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
	               
		               var form_dx = this.up('panel').down('#dx_form');
		               //pulisco eventuali filtri
		               form_dx.getForm().reset();
		               //ricarico i dati della form
		               
	                   //form_dx.getForm().setValues(selected[0].data);
	                   acs_form_setValues(form_dx, selected[0].data);
	                   }
		          },
	          
				  
				 }
			
		
		
		}
		
	
		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [
     		           {
    					name: 'RRN',
    					fieldLabel : 'rrn',
    					xtype: 'textfield',
    					hidden : true							
    				   }, 
    				       
    				       { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
    						{
        					name: 'LCRIGA',
        					fieldLabel : 'Riga',
        					xtype: 'textfield',
        					anchor: '-15',
        				    maxLength : 4,
        				    width : 140						
        				   }, {
        					name: 'LCPRZ',
        					fieldLabel : 'Prezzo',
        					xtype: 'numberfield',
        					hideTrigger : true,	
        					labelAlign : 'right',
        					labelWidth : 60,
        					width : 165,	
        					anchor: '-15'					
        				   }
						]} ,  { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
    						{
        					name: 'LCSC1',
        					fieldLabel : 'Percentuale',
        					xtype: 'numberfield',
        					hideTrigger : true,	
        					decimalPrecision : 2,
        					width : 200,
        					anchor: '-15'						
        				   },{
        					name: 'LCSC2',
        					fieldLabel : '-',
        					labelSeparator : '',
        					xtype: 'numberfield',
        					margin : '0 0 0 5',
        					labelWidth : 10,
        					hideTrigger : true,	
        					decimalPrecision : 2,
        					width : 100,
        					anchor: '-15'						
        				   }
						]},
						    
    				       { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
    							 {
        					name: 'LCQMIN',
        					fieldLabel : 'Q.t&agrave; minima',
        					xtype: 'numberfield',
        					width : 195,
        					hideTrigger : true,	
        					decimalPrecision : 4,
        					anchor: '-15'						
        				   }, {
        					name: 'LCQMAX',
        					fieldLabel : 'max',
        					xtype: 'numberfield',
        					labelWidth : 30,
        					labelAlign : 'right',
							width : 110,        					
        					hideTrigger : true,	
        					decimalPrecision : 4,
        					anchor: '-15'						
        				   }
						]} 
						,{
    					name: 'note',
    					fieldLabel : 'Note',
    					xtype: 'textfield',
    					anchor: '-15'						
    				   }, {
    					name: 'LCARTM',
    					fieldLabel : 'Articolo rif.',
    					xtype: 'textfield',
    					anchor: '-15',
    					listeners : {
						'blur': function(field){
    					 var n_art = field.getValue();
    					 var form = this.up('form').getForm();
    					 
    					 Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_new_art',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				codice : n_art
								},				
										        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							     	form.findField('descrizione').setValue(jsonData.rec.ARDART);
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
      				}
					
					}						
    				   },{
    					name: 'descrizione',
    					fieldLabel : 'Descrizione',
    					xtype: 'displayfield',
    					fieldStyle: 'font-weight: bold;',
    					anchor: '-15'						
    				   },{
                			name: 'LCINE1',
                			xtype: 'combo',
                			fieldLabel: 'Includi/escludi',
                			
                			forceSelection: true,								
                			displayField: 'text',
                			valueField: 'id',								
                			emptyText: ' - seleziona -',
                	   		allowBlank: true,								
                		    anchor: '-15',
                		    width : 100,
                			store: {
                				editable: false,
                				autoDestroy: true,
                				fields: [{name:'id'}, {name:'text'}],
                				data: [								    
                					 {id: 'E', text : '[E] Escludi'},
                					 {id: 'I', text : '[I] Includi'}
                				]
                			}
                		 },
                		 
                		 
                		 <?php
                		 $proc = new ApiProc();
                		 for($i = 1; $i <=5 ; $i++){
        				  echo $proc->get_json_response(
        				      extjs_combo_regola_config(array('label' =>"Condizione ({$i})",
				                'f_var' => "LCVAR{$i}",
				                'f_tp'  => "LCTPS{$i}",
				                'f_van' => "LCVAN{$i}",
				                'f_os'  => "LCOSS{$i}"
                              ))
        				  );
        				 echo ",";
                        		  
						}?>
                		 
				  ],
		    dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);							        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     }, {
                     xtype: 'button',
                    text: 'Refresh',
		            scale: 'small',	               
		            iconCls : 'icon-button_black_repeat_dx-16',      
			        handler: function() {		          
	       			  var grid = this.up('panel').up('panel').down('grid'),
	       			  	  form = this.up('form'); 
 			          grid.getStore().load();
 			          form.getForm().reset();
 			        }
			     }, '->',
                 
                  
		           
			      {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			    var grid = this.up('form').up('panel').down('grid');
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				params : <?php echo acs_je($m_params); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			  var grid = this.up('form').up('panel').down('grid');
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
			        				
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     }
			     ]
		   }]
				  
				  
				  }
				],
					 
					
					
	}
	
]}
<?php

exit;
}

