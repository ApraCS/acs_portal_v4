<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();

$m_params = acs_m_params_json_decode();

//*************************************************
if($_REQUEST['fn'] == 'exe_upd_commento_articolo'){
//*************************************************
    
    if($m_params->f_db == 'Y')
        $tpno = "DB";
    else
        $tpno = "AR";

    $form_values = $m_params->form_values;

    
    //c_art puo' essere un codice articolo o un array di codici
    if (is_string($m_params->c_art)){
        $ar_codici_articolo = array($m_params->c_art);
    } else {    //array di codici
        $ar_codici_articolo = $m_params->c_art;
    }
            
    foreach ($ar_codici_articolo as $c_art){ 
            //elimino il vecchio commento
            $sql = "DELETE
                    FROM {$cfg_mod_DeskUtility['file_note_anag']}
                    WHERE RLDT= '{$id_ditta_default}' AND RLRIFE1 = '{$c_art}'
                    AND RLRIFE2 = '{$m_params->bl}'
                    AND RLTPNO = '{$tpno}'";
        			
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);
            
            
            //divido il testo in blocchi da 80 caratteri e creo i record
            //$str = str_split($p['f_text'], 80);
            $str = array();
            $str[] = $form_values->f_text_0;
            $str[] = $form_values->f_text_1;
            $str[] = $form_values->f_text_2;
            $str[] = $form_values->f_text_3;
            $str[] = $form_values->f_text_4;            
            $str[] = $form_values->f_text_5;
            $str[] = $form_values->f_text_6;
            $str[] = $form_values->f_text_7;
            $str[] = $form_values->f_text_8;
            $str[] = $form_values->f_text_9;
        
            $ar_ins = array();
            $ar_ins['RLDTGE'] 	= oggi_AS_date();
            $ar_ins['RLUSGE'] 	= $auth->get_user();
            $ar_ins['RLDTUM'] 	= oggi_AS_date();
            $ar_ins['RLUSUM'] 	= $auth->get_user();
            $ar_ins['RLDT'] 	= $id_ditta_default;
            $ar_ins['RLRIFE1'] 	= $c_art;
            $ar_ins['RLRIFE2'] 	= $m_params->bl;
            $ar_ins['RLTPNO'] 	= $tpno;
        
            $c=0;
            foreach($str as $t){
                
                $ar_ins['RLRIGA']   = ++$c;
                $ar_ins['RLSWST'] 	= substr($t, 0, 1);  //1
                $ar_ins['RLREST1'] 	= substr($t, 1, 15);  //15
                $ar_ins['RLFIL1'] 	= substr($t, 16, 64);  //64
        
                //scrivo solo le righe non blank
                if (strlen(trim($t)) > 0){                
                    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_note_anag']}(" . create_name_field_by_ar($ar_ins) . ")
        			         VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
             
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt, $ar_ins);
                    echo db2_stmt_errormsg();
                }
            }
    
    } //foreach codice articolo
    

    //solo se era per un singolo articolo
    if (is_string($m_params->c_art)){
        if($m_params->f_db == 'Y')
            $ha_commenti = $main_module->has_commento_articolo($m_params->c_art, $m_params->bl, 'DB');
        else 
            $ha_commenti = $main_module->has_commento_articolo($m_params->c_art, $m_params->bl);
    } else {
        $ha_commenti = true;
    }
            
    if ($ha_commenti == FALSE)
        $img_com_name = "iconCommGray";
    else
        $img_com_name = "iconCommYellow";    

    $ret = array();
    $ret['success'] = true;
    $ret['icon'] = $img_com_name;
    $ret['note'] = $ha_commenti;
    echo acs_je($ret);
    exit;
}


//*************************************************
//Apertura elenco blocchi note disponibili
//*************************************************
if($_REQUEST['fn'] == 'open_bl'){
//*************************************************
if ($m_params->multi == 'Y')
    $c_art = $m_params->list_selected_art;
else
    $c_art = $_REQUEST['c_art'];
?>    
    
 {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
            frame: true,
            title: '',
            items: [ 
                
                <?php
                
                
                if($m_params->f_db == 'Y'){
                    
                    $sql = "SELECT RRN(TA) AS RRN, TAKEY1, TADESC
                            FROM {$cfg_mod_DeskArt['file_tabelle']} TA
                            WHERE TADT = '{$id_ditta_default}' AND TATAID = 'BLDB0'
                            ORDER BY TAKEY1";
                }else{                
                    $sql = "SELECT RRN(TA) AS RRN, TANR, TADESC
                            FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
                            WHERE TADT = '{$id_ditta_default}'
                            AND TAID = 'NUSN'
                            AND TACOR1 = 'AR' AND TACOR2 = '*ST' ORDER BY TANR";
                }
                
               
                $stmt = db2_prepare($conn, $sql);
                $result = db2_execute($stmt);
                
                                
                while($row = db2_fetch_assoc($stmt)){
                    
                    $c++;
                    if($m_params->f_db == 'Y'){
                        $txt_bl = "[".trim($row['TAKEY1'])."] ".trim($row['TADESC']);
                        $ha_commenti = $main_module->has_commento_articolo($m_params->rife1, $row['TAKEY1'], 'DB');
                    }else{
                        $txt_bl = "[".trim($row['TANR'])."] ".trim($row['TADESC']);
                        
                        if (!$m_params->multi == 'Y')
                            $ha_commenti = $main_module->has_commento_articolo($c_art, $row['TANR']);
                    }
                   
                
                if ($ha_commenti == FALSE)
                    $img_com_name = "iconCommGray";
                else
                    $img_com_name = "iconCommYellow";
                
                if($m_params->f_db == 'Y')
                    $commento_txt = $main_module->get_commento_articolo($m_params->rife1, $row['TAKEY1'], 'DB');
                else {
                    if (!$m_params->multi == 'Y')
                        $commento_txt = $main_module->get_commento_articolo($c_art, $row['TANR']);
                }
                
                
                if($m_params->multi == 'Y')
                    $recap = $main_module->get_recap_articoli($c_art, $row['TANR']);
                 
                $data_user = print_date($commento_txt['data']) ." ".$commento_txt['user'];
                    
                  
                ?>
                	
                		{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								xtype: 'button',
								text: '',
								itemId: 'f_bt_<?php echo $row['RRN']; ?>',
								margin : '0 5 0 0',
								iconCls: '<?php echo $img_com_name; ?>',
								scale: 'small',
								handler : function(){
								
								var form = this.up('form').getForm();
								var win = this.up('window');
								
								<?php if($m_params->f_db == 'Y'){?>
								acs_show_win_std('Blocco note ' + '[<?php echo $row['TAKEY1'] ?>] distinta ' + <?php echo j($m_params->title); ?>, 
				    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
				    					{c_art: '<?php echo $m_params->rife1 ?>', bl: '<?php echo $row['TAKEY1']; ?>', f_db : '<?php echo $m_params->f_db; ?>'}, 800, 400,  {
			        					'afterSave': function(from_win, src){
			        					    var bt = win.down('#f_bt_<?php echo $row['RRN']; ?>');
			        					    bt.setIconCls(src.icon);
			        						from_win.close();
			        					}
			        				}, 'iconCommGray');
			        		
			        		<?php } else{ 
			        		
			        		    if ($m_params->multi == 'Y'){
			        		        $title = "Aggiorna blocco note {$row['TANR']} su tutti gli articoli selezionati";
			        		    } else {
                                    $title = "Annotazioni articolo {$c_art} blocco note {$row['TANR']}";
			        		       
			        		    }
			        		    
			        		    ?>
			        		
			        			acs_show_win_std(<?php echo j($title)?>, 
				    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
				    					{
				    						c_art: <?php echo acs_je($c_art); ?>, 
				    						bl: '<?php echo $row['TANR']; ?>'
				    					}, 800, 400,  {
			        					'afterSave': function(from_win, src){
			        					    var bt = win.down('#f_bt_<?php echo $row['RRN']; ?>');
			        					    bt.setIconCls(src.icon);
			        						from_win.close();
			        									        						
			        						win.fireEvent('afterSave', win, src);
			        						
			        					}
			        				}, 'iconCommGray');
			        		
			        		<?php }?>
									
								} 
							},
						
						{
						name: 'f_text_<?php echo $row['RRN'] ?>',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: <?php echo j($txt_bl); ?>						
					},
					
					{
						name: 'f_data_user',
						xtype: 'displayfield',
						margin : '0 0 0 50',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: <?php echo j($data_user); ?>						
					},
					
					
				
						]},
						
						<?php if($m_params->multi == 'Y'){?>
						
						   { 
					            name: 'f_recap',
					            xtype: 'displayfield',
					            margin: '0 0 0 25',
					            anchor: '-15',
					            value: <?php echo j($recap); ?>,
					            fieldStyle: 'font-size: 10px;'
					         },
						
						<?php }?>
						
					<?php  if($ha_commenti == true){
										    
					    $comm = "";
					    for($i = 0; $i <= count($commento_txt); $i++){
					    
					        $comm .= $commento_txt[$i]."\r\n";
					    }
				
					    
					        ?>
					        
					        {
					            name: 'f_text_<?php echo $i ?>',
					            //xtype: 'displayfield',
					            xtype     : 'textareafield',
				                height: 40, width: '100%',
					            readOnly:true,
					            margin: '0 0 0 25',
					            anchor: '-15',
					            value: <?php echo j($comm); ?>
					         },
					
					
					
					<?php  }?>
					
					<?php }?>	
                	
                	
				]            
				
        }
]}   
    
    
    
    
<?php  
exit;
}


//*************************************************
//Form gestione singolo blocco note
//*************************************************
if($_REQUEST['fn'] == 'open_tab'){
//*************************************************    
 $bl = $m_params->bl;
 $c_art = $m_params->c_art;

 if($m_params->f_db == 'Y')
    $commento_txt = $main_module->get_commento_articolo($c_art, $bl, 'DB');
 else {
    if (is_string($m_params->c_art))
        $commento_txt = $main_module->get_commento_articolo($c_art, $bl);
 }
 
 $num_ann = 9;
    
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            items: [ 
                
                <?php for($i=0; $i<= $num_ann;$i++){ ?>
                	
                	{
						name: 'f_text_<?php echo $i ?>',
						xtype: 'textfield',
						fieldLabel: '',
					    anchor: '-15',
					    maxLength: 80,					    
					    value: <?php echo j(trim($commento_txt[$i])); ?>,							
					},	
					
					<?php }?>	
                	
                	
				],
			buttons: [
			<?php if(trim($bl) == 'ACR'){?>
		      {
	            text: 'Limitazioni per Divisione/Modello',
	            iconCls: 'icon-folder_block-32', 
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');
	            	
	            	var my_listeners = {
	            		afterOkSave: function(from_win, t_div, t_mods){
	            			//t_div va nella prima riga
	            			//t_mods lo spalmo nelle successive
	            			
	            			form.reset();
	            			
	            			form.findField('f_text_0').setValue(t_div);
	            			
	            			for (var i=0; i<t_mods.length; i++){
	            				var f_name = 'f_text_' + (i+1);
	            				form.findField(f_name).setValue(t_mods[i]);
	            			}
	            			from_win.destroy();
	            		} //afterOkSave
	            		
	            		, afterAggiorna: function(from_win, type, text){
	            			
	            			if(type == 'DIV')
	            			    form.findField('f_text_0').setValue(text);
	            			
	            			if(type == 'MOD'){
    	            			for (var i=0; i<text.length; i++){
    	            				var f_name = 'f_text_' + (i+1);
    	            				form.findField(f_name).setValue(text[i]);
    	            			}
	            			}
	            			from_win.destroy();
	            		} //afterOkSave
	            	};
	            	
	            	
	            	
	            		acs_show_win_std('Limitazioni per Divisione/Modello', 
	            			'acs_anag_art_note_ACR.php?fn=open_tab', 
	            			{bl_rows: this.up('form').getValues()}, 
	            			1000, 450, 
	            			my_listeners, 'icon-folder_block-16');   	     
	            	
	            	}
	            	
	            	}, '->',
	            	<?php } ?>
				{
				
				<?php
				
				if (is_string($c_art)){
				  $b_salva = 'Salva';
				} else {
				  $b_salva = '<b> Conferma Inserim./Modifica blocco note <br>
                              su tutti gli articoli selezionati [' . count($c_art) . ']</b>';
				}
				
				?>
				
	            text: <?php echo j($b_salva) ?>,
	            iconCls: 'icon-save-32', 
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');
                
					if(form.isValid()){	  
					
						Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upd_commento_articolo',
					        jsonData: {
					        	form_values: form.getValues(),
					        	c_art : <?php echo acs_je($c_art); ?>,
					        	bl : '<?php echo $bl; ?>',
					        	f_db : '<?php echo $m_params->f_db; ?>'
					        	
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	
					        try {
                               var jsonData = Ext.decode(result.responseText);
							   loc_win.fireEvent('afterSave', loc_win, jsonData);
                            }
                            catch(err) {
                                Ext.Msg.alert('Try&catch error', err.message);
                            }
					       
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					            console.log('errorrrrr');
					        }
					    });	  
					
	

				    }            	                	                
	            }
	        }
   
	        ]             
				
        }
]}

<?php }
exit;