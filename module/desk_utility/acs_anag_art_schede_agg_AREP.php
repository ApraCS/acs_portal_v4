<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    $nr['tades2'] = $row['TADES2'];  
    $nr['coeff'] = substr($row['TAREST'], 0, 6);
    $nr['ordine'] = $row['TAORDI'];
    $desc_centro = get_TA_sys('PUCE', trim($row['TACOR2']));
    $nr['d_centro'] = "[".trim($row['TACOR2'])."] ".$desc_centro['text'];
    $nr['centro'] = trim($row['TACOR2']); 
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'ordine', 'coeff', 'centro', 'd_centro');

    
    return acs_je($ar_fields);
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
			{
            header   : 'Ordine',
            dataIndex: 'ordine',
            width : 70
            },
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }, {
            header   : 'Note',
            dataIndex: 'tades2',
            flex : 1
            },  {
            header   : 'Coefficiente',
            dataIndex: 'coeff',
            flex : 1
            },{
            header   : 'Centro di lavoro',
            dataIndex: 'd_centro',
            flex : 1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "note" => trim($row->tades2),
        "coeff" => trim($row->coeff),
        "ordine" => trim($row->ordine),
        "centro" => trim($row->centro),
    );
 
    return $ar_values;
    
}

function get_values_from_array($row){
    
    $ar_values = array(
        "riga" => trim($row['riga']),
        "descrizione" => trim($row['tadesc']),
        "note" => trim($row['tades2']),
        "coeff" => trim($row['coeff']),
        "ordine" => trim($row['ordine']),
        "centro" => trim($row['centro']),
    );
    
    return $ar_values;
    
}

function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		maxLength: 3,
		anchor: '-15',	
		value:  <?php echo j($ar_values['riga']); ?>
	  },
         {xtype: 'textfield',
		name: 'ordine',
		fieldLabel: 'Ordine',
		maxLength: 5,
		anchor: '-15',	
		value:  <?php echo j($ar_values['ordine']); ?>
		},
       {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
    	  {xtype: 'textfield',
    	name: 'note',
    	fieldLabel: 'Note',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['note']); ?>
    	},   {xtype: 'textfield',
    	name: 'coeff',
    	fieldLabel: 'Coefficiente',
    	maxLength : 6,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['coeff']); ?>
    	},
    	{name: 'centro',
			xtype: 'combo',
			fieldLabel: 'Centro di lavoro',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['centro']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            width : 250,
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUCE'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	 }
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-6s", $form_values->coeff);
    $ar_ins['TANR']   = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;
    $ar_ins['TADES2'] = $form_values->note;
    $ar_ins['TAORDI'] = $form_values->ordine;
    $ar_ins['TACOR2'] = $form_values->centro;
    
    
    return $ar_ins;
    
}