<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_salva_traduzione'){
    
    $m_params = acs_m_params_json_decode();
    
    $tarest = "";
    $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_3)));
    $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_4)));
    $tarest .= sprintf("%-30s",  trim(acs_toDb($m_params->form_values->f_text_5)));
        
    if(isset($m_params->rrn) && trim($m_params->rrn) != ''){
           
            $ar_ins = array();
            $ar_ins['TAUSUM'] 	= $auth->get_user();
            $ar_ins['TADTUM']   = oggi_AS_date();
            $ar_ins["TADESC"] = acs_toDb($m_params->form_values->f_text_1);
            $ar_ins["TADES2"] = acs_toDb($m_params->form_values->f_text_2);
            $ar_ins["TAREST"] = $tarest;
         
            
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_tab_sys']} TA
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE RRN(TA) = '{$m_params->rrn}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
        
    }else{
        
            $ar_ins = array();
            
            $ar_ins['TAUSGE'] 	= $auth->get_user();
            $ar_ins['TADTGE']   = oggi_AS_date();
            $ar_ins['TAUSUM'] 	= $auth->get_user();
            $ar_ins['TADTUM']   = oggi_AS_date();
            $ar_ins["TADESC"] = acs_toDb($m_params->form_values->f_text_1);
            $ar_ins["TADES2"] = acs_toDb($m_params->form_values->f_text_2);
            $ar_ins["TAREST"] = $tarest;
            $ar_ins["TADT"]   = $id_ditta_default;
            $ar_ins["TAID"]   =  $m_params->taid;
            $ar_ins["TANR"]  = $m_params->tanr;
            $ar_ins["TALINV"] = acs_toDb($m_params->lng);
            
            if(isset($m_params->tacor2) && strlen($m_params->tacor2) > 0)
                $ar_ins["TACOR2"]  = $m_params->tacor2;
            
        
            $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tab_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
       
        
    }

    
    $ret = array();
    $ret['success'] = true;
    $ret['row'] = "<b>{$m_params->form_values->f_text_1}<br>
    {$m_params->form_values->f_text_2}<br>
    {$m_params->form_values->f_text_3}<br>
    {$m_params->form_values->f_text_4}</b>";
    echo acs_je($ret);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
      if(isset($m_params->tacor2) && strlen(trim($m_params->tacor2)) > 0)
         $sql_where = " AND TACOR2 = '{$m_params->tacor2}'";
  
        $sql = "SELECT RRN(TA) AS RRN, TADESC, TADES2, TAREST
                FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
                WHERE TADT = '{$id_ditta_default}'
                AND TAID = '{$m_params->taid}'
                AND TANR = '{$m_params->tanr}'
                AND TALINV = '{$m_params->lng}' {$sql_where}";
        
       
        
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        $row['trad1'] = trim($row['TADESC']);
        $row['trad2'] = trim($row['TADES2']);
        $row['trad3'] = substr(acs_u8e($row['TAREST']), 0, 30);
        $row['trad4'] = substr(acs_u8e($row['TAREST']), 30, 30);
        $row['trad5'] = substr(acs_u8e($row['TAREST']), 60, 30);
        
         ?>
 {"success":true, "items": [
        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            
					buttons: [{
			            text: 'Salva',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var form_values = form.getValues();
							 var loc_win = this.up('window');
							
							 
								if(form.isValid()){
									Ext.Ajax.request({
									        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_salva_traduzione',
									        jsonData: {
									        	form_values: form.getValues(),
									        	tanr : '<?php echo $m_params->tanr; ?>',
									        	taid : '<?php echo $m_params->taid; ?>',
									        	tacor2 : '<?php echo $m_params->tacor2; ?>',
									        	lng : '<?php echo $m_params->lng; ?>',
									        	rrn : '<?php echo $row['RRN']; ?>',
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){	            	  													        
									            var jsonData = Ext.decode(result.responseText);
									          	loc_win.fireEvent('afterSave', loc_win, jsonData);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									            console.log('errorrrrr');
									        }
									    });	  
							    }							 
				
			            }
			         }],   		            
		
		            items: [{
    					xtype: 'fieldset',
    	                title: 'Lingua : <?php echo $m_params->lng; ?>',
    	                layout: 'anchor',
    	                flex:1,
    	                items: [
    	                 
    	                <?php  for($i=1; $i<= 5;$i++){ ?>
    	                
                    	{
    						name: 'f_text_<?php echo $i ?>',
    						xtype: 'textfield',
    						fieldLabel: '',
    						anchor: '-15',
    					    maxLength: 30,					    
    					    value: '<?php echo trim(acs_u8e($row["trad{$i}"])); ?>',					
    					},	
    					
    									
					<?php   }?>	
								
									
					 
	             ]}
								 
		           			]
		              }
		
			]
}	
	
	
	
<?php 
exit;
}

if ($_REQUEST['fn'] == 'open_al'){
    $m_params = acs_m_params_json_decode();
    $lingue = $cfg_mod_DeskUtility['ar_lingue'];
    
    if(isset($m_params->tacor2) && strlen(trim($m_params->tacor2)) > 0)
        $sql_where = " AND TACOR2 = '{$m_params->tacor2}'";

    $sql = "SELECT RRN(TA) AS RRN, TADESC,TADES2,TAREST
            FROM {$cfg_mod_Spedizioni['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}'
            AND TANR = '{$m_params->tanr}'
            AND TAID = '{$m_params->taid}'
            AND TALINV = ?  {$sql_where}";
    
    
    $stmt = db2_prepare($conn, $sql);
      
?>

{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: false,
	            autoScroll : true,
	            title: '',
	            
	            items: [
	          
	 	
	 	<?php 
	 	
	 	foreach($lingue as $k => $v){
	 	    
	 	    $result = db2_execute($stmt, array($v['id']));
	 	    $row = db2_fetch_assoc($stmt);
	 	    
	 	    $trad1 = trim($row['TADESC']);
	 	    $trad2 = trim($row['TADES2']);
	 	    $trad3 = substr(acs_u8e($row['TAREST']), 0, 30);
	 	    $trad4 = substr(acs_u8e($row['TAREST']), 30, 30);
	 	    $trad5 = substr(acs_u8e($row['TAREST']), 60, 30);
	 	
	 	    $trad = "<b>{$trad1}<br>{$trad2}<br>{$trad3}<br>{$trad4}<br>{$trad5}</b>";
	 	
	 	?>
	 	
	 			
							 {
					xtype: 'fieldset',
	                title: <?php echo j($v['text'])?>,
	                layout: 'anchor',
	                flex:1,
	                items: [
						
							 {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox'},						
						items: [
            	          		{xtype : 'displayfield',
            	          		 name : 'f_ling_<?php echo $v['id']?>',
            	          		 flex: 1,
            	          		 labelWidth : 60,
            	          		 value : <?php echo j($trad); ?>
            	          		
            	          		},
						{
								xtype: 'button',
								text: 'Modifica',
								scale: 'small',
								handler : function(){
								
								var form = this.up('form').getForm();
			        			
								   acs_show_win_std('Traduzione ' + '<?php echo $v['text']; ?>', 
				    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
				    					{tanr: '<?php echo $m_params->tanr; ?>', lng: '<?php echo $v['id']; ?>', taid : '<?php echo $m_params->taid; ?>', tacor2 : '<?php echo $m_params->tacor2; ?>'}, 400, 270,  {
			        					'afterSave': function(from_win, new_trad){
			        					    form.findField('f_ling_<?php echo $v['id']?>').setValue(new_trad.row);
			        						from_win.close();
			        					}
			        				}, 'icon-globe-16');
			        		
									
								} 
							}
						    
						
						]}	   
 					
					 
	             ]},
						
						<?php }	?>
						    
						    
	           
	          		
	            ]
	                     
	            
	           
	}
		
	]}

<?php 
exit;
}