<?php



function artd_ARART_add_where($radice){
    //AND ARART LIKE '{$radice}%'
    
    $radice = str_replace(" ", "_", $radice);
    $radice = str_replace("?", "_", $radice);
    
    if (strlen($radice) == 0)
      return "";
    $exp_radice = explode("|", $radice);
    if (count($exp_radice) == 1)
      return " AND ARART LIKE '{$radice}%' ";
    else { //ho valore divisi da | pipe
      $ar_tmp = array();
      foreach($exp_radice as $r){
          $ar_tmp[] = " ARART LIKE '{$r}%' ";
      }
      return " AND (" . implode(" OR ", $ar_tmp) . ") ";
    }
}


function gest_form_values($form_values){
     
    $sql_where_ar = "";
    
    if(isset($form_values->f_sosp) && $form_values->f_sosp == 'Y')
        $sql_where_ar .= " AND ARSOSP = 'S'";
    
    return $sql_where_ar;
}

function get_risposte_START($row, $form_values){
    
    global $cfg_mod_DeskArt, $conn, $id_ditta_default;
    $desk_art = new DeskArt();
    
    $sql_where_ar = gest_form_values($form_values);
     
        $nr = array();
     
        $nr['gruppo'] = trim($row['CDSLAV']);
        $nr['distinta'] = trim($row['CDDICL']);
        $nr['desc'] = trim($row['CDDESC']);
        $nr['voce'] = $row['CDSLAV'];
        $nr['radice'] = trim($row['CDRSTR']);
        $nr['ul_row'] = get_cd_row($nr['distinta'], $nr['gruppo']);
        
        $sql_ar = "SELECT COUNT(*) AS C_ROW
                    FROM {$cfg_mod_DeskArt['file_anag_art']} AR
                    WHERE ARDT = '{$id_ditta_default}'
                   {$sql_where_ar}
                    ". artd_ARART_add_where(trim($row['CDRSTR'])) ."";
     
        $stmt_ar = db2_prepare($conn, $sql_ar);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_ar);
        $row_ar = db2_fetch_assoc($stmt_ar);
        $nr['red_ant'] = $row_ar['C_ROW'];
        
        $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
        $chiave = implode ("|", $ar_comp);
        $nr['nota'] =  $desk_art->has_nota_indici($chiave);
        $row_nt = $desk_art->get_note_indici($chiave);
        $nr['t_nota'] = utf8_decode($row_nt['NTMEMO']);
        $nr['todo'] = trim($row['CDTODO']);
        $todo = $desk_art->get_TA_std('ATTAV', $nr['todo']);
        $nr['t_todo'] = "[".trim($row['CDTODO'])."] ".$todo['TADESC'];
        $ar[] = $nr;    
    
    return $ar;    
}



function get_risposte_L($rec, $radice_temp = ''){
    
    global $cfg_mod_DeskArt, $conn, $id_ditta_default;
    $desk_art = new DeskArt();
    
    $sql = "SELECT * 
            FROM {$cfg_mod_DeskArt['file_cfg_distinte_liste']}
            WHERE CLDT = '{$id_ditta_default}' AND CLCDLI = '{$rec['CDCDLI']}'";    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['gruppo'] = trim($rec['CDCMAS']) . trim($rec['CDSLAV']);
        $nr['desc'] = trim($row['CLDESC']);
        $nr['voce'] = trim($rec['CDSLAV']);
        $nr['opzione'] = trim($row['CLCELE']);
        
        //$nr['flag'] = trim($rec['CDFLLG']);
        $nr['flag']         = trim($rec['CDFLGA']);
        $nr['tipo_lista']   = trim($rec['CDFLLG']);
        $ar_comp = array(trim($rec['CDDT']), trim($rec['CDDICL']), trim($rec['CDCMAS']), trim($rec['CDSEQU']), trim($rec['CDSLAV']));
        $chiave = implode ("|", $ar_comp);
        $nr['nota'] =  $desk_art->has_nota_indici($chiave);
        $row_nt = $desk_art->get_note_indici($chiave);
        $nr['t_nota'] = utf8_decode($row_nt['NTMEMO']);
        $nr['todo'] = trim($rec['CDTODO']);
        $todo = $desk_art->get_TA_std('ATTAV', $nr['todo']);
        $nr['t_todo'] = "[".trim($rec['CDTODO'])."] ".$todo['TADESC'];
        $nr['distinta'] = trim($rec['CDDICL']);
                
        //da config. lista in distinta
        $nr['p_da'] = trim($rec['CDSTIN']);
        $nr['p_a'] = trim($rec['CDSTFI']);
        
        $nr['ul_row'] = get_cd_row($nr['distinta'], $nr['gruppo']);
        
        $nr['conf_radice'] = show_conf_radice($rec, $row['CLCELE']);
        $nr['radice']       = crea_nuova_radice($radice_temp, $row['CLCELE'], $nr['p_da'], $nr['p_a']);
        //$nr['radice'] = trim($row['CLCELE']);
        
        //Per duplica
        // Default da configurazione CD (lista), eventualmente specifico per voce lista
        if (strlen(trim($row['CLNEWC'])) > 0){
            $nr['new'] = trim($row['CLNEWC']);
            $nr['prog'] = trim($row['CLPROG']);
            $nr['nr_id'] = trim($row['CLNCAR']);
            $nr['ardu'] = trim($row['CLARDU']);
        } else {
            $nr['new'] = trim($rec['CDNEWC']);
            $nr['prog'] = trim($rec['CDPROG']);
            $nr['nr_id'] = trim($rec['CDNCAR']);
            $nr['ardu'] = trim($rec['CDARDU']);
        }        
        
        $ar[] = $nr;
    }
    
    return $ar;
    
}

function get_risposte_A($radice, $master, $rec){
    
    global $cfg_mod_DeskArt, $conn, $id_ditta_default;
    $desk_art = new DeskArt();
    
    $sql = "SELECT ARART, ARDART
            FROM {$cfg_mod_DeskArt['file_anag_art']} AR
            WHERE ARDT = '{$id_ditta_default}' " . artd_ARART_add_where($radice);
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['codice'] = trim($row['ARART']);
        $nr['desc'] = trim($row['ARDART']);
        $nr['voce'] = trim($rec['CDSLAV']);
        $nr['radice'] = trim($row['ARART']);
        $ar_comp = array(trim($rec['CDDT']), trim($rec['CDDICL']), trim($rec['CDCMAS']), trim($rec['CDSEQU']), trim($rec['CDSLAV']));
        $chiave = implode ("|", $ar_comp);
        $nr['nota'] =  $desk_art->has_nota_indici($chiave);
        $row_nt = $desk_art->get_note_indici($chiave);
        $nr['t_nota'] = utf8_decode($row_nt['NTMEMO']);
        $nr['todo'] = trim($rec['CDTODO']);
        $todo = $desk_art->get_TA_std('ATTAV', $nr['todo']);
        $nr['t_todo'] = "[".trim($rec['CDTODO'])."] ".$todo['TADESC'];
        $ar[] = $nr;
    }
    
    return $ar;
    
}


function show_conf_radice($row, $val){
    $da = $row['CDSTIN'];
    $a  = $row['CDSTFI'];
    if ((int)$da == 0 || (int)$a == 0) return '';
    return trim($row['CDSTIN']). " - " .trim($row['CDSTFI']).", ". trim($val);
}

function crea_nuova_radice($radice_temp, $radice, $da, $a){
    
    if ((int)$da == 0 || (int)$a == 0) return $radice_temp;
    
    $ret = '';
    $radice_orig = str_split($radice_temp);    
    $radice_app = str_split(str_repeat(" ", $da - 1) . $radice);

    for ($i=0; $i<100; $i++){

        if ($i+1 >= $da && $i+1 <= $a)        
          $ret .= $radice_app[$i];
        else if(isset($radice_orig[$i]))
          $ret .= $radice_orig[$i];
        else
          $ret .= ' ';
    }

    return trim($ret);
}



function get_risposte_G($master, $row, $radice_temp = ''){    
    global $cfg_mod_DeskArt, $conn, $id_ditta_default;
    $ar = array();
    $desk_art = new DeskArt();
    
    $sql_where_ar = gest_form_values($form_values);

    $nr = array();    
    $nr['gruppo'] = $master.trim($row['CDSLAV']);
    $nr['master'] = $master;
    $nr['distinta'] = trim($row['CDDICL']);
    $nr['desc'] = trim($row['CDDESC']);
    $nr['voce'] = trim($row['CDSLAV']);
    
    //Per Duplica
    $nr['new'] = trim($row['CDNEWC']);
    $nr['prog'] = trim($row['CDPROG']);
    $nr['nr_id'] = trim($row['CDNCAR']);
    $nr['ardu'] = trim($row['CDARDU']);
    
    $nr['idme'] = trim($row['CDIDME']);
    $nr['flag'] = trim($row['CDFLGA']);
    $nr['p_da'] = trim($row['CDSTIN']);
    $nr['p_a'] = trim($row['CDSTFI']);
    $nr['ul_row'] = get_cd_row($nr['distinta'], $nr['gruppo']);
    
    $nr['conf_radice'] = show_conf_radice($row, $row['CDRSTR']);
    $nr['radice']       = crea_nuova_radice($radice_temp, $row['CDRSTR'], $nr['p_da'], $nr['p_a']);
    //$nr['radice'] = trim($row['CDRSTR']);
    $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
    $chiave = implode ("|", $ar_comp);
    $nr['nota'] =  $desk_art->has_nota_indici($chiave);
    $row_nt = $desk_art->get_note_indici($chiave);
    $nr['t_nota'] = utf8_decode($row_nt['NTMEMO']);
    $nr['todo'] = trim($row['CDTODO']);
    $todo = $desk_art->get_TA_std('ATTAV', $nr['todo']);
    $nr['t_todo'] = "[".trim($row['CDTODO'])."] ".$todo['TADESC'];
    
    $sql_ar = "SELECT COUNT(*) AS C_ROW
                FROM {$cfg_mod_DeskArt['file_anag_art']} AR
                WHERE ARDT = '{$id_ditta_default}'
                {$sql_where_ar}
                ". artd_ARART_add_where(trim($nr['radice'])) ."";

    $stmt_ar = db2_prepare($conn, $sql_ar);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_ar);
    $row_ar = db2_fetch_assoc($stmt_ar);
    $nr['red_ant'] = $row_ar['C_ROW'];
    
    $ar[] = $nr;    
    return $ar;
}




function get_cd_row($distinta, $gruppo){
    
    global $cfg_mod_DeskArt, $conn, $id_ditta_default;
 
        
    $sql = "SELECT COUNT(*) AS C_ROW
            FROM {$cfg_mod_DeskArt['file_cfg_distinte']}
            WHERE CDDT = '{$id_ditta_default}' AND CDDICL = '{$distinta}'
            AND CDCMAS = '{$gruppo}'";
    
      
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        if($row['C_ROW'] > 0)
            $ul_row = 'N';
        else     
            $ul_row = 'Y';
    
      return $ul_row;
    
}