<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    $nr['tades2'] = $row['TADES2'];  
    $desc_camp = get_TA_sys('CAMP', trim($row['TACOR2']));
    $nr['d_camp'] = "[".trim($row['TACOR2'])."] ".$desc_camp['text'];
    $nr['camp'] = trim($row['TACOR2']); 
    $nr['val_ini'] = substr($row['TAREST'], 0, 8);
    $nr['val_fin'] = substr($row['TAREST'], 8, 8);
    //$nr['ordine'] = $row['TAORDI'];
   
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'val_ini', 'val_fin', 'camp', 'd_camp');

    
    return $ar_fields;
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
           {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            },{
            header   : 'Note',
            dataIndex: 'tades2',
            flex : 1
            },{
            header   : 'Campagna',
            dataIndex: 'd_camp',
            flex : 1
            },
            {header: 'Data validit&agrave;',
            columns: [
              {header: 'Iniziale', dataIndex: 'val_ini', renderer: date_from_AS, width: 60, sortable: true}
             ,{header: 'Finale', dataIndex: 'val_fin', renderer: date_from_AS, width: 60, sortable: true}
         	 ]}

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "note" => trim($row->tades2),
        "val_ini" => trim($row->val_ini),
        "val_fin" => trim($row->val_fin),
        "camp" => trim($row->camp),
    );
 
    return $ar_values;
    
}


function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		maxLength: 3,
		anchor: '-15',	
		value:  <?php echo j($ar_values['riga']); ?>
	  },
	  {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
    	  {xtype: 'textfield',
    	name: 'note',
    	fieldLabel: 'Note',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['note']); ?>
    	},{name: 'camp',
			xtype: 'combo',
			fieldLabel: 'Campagna',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['camp']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            width : 400,
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('CAMP'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	        },{ xtype: 'datefield'
			   , startDay: 1 //lun.
			   , fieldLabel: 'Validit&agrave; iniziale'
			   , value : '<?php echo $ar_values['val_ini']; ?>'
			   , name: 'val_ini'
			   , format: 'd/m/Y'					   
			   , submitFormat: 'Ymd'
			   , allowBlank: false
			   , listeners: {
			       invalid: function (field, msg) {
			       Ext.Msg.alert('', msg);}
				}
			}, {
			   xtype: 'datefield'
			   , startDay: 1 //lun.
			   , fieldLabel: 'Validit&agrave; finale'
			   , value : '<?php echo $ar_values['val_fin']; ?>'
			   , name: 'val_fin'
			   , format: 'd/m/Y'							   
			   , submitFormat: 'Ymd'
			   , allowBlank: false
			   , listeners: {
			       invalid: function (field, msg) {
			       Ext.Msg.alert('', msg);}
				}
			}	
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->val_ini);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->val_fin);
    $ar_ins['TANR']  .= sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] .= $form_values->descrizione;
    $ar_ins['TADES2'] .= $form_values->note;
    $ar_ins['TACOR2'] .= $form_values->camp;
    
    
    return $ar_ins;
    
}