<?php

require_once "../../config.inc.php";
require_once("acs_panel_cmr.php");

$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
$main_module = new Spedizioni();


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>


<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv1 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv3 td{font-weight: bold; font-size: 0.9em;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
   
   div.with_todo_tooltip{text-align: right;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>


<?php



//bug parametri linux
global $is_linux;
if ($is_linux == 'Y')
    $_REQUEST['form_values'] = strtr($_REQUEST['form_values'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
    
    $form_ep = (array)json_decode($_REQUEST['form_values']);

$ar = crea_ar_tree_CRM($_REQUEST['node'], (object)$form_ep);

//stampo
$cl_liv_cont = 0;
$liv1_row_cl = 3;
$liv2_row_cl = 2;

echo "<div id='my_content'>";

echo "
  		<div class=header_page>
			<H2>Riepilogo documenti in attesa di rientro CMR</H2>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>";
		
foreach ($ar as $kl0 => $l0){
	
	echo liv0_intestazione_open($l0['record'], ++$c_liv0);
	echo liv_std_intestazione_open($l0, 1);	

	foreach ($l0['children'] as $kl1 => $l1){
		echo liv_std_intestazione_open($l1, $liv1_row_cl);
			
		foreach ($l1['children'] as $kl2 => $l2){
			echo liv_std_intestazione_open($l2, $liv2_row_cl);

			foreach ($l2['children'] as $kl3 => $l3){
				echo liv_std_intestazione_open($l3, $liv3_row_cl, 'Y');
					
				//foreach ($l3['children'] as $kriga => $l_riga)
				//	echo liv_riga_intestazione_open($l_riga, $liv3_row_cl);

			}

			if ($_REQUEST['stampa_rilevazione_scarico']=="Y")
				echo liv2_rilevazione_scarico();

		}
	}

	echo liv0_intestazione_close($l0['record']);
}
echo "</div>";











//Agenzia
function liv0_intestazione_open($r, $c_liv0 = 1){
	global $s, $spedizione;

	$ret = "";

	//salto pagina
	if ($c_liv0 > 1)
		$ret .= "<div class=\"page-break\"></div>";	
			
			
	$ret .= "
	<table class=int1>
			 <tr>
			  <th>Nazione / Cliente / Documento</th>
			  <th>Codice</th>
			  <th>Data</th>					
			  <th class=number>Totale documento</th>
			  <th class=number>Totale imponibile</th>
			  <th class=number>Ancitipo</th>
			  <th class=number>Netto merce</th>
			  <th>Vettore</th>
			  <th>Memo</th>
			 </tr>							
			";

	return $ret;
}

function liv0_intestazione_close($r){
return "</table><br/>&nbsp;<br/>";

}




function liv_std_intestazione_open($l, $cl_liv, $is_row_documento = 'N'){
	$n_col_span = 6;

	$add_task = "";
	
	if ($is_row_documento == 'Y'){
		$as = new SpedAssegnazioneOrdini;
		$ret_stato_tooltip = $as->stato_tooltip_entry_per_ordine($l['liv_cod'], null, 'Y');
		$add_task = "<div>{$ret_stato_tooltip['tooltip']}</div>";
		
		$bd_exp = explode("_", $l['bolletta_doganale']);
		if (count($bd_exp) == 7)
			$out_bolletta_doganale = implode("_", array($bd_exp[4]));
		else
			$out_bolletta_doganale = trim($l['bolletta_doganale']);

		$r = $l;
		if (trim($r['TFBDOG']) == 'P')
			$ar_bolletta_doganale_ar = array($out_bolletta_doganale . " (P)");
		else
			$ar_bolletta_doganale_ar = array($out_bolletta_doganale);
		
		$ar_bolletta_doganale_data_ar = array(print_date($r['TF_ABB_TFDTRG']));
				
		if (strlen(trim($r['TF_ABB_NOTE'])) > 0){
			$ar_bolletta_doganale_ar[] = "[" . acs_u8e(trim($r['TF_ABB_NOTE'])) . "]";
			$ar_bolletta_doganale_data_ar[] = "&nbsp;";
		}				
		
		//bolletta doganale secondaria (se abbinamento parziale)
		if (strlen(trim($r['TFDOA2'])) > 0){
			$bd_exp = explode("_", $r['TFDOA2']);
			if (count($bd_exp) == 7)
				$ar_bolletta_doganale_ar[] = implode("_", array($bd_exp[4]));
			else
				$ar_bolletta_doganale_ar[] = trim($r['TFDOA2']);
				
			$ar_bolletta_doganale_data_ar[] = print_date($r['TF_ABB_2_TFDTRG']);
				
			if (strlen(trim($r['TF_ABB_2_NOTE'])) > 0){
				$ar_bolletta_doganale_ar[] = "[" . acs_u8e(trim($r['TF_ABB_2_NOTE'])) . "]";
				$ar_bolletta_doganale_data_ar[] = "&nbsp;";
			}
		}
		
		$out_bolletta_doganale = implode("<br/>", $ar_bolletta_doganale_ar);
		$out_bolletta_doganale_data = implode("<br/>", $ar_bolletta_doganale_data_ar);
		
	}

	
	
	$ret = "
		<tr class=liv{$cl_liv}>
		<td>" . trim($l['task']) . "</TD>
		<td>" . trim($l['liv_cod_out']) . "</TD>
		<td>" . print_date($l['liv_data']) . "</TD>
		<td class=number>" . n($l['tot_documento']) . "</TD>
		<td class=number>" . n($l['tot_imponibile']) . "</TD>
		<td class=number>" . n($l['tot_anticipo']) . "</TD>
	    <td class=number>" . n($l['tot_netto_merce']) . "</TD>
	    <td>" . acs_u8e(trim($l['vettore_out'])) . "</TD>
 		<td>" . $add_task . "</td>											
	 ";

	$ret .="</tr>";
	return $ret;
}



//Agente
function liv1_intestazione_open($l, $cl_liv){

	$n_col_span = 4;


	$span_carico_class = '';
	if ($_REQUEST['stampa_dettaglio_ordini'] != "Y" && $_REQUEST['stampa_dettaglio_ordini'] != "R")
		$span_carico_class = 'grassetto';

	$ret = "
		<tr class=liv{$cl_liv}>
		<td colspan={$n_col_span}><span class=\"{$span_carico_class}\">AGENTE " . trim($l['descr']) . " [" . trim($l['record']['RFCAGE']) . "]</span>" . $orario_carico . $note_carico . "</TD>
		<td class=number colspan=6>" . n($l['val']['RFIMPO']) . "</pre></td>
	 ";

	$ret .="</tr>";
	return $ret;
}






//Cliente
function liv2_intestazione_open($l, $cl_liv){

	global $main_module, $cfg_mod_Spedizioni;



	$td_cli_class = '';
	if ($_REQUEST['r_type'] == 'per_cliente')
		$td_cli_class = 'grassetto';

	$txt_note_cell_cli = '';

	$ret = "
	 <tr class=\"tr-cli liv{$cl_liv}\">
	 <td colspan=1 class=\"{$td_cli_class}\">" . $l['record']['TFDCON'] . " [" . $l['record']['TFCCON']  . "]</TD>
	 <td colspan=5>&nbsp;</td>
	 <td class=number colspan=4>" . n($l['val']['RFIMPO']) . "</pre></td>
	";	

	$ret .= "</tr>";


	return $ret;
}




//Documento/riga
function liv3_intestazione_open($l, $cl_liv){


	if ($_REQUEST['stampa_dettaglio_ordini']=="R")
		$add_grassetto = " grassetto ";
	else
		$add_grassetto = "";

	$n_col_span = 3;


	$ret = "
		<tr class=\"liv{$cl_liv} $add_grassetto\">
		<td>&nbsp;</td>
		<td>" . implode("_", array($l['record']['TFAADO'],  $l['record']['TFNRDO'], $l['record']['TFTPDO'])) . "</TD>
		<td>" . print_date($l['record']['TFDTRG']) . "</TD>
		<td class=number>" . n($l['record']['TFTIMP']) . "</TD>					
		<td class=number>" . n($l['record']['RFIMPP']) . "</TD>
	    <td class=number>" . n_auto($l['record']['RFPPRA']) . " %</TD>
	    <td class=number>" . n($l['record']['RFIMPO']) . "</TD>
	 ";

	$ret .= "</tr>";
	return $ret;
}





function liv_riga_intestazione_open($l, $cl_liv){


	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 4;
	else
		$n_col_span = 3;

	if ($_REQUEST['stampa_data_scarico']=='Y')
		$n_col_span++;


	$ret = "
	<tr class=liv{$cl_liv}>
	<td colspan=1>&nbsp;</TD>
	<td colspan={$n_col_span}>" . trim($l['DES_ART']) . " [" . trim($l['COD_ART']) . "]" . "</TD>
    <td colspan=1 class=number>" . n($l['QTA_RIGA']) . " [" . trim($l['UM_RIGA']) . "]" . "</TD>";

	if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>&nbsp;</TD> ";
	$ret .="
		 <td colspan=1 class=number>&nbsp;</TD>
		 <td colspan=1 class=number>&nbsp;</TD>
		 ";
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>&nbsp;</TD>";

	$ret .= "</tr>";
	return $ret;
}







?>





 </body>
</html>