<?php

require_once("../../config.inc.php");
require_once("acs_panel_fatture_anticipo_include.php");

$main_module = new DeskGest();

if ($_REQUEST['fn'] == 'get_json_rapporto_sincro_data'){
	
	$m_params = acs_m_params_json_decode();
	
  
	$m_where_ar = array();

	$sql = "SELECT * FROM {$cfg_mod_Gest['fatture_anticipo']['file_righe']}  WHERE RFNRDO LIKE '{$m_params->open_request->chiave}%' ORDER BY RFDTGE DESC, RFORGE DESC";
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, $m_where_ar);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['data'] 	= $r['RFDTGE'];
		$r['ora'] 	= $r['RFORGE'];
		$r['segnalazioni'] 	= trim($r['RFNOTE']);
		$r['data_ora'] = print_date($r['data']) . " " . print_ora($r['ora']);
		$ar[] = $r;
	}
	
	echo acs_je($ar);
	exit();
}

	if ($_REQUEST['fn'] == 'open_tab'){
	
		$m_params = acs_m_params_json_decode();
	
		?>
		
	{"success":true, "items":
	 {
	  xtype: 'panel',
	  title: 'SynchroLog',
	  closable: true,
	  autoScroll: true,    
	  items: [		
		{
			xtype: 'grid',
			title: 'Rapporto sincronizzazione fatture anticipo',
			loadMask: true,
			
			features: [
			new Ext.create('Ext.grid.feature.Grouping',{
				groupHeaderTpl: 'Data/ora elaborazione: {name}',
				sortInfo:{field: 'data_ora', direction: 'DESC'},			
				hideGroupedHeader: false
			}),
			{
				ftype: 'filters',
				// encode and local configuration options defined previously for easier reuse
				encode: false, // json encode the filter query
				local: true,   // defaults to false (remote filtering)
		
				// Filters are most naturally placed in the column definition, but can also be added here.
			filters: [
			{
				type: 'boolean',
				dataIndex: 'visible'
			}
			]
			}],
		
			store: {
				xtype: 'store',
					
				groupField: 'data_ora',	
				groupOnSort: false, remoteGroup: true,			
				sortInfo:{field: 'data_ora', direction: 'DESC'},					
				autoLoad:true,
		
				proxy: {
					url: 'acs_panel_rapporto_sincronizzazione.php?fn=get_json_rapporto_sincro_data',
					method: 'POST',
					type: 'ajax',
					
					//Add these two properties
					actionMethods: {
						read: 'POST'					
					},
		
					reader: {
						type: 'json',
						method: 'POST',
						root: 'root'					
					},
				 extraParams: {
                                    open_request: <?php echo acs_je(array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())) ?>
                                }, 
				doRequest: personalizza_extraParams_to_jsonData
				},
				
				
					
				fields: ['data', 'ora', 'segnalazioni', 'data_ora']
							
							
			}, //store
			multiSelect: false,
		
			columns: [
				{header: 'Segnalazioni',  dataIndex: 'segnalazioni', flex: 1, filterable: true}
			]
			 
		}
	]
	 } 
	
	}	
		
		
	<?php 	
	  exit();
	}
