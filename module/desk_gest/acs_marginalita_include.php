<?php


function get_field_for_group($tipo_raggruppamento){
	switch ($tipo_raggruppamento){
		case 'f_centro_di_costo': return array('SMCTD0', 'SMCDE0');
		case 'f_agente'			: return array('COD_AGE', 'DES_AGE');
		case 'f_nazione'		: return array('COD_NAZ', 'DES_NAZ');		
		case 'f_nielsen'		: return array('COD_NIELSEN', 'COD_NIELSEN');
	}
}

function get_short_des_for_group($tipo_raggruppamento){
	switch ($tipo_raggruppamento){
		case 'f_centro_di_costo': return 'Centro';
		case 'f_agente'			: return 'Agente';
		case 'f_nazione'		: return 'Nazione';
		case 'f_nielsen'		: return 'Nielsen';
	}
}



function sql_where_by_request($m_params, $solo_su_mov = 'N', $add_filtri_utente = 'N', $tab_alias = '', $escludi_filtro_su = '______'){
	if (strlen(trim($m_params->open_tab_filters->nome_analisi_1)) > 0)
		$sql_where .= " AND {$tab_alias}NOME_ANALISI = " . sql_t($m_params->open_tab_filters->nome_analisi_1);

	if ($solo_su_mov != 'Y')
		$sql_where .= " AND {$tab_alias}NOME_ANALISI_CONFR = " . sql_t($m_params->open_tab_filters->nome_analisi_2);

	if (count($m_params->open_tab_filters->f_ditta) > 0){
		$sql_where .= " AND {$tab_alias}CODICE_DITTA IN(" . sql_t_IN($m_params->open_tab_filters->f_ditta) .")";
	}

	if (count($m_params->open_tab_filters->codice_ditta) > 0){
		$sql_where .= " AND {$tab_alias}CODICE_DITTA = " . sql_t($m_params->open_tab_filters->codice_ditta);
	}	

	if (count($m_params->open_tab_filters->f_divisione) > 0){
		$sql_where .= " AND {$tab_alias}COD_DIV IN(" . sql_t_IN($m_params->open_tab_filters->f_divisione) .")";
	}
	
	
	
	//i filtri fatti dopo l'apertura (agente, nazione, ....)
	if ($add_filtri_utente == 'Y'){
		if ('f_centro_di_costo' != $escludi_filtro_su) $sql_where .= sql_where_by_combo_value("{$tab_alias}SMCTD0",  $m_params->m_filters->f_centro_di_costo);
		if ('f_agente' != $escludi_filtro_su) $sql_where .= sql_where_by_combo_value("{$tab_alias}COD_AGE", $m_params->m_filters->f_agente);
		if ('f_nazione' != $escludi_filtro_su) $sql_where .= sql_where_by_combo_value("{$tab_alias}COD_NAZ", $m_params->m_filters->f_nazione);
		if ('f_nielsen' != $escludi_filtro_su) $sql_where .= sql_where_by_combo_value("{$tab_alias}COD_NIELSEN", $m_params->m_filters->f_nielsen);		
	}

	return $sql_where;
}





function write_tabella_per_accordion($input_name, $p, $open_tab_filters){
	?>
        {
            xtype: 'grid',
            flex: 1,
            title: <?php echo j($p['title']); ?>,
            layout: 'fit',
            
            
		features: [
			{
				ftype: 'filters',
				encode: false, // json encode the filter query
				local: true,   // defaults to false (remote filtering)
		
				// Filters are most naturally placed in the column definition, but can also be added here.
			filters: [
			{
				type: 'boolean',
				dataIndex: 'visible'
			}]
		}],            
            
		    tools: [ {
	            type: 'pin',
	            tooltip: 'Gestione gruppi2',
	            handler: function() {
						acs_show_win_std(<?php echo j("Gestione gruppi - {$p['title']}"); ?>, 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_win_gestione_gruppi', 
											{								  				
					                    		input_name: <?php echo j($input_name); ?>,
					                    		cod: <?php echo j($p['cod']); ?>,
					                    		des: <?php echo j($p['des']); ?>,
					                    		from_grid_id: this.up('grid').id 								  				
								  			}, 450, 300, {}, 'icon-sub_blue_add-16');
	            }
	        }],            
            
			store: {
					xtype: 'store',
					groupField: 'cod_web',
					autoLoad: true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tab_accordion',
								method: 'POST',								
								type: 'ajax',

							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
			                    		input_name: <?php echo j($input_name); ?>,
			                    		cod: <?php echo j($p['cod']); ?>,
			                    		des: <?php echo j($p['des']); ?>,
			                    		open_tab_filters: <?php echo acs_je($open_tab_filters); ?>,
			                    		m_filters: {} 
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['COD', 'DES', 'selected']							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Cod',
	                dataIndex: 'COD',
	                width: 40 
	            }, {
	                header   : 'Denominazione',
	                dataIndex: 'DES',
	                flex: 10,
	                filter: {type: 'string'}, filterable: true
	            }, {
	                header   : 'S',
	                dataIndex: 'selected',
	                width: 25,
	                filter: {type: 'string'}, filterable: true 
	            }  
	         ],																					
	         
	        listeners: {
	        
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);

							mp = iView.up('#mainPanelTab');
							if (Ext.isEmpty(mp.store.proxy.extraParams.m_filters.<?php echo $input_name;?>))       			
								mp.store.proxy.extraParams.m_filters.<?php echo $input_name;?> = [];
							
						  	
						  	if (rec.get('selected') == 'Y'){
						  		rec.set('selected', '');						  		
								Ext.Array.remove(mp.store.proxy.extraParams.m_filters.<?php echo $input_name;?>, rec.get('COD'));						  		
						  	}
						  	else {
						  	 	rec.set('selected', 'Y');
								mp.store.proxy.extraParams.m_filters.<?php echo $input_name;?>.push(rec.get('COD'));						  	 	
						  	 }

							mp.store.reload();
							
							mp.refresh_all_accordion();						  	
						  }
					  }		  	        	        

			}
			
			
			, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = '';
			           if (record.get('selected') == 'Y')
			           	ret = ' colora_riga_rosso';
			           		        	
			           return ret;																
			         }   
			    }				
				  
	            
        }   	
	
	
	<?php
} //tabella per accordion



function add_sql_where_field($v, $name_field){
	$sql = "";
	$sql_params = array();
	
	if (isset($v) && count($v) > 0){
		if (count($v) == 1){
			$sql = " AND {$name_field} = ?";
			$sql_params[] = $v;
		}
		if (count($v) > 1)
			$sql .= " AND {$name_field} IN (" . sql_t_IN($v) . ")";
	}
	
  return [$sql, $sql_params];	
}


function sql_where_by_user(){
	global $conn, $cfg_mod_Spedizioni, $id_ditta_default, $auth;
	$sql_where = "";

	//in TATAID ho le eventuali ditte ammesse per un untente. Se non presnti posso mostrare tutto
	//TADT = DITTA
	//TATAID = ABIP
	//TAKEY1 = UTDT (utente -> ditta)
	//TAKEY2 = UTENTE
	//TADES = ELENCO DITTE SEPARATE DA ","
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT=? AND TATAID='ABIP' AND TAKEY1='UTDT' AND TAKEY2=?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $auth->get_user()));
	$r = db2_fetch_assoc($stmt);

	if (strlen(trim($r['TADESC'])) > 0){
		$ar_ditte = explode(",", trim($r['TADESC']));
		$sql_where .= " AND D.CODICE_DITTA IN(" . sql_t_IN($ar_ditte) .") ";
	}

	return $sql_where;
}


function sql_where_by_form_filters($m_filters){
	$sql = "";
	$sql_params = array();
	
		list($sql_field, $params_field) = add_sql_where_field($m_filters->f_centro_di_costo, "SMCTD0");
		$sql .= $sql_field;
		$sql_params = array_merge($sql_params, $params_field);
	
	return [$sql, $sql_params];	
}





// ###################################################
if ($_REQUEST ['fn'] == 'get_json_data_centri_di_costo') {
// ###################################################
	$ar = array ();
	$cfg_mod = $main_module->get_cfg_mod ();
	
	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT SMCTD0, SMCDE0
			FROM {$cfg_mod_Gest['abi']['file_movimenti']}
			WHERE UPPER(
			REPLACE(REPLACE(SMCDE0, '.', ''), ' ', '')
			)  " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			GROUP BY SMCTD0, SMCDE0
 			";
	
	$stmt = db2_prepare ( $conn, $sql );
	echo db2_stmt_errormsg ();
	$result = db2_execute ( $stmt );
	
	$ret = array ();
	// $ret[] = array("cod" => '', "descr" => "- Tutti - ");
	
	while ( $row = db2_fetch_assoc ( $stmt ) ) {
		
		$ret [] = array (
				"cod" => trim ( $row ['SMCTD0'] ),
				"descr" => utf8_encode ( trim ( $row ['SMCDE0'] ) ) 
		);
	}
	
	echo json_encode ( $ret );
	
 exit ();
}


?>