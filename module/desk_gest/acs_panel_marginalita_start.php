<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();

require_once("acs_marginalita_include.php");



//recupero nome_ditta
function get_nome_ditta($codice_ditta){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT DITTA FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE CODICE_DITTA=? GROUP BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($codice_ditta));
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		return $r['DITTA'];
	}
	return '';
}









//recupero elenco date bilanci (ceal) per scelta in menu iniziale
function get_ar_ceal(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT NOME_ANALISI, NOME_ANALISI_CONFR FROM {$cfg_mod_Gest['abi']['file_conti']} GROUP BY NOME_ANALISI, NOME_ANALISI_CONFR ORDER BY NOME_ANALISI";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => implode("|", array(trim($r['NOME_ANALISI']), trim($r['NOME_ANALISI_CONFR']))), "text" => implode(' -> ', array($r['NOME_ANALISI'], $r['NOME_ANALISI_CONFR'])));
	}
	return $ar;
}


//recupero elenco ditte per scelta in menu iniziale
function get_ar_ditte(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT CODICE_DITTA, DITTA 
			 FROM {$cfg_mod_Gest['abi']['file_conti']} D
			 WHERE 1=1 " . sql_where_by_user() . "
			 GROUP BY CODICE_DITTA, DITTA ORDER BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['CODICE_DITTA']), "text" => $r['DITTA']);
	}
	return $ar;
}


//recupero elenco ditte per scelta in menu iniziale
function get_ar_divisioni(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT COD_DIV , DES_DIV
	FROM {$cfg_mod_Gest['abi']['file_movimenti']} D
	WHERE 1=1 " . sql_where_by_user() . "
			 GROUP BY COD_DIV, DES_DIV ORDER BY DES_DIV";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD_DIV']), "text" => $r['DES_DIV']);
	}
	return $ar;
}




// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_report'){
	include_once('acs_panel_marginalita_start_report.php');
	exit;
}





// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tab_accordion'){	
	$m_params = acs_m_params_json_decode();

	$sql_where = "";
	$sql_where .= sql_where_by_request($m_params, 'Y', 'Y', 'M.', $m_params->input_name);
	$sql_where .= sql_where_by_user();
	
	$sql = "SELECT {$m_params->cod} AS COD, {$m_params->des} AS DES
			FROM {$cfg_mod_Gest['abi']['file_movimenti']} M
			WHERE 1=1 {$sql_where}
			GROUP BY {$m_params->cod}, {$m_params->des}
			ORDER BY {$m_params->des}
			";	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	
	$ar_selected = $m_params->m_filters->{$m_params->input_name};
	if (is_null($ar_selected))
		$ar_selected = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$r['COD'] = trim(acs_u8e($r['COD']));
		$r['DES'] = trim(acs_u8e($r['DES']));
		
		//verifico se e' tra quelli selezionati
		if (in_array(trim($r['COD']), $ar_selected))
		 $r['selected'] = 'Y';
		
		$ar[] = $r;
	}
	
	echo acs_je($ar);
 exit;
}


// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();	
	
	$sql_where = "";
	$sql_where .= sql_where_by_request(acs_m_params_json_decode(), 'Y', 'Y');		
	$sql_where .= sql_where_by_user();
		
	//recuper la somma dei record 1001 (a cui fanno riferimento le percentuali)
	$sql = "SELECT CODICE_DITTA, SUM(IMPORTO) AS TOT_1001 
			FROM {$cfg_mod_Gest['abi']['file_movimenti']} MV WHERE VOCE_ANALISI = '1001' {$sql_where}
			GROUP BY CODICE_DITTA";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);	
	while ($r = db2_fetch_assoc($stmt)){
		$tot_1001_ar[trim($r['CODICE_DITTA'])] = $r['TOT_1001'];
	}
	
	$tot_1001_ar;

	
	//creo struttura albero - i totali poi li prendo dai movimenti (anche in base ai filtri)
	$sql_where = "";
	$sql = "SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']} D WHERE 1=1 {$sql_where} ORDER BY ORDINAMENTO";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();	
	
	while ($r = db2_fetch_assoc($stmt)) {

		$id_voce = trim($r['VOCE_ANALISI']);
		
		if (!isset($ar[$id_voce])){
			$ar_n = array();
			$ar_n['id_voce'] 	= $id_voce;
			$ar_n['cod_voce'] = trim($r['VOCE_ANALISI']);
			$ar_n['voce'] 	= trim(acs_u8e($r['DES_VOCE_ANALISI']));
			$ar_n['codice_voce'] = trim($r['VOCE_ANALISI']);			
			$ar_n['tipo_voce'] 	= trim($r['TIPO_VOCE']);
			$ar_n['ditta'] = array();		
			$ar[$id_voce] = $ar_n;						
		}

	} //while $row
	
	
	
	//recupero gli importo dai movimenti
	$sql_where = "";
	$sql_params = array();
	$sql_where .= sql_where_by_request(acs_m_params_json_decode(), 'Y', 'Y');		
	$sql_where .= sql_where_by_user();
	
	$sql_add_select_tot = ',TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05';	
	$sql = "SELECT MV.CODICE_DITTA, MV.VOCE_ANALISI, MV.VOCE_ANALISI,
			SUM(MV.IMPORTO) AS IMPORTO {$sql_add_select_tot}
			FROM {$cfg_mod_Gest['abi']['file_movimenti']} MV
			WHERE 1=1 {$sql_where}
			GROUP BY MV.CODICE_DITTA, MV.VOCE_ANALISI, MV.VOCE_ANALISI {$sql_add_select_tot}
		";

	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $sql_params);
	
	while ($r = db2_fetch_assoc($stmt)) {
		
		$ar[trim($r['VOCE_ANALISI'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] += $r['IMPORTO'];
		if ($tot_1001_ar[trim($r['CODICE_DITTA'])] > 0)
			$ar[trim($r['VOCE_ANALISI'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_B'] = $ar[trim($r['VOCE_ANALISI'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] / $tot_1001_ar[trim($r['CODICE_DITTA'])] * 100; 
		
		if (strlen(trim($r['TOTALE_01'])) > 0){
			$ar[trim($r['TOTALE_01'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] += $r['IMPORTO'];
			if ($tot_1001_ar[trim($r['CODICE_DITTA'])] <> 0)
				$ar[trim($r['TOTALE_01'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_B'] = $ar[trim($r['TOTALE_01'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] / $tot_1001_ar[trim($r['CODICE_DITTA'])] * 100;
		}
		if (strlen(trim($r['TOTALE_02'])) > 0){
			$ar[trim($r['TOTALE_02'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] += $r['IMPORTO'];
			if ($tot_1001_ar[trim($r['CODICE_DITTA'])] <> 0)
				$ar[trim($r['TOTALE_02'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_B'] = $ar[trim($r['TOTALE_02'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] / $tot_1001_ar[trim($r['CODICE_DITTA'])] * 100;			
		}
		if (strlen(trim($r['TOTALE_03'])) > 0){
			$ar[trim($r['TOTALE_03'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] += $r['IMPORTO'];
			if ($tot_1001_ar[trim($r['CODICE_DITTA'])] <> 0)
				$ar[trim($r['TOTALE_03'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_B'] = $ar[trim($r['TOTALE_03'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] / $tot_1001_ar[trim($r['CODICE_DITTA'])] * 100;
		}
		if (strlen(trim($r['TOTALE_04'])) > 0){
			$ar[trim($r['TOTALE_04'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] += $r['IMPORTO'];
			if ($tot_1001_ar[trim($r['CODICE_DITTA'])] <> 0)
				$ar[trim($r['TOTALE_04'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_B'] = $ar[trim($r['TOTALE_04'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] / $tot_1001_ar[trim($r['CODICE_DITTA'])] * 100;			
		}
		if (strlen(trim($r['TOTALE_05'])) > 0){
			$ar[trim($r['TOTALE_05'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] += $r['IMPORTO'];
			if ($tot_1001_ar[trim($r['CODICE_DITTA'])] <> 0)
				$ar[trim($r['TOTALE_05'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_B'] = $ar[trim($r['TOTALE_05'])]['ditta'][trim($r['CODICE_DITTA'])]['IMPORTO_A'] / $tot_1001_ar[trim($r['CODICE_DITTA'])] * 100;			
		}
		
	} //while
	
	
	foreach($ar as $kar => $r)
		$ar_ret[] = array_values_recursive($ar[$kar]);
	
	
	
	echo acs_je(array("success" => true, "root" => $ar_ret));
	exit;
}







// ******************************************************************************************
// GESTIONE GRUPPI (AGENTI, ....)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_win_gestione_gruppi'){
	$m_params = acs_m_params_json_decode();	
	?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "GR_{$m_params->input_name}");  ?>{
			            text: 'Applica filtro',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							if(form.isValid()){			            	
								
								from_grid = Ext.getCmp(<?php echo j($m_params->from_grid_id) ?>);								
							    mp = from_grid.up('#mainPanelTab');								
								
       							//azzero attuali selezioni
								mp.store.proxy.extraParams.m_filters.<?php echo $m_params->input_name;?> = [];																								
								from_grid.store.each(function(rec,id){
								    rec.set('selected', 'N');
								});
								
								val_selected = form.getValues().<?php echo $m_params->input_name ?>;
								
								//imposto selezioni attuali
								from_grid.store.each(function(rec,id){
									if (val_selected.includes(rec.get('COD'))) {
								    	rec.set('selected', 'Y');
										mp.store.proxy.extraParams.m_filters.<?php echo $m_params->input_name;?>.push(rec.get('COD'));								    	
								    }
								});
		
								this.up('window').close();
								mp.store.reload();								
								mp.refresh_all_accordion();									
							}
			            }
			         }],   		            
		            
		            items: [{
							name: <?php echo j($m_params->input_name) ?>,
							xtype: 'combo',
							fieldLabel: 'Valori',
							displayField: 'DES',
							valueField: 'COD',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,
							multiSelect: true,
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								xtype: 'store',
								autoLoad: true,						
				  					proxy: {
											url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tab_accordion',
											method: 'POST',								
											type: 'ajax',
			
										      actionMethods: {
										          read: 'POST'
										      },
											
						                    extraParams: {
						                    		input_name: <?php echo j($m_params->input_name); ?>,
						                    		cod: <?php echo j($m_params->cod); ?>,
						                    		des: <?php echo j($m_params->des); ?>,
						                    		open_tab_filters: {},
						                    		m_filters: {} 
						        				}
						        			, doRequest: personalizza_extraParams_to_jsonData	
											, reader: {
									            type: 'json',
												method: 'POST',						            
									            root: 'root'						            
									        }
										},
										
					        			fields: ['COD', 'DES', 'selected']							
						
						}, //store						 
						}]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}






// ******************************************************************************************
// SCELTA DATA BILANCIO (CEAL....)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_scelta_ceal'){
?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "CEAL_SELECT");  ?>{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							if(form.isValid()){			            	
								acs_show_panel_std('acs_panel_marginalita_start.php?fn=open_tab', 'panel-abi-start', form.getValues());
								this.up('window').close();
							}
			            }
			         }],   		            
		            
		            items: [{
							name: 'f_ceal',
							xtype: 'combo',
							fieldLabel: 'Tipo di analisi',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: false,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_ceal(), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_ditta',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Ditta',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_ditte(), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_divisioni(), ""); ?>	
								    ] 
							}						 
						}]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
	
	$m_params = acs_m_params_json_decode();
	
	//decodifico nome_analisi e nome_analisi_confronto
	$analisi_ar = explode("|", $m_params->f_ceal);
	$m_params->nome_analisi_1 = $analisi_ar[0];
	$m_params->nome_analisi_2 = $analisi_ar[1];
	
	$sql_where = '';
	if (count($m_params->f_ditta) > 0)
		$sql_where .= " AND D.CODICE_DITTA IN(" . sql_t_IN($m_params->f_ditta) .")"; 
	
	$sql_where .= sql_where_by_user();
	
	//recupero elenco ditte (ogni ditta sara' in una colonna)
	$sql = "SELECT D.DITTA, D.CODICE_DITTA, R1001.IMPORTO_A 
			FROM {$cfg_mod_Gest['abi']['file_conti']} D
			  LEFT OUTER JOIN (SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']}) R1001
			    ON D.CODICE_DITTA = R1001.CODICE_DITTA
			   AND R1001.VOCE_ANALISI = '1001' 
	           AND R1001.NOME_ANALISI = " . sql_t($analisi_ar[0]) . "
			   AND R1001.NOME_ANALISI_CONFR = " . sql_t($analisi_ar[1]) . "
			WHERE 1=1 {$sql_where}
			GROUP BY D.CODICE_DITTA, D.DITTA, R1001.IMPORTO_A
			ORDER BY R1001.IMPORTO_A DESC";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar_ditte = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_ditte[trim($r['CODICE_DITTA'])] = trim($r['DITTA']);
	}
	
	//aggiungo Totale
	$ar_ditte['TOT'] = 'TOTALE';
	
?>
{
 success:true, items: [

	{
		xtype: 'grid',
		loadMask: true,
		itemId: 'mainPanelTab',
		m_filters: {},
		title: 'Margini_Start',
		<?php echo make_tab_closable(); ?>,
	
	
		dockedItems: [{
		    xtype: 'panel',
		    dock: 'left',

			layout: {
			    type: 'accordion',
			    align: 'stretch',
			    pack : 'start'
			},		    

			width: 250,
		
			
		    items: [
		    

		{
		    xtype: 'panel',
		    dock: 'left',
			layout: 'fit',
			title: 'Opzioni / Funzioni',
		    
		    items: [

		        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: true,
		            layut: 'fit',
		            

					dockedItems: [{
					    xtype: 'toolbar',
				        flex: 1,
				        dock: 'top',
				        ui: 'footer',
				        layout: {
				            pack: 'justify',
				            align: 'start'
				        },
						
					    items: [
					    
						{
		            		text: 'Disattiva filtri applicati', align: 'left',
		            		width: '90%',
		            		scale: 'medium', iconCls: 'icon-button_black_repeat_dx-24',
		            		handler: function() {
		            			var form = this.up('form').getForm();
								mp = this.up('form').up('#mainPanelTab');	            			
								mp.store.proxy.extraParams.m_filters = {};
								mp.store.reload();
								mp.refresh_all_accordion();
	            			}
	            		}					    
					    
					    ]
					 }],
		            
		            
		            
		            
/*					
					buttons: [{
	            		text: 'Applica',
	            		handler: function() {
	            			var form = this.up('form').getForm();
							mp = this.up('form').up('#mainPanelTab');	            			
							mp.store.proxy.extraParams.m_filters = form.getValues();
							mp.store.reload();
							mp.refresh_all_accordion();
	            			}
	            		}
	            	],
*/	            	
					
		            items: [
/*
						{
				            xtype: 'combo',
							name: 'f_centro_di_costo',
							fieldLabel: 'Centro di costo',
							labelAlign: 'top',
							minChars: 2,			
				            margin: "0 20 0 10",
				            
				            store: {
				            	pageSize: 1000,
				            	
								proxy: {
						            type: 'ajax',
						            
						            
						            url : <?php
						            		$cfg_mod = $main_module->get_cfg_mod();
						            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_centri_di_costo');
						            		?>,
						            		
						            reader: {
						                type: 'json',
						                root: 'root',
						                totalProperty: 'totalCount'
						            }
						        },       
								fields: ['cod', 'descr', 'out_loc'],		             	
				            },
				                        
							valueField: 'cod',                        
				            displayField: 'descr',
				            typeAhead: false,
				            hideTrigger: true,
				            anchor: '100%',
				            //width: 400,
				
				            listConfig: {
				                loadingText: 'Searching...',
				                emptyText: 'Nessun centro di costo trovato',
				                
				
				                // Custom rendering template for each item
				                getInnerTpl: function() {
				                    return '<div class="search-item">' +
				                        '<h3><span>{descr}</span></h3>' +
				                        '[{cod}]' + 
				                    '</div>';
				                }                
				                
				            },
				            
				            pageSize: 1000
				
				        }
*/				        
				      ]
				   }

			]	   
	}		   

	
				   
				, <?php echo write_tabella_per_accordion('f_centro_di_costo',  array('title' => 'Centro di costo',    'cod'=>'SMCTD0', 'des'=>'SMCDE0'), $m_params) ?>				   
				, <?php echo write_tabella_per_accordion('f_agente',  array('title' => 'Agente',    'cod'=>'COD_AGE', 'des'=>'DES_AGE'), $m_params) ?>	   
				, <?php echo write_tabella_per_accordion('f_nazione', array('title' => 'Nazione',   'cod'=>'COD_NAZ', 'des'=>'DES_NAZ'), $m_params) ?>				   
				, <?php echo write_tabella_per_accordion('f_nielsen', array('title' => 'Area Nielsen',   'cod'=>'COD_NIELSEN', 'des'=>'COD_NIELSEN'), $m_params) ?>
				/*
				, <?php echo write_tabella_per_accordion('f_divisione', array('title' => 'Divisione', 'cod'=>'COD_AGE', 'des'=>'DES_AGE'), $m_params) ?>
				*/				   
				   
		    ] //accordion
		}],
	
        tbar: new Ext.Toolbar({
            items:['<b>Analisi margine di contribuzione [Periodo: <?php echo $analisi_ar[0]; ?>, periodo di raffronto: <?php echo $analisi_ar[1]; ?>]</b>', '->'
            
	           	, {iconCls: 'tbar-x-tool x-tool-print', tooltip: 'Stampa', handler: function(event, toolEl, panel){
					//Ext.ux.grid.Printer.print(this.up('grid'));
					
						var submitForm = new Ext.form.FormPanel();
						submitForm.submit({
						        target: '_blank', 
	                        	standardSubmit: true,
	                        	method: 'POST',
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report',
						        params: {extraParams: Ext.JSON.encode(this.up('panel').getStore().proxy.extraParams)},
						        });					
					
					
	           	}}	            
            
	           		            
	           	
           	, {iconCls: 'tbar-x-tool x-tool-refresh', tooltip: 'Rigenera', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
         ]            
        }),  
		
	
		store: {
			xtype: 'store',
				
			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
			    open_tab_filters: {
					nome_analisi_1: <?php echo acs_je($analisi_ar[0]); ?>,
					nome_analisi_2: <?php echo acs_je($analisi_ar[1]); ?>,
					f_ditta: <?php echo acs_je($m_params->f_ditta); ?>,
					f_divisione: <?php echo acs_je($m_params->f_divisione); ?>,
				},	
				m_filters: {}
			}
			
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				, doRequest: personalizza_extraParams_to_jsonData						
			
			},
				
			fields: ['voce', 'tipo_voce', 'codice_voce'],
						
		}, //store

		multiSelect: false,
		columnLines: false, //righe separazione cell in verticale
		enableSort: false, // disable sorting		
		columns: [		
			{header: 'Codice', dataIndex: 'codice_voce', width: 60 /*, locked: true */},
			{header: 'Voce', dataIndex: 'voce', flex: 1, minWidth: 200 /*, locked: true */},
			{header: '', dataIndex: 'spaces', flex: 1}			
			<?php foreach($ar_ditte as $kd => $d){ ?>
			
			 , {header: <?php echo acs_je($d); ?>, width: 180,
			    columns: [ 			
				 , {header: 'Importo', dataIndex: <?php echo acs_je($kd); ?>, width: 100, align: 'right',
				                renderer: function(value, metaData, record, row, col, store, gridView){
				                	if (Ext.isEmpty(record.raw.ditta[<?php echo acs_je($kd); ?>]) == false)
					                	return floatRenderer0(record.raw.ditta[<?php echo acs_je($kd); ?>].IMPORTO_A);
				                }			  
				 }
				 , {header: '%', dataIndex: <?php echo acs_je($kd); ?>, width: 70, align: 'right',
				                renderer: function(value, metaData, record, row, col, store, gridView){
				                	if (Ext.isEmpty(record.raw.ditta[<?php echo acs_je($kd); ?>]) == false)
					                	return floatRenderer2(record.raw.ditta[<?php echo acs_je($kd); ?>].IMPORTO_B) + '%';
				                }			  
				 }				 
				 
			  ]
			 }
			<?php } ?>
					
		],
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					console.log('celldblclick');
					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
				    //in col_name ho in pratico il codice ditta
				    acs_show_panel_std('acs_panel_marginalita_azienda.php?fn=open_tab', Ext.id(), {
				    	open_tab_filters: {
				    		codice_ditta: col_name,
				    		nome_analisi_1: iView.store.proxy.extraParams.open_tab_filters.nome_analisi_1,
				    		nome_analisi_2: iView.store.proxy.extraParams.open_tab_filters.nome_analisi_2,
				    	},
				    	m_filters: iView.store.proxy.extraParams.m_filters
				    });

				}
			},
			
 			afterrender: function (comp) {
 			 //comp.normalGrid.addListener('celldblclick', comp.onCellDblClick,this);
 			 //comp.lockedGrid.addListener('celldblclick', comp.onCellDblClick,this); 			 
 			}			         
			
			
		}
		
		
		, viewConfig: {
			forceFit: true,
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }   
		         
		}												    
		
	
		 , refresh_all_accordion: function(){
		 
		    //grid clienti
		    this.dockedItems.items[1].items.items[1].store.proxy.extraParams.m_filters = this.store.proxy.extraParams.m_filters; 
		 	this.dockedItems.items[1].items.items[1].store.reload();		 
		 
		    //grid agenti
		    this.dockedItems.items[1].items.items[2].store.proxy.extraParams.m_filters = this.store.proxy.extraParams.m_filters; 
		 	this.dockedItems.items[1].items.items[2].store.reload();
		 	
		    //grid nazioni
		    this.dockedItems.items[1].items.items[3].store.proxy.extraParams.m_filters = this.store.proxy.extraParams.m_filters; 
		 	this.dockedItems.items[1].items.items[3].store.reload();		 	
		 	
		    //grid nielsen
		    this.dockedItems.items[1].items.items[4].store.proxy.extraParams.m_filters = this.store.proxy.extraParams.m_filters; 
		 	this.dockedItems.items[1].items.items[4].store.reload();		 	
		 }		         
		
	
		 
	} 
 
 
 
 ]
}
<?php exit; } ?>