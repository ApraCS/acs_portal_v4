<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>";
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'CUCF',
    'title_grid' => 'CUCF-Mastri',
    'fields' => array(
        'TAID'     => array('label'	=> 'tabella', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 300),
        'tp'       => array('label'	=> 'Tipo mastro', 'short' =>'TM', 'c_width' => 40, 'tooltip' => 'Tipo mastro', 'xtype' => 'combo_tipo'),
        'conto1'   => array('label'	=> 'Codifica conto 1', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'n_fc' => 1, 'type' => 'number'),
        'prog1'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto2'   => array('label'	=> 'Codifica conto 2', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 2),
        'prog2'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto3'   => array('label'	=> 'Codifica conto 3', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 3),
        'prog3'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto4'   => array('label'	=> 'Codifica conto 4', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 4),
        'prog4'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 4, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto5'   => array('label'	=> 'Codifica conto 5', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 5),
        'prog5'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 5, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto6'   => array('label'	=> 'Codifica conto 6', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 6),
        'prog6'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 6, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto7'   => array('label'	=> 'Codifica conto 7', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 7),
        'prog7'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 7, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto8'   => array('label'	=> 'Codifica conto 8', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 8),
        'prog8'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 8, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        'conto9'   => array('label'	=> 'Codifica conto 9', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 9),
        'prog9'    => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 9999, 'xtype' => 'fieldcontainer', 'n_fc' => 9, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160,'type' => 'number',  'close' => 'Y'),
        'conto10'  => array('label'	=> 'Codifica conto 10', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200, 'type' => 'number', 'n_fc' => 10),
        'prog10'   => array('label'	=> 'Ultimo codice', 'only_view' => 'F', 'maxValue' => 999, 'xtype' => 'fieldcontainer', 'n_fc' => 10, 'labelAlign' =>'right', 'labelWidth' => 90, 'width' => 160, 'type' => 'number', 'close' => 'Y'),
        
        'ggsc'     => array('label'	=> 'GG posticipo scad.', 'only_view' => 'F', 'maxValue' => 31, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 150, 'type' => 'number', 'n_fc' => 1),
        'aaaa'     => array('label'	=> "(Mesi esclusi: '00' lascia GG origine)", 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 200, 'width' => 220, 'type' => 'display', 'close' => 'Y'),
        
        'gglm'     => array('label'	=> 'GG tolleranza post.', 'only_view' => 'F', 'maxValue' => 30, 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 150, 'type' => 'number', 'n_fc' => 2),
        'bbb'     => array('label'	=> '(Per scadenze a giorno fisso)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 2, 'labelAlign' =>'left', 'labelWidth' => 180, 'width' => 180, 'type' => 'display', 'close' => 'Y'),
       
        'post'     => array('label'	=> 'Posticipo scadenze', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 195, 'type' => 'combo', 'n_fc' => 3),
        'ccc'    => array('label'	=> '(Mesi sucessivi ai mesi esclusi)', 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 3, 'labelAlign' =>'left', 'labelWidth'=> 180, 'width' => 220, 'type' => 'display', 'close' => 'Y'),
        
        'canc'     => array('label'	=> 'Cancellazione', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'vali'     => array('label'	=> 'Contr. validità fido', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'ccon'     => array('label'	=> 'Intest.condiz.standard', 'only_view' => 'F', 'maxLength' => 9),
        'vdoc'     => array('label'	=> 'Visualizz.su docum.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'bmcf'     => array('label'	=> 'Legame da punto vendita', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'comu'     => array('label'	=> 'Controlla comune', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'tivr'     => array('label'	=> 'Tipologia Contract', 'only_view' => 'F',  'xtype' => 'combo_tipo'),
        'tpco1'    => array('label'	=> '>1° Tipo doc. Contract', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'tpco2'    => array('label'	=> '>2° Tipo doc. Contract', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'tpco3'    => array('label'	=> '>3° Tipo doc. Contract', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        //'ggvi'     => array('label'	=> 'Giorno', 'only_view' => 'F'),
        //'mmvi'     => array('label'	=> 'Mese', 'only_view' => 'F'),
        //'aavi'     => array('label'	=> 'Anno', 'only_view' => 'F'),
        'date'     => array('label'	=> 'Data inizio Contract', 'only_view' => 'F', 'xtype' => 'datefield', 'submit' => 'dmY'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TRAD'     => array('label'	=> $trad, 'only_view' => 'C', 'c_width' => 40),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'TADTGE'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'   => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'   => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella mastri cliente/fornitore',
    'TAREST' => array(
        'tp' 	=> array(
            "start" => 0,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'conto1' 	=> array(
            "start" => 1,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto2' 	=> array(
            "start" => 8,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto3' 	=> array(
            "start" => 15,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto4' 	=> array(
            "start" => 22,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto5' 	=> array(
            "start" => 29,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto6' 	=> array(
            "start" => 36,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto7' 	=> array(
            "start" => 43,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto8' 	=> array(
            "start" => 50,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto9' 	=> array(
            "start" => 57,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'conto10' 	=> array(
            "start" => 64,
            "len"   => 3,
            "riempi_con" => "0"
        ),
        'prog1' 	=> array(
            "start" => 4,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog2' 	=> array(
            "start" => 11,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog3' 	=> array(
            "start" => 18,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog4' 	=> array(
            "start" => 25,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog5' 	=> array(
            "start" => 32,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog6' 	=> array(
            "start" => 39,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog7' 	=> array(
            "start" => 46,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog8' 	=> array(
            "start" => 53,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog9' 	=> array(
            "start" => 60,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'prog10' 	=> array(
            "start" => 67,
            "len"   => 4,
            "riempi_con" => "0"
        ),
        'ggsc' 	=> array(
            "start" => 71,
            "len"   => 2,
            "riempi_con" => "0"
        ),
        'gglm' 	=> array(
            "start" => 73,
            "len"   => 2,
            "riempi_con" => "0"
        ),
       'post' 	=> array(
            "start" => 75,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'canc' 	=> array(
            "start" => 76,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'vali' 	=> array(
            "start" => 77,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'ccon' 	=> array(
            "start" => 78,
            "len"   => 9,
            "riempi_con" => ""
        ),
        'vdoc' 	=> array(
            "start" => 87,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'bmcf' 	=> array(
            "start" => 88,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'comu' 	=> array(
            "start" => 89,
            "len"   => 1,
            "riempi_con" => ""
        ),
        'tivr' 	=> array(
            "start" => 90,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpco1' 	=> array(
            "start" => 92,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpco2' 	=> array(
            "start" => 94,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'tpco3' 	=> array(
            "start" => 96,
            "len"   => 2,
            "riempi_con" => ""
        ),
       /* 'ggvi' 	=> array(
            "start" => 98,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'mmvi' 	=> array(
            "start" => 100,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'aavi' 	=> array(
            "start" => 102,
            "len"   => 4,
            "riempi_con" => ""
        ),*/
        'date' 	=> array(
            "start" => 98,
            "len"   => 8,
            "riempi_con" => ""
        )
    
    )
    
);

//ROOT_ABS_PATH.
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
