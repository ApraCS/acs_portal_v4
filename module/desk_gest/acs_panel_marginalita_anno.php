<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();

require_once("acs_marginalita_include.php");

//recupero nome_ditta
function get_nome_ditta($codice_ditta){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT DITTA FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE CODICE_DITTA=? GROUP BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($codice_ditta));
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		return $r['DITTA'];
	}
	return '';
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data_centri_di_costo'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT SMCTD0, SMCDE0
			FROM {$cfg_mod_Gest['abi']['file_movimenti']}
			WHERE UPPER(
			REPLACE(REPLACE(SMCDE0, '.', ''), ' ', '')
					) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			GROUP BY SMCTD0, SMCDE0
 			";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();
	//$ret[] = array("cod" => '', "descr" => "- Tutti - ");

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array( "cod" 		=> trim($row['SMCTD0']),
				"descr" 	=> acs_u8e(trim($row['SMCDE0']))
		);
	}

	echo acs_je($ret);

	exit;
}



// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$m_params = acs_m_params_json_decode();
	
	if ($_REQUEST['node'] == 'root'){
		$ar = array();		
		//parto dal file dei movimenti (raggruppati)
		$sql_where = " AND CT.NOME_ANALISI IN (" . sql_t($m_params->nome_analisi_1) . ", " . sql_t($m_params->nome_analisi_2) . ")";
		$sql_where .= " AND CT.CODICE_DITTA = " . sql_t($m_params->codice_ditta);
		
		/*
		 * Prendo tutto (per mostrare tutta l'alberatura.
		 * Poi prendo soli gli importi dei movimenti uguali all'eventuale centro di costo selezionato
		 * ToDo: migliorare per prestazioni
		*/ 		
			$sql = "SELECT CT.VOCE_ANALISI, CT.DES_VOCE_ANALISI, CT.TIPO_VOCE, CT.ORDINAMENTO,
						SUBSTR(DATA_REGISTRAZIONE, 5, 2) AS MESE,
						SUM(MV.IMPORTO) AS IMPORTO
					FROM {$cfg_mod_Gest['abi']['file_conti']} CT
					LEFT OUTER JOIN {$cfg_mod_Gest['abi']['file_movimenti']} MV
						ON CT.CODICE_DITTA = MV.CODICE_DITTA
						AND MV.NOME_ANALISI = " . sql_t($m_params->ceal) . "
						AND CT.VOCE_ANALISI = MV.VOCE_ANALISI
					WHERE 1=1 {$sql_where}
					GROUP BY CT.VOCE_ANALISI, CT.DES_VOCE_ANALISI, CT.TIPO_VOCE, CT.ORDINAMENTO, SUBSTR(DATA_REGISTRAZIONE, 5, 2)
					ORDER BY CT.ORDINAMENTO
			";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			
			while ($r = db2_fetch_assoc($stmt)) {
				$id_liv = trim($r['VOCE_ANALISI']);
					
				if (!isset($ar["{$id_liv}"])){
					$ar_new = array();
					$ar_new['id'] = $id_liv;
					$ar_new['task'] = "[" . trim($r['VOCE_ANALISI']) . "] " . trim(acs_u8e($r['DES_VOCE_ANALISI']));
					$ar_new['tipo_voce'] =	$r['tipo_voce'] = trim($r['TIPO_VOCE']);
			
					if (trim($r['TIPO_VOCE']) != 'D')
						$ar_new['leaf'] = true;
					
					//per adesso non apriamo i dettagli
						$ar_new['leaf'] = true;
			
					$ar["{$id_liv}"] = $ar_new;
				}
								
				$ar_r = &$ar["{$id_liv}"];
			}			
				
		$sql_where = '';	
		$sql_where .= sql_where_by_request($m_params, 'Y', 'Y', 'MV.');
		$sql_where .= sql_where_by_user();
		
		$sql_add_select_tot = ',TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05';		
		
		$sql = "SELECT MV.VOCE_ANALISI, 				
						  SUBSTR(DATA_REGISTRAZIONE, 5, 2) AS MESE, 
					      SUM(MV.IMPORTO) AS IMPORTO {$sql_add_select_tot}
				FROM {$cfg_mod_Gest['abi']['file_movimenti']} MV
				WHERE 1=1 {$sql_where}
				GROUP BY MV.VOCE_ANALISI, SUBSTR(DATA_REGISTRAZIONE, 5, 2) {$sql_add_select_tot}
				";
		
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);		
		
		while ($r = db2_fetch_assoc($stmt)) {
			$id_liv = trim($r['VOCE_ANALISI']);
			
			//salvo per poi fare le percentuali
			if ($id_liv == '1001')
				$tot_1001 = $r['IMPORTO'];
			
			if (!isset($ar["{$id_liv}"])){
				$ar_new = array();
				$ar_new['id'] = $id_liv;				
				$ar_new['task'] = "[" . trim($r['VOCE_ANALISI']) . "] " . trim(acs_u8e($r['DES_VOCE_ANALISI']));
				$ar_new['tipo_voce'] =	$r['tipo_voce'] = trim($r['TIPO_VOCE']);

				if (trim($r['TIPO_VOCE']) != 'D')
					$ar_new['leaf'] = true;				

				$ar["{$id_liv}"] = $ar_new;
			}

			
			$ar_r = &$ar["{$id_liv}"];

			//importo mese
			$ar_r["imp_mese_{$r['MESE']}"] += $r['IMPORTO'];

			//Aggiorno i totali dei livelli 
			if (strlen(trim($r['TOTALE_01'])) > 0) $ar[trim($r['TOTALE_01'])]["imp_mese_{$r['MESE']}"] += $r['IMPORTO']; 	
			if (strlen(trim($r['TOTALE_02'])) > 0) $ar[trim($r['TOTALE_02'])]["imp_mese_{$r['MESE']}"] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_03'])) > 0) $ar[trim($r['TOTALE_03'])]["imp_mese_{$r['MESE']}"] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_04'])) > 0) $ar[trim($r['TOTALE_04'])]["imp_mese_{$r['MESE']}"] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_05'])) > 0) $ar[trim($r['TOTALE_05'])]["imp_mese_{$r['MESE']}"] += $r['IMPORTO'];

			//tot. riga (per anno)
			$ar_r['tot_anno'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_01'])) > 0) $ar[trim($r['TOTALE_01'])]["tot_anno"] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_02'])) > 0) $ar[trim($r['TOTALE_02'])]["tot_anno"] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_03'])) > 0) $ar[trim($r['TOTALE_03'])]["tot_anno"] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_04'])) > 0) $ar[trim($r['TOTALE_04'])]["tot_anno"] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_05'])) > 0) $ar[trim($r['TOTALE_05'])]["tot_anno"] += $r['IMPORTO'];
			
		}
		
		$ret = array();
		foreach($ar as $kar => $r){
			$ret[] = array_values_recursive(array_merge($ar[$kar], array('tot_1001' => $tot_1001)));
		}
		
		
		echo acs_je($ret);
	} else {
		
		if (strlen($_REQUEST['node']) == 4){
			//nel sottolivello vado a mostrare i conti dal file dei movimenti (raggruppati)
			$sql_where = " AND NOME_ANALISI = " . sql_t($m_params->ceal);
			$sql_where .= " AND CODICE_DITTA = " . sql_t($m_params->codice_ditta);
			$sql_where .= " AND VOCE_ANALISI = " . sql_t($_REQUEST['node']);
			
			if (strlen($m_params->centro_di_costo) > 0)
				$sql_where .= " AND SMCTD0 = " . sql_t($m_params->centro_di_costo);
			
			$sql = "SELECT NOME_ANALISI, CODICE_CONTO, DESCRIZIONE,
			 				SUBSTR(DATA_REGISTRAZIONE, 5, 2) AS MESE, 
							SUM(IMPORTO) AS IMPORTO
						 FROM {$cfg_mod_Gest['abi']['file_movimenti']} WHERE 1=1 {$sql_where}
					GROUP BY NOME_ANALISI, CODICE_CONTO, DESCRIZIONE, SUBSTR(DATA_REGISTRAZIONE, 5, 2) 
					ORDER BY CODICE_CONTO
					";
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			$ar = array();		
			
			while ($r = db2_fetch_assoc($stmt)) {
				$id_liv = trim($r['CODICE_CONTO']);
				
				if (!isset($ar["{$id_liv}"])){
					$ar_new = array();
					$ar_new['id'] = $id_liv;				
					$ar_new['task'] = "[" . trim($r['CODICE_CONTO']) . "] " . trim($r['DESCRIZIONE']);
					$ar_new['tipo_voce'] = 'dett';				
					$ar_new['leaf'] = false;  //posso sempre esplodere per il calendario
					$ar["{$id_liv}"] = $ar_new;
				}
	
				
				$ar_r = &$ar["{$id_liv}"];
	
				//importo mese
				$ar_r["imp_mese_{$r['MESE']}"] += $r['IMPORTO'];
				
				//tot. riga (per anno)
				$ar_r['tot_anno'] += $r['IMPORTO'];
					
			}
		} // if strlen($_REQUEST['node']) == 4	
			
		else {
			//$_REQUEST['node'] > 4 .... sono nel dettaglio per giorno
			//nel sottolivello vado a mostrare i conti dal file dei movimenti (raggruppati)
			$sql_where = " AND NOME_ANALISI = " . sql_t($m_params->ceal);
			$sql_where .= " AND CODICE_DITTA = " . sql_t($m_params->codice_ditta);
			$sql_where .= " AND CODICE_CONTO = " . sql_t($_REQUEST['node']);
			
			if (strlen($m_params->centro_di_costo) > 0)
				$sql_where .= " AND SMCTD0 = " . sql_t($m_params->centro_di_costo);
			
				
			$sql = "SELECT NOME_ANALISI, CODICE_CONTO, DESCRIZIONE,
				SUBSTR(DATA_REGISTRAZIONE, 5, 2) AS MESE,
				SUBSTR(DATA_REGISTRAZIONE, 7, 2) AS GIORNO,
				SUM(IMPORTO) AS IMPORTO
				FROM {$cfg_mod_Gest['abi']['file_movimenti']} WHERE 1=1 {$sql_where}
				GROUP BY NOME_ANALISI, CODICE_CONTO, DESCRIZIONE, SUBSTR(DATA_REGISTRAZIONE, 5, 2),  SUBSTR(DATA_REGISTRAZIONE, 7, 2)
				ORDER BY SUBSTR(DATA_REGISTRAZIONE, 7, 2)
			";
				
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			$ar = array();
			
			$prec_gg = 0;
				
			while ($r = db2_fetch_assoc($stmt)) {
				
				//creo i giorni "vuoti" fino al giorno attuale
				for ($i=$prec_gg +1;$i<$r['GIORNO'];$i++){
						$create_blank_id_liv = trim($r['CODICE_CONTO']) . sprintf("%02s", $i);
						$ar_new = array();
						$ar_new['id'] = $create_blank_id_liv;
						$ar_new['task'] = "Giorno " . sprintf("%02s", $i);
						$ar_new['gg'] = $i;
						$ar_new['tipo_voce'] = 'cal';
						$ar_new['leaf'] = true;
						$ar["{$create_blank_id_liv}"] = $ar_new;
				}
				
				$id_liv = trim($r['CODICE_CONTO']) . trim($r['GIORNO']);
			
				if (!isset($ar["{$id_liv}"])){
					$ar_new = array();
					$ar_new['id'] = $id_liv;
					$ar_new['task'] = "Giorno " . trim($r['GIORNO']);
					$ar_new['gg'] = $i;					
					$ar_new['tipo_voce'] = 'cal';
					$ar_new['leaf'] = true;
					$ar["{$id_liv}"] = $ar_new;
				}
			
			
				$ar_r = &$ar["{$id_liv}"];
			
				//importo mese
				$ar_r["imp_mese_{$r['MESE']}"] += $r['IMPORTO'];
			
				//tot. riga (per anno)
				$ar_r['tot_anno'] += $r['IMPORTO'];
					
				$prec_gg = (int)$r['GIORNO'];
			}
			
			//creo i giorni "vuoti" fino al 31
			for ($i=$prec_gg +1;$i<=31;$i++){
				$create_blank_id_liv = trim($r['CODICE_CONTO']) . sprintf("%02s", $i);
				$ar_new = array();
				$ar_new['id'] = $create_blank_id_liv;
				$ar_new['task'] = "Giorno " . sprintf("%02s", $i);
				$ar_new['gg'] = $i;				
				$ar_new['tipo_voce'] = 'cal';
				$ar_new['leaf'] = true;
				$ar["{$create_blank_id_liv}"] = $ar_new;
			}

		} //if $_REQUEST['node'] > 4
		
		
		$ret = array();
		foreach($ar as $kar => $r){
			$ret[] = array_values_recursive($ar[$kar]);
		}
		
		
		echo acs_je($ret);
	}
		
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();

?>

{
 success:true, items: [

 		{ 
  			xtype: 'treepanel',
	        title: <?php echo acs_je("Margini_Anno {$m_params->ceal} {$m_params->codice_ditta}"); ?>,
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        
	        tbar: new Ext.Toolbar({
	            items:['<b>Analisi margine di contribuzione per azienda <?php echo get_nome_ditta($m_params->codice_ditta); ?> [Periodo: <?php echo $m_params->ceal; ?>]</b>', '->',
	            
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),  
	        
	        
	        
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'tipo_voce', 'tot_anno', 'tot_1001', 'gg' <?php for ($i=1; $i<=12; $i++){echo ",'imp_mese_" . sprintf("%02s", $i) . "'";} ?>],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                        
                      extraParams: {
                        codice_ditta: <?php echo acs_je($m_params->codice_ditta); ?>,
                        nome_analisi_1: <?php echo acs_je($m_params->nome_analisi_1); ?>,
                        nome_analisi_2: <?php echo acs_je($m_params->nome_analisi_2); ?>,
                        ceal: <?php echo acs_je($m_params->ceal); ?>,
                        open_tab_filters:  <?php echo acs_je($m_params->open_tab_filters); ?>,
                      	m_filters: <?php echo acs_je($m_params->m_filters); ?>
                      }
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),

            multiSelect: false,
	        singleExpand: false,
	
			columns: [	
	    		{text: 'Voce', flex: 1, xtype: 'treecolumn', dataIndex: 'task'}

	    		//una colonna per mese
	    		<?php for ($i=1; $i<=12; $i++){ ?> 			
					 , {header: '<?php echo ucfirst(print_month($i)); ?>', dataIndex: 'imp_mese_<?php echo sprintf("%02s", $i); ?>', width: 75, align: 'right',  
							renderer: function (value, metaData, record, row, col, store, gridView){
									  if (record.get('tipo_voce') == 'cal'){						
							  				//in base a anno (da ceal) e mese/giorno verifico il giorno (per evidenziare i sabato/domenica)
											anno = <?php echo 2000 + (int)substr(trim(acs_je($m_params->ceal)), -3); ?>;
											mese = <?php echo $i; ?>;
											giorno = record.get('gg');
											dt = new Date(anno, mese-1, giorno);

											if (Ext.Date.format(dt, 'n') == mese)
												metaData.tdCls += ' day gw' + Ext.Date.format(dt, 'w');
											
											//alterno colori colonne mese											
											if (mese % 2 == 1)
											 metaData.tdCls += ' alt';			
											 
										
									  }
									  									  														
											return floatRenderer0(value);			    
									}					 		
					   }				 					 
				<?php } ?>
				
				 , {header: 'Totale', dataIndex: 'tot_anno', width: 90, align: 'right',
							renderer: function (value, metaData, record, row, col, store, gridView){						
							  	  			metaData.tdCls += ' tot';
											return floatRenderer0(value);			    
									}				
				  }	    		
				 , {header: '%', dataIndex: 'tot_anno', width: 65, align: 'right',
				                renderer: function(value, metaData, record, row, col, store, gridView){
				                	if (Ext.isEmpty(value) == false && Ext.isEmpty(record.get('tot_1001')) == false){
				                		return floatRenderer2(parseFloat(value) / record.get('tot_1001') * 100) + '%';
				                	}
				                }			  
				 }				 
	    			   	
			],
			enableSort: false, // disable sorting

	        listeners: {
		        	beforeload: function(store, options) {
		        		Ext.getBody().mask('Loading... ', 'loading').show();
		        		},		
			
		            load: function () {
		              Ext.getBody().unmask();
		            },
		            
		            
					celldblclick: {
						fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		
							rec = iView.getRecord(iRowEl);
						    col_name = iView.getGridColumns()[iColIdx].dataIndex;
						    
						    //in col_name ho in pratico il codice ditta
						    acs_show_panel_std('acs_panel_marginalita_anno.php?fn=open_tab', Ext.id(), {
		                        codice_ditta: <?php echo acs_je($m_params->codice_ditta); ?>,
		                        nome_analisi_1: <?php echo acs_je($m_params->nome_analisi_1); ?>,
		                        nome_analisi_2: <?php echo acs_je($m_params->nome_analisi_2); ?>,
		                        col_name: col_name
						    });
		
						}
					}
		            
		            	        
		        }
			

		, viewConfig: {
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }   
		    }												    
		        
			
			    		
 	} 
 
 
 
 
 ]
}
<?php exit; } ?>