<?php

require_once("../../config.inc.php");
require_once("acs_bollette_doganali_include.php");

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

function sql_add_join_by_request($m_params){
	$main_module = new DeskGest;
	$cfg_mod = $main_module->get_cfg_mod();
	
	$ret = "";
	if (
			(isset($m_params->f_stato_attivita) && count($m_params->f_stato_attivita) > 0) ||
			trim($m_params->f_filtra_fatture) == 'CON_ATTIVITA' ||
			trim($m_params->f_filtra_fatture) == 'DA_ABBINARE_O_CON_ATTIVITA'
		)
		$ret .= "
			LEFT OUTER JOIN {$cfg_mod['bollette_doganali']['file_assegna_ord']} ATT_OPEN
			   ON ATT_OPEN.ASDOCU = TF.TFDOCU				
			";
	return $ret;
}

function sql_where_by_request($m_params){
	$sql_where = "";
	
	//Solo fatture con bolle doganali
	$sql_where .= " AND TF.TFBDOG in('Y', 'B', 'P') ";
	
	$sql_where .= sql_where_by_combo_value('TF.TFCCON', $m_params->f_cliente_cod);

	switch (trim($m_params->f_filtra_fatture)){
		case 'DA_ABBINARE':
			$sql_where .= " AND TF.TFBDOG = 'Y'";
			break;
		case 'DA_ABBINARE_O_CON_ATTIVITA':
			$sql_where .= " AND (TF.TFBDOG IN('Y','P') OR ATT_OPEN.ASFLRI <> 'Y') ";
			break;			
		case 'CON_ATTIVITA': //aperte
			$sql_where .= " AND ATT_OPEN.ASFLRI != 'Y'";
			break;			
	}	
	
	if (strlen($m_params->f_data_da) > 0)
		$sql_where .= " AND TF.TFDTRG >= {$m_params->f_data_da}";
	if (strlen($m_params->f_data_a) > 0)
		$sql_where .= " AND TF.TFDTRG <= {$m_params->f_data_a}";

	/* data generazione bolletta */
	if (strlen($m_params->f_bolletta_data_da) > 0)
		$sql_where .= " AND TF_ABB.TFDTGE >= {$m_params->f_bolletta_data_da}";
	if (strlen($m_params->f_bolletta_data_a) > 0)
		$sql_where .= " AND TF_ABB.TFDTGE <= {$m_params->f_bolletta_data_a}";
	
	//con stato/attivita' aperta
	if (isset($m_params->f_stato_attivita) && count($m_params->f_stato_attivita) > 0){
		if (count($m_params->f_stato_attivita) == 1)
			$sql_where .= " AND ATT_OPEN.ASCAAS = " . sql_t($m_params->f_stato_attivita[0]);
		if (count($m_params->f_stato_attivita) > 1)
			$sql_where .= " AND ATT_OPEN.ASCAAS IN (" . sql_t_IN($m_params->f_stato_attivita) . ")";
	}
	
	
	return $sql_where;		
}



//elenco agenzie (da file righe)
function get_ar_stato_attivita(){
	global $conn, $cfg_mod_Admin;
	$main_module = new DeskGest();
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TA_ATTAV.TAKEY1, TA_ATTAV.TADESC
			FROM {$cfg_mod['bollette_doganali']['file_assegna_ord']} AS0
			INNER JOIN {$cfg_mod['bollette_doganali']['file_testate']} TF			 
			   ON TFDOCU = ASDOCU
			INNER JOIN {$cfg_mod_Admin['file_tabelle']} TA_ATTAV
			   ON TA_ATTAV.TADT = TF.TFDT AND AS0.ASCAAS = TA_ATTAV.TAKEY1			   			
			WHERE TA_ATTAV.TARIF1 = 'BD'
			GROUP BY TA_ATTAV.TAKEY1, TA_ATTAV.TADESC			
			ORDER BY TA_ATTAV.TADESC";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['TAKEY1']), "text" => $r['TADESC']);
	}
	return $ar;
}



//elenco agenzie (da file righe)
function get_ar_agenzie(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT RFAGEN AS COD, RFDAGN AS DES FROM {$cfg_mod_Gest['provvigioni']['file_righe']} GROUP BY RFAGEN, RFDAGN ORDER BY RFDAGN";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES']);
	}
	return $ar;
}


//elenco agenti (da file righe)
function get_ar_agenti(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT RFCAGE AS COD, RFDAGE AS DES FROM {$cfg_mod_Gest['provvigioni']['file_righe']} GROUP BY RFCAGE, RFDAGE ORDER BY RFDAGE";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES']);
	}
	return $ar;
}



function sum_columns_value(&$ar_r, $r){
	//da imponibile provvigione escludo quelle con importo provvigione = 0
	if ((float)$r['RFIMPO'] != 0) 
		$ar_r['imponibile_provvigione'] += $r['RFIMPP'];
	
	$ar_r['importo_provvigione'] 	+= $r['RFIMPO'];
	
	//se non sono in righe RAEE
	if ($r['RFFG01'] != 'R'){
		$perc_to_txt = n_auto($r['RFPPRA']) . "%";
		$ar_r['lista_percentuali'][$perc_to_txt] ++;
	}

	
	if ($ar_r['liv'] == 'liv_4'){
		$ar_r['perc_provvigione'] = n($r['RFPPRA']) . "%";
	} else {
		$ar_r['perc_provvigione'] = '';
		//nei livelli superiori mostro un elenco delle percentuali presenti
		foreach ($ar_r['lista_percentuali'] as $kp => $p){
			$ar_r['perc_provvigione'] .= "{$kp} ";
		}
	}
	
}

function sum_columns_value_TD(&$ar_liv_tot, &$ar_liv0, &$ar_liv1, &$ar_liv2, &$ar_liv3, &$ar_liv4, $r, $ar_new){
	
	//array globale per non sommare due volte gli importi della stessa fattura
	global $ar_conteggio_td;	
	$ar_conteggio_td[$r['TFDOCU']]++;
	
/*	
	if ($ar_conteggio_td[$r['TFDOCU']] > 1)
		return; //esco perche' gia' conteggiata
		
		
*/		
	
		$ar_liv_tot['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv_tot['tot_netto_merce'] 	+= $r['TFINFI'];
		$ar_liv_tot['tot_imponibile']  	+= $r['TFTIMP'];		
		$ar_liv_tot['tot_anticipo'] 	+= $r['TFTANT'];	
		$ar_liv_tot['volume']           += $r['TFVOLU'];
		$ar_liv_tot['pesi']             += $r['TFPNET'];
		$ar_liv_tot['colli']            += $r['TFTOCO'];
		$ar_liv_tot['conteggio']        += 1;
	
		$ar_liv0['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv0['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv0['tot_imponibile']  += $r['TFTIMP'];		
		$ar_liv0['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv0['volume']          += $r['TFVOLU'];
		$ar_liv0['pesi']            += $r['TFPNET'];
		$ar_liv0['colli']           += $r['TFTOCO'];
		$ar_liv0['conteggio'] 		+= 1;
	
		$ar_liv1['tot_documento'] 	+= $r['TFTOTD'];		
		$ar_liv1['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv1['tot_imponibile']  += $r['TFTIMP'];		
		$ar_liv1['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv1['volume']          += $r['TFVOLU'];
		$ar_liv1['pesi']            += $r['TFPNET'];
		$ar_liv1['colli']           += $r['TFTOCO'];
		$ar_liv1['conteggio'] 		+= 1;
		
		$ar_liv2['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv2['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv2['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv2['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv2['volume']          += $r['TFVOLU'];
		$ar_liv2['pesi']            += $r['TFPNET'];
		$ar_liv2['colli']           += $r['TFTOCO'];
		$ar_liv2['conteggio'] 		+= 1;

		$ar_liv3['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv3['tot_netto_merce'] += $r['TFINFI'];		
		$ar_liv3['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv3['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv3['volume']          += $r['TFVOLU'];
		$ar_liv3['pesi']            += $r['TFPNET'];
		$ar_liv3['colli']           += $r['TFTOCO'];
		$ar_liv3['conteggio'] 		+= 1;

		$ar_liv4['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv4['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv4['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv4['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv4['volume']          += $r['TFVOLU'];
		$ar_liv4['pesi']            += $r['TFPNET'];
		$ar_liv4['colli']           += $r['TFTOCO'];
		$ar_liv4['conteggio'] 		+= 1;
		

		
			if ((int)$ar_new['with_todo'] > 0){
				$ar_liv2['with_todo'] = max((int)$ar_liv2['with_todo'], (int)$ar_new['with_todo']);
				$ar_liv3['with_todo'] = max((int)$ar_liv3['with_todo'], (int)$ar_new['with_todo']);
			}

		
}




####################################################
if ($_REQUEST['fn'] == 'get_json_data_assegna_bolletta_doganale'){
####################################################
	$m_params = acs_m_params_json_decode();
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();


	$sql = "SELECT *
			FROM {$cfg_mod['bollette_doganali']['file_testate']}
			WHERE TFCCON = ? AND TFTIDO = 'BD' ORDER BY TFDTRG DESC";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->k_cliente));

	$ret = array();
	
	$sql_abbinato = "SELECT SUM(TFINFI) AS TOT_ABBINATO 
						FROM {$cfg_mod['bollette_doganali']['file_testate']}
						WHERE TFDOAB = ? or TFDOA2 = ? ";
	$stmt_abbinato = db2_prepare($conn, $sql_abbinato);
	echo db2_stmt_errormsg();	

	while ($row = db2_fetch_assoc($stmt)) {
		
		//per ogni bolletta devo calcolare il residuo
		$result_abbinato = db2_execute($stmt_abbinato, array($row['TFDOCU'], $row['TFDOCU']));		
		$row_abbinato = db2_fetch_assoc($stmt_abbinato);		
		$row['abbinato'] = (float)$row_abbinato['TOT_ABBINATO'];
		$row['residuo'] = (float)$row['TFTOTD'] - (float)$row['abbinato'];
		
		if ($m_params->mode == 'EDIT' || $row['residuo'] > 0)				
			$ret[] = $row;
	}

	echo acs_je($ret);

	exit;
	
}


####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TFCCON, TFDCON
			FROM {$cfg_mod['bollette_doganali']['file_testate']}
			WHERE UPPER(
			REPLACE(REPLACE(TFDCON, '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
					GROUP BY TFCCON, TFDCON
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array( "cod" 		=> trim($row['TFCCON']),
						"descr" 	=> acs_u8e(trim($row['TFDCON']))
		);
	}

	echo acs_je($ret);

	exit;
}


// ******************************************************************************************
// Eliminazione bolletta doganale (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete_bolletta_doganale'){
	$m_params = acs_m_params_json_decode();
	
	if (strlen($m_params->tfdocu) == 0){
		$ret['success'] = false;
		echo acs_je($ret);
		exit;
	}
	
	//VERIFICO CHE NON SIA GIA ABBINATA IN FATTURE
	$sql = "SELECT COUNT(*) AS R_COUNT FROM {$cfg_mod_Gest['bollette_doganali']['file_testate']}
			WHERE TFDOAB = ? OR TFDOA2 = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->tfdocu, $m_params->tfdocu));	
	$row = db2_fetch_assoc($stmt);
	
	if ($row['R_COUNT'] > 0){
		$ret['success'] = false;
		$ret['message'] = "La bolletta &egrave; abbinata a una o pi&ugrave; fatture. Impossibile rimuoverla"; 
		echo acs_je($ret);
		exit;
	}
	
	
	$ret = array();
	
	$sql = "DELETE FROM {$cfg_mod_Gest['bollette_doganali']['file_testate']} WHERE TFDOCU =?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->tfdocu));

/*
 * IMPEDISCO ELIMINAZIONE SE E' ABBINATA	
	//resetto anche le fatture a cui era stata abbinata la bolla doganale appena eliminata
	//recupero una dira di questo cliente (per recperare i dati)
	$sql = "UPDATE {$cfg_mod_Gest['bollette_doganali']['file_testate']}
			SET TFDOAB='', TFBDOG='Y'
	 		WHERE TFDOAB =?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->tfdocu));
*/	
	
	$ret['success'] = true;
	echo acs_je($ret);
	
exit;
}



// ******************************************************************************************
// Rimuovi abbinamento bolla doganale (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_rimuovi_abbinamento_bolletta_doganale'){
	$m_params = acs_m_params_json_decode();

	//udpdate
	$sql = "UPDATE
			{$cfg_mod_Gest['bollette_doganali']['file_testate']}
			SET TFDOAB = '', TFDOA2 = '', TFBDOG = 'Y'
			WHERE 1=1 AND TFDOCU IN (" . sql_t_IN($m_params->list_selected_id) . ")
		   ";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($bolletta_out, $flag_TFBDOG));


	$ret = array();
	$ret['success'] = true;


	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// Abbina bolletta doganale (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_abbina_bolletta_doganale'){
	$m_params = acs_m_params_json_decode();
	
	$tfdocu_bol = $m_params->tfdocu;
	$tfdocu_bol_exp = explode("_", $tfdocu_bol); 
	
	//$bolletta_out = implode("_", array($tfdocu_bol_exp[3], $tfdocu_bol_exp[4]));
	$bolletta_out = $tfdocu_bol;
	
	
	//flag bollettato o parziale
	if (strlen(trim($m_params->set_flag_TFBDOG)) == 0)
		$flag_TFBDOG = 'B';
	else
		$flag_TFBDOG = trim($m_params->set_flag_TFBDOG);
	
	//campo in cui salvare la bolla
	if (strlen(trim($m_params->save_in_field)) == 0)
		$save_in_field = 'TFDOAB';
	else
		$save_in_field = trim($m_params->save_in_field);
	
	
	//udpdate
	$sql = "UPDATE
			{$cfg_mod_Gest['bollette_doganali']['file_testate']}
			SET {$save_in_field} = ?, TFBDOG = ?
			WHERE 1=1 AND TFDOCU IN (" . sql_t_IN($m_params->list_selected_id) . ")
		";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($bolletta_out, $flag_TFBDOG));
	
	
	$ret = array();
	$ret['success'] = true;
	$ret['bolletta_doganale'] = $bolletta_out;
	
	//retrieve (prendo il primo)
	$sql = "SELECT * FROM {$cfg_mod_Gest['bollette_doganali']['file_testate']}
			WHERE 1=1 AND TFDOCU IN (" . sql_t_IN($m_params->list_selected_id) . ")
			";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array());
	$r = db2_fetch_assoc($stmt);
	
	//bolletta doganale principale
	$bd_exp = explode("_", $r['TFDOAB']);
	if (count($bd_exp) == 7)
		$ar_new['bolletta_doganale'] = implode("_", array($bd_exp[4]));
	else
		$ar_new['bolletta_doganale'] = trim($r['TFDOAB']);
	
	if (trim($r['TFBDOG']) == 'P')
		$ar_bolletta_doganale_ar= array($ar_new['bolletta_doganale'] . " (P)");
	else
		$ar_bolletta_doganale_ar= array($ar_new['bolletta_doganale']);
	
	//bolletta doganale secondaria (se abbinamento parziale)
	if (strlen(trim($r['TFDOA2'])) > 0){
		$bd_exp = explode("_", $r['TFDOA2']);
		if (count($bd_exp) == 7)
			$ar_new['bolletta_doganale2'] = implode("_", array($bd_exp[4]));
		else
			$ar_new['bolletta_doganale2'] = trim($r['TFDOA2']);
			
		$ar_bolletta_doganale_ar[] = $ar_new['bolletta_doganale2'];
	}
	
	$ret['bolletta_doganale_out'] = implode("<br/>", $ar_bolletta_doganale_ar);
	
	echo acs_je($ret);	
 exit;
}



// ******************************************************************************************
// Abbina bolletta doganale (form)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_abbina_bolletta_doganale'){
	$m_params = acs_m_params_json_decode();
	
	if ($m_params->mode != 'EDIT'){	
		//conto le fatture selezionate e il totale
		$fatture_selezionate = $m_params->list_selected_id;
		
		$sql = "SELECT COUNT(*) AS T_ROW, SUM(TFINFI) AS TOT_FATTURE FROM {$cfg_mod_Gest['bollette_doganali']['file_testate']}
				WHERE TFDOCU IN(" . sql_t_IN($fatture_selezionate) . ")";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array());
		$row_fatture_abbinate = db2_fetch_assoc($stmt);
		
		$form_title = "N.fatture selezionate: {$row_fatture_abbinate['T_ROW']}, totale importo: " . n($row_fatture_abbinate['TOT_FATTURE']);
	} else {
		$form_title = 'Gestione bolle doganali per cliente';
	}		
	
?>
{"success":true, "items": [
						{
							xtype: 'gridpanel',
							flex: 1,
							title: <?php echo j($form_title); ?>,
							
					  	    store: Ext.create('Ext.data.Store', {
										autoLoad: true,				        
					  					proxy: {
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_assegna_bolletta_doganale',
												type: 'ajax',
												reader: {
										            type: 'json',
										            root: 'root'
										        },
										        
												actionMethods: {read: 'POST'},										        
										        
										        extraParams: {
										        	k_cliente: <?php echo j($m_params->k_cliente)?>,
										        	mode:  <?php echo j($m_params->mode)?>
										        }
										         /* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
												, doRequest: personalizza_extraParams_to_jsonData
											},
						        			fields: ['TFDOCU', 'TFDTRG', 'TFAADO', 'TFNRDO', 'TFTOTD', 'abbinato', 'residuo', 'TFCARI']
						    			}),
						    			
							        columns: [
							             {
							                header   : 'Data',
							                dataIndex: 'TFDTRG', 
							                widht: 100, 
							                renderer: date_from_AS
							             }, {
							                header   : 'Anno',
							                dataIndex: 'TFAADO', 
							                width: 80
							             }, {
							                header   : 'Numero',
							                dataIndex: 'TFNRDO', 
							                flex: 1
							             }, {
							                header   : 'Importo',
							                dataIndex: 'TFTOTD', 
							                renderer: floatRenderer2, align: 'right',
							                width: 100
							             }, {
							                header   : 'Abbinato',
							                dataIndex: 'abbinato', 
							                renderer: floatRenderer2, align: 'right',							                
							                width: 100
							             }, {
							                header   : 'Residuo',
							                dataIndex: 'residuo',
							                renderer: floatRenderer2, align: 'right', 
							                width: 100
							             }										             
							         ]	    					
							
							, listeners: {
							
							
									itemcontextmenu : function(grid, rec, node, index, event) {
										event.stopEvent();			                         
										var voci_menu = [];
										loc_win = grid.up('window');

										save_in_field = 'TFDOAB';
<?php if ($m_params->stato_attuale == 'P'){ ?> save_in_field = 'TFDOA2';<?php } ?>										
										
<?php if ($m_params->mode != 'EDIT'){ ?>
									      voci_menu.push({
								      		text: 'Abbina',
								    		iconCls : 'icon-lock-16',      		
								    		handler: function() {

								    		importo_bolla = grid.getSelectionModel().getSelection()[0].get('residuo');								    		
											if (importo_bolla != <?php echo $row_fatture_abbinate['TOT_FATTURE']; ?>)
												msg_text_add = '<br/>Attenzione: Importi non coincidenti';
											else
												msg_text_add = '';								    		
								    		
											    Ext.Msg.confirm('Abbinamento bolla doganale', 'Confermi abbinamento?' + msg_text_add, function(btn, text){
											      if (btn == 'yes'){
											      
													Ext.Ajax.request({
													        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_abbina_bolletta_doganale',
													        jsonData: {
													        	tfdocu: rec.get('TFDOCU'),
													        	list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>,
													        	set_flag_TFBDOG: 'B',
													        	save_in_field: save_in_field
													        },
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
													            var jsonData = Ext.decode(result.responseText);
													            loc_win.fireEvent('afterUpdateRecord', loc_win, jsonData.bolletta_doganale_out);			            
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });											      
													    
													    
													    
											      } else {
											        //nothing
											      }
											    });								    		
								    		
								    							    			
								    		}
										  });										
										

<?php if ($m_params->stato_attuale != 'P'){ ?>									      
									      voci_menu.push({
								      		text: 'Abbina parziale',
								    		iconCls : 'icon-lock_grey-16',      		
								    		handler: function() {
								    		
								    		importo_bolla = grid.getSelectionModel().getSelection()[0].get('residuo');								    		
											if (importo_bolla < <?php echo $row_fatture_abbinate['TOT_FATTURE']; ?>)
												msg_text_add = '<br/>Attenzione: Importi non coincidenti';
											else
												msg_text_add = '';								    		
								    		
											    Ext.Msg.confirm('Abbinamento bolla doganale', 'Confermi abbinamento parziale?', function(btn, text){
											      if (btn == 'yes'){
											      
													Ext.Ajax.request({
													        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_abbina_bolletta_doganale',
													        jsonData: {
													        	tfdocu: rec.get('TFDOCU'),
													        	list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>,
													        	set_flag_TFBDOG: 'P',
													        	save_in_field: 'TFDOAB'
													        },
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
													            var jsonData = Ext.decode(result.responseText);
													            loc_win.fireEvent('afterUpdateRecord', loc_win, jsonData.bolletta_doganale_out);			            
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });											      
													    
													    
													    
											      } else {
											        //nothing
											      }
											    });								    		
								    		
								    							    			
								    		}
										  });
<?php } //statu_attuale != P ?>										  
										  
										  
<?php } //!EDIT?>										  
										
										
<?php if ($m_params->mode == 'EDIT'){ ?>										
									      voci_menu.push({
								      		text: 'Rimuovi bolla doganale',
								    		iconCls : 'icon-sub_red_delete-16',      		
								    		handler: function() {
								    		
											    Ext.Msg.confirm('Richiesta conferma', 'Confermi eliminazione bolla doganale?', function(btn, text){
											      if (btn == 'yes'){
											      	//Ext.getBody().mask('Loading... ', 'loading').show();
													Ext.Ajax.request({
													        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete_bolletta_doganale',
													        jsonData: {
													        	tfdocu: rec.get('TFDOCU')
													        },
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
													            var jsonData = Ext.decode(result.responseText);
													            
																	if (jsonData.success===false){
																		Ext.Msg.alert('Message', jsonData.message);
																		return false;																		            
																	} 
													            
													            
													            grid.store.load();			            
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });	
											      } else {
											        //nothing
											      }
											    });								    		
								    		
								    							    			
								    		}
										  });
										  
										   voci_menu.push({
								      		text: 'Ristampa',
								    		iconCls : 'icon-print-16',      		
								    		handler: function() {
								    		
													Ext.Ajax.request({
													        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_print_barcode_bolletta_doganale&f_prog=' + rec.get('TFCARI'),
													      	method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
													             acs_show_msg_info('Richiesta inviata');			            
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });	
											      			    		
								    		
								    							    			
								    		}
										  });
<?php } //EDIT ?>										  
										
								      var menu = new Ext.menu.Menu({
									            items: voci_menu
										}).showAt(event.xy);										
										
									},							
									
									
<?php if (1==2 && $m_params->mode != 'EDIT'){ ?>									
									  celldblclick: {								
										  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
										  	tfdocu = iView.getSelectionModel().getSelection()[0].get('TFDOCU');
										  	importo_bolla = iView.getSelectionModel().getSelection()[0].get('TFTOTD');
										  	loc_win = iView.up('window');
										  	
										  	
										  	//devo verificare se l'importo della bolla corrisponde al totale delle fatture selezionate
										  	if (importo_bolla != <?php echo $row_fatture_abbinate['TOT_FATTURE']; ?>){
										  	
											    Ext.Msg.confirm('Richiesta conferma', 'Attenzione, importi non coincidenti. Confermi?', function(btn, text){
											      if (btn == 'yes'){
											      	//Ext.getBody().mask('Loading... ', 'loading').show();
													Ext.Ajax.request({
													        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_abbina_bolletta_doganale',
													        jsonData: {
													        	tfdocu: tfdocu,
													        	list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>
													        },
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
													            var jsonData = Ext.decode(result.responseText);
													            loc_win.fireEvent('afterUpdateRecord', loc_win, jsonData.bolletta_doganale_out);			            
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });	
											      } else {
											        //nothing
											      }
											    });
											 } else {
											 
											    //importo coincidente .... non richiedo conferma
													Ext.Ajax.request({
													        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_abbina_bolletta_doganale',
													        jsonData: {
													        	tfdocu: tfdocu,
													        	list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>
													        },
													        method     : 'POST',
													        waitMsg    : 'Data loading',
													        success : function(result, request){
													            var jsonData = Ext.decode(result.responseText);
													            loc_win.fireEvent('afterUpdateRecord', loc_win, jsonData.bolletta_doganale_out);			            
													        },
													        failure    : function(result, request){
													            Ext.Msg.alert('Message', 'No data to be loaded');
													        }
													    });	
											    
											 
											 }
										  	
																					  
										  }
									  }
<?php } //!EDIT ?>									  
						 			 			
								}
							  
							         
						}
]}	
<?php	
  exit;
} 








// ******************************************************************************************
// Nuova bolletta doganale (form)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_nuova_bolletta_dognale'){
	$m_params = acs_m_params_json_decode();	
	
	//assegno id progressivo a bolle doganali gia' presenti (senza progressivo)
	$sql = "SELECT TFDOCU FROM {$cfg_mod_Gest['bollette_doganali']['file_testate']} TF 
            WHERE TFDT='{$id_ditta_default}' AND TFTIDO = 'BD' AND TFCARI='' ";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array());
	while ($row = db2_fetch_assoc($stmt)) {
	    $sqlU = "UPDATE {$cfg_mod_Gest['bollette_doganali']['file_testate']} 
                 SET TFCARI=? WHERE TFDOCU=?";
	    $stmtU = db2_prepare($conn, $sqlU);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmtU, array(
	           sprintf("%08s", $s->next_num('BDPROG')),
	           $row['TFDOCU'])
	        );
	}    	
	
	?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Salva e stampa barcode',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_nuova_bolletta_doganale',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues()), stampa : 'Y'},
										success: function(form,action) {
										     acs_show_msg_info('Richiesta inviata');
											 loc_win.fireEvent('afterUpdateRecord', loc_win);										
										}																                        
				                });
				                
				            
			                }	
			                	                	                
				               
				         }
				        }, {xtype: 'tbfill'},
					
					
					{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Salva',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_nuova_bolletta_doganale',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {										
										    loc_win.fireEvent('afterUpdateRecord', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }],   		            
		            
		            items: [
						   {
							     name: 'k_cliente'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->k_cliente); ?>
							}, {
							     name: 'f_prog'   
							   , flex: 1                		
							   , xtype: 'textfield'
							   , fieldLabel: 'Progressivo richiesta'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , maxLength: 20
							   , anchor: '-15'
							   , readOnly: true
							   , value: <?php echo j(sprintf("%08s", $s->next_num('BDPROG'))) ?>
							}, {
							     name: 'f_nr'   
							   , flex: 1                		
							   , xtype: 'textfield'
							   , fieldLabel: 'Numero'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , maxLength: 20
							   , anchor: '-15'
							}, {
							     name: 'f_data'   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_importo'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Importo'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							}, {
							     name: 'f_note'   
							   , flex: 1                		
							   , xtype: 'textfield'
							   , fieldLabel: 'Note/Dichiarante'
							   , labelAlign: 'left'
							   , allowBlank: true
							   , maxLength: 50
							   , anchor: '-15'
							}
		            
		            
						]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}





// ******************************************************************************************
// Print barcode (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_print_barcode_bolletta_doganale'){
    $m_params = acs_m_params_json_decode();
    print_r($m_params);
    @include '../../personal/print_barcode_bolletta_doganale.php';
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    
    exit;
}






// ******************************************************************************************
// Nuova bolletta doganale (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_nuova_bolletta_doganale'){

	$k_cliente = $_REQUEST['k_cliente'];

	//recupero una dira di questo cliente (per recperare i dati)
	$sql = "SELECT *
			FROM {$cfg_mod_Gest['bollette_doganali']['file_testate']} TF
			WHERE 1=1 AND TFCCON = ?
			FETCH FIRST 1 ROWS ONLY			
	";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($k_cliente));
	$rowCliente = db2_fetch_assoc($stmt);


	//preparo ar per insert
	global $auth;
	$ar_ins = array();
	$ar_ins['TFUSGE'] 	= $auth->get_user();
	$ar_ins['TFDTGE'] 	= oggi_AS_date();
	$ar_ins['TFORGE'] 	= oggi_AS_time();
	
	$ar_ins['TFCARI'] 	= $_REQUEST['f_prog'];

	//costurisco TFDOCU
	$ar_ins['TFDT'] 	= sprintf("%-2s", $id_ditta_default);
	$ar_ins['TFTIDO'] 	= 'BD';
	$ar_ins['TFINUM'] 	= 'BD1';
	$ar_ins['TFAADO'] 	= substr($_REQUEST['f_data'], 0, 4);
	$ar_ins['TFNRDO'] 	= sprintf("%-20s", $_REQUEST['f_nr']);
	$ar_ins['TFPROG'] 	= sprintf("%-10s", '');	
	$ar_ins['TFDTOR'] 	= sprintf("%-2s", $id_ditta_default);
	$ar_ins['TFDOCU'] 	= implode('_', array(
			$ar_ins['TFDT'],
			$ar_ins['TFTIDO'],
			$ar_ins['TFINUM'],
			$ar_ins['TFAADO'],
			$ar_ins['TFNRDO'],
			$ar_ins['TFPROG'],
			$ar_ins['TFDTOR']
	));

	
	$ar_ins['TFDTRG'] = $_REQUEST['f_data'];
	$ar_ins['TFTOTD'] = sql_f($_REQUEST['f_importo']);

	
	//in base ad cliente selezionato
	$ar_ins['TFCCON'] 	= $rowCliente['TFCCON'];
	$ar_ins['TFDCON'] 	= $rowCliente['TFDCON'];
	$ar_ins['TFINDI'] 	= $rowCliente['TFINDI'];
	$ar_ins['TFCAP'] 	= $rowCliente['TFCAP'];
	$ar_ins['TFLOCA'] 	= $rowCliente['TFLOCA'];
	$ar_ins['TFPROV'] 	= $rowCliente['TFPROV'];
	$ar_ins['TFNAZI'] 	= $rowCliente['TFNAZI'];
	$ar_ins['TFDNAZ'] 	= $rowCliente['TFDNAZ'];
	

	//note/dichiarante
	$ar_ins['TFDOAB'] 	= utf8_decode($_REQUEST['f_note']);

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['bollette_doganali']['file_testate']}(
	" . create_name_field_by_ar($ar_ins) . "
			)
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")
			";
	
	acs_portal_db_debug('bollette_doganali/exe_nuova_bolletta_doganale', $sql, $ar_ins, array());
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	echo db2_stmt_errormsg($stmt);
	
	if($_REQUEST['stampa'] == 'Y')
	    @include '../../personal/print_barcode_bolletta_doganale.php';

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}







// ******************************************************************************************
// BLOCCO/SBLOCCO flag ricalcolo provvigione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_blocca_sblocca_ricalcolo'){
	$m_params = acs_m_params_json_decode();
	
	//recupero id fattura
	$id_fattura = $m_params->selected_id;
	
	if ($m_params->local_flag == 'Y')
		$to_flag = '';
	else
		$to_flag = 'Y';
	
	$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_testate']} SET TFFG01 = ? WHERE TFDOCU = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($to_flag, trim($id_fattura)));

	//ritorno attuale valore
	$sql = "SELECT TFFG01 FROM {$cfg_mod_Gest['provvigioni']['file_testate']} WHERE TFDOCU = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_fattura));
	$r = db2_fetch_assoc($stmt);	
	
	echo acs_je(array('success' => $result, 'remote_flag' => $r['TFFG01']));
	exit;
}




// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	ini_set('max_execution_time', 300);	
	$m_params = acs_m_params_json_decode();
	
		$sql_where 		= sql_where_by_request($m_params);
		$sql_add_join 	= sql_add_join_by_request($m_params);
		
		
		if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
			//ITA|070012762	
			$node_exp = explode("|", $_REQUEST['node']);
			$sql_where .= " AND TF.TFNAZI=" . sql_t($node_exp[0]);		
			$sql_where .= " AND TF.TFCCON=" . sql_t($node_exp[1]);
		}		
		
		
			$sql = "SELECT TF.*, 
					 TF_ABB.TFDTGE AS TF_ABB_TFDTGE, TF_ABB.TFDTRG AS TF_ABB_TFDTRG, TF_ABB.TFDOAB AS TF_ABB_NOTE,
					 TF_ABB_2.TFDTGE AS TF_ABB_2_TFDTGE, TF_ABB_2.TFDTRG AS TF_ABB_2_TFDTRG, TF_ABB_2.TFDOAB AS TF_ABB_2_NOTE
					FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
					 LEFT OUTER JOIN {$cfg_mod_Gest['provvigioni']['file_testate']} TF_ABB
					   ON TF.TFDOAB = TF_ABB.TFDOCU
					 LEFT OUTER JOIN {$cfg_mod_Gest['provvigioni']['file_testate']} TF_ABB_2
					   ON TF.TFDOA2 = TF_ABB_2.TFDOCU
					" . $sql_add_join . "					   
					WHERE 1=1 {$sql_where}
					ORDER BY TF.TFDNAZ, TF.TFDCON, TF.TFDTRG, TF.TFTPDO, TF.TFDOCU"
					;

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();
		
		while ($r = db2_fetch_assoc($stmt)) {
						
			$tmp_ar_id = array();
			$ar_r = &$ar;			
			
			$cli_liv_tot = 'TOTALE';						
			$cod_liv0 = trim($r['TFNAZI']); 	//nazione				
			$cod_liv1 = trim($r['TFCCON']);		//codice cliente
			$cod_liv2 = trim($r['TFDOCU']);		//documento
				
			
			//LIVELLO TOTALE
			$liv = $cod_liv_tot;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = 'liv_totale';
				$ar_new['liv'] = 'liv_totale dett_selezionato';
				$ar_new['liv_cod'] = 'TOTALE';
				$ar_new['liv_cod_out'] = '';
				$ar_new['task'] = 'Totale';
				$ar_new['leaf'] = false;
				$ar_new['expanded'] = true;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv_tot = &$ar_r;
			sum_columns_value($ar_r, $r);
				

			//LIVELLO 0			
			$liv = $cod_liv0;
			$ar_r = &$ar_r['children'];			
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_1';
				$ar_new['liv_cod'] = $liv;
				$ar_new['liv_cod_out'] = $liv;				
				$ar_new['task'] = acs_u8e($r['TFDNAZ']);
				$ar_new['leaf'] = false;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv2 = &$ar_r;
			sum_columns_value($ar_r, $r);
			

			//LIVELLO 2
			$liv = $cod_liv1;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_2';
				$ar_new['liv_cod'] = $liv;
				$ar_new['liv_cod_out'] = $liv;				
				$ar_new['task'] = acs_u8e($r['TFDCON']);
				$ar_new['leaf'] = false;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv3 = &$ar_r;
			sum_columns_value($ar_r, $r);			
				

			//LIVELLO 4
			$liv = $cod_liv2;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_3';
				$ar_new['liv_cod'] = acs_u8e($r['TFDOCU']);
				$ar_new['liv_cod_out'] = acs_u8e($r['TFTPDO']);
				$ar_new['liv_cod_qtip'] = acs_u8e($r['TFDTPD']);
				$ar_new['liv_data'] = $r['TFDTRG'];
				$ar_new['TFBDOG'] 	= $r['TFBDOG'];

				//bolletta doganale principale
				$bd_exp = explode("_", $r['TFDOAB']);				
				if (count($bd_exp) == 7)
					$ar_new['bolletta_doganale'] = implode("_", array($bd_exp[4]));
				else
					$ar_new['bolletta_doganale'] = trim($r['TFDOAB']);			
				$ar_new['data_bolletta_doganale'] = $r['TF_ABB_TFDTRG'];

				if (trim($r['TFBDOG']) == 'P')
					$ar_bolletta_doganale_ar = array($ar_new['bolletta_doganale'] . " (P)");
				else
					$ar_bolletta_doganale_ar = array($ar_new['bolletta_doganale']);
				
				$ar_bolletta_doganale_data_ar = array(print_date($r['TF_ABB_TFDTRG']));
				
				if (strlen(trim($r['TF_ABB_NOTE'])) > 0){
					$ar_bolletta_doganale_ar[] = "[" . acs_u8e(trim($r['TF_ABB_NOTE'])) . "]";
					$ar_bolletta_doganale_data_ar[] = "&nbsp;";
				}
				
				//bolletta doganale secondaria (se abbinamento parziale)
				if (strlen(trim($r['TFDOA2'])) > 0){
					$bd_exp = explode("_", $r['TFDOA2']);
					if (count($bd_exp) == 7)
						$ar_new['bolletta_doganale2'] = implode("_", array($bd_exp[4]));
					else
						$ar_new['bolletta_doganale2'] = trim($r['TFDOA2']);
					
					$ar_bolletta_doganale_ar[] = $ar_new['bolletta_doganale2'];
					$ar_bolletta_doganale_data_ar[] = print_date($r['TF_ABB_2_TFDTRG']);
					
					if (strlen(trim($r['TF_ABB_2_NOTE'])) > 0){
						$ar_bolletta_doganale_ar[] = "[" . acs_u8e(trim($r['TF_ABB_2_NOTE'])) . "]";
						$ar_bolletta_doganale_data_ar[] = "&nbsp;";
					}					
				}				
				
				$ar_new['bolletta_doganale_out'] = implode("<br/>", $ar_bolletta_doganale_ar);
				$ar_new['data_bolletta_doganale_out'] = implode("<br/>", $ar_bolletta_doganale_data_ar);

				$as = new SpedAssegnazioneOrdini;
				$ret_stato_tooltip = $as->stato_tooltip_entry_per_ordine($r['TFDOCU']);
				$ar_new['with_todo'] = $ret_stato_tooltip['stato'];
				$ar_new['with_todo_qtip'] = $ret_stato_tooltip['tooltip'];
				$ar_new['stato'] = $ret_stato_tooltip['att_open'];
				
				$ar_new['vettore_out'] = acs_u8e(trim($r['TFRGSV']));	
			
				
				if ($r['TFFG02'] == 'M') //multiagente
				{
					$ar_new['liv_cod_out'] .= " " . "<img src=" . img_path("icone/48x48/user_group.png") . " width=18>";
				}
				
				//$ar_new['task'] = acs_u8e($r['TFDOCU']);
				$ar_new['task'] = "[{$r['TFINUM']}] " . implode("_", array($r['TFAADO'], $r['TFNRDO'], $r['TFDT']));

				$ar_new['fl_blocco_sblocco_ricalcolo'] = acs_u8e($r['TFFG01']);
				$ar_new['leaf'] = true;
				$ar_r["{$liv}"] = $ar_new;
				
				sum_columns_value_TD($ar_liv_tot, $ar_liv0, $ar_liv1, $ar_liv2, $ar_liv3, $ar_r["{$liv}"], $r, $ar_new);
			}
			$ar_r = &$ar_r["{$liv}"];
			sum_columns_value($ar_r, $r);			
			
	}
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
		$ret = $ret[0]['children'][0]['children'][0]['children'];
	}	
	
	 echo acs_je(array('success' => true, 'children' => $ret));	
	
	exit;
} //get_json_data









// ******************************************************************************************
// SCELTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_parameters'){
?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "BOLL_DOGANALI");  ?>{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
								//deve inserire o un filtro per data fattura o per data generazione bolletta
								if (
									Ext.isEmpty(form.findField('f_data_da').getValue()) &&
									Ext.isEmpty(form.findField('f_data_a').getValue()) &&
									Ext.isEmpty(form.findField('f_bolletta_data_da').getValue()) &&
									Ext.isEmpty(form.findField('f_bolletta_data_a').getValue())
								){
									acs_show_msg_error('Impostare un filtro su data fattura o data bolletta');
									return false;
								}
										            	
								acs_show_panel_std('acs_panel_bollette_doganali.php?fn=open_tab', 'panel-bollette_doganali', form.getValues());
								this.up('window').close();
							
			            }
			         }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-folder_search-32',
	                     text: 'ToDo',
				            handler: function() {				            
			            	var form = this.up('form').getForm();
							
								//deve inserire o un filtro per data fattura o per data generazione bolletta
								if (
									Ext.isEmpty(form.findField('f_data_da').getValue()) &&
									Ext.isEmpty(form.findField('f_data_a').getValue()) &&
									Ext.isEmpty(form.findField('f_bolletta_data_da').getValue()) &&
									Ext.isEmpty(form.findField('f_bolletta_data_a').getValue())
								){
									acs_show_msg_error('Impostare un filtro su data fattura o data bolletta');
									return false;
								}
										            	
								acs_show_panel_std('acs_panel_bollette_doganali_todolist.php?fn=open_tab', null, form.getValues());
								this.up('window').close();
											            
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Report',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_bollette_doganali_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Bolle',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_bollette_doganali_report_bolle.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        }],   		            
		            
		            items: [
		            
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "10 20 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "10 20 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
					 
					 
		            
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_bolletta_data_da'
							   , margin: "10 20 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data immissione bolla da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_bolletta_data_a'
							   , margin: "10 20 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data immissione bolla a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
					 					 
					 
					 
					 
		 , {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,			
            margin: "0 20 0 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		$cfg_mod = $main_module->get_cfg_mod();
		            		if ($cfg_mod['bollette_doganali']['anagrafiche_cliente_da_modulo'] == 'Y')
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
		            		else
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=search_cli_anag');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            	var form = this.up('form').getForm();
	            	form.findField('f_destinazione_cod').setValue(''); 	            	
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }					 
				
				
				
						, {
							name: 'f_stato_attivita',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Stato/Attivita&grave;',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_stato_attivita(), ""); ?>	
								    ] 
							}						 
						}
										
					 
			
					 , {
							name: 'f_filtra_fatture',
							xtype: 'radiogroup',
							fieldLabel: 'Fatture',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "20 10 10 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Tutte'
		                          , inputValue: ''
		                          , width: 50
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Da abbinare/con attivit&agrave;'
		                          , inputValue: 'DA_ABBINARE_O_CON_ATTIVITA'
		                          , checked: true
		                          , width: 200
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Con attivit&agrave;'
		                          , inputValue: 'CON_ATTIVITA'
		                          , checked: false
		                        }]
						}
		            
		            
		            
		            

					 
					 


					 
		            
		            
		            
		            
		            
		           ]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();	
?>
{
 success:true, 
 items: [
  <?php write_main_tree((array)$m_params); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// SCADUTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_scaduto'){
	$m_params = acs_m_params_json_decode();
	
	//sposto indietro le date di un anno
	$m_params->dataStart = date('Ymd', strtotime($m_params->dataStart . " -1 years"));
	$m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " -1 years"));
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "scaduto",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// OLTRE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_oltre'){
	$m_params = acs_m_params_json_decode();
	
	//sposto indietro le date di un anno
	$m_params->dataStart = date('Ymd', strtotime($m_params->dataStart . " +1 years"));
	$m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " +1 years"));
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "oltre",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// DETTAGLIO PER RIFERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_riferimento'){
	$m_params = acs_m_params_json_decode();
	
	//recuper data iniziale e data finale (dalla cella su cui ho fatto dbclick)
	$col_name_ar = explode('_', $m_params->col_name);
	$dataStart = $col_name_ar[1];
	$dataEnd 	 = $col_name_ar[2];

	//da record_id recupero raggruppamento e categoria
	$record_id_ar = explode("|", $m_params->record_id);
	$raggruppamento = $record_id_ar[0];
	$categoria		= $record_id_ar[1];
	
	?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'hbox',
	    align: 'stretch',
	    pack : 'start',
	},
	items: [ 
		  <?php write_dettaglio_riferimento(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria
		  )); ?>,
		  <?php write_dettaglio_righe(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria
		  )); ?>		  
	]
  }	   
 ]
}
<?php exit; } ?>

