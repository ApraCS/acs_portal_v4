<?php

/*
 * FILTRO: TFFG04 = 'Y'
 * CAUSALI CON TARIF1 = 'CM'
 */


require_once("../../config.inc.php");
require_once("acs_panel_cmr_include.php");

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));





function sum_columns_value(&$ar_r, $r){
	//da imponibile provvigione escludo quelle con importo provvigione = 0
	if ((float)$r['RFIMPO'] != 0) 
		$ar_r['imponibile_provvigione'] += $r['RFIMPP'];
	
	$ar_r['importo_provvigione'] 	+= $r['RFIMPO'];
	
	//se non sono in righe RAEE
	if ($r['RFFG01'] != 'R'){
		$perc_to_txt = n_auto($r['RFPPRA']) . "%";
		$ar_r['lista_percentuali'][$perc_to_txt] ++;
	}

	
	if ($ar_r['liv'] == 'liv_4'){
		$ar_r['perc_provvigione'] = n($r['RFPPRA']) . "%";
	} else {
		$ar_r['perc_provvigione'] = '';
		//nei livelli superiori mostro un elenco delle percentuali presenti
		foreach ($ar_r['lista_percentuali'] as $kp => $p){
			$ar_r['perc_provvigione'] .= "{$kp} ";
		}
	}
	
}

function sum_columns_value_TD(&$ar_liv_tot, &$ar_liv0, &$ar_liv1, &$ar_liv2, &$ar_liv3, &$ar_liv4, $r, $ar_new){
	
	//array globale per non sommare due volte gli importi della stessa fattura
	global $ar_conteggio_td;	
	$ar_conteggio_td[$r['TFDOCU']]++;
	
/*	
	if ($ar_conteggio_td[$r['TFDOCU']] > 1)
		return; //esco perche' gia' conteggiata
		
		
*/		
	
		$ar_liv_tot['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv_tot['tot_netto_merce'] 	+= $r['TFINFI'];
		$ar_liv_tot['tot_imponibile']  	+= $r['TFTIMP'];		
		$ar_liv_tot['tot_anticipo'] 	+= $r['TFTANT'];	
		$ar_liv_tot['volume']           += $r['TFVOLU'];
		$ar_liv_tot['pesi']             += $r['TFPNET'];
		$ar_liv_tot['colli']            += $r['TFTOCO'];
		$ar_liv_tot['conteggio']        += 1;
	
		$ar_liv0['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv0['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv0['tot_imponibile']  += $r['TFTIMP'];		
		$ar_liv0['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv0['volume']          += $r['TFVOLU'];
		$ar_liv0['pesi']            += $r['TFPNET'];
		$ar_liv0['colli']           += $r['TFTOCO'];
		$ar_liv0['conteggio'] 		+= 1;
	
		$ar_liv1['tot_documento'] 	+= $r['TFTOTD'];		
		$ar_liv1['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv1['tot_imponibile']  += $r['TFTIMP'];		
		$ar_liv1['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv1['volume']          += $r['TFVOLU'];
		$ar_liv1['pesi']            += $r['TFPNET'];
		$ar_liv1['colli']           += $r['TFTOCO'];
		$ar_liv1['conteggio'] 		+= 1;
		
		$ar_liv2['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv2['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv2['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv2['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv2['volume']          += $r['TFVOLU'];
		$ar_liv2['pesi']            += $r['TFPNET'];
		$ar_liv2['colli']           += $r['TFTOCO'];
		$ar_liv2['conteggio'] 		+= 1;

		$ar_liv3['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv3['tot_netto_merce'] += $r['TFINFI'];		
		$ar_liv3['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv3['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv3['volume']          += $r['TFVOLU'];
		$ar_liv3['pesi']            += $r['TFPNET'];
		$ar_liv3['colli']           += $r['TFTOCO'];
		$ar_liv3['conteggio'] 		+= 1;

		$ar_liv4['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv4['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv4['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv4['tot_anticipo'] 	+= $r['TFTANT'];
		$ar_liv4['volume']          += $r['TFVOLU'];
		$ar_liv4['pesi']            += $r['TFPNET'];
		$ar_liv4['colli']           += $r['TFTOCO'];
		$ar_liv4['conteggio'] 		+= 1;
		

		
			if ((int)$ar_new['with_todo'] > 0){
				$ar_liv2['with_todo'] = max((int)$ar_liv2['with_todo'], (int)$ar_new['with_todo']);
				$ar_liv3['with_todo'] = max((int)$ar_liv3['with_todo'], (int)$ar_new['with_todo']);
			}

		
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TFCCON, TFDCON
			FROM {$cfg_mod['bollette_doganali']['file_testate']}
			WHERE UPPER(
			REPLACE(REPLACE(TFDCON, '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
					GROUP BY TFCCON, TFDCON
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array( "cod" 		=> trim($row['TFCCON']),
						"descr" 	=> acs_u8e(trim($row['TFDCON']))
		);
	}

	echo acs_je($ret);

	exit;
}




// ******************************************************************************************
// Annulla attesa
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_annulla_attesa_CMR'){
	$m_params = acs_m_params_json_decode();

	//udpdate
	$sql = "UPDATE
			{$cfg_mod_Gest['bollette_doganali']['file_testate']}
			SET TFFG04 = 'M'
			WHERE 1=1 AND TFDOCU IN (" . sql_t_IN($m_params->list_selected_id) . ")
		   ";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);


	$ret = array();
	$ret['success'] = true;

	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	ini_set('max_execution_time', 300);	
	$m_params = acs_m_params_json_decode();
	
	$ar = crea_ar_tree_CRM($_REQUEST['node'], $m_params);
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
		$ret = $ret[0]['children'][0]['children'][0]['children'];
	}	
	
	 echo acs_je(array('success' => true, 'children' => $ret));	
	
	exit;
} //get_json_data









// ******************************************************************************************
// SCELTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_parameters'){
?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "CMR_ATTESI");  ?>{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
								//deve inserire o un filtro per data fattura o per data generazione bolletta
								if (
									Ext.isEmpty(form.findField('f_data_da').getValue()) &&
									Ext.isEmpty(form.findField('f_data_a').getValue()) 
								){
									acs_show_msg_error('Impostare un filtro su data fattura');
									return false;
								}
										            	
								acs_show_panel_std('acs_panel_cmr.php?fn=open_tab', 'panel-cmr', form.getValues());
								this.up('window').close();
							
			            }
			         }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-folder_search-32',
	                     text: 'ToDo',
				            handler: function() {				            
			            	var form = this.up('form').getForm();
							
								//deve inserire o un filtro per data fattura o per data generazione bolletta
								if (
									Ext.isEmpty(form.findField('f_data_da').getValue()) &&
									Ext.isEmpty(form.findField('f_data_a').getValue())
								){
									acs_show_msg_error('Impostare un filtro su data fattura');
									return false;
								}
										            	
								acs_show_panel_std('acs_panel_cmr_todolist.php?fn=open_tab', null, form.getValues());
								this.up('window').close();
											            
				         }
				        }
				        
				        
    				        , {
    	                     xtype: 'button',
    		            	 scale: 'large',
    		            	 iconCls: 'icon-print-32',
    	                     text: 'Report',
    				            handler: function() {
    				            
    			                form = this.up('form').getForm();
    			                
    			                if (form.isValid()){	                	                
    				                form.submit({
    				                		url: 'acs_panel_cmr_report.php',
    						                standardSubmit: true,
    				                        method: 'POST',
    										target : '_blank',
    										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
    				                });
    			                }								            
    				            
    				         }
    				        }
				        
				        
				     /* schedulare perche' troppo lento?   
				     , {
			            text: 'Verifica su documentale',
			            iconCls: 'icon-button_black_repeat_dx-32',
			            scale: 'large',
			            handler: function() {
			            
				            Ext.MessageBox.show({
	                            msg: 'Loading',
	                            progressText: 'Loading...',
	                            width:200,
	                            wait:true,
	                            waitConfig: {interval:200},
	                            icon:'ext-mb-download', //custom class in msg-box.html               
	                        });
	                        
	                        
							Ext.Ajax.request({
							        url: '<?php echo ROOT_PATH; ?>/personal/adiuto_verifica_esistenza_cmr.php?fn=verifica_esistenza_per_carico_cliente_ALL',
							        timeout: 2400000,
							        jsonData: {},	
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){	            	  													        
							            var jsonData = Ext.decode(result.responseText);
							            if (jsonData.success == true){
											Ext.MessageBox.hide();							            
											acs_show_msg_info('Operazione conclusa con successo', 'Numero clienti/carichi aggiornati: ' + jsonData.num_upd + ' su ' + jsonData.num_verifiche);
							            } else {
							            	acs_show_msg_error('Errore in fase di salvataggio dati');
							            }							            	
							        },
							        failure    : function(result, request){							        
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });		                        
			
			            }
			         }
			         */
				        
				        
				        ],   		            
		            
		            items: [
		            
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "10 20 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data fattura da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "10 20 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data fattura a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
			 
					 
        		 , {
                    xtype: 'combo',
        			name: 'f_cliente_cod',
        			fieldLabel: 'Cliente',
        			minChars: 2,			
                    margin: "0 20 0 10",
                    
                    store: {
                    	pageSize: 1000,
                    	
        				proxy: {
        		            type: 'ajax',
        		            
        		            
        		            url : <?php
        		            		$cfg_mod = $main_module->get_cfg_mod();
        		            		if ($cfg_mod['bollette_doganali']['anagrafiche_cliente_da_modulo'] == 'Y')
        		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
        		            		else
        		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=search_cli_anag');
        		            		?>,
        		            		
        		            reader: {
        		                type: 'json',
        		                root: 'root',
        		                totalProperty: 'totalCount'
        		            }
        		        },       
        				fields: ['cod', 'descr', 'out_loc'],		             	
                    },
                                
        			valueField: 'cod',                        
                    displayField: 'descr',
                    typeAhead: false,
                    hideTrigger: true,
                    anchor: '100%',
                    
        	        listeners: {
        	            change: function(field,newVal) {	            	
        	            		
        	            }
        	        },            
        
                    listConfig: {
                        loadingText: 'Searching...',
                        emptyText: 'Nessun cliente trovato',
                        
        
                        // Custom rendering template for each item
                        getInnerTpl: function() {
                            return '<div class="search-item">' +
                                '<h3><span>{descr}</span></h3>' +
                                '[{cod}] {out_loc}' + 
                            '</div>';
                        }                
                        
                    },
                    
                    pageSize: 1000
        
                },
                
                
                
                
                {
					name: 'f_nazione',
					xtype: 'combo',
					multiSelect: true,
					fieldLabel: 'Nazione',
					displayField: 'text',
					valueField: 'id',
					emptyText: '- seleziona -',
					forceSelection: true,
				   	allowBlank: true,		
				    anchor: '-15',
				    margin: "20 10 10 10",						   													
					store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
						     <?php echo acs_ar_to_select_json(get_ar_nazioni(), ""); ?>	
						    ] 
					}						 
				}
				    
		       ]
		      }			  
			  
			  
			]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();	
?>
{
 success:true, 
 items: [
  <?php write_main_tree((array)$m_params); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// SCADUTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_scaduto'){
	$m_params = acs_m_params_json_decode();
	
	//sposto indietro le date di un anno
	$m_params->dataStart = date('Ymd', strtotime($m_params->dataStart . " -1 years"));
	$m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " -1 years"));
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "scaduto",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// OLTRE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_oltre'){
	$m_params = acs_m_params_json_decode();
	
	//sposto indietro le date di un anno
	$m_params->dataStart = date('Ymd', strtotime($m_params->dataStart . " +1 years"));
	$m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " +1 years"));
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "oltre",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// DETTAGLIO PER RIFERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_riferimento'){
	$m_params = acs_m_params_json_decode();
	
	//recuper data iniziale e data finale (dalla cella su cui ho fatto dbclick)
	$col_name_ar = explode('_', $m_params->col_name);
	$dataStart = $col_name_ar[1];
	$dataEnd 	 = $col_name_ar[2];

	//da record_id recupero raggruppamento e categoria
	$record_id_ar = explode("|", $m_params->record_id);
	$raggruppamento = $record_id_ar[0];
	$categoria		= $record_id_ar[1];
	
	?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'hbox',
	    align: 'stretch',
	    pack : 'start',
	},
	items: [ 
		  <?php write_dettaglio_riferimento(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria
		  )); ?>,
		  <?php write_dettaglio_righe(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria
		  )); ?>		  
	]
  }	   
 ]
}
<?php exit; } ?>

