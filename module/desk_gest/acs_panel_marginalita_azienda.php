<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();

require_once("acs_marginalita_include.php");

//recupero nome_ditta
function get_nome_ditta($codice_ditta){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT DITTA FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE CODICE_DITTA=? GROUP BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($codice_ditta));
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		return $r['DITTA'];
	}
	return '';
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data_centri_di_costo'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT SMCTD0, SMCDE0
			FROM {$cfg_mod_Gest['abi']['file_movimenti']}
			WHERE UPPER(
			REPLACE(REPLACE(SMCDE0, '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
							GROUP BY SMCTD0, SMCDE0
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array( "cod" 		=> trim($row['SMCTD0']),
						"descr" 	=> acs_u8e(trim($row['SMCDE0']))
		);
	}

	echo acs_je($ret);

	exit;
}



// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_report'){
	include_once('acs_panel_marginalita_azienda_report.php');
	exit;
}

if ($_REQUEST['fn'] == 'get_report_elenco'){
	include_once('acs_panel_marginalita_azienda_report_elenco.php');
	exit;
}


// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$m_params = acs_m_params_json_decode();
	
	if ($_REQUEST['node'] == 'root'){
		$sql_params = array();		
		$sql_where = " AND NOME_ANALISI = " . sql_t($m_params->open_tab_filters->nome_analisi_1);
		$sql_where .= " AND NOME_ANALISI_CONFR = " . sql_t($m_params->open_tab_filters->nome_analisi_2);		
		$sql_where .= " AND CODICE_DITTA = " . sql_t($m_params->open_tab_filters->codice_ditta);
		
		
		//creo struttura
		$sql = "SELECT * 
				FROM {$cfg_mod_Gest['abi']['file_conti']} 
				WHERE 1=1 {$sql_where} ORDER BY ORDINAMENTO";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();
		
		
		while ($r = db2_fetch_assoc($stmt)) {
			$r['task'] 		= "[" . trim($r['VOCE_ANALISI']) . "] " . trim(acs_u8e($r['DES_VOCE_ANALISI']));
			$r['id'] 		= trim($r['VOCE_ANALISI']);
			$r['tipo_voce'] = trim($r['TIPO_VOCE']);

			//if (trim($r['TIPO_VOCE']) != 'D')
				//qui non voglio gestire i sottolivelli, per ora
				$r['leaf'] = true;
			
			$r['IMPORTO_A'] = $r['IMPORTO_B'] = $r['IMPORTO_C'] = $r['IMPORTO_D'] = $r['IMPORTO_E'] = $r['IMPORTO_F'] = $r['IMPORTO_G'] = $r['IMPORTO_H'] = 0;  
			
			$ar[$r['VOCE_ANALISI']] = $r;
		}

		
		//seleziono i dati dai movimenti
		$sql_where = " AND M.NOME_ANALISI IN (" . sql_t($m_params->open_tab_filters->nome_analisi_1) . ", " . sql_t($m_params->open_tab_filters->nome_analisi_2) . ")";
		$sql_where .= " AND M.CODICE_DITTA = " . sql_t($m_params->open_tab_filters->codice_ditta);

		/*
		list($sql_where_add, $sql_params_add) = sql_where_by_form_filters($m_params->m_filters);
		$sql_where .= $sql_where_add;
		$sql_params = array_merge($sql_params, $sql_params_add);
		*/
		
		$sql_where .= sql_where_by_request($m_params, 'Y', 'Y', 'M.');		
		$sql_where .= sql_where_by_user();
		
		$sql = "SELECT M.VOCE_ANALISI, M.NOME_ANALISI, SUM(M.IMPORTO) AS IMPORTO, R1001.IMPORTO_A
					,TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05		
				FROM {$cfg_mod_Gest['abi']['file_movimenti']} M
				LEFT OUTER JOIN (SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']}) R1001
				ON M.CODICE_DITTA = R1001.CODICE_DITTA
				AND R1001.VOCE_ANALISI = '1001'
				AND R1001.NOME_ANALISI = " . sql_t($m_params->open_tab_filters->nome_analisi_1) . "
			   		   AND R1001.NOME_ANALISI_CONFR = " . sql_t($m_params->open_tab_filters->nome_analisi_2) . "
		
					   		   WHERE 1=1 {$sql_where}
				GROUP BY M.VOCE_ANALISI, M.NOME_ANALISI, R1001.IMPORTO_A
					,TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05				
			";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $sql_params);


		
		while ($r = db2_fetch_assoc($stmt)) {
			$id_liv = trim($r['VOCE_ANALISI']);
			
			$ar_r = &$ar["{$id_liv}"];			
			
			if ($r['NOME_ANALISI'] == $m_params->open_tab_filters->nome_analisi_1){
				$ar_r['IMPORTO_A'] = trim($r['IMPORTO']);
				
				if (strlen(trim($r['TOTALE_01'])) > 0) $ar[trim($r['TOTALE_01'])]['IMPORTO_A'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_02'])) > 0) $ar[trim($r['TOTALE_02'])]['IMPORTO_A'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_03'])) > 0) $ar[trim($r['TOTALE_03'])]['IMPORTO_A'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_04'])) > 0) $ar[trim($r['TOTALE_04'])]['IMPORTO_A'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_05'])) > 0) $ar[trim($r['TOTALE_05'])]['IMPORTO_A'] += $r['IMPORTO'];				
				
				if ((float)trim($r['IMPORTO_A']) != 0){
					$ar_r['IMPORTO_B'] = (int)trim($r['IMPORTO']) / (int)trim($r['IMPORTO_A']) * 100;
				}
			}
			if ($r['NOME_ANALISI'] == $m_params->open_tab_filters->nome_analisi_2){
				$ar_r['IMPORTO_C'] = trim($r['IMPORTO']);

				if (strlen(trim($r['TOTALE_01'])) > 0) $ar[trim($r['TOTALE_01'])]['IMPORTO_C'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_02'])) > 0) $ar[trim($r['TOTALE_02'])]['IMPORTO_C'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_03'])) > 0) $ar[trim($r['TOTALE_03'])]['IMPORTO_C'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_04'])) > 0) $ar[trim($r['TOTALE_04'])]['IMPORTO_C'] += $r['IMPORTO'];
				if (strlen(trim($r['TOTALE_05'])) > 0) $ar[trim($r['TOTALE_05'])]['IMPORTO_C'] += $r['IMPORTO'];				
				
				if ((float)trim($r['IMPORTO_A']) != 0)
					$ar_r['IMPORTO_D'] = (int)trim($r['IMPORTO']) / (int)trim($r['IMPORTO_A']) * 100;
			}
			
		
		}		
		
		
		
		$ret = array();
		foreach($ar as $kar => $r){
			$ret[] = array_values_recursive($ar[$kar]);
		}				
		echo acs_je($ret);
		
	} else {
		$sql_params = array();		
		//nel sottolivello vado a mostrare i conti dal file dei movimenti (raggruppati)
		$sql_where = " AND M.NOME_ANALISI IN (" . sql_t($m_params->open_tab_filters->nome_analisi_1) . ", " . sql_t($m_params->open_tab_filters->nome_analisi_2) . ")";
		$sql_where .= " AND M.CODICE_DITTA = " . sql_t($m_params->open_tab_filters->codice_ditta);
		$sql_where .= " AND M.VOCE_ANALISI = " . sql_t($_REQUEST['node']);

		$sql_where .= sql_where_by_request($m_params, 'Y', 'Y', 'M.');
		
		$sql = "SELECT M.NOME_ANALISI, M.CODICE_CONTO, M.DESCRIZIONE, SUM(M.IMPORTO) AS IMPORTO, R1001.IMPORTO_A
					 FROM {$cfg_mod_Gest['abi']['file_movimenti']} M
					  LEFT OUTER JOIN (SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']}) R1001
					    ON M.CODICE_DITTA = R1001.CODICE_DITTA
					   AND R1001.VOCE_ANALISI = '1001' 
	           		   AND R1001.NOME_ANALISI = " . sql_t($m_params->open_tab_filters->nome_analisi_1) . "
			   		   AND R1001.NOME_ANALISI_CONFR = " . sql_t($m_params->open_tab_filters->nome_analisi_2) . "
		
				WHERE 1=1 {$sql_where}
				GROUP BY M.NOME_ANALISI, M.CODICE_CONTO, M.DESCRIZIONE, R1001.IMPORTO_A
				ORDER BY M.CODICE_CONTO
				";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $sql_params);
		$ar = array();		
		
		while ($r = db2_fetch_assoc($stmt)) {
			$id_liv = trim($r['CODICE_CONTO']);
			
			if (!isset($ar["{$id_liv}"])){
				$ar_new = array();
				$ar_new['id'] = $id_liv;				
				$ar_new['task'] = "[" . trim($r['CODICE_CONTO']) . "] " . trim(acs_u8e($r['DESCRIZIONE']));
				$ar_new['tipo_voce'] = 'dett';				
				$ar_new['leaf'] = true;
				$ar["{$id_liv}"] = $ar_new;
			}

			
			$ar_r = &$ar["{$id_liv}"];

			
			if ($r['NOME_ANALISI'] == $m_params->open_tab_filters->nome_analisi_1){
				$ar_r['IMPORTO_A'] = trim($r['IMPORTO']);
				if ((float)trim($r['IMPORTO_A']) != 0)
					$ar_r['IMPORTO_B'] = (int)trim($r['IMPORTO']) / (int)trim($r['IMPORTO_A']) * 100;
			}
			if ($r['NOME_ANALISI'] == $m_params->open_tab_filters->nome_analisi_2){
				$ar_r['IMPORTO_C'] = trim($r['IMPORTO']);
				if ((float)trim($r['IMPORTO_A']) != 0)				
					$ar_r['IMPORTO_D'] = (int)trim($r['IMPORTO']) / (int)trim($r['IMPORTO_A']) * 100;
			}

		}
		
		
		$ret = array();
		foreach($ar as $kar => $r){
			$ret[] = array_values_recursive($ar[$kar]);
		}
		
		
		echo acs_je($ret);
	}
		
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();

?>

{
 success:true, items: [

 		{ 
  			xtype: 'treepanel',
	        title: <?php echo acs_je("Margini_Azienda {$m_params->open_tab_filters->codice_ditta}"); ?>,
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        
	        tbar: new Ext.Toolbar({
	            items:['<b>Analisi margine di contribuzione per azienda <?php echo get_nome_ditta($m_params->open_tab_filters->codice_ditta); ?> [Periodo: <?php echo $m_params->open_tab_filters->nome_analisi_1; ?>, periodo raffronto: <?php echo $m_params->open_tab_filters->nome_analisi_2; ?>]</b>', '->'
            
				, {iconCls: 'tbar-x-tool x-tool-print',
							tooltip: 'Stampa riclassifica', 
							handler: function(event, toolEl, panel){

								var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report&dettagliata=N',
								        params: {extraParams: Ext.JSON.encode(this.up('panel').getStore().proxy.extraParams)},
								        });	           	
	           		           	
	           	}}
				, {iconCls: 'tbar-x-tool x-tool-print',
							tooltip: 'Stampa esplosa', 
							handler: function(event, toolEl, panel){

								var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report_elenco',
								        	params: {extraParams: Ext.JSON.encode(this.up('panel').getStore().proxy.extraParams)},
								        });	           	
	           		           	
	           	}}	            	           	
	           	
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),  
	        
	        
	        
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'IMPORTO_A', 'IMPORTO_B', 'IMPORTO_C', 'IMPORTO_D', 'IMPORTO_E', 'IMPORTO_F', 'IMPORTO_G', 'IMPORTO_H',
				    		 'tipo_voce'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                        
                      extraParams: {
                      	open_tab_filters:  <?php echo acs_je($m_params->open_tab_filters); ?>,
                      	m_filters: <?php echo acs_je($m_params->m_filters); ?>
                      }
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),

            multiSelect: false,
	        singleExpand: false,
	
			columns: [	
	    		{text: 'Voce', flex: 1, xtype: 'treecolumn', dataIndex: 'task'}
	    		
				 , {header: <?php echo acs_je("Periodo {$m_params->open_tab_filters->nome_analisi_1}"); ?>, width: 180,
				    columns: [ 			
					 , {header: 'Importo', dataIndex: 'IMPORTO_A', width: 100, align: 'right', renderer: floatRenderer0}
					 , {header: '%', dataIndex: 'IMPORTO_B', width: 70, align: 'right', renderer: floatRenderer2}				 					 
				  ]
				 }	    		

				 , {header: <?php echo acs_je("Periodo {$m_params->nome_analisi_2}"); ?>, width: 180,
				    columns: [ 			
					 , {header: 'Importo', dataIndex: 'IMPORTO_C', width: 100, align: 'right', renderer: floatRenderer0}
					 , {header: '%', dataIndex: 'IMPORTO_D', width: 70, align: 'right', renderer: floatRenderer2}				 					 
				  ]
				 }	    		
				 
				 
				 , {header: 'Scostamento', width: 180,
				    columns: [ 			
					 , {header: 'Importo', width: 100, align: 'right',
			          	  renderer: function (value, metaData, record, row, col, store, gridView){						
							return floatRenderer0(parseFloat(record.get('IMPORTO_A')) - parseFloat(record.get('IMPORTO_C')));			    
							}					 
					   }
					 , {header: '%', width: 70, align: 'right',
			          	  renderer: function (value, metaData, record, row, col, store, gridView){
			          	    if (parseFloat(record.get('IMPORTO_C')) != 0)						
								return floatRenderer2( (parseFloat(record.get('IMPORTO_A')) - parseFloat(record.get('IMPORTO_C'))) / parseFloat(record.get('IMPORTO_C')) * 100);			    
							}					 					 
					 }				 					 
				  ]
				 }				 
	    		
	    		
				, {
			                xtype: 'actioncolumn',
			                width: 30,
			                text: '<?php echo "<img src=" . img_path("icone/16x16/search.png") . " height=16>"; ?>',
			                items: [{
			                    icon   : <?php echo img_path("icone/16x16/search.png") ?>, 
			                    tooltip: 'Esplodi',
			                    handler: function(grid, rowIndex, colIndex, d, event) {
			                        var rec = grid.store.getAt(rowIndex);
			                    
									var voci_menu = [];
									
								    voci_menu.push({
							      		text: 'Per Centro di costo', iconCls : 'icon-sub_blue_add-16',      		
							    		handler: function() {
							    			grid.openDrillDown({
							    				field_group: 'f_centro_di_costo',
												voce_analisi: rec.get('id'),
												open_tab_filters:  <?php echo acs_je($m_params->open_tab_filters); ?>,
                      							m_filters: <?php echo acs_je($m_params->m_filters); ?>
							    			});	 
							    		}
									});	
								    voci_menu.push({
							      		text: 'Per Agente', iconCls : 'icon-sub_blue_add-16',      		
							    		handler: function() {
							    			grid.openDrillDown({
							    				field_group: 'f_agente',
												voce_analisi: rec.get('id'),
												open_tab_filters:  <?php echo acs_je($m_params->open_tab_filters); ?>,
                      							m_filters: <?php echo acs_je($m_params->m_filters); ?>
							    			});	 
							    		}
									});	
								    voci_menu.push({
							      		text: 'Per Nazione', iconCls : 'icon-sub_blue_add-16',      		
							    		handler: function() {
							    			grid.openDrillDown({
							    				field_group: 'f_nazione',
												voce_analisi: rec.get('id'),
												open_tab_filters:  <?php echo acs_je($m_params->open_tab_filters); ?>,
                      							m_filters: <?php echo acs_je($m_params->m_filters); ?>
							    			});	 
							    		}
									});	
								    voci_menu.push({
							      		text: 'Per Area Nielsen', iconCls : 'icon-sub_blue_add-16',      		
							    		handler: function() {
							    			grid.openDrillDown({
							    				field_group: 'f_nielsen',
												voce_analisi: rec.get('id'),
												open_tab_filters:  <?php echo acs_je($m_params->open_tab_filters); ?>,
                      							m_filters: <?php echo acs_je($m_params->m_filters); ?>
							    			});	 
							    		}
									});	
								    
								    
									
								 var menu = new Ext.menu.Menu({
					            	items: voci_menu
								 }).showAt(event.xy);
			                    
			                    }
			                }]
			            }	    		
	    		
	    			   	
			],
			enableSort: false, // disable sorting

	        listeners: {
		        	beforeload: function(store, options) {
		        		Ext.getBody().mask('Loading... ', 'loading').show();
		        		},		
			
		            load: function () {
		              Ext.getBody().unmask();
		            },
		            
		            
					celldblclick: {
						fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		
							rec = iView.getRecord(iRowEl);
						    col_name = iView.getGridColumns()[iColIdx].dataIndex;
						    
						    //esco se sono in altrec colonne
						    if (col_name != 'IMPORTO_A' && col_name != 'IMPORTO_C')
						     return false;
						    
						    //verifico su quale colonna ho fatto doppio click

						    if (col_name == 'IMPORTO_A')
						    	ceal = <?php echo acs_je($m_params->open_tab_filters->nome_analisi_1); ?>;
						    if (col_name == 'IMPORTO_C')
						    	ceal = <?php echo acs_je($m_params->open_tab_filters->nome_analisi_2); ?>; 	
						    
						    
						    
						    //in col_name ho in pratico il codice ditta
						    acs_show_panel_std('acs_panel_marginalita_anno.php?fn=open_tab', Ext.id(), {
		                        codice_ditta: <?php echo acs_je($m_params->open_tab_filters->codice_ditta); ?>,
		                        nome_analisi_1: <?php echo acs_je($m_params->open_tab_filters->nome_analisi_1); ?>,
		                        nome_analisi_2: <?php echo acs_je($m_params->open_tab_filters->nome_analisi_2); ?>,
		                        ceal: ceal,
		                        open_tab_filters:  <?php echo acs_je($m_params->open_tab_filters); ?>,
                      			m_filters: <?php echo acs_je($m_params->m_filters); ?>
						    });
		
						}
					}
		            
		            	        
		        }
			

		, viewConfig: {
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }
		         
		         
			 , openDrillDown: function(p){
						    acs_show_panel_std('acs_panel_marginalita_drill_down.php?fn=open_tab', Ext.id(), {
		                        voce_analisi: p.voce_analisi,
		                        field_group: p.field_group,
		                        open_tab_filters: p.open_tab_filters,
		                        m_filters: p.m_filters
						    });			 	
			 }		         
   
		    }												    
		        
			
			    		
 	} 
 
 
 
 
 ]
}
<?php exit; } ?>