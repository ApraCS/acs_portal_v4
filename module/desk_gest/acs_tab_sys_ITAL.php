<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'TAID' => 'ITAL',
    'msg_on_save' => true,
    'title_grid' => 'ITAL- Itinerario alternativo',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160, 'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 60),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30, 'only_view' => 'F'),
        'itin'     => array('label'	=> 'Itinerario', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'gr_doc'   => array('label'	=> 'Gruppo documento',  'short' =>'Gr.Doc.','c_width' => 50, 'tooltip' => 'Gruppo documento', 'xtype' => 'combo_tipo'),
        'divis'    => array('label'	=> 'Divisione', 'short' =>'Div.','c_width' => 50, 'tooltip' => 'Divisione', 'xtype' => 'combo_tipo'),
        'refe'    => array('label'	=> 'Referente', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'tab_sys' => 'BREF'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'   => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'   => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli itinerario alternativo',
    'TAREST' => array(
        'itin'	=> array(
            "start" => 0,
            "len"   => 3,
            "riempi_con" => ""
        )
       ,'gr_doc'	=> array(
            "start" => 33,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'divis'	=> array(
            "start" => 36,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'refe'	=> array(
            "start" => 39,
            "len"   => 3,
            "riempi_con" => ""
        )
    
    )
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
