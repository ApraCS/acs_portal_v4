<?php

function cmr_base_sql_where(){
    $sql = " AND TF.TFFG04 = 'Y' ";
    return $sql;
}

function sql_where_by_request($m_params){
    $sql_where = "";
    
    //Solo fatture in attesa di rientro CMR
    $sql_where .= cmr_base_sql_where();
    
    $sql_where .= sql_where_by_combo_value('TF.TFCCON', $m_params->f_cliente_cod);
    $sql_where .= sql_where_by_combo_value('TF.TFNAZI', $m_params->f_nazione);
    $sql_where .= sql_where_by_combo_value('TF.TFDOCU', $m_params->k_ordine);
    
    switch (trim($m_params->f_filtra_fatture)){
        case 'DA_ABBINARE':
            $sql_where .= " AND TF.TFBDOG = 'Y'";
            break;
        case 'DA_ABBINARE_O_CON_ATTIVITA':
            $sql_where .= " AND (TF.TFBDOG IN('Y','P') OR ATT_OPEN.ASFLRI <> 'Y') ";
            break;
        case 'CON_ATTIVITA': //aperte
            $sql_where .= " AND ATT_OPEN.ASFLRI != 'Y'";
            break;
    }
    
    if (strlen($m_params->f_data_da) > 0)
        $sql_where .= " AND TF.TFDTRG >= {$m_params->f_data_da}";
    if (strlen($m_params->f_data_a) > 0)
        $sql_where .= " AND TF.TFDTRG <= {$m_params->f_data_a}";
            
            
    //con stato/attivita' aperta
    if (isset($m_params->f_stato_attivita) && count($m_params->f_stato_attivita) > 0){
        if (count($m_params->f_stato_attivita) == 1)
            $sql_where .= " AND ATT_OPEN.ASCAAS = " . sql_t($m_params->f_stato_attivita[0]);
            if (count($m_params->f_stato_attivita) > 1)
                $sql_where .= " AND ATT_OPEN.ASCAAS IN (" . sql_t_IN($m_params->f_stato_attivita) . ")";
    }
            
    return $sql_where;
}



function get_ar_nazioni(){
    global $conn, $cfg_mod_Gest, $id_ditta_default;
    global $main_module;
    $cfg_mod = $main_module->get_cfg_mod();
    $sql = "SELECT DISTINCT TFNAZI, TFDNAZ
	        FROM {$cfg_mod_Gest['bollette_doganali']['file_testate']} TF
	        WHERE TFDT = '{$id_ditta_default}' " . cmr_base_sql_where();
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ar[] = array("id" => trim($r['TFNAZI']), "text" => $r['TFDNAZ']);
    }
    return $ar;
}


function crea_ar_tree_CRM($node, $m_params){
    global $cfg_mod_Gest, $conn;
    $ar = array();
   
    $sql_where 		= sql_where_by_request($m_params);
    
    if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
        //ITA|070012762
        $node_exp = explode("|", $_REQUEST['node']);
        $sql_where .= " AND TF.TFNAZI=" . sql_t($node_exp[0]);
        $sql_where .= " AND TF.TFCCON=" . sql_t($node_exp[1]);
    }
    
    
    $sql = "SELECT TF.*
    FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
    WHERE 1=1 {$sql_where}
    ORDER BY TF.TFDNAZ, TF.TFDCON, TF.TFDTRG, TF.TFTPDO, TF.TFDOCU"
    ;
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar = array();
    
    while ($r = db2_fetch_assoc($stmt)) {
        
        $tmp_ar_id = array();
        $ar_r = &$ar;
        
        $cli_liv_tot = 'TOTALE';
        $cod_liv0 = trim($r['TFNAZI']); 	//nazione
        $cod_liv1 = trim($r['TFCCON']);		//codice cliente
        $cod_liv2 = trim($r['TFDOCU']);		//documento
        
        
        //LIVELLO TOTALE
        $liv = $cod_liv_tot;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = 'liv_totale';
            $ar_new['liv'] = 'liv_totale dett_selezionato';
            $ar_new['liv_cod'] = 'TOTALE';
            $ar_new['liv_cod_out'] = '';
            $ar_new['task'] = 'Totale';
            $ar_new['leaf'] = false;
            $ar_new['expanded'] = true;
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        $ar_liv_tot = &$ar_r;
        sum_columns_value($ar_r, $r);
        
        
        //LIVELLO 0
        $liv = $cod_liv0;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['liv'] = 'liv_1';
            $ar_new['liv_cod'] = $liv;
            $ar_new['liv_cod_out'] = $liv;
            $ar_new['task'] = acs_u8e($r['TFDNAZ']);
            $ar_new['leaf'] = false;
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        $ar_liv2 = &$ar_r;
        sum_columns_value($ar_r, $r);
        
        
        //LIVELLO 2
        $liv = $cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['liv'] = 'liv_2';
            $ar_new['liv_cod'] = $liv;
            $ar_new['liv_cod_out'] = $liv;
            $ar_new['task'] = acs_u8e($r['TFDCON']);
            $ar_new['leaf'] = false;
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        $ar_liv3 = &$ar_r;
        sum_columns_value($ar_r, $r);
        
        
        //LIVELLO 4
        $liv = $cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['liv'] = 'liv_3';
            $ar_new['liv_cod'] = acs_u8e($r['TFDOCU']);
            $ar_new['liv_cod_out'] = acs_u8e($r['TFTPDO']);
            $ar_new['liv_cod_qtip'] = acs_u8e($r['TFDTPD']);
            $ar_new['liv_data'] = $r['TFDTRG'];
            $ar_new['TFBDOG'] 	= $r['TFBDOG'];
            
            $as = new SpedAssegnazioneOrdini;
            $ret_stato_tooltip = $as->stato_tooltip_entry_per_ordine($r['TFDOCU'], null, 'N', 'CM');
            $ar_new['with_todo'] = $ret_stato_tooltip['stato'];
            $ar_new['with_todo_qtip'] = $ret_stato_tooltip['tooltip'];
            $ar_new['stato'] = $ret_stato_tooltip['att_open'];
            
            $ar_new['vettore_out'] = acs_u8e(trim($r['TFRGSV']));
            
            
            if ($r['TFFG02'] == 'M') //multiagente
            {
                $ar_new['liv_cod_out'] .= " " . "<img src=" . img_path("icone/48x48/user_group.png") . " width=18>";
            }
            
            //$ar_new['task'] = acs_u8e($r['TFDOCU']);
            $ar_new['task'] = "[{$r['TFINUM']}] " . implode("_", array($r['TFAADO'], $r['TFNRDO'], $r['TFDT']));
            
            $ar_new['fl_blocco_sblocco_ricalcolo'] = acs_u8e($r['TFFG01']);
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
            
            sum_columns_value_TD($ar_liv_tot, $ar_liv0, $ar_liv1, $ar_liv2, $ar_liv3, $ar_r["{$liv}"], $r, $ar_new);
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $r);
        
    }
    
    return $ar;
    
}


//**************************************************************************************
// TREE	PRINCIPALE
//**************************************************************************************
function write_main_tree($p){	
?>	
			{
				xtype: 'treepanel',
				cls: '',
		        title: 'CMR',
		        <?php echo make_tab_closable(); ?>,
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
		        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Riepilogo documenti in attesa di rientro CMR</b>', '->'
		            
		            
		            
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),  
		        
		        
		        
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					    fields: ['TFBDOG', 'bolletta_doganale', 'bolletta_doganale2', 'bolletta_doganale_out', 'data_bolletta_doganale', 'data_bolletta_doganale_out', 'trCls', 'task', 'liv', 'liv_data', 'liv_cod', 'liv_cod_out', 'liv_cod_qtip', 'perc_provvigione', 'imponibile_provvigione', 
					    'importo_provvigione', 'tot_documento', 'tot_anticipo', 'tot_netto_merce', 'tot_imponibile', 
					    'tot_documento', 'fl_blocco_sblocco_ricalcolo', 'fl_raee', 'vettore_out', 'stato','conteggio', 
					    'with_todo', 'with_todo_qtip', 'volume', 'pesi', 'colli', 'flag'],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							actionMethods: {read: 'POST'},
							timeout: 240000,
	                        
	                        reader: {
	                            root: 'children'
	                        },
	                        
							extraParams: <?php echo acs_je($p) ?>
	                	    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),
	
	            multiSelect: true,
		        singleExpand: false,
		
				columns: [
					<?php echo dx_mobile() ?>	
		    		, {text: 'Nazione / Cliente / Documento', flex: 1, xtype: 'treecolumn', dataIndex: 'task', menuDisabled: true, sortable: false}
					, {text: 'Codice', width: 100, dataIndex: 'liv_cod_out', menuDisabled: true, sortable: false,					
						renderer: function(value, metaData, record){
						
					    	if (record.get('liv_cod_qtip') != ''){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('liv_cod_qtip')) + '"';			    	
					    	}						
						
						 return value;
						}
					}		    		
					
					, {text: 'Data', width: 70, dataIndex: 'liv_data', renderer: date_from_AS,  menuDisabled: false, sortable: false}
					, {header: 'Totale documenti', tdCls: '', dataIndex: 'tot_documento', width: 120, renderer: floatRenderer2, align: 'right'}					
					, {header: 'Totale imponibile', tdCls: '', dataIndex: 'tot_imponibile', width: 120, renderer: floatRenderer2, align: 'right'}
					, {header: 'Anticipo', tdCls: '', dataIndex: 'tot_anticipo', width: 120, renderer: floatRenderer2, align: 'right'}
					, {header: 'Netto merce', tdCls: '', dataIndex: 'tot_netto_merce', width: 120, renderer: floatRenderer2, align: 'right'}					
					
					, {header: 'Data', tdCls: '', dataIndex: 'data_bolletta_doganale_out', width: 60}					
					, {header: 'Vettore', tdCls: '', dataIndex: 'vettore_out', width: 100}
					, {header: '#', tdCls: '', dataIndex: 'conteggio', width: 40, align: 'right'}					
					, {header: '<img src=<?php echo img_path("icone/48x48/arrivi.png") ?> width=25>',  dataIndex: 'ha_POSTM', width: 32,
							renderer: function(value, metaData, record){
							
								if (record.get('with_todo_qtip') != ''){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('with_todo_qtip')) + '"';			    	
					    		}
							
						    	if (record.get('with_todo') == 1) return '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=18>';			    	
								if (record.get('with_todo') == 2) return '<img src=<?php echo img_path("icone/48x48/arrivi.png") ?> width=18>';			    	
						    }		 
					}
					, {header: 'Stato', tdCls: '', dataIndex: 'stato', width: 100}	
					, {header: 'Volume', dataIndex: 'volume', width: 100, renderer: floatRenderer2, align: 'right'}	
					, {header: 'Peso', dataIndex: 'pesi', width: 100, renderer: floatRenderer2, align: 'right'}	
					, {header: 'Colli', dataIndex: 'colli', width: 100, renderer: floatRenderer2, align: 'right'}									
		    			   	
				],
				enableSort: false, // disable sorting
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
			          
			          
					  celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  }
					  },
			            
			          
			          
			          itemcontextmenu : function(grid, rec, node, index, event) {			          	
			                event.stopEvent();			                         
							var voci_menu = [];
			                          
			                          
							if (rec.get('liv') == 'liv_2'){ //livello cliente
										  
								  my_listeners = {
		        					afterUpdateRecord: function(from_win){	
		        						from_win.close();
						        	}								  
								  }		  
			  								  										  
							} //liv cliente
							
							
							
							
							if (rec.get('liv') == 'liv_3'){ //livello fatture		  
							
								id_selected = grid.getSelectionModel().getSelection();							
								list_selected_id = [];
							  	for (var i=0; i<id_selected.length; i++) 
								   list_selected_id.push(id_selected[i].data.liv_cod);
							
								//verificare che appartengano allo stesso cliente
								parentNode_id = rec.parentNode.get('id');
							  	for (var i=0; i<id_selected.length; i++)
							  	 if (id_selected[i].parentNode.data.id != parentNode_id){
									acs_show_msg_error('Selezionare documenti appartenenti allo stesso cliente');
									return false;							  	 
							  	 }
							  	   								
								
							  	
							
							
								my_listeners = {
		        					afterUpdateRecord: function(from_win, bolletta_doganale){	
		        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
		        						
		        						for (var i=0; i<id_selected.length; i++) 
								   			id_selected[i].set('bolletta_doganale_out', bolletta_doganale);
		        								        						
		        						from_win.close();
						        		}
				    				};							
							

								  
								  
						  		  voci_menu.push({
						         		text: 'Visualizza righe',
						        		iconCls : 'icon-folder_search-16',          		
						        		handler: function() {
						        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('liv_cod')}, 900, 450, null, 'icon-folder_search-16');          		
						        		}
						    		});								  
								  

						  		  
						  		  
								my_listeners_inserimento = {
		        					afterInsertRecord: function(from_win){	
		        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
		        						rec.store.treeStore.load({node: rec.parentNode});		        						
		        						from_win.close();
						        		}
				    				};						  		  
						  		  
						  		  voci_menu.push({
						         		text: 'Inserimento nuovo stato/attivit&agrave;',
						        		iconCls : 'icon-arrivi-16',          		
						        		handler: function() {
										acs_show_win_std('Nuovo stato/attivit&agrave;', 
											<?php echo j('acs_form_json_create_entry.php'); ?>, 
											{
												tipo_op: 'POSTM',
												blocco_op: 'CM',
								  				list_selected_id: list_selected_id
								  			}, 500, 450, my_listeners_inserimento, 'icon-arrivi-16');						        		          		
						        		}
						    		});
						    		
						    		
						    		
						  		  voci_menu.push({
						         		text: 'Gestione stato/attivit&agrave; per documento',
						        		iconCls : 'icon-arrivi-16',          		
						        		handler: function() {
										acs_show_panel_std( 
											<?php echo j('acs_panel_cmr_todolist.php?fn=open_tab'); ?>,
											null, {k_ordine: rec.get('liv_cod')} 
											);						        		          		
						        		}
						    		});			
						    		
						    		
						    		voci_menu.push({
						         		text: 'Annulla attesa',
						        		iconCls : 'iconDelete',          		
						        		handler: function() {
						        		
						        		Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta?' , function(btn, text){
            	   						  if (btn == 'yes'){
						        		
						        			Ext.Ajax.request({
													url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_annulla_attesa_CMR',
											        jsonData: {list_selected_id: list_selected_id},
											        method     : 'POST',
											        waitMsg    : 'Data loading',
											        success : function(result, request){
											        	
											        	var id_selected = grid.getSelectionModel().getSelection();							
                        								list_selected_id = [];
                        							  	for (var i=0; i<id_selected.length; i++) 
                        								   id_selected[i].set('flag', 'Y');									        	
											        }, scope: id_selected[i],
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });	
										   } //yes
										  });
															        		          		
						        		} //handler
						    		});			    		

							} //liv fatture
							
							
							
							
							
					      var menu = new Ext.menu.Menu({
					            items: voci_menu
						}).showAt(event.xy);
			                          
			          }
			          
			          
			            	        
			        } //listeners
				
	
			, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			           if (record.get('trCls')=='grassetto')
					        return ret + ' grassetto';
			           
			           if (record.get('trCls')=='G')
					        return ret + ' colora_riga_grigio';
			           if (record.get('trCls')=='Y')
					        return ret + ' colora_riga_giallo';					        
			           		 
			           if (record.get('flag') == 'Y') //rilasciato
		           			return ret + ' barrato';
			           		        	
			           return ret;																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree













function write_dettaglio_riferimento($p){
	?>
        {
            xtype: 'grid', itemId: 'dettaglio_per_riferimento',
            flex: 1,
			
		    		features: new Ext.create('Ext.grid.feature.Grouping',{
        							groupHeaderTpl: 'Intestatario: {[values.rows[0].data.cod_web]} - {[values.rows[0].data.des_web]}',
        							hideGroupedHeader: true
    						}),			
			
			store: {
					xtype: 'store',
					groupField: 'cod_web',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_per_riferimento',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
										f_ditta: <?php echo acs_je($p['f_ditta']); ?>,
										dataStart:  <?php echo acs_je($p['dataStart']); ?>,
										dataEnd:  <?php echo acs_je($p['dataEnd']); ?>,
										raggruppamento: <?php echo acs_je($p['raggruppamento']); ?>,
										categoria: <?php echo acs_je($p['categoria']); ?>  
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['data', 'riferimento', 'importo', 'cod_web', 'des_web']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Data',
	                dataIndex: 'data', renderer: date_from_AS,
	                width: 60 
	            }, {
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                flex: 10
	            }, {
	                header   : 'Importo',
	                dataIndex: 'importo', renderer: floatRenderer2, align: 'right',
	                flex: 10
	            }   
	         ],																					

	         
//itemclick: function(view,rec,item,index,eventObj) {	         
	         
	        listeners: {
	        
					  itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  	grid_dettagli_riga = iView.up('panel').up('panel').getComponent('dettaglio_per_riga');
						  	grid_dettagli_riga.store.proxy.extraParams.data = rec.get('data');
						  	grid_dettagli_riga.store.proxy.extraParams.riferimento = rec.get('riferimento');
						  	grid_dettagli_riga.store.load();
						  }
					  }	  	        	        

			}			
			
				  
	            
        }   	
	
	
	<?php
} //write_dettaglio_riferimento






function write_dettaglio_righe($p){
	?>
        {
            xtype: 'grid', itemId: 'dettaglio_per_riga',
			flex: 1,
			store: {
					xtype: 'store',
					autoLoad: false,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_per_riga',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
										f_ditta: <?php echo acs_je($p['f_ditta']); ?>,
										dataStart:  <?php echo acs_je($p['dataStart']); ?>,
										dataEnd:  <?php echo acs_je($p['dataEnd']); ?>,
										raggruppamento: <?php echo acs_je($p['raggruppamento']); ?>,
										categoria: <?php echo acs_je($p['categoria']); ?>,
										data: null,
										riferimento: null 
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['riga', 'cod_articolo', 'descr_articolo', 'riferimento', 'importo']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width: 60 
	            }, {
	                header   : 'Articolo',
	                dataIndex: 'cod_articolo',
	                width: 80
	            }, {
	                header   : 'Descrizione',
	                dataIndex: 'descr_articolo',
	                flex: 10
	            }, {
	                header   : 'Importo',
	                dataIndex: 'importo', renderer: floatRenderer2, align: 'right',
	                width: 90
	            }   
	         ],																					

	         
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
						  }
					  }	  
			}			
			
				  
	            
        }   	
	
	
	<?php
} //write_dettaglio_righe



/* TODOLIST */
function write_main_cmr_todolist($p){
	?>
        {
            xtype: 'grid',
			flex: 1,
			title: 'CMR - ToDo',
			closable: true,
			
	        tbar: new Ext.Toolbar({
	            items:[
	            '<b>Gestione attivit&agrave; rientro CMR</b>', '->',
	            
	            {iconCls: 'icon-gear-16',
    	             text: 'CAUSALI', 
		           		handler: function(event, toolEl, panel){
			           		acs_show_panel_std('acs_gestione_attav.php?fn=open_grid', 'panel_gestione_attav', {sezione: 'CM'});
			          
		           		 }
		           	 }
	            
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),  
			
			
			store: {
					xtype: 'store',
					autoLoad: true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: <?php echo acs_je($p)?>
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['id', 'prog', 'k_ordine', 'rec_stato', 'data', 'ora', 'utente', 'fattura', 'cliente', 'causale', 'causale_out', 'note_base', 'note_estese', 'utente_assegnato', 'scadenza']							
							
			
			}, //store
			
			
			
	        columns: [	
	        	<?php  echo dx_mobile() ?>
				, {header   : 'Data', 		dataIndex: 'data', 			width: 70, renderer: date_from_AS}
				, {header   : 'Ora', 		dataIndex: 'ora', 			width: 45, renderer: time_from_AS}
				, {header   : 'Immesso da', dataIndex: 'utente', 	width: 90}
				, {header   : 'Fattura', 	dataIndex: 'fattura', 		width: 150}
				, {header   : 'Cliente', 	dataIndex: 'cliente', 		flex: 1}
				, {header   : 'Causale', 	dataIndex: 'causale_out',	flex: 1}
				, {header   : 'Note', 		dataIndex: 'note_base', 	flex: 1}
				, {header   : 'Memo',		dataIndex: 'note_estese',	flex: 1}
				, {header   : 'Assegnato a', 	dataIndex: 'utente_assegnato', 		width: 90}
				, {header   : 'Scadenza', 	dataIndex: 'scadenza', 		width: 70,
						renderer: function (value, metaData, record, row, col, store, gridView){

							if (parseFloat(record.get('scadenza')) > 0){
								oggi = new Date();
							
								if (parseFloat(record.get('scadenza')) < parseFloat(Ext.Date.format(oggi, 'Ymd')))
									metaData.tdCls += ' tpSfondoRosa grassetto';																	
								if (parseFloat(record.get('scadenza')) == parseFloat(Ext.Date.format(oggi, 'Ymd')))
									metaData.tdCls += ' tpSfondoGrigio grassetto';								
								
							}

    						return date_from_AS(value);	
    					}				
				}
				, {header: '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=25>',  dataIndex: 'rec_stato', width: 32, tooltip: 'Evasa',				
							renderer: function(value, metaData, record){
							
								if (value == 'Y'){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Evasa') + '"';			    	
					    		}
							
						    	if (value=='Y') return '<img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=18>';			    				    	
						    }				
				}
	         ],																					

	         
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
						  }
					  },
					  
					  itemcontextmenu : function(grid, rec, node, index, event) {			  	
					  
						  event.stopEvent();		  
					      var voci_menu = [];
					      
						  id_selected = grid.getSelectionModel().getSelection();
						  list_selected_id = [];
						  for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push(id_selected[i].data);
														      
		      
						  voci_menu.push({
				      		text: 'Avanzamento/Rilascio attivit&agrave;',
				    		iconCls: 'icon-sub_blue_accept-16',
				    		handler: function() {
	  	
					    		//verifico che abbia selezionato solo righe non gia' elaborate 
								  for (var i=0; i<id_selected.length; i++){ 
									  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
										  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
										  return false;
									  }
								  }  	
	  	
	
				    			//apro form per richiesta parametri
								var mw = new Ext.Window({
								  width: 800
								, height: 380
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Avanzamento/Rilascio attivit&agrave;'
								, iconCls: 'iconAccept'			
								, layout: 'fit'
								, border: true
								, closable: true
								, id_selected: id_selected
								, listeners:{
						                 'close': function(win){
						                      for (var i=0; i<id_selected.length; i++){
						                      	//per i record che erano selezionato verifico se hanno 
						                      	//adesso il flag di rilasciato (e devo sbarrarli)
						                        
												Ext.Ajax.request({
													url: 'acs_form_json_avanzamento_entry.php?fn=exe_richiesta_record_rilasciato',
											        jsonData: {prog: id_selected[i].get('prog')},
											        method     : 'POST',
											        waitMsg    : 'Data loading',
											        success : function(result, request){
											        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
											        }, scope: id_selected[i],
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });					                        
						                      }
						                  }
						
						         }								
								});				    			
				    			mw.show();			    			
	
								//carico la form dal json ricevuto da php
								Ext.Ajax.request({
								        url        : 'acs_form_json_avanzamento_entry.php',
								        jsonData: {list_selected_id: list_selected_id, grid_id: grid.id},
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								            var jsonData = Ext.decode(result.responseText);
								            mw.add(jsonData.items);
								            mw.doLayout();				            
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
					    		
				    		}
						  });
						  
						  
								  my_listeners = {
		        					afterUpdateRecord: function(from_win){	
		        						from_win.close();
		        						grid.store.reload();
						        	}								  
								  }		  
						  

						  voci_menu.push({
				      		text: 'Modifica',
				    		iconCls: 'icon-leaf-16',
				    		handler: function() {

				    					acs_show_win_std('Modifica', 
											'acs_form_json_modify_entry.php', 
											{
								  				prog: rec.get('prog'),
								  			}, 450, 300, my_listeners, 'icon-leaf-16');
				    		
				    		}
						  });
						  
						  
						  
						      
					    var menu = new Ext.menu.Menu({
				            items: voci_menu
						}).showAt(event.xy);
					  
					      
					      
					  } //itemcontextmenu
					  	  	  
			} //listeners
			
			
			, viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = '';
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }
				  
	            
        }   	
	
	
	<?php
} //function








?>