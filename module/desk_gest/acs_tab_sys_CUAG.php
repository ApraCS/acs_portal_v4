<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
$puvn = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";



$m_table_config = array(
    'TAID' => 'CUAG',
    'msg_on_save' => true,
    'title_grid' => 'CUAG-Agenti',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160, 'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Denominazione', 'maxLength' => 60),
        'TADES2'   => array('label'	=> 'Denominazione 2', 'maxLength' => 30, 'only_view' => 'F'),
        'denom'    => array('label'	=> 'Denominazione 3', 'maxLength' => 60, 'only_view' => 'F'),
        'indi1'    => array('label'	=> 'Indirizzo 1', 'maxLength' => 30, 'only_view' => 'F'),
        'indi2'    => array('label'	=> 'Indirizzo 2', 'maxLength' => 30, 'only_view' => 'F'),
        'loca1'    => array('label'	=> 'Localit� 1', 'maxLength' => 30, 'only_view' => 'F'),
        'loca2'    => array('label'	=> 'Localit� 2', 'maxLength' => 30, 'only_view' => 'F'),
        'cap'      => array('label'	=> 'Cap', 'maxLength' => 10, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 230, 'n_fc' => 1),
        'prov'     => array('label'	=> 'Provincia', 'maxLength' => 2, 'only_view' => 'F', 'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'labelWidth' => 60, 'width' => 130, 'close' => 'Y'),
        //'forn'     => array('label'	=> 'Fornitore', 'maxLength' => 9),
        'provg'    => array('label' => 'Provvigioni', 'maxLength' => 5, 'xtype' => 'number', 'only_view' => 'F'),
        'tipo'     => array('label'	=> 'Tipo', 'maxLength' => 1, 'c_width' => 40, 'tooltip' => 'Tipo', 'xtype' => 'combo_tipo'),
        'b2b'      => array('label'	=> 'B2B', 'maxLength' => 3, 'c_width' => 40, 'tooltip' => 'Raggruppamento B2B', 'xtype' => 'combo_tipo'),
        'g_prov'   => array('label'	=> 'Genera provvigioni', 'short'	=> 'Gen.Pr.', 'maxLength' => 1,  'c_width' => 50, 'tooltip' => 'Genera provvigioni', 'xtype' => 'combo_tipo'),
        'agenti'   => array('label'	=> 'Gruppo agenti', 'maxLength' => 3,  'short' =>'Gr.A.','c_width' => 50, 'tooltip' => 'Gruppo agenti', 'xtype' => 'combo_tipo'),
        'invio'   => array('label'	=> 'Invio ordini', 'maxLength' => 1, 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'   => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'   => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli agente',
    'TAREST' => array(
        'denom'	=> array(
            "start" => 0,
            "len"   => 30,
            "riempi_con" => ""
        )
       ,'indi1'	=> array(
            "start" => 30,
            "len"   => 30,
            "riempi_con" => ""
        )
        ,'indi2'	=> array(
            "start" => 60,
            "len"   => 30,
            "riempi_con" => ""
        )
        ,'loca1'	=> array(
            "start" => 90,
            "len"   => 30,
            "riempi_con" => ""
        )
        ,'loca2'	=> array(
            "start" => 120,
            "len"   => 30,
            "riempi_con" => ""
        )
        ,'cap'	=> array(
            "start" => 150,
            "len"   => 10,
            "riempi_con" => ""
        )
        ,'prov'	=> array(
            "start" => 160,
            "len"   => 2,
            "riempi_con" => ""
        )
        ,'forn'	=> array(
            "start" => 162,
            "len"   => 9,
            "riempi_con" => "0"
        )
        ,'provg'	=> array(
            "start" => 171,
            "len"   => 4,
            "riempi_con" => ""
        )
        ,'tipo'	=> array(
            "start" => 195,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'b2b'	=> array(
            "start" => 208,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'g_prov'	=> array(
            "start" => 211,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'g_age'	=> array(
            "start" => 240,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'invio'	=> array(
            "start" => 243,
            "len"   => 1,
            "riempi_con" => ""
        )
    )
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
