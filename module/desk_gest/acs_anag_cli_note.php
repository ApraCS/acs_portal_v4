<?php
require_once "../../config.inc.php";

$main_module = new DeskGest();

$m_params = acs_m_params_json_decode();

if (!function_exists('ord_utf8')) {
    function ord_utf8($s){
        return (int) ($s=unpack('C*',$s[0].$s[1].$s[2].$s[3]))&&$s[1]<(1<<7)?$s[1]:
        ($s[1]>239&&$s[2]>127&&$s[3]>127&&$s[4]>127?(7&$s[1])<<18|(63&$s[2])<<12|(63&$s[3])<<6|63&$s[4]:
            ($s[1]>223&&$s[2]>127&&$s[3]>127?(15&$s[1])<<12|(63&$s[2])<<6|63&$s[3]:
                ($s[1]>193&&$s[2]>127?(31&$s[1])<<6|63&$s[2]:0)));
    }
}


if($_REQUEST['fn'] == 'exe_upd_commento_cliente'){
    
    $tpno = "CF";

    $form_values = $m_params->form_values;
   
    foreach($m_params->form_values as $k => $v){
  
        if (substr($k, 0, 7) == 'f_text_'){
            $ar_value = explode('_', $k);
            $riga = $ar_value[2];
            $rrn = $ar_value[3];
                   
            $name = "f_h_text_{$riga}";
            $old_value = trim($m_params->form_values->$name);
            
            $ar_ins = array();
            
            //echo mb_detect_encoding($v);
            
            $v2 = "";
            
            $stringlength = mb_strlen($v);
            for ($i=0; $i < $stringlength; $i++) {
                $c = mb_substr($v, $i, 1);
                if (ord_utf8($c) == 8364)
                    $v2 .= 'E'; //al momento sostituisco l'euro con la E //html_entity_decode("&#039;");
                else 
                    $v2 .= $c;
            }
            
            
            
            $ar_ins['RLSWST'] 	= acs_toDb(mb_substr($v2, 0, 1));    //1
            $ar_ins['RLREST1'] 	= acs_toDb(mb_substr($v2, 1, 15));   //15
            $ar_ins['RLFIL1'] 	= acs_toDb(mb_substr($v2, 16, 64));  //64
            $ar_ins['RLDTUM'] 	= oggi_AS_date();
            $ar_ins['RLUSUM'] 	= $auth->get_user();
            
            if($rrn != ""){
                
                if(trim($v) == ""){
                    $sql = "DELETE
                            FROM {$cfg_mod_DeskUtility['file_note_anag']} RL
                            WHERE RRN(RL) = '{$rrn}' ";
                    
                    $stmt = db2_prepare($conn, $sql);
                    $result = db2_execute($stmt);
                    
                    
                }else{
                    if($old_value != trim($v)){
                        $sql = "UPDATE {$cfg_mod_DeskUtility['file_note_anag']} RL
                        SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                        WHERE RRN(RL) = '{$rrn}'";
                    
                        $stmt = db2_prepare($conn, $sql);
                        echo db2_stmt_errormsg();
                        $result = db2_execute($stmt, $ar_ins);
                        echo db2_stmt_errormsg($stmt);
                
                     }
                    
                   
                }
                
            }else{
                if(trim($v) != ""){
                    
                    $ar_ins['RLDTGE'] 	= oggi_AS_date();
                    $ar_ins['RLUSGE'] 	= $auth->get_user();
                    $ar_ins['RLDT'] 	= $id_ditta_default;
                    $ar_ins['RLRIFE1'] 	= $m_params->cliente;
                    $ar_ins['RLRIFE2'] 	= $m_params->bl;
                    $ar_ins['RLTPNO'] 	= $tpno;
                    $ar_ins['RLRIGA']   = $riga;
                                      
                    $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_note_anag']}(" . create_name_field_by_ar($ar_ins) . ")
			                VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                    
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt, $ar_ins);
                    echo db2_stmt_errormsg();
                    
                    
                }
                
            }
        
        
    }
    
        
    }
     
   $ha_commenti = $main_module->has_commento_cliente($m_params->cliente, $m_params->bl);
   $comm = "";
   $grassetto = "";
    if ($ha_commenti == FALSE)
        $img_com_name = "iconCommGray";
    else{
        $img_com_name = "iconCommYellow";
        $grassetto = 'Y';
        $commento_txt = $main_module->get_commento_cliente($m_params->cliente, $m_params->bl);
        for($i = 0; $i <= count($commento_txt); $i++){
            if(trim( $commento_txt[$i]) != '')
                $comm .= $commento_txt[$i]."\r\n";
        }
    }
    $ret = array();
    $ret['success'] = true;
    $ret['icon'] = $img_com_name;
    $ret['txt'] = $comm;
    $ret['grassetto'] = $grassetto;
    echo acs_je($ret);
    exit;
}


if($_REQUEST['fn'] == 'open_bl'){
    
    $mastro = substr($_REQUEST['cliente'], 0, 2);
    
    $title = " {$_REQUEST['desc']} blocco note {$row['TANR']}";
    
?>    
    
 {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
            frame: true,
            title: '',
            items: [ 
                
                <?php 
            
                $sql = "SELECT RRN(TA) AS RRN, TANR, TADESC, TATP
                        FROM {$cfg_mod_DeskUtility['file_tab_sys']} TA
                        WHERE TADT = '{$id_ditta_default}'
                        AND TAID = 'NUSN'
                        AND TACOR1 = 'CF' AND TACOR2 = '{$mastro}' ORDER BY TATP, TANR";
                
               
                $stmt = db2_prepare($conn, $sql);
                $result = db2_execute($stmt);
                
                                
                while($row = db2_fetch_assoc($stmt)){
                    
                    $c++;
                    
                    $ha_commenti = $main_module->has_commento_cliente($_REQUEST['cliente'], $row['TANR']);
                    $txt_bl = "[".trim($row['TANR'])."] ".trim($row['TADESC']);
                   
                
                if ($ha_commenti == FALSE){
                    $img_com_name = "iconCommGray";
                    $grassetto = 'N';
                    
                }else{
                    $img_com_name = "iconCommYellow";
                    $grassetto = 'Y';
                   
                }
                
                 $commento_txt = $main_module->get_commento_cliente($_REQUEST['cliente'], $row['TANR']);
                 $data_user =  $commento_txt['data_user'];
                 $min_data_user_ge = print_date($data_user['min_data_ge']) ." ".$data_user['min_user_ge'];
                 $max_data_user_um = print_date($data_user['max_data_um']) ." ".$data_user['max_user_um'];
                 if($min_data_user_ge != $max_data_user_um && $data_user['max_data_um'] > 0)
                     $data_user_txt = $min_data_user_ge." - ".$max_data_user_um;
                 else
                     $data_user_txt = $min_data_user_ge;
                  
                ?>
                	
                		{
							 xtype: 'fieldcontainer',
							 flex: 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
								xtype: 'button',
								text: '',
								itemId: 'f_bt_<?php echo $row['RRN']; ?>',
								margin : '0 5 0 0',
								iconCls: '<?php echo $img_com_name; ?>',
								scale: 'small',
								width : 25,
								handler : function(){
								
								var form = this.up('form').getForm();
								var win = this.up('window');
								
			        			acs_show_win_std('Annotazioni cliente ' + <?php echo j($title); ?>, 
				    					'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
				    					{cliente: '<?php echo $_REQUEST['cliente']; ?>', bl: '<?php echo $row['TANR']; ?>'}, 800, 400,  {
			        					'afterSave': function(from_win, src){
			        					    var bt = win.down('#f_bt_<?php echo $row['RRN']; ?>');
			        					    bt.setIconCls(src.icon);
			        					    var bl = win.down('#f_bl_<?php echo $row['RRN']; ?>');
			        					    if(src.grassetto == 'Y'){
			        					   	   bl.setValue('<b>'+ bl.getValue() +'</b>');
			        					    }else{
			        					       bl.setValue('<?php echo $txt_bl ?>');
			        					    }
			        					    var area = win.down('#f_area_<?php echo $row['RRN']; ?>');
			        					    if(src.txt.trim() != ''){
			        					    	area.show();
			        							area.setValue(src.txt);
			        						}else{
			        						    area.hide();
			        						}
			        						from_win.close();
			        					}
			        				}, 'iconCommGray');
			        		
			        				
			        				
			        				//this.up('window').close();
									
								} 
							},
						
						{
						name: 'f_text_<?php echo $row['RRN'] ?>',
						xtype: 'displayfield',
						itemId: 'f_bl_<?php echo $row['RRN']; ?>',
						fieldLabel: '',
					    anchor: '-15',	
					    flex: 1,			    
					    value: <?php echo j($txt_bl); ?>,
					    <?php if(trim($row['TATP']) == 'S'){?>
					    fieldStyle:{
                                "color": 'red'
                         },
					    <?php }?>
					    listeners: {
								render: function(component) {
								<?php if($grassetto == 'Y'){?>
									component.setValue('<b>'+ component.getValue() +'</b>');
							    <?php }?>		
                            }
                        }						
					},
					
					{
						name: 'f_data_user',
						xtype: 'displayfield',
						flex : 0.7,
						margin : '0 15 0 0',
						fieldStyle:{
                                "text-align": 'right',
                         },
					    anchor: '-15',					    
					    value: <?php echo j($data_user_txt); ?>						
					},
					
					
				
						]},
						
					<?php  if($ha_commenti == true){
										    
					    $comm = "";
					    $hidden = "hidden : false,";
					    for($i = 0; $i <= count($commento_txt['testi']); $i++){
					        if(trim( $commento_txt['testi'][$i]) != '')
					            $comm .= $commento_txt['testi'][$i]."\r\n";
					    }
                        }else 
                         $hidden = "hidden : true,"; ?>
                      
                        
					        
					        {
					            name: 'f_text_<?php echo $i ?>',
					            <?php echo $hidden; ?>
					            xtype     : 'textareafield',
					            itemId: 'f_area_<?php echo $row['RRN']; ?>',
				                height: 100, width: '100%',
					            readOnly:true,
					            margin: '0 0 0 25',
					            anchor: '-15',
					            value: <?php echo j($comm); ?>
					         },
					
					
					
					
					
					<?php }?>	
                	
                	
				]            
				
        }
]}   
    
    
    
    
<?php  
exit;
}


if($_REQUEST['fn'] == 'open_tab'){
    
 $bl = $m_params->bl;
 $cliente = $m_params->cliente;
 $commento_txt = $main_module->get_riga_commento_cliente($cliente, $bl);
 
 $num_ann = 10;
    
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            items: [ 
                
                <?php for($i = 1; $i<= $num_ann;$i++){ ?>
                	{
						name: 'f_h_text_<?php echo $i ?>',
						xtype: 'textfield',
						hidden : true,
					    anchor: '-15',
					    maxLength: 80,					    
					    value: <?php echo j(trim($commento_txt[$i]['text'])); ?>,							
					},	
                	{
						name: 'f_text_<?php echo "{$i}_{$commento_txt[$i]['rrn']}" ?>',
						xtype: 'textfield',
						fieldLabel: '',
					    anchor: '-15',
					    maxLength: 80,					    
					    value: <?php echo j(trim($commento_txt[$i]['text'])); ?>,							
					},	
					
					<?php }?>	
                	
                	
				],
			buttons: [
		
				{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	  
					
						Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upd_commento_cliente',
					        jsonData: {
					        	form_values: form.getValues(),
					        	cliente : '<?php echo $cliente; ?>',
					        	bl : '<?php echo $bl; ?>'
					        	
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	
					        try {
                               var jsonData = Ext.decode(result.responseText);
							   loc_win.fireEvent('afterSave', loc_win, jsonData);
                            }
                            catch(err) {
                                Ext.Msg.alert('Try&catch error', err.message);
                            }
					       
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					            console.log('errorrrrr');
					        }
					    });	  
					
	

				    }            	                	                
	            }
	        }
   
	        ]             
				
        }
]}

<?php }
exit;