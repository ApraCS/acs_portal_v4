<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    $nr['denom1'] = $row['TADES2'];  
    $nr['ordine'] = $row['TAORDI'];
    $nr['denom2'] = substr($row['TAREST'], 0, 30);
    $nr['denom3'] = substr($row['TAREST'], 30, 30);
    $nr['indi1'] = substr($row['TAREST'], 60, 30);
    $nr['indi2'] = substr($row['TAREST'], 90, 30);
    $nr['loca1'] = substr($row['TAREST'], 120, 30);
    $nr['loca2'] = substr($row['TAREST'], 150, 30);
    $nr['cap'] = substr($row['TAREST'], 180, 10);
    $nr['prov'] = substr($row['TAREST'], 190, 2);
    $nr['stre'] = substr($row['TAREST'], 192, 10);
    $nr['data_val'] = substr($row['TAREST'], 202, 8);
    $nr['dest1'] = substr($row['TAREST'], 210, 3);
    $nr['dest2'] = substr($row['TAREST'], 213, 3);
    $nr['dest3'] = substr($row['TAREST'], 216, 3);
    $nr['dest4'] = substr($row['TAREST'], 219, 3);
    $nr['dest5'] = substr($row['TAREST'], 222, 3);
    $nr['dest6'] = substr($row['TAREST'], 225, 3);
    $nr['dest7'] = substr($row['TAREST'], 228, 3);
    $nr['dest8'] = substr($row['TAREST'], 231, 3);
    $nr['dest9'] = substr($row['TAREST'], 234, 3);
    $nr['dest10'] = substr($row['TAREST'], 237, 3);
    
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'denom1', 'ordine', 'denom2', 'denom3', 'indi1', 'indi2', 'loca1', 'loca2', 'cap', 'prov', 'stre', 'data_val', 'dest');

    
    return $ar_fields;
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }, {
            header   : 'Denominazione',
            dataIndex: 'denom1',
            flex : 1
            }
            ,{
            header   : 'Denom. 2',
            dataIndex: 'denom2',
            flex : 1
            },  {
            header   : 'Denom 3',
            dataIndex: 'denom3',
            flex : 1
            },
             {
            header   : 'Indirizzo 1',
            dataIndex: 'indi1',
            flex : 1
            },
            {
            header   : 'Indirizzo 2',
            dataIndex: 'indi2',
            flex : 1
            },{
            header   : 'Localit&agrave; 1',
            dataIndex: 'loca1',
            flex : 1
            },{
            header   : 'Localit&agrave; 2',
            dataIndex: 'loca2',
            flex : 1
            },{
            header   : 'CAP',
            dataIndex: 'cap',
            width: 50
            },{
            header   : 'Prov.',
            dataIndex: 'prov',
            width: 40
            }/*,{
            header   : 'Stato/regione',
            dataIndex: 'stre',
            flex : 1
            }*/,{
            header   : 'Validit&agrave;',
            dataIndex: 'data_val',
            renderer: date_from_AS, width: 60
            },{
            header   : 'Destinazioni',
            dataIndex: 'dest',
            flex:1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "denom1" => trim($row->denom1),
        "denom2" => trim($row->denom2),
        "denom3" => trim($row->denom3),
        "ordine" => trim($row->ordine),
        "indi1" => trim($row->indi1),
        "indi2" => trim($row->indi2),
        "loca1" => trim($row->loca1),
        "loca2" => trim($row->loca2),
        "cap" => trim($row->cap),
        "prov" => trim($row->prov),
        "stre" => trim($row->stre),
        "data_val" => trim($row->data_val),
        "dest" => trim($row->dest),
        
    );
 
    return $ar_values;
    
}


function out_component_form($ar_values, $params){
    
    global $main_module;
    
    ob_start();
    
    ?>
  	{name: 'riga',
	xtype: 'combo',
	fieldLabel: 'Codice',
	forceSelection: true,								
	displayField: 'text',
	width : 250,
	valueField: 'id',
	value:  <?php echo j($ar_values['riga']); ?>,					
	emptyText: '- seleziona -',
   // anchor: '-15',
    allowBlank : false,
	store: {
		editable: false,
		autoDestroy: true,
	    fields: [{name:'id'}, {name:'text'}],
	    data: [								    
	       {id : '*SA', text : '[*SA] Sede amm.'},
     	   {id : '*SR', text : '[*SR] Storico'},
     	   {id : '*SR1', text : '[*SR1] Storico'},
     	   {id : '*CP', text : '[*CP] Cod.prec.'},
     	   {id : '*CS', text : '[*CS] '}
	    ]
	}
			
	 },
	  
       {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
    	  {xtype: 'textfield',
    	name: 'denom1',
    	fieldLabel: 'Denominazione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['denom1']); ?>
    	},{xtype: 'textfield',
    	name: 'denom2',
    	fieldLabel: 'Denominazione 2',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['denom2']); ?>
    	},{xtype: 'textfield',
    	name: 'denom3',
    	fieldLabel: 'Denominazione 3',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['denom3']); ?>
    	},{xtype: 'textfield',
    	name: 'indi1',
    	fieldLabel: 'Indirizzo 1',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['indi1']); ?>
    	},{xtype: 'textfield',
    	name: 'indi2',
    	fieldLabel: 'Indirizzo 2',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['indi2']); ?>
    	},
    	{xtype: 'textfield',
    	name: 'loca1',
    	fieldLabel: 'Localit&agrave; 1',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['loca1']); ?>
    	},{xtype: 'textfield',
    	name: 'loca2',
    	fieldLabel: 'Localit&agrave; 2',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['loca2']); ?>
    	}, {xtype: 'textfield',
    	name: 'cap',
    	fieldLabel: 'CAP',
    	maxLength : 10,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['cap']); ?>
    	},{xtype: 'textfield',
    	name: 'prov',
    	fieldLabel: 'Provincia',
    	maxLength : 2,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['prov']); ?>
    	}/*,{xtype: 'textfield',
    	name: 'stre',
    	fieldLabel: 'Stre',
    	maxLength : 10,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['stre']); ?>
    	}*/,{ xtype: 'datefield'
		   , startDay: 1 //lun.
		   , fieldLabel: 'Valido fino al'
		   , value: '<?php echo print_date($ar_values['data_val'], "%d/%m/%Y"); ?>'	
		   , name: 'data_val'
		   , format: 'd/m/Y'					   
		   , submitFormat: 'Ymd'
		   , allowBlank: false
		   , listeners: {
		       invalid: function (field, msg) {
		       Ext.Msg.alert('', msg);}
			}
		},{name: 'dest',
			xtype: 'combo',
			fieldLabel: 'Destinazioni',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['dest']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            width : 400,
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json(find_TA_sys('VUDE', null, $params->cliente, null, null, null, 0, '', 'Y', 'Y'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			}
	 
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values){
    
    $ar_ins = array();
    $ar_ins['TANR']  .= sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] .= $form_values->descrizione;
    $ar_ins['TADES2'] .= $form_values->denom1;
    $ar_ins['TAORDI'] .= $form_values->ordine;
    $ar_ins['TAREST'] = "";
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->denom2);
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->denom3);
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->indi1);
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->indi2);
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->loca1);
    $ar_ins['TAREST'] .= sprintf("%-30s", $form_values->loca2);
    $ar_ins['TAREST'] .= sprintf("%-10s", $form_values->cap);
    $ar_ins['TAREST'] .= sprintf("%-2s", $form_values->prov);
    $ar_ins['TAREST'] .= sprintf("%-10s", $form_values->stre);
    $ar_ins['TAREST'] .= sprintf("%-8s", $form_values->data_val);
    $ar_ins['TAREST'] .= sprintf("%-3s", $form_values->dest);
    
    return $ar_ins;
    
}