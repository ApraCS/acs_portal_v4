<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $ret = array();
    $row = $main_module->get_condizioni_commerciali_cliente(null, $m_params->prog, $m_params->scadute);
  
    echo acs_je($row);
    exit;
    
}


if ($_REQUEST['fn'] == 'open_form'){ ?>
{
 success:true,  
 items: [
  	{
				xtype: 'grid',
		        flex: 1,
		        loadMask: true,
		        title: 'Condizioni commerciali',
		        features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),
				tbar: new Ext.Toolbar({
    	            items:[ '->',
					{
						name: 'f_scadute',
						xtype: 'checkboxgroup',
						fieldLabel: 'Evidenzia scadute',
						labelAlign: 'right',
					   	allowBlank: true,
					   	items: [{								   	
					            xtype: 'checkbox'
						      , width: 150
						      , labelWidth: 170
					          , name: 'f_scadute' 
					          , boxLabel: ''
					          , inputValue: 'Y'		                          
					        }],
					    listeners: {
					    	change: function(checkbox, checked){
					    	       var value = checked.f_scadute;
					    	       if(typeof(checked.f_scadute) ==='undefined') 
					    	       		value = 'N';
								   m_grid = this.up('window').down('grid');
								   m_grid.store.proxy.extraParams.scadute = value
								   m_grid.store.load();			            						            			 	            					            					                    		
					    	}
					    }    														
					}				    	            
    	         ]            
				}),		    			
  				store: {
					xtype: 'store',
					groupField: 'd_tipo',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
			                    extraParams: {
										prog: <?php echo j($m_params->prog) ?>,
										scadute : 'N'
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['d_tipo','tipo', 'descrizione', 'val_ini' , 'val_fin', 'dettagli']							
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		  {text: 'Codice', width: 50, dataIndex: 'codice'}
		    		, {text: 'Descrizione', flex : 1, dataIndex: 'descrizione', 
		    		   renderer: function (value, metaData, rec){	
		    		       if(rec.get('caricata') != '')	    		
		    					metaData.tdCls += ' grassetto';
		    			   if(rec.get('scaduta') == 'Y'){
		    			    	metaData.tdCls += ' grassetto';
		    			    	return '<span style = "color: red;">'+ value +'</span>';
		    			   }
		    			
		    			return value;
		    						    				
		    			}}
		    	    , {header: 'Validit&agrave;',
                    columns: [
                      {header: 'Iniziale', dataIndex: 'val_ini', width: 60, renderer : date_from_AS},
                      {header: 'Finale', dataIndex: 'val_fin', width: 60, renderer : date_from_AS}
                   	 ]} 		
		    		, {text: '&nbsp;', width: 200, dataIndex: 'dettagli'}
				],
				enableSort: true
				
				
	        	, listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
	            			my_grid = iView;
	            
						  	
						  }
					  }
				}
				

			, buttons: [
					/*{
							xtype: 'button',
				            text: 'Nuova condizione', 
				            iconCls: 'icon-outbox-32', scale: 'large',
				            handler: function() {
				            
		            			my_grid = this.up('grid');
		            
								my_listeners_add_cc = {
		        					afterEditRecord: function(from_win){	
		        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
		        						my_grid.store.load();		        						
		        						from_win.close();
						        		}
				    				};					  

		            		 			 			acs_show_win_std('Nuova condizione commerciale', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
											{
								  				mode: 'CREATE',
								  				modan: 'ToDo',
								  				CCPROG: <?php echo j($all_params['rec_id']) ?>
								  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');
				            
				            }
				        }*/
				          
				    ]
											    
				    		
	 	}
  	
  	
  	
  	
  	]}
  	
<?php }?>  	