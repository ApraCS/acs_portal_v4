<?php
require_once "../../config.inc.php";

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'open_form'){
    
    $d_cli = "[".$m_params->cliente."] ". $main_module->decod_cliente($m_params->cliente);
    
    ?>


{"success":true, "items": [

    {
        xtype: 'form',
        cod_cli : '',
        autoScroll : true,
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        flex:1,
        frame: true,
        items: [
        
        <?php
        $sql = "SELECT RRN(TA) AS RRN, TAKEY1, TADESC
                FROM {$cfg_mod_Gest['file_tabelle']} TA
                WHERE TADT='{$id_ditta_default}' AND TATAID = 'SCCLI'";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
                
        while($row = db2_fetch_assoc($stmt)){
            $txt_bl = "[".trim($row['TAKEY1'])."] ".trim($row['TADESC']);
            $ha_schede = $main_module->has_scheda_cliente($m_params->cliente, $row['TAKEY1']);
        
            if ($ha_schede == FALSE)
                $img_com_name = "icon-tag_grey-16";
            else
                $img_com_name = "icon-tag-16";
            
                $text = "{$txt_bl} cliente {$d_cli}";
        ?>
        
		            {
				 	xtype: 'fieldcontainer',
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
						
						{
								xtype: 'button',
								text: '',
								itemId: 'f_bt_<?php echo $row['RRN']; ?>',
								margin : '0 5 0 0',
								iconCls: '<?php echo $img_com_name; ?>',
								scale: 'small',
								handler : function(){
								
								var form = this.up('form').getForm();
								var win = this.up('window');
							 
                                    acs_show_win_std(<?php echo j($text); ?>, 
    		    				    'acs_anag_cli_schede_aggiuntive.php?fn=open_tab', 
    		    					{cliente: <?php echo j($m_params->cliente); ?>, tipo_scheda: '<?php echo trim($row['TAKEY1']); ?>'}, 1200, 400,
    	        					null, 'icon-tag-16');
													
								} 
							},
						
						{
						name: 'f_text_<?php echo $row['RRN'] ?>',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: <?php echo j($txt_bl); ?>						
					  }
					
					
				]},
 		    	<?php }?>
 		    	 
					{
				 	xtype: 'fieldcontainer',
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					<?php 
					$sql = " SELECT COUNT(*) AS C_ROW
					         FROM {$cfg_mod_Gest['file_xspm']} TV
					         WHERE TVDT = '{$id_ditta_default}' AND TVCCON = '{$m_params->cliente}'
                             AND TVTPTB = 'C'";
					
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					$row = db2_fetch_assoc($stmt);
					
					if ($row['C_ROW'] > 0)
					    $img_com_name = "icon-tag-16";
				    else
				        $img_com_name = "icon-tag_grey-16";
					
					
					?>
 		    	, {
					xtype: 'button',
					margin : '0 5 0 0',
					iconCls: '<?php echo $img_com_name; ?>',
					scale: 'small',
					handler : function(){
					
					    acs_show_win_std(<?php echo j("[XSPM] Sconti variante cliente {$d_cli}"); ?>, 
    		    				    'acs_anag_cli_XSPM.php?fn=open_tab', 
    		    					{cliente: <?php echo j($m_params->cliente); ?>}, 1200, 600,
    	        					null, 'icon-tag-16');
					
					}
					
					},{
						name: 'f_text_fsc',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: '[XSPM] Sconti variante'						
					  }
					
					]},
					
					{
				 	xtype: 'fieldcontainer',
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					<?php 
					$sql = " SELECT COUNT(*) AS C_ROW
					         FROM {$cfg_mod_Gest['file_scala_premi']} SP
					         WHERE SPDT = '{$id_ditta_default}' AND SPCCON = '{$m_params->cliente}'";
					
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					$row = db2_fetch_assoc($stmt);
					
					if ($row['C_ROW'] > 0)
					    $img_com_name = "icon-tag-16";
				    else
				        $img_com_name = "icon-tag_grey-16";
					
					
					?>
 		    	, {
					xtype: 'button',
					margin : '0 5 0 0',
					iconCls: '<?php echo $img_com_name; ?>',
					scale: 'small',
					handler : function(){
					
					    acs_show_win_std(<?php echo j("[BUCF] Scala premi cliente {$d_cli}"); ?>, 
    		    				    'acs_anag_cli_BUCF.php?fn=open_panel', 
    		    					{cliente: <?php echo j($m_params->cliente); ?>}, 1300, 400,
    	        					null, 'icon-tag-16');
					
					}
					
					},{
						name: 'f_text_fsc',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: '[BUCF] Scala premi'						
					  }
					
					]} , 
					
					{
				 	xtype: 'fieldcontainer',
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
					
					<?php 
					$sql = " SELECT COUNT(*) AS C_ROW
					         FROM {$cfg_mod_Gest['file_xspm']} TV
					         WHERE TVDT = '{$id_ditta_default}' AND TVCCON = '{$m_params->cliente}'
                             AND TVTPTB = 'P'";
					
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					$row = db2_fetch_assoc($stmt);
					
					if ($row['C_ROW'] > 0)
					    $img_com_name = "icon-tag-16";
				    else
				        $img_com_name = "icon-tag_grey-16";
					
					
					?>
 		    	, {
					xtype: 'button',
					margin : '0 5 0 0',
					iconCls: '<?php echo $img_com_name; ?>',
					scale: 'small',
					handler : function(){
					
					    acs_show_win_std(<?php echo j("[XSPG] Sconti pagamenti cliente {$d_cli}"); ?>, 
    		    				    'acs_sconti_variante_riga.php?fn=open_tab', 
    		    					{cliente: <?php echo j($m_params->cliente); ?>}, 1200, 600,
    	        					null, 'icon-tag-16');
					
					}
					
					},{
						name: 'f_text_fsc',
						xtype: 'displayfield',
						fieldLabel: '',
					    anchor: '-15',					    
					    value: '[XSPG] Sconti pagamenti'						
					  }
					
					]}, 
	
 		    	
 		    ], listeners : {
 		    			
 		    
 		    },
 		     	       
		
 	 }
	
]}

<?php

exit;
}

