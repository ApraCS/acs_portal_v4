<?php

require_once("../../config.inc.php");
require_once("acs_panel_dichiarazione_intenti_include.php");

$main_module = new DeskGest();
$s = new Spedizioni();


//elenco nazioni combo
function get_ar_nazioni(){
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFNAZI, TFDNAZ
	FROM {$cfg_mod_Gest['dichiarazione_intenti']['file_testate']} TF
	WHERE TFTIDO='DI'";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['TFNAZI']), "text" => $r['TFDNAZ']);
	}
	return $ar;
}

function get_ar_tipo_documento(){
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFTPDO AS COD, TFDTPD AS DES
	FROM {$cfg_mod_Gest['dichiarazione_intenti']['file_testate']} TF
	WHERE TFTIDO='DI'
	GROUP BY TFTPDO, TFDTPD ORDER BY TFDTPD";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES'] . " [" . trim($r['COD'] . "]"));
	}
	return $ar;
}


if ($_REQUEST['fn'] == 'get_json_data'){

	$m_params = acs_m_params_json_decode();

	$ar=crea_ar_tree_dichiarazione_intenti($_REQUEST['node'], $m_params->open_request->form_values);

	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}

	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}


// ******************************************************************************************
// INVIO A GESTIONALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_send_to_gest'){
	$m_params = acs_m_params_json_decode();

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $id_ditta_default);
	$cl_p .= sprintf("%-3s", '*ST');

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31P1('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31P1', $libreria_predefinita_EXE, $cl_in, null, null);

		//$call_return = $tkObj->PgmCall('UTIBR31G5', $libreria_predefinita_EXE, '', null, null);
	}

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}
####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
	####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TFCCON, TFDCON
	FROM {$cfg_mod['dichiarazione_intenti']['file_testate']}
	WHERE UPPER(
	REPLACE(REPLACE(TFDCON, '.', ''), ' ', '')
	) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
					GROUP BY TFCCON, TFDCON
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array("cod"=> trim($row['TFCCON']),"descr"=> acs_u8e(trim($row['TFDCON'])));
	}

	echo acs_je($ret);

	exit;
}




if ($_REQUEST['fn'] == 'open_parameters'){
	?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "DICHIAR_INTENTI");  ?> 
					/*{
			            text: 'Aggiorna',
			            iconCls: 'icon-button_black_repeat_dx-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_dichiarazione_intenti.php?fn=exe_send_to_gest', 'panel_dichiarazione_intenti', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }, {
	            text: 'Rapporto<br>sincronizzazione',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
	         
							acs_show_panel_std('acs_panel_rapporto_sincronizzazione.php?fn=open_tab', 'panel_fatture_anticipo', {chiave: 'ANT_REPORT'});
							this.up('window').close();
			
	            }
	        },*/
	        
	         {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Report',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_dichiarazione_intenti_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        }, {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_dichiarazione_intenti.php?fn=open_tab', 'panel_dichiarazione_intenti', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }],   		            
		            
		            items: [
		         	  	{
           				 xtype: 'combo',
						name: 'f_cliente_cod',
						fieldLabel: 'Cliente',
						minChars: 2,			
           				 margin: "20 20 0 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		$cfg_mod = $main_module->get_cfg_mod();
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }, 
        
       {
							name: 'f_nazione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Nazione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_nazioni(), ""); ?>	
								    ] 
							}						 
						}
						
						, {
							name: 'f_tipo_documento',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Tipo dichiarazione di intenti',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_tipo_documento(), ""); ?>	
								    ] 
							}						 
						} 
						
						,{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "10 20 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "10 20 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
										 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}
	]
}		
	
		
<?php
	exit;
}



if ($_REQUEST['fn'] == 'open_tab'){
	$request = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	?>

{"success":true, "items": [

        {
            xtype: 'treepanel',
	        title: 'Dichiarazioni di intento',
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Interrogazioni dichiarazioni di intento</b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),             
			
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'codice', 'rif', 'data_in', 'data_fin', 'cod_ass', 'ditta', 
				    'tot_cod', 'TFTIMP', 'detratto', 'residuo', 'stato', 'liv', 'liv_cod_qtip'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
					
                        extraParams: {
                                    open_request: <?php echo acs_je($request) ?>
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData, 
								

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }       				
                    }

                }),
				

		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            flex: 3,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Nazione/Cliente/Dichiarazione/Fattura'
			        }, { 
			            dataIndex: 'codice',
			            header: 'Codice', 
			            flex: 1
			        },{ 
			            dataIndex: 'ditta',
			            header: 'Ditta',
			            align: 'right',  
			             width: 40
			        },{ 
			            dataIndex: 'data_in',
			            header: 'Data inizio', 
			            renderer: date_from_AS,
			            width: 80
			        },{ 
			            dataIndex: 'data_fin',
			            header: 'Data fine', 
			            renderer: date_from_AS,
			            width: 80
			        } ,{ 
			            dataIndex: 'rif',
			            header: 'Riferimento', 
			            flex: 1
			        }, { 
			            dataIndex: 'cod_ass',
			            header: 'Codice assogget.', 
			            flex: 1
			        }, { 
			            dataIndex: 'tot_cod',
			            header: ' Totale documento', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, { 
			            dataIndex: 'TFTIMP',
			            header: 'Totale imponibile', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, { 
			            dataIndex: 'detratto',
			            header: 'Importo detrazioni', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },  { 
			            dataIndex: 'residuo',
			            header: 'Residuo', 
			            flex: 1,
			            align: 'right', 
			           renderer: function (value, metaData, record, row, col, store, gridView){

                                   if (record.get('liv')== 'liv_2' && parseFloat(record.get('residuo')) <= 0){
                                    metaData.tdCls += ' sfondo_rosso';
                                    }
                            
                            return floatRenderer2(value);
                            
                             }
			        },{ 
			            dataIndex: 'stato',
			            header: 'Stato', 
			            align: 'center',
			            width: 80,
			            renderer: function(value, metaData, record){
			            
						    	if (record.get('stato') == 'C') 
						    	return '<img src=<?php echo img_path("icone/48x48/lock_grey.png") ?> width=18>';
						    	    	
								if (record.get('stato') == 'M') 
								return '<img src=<?php echo img_path("icone/48x48/lock.png") ?> width=18>';	
											
						    }
			        }
			     
	         ]	
	          , listeners:{
						beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                        load: function () {
                          Ext.getBody().unmask();
                        }      
	         
	         }	
	         
	         , viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }																				  			
	            
        }  

]
}

<?php exit; }
