<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    $nr['tades2'] = $row['TADES2']; 
    $nr['sosp'] = $row['TATP'];
    $nr['ns_pro'] = substr($row['TAREST'], 0, 20);
    $nr['vs_pro'] = substr($row['TAREST'], 20, 20);
    $nr['val_ini'] = substr($row['TAREST'], 40, 8);
    $nr['val_fin'] = substr($row['TAREST'], 48, 8);
    $nr['as_iva'] = substr($row['TAREST'], 65, 3);
    $desc_camp = get_TA_sys('CUAE', trim($nr['as_iva']));
    $nr['d_as_iva'] = "[".trim($nr['as_iva'])."] ".$desc_camp['text'];
    $nr['imp'] = substr($row['TAREST'], 98, 10);
    $nr['data_agg'] = substr($row['TAREST'], 128, 8);
    $nr['p_age'] = substr($row['TAREST'], 140, 25);
    $nr['data_p'] = substr($row['TAREST'], 165, 8);
    $nr['hm_agg'] = substr($row['TAREST'], 136, 4);
    $nr['tarest'] = $row['TAREST'];
    if(trim($nr['data_agg']) != '')
        $nr['ultimo_agg'] = print_date($nr['data_agg'])."-".print_ora($nr['hm_agg'], 4);
    
    
    
    return $nr;
    
}


function out_fields(){
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'ns_pro', 'vs_pro', 'val_ini', 'val_fin', 
                       'as_iva', 'd_as_iva', 'imp', 'data_agg', 'p_age', 'data_p', 'hm_agg', 'ultimo_agg', 'tarest');
    return $ar_fields;
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }, {
            header   : 'Ufficio iva',
            dataIndex: 'tades2',
            flex : 1
            }
            ,{
            header   : 'Ns protocollo',
            dataIndex: 'ns_pro',
            flex : 1
            },  {
            header   : 'Vs protocollo',
            dataIndex: 'vs_pro',
            flex : 1
            },
             {header: 'Validit&agrave;',
            	columns: [
              {header: 'Iniziale', dataIndex: 'val_ini', renderer: date_from_AS, width: 60, sortable: true}
             ,{header: 'Finale', dataIndex: 'val_fin', renderer: date_from_AS, width: 60, sortable: true}
         	 ]},{
            header   : 'Importo limite',
            dataIndex: 'imp',
            flex : 1,
            align: 'right', 
           renderer: function (value, metaData, record, row, col, store, gridView){
            	if (record.get('imp').trim() == '') return '';
		 		else return floatRenderer2(value);
		            }
            },{
            header   : 'Ultimo aggiornam.',
            dataIndex: 'ultimo_agg',
            flex : 1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
 
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "note" => trim($row->tades2),
        "ns_pro" => trim($row->ns_pro),
        "vs_pro" => trim($row->vs_pro),
        "val_ini" => trim($row->val_ini),
        "val_fin" => trim($row->val_fin),
        "as_iva" => trim($row->as_iva),
        "imp" => trim($row->imp),
        "data_agg" => trim($row->data_agg),
        "hm_agg" => trim($row->hm_agg),
        "p_age" => trim($row->p_age),
        "data_p" => trim($row->data_p),
        "tarest" => trim($row->tarest),
        "sosp" => trim($row->sosp),
        
    );
 
    return $ar_values;
    
}


function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		maxLength: 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
	   {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
       {xtype: 'textfield',
    	name: 'note',
    	fieldLabel: 'Ufficio iva',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['note']); ?>
    	},
    	{xtype: 'textfield',
    	name: 'ns_pro',
    	fieldLabel: 'Ns protocollo',
    	maxLength : 20,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['ns_pro']); ?>
    	},
    	{xtype: 'textfield',
    	name: 'vs_pro',
    	fieldLabel: 'Vs protocollo',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['vs_pro']); ?>
    	},
    	{ 
			xtype: 'fieldcontainer',
			flex:1,
			anchor: '-15',
			layout: { 	type: 'hbox',
					    pack: 'start',
					    align: 'stretch'},						
			items: [
			
			{
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Validit&agrave; iniziale'
	   , name: 'val_ini'
	   , format: 'd/m/Y'
	   , value: '<?php echo print_date($ar_values['val_ini'], "%d/%m/%Y"); ?>'							   
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
		}
		, anchor: '-15'
	   , flex :1.3
	   
	}, {
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Finale'
	   , labelAlign : 'right'
	   , labelWidth : 45
	   , name: 'val_fin'
	   , format: 'd/m/Y'
	   , value: '<?php echo print_date($ar_values['val_fin'], "%d/%m/%Y"); ?>'							   
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
		}
	   , anchor: '-15'
	   , flex :1
	}]}
    	,{name: 'as_iva',
			xtype: 'combo',
			fieldLabel: 'Assogget. IVA',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['as_iva']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            anchor: '-15',
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAE'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	 },{
		name: 'imp',
		xtype: 'numberfield', hideTrigger:true,
		fieldLabel: 'Importo limite', 
	    maxLength: 10, width: 200,
	    value:  <?php echo j($ar_values['imp']); ?>,					
	},
    { 
			xtype: 'fieldcontainer',
			flex:1,
			anchor: '-15',
			layout: { 	type: 'hbox',
					    pack: 'start',
					    align: 'stretch'},						
			items: [
			
			{
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Ultimo aggiorn.'
	   , name: 'data_agg'
	   , format: 'd/m/Y'
	   , value: '<?php echo print_date($ar_values['data_agg'], "%d/%m/%Y"); ?>'							   
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
		}
		, anchor: '-15'
	    , flex :1.3
	    , readOnly : true
	   
	}, 
	{
       xtype:'timefield'
	   , fieldLabel: '-'
	   , labelWidth : 10
	   , labelSeparator : ''
	   , margin : '0 0 0 7'
	   , value: '<?php echo print_ora($ar_values['hm_agg'], 4); ?>'		
	   , name: 'hm_agg'
	   , format: 'H:i'						   
	   , submitFormat: 'Hi'
	   , increment: 1
       , anchor:'96%'
       , readOnly : true
   }

]},
    {xtype: 'textfield',
    	name: 'p_age',
    	fieldLabel: 'Protocollo agenzia entrate',
    	maxLength : 25,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['p_age']); ?>
    },  
    { 
		xtype: 'fieldcontainer',
		flex:1,
		anchor: '-15',
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
		
	   {
	   xtype: 'datefield'
	   , startDay: 1 //lun.
	   , fieldLabel: 'Data protocollo agenzia entrate'
	   , name: 'data_p'
	   , format: 'd/m/Y'
	   , value: '<?php echo print_date($ar_values['data_p'], "%d/%m/%Y"); ?>'							   
	   , submitFormat: 'Ymd'
	   , allowBlank: true
	   , listeners: {
	       invalid: function (field, msg) {
	       Ext.Msg.alert('', msg);}
		}
	   , anchor: '-15'
	   , width: 260
	   
	}]}
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values, $tarest){
    
    $ar_ins = array();
    
    $m_table_config = array( 'TAREST' =>
        array(
            'ns_pro' 	=> array(
                "start" => 0,
                "len"   => 20,
                "riempi_con" => ""
            ),
            'vs_pro' 	=> array(
                "start" => 20,
                "len"   => 20,
                "riempi_con" => ""
            ),
            'val_ini' 	=> array(
                "start" => 40,
                "len"   => 8,
                "riempi_con" => ""
            ),
            'val_fin' 	=> array(
                "start" => 48,
                "len"   => 8,
                "riempi_con" => ""
            ),
            'as_iva' 	=> array(
                "start" => 65,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'imp' 	=> array(
                "start" => 98,
                "len"   => 10,
                "riempi_con" => ""
            ),
            'data_agg' 	=> array(
                "start" => 128,
                "len"   => 8,
                "riempi_con" => ""
            ),
            'hm_agg' 	=> array(
                "start" => 136,
                "len"   => 4,
                "riempi_con" => ""
            ),
            'p_age' 	=> array(
                "start" => 140,
                "len"   => 25,
                "riempi_con" => ""
            ),
            'data_p' 	=> array(
                "start" => 165,
                "len"   => 8,
                "riempi_con" => ""
            ),
        ));
    
    $tarest = genera_TAREST($m_table_config, $form_values, $tarest);
    $ar_ins['TAREST'] = $tarest;
    $ar_ins['TANR']  = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;
    $ar_ins['TADES2'] = $form_values->note;

    return $ar_ins;
    
}

function genera_TAREST($m_table_config, $values, $tarest){
    $value_attuale = $tarest;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
    //$new_value = "";
    foreach($m_table_config['TAREST'] as $k => $v){
        if(isset($values->$k)){
            $chars = $values->$k;
         if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
             $len = "%{$v['riempi_con']}{$v['len']}s";
        else{
            if(in_array($k, array('imp')))
                $len = "%{$v['len']}s";
            else
                $len = "%-{$v['len']}s";
        }     
            
            
        $chars = substr(sprintf($len, $chars), 0, $v['len']);
        $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
        $value_attuale = $new_value;
                    
        }
        
    }
        
    return $new_value;
}

