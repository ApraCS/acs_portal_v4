<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_inserisci_attav'){
	
	$codice= $m_params->form_values->f_codice;
	
	$descr= $m_params->form_values->f_descr;
	
	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'ATTAV';
	$ar_ins['TAKEY1'] 	= $codice;
	$ar_ins['TARIF1'] 	= 'CLI';
	$ar_ins['TADESC'] 	= utf8_decode($descr);
	$ar_ins['TARIF1'] 	= $m_params->form_values->f_rif;
	
	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();
	

	$sql = "INSERT INTO {$cfg_mod_Gest['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_inserisci_rilav'){

	$codice= $m_params->form_values->f_codice;

	$descr= $m_params->form_values->f_descr;
	
	$attav= $m_params->attav;
	
	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'RILAV';
	//////$ar_ins['TARIF1'] 	= 'CLI';
	$ar_ins['TAKEY1'] 	= $attav;
	$ar_ins['TAKEY2'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);
	
	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();

	$sql = "INSERT INTO {$cfg_mod_Gest['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_attav'){
    
	$mod_rows= $m_params->mod_rows;
	
	foreach ($mod_rows as $v){

		if(isset($v)){
			
			$ar_ins = array();
		
			$ar_ins['TAKEY1'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
		//	$ar_ins['TAPESO'] 	= $v->seq;
		   	$ar_ins['TARIF1'] 	= $v->rif;
			$ar_ins['TANAZI'] 	= $v->nazi;
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$cfg_mod_Gest['file_tabelle']} TA
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE RRN(TA) = '$v->rrn' ";
			
		
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
			
			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);
			
			//exit;

		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_rilav'){

	$mod_rows= $m_params->mod_rows;
	
	foreach ($mod_rows as $v){

		if(isset($v)){
			
			$ar_ins = array();
			
			$ar_ins['TAKEY2'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TAFG01'] 	= $v->write_ri;
			$ar_ins['TAFG02'] 	= $v->note_obbl;
			$ar_ins['TACOGE'] 	= $v->prog;
			$ar_ins['TAMAIL'] 	= $v->stati_ammessi;
			$ar_ins['TATELE']   = $v->msg_ri;
			$ar_ins['TAPESO'] 	= $v->seq;
				
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();
		
			$sql = "UPDATE {$cfg_mod_Gest['file_tabelle']} TA
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE RRN(TA) = '$v->rrn' ";
				
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
				
			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);			
		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'get_data_grid'){

    $sql_where = '';
    $sql_where .= sql_where_by_combo_value('TA.TARIF1', $m_params->sezione);
    
	
	$sql = "SELECT RRN (TA) AS RRN, TADESC, TAKEY1, TAPESO, TAFG01, TARIF1, TANAZI
	        FROM {$cfg_mod_Gest['file_tabelle']} TA
			WHERE TADT='$id_ditta_default' AND TATAID='ATTAV' {$sql_where} ";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {
		
		$row['codice'] = acs_u8e(trim($row['TAKEY1']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['f_ut'] = acs_u8e(trim($row['TAFG01']));
		$row['rif'] = acs_u8e(trim($row['TARIF1']));
		$row['seq'] = (int)$row['TAPESO'];
		$row['nazi'] = acs_u8e(trim($row['TANAZI']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));

		$data[] = $row;
	}

	echo acs_je($data);
	exit();
}


if ($_REQUEST['fn'] == 'get_data_grid_rilav'){
	
	$attav= $m_params->attav;

	$sql = "SELECT RRN(TA) AS RRN, TADESC, TAKEY2, TAFG01, TAFG02, TACOGE, TAPESO, TATELE, TAMAIL
	        FROM {$cfg_mod_Gest['file_tabelle']} TA
			WHERE TATAID='RILAV' AND TAKEY1='{$attav}' /* AND TARIF1 = 'CLI' */";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['codice'] = acs_u8e(trim($row['TAKEY2']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['write_ri'] = acs_u8e(trim($row['TAFG01']));
		$row['note_obbl'] = acs_u8e(trim($row['TAFG02']));
		$row['msg_ri'] = acs_u8e(trim($row['TATELE']));
		$row['prog'] = acs_u8e(trim($row['TACOGE']));
		$row['stati_ammessi'] = acs_u8e(trim($row['TAMAIL']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));
		$row['seq'] = (int)$row['TAPESO'];

		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}
 

 

if ($_REQUEST['fn'] == 'open_grid'){

?>

				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'To Do Entry',
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
		store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							        extraParams: <?php echo acs_je($m_params) ?>,
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'seq', 'codice', 'descr', 'rrn', 'f_ut', 'rif', 'nazi'
		        			]
		    			},
		    		
			        columns: [
			       		 {
			                header   : 'Seq',
			                dataIndex: 'seq', 
			                width     : 100,
			                editor: {
			                	xtype: 'textfield',
			                	allowBlank: true
			           	  	}
			             },			        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },  { 
			                header   : 'Riferimento',
			                dataIndex: 'rif', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },{ 
			                header   : 'TAFG01',
			                dataIndex: 'f_ut',
			                renderer: function (value, metaData, record, row, col, store, gridView){						
							
							if (record.get('f_ut') != ''){
    							if (record.get('f_ut') == 'C'){
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('ref. cliente') + '"';
    	    		 	        }else if(record.get('f_ut') == 'E'){
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('rec.cliente + ref. ordine') + '"';
    	    		 	        }else if(record.get('f_ut') == 'U'){
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Utente corrente') + '"';
    	    		 	        }else{
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('ref. ordine') + '"';
    	    		 	        }	    		 	        
	    		 	      }
	    		 	        return value;		    
						}
			             }
			             
					/* ,  { 
			                header   : 'TANAZI',
			                dataIndex: 'nazi',
			                tooltip: 'Per le traduzioni inserire il codice lingua', 
			                width: 50,
			                editor: {
			                xtype: 'textfield',
			                	allowBlank: true
			           		}
			             }	*/		             
			            
			         ],  
					
						listeners: {
						
						
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
				
			           voci_menu.push({
				         		text: 'Causali di rilascio',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Causali di rilascio [' + rec.get('codice') + '] ' + rec.get('descr'), '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_rilav', {attav: rec.get('codice')}, 900, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}
					
					}, 
					
					 viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
						{ xtype: 'hiddenfield', name: 'f_rif', value: <?php echo acs_je($m_params->sezione)?>},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
					            console.log(m_grid);
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_attav',
											        method     : 'POST',
								        			jsonData: {
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												      m_grid.getStore().load();
												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
					   ]
					}, '->' ,
			     
			     {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			                console.log(list_rows_modified);
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_attav',
									        method     : 'POST',
						        			jsonData: {
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php 

exit; 

}

if ($_REQUEST['fn'] == 'open_rilav'){
	
	$attav= $m_params->attav;

?>

{"success":true, "items": [

        {
        
        xtype: 'grid',
        multiSelect: true,
        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
			store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid_rilav',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										
										attav: '<?php echo $attav; ?>'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'codice', 'descr', 'write_ri', 'note_obbl', 'prog', 'rrn', 'seq', 'msg_ri', 'stati_ammessi'
		        			]
		    			},
		    		
			        columns: [
						{
			                header   : 'Seq',
			                dataIndex: 'seq', 
			                width     : 100,
			                editor: {
			                	xtype: 'textfield',
			                	allowBlank: true
			           	  	}
			             },			        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			           
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             },
			             { 
			                header   : 'Note<br/>obbl.',
			                dataIndex: 'note_obbl', 
			                width: 50,
			                editor: {
 			                	xtype: 'textfield',
			                	allowBlank: true
			           		},			    
				         	renderer: function(value, p, record){
	    			    			if (value=='Y'){ 
	    			    			 return '<input type="checkbox" checked="true" />';
	    			    			 }else{
	    			    			  return '<input type="checkbox"/>';
	    			    			 }
	    			    			}
			             },			             
			             { 
			                header   : 'Messaggio RI<br/>personalizzato',
			                dataIndex: 'msg_ri', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             },
			             { 
			                header   : 'Notifica<br/> su RI',
			                tooltip : 'Scrive in RI il messaggio RIL_ENTRY',
			                dataIndex: 'write_ri', 
			                width: 50,
			               editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 },
				         	renderer: function(value, p, record){
	    			    			if (record.get('write_ri')=='Y'){ 
	    			    			 return '<input type="checkbox" checked="true" />';
	    			    			 }else{
	    			    			  return '<input type="checkbox"/>';
	    			    			 }
	    			    			}	
			              
			             },
			             { 
			                header   : 'Call programma',
			                dataIndex: 'prog', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }/*, { 
			                header   : 'Stati ammessi',
			                tooltip: 'Elenco stati di partenza ammessi, separati dal carattere \'|\' (opzionale)',
			                dataIndex: 'stati_ammessi', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }*/
			            
			         ],  
					
		
					 viewConfig: {
			         getRowClass: function(record, index) {
			         return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_rilav',
											        method     : 'POST',
								        			jsonData: {
								        				attav: '<?php echo $attav ?>',
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												         m_grid.getStore().load();
								            		  
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
			     
			  
					   ]
					},  '->',
					   {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_rilav',
									        method     : 'POST',
						        			jsonData: {
						        			    attav: '<?php echo $attav ?>',
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }
					
					
					
					
					]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php

exit;

}?>
