<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn'] = $row['RRN'];
    $nr['riga'] = $row['TANR'];
    $nr['tadesc'] = $row['TADESC'];
    $nr['tades2'] = $row['TADES2']; 
    $nr['sosp'] = $row['TATP'];
    $nr['telefono'] = substr($row['TAREST'], 0, 20);
    $nr['email'] = substr($row['TAREST'], 40, 60);
    $nr['cliente'] = trim($row['TACOR1']);
    $destinazione = "";
    $nr['dest1'] = substr($row['TAREST'], 100, 3);
    if(trim($nr['dest1']) != ''){
        $desc_dest1 = get_TA_sys('VUDE', trim($nr['dest1']), $row['TACOR1']);
        $destinazione .= "[".substr($row['TAREST'], 100, 3)."] ".$desc_dest1['text'];
    }
    $nr['dest2'] = substr($row['TAREST'], 103, 3);
    if(trim($nr['dest2']) != ''){
        $desc_dest2 = get_TA_sys('VUDE', trim($nr['dest2']), $row['TACOR1']);
        $destinazione .=  "<br>[".substr($row['TAREST'], 100, 3)."] ".$desc_dest2['text'];
    }
    $nr['dest3'] = substr($row['TAREST'], 106, 3);
    if(trim($nr['dest3']) != ''){
        $desc_dest3 = get_TA_sys('VUDE', trim($nr['dest3']), $row['TACOR1']);
        $destinazione .= "<br>[".substr($row['TAREST'], 100, 3)."] ".$desc_dest3['text'];
    }
    $nr['dest4'] = substr($row['TAREST'], 109, 3);
    if(trim($nr['dest4']) != ''){
        $desc_dest4 = get_TA_sys('VUDE', trim($nr['dest4']), $row['TACOR1']);
        $destinazione .= "<br>[".substr($row['TAREST'], 100, 3)."] ".$desc_dest4['text'];
    }
    $nr['dest5'] = substr($row['TAREST'], 112, 3);
    if(trim($nr['dest5']) != ''){
        $desc_dest5 = get_TA_sys('VUDE', trim($nr['dest5']), $row['TACOR1']);
        $destinazione .= "<br>[".substr($row['TAREST'], 100, 3)."] ".$desc_dest5['text'];
    }
    
    $nr['destinazioni'] = $destinazione;
    
    $nr['tp_contatto'] = substr($row['TAREST'], 228, 2);
    
    return $nr;
    
}


function out_fields(){
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'telefono', 'email', 'tp_contatto', 
                       'dest1',  'dest2', 'dest3', 'dest4', 'dest5', 'destinazioni', 'cliente', 'tarest');
    return $ar_fields;
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }, 
            {
            header   : 'Telefono',
            dataIndex: 'telefono',
            flex : 1
            },  {
            header   : 'Email',
            dataIndex: 'email',
            flex : 1
            },{
            header   : 'Destinazioni',
            dataIndex: 'destinazioni',
            flex : 1
            }
            ,{
            header   : 'OV',
            dataIndex: 'tp_contatto',
            width : 40
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row, $cliente){
 
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        //"note" => trim($row->tades2),
        "telefono"    => trim($row->telefono),
        "email"       => trim($row->email),
        "tp_contatto" => trim($row->tp_contatto),
        "dest1"   => trim($row->dest1),
        "dest2"   => trim($row->dest2),
        "dest3"   => trim($row->dest3),
        "dest4"   => trim($row->dest4),
        "dest5"   => trim($row->dest5),
        "cliente" => $cliente,
        "tarest"  => trim($row->tarest),
        "sosp"    => trim($row->sosp),
        
    );
 
    return $ar_values;
    
}


function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		maxLength: 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
	   {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
       {xtype: 'textfield',
    	name: 'telefono',
    	fieldLabel: 'Telefono',
    	maxLength : 20,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['telefono']); ?>
    	},
    	{xtype: 'textfield',
    	name: 'email',
    	fieldLabel: 'Email',
    	maxLength : 60,
    	anchor: '-15',
    	regex: <?php echo regex_multiemail(); ?>,
    	value:  <?php echo j($ar_values['email']); ?>
    	},
    	<?php write_combo_std('dest1', 'Destinazione 1', $ar_values['dest1'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $ar_values['cliente'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "width: 330", "show_det" => 'Y') ) ?>,
    	<?php write_combo_std('dest2', 'Destinazione 2', $ar_values['dest2'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $ar_values['cliente'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "width: 330", "show_det" => 'Y') ) ?>,
    	<?php write_combo_std('dest3', 'Destinazione 3', $ar_values['dest3'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $ar_values['cliente'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "width: 330", "show_det" => 'Y') ) ?>,
    	<?php write_combo_std('dest4', 'Destinazione 4', $ar_values['dest4'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $ar_values['cliente'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "width: 330", "show_det" => 'Y') ) ?>,
    	<?php write_combo_std('dest5', 'Destinazione 5', $ar_values['dest5'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $ar_values['cliente'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "width: 330", "show_det" => 'Y') ) ?>,
    	
    	
		{
			xtype: 'checkboxgroup',
			fieldLabel: 'Operatore vendita',
			labelWidth : 120,
		   	allowBlank: true,
		   	items: [{								   	
		            xtype: 'checkbox'
		          , name: 'tp_contatto' 
		          , inputValue: 'OV'
		          <?php if($ar_values['tp_contatto'] == 'OV'){?>
		          , checked : true
		          <?php }?>
		           	                          
		        }]
		        
		     														
		}
    	
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values, $tarest){
    
    $ar_ins = array();
    
    $m_table_config = array( 'TAREST' =>
        array(
            'telefono' 	=> array(
                "start" => 0,
                "len"   => 20,
                "riempi_con" => ""
            ),
            'email' 	=> array(
                "start" => 40,
                "len"   => 60,
                "riempi_con" => ""
            ),
            'dest1' 	=> array(
                "start" => 100,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'dest2' 	=> array(
                "start" => 103,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'dest3' 	=> array(
                "start" => 106,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'dest4' 	=> array(
                "start" => 109,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'dest5' 	=> array(
                "start" => 112,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'tp_contatto' 	=> array(
                "start" => 228,
                "len"   => 2,
                "riempi_con" => ""
            )
            
        ));
    
    $tarest = genera_TAREST($m_table_config, $form_values, $tarest);
    $ar_ins['TAREST'] = $tarest;
    $ar_ins['TANR']  = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;

    return $ar_ins;
    
}

function genera_TAREST($m_table_config, $values, $tarest){
    $value_attuale = $tarest;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
    //$new_value = "";
    foreach($m_table_config['TAREST'] as $k => $v){
        if(isset($values->$k)){
            $chars = $values->$k;
         if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
              $len = "%{$v['riempi_con']}{$v['len']}s";
        else
            $len = "%-{$v['len']}s";
                    
        $chars = substr(sprintf($len, $chars), 0, $v['len']);
        $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
        $value_attuale = $new_value;
                    
        }
        
    }
        
    return $new_value;
}

