<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();


//recupero nome_ditta
function get_nome_ditta($codice_ditta){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT DITTA FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE CODICE_DITTA=? GROUP BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($codice_ditta));
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		return $r['DITTA'];
	}
	return '';
}



function sql_where_by_user(){
	global $conn, $cfg_mod_Spedizioni, $id_ditta_default, $auth;
	$sql_where = "";
	
	//in TATAID ho le eventuali ditte ammesse per un untente. Se non presnti posso mostrare tutto
	 //TADT = DITTA
	 //TATAID = ABIP
	 //TAKEY1 = UTDT (utente -> ditta)
	 //TAKEY2 = UTENTE
	 //TADES = ELENCO DITTE SEPARATE DA ","
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT=? AND TATAID='ABIP' AND TAKEY1='UTDT' AND TAKEY2=?";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $auth->get_user()));	
	$r = db2_fetch_assoc($stmt);
	
	if (strlen(trim($r['TADESC'])) > 0){
		$ar_ditte = explode(",", trim($r['TADESC']));
		$sql_where .= " AND D.CODICE_DITTA IN(" . sql_t_IN($ar_ditte) .") ";
	}

	return $sql_where;
}


function sql_where_by_request($m_params){
	$sql_where .= " AND NOME_ANALISI = " . sql_t($m_params->nome_analisi_1);
	$sql_where .= " AND NOME_ANALISI_CONFR = " . sql_t($m_params->nome_analisi_2);	
	if (count($m_params->f_ditta) > 0){
		$sql_where .= " AND CODICE_DITTA IN(" . sql_t_IN($m_params->f_ditta) .")";
	}
	return $sql_where;		
}


//recupero elenco date bilanci (ceal) per scelta in menu iniziale
function get_ar_ceal(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT NOME_ANALISI, NOME_ANALISI_CONFR FROM {$cfg_mod_Gest['abi']['file_conti']} GROUP BY NOME_ANALISI, NOME_ANALISI_CONFR ORDER BY NOME_ANALISI";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => implode("|", array(trim($r['NOME_ANALISI']), trim($r['NOME_ANALISI_CONFR']))), "text" => implode(' -> ', array($r['NOME_ANALISI'], $r['NOME_ANALISI_CONFR'])));
	}
	return $ar;
}


//recupero elenco ditte per scelta in menu iniziale
function get_ar_ditte(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT CODICE_DITTA, DITTA 
			 FROM {$cfg_mod_Gest['abi']['file_conti']} D
			 WHERE 1=1 " . sql_where_by_user() . "
			 GROUP BY CODICE_DITTA, DITTA ORDER BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['CODICE_DITTA']), "text" => $r['DITTA']);
	}
	return $ar;
}



// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_report'){
	include_once('acs_panel_abi_start_report.php');
	exit;
}




// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
	$sql_where = "";
	$sql_where .= sql_where_by_request(acs_m_params_json_decode());
	$sql_where .= sql_where_by_user();	
	
	//recuper la somma dei record 1001 (a cui fanno riferimento le percentuali)
	$sql = "SELECT SUM(IMPORTO_A) AS TOT_1001 FROM {$cfg_mod_Gest['abi']['file_conti']} D WHERE VOCE_ANALISI = '1001' {$sql_where}";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);	
	$r = db2_fetch_assoc($stmt);
	$tot_1001 = $r['TOT_1001'];

	
	$sql = "SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']} D WHERE 1=1 {$sql_where} ORDER BY ORDINAMENTO";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();	
	
	while ($r = db2_fetch_assoc($stmt)) {

		$id_voce = trim($r['ORDINAMENTO']);
		
		if (!isset($ar[$id_voce])){
			$ar_n = array();
			$ar_n['id_voce'] 	= $id_voce;
			$ar_n['cod_voce'] = trim($r['VOCE_ANALISI']);
			$ar_n['voce'] 	= trim(acs_u8e($r['DES_VOCE_ANALISI']));
			$ar_n['codice_voce'] = trim($r['VOCE_ANALISI']);			
			$ar_n['tipo_voce'] 	= trim($r['TIPO_VOCE']);
			$ar_n['ditta'] = array();		
			$ar[$id_voce] = $ar_n;						
		}
		
		//creo il record per la ditta
		$ar_voce_ditta = array();
		$ar_voce_ditta['IMPORTO_A'] = $r['IMPORTO_A'];
		$ar_voce_ditta['IMPORTO_B'] = $r['IMPORTO_B'];
		$ar_voce_ditta['IMPORTO_C'] = $r['IMPORTO_C'];
		$ar_voce_ditta['IMPORTO_D'] = $r['IMPORTO_D'];
		$ar_voce_ditta['IMPORTO_E'] = $r['IMPORTO_E'];
		$ar_voce_ditta['IMPORTO_F'] = $r['IMPORTO_F'];
		$ar_voce_ditta['IMPORTO_G'] = $r['IMPORTO_G'];
		$ar_voce_ditta['IMPORTO_H'] = $r['IMPORTO_H'];						
		$ar[$id_voce]['ditta'][trim($r['CODICE_DITTA'])] = $ar_voce_ditta;
		
		//aggiorno riga di totale
		$ar[$id_voce]['ditta']['TOT']['IMPORTO_A'] += $r['IMPORTO_A'];
		 
		if (isset($tot_1001) && $tot_1001 != 0)
			$ar[$id_voce]['ditta']['TOT']['IMPORTO_B'] = $ar[$id_voce]['ditta']['TOT']['IMPORTO_A'] / $tot_1001 * 100;
		

	} //while $row
	
	foreach($ar as $kar => $r)
		$ar_ret[] = array_values_recursive($ar[$kar]);
	
	
	
	echo acs_je(array("success" => true, "root" => $ar_ret));
	exit;
}



// ******************************************************************************************
// SCELTA DATA BILANCIO (CEAL....)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_scelta_ceal'){
?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "CEAL_SELECT");  ?>{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							if(form.isValid()){
														
								//se ha selezionato un'unica azienda apro direttamente abi_azienda
								var ditte_selected = form.findField('f_ditta').getValue();
								var analisi_selected = form.findField('f_ceal').getValue();
								if (!Ext.isEmpty(ditte_selected) && ditte_selected.length == 1){
									console.log('selezionata singola ditta');
									var analisi_ar = analisi_selected.split("|");
								    acs_show_panel_std('../desk_gest/acs_panel_abi_azienda.php?fn=open_tab', Ext.id(), {
								    	codice_ditta: ditte_selected[0],
								    	nome_analisi_1: analisi_ar[0],
								    	nome_analisi_2: analisi_ar[1]
								    });
								    this.up('window').close();
									return false;
								}	
										            	
								acs_show_panel_std('../desk_gest/acs_panel_abi_start.php?fn=open_tab', 'panel-abi-start', form.getValues());
								this.up('window').close();
								
							}
			            }
			         }],   		            
		            
		            items: [{
							name: 'f_ceal',
							xtype: 'combo',
							fieldLabel: 'Tipo di analisi',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: false,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_ceal(), ""); ?>	
								    ] 
							},
							
							//ricerca locale
							typeAhead: true,
        					queryMode: "local",                             
        					anyMatch: true, 		//disponibile solo dalla versione 4.2.1
        											//uso questo listerners per il filtro dei dati con anyMatch
							listeners   : {
								beforequery: function(record){  
								    record.query = new RegExp(record.query, 'ig');
        							record.forceAll = true;
								}
							}        					
							
						}, {
							name: 'f_ditta',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Ditta',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_ditte(), ""); ?>	
								    ] 
							}						 
						}]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
	
	$m_params = acs_m_params_json_decode();
	
	//decodifico nome_analisi e nome_analisi_confronto
	$analisi_ar = explode("|", $m_params->f_ceal);
	
	$sql_where = '';
	if (count($m_params->f_ditta) > 0)
		$sql_where .= " AND D.CODICE_DITTA IN(" . sql_t_IN($m_params->f_ditta) .")"; 
	
	$sql_where .= sql_where_by_user();
	
	//recupero elenco ditte (ogni ditta sara' in una colonna)
	$sql = "SELECT D.DITTA, D.CODICE_DITTA, R1001.IMPORTO_A 
			FROM {$cfg_mod_Gest['abi']['file_conti']} D
			  LEFT OUTER JOIN (SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']}) R1001
			    ON D.CODICE_DITTA = R1001.CODICE_DITTA
			   AND R1001.VOCE_ANALISI = '1001' 
	           AND R1001.NOME_ANALISI = " . sql_t($analisi_ar[0]) . "
			   AND R1001.NOME_ANALISI_CONFR = " . sql_t($analisi_ar[1]) . "
			WHERE 1=1 {$sql_where}
			GROUP BY D.CODICE_DITTA, D.DITTA, R1001.IMPORTO_A
			ORDER BY R1001.IMPORTO_A DESC";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar_ditte = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$ar_ditte[trim($r['CODICE_DITTA'])] = trim($r['DITTA']);
	}
	
	//aggiungo Totale
	$ar_ditte['TOT'] = 'TOTALE';
	
?>
{
 success:true, items: [
 

	{
		xtype: 'grid',
//		id: 'panel-abi-start',
		loadMask: true,
		title: 'ABI_Start',
		<?php echo make_tab_closable(); ?>,
	
        tbar: new Ext.Toolbar({
            items:['<b>Analisi riclassificazione dati di bilancio [Periodo: <?php echo $analisi_ar[0]; ?>, periodo di raffronto: <?php echo $analisi_ar[1]; ?>]</b>', '->'
            
	           	, {iconCls: 'tbar-x-tool x-tool-print', handler: function(event, toolEl, panel){
					//Ext.ux.grid.Printer.print(this.up('grid'));
					
						var submitForm = new Ext.form.FormPanel();
						submitForm.submit({
						        target: '_blank', 
	                        	standardSubmit: true,
	                        	method: 'POST',
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report',
						        params: {extraParams: Ext.JSON.encode(this.up('panel').getStore().proxy.extraParams)},
						        });					
					
					
	           	}}	            
            
            
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
         ]            
        }),  
		
	
		store: {
			xtype: 'store',
				
			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
				nome_analisi_1: <?php echo acs_je($analisi_ar[0]); ?>,
				nome_analisi_2: <?php echo acs_je($analisi_ar[1]); ?>,
				f_ditta: <?php echo acs_je($m_params->f_ditta); ?>
			}
			
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				, doRequest: personalizza_extraParams_to_jsonData						
			
			},
				
			fields: ['voce', 'tipo_voce', 'codice_voce']
						
		}, //store

		multiSelect: false,
		columnLines: false, //righe separazione cell in verticale
		enableSort: false, // disable sorting		
		columns: [		
			{header: 'Codice', dataIndex: 'codice_voce', width: 60 /*, locked: true */},
			{header: 'Voce', dataIndex: 'voce', flex: 1, minWidth: 200 /*, locked: true */},
			{header: '', dataIndex: 'spaces', flex: 1}			
			<?php foreach($ar_ditte as $kd => $d){ ?>
			
			 , {header: <?php echo acs_je($d); ?>, width: 180,
			    columns: [ 			
				 , {header: 'Importo', dataIndex: <?php echo acs_je($kd); ?>, width: 100, align: 'right',
				                renderer: function(value, metaData, record, row, col, store, gridView){
				                	if (Ext.isEmpty(record.raw.ditta[<?php echo acs_je($kd); ?>]) == false)
					                	return floatRenderer0(record.raw.ditta[<?php echo acs_je($kd); ?>].IMPORTO_A);
				                }			  
				 }
				 , {header: '%', dataIndex: <?php echo acs_je($kd); ?>, width: 70, align: 'right',
				                renderer: function(value, metaData, record, row, col, store, gridView){
				                	if (Ext.isEmpty(record.raw.ditta[<?php echo acs_je($kd); ?>]) == false)
					                	return floatRenderer2(record.raw.ditta[<?php echo acs_je($kd); ?>].IMPORTO_B) + '%';
				                }			  
				 }				 
				 
			  ]
			 }
			<?php } ?>
					
		],
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					console.log('celldblclick');
					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
				    //in col_name ho in pratico il codice ditta
				    acs_show_panel_std('acs_panel_abi_azienda.php?fn=open_tab', Ext.id(), {
				    	codice_ditta: col_name,
				    	nome_analisi_1: iView.store.proxy.extraParams.nome_analisi_1,
				    	nome_analisi_2: iView.store.proxy.extraParams.nome_analisi_2
				    });

				}
			},
			
 			afterrender: function (comp) {
 			 //comp.normalGrid.addListener('celldblclick', comp.onCellDblClick,this);
 			 //comp.lockedGrid.addListener('celldblclick', comp.onCellDblClick,this); 			 
 			}			         
			
			
		}
		
		
		, viewConfig: {
			forceFit: true,
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }   
		    }												    
		
		
		 
	} 
 
 
 
 ]
}
<?php exit; } ?>