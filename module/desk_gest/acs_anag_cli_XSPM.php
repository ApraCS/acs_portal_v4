<?php
require_once "../../config.inc.php";

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $ar_ins['TVDTUM'] = oggi_AS_date();
    $ar_ins['TVUSUM'] = substr($auth->get_user(), 0, 8);
    $ar_ins['TVDTGE'] = oggi_AS_date();
    $ar_ins['TVUSGE'] = substr($auth->get_user(), 0, 8);
    
    $ar_ins['TVLIST']  = $form_values->listino;
    $ar_ins['TVVALU']  = $form_values->valuta;
    
    if(strlen($form_values->sc1) > 0)
        $ar_ins['TVSC1']   = sql_f($form_values->sc1);
    if(strlen($form_values->sc2) > 0)
        $ar_ins['TVSC2']   = sql_f($form_values->sc2);
    if(strlen($form_values->sc3) > 0)
        $ar_ins['TVSC3']   = sql_f($form_values->sc3);
    if(strlen($form_values->sc4) > 0)
        $ar_ins['TVSC4']   = sql_f($form_values->sc4);
                    
    if(strlen($form_values->prov1) > 0)
        $ar_ins['TVPR1']   = sql_f($form_values->prov1);
    if(strlen($form_values->prov2) > 0)
        $ar_ins['TVPR2']   = sql_f($form_values->prov2);
                            
    if(!is_null($form_values->azz_sc))
        $ar_ins['TVAZZS']  = $form_values->azz_sc;
    if(!is_null($form_values->azz_pr))
        $ar_ins['TVAZZP']  = $form_values->azz_pr;
    if(strlen($form_values->val_ini_df) > 0)
        $ar_ins['TVDTVI']  = $form_values->val_ini_df;
    if(strlen($form_values->val_fin_df) > 0)
        $ar_ins['TVDTVF']  = $form_values->val_fin_df;
    if(!is_null($form_values->check_data))
        $ar_ins['TVCKDT']  = $form_values->check_data;
    if(!is_null($form_values->gr_doc))
        $ar_ins['TVGRDO']  = $form_values->gr_doc;
    if(!is_null($form_values->gr_doc2))
        $ar_ins['TVGRD2']  = $form_values->gr_doc2;
    if(!is_null($form_values->gr_doc3))
        $ar_ins['TVGRD3']  = $form_values->gr_doc3;
  //  if(strlen($form_values->var1) > 0)
        $ar_ins['TVVAR01'] = $form_values->var1;
  //  if(strlen($form_values->van1) > 0)
        $ar_ins['TVVAL01'] = $form_values->van1;
 //   if(strlen($form_values->var2) > 0)
        $ar_ins['TVVAR02'] = $form_values->var2;
  //  if(strlen($form_values->van2) > 0)
        $ar_ins['TVVAL02'] = $form_values->van2;
    // if(strlen($form_values->agente) > 0)
    if($m_params->type == 'A')
        $ar_ins['TVAGE1'] = $form_values->agente;
    if(!is_null($form_values->tipo1))
        $ar_ins['TVTIP01'] = $form_values->tipo1;
    if(!is_null($form_values->tipo2))
        $ar_ins['TVTIP02'] = $form_values->tipo2;
        
        //altro
    if(strlen($form_values->TVVALO) > 0)
        $ar_ins['TVVALO']   = sql_f($form_values->TVVALO);
  //  if(strlen($form_values->TVTPRZ) > 0)
        $ar_ins['TVTPRZ'] = $form_values->TVTPRZ;
   // if(strlen($form_values->TVTPRG) > 0)
        $ar_ins['TVTPRG'] = $form_values->TVTPRG;
    if(strlen($form_values->TVISMI) > 0)
        $ar_ins['TVISMI']   = sql_f($form_values->TVISMI);
    if(strlen($form_values->TVISMA) > 0)
        $ar_ins['TVISMA']   = sql_f($form_values->TVISMA);
    $ar_ins['TVTISE'] = $form_values->TVTISE;
    if(strlen($form_values->TVIOMI) > 0)
        $ar_ins['TVIOMI']   = sql_f($form_values->TVIOMI);
    if(strlen($form_values->TVIOMA) > 0)
        $ar_ins['TVIOMA']   = sql_f($form_values->TVIOMA);
  //  if(strlen($form_values->TVDIV1) > 0)
        $ar_ins['TVDIV1'] = $form_values->TVDIV1;
   // if(strlen($form_values->TVDIV2) > 0)
        $ar_ins['TVDIV2'] = $form_values->TVDIV2;
 //   if(strlen($form_values->TVDIV3) > 0)
        $ar_ins['TVDIV3'] = $form_values->TVDIV3;
    
        $ar_ins['TVARVA'] = $form_values->art_riga;
      
    $ar_ins["TVDT"]   = $id_ditta_default;
    if($m_params->cliente != ''){
        $ar_ins["TVCCON"] = $m_params->cliente;
        $ar_ins["TVTPTB"] = "C";
    }else{
        $ar_ins["TVTPTB"] = $m_params->type;
    }
    $ar_ins["TVPROG"] = $main_module->next_num('XSPM');
    
    $sql = "INSERT INTO {$cfg_mod_Gest['file_xspm']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $ar_ins['TVDTUM'] = oggi_AS_date();
    $ar_ins['TVUSUM'] = substr($auth->get_user(), 0, 8);
    
    $ar_ins['TVLIST']  = $form_values->listino;
    $ar_ins['TVVALU']  = $form_values->valuta;
    
    if(strlen($form_values->sc1) > 0)
        $ar_ins['TVSC1']   = sql_f($form_values->sc1);
    if(strlen($form_values->sc2) > 0)
        $ar_ins['TVSC2']   = sql_f($form_values->sc2);
    if(strlen($form_values->sc3) > 0)
        $ar_ins['TVSC3']   = sql_f($form_values->sc3);
    if(strlen($form_values->sc4) > 0)
        $ar_ins['TVSC4']   = sql_f($form_values->sc4);
    
    if(strlen($form_values->prov1) > 0)
        $ar_ins['TVPR1']   = sql_f($form_values->prov1);
    if(strlen($form_values->prov2) > 0)
        $ar_ins['TVPR2']   = sql_f($form_values->prov2);

   // if(strlen($form_values->azz_sc) > 0)
        $ar_ins['TVAZZS']  = $form_values->azz_sc;
   // if(strlen($form_values->azz_pr) > 0)
        $ar_ins['TVAZZP']  = $form_values->azz_pr;
    if(strlen($form_values->val_ini_df) > 0)
        $ar_ins['TVDTVI']  = $form_values->val_ini_df;
    if(strlen($form_values->val_fin_df) > 0)
        $ar_ins['TVDTVF']  = $form_values->val_fin_df;
   // if(strlen($form_values->check_data) > 0)
        $ar_ins['TVCKDT']  = $form_values->check_data;
  //  if(strlen($form_values->gr_doc) > 0)
        $ar_ins['TVGRDO']  = $form_values->gr_doc;
  //  if(strlen($form_values->gr_doc2) > 0)
        $ar_ins['TVGRD2']  = $form_values->gr_doc2;
 //   if(strlen($form_values->gr_doc3) > 0)
        $ar_ins['TVGRD3']  = $form_values->gr_doc3;
  //  if(strlen($form_values->var1) > 0)
        $ar_ins['TVVAR01'] = $form_values->var1;
 //   if(strlen($form_values->van1) > 0)
        $ar_ins['TVVAL01'] = $form_values->van1;
  //  if(strlen($form_values->var2) > 0)
        $ar_ins['TVVAR02'] = $form_values->var2;
 //   if(strlen($form_values->van2) > 0)
        $ar_ins['TVVAL02'] = $form_values->van2;
   // if(strlen($form_values->agente) > 0)
   if($m_params->type == 'A')
        $ar_ins['TVAGE1'] = $form_values->agente;
//   if(strlen($form_values->tipo1) > 0)
        $ar_ins['TVTIP01'] = $form_values->tipo1;
 //  if(strlen($form_values->tipo2) > 0)
        $ar_ins['TVTIP02'] = $form_values->tipo2;
        
        //altro
    if(strlen($form_values->TVVALO) > 0)
        $ar_ins['TVVALO']   = sql_f($form_values->TVVALO);
  //  if(strlen($form_values->TVTPRZ) > 0)
        $ar_ins['TVTPRZ'] = $form_values->TVTPRZ;
  //  if(strlen($form_values->TVTPRG) > 0)
        $ar_ins['TVTPRG'] = $form_values->TVTPRG;
    if(strlen($form_values->TVISMI) > 0)
        $ar_ins['TVISMI']   = sql_f($form_values->TVISMI);
    if(strlen($form_values->TVISMA) > 0)
        $ar_ins['TVISMA']   = sql_f($form_values->TVISMA);
    $ar_ins['TVTISE'] = $form_values->TVTISE;
    if(strlen($form_values->TVIOMI) > 0)
        $ar_ins['TVIOMI']   = sql_f($form_values->TVIOMI);
    if(strlen($form_values->TVIOMA) > 0)
        $ar_ins['TVIOMA']   = sql_f($form_values->TVIOMA);
  //  if(strlen($form_values->TVDIV1) > 0)
        $ar_ins['TVDIV1'] = $form_values->TVDIV1;
   // if(strlen($form_values->TVDIV2) > 0)
        $ar_ins['TVDIV2'] = $form_values->TVDIV2;
   // if(strlen($form_values->TVDIV3) > 0)
        $ar_ins['TVDIV3'] = $form_values->TVDIV3;
                
    $ar_ins['TVARVA'] = $form_values->art_riga;
                
    $sql = "UPDATE {$cfg_mod_Gest['file_xspm']} TV
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(TV) = '{$form_values->rrn}'";
    
   
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc_scheda'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->form_values->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_Gest['file_xspm']} TV WHERE RRN(TV) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    
    $sql_where = "";
    $cliente = $m_params->open_request->cliente;
    $type    = $m_params->open_request->type;
    
    if(strlen($cliente) > 0)
        $sql_where .= " AND TVTPTB = 'C' AND TVCCON = '{$cliente}'"; 
    
    if(strlen($type) > 0)
        $sql_where .= " AND TVTPTB = '{$type}'"; 
    
    
    $sql = " SELECT RRN(TV) AS RRN, TV.*,CF.CFRGS1 AS D_FOR
             FROM {$cfg_mod_Gest['file_xspm']} TV
             LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
             ON CF.CFDT = TV.TVDT AND CF.CFCD = TV.TVCCON
             WHERE TVDT = '{$id_ditta_default}' {$sql_where}";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = array();
        $nr = array_map('rtrim', $row);
        $nr['rrn'] = $row['RRN'];
        $nr['listino'] = trim($row['TVLIST']);
        $val_listino   = find_TA_sys('BITL', $nr['listino']);
        $nr['t_listino'] = "[".$nr['listino']."] ".trim($val_listino[0]['text']);
        
        $nr['valuta']  = trim($row['TVVALU']);
        $val_valuta   = find_TA_sys('VUVL', $nr['valuta']);
        $nr['t_valuta'] = "[".$nr['valuta']."] ".trim($val_valuta[0]['text']);
        
        $sconti = "";
        $nr['sc1'] = $row['TVSC1'];
        if(trim($nr['sc1']) != '0')
            $sconti .= n($nr['sc1'], 1)."%";
        $nr['sc2'] = $row['TVSC2'];
        if(trim($nr['sc2']) != '0')
            $sconti .= " + ".n($nr['sc2'], 1)."%";
        $nr['sc3'] = $row['TVSC3'];
        if(trim($nr['sc3']) != '0')
            $sconti .= " + ".n($nr['sc3'], 1)."%";
        $nr['sc4'] = $row['TVSC4'];
        if(trim($nr['sc4']) != '0')
            $sconti .= " + ".n($nr['sc4'], 1)."%";
        $nr['sconti'] = $sconti;
   
        $nr['azz_sc'] = trim($row['TVAZZS']);
        $nr['prov1']  = $row['TVPR1'];
        $nr['prov2']  = $row['TVPR2'];
        $prov = '';
        if(trim($nr['prov1']) != '0')
            $prov .= n($nr['prov1'], 1)."%";
        if(trim($nr['prov2']) != '0')
            $prov .= " + ".n($nr['prov2'], 1)."%";
        $nr['provv'] = $prov;
            
        $nr['azz_pr'] = trim($row['TVAZZP']);
        
        $nr['val_ini'] = trim($row['TVDTVI']);
        $nr['val_fin'] = trim($row['TVDTVF']);
        
        $nr['val_ini_df']  = print_date($row['TVDTVI']);
        $nr['val_fin_df']  = print_date($row['TVDTVF']);
        
        $nr['check_data'] = trim($row['TVCKDT']);
        $nr['cliente'] = trim($row['TVCCON']);
        if(trim($nr['cliente']) != ''){
            $d_cliente = $main_module->decod_cliente($nr['cliente']);
            $nr['d_cli']  = $d_cliente;
        }else
            $nr['d_cli']  = "";
        
        $nr['gr_doc'] = trim($row['TVGRDO']);
        $val_grdoc   = find_TA_sys('RDOC', $nr['gr_doc']);
        $nr['t_gr_doc'] = trim($val_grdoc[0]['text']);
        
        $nr['gr_doc2'] = trim($row['TVGRD2']);
        $val_grdoc2   = find_TA_sys('RDOC', $nr['gr_doc2']);
        $nr['t_gr_doc2'] = trim($val_grdoc2[0]['text']);
        
        $nr['gr_doc3'] = trim($row['TVGRD3']);
        $val_grdoc3   = find_TA_sys('RDOC', $nr['gr_doc3']);
        $nr['t_gr_doc3'] = trim($val_grdoc3[0]['text']);
        
        $nr['gruppo_documenti'] = "{$nr['gr_doc']} {$nr['gr_doc2']} {$nr['gr_doc3']}";
        $nr['t_gruppo_documenti'] = "{$nr['t_gr_doc']}<br>{$nr['t_gr_doc2']}<br>{$nr['t_gr_doc3']}";
        
        $nr['var1'] = trim($row['TVVAR01']);
        $val_var1   = find_TA_sys('PUVR', $nr['var1']);
      
        $nr['van1'] = trim($row['TVVAL01']);
        
        $nr['var2'] = trim($row['TVVAR02']);
        $val_var2   = find_TA_sys('PUVR', $nr['var2']);
       
        $nr['van2'] = trim($row['TVVAL02']);
        
        $val_van1   = find_TA_sys('PUVN', $nr['van1'], null, $nr['var1']);
        $val_van2   = find_TA_sys('PUVN', $nr['van2'], null, $nr['var2']);
        $nr['t_van1'] = trim($val_van1[0]['text']);
        $nr['t_van2'] = trim($val_van2[0]['text']);
        
        $nr['tipo1'] = trim($row['TVTIP01']);
        $tipo1 = find_TA_sys('PUTI', trim($row['TVTIP01']));
        
        $nr['tipo2'] = trim($row['TVTIP02']);
        $tipo2 = find_TA_sys('PUTI', trim($row['TVTIP02']));
        
        if(trim($nr['var1']) != '')
            $nr['out_v1'] = "[{$nr['var1']}] ".trim($val_var1[0]['text']). ", [{$nr['van1']}] ".$nr['t_van1'];
        if(trim($nr['var2']) != '')
            $nr['out_v2'] = "[{$nr['var2']}] ".trim($val_var2[0]['text']). ", [{$nr['van2']}] ".$nr['t_van2'];
        
        if(trim($nr['var1']) != '' || trim($nr['tipo1']) != '')
            $nr['variante'] = "{$nr['var1']}, {$nr['van1']}, {$nr['tipo1']}";
        if(trim($nr['var2']) != '' || trim($nr['tipo2']) != '')
            $nr['variante'] .= "<br> {$nr['var2']}, {$nr['van2']}, {$nr['tipo2']}";
        
        if($nr['tipo1'] != '')
            $nr['tipologia'] = "[".trim($row['TVTIP01'])."] ".$tipo1[0]['text'];
        if($nr['tipo2'] != '')
            $nr['tipologia'] .= "<br>[".trim($row['TVTIP02'])."] ".$tipo2[0]['text'];
        
        $nr['agente'] = trim($row['TVAGE1']);
        $val_agente   = find_TA_sys('CUAG', $nr['agente']);
        $nr['t_agente'] = "[".$nr['agente']."] ".trim($val_agente[0]['text']);
        $nr['art_riga'] = trim($row['TVARVA']);
        
        $ar[] = $nr;
        
    }
    
    echo acs_je($ar);
    exit;
}
if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
			xtype: 'panel',
			<?php if($m_params->cliente == ''){?>
				title: 'Sconti VA',
				<?php echo make_tab_closable(); ?>,
			<?php }?>
            layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
							{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            autoScroll: true,
            title: '',
            flex:0.7,
            items: [
						
				{
			xtype: 'grid',
			title: '',
			flex:0.75,
			autoScroll: true,
	        loadMask: true,
	        stateful: true,
	        stateId: 'seleziona-sconti-age',
	        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],	
	        store: {
			//xtype: 'store',
			autoLoad:true,

					proxy: {
					   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
					   method: 'POST',								
					   type: 'ajax',

				       actionMethods: {
				          read: 'POST'
				        },
				        
				        
				           extraParams: {
							 open_request: <?php echo acs_je($m_params) ?>
        				},
        				
        				doRequest: personalizza_extraParams_to_jsonData, 
			
					   reader: {
			            type: 'json',
						method: 'POST',						            
			            root: 'root'						            
			        }
				},
				
    			fields: [ 'rrn', 'listino', 'valuta', 'sconti', 'provv', 'sc1', 'sc2', 'sc3', 'sc4', 'azz_sc', 'val_ini', 'val_fin', 'val_ini_df', 'val_fin_df',
                          'check_data', 'cliente', 'd_cli', 'gr_doc', 'gr_doc2', 'gr_doc3', 'var1', 'van1', 'var2', 'van2', 't_var1', 't_var2', 
                          't_van1', 't_van2', 'variante', 't_gr_doc', 't_gr_doc2', 't_gr_doc3', 't_valuta', 't_listino', 'agente', 't_agente',
                          'tipo1', 'tipo2', 'tipologia', 'azz_pr', 'prov1', 'prov2', 'out_v1', 'out_v2', 'art_riga', 'TVDTGE', 'TVDTUM', 'TVUSGE', 'TVUSUM',
                          'gruppo_documenti', 't_gruppo_documenti', 'art_riga', 'TVVALO', 'TVTPRZ', 'TVTPRG',
                          'TVISMI', 'TVISMA', 'TVTISE', 'TVIOMI', 'TVIOMA', 'TVDIV1', 'TVDIV2', 'TVDIV3'
                        ]							
						
			}, //store
			
			      columns: [	
			       {
	                header   : 'Sconti',
	                dataIndex: 'sconti',
	                flex : 1,
	                },{
	                header   : 'Provv.',
	                dataIndex: 'provv',
	                flex : 0.5,
	                },{
	                header   : 'List.',
	                dataIndex: 'listino',
	                width : 40,
	                },
	                {
	                header   : 'Val.',
	                dataIndex: 'valuta',
	                width : 40,
	                },
	                {
                    header   : 'C',
                    dataIndex: 'check_data',
                    width: 30,
                    tooltip : 'Data controllo validit&agrave;'
                    },
	                {header: 'Validit&agrave;',
                	columns: [
                    {header: 'Iniziale', dataIndex: 'val_ini', renderer: date_from_AS, width: 60, sortable : true},
                	{header: 'Finale', dataIndex: 'val_fin', renderer: date_from_AS, width: 60, sortable : true}
                	]}
                
                   
                  ,{ header: 'Variante'
                  , dataIndex: 'variante'
                  , flex : 1
                  , filter: {type: 'string'}, filterable: true
                  
                  },
                   {
                    header   : 'GD',
                    dataIndex: 'gruppo_documenti',
                    width: 70,
                    renderer: function(value, metaData, record){
                         if(record.get('gr_doc').trim() != '')
            			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('t_gruppo_documenti')) + '\"';
                          
                          return value;   
            		  }
                    }
                 <?php if($m_params->type == 'A'){?>
                , {
	                header   : 'Ag.',
	                dataIndex: 'agente',
	                width : 40,
                   	renderer: function(value, metaData, record){
                     if(record.get('agente').trim() != '')
        			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('t_agente')) + '\"';
                      
                      return value;   
        		  	}
                  }
                  <?php }?>
          	  , {header: 'Immissione',
            	 columns: [
            	 {header: 'Data', dataIndex: 'TVDTGE', renderer: date_from_AS, width: 70, sortable : true,
            	 renderer: function(value, metaData, record){
            	         if (record.get('TVDTGE') != record.get('TVDTUM')) 
            	          metaData.tdCls += ' grassetto';
            	 
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TVDTUM')) + ', Utente ' +record.get('TVUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return date_from_AS(value);	
            	}
            	 
            	 }
            	 ,{header: 'Utente', dataIndex: 'TVUSGE', width: 70, sortable : true,
            	  renderer: function(value, metaData, record){
            	  
            	  		if (record.get('TVUSGE') != record.get('TVUSUM')) 
            	          metaData.tdCls += ' grassetto';
            	  
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TVDTUM')) + ', Utente ' +record.get('TVUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return value;	
            	} 
            	 }
            	 ]}
                 	 
	                     
	                
	                
	         ], listeners: {
	         
	         selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('form').up('panel').down('#dx_form');
		               form_dx.getForm().reset();
		               rec_index = selected[0].index;
		               form_dx.getForm().findField('rec_index').setValue(rec_index);
	                   acs_form_setValues(form_dx, selected[0].data);
	                   
	                   }
		          }
		   	 }
				     
		
		
		}
		]}, 
		
	
		    
		
		   {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Dettaglio sconti per variante/agente',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		             defaults:{ anchor: '-5' , labelWidth: 85 },
 		            frame: true,
 		            items: [
 		            
 		            {xtype: 'textfield', name: 'rrn', hidden : true}, 
				    {xtype: 'textfield', name: 'rec_index', hidden : true},
				    {xtype: 'textfield', name: 'cliente', hidden : true},
    			 {
                	xtype: 'tabpanel',
                	items: [
                	
                	{
                	xtype: 'panel',
                	title: 'Dettagli', 
                	autoScroll : true,
                	frame : true,
                	layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
                	//width: 165,
                	items: [
        			{
            		name: 'listino',
            		xtype: 'combo',
            		fieldLabel: 'Listino',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
               		allowBlank: false,	
               		queryMode: 'local',
            	    minChars: 1, 		
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [								    
            		     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BITL'), ''); ?>	
            		    ]
            		}
            		,listeners: { 
            	 		beforequery: function (record) {
                     		record.query = new RegExp(record.query, 'i');
                     		record.forceAll = true;
            			 }
            		 }
            		
                  },
            	 {
        		name: 'valuta',
        		xtype: 'combo',
        		fieldLabel: 'Valuta',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
           		allowBlank: false,	
           		queryMode: 'local',
        	    minChars: 1, 	
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [								    
        		     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('VUVL'), ''); ?>	
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
              
               <?php if($m_params->type == 'A'){?>
        		   {
        		      name: 'agente',
        		      xtype: 'combo',
        		      fieldLabel: 'Agente',
        		      forceSelection: true,
        		      displayField: 'text',
        		      valueField: 'id',
        		      emptyText: '- seleziona -',
        		      allowBlank: false,
        		      queryMode: 'local',
        		      minChars: 1,
                		store: {
                			editable: false,
                			autoDestroy: true,
                		    fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), ''); ?>	
                		    ]
                		}
                		,listeners: { 
                	 		beforequery: function (record) {
                         		record.query = new RegExp(record.query, 'i');
                         		record.forceAll = true;
                			 }
                		 }
                		
                      },
                      <?php }?>
              
              { 
        		xtype: 'fieldcontainer',
        		flex:1,
        		layout: { 	type: 'hbox',
        				    pack: 'start',
        				    align: 'stretch'},						
        		items: [
        		
            		{
                    xtype : 'numberfield',
            	 	name: 'sc1', 
                    fieldLabel: 'Sconto 1',
                    hideTrigger:true,
                    decimalPrecision : 3,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		flex:1,
            		//labelWidth : 85
                  },
                  {
                    xtype : 'numberfield',
            	 	name: 'sc2', 
            	 	labelAlign : 'right',
                    fieldLabel: 'Sconto 2',
                    labelWidth : 90,
                    hideTrigger:true,
                    decimalPrecision : 3,
                    keyNavEnabled : false,
            		mouseWheelEnabled : false,
            		flex:1,
                  }
        		]},
        		{ 
            		xtype: 'fieldcontainer',
            		flex:1,
            		layout: { 	type: 'hbox',
            				    pack: 'start',
            				    align: 'stretch'},						
            		items: [
        		
                		 {
                        xtype : 'numberfield',
                	 	name: 'sc3', 
                        fieldLabel: 'Sconto 3',
                        hideTrigger:true,
                        decimalPrecision : 3,
                        keyNavEnabled : false,
                		mouseWheelEnabled : false,
                		flex:1,
                		//labelWidth : 85
                      },
                        {
                        xtype : 'numberfield',
                	 	name: 'sc4', 
                        fieldLabel: 'Sconto 4',
                        hideTrigger:true,
                        decimalPrecision : 3,
                        labelAlign : 'right',
                        labelWidth : 90,
                        keyNavEnabled : false,
                		mouseWheelEnabled : false,
                		flex:1,
                      }
        		
        		]},
              { 
            		xtype: 'fieldcontainer',
            		flex:1,
            		layout: { 	type: 'hbox',
            				    pack: 'start',
            				    align: 'stretch'},						
            		items: [
        		
                		 {
                        xtype : 'numberfield',
                	 	name: 'prov1', 
                        fieldLabel: 'Provvigione 1',
                        hideTrigger:true,
                        decimalPrecision : 3,
                      //  labelWidth : 85,
                        keyNavEnabled : false,
                		mouseWheelEnabled : false,
                		flex:1,
                      },
                        {
                        xtype : 'numberfield',
                	 	name: 'prov2', 
                        fieldLabel: 'Provvigione 2',
                        hideTrigger:true,
                        decimalPrecision : 3,
                        labelAlign : 'right',
                        labelWidth : 90,
                        keyNavEnabled : false,
                		mouseWheelEnabled : false,
                		flex:1,
                      }
        		
        		]},
        		
        		{ 
        		xtype: 'fieldcontainer',
        		flex:1,
        		layout: { 	type: 'hbox',
        				    pack: 'start',
        				    align: 'stretch'},						
        		items: [
        		
            		{
            		name: 'azz_sc',
            		xtype: 'combo',
            		fieldLabel: 'Azzera sconti',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
               		//allowBlank: false,	
               		queryMode: 'local',
               		//labelWidth : 85,
            	    minChars: 1, 	
            	    flex : 1, 		
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [								    
            		     <?php echo acs_ar_to_select_json($main_module->find_TA_std('CCT02'), '', true, 'N', 'Y'); ?>	
            		    ]
            		}
            		,listeners: { 
            	 		beforequery: function (record) {
                     		record.query = new RegExp(record.query, 'i');
                     		record.forceAll = true;
            			 }
            		 }
        		
                  },{
            		name: 'azz_pr',
            		xtype: 'combo',
            		fieldLabel: 'Azzera prov.',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
               		//allowBlank: false,	
               		queryMode: 'local',
            	    minChars: 1, 	
            	  	labelWidth : 90,	
            	  	flex :1,
            	  	labelAlign : 'right',
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [								    
            		     <?php echo acs_ar_to_select_json($main_module->find_TA_std('CCT02'), '', true, 'N', 'Y'); ?>	
            		    ]
            		}
            		,listeners: { 
            	 		beforequery: function (record) {
                     		record.query = new RegExp(record.query, 'i');
                     		record.forceAll = true;
            			 }
            		 }
            		
                    }
        		
        		]},
              
               	{ 
        		xtype: 'fieldcontainer',
        		flex:1,
        		layout: { 	type: 'hbox',
        				    pack: 'start',
        				    align: 'stretch'},						
        		items: [
        		
        		{
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	  // , labelWidth : 85
        	   , fieldLabel: 'Validit&agrave; iniziale'
        	   , name: 'val_ini_df'
        	   , format: 'd/m/y'							   
        	   , submitFormat: 'Ymd'
        	   , allowBlank: false
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        	
        	   , flex :1.3
        	   
        	}, {
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	   , fieldLabel: 'Finale'
        	   , labelAlign : 'right'
        	   , labelWidth : 40
        	   , name: 'val_fin_df'
        	   , format: 'd/m/y'						   
        	   , submitFormat: 'Ymd'
        	   , allowBlank: false
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        	   
        	   , flex :0.95
        	}]},
        	 {
        		name: 'check_data',
        		xtype: 'combo',
        		fieldLabel: 'Check data',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
           		//allowBlank: false,	
           		queryMode: 'local',
        	    minChars: 1, 	
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [								    
        		     <?php echo acs_ar_to_select_json($main_module->find_TA_std('CCT08'), ''); ?>	
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
        	  {
        		name: 'gr_doc',
        		xtype: 'combo',
        		fieldLabel: 'Gruppo doc. 1',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -', 
           		queryMode: 'local',
        	    minChars: 1, 		
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [	
        		    
        		    <?php if($cfg_mod_Gest['gruppo_documenti_ob'] == 'Y')
        		              echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '',  true, 'N', 'Y'); 
               		      else
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '', true, 'N', 'Y'); 
               		 ?>	
               		
        		    							    
        		     
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
                {
        		name: 'gr_doc2',
        		xtype: 'combo',
        		fieldLabel: 'Gruppo doc. 2',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
        		queryMode: 'local',
        	    minChars: 1, 		
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [	
        		    
        		    <?php if($cfg_mod_Gest['gruppo_documenti_ob'] == 'Y')
        		        echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '',  true, 'N', 'Y'); 
               		      else
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '', true, 'N', 'Y'); 
               		 ?>	
               		
        		    							    
        		     
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },  {
        		name: 'gr_doc3',
        		xtype: 'combo',
        		fieldLabel: 'Gruppo doc. 3',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
        		queryMode: 'local',
        	    minChars: 1, 	
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [	
        		    
        		    <?php if($cfg_mod_Gest['gruppo_documenti_ob'] == 'Y')
        		        echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '',  true, 'N', 'Y'); 
               		      else
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '', true, 'N', 'Y'); 
               		 ?>	
               		
        		    							    
        		     
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
                 <?php		      
                  $proc = new ApiProc();
        		  echo $proc->get_json_response(
        		      extjs_combo_dom_ris(array(
        		          'initial_value' => array('dom_c'  => $m_params->var1,
        		                                   'ris_c'  => $m_params->van1,
        		                                   'tipo_c' => "",
        		                                   'out_d'  => $m_params->out_v1),
        		          'risposta_a_capo' => true,
        		          'label' =>'Variabile 1<br>Variante', 
        		          'file_TA' => 'sys',
        		          'tipo_opz_risposta' => array('output_domanda' => true,  
        	                 'tipo_cf' => '',
        		             'opzioni' => array(
        		                  array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
        		              )),
        		          'dom_cf'  => 'var1',  'ris_cf' => 'van1'))
        		  );
        		  echo ",";
        		  
        		  ?>
        		  
        		    { name: 'tipo1',
					  xtype: 'combo',
				      fieldLabel: 'Tipologia',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUTI'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 }
        		  
        		  
        		  <?php
        		  echo ",";
        		  echo $proc->get_json_response(
        		      extjs_combo_dom_ris(array(
        		          'initial_value' => array('dom_c' => $m_params->row->var2,
        		                                   'ris_c' => $m_params->row->van2,
        		                                   'out_d' => $m_params->row->out_v2),
        		          'risposta_a_capo' => true,
        		          'label' =>'Variabile 2<br>Variante',
        		          'file_TA' => 'sys',
        		          'tipo_opz_risposta' => array('output_domanda' => true, 
        		             'tipo_cf' => '',
        		              'opzioni' => array(
        		                  array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
        		              )
        		              ),
        		          'dom_cf'  => 'var2',  'ris_cf' => 'van2'))
        		      );
        		  echo ", ";
        		  ?>
        		  
        		   { name: 'tipo2',
					  xtype: 'combo',
				      fieldLabel: 'Tipologia',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUTI'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 }	
				 
				 
				 
				 
				 ]},
				 
				{
				xtype: 'panel',
				title: 'Articolo', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false , pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
			     {
            		name: 'art_riga',
            		xtype: 'textfield',
            		fieldLabel: 'Art. riga promo',
            		maxLength : 15,
                    width: 200
            		},
				   {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
						 <?php write_numberfield_std('TVVALO', 'Valore', $m_params->row->TVVALO, 0, array("flex_width" =>"width: 180")) ?>	
				       , <?php write_combo_std('TVTPRZ', 'Tipo valore', $m_params->row->TVTPRZ, acs_ar_to_select_json($main_module->find_TA_std('STPRZ', null, 'N', 'N', null, null, null, 'N', 'Y'), '',  true, 'N', 'Y'), array("labelWidth" => 70, 'labelAlign' => 'right')) ?>			
				      
						
					]}
					 , <?php write_combo_std('TVTPRG', 'Tipo riga', $m_params->row->TVTPRG, acs_ar_to_select_json(find_TA_sys('BTRD', null, null, null, null, null, 0, '', 'Y', 'Y'), '',  true, 'N', 'Y')) ?>	
					 , <?php write_combo_std('TVTISE', 'Tipo importo', $m_params->row->TVTISE, acs_ar_to_select_json($main_module->find_TA_std('STPIS', null, 'N', 'N', null, null, null, 'N', 'Y'), '',  true, 'N', 'Y')) ?>							
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
						 <?php write_numberfield_std('TVISMI', 'Imp. selez. min.', $m_params->row->TVISMI, null, array('flex_width' => "width: 170")); ?>	
				       , <?php write_numberfield_std('TVISMA', 'max', $m_params->row->TVISMA, null, array('flex_width' => "width: 120", "labelWidth" => 50, 'labelAlign' => 'right')) ?>	
					   
					]},
					
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
						 <?php write_numberfield_std('TVIOMI', 'Imp. ordine min.', $m_params->row->TVIOMI, null, array('flex_width' => "width: 170")) ?>	
				        , <?php write_numberfield_std('TVIOMA', 'max', $m_params->row->TVIOMA, null, array('flex_width' => "width: 120", "labelWidth" => 50, 'labelAlign' => 'right')) ?>	
						
					]},
					
					{
						xtype: 'fieldcontainer',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						items: [
						  <?php write_combo_std('TVDIV1', 'Divisione 1', $m_params->row->TVDIV1, acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), '',  true, 'N', 'Y')) ?>
						, <?php write_combo_std('TVDIV2', 'Divisione 2', $m_params->row->TVDIV2, acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), '',  true, 'N', 'Y')) ?>
						, <?php write_combo_std('TVDIV3', 'Divisione 3', $m_params->row->TVDIV3, acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), '',  true, 'N', 'Y')) ?>
						
					]}
				
				]}
				 
	]}
				 
				 
				 			
                	 ],
			
			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_scheda',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     }, '->',
                 {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				cliente : <?php echo j($m_params->cliente); ?>,
				        	type : <?php echo j($m_params->type); ?> 
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        form.getForm().reset();
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
									cliente : <?php echo j($m_params->cliente); ?>,
						        	type : <?php echo j($m_params->type); ?>  
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        form.getForm().reset();
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
		  
			 	 }
			 ],
					 
					
					
	}
	
]}


<?php

exit;
}





