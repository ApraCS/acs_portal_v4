<?php

function sql_where_params($form_ep){
    
    global $id_ditta_default, $cfg_mod_Gest;
    
    //AREA MANAGER  E AGENTE IN JOIN CC IN GET JASON DATA
    
    $sql_where = "";
    if(strlen($form_ep->f_cliente_cod) > 0){
        $cliente =  sprintf("%09d", $form_ep->f_cliente_cod);
        $sql_where.= sql_where_by_combo_value('GCCDCF', $form_ep->f_cliente_cod, 'LIKE');
    }
    
    $sql_where.= sql_where_by_combo_value('GCCDCF', $form_ep->f_cod, 'LIKE');
    $sql_where.= sql_where_by_combo_value('GCCDFI', $form_ep->f_cod_fi, 'LIKE');
    $sql_where.= sql_where_by_combo_value('GCTEL', $form_ep->f_tel, 'LIKE');
    $sql_where.= sql_where_by_combo_value('GCPIVA', $form_ep->f_piva, 'LIKE');
    $sql_where.= sql_where_by_combo_value('GCCITI', $form_ep->GCCITI);
    $sql_where.= sql_where_by_combo_value('GCNAZI', $form_ep->GCNAZI);
    $sql_where.= sql_where_by_combo_value('GCLOCA', $form_ep->GCLOCA);
    $sql_where.= sql_where_by_combo_value('GCPROV', $form_ep->GCPROV);
    $sql_where.= sql_where_by_combo_value('GCCIVI', $form_ep->GCCIVI);
    $sql_where.= sql_where_by_combo_value('GCSEL1', $form_ep->GCSEL1);
    $sql_where.= sql_where_by_combo_value('GCSEL2', $form_ep->GCSEL2);
    $sql_where.= sql_where_by_combo_value('GCSEL3', $form_ep->GCSEL3);
    $sql_where.= sql_where_by_combo_value('GCSEL4', $form_ep->GCSEL4);
    
    if(strlen($form_ep->f_email) > 0){
        $sql_where .= " AND (GCMAIL LIKE '%{$form_ep->f_email}%'
        OR GCMAI1 LIKE '%{$form_ep->f_email}%'
        OR GCMAI2 LIKE '%{$form_ep->f_email}%'
        OR GCMAI3 LIKE '%{$form_ep->f_email}%'
        OR GCMAI4 LIKE '%{$form_ep->f_email}%'
        OR GCPEC LIKE '%{$form_ep->f_email}%' )";
    }
    
    if ($form_ep->f_sospeso == 'Y')
        $sql_where.= " AND GCSOSP = 'S'";
    if ($form_ep->f_sospeso == 'N')
        $sql_where.= " AND GCSOSP <> 'S'";
            
            
    if (isset($form_ep->f_todo) && count($form_ep->f_todo) > 0){
        $where_as = sql_where_by_combo_value('ASCAAS' , $form_ep->f_todo);
        $sql_where .= " AND (GCCDCF IN (
        SELECT DISTINCT ASDOCU FROM {$cfg_mod_Gest['file_assegna_ord']}
        WHERE ASDT = '{$id_ditta_default}' {$where_as}
        AND ASFLRI <> 'Y'
        ))";
    }
            
    $where_des = "";
    ///if(count($form_ep->GCTPIN) > 0)
        $where_des .= sql_where_by_combo_value('GCTPIN', $form_ep->GCTPIN);
    ///if(count($form_ep->GCCRM1) > 0)
        $where_des .= sql_where_by_combo_value('GCCRM1', $form_ep->GCCRM1);
    ///if(count($form_ep->GCCRM2) > 0)
        $where_des .= sql_where_by_combo_value('GCCRM2', $form_ep->GCCRM2);
    if(strlen($where_des) > 0){
        $sql_where .= " AND (GCPROG IN (
        SELECT DISTINCT GCPRAB
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
        WHERE GCDT = '{$id_ditta_default}'
        {$where_des}) OR (1=1 {$where_des}))";
    }
                        
    return $sql_where;
                        
}

function get_pagamenti($params){
    
    global $cfg_mod_Gest, $id_ditta_default, $conn;
    $where = "";
    $ar = array();
    
    if(isset($params->gruppo) && strlen($params->gruppo) > 0)
        $where = " AND SUBSTRING(TAREST, 126, 3) IN ('{$params->gruppo}', 'X')";
    
    
    $sql = "SELECT TA.*
            FROM {$cfg_mod_Gest['file_tab_sys']} TA
            WHERE TADT='{$id_ditta_default}' AND TAID = 'CUCP' AND TALINV=''
            {$where}
            ORDER BY TATP, TADESC";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while ($row = db2_fetch_assoc($stmt)) {
        $r = array();
        $r['id'] 	= trim($row['TANR']);
        $r['text'] = "[".$r['id']."] ".trim($row['TADESC']);
        if(trim($row['TATP']) == 'S'){
            $r['sosp']  = true;
            $r['color']  = "#F9BFC1";
            
        }
        $ar[] = $r;
    }
    if(isset($params->gruppo) && strlen($params->gruppo) > 0)
        $ar[] = array('id'=>'ALL', 'text' => '<b><u>* * * MOSTRA TUTTO * * *</u></b>');
    
    return $ar;
    
}


function sql_where_params_comm($form_ep){
    $sql_where = "";
    
    if(strlen($form_ep->f_divisione) > 0)
        $sql_where .= sql_where_by_combo_value('CCDIVI', $form_ep->f_divisione);
    if(strlen($form_ep->f_codice) > 0)
        $sql_where .= " AND CCCCON = '{$form_ep->f_codice}'";
    if(strlen($form_ep->f_descrizione) > 0)
        $sql_where .= " AND CCDCON = '{$form_ep->f_descrizione}'";
    if(strlen($form_ep->val_ini) > 0)
        $sql_where .= " AND CCDTVI >= {$form_ep->val_ini}";
    if(strlen($form_ep->val_fin) > 0)
        $sql_where .= " AND CCDTVF <= {$form_ep->val_fin}";
    if(strlen($form_ep->f_scadute) > 0){
        $data_meno_mese = date('Ymd', strtotime(oggi_AS_date(). " -1 months"));
        $sql_where .= " AND CCDTVF >=  {$data_meno_mese} ";
     }
     
    return $sql_where;
}
