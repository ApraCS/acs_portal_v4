<?php

require_once("../../config.inc.php");
require_once("acs_panel_ins_new_anag.php");

$main_module = new DeskGest(array('abilita_su_modulo' => 'DESK_VEND'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data_cc'){

    $ret = array();

    $rowgc = get_gcrow_by_prog($m_params->CCPROG);
    
    $id_ditta_default_W = trim($id_ditta_default) . 'W';
    $row_gc_modan = get_gcrow_by_modan_default($rowgc['GCDEFA']);
    
    
    $sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID
            FROM {$cfg_mod_Gest['file_tabelle']} TA
            LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}  AN ON
            CCDT = TADT AND CCPROG = ? AND CCSEZI = 'BCCO' AND CCCCON = TAKEY2
            WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ANCCO' AND TAKEY1 = ?
            AND TAKEY2 <> 'STD'
            ORDER BY CCCCON
            ";
    
    $sqlD = "SELECT RRN(CC) AS RRN, CC.*
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
            WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->CCPROG, trim($rowgc['GCDEFA'])));
    echo db2_stmt_errormsg($stmt);
    
    while ($row = db2_fetch_assoc($stmt)) {
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        $stmtD = db2_prepare($conn, $sqlD);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmtD, array($id_ditta_default_W, $row_gc_modan['GCPROG'], 'BCCO', trim($row['TAKEY2'])));
        $r_def = db2_fetch_assoc($stmtD);
        
        
        if(trim($row['CCDIVI']) == '')
            $divisione = trim($r_def['CCDIVI']);
        else
            $divisione = trim($row['CCDIVI']);
        
        $cod_liv0 = $divisione; //divisione
        $cod_liv1 = trim($row['TAKEY2']); //codice cc
                
        //LIVELLO TOTALE
        $liv = $cod_liv0;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = $liv;
            $ar_new['task'] = $liv;
            if (strlen(trim($divisione)) > 0)
                $desc_divisione = get_TA_sys('DDOC', $divisione);
            else
                $desc_divisione = array('text' => '');
            $ar_new['descrizione'] = $desc_divisione['text'];
            $ar_new['liv'] = 'liv_0';            
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        
        //chiave documento
        $liv = $cod_liv1;
        $ar_r=&$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new= array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice'] = $row['TAKEY2'];
            $ar_new['task']= $row['TAKEY2'];
            $ar_new['descrizione'] = $row['TADESC'];
            $ar_new['liv'] = 'liv_1';
            $ar_new['prog'] = $m_params->CCPROG;
            $ar_new['GCDEFA'] = $rowgc['GCDEFA'];
            //$n['cdcf'] = $m_params->cdcf;
            $ar_new['ditta'] = $id_ditta_default;
            $ar_new['condizioni'] = 'BCCO';
            $ar_new['leaf'] = true;
            $ar_r[$liv]=$ar_new;
        }
        
        $ar_r=&$ar_r[$liv];
        
        
    }
    
    foreach($ar as $kar => $r){
        
        //parto con tree aperto se ho un solo record di primo livello
        if (count($ar) == 1) $ar[$kar]['expanded'] = true;
        
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}


if ($_REQUEST['fn'] == 'open_filtra_CC'){ ?>

{"success":true, "items": [
  	{
				xtype: 'treepanel',
		        flex: 1,
		        loadMask: true,
		        useArrows: true,
	            rootVisible: false,
			    store: Ext.create('Ext.data.TreeStore', {
					autoLoad:true,
			        proxy: {
							 type: 'ajax',
                             url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_cc',
							 extraParams: {
		                            CCPROG: <?php echo j($m_params->CCPROG) ?>
		        				}
		        			, doRequest: personalizza_extraParams_to_jsonData	
							//Add these two properties
							     , actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }    
						},
							
	        			fields: ['liv', 'task', 'codice', 'descrizione', 'prog', 'ditta', 'condizioni', 'val', 'scaduta']							
			
			 }),
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		  {text: 'Divisione/Codice', width: 150, dataIndex: 'task', xtype: 'treecolumn'}
		    		, {text: 'Descrizione', flex: 1, dataIndex: 'descrizione'}
		    		
				],
				enableSort: true
				, listeners: {
					afterrender: function (grid, eOpts) {
                        var groupingFeature = grid.getView().features[0];
                       // groupingFeature.collapseAll();
                           
                        },
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	var loc_win = this.up('window');
	            			var my_grid = iView;
	            
							my_listeners_add_cc = {
	        					afterEditRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						grid_id = '<?php echo $m_params->grid_id; ?>';				        						
				        			var grid_origine = Ext.getCmp(grid_id);
				        			grid_origine.getStore().load();
				        			from_win.close();
				        			
					        		}
			    				};					  

			    				//crea nuovo
		            			acs_show_win_std('Crea nuova condizione commerciale', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
									{
						  				mode: 'CREATE',
						  				modan: 'ToDo',
						  				g_paga : <?php echo j($m_params->g_paga); ?>,
						  				CCDT: rec.get('ditta'),
						  				CCPROG: rec.get('prog'),
						  				CCSEZI: rec.get('condizioni'),
						  				CCCCON: rec.get('codice'),
						  				CCDCON: rec.get('descrizione'),
						  				GCDEFA: rec.get('GCDEFA')
						  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');			    				
			    				loc_win.close();
			    				
						  	
						  }
					  }
					  
				}
				   
	         , viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           		    
			           return ret;																
			         }   
			    }
				


											    
				    		
	 	}
]}
<?php }


if ($_REQUEST['fn'] == 'open_form_filtri'){ ?>

   {"success":true, "items": [

        {
            xtype: 'form',
            flex: 1,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
        	frame: true,
            items: [
            <?php if($m_params->from_opm != 'Y'){?>
            	{xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
				items: [
				
			   	{
                    xtype: 'combo',
        			name: 'f_cliente_cod',
        			flex:1,
        			fieldLabel: 'Denominazione',
        			minChars: 0,			
                    margin: "5 20 5 10",
                    itemId : 'search_cli',
                    store: {
                    	pageSize: 1000,
                    	
        				proxy: {
        		            type: 'ajax',
        		            url : <?php
        		            		echo acs_je('acs_get_select_json.php?select=search_cli_anag');
        		            		?>,
        		            reader: {
        		                type: 'json',
        		                root: 'root',
        		                totalProperty: 'totalCount'
        		            },
        		              actionMethods: {
        							          read: 'POST'
        							        },
        		         
                		 doRequest: personalizza_extraParams_to_jsonData
        		        }, 
        		              
        				fields: ['cod', 'descr', 'out_loc', 'color', 'sosp'],		             	
                    },
                	valueField: 'cod',                        
                    displayField: 'descr',
                    typeAhead: false,
                    hideTrigger: true,
                    anchor: '100%',
                    
        	       listeners: {
                                    'change': function(field,newVal, b){
                                   
                                         combo_cli = this.up('form').down('#search_cli');   
                                         delete combo_cli.lastQuery;
                            			
                                    }
                                    
                                  },        
        
                    listConfig: {
                        loadingText: 'Searching...',
                        emptyText: 'Nessun cliente trovato',
                        
        
                        // Custom rendering template for each item
                        getInnerTpl: function() {
                            return '<div class="search-item" style="background-color:{color}">' +
                                '<h3><span>{descr}</span></h3>' +
                                '[{cod}] {out_loc}' + 
                            '</div>';
                        }               
                        
                    },
                    
                    pageSize: 1000
        
                }, 
        
                { 
        			xtype: 'textfield',
        			fieldLabel: 'Codice', 
        			name: 'f_cod',
        			margin : '5 0 5 10',
        			flex : 1,
        			labelWidth : 50,
        			labelAlign : 'right'
        			 }
			 
				]},
				
        		{
					xtype: 'fieldset',
	                title: 'Riferimenti diretti',
	                layout: 'anchor',
	         		items: [  
	         		
	         		{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				{
            			name: 'f_cod_fi',						
            			xtype: 'textfield',
            			fieldLabel: 'Codice fiscale',
            		  	flex : 1					
                		},{
            			name: 'f_piva',						
            			xtype: 'textfield',
            			fieldLabel: 'Partita iva',
            		  	labelAlign : 'right',
            		  	labelWidth : 70,
            		  	flex : 1					
                		}
        				
    				]},{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				{
            			name: 'f_tel',						
            			xtype: 'textfield',
            			fieldLabel: 'Telefono',	
            			flex : 1							
            		},{
            			name: 'f_email',						
            			xtype: 'textfield',
            			fieldLabel: 'Email',
            			labelAlign : 'right',
            		  	labelWidth : 70,
            		  	flex : 1									
            		}
        				
    				]},
    				{xtype: 'fieldcontainer',
    				flex:1,
    				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
    				items: [
    				 {
					name: 'f_sos',
					xtype: 'radiogroup',
					fieldLabel: 'Sospesi',
					flex:1,
					allowBlank: true,
				   	
				   	items: [{
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Solo'
                          , inputValue: 'Y'
                          , width: 45
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Inclusi'
                          , inputValue: 'T'
                          , width: 60   
						  , checked: true                       
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Esclusi'	
                          , inputValue: 'N'
                          , width: 60
                         
                          
                        }]
					}, {
                            fieldLabel: 'Condizioni per articolo/categoria',
                            name: 'f_cond',	
                            labelWidth: 200,
                            xtype: 'checkboxfield',
                            inputValue: 'Y',
                            flex:1,
                            margin: '0 0 0 150',         
                        },
	         		
        				
        			]}
    				 
	         	 
            	 
	             ]},
	             
	             <?php }?>
	             
	             <?php if($m_params->from_opm == 'Y'){?>
	             
	             {
					name: 'f_sos',
					xtype: 'radiogroup',
					margin : '0 0 0 10',
					fieldLabel: 'Sospesi',
					width : 400,
					allowBlank: true,
				   	
				   	items: [{
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Solo'
                          , inputValue: 'Y'
                          , width: 45
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Inclusi'
                          , inputValue: 'T'
                          , width: 60   
						  , checked: true                       
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Esclusi'	
                          , inputValue: 'N'
                          , width: 60
                         
                          
                        }]
					},
	             
	             <?php }?>
	             
	             {
					xtype: 'fieldset',
	                title: 'Raggruppamenti',
	                layout: 'anchor',
	         		items: [  
	         		
	         		{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				 <?php write_combo_std('CCAGE', 'Agente', '', acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>
        				,<?php write_combo_std('GCNAZI', 'Nazione', '', acs_ar_to_select_json(find_TA_sys('BNAZ', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true', "labelAlign" => "right", "labelWidth" => 70)) ?>
        				
    				]},
	         		
	         	 	{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				 {  xtype: 'combo',
    						name: 'CCARMA',
    						flex : 1.1,
    						fieldLabel: 'Area manager',
    						multiSelect : true,
    						margin : '0 17 0 0',
    						store: {
    								autoLoad: true,
    								editable: false,
    								autoDestroy: true,	 
    							    fields: [{name:'id'}, {name:'text'}, 'perc_provv'],
    							    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('tipo' => 'area_manager')), '', 'R'); ?>] 
    						},
    			            valueField: 'id',                       
    			            displayField: 'text',
    			            anchor: '100%',   										
        			        },
        			        {xtype: 'textfield', 
        			         name: 'GCLOCA', 
        			         flex: 1,
        			         fieldLabel: 'Localit&agrave;',
        			         labelAlign : 'right',
							 labelWidth : 52,
    						 },{										  
								xtype: 'displayfield',
								editable: false,
								fieldLabel: '',
								padding: '0 0 0 5',
							    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
								listeners: {
							            render: function( component ) {
							            
							            	m_form = this.up('form').getForm();
							            	component.getEl().on('dblclick', function( event, el ) {
											my_listeners_search_loc = {
					        					afterSelected: function(from_win, record_selected){
						        						m_form.findField('GCLOCA').setValue(record_selected.get('descr'));
						        						m_form.findField('GCPROV').setValue(record_selected.get('COD_PRO'));
						        						m_form.findField('GCNAZI').setValue(record_selected.get('COD_NAZ'));
						        						from_win.close();
									        		}										    
										    },
											acs_show_win_std('Seleziona localit&agrave;', 
												'../desk_gest/search_loc.php?fn=open_search', 
												{}, 450, 150, my_listeners_search_loc, 'icon-sub_blue_add-16');
											});										            
							             }
									}										    
							}
        				
    				]},
	         		{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				 <?php write_combo_std('GCCITI', 'Itinerario', '', acs_ar_to_select_json(find_TA_sys('BITI', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('multiSelect' => 'true')) ?>
        				,{  xtype : 'textfield',
							name: 'GCPROV', labelWidth: 70,
							flex : 1,
							fieldLabel: 'Provincia', labelAlign: 'right',
						    maxLength: 5
						}
        				
    				]},
    				{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				  <?php write_combo_std('GCTPIN', 'Tipo indirizzo', $r['GCTPIN'], acs_ar_to_select_json($main_module->find_TA_std('INDI', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('multiSelect' => 'true', 'transorm' => 'NO')) ?>	  
        				 ,<?php write_combo_std('GCCIVI', 'Ciclo di vita', $r['GCCIVI'], acs_ar_to_select_json(find_TA_sys('CIVI', null, null, null, null, null, 0, '', 'Y', 'Y'), ''),  array('multiSelect' => 'true', 'labelAlign' => 'right', "labelWidth" => 70)) ?>					
					]},
    				{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				  <?php write_combo_std('GCCRM1', 'Class. CRM1', $r['GCCRM1'], acs_ar_to_select_json(find_TA_sys('BCR1', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>	
						, <?php write_combo_std('GCCRM2', 'Class. CRM2', $r['GCCRM2'], acs_ar_to_select_json(find_TA_sys('BCR2', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'labelWidth' => 70, 'multiSelect' => 'true')) ?>	
					]},
    					{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				      <?php write_combo_std('GCSEL1', 'Classific.1', $r['GCSEL1'], acs_ar_to_select_json(find_TA_sys('CUS1', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>	
    						, <?php write_combo_std('GCSEL2', 'Classific.2', $r['GCSEL2'], acs_ar_to_select_json(find_TA_sys('CUS2', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true', 'labelWidth' => 70, 'labelAlign' => 'right')) ?>	
					]},
    				{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				  <?php write_combo_std('GCSEL3', 'Classific.3', $r['GCSEL3'], acs_ar_to_select_json(find_TA_sys('CUS3', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>	
    				    , <?php write_combo_std('GCSEL4', 'Classific.4', $r['GCSEL4'], acs_ar_to_select_json(find_TA_sys('CUS4', null, null, null, null, null, 0, '', 'Y', 'Y'), '', false), array('multiSelect' => 'true', 'labelWidth' => 70, 'labelAlign' => 'right', 'transorm' => 'NO')) ?>			
        			]},
        			
        			{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
                			{
        						name: 'f_todo',
        						xtype: 'combo',
        						flex : 1,
        						fieldLabel: 'To Do',
        						displayField: 'text',
        						valueField: 'id',
        						emptyText: '- seleziona -',
        						forceSelection: true,
        					   	allowBlank: true,		
        					   	multiSelect : true,											
        						store: {
        							autoLoad: true,
        							editable: false,
        							autoDestroy: true,	 
        						    fields: [{name:'id'}, {name:'text'}],
        						    data: [								    
        							     <?php
        							     echo acs_ar_to_select_json($main_module->find_TA_std('ATTAV', null, 'Y', 'N', null, null, 'CLI'), "");
        							       ?>	
        							    ] 
        						}						 
        					}
        				]
        			}
	         		
            	 
	             ]}
						
						            
            ],
            
            buttons: [
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "ANAG_CLI");  ?>
            <?php if($m_params->from_opm == 'Y'){?>
             {
	            text: 'Visualizza clienti inclusi',
	            iconCls: 'icon-gift-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            		
	            	if(form.isValid()){
	            		            	
	            		acs_show_win_std('Genera ordine marketing', 'acs_genera_ordine_marketing.php?fn=open_form', {
	            		form_values: form.getValues()}, 
	            		400, 250, null, 'icon-gift-16');		            
			        }
		            	
			           // this.up('window').destroy();
			                 	                	                
	            }
	        }
	        <?php }else{?>
	       
	      {
	         	xtype: 'splitbutton',
	            text: 'Utilit&agrave;',
            	iconCls: 'icon-database_active-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	 {
            	        xtype: 'button',
        	            text: 'Modifica agente/provvigioni in anagrafica',
        	            scale: 'large',
        	            handler: function() {
        	                acs_show_win_std('Modifica agente/provvigioni in ANAGRAFICA', 
							'acs_panel_ins_new_anag_utility.php?fn=open_age_prov', 
							{}, 600, 300, {}, 'icon-clienti-16');
				            
        			                 	                	                
        	            }
            	        },
            	        {
            	        xtype: 'button',
        	            text: 'Aggiorna agente/provvigioni su ordini aperti',
        	            scale: 'large',
        	            handler: function() {
        	            
            	            my_listeners = {
            	                  afterOkSave: function(from_win, list_ordini){
            	                  
                	                acs_show_win_std('Aggiorna agente/provvigioni su ordini aperti', 
        							'acs_panel_ins_new_anag_utility.php?fn=open_age_prov', 
        							{ord : 'Y', list_ordini : list_ordini}, 600, 300, {}, 'icon-clienti-16');
        							
        							from_win.destroy()
            	                  }
            	            };
        	                acs_show_win_std('Ricerca su archivio ordini NON EVASI', '../base/acs_seleziona_ordini.php', {
            					doc_wpi_search: 'UPDATE_agente_provvigioni',
     							show_parameters: true,
							}, 1000, 550, my_listeners, 'icon-clessidra-16');
        	                
				            
        			                 	                	                
        	            }
            	        }
		        	
		        	]}},   
               {
	            text: 'Crea nuovo',
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            		//per ora: apro elenco vuoto e poi dovra' riselezionare 'Crea nuova anagrafica'           	
	            	     acs_show_panel_std('<? echo ROOT_PATH ?>module/desk_gest/acs_panel_ins_new_anag.php?fn=open_tab', null, {
        		            	form_open: {f_cliente_cod: -1}, 
        		                recenti : 200
    		            	}, null, null);
    		            
		            	
			            this.up('window').destroy();
			                 	                	                
	            }
	        }, {
	            text: 'Recenti [50]',
	            iconCls: 'icon-news-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){	            	
	            	     acs_show_panel_std('<? echo ROOT_PATH ?>module/desk_gest/acs_panel_ins_new_anag.php?fn=open_tab', null, {
        		            	form_open: form.getValues(), 
        		                recenti : 50
    		            	}, null, null);
    		            
		            	
			            this.up('window').hide();
			        }            	                	                
	            }
	        },	{
	            text: 'Grafici',
	            iconCls: 'icon-grafici-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){
	            		            	
	            		acs_show_win_std('Anagrafica articoli', 'acs_panel_ins_new_anag_grafici.php?fn=open', {
	            		form_open: form.getValues()}, 
	            		800, 400, null, 'icon-grafici-16', 'Y');		            
			        }
	                      	                	                
	            }
	            }, {
	         	xtype: 'splitbutton',
	            text: 'Reports',
            	iconCls: 'icon-database_active-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
					 {
	            xtype : 'button',
	            text: 'Riepilogo scala premi',
	            scale: 'large',
	            handler: function() {
	            
                   acs_show_win_std('Filtri report', 'acs_panel_ins_new_anag_report_bucf.php?fn=open_form', {}, 440, 200, null, 'icon-print-16');    	                	     
    	         }
	            } , {
				xtype : 'button',	
	            text: 'Report anagrafica clienti',
	            scale: 'large',
	            handler: function() {
	               var form = this.up('form').getForm();
                   acs_show_win_std('Filtri report', 'acs_panel_ins_new_anag_report.php?fn=open_form', {form_open: form.getValues()}, 440, 200, null, 'icon-print-16');    	                	     
    	         }
	            } 
		       ]}},{
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){	            	
            			acs_show_panel_std('acs_panel_ins_new_anag.php?fn=open_tab', null, {
    		            	form_open: form.getValues()
    		           }, null, null);
    		            
		            	this.up('window').close();
			        }            	                	                
	            }
	        }
	        <?php }?>
            ]
            } 
    
    ]}
    
 <?php 
 
 exit;
}

if ($_REQUEST['fn'] == 'open_filtri_comm'){ ?>

{"success":true, "items": [
  {
        xtype: 'form',
        flex: 1,
       // layout : 'fit',
        autoScroll : true,
    	frame: true,
        items: [
        
        {
		name: 'f_divisione',
		xtype: 'combo',
		fieldLabel: 'Divisione',
		displayField: 'text',
		valueField: 'id',
		labelWidth : 80,
		emptyText: '- seleziona -',
		anchor: '-15',
		forceSelection:true,
	   	allowBlank: false,														
		store: {
			autoLoad: true,
			editable: false,
			autoDestroy: true,	 
		    fields: [{name:'id'}, {name:'text'}],
	      	data: [	 <?php  echo acs_ar_to_select_json($main_module->find_sys_TA('DDOC'), '');  ?>	
				  ] 
			}						 
		},
		
		{ 
			xtype: 'fieldcontainer',
			flex:1,
			layout: { 	type: 'hbox',
					    pack: 'start',
					    align: 'stretch'},						
			items: [
			
			   {
			   xtype: 'datefield'
			   , startDay: 1 //lun.
			   , fieldLabel: 'Validit&agrave; dal'
			   , labelWidth : 80			
			   , name: 'val_ini'
			   , width : 210			
			   , format: 'd/m/y'
			   , submitFormat: 'Ymd'
			   , allowBlank: true
			   , anchor: '-15'
			   , listeners: {
			       invalid: function (field, msg) {
			       Ext.Msg.alert('', msg);}
					}
			},  {
			   xtype: 'datefield'
			   , startDay: 1 //lun.
			   , fieldLabel: 'al'
			   , labelAlign : 'right'
    		   , width : 150
    		   , labelWidth : 20
			   , name: 'val_fin'
			   , format: 'd/m/y'
			   , submitFormat: 'Ymd'
			   , allowBlank: true
			   , anchor: '-15'
			   , listeners: {
			       invalid: function (field, msg) {
			       Ext.Msg.alert('', msg);}
					}
			}
						   
        
        
        ]},
            
            { 
    			xtype: 'textfield',
    			fieldLabel: 'Codice', 
    			name: 'f_codice',
    			width : 120,
    			//anchor: '-15',
    			labelWidth : 80,
    	    },
        			      
            { 
    			xtype: 'textfield',
    			fieldLabel: 'Descrizione', 
    			name: 'f_descrizione',
    			flex : 1,
    			anchor: '-15',
    			labelWidth : 80,
        	 },
        	 {
				name: 'f_scadute',
				xtype: 'checkboxgroup',
				fieldLabel: 'Escludi scadute(30 gg)',
				allowBlank: true,
				labelWidth: 150,
			   	items: [{								   	
			            xtype: 'checkbox'
				      , name: 'f_scadute' 
			          , boxLabel: ''
			          , inputValue: 'Y'
			          , checked : true
			          		                          
			        }]
			  														
			}

        ],
          buttons: [
			
             {
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	             var form = this.up('form').getForm();
	             var loc_win = this.up('window');
	             loc_win.fireEvent('afterFilter', loc_win, form.getValues());
			                 	                	                
	            }
	        }
	      ]
        }
]}
<?php }?>