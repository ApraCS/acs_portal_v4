<?php

require_once "../../config.inc.php";
ini_set('max_execution_time', 300);

$s = new Spedizioni();
$main_module = new Spedizioni();

	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);

	
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div#my_content h1{font-size: 22px; padding: 5px;}
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}
	   

   /*tr.liv_totale td{background-color: #cccccc; font-weight: bold;}*/
   tr.liv_totale td{font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
      
	/* ANALISI BILANCIO */
	TR.d_elenco th.cod_voce {font-size: 13px; font-weight: bold;}
	TR.d_elenco th.des_voce {font-size: 13px; font-weight: normal;}
    TR.d_elenco td.D{background-color: #C0C0C0; padding-top: 3px; padding-bottom: 3px; }
	TR.d_elenco td.S1{background-color: #99CCCC; padding-top: 3px; padding-bottom: 3px; }
	TR.d_elenco td.S2{background-color: #99B3CC; font-weight: bold; padding-top: 4px; padding-bottom: 4px;}
	TR.dett .tdl{background-color: #FFFFE0; font-weight: normal;}
	TR td.tot{font-weight: bold;}      
    table.int1 th {font-weight: bold;}  
      
    table.int1 tr.h_title td {
    	font-size: 9px;   
    }  
  </style>

  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {

	//apertura automatica finestra invio email
	<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
		Ext.get('p_send_email').dom.click();
	<?php } ?>
	
	});    
    

  </script>


 </head>
 <body>
 

 <?php
 //valori da tree/grid
 $extraParamsValueConverted =	strtr($_REQUEST['extraParams'], array('\"' => '"'));
 $extraParamsValue = (object)json_decode($extraParamsValueConverted);
 ?>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";

?>
			<span>
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report_elenco" method="post">
				   <input type="hidden" name="extraParams" value="<?php echo htmlspecialchars($_REQUEST['extraParams']); ?>" />
				   <button name="field_group_in_page" value="f_centro_di_costo" type="submit">Per Centro di costo</button>
				   <button name="field_group_in_page" value="f_agente" type="submit">Per Agente</button>
				   <button name="field_group_in_page" value="f_nazione" type="submit">Per Nazione</button>
				   <button name="field_group_in_page" value="f_nielsen" type="submit">Per Area Nielsen</button>				   
				</form>
			</span>
</div>


<?php

//recupero dati

//QUERY SU CONTI
$sql_where = " AND NOME_ANALISI = " . sql_t($extraParamsValue->open_tab_filters->nome_analisi_1);
$sql_where .= " AND NOME_ANALISI_CONFR = " . sql_t($extraParamsValue->open_tab_filters->nome_analisi_2);
$sql_where .= " AND CODICE_DITTA = " . sql_t($extraParamsValue->open_tab_filters->codice_ditta);


$sql = "SELECT C.*, 0 AS IMPORTO_A, 0 AS IMPORTO_B, 0 AS IMPORTO_C, 0 AS IMPORTO_D, 0 AS IMPORTO_E 
	 FROM {$cfg_mod_Gest['abi']['file_conti']} C WHERE 1=1 {$sql_where} ORDER BY ORDINAMENTO";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$ar = array();
$ar_voci = array();

while ($r = db2_fetch_assoc($stmt)) {
	$ar_voci[trim($r['VOCE_ANALISI'])] = array(
			'TIPO_VOCE' => trim($r['TIPO_VOCE']),
			'DES_VOCE'  => trim($r['DES_VOCE_ANALISI'])
	); 
}


$ar_mov = array();



	//QUERY SU MOVIMENTI
	$sql_params = array(); 
	$sql_mov_where = " AND M.NOME_ANALISI IN (" . sql_t($extraParamsValue->open_tab_filters->nome_analisi_1) . ", " . sql_t($extraParamsValue->open_tab_filters->nome_analisi_2) . ")";
	$sql_mov_where .= " AND M.CODICE_DITTA = " . sql_t($extraParamsValue->open_tab_filters->codice_ditta);
	
	$sql_mov_where .= sql_where_by_request($extraParamsValue, 'Y', 'Y', 'M.');
	
	$sql_move_where .= sql_where_by_user();	
	
	if (!isset($extraParamsValue->field_group))
		$extraParamsValue->field_group = 'f_centro_di_costo';
	
	if (isset($_REQUEST['field_group_in_page']))
		$extraParamsValue->field_group = $_REQUEST['field_group_in_page'];
	
	list($cod_field, $des_field) = get_field_for_group($extraParamsValue->field_group);	
	
	$sql_mov = "SELECT M.{$cod_field} AS COD, M.{$des_field} AS DESCR
					,TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05, 
			M.VOCE_ANALISI, M.NOME_ANALISI, SUM(M.IMPORTO) AS IMPORTO
			FROM {$cfg_mod_Gest['abi']['file_movimenti']} M
	  		WHERE 1=1 {$sql_mov_where}
			GROUP BY M.{$cod_field}, M.{$des_field}, M.VOCE_ANALISI, M.NOME_ANALISI
				,TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05
			ORDER BY M.{$des_field}
	";
	
	$stmt_mov = db2_prepare($conn, $sql_mov);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_mov, $sql_params);
	$ar_mov = array();
	
	
	while ($r = db2_fetch_assoc($stmt_mov)) {
	
		$ar_mov[trim($r['COD'])]['DESCR'] = acs_u8e(trim($r['DESCR']));
		
		if ($r['NOME_ANALISI'] == $extraParamsValue->open_tab_filters->nome_analisi_1){		
			$ar_mov[trim($r['COD'])]['VOCI'][trim($r['VOCE_ANALISI'])]['IMPORTO_A'] += $r['IMPORTO'];
			
			if (strlen(trim($r['TOTALE_01'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_01'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_02'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_02'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_03'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_03'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_04'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_04'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_05'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_05'])]['IMPORTO_A'] += $r['IMPORTO'];

			//per riga di totale
			$tot_mov['VOCI'][trim($r['VOCE_ANALISI'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_01'])) > 0) $tot_mov['VOCI'][trim($r['TOTALE_01'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_02'])) > 0) $tot_mov['VOCI'][trim($r['TOTALE_02'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_03'])) > 0) $tot_mov['VOCI'][trim($r['TOTALE_03'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_04'])) > 0) $tot_mov['VOCI'][trim($r['TOTALE_04'])]['IMPORTO_A'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_05'])) > 0) $tot_mov['VOCI'][trim($r['TOTALE_05'])]['IMPORTO_A'] += $r['IMPORTO'];
				
				
		}	
		if ($r['NOME_ANALISI'] == $extraParamsValue->open_tab_filters->nome_analisi_2){
			$ar_mov[trim($r['COD'])]['VOCI'][trim($r['VOCE_ANALISI'])]['IMPORTO_C'] += $r['IMPORTO'];
			
			if (strlen(trim($r['TOTALE_01'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_01'])]['IMPORTO_C'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_02'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_02'])]['IMPORTO_C'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_03'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_03'])]['IMPORTO_C'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_04'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_04'])]['IMPORTO_C'] += $r['IMPORTO'];
			if (strlen(trim($r['TOTALE_05'])) > 0) $ar_mov[trim($r['COD'])]['VOCI'][trim($r['TOTALE_05'])]['IMPORTO_C'] += $r['IMPORTO'];
				
		}
		
	}





?>


 <div id='my_content'>
 
 
 <h1>Analisi margine di contribuzione per azienda <?php echo get_nome_ditta($extraParamsValue->open_tab_filters->codice_ditta); ?></h1>
 <h2>&nbsp;[Periodo: <?php echo $extraParamsValue->open_tab_filters->nome_analisi_1; ?>, periodo raffronto: <?php echo $extraParamsValue->open_tab_filters->nome_analisi_2; ?>]</h2>
 <div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
 <br/>
 

 	<table class="int1" width="100%">

 	 
		<tr class=h_title>
		  <th colspan=2>Voce</th> 	 
			<?php foreach($ar_voci as $k_voce => $ar_voce) { ?>
			  <td width=10 class='number <?php echo trim($ar_voce['TIPO_VOCE']); ?>'><?php echo trim($ar_voce['DES_VOCE']); ?></td>
			<?php } ?>
		</tr>
		<tr>
		  <th colspan=2>Codice voce</th> 	 
			<?php foreach($ar_voci as $k_voce => $ar_voce) { ?>
			  <td width=10 class='number <?php echo trim($ar_voce['TIPO_VOCE']); ?>'><?php echo trim($k_voce); ?></td>
			<?php } ?>
		</tr>
		
 	 
 	
 	<?php foreach($ar_mov as $k=>$v ){ ?>
 	 <tr class=d_elenco>
 	  <th class=cod_voce><?php echo $k; ?></th>
 	  <th class=des_voce><?php echo $v['DESCR']; ?></th>
		<?php foreach($ar_voci as $k_voce => $ar_voce) { ?>
		  <td class='number <?php echo trim($ar_voce['TIPO_VOCE']); ?>'><?php echo n($v['VOCI'][$k_voce]['IMPORTO_A'], 2, 'N', 'Y'); ?></td>
		<?php } ?> 	  
 	 </tr>
 	<?php } ?>
 	
 	
 	<!-- riga con totali -->
 	 <tr class=d_elenco>
 	  <th colspan=2>Totali</th>
		<?php foreach($ar_voci as $k_voce => $ar_voce) { ?>
		  <td class='number <?php echo trim($ar_voce['TIPO_VOCE']); ?>'><?php echo n($tot_mov['VOCI'][$k_voce]['IMPORTO_A'], 2, 'N', 'Y'); ?></td>
		<?php } ?> 	  
 	 </tr> 	 
 	 
	</table>

	

	
	<br/>
	
	
 </div>

 </body>
</html>
		