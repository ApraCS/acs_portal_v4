<?php

require_once "../../config.inc.php";

$main_module = new DeskGest();
//$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'open_filtri'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	            
	           	    {
							name: 'f_imp_prov',
							xtype: 'checkboxgroup',
							fieldLabel: 'Includi righe con imponibile provvigionale a zero',
						   	allowBlank: true,
						   	labelWidth : 270,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_imp_prov' 
		                          , boxLabel: ''
		                          , inputValue: 'Y'
		                          , checked : false
		                        }]														
							},
							
				
				
						
                 
	            ],
	            
				buttons: [	
				{
			            text: 'Elenco',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        } 
		        
		        ]            
	           
	}
		
  ]}
	
<?php 
exit;
}


if($_REQUEST['fn'] == 'open_report'){

	$ar_email_to = array();

	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);	 
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js?d=20171024") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
  
  </script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>


<?php

function sql_where_by_request($m_params){

	$sql_where = "";
	
	if ($m_params->f_tipologia == 'LIQUIDATO') {
		$sql_where .= sql_where_by_combo_value('RF.RFTPRG', 'MATURATO');
		if (strlen($m_params->f_data_da) > 0)
			$sql_where .= " AND RF.RFDTLQ >= {$m_params->f_data_da}";		
		if (strlen($m_params->f_data_a) > 0)
			$sql_where .= " AND RF.RFDTLQ <= {$m_params->f_data_a}";		
	} else {
		$sql_where .= sql_where_by_combo_value('RF.RFTPRG', $m_params->f_tipologia);
		if (strlen($m_params->f_data_da) > 0)
			$sql_where .= " AND RF.RFDTRG >= {$m_params->f_data_da}";		
		if (strlen($m_params->f_data_a) > 0)
			$sql_where .= " AND RF.RFDTRG <= {$m_params->f_data_a}";
	}
	
	$sql_where .= sql_where_by_combo_value('RF.RFGRAG', $m_params->f_gruppo);	
	$sql_where .= sql_where_by_combo_value('RF.RFAGEN', $m_params->f_agenzia);
	$sql_where .= sql_where_by_combo_value('RF.RFCAGE', $m_params->f_agente);	

	
	//fatture o ordini? con rettifiche o meno o solo rettifiche?
	$ar_tipologia = array();
	if ($m_params->f_rettifiche == 'SOLO')
		$ar_tipologia[] = 'AG';
	else {
		$ar_tipologia[] = $m_params->f_fatture_o_ordini;
	
		if ($m_params->f_rettifiche == 'INCLUDI')
			$ar_tipologia[] = 'AG';
	}
	$sql_where .= sql_where_by_combo_value('RF.RFTIDO', $ar_tipologia);

	
	$sql_where .= sql_where_by_combo_value('TF.TFCDIV', $m_params->f_divisione);	
	$sql_where .= sql_where_by_combo_value('TF.TFTPDO', $m_params->f_tipo_documento);	
	$sql_where .= sql_where_by_combo_value('TF.TFCCON', $m_params->f_cliente_cod);		
	return $sql_where;		
}


function somma_valori($ar, $r){
	//ToDo
	//$ar['COLLI'] 	+= $r['TDTOCO'];
	
	$ar['conteggio_record']++;
	
	$ar['TFTIMP'] 	+= $r['TFTIMP'];
		
	$ar['RFIMPO'] 	+= $r['RFIMPO'];

	if ((float)$r['RFIMPO'] != 0)		
		$ar['RFIMPP'] 	+= $r['RFIMPP'];
	return $ar;
}
//parametri per report
$form_values =	strtr($_REQUEST['filter'], array('\"' => '"'));
$form_values = json_decode($form_values);

//parametri form
$filter =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$filter = json_decode($filter);


//se ha chiesto residuo, lavoro sempre sull'accreditato e poi lo confronto con il maturato o liquidato
if ($form_values->f_residuo == 'Y') {
	$form_values->f_tipologia_selezionata = $form_values->f_tipologia;		
	$form_values->f_tipologia = 'ACCREDITATO';		
}


	
	$sql_where = sql_where_by_request((object)$form_values);	

	$sql = "SELECT RF.*, TFCCON, TFDCON, TFTIMP, TFAADO, TFNRDO, TFTPDO, TFDTRG
		FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
		INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
		ON TF.TFDOCU = RF.RFDOCU
		WHERE 1=1 {$sql_where}
		";
	


$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);

$liv0_in_linea = null;
$ar = array();

while ($r = db2_fetch_assoc($stmt)) {

	//tree
	$liv0 = implode("|", array($r['RFAGEN'])); //Agenzia
	$liv1 = implode("|", array($r['RFCAGE'])); //Agente
	$liv2 = implode("|", array($r['TFCCON'])); //Cliente
	
	if ($form_values->f_residuo == 'Y')
		$liv3 = implode("|", array($r['RFDOCU'])); //Documento/riga provv
	else
		$liv3 = implode("|", array($r['RFDOCU'], $r['RFRIGA'])); //Documento/riga provv	

	
	//agenzia
	$t_ar = &$ar;
	$l = $liv0;
	if (!isset($t_ar[$l]))
		$t_ar[$l] = array("cod" => $l, "descr"=>$r['RFDAGN'], "record" => $r,
				"val" => array(), "children"=>array());

		$t_ar_liv0 = &$t_ar[$l]['val'];
		$t_ar = &$t_ar[$l]['children'];
	
		
		//agente
		$l = $liv1;
		if (!isset($t_ar[$l]))
			$t_ar[$l] = array("cod" => $l, "descr"=>$r['RFDAGE'], "record" => $r,
					"val" => array(), "children"=>array());

			$t_ar_liv1 = &$t_ar[$l]['val'];
			$t_ar = &$t_ar[$l]['children'];
			 
			
			//cliente
			$l = $liv2;
			if (!isset($t_ar[$l]))
				$t_ar[$l] = array("cod" => $l, "descr"=>$r['TFDCON'], "record" => $r,
						"val" => array(), "children"=>array());

				$t_ar_liv2 = &$t_ar[$l]['val'];
				$t_ar = &$t_ar[$l]['children'];
	
				//documento o documento/riga
				$l = $liv3;
				if (!isset($t_ar[$l])){

					$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
							"val" => array(), "children"=>array());
					
					if ($form_values->f_residuo != 'Y') {					
						$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
						$t_ar_liv0 = somma_valori($t_ar_liv0, $r);
						$t_ar_liv1 = somma_valori($t_ar_liv1, $r);
						$t_ar_liv2 = somma_valori($t_ar_liv2, $r);
					}
					
					
					if ($form_values->f_residuo == 'Y') {
						//recupero il liquidato/maturato per documento
						$sql2 = "SELECT SUM(RFIMPO) AS S_RFIMPO, SUM(RFIMLQ) AS S_RFIMLQ FROM {$cfg_mod_Gest['provvigioni']['file_righe']} WHERE RFDOCU=? AND RFCAGE=? AND RFTPRG=?";
						$stmt2 = db2_prepare($conn, $sql2);
						$result = db2_execute($stmt2, array($r['RFDOCU'], $r['RFCAGE'], 'MATURATO'));
						
						$r2 = db2_fetch_assoc($stmt2);
						$t_ar[$l]['val']['sum_per_residuo'] = $r2['S_RFIMPO'];
						$t_ar[$l]['val']['sum_per_residuo_liq'] = $r2['S_RFIMLQ'];
							
					}
				}
				

				//su residuo devo sempre sommare perche' sto raggruppando per documento
				if ($form_values->f_residuo == 'Y') {
					$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
					$t_ar_liv0 = somma_valori($t_ar_liv0, $r);
					$t_ar_liv1 = somma_valori($t_ar_liv1, $r);
					$t_ar_liv2 = somma_valori($t_ar_liv2, $r);
				}				
				
				
				
				$t_ar = &$t_ar[$l]['children'];
					
				//accodo le righe ordine
				$t_ar[] = $r;

} //per ogni row


//echo "<pre>"; print_r($ar); exit;


//stampo
$cl_liv_cont = 0;
$liv1_row_cl = 3;
$liv2_row_cl = 2;

echo "<div id='my_content'>";
foreach ($ar as $kl0 => $l0){
    echo liv0_intestazione_open($l0['record'], ++$c_liv0, $form_values);

	foreach ($l0['children'] as $kl1 => $l1){
	    echo liv1_intestazione_open($l1, $liv1_row_cl, $form_values);
			
		foreach ($l1['children'] as $kl2 => $l2){
		    echo liv2_intestazione_open($l2, $liv2_row_cl, $form_values);

			foreach ($l2['children'] as $kl3 => $l3){
			    echo liv3_intestazione_open($l3, $liv3_row_cl, $form_values, $filter);
					
				//foreach ($l3['children'] as $kriga => $l_riga)
				//	echo liv_riga_intestazione_open($l_riga, $liv3_row_cl);

			}

			if ($_REQUEST['stampa_rilevazione_scarico']=="Y")
				echo liv2_rilevazione_scarico();

		}
	}

	echo liv0_intestazione_close($l0['record']);
}
echo "</div>";

?>


 </body>
</html>

<?php 

exit;

}


//Agenzia
function liv0_intestazione_open($r, $c_liv0 = 1, $form_values){
    global $form_values;
    global $s, $spedizione;
    
    $ret = "";
    
    //salto pagina
    if ($c_liv0 > 1)
        $ret .= "<div class=\"page-break\"></div>";
        
        $ret .= "
        <div class=header_page>
        <H2>Elenco provvigioni - {$form_values->f_tipologia}</H2>
        <h3>" . trim($r['RFDAGN'])  . " [" . trim($r['RFAGEN']) . "]</H3>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>
	                
	                
	                
	                
			<table class=int0 width=\"100%\">
			 <tr>
			  <td width=\"50%\">Periodo dal " . print_date($form_values->f_data_da) . " al " . print_date($form_values->f_data_a) . "</td>";
        
        $ret .= "
			 </TR>
	</table>
		                              
		                              
	<table class=int1>
			 <tr>
			  <th>Cliente</th>
			  <th>Documento</th>
			  <th>Data</th>
			  <th class=number>Imponibile</th>
			  <th class=number>Imponibile Provv.</th>
			  <th class=number>% Provv.</th>
			  <th class=number>Importo Provv.</th>";
        
        if ($form_values->f_residuo == 'Y') {
            
            if ($form_values->f_tipologia_selezionata == 'LIQUIDATO')
                $ret .= "
				  <th class=number>Maturato</th>
				  <th class=number>Liquidato</th>
				  <th class=number>Residuo</th>
				";
                if ($form_values->f_tipologia_selezionata == 'MATURATO')
                    $ret .= "
				  <th class=number>Maturato</th>
				  <th class=number>Residuo</th>
				";
        }
        
        $ret .= "</tr>";
        
        return $ret;
}

function liv0_intestazione_close($r){
    return "</table>";
}


//Agente
function liv1_intestazione_open($l, $cl_liv, $form_values){
    global $form_values;
    $n_col_span = 4;
    
    
    $span_carico_class = '';
    if ($_REQUEST['stampa_dettaglio_ordini'] != "Y" && $_REQUEST['stampa_dettaglio_ordini'] != "R")
        $span_carico_class = 'grassetto';
        
        $ret = "
        <tr class=liv{$cl_liv}>
        <td colspan={$n_col_span}><span class=\"{$span_carico_class}\">AGENTE " . trim($l['descr']) . " [" . trim($l['record']['RFCAGE']) . "]</span>" . $orario_carico . $note_carico . "</TD>
		<td class=number>" . n($l['val']['RFIMPP']) . "</pre></td>
		<td>&nbsp;</td>
		<td class=number>" . n($l['val']['RFIMPO']) . "</pre></td>";
        
        if ($form_values->f_residuo == 'Y') {
            if ($form_values->f_tipologia_selezionata == 'LIQUIDATO')
                $ret .= "<td colspan=3>&nbsp;</td>";
                if ($form_values->f_tipologia_selezionata == 'MATURATO')
                    $ret .= "<td colspan=2>&nbsp;</td>";
        }
        
        $ret .= "</tr>";
        
        return $ret;
}






//Cliente
function liv2_intestazione_open($l, $cl_liv, $form_values){
    global $form_values;
    global $main_module, $cfg_mod_Spedizioni;
    
    $td_cli_class = '';
    if ($_REQUEST['r_type'] == 'per_cliente')
        $td_cli_class = 'grassetto';
        
        $txt_note_cell_cli = '';
        
        $ret = "
        <tr class=\"tr-cli liv{$cl_liv}\">
        <td colspan=1 class=\"{$td_cli_class}\">" . $l['record']['TFDCON'] . " [" . $l['record']['TFCCON']  . "]</TD>
	 <td colspan=5>&nbsp;</td>
	 <td class=number colspan=1>" . n($l['val']['RFIMPO']) . "</pre></td>
	";
        
        if ($form_values->f_residuo == 'Y') {
            if ($form_values->f_tipologia_selezionata == 'LIQUIDATO')
                $ret .= "<td colspan=3>&nbsp;</td>";
                if ($form_values->f_tipologia_selezionata == 'MATURATO')
                    $ret .= "<td colspan=2>&nbsp;</td>";
        }
        
        $ret .= "</tr>";
        
        
        return $ret;
}




//Documento/riga
function liv3_intestazione_open($l, $cl_liv, $form_values, $filter){
    global $form_values;
    
    if ($_REQUEST['stampa_dettaglio_ordini']=="R")
        $add_grassetto = " grassetto ";
        else
            $add_grassetto = "";
            
            $n_col_span = 3;
            
            if($filter->f_imp_prov != 'Y'){
              if($l['val']['RFIMPP'] != 0){
                $ret = "
                <tr class=\"liv{$cl_liv} $add_grassetto\">
                <td>&nbsp;</td>
                <td>" . implode("_", array(trim($l['record']['TFAADO']),  trim($l['record']['TFNRDO']), trim($l['record']['TFTPDO']))) . "</TD>
		<td>" . print_date($l['record']['TFDTRG']) . "</TD>
		<td class=number>" . n($l['record']['TFTIMP']) . "</TD>
		<td class=number>" . n($l['val']['RFIMPP']) . "</TD>
	    <td class=number>" . n_auto($l['record']['RFPPRA']) . " %</TD>
	    <td class=number>" . n($l['val']['RFIMPO']) . "</TD>
	 ";
                
                if ($form_values->f_residuo == 'Y') {
                    
                    if ($form_values->f_tipologia_selezionata == 'LIQUIDATO') {
                        $residuo = round($l['val']['RFIMPO'], 3) - round($l['val']['sum_per_residuo_liq'], 3);
                        $ret .= "<td class=number>" . n($l['val']['sum_per_residuo']) . "</td>
					 <td class=number>" . n($l['val']['sum_per_residuo_liq']) . "</td>
			  	 	 <td class=number>" . n($residuo, 2, 'N', 'Y') . "</td>";
                    }
                    
                    if ($form_values->f_tipologia_selezionata == 'MATURATO') {
                        $residuo = round($l['val']['RFIMPO'], 3) - round($l['val']['sum_per_residuo'], 3);
                        $ret .= "<td class=number>" . n($l['val']['sum_per_residuo'], 2) . "</td>
			  	 	 <td class=number>" . n($residuo, 2, 'N', 'Y') . "</td>";
                    }
                    
                    
                    
                }
                
                $ret .= "</tr>";
              }
            }else{
                
                
                $ret = "
                <tr class=\"liv{$cl_liv} $add_grassetto\">
                <td>&nbsp;</td>
                <td>" . implode("_", array(trim($l['record']['TFAADO']),  trim($l['record']['TFNRDO']), trim($l['record']['TFTPDO']))) . "</TD>
		<td>" . print_date($l['record']['TFDTRG']) . "</TD>
		<td class=number>" . n($l['record']['TFTIMP']) . "</TD>
		<td class=number>" . n($l['val']['RFIMPP']) . "</TD>
	    <td class=number>" . n_auto($l['record']['RFPPRA']) . " %</TD>
	    <td class=number>" . n($l['val']['RFIMPO']) . "</TD>
	 ";
                
                if ($form_values->f_residuo == 'Y') {
                    
                    if ($form_values->f_tipologia_selezionata == 'LIQUIDATO') {
                        $residuo = round($l['val']['RFIMPO'], 3) - round($l['val']['sum_per_residuo_liq'], 3);
                        $ret .= "<td class=number>" . n($l['val']['sum_per_residuo']) . "</td>
					 <td class=number>" . n($l['val']['sum_per_residuo_liq']) . "</td>
			  	 	 <td class=number>" . n($residuo, 2, 'N', 'Y') . "</td>";
                    }
                    
                    if ($form_values->f_tipologia_selezionata == 'MATURATO') {
                        $residuo = round($l['val']['RFIMPO'], 3) - round($l['val']['sum_per_residuo'], 3);
                        $ret .= "<td class=number>" . n($l['val']['sum_per_residuo'], 2) . "</td>
			  	 	 <td class=number>" . n($residuo, 2, 'N', 'Y') . "</td>";
                    }
                    
                    
                    
                }
                
                $ret .= "</tr>";
                
            }
            return $ret;
}





function liv_riga_intestazione_open($l, $cl_liv, $form_values){
    
    
    if ($_REQUEST['stampa_tel']=='Y')
        $n_col_span = 4;
        else
            $n_col_span = 3;
            
            if ($_REQUEST['stampa_data_scarico']=='Y')
                $n_col_span++;
                
                
                $ret = "
                <tr class=liv{$cl_liv}>
                <td colspan=1>&nbsp;</TD>
                <td colspan={$n_col_span}>" . trim($l['DES_ART']) . " [" . trim($l['COD_ART']) . "]" . "</TD>
    <td colspan=1 class=number>" . n($l['QTA_RIGA']) . " [" . trim($l['UM_RIGA']) . "]" . "</TD>";
                
                if ($_REQUEST['stampa_pallet']=='Y')  $ret .= "<td colspan=1 class=number>&nbsp;</TD> ";
                $ret .="
		 <td colspan=1 class=number>&nbsp;</TD>
		 <td colspan=1 class=number>&nbsp;</TD>
		 ";
                if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>&nbsp;</TD>";
                
                $ret .= "</tr>";
                return $ret;
}