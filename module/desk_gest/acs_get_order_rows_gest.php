<?php

require_once "../../config.inc.php";

$main_module = new DeskGest();
$sped_module = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));

	$raw_post_data = acs_raw_post_data();
	if (strlen($raw_post_data) > 0)
	{	
		$m_params = acs_m_params_json_decode();
		$k_ordine = $m_params->k_ordine;
	}

	if (isset($_REQUEST['k_ordine']))
		$k_ordine = $_REQUEST['k_ordine'];

	
	$oe = $sped_module->k_ordine_td_decode_xx($k_ordine);
	
	//recupero testata fattura
	$sql = "SELECT * FROM {$cfg_mod_Gest['provvigioni']['file_testate']} WHERE TFDOCU = ?";	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($k_ordine));
	$oe_fat = db2_fetch_assoc($stmt);
	
	
	
	
// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
	
	
	global $backend_ERP;
	
	switch ($backend_ERP){

		case 'GL':
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					LEFT OUTER JOIN E42_\$FILRV.\$MAARTLA ART
					ON RD.MECAR0 = ART.MACAR0
					WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $oe);
			
			while ($row = db2_fetch_assoc($stmt)) {
			$row['data_selezionata'] = sprintf("%08s", $dtep);
				$row['data_consegna'] = $row['MEDT20'];
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['MECAR0']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
			
				$row['RDRIGA'] = $row['MERID0'];
				$row['RDQTA'] = $row['MEQTA0'];
				$row['RDQTE'] = $row['MEQTS0'];
			
				$row['residuo'] = max((int)$row['MEQTA0'] - (int)$row['RDQTE'], 0);
				$row['RDUM'] = $row['MEUNM0'];
				$row['RDDART'] = acs_u8e($row['MADES0']);		
			
				$data[] = $row;
			}
							
		break;
		
		default: //SV2
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					 INNER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest_valuta']} RT
					    ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'  
					WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?					
					AND RD.RDTISR = '' AND RD.RDSRIG = 0";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $oe);
			
			while ($row = db2_fetch_assoc($stmt)) {
				$row['data_selezionata'] = sprintf("%08s", $dtep);
					$row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['RDART']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
				$row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
				
				//note, da filler
				$row['note'] = implode(";", array(
						substr($row['RDFIL1'], 14, 1) . ":" . substr($row['RDFIL1'], 8, 3), // Flag condizioni commerciali applicate : Tipo campagna						
						substr($row['RDFIL1'], 15, 1), // Flag azzera sconti
						substr($row['RDFIL1'], 54, 1), // Flag azzera provvigioni
				));
				
				$data[] = $row;
			}
							
		
	} //switch $backend_ERP
	

			//metto prima le righe della data selezionata
				////usort($data, "cmp_by_inDataSelezionata");
			
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $sped_module->get_ordine_by_k_docu($n_ord);

			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		
exit();				
}


?>


{"success": true, "items":
	{
		xtype: 'gridpanel',
		
  	    store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>',
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 
	            			'RDART', 'RDDART', 'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'RTINFI', 'RDPR1', 'RDTPRI', 'note'
	        			]
	    			}),
	    			
		        columns: [
		        /*
					{
		                header   : '<br/>&nbsp;Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
						  					return date_from_AS(value);			    
										}		                
		                 
		             },*/		        
		             {
		                header   : '<br/>&nbsp;Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },			        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		         /*    
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
		    			    	}}			    			             
		         */    
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>&nbsp;Importo netto',
		                dataIndex: 'RTINFI', 
		                width     : 110,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                
		             }, {
		                header   : '<br/>&nbsp;Perc. Provv',
		                dataIndex: 'RDPR1', 
		                width     : 110,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                
		             }, {
		                header   : 'Tipo<br/>riga',
		                dataIndex: 'RDTPRI', 
		                width     : 40,
		                align: 'right'		                
		             }, {
		                header   : '&nbsp;<br/>Note',
		                dataIndex: 'note', 
		                width     : 90,
		                align: 'right'		                
		             }            
		         ]	    					
		
		, listeners: {		
	 			afterrender: function (comp) {
					comp.up('window').setTitle('<?php echo "Righe documento [{$oe_fat['TFINUM']}] " . implode("_", array($oe_fat['TFAADO'], $oe_fat['TFNRDO'], $oe_fat['TFDT'])); ?>');	 				
	 			},
	 			
	 			
	          itemcontextmenu : function(grid, rec, node, index, event) {			          	
	                event.stopEvent();			                         
					var voci_menu = [];
					
					
					
							/* ToDo: solo se entro con possibilita' di modifica provvigione su riga */
					
								my_listeners = {
									afterUpdateRecord: function(from_win){	
		        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
		        						grid.store.reload();
		        						from_win.close();
						        		},
									afterAzzeraRecord: function(from_win){	
		        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
		        						grid.store.reload();
		        						from_win.close();
						        		},						        										
				    				};					
											
						<?php  if($m_params->mod_prv != 'Y'){	?>					
							      voci_menu.push({
						      		text: 'Modifica provvigione',
						    		iconCls : 'icon-briefcase-16',      		
						    		handler: function() {
										acs_show_win_std('Modifica provvigione', 
											'acs_panel_provvigioni.php?fn=open_modifica_provvigione_su_riga', 
											{
								  				docu: <?php echo j($k_ordine); ?>,
								  				nrec: rec.get('RDNREC'),
								  			}, 400, 300, my_listeners, 'icon-info_blue-16');
						    		}
								  });			
						<?php }?> 
								  
					      var menu = new Ext.menu.Menu({
					            items: voci_menu
						}).showAt(event.xy);
					
			   },
	 			
	 			
	 			
	 			
				  celldblclick2222: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	rec = iView.getRecord(iRowEl);
					  	acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('RDDT'), rdart: rec.get('RDART')}, 1100, 600, null, 'icon-shopping_cart_gray-16');
					  }
				  }	 			
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {		        
		        	if (rec.get('RDPR1') == 0) return 'rigaNonSelezionata'; //GRIGIA - riga senza provvigioni
		        	if (rec.get('RTINFI') < 0) return 'rigaParziale'; 		//GIALLO - riga con provvigioni negative		        
		         }   
		    }												    
			
		         
	}
}
