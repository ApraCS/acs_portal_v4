<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn']       = $row['RRN'];
    $nr['riga']      = $row['TANR'];
    $nr['tadesc']    = $row['TADESC'];
    $nr['tades2']    = $row['TADES2']; 
    $nr['sosp']      = $row['TATP'];
    $nr['stampa_vc'] = substr($row['TAREST'], 0, 1);
    $nr['v_lingua']  = substr($row['TAREST'], 1, 1);
    $nr['intest']    = substr($row['TAREST'], 2, 1);
    $nr['d_art']     = substr($row['TAREST'], 3, 1);
    $nr['c_art1']    = substr($row['TAREST'], 4, 1);
    $nr['c_art2']    = substr($row['TAREST'], 5, 1);
    $nr['c_art3']    = substr($row['TAREST'], 6, 1);
    $nr['c_art4']    = substr($row['TAREST'], 7, 1);
    $nr['c_art5']    = substr($row['TAREST'], 8, 1);
    $nr['c_art']     = "{$nr['c_art1']} {$nr['c_art2']} {$nr['c_art3']} {$nr['c_art4']} {$nr['c_art5']}";    
    
    $nr['prezzi']    = substr($row['TAREST'], 9, 1);
    $nr['hs_code']   = substr($row['TAREST'], 10, 1);
    $nr['raggr']     = substr($row['TAREST'], 11, 1);
    $nr['art_sm']    = substr($row['TAREST'], 12, 15);
    $nr['bl_note']   = substr($row['TAREST'], 27, 3);
    $nr['gr_doc']    = substr($row['TAREST'], 30, 3);
    $nr['subtot']    = substr($row['TAREST'], 33, 1);
    $nr['lingua']    = substr($row['TAREST'], 34, 3);
    $nr['gr_nom']    = substr($row['TAREST'], 37, 3);
    $nr['tarest']    = $row['TAREST'];
   
    return $nr;
    
}


function out_fields(){
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'tarest', 'sosp', 'stampa_vc', 'v_lingua',
                       'intest', 'd_art', 'c_art', 'c_art1', 'c_art2', 'c_art3', 'c_art4', 'c_art5',
                       'prezzi', 'hs_code', 'raggr', 'art_sm', 'bl_note', 'gr_doc', 'subtot', 'lingua', 'gr_nom'
    );
    return $ar_fields;
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
    
     	    {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            }, {
            header   : 'Note',
            dataIndex: 'tades2',
            flex : 1
            }
            ,{
            header   : 'Stampa vol./colli',
            dataIndex: 'stampa_vc',
            flex : 1
            },  {
            header   : 'Vol. lingua',
            dataIndex: 'v_lingua',
            flex : 1
            },
            {
            header   : 'Intest.',
            dataIndex: 'intest',
            flex : 1
            },  {
            header   : 'Desc. art.',
            dataIndex: 'd_art',
            flex : 1
            }, {
            header   : 'Cod. art.',
            dataIndex: 'c_art',
            flex : 1
            }, {
            header   : 'Prezzi',
            dataIndex: 'prezzi',
            flex : 1
            }, {
            header   : 'HScode',
            dataIndex: 'hs_code',
            flex : 1
            }, {
            header   : 'Raggrup.',
            dataIndex: 'raggr',
            flex : 1
            }, {
            header   : 'Sc.mostra',
            dataIndex: 'art_sm',
            flex : 1
            }, {
            header   : 'Bl.note',
            dataIndex: 'bl_note',
            flex : 1
            }, {
            header   : 'Gr.doc.',
            dataIndex: 'gr_doc',
            flex : 1
            }, {
            header   : 'Subtot.',
            dataIndex: 'subtot',
            flex : 1
            }, {
            header   : 'Lingua',
            dataIndex: 'lingua',
            flex : 1
            },
             {
            header   : 'Gr.doc. nom.',
            dataIndex: 'gr_nom',
            flex : 1
            }
    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
   
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "tades2" => trim($row->tades2),
        "stampa_vc" => trim($row->stampa_vc),
        "v_lingua" => trim($row->v_lingua),
        "intest" => trim($row->intest),
        "d_art" => trim($row->d_art),
        "c_art1" => trim($row->c_art1),
        "c_art2" => trim($row->c_art2),
        "c_art3" => trim($row->c_art3),
        "c_art4" => trim($row->c_art4),
        "c_art5" => trim($row->c_art5),
        "prezzi" => trim($row->prezzi),
        "hs_code" => trim($row->hs_code),
        "raggr" => trim($row->raggr),
        "art_sm" => trim($row->art_sm),
        "bl_note" => trim($row->bl_note),
        "gr_doc" => trim($row->gr_doc),
        "subtot" => trim($row->subtot),
        "lingua" => trim($row->lingua),
        "gr_nom" => trim($row->gr_nom),
        "tarest" => trim($row->tarest),
        "sosp" => trim($row->sosp),
        
    );
 
    return $ar_values;
    
}


function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		maxLength: 3,
		value:  <?php echo j($ar_values['riga']); ?>
	  },
	   {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
       {xtype: 'textfield',
    	name: 'tades2',
    	fieldLabel: 'Note',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['tades2']); ?>
    	},
		{ 
		xtype: 'fieldcontainer',
		anchor: '-15',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
		
    		{name: 'stampa_vc',
    		xtype: 'combo',
    		fieldLabel: 'Stampa volume',
    		forceSelection: true,								
    		displayField: 'text',
    		flex : 1,
    		valueField: 'id',
    		value:  <?php echo j($ar_values['stampa_vc']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'Y', text : '[Y] Yes'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 },
    	 {name: 'v_lingua',
    		xtype: 'combo',
    		fieldLabel: 'Lingua',
    		forceSelection: true,								
    		displayField: 'text',
    		labelAlign : 'right',
    		labelWidth : 85,
    		flex : 1,
    		valueField: 'id',
    		value:  <?php echo j($ar_values['v_lingua']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'I', text : '[I] Italiano'},
    		       {id : 'F', text : '[F] Francese'},
    		       {id : 'S', text : '[S] Spagnolo'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 }
    		   
		]},
		
		{ 
		xtype: 'fieldcontainer',
		anchor: '-15',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
    		{name: 'intest',
    		xtype: 'combo',
    		fieldLabel: 'Intestazione',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['intest']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'I', text : '[I] Inglese'},
    		       {id : 'S', text : '[S] Spagnolo'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 },{name: 'd_art',
    		xtype: 'combo',
    		fieldLabel: 'Desc. articolo',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['d_art']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            labelAlign : 'right',
    		labelWidth : 85,
    		flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'B', text : '[B] Bilingue'},
    		       {id : 'E', text : '[E] Estero'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
	 }
		]},
	
		{ 
		xtype: 'fieldcontainer',
		anchor: '-15',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
    		{name: 'c_art',
    		xtype: 'combo',
    		fieldLabel: 'Cod. articolo 1',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['c_art1']); ?>,							
    		emptyText: '- seleziona -',
    		flex : 1,
            anchor: '-15',
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'C', text : '[C] Articolo'},
    		       {id : 'A', text : '[A] Alternativo'},
    		       {id : 'R', text : '[R] Riferimento'},
    		       {id : 'L', text : '[L] Listino'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 },{name: 'c_art',
    		xtype: 'combo',
    		fieldLabel: 'Cod. articolo 2',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['c_art2']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            labelAlign : 'right',
    		labelWidth : 85,
    		flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'C', text : '[C] Articolo'},
    		       {id : 'A', text : '[A] Alternativo'},
    		       {id : 'R', text : '[R] Riferimento'},
    		       {id : 'L', text : '[L] Listino'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 }
		]},
		
		{ 
		xtype: 'fieldcontainer',
		anchor: '-15',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
		
    		{name: 'c_art',
    		xtype: 'combo',
    		fieldLabel: 'Cod. articolo 3',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['c_art3']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'C', text : '[C] Articolo'},
    		       {id : 'A', text : '[A] Alternativo'},
    		       {id : 'R', text : '[R] Riferimento'},
    		       {id : 'L', text : '[L] Listino'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 },{name: 'c_art',
    		xtype: 'combo',
    		fieldLabel: 'Cod. articolo 4',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['c_art4']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            labelAlign : 'right',
    		labelWidth : 85,
    		flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'C', text : '[C] Articolo'},
    		       {id : 'A', text : '[A] Alternativo'},
    		       {id : 'R', text : '[R] Riferimento'},
    		       {id : 'L', text : '[L] Listino'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 }
		
		]},
		
		{ 
		xtype: 'fieldcontainer',
		anchor: '-15',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
    		{name: 'c_art',
    		xtype: 'combo',
    		fieldLabel: 'Cod. articolo 5',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['c_art5']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'C', text : '[C] Articolo'},
    		       {id : 'A', text : '[A] Alternativo'},
    		       {id : 'R', text : '[R] Riferimento'},
    		       {id : 'L', text : '[L] Listino'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 },{name: 'prezzi',
    		xtype: 'combo',
    		fieldLabel: 'Prezzi',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['prezzi']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            labelAlign : 'right',
            labelWidth : 85,
        	flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'Y', text : '[Y] Yes'},
    		       {id : '5', text : '5 decimali'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 }
		]},
		
		{ 
		xtype: 'fieldcontainer',
		anchor: '-15',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
		
    		{name: 'hs_code',
    		xtype: 'combo',
    		fieldLabel: 'Stampa HSCODE',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['hs_code']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'Y', text : '[Y] Yes'},
    		       {id : 'H', text : '[H] Hscode'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	  },{name: 'raggr',
    		xtype: 'combo',
    		fieldLabel: 'Raggruppam.',
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',
    		value:  <?php echo j($ar_values['raggr']); ?>,							
    		emptyText: '- seleziona -',
            anchor: '-15',
            labelAlign : 'right',
            labelWidth : 85,
        	flex : 1,
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		       {id : 'Y', text : '[Y] Per articolo'},
    		       {id : 'O', text : '[O] Per ordine'},
    		       {id : 'R', text : '[R] Rottura'},
    	     	   {id : '', text : '- vuoto -'}
    		    ]
    		}
    			
    	 }
		
		]}
		
	    , {name: 'art_sm',
		xtype: 'textfield',
		fieldLabel: 'Sconto mostra',
		value:  <?php echo j($ar_values['art_sm']); ?>,							
		anchor: '-15',
		maxLength : 15
      },{name: 'bl_note',
		xtype: 'combo',
		fieldLabel: 'Blocco note',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		value:  <?php echo j($ar_values['bl_note']); ?>,							
		emptyText: '- seleziona -',
        anchor: '-15',
        queryMode: 'local',
        minChars: 1,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		     data: [ <?php echo acs_ar_to_select_json(find_TA_sys('NUSN', null, 'CF', null, null, null, 0, '', 'Y'), '');?>]
		},listeners: { 
		 		beforequery: function (record) {
         		record.query = new RegExp(record.query, 'i');
         		record.forceAll = true;
		 }
		}
			
	 }, {name: 'gr_doc',
		xtype: 'combo',
		fieldLabel: 'Gr. documento',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		value:  <?php echo j($ar_values['gr_doc']); ?>,							
		emptyText: '- seleziona -',
        anchor: '-15',
        queryMode: 'local',
        minChars: 1,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		     data: [ <?php echo acs_ar_to_select_json(find_TA_sys('RDOC', null, null, null, null, null, 0, '', 'Y'), '', true, 'N','Y');?>]
		},listeners: { 
		 		beforequery: function (record) {
         		record.query = new RegExp(record.query, 'i');
         		record.forceAll = true;
		 }
		}
			
	 },
	 
	 	{ 
		xtype: 'fieldcontainer',
		anchor: '-15',
		flex:1,
		layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
		items: [
		
		{name: 'subtot',
		xtype: 'combo',
		fieldLabel: 'Subtotale',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		value:  <?php echo j($ar_values['subtot']); ?>,							
		emptyText: '- seleziona -',
        anchor: '-15',
        flex : 1,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		    data: [								    
		       {id : 'Y', text : '[Y] Yes'},
	     	   {id : '', text : '- vuoto -'}
		    ]
		}
			
	 },{name: 'lingua',
		xtype: 'combo',
		fieldLabel: 'Lingua',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		value:  <?php echo j($ar_values['lingua']); ?>,							
		emptyText: '- seleziona -',
        anchor: '-15',
        labelAlign : 'right',
        labelWidth : 85,
        flex : 1,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		    data: [ <?php echo acs_ar_to_select_json(find_TA_sys('VULN', null, null, null, null, null, 0, '', 'Y'), '');?>]
		}
			
	 }
	]},
	
	 {name: 'gr_nom',
		xtype: 'combo',
		fieldLabel: 'Gr. doc. nom.',
		forceSelection: true,								
		displayField: 'text',
		valueField: 'id',
		value:  <?php echo j($ar_values['gr_nom']); ?>,							
		emptyText: '- seleziona -',
        anchor: '-15',
        queryMode: 'local',
        minChars: 1,
		store: {
			editable: false,
			autoDestroy: true,
		    fields: [{name:'id'}, {name:'text'}],
		     data: [ <?php echo acs_ar_to_select_json(find_TA_sys('RDOC', null, null, null, null, null, 0, '', 'Y'), '', true, 'N','Y');?>]
		},listeners: { 
		 		beforequery: function (record) {
         		record.query = new RegExp(record.query, 'i');
         		record.forceAll = true;
		 }
		}
			
	 }
	 
  <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values, $tarest){
    
    $ar_ins = array();
    
    $m_table_config = array( 'TAREST' =>
        array(
            'stampa_vc' 	=> array(
                "start" => 0,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'v_lingua' 	=> array(
                "start" => 1,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'intest' 	=> array(
                "start" => 2,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'd_art' 	=> array(
                "start" => 3,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'c_art1' 	=> array(
                "start" => 4,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'c_art2' 	=> array(
                "start" => 5,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'c_art3' 	=> array(
                "start" => 6,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'c_art4' 	=> array(
                "start" => 7,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'c_art5' 	=> array(
                "start" => 8,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'prezzi' 	=> array(
                "start" => 9,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'hs_code' 	=> array(
                "start" => 10,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'raggr' 	=> array(
                "start" => 11,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'art_sm' 	=> array(
                "start" => 12,
                "len"   => 15,
                "riempi_con" => ""
            ),
            'bl_note' 	=> array(
                "start" => 27,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'gr_doc' 	=> array(
                "start" => 30,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'subtot' 	=> array(
                "start" => 33,
                "len"   => 1,
                "riempi_con" => ""
            ),
            'lingua' 	=> array(
                "start" => 34,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'gr_nom' 	=> array(
                "start" => 37,
                "len"   => 3,
                "riempi_con" => ""
            ),
        ));
    
    $tarest = genera_TAREST($m_table_config, $form_values, $tarest);
    $ar_ins['TAREST'] = $tarest;
    $ar_ins['TANR']  = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;
    $ar_ins['TADES2'] = $form_values->tades2;

    return $ar_ins;
    
}

function genera_TAREST($m_table_config, $values, $tarest){
    $value_attuale = $tarest;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
    //$new_value = "";
    foreach($m_table_config['TAREST'] as $k => $v){
        if(isset($values->$k)){
            $chars = $values->$k;
         if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
              $len = "%{$v['riempi_con']}{$v['len']}s";
        else
            $len = "%-{$v['len']}s";
                    
        $chars = substr(sprintf($len, $chars), 0, $v['len']);
        $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
        $value_attuale = $new_value;
                    
        }
        
    }
        
    return $new_value;
}

