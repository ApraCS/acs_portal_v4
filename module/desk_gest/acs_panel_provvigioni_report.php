<?php

require_once "../../config.inc.php";
ini_set('max_execution_time', 300);


	$main_module = new DeskGest();

	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);

	
	//parametri per report
	$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
	$form_values = json_decode($form_values);
	
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div#my_content h1{font-size: 22px; padding: 5px;}
   div#my_content h2{font-size: 15px; padding: 5px;}
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}
	   

   /*tr.liv_totale td{background-color: #cccccc; font-weight: bold;}*/
   tr.liv_totale td{font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.liv_div td{background-color: #333333; font-weight: bold; color: white; font-size: 0.7em;}   
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}      
   table.int1 td.blocco{padding-bottom: 40px; border: 0px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js?d=20171024") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {

	//apertura automatica finestra invio email
	<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
		Ext.get('p_send_email').dom.click();
	<?php } ?>
	
	});    
    

  </script>


 </head>
 <body>
 
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

 <div id='my_content'>
 
 
 <h1>Controllo fatturato/provvigioni accreditate</h1>
 <h2>Periodo dal <?php echo print_date($_REQUEST['f_data_da']) . " al " . print_date($_REQUEST['f_data_a']); ?>  </h2>
 <div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
 <br/>
 

 	<table class="int1" width="100%"> 	
		<tr><td class=blocco><?php echo print_table_tree_fatturato(); ?></td></tr>
		<tr><td class=blocco><?php echo print_table_tree_agenzia_agente(); ?></td></tr>
		<tr><td class=blocco><?php echo print_table_tree_pagamenti(); ?></td></tr>
		<tr><td class=blocco><?php echo print_table_tree_sconti(); ?></td></tr>		
		<tr><td class=blocco><?php echo print_table_tree_nazioni(); ?></td></tr>
	</table>

	
	<br/>
	
	
 </div>

 </body>
</html>
<?php




function print_table_tree_fatturato(){

	global $form_values;
	$ar_ret = get_table_data($form_values, 'TFNATD', 'TFNATD', null, 0, 'TFTPDO', 'TFDTPD', null, 'N', 'Y', 'Y');

	$ret = "";
	$ret .= print_tabella_tree('Fatturato', $ar_ret, 'N', 'Y', 'Y', 'Y', 'YY', 2, 'N', 'Y');
	return $ret;
}

function print_table_tree_agenzia_agente(){

	global $form_values;
	$ar_ret = get_table_data($form_values, 'RF.RFAGEN', 'RFDAGN', null, 0, 'RFCAGE', 'RFDAGE', null, 'Y', 'N');

	$ret = "";
	$ret .= print_tabella_tree('Agenzie / Agenti', $ar_ret, 'Y', 'Y', 'N');
	return $ret;
}


function print_table_tree_pagamenti(){

	global $form_values;
	$ar_ret = get_table_data($form_values, 'TFTPAG', 'TFTPAG', null, 0, 'TFCPAG', 'TFDPAG', null, 'N', 'Y');

	$ret = "";
	$ret .= print_tabella_tree('Pagamenti', $ar_ret, 'N', 'Y', 'Y');
	return $ret;
}


function print_table_tree_sconti(){

	global $form_values;
	$ar_ret = get_table_data($form_values, 'SCONTI', 'SCONTI', null, 0, null, null, null, 'N', 'Y');

	$ret = "";
	$ret .= print_tabella_tree('Sconti', $ar_ret, 'N', 'Y', 'Y', 'N', 'N', 1);	
	return $ret;
}


function print_table_tree_nazioni(){

	global $form_values;
	$ar_ret = get_table_data($form_values, 'TF.TFNAZI', 'TFDNAZ', null, 0, 'RFPPRA', 'RFPPRA', null, 'Y', 'N');

	$ret = "";
	$ret .= print_tabella_tree('Nazioni', $ar_ret, 'N', 'Y', 'N', 'N', 'YN', 2, 'Y');	
	return $ret;
}

 
 



 
 function print_tabella($title, $ar_ret, $stampa_cod = 'Y', $top = 0){
 	
 	//scorro per tot import e nr
 	$tot_imp_rec = $tot_nr_rec = 0; //somma di quelli che mostro (per devo aggiungere ALTRO)
 	foreach($ar_ret as $r){
 		$tot_imp_rec += $r['S_IMP'];
 		$tot_nr_rec  += $r['NUM_ORD'];
 	}


 	
 	if ($top > 0){
 		//creo riga "altro"
 		$ar_ret[] = array(	'TDASPE' => 'ALTRO', 
 							'TDASPE_D' => 'ALTRO', 
 							'S_IMP' => ($tot_imp - $tot_imp_rec),
 							'NUM_ORD' => ($tot_nr - $tot_nr_rec) 				
 						);		
 	}
 	
 	
 	
 	$ret = "";
 	$ret .= "
	 <table>
   		
	  <tr class=liv3>
		 <td colspan=6>" . $title . "</td>
   	  </tr>	
   		
	  <tr>
 		";
 	
 	if ($stampa_cod == 'Y')
 		$ret .= "
 		<th>Cod</th>
 		<th>Descrizione</th>
 		";
 	else
 		$ret .= "<th colspan=2>Descrizione</th>";
 	
 		
 	$ret .= "	
	   <th class=number>Importo</th>
	   <th class=number>%</th>
	   <th class=number>Nr</th>
	   <th class=number>%</th>
	  </tr>
	 ";
 	
 	foreach($ar_ret as $r){
 		
 		$r_perc_imp = $r_perc_nr = 0;
 		if ($tot_imp != 0)
 			$r_perc_imp = ($r['S_IMP'] / $tot_imp) * 100;
 		if ($tot_nr != 0)
 			$r_perc_nr = ($r['NUM_ORD'] / $tot_nr) * 100; 		
 		
 		$ret .= "
			<tr>
			";

 		if ($stampa_cod == 'Y')
			 $ret .= "
		 		<td>{$r['TDASPE']}</td>
		 		<td>{$r['TDASPE_D']}</td>
 			";
 		else
 			$ret .= "<td colspan=2>{$r['TDASPE_D']}</td>";
 		
 		$ret .= " 			
			 <td class=number>" . n($r['S_IMP']) . "</td>
			 <td class=number>" . n($r_perc_imp) . "</td>
			 <td class=number>" . n($r['NUM_ORD'], 0) . "</td>
			 <td class=number>" . n($r_perc_nr) . "</td>
			</tr>
		";
 	} 	
 	

   //totale
 	$ret .= "
 	  <tr class=liv_totale>
 		<td colspan=2 class=number>Totale</td>
 		<td class=number>" . n($tot_imp) . "</td>
		<td class=number>&nbsp;</td>
		<td class=number>" . n($tot_nr, 0) . "</td>
		<td class=number>&nbsp;</td>
	  </tr>
	";
 	
 	
   $ret .= "					  
	 </table>
	";
   return $ret;	
 }


 
 
 
 
 
 function print_tabella_tree($title, $ar_ret, $separa_per_provv = 'N', $stampa_colonne_provv = 'N', $tot_per_testata = 'N', $liv_divisione = 'N', $stampa_codice = 'YY', $livelli = 2, $livello2_as_perc = 'N'){
 	
 	$tot_colspan = 8;
 	
 	if ($separa_per_provv == 'Y') $tot_colspan++;
 	if ($stampa_colonne_provv == 'Y') $tot_colspan += 3;
 	
 	//scorro per tot import e nr
 	$tot_imp = $tot_nr = 0;


 	//creo alberatura
 	$ar = array();
 	foreach($ar_ret as $row){
 		
 		$s_ar = &$ar; 		
 		
 		
	 	//configurazione stacco per livelli
	 	$r_livs['liv1_v'] = trim($row['CAMPO_NAME']);
	 	if ($separa_per_provv == 'Y')
	 		$r_livs['liv2_v'] = implode("_", array(trim($row['CAMPO_NAME']), $row['PERC_PROVV']));
	 	else
	 		$r_livs['liv2_v'] = trim($row['CAMPO2_NAME']);	 		

	 	
	 	//aggiungo a livello 0 la divisione
	 	if ($liv_divisione == 'Y'){
	 		$r_livs['liv0_v'] = trim($row['TFCDIV']);
	 		
			 		$liv = $r_livs['liv0_v'];
			 		if (is_null($s_ar[$liv])){
			 			$s_ar[$liv] = $row;
			 		
			 			/*
			 			$tot_imp += $row['S_TIMP_LIV1'];
			 			$tot_infi += $row['S_INFI_LIV1'];
			 			$tot_anticipo += $row['S_TANT_LIV1'];
			 			$tot_nr  += $row['NUM_ROW_LIV1'];
			 			*/
			 		
			 		} else {
			 		
			 			if ($tot_per_testata == 'Y'){
			 				//incremento liv1
			 				$s_ar[$liv]['S_TIMP_LIV1'] += $row['S_TIMP'];
			 				$s_ar[$liv]['S_INFI_LIV1'] += $row['S_INFI'];
			 				$s_ar[$liv]['S_TANT_LIV1'] += $row['S_TANT'];
			 				$s_ar[$liv]['NUM_ROW_LIV1'] += $row['NUM_ROW'];
			 				 
			 				/*
			 				$tot_imp += $row['S_TIMP'];
			 				$tot_infi += $row['S_INFI'];
			 				$tot_anticipo += $row['S_TANT'];
			 				$tot_nr  += $row['NUM_ROW'];
			 				*/
			 			}
			 				
			 			$s_ar[$liv]['S_IMPONIBILE_PROVV'] += $row['S_IMPONIBILE_PROVV'];
			 			$s_ar[$liv]['S_IMPORTO_PROVV'] += $row['S_IMPORTO_PROVV'];
			 		}
			 		
			 		/*
			 		$tot_imponibile_provv += $row['S_IMPONIBILE_PROVV'];
			 		$tot_importo_provv += $row['S_IMPORTO_PROVV'];
			 		*/
			 		
			 		
			 		
			 		//memorizzo elenco diverse percentuali
			 		$perc_to_txt = n_auto($row['PERC_PROVV']) . "%";
			 		$s_ar[$liv]['lista_percentuali'][$perc_to_txt] ++;
			 		
			 		$s_ar[$liv]['lista_perc_provvigione'] = '';
			 		foreach ($s_ar[$liv]['lista_percentuali'] as $kp => $p){
			 			$s_ar[$liv]['lista_perc_provvigione'] .= "{$kp} ";
			 		}
			 		 
			 		 
			 		$s_ar = &$s_ar[$liv]['children'];
			 		
			 		
	 	} //liv_divisione
	 	 
	 	
	 	//liv1
	 	$liv = $r_livs['liv1_v'];
	 	if (is_null($s_ar[$liv])){
	 		$s_ar[$liv] = $row;
	 			 
	 		$tot_imp += $row['S_TIMP_LIV1'];
	 		$tot_infi += $row['S_INFI_LIV1'];
	 		$tot_anticipo += $row['S_TANT_LIV1'];
	 		$tot_nr  += $row['NUM_ROW_LIV1'];
	 		
	 	} else {
	 		
	 		if ($tot_per_testata == 'Y'){	 		
		 		//incremento liv1
		 		$s_ar[$liv]['S_TIMP_LIV1'] += $row['S_TIMP'];
		 		$s_ar[$liv]['S_INFI_LIV1'] += $row['S_INFI'];
		 		$s_ar[$liv]['S_TANT_LIV1'] += $row['S_TANT'];
		 		$s_ar[$liv]['NUM_ROW_LIV1'] += $row['NUM_ROW'];
		 		
		 		$tot_imp += $row['S_TIMP'];
		 		$tot_infi += $row['S_INFI'];
		 		$tot_anticipo += $row['S_TANT'];
		 		$tot_nr  += $row['NUM_ROW'];		 		
	 		}
	 			
	 		$s_ar[$liv]['S_IMPONIBILE_PROVV'] += $row['S_IMPONIBILE_PROVV'];
	 		$s_ar[$liv]['S_IMPORTO_PROVV'] += $row['S_IMPORTO_PROVV'];
	 	}
	 		
	 	$tot_imponibile_provv += $row['S_IMPONIBILE_PROVV'];
	 	$tot_importo_provv += $row['S_IMPORTO_PROVV'];
	 	 
	 	 
	 		
	 	//memorizzo elenco diverse percentuali
		 	$perc_to_txt = n_auto($row['PERC_PROVV']) . "%";
		 	$s_ar[$liv]['lista_percentuali'][$perc_to_txt] ++;
		 	
	 		$s_ar[$liv]['lista_perc_provvigione'] = '';
	 		foreach ($s_ar[$liv]['lista_percentuali'] as $kp => $p){
	 			$s_ar[$liv]['lista_perc_provvigione'] .= "{$kp} ";
	 		}
	 	
	 	
	 	$s_ar = &$s_ar[$liv]['children'];
	 	
	 	//liv2 -------------------------
	 	$liv = $r_livs['liv2_v'];	 	
	 	if (is_null($s_ar[$liv]))
	 		$s_ar[$liv] = $row;
	 	else {
	 		$s_ar[$liv]['S_TIMP'] += $row['S_TIMP'];
	 		$s_ar[$liv]['S_INFI'] += $row['S_INFI'];
	 		$s_ar[$liv]['S_TANT'] += $row['S_TANT'];
	 		$s_ar[$liv]['NUM_ROW'] += $row['NUM_ROW'];
	 		$s_ar[$liv]['S_IMPONIBILE_PROVV'] += $row['S_IMPONIBILE_PROVV'];
	 		$s_ar[$liv]['S_IMPORTO_PROVV'] += $row['S_IMPORTO_PROVV'];
	 	}
	 	
 	}
 	

 
 	$ret = "";
 	$ret .= "
	 <table>
   
	  <tr class=liv3>
		 <td colspan={$tot_colspan}>" . $title . "</td>
   	  </tr>
   
	  <tr>
	   <th>Cod</th>
	   <th>Descrizione</th>";
 	if ($separa_per_provv == 'Y') $ret .= "<th class=number>Perc.Provv.</th>";
 	$ret .= "
	   <th class=number>Imponibile</th>
	   <th class=number>%</th>
 	   <th class=number>Anticipo</th> 					
	   <th class=number>Netto merce</th> 		
	   <th class=number>Nr</th>
	   <th class=number>%</th>";
 	
 	if ($stampa_colonne_provv == 'Y')
 		$ret .= "
 					<th class=number>Imponibile provv</th>
 					<th class=number>Importo.Provv.</th>
 				    <th class=number>Media Provv.</th>
 				";
 	
 	$ret .= "				
	  </tr>
	 ";
 
 	if ($liv_divisione== 'Y'){
 		//liv0 divisione
 		
 		foreach($ar as $r){
 		
	 			$r_perc_imp = $r_perc_nr = 0;
	 			if ($tot_imp != 0)
	 				$r_perc_imp = ($r['S_TIMP_LIV1'] / $tot_imp) * 100;
	 			if ($tot_nr != 0)
	 				$r_perc_nr = ($r['NUM_ROW_LIV1'] / $tot_nr) * 100;
	 		
	 			$ret .= "
		 			<tr class=liv_div>
		 			<td>{$r['TFCDIV']}</td>
		 			<td>{$r['TFDDIV']}</td>";
		 			if ($separa_per_provv == 'Y') $ret .= "<td class=number>" . $r['lista_perc_provvigione'] . "</td>";
		 			$ret .= "
		 		 <td class=number>" . n($r['S_TIMP_LIV1']) . "</td>
				 <td class=number>" . n($r_perc_imp) . "</td>
		 		 <td class=number>" . n($r['S_TANT_LIV1']) . "</td>
			     <td class=number>" . n($r['S_INFI_LIV1']) . "</td>
				 <td class=number>" . n($r['NUM_ROW_LIV1'], 0) . "</td>
				 <td class=number>" . n($r_perc_nr) . "</td>
				";
	 		
	 			if ($stampa_colonne_provv == 'Y'){
	 				$ret .= "<td class=number>" . n($r['S_IMPONIBILE_PROVV']) . "</td>";
	 				$ret .= "<td class=number>" . n($r['S_IMPORTO_PROVV']) . "</td>";
	 				if ($r['S_INFI_LIV1'] > 0)
	 					$ret .= "<td class=number>" . n($r['S_IMPORTO_PROVV'] / $r['S_INFI_LIV1'] * 100, 2) . "</td>";
	 				else
	 					$ret .= "<td class=number>" . n(0, 2) . "</td>";
	 			}
	 		
	 			$ret .= "<tr>";
	 				
 		
		$ret .= create_liv1_liv2($r['children'], array('tot_imp' => $tot_imp, 'tot_nr' => $tot_nr), $separa_per_provv, $stampa_colonne_provv, $tot_per_testata, $liv_divisione, $stampa_codice, $livelli, $livello2_as_perc);
 	  } //per ogni divisione
 	} else {
 		$ret .= create_liv1_liv2($ar, array('tot_imp' => $tot_imp, 'tot_nr' => $tot_nr), $separa_per_provv, $stampa_colonne_provv, $tot_per_testata, $liv_divisione, $stampa_codice, $livelli, $livello2_as_perc);
 	}
 

 	$liv_totale_colspan=2;
 	if ($separa_per_provv == 'Y') $liv_totale_colspan++;
 	//totale
 	$ret .= "
 	  <tr class=liv_totale>
 		<td colspan={$liv_totale_colspan} class=number>Totale</td>
 		<td class=number>" . n($tot_imp) . "</td>
		<td class=number>&nbsp;</td>
	 	<td class=number>" . n($tot_anticipo) . "</td>	 			
        <td class=number>" . n($tot_infi) . "</td>			 		
		<td class=number>" . n($tot_nr, 0) . "</td>
		<td class=number>&nbsp;</td>";
 	
 	if ($stampa_colonne_provv == 'Y')
 		$ret .= "
        	<td class=number>" . n($tot_imponibile_provv) . "</td>	 				
	 		<td class=number>" . n($tot_importo_provv) . "</td>
	   		<td class=number>" . n($tot_importo_provv / $tot_infi * 100, 2) . "</td>
	 	"; 	
 	
 	$ret .= "
	  </tr>
	";
 	
 	
 	$ret .= "
	 </table>
	";
 	return $ret;
 }
 
 
 
 
 
 
 
 
 function create_liv1_liv2($ar, $ar_tots, $separa_per_provv, $stampa_colonne_provv, $tot_per_testata, $liv_divisione, $stampa_codice, $livelli, $livello2_as_perc){ 	
 	$ret = "";
 	$tot_imp 	= $ar_tots['tot_imp'];
 	$tot_nr		= $ar_tots['tot_nr'];
 	foreach($ar as $r){
 
 		$r_perc_imp = $r_perc_nr = 0;
 		if ($tot_imp != 0)
 			$r_perc_imp = ($r['S_TIMP_LIV1'] / $tot_imp) * 100;
 		if ($tot_nr != 0)
 			$r_perc_nr = ($r['NUM_ROW_LIV1'] / $tot_nr) * 100;
 
 		if ($livelli == 1)
 			$ret .= "<tr>";
 		else 		
 			$ret .= "<tr class=liv2>";

 		if (substr($stampa_codice, 0, 1) == 'Y')
 			$ret .= "<td>{$r['CAMPO_NAME']}</td>";
 		else 
 			$ret .= "<td>&nbsp;</td>";
 		 			
 		$ret .= "
 		<td>{$r['CAMPO_DES']}</td>";
 		if ($separa_per_provv == 'Y') $ret .= "<td class=number>" . $r['lista_perc_provvigione'] . "</td>";
 		$ret .= "
 		 <td class=number>" . n($r['S_TIMP_LIV1']) . "</td>
		 <td class=number>" . n($r_perc_imp) . "</td>
 		 <td class=number>" . n($r['S_TANT_LIV1']) . "</td>
	     <td class=number>" . n($r['S_INFI_LIV1']) . "</td>
		 <td class=number>" . n($r['NUM_ROW_LIV1'], 0) . "</td>
		 <td class=number>" . n($r_perc_nr) . "</td>
		";
 			
 		if ($stampa_colonne_provv == 'Y'){
 			$ret .= "<td class=number>" . n($r['S_IMPONIBILE_PROVV']) . "</td>";
 			$ret .= "<td class=number>" . n($r['S_IMPORTO_PROVV']) . "</td>";
 			if ((float)$r['S_INFI_LIV1'] == 0)
 				$ret .= "<td class=number>" . n(0) . "</td>";
 			else
 				$ret .= "<td class=number>" . n($r['S_IMPORTO_PROVV'] / $r['S_INFI_LIV1'] * 100, 2) . "</td>";
 		}
 			
 		$ret .= "<tr>";


 		if ($livelli == 1)
 			continue;
 			
 		//livello2
 		foreach($r['children'] as $r2){
 				
 			$r2_perc_imp = $r2_perc_nr = 0;
 			if ($tot_imp != 0)
 				$r2_perc_imp = ($r2['S_TIMP'] / $tot_imp) * 100;
 			if ($tot_nr != 0)
 				$r2_perc_nr = ($r2['NUM_ROW'] / $tot_nr) * 100;
 				
 			$ret .= "
 			<tr>";
 			
 			if (substr($stampa_codice, 1, 1) == 'Y')
 				$ret .= "<td>{$r2['CAMPO2_NAME']}</td>";
 			else
 				$ret .= "<td>&nbsp;</td>";
 			
 			
 		   $campo2_des_out = '';	
 		   if ($livello2_as_perc == 'Y')
 		   	$campo2_des_out = n($r2['CAMPO2_DES']) . "%";
 		   else
		   $campo2_des_out = trim($r2['CAMPO2_DES']);
 		   
 		   if ($title == 'Fatturato'){
 		   	$campo2_des_out .= "aaaa";
 		   }
 		   
 		   $ret .= "<td>{$campo2_des_out}</td>"; 		   
		   
		   
 			if ($separa_per_provv == 'Y') $ret .= "<td class=number>" . n($r2['PERC_PROVV']) . "</td>";
 			$ret .= "
 			 <td class=number>" . n($r2['S_TIMP']) . "</td>
			 <td class=number>" . n($r2_perc_imp) . "</td>
	 		 <td class=number>" . n($r2['S_TANT']) . "</td>
 			 <td class=number>" . n($r2['S_INFI']) . "</td>
			 <td class=number>" . n($r2['NUM_ROW'], 0) . "</td>
			 <td class=number>" . n($r2_perc_nr) . "</td>";
 
 
 			if ($stampa_colonne_provv == 'Y'){
 				$ret .= "<td class=number>" . n($r2['S_IMPONIBILE_PROVV']) . "</td>";
 				$ret .= "<td class=number>" . n($r2['S_IMPORTO_PROVV']) . "</td>";
 				if ((float)$r2['S_INFI'] == 0)
 					$ret .= "<td class=number>" . n(0) . "</td>";
 				else
 					$ret .= "<td class=number>" . n($r2['S_IMPORTO_PROVV'] / $r2['S_INFI'] * 100, 2) . "</td>";
 			}
 
 			$ret .= "
			</tr>
		";
 		} //livello2
 			
 	} //livello1
 	return $ret;
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 #DRY: da acs_panel_provvigioni
 function sql_where_by_request($m_params){
 	$sql_where = "";

 	if ($m_params->f_tipologia == 'LIQUIDATO') {
 		$sql_where .= sql_where_by_combo_value('RFTPRG', 'MATURATO');
 		if (strlen($m_params->f_data_da) > 0)
 			$sql_where .= " AND RFDTLQ >= {$m_params->f_data_da}";
 		if (strlen($m_params->f_data_a) > 0)
 			$sql_where .= " AND RFDTLQ <= {$m_params->f_data_a}"; 		
 	} else {
	 	$sql_where .= sql_where_by_combo_value('RFTPRG', $m_params->f_tipologia); 	
	 	if (strlen($m_params->f_data_da) > 0)
	 		$sql_where .= " AND RFDTRG >= {$m_params->f_data_da}";
	 	if (strlen($m_params->f_data_a) > 0)
	 		$sql_where .= " AND RFDTRG <= {$m_params->f_data_a}";
 	}
 	
 	$sql_where .= sql_where_by_combo_value('RFGRAG', $m_params->f_gruppo); 	
 	$sql_where .= sql_where_by_combo_value('RFAGEN', $m_params->f_agenzia);
 	$sql_where .= sql_where_by_combo_value('RFCAGE', $m_params->f_agente);
 	
 	if ($m_params->f_fatture_o_ordini == 'VF')
 		//qui non mostriamo mai le rettifiche
 		$sql_where .= sql_where_by_combo_value('RFTIDO', ['VF']);
 	else
 		$sql_where .= sql_where_by_combo_value('RFTIDO', $m_params->f_fatture_o_ordini); 	
 	
 	
 	$sql_where .= sql_where_by_combo_value('TFCDIV', $m_params->f_divisione); 	
 	
 	$sql_where .= sql_where_by_combo_value('TFTPDO', $m_params->f_tipo_documento);
 	
 	$sql_where .= sql_where_by_combo_value('TFCCON', $m_params->f_cliente_cod); 	
 
 	return $sql_where;
 }
 
 

function get_table_data($form_values, $campo_name, $campo_des, $campo_des_f = null, $top = 0, $campo2_name = null, $campo2_des = null, $campo2_des_f = null, $separa_per_provv = 'N', $tot_per_testata = 'N', $liv_divisione='N'){
  global $cfg_mod_Gest, $main_module, $conn;

  $sql_where = sql_where_by_request($form_values);
  
  
  if ($campo_name == 'SCONTI'){
  	$raggruppa_sconti = 'Y';
  	$campo_name = "TFSCA1";
  	$campo_des = "TFSCA1";
  	$elenco_campi_sconto = array(
  			"TFSCA1", "TFSCA2", "TFSCA3", "TFSCA4", 
  			"TFSC11", "TFSC12", "TFSC13", "TFSC14", "TFMAG1",
  			"TFSC21", "TFSC22", "TFSC23", "TFSC24", "TFMAG2",
  			"TFSC31", "TFSC32", "TFSC33", "TFSC34", "TFMAG3",
  			"TFSC41", "TFSC42", "TFSC43", "TFSC44", "TFMAG4",
  			"TFSC51", "TFSC52", "TFSC53", "TFSC54", "TFMAG5"
  	);
  	$campo_sconti_select = implode(",", $elenco_campi_sconto) . ",";
  	$campo_sconti_group  = ", " . implode(",", $elenco_campi_sconto);  	
  }
 
  $campo2_select = $campo2_group = '';
  if (isset($campo2_name)){
  	$campo2_select = "{$campo2_name} AS CAMPO2_NAME, {$campo2_des} AS CAMPO2_DES,";
 	$campo2_group  = ", {$campo2_name}, {$campo2_des}";
  }
  
  //se divido per tipo ordini aggiungo anche l'indice (da accodare in stampa)
  if ($campo2_name == 'TFTPDO'){
  	$campo2_select .= "TFINUM, ";
  	$campo2_group  .= ", TFINUM";  	
  }
  
  if ($separa_per_provv == 'Y'){ //il secondo livello lo separo per perc di provvigione
  	$campo2_select .= "RFPPRA AS PERC_PROVV,";
  	$campo2_group  .= ", RFPPRA";  	
  }


 if ($liv_divisione == 'Y'){
 	$campo2_select .= "TFCDIV, TFDDIV,";
 	$campo2_group  .= ", TFCDIV, TFDDIV"; 	
 }
  
 $sql_where_in_inner = $sql_where;
 $sql_where = '';
  
  
 $sql_inner_join = "
			INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF ON TF.TFDOCU = RF.RFDOCU  " . $sql_where_in_inner . " 					
 	";
 
 if ($tot_per_testata == 'Y'){
 	
 	$sql_inner_join = "
		  INNER JOIN
		  (
		  		SELECT RFDOCU, SUM(CASE WHEN RFIMPO<>0 THEN RFIMPP ELSE 0 END) AS RFIMPP, SUM(RFIMPO) AS RFIMPO
		  		FROM {$cfg_mod_Gest['provvigioni']['file_righe']}
 				 INNER JOIN {$cfg_mod_Gest['provvigioni']['file_testate']} ON RFDOCU=TFDOCU
 				WHERE 1=1 " . $sql_where_in_inner . "
		  		GROUP BY RFDOCU
		  ) RF ON TF.TFDOCU = RF.RFDOCU 					
 	";
 }
 	
  
  
 $sql = "
 	SELECT {$campo_name} AS CAMPO_NAME, {$campo_des} AS CAMPO_DES, {$campo2_select} {$campo_sconti_select} 
 			COUNT(*) AS NUM_ROW, SUM(TFTIMP) AS S_TIMP, SUM(TFINFI) AS S_INFI, SUM(TFTANT) AS S_TANT,
 			SUM(CASE WHEN RFIMPO<>0 THEN RFIMPP ELSE 0 END) AS S_IMPONIBILE_PROVV, SUM(RFIMPO) AS S_IMPORTO_PROVV
 	FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			{$sql_inner_join}  
 	WHERE 1=1 " . $sql_where . "
 	GROUP BY {$campo_name}, {$campo_des} {$campo2_group} {$campo_sconti_group} 
 	ORDER BY SUM(TFTIMP) DESC, COUNT(*) DESC 	
 ";

 if ($tot_per_testata == 'N'){ 	
 	//se non sto raggruppando per testata, recupero i totali per raggruppamento (senza conteggiare le righe)
 	$sqlTdLiv1 = "
	 	SELECT {$campo_name} AS CAMPO_NAME,
		 	COUNT(*) AS NUM_ROW, SUM(TFTIMP) AS S_TIMP, SUM(TFINFI) AS S_INFI, SUM(TFTANT) AS S_TANT
	 	FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
	 		INNER JOIN
		  (
		  		SELECT RFDOCU, B{$campo_name}
				FROM {$cfg_mod_Gest['provvigioni']['file_testate']} BTF		  		
		  		INNER JOIN {$cfg_mod_Gest['provvigioni']['file_righe']} BRF ON BTF.TFDOCU=BRF.RFDOCU {$sql_where_in_inner}
		  		GROUP BY RFDOCU, B{$campo_name}  
		  ) RF ON TF.TFDOCU = RF.RFDOCU
	 	WHERE 1=1 " . $sql_where . "
	 	GROUP BY {$campo_name}
	 	ORDER BY SUM(TFTIMP) DESC, COUNT(*) DESC
 	";

 	
	 $stmtTdLiv1 = db2_prepare($conn, $sqlTdLiv1);
	 echo db2_stmt_errormsg();
	 $result = db2_execute($stmtTdLiv1);

	 while ($r = db2_fetch_assoc($stmtTdLiv1)){
	 	//decodifico descrizione
	 	
	 	$ar_records_TdLiv1[trim($r['CAMPO_NAME'])] = $r;
	 }
 	
	 
 	
 }	//$tot_per_testata = N		
			
			
 if ($top > 0)
 	$sql .= " FETCH FIRST {$top} ROWS ONLY ";

	 $stmt = db2_prepare($conn, $sql);
 	echo db2_stmt_errormsg();
 	$result = db2_execute($stmt);
 
 	
 	$ar_records = array();
 	while ($r = db2_fetch_assoc($stmt)){
 		
 		if ($raggruppa_sconti == 'Y'){

 			//costruisco la chiave dello sconto (la concatenazione di tutti gli sconti)
 			$ar_campi_sconto = array();
 			foreach($elenco_campi_sconto as $kf){
 				$ar_campi_sconto[] = $r[$kf];
 			}
 			$r['CAMPO_NAME'] = implode("_", $ar_campi_sconto);
 			
 			
 			//nella descrizione metto solo quelli <> 0
 			$ar_campi_sconto_des = array(); 			

 				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSCA1", "TFSCA2", "TFSCA3", "TFSCA4"));
 				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC11", "TFSC12", "TFSC13", "TFSC14"), "TFMAG1");
 				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC21", "TFSC22", "TFSC23", "TFSC24"), "TFMAG2");
 				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC31", "TFSC32", "TFSC33", "TFSC34"), "TFMAG3");
 				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC41", "TFSC42", "TFSC43", "TFSC44"), "TFMAG4");
 				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC51", "TFSC52", "TFSC53", "TFSC54"), "TFMAG5"); 				
 				
 				//rimuovo quelli vuoti?
 				//$ar_campi_sconto_des = array_diff($ar_campi_sconto_des, array("-"));

 			$r['CAMPO_DES'] = implode(" | ", $ar_campi_sconto_des);

 		}

 		if ($tot_per_testata == 'N'){
 		 	$r['S_TIMP_LIV1']  = $ar_records_TdLiv1[trim($r['CAMPO_NAME'])]['S_TIMP'];
	 		$r['S_INFI_LIV1']  = $ar_records_TdLiv1[trim($r['CAMPO_NAME'])]['S_INFI'];
	 		$r['S_TANT_LIV1']  = $ar_records_TdLiv1[trim($r['CAMPO_NAME'])]['S_TANT'];
	 		$r['NUM_ROW_LIV1'] = $ar_records_TdLiv1[trim($r['CAMPO_NAME'])]['NUM_ROW'];
 		} else {
 			$r['S_TIMP_LIV1']  += $r['S_TIMP'];
 			$r['S_INFI_LIV1']  += $r['S_INFI'];
 			$r['S_TANT_LIV1']  += $r['S_TANT'];
 			$r['NUM_ROW_LIV1'] += $r['NUM_ROW']; 			
 		}
 			
 		
 		if ($campo2_name == 'TFTPDO'){
 			$r['CAMPO2_NAME'] .= "&nbsp; [{$r['TFINUM']}]";
 		}
 		
 		$ar_records[] = $r;
 	}
 
	$tot_ar_ret = 0;
	foreach($ar_records as $tot_ar)
		$tot_ar_ret += $tot_ar['NUM_ORD'];
 
	$ar_ret = array();
	foreach($ar_records as $ar1){
		$ar_ret[] = $ar1;
	}
					
	
	
	
  return $ar_ret;		
 } // 
 
 
 function genera_des_sconto($r, $ar_field, $field_maggiorazione = ''){
 	$ret = array(); 
 	foreach ($ar_field as $kf){
 		if ((float)$r[$kf] != 0){
 			$ret[] = "-" . n_auto($r[$kf]);
 		}
 	}
 	
 	if (trim($r[$field_maggiorazione]) != ''){
 		if ((float)$r[$field_maggiorazione] != 0){
 			$ret[] = "+" . n_auto($r[$field_maggiorazione]);
 		} 		
 	}
 	
 	
	if (count($ret) > 0)
 		return implode(" ", $ret);
	else
		return "_";
 }
 
 
 ?>
		