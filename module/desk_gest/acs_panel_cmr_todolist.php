<?php

require_once("../../config.inc.php");
require_once("acs_panel_cmr_include.php");

$main_module = new DeskGest();
$cfg_mod = $main_module->get_cfg_mod();



function sql_add_join_by_request($m_params){
	$main_module = new DeskGest;
	$cfg_mod = $main_module->get_cfg_mod();

	$ret = "";
	if (
			(isset($m_params->f_stato_attivita) && count($m_params->f_stato_attivita) > 0) ||
			trim($m_params->f_filtra_fatture) == 'CON_ATTIVITA' ||
			trim($m_params->f_filtra_fatture) == 'DA_ABBINARE_O_CON_ATTIVITA'
		)
		$ret .= "
		INNER JOIN {$cfg_mod['bollette_doganali']['file_assegna_ord']} ATT_OPEN
		ON ATT_OPEN.ASDOCU = TF.TFDOCU
		";
	return $ret;
}



// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	ini_set('max_execution_time', 300);	
	$m_params = acs_m_params_json_decode();
	global $cfg_mod_Admin, $cfg_mod_Spedizioni;

	$sql = "SELECT ATT_OPEN.*, TF.*, TA_ATTAV.*, NT_MEMO.NTMEMO 
			FROM {$cfg_mod['bollette_doganali']['file_assegna_ord']} ATT_OPEN
			INNER JOIN {$cfg_mod['bollette_doganali']['file_testate']} TF			 
			   ON TFDOCU = ATT_OPEN.ASDOCU
			INNER JOIN {$cfg_mod_Admin['file_tabelle']} TA_ATTAV
			   ON TA_ATTAV.TADT = TF.TFDT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
			/* eventuale nota memo */
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
			   ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0				
			WHERE TA_ATTAV.TARIF1 = 'CM' AND ATT_OPEN.ASFLRI <> 'Y' " . sql_where_by_request($m_params) . "			
			ORDER BY TF.TFDTRG DESC";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->k_cliente));
	
	$ret = array();
	
	while ($row = db2_fetch_assoc($stmt)) {
		
		$n = array();

		$n['data'] 				= $row['ASDTAS'];
		$n['ora'] 				= $row['ASHMAS'];		
		$n['utente'] 			= trim($row['ASUSAS']);
		$n['k_ordine'] 			= trim($row['ASDOCU']);
		$n['fattura'] 			= "[{$row['TFINUM']}] " . implode("_", array($row['TFAADO'], $row['TFNRDO'], $row['TFDT']));		
		$n['cliente']			= trim($row['TFDCON']);
		$n['causale']			= trim($row['ASCAAS']);
		$n['causale_out']		= trim($row['TADESC']);
		$n['note_base']			= acs_u8e(trim($row['ASNOTE']));
		$n['note_estese']		= acs_u8e(trim($row['NTMEMO']));
		$n['utente_assegnato']	= trim($row['ASUSAT']);
		$n['scadenza']			= $row['ASDTSC'];
		$n['rec_stato']			= trim($row['ASFLRI']);
		$n['prog']				= trim($row['ASIDPR']);
		
		$ret[] = $n;
	}
	
	echo acs_je($ret);
	
	
	exit;
} //get_json_data



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();	
?>
{
 success:true, 
 items: [
  <?php write_main_cmr_todolist((array)$m_params); ?> 
 ]
}
<?php exit; } ?>