<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
$puvn = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";



$m_table_config = array(
    'TAID' => 'CUBA',
    'msg_on_save' => true,
    'title_grid' => 'CUBA-Banca',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160, 'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Istituto', 'maxLength' => 60),
        //'TADES2'   => array('label'	=> 'Descrizione 2', 'maxLength' => 30, 'only_view' => 'F'),
        'age'      => array('label'	=> 'Agenzia', 'maxLength' => 60),
        'abi'      => array('label'	=> 'ABI', 'maxLength' => 5, 'c_width' => 60),
        'cab'      => array('label'	=> 'CAB', 'maxLength' => 5,'c_width' => 60),
        'c_cont'   => array('label'	=> 'Conto contabile', 'maxLength' => 9, 'only_view' => 'F'),
        'riba'   => array('label'	=> 'Riba/rete incassi', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'c_int'   => array('label'	=> 'Cod. internazionale', 'maxLength' => 15, 'only_view' => 'F'),
        'c_sia'   => array('label'	=> 'Cod. sia', 'maxLength' => 5, 'only_view' => 'F'),
        'telex'   => array('label'	=> 'Telex', 'maxLength' => 20, 'only_view' => 'F'),
        'c_cor'   => array('label'	=> 'Conto corrente', 'maxLength' => 20, 'only_view' => 'F'),
        'iban'   => array('label'	=> 'Iban', 'maxLength' => 40, 'only_view' => 'F'),
        'sire'   => array('label'	=> 'Siren/Siret', 'maxLength' => 15, 'only_view' => 'F'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'   => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'   => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'   => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli banca',
    'TAREST' => array(
        'age'	=> array(
            "start" => 0,
            "len"   => 60,
            "riempi_con" => ""
        )
       ,'abi'	=> array(
            "start" => 60,
            "len"   => 5,
            "riempi_con" => ""
        )
        ,'cab'	=> array(
            "start" => 65,
            "len"   => 5,
            "riempi_con" => ""
        )
        ,'c_cont'	=> array(
            "start" => 70,
            "len"   => 9,
            "riempi_con" => ""
        )
        ,'riba'	=> array(
            "start" => 79,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'c_int'	=> array(
            "start" => 80,
            "len"   => 15,
            "riempi_con" => ""
        )
        ,'c_sia'	=> array(
            "start" => 95,
            "len"   => 5,
            "riempi_con" => ""
        )
        ,'telex'	=> array(
            "start" => 100,
            "len"   => 20,
            "riempi_con" => ""
        )
        ,'c_cor'	=> array(
            "start" => 120,
            "len"   => 20,
            "riempi_con" => ""
        )
        ,'iban'	=> array(
            "start" => 140,
            "len"   => 40,
            "riempi_con" => ""
        )
        ,'sire'	=> array(
            "start" => 180,
            "len"   => 15,
            "riempi_con" => ""
        )
        
    )
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
