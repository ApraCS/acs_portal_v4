<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_aggiorna'){
    
    $ar_ins = array();
    
    $ar_ins['CLUSUM'] = $auth->get_user();
    $ar_ins['CLDTUM'] = oggi_AS_date();
    $ar_ins['CLHMUM'] = oggi_AS_time();
    
    if(strlen($m_params->form_values->CLLIST) > 0)
        $ar_ins['CLLIST']   = $m_params->form_values->CLLIST;
    if(strlen($m_params->form_values->CLCLIP) > 0)
        $ar_ins['CLCLIP']   = $m_params->form_values->CLCLIP;
    if(strlen($m_params->form_values->CLAGE) > 0)
        $ar_ins['CLAGE']   = $m_params->form_values->CLAGE;
    if(strlen($m_params->form_values->val_ini) > 0)
        $ar_ins['CLDTVI']   = $m_params->form_values->val_ini;
    if(strlen($m_params->form_values->val_fin) > 0)
        $ar_ins['CLDTVF']   = $m_params->form_values->val_fin;
    $ar_ins['CLMOLT']   = sql_f($m_params->form_values->CLMOLT);
    
    if($m_params->form_values->RRN == ''){
        
        $ar_ins['CLUSGE'] = $auth->get_user();
        $ar_ins['CLDTGE'] = oggi_AS_date();
        $ar_ins['CLHMGE'] = oggi_AS_time();
        $ar_ins['CLDT']   = $id_ditta_default;
        $ar_ins['CLCDCF'] = $m_params->cdcf;
    
        $sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_listino_punti']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
            
    }else{
        
     
        $sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_listino_punti']} CL
        SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
        WHERE RRN(CL) = '{$m_params->form_values->RRN}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
        
    }
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql = "DELETE FROM {$cfg_mod_Gest['ins_new_anag']['file_listino_punti']} CL
            WHERE RRN(CL) = '{$m_params->form_values->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'get_json_data_list_p'){
    
    $m_params = acs_m_params_json_decode();
    $ret = array();
    $sql = "SELECT RRN(CL) AS RRN, CL.*
            FROM  {$cfg_mod_Gest['ins_new_anag']['file_listino_punti']} CL
            WHERE CLDT = '{$id_ditta_default}' AND CLCDCF = '{$m_params->cdcf}'";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    
    while ($row = db2_fetch_assoc($stmt)) {
        
        if(trim($row['CLAGE']) != ''){
            $age = get_TA_sys('CUAG', trim($row['CLAGE']));
            $row['agente'] =  "[". trim($row['CLAGE']). "] ".$age['text'];
            
        }

        if(trim($row['CLLIST']) != ''){
            $list = get_TA_sys('BITL', trim($row['CLLIST']));
            $row['listino'] =  "[". trim($row['CLLIST']). "] ".$list['text'];
            $row['CLLIST'] = trim($row['CLLIST']);
        }
        
        if(trim($row['CLCLIP']) != ''){
            $clip = get_TA_sys('CLIP', trim($row['CLCLIP']));
            $row['clip'] =   "[". trim($row['CLCLIP']). "] ".$clip['text'];
        }
        
        $row['val_ini']  = $row['CLDTVI']; //print_date($row['CLDTVI']);
        $row['val_fin']  = $row['CLDTVF']; //print_date($row['CLDTVF']);
        
        $ret[] = $row;
    }
    
    echo acs_je($ret);
    exit;
}

//*************************************************************
if ($_REQUEST['fn'] == 'open_panel'){
//*************************************************************
    $d_cliente = trim($main_module->decod_cliente($m_params->cdcf));
    $text_form = "Dettagli listino punti [{$m_params->cdcf}] {$d_cliente}";
 
    ?>
   {"success":true, "items": [

   {
			xtype: 'panel',
			
			title: 'Listino punti',
        	<?php echo make_tab_closable(); ?>,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
    
    			{
				xtype: 'grid',
				flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		   		store: {
					xtype: 'store',
					autoLoad:true,
					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_list_p',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
			                    extraParams: {
			                     
			                     cdcf : <?php echo j($m_params->cdcf); ?>
									
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['val_ini', 'val_fin', 'agente', 'listino', 'clip', 'CLAGE', 'CLDEST', 'CLLIST', 'CLCLIP', 'CLDTVI', 'CLDTVF', 'CLMOLT', 'RRN']							
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		  {text: 'Listino', flex : 1, dataIndex: 'listino'}
		    		, {text: 'Categoria', flex : 1, dataIndex: 'clip'}
		    		, {text: 'Agente', flex : 1, dataIndex: 'agente'}
		    		, {text: 'Data iniziale', width: 100, dataIndex: 'CLDTVI', renderer: date_from_AS}
		    		, {text: 'Data finale', width: 100, dataIndex: 'CLDTVF', renderer: date_from_AS}
		    		, {text: 'Moltiplicatore', width: 100, dataIndex: 'CLMOLT', renderer: floatRenderer4}
				],
				enableSort: true
				
				
	        	, listeners: {
					   selectionchange: function(selModel, selected) { 
	               
            	               if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            		               rec_index = selected[0].index;
            		               form_dx.getForm().findField('rec_index').setValue(rec_index);
            	                   form_dx.getForm().setValues(selected[0].data);
	                 			}
		       		  	 	 }
		       		  	 	 
				}
	    		
	 	},
	 	
	 	{
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: <?php echo j($text_form); ?>,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.4,
 		            frame: true,
 		            items: [ 
 		             	{xtype : 'textfield', name : 'rec_index', hidden : true},
 		             	{xtype : 'textfield', name : 'RRN', hidden : true},
 		                 {
            			xtype : 'combo',
            		 	name: 'CLLIST', 
            		 	fieldLabel: 'Listino',
            		 	labelWidth : 80,
            		 	forceSelection: true,  							
            			displayField: 'text',
            		    valueField: 'id',							
            			emptyText: '- seleziona -',
            		   	queryMode: 'local',
                 		minChars: 1,	
                 	    anchor: '-15',	
            			store: {
                			editable: false,
                			autoDestroy: true,
                			fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     	<?php echo acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, '', 'Y'), ''); ?>
                		    ]
            			}, listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
                				 }
             			}	
            		 	
            		  },	
            		  
            		    {
            			xtype : 'combo',
            		 	name: 'CLCLIP', 
            		 	fieldLabel: 'Categoria',
            		 	labelWidth : 80,
            		 	forceSelection: true,  							
            			displayField: 'text',
            		    valueField: 'id',							
            			emptyText: '- seleziona -',
            		   	queryMode: 'local',
                 		minChars: 1,	
                 	    anchor: '-15',	
            			store: {
                			editable: false,
                			autoDestroy: true,
                			fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     	<?php echo acs_ar_to_select_json(find_TA_sys('CLIP', null, null, null, null, null, 0, '', 'Y'), ''); ?>
                		    ]
            			}, listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
                				 }
             			}	
            		 	
            		  },  {
            			xtype : 'combo',
            		 	name: 'CLAGE', 
            		 	fieldLabel: 'Agente',
            		 	labelWidth : 80,
            		 	forceSelection: true,  							
            			displayField: 'text',
            		    valueField: 'id',							
            			emptyText: '- seleziona -',
            		   	queryMode: 'local',
                 		minChars: 1,	
                 	    anchor: '-15',	
            			store: {
                			editable: false,
                			autoDestroy: true,
                			fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     	<?php echo acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, '', 'Y'), ''); ?>
                		    ]
            			}, listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
                				 }
             			}	
            		 	
            		  },{
        				   xtype: 'datefield'
        				   , startDay: 1 //lun.
        				   , fieldLabel: 'Data iniziale'
                		   , labelWidth :80
        				   , name: 'val_ini'
        				   , format: 'd/m/Y'
        				   , altFormats: 'Ymd|dmY'
        				   , submitFormat: 'Ymd'
        				   , allowBlank: false
        				   , anchor: '-15'
        				   , listeners: {
        				       invalid: function (field, msg) {
        				       		Ext.Msg.alert('', msg);}
        					}
        				}, {
        				   xtype: 'datefield'
        				   , startDay: 1 //lun.
        				   , fieldLabel: 'Data finale'
                		   , labelWidth :80
        				   , name: 'val_fin'
        				   , format: 'd/m/Y'
        				   , altFormats: 'Ymd|dmY'
        				   , submitFormat: 'Ymd'
        				   , allowBlank: false
        				   , anchor: '-15'
        				   , listeners: {
        				       invalid: function (field, msg) {
        				       		Ext.Msg.alert('', msg);}
        						}
        				}, {xtype: 'numberfield',
    						name: 'CLMOLT',
    						labelWidth: 80,
    						fieldLabel: 'Moltiplicatore',
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						decimalPrecision : 4
    						}
 		            
 		                    
 		                    ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
        		
        		     
        		      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				       Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
										
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
					      	    		 	acs_show_msg_error(jsonData.msg_error);
							        	 	return;
				        			     }else{
        					         		form.getForm().reset();
							       	        grid.getStore().load();	
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
					
					    	}
            	   		  });		    
					        
				        }

			     },  {
                     xtype: 'button',
                    text: 'Reset',
		            iconCls: 'icon-button_black_repeat-16',
		            scale: 'small',	                     

		           handler: function() {
		              var form = this.up('form').getForm();
                	  form.reset();
			
			            }

			     }, '->',
			      
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			var index = form_values.rec_index;
 			       			var record_grid = grid.getStore().getAt(index);
 			       	        if(form.isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				cdcf : <?php echo j($m_params->cdcf); ?>
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error('errore');
					      	    		return false;
					        			}else{
					        			   grid.getStore().load();
					        		   }
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               }
			            }

			     }
			     ]
		   }]
			 	 }  
		 ]
					 
					
					
	}
    
    ]}
    
    <?php 
    
    exit;
}