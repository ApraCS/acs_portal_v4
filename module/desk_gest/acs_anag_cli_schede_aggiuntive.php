<?php
require_once "../../config.inc.php";

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

require_once "acs_anag_cli_schede_agg_{$m_params->tipo_scheda}.php";

if ($_REQUEST['fn'] == 'exe_sos_att'){
    $m_params = acs_m_params_json_decode();
    $ar_upd = array();
    
    $tatp = trim($m_params->row->sosp);
    
    if($tatp == "" || $tatp == "N")
        $new_value = "S";
    else
        $new_value = "";
            
            
    $ar_upd['TAUSUM'] = $auth->get_user();
    $ar_upd['TADTUM'] = oggi_AS_date();
    $ar_upd['TATP']   = $new_value;
            
    $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    WHERE RRN(TA) = '{$m_params->row->rrn}'";
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
            
}


if ($_REQUEST['fn'] == 'exe_mod_scheda'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    $cliente = sprintf("%09s", $m_params->cliente);
    
    $ar_ins = out_ar_ins($form_values, $m_params->row->tarest);
    $ar_ins['TADTUM'] = oggi_AS_date();
    $ar_ins['TAUSUM'] = substr($auth->get_user(), 0, 8);
    
    if(isset($m_params->row->rrn) && $m_params->row->rrn != ''){
            
        $sql = "UPDATE {$cfg_mod_Gest['file_tab_sys']} TA
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(TA) = '{$m_params->row->rrn}'";
            
     }else{
            
         $row = get_TA_sys($m_params->tipo_scheda, $m_params->form_values->riga, $cliente);
                       
            if($row != false && count($row) > 0){
                $ret['success'] = false;
                $ret['msg_error'] = "Codice esistente";
                echo acs_je($ret);
                return;
            }
            
            $ar_ins['TADTGE'] = oggi_AS_date();
            $ar_ins['TAUSGE'] = $auth->get_user();
            
            
            $ar_ins["TADT"] = $id_ditta_default;
            $ar_ins["TAID"] = $m_params->tipo_scheda;
            if($m_params->tipo_scheda != 'XSPM')
                $ar_ins["TACOR1"] = $cliente;
            
            $sql = "INSERT INTO {$cfg_mod_Gest['file_tab_sys']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
        }
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg($stmt);
        
  
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_canc_scheda'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->row->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_Gest['file_tab_sys']} TA WHERE RRN(TA) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}





if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $m_params = acs_m_params_json_decode();
    $ar = array();
    
    $cliente = $m_params->open_request->cliente;
    $tipo_scheda = $m_params->tipo_scheda;
    
    if($tipo_scheda == 'XSPM')
        $sql_where = " AND SUBSTRING(TAREST, 35, 9) = '{$cliente}'";
    else
        $sql_where = " AND TACOR1 = '{$cliente}'";
    
    /*if($tipo_scheda == 'BRCF')
        $sql_where .= " AND SUBSTRING(TAREST, 229, 2) = 'OV'";*/
        
    $sql = " SELECT RRN(TA) AS RRN, TA.*
    FROM {$cfg_mod_Gest['file_tab_sys']} TA
    WHERE TADT = '{$id_ditta_default}' AND TAID = '{$tipo_scheda}' {$sql_where}
    ORDER BY TADTGE DESC";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    while($row = db2_fetch_assoc($stmt)){
  
        $nr = get_array_data($row);
        $nr['TADTGE'] = $row['TADTGE'];
        $nr['TAUSGE'] = $row['TAUSGE'];
        $nr['TADTUM'] = $row['TADTUM'];
        $nr['TAUSUM'] = $row['TAUSUM'];
        $ar[] = $nr;
        
    }
    
    
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    $d_cli = "[".$m_params->cliente."] ". $main_module->decod_cliente($m_params->cliente);
    
    ?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
				items: [
						{
						xtype: 'grid',
						title: '',
						itemId : 'm_grid',
						flex:0.7,
				        loadMask: true,	
				        tbar: new Ext.Toolbar({
	                    items:[ '->',
	                    
	            		{
	            		 //iconCls: 'icon-gear-16',
        	             text: 'AGGIUNGI', 
        		           		handler: function(event, toolEl, panel){
        		           		 var m_grid = this.up('window').down('#m_grid');
        		           		 var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    m_grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					  	 acs_show_win_std('Aggiungi scheda ' + <?php echo j($m_params->tipo_scheda); ?> + ' ' + <?php echo j($d_cli); ?>, '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								 cliente : '<?php echo $m_params->cliente; ?>', tipo_scheda : '<?php echo $m_params->tipo_scheda; ?>'  
							}, 500, 470, my_listeners, 'icon-pencil-16');
        			          
        		           		 
        		           		 }
        		           	 }
	           
	         ]            
	        }),	 
				   		store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>,
										 tipo_scheda : <?php echo acs_je($m_params->tipo_scheda) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: <?php 
		        			$ar_values_std = array('sosp', 'TAUSGE', 'TADTGE', 'TADTUM', 'TAUSUM');
		        			$ar_values = array_merge($ar_values_std, out_fields());
		        			
		        			echo acs_je($ar_values); 
		        			
		        			?>
		        		
			}, //store
				  <?php  $divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
			      columns: [
			      
			       {
	                text: '<?php echo $divieto; ?>',
	                dataIndex: 'sosp',
	                width : 40,
	                tooltip : 'Sospeso',
	                renderer: function(value, metaData, record){
	                      if(record.get('sosp') == 'S') return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=15>';
	                }
	                },{
	                header   : 'Cod.',
	                dataIndex: 'riga',
	                width : 40
	                },
	                
	                <?php echo out_columns(); ?>
	                
	             ,{header: 'Immissione',
            	 columns: [
            	 {header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
            	 renderer: function(value, metaData, record){
            	         if (record.get('TADTGE') != record.get('TADTUM')) 
            	          metaData.tdCls += ' grassetto';
            	 
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return date_from_AS(value);	
            	}
            	 
            	 }
            	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
            	  renderer: function(value, metaData, record){
            	  
            	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
            	          metaData.tdCls += ' grassetto';
            	  
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return value;	
            	} 
            	 }
            	 ]}
			
                 	 
	                
	         ], listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var row = rec.data;
					     var m_grid = this;
					     
					 	 voci_menu.push({
			         		text: 'Cancella',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		
			        		 Ext.Msg.confirm('Richiesta conferma', 'Confermi cancellazione definitiva della registrazione selezionata?' , function(btn, text){
            	   		       if (btn == 'yes'){
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_scheda',
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				tipo_scheda : <?php echo acs_je($m_params->tipo_scheda); ?>
										},							        
								        success : function(result, request){
								        
								   			m_grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
								    
								   }
            	   		          });		    
							  
				                }
			    		});
			    		
			    		 voci_menu.push({
			         		text: 'Modifica',
			        		iconCls : 'icon-pencil-16',          		
			        		handler: function () {
			        		   	var my_listeners = {
    			    		  			afterOkSave: function(from_win, src){
    			    		  			    grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
    					  	
    					  	 acs_show_win_std('Modifica scheda ' + <?php echo j($m_params->tipo_scheda); ?> + ' ' + <?php echo j($d_cli); ?> , '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod', {
								row: row, cliente : '<?php echo $m_params->cliente; ?>', tipo_scheda : '<?php echo $m_params->tipo_scheda; ?>' 
							}, 500, 470, my_listeners, 'icon-pencil-16');
				                }
			    		});		
			    		
			    		 voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				tipo_scheda : '<?php echo $m_params->tipo_scheda; ?>' 
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          rec.set('sosp', jsonData.new_value);	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    	
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	  
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}


if ($_REQUEST['fn'] == 'open_mod'){
    
    $m_params = acs_m_params_json_decode();
    $ar_values = get_values_from_row($m_params->row, $m_params->form_values->f_cliente_cod);
   
    if(isset($m_params->email) && strlen($m_params->email) > 0)
        $ar_values['email'] = $m_params->email;
    if(isset($m_params->form_values->f_cliente_cod) && strlen($m_params->form_values->f_cliente_cod) > 0)
        $ar_values['cliente'] = sprintf("%09s", $m_params->form_values->f_cliente_cod);
    if(isset($m_params->form_values->f_pven_cod) && strlen($m_params->form_values->f_pven_cod) > 0)
        $ar_values['dest1'] = $m_params->form_values->f_pven_cod;
    if(isset($m_params->form_values->f_denominazione) && strlen($m_params->form_values->f_denominazione) > 0)
        $ar_values['descrizione'] = $m_params->form_values->f_denominazione;
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            layout: {pack: 'start', align: 'stretch'},
	            items: [
	               <?php echo out_component_form($ar_values, $m_params)?>
 		       ],
	            
				buttons: [					
					{
			            text: 'Salva',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var form = this.up('form');
			            	var loc_win = this.up('window');
			            	
			            	 if(form.getForm().isValid()){	
			                Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_scheda',
						        jsonData: {
						        	form_values : form.getValues(),
						        	row : <?php echo acs_je($m_params->row) ?>,
						        	cliente : <?php echo j($m_params->cliente); ?>,
						        	tipo_scheda : '<?php echo $m_params->tipo_scheda ?>'
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						          var jsonData = Ext.decode(result.responseText);
						          
						          if(jsonData.success == false){
				      	    		acs_show_msg_error(jsonData.msg_error);
							        	return;
				        		  }else{
					          		loc_win.fireEvent('afterOkSave', loc_win, jsonData);
						          }		            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
						    }
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	exit;
   }
   

