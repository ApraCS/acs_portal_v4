<?php

require_once("../../config.inc.php");
$main_module = new DeskGest();

$m_params = acs_m_params_json_decode();

$all_params = array();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());

// ******************************************************************************************
// FORM APERTURA per ricerca anagrafica clienti/fornitori
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
    ?>

{"success":true, "items": [

  { xtype: 'form'
  , bodyStyle: 'padding: 10px'
  , bodyPadding: '5 5 0'
  , frame: true
  , title: ''
  , flex: 1
  , layout:
      { type: 'vbox'
      , align: 'stretch'
      }
  , autoScroll: true

  , items: [
      { xtype: 'fieldcontainer'
	  , layout:
	      {	type: 'hbox'
  		  , pack: 'start'
    	  }
      , defaults:
          { labelWidth: 90
          , margin: '0 15 0 0'}
		  , items: [
  		      { name: 'f_codice'
  			  , xtype: 'textfield'
  			  , fieldLabel: 'Codice'
  			  , displayField: 'text'
  			  , labelWidth: 45
  			  , maxLength: 3
  			  , width: 90
  			  }
            , { name: 'f_desc'
  		      , xtype: 'textfield'
  		      , fieldLabel: 'Descrizione'
  		      , maxLength: 60
  		      , displayField: 'text'
  		      , labelAlign : 'right'
  		      , labelWidth: 70
  			  , flex : 1
              }
            , { name: 'f_age'
  		      , xtype: 'textfield'
  		      , fieldLabel: 'Agenzia'
  		      , maxLength: 60
  		      , displayField: 'text'
  		      , labelAlign : 'right'
  		      , labelWidth: 70
  			  , flex : 1
              }
              ,{
                xtype: 'textfield',
				name: 'f_abi',
				fieldLabel: 'ABI/CAB',
			    maxLength: 5,
			    labelWidth: 50,
			    labelAlign : 'right',
			    margin : 0,
  			    width: 100							
			  },{
				xtype: 'textfield',
				name: 'f_cab',
				fieldLabel: '',
			    maxLength: 5,
			    width: 45,						
			  }
            , { xtype: 'button'	
			  , text: 'Ricerca'
			  , margin: "0 0 10 5"
			  , anchor: '-15'
			  , width: 70
			  , handler: function() 
			      { l_form = this.up('form').getForm();
            		l_grid = this.up('window').down('grid');
					l_grid.store.proxy.extraParams.form_values = l_form.getValues();
            		l_grid.store.load();
            		l_grid.getView().refresh();
            		            			   
	              }  
	          },
	           { xtype: 'button'	
			  , text: 'Aggiungi'
			  , margin: "0 0 10 5"
			  , anchor: '-15'
			  , width: 70
			  , handler: function() 
			      { l_form = this.up('form').getForm();
            		l_grid = this.up('window').down('grid');
            		
    			   	var my_listeners = {
    		  			afterSave: function(from_win, new_rec){
    		  			    l_grid.getStore().add(new_rec);
    					    var rec_index = l_grid.getStore().findRecord('codice', new_rec.codice);
	  					 	l_grid.getView().select(rec_index);
    			  			l_grid.getView().focusRow(rec_index);
    						from_win.close();  
			        		}
	    				};
            		
					acs_show_win_std('Dettagli banca', 'acs_tab_sys_CUBA.php?fn=open_panel', {hide_grid : 'Y', from_ricerca : 'Y', aggiungi : 'Y'}, 410, 480, my_listeners, 'icon-leaf-16');         	          	 
            		            			   
	              }  
	          },
	          
	          
            ] 
      } <!-- end fieldcontainer -->
      
	, { xtype: 'grid'
      , loadMask: true
      , store:
          { xtype: 'store'
          , autoLoad: false	
          , proxy:
              { url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_ricerca_tab'
              , method: 'POST'
              , type: 'ajax'
              , extraParams: 
                  { form_values: ''
                  , taid: '<?php echo $m_params->taid ?>'}
              , doRequest: personalizza_extraParams_to_jsonData
              , actionMethods: { read: 'POST'}
	          , reader: 
	              { type: 'json'
	              , method: 'POST'
	              , root: 'root'
	              } <!-- end reader -->			
              } <!-- end proxy -->	
          , fields: ['codice', 'descrizione', 'sosp', 'abi', 'cab', 'age']
          } <!-- end store -->
      , columns: [
         <?php  $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
         {text: '<?php echo $sosp; ?>', width: 40, dataIndex: 'sosp', 
		  tooltip: 'Sospeso',
	      renderer: function(value, metaData, record){
  			  if(record.get('sosp') == 'S') return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	   
		  }},
          { header: 'Codice'
          , dataIndex: 'codice'
          , width: 50
          }
          , { header: 'Istituto'
          , dataIndex: 'descrizione'
          , flex: 1
          }, { header: 'Agenzia'
          , dataIndex: 'age'
          , flex: 1
          }, { header: 'ABI'
          , dataIndex: 'abi'
          , width: 50
          }, { header: 'CAB'
          , dataIndex: 'cab'
          , width: 50
          }
        ]	
      , listeners: 
          { celldblclick: {
                fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		        rec = iView.getRecord(iRowEl);
		       
		        var loc_win = this.up('window');
		       
		        if(rec.get('sosp') == 'S'){
		        	acs_show_msg_error('Selezionare fornitori non sospesi');
    				return false;
		        }else{
		        	loc_win.fireEvent('afterSelected', loc_win, rec.data);
		        }
		        
		        
		      }		
          } ,
            itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
			  													  
			    var voci_menu = [];
		        var row = rec.data;
	   	        var m_grid = this; 
	   	        
	   	        	   voci_menu.push({
			         		text: 'Modifica',
			        		iconCls : 'icon-pencil-16',          		
			        		handler: function () {
			        		   	var my_listeners = {
    			    		  			afterSave: function(from_win, new_rec){
    			    		  			    rec.set(new_rec);
    			    		  			  	from_win.close();  
    						        		}
    				    				};
    					  acs_show_win_std('Dettagli banca', 'acs_tab_sys_CUBA.php?fn=open_panel', {autoload : 'firstRec', hide_grid : 'Y', tanr : rec.get('codice'), from_ricerca : 'Y', modifica : 'Y'}, 410, 480, my_listeners, 'icon-pencil-16');         	          	
				                }
			    		});	
					     
			          var menu = new Ext.menu.Menu({
			            items: voci_menu
					  }).showAt(event.xy);	
					     
		     }
          }    
      } <!-- end grid -->		      
	      
	]
  } <!-- end form -->

]}		

<?php exit; }
/*--------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------*/
if ($_REQUEST['fn'] == 'get_ricerca_tab'){
    global $cfg_mod_DeskArt, $id_ditta_default;
	
	$taid    = $m_params->taid;
	$codice  = $m_params->form_values->f_codice; 
	$desc    = $m_params->form_values->f_desc;
	$agenzia = $m_params->form_values->f_age;
	$abi     = $m_params->form_values->f_abi;
	$cab     = $m_params->form_values->f_cab;
	$sql_where = '';
	if (isset($codice) && strlen($codice)>0) 
	    $sql_where .= " AND TANR LIKE '%{$codice}%'";
    if (isset($desc) && strlen($desc)>0) 
        $sql_where .= " AND CONCAT(TADESC, TADES2) LIKE '%".strtoupper($desc)."%'";
    if (isset($agenzia) && strlen($agenzia)>0)
        $sql_where .= " AND CONCAT(SUBSTRING(TAREST, 1, 30), SUBSTRING(TAREST, 31, 30)) LIKE '%".strtoupper($agenzia)."%'";
    if (isset($abi) && strlen($abi)>0)
        $sql_where .= " AND SUBSTRING(TAREST, 61, 5) LIKE '%{$abi}%'";
    if (isset($cab) && strlen($cab)>0)
        $sql_where .= " AND SUBSTRING(TAREST, 66, 5) LIKE '%{$cab}%'";
	$ret = array();
    $sql = "SELECT TATP, TANR, TADESC, TADES2, TAREST 
	        FROM {$cfg_mod_Gest['file_tab_sys']}
	        WHERE TADT = '{$id_ditta_default}' AND TAID = '{$taid}' {$sql_where}
	        /*fetch first 100 rows only*/";
	//print_r($sql); exit;
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	while ($r = db2_fetch_assoc($stmt)){
	   
	    $r['sosp']        = trim($r['TATP']);
	    $r['codice']      = trim($r['TANR']);
	    $r['descrizione'] = trim($r['TADESC']).trim($r['TADES2']);
	    $r['abi']     = substr($r['TAREST'], 60, 5);
	    $r['cab']     = substr($r['TAREST'], 65, 5);
	    $r['age'] = substr($r['TAREST'], 0, 60);
	    $ret[] = $r;
	    //print_r($r); exit;
	}
	
	echo acs_je($ret);
exit; }
