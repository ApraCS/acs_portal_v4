<?php

require_once("../../config.inc.php"); 
require_once("acs_panel_fatture_anticipo_include.php");

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));

//elenco nazioni combo
function get_ar_nazioni(){
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFNAZI, TFDNAZ
	FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFTIDO='VA'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['TFNAZI']), "text" => $r['TFDNAZ']);
	}
	return $ar;
}

function get_ar_tipo_documento(){
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFTPDO AS COD, TFDTPD AS DES 
	FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']} 
	WHERE TFTIDO='VA'
	GROUP BY TFTPDO, TFDTPD ORDER BY TFDTPD";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES'] . " [" . trim($r['COD'] . "]"));
	}
	return $ar;
}


if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();
	
	$ar=crea_ar_tree_fatture_anticipo($_REQUEST['node'], $m_params->open_request->form_values);
	
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}

	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}


// ******************************************************************************************
// INVIO A GESTIONALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_send_to_gest'){
	$m_params = acs_m_params_json_decode();

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $id_ditta_default);
	$cl_p .= sprintf("%-3s", '*ST');

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31P1('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31P1', $libreria_predefinita_EXE, $cl_in, null, null);
		
		//$call_return = $tkObj->PgmCall('UTIBR31G5', $libreria_predefinita_EXE, '', null, null);
	}

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

// ******************************************************************************************
// genera fattura anticipo FORNITORE (DA CHECK FATTURE)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_genera_fattura_anticipo_fornitore'){
    $m_params = acs_m_params_json_decode();
    
    $use_session_history = microtime(true);
    
    
    foreach($m_params->form_values as $k => $v){
        
        if (substr($k, 0, 7) == 'f_text_'){
            
            $sh = new SpedHistory($m_DeskAcq);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'GEN_FAT_ANTG',
                    "use_session_history" 	=> $use_session_history,
                    "vals" => array(
                        "RICITI" => substr($k, 7, 1),
                        "RIDART" => $v
                    )
                    
                )
                );
            
        }
    }
    
    $sh = new SpedHistory($m_DeskAcq);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'GEN_FAT_ANTG',
            "end_session_history" 	=> $use_session_history,
            "vals" => array(
                "RICVES" => $m_params->fornitore,
                "RIDTEP" => (int)$m_params->form_values->f_data_reg,
                "RIIMPO" => sql_f($m_params->form_values->f_importo),
                "RIDART" => $m_params->form_values->f_rif
            )
        )
        );
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
// genera fattura anticipo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_genera_fattura_anticipo'){
	$m_params = acs_m_params_json_decode();

	$use_session_history = microtime(true);
	
	
	foreach($m_params->form_values as $k => $v){
	    
	    if (substr($k, 0, 7) == 'f_text_'){
	        
	       $sh = new SpedHistory();
	       $sh->crea(
	            'pers',
	            array(
	                "messaggio"	=> 'GEN_FAT_ANTG',
	                "k_ordine" => $m_params->k_ordine,
	                "use_session_history" 	=> $use_session_history,
	                "vals" => array(
	                    "RICITI" => substr($k, 7, 1),
	                    "RIDART" => $v
	                )
	                
	            )
	            );
	        
	    }
	}

	$sh = new SpedHistory();
	$sh->crea(
			'pers',
			array(
					"messaggio"	=> 'GEN_FAT_ANTG',
					"k_ordine" => $m_params->k_ordine,
			        "end_session_history" 	=> $use_session_history,
					"vals" => array(
							"RICITI" => $m_params->form_values->f_tipo_doc,
					        "RIDTEP" => (int)$m_params->form_values->f_data_reg,
					        "RIIMPO" => sql_f($m_params->form_values->f_importo),
					        "RIDART" => $m_params->form_values->f_rif
					)
			)
			);


	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}

####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TFCCON, TFDCON
	FROM {$cfg_mod['fatture_anticipo']['file_testate']}
	WHERE UPPER(
	REPLACE(REPLACE(TFDCON, '.', ''), ' ', '')
	) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
					GROUP BY TFCCON, TFDCON
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array("cod"=> trim($row['TFCCON']),"descr"=> acs_u8e(trim($row['TFDCON'])));
	}

	echo acs_je($ret);

	exit;
}




if ($_REQUEST['fn'] == 'open_parameters'){
	?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "FATTURE_ANTICIPO");  ?> {
			            text: 'Aggiorna',
			            iconCls: 'icon-button_black_repeat_dx-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=exe_send_to_gest', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }, {
	            text: 'Rapporto<br>sincronizzazione',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
	         
							acs_show_panel_std('acs_panel_rapporto_sincronizzazione.php?fn=open_tab', 'panel_fatture_anticipo', {chiave: 'ANT_REPORT'});
							this.up('window').close();
			
	            }
	        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Report',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_fatture_anticipo_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        }, {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=open_tab', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }],   		            
		            
		            items: [
		         	  	{
           				 xtype: 'combo',
						name: 'f_cliente_cod',
						fieldLabel: 'Cliente',
						minChars: 2,			
           				 margin: "20 20 0 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		$cfg_mod = $main_module->get_cfg_mod();
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }, {
							name: 'f_nazione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Nazione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_nazioni(), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_tipo_documento',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Tipo fattura anticipo',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_tipo_documento(), ""); ?>	
								    ] 
							}						 
						}, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "10 20 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "10 20 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
						
						
						,{
							name: 'f_filtra_fatture',
							xtype: 'radiogroup',
							fieldLabel: 'Fatture',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "20 10 10 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Tutte'
		                          , inputValue: 'T'
		                          , width: 50
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Chiuse'
		                          , inputValue: 'C'
		                          , checked: false
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Aperte'
		                          , inputValue: 'A'
		                          , checked: true
		                        }]
						}, 
						
							{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {
	                     xtype: 'button',
		            	 scale: 'large',
		            	// iconCls: 'icon-print-32',
	                     text: 'Anticipi da generare',
	                     flex:1,
	                     margin : '10px',
				         handler: function() {
				            
			                this.up('form').down('#id_tido').setValue('RA');
			                var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=open_tab', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();
			                
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 //iconCls: 'icon-print-32',
	                     text: 'Anticipi da chiudere',
	                     flex:1,
	                     margin : '10px',
				         handler: function() {
				            
			               this.up('form').down('#id_tido').setValue('VA');
			           		var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=open_tab', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();			            
				            
				         }
				        },
				        {
 							name: 'tftido',
 							itemId : 'id_tido',
 							xtype: 'textfield',
 							hidden: true,
 							value: '',
 							anchor: '-15'							
 									 }
						
						]}
						
				
										 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}
	]
}		
	
		
<?php
	exit;
}



if ($_REQUEST['fn'] == 'open_tab'){
	$request = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	?>

{"success":true, "items": [

        {
            xtype: 'treepanel',
	        title: 'Fatturazione anticipi',
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Elenco fatture anticipo da generare</b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),             
			
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'codice', 'rif', 'data', 'TFTOTD', 'TFTIMP', 'detratto', 'residuo', 
				    'stato', 'liv', 'liv_cod_qtip', 'k_ordine_fatt'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
					
                        extraParams: {
                                    open_request: <?php echo acs_je($request) ?>
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData, 
								

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }       				
                    }

                }),
				

		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            flex: 3,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Nazione/Cliente/Fattura di anticipo'
			        }, { 
			            dataIndex: 'codice',
			            header: 'Codice', 
			            flex: 1,
			            renderer: function(value, metaData, record){
						
					    	if (record.get('liv_cod_qtip') != ''){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('liv_cod_qtip')) + '"';			    	
					    	}						
						
						 return value;
						}
			        },{ 
			            dataIndex: 'data',
			            header: 'Data', 
			            renderer: date_from_AS,
			            width: 80
			        }, { 
			            dataIndex: 'rif',
			            header: 'Riferimento', 
			            flex: 1
			        }, { 
			            dataIndex: 'TFTOTD',
			            header: ' Totale documento', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, { 
			            dataIndex: 'TFTIMP',
			            header: 'Totale imponibile', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, { 
			            dataIndex: 'detratto',
			            header: 'Importo detrazioni', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },  { 
			            dataIndex: 'residuo',
			            header: 'Residuo', 
			            flex: 1,
			            align: 'right', 
			            //coloro la cella
			            renderer: function (value, metaData, record, row, col, store, gridView){

                               /* if (parseFloat(record.get('residuo')) > 0){
                                    metaData.tdCls += ' sfondo_rosso';
                                    }*/
                            //ritorno il valore da visualizzare (con eventuale formattazione data, numero, ....)
                            return floatRenderer2(value); }
			        },{ 
			            dataIndex: 'stato',
			            header: 'Stato', 
			            align: 'center',
			            width: 80,
			            renderer: function(value, metaData, record){
			            
						    	if (record.get('stato') == 'C') 
						    	return '<img src=<?php echo img_path("icone/48x48/lock_grey.png") ?> width=18>';
						    	    	
								if (record.get('stato') == 'M') 
								return '<img src=<?php echo img_path("icone/48x48/lock.png") ?> width=18>';	
											
						    }
			        }
			     
	         ],	
	         listeners: {
	          
	            beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                load: function () {
                          Ext.getBody().unmask();
                        },
                     
                 itemcontextmenu : function(grid, rec, node, index, event) {
		  		 
		  		 event.stopEvent();
		  		
			  	id_selected = grid.getSelectionModel().getSelection();
			  	list_selected_id = [];
			  	for (var i=0; i<id_selected.length; i++) 
				   list_selected_id.push({k_ordine_bolla: id_selected[i].get('k_ordine_bolla')});
		  														  
				  var voci_menu = [];
				  
				if (rec.get('liv') == 'liv_3'){
				
				voci_menu.push({
	         		text: 'Genera fattura anticipo',
	        		iconCls : 'icon-credit_cards-16',          		
	        		handler: function() {
	        		
	        		  my_listeners = {
		        					afterSave: function(from_win){	
		        					    grid.store.treeStore.load();
		        						from_win.close();
						        	}								  
								  }	
	        			
	        			acs_show_win_std('Genera fattura anticipo', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_genera_fattura', {
			        				k_ordine_fatt: rec.get('k_ordine_fatt'),
			        				f_forn: rec.parentNode.get('codice'),
			        				riferimento: rec.get('rif'),
			        				tot_doc : rec.get('TFTOTD')
			        				}, 400, 400, my_listeners, 'icon-credit_cards-16');
			        		}
				    	});
				 }
				  
				  
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    			
			  },     
                                            
                   }
	         , viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }																				  			
	            
        }  

]
}

<?php exit; }
// ******************************************************************************************
// _anticipo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_genera_fattura'){
	$m_params = acs_m_params_json_decode();

	if (isset($m_params->f_forn))
	  $fornitore = $m_params->f_forn;
	else
	  $fornitore = $m_params->form_values->f_forn;
	
	$sql = "SELECT CFRGS1 FROM {$cfg_mod_Spedizioni['file_anag_cli']} CF
            WHERE CFDT = '{$id_ditta_default}' AND CFCD = '{$fornitore}'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);
	$desc_forn = $row['CFRGS1'];
	
	

	?>
 {"success":true, "items": [
        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            
					buttons: [{
			            text: 'Genera',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var form_values = form.getValues();
							 var loc_win = this.up('window');
							 
						 <?php if($m_params->from_check_fat != 'Y'){?>
						if( form_values.f_importo > <?php echo $m_params->tot_doc; ?>){
	           				acs_show_msg_error('Importo superiore al totale documento');
	              			return false;
		           				}
							<?php }?> 
							
							 
								if(form.isValid()){
									Ext.Ajax.request({
									
									<?php if($m_params->from_check_fat == 'Y'){?>
										url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_fattura_anticipo_fornitore',
									<?php }else{?>
									    url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_fattura_anticipo',
									 <?php }?>       
									        jsonData: {
									        	form_values: form.getValues(),
									        	fornitore : '<?php echo $fornitore; ?>',
									        	k_ordine : '<?php echo $m_params->k_ordine_fatt; ?>'
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){	            	  													        
									            var jsonData = Ext.decode(result.responseText);
									          	loc_win.fireEvent('afterSave', loc_win);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									            console.log('errorrrrr');
									        }
									    });	  
							    }							 
				
			            }
			         }],   		            
		
		            items: [   
		            <?php if($m_params->from_check_fat != 'Y'){?>
		            		{
								name: 'f_tipo_doc',
								xtype: 'combo',
								flex: 1,
				            	anchor: '-15',
								fieldLabel: 'Tipo documento',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: false,
				 			    labelWidth: 120,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [
									    <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'VF', null, null, 0, " AND SUBSTR(TAREST, 17, 1) = 'A'"), '') ?>
									    ]
								}
					
							},
							<?php }else{?>
							{xtype : 'displayfield',
							fieldLabel : 'Fornitore',
							value : '<?php echo $desc_forn; ?>',
						    labelWidth: 120
							},
							
							
							<?php }?>
							
								{
							     name: 'f_data_reg'
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data registrazione'
							   , labelAlign: 'left'
							   , labelWidth: 120
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
							 }
							},{
							xtype: 'numberfield',
							hideTrigger: true,
							fieldLabel: 'Importo',
							name: 'f_importo',
							labelWidth: 120,
							anchor: '-15',
							value: '<?php echo $m_params->tot_doc; ?>'
							},
							{
							xtype: 'textfield',
							fieldLabel: 'Riferimento',
							name: 'f_rif',
							labelWidth: 120,
							maxLength: 30,
							anchor: '-15',
							value: '<?php echo $m_params->riferimento; ?>'
							},	
							
									 {
					xtype: 'fieldset',
	                title: 'Annotazioni',
	                layout: 'anchor',
	                flex:1,
	                items: [
						<?php for($i=1; $i<= 5;$i++){ ?>
                	
                	{
						name: 'f_text_<?php echo $i ?>',
						xtype: 'textfield',
						fieldLabel: '',
						anchor: '-15',
					    maxLength: 80,					    
					    //value: <?php echo j(trim($commento_txt[$i])); ?>,							
					},	
					
					<?php }?>	
								
									
					 
	             ]}
								 
		           			]
		              }
		
			]
}	
	
	
	
<?php 
exit;
}
