<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>";
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";

$m_table_config = array(
    'msg_on_save' => true,
    'TAID' => 'XSPM',
    'title_grid' => 'XSPM-Sconti variante',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 35, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160, 'c_width' => 40, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 60),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30, 'only_view' => 'F'),
        'TACOR1'   => array('label'	=> 'Listino','only_view' => 'F', 'xtype' => 'combo_tipo', 'allowBlank' => 'false'),
        'TACOR2'   => array('label'	=> 'Valuta', 'only_view' => 'F', 'xtype' => 'combo_tipo', 'allowBlank' => 'false'),
        'sc1'      => array('label'	=> 'Sconto 1', 'maxLength' => 5, 'only_view' => 'F', 'xtype' => 'number'),
        'sc2'      => array('label'	=> 'Sconto 2', 'maxLength' => 5, 'only_view' => 'F', 'xtype' => 'number'),
        'sc3'      => array('label'	=> 'Sconto 3', 'maxLength' => 5, 'only_view' => 'F', 'xtype' => 'number'),
        'sc4'      => array('label'	=> 'Sconto 4', 'maxLength' => 5, 'only_view' => 'F', 'xtype' => 'number'),
        'azz_sc'   => array('label'	=> 'Azzera', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'val_ini'  => array('label'	=> 'Val. iniziale', 'xtype' => 'datefield', 'c_width' => 70),
        'val_fin'  => array('label'	=> 'Val. finale', 'xtype' => 'datefield','c_width' => 70),
        'check_data'  => array('label'	=> 'Check data', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        // 'cliente'  => array('label' => 'Cliente', 'only_view' => 'C'),
        'gr_doc'  => array('label' => 'Gruppo doc.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        // 'var1'    => array('label' => 'Variabile 1', 'xtype' => 'combo_tipo'),
        // 'van1'    => array('label' => 'Variante 1',  'xtype'  => 'combo_tipo'),
        // 'var2'    => array('label' => 'Variabile 2', 'xtype' => 'combo_tipo'),
        // 'van2'    => array('label' => 'Variante 2',  'xtype' => 'combo_tipo'),
        'TAREST'      => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'         => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli sconti per variante',
    'TAREST' => array(
        'sc1'	=> array(
            "start" => 0,
            "len"   => 4,
            "riempi_con" => ""
        )
        ,'sc2'	=> array(
            "start" => 4,
            "len"   => 4,
            "riempi_con" => ""
        )
        ,'sc3'	=> array(
            "start" => 8,
            "len"   => 4,
            "riempi_con" => ""
        )
        ,'sc4'	=> array(
            "start" => 12,
            "len"   => 4,
            "riempi_con" => ""
        )
        ,'azz_sc'	=> array(
            "start" => 16,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'val_ini'	=> array(
            "start" => 17,
            "len"   => 8,
            "riempi_con" => ""
        )
        ,'val_fin'	=> array(
            "start" => 25,
            "len"   => 8,
            "riempi_con" => ""
        )
        ,'check_data' => array(
            "start" => 33,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'cliente' => array(
            "start" => 34,
            "len"   => 9,
            "riempi_con" => ""
        )
        ,'gr_doc' => array(
            "start" => 43,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'var1' => array(
            "start" => 46,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'van1' => array(
            "start" => 50,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'var2' => array(
            "start" => 53,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'van2' => array(
            "start" => 57,
            "len"   => 3,
            "riempi_con" => ""
        )
    )
    
);

//ROOT_ABS_PATH.
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
