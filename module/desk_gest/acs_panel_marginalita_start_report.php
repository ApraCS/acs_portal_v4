<?php

require_once "../../config.inc.php";
ini_set('max_execution_time', 300);

$s = new Spedizioni();
$main_module = new Spedizioni();

	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);

	
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div#my_content h1{font-size: 22px; padding: 5px;}
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}
	   

   /*tr.liv_totale td{background-color: #cccccc; font-weight: bold;}*/
   tr.liv_totale td{font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
      
	/* ANALISI BILANCIO */
    TR.D td{background-color: #C0C0C0; padding-top: 3px; padding-bottom: 3px; }
	TR.S1 td{background-color: #99CCCC; padding-top: 3px; padding-bottom: 3px; }
	TR.S2 td{background-color: #99B3CC; font-weight: bold; padding-top: 4px; padding-bottom: 4px;}
	TR.dett .tdl{background-color: #FFFFE0; font-weight: normal;}
	TR td.tot{font-weight: bold;}      
    table.int1 th {font-weight: bold;}  
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {

	//apertura automatica finestra invio email
	<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
		Ext.get('p_send_email').dom.click();
	<?php } ?>
	
	});    
    

  </script>


 </head>
 <body>
 
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>


<?php

//valori da tree/grid
$extraParamsValueConverted =	strtr($_REQUEST['extraParams'], array('\"' => '"'));
$extraParamsValue = (object)json_decode($extraParamsValueConverted);


//recupero dati

//QUERY SU CONTI
$sql_where = " AND D.NOME_ANALISI = " . sql_t($extraParamsValue->open_tab_filters->nome_analisi_1);
if (count($extraParamsValue->f_ditta) > 0){
	$sql_where .= " AND D.CODICE_DITTA IN(" . sql_t_IN($extraParamsValue->open_tab_filters->f_ditta) .")";
}

//recupero elenco ditte (ogni ditta sara' in una colonna)
$sql = "SELECT D.DITTA, D.CODICE_DITTA, R1001.IMPORTO_A
		FROM {$cfg_mod_Gest['abi']['file_conti']} D
		LEFT OUTER JOIN (SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']}) R1001
		ON D.CODICE_DITTA = R1001.CODICE_DITTA
		AND R1001.VOCE_ANALISI = '1001'
		AND R1001.NOME_ANALISI = " . sql_t($extraParamsValue->open_tab_filters->nome_analisi_1) . "
		WHERE 1=1 {$sql_where}
		GROUP BY D.CODICE_DITTA, D.DITTA, R1001.IMPORTO_A
		ORDER BY R1001.IMPORTO_A DESC";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$ar_ditte = array();

while ($r = db2_fetch_assoc($stmt)) {
	$ar_ditte[trim($r['CODICE_DITTA'])] = trim($r['DITTA']);
}


//aggiungo Totale
$ar_ditte['TOT'] = 'TOTALE';


//recuper la somma dei record 1001 (a cui fanno riferimento le percentuali)
$sql = "SELECT SUM(IMPORTO_A) AS TOT_1001 FROM {$cfg_mod_Gest['abi']['file_conti']} D WHERE VOCE_ANALISI = '1001' {$sql_where}";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$r = db2_fetch_assoc($stmt);
$tot_1001 = $r['TOT_1001'];


$sql = "SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']} D WHERE 1=1 {$sql_where} ORDER BY ORDINAMENTO";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$ar = array();

while ($r = db2_fetch_assoc($stmt)) {

	$id_voce = trim($r['ORDINAMENTO']);

	if (!isset($ar[$id_voce])){
		$ar_n = array();
		$ar_n['id_voce'] 	= $id_voce;
		$ar_n['cod_voce'] = trim($r['VOCE_ANALISI']);
		$ar_n['voce'] 	= trim(acs_u8e($r['DES_VOCE_ANALISI']));
		$ar_n['codice_voce'] = trim($r['VOCE_ANALISI']);
		$ar_n['tipo_voce'] 	= trim($r['TIPO_VOCE']);
		$ar_n['ditta'] = array();
		$ar[$id_voce] = $ar_n;
	}

	//creo il record per la ditta
	$ar_voce_ditta = array();
	$ar_voce_ditta['IMPORTO_A'] = $r['IMPORTO_A'];
	$ar_voce_ditta['IMPORTO_B'] = $r['IMPORTO_B'];
	$ar_voce_ditta['IMPORTO_C'] = $r['IMPORTO_C'];
	$ar_voce_ditta['IMPORTO_D'] = $r['IMPORTO_D'];
	$ar_voce_ditta['IMPORTO_E'] = $r['IMPORTO_E'];
	$ar_voce_ditta['IMPORTO_F'] = $r['IMPORTO_F'];
	$ar_voce_ditta['IMPORTO_G'] = $r['IMPORTO_G'];
	$ar_voce_ditta['IMPORTO_H'] = $r['IMPORTO_H'];
	$ar[$id_voce]['ditta'][trim($r['CODICE_DITTA'])] = $ar_voce_ditta;

	//aggiorno riga di totale
	$ar[$id_voce]['ditta']['TOT']['IMPORTO_A'] += $r['IMPORTO_A'];
		
	if (isset($tot_1001) && $tot_1001 != 0)
		$ar[$id_voce]['ditta']['TOT']['IMPORTO_B'] = $ar[$id_voce]['ditta']['TOT']['IMPORTO_A'] / $tot_1001 * 100;


} //while $row

foreach($ar as $kar => $r)
	$ar_ret[] = array_values_recursive($ar[$kar]);


echo "<pre>";
//print_r($ar_ret);
echo "</pre>";

// - recupero dati

?>




 <div id='my_content'>
 
 
 <h1>Analisi margine di contribuzione</h1>
 <h2>&nbsp;[Periodo: <?php echo $extraParamsValue->open_tab_filters->nome_analisi_1; ?>]</h2>
 <div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
 <br/>
 

 	<table class="int1" width="100%"> 	  
 	 	<tr>
 	 	    <th colspan=2>&nbsp;</th>
			<?php foreach($ar_ditte as $kd => $d){ ?>
			  <th colspan=2 style="text-align: center;"><?php echo $d; ?></th>
			<?php } ?>
		</tr>	 	 
		<tr>	
			<th>Codice</th>
			<th>Voce</th>
			<?php foreach($ar_ditte as $kd => $d){ ?>
			  <th class=number>Importo</th>
			  <th class=number>%</th>
			<?php } ?>						
 	    </tr>
 	 
<?php	 
//while ($r = db2_fetch_assoc($stmt)) {
foreach ($ar_ret as $r) {
 ?>
  <tr class='<?php echo trim($r['tipo_voce']); ?>'>
   <td><?php echo "[" . trim($r['codice_voce']) . "] "?></td>
   <td><?php echo trim(acs_u8e($r['voce'])); ?></td>
   
	<?php foreach($ar_ditte as $kd => $d){ ?>
		<td class=number><?php echo n($r['ditta'][$kd]['IMPORTO_A']); ?></td>	
		<td class=number><?php echo n($r['ditta'][$kd]['IMPORTO_B']); ?></td>		
	<?php } ?>   
   
    	

  </tr>
 <?php
 
 //stampo le righe di dettaglio
 if (isset($ar_mov[trim($r['VOCE_ANALISI'])])){
 foreach ($ar_mov[trim($r['VOCE_ANALISI'])] as $km => $m){
 	
 	if (isset($m[$extraParamsValue->open_tab_filters->nome_analisi_1])){
 		$m_des = trim($m[$extraParamsValue->open_tab_filters->nome_analisi_1]['DESCRIZIONE']);
 		$m_IMPORTO_A = $m[$extraParamsValue->open_tab_filters->nome_analisi_1]['IMPORTO'];
 	}	
 	if (isset($m[$extraParamsValue->open_tab_filters->nome_analisi_2])){
 		$m_des = trim($m[$extraParamsValue->open_tab_filters->nome_analisi_2]['DESCRIZIONE']);
 		$m_IMPORTO_C = $m[$extraParamsValue->open_tab_filters->nome_analisi_2]['IMPORTO'];
 	}
 	
 ?>
  <tr class=dett>
   <td><?php echo "[" . $km . "] " . trim(acs_u8e($m_des)); ?></td>
   <td class=number><?php echo n($m_IMPORTO_A); ?></td>
   <td class=number><?php echo n($m_IMPORTO_B); ?></td>
   <td class=number><?php echo n($m_IMPORTO_C); ?></td>
   <td class=number><?php echo n($m_IMPORTO_D); ?></td>   
   <td class=number><?php echo n($m_IMPORTO_A - $m_IMPORTO_C); ?></td>
   <?php if ((int)$m_IMPORTO_C != 0){ ?> 
    	<td class=number><?php echo n( ($m_IMPORTO_A - $m_IMPORTO_C) / $m_IMPORTO_C * 100); ?></td>
   <?php } else { ?> 	 
    	<td class=number>&nbsp;</td>
   <?php } ?>   
  </tr> 
 <?php 	
 } //foreach
 } //if iseset
 
}

?>
 	 
 	 
 	 
 	 
	</table>

	
	<br/>
	
	
 </div>

 </body>
</html>
		