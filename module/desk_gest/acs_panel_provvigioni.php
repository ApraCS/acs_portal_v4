<?php

require_once("../../config.inc.php");
require_once("acs_provvigioni_include.php");

$main_module = new DeskGest();



function genera_des_sconto($r, $ar_field, $field_maggiorazione = ''){
	$ret = array();
	foreach ($ar_field as $kf){
		if ((float)$r[$kf] != 0){
			$ret[] = "-" . n_auto($r[$kf]);
		}
	}

	if (trim($r[$field_maggiorazione]) != ''){
		if ((float)$r[$field_maggiorazione] != 0){
			$ret[] = "+" . n_auto($r[$field_maggiorazione]);
		}
	}


	if (count($ret) > 0)
		return implode(" ", $ret);
	else
		return "_";
}




function sql_where_by_request($m_params){
	$sql_where = "";

	
	if ($m_params->f_tipologia == 'LIQUIDATO') {
		$sql_where .= sql_where_by_combo_value('RF.RFTPRG', 'MATURATO');
		if (strlen($m_params->f_data_da) > 0)
			$sql_where .= " AND RF.RFDTLQ >= {$m_params->f_data_da}";
		if (strlen($m_params->f_data_a) > 0)
			$sql_where .= " AND RF.RFDTLQ <= {$m_params->f_data_a}";		
	} else {
		$sql_where .= sql_where_by_combo_value('RF.RFTPRG', $m_params->f_tipologia);	
		if (strlen($m_params->f_data_da) > 0)
			$sql_where .= " AND RF.RFDTRG >= {$m_params->f_data_da}";
		if (strlen($m_params->f_data_a) > 0)
			$sql_where .= " AND RF.RFDTRG <= {$m_params->f_data_a}";
	}
	
	$sql_where .= sql_where_by_combo_value('RF.RFGRAG', $m_params->f_gruppo);	
	$sql_where .= sql_where_by_combo_value('RF.RFAGEN', $m_params->f_agenzia);
	$sql_where .= sql_where_by_combo_value('RF.RFCAGE', $m_params->f_agente);
	
	
	
	//fatture o ordini? con rettifiche o meno o solo rettifiche?
	$ar_tipologia = array();	
	if ($m_params->f_rettifiche == 'SOLO')
		$ar_tipologia[] = 'AG';
	else {
		$ar_tipologia[] = $m_params->f_fatture_o_ordini;
		
		if ($m_params->f_rettifiche == 'INCLUDI')
			$ar_tipologia[] = 'AG';
	}
	$sql_where .= sql_where_by_combo_value('RF.RFTIDO', $ar_tipologia);
	
	
	$sql_where .= sql_where_by_combo_value('TF.TFCDIV', $m_params->f_divisione);	
	
	$sql_where .= sql_where_by_combo_value('TF.TFTPDO', $m_params->f_tipo_documento);
	
	$sql_where .= sql_where_by_combo_value('TF.TFCCON', $m_params->f_cliente_cod);
		
	return $sql_where;		
}



//elenco divisioni (da file testate)
function get_ar_divisioni(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT TFCDIV AS COD, TFDDIV AS DES FROM {$cfg_mod_Gest['provvigioni']['file_testate']} GROUP BY TFCDIV, TFDDIV ORDER BY TFDDIV";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES']);
	}
	return $ar;
}



//elenco agenzie (da file righe)
function get_ar_agenzie(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT RFAGEN AS COD, RFDAGN AS DES FROM {$cfg_mod_Gest['provvigioni']['file_righe']} GROUP BY RFAGEN, RFDAGN ORDER BY RFDAGN";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES']);
	}
	return $ar;
}


//elenco gruppi (da file righe)
function get_ar_gruppi(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT RFGRAG AS COD, RFDGRA AS DES FROM {$cfg_mod_Gest['provvigioni']['file_righe']} GROUP BY RFGRAG, RFDGRA ORDER BY RFDGRA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES']);
	}
	return $ar;
}


//elenco agenzie (da file righe)
function get_ar_tipo_documento(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT TFTPDO AS COD, TFDTPD AS DES FROM {$cfg_mod_Gest['provvigioni']['file_testate']} GROUP BY TFTPDO, TFDTPD ORDER BY TFDTPD";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES'] . " [" . trim($r['COD'] . "]"));
	}
	return $ar;
}


//elenco agenti (da file righe)
function get_ar_agenti(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT RFCAGE AS COD, RFDAGE AS DES FROM {$cfg_mod_Gest['provvigioni']['file_righe']} GROUP BY RFCAGE, RFDAGE ORDER BY RFDAGE";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES']);
	}
	return $ar;
}



function sum_columns_value(&$ar_r, $r){

	//da imponibile provvigione escludo quelle con importo provvigione = 0
	if ((float)$r['RFIMPO'] != 0) 
		$ar_r['imponibile_provvigione'] += $r['RFIMPP'];
	
	$ar_r['importo_liquidato'] 		+= $r['RFIMLQ'];	
	
	$ar_r['importo_provvigione'] 	+= $r['RFIMPO'];
	
	if (is_array($ar_r['lista_percentuali']) == false)
		$ar_r['lista_percentuali'] = array();	
	
	//se non sono in righe RAEE
	if ($r['RFFG01'] != 'R'){
		$perc_to_txt = n_auto($r['RFPPRA']) . "%";
		$ar_r['lista_percentuali'][$perc_to_txt] ++;
	}	
	
	
	if ($r['RFFG04'] == 'P'){
		$ar_r['fl_provvisorio'] = 'P';
	}	
	
	if ($ar_r['liv'] == 'liv_4'){
		$ar_r['perc_provvigione'] = n($r['RFPPRA']) . "%";
	} else {
		$ar_r['perc_provvigione'] = '';
		//nei livelli superiori mostro un elenco delle percentuali presenti
		foreach ($ar_r['lista_percentuali'] as $kp => $p){
			$ar_r['perc_provvigione'] .= "{$kp} ";
		}
	}
	
	
	//M = Maturato, P = parziale
	if ($r['RFFG03'] == 'M'){
		if ($ar_r['fl_con_maturato'] == 'M' || $ar_r['fl_con_maturato'] == '*FIRST')
			 $ar_r['fl_con_maturato'] = 'M';
		else 
			$ar_r['fl_con_maturato'] = 'P';
	}
	if ($r['RFFG03'] == 'P'){
		$ar_r['fl_con_maturato'] = 'P';
	}	
	if (trim($r['RFFG03']) == ''){
		if ($ar_r['fl_con_maturato'] == '' || $ar_r['fl_con_maturato'] == '*FIRST')
			$ar_r['fl_con_maturato'] = '';
		else 
			$ar_r['fl_con_maturato'] = 'P';
	}	
	
}



function sum_columns_value_TD(&$ar_liv_tot, &$ar_liv0, &$ar_liv1, &$ar_liv2, &$ar_liv3, &$ar_liv4, $r){
	
	//array globale per non sommare due volte gli importi della stessa fattura
	global $ar_conteggio_td;	
	$ar_conteggio_td[$r['TFDOCU']]++;
	
/*	
	if ($ar_conteggio_td[$r['TFDOCU']] > 1)
		return; //esco perche' gia' conteggiata
*/		
	
		$ar_liv_tot['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv_tot['tot_netto_merce'] 	+= $r['TFINFI'];
		$ar_liv_tot['tot_imponibile']  	+= $r['TFTIMP'];
		$ar_liv_tot['tot_anticipo'] 	+= $r['TFTANT'];	
	
		$ar_liv0['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv0['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv0['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv0['tot_anticipo'] 	+= $r['TFTANT'];
	
	
		$ar_liv1['tot_documento'] 	+= $r['TFTOTD'];		
		$ar_liv1['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv1['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv1['tot_anticipo'] 	+= $r['TFTANT'];
		
		$ar_liv2['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv2['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv2['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv2['tot_anticipo'] 	+= $r['TFTANT'];

		$ar_liv3['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv3['tot_netto_merce'] += $r['TFINFI'];		
		$ar_liv3['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv3['tot_anticipo'] 	+= $r['TFTANT'];

		$ar_liv4['tot_documento'] 	+= $r['TFTOTD'];
		$ar_liv4['tot_netto_merce'] += $r['TFINFI'];
		$ar_liv4['tot_imponibile']  += $r['TFTIMP'];
		$ar_liv4['tot_anticipo'] 	+= $r['TFTANT'];
}




####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
	####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TFCCON, TFDCON
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']}
			WHERE UPPER(
				REPLACE(REPLACE(TFDCON, '.', ''), ' ', '')
				)" . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			GROUP BY TFCCON, TFDCON
 			";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array( "cod" 		=> trim($row['TFCCON']),
				"descr" 	=> acs_u8e(trim($row['TFDCON']))
		);
	}

	echo acs_je($ret);

	exit;
}





// ******************************************************************************************
// BLOCCO/SBLOCCO flag ricalcolo provvigione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_nuova_ins_imp_age'){
	$m_params = acs_m_params_json_decode();
	
	//scrittura RI
	$sh = new SpedHistory(new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST')));
	$sh->crea(
			'provvigioni_inserisci_maggiorazione_diminuzione_per_agente',
			array(
					"k_agenzia" 	=> $_REQUEST['f_agenzia'],
					"k_agente" 		=> $_REQUEST['f_agente'],
					"data" 			=> $_REQUEST['f_data'],
					"perc" 			=> sql_f($_REQUEST['f_perc_provv']),
					"importo" 		=> sql_f($_REQUEST['f_importo_provv']),
					"causale" 		=> $_REQUEST['f_cage'],
					"note"			=> $_REQUEST['f_memo'],
					"ditta"			=> $id_ditta_default,
					"anno"			=> date('Y')
			)
	);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);	
  exit;
}




// ******************************************************************************************
// BLOCCO/SBLOCCO flag ricalcolo provvigione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_blocca_sblocca_ricalcolo'){
	$m_params = acs_m_params_json_decode();

	if(isset($m_params->list_selected_id) && count($m_params->list_selected_id) > 0){
	    foreach($m_params->list_selected_id as $k=> $v){
	        
	        if ($v->local_flag == 'Y')
	            $to_flag = '';
	        else
	            $to_flag = 'Y';
	                
            $sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_testate']} SET TFFG01 = ? WHERE TFDOCU = ?";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($to_flag, trim($v->tfdocu)));
	       
	    }
	    
	    $ret = array();
	    $ret['success'] = true;
	    echo acs_je($ret);
	    
	}else{
	    
	    //recupero id fattura
	    $id_fattura = $m_params->selected_id;
	    
	    if ($m_params->local_flag == 'Y')
	        $to_flag = '';
	    else
	        $to_flag = 'Y';
	            
        $sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_testate']} SET TFFG01 = ? WHERE TFDOCU = ?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($to_flag, trim($id_fattura)));
        
        //ritorno attuale valore
        $sql = "SELECT TFFG01 FROM {$cfg_mod_Gest['provvigioni']['file_testate']} WHERE TFDOCU = ?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($id_fattura));
        $r = db2_fetch_assoc($stmt);
        
        echo acs_je(array('success' => $result, 'remote_flag' => $r['TFFG01']));
	    
	}
	


	
	exit;
}




// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	ini_set('max_execution_time', 300);	
	$m_params = acs_m_params_json_decode();
	
		$sql_where = sql_where_by_request($m_params);
		
		
		//dettaglio righe fattura
		//----------------------------------------------------------------------------
		if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
			
			//da node recupero la fattura e mostro tutte le righe (anche se non appartenenti a questo agenzia_agente)
			$liv_exp = explode("|", $_REQUEST['node']);
			$sql = "SELECT
					TFDOCU, TFINUM, TFAADO, TFNRDO, TFDT, TFTPDO, TFDTPD,
					RFAGEN, RFDAGN, RFCAGE, RFDAGE, TFCCON, TFDCON,
					RFDTRG, RFRIGA, RFTPRG, RFIMPP, RFIMPO, RFPPRA, TFTOTD, TFTIMP, TFINFI, TFTANT, TFFG01, RFFG01, RFFG03, RFFG04, RFFG05, RFIMLQ
					FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
					INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
					ON TF.TFDOCU = RF.RFDOCU
					WHERE 1=1 AND TFDOCU = ? AND RF.RFTPRG=?";
			
			if ($m_params->f_tipologia == 'LIQUIDATO')
				$filtra_tipologia = 'MATURATO';
			else
				$filtra_tipologia = $m_params->f_tipologia;			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($liv_exp[4], $filtra_tipologia));
			
			while ($r = db2_fetch_assoc($stmt)) {
				$tmp_ar_id = $liv_exp;
				array_push($tmp_ar_id, trim($r['RFRIGA']), trim($r['RFTPRG']));
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_4';
				$ar_new['liv_cod'] = implode('|', array(trim($r['RFRIGA']), trim($r['RFTPRG'])));
				$ar_new['liv_data'] = $r['RFDTRG'];
				$ar_new['task'] = acs_u8e("Riga {$r['RFRIGA']}: {$r['RFDAGE']}");
				$ar_new['fl_blocco_sblocco_ricalcolo'] = acs_u8e($r['TFFG01']);
				$ar_new['leaf'] = true;
				
				$ar_new['imponibile_provvigione'] = $r['RFIMPP'];
				$ar_new['importo_provvigione'] 	= $r['RFIMPO'];
				$ar_new['importo_liquidato'] 	= $r['RFIMLQ'];
				$ar_new['perc_provvigione'] = n($r['RFPPRA']) . "%";
				
				//Se la riga e' dell'agente da cui provengo.... segnalo in grigio
				if (trim($liv_exp[2]) == trim($r['RFCAGE']))
					$ar_new['trCls'] = 'grassetto'; //yellow 
				
				//Segnalo riga RAEE
				if ($r['RFFG01'] == 'R'){
					$ar_new['fl_raee'] = "R";
					//$ar_new['trCls'] = 'G';
				}
				
				//Segnalo riga con provvigioni azzerate
				if ($r['RFFG01'] == 'A' || $r['RFFG01'] == 'B'){
					$ar_new['fl_azzerate'] = "A";
				}

				//Segnalo riga gia' con maturato
				if ($r['RFFG03'] == 'M'){
					$ar_new['fl_con_maturato'] = "M";
				}
				if ($r['RFFG03'] == 'P'){
					$ar_new['fl_con_maturato'] = "P"; //maturato parziale
				}				

				$ar_new['fl_provvisorio'] = $r['RFFG04']; //flag provvisorio
				$ar_new['RFFG05'] = $r['RFFG05']; //Sospendi/Riattiva liquidazione				
				
				if ($m_params->f_tipologia == 'MATURATO') {
					
				}
				

				$ret[] = $ar_new;
			}			
				
				
			echo acs_je(array('success' => true, 'children' => $ret));
			exit;
		}

		
		//root
		//-------------------------------------------------------------
			$sql = "SELECT 
					TFDOCU, TFINUM, TFAADO, TFNRDO, TFDT, TFTPDO, TFDTPD,
					RFGRAG, RFDGRA, RFAGEN, RFDAGN, RFCAGE, RFDAGE, TFCCON, TFDCON,
					TFDTRG, RFRIGA, RFTPRG, RFIMPP, RFIMPO, RFPPRA, TFTOTD, TFTIMP, TFINFI, TFTANT, TFFG01, TFFG02, RFFG01, RFFG03, RFFG04, RFIMLQ,
					TFSCA1, TFSCA2, TFSCA3, TFSCA4,
					TFSC11, TFSC12, TFSC13, TFSC14, TFMAG1,
					TFSC21, TFSC22, TFSC23, TFSC24, TFMAG2,
					TFSC31, TFSC32, TFSC33, TFSC34, TFMAG3,
					TFSC41, TFSC42, TFSC43, TFSC44, TFMAG4,
					TFSC51, TFSC52, TFSC53, TFSC54, TFMAG5,
					TFCPAG, TFDPAG
					FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
					INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
					   ON TF.TFDOCU = RF.RFDOCU
					WHERE 1=1 {$sql_where}
					ORDER BY RFDAGN, RFDAGE, TFDCON"
					;


		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();
		
		$class_liv_totale = '';
		$class_liv_totale .= ' ' . trim($m_params->f_tipologia);
		
		while ($r = db2_fetch_assoc($stmt)) {
						
			$tmp_ar_id = array();
			$ar_r = &$ar;			
			
			$cli_liv_tot = 'TOTALE';						
			$cod_liv0 = trim($r['RFGRAG']); 	//gruppo di agenti			
			$cod_liv1 = trim($r['RFAGEN']); 	//codice agenzia
			$cod_liv2 = trim($r['RFCAGE']);		//codice agente		
			$cod_liv3 = trim($r['TFCCON']);		//codice cliente
			$cod_liv4 = trim($r['TFDOCU']);		//fattura
			$cod_liv5 = implode("_", array(trim($r['RFRIGA']), trim($r['RFTPRG'])));	//fattura
				
			
			//LIVELLO TOTALE
			$liv = $cod_liv_tot;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = 'liv_totale';
				$ar_new['liv'] = "liv_totale dett_selezionato {$class_liv_totale}";
				$ar_new['liv_cod'] = 'TOTALE';
				$ar_new['liv_cod_out'] = '';
				$ar_new['task'] = 'Totale';
				$ar_new['leaf'] = false;
				$ar_new['fl_con_maturato'] = '*FIRST';
				$ar_new['expanded'] = true;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv_tot = &$ar_r;
			sum_columns_value($ar_r, $r);
				
			

			//LIVELLO 0
			$liv = $cod_liv0;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = "liv_totale  {$class_liv_totale}";
				$ar_new['liv_menu'] = "liv_0_0";
				$ar_new['liv_cod'] = $liv;
				$ar_new['liv_cod_out'] = $liv;
				$ar_new['task'] = acs_u8e($r['RFDGRA']);
				$ar_new['leaf'] = false;
				$ar_new['fl_con_maturato'] = '*FIRST';
				$ar_new['expanded'] = false;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv0 = &$ar_r;
			sum_columns_value($ar_r, $r);
				
			
			//LIVELLO 1
			$liv = $cod_liv1;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;						
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_0_2';
				$ar_new['liv_cod'] = $liv;
				$ar_new['liv_cod_out'] = $liv;
				$ar_new['task'] = acs_u8e($r['RFDAGN']);
				$ar_new['leaf'] = false;
				$ar_new['expanded'] = false;
				$ar_new['fl_con_maturato'] = '*FIRST'; 
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv1 = &$ar_r;
			sum_columns_value($ar_r, $r);


			//LIVELLO 2			
			$liv = $cod_liv2;
			$ar_r = &$ar_r['children'];			
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_1';
				$ar_new['liv_cod'] = $liv;
				$ar_new['liv_cod_out'] = $liv;				
				$ar_new['task'] = acs_u8e($r['RFDAGE']);
				$ar_new['leaf'] = false;
				$ar_new['fl_con_maturato'] = '*FIRST';
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv2 = &$ar_r;
			sum_columns_value($ar_r, $r);
			

			//LIVELLO 3
			$liv = $cod_liv3;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_2';
				$ar_new['liv_cod'] = $liv;
				$ar_new['liv_cod_out'] = $liv;				
				$ar_new['task'] = acs_u8e($r['TFDCON']);
				$ar_new['leaf'] = false;
				$ar_new['fl_con_maturato'] = '*FIRST';
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv3 = &$ar_r;
			sum_columns_value($ar_r, $r);			
				

			//LIVELLO 4
			$liv = $cod_liv4;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_3';
				$ar_new['liv_cod'] = acs_u8e($r['TFDOCU']);
				$ar_new['liv_cod_out'] = acs_u8e($r['TFTPDO']);
				$ar_new['liv_cod_qtip'] = acs_u8e($r['TFDTPD']);
				$ar_new['liv_data'] = $r['TFDTRG'];
				
				if ($r['TFFG02'] == 'M') //multiagente
				{
					$ar_new['liv_cod_out'] .= " " . "<img src=" . img_path("icone/48x48/user_group.png") . " width=18>";
				}
				
				//$ar_new['task'] = acs_u8e($r['TFDOCU']);
				$ar_new['task'] = "[{$r['TFINUM']}] " . implode("_", array($r['TFAADO'], $r['TFNRDO'], $r['TFDT']));

				$ar_new['fl_blocco_sblocco_ricalcolo'] = acs_u8e($r['TFFG01']);
				$ar_new['leaf'] = false;
				$ar_new['fl_con_maturato'] = '*FIRST';
								
				$ar_campi_sconto_des = array();				
				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSCA1", "TFSCA2", "TFSCA3", "TFSCA4"));
				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC11", "TFSC12", "TFSC13", "TFSC14"), "TFMAG1");
				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC21", "TFSC22", "TFSC23", "TFSC24"), "TFMAG2");
				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC31", "TFSC32", "TFSC33", "TFSC34"), "TFMAG3");
				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC41", "TFSC42", "TFSC43", "TFSC44"), "TFMAG4");
				$ar_campi_sconto_des[] = genera_des_sconto($r, array("TFSC51", "TFSC52", "TFSC53", "TFSC54"), "TFMAG5");				
				$ar_new['docu_sconti_qtip'] = "Sconti: " . implode(" | ", $ar_campi_sconto_des);
				$ar_new['docu_sconti_qtip'] .= "<br/>Pagamento: " . implode(" : ", array("[" . trim($r['TFCPAG']) . "]", trim($r['TFDPAG'])));
				
				
				$ar_r["{$liv}"] = $ar_new;
				
				sum_columns_value_TD($ar_liv_tot, $ar_liv0, $ar_liv1, $ar_liv2, $ar_liv3, $ar_r["{$liv}"], $r);
				
			}
			$ar_r = &$ar_r["{$liv}"];
			sum_columns_value($ar_r, $r);			
			
			continue;
				
			//LIVELLO 5 - LEAF
			$liv = $cod_liv5;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_4';
				$ar_new['task'] = acs_u8e("Riga {$r['RFRIGA']}: {$r['RFTPRG']}");
				$ar_new['fl_blocco_sblocco_ricalcolo'] = 'Y'; //acs_u8e($r['TFFG01']);
				$ar_new['leaf'] = true;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			sum_columns_value($ar_r, $r);			
				
			
	}
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	 echo acs_je(array('success' => true, 'children' => $ret));	
	
	exit;
} //get_json_data






// ******************************************************************************************
// 	exe_toggle_RFFG05 (exe) - Sospendi/Riattiva liquidazione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_toggle_RFFG05'){
	$m_params = acs_m_params_json_decode();	
	$riga_exp = explode("|", $m_params->selected_id);
	
	//recupero riga da modificare
	$sql = "SELECT *
				FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
				INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
				ON TF.TFDOCU = RF.RFDOCU
				WHERE 1=1 AND RFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($riga_exp[4], $riga_exp[6], $riga_exp[5]));
	$rowOld = db2_fetch_assoc($stmt);
	
	if (trim($rowOld['RFFG05']) != 'Y')
		$new_RFFG05 = 'Y';
	else
		$new_RFFG05 = '';
	

	//update riga (percentuale, importo)
	$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_righe']}
			SET RFFG05 = ?
			WHERE 1=1 AND RFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($new_RFFG05, $riga_exp[4], $riga_exp[6], $riga_exp[5]));

	$ret = array();
	$ret['success'] = $result;
	$ret['new_RFFG05'] = $new_RFFG05;
	echo acs_je($ret);	
 exit;
}






// ******************************************************************************************
// Modifica provvigione (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_provvigione'){
	$riga_exp = explode("|", $_REQUEST['riga']);	
	
	//recupero riga da modificare
	$sql = "SELECT *
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
			ON TF.TFDOCU = RF.RFDOCU
			WHERE 1=1 AND TFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($_REQUEST['docu'], $riga_exp[1], $riga_exp[0]));
	$rowOld = db2_fetch_assoc($stmt);
	
	
	//flag blocco modifiche su riga fattura (a meno che non ha modificato agente e/o percentuale)
	if (trim($rowOld['RFCAGE']) != $_REQUEST['f_agente'] || (float)$rowOld['RFPPRA'] != (float)$_REQUEST['f_perc_provv']){
		//non faccio niente
	} else {
		$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_testate']} SET TFFG01 = ? WHERE TFDOCU = ?";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array('Y', $_REQUEST['docu']));
	}
	
	
	//ho modificato percentuale o importo o imponibile
	if (
			(float)$rowOld['RFPPRA'] != (float)sql_f($_REQUEST['f_perc_provv']) ||
			(float)$rowOld['RFIMPO'] != (float)sql_f($_REQUEST['f_importo_provv']) ||
			(float)$rowOld['RFIMPP'] != (float)sql_f($_REQUEST['f_imponibile_provv'])
		){
		
			//update riga (percentuale, importo)
			$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_righe']}
					SET RFPPRA=?, RFIMPO=?, RFIMPP=?, RFFG02='M'
					WHERE 1=1 AND RFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";
		
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array(
					sql_f($_REQUEST['f_perc_provv']), 
					sql_f($_REQUEST['f_importo_provv']), 
					sql_f($_REQUEST['f_imponibile_provv']),
					$_REQUEST['docu'], 
					$riga_exp[1], 
					$riga_exp[0]));
		
			//scrittura RI
			$sh = new SpedHistory(new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST')));
			$sh->crea(
					'provvigioni_aggiorna_perc_importo_agente',
					array(
							"k_ordine" 		=> $rowOld['TFDOCU'],
							"k_cliente" 	=> $rowOld['TFCCON'],
							"k_agente" 		=> $rowOld['RFCAGE'],
							"newPercProvv" 	=> sql_f($_REQUEST['f_perc_provv']),
							"oldPercProvv" 	=> $rowOld['RFPPRA'],
							"newImpoProvv" 	=> sql_f($_REQUEST['f_importo_provv'])
					)
			);
		
	}	
	
	
	
	
	//ho modificato anche l'agente?
	if (trim($rowOld['RFCAGE']) != trim($_REQUEST['f_agente']) && trim($_REQUEST['f_agente']) != ''){
		//recupero una dira di questo agente per poi copiare agenzia/gruppo/....
		$sql = "SELECT * FROM {$cfg_mod_Gest['provvigioni']['file_righe']} RF WHERE 1=1 AND RFCAGE = ?";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($_REQUEST['f_agente']));
		$rowAgente = db2_fetch_assoc($stmt);
		
		
		//preparo campi per update in base ad agente selezionat
		$ar_ins['RFCAGE'] 	= $rowAgente['RFCAGE'];
		$ar_ins['RFDAGE'] 	= $rowAgente['RFDAGE'];
		$ar_ins['RFAGEN'] 	= $rowAgente['RFAGEN'];
		$ar_ins['RFDAGN'] 	= $rowAgente['RFDAGN'];
		$ar_ins['RFDAGE'] 	= $rowAgente['RFDAGE'];
		$ar_ins['RFDAGE'] 	= $rowAgente['RFDAGE'];
		$ar_ins['RFGRAG'] 	= $rowAgente['RFGRAG'];
		$ar_ins['RFDGRA'] 	= $rowAgente['RFDGRA'];
		$ar_ins['RFTAGE'] 	= $rowAgente['RFTAGE'];
		
		
		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_righe']}
			SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			WHERE 1=1 AND RFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";
				
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_ins,
				array($_REQUEST['docu'], $riga_exp[1], $riga_exp[0])
				));
		echo db2_stmt_errormsg($stmt);
		
		//scrittura RI
		$sh = new SpedHistory(new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST')));
		$sh->crea(
				'provvigioni_aggiorna_agente',
				array(
						"k_ordine" 		=> $rowOld['TFDOCU'],
						"k_cliente" 	=> $rowOld['TFCCON'],
						"k_agente" 		=> $rowAgente['RFCAGE'],
						"oldAgente" 	=> $rowOld['RFCAGE']
				)
		);		
				
	} //se ho modificato agente
	
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);	
	exit;
}





// ******************************************************************************************
// Azzera provvigione (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_provvigione_azzera'){
	$riga_exp = explode("|", $_REQUEST['riga']);

	//recupero riga da modificare
	$sql = "SELECT *
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
			ON TF.TFDOCU = RF.RFDOCU
			WHERE 1=1 AND TFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($_REQUEST['docu'], $riga_exp[1], $riga_exp[0]));
	$rowOld = db2_fetch_assoc($stmt);


		//update riga (percentuale, importo)
		$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_righe']}
					SET RFPPRA=?, RFIMPO=?, RFFG02='M'
					WHERE 1=1 AND RFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array(sql_f(0), sql_f(0), $_REQUEST['docu'], $riga_exp[1], $riga_exp[0]));

		//scrittura RI
		$sh = new SpedHistory(new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST')));
		$sh->crea(
				'provvigioni_azzera_perc_importo_agente',
				array(
						"k_ordine" 		=> $rowOld['TFDOCU'],
						"k_cliente" 	=> $rowOld['TFCCON'],
						"k_agente" 		=> $rowOld['RFCAGE'],
						"newPercProvv" 	=> sql_f(0),
						"oldPercProvv" 	=> $rowOld['RFPPRA'],
						"newImpoProvv" 	=> sql_f(0)
				)
		);


	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// Nuova provvigione (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_nuova_provvigione'){
	$rec_exp = explode("|", $_REQUEST['rec_id']);
	
	$docu_exp = explode("_", $_REQUEST['docu']);

	
	//recupero riga testata
	$sql = "SELECT *
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			WHERE 1=1 AND TFDOCU = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($_REQUEST['docu']));
	$rowFat = db2_fetch_assoc($stmt);

	//recupero max idriga per fattura/tipo_riga
	$sql = "SELECT MAX(RFRIGA) AS MAX_RFRIGA
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
			ON TF.TFDOCU = RF.RFDOCU
			WHERE 1=1 AND TFDOCU = ? AND RFTPRG = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($_REQUEST['docu'], $_REQUEST['tipologia']));
	$row = db2_fetch_assoc($stmt);	
	$max_rfriga = $row['MAX_RFRIGA'];

	//recupero una riga di questo agente per poi copiare agenzia/gruppo/....
	$sql = "SELECT *
			FROM {$cfg_mod_Gest['provvigioni']['file_righe']} RF
			WHERE 1=1 AND RFCAGE = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($_REQUEST['f_agente']));
	$rowAgente = db2_fetch_assoc($stmt);	
	
	
	//preparo ar per insert
	global $auth;
	$ar_ins = array();
	$ar_ins['RFUSGE'] 	= $auth->get_user();
	$ar_ins['RFDTGE'] 	= oggi_AS_date();
	$ar_ins['RFORGE'] 	= oggi_AS_time();
	$ar_ins['RFDOCU'] 	= $_REQUEST['docu'];
	$ar_ins['RFDT'] 	= $docu_exp[0];
	$ar_ins['RFTIDO'] 	= $docu_exp[1];	
	$ar_ins['RFINUM'] 	= $docu_exp[2];
	$ar_ins['RFAADO'] 	= $docu_exp[3];
	$ar_ins['RFNRDO'] 	= $docu_exp[4];
	$ar_ins['RFRIGA'] 	= $max_rfriga + 1;
	$ar_ins['RFTPRG'] 	= $_REQUEST['tipologia'];	
	$ar_ins['RFCAGE'] 	= $_REQUEST['f_agente'];
	
	//in base ad agente selezionat
	$ar_ins['RFDAGE'] 	= $rowAgente['RFDAGE'];	
	$ar_ins['RFAGEN'] 	= $rowAgente['RFAGEN'];
	$ar_ins['RFDAGN'] 	= $rowAgente['RFDAGN'];
	$ar_ins['RFDAGE'] 	= $rowAgente['RFDAGE'];
	$ar_ins['RFDAGE'] 	= $rowAgente['RFDAGE'];
	$ar_ins['RFGRAG'] 	= $rowAgente['RFGRAG'];
	$ar_ins['RFDGRA'] 	= $rowAgente['RFDGRA'];
	$ar_ins['RFTAGE'] 	= $rowAgente['RFTAGE'];	
	
	
	$ar_ins['RFDTRG'] 	= $rowFat['TFDTRG'];
	$ar_ins['RFPPRA'] 	= sql_f($_REQUEST['f_perc_provv']);
	$ar_ins['RFIMPO'] 	= sql_f($_REQUEST['f_importo_provv']);
	$ar_ins['RFIMPP'] 	= sql_f($_REQUEST['f_imponibile_provv']);
	
	$ar_ins['RFFG02'] 	= 'M'; //riga inserita/modificata manualmente da desktop
	
	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['provvigioni']['file_righe']}(
			" . create_name_field_by_ar($ar_ins) . "
			)
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")
			";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	echo db2_stmt_errormsg($stmt);
	
	//flag blocco modifiche su riga fattura
	$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_testate']} SET TFFG01 = ? WHERE TFDOCU = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array('Y', $rec_exp[3]));

	//scrittura RI
	$sh = new SpedHistory(new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST')));
	$sh->crea(
			'provvigioni_nuova_perc_importo_agente',
			array(
					"k_ordine" 		=> $rowFat['TFDOCU'],
					"k_cliente" 	=> $rowFat['TFCCON'],
					"k_agente" 		=> $_REQUEST['f_agente'],
					"newPercProvv" 	=> sql_f($_REQUEST['f_perc_provv']),
					"newImpoProvv" 	=> sql_f($_REQUEST['f_importo_provv'])
			)
	);

	exit;
}









// ******************************************************************************************
// Modifica provvigione su riga (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_provvigione_su_riga'){

	$sped_module = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
	$oe = $sped_module->k_ordine_td_decode_xx($_REQUEST['docu']);	
	
	//recupero riga da modificare (da righe gest)
		$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?					
					AND RD.RDNREC = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge($oe, array($_REQUEST['nrec'])));
	$rowOld = db2_fetch_assoc($stmt);

	
		//update riga (percentuale)
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
				SET RDPR1=?
				WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?					
					AND RD.RDNREC = ?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(array(sql_f($_REQUEST['f_perc_provv'])), array_merge($oe, array($_REQUEST['nrec']))));
		
		//scrittura RI
		$sh = new SpedHistory(new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST')));
		$sh->crea(
				'provvigioni_aggiorna_perc_provv_su_riga',
				array(
						"k_ordine" 		=> $_REQUEST['docu'],
						"newPercProvv" 	=> sql_f($_REQUEST['f_perc_provv']),
						"oldPercProvv" 	=> $rowOld['RDPR1'],
						"nrec"			=> $_REQUEST['nrec']
				)
		);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}






// ******************************************************************************************
// Modifica provvigione su riga (exe)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_provvigione_azzera_su_riga'){

	$sped_module = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
	$oe = $sped_module->k_ordine_td_decode_xx($_REQUEST['docu']);

	//recupero riga da modificare (da righe gest)
	$sql = "SELECT *
			FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
			WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
			AND RD.RDNREC = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge($oe, array($_REQUEST['nrec'])));
	$rowOld = db2_fetch_assoc($stmt);


	//update riga (percentuale)
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
			SET RDPR1=?
			WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
			AND RD.RDNREC = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(array(sql_f(0)), array_merge($oe, array($_REQUEST['nrec']))));

	//scrittura RI
	$sh = new SpedHistory(new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST')));
	$sh->crea(
			'provvigioni_aggiorna_perc_provv_azzera_su_riga',
			array(
					"k_ordine" 		=> $_REQUEST['docu'],
					"newPercProvv" 	=> 0,
					"oldPercProvv" 	=> $rowOld['RDPR1'],
					"nrec"			=> $_REQUEST['nrec']
			)
	);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}












// ******************************************************************************************
// Modifica provvigione (form)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_modifica_provvigione'){
	$m_params = acs_m_params_json_decode();
	
	//recupero tipo/riga in modifica
	$riga_exp = explode("|", $m_params->riga);
	$sql = "SELECT *
				FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
				INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
				ON TF.TFDOCU = RF.RFDOCU
				WHERE 1=1 AND TFDOCU = ? AND RFTPRG = ? AND RFRIGA = ?";
		
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->docu, $riga_exp[1], $riga_exp[0]));	
	$row = db2_fetch_assoc($stmt);
	
	?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Salva',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_provvigione',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {										
										    loc_win.fireEvent('afterUpdateRecord', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-sub_red_delete-32',
	                     text: 'Azzera',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_provvigione_azzera',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {										
										    loc_win.fireEvent('afterAzzeraRecord', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }],   		            
		            
		            items: [
						   {
							     name: 'docu'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->docu); ?>
							},{
							     name: 'riga'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->riga); ?>
							}, {
								name: 'f_agente',
								xtype: 'combo',							
								fieldLabel: 'Agente',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,		
							    anchor: '-15',
							    value: <?php echo j(trim($row['RFCAGE'])); ?>,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
									     <?php echo acs_ar_to_select_json(get_ar_agenti(), ""); ?>	
									    ] 
								}						 
							}, {
							     name: 'f_imponibile_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Imponibile provv.'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($row['RFIMPP']); ?>
							},		            
						   {
							     name: 'f_perc_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Percentuale'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($row['RFPPRA']); ?>
							   , listeners: {
								    'change': function(f,b){
								      //ricalcolo importo
								      form = f.up('form').getForm();
								      nuovo_importo_provv = 
								      	parseFloat(form.findField('f_perc_provv').getValue()) * parseFloat(form.findField('f_imponibile_provv').getValue()) / 100;
								      
								      form.findField('f_importo_provv').setValue(nuovo_importo_provv);
								    }
								 }
							}, {
							     name: 'f_importo_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Importo'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($row['RFIMPO']); ?>
							}
		            
		            
						]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}






// ******************************************************************************************
// Modifica provvigione di riga (form) - da visualizza righe
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_modifica_provvigione_su_riga'){
	$m_params = acs_m_params_json_decode();

	?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Salva',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_provvigione_su_riga',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {										
										    loc_win.fireEvent('afterUpdateRecord', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-sub_red_delete-32',
	                     text: 'Azzera',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_provvigione_azzera_su_riga',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {										
										    loc_win.fireEvent('afterAzzeraRecord', loc_win);										
										}																                        
				                });
								            
				            
				         }
				        }],   		            
		            
		            items: [
						   {
							     name: 'docu'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->docu); ?>
							},{
							     name: 'nrec'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->nrec); ?>
							}, {
							     name: 'f_perc_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Percentuale'
							   , labelAlign: 'left'
							   , allowBlank: true
							   , anchor: '-15'
							   , value: <?php echo j($row['RFPPRA']); ?>
							}
		            
		            
						]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}





// ******************************************************************************************
// Avanza in maturato (form)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_avanza_in_maturato'){
	$m_params = acs_m_params_json_decode();

	//recupero riga in modifica
	$sql = "SELECT * FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF WHERE 1=1 AND TFDOCU = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->docu));
	$row = db2_fetch_assoc($stmt);

	$rec_id_ar = explode("|", $m_params->rec_id);
	$agente_cod = $rec_id_ar[2];
	
	//recupero righe da ancora in accreditato (e non maturate -> RFFGOE <> 'M')
	$sql = "SELECT * FROM {$cfg_mod_Gest['provvigioni']['file_righe']} RF WHERE 1=1 AND RFDOCU = ? AND RFTPRG=? AND RFCAGE=? AND RFFG03<>'M'";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->docu, 'ACCREDITATO', $agente_cod));
	$c_righe = 0;
	while($rowRF = db2_fetch_assoc($stmt)){
		$c_righe++;
	}
	
	//devo calcolare la differenza tra il maturato e l'accreditato
	//ACCREDITATO
	$sql = "SELECT * FROM {$cfg_mod_Gest['provvigioni']['file_righe']} RF WHERE 1=1 AND RFDOCU = ? AND RFTPRG=? AND RFCAGE=?";	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->docu, 'ACCREDITATO', $agente_cod));
	$rfimpp_mat = $rfimpp_acc =0;
	while($rowRF = db2_fetch_assoc($stmt)){
		$rfimpo_acc += $rowRF['RFIMPO'];
	}
	

	$sql = "SELECT * FROM {$cfg_mod_Gest['provvigioni']['file_righe']} RF WHERE 1=1 AND RFDOCU = ? AND RFTPRG=? AND RFCAGE=?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->docu, 'MATURATO', $agente_cod));
	$rfimpo = $rfimpp = 0;
	while($rowRF = db2_fetch_assoc($stmt)){
		$rfimpo_mat += $rowRF['RFIMPO'];
	}
	
	$importo_provvigione_proposto = $rfimpo_acc - $rfimpo_mat;
	
	
	//VIEW INFO
	$view_info = array();
	$view_info[] = "Agente: {$agente_cod}";
	$view_info[] = "Somma di importo provv.  = {$rfimpo}";
	?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [
					
<?php if ($importo_provvigione_proposto <> 0 && $c_righe > 0) { ?>					
					{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Conferma',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_avanza_in_maturato',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {										
										    loc_win.fireEvent('afterUpdateRecord', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }
<?php } else { ?>
					
					{
						xtype: 'label', text: 'Non ci sono righe da portare in maturato'
					}, 
					{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Chiudi',
				            handler: function() {
				            
				            loc_win = this.up('window');
				            loc_win.close();				            
				         }
				        }
<?php } ?>				        
				        
				        
				        ],   		            
		            
		            items: [
						   {
							     name: 'docu'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->docu); ?>
							},{
							     name: 'tipologia'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->tipologia); ?>
							},{
							     name: 'agente'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($agente_cod); ?>
							},{
							     name: 'check_importo'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($rfimpo); ?>
							}
							
							/*
							,{
							     name: 'info_view'                   		
							   , xtype: 'textarea'
							   , value: <?php echo j(implode("\n", $view_info)); ?>
							   , width: '100%'
							}
							*/
							
							, {
								     name: 'f_data_registrazione'
								   , fieldLabel: 'Data registrazione'
								   , labelWidth: 120, flex: 1
								   , disabled: false                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , format: 'd/m/Y'						   						   
								   , submitFormat: 'Ymd'
								   , allowBlank: false
								   , listeners: {
								       invalid: function (field, msg) {
								       Ext.Msg.alert('', msg);}
									 }	 
								}, {
							     name: 'f_note'                   		
							   , xtype: 'textfield'
							   , fieldLabel: 'Note'
							   , labelWidth: 120
							   , maxLength: 100
							   , anchor: '0'
							}, {
							     name: 'f_importo_provv'                   		
							   , xtype: 'textfield'
							   , fieldLabel: 'Importo provvigione'
							   , labelWidth: 120
							   , maxLength: 100
							   , anchor: '0'
							   , value: <?php echo $importo_provvigione_proposto ?>
							},{
							     name: 'f_importo_provv_proposto'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($importo_provvigione_proposto); ?>
							}
		            
						]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
<?php
	exit;
}


function calcola_next_RFRIGA($r){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT max(RFRIGA) AS MAX_RFRIGA FROM {$cfg_mod_Gest['provvigioni']['file_righe']} RF WHERE 1=1 AND RFDOCU = ? AND RFTPRG=?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($r['RFDOCU'], 'MATURATO'));
	$row = db2_fetch_assoc($stmt);
	return (int)$row['MAX_RFRIGA'] + 1;
}



// ******************************************************************************************
// Avanza in maturato (EXE)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_avanza_in_maturato'){
	$m_params = acs_m_params_json_decode();
	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());	

	//recupero righe da ancora in accreditato (e non maturate -> RFFGOE <> 'M')
	$sql = "SELECT * FROM {$cfg_mod_Gest['provvigioni']['file_righe']} RF WHERE 1=1 AND RFDOCU = ? AND RFTPRG=? AND RFCAGE=? AND RFFG03<>'M'";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($all_params['docu'], $all_params['tipologia'], $all_params['agente']));
	$rfimpo = 0;
	
	$c = 0;
	while($rowRF = db2_fetch_assoc($stmt)){
		$c++;
		if ($c == 1) $fromRF_row = $rowRF;
		//chiave: RFDOCU, RFTPRG (ACCREDITATO), RFRIGA
		
		$rfimpo += $rowRF['RFIMPO']; //importo provv
		if ((float)$rowRF['RFIMPO'] != 0)		
			$rfimpp += $rowRF['RFIMPP']; //imponibile		
	}
	
	
	$ar_ins = $fromRF_row; //parto dall'ultima riga per inizializzar l'array per l'insert
	$ar_ins['RFUSGE'] = $auth->get_user();
	$ar_ins['RFDTGE'] = oggi_AS_date();
	$ar_ins['RFORGE'] = oggi_AS_time();
	$ar_ins['RFDTRG'] = $all_params['f_data_registrazione'];
	$ar_ins['RFTPRG'] = 'MATURATO';
	$ar_ins['RFIMPP'] = $rfimpp;
	$ar_ins['RFIMPO'] = sql_f($all_params['f_importo_provv']);
	$ar_ins['RFPPRA'] = 0;
	$ar_ins['RFRIGA'] = calcola_next_RFRIGA($fromRF_row);
	$ar_ins['RFNOTE'] = utf8_decode($all_params['f_note']);
	$ar_ins['RFFU01'] = 'D';
	
	$ar_ins['RFFG04'] = 'D';	
	$ar_ins['RFUSCO'] = $auth->get_user();
	$ar_ins['RFDTCO'] = oggi_AS_date();
	$ar_ins['RFORCO'] = oggi_AS_time();

	$sql = "INSERT INTO {$cfg_mod_Gest['provvigioni']['file_righe']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	echo db2_stmt_errormsg($stmt);

	//blocco fattura
	$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_testate']} SET TFFG01 = ? WHERE TFDOCU = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array('Y', $all_params['docu']));	
	
	
	if ((float)$all_params['f_importo_provv'] == (float)$all_params['f_importo_provv_proposto'])
		$new_flag = 'M';	//maturato
	else 
		$new_flag = 'P';	//provvisorio
	
	//flaggo le righe come maturate
	$sql = "UPDATE {$cfg_mod_Gest['provvigioni']['file_righe']} SET RFFG03='{$new_flag}' WHERE 1=1 AND RFDOCU = ? AND RFTPRG=? AND RFCAGE=? AND RFFG03<>'M'";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($all_params['docu'], $all_params['tipologia'], $all_params['agente']));
		
	echo acs_je(array('success' => $result));
	exit;
}















// ******************************************************************************************
// Nuova provvigione (form)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_nuova_provvigione'){
	$m_params = acs_m_params_json_decode();

	//recupero riga in modifica
	$sql = "SELECT *
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			WHERE 1=1 AND TFDOCU = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->docu));
	$row = db2_fetch_assoc($stmt);

	?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Salva',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_nuova_provvigione',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {										
										    loc_win.fireEvent('afterUpdateRecord', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }],   		            
		            
		            items: [
						   {
							     name: 'docu'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->docu); ?>
							},{
							     name: 'tipologia'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($m_params->tipologia); ?>
							}, {
								name: 'f_agente',
								xtype: 'combo',							
								fieldLabel: 'Agente',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: false,		
							    anchor: '-15',						   													
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
									     <?php echo acs_ar_to_select_json(get_ar_agenti(), ""); ?>	
									    ] 
								}						 
							}, {
							     name: 'f_imponibile_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Imponibile provv.'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($row['TFTIPR']); ?>
							},		            
						   {
							     name: 'f_perc_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Percentuale'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($row['RFPPRA']); ?>
							   , listeners: {
								    'change': function(f,b){
								      //ricalcolo importo
								      form = f.up('form').getForm();
								      nuovo_importo_provv = floatRenderer2(
								      	form.findField('f_perc_provv').getValue() * form.findField('f_imponibile_provv').getValue() / 100
								      );
								      form.findField('f_importo_provv').setValue(nuovo_importo_provv);
								    }
								 }
							}, {
							     name: 'f_importo_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Importo'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($row['RFIMPO']); ?>
							}
		            
		            
						]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}





// ******************************************************************************************
// Nuova importo/percentuale positivo/negativo per cliente (form)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_nuova_ins_imp_age'){
	$m_params = acs_m_params_json_decode();
	$sped_module = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));	

	//recupero riga in modifica
	$sql = "SELECT *
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			WHERE 1=1 AND TFDOCU = ?";
	
	$rec_id_exp = explode("|", $m_params->rec_id);

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->docu));
	$row = db2_fetch_assoc($stmt);

	?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Salva e rimani',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_nuova_ins_imp_age',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {
										    loc_win.fireEvent('afterUpdateRecordRemain', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-save-32',
	                     text: 'Salva',
				            handler: function() {
				            
				            loc_win = this.up('window');
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_nuova_ins_imp_age',
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},
										success: function(form,action) {
										    loc_win.fireEvent('afterUpdateRecord', loc_win);										
										}																                        
				                });
			                }								            
				            
				         }
				        }],   		            
		            
		            items: [
						    {
							     name: 'f_agenzia'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($rec_id_exp[1]); ?>
							}, {
							     name: 'f_agente'                   		
							   , xtype: 'hiddenfield'
							   , value: <?php echo j($rec_id_exp[2]); ?>
							}, {
							     name: 'f_imponibile_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Imponibile provv.'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($m_params->imponibile_provv); ?>
							   , disabled: true
							},
							   {
							     name: 'f_data'                   		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, 
			            {
							name: 'f_cage',
							xtype: 'combo',
							fieldLabel: 'Causale',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,
						   	anchor: '-15',														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($sped_module->find_TA_std('CAGE'), ""); ?>	
								    ] 
							}						 
							},
						   {
							     name: 'f_perc_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Percentuale'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , listeners: {
									    'change': function(f,b){
									      //ricalcolo importo
									      form = f.up('form').getForm();
									      nuovo_importo_provv = floatRenderer2(
									      	form.findField('f_perc_provv').getValue() * form.findField('f_imponibile_provv').getValue() / 100
									      );
									      form.findField('f_importo_provv').setValue(nuovo_importo_provv);
									    }
									 }							   
							}, {
							     name: 'f_importo_provv'   
							   , flex: 1                		
							   , xtype: 'numberfield'
							   , fieldLabel: 'Importo'
							   , labelAlign: 'left'
							   , allowBlank: false
							   , anchor: '-15'
							   , value: <?php echo j($row['RFIMPO']); ?>
							}, {
					            xtype: 'textfield',
					            maxLenght: 100,
					            grow: true,
					            name: 'f_memo',
					            fieldLabel: 'Memo',
					            anchor: '-15'			            
					        }
		            
		            
						]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}






// ******************************************************************************************
// SCELTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_parameters'){
?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            id: 'acs_panel_provvigioni_open_parameters',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "PROVVIGIONI");  ?>{
	            text: 'Rapporto<br>sincronizzazione',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
	         
							acs_show_panel_std('acs_panel_rapporto_sincronizzazione.php?fn=open_tab', 'panel_fatture_anticipo', {chiave: 'REPORT'});
							this.up('window').close();
			
	            }
	        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Riepilogo<br/>Accreditato/Maturato',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_provvigioni_report_accreditato_maturato.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Riepilogo<br/>fatturato',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_provvigioni_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Elenco',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                   if (form.isValid()){
                                acs_show_win_std('Filtro', 'acs_panel_provvigioni_report_elenco.php?fn=open_filtri', {
		        				filter: form.getValues()
		    					},		        				 
		        				330, 150, {}, 'icon-print-16');	
		        	    	}
			                
			                
			               /* if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_provvigioni_report_elenco.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }	*/							            
				            
				         }
				        }, {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							if(form.isValid()){			            	
								acs_show_panel_std('acs_panel_provvigioni.php?fn=open_tab', 'panel-provvigioni', form.getValues());
								//this.up('window').close();
								this.up('window').hide();
							}
			            }
			         }],   		            
		            
		            items: [
		            
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "0 20 0 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "0 20 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
					 
					, { 
						xtype: 'fieldcontainer',
						anchor: '-15',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [					 
							   {
									name: 'f_divisione',
									xtype: 'combo',
									flex: 1,
									multiSelect: true,
									fieldLabel: 'Divisione',
									displayField: 'text',
									valueField: 'id',
									emptyText: '- seleziona -',
									forceSelection: true,
								   	allowBlank: true,		
								    anchor: '-15',
								    margin: "10 10 5 10",						   													
									store: {
										autoLoad: true,
										editable: false,
										autoDestroy: true,	 
									    fields: [{name:'id'}, {name:'text'}],
									    data: [								    
										     <?php echo acs_ar_to_select_json(get_ar_divisioni(), ""); ?>	
										    ] 
									}						 
								}, {
									name: 'f_gruppo',
									xtype: 'combo',
									multiSelect: true,
									fieldLabel: 'Gruppo', labelAlign: 'right',
									displayField: 'text',
									valueField: 'id',
									emptyText: '- seleziona -',
									forceSelection: true,
								   	allowBlank: true,		
								    anchor: '-15',
								    margin: "10 10 5 10",	
								    flex: 1,					   													
									store: {
										autoLoad: true,
										editable: false,
										autoDestroy: true,	 
									    fields: [{name:'id'}, {name:'text'}],
									    data: [								    
										     <?php echo acs_ar_to_select_json(get_ar_gruppi(), ""); ?>	
										    ] 
									}						 
								}
							]
						}	
						
						
						
						
						, {
							name: 'f_agenzia',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Agenzia',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_agenzie(), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_agente',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Agente',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_agenti(), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_tipo_documento',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Tipo documento',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_tipo_documento(), ""); ?>	
								    ] 
							}						 
						}, {
				            xtype: 'combo',
							name: 'f_cliente_cod',
							fieldLabel: 'Cliente',
							minChars: 2,			
				            margin: "7 20 7 10",
				            
				            store: {
				            	pageSize: 1000,
				            	
								proxy: {
						            type: 'ajax',
						            
						            
						            url : <?php echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli'); ?>,
						            		
						            reader: {
						                type: 'json',
						                root: 'root',
						                totalProperty: 'totalCount'
						            }
						        },       
								fields: ['cod', 'descr', 'out_loc'],		             	
				            },
				                        
							valueField: 'cod',                        
				            displayField: 'descr',
				            typeAhead: false,
				            hideTrigger: true,
				            anchor: '100%',
				            
					        listeners: {
					            change: function(field,newVal) {	            	
					            }
					        },            
				
				            listConfig: {
				                loadingText: 'Searching...',
				                emptyText: 'Nessun cliente trovato',
				                
				
				                // Custom rendering template for each item
				                getInnerTpl: function() {
				                    return '<div class="search-item">' +
				                        '<h3><span>{descr}</span></h3>' +
				                        '[{cod}] {out_loc}' + 
				                    '</div>';
				                }                
				                
				            },
				            
				            pageSize: 1000
				
				        }, {
							name: 'f_tipologia',
							xtype: 'radiogroup',
							fieldLabel: 'Stato',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "0 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_tipologia' 
		                          , boxLabel: 'Accreditato'
		                          , inputValue: 'ACCREDITATO'
		                          , checked: true		                          
		                        },{
		                            xtype: 'radio'
		                          , name: 'f_tipologia' 
		                          , boxLabel: 'Maturato'
		                          , inputValue: 'MATURATO'		                          
		                        },{
		                            xtype: 'radio'
		                          , name: 'f_tipologia' 
		                          , boxLabel: 'Liquidato'
		                          , inputValue: 'LIQUIDATO'		                          
		                        }]
						}, {
							name: 'f_fatture_o_ordini',
							xtype: 'radiogroup',
							fieldLabel: 'Fatture/Ordini',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "0 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_fatture_o_ordini' 
		                          , boxLabel: 'Fatture'
		                          , inputValue: 'VF'
		                          , checked: true		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_fatture_o_ordini' 
		                          , boxLabel: 'Ordini'
		                          , inputValue: 'VO'		                          
		                        }, {
		                            xtype: 'label' 
		                          , label: ''		                          
		                        }]
						}, {
							name: 'f_rettifiche',
							xtype: 'radiogroup',
							fieldLabel: 'Rettifiche',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "0 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_rettifiche' 
		                          , boxLabel: 'Includi'
		                          , inputValue: 'INCLUDI'
		                          , checked: true		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_rettifiche' 
		                          , boxLabel: 'Escludi'
		                          , inputValue: 'ESCLUDI'		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_rettifiche' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'SOLO'		                          
		                        }]
						}, {
							name: 'f_residuo',
							xtype: 'radiogroup',
							fieldLabel: 'Residuo',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "0 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_residuo' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_residuo' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          , checked: true		                          
		                        }, {
		                            xtype: 'label' 
		                          , label: ''		                          
		                        }]
						}]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();	
?>
{
 success:true, 
 items: [
  <?php write_main_tree((array)$m_params); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// SCADUTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_scaduto'){
	$m_params = acs_m_params_json_decode();
	
	//sposto indietro le date di un anno
	$m_params->dataStart = date('Ymd', strtotime($m_params->dataStart . " -1 years"));
	$m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " -1 years"));
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "scaduto",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// OLTRE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_oltre'){
	$m_params = acs_m_params_json_decode();
	
	//sposto indietro le date di un anno
	$m_params->dataStart = date('Ymd', strtotime($m_params->dataStart . " +1 years"));
	$m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " +1 years"));
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "oltre",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// DETTAGLIO PER RIFERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_riferimento'){
	$m_params = acs_m_params_json_decode();
	
	//recuper data iniziale e data finale (dalla cella su cui ho fatto dbclick)
	$col_name_ar = explode('_', $m_params->col_name);
	$dataStart = $col_name_ar[1];
	$dataEnd 	 = $col_name_ar[2];

	//da record_id recupero raggruppamento e categoria
	$record_id_ar = explode("|", $m_params->record_id);
	$raggruppamento = $record_id_ar[0];
	$categoria		= $record_id_ar[1];
	
	?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'hbox',
	    align: 'stretch',
	    pack : 'start',
	},
	items: [ 
		  <?php write_dettaglio_riferimento(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria
		  )); ?>,
		  <?php write_dettaglio_righe(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria
		  )); ?>		  
	]
  }	   
 ]
}
<?php exit; } ?>

