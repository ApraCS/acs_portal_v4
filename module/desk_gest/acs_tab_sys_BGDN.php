<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
$puvn = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";



$m_table_config = array(
    'TAID' => 'BGDN',
    'msg_on_save' => true,
    'title_grid' => 'BGDN-Gruppo doganale',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        //'TACINT'   => array('label'	=> 'Interfaccia', 'only_view' => 'F',  'maxLength' => 10),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        //'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella gruppo doganale nazioni',
   // 'j_puvn' => 'Y',
   // 'ricerca_var' => 'Y',
   // 'tab_tipo' => 'PUVR',
  
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
