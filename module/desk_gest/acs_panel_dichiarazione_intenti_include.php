<?php

function sum_columns_value(&$ar_r, $r){
	$ar_r['tot_cod'] += $r['TFTOTD'];
	$ar_r['TFTIMP'] += $r['TFTIMP'];
	$ar_r['detratto'] += $r['detratto'];
	

	//if ($r['TFFG01']!= 'C' && $r['TFFG01'] != 'M' ){
		$ar_r['residuo'] += $r['TFTIMP']+$r['detratto'];
	//}


}

function detratto($r, $stmt){

	$fat= $r['TFDOCU'];
	$result = db2_execute($stmt, array($fat));
	$row = db2_fetch_assoc($stmt);
	return array($row['S_RFIMPP'], $row['N_RFIMPP']);
}

function crea_ar_tree_dichiarazione_intenti($node, $form_values){

	global $cfg_mod_Gest, $conn;

	$ar = array();
	$ret = array();


	if ($node == '' || $node == 'root'){

		$sql_where.= sql_where_by_combo_value('TF.TFCCON', $form_values->f_cliente_cod);

		//$sql_where.= sql_where_by_combo_value('TF.TFNAZI', $form_values->f_nazione);

		//$sql_where.= sql_where_by_combo_value('TF.TFTPDO', $form_values->f_tipo_documento);

		//controllo data
		if (strlen($form_values->f_data_da) > 0)
			$sql_where .= " AND TF.TFDTRG >= {$form_values->f_data_da}";
			if (strlen($form_values->f_data_a) > 0)
				$sql_where .= " AND TF.TFDTRG <= {$form_values->f_data_a}";


				//controllo fattura(aperta/chiusa)
				/*$filtro_res=$form_values->f_filtra_fatture;

				if($filtro_res== "C"){
					$sql_where.=" AND TFFG01 IN ('C','M')";
						
				}else if ($filtro_res== "A"){
						
					$sql_where.=" AND TFFG01 <>'C' AND TFFG01 <> 'M'";
				}
*/

				$livello1_expanded=false;
				$livello2_expanded=false;


				/*if (is_array($form_values->f_nazione)){
					if (count($form_values->f_nazione) > 0)
						$livello1_expanded=true;
				}*/

				if (strlen($form_values->f_cliente_cod)>0){
					$livello1_expanded=true;
					$livello2_expanded=true;
				}


				$sql = "SELECT TFTIDO, TFAADO, TFNRDO, TFCCON, TFDCON, TFNAZI, TFDOCU,
				TFDNAZ, TFTOTD, TFTIMP, TFDTRG, TFAADO, TFNRDO, TFTPDO, TFFG01, TFRGSV,
				TFINUM, TFAADO, TFTPDO, TFDTPD, TFDT, TFPROG, TFCODO, TFDTIM
				FROM  {$cfg_mod_Gest['dichiarazione_intenti']['file_testate']} TF
				WHERE TFTIDO='DI' {$sql_where} ORDER BY TFDNAZ, TFDCON";
	

				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt);

				$sql= "SELECT sum(RFIMPP) as S_RFIMPP, count(*) as N_RFIMPP, RFDOCU
				FROM {$cfg_mod_Gest['dichiarazione_intenti']['file_righe']} RF
				WHERE RFDOCU= ? GROUP BY RFDOCU";

				$stmt_detratto = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();

				while ($row = db2_fetch_assoc($stmt)) {

					$tmp_ar_id = array();
					$ar_r= &$ar;

					//stacco dei livelli
					$cli_liv_tot = 'TOTALE';	//totale
					$cod_liv0 = trim($row['TFNAZI']); //nazione
					$cod_liv1 = trim($row['TFCCON']); //codice cliente
					$cod_liv2 = trim($row['TFDOCU']); //chiave documento

					$ar_detratto=detratto($row, $stmt_detratto);

					$row['detratto']=$ar_detratto[0];


					//LIVELLO TOTALE
					$liv = $cod_liv_tot;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = 'liv_totale';
						$ar_new['liv_cod'] = 'TOTALE';
						$ar_new['task'] = 'Totale';
						$ar_new['liv'] = 'liv_totale';
						$ar_new['expanded'] = true;
						$ar_r["{$liv}"] = $ar_new;
					}
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $row);

					//creo la nazione
					$liv=$cod_liv0;
					$ar_r = &$ar_r['children'];
					$tmp_ar_id[] = $liv;
					if(!isset($ar_r[$liv])){
						$ar_new= array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						$ar_new['codice']=$liv;
						$ar_new['task'] = $row['TFDNAZ'];
						$ar_new['liv'] = 'liv_1';
						$ar_new['expanded'] =$livello1_expanded;
						$ar_r[$liv]=$ar_new;
					}

					$ar_r=&$ar_r[$liv];
					sum_columns_value($ar_r, $row);

					//creo cliente
					$liv=$cod_liv1;
					$ar_r=&$ar_r['children'];
					$tmp_ar_id[] = $liv;
					if(!isset($ar_r[$liv])){
						$ar_new= array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						$ar_new['codice']=$liv;
						$ar_new['task']= $row['TFDCON'];
						$ar_new['ditta']= $row['TFDT'];
						$ar_new['liv'] = 'liv_2';
						$ar_new['expanded'] =$livello2_expanded;
						$ar_r[$liv]=$ar_new;
					}

					$ar_r=&$ar_r[$liv];
					sum_columns_value($ar_r, $row);


					//chiave documento
					$liv=$cod_liv2;
					$ar_r=&$ar_r['children'];
					$tmp_ar_id[] = $liv;
					
					if(!isset($ar_r[$liv])){
						$ar_new= array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						$ar_new['codice']=$row['TFPROG'];
						$ar_new['task']='Dichiarazione di intento';
						$ar_new['data_in'] = $row['TFDTRG'];
						$ar_new['data_fin'] = $row['TFDTIM'];
						$ar_new['cod_ass'] = $row['TFCODO'];
						$ar_new['rif'] = trim($row['TFRGSV']);
						$ar_new['liv'] = 'liv_3';
						$ar_new['liv_cod_qtip'] = acs_u8e($row['TFDTPD']);
						$ar_new['stato']=$row['TFFG01'];
						if($ar_detratto[1]==0){
							$ar_new['leaf']=true;
						}

						$ar_r[$liv]=$ar_new;
					}

					$ar_r=&$ar_r[$liv];
					sum_columns_value($ar_r, $row);
				}

	}else{

		$value = explode("|", $node);
		$value[0]; //id_nazione
		$value[1]; //id_cliente
		$value[2]; //id_fattura
			

		$sql= "SELECT * FROM {$cfg_mod_Gest['dichiarazione_intenti']['file_righe']} RF WHERE RFDOCU= ".sql_t($value[2]). "ORDER BY RFDTRG";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);

		while ($r = db2_fetch_assoc($stmt)){
	
			$ar_new = array();
			$ar_new['id'] = implode("|", array($node, $r['RFNOTE']));
			$ar_new['codice']=$r['RFCAUS'];
			$ar_new['task']= $r['RFNOTE'];
			$ar_new['detratto']=$r['RFIMPP'];
			$ar_new['data_in']=$r['RFDTRG'];
			$ar_new['cod_ass'] = $r['RFCAGE'];
			$ar_new['tot_cod'] = $r['RFIMLQ'];
			$ar_new['liv'] = 'liv_4';
			$ar_new['leaf']=true;
			$ar[] = $ar_new;

		}
	}

	return $ar;
}