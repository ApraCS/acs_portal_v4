<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();



//recupero nome_ditta
function get_nome_ditta($codice_ditta){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT DITTA FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE CODICE_DITTA=? GROUP BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($codice_ditta));
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		return $r['DITTA'];
	}
	return '';
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data_centri_di_costo'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT SMCTD0, SMCDE0
			FROM {$cfg_mod_Gest['abi']['file_movimenti']}
			WHERE UPPER(
			REPLACE(REPLACE(SMCDE0, '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
							GROUP BY SMCTD0, SMCDE0
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array( "cod" 		=> trim($row['SMCTD0']),
						"descr" 	=> acs_u8e(trim($row['SMCDE0']))
		);
	}

	echo acs_je($ret);

	exit;
}



// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_report'){
	include_once('acs_panel_abi_azienda_report.php');
	exit;
}


// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$m_params = acs_m_params_json_decode();
	
	if ($_REQUEST['node'] == 'root'){
		$sql_where = " AND NOME_ANALISI = " . sql_t($m_params->nome_analisi_1);
		$sql_where .= " AND NOME_ANALISI_CONFR = " . sql_t($m_params->nome_analisi_2);		
		$sql_where .= " AND CODICE_DITTA = " . sql_t($m_params->codice_ditta);
		
		$sql = "SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE 1=1 {$sql_where} ORDER BY ORDINAMENTO";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();
		
		
		while ($r = db2_fetch_assoc($stmt)) {
			$r['task'] 		= "[" . trim($r['VOCE_ANALISI']) . "] " . trim(acs_u8e($r['DES_VOCE_ANALISI']));
			$r['id'] 		= trim($r['VOCE_ANALISI']);
			$r['tipo_voce'] = trim($r['TIPO_VOCE']);

			if (trim($r['TIPO_VOCE']) != 'D')
				$r['leaf'] = true;
			
			$ar[] = $r; 	
		}		
		echo acs_je($ar);
	} else {
		
		//nel sottolivello vado a mostrare i conti dal file dei movimenti (raggruppati)
		$sql_where = " AND M.NOME_ANALISI IN (" . sql_t($m_params->nome_analisi_1) . ", " . sql_t($m_params->nome_analisi_2) . ")";
		$sql_where .= " AND M.CODICE_DITTA = " . sql_t($m_params->codice_ditta);
		$sql_where .= " AND M.VOCE_ANALISI = " . sql_t($_REQUEST['node']);
		
		if (strlen($m_params->centro_di_costo) > 0)
			$sql_where .= " AND SMCTD0 = " . sql_t($m_params->centro_di_costo);
	
		$sql = "SELECT M.NOME_ANALISI, M.CODICE_CONTO, M.DESCRIZIONE, SUM(M.IMPORTO) AS IMPORTO, R1001.IMPORTO_A
					 FROM {$cfg_mod_Gest['abi']['file_movimenti']} M
					  LEFT OUTER JOIN (SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']}) R1001
					    ON M.CODICE_DITTA = R1001.CODICE_DITTA
					   AND R1001.VOCE_ANALISI = '1001' 
	           		   AND R1001.NOME_ANALISI = " . sql_t($m_params->nome_analisi_1) . "
			   		   AND R1001.NOME_ANALISI_CONFR = " . sql_t($m_params->nome_analisi_2) . "
		
				WHERE 1=1 {$sql_where}
				GROUP BY M.NOME_ANALISI, M.CODICE_CONTO, M.DESCRIZIONE, R1001.IMPORTO_A
				ORDER BY M.CODICE_CONTO
				";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();		
		
		while ($r = db2_fetch_assoc($stmt)) {
			$id_liv = trim($r['CODICE_CONTO']);
			
			if (!isset($ar["{$id_liv}"])){
				$ar_new = array();
				$ar_new['id'] = $id_liv;				
				$ar_new['task'] = "[" . trim($r['CODICE_CONTO']) . "] " . trim(acs_u8e($r['DESCRIZIONE']));
				$ar_new['tipo_voce'] = 'dett';				
				$ar_new['leaf'] = true;
				$ar["{$id_liv}"] = $ar_new;
			}

			
			$ar_r = &$ar["{$id_liv}"];

			
			if ($r['NOME_ANALISI'] == $m_params->nome_analisi_1){
				$ar_r['IMPORTO_A'] = trim($r['IMPORTO']);
				if ((float)trim($r['IMPORTO_A']) != 0)
					$ar_r['IMPORTO_B'] = (int)trim($r['IMPORTO']) / (int)trim($r['IMPORTO_A']) * 100;
			}
			if ($r['NOME_ANALISI'] == $m_params->nome_analisi_2){
				$ar_r['IMPORTO_C'] = trim($r['IMPORTO']);
				if ((float)trim($r['IMPORTO_A']) != 0)				
					$ar_r['IMPORTO_D'] = (int)trim($r['IMPORTO']) / (int)trim($r['IMPORTO_A']) * 100;
			}

		}
		
		
		$ret = array();
		foreach($ar as $kar => $r){
			$ret[] = array_values_recursive($ar[$kar]);
		}
		
		
		echo acs_je($ret);
	}
		
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();

?>

{
 success:true, items: [

 		{ 
  			xtype: 'treepanel',
	        title: <?php echo acs_je("ABI_Azienda {$m_params->codice_ditta}"); ?>,
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        
	        tbar: new Ext.Toolbar({
	            items:[<?php echo j('<b>Analisi riclassificazione dati di bilancio per azienda ' . get_nome_ditta($m_params->codice_ditta) . ' [Periodo: ' . $m_params->nome_analisi_1. ', periodo raffronto: ' . $m_params->nome_analisi_2 . ']</b>'); ?>, '->'
/*	            
		  , {
            xtype: 'combo',
			name: 'f_centro_di_costo',
			fieldLabel: 'Centro di costo',
			minChars: 2,			
            margin: "0 20 0 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		$cfg_mod = $main_module->get_cfg_mod();
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_centri_di_costo');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	
					if (Ext.isEmpty(newVal)){
						this.up('panel').getStore().proxy.extraParams.centro_di_costo = '';
						this.up('panel').getStore().load();					
					}						            
	            },
	            select: function(field,newVal) {	
					this.up('panel').getStore().proxy.extraParams.centro_di_costo = field.getValue();
					this.up('panel').getStore().load();	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun centro di costo trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}]' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }	            
 */
	            
	           	, {iconCls: 'tbar-x-tool x-tool-print',
							 tooltip: 'Stampa riclassifica dettagliata',	           	
	           				 handler: function(event, toolEl, panel){

								var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report',
								        params: {extraParams: Ext.JSON.encode(this.up('panel').getStore().proxy.extraParams)},
								        });	           	
	           		           	
	           	}}	            
				, {iconCls: 'tbar-x-tool x-tool-print',
							tooltip: 'Stampa riclassifica', 
							handler: function(event, toolEl, panel){

								var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report&dettagliata=N',
								        params: {extraParams: Ext.JSON.encode(this.up('panel').getStore().proxy.extraParams)},
								        });	           	
	           		           	
	           	}}	            	           	
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),  
	        
	        
	        
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'IMPORTO_A', 'IMPORTO_B', 'IMPORTO_C', 'IMPORTO_D', 'IMPORTO_E', 'IMPORTO_F', 'IMPORTO_G', 'IMPORTO_H',
				    		 'tipo_voce'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                        
                      extraParams: {
                        codice_ditta: <?php echo acs_je($m_params->codice_ditta); ?>,
                        nome_analisi_1: <?php echo acs_je($m_params->nome_analisi_1); ?>,
                        nome_analisi_2: <?php echo acs_je($m_params->nome_analisi_2); ?>,
                        centro_di_costo: ''
                      }
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),

            multiSelect: false,
	        singleExpand: false,
	
			columns: [	
	    		{text: 'Voce', flex: 1, xtype: 'treecolumn', dataIndex: 'task'}
	    		
				 , {header: <?php echo acs_je("Periodo {$m_params->nome_analisi_1}"); ?>, width: 180,
				    columns: [ 			
					 , {header: 'Importo', dataIndex: 'IMPORTO_A', width: 100, align: 'right', renderer: floatRenderer0}
					 , {header: '%', dataIndex: 'IMPORTO_B', width: 70, align: 'right', renderer: floatRenderer2}				 					 
				  ]
				 }	    		

				 , {header: <?php echo acs_je("Periodo {$m_params->nome_analisi_2}"); ?>, width: 180,
				    columns: [ 			
					 , {header: 'Importo', dataIndex: 'IMPORTO_C', width: 100, align: 'right', renderer: floatRenderer0}
					 , {header: '%', dataIndex: 'IMPORTO_D', width: 70, align: 'right', renderer: floatRenderer2}				 					 
				  ]
				 }	    		
				 
				 
				 , {header: 'Scostamento', width: 180,
				    columns: [ 			
					 , {header: 'Importo', width: 100, align: 'right',
			          	  renderer: function (value, metaData, record, row, col, store, gridView){						
							return floatRenderer0(parseFloat(record.get('IMPORTO_A')) - parseFloat(record.get('IMPORTO_C')));			    
							}					 
					   }
					 , {header: '%', width: 70, align: 'right',
			          	  renderer: function (value, metaData, record, row, col, store, gridView){
			          	    if (parseFloat(record.get('IMPORTO_C')) != 0)						
								return floatRenderer2( (parseFloat(record.get('IMPORTO_A')) - parseFloat(record.get('IMPORTO_C'))) / parseFloat(record.get('IMPORTO_C')) * 100);			    
							}					 					 
					 }				 					 
				  ]
				 }				 
	    		
	    			   	
			],
			enableSort: false, // disable sorting

	        listeners: {
		        	beforeload: function(store, options) {
		        		Ext.getBody().mask('Loading... ', 'loading').show();
		        		},		
			
		            load: function () {
		              Ext.getBody().unmask();
		            },
		            
		            
					celldblclick: {
						fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		
							rec = iView.getRecord(iRowEl);
						    col_name = iView.getGridColumns()[iColIdx].dataIndex;
						    
						    //esco se sono in altrec colonne
						    if (col_name != 'IMPORTO_A' && col_name != 'IMPORTO_C')
						     return false;
						    
						    //verifico su quale colonna ho fatto doppio click

						    if (col_name == 'IMPORTO_A')
						    	ceal = <?php echo acs_je($m_params->nome_analisi_1); ?>;
						    if (col_name == 'IMPORTO_C')
						    	ceal = <?php echo acs_je($m_params->nome_analisi_2); ?>; 	
						    
						    
						    
						    //in col_name ho in pratico il codice ditta
						    acs_show_panel_std('acs_panel_abi_anno.php?fn=open_tab', Ext.id(), {
		                        codice_ditta: <?php echo acs_je($m_params->codice_ditta); ?>,
		                        nome_analisi_1: <?php echo acs_je($m_params->nome_analisi_1); ?>,
		                        nome_analisi_2: <?php echo acs_je($m_params->nome_analisi_2); ?>,
		                        ceal: ceal
						    });
		
						}
					}
		            
		            	        
		        }
			

		, viewConfig: {
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }
   
		    }												    
		        
			
			    		
 	} 
 
 
 
 
 ]
}
<?php exit; } ?>