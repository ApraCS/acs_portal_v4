<?php

require_once "../../config.inc.php";

$m_params = acs_m_params_json_decode();

function genera_TAREST($m_table_config, $values){
    $value_attuale = $values->TAREST;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
        //$new_value = "";
        foreach($m_table_config['TAREST'] as $k => $v){
            if(isset($values->$k)){
                if(in_array($k, array('sc1', 'sc2', 'sc3', 'sc4', 'provg')))
                    $chars = number_to_text($values->$k, 2, 2);
                    else
                        $chars = $values->$k;
                        
                        if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
                            $len = "%{$v['riempi_con']}{$v['len']}s";
                            else
                                $len = "%-{$v['len']}s";
                                
                                $chars = substr(sprintf($len, $chars), 0, $v['len']);
                                $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                                $value_attuale = $new_value;
                                
            }
            
        }
        
        
        
        
        return $new_value;
}

function get_m_row($row, $m_table_config){
    
    $row['RRN'] = acs_u8e(trim($row['RRN']));
    
    $row['TADESC'] = acs_u8e(trim($row['TADESC']));
    $row['TADES2'] = trim($row['TADES2']);
    $row['TACOR1'] = trim($row['TACOR1']);
    $row['TACOR2'] = trim($row['TACOR2']);
    $row['TACINT'] = trim($row['TACINT']);
    $row['TANR'] = trim($row['TANR']);
    
    if(is_array($m_table_config['TAREST'])){
        foreach($m_table_config['TAREST'] as $k => $v){
            $row[$k] = trim(substr($row['TAREST'], $v['start'], $v['len']));
            
            if(substr($k, 0, 4) == 'data'){
                $row[$k] = print_date(substr($row['TAREST'], $v['start'], $v['len']));
            }else{
                $row[$k] = trim(substr($row['TAREST'], $v['start'], $v['len']));
            }
        }
        
    }
    
    
    if($m_table_config['TAID'] == 'CUAG'){
        $row['provg']  = text_to_number(substr($row['TAREST'], 171, 4), 2, 2);
        $row['provvigione'] = $row['provg'] . "%";
        $forn = substr($row['TAREST'], 162, 9);
        if(trim($forn) != ''){
            $d_forn = $main_module->decod_cliente($forn, 'F');
            $row['d_forn']  = $d_forn;
            $row['c_forn']  = "[{$forn}]";
        }}
        
        if($m_table_config['TAID'] == 'ITAL'){
            $itin  = substr($row['TAREST'], 0, 3);
            
            if(trim($itin) != ''){
                $desc_itin  = get_TA_sys('BITI', trim($itin));
                $row['d_itin'] = "[". trim($itin)."] ".$desc_itin['text'];
            }}
            
            
            if($m_table_config['TAID'] == 'CUBA'){
                $row['TADESC'] = $row['TADESC'].$row['TADES2'];
                $row['age'] = trim(substr($row['TAREST'], 0, 30)).trim(substr($row['TAREST'], 30, 30));
            }
            
            if($m_table_config['TAID'] == 'XSPM'){
                $val_var1   = find_TA_sys('PUVR', substr($row['TAREST'], 46, 3));
                $val_var2   = find_TA_sys('PUVR', substr($row['TAREST'], 53, 3));
                $row['t_var1'] = $val_var1[0]['text'];
                $row['t_var2'] = $val_var2[0]['text'];
                
                $val_van1   = find_TA_sys('PUVN', substr($row['TAREST'], 50, 3), null, substr($row['TAREST'], 46, 3));
                $val_van2   = find_TA_sys('PUVN', substr($row['TAREST'], 57, 3), null, substr($row['TAREST'], 53, 3));
                $row['t_van1'] = $val_van1[0]['text'];
                $row['t_van2'] = $val_van2[0]['text'];
                
                
                $row['val_ini'] = print_date(substr($row['TAREST'], 17, 8));
                $row['val_fin'] = print_date(substr($row['TAREST'], 25, 8));
                
                
                $sconti = "";
                $row['sc1'] = text_to_number(substr($row['TAREST'], 0, 4), 2, 2);
                if(trim($row['sc1']) != '0')
                    $sconti .= $row['sc1']."%";
                    $row['sc2'] = text_to_number(substr($row['TAREST'], 4, 4), 2, 2);
                    if(trim($row['sc2']) != '0')
                        $sconti .= ", ".$row['sc2']."%";
                        $row['sc3'] = text_to_number(substr($row['TAREST'], 8, 4), 2, 2);
                        if(trim($row['sc3']) != '0')
                            $sconti .= ", ".$row['sc3']."%";
                            $row['sc4'] = text_to_number(substr($row['TAREST'], 12, 4), 2, 2);
                            if(trim($row['sc4']) != '0')
                                $sconti .= ", ".$row['sc4']."%";
                                $row['sconti'] = $sconti;
                                $cliente = substr($row['TAREST'], 34, 9);
                                if(trim($cliente) != ''){
                                    $d_cliente = $main_module->decod_cliente($cliente);
                                    $row['d_cli']  = $d_cliente;
                                }
                                
            }
            
            if($m_table_config['TAID'] == 'BCCG'){
                
                $tarest2 = sprintf("%-5s", $row['TAORDI2']);
                $tarest2 .= sprintf("%-30s", $row['TADESC2']);
                $tarest2 .= sprintf("%-30s", $row['TADES22']);
                $tarest2 .= $row['TAREST2'];
                $row['TAREST2'] = $tarest2;
                
                $ini = substr($row['TAREST'], 95, 4).substr($row['TAREST'], 93, 2).substr($row['TAREST'], 91, 2);
                $fin = substr($row['TAREST'], 103, 4).substr($row['TAREST'], 101, 2).substr($row['TAREST'], 99, 2);
                
                $row['dtvi'] = print_date($ini);
                $row['dtvf'] = print_date($fin);
                if($fin < oggi_AS_date())
                    $row['scaduta'] = 'Y';
                
                $n = 0;
                for($a = 0; $a <=27 ; $a+=3){
                    $n++;
                    $row["agente{$n}"] = trim(substr($row['TAREST2'], $a, 3));
                }
                
                $row["agente_ie"] = trim(substr($row['TAREST2'], 30, 1));
                
                $n = 0;
                for($a = 31; $a <=58 ; $a+=3){
                    $n++;
                    $row["nazione{$n}"] = trim(substr($row['TAREST2'], $a, 3));
                }
                $row["nazione_ie"] = trim(substr($row['TAREST2'], 61, 1));
                
                $n = 0;
                for($a = 62; $a <=89 ; $a+=3){
                    $n++;
                    $row["zona{$n}"] = trim(substr($row['TAREST2'], $a, 3));
                }
                $row["zona_ie"] = trim(substr($row['TAREST2'], 92, 1));
                
                $n = 0;
                for($a = 93; $a <=111 ; $a+=2){
                    $n++;
                    $row["prov{$n}"] = trim(substr($row['TAREST2'], $a, 2));
                }
                $row["prov_ie"] = trim(substr($row['TAREST2'], 113, 1));
                
            }
            
            return $row;
    
}