<?php

require_once("../../config.inc.php");

$main_module = new DeskGest(array('no_verify' => 'Y'));
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));





// ******************************************************************************************
//
// ******************************************************************************************
if ($_REQUEST['fn'] == 'search_comune'){
$m_params = acs_m_params_json_decode();

global $conn;
global $cfg_mod_Spedizioni;
$ar = array();

$sql_where = '';
$sql_where.= sql_where_by_combo_value('TAKEY4', $m_params->gcnazi);

$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} 
            WHERE TATAID = 'ANCOM' {$sql_where}
			AND UPPER(
			REPLACE(REPLACE(TADESC, '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			order by TADESC
			FETCH FIRST 500 ROWS ONLY
		";

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);

$ret = array();
while ($row = db2_fetch_assoc($stmt)) {
if (is_null($k1) || $is_get == 'Y') $m_id = $row['TAKEY1'];
else $m_id = $row['TAKEY2'];

$r = array();

		$r['id'] 		= trim($row['TAKEY1']);
		$r['text']		= acs_u8e(trim($row['TADESC']));
		
		$r['cod'] 		= trim($row['TAKEY1']);
		$r['descr']		= acs_u8e(trim($row['TADESC']));


		$r['TAKEY1'] = $row['TAKEY1'];
		$r['TAKEY2'] = $row['TAKEY2'];
		$r['TAKEY3'] = $row['TAKEY3'];
		$r['TAKEY4'] = $row['TAKEY4'];
		$r['TAKEY5'] = $row['TAKEY5'];
		$r['TARIF2'] = $row['TARIF2'];
		$r['TAFG01'] = $row['TAFG01'];
		$r['TAFG02'] = $row['TAFG02'];

		$r['COD_PRO'] = trim($row['TAKEY2']);
		$r['COD_REG'] = trim($row['TAKEY3']);
		$r['COD_NAZ'] = trim($row['TAKEY4']);
				 
 		$r['DES_PRO'] = trim($s->decod_std('ANPRO', $r['COD_PRO']));
		$r['DES_REG'] = trim($s->decod_std('ANREG', $r['COD_REG']));
		$r_regione 	= $s->get_TA_std_by_des('ANREG', null, $r['COD_REG']);
		$r['ZONA'] = trim($r_regione['TANAZI']);
		$r['DES_NAZ'] = trim($s->decod_std('ANNAZ', $r['COD_NAZ']));

		$r['out_loc'] = implode(", ", array($r['DES_PRO'], $r['DES_REG']));

		$ret[] = $r;
	} //while

echo acs_je($ret);
		exit;
}


// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_search'){	
	$m_params = acs_m_params_json_decode();

?>

{
 success:true, items: [

 		{ 
 			xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '15 5 10',
            frame: true,
            layout: 'fit',
            title: '',
            
			buttons: [{
	            text: 'Seleziona',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	if(form.isValid()){
	            		loc_win.fireEvent('afterSelected', loc_win, form.findField('f_comune').valueModels[0]);
	            	}
	           	}
	         }
	        ],
            
            
 		
	        items: [

	        
							{
						            xtype: 'combo',
									name: 'f_comune',
									flex: 1,
									fieldLabel: 'Localit&agrave;', labelAlign: 'top',
									minChars: 2,			
						            
						            store: {
						            	pageSize: 500,
						            	
										proxy: {
								            type: 'ajax',		            
								            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=search_comune',
								            actionMethods: {read: 'POST'},
								            extraParams: <?php echo json_encode($m_params) ?>,		            		
								            reader: {
								                type: 'json',
								                root: 'root',
								                totalProperty: 'totalCount'
								            }
								            , doRequest: personalizza_extraParams_to_jsonData  
								        },       
										fields: ['cod', 'descr', 'out_loc', 'COD_PRO', 'COD_REG', 'COD_NAZ', 'DES_PRO', 'DES_REG', 'DES_NAZ', 'ZONA'],		             	
						            },
						                        
									valueField: 'cod',                        
						            displayField: 'descr',
						            typeAhead: false,
						            hideTrigger: true,
						            
							        listeners: {
							            change: function(field, newVal) {
							     			//var m_form = this.up('form').getForm();
							     			//m_form.findField('f_provincia').setValue(field.lastSelection[0].data.DES_PRO);
							     			//m_form.findField('f_regione').setValue(field.lastSelection[0].data.DES_REG);
							     			//m_form.findField('f_nazione').setValue(field.lastSelection[0].data.DES_NAZ);	            	
							            }
							        },            
						
						            listConfig: {
						                loadingText: 'Searching...',
						                emptyText: 'Nessun dato trovato',
						                
						
						                // Custom rendering template for each item
						                getInnerTpl: function() {
						                    return '<div class="search-item">' +
						                        '<h3><span>{descr}</span></h3>' +
						                        '[{out_loc}]' + 
						                    '</div>';
						                }                
						                
						            },            
						            pageSize: 500
						        }	        
	        

	            
		]
			    		
 	} 
 
 ]
}
<?php exit; } ?>