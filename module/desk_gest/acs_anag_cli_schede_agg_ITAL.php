<?php


function get_array_data($row){
    global $s;
    
    $nr = array();
    
    $nr['rrn']           = $row['RRN'];
    $nr['riga']          = $row['TANR'];
    $nr['tadesc']        = $row['TADESC'];
    $nr['tades2']        = $row['TADES2'];
    $nr['cliente']       = $row['TACOR1'];
   // $nr['dest']          = trim($row['TACOR2']); 
    $nr['itinerario']    = substr($row['TAREST'], 0, 3);
    $desc_itin           = get_TA_sys('BITI', trim($nr['itinerario']));
    $nr['d_itin']        = "[". $nr['itinerario'] ."] ".$desc_itin['text'];
    $nr['gr_doc']        = substr($row['TAREST'], 33, 3);
    $desc_gru            = get_TA_sys('RDOC', trim($nr['gr_doc']));
    $nr['d_gruppo']      = "[". $nr['gr_doc'] ."] ".$desc_gru['text'];
    $nr['divisione']     = substr($row['TAREST'], 36, 3);
    $desc_div            = get_TA_sys('DDOC', trim($nr['divisione']));
    $nr['d_div']         = "[". $nr['divisione'] ."] ".$desc_div['text'];
    $nr['tarest']        = $row['TAREST'];
    return $nr;
    
}


function out_fields(){
    global $s;
    
    $ar_fields = array('rrn', 'riga', 'tadesc', 'tades2', 'itinerario', 'd_itin', 'gr_doc', 'd_gruppo', 'divisione', 'd_div', 'tarest');

    
    return $ar_fields;
    
}


function out_columns(){
    
    ob_start(); 
    
    ?>
           {
            header   : 'Descrizione',
            dataIndex: 'tadesc',
            flex : 1
            },{
            header   : 'Itinerario',
            dataIndex: 'd_itin',
            flex : 1
            }
            ,{
            header   : 'Gruppo documento',
            dataIndex: 'd_gruppo',
            flex : 1
            },
            {
            header   : 'Divisione',
            dataIndex: 'd_div',
            flex : 1
            }

    <?php 
    
    $out_columns = ob_get_contents();
    ob_end_clean();
    
    return $out_columns;
    
}

function get_values_from_row($row){
    
    $ar_values = array( 
        "riga" => trim($row->riga),
        "descrizione" => trim($row->tadesc),
        "note" => trim($row->tades2),
        "itinerario" => trim($row->itinerario),
        "divisione" => trim($row->divisione),
        "gr_doc" => trim($row->gr_doc),
        "tarest" => trim($row->tarest),
    );
 
    return $ar_values;
    
}


function out_component_form($ar_values){
    
    global $main_module;
    
    ob_start();
    
    ?>
     {xtype: 'textfield',
		name: 'riga',
		fieldLabel: 'Riga',
		width : 150,
		maxLength: 3,
		anchor: '-15',	
		value:  <?php echo j($ar_values['riga']); ?>
	  },
	  {xtype: 'textfield',
    	name: 'descrizione',
    	fieldLabel: 'Descrizione',
    	maxLength : 30,
    	anchor: '-15',
    	value:  <?php echo j($ar_values['descrizione']); ?>
    	}, 
    	{name: 'itinerario',
			xtype: 'combo',
			fieldLabel: 'Itinerario',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['itinerario']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            anchor: '-15',	
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'},'sosp', 'color'],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BITI', array('i_sosp' => 'Y')), ''); ?>	
			    ]
			},
			        tpl: [
                	 '<ul class="x-list-plain">',
                        '<tpl for=".">',
                        '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                            '</tpl>',
                            '</ul>'
                		 ],
                           // template for the content inside text field
                        displayTpl: Ext.create('Ext.XTemplate',
                               '<tpl for=".">',
                                    '{text}',
                               '</tpl>'
                                 
                                ),
                                        listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	        },
	        {name: 'gr_doc',
			xtype: 'combo',
			fieldLabel: 'Gr. documento',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['gr_doc']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            anchor: '-15',	
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	        },
	        {name: 'divisione',
			xtype: 'combo',
			fieldLabel: 'Divisione',
			forceSelection: true,								
			displayField: 'text',
			valueField: 'id',
			value:  <?php echo j($ar_values['divisione']); ?>,							
			emptyText: '- seleziona -',
	   	    queryMode: 'local',
            minChars: 1,
            anchor: '-15',	
			store: {
				editable: false,
				autoDestroy: true,
			    fields: [{name:'id'}, {name:'text'}],
			    data: [								    
			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('DDOC'), ''); ?>	
			    ]
			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
			 }
 			}
			
	        },
    
    <?php 
    
    $out_xtype = ob_get_contents();
    ob_end_clean();
    
    return $out_xtype;
    
}

function out_ar_ins($form_values, $tarest){
    
    $ar_ins = array();

    
    $m_table_config = array( 'TAREST' =>
        array(
            'itinerario' 	=> array(
                "start" => 0,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'gr_doc' 	=> array(
                "start" => 33,
                "len"   => 3,
                "riempi_con" => ""
            ),
            'divisione' 	=> array(
                "start" => 36,
                "len"   => 3,
                "riempi_con" => ""
            )
        ));
    
    $tarest = genera_TAREST($m_table_config, $form_values, $tarest);
    $ar_ins['TAREST'] = $tarest;
    $ar_ins['TANR']   = sprintf("%-3s", $form_values->riga);
    $ar_ins['TADESC'] = $form_values->descrizione;
    $ar_ins['TACOR2'] = "*ST";
    
    return $ar_ins;
    
}

function genera_TAREST($m_table_config, $values, $tarest){
    $value_attuale = $tarest;
    if(trim($value_attuale) == "")
        $value_attuale = sprintf("%-248s", "");
        
        //$new_value = "";
        foreach($m_table_config['TAREST'] as $k => $v){
            if(isset($values->$k)){
                $chars = $values->$k;
                if (isset($v['riempi_con']) && strlen($v['riempi_con']) == 1 && strlen(trim($chars)) > 0)
                    $len = "%{$v['riempi_con']}{$v['len']}s";
                    else
                        $len = "%-{$v['len']}s";
                        
                        $chars = substr(sprintf($len, $chars), 0, $v['len']);
                        $new_value = substr_replace($value_attuale, $chars, $v['start'], $v['len']);
                        $value_attuale = $new_value;
                        
            }
            
        }
        
        return $new_value;
}
