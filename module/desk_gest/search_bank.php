<?php

require_once("../../config.inc.php");

$main_module = new DeskGest(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'get_json_data_ABI'){
 $m_params = acs_m_params_json_decode();
 $ar = array();
 $cfg_mod = $main_module->get_cfg_mod();
 
 /* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
 $sql = "SELECT *
 		FROM {$cfg_mod['file_ABI']}
 		WHERE  XSTA = 'A' AND UPPER(
 				REPLACE(REPLACE(XDES, '.', ''), ' ', '')
 			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
		
 			";
 
 $stmt = db2_prepare($conn, $sql);
 echo db2_stmt_errormsg();
 $result = db2_execute($stmt);
 
 $ret = array();
 
 while ($row = db2_fetch_assoc($stmt)) {
 
 	$row['cod'] 	= trim($row['XABI']);
 	$row['descr'] 	= trim($row['XDES']);
 	$row['stato'] 	= trim($row['XSTA']);

 	$ret[] = $row;
 }
 
 echo acs_je($ret);
 exit;
}	





if ($_REQUEST['fn'] == 'get_json_data_CAB'){
	$m_params = acs_m_params_json_decode();
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT *
			FROM {$cfg_mod['file_CAB']}
			WHERE
			( 
			  UPPER(
				REPLACE(REPLACE(XDSCAB, '.', ''), ' ', '')
				)  " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
				OR 
			  UPPER(
				REPLACE(REPLACE(XCITTA, '.', ''), ' ', '')
				)  " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			 )			
 			";
	//filtro su ABI
	if (strlen($m_params->f_abi)){
		$sql .= " AND XABI = " . $m_params->f_abi;
	}

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['cod'] 	= trim($row['XCAB']);
		$row['descr'] 	= acs_u8e(trim($row['XDSCAB']));
		$row['loca'] 	= acs_u8e(trim($row['XCITTA']));
		$row['prov'] 	= acs_u8e(trim($row['XCITTA']));
		$row['abi']		= trim($row['XABI']);
		$row['cab']		= trim($row['XCAB']);
		if (strlen(trim($row['descr'])) == 0)
			$row['descr'] = "(". implode(', ', array(acs_u8e(trim($row['XCITTA'])), trim($row['XPRO']))) . ")";		
		$ret[] = $row;
	}

	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_search'){	
	$m_params = acs_m_params_json_decode();

?>

{
 success:true, items: [

 		{ 
 			xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '15 5 10',
            frame: true,
            title: '',
            
			buttons: [{
	            text: 'Seleziona',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	f_v = form.getValues();
	            	f_v['des_banca'] = form.findField('f_abi').rawValue + ' - ' + form.findField('f_cab').rawValue;;
	            	if(form.isValid()){
	            		loc_win.fireEvent('afterSelected', loc_win, f_v);
	            	}
	           	}
	         }
	        ],
            
            
 		
	        items: [
	            
			  {
	            xtype: 'combo',
				name: 'f_abi',
				fieldLabel: 'Istituto',
				minChars: 1, allowBlank: false,
	            margin: "10 20 10 10",
	            
	            store: {
	            	pageSize: 1000,
	            	
					proxy: {
			            type: 'ajax',
			            
			            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_ABI',
			            		
			            reader: {
			                type: 'json',
			                root: 'root',
			                totalProperty: 'totalCount'
			            }
			        },       
					fields: ['cod', 'descr', 'out_loc', 'TALOCA'],		             	
	            },
	                        
				valueField: 'cod',                        
	            displayField: 'descr',
	            typeAhead: false,
	            hideTrigger: true,
	            anchor: '100%',
	            
		        listeners: {
		            change: function(field,newVal) {	
						if (Ext.isEmpty(newVal)){
							this.up('form').getForm().findField('f_cab').setValue('');
							this.up('form').getForm().findField('f_cab').store.proxy.extraParams = this.up('form').getForm().getValues();					
						}						            
		            },
		            select: function(field,newVal) {
		            		this.up('form').getForm().findField('f_cab').setValue('');	
							this.up('form').getForm().findField('f_cab').store.proxy.extraParams = this.up('form').getForm().getValues();	            	
		            }
		        },            
	
	            listConfig: {
	                loadingText: 'Searching...',
	                emptyText: 'Nessun dato trovato',
	                
	
	                // Custom rendering template for each item
	                getInnerTpl: function() {
	                    return '<div class="search-item">' +
	                        '<h3><span>{descr}</span></h3>' +
	                        '[{cod}]' + 
	                    '</div>';
	                }                
	                
	            },
	            
	            pageSize: 1000
	
	        },
	        
	        
	        {
	            xtype: 'combo',
				name: 'f_cab',
				fieldLabel: 'Sportello',
				minChars: 1, allowBlank: false,
	            margin: "0 20 0 10",				
	            
	            store: {
	            	pageSize: 1000,
	            	
					proxy: {
			            type: 'ajax',
			            actionMethods: {read: 'POST'},
			            
			            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_CAB',
			            extraParams: {
			            	f_ABI: null
			            },
			            doRequest: personalizza_extraParams_to_jsonData,
			            		
			            reader: {
			                type: 'json',
			                root: 'root',
			                totalProperty: 'totalCount'
			            }
			        },       
					fields: ['cod', 'descr', 'out_loc', 'loca', 'prov', 'abi', 'cab'],
	            },
	                        
				valueField: 'cod',                        
	            displayField: 'descr',
	            typeAhead: false,
	            hideTrigger: true,
	            anchor: '100%',
	            
		        listeners: {
		            change: function(field,newVal) {	
						if (Ext.isEmpty(newVal)){
							//this.up('form').findField('f_abi').proxy.extraParam
							//this.up('panel').getStore().load();					
						}						            
		            },
		            select: function(field,newVal) {	
						//this.up('panel').getStore().proxy.extraParams.centro_di_costo = field.getValue();
						//this.up('panel').getStore().load();	            	
		            }
		        },            
	
	            listConfig: {
	                loadingText: 'Searching...',
	                emptyText: 'Nessun centro di costo trovato',
	                
	
	                // Custom rendering template for each item
	                getInnerTpl: function() {
	                    return '<div class="search-item">' +
	                        '<h3><span>{descr}</span></h3>' +
	                        '[{abi}|{cab}] {loca}, {prov}' + 
	                    '</div>';
	                }                
	                
	            },
	            
	            pageSize: 1000
	
	        }

	            
		]
			    		
 	} 
 
 ]
}
<?php exit; } ?>