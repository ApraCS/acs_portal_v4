<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();

require_once("acs_marginalita_include.php");



//recupero nome_ditta
function get_nome_ditta($codice_ditta){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT DITTA FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE CODICE_DITTA=? GROUP BY DITTA";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($codice_ditta));
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		return $r['DITTA'];
	}
	return '';
}





/*
function sql_where_by_request($m_params, $solo_su_mov = 'N'){
	$sql_where = " AND NOME_ANALISI = " . sql_t($m_params->nome_analisi_1);
	if ($solo_su_mov != 'Y')
		$sql_where = " AND NOME_ANALISI_CONFR = " . sql_t($m_params->nome_analisi_2);	
	if (count($m_params->f_ditta) > 0){
		$sql_where .= " AND CODICE_DITTA IN(" . sql_t_IN($m_params->f_ditta) .")";
	}
	return $sql_where;		
}
*/


// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_report'){
	include_once('acs_panel_marginalita_start_report.php');
	exit;
}
if ($_REQUEST['fn'] == 'get_report_elenco'){
	include_once('acs_panel_marginalita_azienda_report_elenco.php');
	exit;
}




// ******************************************************************************************
// RECUPERO DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();	
	
	$sql_where = "";
	$sql_where .= sql_where_by_request(acs_m_params_json_decode());
	$sql_where .= sql_where_by_user();	
	
	//recuper la somma dei record 1001 (a cui fanno riferimento le percentuali)
	$sql = "SELECT SUM(IMPORTO_A) AS TOT_1001 FROM {$cfg_mod_Gest['abi']['file_conti']} D WHERE VOCE_ANALISI = '1001' {$sql_where}";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);	
	$r = db2_fetch_assoc($stmt);
	$tot_1001 = $r['TOT_1001'];
	
	
	//recupero gli importo dai movimenti
	$sql_where = "";
	$sql_params = array();
	$sql_where .= sql_where_by_request($m_params, 'Y', 'Y', 'M.');
	
	$sql_where .= " AND VOCE_ANALISI = " .sql_t($m_params->voce_analisi);
		
	$sql_where .= sql_where_by_user();
	
	list($cod_field, $des_field) = get_field_for_group($m_params->field_group);
	
	$sql_add_select_tot = ',TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05';	
	$sql_mov = "SELECT M.{$cod_field} AS COD, M.{$des_field} AS DESCR
					,TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05, 
					M.VOCE_ANALISI, M.NOME_ANALISI, SUM(M.IMPORTO) AS IMPORTO
				FROM {$cfg_mod_Gest['abi']['file_movimenti']} M
	  			WHERE 1=1 {$sql_where}
				GROUP BY M.{$cod_field}, M.{$des_field}, M.VOCE_ANALISI, M.NOME_ANALISI
					,TOTALE_01, TOTALE_02, TOTALE_03, TOTALE_04, TOTALE_05
				ORDER BY M.{$des_field}
	";
	
	$stmt_mov = db2_prepare($conn, $sql_mov);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_mov, $sql_params);
	$ar = array();
		
	
	while ($r = db2_fetch_assoc($stmt_mov)) {

		$cod_liv1 = trim($r['COD']);		//codice raggruppamento
		
		$liv = $cod_liv1;
		if (!isset($ar_r["{$liv}"])){
			$ar_new = array();
			$ar_new['id'] = $liv;
			$ar_new['liv'] = $liv;
			$ar_new['liv_cod'] = $liv;
			$ar_new['COD'] = $liv;			
			$ar_new['DESCR'] = acs_u8e(trim($r['DESCR']));
			$ar["{$liv}"] = $ar_new;
		}

		if (trim($r['NOME_ANALISI']) == $m_params->open_tab_filters->nome_analisi_1)
			$ar["{$liv}"]['IMPORTO_A'] += $r['IMPORTO'];
		
		if (trim($r['NOME_ANALISI']) == $m_params->open_tab_filters->nome_analisi_2)
			$ar["{$liv}"]['IMPORTO_C'] += $r['IMPORTO'];		
		
	}
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}	
	
	echo acs_je($ret);
	exit;
}






// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
	
	$m_params = acs_m_params_json_decode();
	
?>
{
 success:true, items: [
 

	{
		xtype: 'grid',
		loadMask: true,
		m_filters: {},
		title: <?php echo j(implode("_", array('Margini', $m_params->voce_analisi, get_short_des_for_group($m_params->field_group)))) ?>,
		<?php echo make_tab_closable(); ?>,
	
	
		dockedItems: [],
	
        tbar: new Ext.Toolbar({
            items:['<b>Analisi margine di contribuzione [Periodo: <?php echo $analisi_ar[0]; ?>, periodo di raffronto: <?php echo $analisi_ar[1]; ?>]</b>', '->'
            
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
         ]            
        }),  
		
	
		store: {
			xtype: 'store',
				
			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
				field_group: <?php echo j($m_params->field_group); ?>,
				voce_analisi: <?php echo j($m_params->voce_analisi); ?>,
				open_tab_filters: <?php echo acs_je($m_params->open_tab_filters); ?>,
				m_filters: <?php echo acs_je($m_params->m_filters); ?>
			}
			
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				, doRequest: personalizza_extraParams_to_jsonData						
			
			},
				
			fields: ['COD', 'DESCR', 'IMPORTO_A', 'IMPORTO_B', 'IMPORTO_C', 'IMPORTO_D'],
						
		}, //store

		multiSelect: false,
		columnLines: false, //righe separazione cell in verticale
		enableSort: true,		
		columns: [		
			{header: 'Codice', dataIndex: 'COD', width: 90 /*, locked: true */},
			{header: 'Descrizione', dataIndex: 'DESCR', flex: 1, minWidth: 200 /*, locked: true */},
			{header: '', dataIndex: 'spaces', flex: 1}			

				 , {header: <?php echo acs_je("Periodo {$m_params->open_tab_filters->nome_analisi_1}"); ?>, width: 180,
				    columns: [ 			
					 , {header: 'Importo', dataIndex: 'IMPORTO_A', width: 100, align: 'right', renderer: floatRenderer0, sortable: true}
					 , {header: '%', dataIndex: 'IMPORTO_B', width: 70, align: 'right', renderer: floatRenderer2, sortable: true}				 					 
				  ]
				 }			
				 
				 
				 , {header: <?php echo acs_je("Periodo {$m_params->open_tab_filters->nome_analisi_2}"); ?>, width: 180,
				    columns: [ 			
					 , {header: 'Importo', dataIndex: 'IMPORTO_C', width: 100, align: 'right', renderer: floatRenderer0, sortable: true}
					 , {header: '%', dataIndex: 'IMPORTO_D', width: 70, align: 'right', renderer: floatRenderer2, sortable: true}				 					 
				  ]
				 }
				 
				 
				 , {header: 'Scostamento', width: 180,
				    columns: [ 			
					 , {header: 'Importo', width: 100, align: 'right', sortable: true,
			          	  renderer: function (value, metaData, record, row, col, store, gridView){						
							return floatRenderer0(parseFloat(record.get('IMPORTO_A')) - parseFloat(record.get('IMPORTO_C')));			    
							}					 
					   }
					 , {header: '%', width: 70, align: 'right', sortable: true,
			          	  renderer: function (value, metaData, record, row, col, store, gridView){
			          	    if (parseFloat(record.get('IMPORTO_C')) != 0)						
								return floatRenderer2( (parseFloat(record.get('IMPORTO_A')) - parseFloat(record.get('IMPORTO_C'))) / parseFloat(record.get('IMPORTO_C')) * 100);			    
							}					 					 
					 }				 					 
				  ]
				 }					 				 
					
		],
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					console.log('celldblclick');
					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
				    //in col_name ho in pratico il codice ditta
				    acs_show_panel_std('acs_panel_marginalita_azienda.php?fn=open_tab', Ext.id(), {
				    	codice_ditta: col_name,
				    	nome_analisi_1: iView.store.proxy.extraParams.nome_analisi_1,
				    	nome_analisi_2: iView.store.proxy.extraParams.nome_analisi_2,
				    	m_filters: iView.store.proxy.extraParams.m_filters
				    });

				}
			},
			
 			afterrender: function (comp) {
 			 //comp.normalGrid.addListener('celldblclick', comp.onCellDblClick,this);
 			 //comp.lockedGrid.addListener('celldblclick', comp.onCellDblClick,this); 			 
 			}			         
			
			
		}
		
		
		, viewConfig: {
			forceFit: true,
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }   
		         
		    }												    
		
	
	
		 
	} 
 
 
 
 ]
}
<?php exit; } ?>