<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));

$oggi = oggi_AS_date();

$m_params = acs_m_params_json_decode();

//recupero l'elenco degli ordini interessati
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

foreach ($m_params->list_selected_id as $ar_selected_ord)
	$ar_ord[] = $ar_selected_ord->k_ordine;

//recupere gli ordini
global $conn, $cfg_mod_Spedizioni;
$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} WHERE TDDOCU IN (" . sql_t_IN($ar_ord) . ")";
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);

//dal primo ordine prendo (in base all'operazione) come utente assegnato ref. cliente o ordine
$row = db2_fetch_assoc($stmt);

$sql = "SELECT * FROM {$cfg_mod_Admin['file_tabelle']} TA_ATTAV	WHERE TADT=? AND TA_ATTAV.TATAID = 'ATTAV' AND TAKEY1=? ";
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$rows = db2_execute($stmt, array($id_ditta_default, $m_params->tipo_op));
$r_ATTAV = db2_fetch_assoc($stmt);

switch ($r_ATTAV['TAFG01']){
	case "C": //ref. cliente
		$v_utente_assegnato = trim($row['TDSTOE']);
		break;
	case "E": //ENTRAMBI
		$v_utente_assegnato= trim($row_ord['TDUSIO']);
		break;
	default: //ref. ordine
		$v_utente_assegnato = trim($row_ord['TDUSIO']);
}



//elenco possibili utenti destinatari
$user_to = array();
$users = new Users;
/* TO DO: sto passando BDSOLLE per recuperare gli utenti a cui associare l'ttivita'. Non sarebbe giusto */
$ar_users = $users->find_all_by_ATTAV('BDSOLLE', null, $row); //al momento non gestisco l'area di spedizione

foreach ($ar_users as $ku=>$u)
	$user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");

$ar_users_json = acs_je($user_to);


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'exe_crea_segnalazione_arrivi'
                	}, {
	                	xtype: 'hidden',
	                	name: 'list_selected_id',
	                	value: '<?php echo $list_selected_id_encoded; ?>'
                	}, 
                	
                	<?php if (isset($m_params->entry_prog)){ ?>
                	
                			{
			                	xtype: 'hidden',
			                	name: 'f_entry_prog',
			                	value: <?php echo $m_params->entry_prog; ?>
		                	}, {
			                	xtype: 'hidden',
			                	name: 'f_entry_prog_causale',
			                	value: <?php echo j($m_params->entry_prog_causale); ?>
		                	}, {
								name: 'f_entry_prog_note',
								xtype: 'hidden',
							    value: <?php echo j($m_params->entry_prog_note); ?>							
							},			              

					
					<?php } ?>                	
                	
                	{
						name: 'f_causale',
						xtype: 'combo',
						fieldLabel: 'Causale',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: false,							
					    value: <?php echo j($m_params->tipo_op) ?>,							
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php
							     // echo acs_ar_to_select_json($s->find_TA_std('ATTAV', N, 'Y'), "");
							     echo acs_ar_to_select_json($s->find_TA_std('ATTAV', null, 'Y', 'N', null, null, $m_params->blocco_op), "");
							       ?>	
							    ] 
						}						 
					}, {
						name: 'f_note',
						xtype: 'textfield',
						fieldLabel: 'Note',
					    maxLength: 100							
					}, {
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente assegnato',
			            queryMode: 'local',
			            selectOnTab: false,
			            name: 'f_utente_assegnato',
			            allowBlank: false,
						forceSelection: true,			            
			            value: '<?php echo $v_utente_assegnato; ?>'
			        }, {
						xtype: 'checkboxgroup',
						fieldLabel: 'Notifica assegnazione',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_notifica_assegnazione' 
                          , boxLabel: 'Si'
                          , inputValue: 'Y'
                        }]							
					}, {
						xtype: 'checkboxgroup',
						fieldLabel: 'Notifica rilascio',
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_notifica_rilascio' 
                          , boxLabel: 'Si'
                          , inputValue: 'Y'
                        }]							
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Attiva dal'
					   , name: 'f_attiva_dal'
					   , format: 'd/m/Y'
					   , value: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'
					   , minValue: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   , width: 240, anchor: 'none'
					}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Scadenza'
					   , name: 'f_scadenza'
					   , format: 'd/m/Y'
					   , minValue: '<?php echo print_date($oggi, "%d/%m/%Y"); ?>'							   
					   , submitFormat: 'Ymd'
					   , allowBlank: true
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   , width: 240, anchor: 'none'
					}, {
			            xtype: 'textareafield',
			            grow: true,
			            name: 'f_memo',
			            fieldLabel: 'Memo',
			            anchor: '100%',
			            height: 200
			            
			        }
				],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-24', scale: 'medium',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	            	
			                form.submit({
				                            //waitMsg:'Loading...',
				                            success: function(form,action) {
												loc_win.fireEvent('afterInsertRecord', loc_win);				                                 	
				                            },
				                            failure: function(form,action){
				                                Ext.MessageBox.alert('Erro');
				                            }
				                        });
				            
				    }            	                	                
	            }
	        }],             
				
        }
]}