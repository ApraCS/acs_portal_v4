<?php 
	require_once("../../config.inc.php");	
	$_module_descr = "Desktop Amministrazione";
			
	$main_module = new DeskGest();
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}
	
	
	$ar_email_json = acs_je($ar_email_to);	
		
?>



<html>
<head>
<title>ACS Portal_Gest</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />

<!-- <link rel="stylesheet" type="text/css" href="resources/css/calendar.css" /> -->
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/extensible-all.css" />
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/calendar.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">
 /* REGOLE RIORDINO */
 .x-grid-cell.lu{background-color: #dadada;} 
 .x-grid-cell.ma{background-color: #e5e5e5;} 
 .x-grid-cell.me{background-color: #eeeeee;} 
 .x-grid-cell.gi{background-color: #FFFFFF;} 
 .x-grid-cell.ve{background-color: #F4EDAF;} 
 .x-grid-cell.sa{background-color: #FFFFFF;}
 
 .x-grid-cell.attivi{background-color: #A0DB8E;}
 .x-grid-cell.elaborazione-old-1{background-color: #e5e5e5;} 
 .x-grid-cell.elaborazione-old-2{background-color: #F9BFC1;} 

.x-panel td.x-grid-cell.festivo{opacity: 0.6;}

.x-panel td.x-grid-cell.con_carico_1{background-color: #909090; color: white; font-weight: bold;} /* SOLO ALCUNI HANNO IL CARICO */
.x-panel td.x-grid-cell.con_carico_2{background-color: #3c3c3c; color: white; font-weight: bold;}  /* HANNO TUTTI IL CARICO */
.x-panel td.x-grid-cell.con_carico_3xxx{background-color: #F9827F; font-weight: bold;}  /* NON CARICO. CON DATE CONF. */
.x-panel td.x-grid-cell.con_carico_4xxx{background-color: #F9BFC1; font-weight: bold;}  /* NON CARICO. CON DATE TASS. */
.x-panel td.x-grid-cell.con_carico_5xxx{background-color: #CEEA82; font-weight: bold;}  /* ALCUNI EVASI */
.x-panel td.x-grid-cell.con_carico_6{background-color: #8CD600; font-weight: bold;}  /* TUTTI EVASI */


.x-panel .x-column-header-text{font-weight: bold;}


tr.liv_totale td.x-grid-cell{background-color: #F4EDAF; border: 1px solid white;}
tr.liv_totale td.x-grid-cell.festivo{opacity: 0.6;}
tr.liv_0 td.x-grid-cell{border: 1px solid white; font-weight: bold;}
tr.liv_1 td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: bold;}
tr.liv_1_no_b td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: normal;}
tr.liv_1.no_carico td.x-grid-cell{background-color: #C4D8E2}

.x-panel.elenco_ordini tr.liv_1 td.x-grid-cell{font-weight: normal;}
.x-panel.elenco_ordini tr.liv_2 td.x-grid-cell{font-weight: bold;}

tr.liv_totale.dett_selezionato td.x-grid-cell{background-color: #F4ED7C; font-weight: bold;}

/*TIPO ORDINE (raggr)*/
.x-grid-cell.tipoOrd.O {background-color: #93C6E0;} /* mostre */
.x-grid-cell.tipoOrd.S {background-color: #D3BFB7;} /* materiale promozionale */
.x-grid-cell.tipoOrd.R {background-color: #E2D67C;} /* composizioni promozionali */
.x-grid-cell.tipoOrd.L {background-color: white;}   /* assistenze */


/*RIGHE ORDINE*/
.x-grid-row.rigaRevocata .x-grid-cell{background-color: #F9BFC1}
.x-grid-row.rigaChiusa .x-grid-cell{background-color: #CEEA82}
.x-grid-row.rigaParziale .x-grid-cell{background-color: #F4EDAF}
.x-grid-row.rigaNonSelezionata .x-grid-cell{background-color: #cccccc}


/*TIPO PRIORITA*/
 .x-grid-cell.tpSfondoRosa{background-color: #F9BFC1;}
 .x-grid-cell.tpSfondoGrigio{background-color: #cecece;}
 .x-grid-cell.tpSfondoCelesteEl{background-color: #99D6DD;}

 

/*ICONE*/
.iconSpedizione {background-image: url(<?php echo img_path("icone/16x16/spedizione.png") ?>) !important;}
.iconConf {background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/puntina_rossa.png") ?>) !important;
}
 
/* celle classe ABC */
.x-panel.x-table-index .x-panel-body {
		text-align: center;
    	background-color: #D3D3D3;
		font-size: 18;
		/*font-weight: bold;*/
}
 .x-panel.x-table-index.leg .x-panel-body {		
		font-size: 14;
}
.x-panel.x-table-index .x-panel-body span {font-size: 10px;}
 
/* bordi laterali tabella non visualizzabili */
.table-border {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	text-align: right;
}
.table-border-final {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	border-right: 1px solid #99BBE8;
	text-align: right;
}

a.view_dett_class_gc{text-decoration: none;}
 

.x-toolbar .strong{font-weight: bold;}

.x-grid-cell-inner p.sottoriga-liv_2{text-indent: 70px}



/* ANALISI BILANCIO */
.x-grid-row.S1 .x-grid-cell{background-color: #99CCCC; padding-top: 3px; padding-bottom: 3px; }
.x-grid-row.S2 .x-grid-cell{background-color: #99B3CC; font-weight: bold; padding-top: 4px; padding-bottom: 4px;}
.x-grid-row.S2_prog .x-grid-cell{background-color: #A9C3DC; font-weight: normal; font-size: 12px; padding-top: 2px; padding-bottom: 2px;}
.x-grid-row.dett .x-grid-cell{background-color: #FFFFE0; font-weight: normal;}
.x-grid-row .x-grid-cell.tot{font-weight: bold;}
/* cal: dettaglio per mese/giorno */
.x-grid-row.cal .x-grid-cell{padding-top: 1px; padding-bottom: 0px; border: 1px solid #f9f9f9;}
.x-grid-row.cal .x-grid-cell.day{background-color: #84E2A8} /* giorno calendario */
.x-grid-row.cal .x-grid-cell.day.alt{background-color: #B5E8BF} /* alterno colori colonne mese */
.x-grid-row.cal .x-grid-cell.day.gw6{background-color: #F9DDD6} /* SABATO */
.x-grid-row.cal .x-grid-cell.day.gw0{background-color: #F9BA82} /* DOMENICA */
.x-grid-row.cal .x-grid-cell.tot{background-color: #D8DDCE} /* TOTALI */

/* FLUSSI DI CASSA  ----------------------------------------- */
.x-panel.flussi_cassa_tree_ .x-grid-table{border-collapse: collapse;}
.x-panel.flussi_cassa_tree_ .x-grid-row.S1 .x-grid-cell{border: 1px solid gray; font-weight: bold;}
.x-panel.flussi_cassa_tree_ .x-grid-row.dett .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_ .x-grid-row.S2 .x-grid-cell{border: 1px solid gray; font-weight: bold;}
.x-panel.flussi_cassa_tree_ .x-grid-row.S2_prog .x-grid-cell{border: 1px solid gray; font-weight: normal;}

.x-panel.flussi_cassa_tree_ .x-grid-row.S1 .x-grid-cell.scaduto{background-color: #F99B0C;} /* colonna scaduto: arancione SI */
.x-panel.flussi_cassa_tree_ .x-grid-row.dett .x-grid-cell.scaduto{background-color: #F2CE68;} /* colonna scaduto: arancione dett */

.x-panel.flussi_cassa_tree_ .x-grid-row.S1 .x-grid-cell.oltre{background-color: #E8DD21;} /* colonna oltre: giallo SI */
.x-panel.flussi_cassa_tree_ .x-grid-row.dett .x-grid-cell.oltre{background-color: #F4EDAF;} /* colonna oltre: giallo dett */

/* tree scaduto */
.x-panel.flussi_cassa_tree_scaduto .x-grid-table{border-collapse: collapse;}
.x-panel.flussi_cassa_tree_scaduto .x-grid-row.S1 .x-grid-cell{border: 1px solid gray; font-weight: bold; background-color: #F99B0C;}
.x-panel.flussi_cassa_tree_scaduto .x-grid-row.dett .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_scaduto .x-grid-row.S2 .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_scaduto .x-grid-row.S2_prog .x-grid-cell{border: 1px solid gray;}

/* tree oltre */
.x-panel.flussi_cassa_tree_oltre .x-grid-table{border-collapse: collapse;}
.x-panel.flussi_cassa_tree_oltre .x-grid-row.S1 .x-grid-cell{border: 1px solid gray; font-weight: bold; background-color: #E8DD21;}
.x-panel.flussi_cassa_tree_oltre .x-grid-row.dett .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_oltre .x-grid-row.S2 .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_oltre .x-grid-row.S2_prog .x-grid-cell{border: 1px solid gray;}


/* PROVVIGIONI */
.x-grid-row.liv_0 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_totale .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_1 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_2 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_3 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_4 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_0_2 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */

.x-grid-row.liv_0 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_totale .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_1 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_2 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_3 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_4 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_0_2 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C;} /* arancio */

.x-grid-row.liv_1 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_2 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_3 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_4 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */


.x-grid-row.liv_totale.MATURATO.dett_selezionato .x-grid-cell{background-color: #FFB777; border: 1px solid white;}
.x-grid-row.liv_totale.MATURATO .x-grid-cell{background-color: #FFD69B; border: 1px solid white;}

.x-grid-row.liv_totale.LIQUIDATO.dett_selezionato .x-grid-cell{background-color: #15b25e; border: 1px solid white;}
.x-grid-row.liv_totale.LIQUIDATO .x-grid-cell{background-color: #69935e; border: 1px solid white;}

</style>



<script type="text/javascript" src="../../../extjs/ext-all.js"></script>

<script type="text/javascript" src="../../js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
<script type="text/javascript" src="../../js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>


<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>

<?php if (!isset($disabilita_gmap) || $disabilita_gmap != 'Y'){ ?>
	<script type="text/javascript" src="<?php echo site_protocol();?>maps.google.com/maps/api/js?sensor=false"></script>	
	<script src=<?php echo acs_url("js/gmaps.js") ?>></script>
<?php } ?>


<script src=<?php echo acs_url("js/acs_js.js?d=20191126") ?>></script>



<script type="text/javascript">

	function gmapPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=400, height=400, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}


	function allegatiPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=600, height=600, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}	


	var refresh_stato_aggiornamento = {
        run: doRefreshStatoAggiornamento,
        interval: 30 * 1000 //millisecondi
      }

	function doRefreshStatoAggiornamento() {
        Ext.Ajax.request({
            url : 'get_stato_aggiornamento.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btStatoAggiornamento = document.getElementById('bt-stato-aggiornamento-esito');
                btStatoAggiornamento.innerHTML = jsonData.html;
            },
            failure: function ( result, request) {
                console.log('failed');
            }
        });
	}


	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
            //'Ext.calendar': 'src',
	        "Extensible": "../../js/extensible-1.5.2/src", 
			"Extensible.example": "../../js/extensible-1.5.2/examples"	                   
        }	    
	});Ext.Loader.setPath('Ext.ux', '../../ux');


    Ext.require(['*'
                 , 'Ext.ux.grid.FiltersFeature'                 
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
                 , 'Extensible.calendar.data.MemoryEventStore'
                 , 'Extensible.calendar.CalendarPanel'  
                 , 'Extensible.example.calendar.data.Events'
				 , 'Extensible.calendar.data.CalendarMappings'
    			 , 'Extensible.calendar.data.EventMappings'
    			 , 'Ext.ux.grid.Printer'
      			 , 'Ext.ux.RowExpander' 
                 ]);

//    ,
//    'Ext.calendar.data.MemoryCalendarStore',
//    'Ext.calendar.data.MemoryEventStore',
//    'Ext.calendar.data.Events',
//    'Ext.calendar.data.Calendars'	  




    

    Ext.define('ModelOrdini', {
        extend: 'Ext.data.Model',
        fields: [
        			{name: 'task'}, {name: 'n_carico'}, {name: 'k_carico'}, {name: 'sped_id'}, {name: 'cod_iti'},
        			{name: 'k_ordine'}, {name: 'prog'}, {name: 'n_O'}, {name: 'n_M'}, {name: 'n_P'}, {name: 'n_CAV'},
        			{name: 'liv'},
					{name: 'cliente'}, {name: 'k_cli_des'}, {name: 'gmap_ind'}, {name: 'scarico_intermedio'},
					{name: 'nr'},
					{name: 'volume'},{name: 'pallet'},{name: 'peso'},
					{name: 'volume_disp'},{name: 'pallet_disp'},{name: 'peso_disp'},
					{name: 'colli'},{name: 'colli_disp'},{name: 'colli_sped'},{name: 'data_disp'}, {name: 'data_cons_cli'}, {name: 'data_conf_ord'}, {name: 'fl_data_disp'},
					{name: 'riferimento'},
					{name: 'tipo'},{name: 'raggr'},
					{name: 'cons_rich'},					
					{name: 'priorita'},{name: 'tp_pri'},
					{name: 'seq_carico'},					
					{name: 'stato'},
					{name: 'cons_conf'},{name: 'cons_prog'},{name: 'ind_modif'},
					{name: 'data_reg'},{name: 'data_scadenza'},					
					{name: 'importo'}, {name: 'referenze'}, {name: 'referenze_cons'},
					{name: 'fl_evaso'}, {name: 'fl_ref'}, {name: 'fl_bloc'}, {name: 'fl_art_manc'}, {name: 'fl_da_prog'}, {name: 'art_da_prog'}, {name: 'fl_new'}
        ]
    });


    Ext.define('RowCalendar', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task',     type: 'string'},
            {name: 'liv',      type: 'string'},            
            {name: 'flag1',    type: 'string'},            
            {name: 'user',     type: 'string'},
            {name: 'duration', type: 'string'},
            {name: 'done',     type: 'boolean'},
            {name: 'da_data',  type: 'float'}, {name: 'itin'}, {name: 'sped_id'}, {name: 'k_cliente'},            
            {name: 'd_1'}, {name: 'd_1_f'}, {name: 'd_1_t'}, {name: 'd_1_d'},
            {name: 'd_2'}, {name: 'd_2_f'}, {name: 'd_2_t'}, {name: 'd_2_d'},
            {name: 'd_3'}, {name: 'd_3_f'}, {name: 'd_3_t'}, {name: 'd_3_d'},
            {name: 'd_4'}, {name: 'd_4_f'}, {name: 'd_4_t'}, {name: 'd_4_d'},
            {name: 'd_5'}, {name: 'd_5_f'}, {name: 'd_5_t'}, {name: 'd_5_d'},
            {name: 'd_6'}, {name: 'd_6_f'}, {name: 'd_6_t'}, {name: 'd_6_d'},
            {name: 'd_7'}, {name: 'd_7_f'}, {name: 'd_7_t'}, {name: 'd_7_d'},
            {name: 'd_8'}, {name: 'd_8_f'}, {name: 'd_8_t'}, {name: 'd_8_d'},
            {name: 'd_9'}, {name: 'd_9_f'}, {name: 'd_9_t'}, {name: 'd_9_d'},
            {name: 'd_10'}, {name: 'd_10_f'}, {name: 'd_10_t'}, {name: 'd_10_d'},
            {name: 'd_11'}, {name: 'd_11_f'}, {name: 'd_11_t'}, {name: 'd_11_d'},
            {name: 'd_12'}, {name: 'd_12_f'}, {name: 'd_12_t'}, {name: 'd_12_d'},
            {name: 'd_13'}, {name: 'd_13_f'}, {name: 'd_13_t'}, {name: 'd_13_d'},
            {name: 'd_14'}, {name: 'd_14_f'}, {name: 'd_14_t'}, {name: 'd_14_d'},
			{name: 'fl_bloc'}, {name: 'fl_ord_bloc'}, {name: 'fl_art_mancanti'}, {name: 'fl_da_prog'}, {name: 'ha_contratti'}, {name: 'ha_proposte_MTO'}                                   
        ]
    });    

    //Model per albero itinerari
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task', type: 'string'}, {name: 'user', type: 'string'}, 'liv', 'sped_id', 'k_cliente', 'data'
        ]
    });    

    
	//TODO: da spostare in PLAN
	function getCellClass (val, meta, rec, rowIndex, colIndex, store) {
		idx = colIndex - 6; //colonna5 corrisponde a d_1
		 
		if (rec.get('d_' + idx + '_f') == 'F' || rec.get('d_' + idx + '_f') == 'S' || rec.get('d_' + idx + '_f') == 'D'){
			meta.tdCls += ' festivo';
		}
		if (rec.get('d_' + idx + '_t') == '1'){
			meta.tdCls += ' con_carico_1';
		}		
		if (rec.get('d_' + idx + '_t') == '2'){
			meta.tdCls += ' con_carico_2';
		}
		if (rec.get('d_' + idx + '_t') == '3'){
			meta.tdCls += ' con_carico_3';
		}		
		if (rec.get('d_' + idx + '_t') == '4'){
			meta.tdCls += ' con_carico_4';
		}		
		if (rec.get('d_' + idx + '_t') == '5'){
			meta.tdCls += ' con_carico_5';
		}
		if (rec.get('d_' + idx + '_t') == '6'){
			meta.tdCls += ' con_carico_6';
		}				
		
		if ((rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3') && rec.get('d_' + idx).length > 0 && rec.get('d_' + idx + '_d').length > 0)		
			return val += "<span class=c_disp>/" + rec.get('d_' + idx + '_d') + "</span>";
		else
			return val;	
	}

    

    

    //PLAN ARRIVI
    function show_el_arrivi(){
    	mp = Ext.getCmp('m-panel');
    	arrivi = Ext.getCmp('panel-arrivi');

    	if (arrivi){
        	arrivi.store.reload();
        	arrivi.show();		    	
    	} else {

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_arrivi_json_plan.php',
    		        method     : 'GET',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            mp.add(jsonData.items);
    		            mp.doLayout();
    		            mp.show();
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });

    	}
    }



    //PANEL fo_presca
    function show_fo_presca(){
       	   acs_show_win_std('Prenotazione scarichi', 'acs_panel_fo_presca.php?fn=get_open_form', null, 530, 420);	
    }
    
    
    function show_win_bl_cliente(cliente, desc){	
    	// create and show window
    	print_w = new Ext.Window({
    			  width: 700
    			, height: 400
    			, plain: false
    			, title: 'Blocco note cliente: ' + desc
    			, layout: 'fit'
    			, border: true
    			, closable: true
    			, maximizable: false										
    		});	
    	print_w.show();

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_anag_cli_note.php?fn=open_bl&cliente=' + cliente+ '&desc=' +desc,
    		        method     : 'POST',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            print_w.add(jsonData.items);
    		            print_w.doLayout();				            
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });		
    } 


    function show_win_righe_ord(ord, k_ord){
 	   acs_show_win_std('Righe ordine', <?php echo acs_url('module/desk_acq/acs_get_order_rows.php') ?>, {k_ord: k_ord, dtep: ord.get('cons_prog')}, 1150, 480);        
    }



    function show_win_crt_promemoria(){

    var f = Ext.create('Ext.form.Panel', {
           frame:true,
           title: '',
           bodyStyle:'padding:5px 5px 0',
           width: 600,
           url: 'acs_op_exe.php',
           fieldDefaults: {
               labelAlign: 'top',
               msgTarget: 'side'
           },

           items: [{
           	xtype: 'hidden',
           	name: 'fn',
           	value: 'crt_promemoria'
           	}, {
                   xtype: 'combo',
                   anchor: '100%',                
                   store: Ext.create('Ext.data.ArrayStore', {
                       fields: [ 'email', 'descr' ],
                       data: <?php echo $ar_email_json ?>
                   }),
                   displayField: 'descr',
                   valueField: 'email',
                   value: '<?php global $auth; echo $auth->get_email(); ?>',
                   fieldLabel: 'A',
                   queryMode: 'local',
                   selectOnTab: false,
                   allowBlank: false,
                   name: 'to'
               },
               {
               xtype: 'container',
               anchor: '100%',
               layout:'column',
               items:[{
                   xtype: 'container',
                   columnWidth:.5,
                   layout: 'anchor',
                   items: [{
                       xtype:'datefield'
   					   , startDay: 1 //lun.
   					   , fieldLabel: 'Data'
   					   , name: 'f_data'
   					   , format: 'd/m/Y'
   					   , submitFormat: 'Ymd'
   					   , allowBlank: false
                          , anchor:'96%'
                          , minValue: new Date()
                   }]
               },{
                   xtype: 'container',
                   columnWidth:.5,
                   layout: 'anchor',
                   items: [{
                       xtype:'timefield'
   					   , fieldLabel: 'Ora'
   					   , name: 'f_ora'
   					   , format: 'H:i'						   
   					   , submitFormat: 'Hi'
   					   , allowBlank: false
                       , anchor:'96%'
                   }]
               }]
           }, {
               xtype: 'textfield',
               name: 'f_oggetto',
               fieldLabel: 'Oggetto',
               anchor: '96%',
   		    allowBlank: false
           }
           , {
               //xtype: 'htmleditor',
               xtype: 'textareafield',
               name: 'f_text',
               fieldLabel: 'Testo del messaggio',
               height: 200,
               anchor: '96%',
   		    allowBlank: false            
           }],

           buttons: [{
               text: 'Conferma',
   	            handler: function() {
   	            	var form = this.up('form').getForm();
   	            	var m_win = this.up('window');	            	
   	                form.submit(
   								{
   		                            //waitMsg:'Loading...',
   		                            success: function(form,action) {
   										m_win.close();		                            	
   		                            },
   		                            failure: function(form,action){
   		                            }
   		                        }	                	
   	                );
   		            //this.up('window').close();            	                	                
   	             }            
           		},{
               text: 'Annulla'
           }]
       });


   		// create and show window
   		var win = new Ext.Window({
   		  width: 800
   		, height: 380
   		, minWidth: 300
   		, minHeight: 300
   		, plain: true
   		, title: 'Registra promemoria via email con data/ora invio'
   		, iconCls: 'icon-clock-16'			
   		, layout: 'fit'
   		, border: true
   		, closable: true
   		, items: f
   		});
   		win.show();  

   }
    


    
    


    Ext.onReady(function() {

        Ext.QuickTips.init();






    Extensible.calendar.data.EventMappings = {
        // These are the same fields as defined in the standard EventRecord object but the
        // names and mappings have all been customized. Note that the name of each field
        // definition object (e.g., 'EventId') should NOT be changed for the default fields
        // as it is the key used to access the field data programmatically.
        EventId:     {name: 'EventId', mapping:'id', type:'int'}, // int by default
        CalendarId:  {name: 'CalendarId', mapping: 'cid', type: 'string'}, // int by default
        Title:       {name: 'Title', mapping: 'title'},
        StartDate:   {name: 'StartDate', mapping: 'start', type: 'date', dateFormat: 'c'},
        EndDate:     {name: 'EndDate', mapping: 'end', type: 'date', dateFormat: 'c'},
        RRule:       {name: 'RecurRule', mapping: 'rrule'},
        Location:    {name: 'Location', mapping: 'loc'},
        Notes:       {name: 'Notes', mapping: 'notes'},
        Url:         {name: 'Url', mapping: 'url'},
        IsAllDay:    {name: 'IsAllDay', mapping: 'ad', type: 'boolean'},
        Reminder:    {name: 'Reminder', mapping: 'rem'},        


        //Acs
        Risorsa: 	  {name:'Risorsa', mapping: 'risorsa', type: 'string'},
        Stabilimento: {name:'Stabilimento', mapping: 'stabilimento', type: 'string'},
        Trasp:		  {name:'Trasp', mapping: 'trasp', type: 'string'},
        Porta:    	  {name:'Porta', mapping: 'porta'},        
        
        
        // We can also add some new fields that do not exist in the standard EventRecord:
        CreatedBy:   {name: 'CreatedBy', mapping: 'created_by'},
        IsPrivate:   {name: 'Private', mapping:'private', type:'boolean'}
        
    };
    // Don't forget to reconfigure!
    Extensible.calendar.data.EventModel.reconfigure();






 /* PER DRAG & DROP **********/
    /*
     * Currently the calendar drop zone expects certain drag data to be present, so if dragging
     * from the grid, default the data as required for it to work
     */
    var nodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag1';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
        }
    };
    
    /*
     * Need to hook into the drop to provide a custom mapping between the existing grid
     * record being dropped and the new calendar record being added
     */
    var nodeDropInterceptor = function(n, dd, e, data){
        if(n && data){
            if(typeof(data.type) === 'undefined'){
                var rec = Ext.create('Extensible.calendar.data.EventModel'),
                    M = Extensible.calendar.data.EventMappings;
                
                // These are the fields passed from the grid's record
                //var gridData = data.selections[0].data;
                var gridData = data.records[0].data
                rec.data[M.Title.name] = 'salvataggio...';
                rec.data[M.CalendarId.name] = gridData.cid;
                
                // You need to provide whatever default date range logic might apply here:
                rec.data[M.StartDate.name] = n.date;
                rec.data[M.EndDate.name] = Extensible.Date.add(n.date, { hours: 1 });;
                rec.data[M.IsAllDay.name] = false;

                rec.data[M.CreatedBy.name] = gridData.TDNBOC;  //lo appoggio qui altrimenti non mi esegui il create               
                rec.data[M.Stabilimento.name] = gridData.TDSTAB;
                rec.data[M.Porta.name] = this.view.store.proxy.extraParams.f_porta;
                
                // save the evrnt and clean up the view
                ///////this.view.onEventDrop(rec, n.date);
                this.view.store.add(rec.data);                                                
                this.onCalendarDragComplete();
				this.view.fireEvent('eventadd');
                return false; //per non eseguire onDrop Normale
            }
        }
    };
    
    /*
     * Day/Week view require special logic when dragging over to create a 
     * drag shim sized to the event being dragged
     */
    var dayViewNodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag2';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
    
            var dayCol = e.getTarget('.ext-cal-day-col', 5, true);
            if (dayCol) {
                var box = {
                    height: Extensible.calendar.view.Day.prototype.hourHeight,
                    width: dayCol.getWidth(),
                    y: n.timeBox.y,
                    x: n.el.getLeft()
                }
                this.shim(n.date, box);
            }
        }
    };
    
    // Apply the interceptor functions to each class:
    var dropZoneProto = Extensible.calendar.dd.DropZone.prototype,
        dayDropZoneProto = Extensible.calendar.dd.DayDropZone.prototype 
    
    dropZoneProto.onNodeOver = Ext.Function.createInterceptor(dropZoneProto.onNodeOver, nodeOverInterceptor);
    dropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dropZoneProto.onNodeDrop, nodeDropInterceptor);
    
    dayDropZoneProto.onNodeOver = Ext.Function.createInterceptor(dayDropZoneProto.onNodeOver, dayViewNodeOverInterceptor);
    dayDropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dayDropZoneProto.onNodeDrop, nodeDropInterceptor);



    
    
    /*
     * This is a simple override required for dropping from outside the calendar. By default it
     * assumes that any drag originated within itelf, so a drag would be a move of an existing event.
     * This is no longer the case and it must check to see what the record state is.
     */
    Extensible.calendar.view.AbstractCalendar.override({
        onEventDrop : function(rec, dt){
            if (rec.phantom) {
                this.onEventAdd(null, rec);
            }
            else {
                this.moveEvent(rec, dt);
            }
        }
    });    	


	//disabilito il menu sugli eventi
	Extensible.calendar.menu.Event.override({
		showForEvent: function(rec, el, xy){
				return false;
		}
	});
    

	//formato data negli header
	Extensible.calendar.template.BoxLayout.prototype.singleDayDateFormat = 'd / m';	

    
    //PER RIMUOVERE L'ORARIO NEGLI EVENTI
    Extensible.calendar.view.DayBody.override({
    getTemplateEventData : function(evt){
        var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            colorCls = 'x-cal-default',
            title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ',
            recurring = evt[M.RRule.name] != '';
        
        this.getTemplateEventBox(evt);
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, 
                    evt[M.CalendarId.name]);
                
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '') + (Ext.isIE || Ext.isOpera ? '-x' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
        data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        data.Title =  (!title || title.length == 0 ? this.defaultEventTitleText : title);
        return Ext.applyIf(data, evt);
    }    
    });
    
  






    //COMBO: possibilita di rimuovere valore selezionato (con forceSelection lasciava sempre l'ultimo)
    Ext.form.field.ComboBox.override({
     beforeBlur: function(){
         var value = this.getRawValue();
         if(value == ''){
             this.lastSelection = [];
         }
         this.doQueryTask.cancel();
         this.assertValue();
     }
    });
    




            

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            // create instance immediately
            Ext.create('Ext.Component', {
                region: 'north',
                id: 'page-header',
                height: 110, // give north and south regions a height
                contentEl: 'header'
            }),

            
            Ext.create('Ext.tab.Panel', {
            	id: 'm-panel',
        		cls: 'supply_desk_main_panel',            	            	
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 1,     // first tab initially active
                items: [
			                                   
                             					                                                
                ]
            })]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------
      	
      	
        //--------------------------------------------------------------------------------        

        // ------------------- GESTIONE TABELLE ------------------------             
        if (Ext.get("bt-toolbar") != null){
         	Ext.QuickTips.register({
    			target: "bt-toolbar",
   			title: 'Setup',
   			text: 'Manutenzione voci/parametri gestionali di base'
    		});
                
           Ext.get("bt-toolbar").on('click', function(){
        	   acs_show_win_std('Gestione tabelle', 'acs_form_json_toolbar.php', {}, 1160, 300, {}, '');	
           });
        }



         if (Ext.get("add-promemoria") != null){
        	Ext.QuickTips.register({
    			target: "add-promemoria",
    			title: 'Memo',
    			text: 'Attiva promemoria via email'
    		});
            
	        Ext.get("add-promemoria").on('click', function(){
	        	show_win_crt_promemoria();
	        });
        }            
      	
      	
    	//Analisi di bilancio
        if (Ext.get("bt-abi") != null){
        	Ext.QuickTips.register({
    			target: "bt-abi",
    			title: 'Riclassificazione',
    			text: 'Analisi riclassificazione dati di bilancio'
    		});
                    
	        Ext.get("bt-abi").on('click', function(){
	        	acs_show_win_std('Analisi riclassificazione dati di bilancio', 'acs_panel_abi_start.php?fn=open_scelta_ceal', {}, 600, 400, {}, 'icon-bilancio2-16');	        	
	        });
        }            
    	

    	//Analisi marginalita
        if (Ext.get("bt-marginalita") != null){
        	Ext.QuickTips.register({
    			target: "bt-marginalita",
    			title: 'Margini',
    			text: 'Analisi margine di contribuzione'
    		});
                    
	        Ext.get("bt-marginalita").on('click', function(){
	        	acs_show_win_std('Analisi marginalit&agrave;', 'acs_panel_marginalita_start.php?fn=open_scelta_ceal', {}, 600, 400, {}, 'icon-margini-16');	        	
	        });
        }            
        
    	
    	//Flusso di cassa
        if (Ext.get("bt-flusso_cassa") != null){
        	Ext.QuickTips.register({
    			target: "bt-flusso_cassa",
    			title: 'Flussi di cassa',
    			text: 'Proiezione finanziaria flussi attivi/passivi'
    		});
                    
	        Ext.get("bt-flusso_cassa").on('click', function(){
	        	acs_show_win_std('Analisi flussi di cassa', 'acs_panel_flussi_cassa.php?fn=open_parameters', {}, 600, 400, {}, 'icon-chart_bar-16');	        	
	        });
        }            
        

    	//Provvigioni
        if (Ext.get("bt-provvigioni") != null){
        	Ext.QuickTips.register({
    			target: "bt-provvigioni",
    			title: 'Provvigioni',
    			text: 'Controllo provvigioni accreditate/maturate/liquidate'
    		});
                    
	        Ext.get("bt-provvigioni").on('click', function(){
	        	acs_show_win_std('Analisi provvigioni', 'acs_panel_provvigioni.php?fn=open_parameters', {}, 800, 450, {}, 'icon-briefcase-16');	        	
	        });
        }            

    	
    	//Bollette doganali
        if (Ext.get("bt-bollette_doganali") != null){
        	Ext.QuickTips.register({
    			target: "bt-bollette_doganali",
    			title: 'Bolle doganali',
    			text: 'Registrazione/abbinamento bolle doganali'
    		});
                    
	        Ext.get("bt-bollette_doganali").on('click', function(){
	        	acs_show_win_std('Bolle doganali', 'acs_panel_bollette_doganali.php?fn=open_parameters', {}, 600, 400, {}, 'icon-globe-16');	        	
	        });
        }            
    	


    	//Inserimento anagrafiche gestionali
        if (Ext.get("bt-ins_anagrafiche") != null){
        	Ext.QuickTips.register({
    			target: "bt-ins_anagrafiche",
    			title: 'Inserimento anagrafiche',
    			text: 'Inserimento anagrafiche clienti/fornitori'
    		});
                    
	        Ext.get("bt-ins_anagrafiche").on('click', function(){
	        	acs_show_win_std('Parametri selezione anagrafiche clienti', 'acs_panel_ins_new_anag_form.php?fn=open_form_filtri', 
	    	        	null, 950, 500, null, 'iconClienti');
					        		          		
	        });	        	        	
        }            



    	//Inserimento anagrafiche gestionali
        if (Ext.get("bt-modulo_anag_cli") != null){
        	Ext.QuickTips.register({
    			target: "bt-modulo_anag_cli",
    			title: 'Gestione anagrafiche cliente',
    			text: 'Gestione anagrafiche clienti/fornitori'
    		});
                    
	        Ext.get("bt-modulo_anag_cli").on('click', function(){
				acs_show_panel_std(<?php echo acs_module_url('anag_cli', 'w_start', 'open_tab') ?>);						        		          		
	        });	        	        	
        }        

      //Gestione fatture anticipo
        if (Ext.get("bt-fatture_anticipo") != null){
        	Ext.QuickTips.register({
    			target: "bt-fatture_anticipo",
    			title: 'Fatturazione anticipi',
    			text: 'Interrogazione e controllo detrazione anticipi'
    		});
                    
	        Ext.get("bt-fatture_anticipo").on('click', function(){
				//acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=open_tab', 'panel_fatture_anticipo');	
	        	acs_show_win_std('Interrogazione anticipi/detrazioni','acs_panel_fatture_anticipo.php?fn=open_parameters', {}, 700, 400, {}, 'icon-credit_cards-16');					        		          		
	        });	        	        	
        }    

        //Dichiarazione di intenti
        if (Ext.get("bt-dichiarazione-intenti") != null){
        	Ext.QuickTips.register({
    			target: "bt-dichiarazione-intenti",
    			title: 'Dichiarazioni di intento',
    			text: 'Interrogazione e controllo dichiarazioni di intento'
    		});
                    
	        Ext.get("bt-dichiarazione-intenti").on('click', function(){
				//acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=open_tab', 'panel_fatture_anticipo');	
	        	acs_show_win_std('Dichiarazioni di intento','acs_panel_dichiarazione_intenti.php?fn=open_parameters', {}, 700, 400, {}, 'icon-email_compose-16');					        		          		
	        });	        	        	
        }    


        //CMR
        if (Ext.get("bt-controllo-rientro-cmr") != null){
        	Ext.QuickTips.register({
    			target: "bt-controllo-rientro-cmr",
    			title: 'CMR',
    			text: 'Riepilogo documenti in attesa di rientro CMR'
    		});
                    
	        Ext.get("bt-controllo-rientro-cmr").on('click', function(){
				//acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=open_tab', 'panel_fatture_anticipo');	
	        	acs_show_win_std('Riepilogo documenti in attesa di rientro CMR','acs_panel_cmr.php?fn=open_parameters', {}, 700, 300, {}, 'icon-delivery-16');					        		          		
	        });	        	        	
        }    

        if (Ext.get("bt-arrivi") != null){
           	Ext.QuickTips.register({
       			target: "bt-arrivi",
       			title: 'To Do List',
       			text: 'Gestione attivit&agrave; di manutenzione'
       		});            
   	        Ext.get("bt-arrivi").on('click', function(){
   	        	acs_show_win_std('Parametri interrogazione To Do list', 'acs_panel_todolist.php?fn=open_filtri', null , 500, 250, {}, 'icon-arrivi-16');             
   	        	});	    
           }


    	// ------------------- REGOLE RIORDINO FORNITORI ------------------------        
        if (Ext.get("bt-marketing") != null) {
          	Ext.QuickTips.register({
   			target: "bt-marketing",
   			title: 'Righe ordine marketing',
   			text: 'Righe ordine marketing'
   		});    	 
            
           Ext.get("bt-marketing").on('click', function(){
        	   acs_show_win_std('Righe ordine marketing', 'acs_righe_ordine_marketing.php?fn=ricerca_ordini_grid', {}, 1100, 450, null, 'icon-gift-16');	
           });        
        }   

  		//schedulo la richiesta dell'ora ultimo aggiornamento
   		/////Ext.TaskManager.start(refresh_stato_aggiornamento);	
     		

        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
