<?php

require_once("../../config.inc.php"); 
require_once("acs_panel_cmr_include.php");

$main_module = new DeskGest();
$s = new Spedizioni();



//elenco nazioni combo
function get_ar_nazioni(){
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFNAZI, TFDNAZ
	FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFFG04='Y' AND TFTIDO='VF'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['TFNAZI']), "text" => $r['TFDNAZ']);
	}
	return $ar;
}

function get_ar_tipo_documento(){
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFTPDO AS COD, TFDTPD AS DES 
	FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']} 
	WHERE TFFG04='Y' AND TFTIDO='VF'
	GROUP BY TFTPDO, TFDTPD ORDER BY TFDTPD";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES'] . " [" . trim($r['COD'] . "]"));
	}
	return $ar;
}


if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();
	
	
	$ar=crea_ar_tree_fatture_anticipo($_REQUEST['node'],  $m_params->open_request->form_values);
	
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}

	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TFCCON, TFDCON
	FROM {$cfg_mod['fatture_anticipo']['file_testate']}
	WHERE UPPER(
	REPLACE(REPLACE(TFDCON, '.', ''), ' ', '')
	) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
					GROUP BY TFCCON, TFDCON
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array("cod"=> trim($row['TFCCON']),"descr"=> acs_u8e(trim($row['TFDCON'])));
	}

	echo acs_je($ret);

	exit;
}




if ($_REQUEST['fn'] == 'open_parameters'){
	?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "FATTURE_ANTICIPO");  ?> {
			            text: 'Aggiorna',
			            iconCls: 'icon-button_black_repeat_dx-32',
			            scale: 'large',
			            handler: function() {
			            
				            Ext.MessageBox.show({
	                            msg: 'Loading',
	                            progressText: 'Loading...',
	                            width:200,
	                            wait:true,
	                            waitConfig: {interval:200},
	                            icon:'ext-mb-download', //custom class in msg-box.html               
	                        });
	                        
	                        
							Ext.Ajax.request({
							        url: '<?php echo ROOT_PATH; ?>/personal/adiuto_verifica_esistenza_cmr.php?fn=verifica_esistenza_per_carico_cliente_ALL',
							        timeout: 2400000,
							        jsonData: {},	
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){	            	  													        
							            var jsonData = Ext.decode(result.responseText);
							            if (jsonData.success == true){
											Ext.MessageBox.hide();							            
											acs_show_msg_info('Operazione conclusa con successo', 'Numero clienti/carichi aggiornati: ' + jsonData.num_upd + ' su ' + jsonData.num_verifiche);
							            } else {
							            	acs_show_msg_error('Errore in fase di salvataggio dati');
							            }							            	
							        },
							        failure    : function(result, request){							        
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });		                        
			
			            }
			         }, {
	                     xtype: 'button',
		            	 scale: 'large',
		            	 iconCls: 'icon-print-32',
	                     text: 'Report',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_cmr_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        }, {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }],   		            
		            
		            items: [
		         	  	{
           				 xtype: 'combo',
						name: 'f_cliente_cod',
						fieldLabel: 'Cliente',
						minChars: 2,			
           				 margin: "20 20 0 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		$cfg_mod = $main_module->get_cfg_mod();
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }, {
							name: 'f_nazione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Nazione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_nazioni(), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_tipo_documento',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Tipo documento',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_tipo_documento(), ""); ?>	
								    ] 
							}						 
						}, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "10 20 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "10 20 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
										 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}
	]
}		
	
		
<?php
	exit;
}



if ($_REQUEST['fn'] == 'open_tab'){
	$request = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	?>

{"success":true, "items": [

        {
            xtype: 'treepanel',
	        title: 'CMR',
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Interrogazioni fatture</b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),             
			
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'codice', 'rif', 'data', 'TFTOTD', 'TFTIMP', 'stato', 'liv', 'liv_cod_qtip', 'carico'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
					
                        extraParams: {
                                    open_request: <?php echo acs_je($request) ?>
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData, 
								

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }       				
                    }

                }),
				

		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            flex: 3,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Nazione/Cliente/Fattura'
			        }, { 
			            dataIndex: 'codice',
			            header: 'Codice', 
			            flex: 1,
			            renderer: function(value, metaData, record){
						
					    	if (record.get('liv_cod_qtip') != ''){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('liv_cod_qtip')) + '"';			    	
					    	}						
						
						 return value;
						}
			        },{ 
			            dataIndex: 'data',
			            header: 'Data', 
			            renderer: date_from_AS,
			            width: 80
			        }, { 
			            dataIndex: 'rif',
			            header: 'Riferimento', 
			            flex: 1
			        }, { 
			            dataIndex: 'carico',
			            header: 'Carico', 
			            flex: 1
			        }, { 
			            dataIndex: 'TFTOTD',
			            header: ' Totale documento', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, { 
			            dataIndex: 'TFTIMP',
			            header: 'Totale imponibile', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'stato',
			            header: 'Stato', 
			            align: 'center',
			            width: 80,
			            renderer: function(value, metaData, record){
			            
						    	if (record.get('stato') == 'C') 
						    	return '<img src=<?php echo img_path("icone/48x48/lock_grey.png") ?> width=18>';
						    	    	
								if (record.get('stato') == 'M') 
								return '<img src=<?php echo img_path("icone/48x48/lock.png") ?> width=18>';	
											
						    }
			        }
			     
	         ]
	         
	         , listeners:{
						beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                        load: function () {
                          Ext.getBody().unmask();
                        }      
	         
	         }	
	         
	         , viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }																				  			
	            
        }  

]
}

<?php exit; }
