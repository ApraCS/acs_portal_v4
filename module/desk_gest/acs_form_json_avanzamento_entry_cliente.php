<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
$main_module = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));

$oggi = oggi_AS_date();


// ******************************************************************************************
// RECUPERO FLAG RILASCIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_richiesta_record_rilasciato'){

	$m_params = acs_m_params_json_decode();
	$prog = $m_params->prog;

	$sql = "SELECT * FROM {$cfg_mod_Gest['file_assegna_ord']} WHERE ASIDPR = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($prog));

	$r = db2_fetch_assoc($stmt);
	$ret = array();
	$ret['success'] = true;
	$ret['ASFLRI'] = $r['ASFLRI'];
	echo acs_je($ret);
	exit;
} //get_json_data




//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

//elenco possibili utenti destinatari
$user_to = array();
$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");

$ar_users_json = acs_je($user_to);

//recupero l'elenco degli ordini interessati
$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

//attualmente e' selezione singola
$row_selected = $m_params->list_selected_id[0];

//recupero causale/utente/ordine da usare come chiave per AS0
$m_k_exp = explode("|", $row_selected->id);
 

$m_causale		= $m_k_exp[0];
$m_utente		= $row_selected->utente;	
$m_documento	= $row_selected->cliente;
$entry_prog		= $row_selected->prog;

//elenco ordini che passo ad un eventuale create_entry
foreach ($m_params->list_selected_id as $list_selected_ord){
	$ar_documenti[] = $list_selected_ord->k_ordine;
}

//$ar_documenti[] = $m_documento; 

global $conn, $cfg_mod_Spedizioni, $id_ditta_default;
$sql = "SELECT * FROM {$cfg_mod_Gest['file_assegna_ord']}
		 WHERE ASDT = '{$id_ditta_default}' AND ASFLRI <> 'Y' /* aperta */
		   AND ASCAAS = ? AND ASUSAT = ? AND ASDOCU = ?";

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, array($m_causale, $m_utente, $m_documento));


$row = db2_fetch_assoc($stmt);

//causali di rilasio
$causali_rilascio = $s->find_TA_std('RILAV', $m_causale, 'N', 'Y');

//escludo eventualmente alcune causali di rilascio in base all'utente e ai permessi
foreach ($causali_rilascio as $key => $value){
	
	//IMMOR - RIA
	//Riassegnazione referente order grafico, solo se non ancora graficato (TDFN13=1) o sono un amministratore modulo (UPFLMP, p_mod_param)	
	if (trim($value['TAKEY1']) == 'IMMOR' && trim($value['TAKEY2']) == 'RIA'){

		$sql_ord = "SELECT COUNT(*) AS T_ROW 
						FROM {$cfg_mod_Spedizioni['file_testate']} TD
						WHERE " . $s->get_where_std() . " AND TDDOCU IN (" . sql_t_IN($ar_documenti) . ")
						  AND TDFN13 = 0  
						";		
		$stmt_ord = db2_prepare($conn, $sql_ord);
 		$result = db2_execute($stmt_ord);
 		$row_ord = db2_fetch_assoc($stmt_ord);
 		
 		if ($row_ord['T_ROW'] > 0 || $js_parameters->p_mod_param != 'Y') //rimuovo la scelta
 			unset($causali_rilascio[$key]);
	}	
}

?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'exe_avanzamento_segnalazione_arrivi_cli'
                	}, {
	                	xtype: 'hidden',
	                	name: 'list_selected_id',
	                	value: '<?php echo $list_selected_id_encoded; ?>'
                	}, {
						name: 'f_entry_prog_causale',
						id: 'f_entry_prog_causale',
						xtype: 'combo',
						fieldLabel: 'Causale di rilascio',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: false,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}, 'TARIF2'],
						    data: [								    
							     <?php echo acs_ar_to_select_json($causali_rilascio, ""); ?>	
							    ],
						},
						
												 
					}, {
						name: 'f_entry_prog_note',
						id: 'f_entry_prog_note',
						xtype: 'textfield',
						fieldLabel: 'Note',
					    maxLength: 100							
					}
					
					
					],
			buttons: [{
	            text: 'Salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')

					if(form.isValid()){
					
							var m_f = form.findField('f_entry_prog_causale');
							var raw_causale_selected = m_f.findRecordByValue(m_f.getValue());
							
							var causale_successiva = raw_causale_selected.get('TARIF2');
							
							if (causale_successiva.trim().length > 0) {
							//se richiesto apro la schermata per la segnalazione successiva
							
									var mw = new Ext.Window({
									  width: 800
									, height: 380
									, minWidth: 300
									, minHeight: 300
									, plain: true
									, title: 'Apertura segnalazione collegata'
									, iconCls: 'iconArtCritici'			
									, layout: 'fit'
									, border: true
									, closable: true
									, id_selected: window.id_selected								
									});				    			
					    			mw.show();							
							
										Ext.Ajax.request({
										        url        : 'acs_form_json_create_entry.php',
										        jsonData: {	tipo_op: causale_successiva.trim(),
										        			entry_prog: <?php echo $entry_prog; ?>,
										        			entry_prog_causale: window.down('#f_entry_prog_causale').getValue(),
										        			entry_prog_note: window.down('#f_entry_prog_note').getValue(),
										        			list_selected_id_old: <?php echo acs_je($ar_documenti)?>,
										        			list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>,
										        			},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										            var jsonData = Ext.decode(result.responseText);
										            mw.add(jsonData.items);
										            mw.doLayout();
										            
										            window.close();
										            				            
										        },
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });
					
									} else { 
										//non e' richiesta la cusale successiva....salvo e chiudo
											
										Ext.getBody().mask('Loading... ', 'loading').show();											
												            	
						                form.submit({
					                            //waitMsg:'Loading...',
					                            success: function(form,action) {					                            
													Ext.getBody().unmask();					                            
				                    				window.close();        	
					                            },
					                            failure: function(form,action){
					                                Ext.MessageBox.alert('Erro');
					                            }
					                        });
				            		}
				    }            	                	                
	            }
	        }],             
			
			
			listeners: {
		        afterrender: function(){
		     	   f = this.getForm().findField('f_entry_prog_causale');
		     	   
		     	   //se imposto direttamente la causale di avanzamento
		     	   <?php if (strlen($m_params->auto_set_causale) > 0) { ?>
		     	    f.setValue(<?php echo j($m_params->auto_set_causale); ?>);
		     	   // b_salva = this.down('#b_salva'); //ToDo: come essere sicuri che ci sia solo "Salva"?
		     	    b_salva = this.down('button');
		     	    b_salva.fireHandler();
		     	    return;
		     	   <?php } ?>
		     	   
		     	   <?php if (count($causali_rilascio) == 1 && strlen(trim($causali_rilascio[0]['TARIF2'])) > 0){ ?>
		     	    f.setValue(<?php echo j(trim($causali_rilascio[0]['id'])); ?>);
		     	    b_salva = this.down('button'); //ToDo: come essere sicuri che ci sia solo "Salva"?
		     	    b_salva.fireHandler();
		     	    //this.up('window').close(); //va in errore     	    
		     	   <?php } ?>
		        },
		    }			
			
				
				
        }
]}