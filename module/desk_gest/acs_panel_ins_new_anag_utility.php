<?php

require_once("../../config.inc.php");
require_once("acs_panel_ins_new_anag.php");

$main_module = new DeskGest(array('abilita_su_modulo' => 'DESK_VEND'));
$m_params = acs_m_params_json_decode();
$s = new Spedizioni(array('no_verify' => 'Y'));

// ******************************************************************************************
//  AGGIORNA ANAGRAFICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiorna'){
    
    $ritime = microtime(true);
    
    if(isset($m_params->list_ord) && count($m_params->list_ord) > 0){
        foreach($m_params->list_ord as $v){
            $ar_upd = array();
            $ar = explode("_", $v->k_ordine);
            
            $ar_upd['RATIME'] = $ritime;
            $ar_upd['RADT']   = $ar[0];
            $ar_upd['RATIDO'] = $ar[1];
            $ar_upd['RAINUM'] = $ar[2];
            $ar_upd['RAAADO'] = $ar[3];
            $ar_upd['RANRDO'] = $ar[4];
            $sql = "INSERT INTO {$cfg_mod_Gest['file_richieste_anag_cli']} (" . create_name_field_by_ar($ar_upd) . ") VALUES (" . create_parameters_point_by_ar($ar_upd) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd);
            echo db2_stmt_errormsg($stmt);
            
        }
    }

    if(isset($m_params->list_ord) && count($m_params->list_ord) > 0){
        $messaggio = 'AGG_PRAG_VO';
        $rifg01 = $m_params->list_ord[0]->age_uno;
        $rifg02 = $m_params->list_ord[0]->age_due;
        $rifg03 = $m_params->list_ord[0]->age_tre;
       
    }else{
        $messaggio = 'CF_AGG_AGE_PROV';
        $rifg01 = $m_params->form_values->f_cond;
        $rifg02 = $m_params->form_values->f_sosp;
        $rifg03 = "";
    }
   
    $sh = new SpedHistory();
    $ret_RI = $sh->crea(
        'pers',
        array(
            "RITIME" => $ritime,
            "messaggio"	=> $messaggio,
            "vals" => array(
                "RICAGE" =>  trim($m_params->form_values->f_c_age),
                "RIVETT" =>  trim($m_params->form_values->f_n_age),
                "RIPPAN " => sql_f($m_params->form_values->f_n_pro),
                "RIPPAO" =>  sql_f($m_params->form_values->f_c_pro),
                "RIFG01" =>  $rifg01,
                "RIFG02" =>  $rifg02,
                "RIFG03" =>  $rifg03
              
            )
            
        )
        );
    
    $ret = array();
    $ret['success'] = true;
    $ret['msg'] = "AGGIORNATI " . (int)$ret_RI['RIQTA'] . " RECORD";
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'open_age_prov'){ ?>

   {"success":true, "items": [

        {
            xtype: 'form',
            flex: 1,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
        	frame: true,
            items: [
            
                <?php if($m_params->ord == 'Y'){?>
                
                {    xtype: 'displayfield',
        	         fieldLabel: 'Ordini selezionati',
        	         value : '<?php echo count($m_params->list_ordini); ?>',
        	         margin : '0 0 10 0'
        	    },
                  
                <?php }?>
            
            
            
            		{xtype: 'fieldcontainer',
    				flex:1,
    				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
    				items: [
    				
    				 <?php if($m_params->ord == 'Y'){
    				     $age = $m_params->list_ordini[0]->agente;
    				     $decode = get_TA_sys('CUAG', $age);
    				     $d_age = "[".trim($age)."] ".$decode['text'];
    				     
    				     ?>
            				 
                        {    xtype: 'displayfield',
                	         fieldLabel: 'Agente corrente',
                	         value : <?php echo j($d_age); ?>,
                	         margin : '0 0 10 0',
                	         flex : 1
                	    },
                	    {
                	         xtype: 'hiddenfield',
                	         value : <?php echo j($age); ?>,
                	         name : 'f_c_age'
                	    }
    				<?php }else{
    				  write_combo_std('f_c_age', 'Agente corrente', '', acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), ''), array('labelWidth' => 120)); 
    				 } ?>
    				 
    				 <?php $list_allowBlank = "
                        'blur': function(field) {
		                    var form = this.up('form').getForm();
                            var n_age  = field.value;
                            var c_prov = form.findField('f_c_pro');
                            var n_prov = form.findField('f_n_pro');
                            if(n_age > 0){
                                c_prov.allowBlank = true;
                                n_prov.allowBlank = true;
                            }                              
                        }
                    ";
    				
		          ?>
    				  
    				 , <?php write_combo_std('f_n_age', 'Nuovo agente', '', acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), ''), array('labelWidth' => 120, 'labelAlign' => 'right', 'list_allowBlank' => $list_allowBlank)) ?>
    				]},
    				{xtype: 'fieldcontainer',
    				flex:1,
    				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
    				items: [
    				
    				<?php 
    				
    				$list_allowBlank = "
                        'blur': function(field) {
		                    var form = this.up('form').getForm();
                            var n_age  = form.findField('f_n_age');
                            var c_prov = form.findField('f_c_pro');
                            var n_prov = field.value;
                            if(n_prov > 0){
                                c_prov.allowBlank = false;
                                n_age.allowBlank = true;
                            }else{
                               c_prov.allowBlank = true;}
                        }
                    ";
    				
		          ?>
    			
    				     <?php write_numberfield_std('f_c_pro', 'Provvigione corrente', '', 11, array('flex_width' => 'flex:1', 'labelWidth' => 120)) ?>	
    				   , <?php write_numberfield_std('f_n_pro', 'Nuova provvigione', '', 11, array('labelAlign' => 'right', 'flex_width' => 'flex:1', 'labelWidth' => 120, 'list_allowBlank' => $list_allowBlank) ) ?>	
    				]}
    				<?php if($m_params->ord != 'Y'){?>
    				,{
						xtype: 'radiogroup',
						fieldLabel: ' Aggiorna condizioni',
						labelWidth: 120,
						labelAlign: 'left',
					   	allowBlank: true,
					   	vertical: true,
					   	columns: 1,
					   	flex : 1,
					   	items: [{
	                            xtype: 'radio'
	                          , name: 'f_cond' 
	                          , boxLabel: 'Standard'
	                          , inputValue: 'S'
	                          , checked: true
	                        }, {
	                            xtype: 'radio'
	                          , name: 'f_cond' 
	                          , boxLabel: 'Condizionate (BCCO)'
	                          , inputValue: 'C'
	                         
	                        }, {
	                            xtype: 'radio'
	                          , name: 'f_cond' 
	                          , boxLabel: 'Tutte'
	                          , inputValue: 'T'
	                          
	                        }]
						},
							{
						name: 'f_sosp',
						xtype: 'checkboxgroup',
						fieldLabel: 'Aggiorna sospesi',
						//labelAlign: 'right',
					   	allowBlank: true,
					   	labelWidth: 120,
					   	//flex : 1,
					   	items: [{								   	
					            xtype: 'checkbox'
						       , width: 200
						      //, labelWidth: 170
					          , name: 'f_sosp' 
					          , boxLabel: ''
					          , inputValue: 'Y'		                          
					        }]   														
					}	
            
            	    <?php }?>
            	
            	],
            	 
            	buttons: [
            	  {
    	            text: 'Conferma',
    	            iconCls: 'icon-button_blue_play-32',
    	            scale: 'large',
    	            handler: function() {
    	                var loc_win = this.up('window');
    	                var form = this.up('form').getForm();
    	                var n_age  = form.findField('f_n_age').getValue();
                        var n_prov = form.findField('f_n_pro').getValue();
                        if(Ext.isEmpty(n_age) && Ext.isEmpty(n_prov)){
                              acs_show_msg_error('Inserire un nuovo agente o una nuova provvigione');
                              return false;
                        }
    	                
    	                
    	            	if(form.isValid()){	            	
                			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form.getValues(),
			        				list_ord : <?php echo acs_je($m_params->list_ordini); ?>
			        			},							        
						        success : function(result, request){
						        	jsonData = Ext.decode(result.responseText);
						            acs_show_msg_info(jsonData.msg);
						        	loc_win.close();
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
        		        }      
    			             	                	                
    	            }
    	        }
            	  ]
            	
            	
            	}
             ]}
            
   <?php }