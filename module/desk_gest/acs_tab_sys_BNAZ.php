<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
$puvn = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";



$m_table_config = array(
    'msg_on_save' => true,
    'TAID' => 'BNAZ',
    'title_grid' => 'BNAZ-Nazioni',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 50, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4),
        'TACINT'   => array('label'	=> 'Interfaccia', 'only_view' => 'F',  'maxLength' => 10),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'area_f1'    => array('label'=> 'Area fiscale', 'short' => 'AF', 'xtype' => 'combo_tipo', 'c_width' => 30),
        'a_geo'    => array('label'	=> 'Area geografica', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'c_iso'    => array('label'	=> 'Codice ISO', 'maxLength' => 3, 'only_view' => 'F'),
        'gg_tras'    => array('label'	=> 'Giorni trasporto', 'only_view' => 'F', 'maxLength' => 3),
        'c_intra'    => array('label'	=> 'Codice intra', 'only_view' => 'F', 'maxLength' => 2),
        'c_iso_2'    => array('label'	=> 'Codice ISO 2', 'short' => 'I2', 'maxLength' => 2, 'c_width' => 40),
        'doga1'    => array('label'	=> 'Gruppo doganale', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella nazioni',
   // 'j_puvn' => 'Y',
   // 'ricerca_var' => 'Y',
   // 'tab_tipo' => 'PUVR',
    'TAREST' => array(
       'area_f1'	=> array(
           "start" => 0,
           "len"   => 1,
           "riempi_con" => ""
       )
       ,'a_geo'	=> array(
           "start" => 1,
           "len"   => 3,
           "riempi_con" => ""
       ),  
        'c_iso'	=> array(
           "start" => 52,
           "len"   => 3,
           "riempi_con" => ""
       ),
        'gg_tras'	=> array(
            "start" => 61,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'c_intra'	=> array(
            "start" => 72,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'c_iso_2'	=> array(
            "start" => 74,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'doga1'	=> array(
            "start" => 77,
            "len"   => 3,
            "riempi_con" => ""
        )
   )
    
);

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
