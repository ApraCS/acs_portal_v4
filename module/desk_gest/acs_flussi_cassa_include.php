<?php

function crea_ar_tree_flussi_cassa($node, $m_params){
    global $cfg_mod_Gest, $conn, $id_ditta_default;
 
    $sql_where = sql_where_by_request($m_params);
    
    if (
           (isset($m_params->open_request->f_raggruppa_per) 
            && $m_params->open_request->f_raggruppa_per == 'DECADE') ||
           (isset($m_params->f_raggruppa_per)
            && $m_params->f_raggruppa_per == 'DECADE')
        ){
        //raggruppamento per decade
        $sql_periodo = " CASE WHEN SUBSTR(FC.DT_RIFERIMENTO, 7, 2) <= '10' THEN 1 
                              WHEN SUBSTR(FC.DT_RIFERIMENTO, 7, 2) <= '20' THEN 2 
                              ELSE 3 END ";
    } else {
        //raggruppamento mensile
        $sql_periodo = " 0 ";
    }
    
    
    if ($cfg_mod_Gest["flussi_cassa"]["flag_escludi_da_tot_generale"] == 'Y'){
        $sql_IMPORTO_ESCLUSO_E = " SUM(CASE WHEN FLESC0='E' THEN 0 ELSE FC.IMP_RIF END) AS IMPORTO_ESCLUSO_E ";        
    } else {
        $sql_IMPORTO_ESCLUSO_E = " SUM(IMP_RIF) AS IMPORTO_ESCLUSO_E ";
    }
    
    $anno_mese_start = substr($m_params->dataStart, 0, 6);
    $anno_mese_end    = substr($m_params->dataEnd, 0, 6);
    
    $sql = "SELECT FC.FLDITT, FC.CATEGORIA AS FLUCAT, R_CAT.FLUSEQ, R_CAT.FLUDES AS D_CAT
            , SUBSTR(FC.DT_RIFERIMENTO, 1, 6) AS ANNO_MESE, {$sql_periodo} AS ID_PERIODO
            , D_GRUPPO.FLUCOP, D_GRUPPO.FLUDES
            , SUM(IMP_RIF) AS IMPORTO
            , {$sql_IMPORTO_ESCLUSO_E}
    FROM {$cfg_mod_Gest['flussi_cassa']['file_movimenti']} FC
    
    INNER JOIN (
      SELECT FLUDIT, FLUCAT, FLUCOP, MAX(FLUSEQ) AS FLUSEQ, FLUDES
        FROM {$cfg_mod_Gest['flussi_cassa']['file_conti']} FT_1
        WHERE FLUDST <> 'T'
        GROUP BY FLUDIT, FLUCAT, FLUCOP, FLUDES
      ) R_CAT
      ON R_CAT.FLUDIT = FC.FLDITT AND R_CAT.FLUCAT = FC.CATEGORIA
    
    INNER JOIN (
    SELECT FLUDIT, FLUCOP, FLUDES
    FROM {$cfg_mod_Gest['flussi_cassa']['file_conti']} FT_1
    WHERE FLUDST = 'T'
    GROUP BY FLUDIT, FLUCOP, FLUDES
    ) D_GRUPPO
    ON D_GRUPPO.FLUDIT = R_CAT.FLUDIT AND D_GRUPPO.FLUCOP = R_CAT.FLUCOP
    
    WHERE 1=1 {$sql_where}
    GROUP BY FC.FLDITT, FC.CATEGORIA, R_CAT.FLUSEQ, R_CAT.FLUDES, 
        SUBSTR(FC.DT_RIFERIMENTO, 1, 6), {$sql_periodo},
        D_GRUPPO.FLUCOP, D_GRUPPO.FLUDES
    ORDER BY R_CAT.FLUSEQ
    ";
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar = array();
    
    //inizializzo con la riga di totale
    $ar_new = array();
    $ar_new['id'] = 'totale';
    $ar_new['liv'] = 'liv_0';
    $ar_new['task'] = 'TOTALE GENERALE';
    $ar_new['tipo_voce'] = 'S2';
    $ar_new['leaf'] = true;
    $ar_new['expanded'] = false;
    $ar['totale'] = $ar_new;
    
    //Totale progressivo *****************************
    /* lo ridefinisco dopo, qui serve per accodarlo subito dopo il totale */
    $ar_new = array();
    $ar_new['id'] = 'totale_prog';
    $ar['totale_prog'] = $ar_new;
    
    
    while ($r = db2_fetch_assoc($stmt)) {
        sum_row_parziale($ar['totale'], $r, $anno_mese_start, $anno_mese_end, true);
        
        $tmp_ar_id = array();
        $ar_r = &$ar;
        
        $cod_liv1 = trim($r['FLUCOP']); //codice raggruppamento
        $cod_liv2 = trim($r['FLUCAT']);	//codice categoria
        
        //GRUPPO
        $liv = $cod_liv1;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['liv'] = 'liv_1';
            $ar_new['task'] = implode(" - ", array($liv, trim($r['FLUDES'])));
            $ar_new['tipo_voce'] = 'S1';
            $ar_new['leaf'] = false;
            $ar_new['expanded'] = false;
            $ar_new['children'] = array();
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_row_parziale($ar_r, $r, $anno_mese_start, $anno_mese_end);
        
        //CATEGORIA
        $liv = $cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['liv'] = 'liv_2';
            $ar_new['task'] = acs_u8e("[" . trim($r['FLUCAT']) . "] " . trim($r['D_CAT']));
            $ar_new['tipo_voce'] = 'dett';
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_row_parziale($ar_r, $r, $anno_mese_start, $anno_mese_end);
    }
    
  
    //Totale progressivo *****************************
    $ar_new = array();
    $ar_new['id'] = 'totale_prog';
    $ar_new['liv'] = 'liv_1';
    $ar_new['task'] = 'TOTALE PROGRESSIVO';
    $ar_new['tipo_voce'] = 'S2_prog';
    $ar_new['leaf'] = true;
    $ar_new['expanded'] = false;
    $totale_progressivo = 0;
    
    
    $mm 	= substr($m_params->dataStart, 4, 2);
    $yyyy 	= substr($m_params->dataStart, 0, 4);
    
    if (isset($m_params->open_request->f_raggruppa_per) && $m_params->open_request->f_raggruppa_per == 'DECADE'){
        //RAGGRUPPO PER DECADE (4mesi x 3 decadi)
        for ($i=0; $i<4; $i++){
            
            //Prima decade
            $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
            $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i,  1, $yyyy));
            $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, 10, $yyyy));
            
            $totale_progressivo += $ar['totale']["d_{$d1}_{$d2}"];
            $ar_new["d_{$d1}_{$d2}"]  = $totale_progressivo;
            
            //Seconda decade
            $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
            $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 11, $yyyy));
            $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, 20, $yyyy));
            
            $totale_progressivo += $ar['totale']["d_{$d1}_{$d2}"];
            $ar_new["d_{$d1}_{$d2}"]  = $totale_progressivo;
            
            
            //Terza decade
            $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
            $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 21, $yyyy));
            $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
            
            $totale_progressivo += $ar['totale']["d_{$d1}_{$d2}"];
            $ar_new["d_{$d1}_{$d2}"]  = $totale_progressivo;            
        }
    } else {
        //RAGGRUPPO PER MESE
        for ($i=0; $i<12; $i++){
            $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
            $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 1, $yyyy));
            $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));

            $totale_progressivo += $ar['totale']["d_{$d1}_{$d2}"];
            $ar_new["d_{$d1}_{$d2}"]  = $totale_progressivo;            
        }
    }

    
    $ar['totale_prog'] = $ar_new;
    
    
 
    
    return $ar;
}


//**************************************************************************************
// TREE	PRINCIPALE
//**************************************************************************************
function write_main_tree($p){
    
    global $cfg_mod_Gest, $conn;

	//calcolo intervalli (per ora parto dal mese in corso, con intervalli mensili)		
	if (isset($p['dataStart'])){
		$mm 	= substr($p['dataStart'], 4, 2);
		$yyyy 	= substr($p['dataStart'], 0, 4);
	} else {
		//da mese/anno attuale	
		$mm 	= date('m');
		$yyyy	= date('Y');
	}
	
	$start_day = mktime(0, 0, 0, $mm, 1, $yyyy); //il primo giorno del mese attuale	
	$ar_mesi = array();
	
	$oldLocale = setlocale(LC_TIME, 'it_IT');
	
	if (isset($p['open_request']->f_raggruppa_per) && $p['open_request']->f_raggruppa_per == 'DECADE'){
	    //RAGGRUPPO PER DECADE (4mesi x 3 decadi)	    
	    for ($i=0; $i<4; $i++){
	        
	        //Prima decade	        
	        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);	        
	        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i,  1, $yyyy));
	        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, 10, $yyyy));
	        
	        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
	            'd1' => $d1,
	            'd2' => $d2,
	            'column_label' => ucfirst(strftime("%b %y-1aD", strtotime($d1)))
	        );	        
	        
	        //Seconda decade
	        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
	        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 11, $yyyy));
	        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, 20, $yyyy));
	        
	        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
	            'd1' => $d1,
	            'd2' => $d2,
	            'column_label' => ucfirst(strftime("%b %y-2aD", strtotime($d1)))
	        );
	        
	        //Terza decade
	        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
	        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 21, $yyyy));
	        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
	        
	        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
	            'd1' => $d1,
	            'd2' => $d2,
	            'column_label' => ucfirst(strftime("%b %y-3aD", strtotime($d1)))
	        );
	        
	        
	        
	        
	    }
	} else {
	    //RAGGRUPPO PER MESE
	    for ($i=0; $i<12; $i++){
	        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
	        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 1, $yyyy));
	        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
	        
	        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
	            'd1' => $d1,
	            'd2' => $d2,
	            'column_label' => ucfirst(strftime("%b %y", strtotime($d1)))
	        );
	    }
	}
	
	
	setlocale(LC_TIME, $oldLocale);
	
	$dataStart = date('Ymd', $start_day);
	$dataEnd =  $d2;	
	
	$sql = "SELECT FLUDED FROM {$cfg_mod_Gest['flussi_cassa']['file_conti']} WHERE FLUDIT = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($p['f_ditta']));
	$r = db2_fetch_assoc($stmt);
	$ditta = "[".$p['f_ditta']."] ".trim($r['FLUDED']);
	
?>	
			{
				xtype: 'treepanel',
				cls: 'flussi_cassa_tree_<?php echo $p['open_type']; ?>',
		        title: <?php echo acs_je("Flussi cassa {$p['open_type']}"); ?>,
		        <?php echo make_tab_closable(); ?>,
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
		        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Analisi flussi di cassa - Ditta <?php echo $ditta; ?> </b>', '->',
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),  
		        
		        
		        
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					    fields: ['task', 'liv', 'tipo_voce', 'scaduto', 'oltre'
								<?php
			  							foreach($ar_mesi as $km => $mesi){
												echo ", '{$km}'";				
			  							}
			 					?>					    
					    ],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							actionMethods: {read: 'POST'},
	                        
	                        reader: {
	                            root: 'children'
	                        },
	                        
							extraParams: {
								f_ditta: <?php echo acs_je($p['f_ditta']); ?>,
								dataStart:  <?php echo acs_je($dataStart); ?>,
								dataEnd:  <?php echo acs_je($dataEnd); ?>,
								open_type: <?php echo acs_je($p['open_type']); ?>,
								open_request: <?php echo acs_je($p['open_request']); ?>
							}
	                	    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),
	
	            multiSelect: false,
		        singleExpand: false,
		
		<?php
				$ss = "<img id=\"flusso_cassa_cal_prev_1y\"src=" .  img_path("icone/48x48/button_grey_rew.png") . " height=30>";
				$ss .= "<img id=\"flusso_cassa_cal_next_1y\"src=" .  img_path("icone/48x48/button_grey_ffw.png") . " height=30>";				
				$ss .= "<br/>&nbsp;";
				
				$scaduto_column_label 	= 'Precedente';
				$oltre_column_label		= 'Successivo';
				
				switch ($p['open_type']){
					case 'scaduto':
						$ss = 'Scaduto';
						break;
					case 'oltre':
						$ss = 'Oltre';
						break;
					default:
						$ss = 'Voce';
						$scaduto_column_label 	= 'Scaduto';
						$oltre_column_label		= 'Oltre';
				}
				
		?>				
		
				columns: [	
		    		{text: '<?php echo $ss; ?>', flex: 1, xtype: 'treecolumn', dataIndex: 'task', menuDisabled: true, sortable: false}

		    		<?php if ($p['open_type'] != 'oltre'){ ?>
		    		, {header: <?php echo acs_je($scaduto_column_label); ?>, tdCls: 'scaduto', dataIndex: 'scaduto', width: 78, renderer: floatRenderer0, align: 'right', sortable : false}
		    		<?php } ?>
		    		
					<?php
						//costruisco le 12 colonne dei mese (dal mese attuale in avanti)
						foreach($ar_mesi as $km => $mesi){
							echo ", {header: " . acs_je($mesi['column_label']) . ", dataIndex: '{$km}', width: 78, renderer: floatRenderer0, align: 'right', sortable : false}";				
						}					
					?>
					
					<?php if ($p['open_type'] != 'scaduto'){ ?>					
					, {header: <?php echo acs_je($oltre_column_label); ?>, tdCls: 'oltre', dataIndex: 'oltre', width: 78, renderer: floatRenderer0, align: 'right', sortable : false}
					<?php } ?>				
		    			   	
				],
				enableSort: false, // disable sorting
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
			          
			          
					  celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  
						  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
						  	col_text = iView.getGridColumns()[iColIdx].text;
						  	rec = iView.getRecord(iRowEl);
						  	
						  	//se fo fatto click su "scaduto" -> apro tab scaduti
						  	if (col_name == 'scaduto'){
						  		acs_show_panel_std('acs_panel_flussi_cassa.php?fn=open_tab_scaduto', 'panel-flussi_cassa_scaduto', iView.getStore().treeStore.proxy.extraParams);
						  		return false;
						  	}
						  	
						  	//se fo fatto click su "oltre" -> apro tab oltre
						  	if (col_name == 'oltre'){
						  		acs_show_panel_std('acs_panel_flussi_cassa.php?fn=open_tab_oltre', 'panel-flussi_cassa_oltre', iView.getStore().treeStore.proxy.extraParams);
						  		return false;
						  	}
						  	
						  	//se sono in una cella di liv_2 apri i dettagli per riferimento
						  	if (rec.get('liv') == 'liv_2'){
						  		acs_show_win_std('Flussi di cassa - ' + rec.get('task') + ' - ' + col_text, 
						  			'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_riferimento', 
						  			{
						  				form_ep: iView.getStore().treeStore.proxy.extraParams,
						  				liv: rec.get('liv'),
						  				record_id: rec.get('id'),
						  				record_task: rec.get('task'),
						  				col_name: col_name
						  			}, 1000, 600, {}, 'icon-info_blue-16');
						  		return false;
						  	}
						  	
						  	
						  }
					  }
			            
			          
			            	        
			        } //listeners
				
	
			, viewConfig: {
			        getRowClass: function(record, index) {		        	
			           return record.get('tipo_voce');																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree













function write_dettaglio_riferimento($p){
	?>
        {
            xtype: 'grid', itemId: 'dettaglio_per_riferimento',
            flex: 1,
			
		    		features: new Ext.create('Ext.grid.feature.Grouping',{
        							groupHeaderTpl: 'Intestatario: {[values.rows[0].data.cod_web]} - {[values.rows[0].data.des_web]}',
        							hideGroupedHeader: true
    						}),			
			
			store: {
					xtype: 'store',
					groupField: 'cod_web',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_per_riferimento',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
										f_ditta: <?php echo acs_je($p['f_ditta']); ?>,
										dataStart:  <?php echo acs_je($p['dataStart']); ?>,
										dataEnd:  <?php echo acs_je($p['dataEnd']); ?>,
										raggruppamento: <?php echo acs_je($p['raggruppamento']); ?>,
										categoria: <?php echo acs_je($p['categoria']); ?>,
										open_request:  <?php echo acs_je($p['open_request']); ?>,
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['data', 'data_out', 'riferimento', 'riferimento_out', 'importo', 'cod_web', 'des_web']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Data',
	                dataIndex: 'data_out', renderer: date_from_AS,
	                width: 60 
	            }, {
	                header   : 'Riferimento',
	                dataIndex: 'riferimento_out',
	                flex: 10
	            }, {
	                header   : 'Importo',
	                dataIndex: 'importo', renderer: floatRenderer2, align: 'right',
	                flex: 10
	            }   
	         ],																					

	         
//itemclick: function(view,rec,item,index,eventObj) {	         
	         
	        listeners: {
	        
					  itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  	grid_dettagli_riga = iView.up('panel').up('panel').getComponent('dettaglio_per_riga');
						  	grid_dettagli_riga.store.proxy.extraParams.data = rec.get('data');
						  	grid_dettagli_riga.store.proxy.extraParams.riferimento = rec.get('riferimento');
						  	grid_dettagli_riga.store.load();
						  }
					  }	  	        	        

			}			
			
				  
	            
        }   	
	
	
	<?php
} //write_dettaglio_riferimento






function write_dettaglio_righe($p){
	?>
        {
            xtype: 'grid', itemId: 'dettaglio_per_riga',
			flex: 1,
			store: {
					xtype: 'store',
					autoLoad: false,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_per_riga',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
										f_ditta: <?php echo acs_je($p['f_ditta']); ?>,
										dataStart:  <?php echo acs_je($p['dataStart']); ?>,
										dataEnd:  <?php echo acs_je($p['dataEnd']); ?>,
										raggruppamento: <?php echo acs_je($p['raggruppamento']); ?>,
										categoria: <?php echo acs_je($p['categoria']); ?>,
										data: null,
										riferimento: null 
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['segnala_riga', 'riga', 'cod_articolo', 'descr_articolo', 'riferimento', 'importo']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width: 60 
	            }, {
	                header   : 'Articolo',
	                dataIndex: 'cod_articolo',
	                width: 80
	            }, {
	                header   : 'Descrizione',
	                dataIndex: 'descr_articolo',
	                flex: 10
	            }, {
	                header   : 'Importo',
	                dataIndex: 'importo', renderer: floatRenderer2, align: 'right',
	                width: 90
	            }   
	         ],																					

	         
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
						  }
					  }	  
			}			
			
				
		   , viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {		        	
		           return record.get('segnala_riga');																
		         }   
		    }				
				  
	            
        }   	
	
	
	<?php
} //write_dettaglio_righe







// DELETE *****************************************************************

function write_main_tree_old($p){
	
 //calcolo intervalli (per ora parto dal mese in corso, con intervalli mensili)
 $mm 	= date('m');
 $yyyy	= date('Y');
 $start_day = mktime(0, 0, 0, $mm, 1, $yyyy); //il primo giorno del mese attuale
 $ar_mesi = array();
 for ($i=0; $i<12; $i++){
 	$d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
 	$d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 1, $yyyy)); 	 	
 	$d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
 	 
 	$ar_mesi[$d1.$d2] = array('d1' => $d1, 'd2' => $d2);
 }
	
?>
{
	xtype: 'grid',
	loadMask: true,
	title: 'FlussiCassa',
		<?php echo make_tab_closable(); ?>,
	
        tbar: new Ext.Toolbar({
            items:['<b>???', '->',
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
         ]            
        }),  
		
	
		store: {
			xtype: 'store',
				
			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
				f_ditta: <?php echo acs_je($p->f_ditta); ?>
			}
			
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				, doRequest: personalizza_extraParams_to_jsonData						
			
			},
				
			fields: ['voce', 'tipo_voce', 'codice_voce'
			 <?php
			  foreach($ar_mesi as $km => $mesi){
				echo ", 'd_{$km}'";				
			  }
			 ?>
			]
						
		}, //store

		multiSelect: false,
		columnLines: false, //righe separazione cell in verticale
		columns: [
			{header: 'Codice', dataIndex: 'codice_voce', width: 60},		
			{header: 'Voce', dataIndex: 'voce', flex: 1}
			
			<?php
			//costruisco le 12 colonne dei mese (dal mese attuale in avanti)
			foreach($ar_mesi as $km => $mesi){
				echo ", {header: '" . print_date($mesi['d1']) . "<br>" . print_date($mesi['d2']) . "', dataIndex: 'd_{$km}', width: 80}";				
			}
			
			?>
			

					
		],
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
				    //in col_name ho in pratico il codice ditta
				    acs_show_panel_std('acs_panel_abi_azienda.php?fn=open_tab', Ext.id(), {
				    	codice_ditta: col_name,
				    	nome_analisi_1: iView.store.proxy.extraParams.nome_analisi_1,
				    	nome_analisi_2: iView.store.proxy.extraParams.nome_analisi_2
				    });

				}
			}
			
		}
		
		
		, viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }   
		    }												    
		
		
		 
	} 
<?php } //write main grid


?>