<?php

require_once "../../config.inc.php";
require_once("acs_panel_ins_new_anag.php");

ini_set('max_execution_time', 6000);
ini_set('memory_limit', '2048M');

$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));

$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){ ?>
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            layout: {pack: 'start', align: 'stretch'},
	            items: [
    	            <?php write_combo_std('f_agente', 'Agente', '', acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), ''), array('allowBlank' => 'true')) ?>
	               ],
	               		buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                            form_values: Ext.encode(this.up('form').getValues())
		                         	
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        
		        
		        ]     
	               
	               
	               }
	            
	            ]}

         


<?php 
 
}

if ($_REQUEST['fn'] == 'open_report'){


$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$agente = $form_values->f_agente;
$where = " AND (CCAG1 = '{$agente}' OR CCAG2 = '{$agente}' OR CCAG3 = '{$agente}')";
$sql_join = " INNER JOIN (
                SELECT CCPROG
                FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
                WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
             ON CC.CCPROG = GC.GCPROG";


//elenco anagrafiche clienti
$sql = "SELECT SP.*, GC.*
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
        {$sql_join}
        INNER JOIN {$cfg_mod_Gest['file_scala_premi']} SP
            ON SP.SPDT = GC.GCDT AND SP.SPCCON = GC.GCCDCF
        WHERE GCDT = '{$id_ditta_default}' AND GCTPAN = 'CLI' AND SPSOSP <> 'S'
        ORDER BY SPPROG
        LIMIT 100";

    
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
while($row = db2_fetch_assoc($stmt)){
    $nr = array();
    
    if(trim($row['SPDEST']) != ''){
        $pven = trim($row['SPDEST']);
        $ret_des = $s->get_cliente_des_anag($id_ditta_default, $row['SPCCON'], $pven);
        $row['pven'] = "[{$pven}] {$ret_des['LOC_D']} [{$ret_des['PRO_D']}]";
    }
    
    for($i=1; $i<= 8; $i++){
        $importo = n($row["SPIM0{$i}"], 0);
        $premio = n($row["SPPP0{$i}"], 2);
        if($importo > 0)
            $row["importo_{$i}"] = $importo;
        if($premio > 0)
            $row["premio_{$i}"] = "{$premio} %";
    }
    
    $nr = $row;
  

    $ar[] = $nr;
}

   

echo "<div id='my_content'>";
echo "<div class=header_page>";
echo "<H2>Riepilogo scala premi</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";
echo "<tr class='liv_data'>";
echo "<th>Codice agente</th>
      <th>Descrizione agente</th>
      <th>Codice cliente</th>
      <th>Ragione sociale</th>
      <th>Punto vendita</th>
      <th>Anno</th>";
  for($i=1; $i<= 8; $i++){
      echo "<th>Importo {$i}</th>";
      echo "<th>% Premio</th>";
  }
echo "<th>Immissione</th>";
echo "</tr>";  

foreach ($ar as $kar => $r){
    
    echo "<tr>";
    
    
    $age = get_TA_sys('CUAG', trim($agente));
    $d_age = trim($age['text']);
    echo "<td>".trim($agente)."</td>";
    echo "<td>".$d_age."</td>";
    echo "<td>".$r['GCCDCF']."</td>
          <td style='width:50%'>".$r['GCDCON']."</td>
          <td>".$r['pven']."</td>
		  <td>".$r['SPANNO']."</td>";
    
    for($i=1; $i<= 8; $i++){
        echo "<td style='width:300'>". $r["importo_{$i}"]."</td>";
        echo "<td style='width:300'>". $r["premio_{$i}"]."</td>";
    }
       
    echo "<td>". print_date($r['SPDTGE'])."</td>";
    echo "</tr>";
           
}




?>
</div>
</body>
</html>	

<?php }


