<?php


//**************************************************************************************
// TREE	PRINCIPALE
//**************************************************************************************
function write_main_tree($p){	
?>	
			{
				xtype: 'treepanel',
				cls: '',
		        title: 'Provvigioni ' + <?php echo acs_je($p['f_tipologia']); ?>,
		        <?php echo make_tab_closable(); ?>,
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
		        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Controllo/gestione provvigioni ' + <?php echo acs_je($p['f_tipologia']); ?> + '</b>', '->',
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),  
		        
		        
		        
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					    fields: ['trCls', 'task', 'liv', 'liv_data', 'liv_cod', 'liv_cod_out', 'liv_cod_qtip', 'perc_provvigione', 'imponibile_provvigione', 'importo_provvigione', 'tot_documento', 'tot_anticipo', 'tot_netto_merce', 'tot_imponibile', 'fl_blocco_sblocco_ricalcolo', 'fl_provvisorio', 'fl_raee', 'fl_azzerate', 'fl_con_maturato', 'importo_liquidato', 'docu_sconti_qtip', 'RFFG05', 'liv_menu'],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							actionMethods: {read: 'POST'},
							timeout: 240000,
	                        
	                        reader: {
	                            root: 'children'
	                        },
	                        
							extraParams: {
								f_data_da: <?php echo acs_je($p['f_data_da']); ?>,
								f_data_a:  <?php echo acs_je($p['f_data_a']); ?>,
								f_agenzia:  <?php echo acs_je($p['f_agenzia']); ?>,
								f_agente: <?php echo acs_je($p['f_agente']); ?>,
								f_tipologia: <?php echo acs_je($p['f_tipologia']); ?>,
								f_divisione: <?php echo acs_je($p['f_divisione']); ?>,
								f_fatture_o_ordini: <?php echo acs_je($p['f_fatture_o_ordini']); ?>,
								f_tipo_documento: <?php echo acs_je($p['f_tipo_documento']); ?>,
								f_gruppo: <?php echo acs_je($p['f_gruppo']); ?>,
								f_cliente_cod: <?php echo acs_je($p['f_cliente_cod']); ?>,
								f_rettifiche: <?php echo acs_je($p['f_rettifiche']); ?>
							}
	                	    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		{text: 'Agenzia / Agente / Cliente / Documento', flex: 1, xtype: 'treecolumn', dataIndex: 'task', menuDisabled: true, sortable: false}
					, {text: 'Codice', width: 100, dataIndex: 'liv_cod_out', menuDisabled: true, sortable: false,					

					}		    		
					
					, {text: 'Data', width: 70, dataIndex: 'liv_data', renderer: date_from_AS,  menuDisabled: false, sortable: false}
					, {text: '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=18>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Blocco/sblocco ricalcolo provvigione', dataIndex: 'fl_blocco_sblocco_ricalcolo', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    		if (record.get('fl_blocco_sblocco_ricalcolo')=='Y') return '<img src=<?php echo img_path("icone/48x48/puntina_rossa.png") ?> width=18>';
    			    		if (record.get('fl_raee')=='R') return '<img src=<?php echo img_path("icone/48x48/sub_black_delete.png") ?> width=18>';
    			    		if (record.get('fl_azzerate')=='A') return '<img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?> width=18>';
    			    	}}
    			    	
    			    	
<?php if ($p['f_tipologia'] == 'MATURATO') { ?>

					, {text: '<img src=<?php echo img_path("icone/48x48/button_black_pause.png") ?> width=18>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Provvisorio', dataIndex: 'fl_provvisorio', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    		if (record.get('fl_provvisorio')=='P') return '<img src=<?php echo img_path("icone/48x48/button_black_pause.png") ?> width=18>';
    			    	}}


					, {text: '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>',	width: 30, tdCls: 'tdAction',
    	    	    	tooltip: 'Sospendi/Riattiva liquidazione', dataIndex: 'fl_provvisorio', menuDisabled: true, sortable: false,    	    	     
    			    	renderer: function(value, p, record){
    			    		if (record.get('RFFG05')=='Y') return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
    			    	}}
	
    			    	
<?php } ?>    			    	
    			    	
		    		
					, {header: 'Totale imponibile', tdCls: '', dataIndex: 'tot_imponibile', width: 120, align: 'right',
						renderer: function(value, metaData, record){
							if (record.get('liv') == 'liv_3')
						 		metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('docu_sconti_qtip')) + '"';
						 return floatRenderer2(value);
						}					
					}
					, {header: 'Anticipo', tdCls: '', dataIndex: 'tot_anticipo', width: 120, renderer: floatRenderer2, align: 'right'}
					, {header: 'Netto merce', tdCls: '', dataIndex: 'tot_netto_merce', width: 120, align: 'right',
						renderer: function(value, metaData, record){
							if (record.get('liv') == 'liv_3')
						 		metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('docu_sconti_qtip')) + '"';
						 return floatRenderer2(value);
						}										
					}					
					
					, {header: '% provvigione', tdCls: '', dataIndex: 'perc_provvigione', width: 200, align: 'right',
						renderer: function(value, metaData, record){
						 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
						 return value;
						}					
					}
					, {header: 'Imponibile<br/>provvigione', tdCls: '', dataIndex: 'imponibile_provvigione', width: 90, renderer: floatRenderer2, align: 'right'}
					, {header: 'Importo<br/>provvigione', tdCls: '', dataIndex: 'importo_provvigione', width: 90, align: 'right',					
						renderer: function(value, metaData, record){
						
						<?php if ($p['f_tipologia'] == 'ACCREDITATO') { ?>
					    	if (record.get('fl_con_maturato') == 'M'){
				    			 metaData.tdCls += ' con_maturato';			    	
					    	}
					    	if (record.get('fl_con_maturato') == 'P'){
				    			 metaData.tdCls += ' con_maturato_parziale';			    	
					    	}					    	
					    <?php } ?>	
					    						
						 return floatRenderer2(value);
						}					
					}
					
<?php if ($p['f_tipologia'] == 'MATURATO') { ?>					
					, {header: 'Importo<br/>Liquidato', tdCls: '', dataIndex: 'importo_liquidato', width: 90, align: 'right',
							renderer: function(value, metaData, record){
								if (parseFloat(record.get('importo_liquidato')) != 0) {
							    	if (parseFloat(record.get('importo_liquidato')) == parseFloat(record.get('importo_provvigione'))) 
						    			 metaData.tdCls += ' con_maturato';
						    		else
						    			 metaData.tdCls += ' con_maturato_parziale';
							    	
						    	}
						        return floatRenderer2(value);
							} 
					}
<?php } ?>				
		    			   	
				],
				enableSort: false, // disable sorting
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
			          
			          
					  celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  }
					  },
			            
			          
			          
			          itemcontextmenu : function(grid, rec, node, index, event) {
			                event.stopEvent();			                         
							var voci_menu = [];
			                          

							if (rec.get('liv') == 'liv_1'){ //livello agente
										  
							      voci_menu.push({
						      		text: 'Inserimento importi positivi/negativi per agente',
						    		iconCls : 'icon-listino',      		
						    		handler: function() {
						    			my_listeners_ins_imp_age = {
											afterUpdateRecord: function(from_win){
												from_win.close();	
												grid.store.treeStore.reload();
											},
											afterUpdateRecordRemain: function(from_win){	
												acs_show_msg_info('Inserimento completato');
											}						    			
						    			};
										acs_show_win_std('Inserimento importo positivo/negativo', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_nuova_ins_imp_age', 
											{
								  				rec_id: rec.get('id'),
								  				agente: rec.get('liv_cod'),
								  				tipologia: <?php echo j($p['f_tipologia']); ?>,
								  				imponibile_provv: rec.get('tot_imponibile') 								  				
								  			}, 450, 350, my_listeners_ins_imp_age, 'icon-info_blue-16');
						    		}
								  });
							}
							
							if(rec.get('liv_menu') == 'liv_0_0'){
							
						   	 voci_menu.push({
						      		text: 'Blocca/sblocca ricalcolo provvigioni',
						    		iconCls : 'iconConf',      		
						    		handler: function() {
						    		
						    		   list_selected_id = [];
						    		
						  			  rec.eachChild(function(rec_1){
                						  rec_1.eachChild(function(rec_2){
                							 rec_2.eachChild(function(rec_3){
                						       	rec_3.eachChild(function(rec_4){
                						        	list_selected_id.push({tfdocu: rec_4.get('liv_cod'), local_flag: rec_4.get('fl_blocco_sblocco_ricalcolo')});
                						});
                						});
                						});
                						});
                						
                						console.log(list_selected_id);
						    	
						    		
								        Ext.Ajax.request({
								            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_blocca_sblocca_ricalcolo',
									        method     : 'POST',
									        waitMsg    : 'Data loading',								            
								            jsonData: {list_selected_id: list_selected_id},
								            success: function ( result, request) {
								                var jsonData = Ext.decode(result.responseText);
								                grid.getStore().treeStore.load();
								                //rec.set('fl_blocco_sblocco_ricalcolo', jsonData.remote_flag);												
								            },
								            failure: function ( result, request) {
								            }
								        });						    			
						    		}
								  });
							
							
							}
							
							
							if(rec.get('liv') == 'liv_0_2'){
							
							
							 voci_menu.push({
						      		text: 'Blocca/sblocca ricalcolo provvigioni',
						    		iconCls : 'iconConf',      		
						    		handler: function() {
						    		
						    		   list_selected_id = [];
						    		
						  			  rec.eachChild(function(rec_1){
                						  rec_1.eachChild(function(rec_2){
                							 rec_2.eachChild(function(rec_3){
                						       	list_selected_id.push({tfdocu: rec_3.get('liv_cod'), local_flag: rec_3.get('fl_blocco_sblocco_ricalcolo')});
                						});
                						});
                						});
                						
                						console.log(list_selected_id);
						    	
						    		
								        Ext.Ajax.request({
								            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_blocca_sblocca_ricalcolo',
									        method     : 'POST',
									        waitMsg    : 'Data loading',								            
								            jsonData: {list_selected_id: list_selected_id},
								            success: function ( result, request) {
								                var jsonData = Ext.decode(result.responseText);
								                grid.getStore().treeStore.load();
								                //rec.set('fl_blocco_sblocco_ricalcolo', jsonData.remote_flag);												
								            },
								            failure: function ( result, request) {
								            }
								        });						    			
						    		}
								  });
							
							
							}
							
							/*if(rec.get('liv') == 'liv_1' ||
							   	rec.get('liv') == 'liv_2' ){
							   	
							   	 voci_menu.push({
						      		text: 'Blocca/sblocca ricalcolo provvigioni',
						    		iconCls : 'iconConf',      		
						    		handler: function() {
								        Ext.Ajax.request({
									        method     : 'POST',
									        waitMsg    : 'Data loading',								            
								            jsonData: {selected_id: rec.get('liv_cod'), local_flag: rec.get('fl_blocco_sblocco_ricalcolo')},
								            success: function ( result, request) {
								                var jsonData = Ext.decode(result.responseText);
								                rec.set('fl_blocco_sblocco_ricalcolo', jsonData.remote_flag);												
								            },
								            failure: function ( result, request) {
								            }
								        });						    			
						    		}
								  });
							   	
							   	}*/
							
							if (rec.get('liv') == 'liv_3' ){ //liv_3 ->livello fattura
										  
							      voci_menu.push({
						      		text: 'Blocca/sblocca ricalcolo provvigioni',
						    		iconCls : 'iconConf',      		
						    		handler: function() {
								        Ext.Ajax.request({
								            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_blocca_sblocca_ricalcolo',
									        method     : 'POST',
									        waitMsg    : 'Data loading',								            
								            jsonData: {selected_id: rec.get('liv_cod'), local_flag: rec.get('fl_blocco_sblocco_ricalcolo')},
								            success: function ( result, request) {
								                var jsonData = Ext.decode(result.responseText);
								                rec.set('fl_blocco_sblocco_ricalcolo', jsonData.remote_flag);												
								            },
								            failure: function ( result, request) {
								            }
								        });						    			
						    		}
								  });		
								  
								  
						      //Visualizzazione righe ordine
						  		  voci_menu.push({
						         		text: 'Visualizza righe',
						        		iconCls : 'icon-folder_search-16',          		
						        		handler: function() {
						        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {k_ordine: rec.get('liv_cod'), mod_prv : rec.get('fl_blocco_sblocco_ricalcolo')}, 900, 450, null, 'icon-folder_search-16');          		
						        		}
						    		});
						    		
<?php  if ($p['f_tipologia'] == 'ACCREDITATO'){ ?>
						  	  //assegna nuova provvigione
								my_listeners = {
		        					afterUpdateRecord: function(from_win){	
		        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
		        						rec.store.treeStore.load({node: rec})		        						
		        						from_win.close();
						        		}
				    				};							
						  	  
							      voci_menu.push({
						      		text: 'Inserisci nuova provvigione',
						    		iconCls : 'icon-briefcase-16',      		
						    		handler: function() {
										acs_show_win_std('Inserimento nuova provvigione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_nuova_provvigione', 
											{
								  				rec_id: rec.get('id'),
								  				docu: rec.get('liv_cod'),
								  				tipologia: <?php echo j($p['f_tipologia']); ?>								  				
								  			}, 400, 300, my_listeners, 'icon-info_blue-16');
						    		}
								  });
								  
								  
								  
						  	  //avanza in maturato						  	  
							      voci_menu.push({
						      		text: 'Avanza in maturato',
						    		iconCls : 'icon-sub_green_next-16',      		
						    		handler: function() {
										acs_show_win_std('Avanza in maturato', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_avanza_in_maturato', 
											{
								  				rec_id: rec.get('id'),
								  				docu: rec.get('liv_cod'),
								  				tipologia: <?php echo j($p['f_tipologia']); ?>								  				
								  			}, 400, 300, {
					        					afterUpdateRecord: function(from_win){	
					        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
					        						rec.store.treeStore.load({node: rec})		        						
					        						from_win.close();
									        		}
							    				}
							    			, 'icon-info_blue-16');
						    		}
								  });
							      
								  
								  
								  
<?php } ?>								  
							      
								  								  										  
							} //liv fattura
							
							
							
							
							if (rec.get('liv') == 'liv_4'){ //livello provvigione		  
							
								my_listeners = {
		        					afterUpdateRecord: function(from_win){	
		        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
		        						
		        						form = from_win.down('form').getForm();
		        						
		        						rec.set('perc_provvigione', form.findField("f_perc_provv").getValue());
		        						rec.set('importo_provvigione', form.findField("f_importo_provv").getValue());
		        						rec.set('imponibile_provvigione', form.findField("f_imponibile_provv").getValue());
		        						from_win.close();
						        		},
						        		
		        					afterAzzeraRecord: function(from_win){	
		        						//dopo che ha chiuso la maschera del carico aggiorno la pagina di composizione
		        						
		        						form = from_win.down('form').getForm();
		        						
		        						rec.set('perc_provvigione', 0);
		        						rec.set('importo_provvigione', 0);
		        						from_win.close();
						        		}						        		
				    				};							
							
							if(rec.get('fl_blocco_sblocco_ricalcolo') != 'Y'){
							      voci_menu.push({
						      		text: 'Modifica provvigione',
						    		iconCls : 'icon-briefcase-16',      		
						    		handler: function() {
										acs_show_win_std('Modifica provvigione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_modifica_provvigione', 
											{
								  				rec_id: rec.get('id'),
								  				docu: rec.parentNode.get('liv_cod'),
								  				riga: rec.get('liv_cod'),
								  				tipologia: <?php echo j($p['f_tipologia']); ?>
								  			}, 400, 300, my_listeners, 'icon-info_blue-16');
						    		}
								  });
								}  
								  
								  
								<?php if ($p['f_tipologia'] == 'MATURATO') { ?>
							      voci_menu.push({
						      		text: 'Sospendi/Riattiva liquidazione',
						    		iconCls : 'icon-button_blue_pause-16',      		
						    		handler: function() {
						    		
									    Ext.Msg.confirm('Richiesta conferma', 'Confermi operazione?', function(btn, text){									    
									      if (btn == 'yes'){						    		
						    		
										        Ext.Ajax.request({
										            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_toggle_RFFG05',
											        method     : 'POST',
											        waitMsg    : 'Data loading',								            
										            jsonData: {selected_id: rec.get('id')},
										            success: function ( result, request) {
										                var jsonData = Ext.decode(result.responseText);
										                rec.set('RFFG05', jsonData.new_RFFG05);												
										            },
										            failure: function ( result, request) {
										            }
										        });
										  }
										});	
						    		

						    		}
								  });
								<?php } ?>								  
								  
								  
								  
								  
							}
							
							
							
							
							
					      var menu = new Ext.menu.Menu({
					            items: voci_menu
						}).showAt(event.xy);
			                          
			          }
			          
			          
			            	        
			        } //listeners
				
	
			, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');

			           if (record.get('trCls')=='grassetto')
					        return ret + ' grassetto';
			           
			           if (record.get('trCls')=='G')
					        return ret + ' colora_riga_grigio';
			           if (record.get('trCls')=='Y')
					        return ret + ' colora_riga_giallo';					        
			           		        	
			           return ret;																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree













function write_dettaglio_riferimento($p){
	?>
        {
            xtype: 'grid', itemId: 'dettaglio_per_riferimento',
            flex: 1,
			
		    		features: new Ext.create('Ext.grid.feature.Grouping',{
        							groupHeaderTpl: 'Intestatario: {[values.rows[0].data.cod_web]} - {[values.rows[0].data.des_web]}',
        							hideGroupedHeader: true
    						}),			
			
			store: {
					xtype: 'store',
					groupField: 'cod_web',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_per_riferimento',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
										f_ditta: <?php echo acs_je($p['f_ditta']); ?>,
										dataStart:  <?php echo acs_je($p['dataStart']); ?>,
										dataEnd:  <?php echo acs_je($p['dataEnd']); ?>,
										raggruppamento: <?php echo acs_je($p['raggruppamento']); ?>,
										categoria: <?php echo acs_je($p['categoria']); ?>  
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['data', 'riferimento', 'importo', 'cod_web', 'des_web']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Data',
	                dataIndex: 'data', renderer: date_from_AS,
	                width: 60 
	            }, {
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                flex: 10
	            }, {
	                header   : 'Importo',
	                dataIndex: 'importo', renderer: floatRenderer2, align: 'right',
	                flex: 10
	            }   
	         ],																					

	         
//itemclick: function(view,rec,item,index,eventObj) {	         
	         
	        listeners: {
	        
					  itemclick: {								
						  fn: function(iView,rec,item,index,eventObj){
						  	grid_dettagli_riga = iView.up('panel').up('panel').getComponent('dettaglio_per_riga');
						  	grid_dettagli_riga.store.proxy.extraParams.data = rec.get('data');
						  	grid_dettagli_riga.store.proxy.extraParams.riferimento = rec.get('riferimento');
						  	grid_dettagli_riga.store.load();
						  }
					  }	  	        	        

			}			
			
				  
	            
        }   	
	
	
	<?php
} //write_dettaglio_riferimento






function write_dettaglio_righe($p){
	?>
        {
            xtype: 'grid', itemId: 'dettaglio_per_riga',
			flex: 1,
			store: {
					xtype: 'store',
					autoLoad: false,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_per_riga',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
										f_ditta: <?php echo acs_je($p['f_ditta']); ?>,
										dataStart:  <?php echo acs_je($p['dataStart']); ?>,
										dataEnd:  <?php echo acs_je($p['dataEnd']); ?>,
										raggruppamento: <?php echo acs_je($p['raggruppamento']); ?>,
										categoria: <?php echo acs_je($p['categoria']); ?>,
										data: null,
										riferimento: null 
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['riga', 'cod_articolo', 'descr_articolo', 'riferimento', 'importo']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width: 60 
	            }, {
	                header   : 'Articolo',
	                dataIndex: 'cod_articolo',
	                width: 80
	            }, {
	                header   : 'Descrizione',
	                dataIndex: 'descr_articolo',
	                flex: 10
	            }, {
	                header   : 'Importo',
	                dataIndex: 'importo', renderer: floatRenderer2, align: 'right',
	                width: 90
	            }   
	         ],																					

	         
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
						  }
					  }	  
			}			
			
				  
	            
        }   	
	
	
	<?php
} //write_dettaglio_righe







// DELETE *****************************************************************

function write_main_tree_old($p){
	
 //calcolo intervalli (per ora parto dal mese in corso, con intervalli mensili)
 $mm 	= date('m');
 $yyyy	= date('Y');
 $start_day = mktime(0, 0, 0, $mm, 1, $yyyy); //il primo giorno del mese attuale
 $ar_mesi = array();
 for ($i=0; $i<12; $i++){
 	$d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
 	$d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 1, $yyyy)); 	 	
 	$d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
 	 
 	$ar_mesi[$d1.$d2] = array('d1' => $d1, 'd2' => $d2);
 }
	
?>
{
	xtype: 'grid',
	loadMask: true,
	title: 'FlussiCassa',
		<?php echo make_tab_closable(); ?>,
	
        tbar: new Ext.Toolbar({
            items:['<b>???', '->',
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
         ]            
        }),  
		
	
		store: {
			xtype: 'store',
				
			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
				f_ditta: <?php echo acs_je($p->f_ditta); ?>
			}
			
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				, doRequest: personalizza_extraParams_to_jsonData						
			
			},
				
			fields: ['voce', 'tipo_voce', 'codice_voce'
			 <?php
			  foreach($ar_mesi as $km => $mesi){
				echo ", 'd_{$km}'";				
			  }
			 ?>
			]
						
		}, //store

		multiSelect: false,
		columnLines: false, //righe separazione cell in verticale
		columns: [
			{header: 'Codice', dataIndex: 'codice_voce', width: 60},		
			{header: 'Voce', dataIndex: 'voce', flex: 1}
			
			<?php
			//costruisco le 12 colonne dei mese (dal mese attuale in avanti)
			foreach($ar_mesi as $km => $mesi){
				echo ", {header: '" . print_date($mesi['d1']) . "<br>" . print_date($mesi['d2']) . "', dataIndex: 'd_{$km}', width: 80}";				
			}
			
			?>
			

					
		],
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
				    //in col_name ho in pratico il codice ditta
				    acs_show_panel_std('acs_panel_abi_azienda.php?fn=open_tab', Ext.id(), {
				    	codice_ditta: col_name,
				    	nome_analisi_1: iView.store.proxy.extraParams.nome_analisi_1,
				    	nome_analisi_2: iView.store.proxy.extraParams.nome_analisi_2
				    });

				}
			}
			
		}
		
		
		, viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {		        	
		           return record.get('tipo_voce');																
		         }   
		    }												    
		
		
		 
	} 
<?php } //write main grid


?>