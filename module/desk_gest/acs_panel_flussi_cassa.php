<?php
require_once("../../config.inc.php");
require_once("acs_flussi_cassa_include.php");

$main_module = new DeskGest();

function sql_where_by_request($m_params){
	$sql_where = "";
	$sql_where .= " AND FC.FLDITT = " . sql_t($m_params->f_ditta);
	
	$sql_where .= sql_where_by_combo_value('FC.FLCLF0', $m_params->open_request->cliente);
	if (strlen($m_params->open_request->f_commessa_da) > 0)
	    $sql_where .= " AND FC.COD_COM >= " . sql_t($m_params->open_request->f_commessa_da);
    if (strlen($m_params->open_request->f_commessa_a) > 0)
        $sql_where .= " AND FC.COD_COM <= " . sql_t($m_params->open_request->f_commessa_a);
	
	return $sql_where;		
}



//recupero elenco ditte per scelta in menu iniziale
function get_ar_ditte(){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT FLUDIT, FLUDED FROM {$cfg_mod_Gest['flussi_cassa']['file_conti']} GROUP BY FLUDIT, FLUDED ORDER BY FLUDED";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['FLUDIT']), "text" => $r['FLUDED']);
	}
	return $ar;
}






// ******************************************************************************************
// GRID DATI: RAGGRUPPAMENTO PER DATA/RIFERIMENTO (passando range data, ditta, categoria)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_per_riferimento'){
	global $backend_ERP;	
	$m_params = acs_m_params_json_decode();
	
	$sql_where = sql_where_by_request($m_params);

	switch ($backend_ERP){
	
		case 'GL':	
		$sql = "SELECT RIFERIMENTO, DT_RIFERIMENTO, SUM(IMP_RIF) AS IMPORTO, COD_WEB, DES_WEB 
			FROM {$cfg_mod_Gest['flussi_cassa']['file_movimenti']} FC
			WHERE FLDITT=? AND CATEGORIA=?
            {$sql_where}
			AND DT_RIFERIMENTO >= ? AND DT_RIFERIMENTO <= ?
			GROUP BY RIFERIMENTO, DT_RIFERIMENTO, COD_WEB, DES_WEB
            ORDER BY DT_RIFERIMENTO
			";
		break;
		
		default: //SV2
		$sql = "SELECT RIFERIMENTO, DT_RIFERIMENTO, SUM(FLIMN0) AS IMPORTO, COD_WEB, DES_WEB,
			TIPO_DOC, STATO_DOC, FLDT10, FLIMN0
			FROM {$cfg_mod_Gest['flussi_cassa']['file_movimenti']} FC
			WHERE FLDITT=? AND CATEGORIA=?
            {$sql_where}
			AND DT_RIFERIMENTO >= ? AND DT_RIFERIMENTO <= ?
			GROUP BY RIFERIMENTO, DT_RIFERIMENTO, COD_WEB, DES_WEB,TIPO_DOC, STATO_DOC, FLDT10, FLIMN0
            ORDER BY DT_RIFERIMENTO
			";			
	}	
 
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->f_ditta, $m_params->categoria, $m_params->dataStart, $m_params->dataEnd));
	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		
		
		switch ($backend_ERP){
		
			case 'GL':		
				$n = array();
				$n['data'] 			 = $r['DT_RIFERIMENTO'];
				$n['data_out'] 		 = $r['DT_RIFERIMENTO'];
				$n['riferimento']	 = acs_u8e($r['RIFERIMENTO']);
				$n['riferimento_out']= acs_u8e($r['RIFERIMENTO']);
				$n['importo']		 = $r['IMPORTO'];
				$n['cod_web']		 = acs_u8e(trim($r['COD_WEB']));
				$n['des_web']		 = acs_u8e(trim($r['DES_WEB']));
				$ar[] = $n;
				break;

			default: //SV2
				
				$docu_exp = explode("_", $r['RIFERIMENTO']);
				
				$n = array();
				$n['data'] 			 	= $r['DT_RIFERIMENTO'];
				$n['data_out'] 				= $r['FLDT10'];
				$n['riferimento']	 	= acs_u8e($r['RIFERIMENTO']);				
				$n['riferimento_out']	= implode("_", array($docu_exp[2], $docu_exp[3], $docu_exp[4], trim($r['TIPO_DOC']))) . " [" . trim($r['STATO_DOC']) . "]";
				$n['importo']		= $r['IMPORTO'];
				$n['cod_web']		= acs_u8e(trim($r['COD_WEB']));
				$n['des_web']		= acs_u8e(trim($r['DES_WEB']));
				$ar[] = $n;
				break;				
		}
	}
	
	echo acs_je($ar);
  exit;
}



// ******************************************************************************************
// GRID DATI: DETTAGLIO RIGHE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_per_riga'){
	global $backend_ERP;
	$m_params = acs_m_params_json_decode();
	
	switch ($backend_ERP){	
		case 'GL':
			$sql = "SELECT *
					FROM {$cfg_mod_Gest['flussi_cassa']['file_movimenti']}
					WHERE FLDITT=? AND CATEGORIA=?
					AND DT_RIFERIMENTO = ? AND RIFERIMENTO = ? 
			";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($m_params->f_ditta, $m_params->categoria, $m_params->data, $m_params->riferimento));
				
		default: //SV2
			$sql = "SELECT *
					FROM {$cfg_mod_Gest['flussi_cassa']['file_movimenti']}
					WHERE FLDITT=? AND CATEGORIA=?
					AND RIFERIMENTO = ?
					ORDER BY DT_RIFERIMENTO
					";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($m_params->f_ditta, $m_params->categoria, $m_params->riferimento));
	}


	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		switch ($backend_ERP){
			case 'GL':		
				$n = array();
				$n['riga'] 			= $r['RIGA_DOC'];
				$n['cod_articolo']	= acs_u8e(trim($r['COD_ARTICOLO']));
				$n['descr_articolo'] = acs_u8e(trim($r['DESCR_ART']));
				$n['importo']		= $r['IMP_RIF'];
				$ar[] = $n;
			default: //SV2
				$n = array();
				$n['riga'] 			= print_date($r['DT_RIFERIMENTO']);
				$n['cod_articolo']	= acs_u8e(trim($r['COD_ARTICOLO']));
				$n['descr_articolo'] = acs_u8e(trim($r['DESCR_ART']));
				$n['importo']		= $r['IMP_RIF'];
				
				if ($m_params->data != $r['DT_RIFERIMENTO']){
					//in grigio le date che non sono del mese selezionato
					$n['segnala_riga'] .= " colora_riga_grigio ";
				}
				
				$ar[] = $n;				
		}
	}

	echo acs_je($ar);
	exit;
}



// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************

function sum_row_parziale(&$ar_r, $r, $anno_mese_start, $anno_mese_end, $importo_escluso_E = false){
    
    if ($importo_escluso_E) {
        $imp = (float)$r['IMPORTO_ESCLUSO_E'];
    } else {
        $imp = (float)$r['IMPORTO'];
    }
    
    if ($r['ANNO_MESE'] < $anno_mese_start)
        $ar_r['scaduto'] += $imp;
    elseif ($r['ANNO_MESE'] > $anno_mese_end)
        $ar_r['oltre'] += $imp;    
    else {
        
        //gestisco mese o decade
        
        switch ($r['ID_PERIODO']) {
            case 0: //mese intero
                $d_inizio = $r['ANNO_MESE'] . '01';
                $d_fine   = date('Ymd', strtotime($d_inizio . " +1 month, -1 day"));            
                break;
            
            case 1: //prima decade
                $d_inizio = $r['ANNO_MESE'] . '01';
                $d_fine   = $r['ANNO_MESE'] . '10';                
                break;
            case 2: //seconda decade
                $d_inizio = $r['ANNO_MESE'] . '11';
                $d_fine   = $r['ANNO_MESE'] . '20';
                break;
            case 3: //terza decade
                $d_inizio = $r['ANNO_MESE'] . '21';
                $d_fine   = date('Ymd', strtotime($r['ANNO_MESE'] . '01' . " +1 month, -1 day"));
                break;
        }
        
 

        $ar_r[implode('_', array('d', $d_inizio, $d_fine))] += $imp;
    }
        
}


// ******************************************************************************************
// get_json_data
// ******************************************************************************************

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $m_params = acs_m_params_json_decode();
    
    $ar=crea_ar_tree_flussi_cassa($_REQUEST['node'], $m_params);
   
    $ret = array();
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}


// ******************************************************************************************
// SCELTA DATA BILANCIO (CEAL....)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_parameters'){
?>
	

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "CEAL_SELECT");  ?>
					{
			            text: 'Report',
			            iconCls: 'icon-print-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_flussi_cassa_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }			
			            }
			         },
					{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							if(form.isValid()){			            	
								acs_show_panel_std('acs_panel_flussi_cassa.php?fn=open_tab', 'panel-flussi_cassa', form.getValues());
								this.up('window').close();
							}
			            }
			         }],   		            
		            
		            items: [{
							name: 'f_ditta',
							xtype: 'combo',
							multiSelect: false,
							fieldLabel: 'Ditta',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: false,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(get_ar_ditte(), ""); ?>	
								    ] 
							}						 
						}, {
                                xtype: 'combo',
                    			name: 'cliente',
                    			fieldLabel: 'Cliente',
                    			minChars: 1,
                    			allowBlank : true,
                    			anchor: '-15',
						    	margin: "20 10 10 10",	
                                store: {
                                	pageSize: 1000,
                                	proxy: {
                    		            type: 'ajax',
                    		            url : <?php echo acs_je('../desk_vend/acs_get_select_json.php?select=search_cli'); ?>,
                    		            reader: {
                    		                type: 'json',
                    		                root: 'root',
                    		                totalProperty: 'totalCount'
                    		            },
                    		              actionMethods: { read: 'POST'},
                    		              doRequest: personalizza_extraParams_to_jsonData
                    		        }, 
                    		        fields: ['cod', 'descr', 'out_loc'],		             	
                                },
                                valueField: 'cod',                        
                                displayField: 'descr',
                                typeAhead: false,
                                hideTrigger: true,
                                anchor: '-15',
                                listConfig: {
                                    loadingText: 'Searching...',
                                    emptyText: 'Nessun cliente trovato',
                                   //Custom rendering template for each item
                                    getInnerTpl: function() {
                                        return '<div class="search-item">' +
                                            '<h3><span>{descr}</span></h3>' +
                                            '[{cod}] {out_loc}' + 
                                        '</div>';
                                    }                
                                    
                                },
                                pageSize: 1000
                            },
                            
                            {
                            	xtype: 'fieldcontainer',
                            	flex: 1,
                            	layout: 'hbox',
                            	items: [
                            		{
        						        xtype: 'textfield', 
        						        fieldLabel: 'Commessa da',
        						        name: 'f_commessa_da',
        						        anchor: '-15', margin: "20 10 10 10"	
    								},
    								{
        						        xtype: 'textfield', 
        						        fieldLabel: 'A', labelAlign: 'right',
        						        name: 'f_commessa_a',
        						        anchor: '-15', margin: "20 10 10 10"	
    								}
                            		
                            	]
                            }
                            
                            
                            , {
    							name: 'f_raggruppa_per',
    							xtype: 'combo',
    							multiSelect: false,
    							fieldLabel: 'Raggruppa per',
    							displayField: 'text',
    							valueField: 'id',
    							emptyText: '- seleziona -',
    							forceSelection: true,
    						   	allowBlank: true, 		
    						    anchor: '-15',
    						    margin: "20 10 10 10",						   													
    							store: {
    								autoLoad: true,
    								editable: false,
    								autoDestroy: true,	 
    							    fields: [{name:'id'}, {name:'text'}],
    							    data: [								    
    								     {id: '',  text: 'Mese'},
    								     {id: 'DECADE', text: 'Decade'}	
    								] 
    							}						 
    						}												
						]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
	
	
	
	
<?php
	exit;
}



// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){	
	$m_params = acs_m_params_json_decode();	
?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" => $m_params->f_ditta,
        "open_request" => $m_params
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// SCADUTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_scaduto'){
	$m_params = acs_m_params_json_decode();
	
	
	$m_params->dataEnd = date('Ymd', strtotime($m_params->dataStart . " -1 days"));
	
	//calcolo nuove data, in base a visualizzazione per mese o decade ....
	if (isset($m_params->open_request) && $m_params->open_request->f_raggruppa_per == 'DECADE'){
	    $m_params->dataStart 	 = date('Ymd', strtotime($m_params->dataStart . " -4 months"));
	} else {
	    $m_params->dataStart 	 = date('Ymd', strtotime($m_params->dataStart . " -1 years"));
	}
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "scaduto",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
        "open_request" => $m_params->open_request
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// OLTRE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_oltre'){
	$m_params = acs_m_params_json_decode();
	
	$m_params->dataStart = date('Ymd', strtotime($m_params->dataEnd . " +1 days"));
	
	//calcolo nuove data, in base a visualizzazione per mese o decade ....
	if (isset($m_params->open_request) && $m_params->open_request->f_raggruppa_per == 'DECADE'){
	    $m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " +4 months"));
	} else {
	    $m_params->dataEnd 	 = date('Ymd', strtotime($m_params->dataEnd . " +1 years"));
	}
	
	
	
	
	?>
{
 success:true, 
 items: [
  <?php write_main_tree(array(
  		"f_ditta" 	=> $m_params->f_ditta,
  		"open_type" => "oltre",
  		"dataStart"	=> $m_params->dataStart,
  		"dataEnd"	=> $m_params->dataEnd,
        "open_request" => $m_params->open_request
  )); ?> 
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// DETTAGLIO PER RIFERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_riferimento'){
	$m_params = acs_m_params_json_decode();
	
	//recuper data iniziale e data finale (dalla cella su cui ho fatto dbclick)
	$col_name_ar = explode('_', $m_params->col_name);
	$dataStart = $col_name_ar[1];
	$dataEnd 	 = $col_name_ar[2];

	//da record_id recupero raggruppamento e categoria
	$record_id_ar = explode("|", $m_params->record_id);
	$raggruppamento = $record_id_ar[0];
	$categoria		= $record_id_ar[1];
	
	?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'hbox',
	    align: 'stretch',
	    pack : 'start',
	},
	items: [ 
		  <?php write_dettaglio_riferimento(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria,
		        "open_request" => $m_params->form_ep->open_request
		  )); ?>,
		  <?php write_dettaglio_righe(array(
		  		"f_ditta" 	=> $m_params->form_ep->f_ditta,
		  		"dataStart"	=> $dataStart,
		  		"dataEnd"	=> $dataEnd,
		  		"raggruppamento" => $raggruppamento,
		  		"categoria"		 => $categoria,
		        "open_request" => $m_params->form_ep->open_request
		  )); ?>		  
	]
  }	   
 ]
}
<?php exit; } ?>

