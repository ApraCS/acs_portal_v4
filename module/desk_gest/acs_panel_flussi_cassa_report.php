<?php

require_once "../../config.inc.php";
require_once("acs_flussi_cassa_include.php");
require_once("acs_panel_flussi_cassa.php");

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>


<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv3 td{font-weight: bold; font-size: 0.9em;}   
   tr.liv_data th{background-color: #333333; color: white;}
   tr.fattura_vendita td{font-style: italic}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
   
   div.with_todo_tooltip{text-align: right;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>
  
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<?php
//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

//calcolo intervalli (per ora parto dal mese in corso, con intervalli mensili)
if (isset($p['dataStart'])){
    $mm 	= substr($p['dataStart'], 4, 2);
    $yyyy 	= substr($p['dataStart'], 0, 4);
} else {
    //da mese/anno attuale
    $mm 	= date('m');
    $yyyy	= date('Y');
}

$start_day = mktime(0, 0, 0, $mm, 1, $yyyy); //il primo giorno del mese attuale
$ar_mesi = array();

$oldLocale = setlocale(LC_TIME, 'it_IT');


/*for ($i=0; $i<12; $i++){
    $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
    $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 1, $yyyy));
    $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
    
    $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
        'd1' => $d1,
        'd2' => $d2,
        'column_label' => ucfirst(strftime("%b %y", strtotime($d1)))
    );
}*/




if (isset($form_values->f_raggruppa_per) && $form_values->f_raggruppa_per == 'DECADE'){
    //RAGGRUPPO PER DECADE (4mesi x 3 decadi)
    for ($i=0; $i<4; $i++){
        
        //Prima decade
        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i,  1, $yyyy));
        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, 10, $yyyy));
        
        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
            'd1' => $d1,
            'd2' => $d2,
            'column_label' => ucfirst(strftime("%b %y-1aD", strtotime($d1)))
        );
        
        //Seconda decade
        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 11, $yyyy));
        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, 20, $yyyy));
        
        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
            'd1' => $d1,
            'd2' => $d2,
            'column_label' => ucfirst(strftime("%b %y-2aD", strtotime($d1)))
        );
        
        //Terza decade
        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 21, $yyyy));
        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
        
        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
            'd1' => $d1,
            'd2' => $d2,
            'column_label' => ucfirst(strftime("%b %y-3aD", strtotime($d1)))
        );
        
        
        
        
    }
} else {
    //RAGGRUPPO PER MESE
    for ($i=0; $i<12; $i++){
        $d1d = mktime(0, 0, 0, $mm + $i, 1, $yyyy);
        $d1 = date('Ymd', mktime(0, 0, 0, $mm + $i, 1, $yyyy));
        $d2 = date('Ymd', mktime(0, 0, 0, $mm + $i, cal_days_in_month(CAL_GREGORIAN, date('m', $d1d), date('Y', $d1d)), $yyyy));
        
        $ar_mesi[implode('_', array('d', $d1, $d2))] = array(
            'd1' => $d1,
            'd2' => $d2,
            'column_label' => ucfirst(strftime("%b %y", strtotime($d1)))
        );
    }
}



setlocale(LC_TIME, $oldLocale);

$dataStart = date('Ymd', $start_day);
$dataEnd =  $d2;

$form_values->dataStart = $dataStart;
$form_values->dataEnd = $dataEnd;

$ar=crea_ar_tree_flussi_cassa('', $form_values);


$sql = "SELECT FLUDED FROM {$cfg_mod_Gest['flussi_cassa']['file_conti']} WHERE FLUDIT = ?";
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt, array($form_values->f_ditta));
$r = db2_fetch_assoc($stmt);
$ditta = "[".$form_values->f_ditta."] ".trim($r['FLUDED']);


echo "<div id='my_content'>";

echo "
  		<div class=header_page>
			<H2>Analisi flussi di cassa - Ditta {$ditta}</H2>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>";

echo "<table class=int1>";
echo "<tr><th>Voce</th>
		<th>Scaduto</th>";


foreach($ar_mesi as $km => $mesi){
    echo "<th>{$mesi['column_label']}</th>";
}	

echo "<th>Oltre</th>";
echo "</tr>";

$count = 0;

foreach($ar as $kar => $r){
    $count++;
    if($count == 1) {
        $c_liv = "liv_totale";
    } elseif ($count == 2) { //liv prog
        $c_liv = "liv2";
    }
    else {
        $c_liv = "liv1";
    }
	
    echo "<tr class = {$c_liv} ><td>".$r['task']. "</td>
	          <td class=number>".n($r['scaduto'], 0)."</td>";	
	
	foreach($ar_mesi as $km => $mesi){
	    echo "<td class=number>".n($r["{$km}"], 0)."</td>";
	}	

	echo "<td>".n($r['oltre'], 0)."</td></tr>";

	if($count > 2){
		foreach($r['children'] as $kar1 => $r1){
			
		    echo "<tr><td>".$r1['task']. "</td>
   		          <td class=number>".n($r1['scaduto'], 0)."</td>";
		    
		    foreach($ar_mesi as $km => $mesi){
		        echo "<td class=number>".n($r1["{$km}"], 0)."</td>";
		    }
		    
		    echo "<td>".n($r1['oltre'],0)."</td></tr>";
        }
    }

}

echo "</table>";


?>

</div>
</body>
</html>

