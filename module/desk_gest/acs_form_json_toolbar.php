<?php
require_once "../../config.inc.php";


?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
			cls: 'acs_toolbar',
			tbar: [
		   {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Tabelle',
			            items: [
			              {
			                text: '<br>Nazioni',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_BNAZ.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'BNAZ'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },    {
			                text: '<br>Pagamenti',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_CUCP.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'CUCP'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			               {
			                text: 'Gruppo<br>doganale',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_BGDN.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'GBDN'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			               {
			                text: '<br>Concorrenti',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_BCES.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'BCES'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            }, {
			                text: '<br>Banca',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_CUBA.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'CUBA'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },{
			                text: 'Sconti<br>per Variante/Agente',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
			                 
			                 acs_show_panel_std('acs_anag_cli_XSPM.php?fn=open_tab', 'panel_gestione_schede_age',
    		    					{type : 'A', cliente : ''});
			                 		panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			            {
			                text: 'Sconti<br>per Variante/Riga',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								 acs_show_panel_std('acs_sconti_variante_riga.php?fn=open_tab', 'panel_gestione_schede',
    		    					{type : 'T', cliente : ''});
    		    					panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			             {
			                text: 'Sconti<br>pagamento',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								 acs_show_panel_std('acs_sconti_variante_riga.php?fn=open_tab', 'panel_gestione_schede',
    		    					{type : 'G', cliente : ''});
    		    					panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			            {
			                text: 'Proposta<br>pagamenti',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_BGPR.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'BGPR'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			               {
			                text: '<br>Agenti',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_CUAG.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'CUAG'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			            /*    {
			                text: '<br>Ciclo',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_win_std('Cambia ciclo vita', 'acs_gestione_ciclo_vita.php?fn=open_grid', {}, 600, 600,  {}, 'icon-clessidra-16');	
								} //handler function()
			            }*/	
			             {
			                text: '<br>Mastri',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_CUCF.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'CUCF'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            },
			               {
			                text: '<br>Condizioni BATCH',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								acs_show_panel_std('acs_tab_sys_BCCG.php?fn=open_panel', 'panel_gestione_tabelle', {taid: 'BCCG'});	          	                	                
				           			panel = this.up('buttongroup').up('panel').up('window');
				           			panel.close();
								} //handler function()
			            }
			            ]
			        },
			]
		}
	]		
}            