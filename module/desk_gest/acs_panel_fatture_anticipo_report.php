<?php

require_once "../../config.inc.php";
require_once("acs_panel_fatture_anticipo_include.php");

$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
$main_module = new Spedizioni();


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>


<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv3 td{font-weight: bold; font-size: 0.9em;}   
   tr.liv_data th{background-color: #333333; color: white;}
   tr.fattura_vendita td{font-style: italic}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
   
   div.with_todo_tooltip{text-align: right;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>
  
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<?php




//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);


$ar=crea_ar_tree_fatture_anticipo('', $form_values);


echo "<div id='my_content'>";

echo "
  		<div class=header_page>
			<H2>Elenco fatture anticipi</H2>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>";

echo "<table class=int1>";
echo "<tr><th>Nazione/Cliente/Fattura di anticipo</th>
		<th>Codice</th>
		<th>Data</th>
		<th>Riferimento</th>
		<th class=number>Totale documento</th>
		<th class=number>Totale imponibile</th>
		<th class=number>Importo detrazioni</th>
		<th class=number>Residuo</th>
		<th>Stato</th></tr>";

foreach($ar as $kar => $r){
	
		echo "<tr class=liv_totale><td>".$r['liv_cod']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   				  <td>&nbsp;</td>	
   				  <td class=number>".n($r['TFTOTD'])."</td>	
   			      <td class=number>".n($r['TFTIMP'])."</td>	
   				  <td class=number>".n($r['detratto'])."</td>
   		          <td class=number>".n($r['residuo'])."</td>
   				  <td>".$r['stato']."</td></tr>";
	
		foreach($r['children'] as $kar1 => $r1){
			
			echo "<tr class=liv3><td>".$r1['task']. "</td>
   		          <td>".$r1['codice']."</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
   				  <td class=number>".n($r1['TFTOTD'])."</td>
   			      <td class=number>".n($r1['TFTIMP'])."</td>
   				  <td class=number>".n($r1['detratto'])."</td>
   		          <td class=number>".n($r1['residuo'])."</td>
   				  <td>".$r1['stato']."</td></tr>";
			
			
			foreach($r1['children'] as $kar2 => $r2){
					
				echo "<tr class=liv2><td>".$r2['task']. "</td>
   		          <td>".$r2['codice']."</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
   				  <td class=number>".n($r2['TFTOTD'])."</td>
   			      <td class=number>".n($r2['TFTIMP'])."</td>
   				  <td class=number>".n($r2['detratto'])."</td>
   		          <td class=number>".n($r2['residuo'])."</td>
   				  <td>".$r2['stato']."</td></tr>";
				
 				foreach($r2['children'] as $kar3 => $r3){
						
					echo "<tr><td>".$r3['task']. "</td>
   		         	 <td>".$r3['codice']."</td>
   				 	 <td>".print_date($r3['data'])."</td>
 				     <td>".$r3['TFRGSV']."</td>
   				 	 <td class=number>".n($r3['TFTOTD'])."</td>
   			      	 <td class=number style= font-weight:bold>".n($r3['TFTIMP'])."</td>
   				 	 <td class=number>".n($r3['detratto'])."</td>";
					
					if($r3['residuo']>0){
						echo "<td class=number style= font-weight:bold>".n($r3['residuo'])."</td>";
					} else{
						echo "<td class=number>".n($r3['residuo'])."</td>";
					}
 				
   					 echo "<td>".$r3['stato']."</td></tr>";
					
					$ar_rf=crea_ar_tree_fatture_anticipo($r3['id'], $form_values);
			
					
					if(is_array($ar_rf)){
					
					foreach($ar_rf as $kar4 => $r4){
					
						echo "<tr class=fattura_vendita><td style=text-align:right>".$r4['task']. "</td>
   		         	 <td>".$r4['codice']."</td>
   				 	 <td>".print_date($r4['data'])."</td>
					 <td> &nbsp;</td>
   				 	 <td> &nbsp;</td>
   			      	 <td> &nbsp;</td>
   				 	 <td class=number>".n($r4['detratto'])."</td>
   		         	 <td class=number>".n($r4['residuo'])."</td>
   					 <td>".$r4['stato']."</td></tr>";
						}
					
					}
				
					
				}
				
				
			
		}
	
   }

}

echo "</table>";


?>

</div>
</body>
</html>

