<?php

require_once("../../config.inc.php");
$m_params = acs_m_params_json_decode();

$divieto = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; 	
$trad = "<img src=" . img_path("icone/48x48/globe.png") . " height=20>";
$puvn = "<img src=" . img_path("icone/48x48/search.png") . " height=20>";



$m_table_config = array(
    'TAID' => 'CUCP',
    'msg_on_save' => true,
    'title_grid' => 'CUCP-Pagamenti',
    'fields' => array(
        'TAID'     => array('label'	=> 'Codice', 'hidden' => 'true'),
        'TATP'     => array('label'	=> $divieto, 'c_width' => 40, 'only_view' => 'C'),
        'TANR'     => array('label'	=> 'Codice',  'short' =>'Cod.', 'width' => 160,  'c_width' => 40, 'upper' => 'Y', 'check' => 'Y', 'maxLength' => 4,
                            'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 160),
        'TACINT'   => array('label'	=> 'Interfaccia', 'only_view' => 'F',  'maxLength' => 10,
                            'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'labelWidth' => 70, 'width' => 200, 'close' => 'Y'),
        'TADESC'   => array('label'	=> 'Descrizione', 'maxLength' => 30),
        't_scad1'    => array('label'	=> 'Tipo scadenza', 'only_view' => 'C', 'short' =>'TS', 'c_width' => 30),
        'gg_inizio1' => array('label'	=> 'Giorni inizio 1a rata', 'only_view' => 'C', 'short' =>'GI', 'c_width' => 30),
        'rate1'      => array('label'	=> 'Numero rate', 'only_view' => 'C', 'short' =>'NR', 'c_width' => 30),
        'gg_freq1'   => array('label'	=> 'Giorni frequenza', 'only_view' => 'C', 'short' =>'Fr', 'c_width' => 30),
        'gg_fisso1'  => array('label'	=> 'Giorno fisso', 'only_view' => 'C', 'short' =>'Fi', 'c_width' => 30),
        'TADES2'   => array('label'	=> 'Note', 'maxLength' => 30),
        'banca_ob'    => array('label'	=> 'Banca obbligatoria', 'only_view' => 'F', 'type' =>'combo',
                              'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 210),
        'contanti'    => array('label'	=> 'Contanti', 'only_view' => 'F', 'type' =>'combo',
                              'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'labelWidth' => 60, 'width' => 150, 'close' => 'Y'),
        'data_cons'    => array('label'	=> 'Da data consegna', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'fascia_ass'    => array('label'	=> 'Fascia assicurativa', 'only_view' => 'F', 'maxLength' => 3,
                                'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'left', 'labelWidth' => 120, 'width' => 200),
        'fascia_alt'    => array('label'	=> 'Alternativa', 'only_view' => 'F', 'maxLength' => 3,
                                'xtype' => 'fieldcontainer', 'n_fc' => 1, 'labelAlign' =>'right', 'labelWidth' => 70, 'width' => 160, 'close' => 'Y'),
        'gg_fisso_cli'    => array('label'	=> 'Giorno fisso cliente', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'contr_ass'    => array('label'	=> 'Contrass.assic.fido', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'cond_pa'    => array('label'	=> 'Condiziona PA', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'gr_ava'    => array('label'	=> 'Gruppo avanzamento', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        'gr_pro'    => array('label'	=> 'Gruppo proposta', 'short' =>'Gr.P.', 'c_width' => 35, 'xtype' => 'combo_tipo'),
        'area_f1'    => array('label'	=> 'Area fiscale', 'only_view' => 'F',  'xtype' => 'combo_tipo'),
        'area_f2'    => array('label'	=> 'Area fiscale', 'only_view' => 'F',  'xtype' => 'combo_tipo'),
        'area_f3'    => array('label'	=> 'Area fiscale', 'only_view' => 'F',  'xtype' => 'combo_tipo'),
        'area_f4'    => array('label'	=> 'Area fiscale', 'only_view' => 'F',  'xtype' => 'combo_tipo'),
        'TAREST'   => array('label'	=> 'tarest', 'hidden' => 'true'),
        'RRN'      => array('label'	=> 'RRN', 'hidden' => 'true'),
        'TADTGE'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSGE'      => array('label'	=> 'utente', 'hidden' => 'true'),
        'TADTUM'      => array('label'	=> 'data', 'hidden' => 'true'),
        'TAUSUM'      => array('label'	=> 'utente', 'hidden' => 'true')
    ),
    'title_form' => 'Dettagli tabella pagamenti',
   // 'j_puvn' => 'Y',
   // 'ricerca_var' => 'Y',
   // 'tab_tipo' => 'PUVR',
    'TAREST' => array(
       't_scad1' 	=> array(
           "start" => 0,
           "len"   => 3,
           "riempi_con" => ""
       ),
        'gg_inizio1' 	=> array(
            "start" => 3,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'rate1' 	=> array(
            "start" => 6,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'gg_freq1' 	=> array(
            "start" => 8,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gg_fisso1' 	=> array(
            "start" => 11,
            "len"   => 2,
            "riempi_con" => ""
        ),
        '%_tot1' 	=> array(
            "start" => 13,
            "len"   => 2,
            "riempi_con" => ""
        ),
       
        'imp_min_eur1' 	=> array(
            "start" => 23,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'n_paga1' 	=> array(
            "start" => 31,
            "len"   => 3,
            "riempi_con" => ""
        )
        /*-------------------------------*/
        ,'t_scad2' 	=> array(
            "start" => 34,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gg_inizio2' 	=> array(
            "start" => 37,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'rate2' 	=> array(
            "start" => 40,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'gg_freq2' 	=> array(
            "start" => 42,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gg_fisso2'	=> array(
            "start" => 45,
            "len"   => 2,
            "riempi_con" => ""
        ),
        '%_tot2' 	=> array(
            "start" => 47,
            "len"   => 2,
            "riempi_con" => ""
        ),
      
        'imp_min_eur2' 	=> array(
            "start" => 57,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'n_paga2' 	=> array(
            "start" => 65,
            "len"   => 3,
            "riempi_con" => ""
        )
        /*-------------------------------*/
        ,'t_scad3' 	=> array(
            "start" => 68,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gg_inizio3' 	=> array(
            "start" => 71,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'rate3' 	=> array(
            "start" => 74,
            "len"   => 2,
            "riempi_con" => ""
        ),
        'gg_freq3' 	=> array(
            "start" => 76,
            "len"   => 3,
            "riempi_con" => ""
        ),
        'gg_fisso3'	=> array(
            "start" => 79,
            "len"   => 2,
            "riempi_con" => ""
        ),
        '%_tot3' 	=> array(
            "start" => 81,
            "len"   => 2,
            "riempi_con" => ""
        ),
       
        'imp_min_eur3' 	=> array(
            "start" => 91,
            "len"   => 8,
            "riempi_con" => ""
        ),
        'n_paga3' 	=> array(
            "start" => 99,
            "len"   => 3,
            "riempi_con" => ""
        )
        /*-------------------------------*/
        , 'gest_eff' => array(
            "start" => 102,
            "len"   => 1,
            "riempi_con" => ""
        ) 
        , 'banca_ob' => array(
            "start" => 103,
            "len"   => 1,
            "riempi_con" => ""
        )  
        , 'data_cons' => array(
            "start" => 104,
            "len"   => 1,
            "riempi_con" => ""
        )
        , 'fascia_ass' => array(
            "start" => 105,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'area_f1' => array(
            "start" => 108,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'area_f2' => array(
            "start" => 109,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'area_f3' => array(
            "start" => 110,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'area_f4' => array(
            "start" => 111,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'fascia_alt' => array(
            "start" => 112,
            "len"   => 3,
            "riempi_con" => ""
        ) 
        ,'gg_fisso_cli' => array(
            "start" => 115,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'contr_ass' => array(
            "start" => 116,
            "len"   => 1,
            "riempi_con" => ""
        )
        ,'cond_pa' => array(
            "start" => 117,
            "len"   => 4,
            "riempi_con" => ""
        )
        ,'contanti' => array(
            "start" => 121,
            "len"   => 1,
            "riempi_con" => ""
        ) 
        ,'gr_ava' => array(
            "start" => 122,
            "len"   => 3,
            "riempi_con" => ""
        )
        ,'gr_pro' => array(
            "start" => 125,
            "len"   => 3,
            "riempi_con" => ""
        )
   )
    
);

$m_table_config_rate1 = array(
    'TAID' => 'CUCP',
    'fields' => array(
        't_scad1'    => array('label'	=> 'Tipo scadenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_inizio1'    => array('label'	=> 'Giorni inizio 1a rata', 'only_view' => 'F', 'maxLength' => 3),
        'rate1'    => array('label'	=> 'Numero rate', 'only_view' => 'F', 'maxLength' => 2),
        'gg_freq1'    => array('label'	=> 'Giorni frequenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_fisso1'    => array('label'	=> 'Giorno fisso', 'only_view' => 'F', 'maxLength' => 2),
        '%_tot1'    => array('label'	=> '% sul totale', 'only_view' => 'F', 'maxLength' => 2),
        'imp_min_eur1'    => array('label'	=> 'Importo minimo', 'only_view' => 'F', 'maxLength' => 8),
        'n_paga1'    => array('label'	=> 'Pagamento assegn.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        
        
));

$m_table_config_rate2 = array(
    'TAID' => 'CUCP',
    'fields' => array(
        't_scad2'    => array('label'	=> 'Tipo scadenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_inizio2'    => array('label'	=> 'Giorni inizio 2a rata', 'only_view' => 'F', 'maxLength' => 3),
        'rate2'    => array('label'	=> 'Numero rate', 'only_view' => 'F', 'maxLength' => 2),
        'gg_freq2'    => array('label'	=> 'Giorni frequenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_fisso2'    => array('label'	=> 'Giorno fisso', 'only_view' => 'F', 'maxLength' => 2),
        '%_tot2'    => array('label'	=> '% sul totale', 'only_view' => 'F', 'maxLength' => 2),
        'imp_min_eur2'    => array('label'	=> 'Importo minimo', 'only_view' => 'F', 'maxLength' => 8),
        'n_paga2'    => array('label'	=> 'Pagamento assegn.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
    ));

$m_table_config_rate3 = array(
    'TAID' => 'CUCP',
    'fields' => array(
        't_scad3'    => array('label'	=> 'Tipo scadenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_inizio3'    => array('label'	=> 'Giorni inizio 3a rata', 'only_view' => 'F', 'maxLength' => 3),
        'rate3'    => array('label'	=> 'Numero rate', 'only_view' => 'F', 'maxLength' => 2),
        'gg_freq3'    => array('label'	=> 'Giorni frequenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_fisso3'    => array('label'	=> 'Giorno fisso', 'only_view' => 'F', 'maxLength' => 2),
        '%_tot3'    => array('label'	=> '% sul totale', 'only_view' => 'F', 'maxLength' => 2),
        'imp_min_eur3'    => array('label'	=> 'Importo minimo', 'only_view' => 'F', 'maxLength' => 8),
        'n_paga3'    => array('label'	=> 'Pagamento assegn.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
    ));

$m_table_config_altro = array(
    'fields' => array(
        't_scad1'    => array('label'	=> 'Tipo scadenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_inizio1'    => array('label'	=> 'Giorni inizio 1a rata', 'only_view' => 'F', 'maxLength' => 3),
        'rate1'    => array('label'	=> 'Numero rate', 'only_view' => 'F', 'maxLength' => 2),
        'gg_freq1'    => array('label'	=> 'Giorni frequenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_fisso1'    => array('label'	=> 'Giorno fisso', 'only_view' => 'F', 'maxLength' => 2),
        '%_tot1'    => array('label'	=> '% sul totale', 'only_view' => 'F', 'maxLength' => 2),
        'imp_min_eur1'    => array('label'	=> 'Importo minimo', 'only_view' => 'F', 'maxLength' => 8),
        'n_paga1'    => array('label'	=> 'Pagamento assegn.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        't_scad2'    => array('label'	=> 'Tipo scadenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_inizio2'    => array('label'	=> 'Giorni inizio 2a rata', 'only_view' => 'F', 'maxLength' => 3),
        'rate2'    => array('label'	=> 'Numero rate', 'only_view' => 'F', 'maxLength' => 2),
        'gg_freq2'    => array('label'	=> 'Giorni frequenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_fisso2'    => array('label'	=> 'Giorno fisso', 'only_view' => 'F', 'maxLength' => 2),
        '%_tot2'    => array('label'	=> '% sul totale', 'only_view' => 'F', 'maxLength' => 2),
        'imp_min_eur2'    => array('label'	=> 'Importo minimo', 'only_view' => 'F', 'maxLength' => 8),
        'n_paga2'    => array('label'	=> 'Pagamento assegn.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
        't_scad3'    => array('label'	=> 'Tipo scadenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_inizio3'    => array('label'	=> 'Giorni inizio 3a rata', 'only_view' => 'F', 'maxLength' => 3),
        'rate3'    => array('label'	=> 'Numero rate', 'only_view' => 'F', 'maxLength' => 2),
        'gg_freq3'    => array('label'	=> 'Giorni frequenza', 'only_view' => 'F', 'maxLength' => 3),
        'gg_fisso3'    => array('label'	=> 'Giorno fisso', 'only_view' => 'F', 'maxLength' => 2),
        '%_tot3'    => array('label'	=> '% sul totale', 'only_view' => 'F', 'maxLength' => 2),
        'imp_min_eur3'    => array('label'	=> 'Importo minimo', 'only_view' => 'F', 'maxLength' => 8),
        'n_paga3'    => array('label'	=> 'Pagamento assegn.', 'only_view' => 'F', 'xtype' => 'combo_tipo'),
    ));

//ROOT_ABS_PATH. 
require ROOT_ABS_PATH . 'module/desk_gest/acs_gestione_tabelle.php';
