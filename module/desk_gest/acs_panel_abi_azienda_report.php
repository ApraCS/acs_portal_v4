<?php

require_once "../../config.inc.php";
ini_set('max_execution_time', 300);

$s = new Spedizioni();
$main_module = new Spedizioni();

	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);

	
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div#my_content h1{font-size: 22px; padding: 5px;}
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}
	   

   /*tr.liv_totale td{background-color: #cccccc; font-weight: bold;}*/
   tr.liv_totale td{font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
      
	/* ANALISI BILANCIO */
    TR.D td{background-color: #C0C0C0; padding-top: 3px; padding-bottom: 3px; }
	TR.S1 td{background-color: #99CCCC; padding-top: 3px; padding-bottom: 3px; }
	TR.S2 td{background-color: #99B3CC; font-weight: bold; padding-top: 4px; padding-bottom: 4px;}
	TR.dett .tdl{background-color: #FFFFE0; font-weight: normal;}
	TR td.tot{font-weight: bold;}      
    table.int1 th {font-weight: bold;}  
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {

	//apertura automatica finestra invio email
	<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
		Ext.get('p_send_email').dom.click();
	<?php } ?>
	
	});    
    

  </script>


 </head>
 <body>
 
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>


<?php

//valori da tree/grid
$extraParamsValueConverted =	strtr($_REQUEST['extraParams'], array('\"' => '"'));
$extraParamsValue = (object)json_decode($extraParamsValueConverted);


//recupero dati

//QUERY SU CONTI
$sql_where .= " AND NOME_ANALISI = " . sql_t($extraParamsValue->nome_analisi_1);
$sql_where .= " AND NOME_ANALISI_CONFR = " . sql_t($extraParamsValue->nome_analisi_2);
$sql_where .= " AND CODICE_DITTA = " . sql_t($extraParamsValue->codice_ditta);

if (strlen($extraParamsValue->centro_di_costo) > 0)
	$sql_where .= " AND SMCTD0 = " . sql_t($extraParamsValue->centro_di_costo);


$sql = "SELECT * FROM {$cfg_mod_Gest['abi']['file_conti']} WHERE 1=1 {$sql_where} ORDER BY ORDINAMENTO";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$ar = array();


$ar_mov = array();

if ($_REQUEST['dettagliata'] != 'N'){ //a meno che non voglio escludere i dettagli
	//QUERY SU MOVIMENTI
	$sql_mov_where = " AND M.NOME_ANALISI IN (" . sql_t($extraParamsValue->nome_analisi_1) . ", " . sql_t($extraParamsValue->nome_analisi_2) . ")";
	$sql_mov_where .= " AND M.CODICE_DITTA = " . sql_t($extraParamsValue->codice_ditta);
	$sql_mov = "SELECT M.VOCE_ANALISI, M.NOME_ANALISI, M.CODICE_CONTO, M.DESCRIZIONE, SUM(M.IMPORTO) AS IMPORTO
			FROM {$cfg_mod_Gest['abi']['file_movimenti']} M
	  		WHERE 1=1 {$sql_mov_where}
			GROUP BY M.VOCE_ANALISI, M.NOME_ANALISI, M.CODICE_CONTO, M.DESCRIZIONE
			ORDER BY M.CODICE_CONTO
	";
	
	$stmt_mov = db2_prepare($conn, $sql_mov);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_mov);
	$ar_mov = array();
	
	
	while ($r = db2_fetch_assoc($stmt_mov)) {
	//	print_r($r);
		$ar_mov[trim($r['VOCE_ANALISI'])][trim($r['CODICE_CONTO'])][trim($r['NOME_ANALISI'])]= $r;	
	}
}




?>


 <div id='my_content'>
 
 
 <h1>Analisi riclassificazione dati di bilancio per azienda <?php echo get_nome_ditta($extraParamsValue->codice_ditta); ?></h1>
 <h2>&nbsp;[Periodo: <?php echo $extraParamsValue->nome_analisi_1; ?>, periodo raffronto: <?php echo $extraParamsValue->nome_analisi_2; ?>]</h2>
 <div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
 <br/>
 

 	<table class="int1" width="100%">
 	 <tr>
 	  <th rowspan=2>Voce</th>
 	  <th style="text-align: center;" colspan=2>Periodo <?php echo $extraParamsValue->nome_analisi_1; ?></th>
 	  <th style="text-align: center;" colspan=2>Periodo <?php echo $extraParamsValue->nome_analisi_2; ?></th>
 	  <th style="text-align: center;" colspan=2>Scostamento</th>
 	 </tr> 	
 	 <tr>
 	  <th class=number>Importo</th>
 	  <th class=number>%</th>
 	  <th class=number>Importo</th>
 	  <th class=number>%</th>
 	  <th class=number>Importo</th>
 	  <th class=number>%</th>
 	 </tr>
 	 
<?php	 
while ($r = db2_fetch_assoc($stmt)) {
    
    $desk_voce = '';
    if ($cfg_mod_Gest['abi']['report_escludi_codice_voce'] != 'Y')
        $desk_voce .= "[" . trim($r['VOCE_ANALISI']) . "] ";
    $desk_voce .= trim(acs_u8e($r['DES_VOCE_ANALISI']));
    
 ?>
  <tr class='<?php echo trim($r['TIPO_VOCE']); ?>'>
   <td><?php echo $desk_voce ?></td>
   <td class=number><?php echo n($r['IMPORTO_A']); ?></td>
   <td class=number><?php echo n($r['IMPORTO_B']); ?></td>
   <td class=number><?php echo n($r['IMPORTO_C']); ?></td>
   <td class=number><?php echo n($r['IMPORTO_D']); ?></td>
   <td class=number><?php echo n($r['IMPORTO_A'] - $r['IMPORTO_C']); ?></td>   
   <?php if ((int)$r['IMPORTO_C'] != 0){ ?> 
    	<td class=number><?php echo n( ($r['IMPORTO_A'] - $r['IMPORTO_C']) / $r['IMPORTO_C'] * 100); ?></td>
   <?php } else { ?> 	 
    	<td class=number>&nbsp;</td>
   <?php } ?> 	
    	

  </tr>
 <?php
 
 //stampo le righe di dettaglio
 if (isset($ar_mov[trim($r['VOCE_ANALISI'])])){
 foreach ($ar_mov[trim($r['VOCE_ANALISI'])] as $km => $m){
 	
 	$m_des = '';
 	$m_IMPORTO_A = 0;
 	$m_IMPORTO_B = 0;
 	$m_IMPORTO_C = 0;
 	$m_IMPORTO_D = 0;
 	
 	if (isset($m[$extraParamsValue->nome_analisi_1])){
 		$m_des = trim($m[$extraParamsValue->nome_analisi_1]['DESCRIZIONE']);
 		$m_IMPORTO_A = $m[$extraParamsValue->nome_analisi_1]['IMPORTO'];
 	}	
 	if (isset($m[$extraParamsValue->nome_analisi_2])){
 		$m_des = trim($m[$extraParamsValue->nome_analisi_2]['DESCRIZIONE']);
 		$m_IMPORTO_C = $m[$extraParamsValue->nome_analisi_2]['IMPORTO'];
 	}
 	
 ?>
  <tr class=dett>
   <td><?php echo "[" . $km . "] " . trim(acs_u8e($m_des)); ?></td>
   <td class=number><?php echo n($m_IMPORTO_A); ?></td>
   <td class=number><?php echo n($m_IMPORTO_B); ?></td>
   <td class=number><?php echo n($m_IMPORTO_C); ?></td>
   <td class=number><?php echo n($m_IMPORTO_D); ?></td>   
   <td class=number><?php echo n($m_IMPORTO_A - $m_IMPORTO_C); ?></td>
   <?php if ((int)$m_IMPORTO_C != 0){ ?> 
    	<td class=number><?php echo n( ($m_IMPORTO_A - $m_IMPORTO_C) / $m_IMPORTO_C * 100); ?></td>
   <?php } else { ?> 	 
    	<td class=number>&nbsp;</td>
   <?php } ?>   
  </tr> 
 <?php 	
 } //foreach
 } //if iseset
 
}

?>
 	 
 	 
 	 
 	 
	</table>

	
	<br/>
	
	
 </div>

 </body>
</html>
		