<?php

require_once "../../config.inc.php";

$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));



	$ar_email_to = array();

	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}
	
	$ar_email_json = acs_je($ar_email_to);	 
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
  
  </script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>


<?php

function sql_where_by_request($m_params){

	$sql_where = "";
	
/*	
	$sql_where .= sql_where_by_combo_value('RF.RFTPRG', array('ACCREDITATO'));
	
	if (strlen($m_params->f_data_da) > 0)
		$sql_where .= " AND RF.RFDTRG >= {$m_params->f_data_da}";		
	if (strlen($m_params->f_data_a) > 0)
		$sql_where .= " AND RF.RFDTRG <= {$m_params->f_data_a}";
*/		
	
	
	//prendo il MATURATO e ACCREDITATO del periodo richiesto, e tutto il non maturato fino a data
	$sql_where .= " AND (
					 	 (RF.RFTPRG IN('MATURATO', 'ACCREDITATO') AND RF.RFDTRG >= {$m_params->f_data_da} AND RF.RFDTRG <= {$m_params->f_data_a})
					  OR (RF.RFTPRG = 'ACCREDITATO' AND RFFG03<>'M' AND RF.RFDTRG <= {$m_params->f_data_a})	  				
				 	)
				 ";

	$sql_where .= sql_where_by_combo_value('RF.RFGRAG', $m_params->f_gruppo);	
	$sql_where .= sql_where_by_combo_value('RF.RFAGEN', $m_params->f_agenzia);
	$sql_where .= sql_where_by_combo_value('RF.RFCAGE', $m_params->f_agente);	

	if ($m_params->f_fatture_o_ordini == 'VF')
		$sql_where .= sql_where_by_combo_value('RF.RFTIDO', ['VF', 'AG']);
	else
		$sql_where .= sql_where_by_combo_value('RF.RFTIDO', $m_params->f_fatture_o_ordini);
	
	$sql_where .= sql_where_by_combo_value('TF.TFCDIV', $m_params->f_divisione);	
	$sql_where .= sql_where_by_combo_value('TF.TFTPDO', $m_params->f_tipo_documento);	
	$sql_where .= sql_where_by_combo_value('TF.TFCCON', $m_params->f_cliente_cod);		
	return $sql_where;		
}


function somma_valori($ar, $r, $m_params){
	//ToDo
	//$ar['COLLI'] 	+= $r['TDTOCO'];
	
	global $conn, $cfg_mod_Gest;
	

	if (trim($r['RFTPRG']) == 'ACCREDITATO'){
		
		if ($r['RFDTRG'] >= $m_params->f_data_da && $r['RFDTRG'] <= $m_params->f_data_a)		
			$ar['S_ACCREDITATO'] += $r['RFIMPO'];

		
		if ($r['RFFG03'] != 'M'){ //non maturato o maturato parziale
			
			if (trim($r['RFFG03']) == '') //non maturato
				$importo_provvigioni = $r['RFIMPO'];
			else if (trim($r['RFFG03']) == 'P') { //matuarato parziale
				//calcolo il residuo

				$sql2 = "SELECT SUM(RFIMPO) AS S_RFIMPO FROM {$cfg_mod_Gest['provvigioni']['file_righe']} WHERE RFDOCU=? AND RFCAGE=? AND RFTPRG=?";
				$stmt2 = db2_prepare($conn, $sql2);
				$result = db2_execute($stmt2, array($r['RFDOCU'], $r['RFCAGE'], 'MATURATO'));
				$r2 = db2_fetch_assoc($stmt2);
				$importo_provvigioni = $r['RFIMPO'] - $r2['S_RFIMPO'];
			}
			
				
			
			
			if ($r['RFDTRG'] >= $m_params->f_data_da && $r['RFDTRG'] <= $m_params->f_data_a)
				$ar['AR_NON_MATURATO']['PERIODO'] += $importo_provvigioni;
			else {
				//recupero l'anno dalla data documento
				$ar['AR_NON_MATURATO'][substr($r['RFDTRG'], 0, 4)] += $importo_provvigioni;
			}							
		} 
	}
	
	//il MATURATO lo divido nei vari anni o nel periodo selezionato
	if (trim($r['RFTPRG']) == 'MATURATO'){
		$ar['S_MATURATO'] += $r['RFIMPO'];		
		
		//Corrisponde al periodo filtrato
		if ($r['TFDTRG'] >= $m_params->f_data_da && $r['TFDTRG'] <= $m_params->f_data_a)
			$ar['AR_MATURATO']['PERIODO'] += $r['RFIMPO'];
		else {
			//recupero l'anno dalla data documento
			$ar['AR_MATURATO'][substr($r['TFDTRG'], 0, 4)] += $r['RFIMPO'];
		}

	}	
	
	$ar['conteggio_record']++;

	return $ar;
}


//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);


//se ha chiesto residuo, lavoro sempre sull'accreditato e poi lo confronto con il maturato o liquidato
if ($form_values->f_residuo == 'Y') {
	$form_values->f_tipologia_selezionata = $form_values->f_tipologia;		
	$form_values->f_tipologia = 'ACCREDITATO';		
}
	
	$sql_where = sql_where_by_request((object)$form_values);	

	$sql = "SELECT RF.*, TFCCON, TFDCON, TFTIMP, TFAADO, TFNRDO, TFTPDO, TFDTRG
			FROM {$cfg_mod_Gest['provvigioni']['file_testate']} TF
			INNER JOIN  {$cfg_mod_Gest['provvigioni']['file_righe']} RF
			ON TF.TFDOCU = RF.RFDOCU
			WHERE 1=1 {$sql_where} 
			";
	

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg(); 
$result = db2_execute($stmt);

$liv0_in_linea = null;
$ar = array();

$t_ar_litT = array();

while ($r = db2_fetch_assoc($stmt)) {
	
	//tree
	$liv0 = implode("|", array($r['RFAGEN'])); //Agenzia
	$liv1 = implode("|", array($r['RFCAGE'])); //Agente
	$liv2 = implode("|", array($r['TFCCON'])); //Cliente
	
	if ($form_values->f_residuo == 'Y')
		$liv3 = implode("|", array($r['RFDOCU'])); //Documento/riga provv
	else
		$liv3 = implode("|", array($r['RFDOCU'], $r['RFRIGA'])); //Documento/riga provv	

	
	//agenzia
	$t_ar = &$ar;
	$l = $liv0;
	if (!isset($t_ar[$l]))
		$t_ar[$l] = array("cod" => $l, "descr"=>$r['RFDAGN'], "record" => $r,
				"val" => array(), "children"=>array());

		$t_ar_liv0 = &$t_ar[$l]['val'];
		$t_ar = &$t_ar[$l]['children'];
	
		
		//agente
		$l = $liv1;
		if (!isset($t_ar[$l]))
			$t_ar[$l] = array("cod" => $l, "descr"=>$r['RFDAGE'], "record" => $r,
					"val" => array(), "children"=>array());

			$t_ar_liv1 = &$t_ar[$l]['val'];
			$t_ar = &$t_ar[$l]['children'];
			 
			
			//cliente
			$l = $liv2;
			if (!isset($t_ar[$l]))
				$t_ar[$l] = array("cod" => $l, "descr"=>$r['TFDCON'], "record" => $r,
						"val" => array(), "children"=>array());

				$t_ar_liv2 = &$t_ar[$l]['val'];
				$t_ar = &$t_ar[$l]['children'];
	
				//documento o documento/riga
				$l = $liv3;
				if (!isset($t_ar[$l])){

					$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r, "val" => array(), "children"=>array());
					
					if ($form_values->f_residuo == 'Y') {
						//recupero il liquidato/maturato per documento
						$sql2 = "SELECT SUM(RFIMPO) AS S_RFIMPO, SUM(RFIMLQ) AS S_RFIMLQ FROM {$cfg_mod_Gest['provvigioni']['file_righe']} WHERE RFDOCU=? AND RFCAGE=? AND RFTPRG=?";
						$stmt2 = db2_prepare($conn, $sql2);
						$result = db2_execute($stmt2, array($r['RFDOCU'], $r['RFCAGE'], 'MATURATO'));
						
						$r2 = db2_fetch_assoc($stmt2);
						$t_ar[$l]['val']['sum_per_residuo'] = $r2['S_RFIMPO'];
						$t_ar[$l]['val']['sum_per_residuo_liq'] = $r2['S_RFIMLQ'];
							
					}
				}
				

					$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r, (object)$form_values);
					$t_ar_liv0 = somma_valori($t_ar_liv0, $r, (object)$form_values);
					$t_ar_liv1 = somma_valori($t_ar_liv1, $r, (object)$form_values);
					$t_ar_liv2 = somma_valori($t_ar_liv2, $r, (object)$form_values);
					$t_ar_litT = somma_valori($t_ar_litT, $r, (object)$form_values);
					
				
				$t_ar = &$t_ar[$l]['children'];
					
				//accodo le righe ordine
				$t_ar[] = $r;

} //per ogni row


//echo "<pre>"; print_r($ar); exit;


//stampo
$cl_liv_cont = 0;
$liv1_row_cl = 3;
$liv2_row_cl = 2;

echo "<div id='my_content'>";


echo "
	<div class=header_page>
	<h2>Riepilogo provvigioni Accreditate e Maturate</h2>
	
	Periodo dal " . print_date($_REQUEST['f_data_da']) . " al " . print_date($_REQUEST['f_data_a']) . "		
		
 	<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
	</div>
";		




echo "<table class=int0 width=\"100%\"><table class=int1>";

	//riga di totale generale
	echo liv_tot_intestazione_open(array('val' => $t_ar_litT), 3, $t_ar_litT);

	foreach ($ar as $kl0 => $l0){
		echo liv0_intestazione_open($l0, 3, $t_ar_litT);
	
		foreach ($l0['children'] as $kl1 => $l1){
			echo liv1_intestazione_open($l1, 2, $t_ar_litT);
				
			foreach ($l1['children'] as $kl2 => $l2)
				echo liv2_intestazione_open($l2, 1, $t_ar_litT);
			
		}
	
		//echo liv0_intestazione_close($l0['record']);
		
	}
	
echo "</table></table>";
echo "</div>";











//Agenzia
function DELETE_liv0_intestazione_open($r, $c_liv0 = 1){
	global $form_values;
	global $s, $spedizione;

	$ret = "";

	//salto pagina
	if ($c_liv0 > 1)
		$ret .= "<div class=\"page-break\"></div>";	

	$ret .= "
  		<div class=header_page>
  			<h2>Riepilogo provvigioni Accreditate e Maturate</h2>
			<H2>Elenco provvigioni - {$_REQUEST['f_tipologia']}</H2>
			<h3>" . trim($r['RFDAGN'])  . " [" . trim($r['RFAGEN']) . "]</H3>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>			
		</div>
			
			
		
		
			<table class=int0 width=\"100%\">
			 <tr>
			  <td width=\"50%\">Periodo dal " . print_date($_REQUEST['f_data_da']) . " al " . print_date($_REQUEST['f_data_a']) . "</td>";

    $ret .= "
			 </TR>
	</table>
		
		
	<table class=int1>
			 <tr>
			  <th>Cliente</th>
			  <th class=number>Importo Provv.</th>";
						
	$ret .= "</tr>";

	return $ret;
}

function liv0_intestazione_close($r){
return "</table>";
}





//Agenzia
function liv_tot_intestazione_open($l, $cl_liv, $t_ar_litT){
	global $form_values;
	$n_col_span = 1;


	$span_carico_class = '';

	$ret = "

	<tr>
		<th>&nbsp;</th>
		<th class=number>Accreditato</th>
		";

	if (is_array($t_ar_litT['AR_MATURATO']))
		foreach($t_ar_litT['AR_MATURATO'] as $k => $v)
			$ret .= "<th class=number>Maturato {$k}</th>";

		if (is_array($t_ar_litT['AR_NON_MATURATO']))
			foreach($t_ar_litT['AR_NON_MATURATO'] as $k => $v)
				$ret .= "<th class=number>Da maturare {$k}</th>";

			$ret .= "</tr>";

			$ret .= "<tr class=liv{$cl_liv}>
					<td colspan={$n_col_span}><span class=\"{$span_carico_class}\">TOTALE GENERALE</span></TD>
					<td class=number>" . n($l['val']['S_ACCREDITATO']) . "</td>
			";

			if (is_array($t_ar_litT['AR_MATURATO']))
				foreach($t_ar_litT['AR_MATURATO'] as $k => $v)
					$ret .= "<td class=number>" . n($l['val']['AR_MATURATO'][$k]) . "</td>";

				if (is_array($t_ar_litT['AR_NON_MATURATO']))
					foreach($t_ar_litT['AR_NON_MATURATO'] as $k => $v)
						$ret .= "<td class=number>" . n($l['val']['AR_NON_MATURATO'][$k]) . "</td>";


					$ret .= "<tr>";

					return $ret;
}



//Agenzia
function liv0_intestazione_open($l, $cl_liv, $t_ar_litT){
	global $form_values;
	$n_col_span = 1;
	$span_carico_class = '';

	$tot_colspan = 2;
	if (is_array($t_ar_litT['AR_MATURATO'])) $tot_colspan += count($t_ar_litT['AR_MATURATO']);
	if (is_array($t_ar_litT['AR_NON_MATURATO'])) $tot_colspan += count($t_ar_litT['AR_NON_MATURATO']);
	
	//riva vuota
	$ret .= "<tr><td colspan={$tot_colspan}>&nbsp;</td></tr>";


	$ret .= "

	<tr>
		<th>&nbsp;</th>
		<th class=number>Accreditato</th>
		";

	if (is_array($t_ar_litT['AR_MATURATO']))
	foreach($t_ar_litT['AR_MATURATO'] as $k => $v)
		$ret .= "<th class=number>Maturato {$k}</th>";
	
	if (is_array($t_ar_litT['AR_NON_MATURATO']))
		foreach($t_ar_litT['AR_NON_MATURATO'] as $k => $v)
			$ret .= "<th class=number>Da maturare {$k}</th>";	
	
	$ret .= "</tr>";	
	
	$ret .= "<tr class=liv{$cl_liv}>
		<td colspan={$n_col_span}><span class=\"{$span_carico_class}\">Agenzia " . trim($l['descr']) . " [" . trim($l['record']['RFDAGN']) . "]</span></TD>
		<td class=number>" . n($l['val']['S_ACCREDITATO']) . "</td>
	";
	
	if (is_array($t_ar_litT['AR_MATURATO']))
	foreach($t_ar_litT['AR_MATURATO'] as $k => $v)
		$ret .= "<td class=number>" . n($l['val']['AR_MATURATO'][$k]) . "</td>";

	if (is_array($t_ar_litT['AR_NON_MATURATO']))
		foreach($t_ar_litT['AR_NON_MATURATO'] as $k => $v)
			$ret .= "<td class=number>" . n($l['val']['AR_NON_MATURATO'][$k]) . "</td>";
	

	$ret .= "<tr>";
	
	return $ret;
}



//Agente
function liv1_intestazione_open($l, $cl_liv, $t_ar_litT){
	global $form_values;
	$n_col_span = 1;


	$span_carico_class = '';
	if ($_REQUEST['stampa_dettaglio_ordini'] != "Y" && $_REQUEST['stampa_dettaglio_ordini'] != "R")
		$span_carico_class = 'grassetto';

	$ret = "
	  <tr class=liv{$cl_liv}>
		<td colspan={$n_col_span}><span class=\"{$span_carico_class}\">AGENTE " . trim($l['descr']) . " [" . trim($l['record']['RFCAGE']) . "]</span>" . $orario_carico . $note_carico . "</TD>
		<td class=number>" . n($l['val']['S_ACCREDITATO']) . "</pre></td>
		";
	
	if (is_array($t_ar_litT['AR_MATURATO']))
	foreach($t_ar_litT['AR_MATURATO'] as $k => $v)
		$ret .= "<td class=number>" . n($l['val']['AR_MATURATO'][$k]) . "</td>";

	if (is_array($t_ar_litT['AR_NON_MATURATO']))
		foreach($t_ar_litT['AR_NON_MATURATO'] as $k => $v)
			$ret .= "<td class=number>" . n($l['val']['AR_NON_MATURATO'][$k]) . "</td>";
	
	
	  $ret .= "</tr>";

	return $ret;
}






//Cliente
function liv2_intestazione_open($l, $cl_liv, $t_ar_litT){
	global $form_values;
	global $main_module, $cfg_mod_Spedizioni;

	$td_cli_class = '';

	$ret = "
	 <tr class=\"liv{$cl_liv}\">
		<td colspan={$n_col_span}><span class=\"{$span_carico_class}\">Cliente " . trim($l['descr']) . " [" . trim($l['record']['TFCCON']) . "]</span></TD>	 
	 	<td class=number colspan=1>" . n($l['val']['S_ACCREDITATO']) . "</pre></td>
	";	

	if (is_array($t_ar_litT['AR_MATURATO']))
	foreach($t_ar_litT['AR_MATURATO'] as $k => $v)
		$ret .= "<td class=number>" . n($l['val']['AR_MATURATO'][$k]) . "</td>";
	
	if (is_array($t_ar_litT['AR_NON_MATURATO']))
		foreach($t_ar_litT['AR_NON_MATURATO'] as $k => $v)
			$ret .= "<td class=number>" . n($l['val']['AR_NON_MATURATO'][$k]) . "</td>";
	
	
	$ret .= "</tr>";


	return $ret;
}












?>





 </body>
</html>