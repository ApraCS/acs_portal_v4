<?php

require_once("../../config.inc.php");

$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_inserisci'){
    
   $sql_where = "";
   $sql_where .= sql_where_by_combo_value('SPANNO', $m_params->form_values->SPANNO);
   $sql_where .= sql_where_by_combo_value('SPVALU', $m_params->form_values->SPVALU);
   $sql_where .= sql_where_by_combo_value('SPDEST', $m_params->form_values->SPDEST);
   $sql_where .= sql_where_by_combo_value('SPCCON', $m_params->cliente);
    
    $sql = "SELECT *
            FROM {$cfg_mod_Gest['file_scala_premi']} SP
            WHERE SPDT = '{$id_ditta_default}' {$sql_where}";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    if($row != false && count($row) > 0){
        $ret['msg_error'] = "Codice esistente";
        $ret['success'] = false;
        echo acs_je($ret);
        exit;
    }
    
   
    $ar_ins = array();
    if(strlen($m_params->form_values->SPANNO) > 0)
        $ar_ins['SPANNO']   = sql_f($m_params->form_values->SPANNO);
    if(strlen($m_params->form_values->SPVALU) > 0)
        $ar_ins['SPVALU']   = $m_params->form_values->SPVALU;
    if(strlen($m_params->form_values->SPDEST) > 0)
        $ar_ins['SPDEST']   = $m_params->form_values->SPDEST;
            
    for($i=1; $i<= 8; $i++){
        $importo = "SPIM0{$i}";
        $premio = "SPPP0{$i}";
        
        $ar_ins["SPIM0{$i}"] = sql_f($m_params->form_values->$importo);
        $ar_ins["SPPP0{$i}"] = sql_f($m_params->form_values->$premio);
        
    }
            
    $ar_ins['SPUSGE'] = $auth->get_user();
    $ar_ins['SPDTGE'] = oggi_AS_date();
    $ar_ins['SPTMGE'] = oggi_AS_time();
    $ar_ins['SPUSUM'] = $auth->get_user();
    $ar_ins['SPDTUM'] = oggi_AS_date();
    $ar_ins['SPTMUM'] = oggi_AS_time();
    $ar_ins['SPDT']   = $id_ditta_default;
    $ar_ins['SPCCON'] = sprintf("%09d", $m_params->cliente);
    $ar_ins['SPPROG'] = $main_module->next_num('BUCF');
    
    $sql = "INSERT INTO {$cfg_mod_Gest['file_scala_premi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg();

    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'exe_aggiorna'){
    
        $sql = "  SELECT *
                  FROM {$cfg_mod_Gest['file_scala_premi']} SP 
                  WHERE RRN(SP) = '{$m_params->form_values->rrn}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        if($row['SPDTGE'] != oggi_AS_date() || trim($row['SPSOSP']) == ''){
            $row['SPSOSP'] = 'S';
            $row['SPUSUM'] = $auth->get_user();
            $row['SPDTUM'] = oggi_AS_date();
            $row['SPTMUM'] = oggi_AS_time();
            
            $sql_or = "UPDATE {$cfg_mod_Gest['file_scala_premi']} SP
                       SET " . create_name_field_by_ar_UPDATE($row) . "
                       WHERE RRN(SP) = '{$m_params->form_values->rrn}'";
          
            
            $stmt_or = db2_prepare($conn, $sql_or);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_or, $row);
            echo db2_stmt_errormsg();
            
            $row['SPSOSP']   = 'N';
            $row['SPPROG']   = $main_module->next_num('BUCF');
            $row['SPANNO']   = sql_f($m_params->form_values->SPANNO);
            $row['SPVALU']   = $m_params->form_values->SPVALU;
            $row['SPDEST']   = $m_params->form_values->SPDEST;
            
            for($i=1; $i<= 8; $i++){
                $importo = "SPIM0{$i}";
                $premio = "SPPP0{$i}";
                
                $row["SPIM0{$i}"] = sql_f($m_params->form_values->$importo);
                $row["SPPP0{$i}"] = sql_f($m_params->form_values->$premio);
                
            }
            
            
            $sql_new = "INSERT INTO {$cfg_mod_Gest['file_scala_premi']}(" . create_name_field_by_ar($row) . ") VALUES (" . create_parameters_point_by_ar($row) . ")";
            
            $stmt_new = db2_prepare($conn, $sql_new);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt_new, $row);
            echo db2_stmt_errormsg();
        }else{
            
            $ar_ins = array();
            $ar_ins['SPUSUM'] = $auth->get_user();
            $ar_ins['SPDTUM'] = oggi_AS_date();
            $ar_ins['SPTMUM'] = oggi_AS_time();
            
            if(strlen($m_params->form_values->SPANNO) > 0)
                $ar_ins['SPANNO']   = sql_f($m_params->form_values->SPANNO);
            if(strlen($m_params->form_values->SPVALU) > 0)
                $ar_ins['SPVALU']   = $m_params->form_values->SPVALU;
            if(strlen($m_params->form_values->SPDEST) > 0)
                $ar_ins['SPDEST']   = $m_params->form_values->SPDEST;
                        
            for($i=1; $i<= 8; $i++){
                $importo = "SPIM0{$i}";
                $premio = "SPPP0{$i}";
                
                $ar_ins["SPIM0{$i}"] = sql_f($m_params->form_values->$importo);
                $ar_ins["SPPP0{$i}"] = sql_f($m_params->form_values->$premio);
                
            }
            
            $sql = "UPDATE {$cfg_mod_Gest['file_scala_premi']} SP
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE RRN(SP) = '{$m_params->form_values->rrn}'";
            
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
            
            
        }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql = "DELETE FROM {$cfg_mod_Gest['file_scala_premi']} SP
            WHERE RRN(SP) = '{$m_params->form_values->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $m_params = acs_m_params_json_decode();
    $ret = array();
    $sql = "SELECT RRN(SP) AS RRN, SP.*
            FROM {$cfg_mod_Gest['file_scala_premi']} SP
            WHERE SPDT = '{$id_ditta_default}' AND SPCCON = '{$m_params->cliente}'";
  
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    
    while ($row = db2_fetch_assoc($stmt)) {
        
        $row['chiave'] = implode('_', array($row['SPDEST'], $row['SPANNO'], $row['SPVALU']));
        
        if(trim($row['SPDEST']) != ''){
            $pven = get_TA_sys('VUDE', trim($row['SPDEST']), sprintf("%09d", $m_params->cliente));
            $row['pven'] =  "[". trim($row['SPDEST']). "] ".$pven['text'];
            $row['SPDEST'] = trim($row['SPDEST']);
        }
        
         if(trim($row['SPVALU']) != ''){
            $valu = get_TA_sys('VUVL', trim($row['SPVALU']));
            $row['valuta'] =  "[". trim($row['SPVALU']). "] ".$valu['text'];
            $row['SPVALU'] = trim($row['SPVALU']);
        }

        
        for($i=1; $i<= 8; $i++){
            $importo = n($row["SPIM0{$i}"], 0);
            $premio = n($row["SPPP0{$i}"], 2);
            if($importo > 0 || $premio > 0)
            $row["range_{$i}"] = "{$importo}, {$premio}  %";
        }
        
        $row['rrn'] = $row["RRN"];
      
        $ret[] = $row;
    }
    
    echo acs_je($ret);
    exit;
}

//*************************************************************
if ($_REQUEST['fn'] == 'open_panel'){
//*************************************************************
    $d_cliente = trim($main_module->decod_cliente($m_params->cliente));
    $text_form = "Dettagli scala premi [{$m_params->cliente}] {$d_cliente}";
 
    ?>
   {"success":true, "items": [

   {
			xtype: 'panel',
			//<?php echo make_tab_closable(); ?>,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
    
    			{
				xtype: 'grid',
				flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		   		store: {
					xtype: 'store',
					autoLoad:true,
					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
			                    extraParams: {
			                     
			                     cliente : <?php echo j($m_params->cliente); ?>
									
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['chiave', 'rrn', 'pven', 'valuta', 'SPDEST', 'SPVALU', 'SPSOSP', 'SPPROG', 'SPANNO', 'SPNOTE', 'SPDTUM', 'SPDTGE', 'SPUSUM'
		        			<?php 
		        			for($i=1; $i<= 8; $i++){
		        			   echo ", 'SPIM0{$i}', 'SPPP0{$i}', 'range_{$i}'";
		        			   
		        			}
		        			?>
		        			
		        			]							
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		  
		    		  {text: 'PV', width: 25, dataIndex: 'SPDEST'}
		    		, {text: 'Anno', width: 40, dataIndex: 'SPANNO'}
		    		, {text: 'Valuta', width: 50, dataIndex: 'SPVALU'}
		    		<?php for($i=1; $i<= 8; $i++){?>
		    	    , {text: <?php echo j("Step {$i}"); ?>, flex : 1, dataIndex: <?php echo j("range_{$i}"); ?>}
		    		<?php }?>
		    		,{header: 'Immissione', dataIndex: 'SPDTGE', renderer: date_from_AS, width: 75, sortable : true,
                	 renderer: function(value, metaData, record){
                	         if (record.get('SPDTGE') != record.get('SPDTUM')) 
                	          metaData.tdCls += ' grassetto';
                	 
                             q_tip = 'Modifica: ' + date_from_AS(record.get('SPDTUM')) + ', Utente: ' +record.get('SPUSUM');
                			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
                		     return date_from_AS(value);	
                	}
                	 
                	 }
		    	
				],
				enableSort: true
				
				
	        	, listeners: {
					   selectionchange: function(selModel, selected) { 
	               
            	               if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            		               rec_index = selected[0].index;
            		               form_dx.getForm().findField('rec_index').setValue(rec_index);
            	                   form_dx.getForm().setValues(selected[0].data);
	                 			}
		       		  	 	 }
		       		  	 	 
				},
					viewConfig: {
        		        getRowClass: function(record, index) {	
        		        
        		        if(record.get('SPSOSP') == 'S'){
        		           return ' colora_riga_rosso';
                		 }
		        														
		         }   
		    }
	    		
	 	},
	 	
	 	{
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: <?php echo j($text_form); ?>,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.4,
 		            frame: true,
 		            items: [ 
 		             	{xtype : 'textfield', name : 'rec_index', hidden : true},
 		             	{xtype : 'textfield', name : 'rrn', hidden : true},
 		                {xtype : 'textfield', name : 'chiave', hidden : true},
 		             	
 		             	 {
			xtype: 'fieldcontainer',
			flex : 1,
			anchor: '-10',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'SPDEST',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'pven',
				    fieldLabel: 'Punto vendita',
                 	labelWidth : 90,
                    anchor: '-5',
                 	flex : 1,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('SPDEST').setValue(record_selected.cod);	
												form.findField('pven').setValue(record_selected.denom);
                                                from_win.close();
												}										    
									};
									;
							  
									acs_show_win_std('Seleziona punto vendita', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : <?php echo j($m_params->cliente); ?>, type : 'P'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
        							 }
        						}
        				}
        				
        				]
        		},
         		             
    				{xtype: 'numberfield',
						name: 'SPANNO',
						labelWidth: 90,
						width : 150,
						fieldLabel: 'Anno',
						value : <?php echo j(date("Y")); ?>,
						minValue : 2000,
						maxValue : 2999,
						hideTrigger : true,
						keyNavEnabled : false,
     					mouseWheelEnabled : false
						},
 		            
 		                 {
            			xtype : 'combo',
            		 	name: 'SPVALU', 
            		 	fieldLabel: 'Valuta',
            		 	labelWidth : 90,
            		 	flex : 1,
            		 	value : 'EUR',
            		 	forceSelection: true,  							
            			displayField: 'text',
            		    valueField: 'id',							
            			emptyText: '- seleziona -',
            		   	queryMode: 'local',
                 		minChars: 1,	
                 	    anchor: '-1',	
            			store: {
                			editable: false,
                			autoDestroy: true,
                			fields: [{name:'id'}, {name:'text'}],
                		    data: [								    
                		     	<?php echo acs_ar_to_select_json(find_TA_sys('VUVL', null, null, null, null, null, 0, '', 'Y'), ''); ?>
                		    ]
            			}, listeners: { 
            			 		beforequery: function (record) {
                	         		record.query = new RegExp(record.query, 'i');
                	         		record.forceAll = true;
                				 }
             			}	
            		 	
            		  }
            		  
            		  
            		  <?php for($i=1; $i<= 8; $i++){?>
            		  
            		 , {
					    xtype: 'fieldcontainer',
					    flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{xtype: 'numberfield',
    						name: 'SPIM0<?php echo $i; ?>',
    						labelWidth: 90,
    						fieldLabel: 'Imp. limite <?php echo $i; ?>',
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						decimalPrecision : 3,
    						flex : 0.4
    						},
    						{xtype: 'numberfield',
    						name: 'SPPP0<?php echo $i; ?>',
    						labelWidth: 60,
    						labelAlign : 'right',
    						fieldLabel: 'Premio <?php echo $i; ?>',
    						anchor: '-15',	
    						hideTrigger : true,
    						keyNavEnabled : false,
         					mouseWheelEnabled : false,
    						decimalPrecision : 2,
    					    flex : 0.25
    						}
						
						]}

		    		<?php }?>	
            		 
 		            
 		                    
 		                    ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
        		
        		     
        		      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				       Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
										
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
					      	    		 	acs_show_msg_error(jsonData.msg_error);
							        	 	return;
				        			     }else{
        					         		form.getForm().reset();
							       	        grid.getStore().load();	
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
					
					    	}
            	   		  });		    
					        
				        }

			     },  {
                     xtype: 'button',
                    text: 'Reset',
		            iconCls: 'icon-button_black_repeat-16',
		            scale: 'small',	                     

		           handler: function() {
		              var form = this.up('form').getForm();
                	  form.reset();
			
			            }

			     }, '->',
			       {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			
 			       	        if(form.isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				cliente : <?php echo j($m_params->cliente); ?>
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error(jsonData.msg_error);
					      	    		return false;
					        			}else{
					        			   grid.getStore().load();
					        		   }
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               }
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			
 			       	        if(form.isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				cliente : <?php echo j($m_params->cliente); ?>
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							           if(jsonData.success == false){
					      	    			acs_show_msg_error('errore');
					      	    		return false;
					        			}else{
					        			   grid.getStore().load();
					        		   }
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               }
			            }

			     }
			     ]
		   }]
			 	 }  
		 ]
					 
					
					
	}
    
    ]}
    
    <?php 
    
    exit;
}