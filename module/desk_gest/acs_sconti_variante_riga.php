<?php
require_once "../../config.inc.php";

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $ar_ins['TVDTUM'] = oggi_AS_date();
    $ar_ins['TVUSUM'] = substr($auth->get_user(), 0, 8);
    
    $ar_ins['TVDESC']  = $form_values->descrizione;
    $ar_ins['TVLIST']  = $form_values->listino;
    $ar_ins['TVVALU']  = $form_values->valuta;
    
    if(strlen($form_values->val_ini_df) > 0)
        $ar_ins['TVDTVI']  = $form_values->val_ini_df;
    if(strlen($form_values->val_fin_df) > 0)
        $ar_ins['TVDTVF']  = $form_values->val_fin_df;
    if(strlen($form_values->check_data) > 0)
        $ar_ins['TVCKDT']  = $form_values->check_data;
    if(strlen($form_values->gr_doc) > 0)
        $ar_ins['TVGRDO']  = $form_values->gr_doc;
    if(strlen($form_values->gr_doc2) > 0)
        $ar_ins['TVGRD2']  = $form_values->gr_doc2;
    if(strlen($form_values->gr_doc3) > 0)
        $ar_ins['TVGRD3']  = $form_values->gr_doc3;
   // if(strlen($form_values->var1) > 0)
        $ar_ins['TVVAR01'] = $form_values->var1;
  //  if(strlen($form_values->van1) > 0)
        $ar_ins['TVVAL01'] = $form_values->van1;
  //  if(strlen($form_values->var2) > 0)
        $ar_ins['TVVAR02'] = $form_values->var2;
  //  if(strlen($form_values->van2) > 0)
        $ar_ins['TVVAL02'] = $form_values->van2;
    if(!is_null($form_values->tipo1))
        $ar_ins['TVTIP01'] = $form_values->tipo1;
    if(!is_null($form_values->tipo2))
        $ar_ins['TVTIP02'] = $form_values->tipo2;
                                
    //altro
    if(strlen($form_values->TVVALO) > 0)
        $ar_ins['TVVALO']   = sql_f($form_values->TVVALO);
    if(strlen($form_values->TVTPRZ) > 0)
        $ar_ins['TVTPRZ'] = $form_values->TVTPRZ;
    if(strlen($form_values->TVTPRG) > 0)
        $ar_ins['TVTPRG'] = $form_values->TVTPRG;
    if(strlen($form_values->TVISMI) > 0)
        $ar_ins['TVISMI']   = sql_f($form_values->TVISMI);
    if(strlen($form_values->TVISMA) > 0)
        $ar_ins['TVISMA']   = sql_f($form_values->TVISMA);
        $ar_ins['TVTISE'] = $form_values->TVTISE;
    if(strlen($form_values->TVIOMI) > 0)
        $ar_ins['TVIOMI']   = sql_f($form_values->TVIOMI);
    if(strlen($form_values->TVIOMA) > 0)
        $ar_ins['TVIOMA']   = sql_f($form_values->TVIOMA);
    if(strlen($form_values->TVDIV1) > 0)
        $ar_ins['TVDIV1'] = $form_values->TVDIV1;
    if(strlen($form_values->TVDIV2) > 0)
        $ar_ins['TVDIV2'] = $form_values->TVDIV2;
    if(strlen($form_values->TVDIV3) > 0)
        $ar_ins['TVDIV3'] = $form_values->TVDIV3;
    if(strlen($form_values->TVTSEL) > 0)
        $ar_ins['TVTSEL'] = $form_values->TVTSEL;
    if(strlen($form_values->TVSEL1) > 0)
        $ar_ins['TVSEL1'] = $form_values->TVSEL1;
    if(strlen($form_values->TVSEL2) > 0)
        $ar_ins['TVSEL2'] = $form_values->TVSEL2;
    if(strlen($form_values->TVSEL3) > 0)
        $ar_ins['TVSEL3'] = $form_values->TVSEL3;
    if(strlen($form_values->TVSEL4) > 0)
        $ar_ins['TVSEL4'] = $form_values->TVSEL4;
    if(strlen($form_values->TVSEL5) > 0)
        $ar_ins['TVSEL5'] = $form_values->TVSEL5;
    if(strlen($form_values->TVSSEL) > 0)
        $ar_ins['TVSSEL'] = $form_values->TVSSEL;
    if(strlen($form_values->TVTPME) > 0)
        $ar_ins['TVTPME'] = $form_values->TVTPME;
    if(strlen($form_values->TVMER1) > 0)
        $ar_ins['TVMER1'] = $form_values->TVMER1;
    if(strlen($form_values->TVMER2) > 0)
        $ar_ins['TVMER2'] = $form_values->TVMER2;
    if(strlen($form_values->TVMER3) > 0)
        $ar_ins['TVMER3'] = $form_values->TVMER3;
    if(strlen($form_values->TVMER4) > 0)
        $ar_ins['TVMER4'] = $form_values->TVMER4;
    if(strlen($form_values->TVMER5) > 0)
        $ar_ins['TVMER5'] = $form_values->TVMER5;
    if(strlen($form_values->TVSEME) > 0)
        $ar_ins['TVSEME'] = $form_values->TVSEME;
    if(strlen($form_values->TVECAM) > 0)
        $ar_ins['TVECAM'] = $form_values->TVECAM;
    if(strlen($form_values->TVEPRO) > 0)
        $ar_ins['TVEPRO'] = $form_values->TVEPRO;
                                    
    if(strlen($form_values->TVSAGE) > 0)
        $ar_ins['TVSAGE'] = $form_values->TVSAGE;
    if(strlen($form_values->TVAGE1) > 0)
        $ar_ins['TVAGE1'] = $form_values->TVAGE1;
    if(strlen($form_values->TVAGE2) > 0)
        $ar_ins['TVAGE2'] = $form_values->TVAGE2;
    if(strlen($form_values->TVAGE3) > 0)
        $ar_ins['TVAGE3'] = $form_values->TVAGE3;
    if(strlen($form_values->TVAGE4) > 0)
        $ar_ins['TVAGE4'] = $form_values->TVAGE4;
    if(strlen($form_values->TVAGE5) > 0)
        $ar_ins['TVAGE5'] = $form_values->TVAGE5;
    
    if($m_params->type != 'T'){
        if(strlen($form_values->TVSPAG) > 0)
            $ar_ins['TVSPAG'] = $form_values->TVSPAG;
        for($a = 1; $a <= 10; $a++){ 
            if($a == 10)
                $campo = "TVPG10";
            else
                $campo = "TVPG0{$a}";
            if(strlen($form_values->$campo) > 0)
                $ar_ins["{$campo}"] = acs_toDb($form_values->$campo);
        }
    }
                         
   
    $articolop = '';
    for($i = 0; $i <= 14; $i++){
        $filtrop = 'artp_'.$i;
        if(trim($form_values->$filtrop) != '')
            $articolop .= $form_values->$filtrop;
    }
    $ar_ins['TVARVA'] = $form_values->art_riga;
    $ar_ins['TVARTI'] = $articolop;
    
    $ar_ins['TVDTGE'] = oggi_AS_date();
    $ar_ins['TVUSGE'] = $auth->get_user();
    $ar_ins["TVDT"]   = $id_ditta_default;
   
    if($m_params->cliente != ''){
        $ar_ins["TVCCON"] = $m_params->cliente;
        $ar_ins["TVTPTB"] = "P";
    }else{
        $ar_ins["TVTPTB"] = $m_params->type; //T o G
    }
    
    
    $ar_ins["TVPROG"] = $main_module->next_num('XSPM');
    
    
    $sql = "INSERT INTO {$cfg_mod_Gest['file_xspm']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $form_values = $m_params->form_values;
    
    $ar_ins['TVDTUM'] = oggi_AS_date();
    $ar_ins['TVUSUM'] = substr($auth->get_user(), 0, 8);
    
    $ar_ins['TVDESC']  = $form_values->descrizione;
    $ar_ins['TVLIST']  = $form_values->listino;
    $ar_ins['TVVALU']  = $form_values->valuta;
    
    if(strlen($form_values->val_ini_df) > 0)
        $ar_ins['TVDTVI']  = $form_values->val_ini_df;
    if(strlen($form_values->val_fin_df) > 0)
        $ar_ins['TVDTVF']  = $form_values->val_fin_df;
    if(strlen($form_values->check_data) > 0)
        $ar_ins['TVCKDT']  = $form_values->check_data;
    if(strlen($form_values->gr_doc) > 0)
        $ar_ins['TVGRDO']  = $form_values->gr_doc;
    if(strlen($form_values->gr_doc2) > 0)
        $ar_ins['TVGRD2']  = $form_values->gr_doc2;
    if(strlen($form_values->gr_doc3) > 0)
        $ar_ins['TVGRD3']  = $form_values->gr_doc3;
    //if(strlen($form_values->var1) > 0)
        $ar_ins['TVVAR01'] = $form_values->var1;
  //  if(strlen($form_values->van1) > 0)
        $ar_ins['TVVAL01'] = $form_values->van1;
  //  if(strlen($form_values->var2) > 0)
        $ar_ins['TVVAR02'] = $form_values->var2;
   // if(strlen($form_values->van2) > 0)
        $ar_ins['TVVAL02'] = $form_values->van2;
    if(!is_null($form_values->tipo1))
        $ar_ins['TVTIP01'] = $form_values->tipo1;
    if(!is_null($form_values->tipo2))
        $ar_ins['TVTIP02'] = $form_values->tipo2;
    
    //altro
    if(strlen($form_values->TVVALO) > 0)
        $ar_ins['TVVALO']   = sql_f($form_values->TVVALO);
    if(strlen($form_values->TVTPRZ) > 0)
        $ar_ins['TVTPRZ'] = $form_values->TVTPRZ;
    if(strlen($form_values->TVTPRG) > 0)
        $ar_ins['TVTPRG'] = $form_values->TVTPRG;
    if(strlen($form_values->TVISMI) > 0)
        $ar_ins['TVISMI']   = sql_f($form_values->TVISMI);
    if(strlen($form_values->TVISMA) > 0)
        $ar_ins['TVISMA']   = sql_f($form_values->TVISMA);
    $ar_ins['TVTISE'] = $form_values->TVTISE;
    if(strlen($form_values->TVIOMI) > 0)
        $ar_ins['TVIOMI']   = sql_f($form_values->TVIOMI);
    if(strlen($form_values->TVIOMA) > 0)
        $ar_ins['TVIOMA']   = sql_f($form_values->TVIOMA);
    if(strlen($form_values->TVDIV1) > 0)
        $ar_ins['TVDIV1'] = $form_values->TVDIV1;
    if(strlen($form_values->TVDIV2) > 0)
        $ar_ins['TVDIV2'] = $form_values->TVDIV2;
    if(strlen($form_values->TVDIV3) > 0)
        $ar_ins['TVDIV3'] = $form_values->TVDIV3;
    if(strlen($form_values->TVTSEL) > 0)
        $ar_ins['TVTSEL'] = $form_values->TVTSEL;
    if(strlen($form_values->TVSEL1) > 0)
        $ar_ins['TVSEL1'] = $form_values->TVSEL1;
    if(strlen($form_values->TVSEL2) > 0)
        $ar_ins['TVSEL2'] = $form_values->TVSEL2;
    if(strlen($form_values->TVSEL3) > 0)
        $ar_ins['TVSEL3'] = $form_values->TVSEL3;
    if(strlen($form_values->TVSEL4) > 0)
        $ar_ins['TVSEL4'] = $form_values->TVSEL4;
    if(strlen($form_values->TVSEL5) > 0)
        $ar_ins['TVSEL5'] = $form_values->TVSEL5;
    if(strlen($form_values->TVSSEL) > 0)
        $ar_ins['TVSSEL'] = $form_values->TVSSEL;
    if(strlen($form_values->TVTPME) > 0)
        $ar_ins['TVTPME'] = $form_values->TVTPME;
    if(strlen($form_values->TVMER1) > 0)
        $ar_ins['TVMER1'] = $form_values->TVMER1;
    if(strlen($form_values->TVMER2) > 0)
        $ar_ins['TVMER2'] = $form_values->TVMER2;
    if(strlen($form_values->TVMER3) > 0)
        $ar_ins['TVMER3'] = $form_values->TVMER3;
    if(strlen($form_values->TVMER4) > 0)
        $ar_ins['TVMER4'] = $form_values->TVMER4;
    if(strlen($form_values->TVMER5) > 0)
        $ar_ins['TVMER5'] = $form_values->TVMER5; 
    if(strlen($form_values->TVSEME) > 0)
        $ar_ins['TVSEME'] = $form_values->TVSEME;
    if(strlen($form_values->TVECAM) > 0)
        $ar_ins['TVECAM'] = $form_values->TVECAM;
    if(strlen($form_values->TVEPRO) > 0)
        $ar_ins['TVEPRO'] = $form_values->TVEPRO;
    
    if($m_params->cliente == ''){
        if(strlen($form_values->TVSAGE) > 0)
            $ar_ins['TVSAGE'] = $form_values->TVSAGE;
        if(strlen($form_values->TVAGE1) > 0)
            $ar_ins['TVAGE1'] = $form_values->TVAGE1;
        if(strlen($form_values->TVAGE2) > 0)
            $ar_ins['TVAGE2'] = $form_values->TVAGE2;
        if(strlen($form_values->TVAGE3) > 0)
            $ar_ins['TVAGE3'] = $form_values->TVAGE3;
        if(strlen($form_values->TVAGE4) > 0)
            $ar_ins['TVAGE4'] = $form_values->TVAGE4;
        if(strlen($form_values->TVAGE5) > 0)
            $ar_ins['TVAGE5'] = $form_values->TVAGE5; 
    }
    
    if($m_params->type != 'T'){
        if(strlen($form_values->TVSPAG) > 0)
            $ar_ins['TVSPAG'] = $form_values->TVSPAG;
        for($a = 1; $a <= 10; $a++){
            if($a == 10)
                $campo = "TVPG10";
            else
                $campo = "TVPG0{$a}";
            if(strlen($form_values->$campo) > 0)
                $ar_ins["{$campo}"] = $form_values->$campo;
        }
    }
    
  
  
    $articolop = '';
    for($i = 0; $i <= 14; $i++){
      $filtrop = 'artp_'.$i;
      if(trim($form_values->$filtrop) != '')
            $articolop .= $form_values->$filtrop;
    }
    $ar_ins['TVARVA'] = $form_values->art_riga;
    $ar_ins['TVARTI'] = $articolop;
    
    
    $sql = "UPDATE {$cfg_mod_Gest['file_xspm']} TV
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(TV) = '{$form_values->rrn}'";
     
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_delete'){
    
    $m_params = acs_m_params_json_decode();
    
    $rrn= $m_params->form_values->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_Gest['file_xspm']} TV WHERE RRN(TV) = '{$rrn}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    
    $sql_where = "";
    $cliente = $m_params->open_request->cliente;
    $type    = $m_params->open_request->type;
    
    if(strlen($cliente) > 0)
        $sql_where .= " AND TVTPTB = 'P' AND TVCCON = '{$cliente}'";
        
    if(strlen($type) > 0)
        $sql_where .= " AND TVTPTB = '{$type}'";  // T o G
    
    $sql = " SELECT RRN(TV) AS RRN, TV.*
             FROM {$cfg_mod_Gest['file_xspm']} TV
             WHERE TVDT = '{$id_ditta_default}' {$sql_where}";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = array();
        $nr = array_map('rtrim', $row);
        $nr['rrn'] = $row['RRN'];
        $nr['listino'] = trim($row['TVLIST']);
        $nr['descrizione'] = trim($row['TVDESC']);
        $val_listino   = find_TA_sys('BITL', $nr['listino']);
        $nr['t_listino'] = "[".$nr['listino']."] ".trim($val_listino[0]['text']);
        
        $nr['valuta']  = trim($row['TVVALU']);
        $val_valuta   = find_TA_sys('VUVL', $nr['valuta']);
        $nr['t_valuta'] = "[".$nr['valuta']."] ".trim($val_valuta[0]['text']);
        
        $nr['val_ini'] = trim($row['TVDTVI']);
        $nr['val_fin'] = trim($row['TVDTVF']);
        
        $nr['val_ini_df']  = print_date($row['TVDTVI']);
        $nr['val_fin_df']  = print_date($row['TVDTVF']);
        
        $nr['check_data'] = trim($row['TVCKDT']);
     
        $nr['gr_doc'] = trim($row['TVGRDO']);
        $val_grdoc   = find_TA_sys('RDOC', $nr['gr_doc']);
        $nr['t_gr_doc'] = trim($val_grdoc[0]['text']);
        
        $nr['gr_doc2'] = trim($row['TVGRD2']);
        $val_grdoc2   = find_TA_sys('RDOC', $nr['gr_doc2']);
        $nr['t_gr_doc2'] = trim($val_grdoc2[0]['text']);
        
        $nr['gr_doc3'] = trim($row['TVGRD3']);
        $val_grdoc3   = find_TA_sys('RDOC', $nr['gr_doc3']);
        $nr['t_gr_doc3'] = trim($val_grdoc3[0]['text']);
        
        $nr['art_riga'] = trim($row['TVARVA']);
        $nr['art_pos'] = trim($row['TVARTI']);
        for($i=0; $i <= 14; $i++){
            $nr["artp_{$i}"] = $nr['art_pos'][$i];
        }
       
        $nr['var1'] = trim($row['TVVAR01']);
        $val_var1   = find_TA_sys('PUVR', $nr['var1']);
      
        $nr['van1'] = trim($row['TVVAL01']);
        
        $nr['var2'] = trim($row['TVVAR02']);
        $val_var2   = find_TA_sys('PUVR', $nr['var2']);
       
        $nr['van2'] = trim($row['TVVAL02']);
        
        $val_van1   = find_TA_sys('PUVN', $nr['van1'], null, $nr['var1']);
        $val_van2   = find_TA_sys('PUVN', $nr['van2'], null, $nr['var2']);
        $nr['t_van1'] = trim($val_van1[0]['text']);
        $nr['t_van2'] = trim($val_van2[0]['text']);
        
        $nr['tipo1'] = trim($row['TVTIP01']);
        $tipo1 = find_TA_sys('PUTI', trim($row['TVTIP01']));
        
        $nr['tipo2'] = trim($row['TVTIP02']);
        $tipo2 = find_TA_sys('PUTI', trim($row['TVTIP02']));
        
        if(trim($nr['var1']) != '')
            $nr['out_v1'] = "[{$nr['var1']}] ".trim($val_var1[0]['text']). ", [{$nr['van1']}] ".$nr['t_van1'];
        if(trim($nr['var2']) != '')
            $nr['out_v2'] = "[{$nr['var2']}] ".trim($val_var2[0]['text']). ", [{$nr['van2']}] ".$nr['t_van2'];
        
        if(trim($nr['var1']) != '' || trim($nr['tipo1']) != '')
            $nr['variante'] = "{$nr['var1']}, {$nr['van1']}, {$nr['tipo1']}";
        if(trim($nr['var2']) != '' || trim($nr['tipo2']) != '')
            $nr['variante'] .= "<br> {$nr['var2']}, {$nr['van2']}, {$nr['tipo2']}";
                    
       
        if($nr['tipo1'] != '')
            $nr['tipologia'] = "[".trim($row['TVTIP01'])."] ".$tipo1[0]['text'];
        if($nr['tipo2'] != '')
            $nr['tipologia'] .= "<br>[".trim($row['TVTIP02'])."] ".$tipo2[0]['text'];
        
        $nr['agente'] = trim($row['TVAGE1']);
       
        if(trim($row['TVAGE2']))
            $nr['agente'] .= "<br>".trim($row['TVAGE2']);
        if(trim($row['TVAGE3']))
            $nr['agente'] .= "<br>".trim($row['TVAGE3']);
        if(trim($row['TVAGE4']))
            $nr['agente'] .= "<br>".trim($row['TVAGE4']);
        if(trim($row['TVAGE5']))
            $nr['agente'] .= "<br>".trim($row['TVAGE5']);
        
        $nr['pagamenti'] = trim($row['TVPG01']);
        for($a = 2; $a <= 9; $a++){
          if(trim($row["TVPG0{$a}"]) != '')
            $nr['pagamenti'] .= "<br>".trim($row["TVPG0{$a}"]);
        }
        if(trim($row['TVPG10']) != '')
            $nr['pagamenti'] .= "<br>".trim($row['TVPG10']);
        
       
        $ar[] = $nr;
        
    }
    
    
    
    echo acs_je($ar);
    exit;
}


if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    if($m_params->type == 'T'){
        $title = "Sconti VR";
        $d_title = "Dettaglio sconti per variante/riga";
        $art_title = 'Art. riga promo';
    }else{
        $title = "Sconti pagamento";
        $d_title = "Dettaglio sconti su condizioni pagamento";
        $art_title = 'Art. riga sconto';
    }
    
    
    ?>

{"success":true, "items": [

        {
			xtype: 'panel',
			<?php if($m_params->cliente == ''){?>
			    title: <?php echo j($title); ?>,
                <?php echo make_tab_closable(); ?>,
            <?php }?>
        	layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
							{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            autoScroll: true,
            title: '',
            flex:0.7,
            items: [
						
				{
			xtype: 'grid',
			title: '',
			flex:0.75,
			autoScroll: true,
	        loadMask: true,
	        stateful: true,
	        stateId: 'seleziona-sconti-riga',
	        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
	        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],	
	        store: {
			//xtype: 'store',
			autoLoad:true,

					proxy: {
					   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
					   method: 'POST',								
					   type: 'ajax',

				       actionMethods: {
				          read: 'POST'
				        },
				        
				        
				           extraParams: {
							 open_request: <?php echo acs_je($m_params) ?>
        				},
        				
        				doRequest: personalizza_extraParams_to_jsonData, 
			
					   reader: {
			            type: 'json',
						method: 'POST',						            
			            root: 'root'						            
			        }
				},
				
    			fields: [
						  'rrn', 'listino', 'valuta', 'val_ini', 'val_fin', 'val_ini_df', 'val_fin_df',
                          'check_data', 'cliente', 'd_cli', 'gr_doc', 'gr_doc2', 'gr_doc3', 'var1', 'van1', 'var2', 'van2', 't_var1', 't_var2', 
                          't_van1', 't_van2', 'variante', 't_gr_doc', 't_gr_doc2', 't_gr_doc3', 't_valuta', 't_listino', 'agente', 't_agente',
                          'tipo1', 'tipo2', 'tipologia', 'out_v1', 'out_v2', 'art_riga', 'TVVALO', 'TVTPRZ', 'TVTPRG',
                          'TVISMI', 'TVISMA', 'TVTISE', 'TVIOMI', 'TVIOMA', 'TVDIV1', 'TVDIV2', 'TVDIV3', 'TVTSEL', 'TVDTGE', 'TVDTUM', 'TVUSGE', 'TVUSUM',
                          'TVSEL1', 'TVSEL2', 'TVSEL3', 'TVSEL4', 'TVSEL5', 'TVSSEL', 'TVTPME', 'TVMER1', 'TVMER2', 'TVMER3', 'TVMER4', 'TVMER5',
                          'TVSEME', 'TVARTI', 'TVECAM', 'TVEPRO', 'descrizione', 'TVAGE1', 'TVAGE2', 'TVAGE3', 'TVAGE4', 'TVAGE5', 'TVSAGE', 'agente'
                          <?php for($i=0; $i <= 14; $i++){ 
                              echo ", 'art_{$i}'"; 
                              echo ", 'artp_{$i}'";
                          }?>
                          
                         <?php 
                         for($a = 1; $a <= 9; $a++){ 
                             echo ", 'TVPG0{$a}'"; 
                         }
                         ?> , 'TVPG10', 'TVSPAG', 'pagamenti'
    			]							
						
			}, //store
			
			      columns: [	
			      {
	                header   : 'Descrizione',
	                dataIndex: 'descrizione',
	                flex : 1,
	                }, {
	                header   : 'List.',
	                dataIndex: 'listino',
	                width : 40,
	                },
	                {
	                header   : 'Val.',
	                dataIndex: 'valuta',
	                width : 40,
	                },
	                {
                    header   : 'C',
                    dataIndex: 'check_data',
                    width: 20,
                    tooltip : 'Data controllo validit&agrave;',
                    renderer: function(value, metaData, record){
                          return '[' + value + ']';   
            		  }
                    },
	                {header: 'Validit&agrave;',
                	columns: [
                    {header: 'Iniziale', dataIndex: 'val_ini', renderer: date_from_AS, width: 70, sortable : true},
                	{header: 'Finale', dataIndex: 'val_fin', renderer: date_from_AS, width: 70, sortable : true}
                	]}
                
                   
                  ,{ header: 'Variante'
                  , dataIndex: 'variante'
                  , flex : 1
                  , filter: {type: 'string'}, filterable: true
                  
                  },
                
                  {
	                header   : 'Articolo',
	                dataIndex: 'art_riga',
	                width : 120,
                  },
                  
                 <?php if($m_params->cliente == ''){?>
                    {
	                header   : 'Agente',
	                dataIndex: 'agente',
	                width : 80,
                  },
                  <?php }?>
                   <?php if($m_params->type != 'T'){?>
                   {
	                header   : 'Pagamenti',
	                dataIndex: 'pagamenti',
	                width : 80,
                  },
                  <?php }?>
                  
          	  {header: 'Immissione',
            	 columns: [
            	 {header: 'Data', dataIndex: 'TVDTGE', renderer: date_from_AS, width: 70, sortable : true,
            	 renderer: function(value, metaData, record){
            	         if (record.get('TVDTGE') != record.get('TVDTUM')) 
            	          metaData.tdCls += ' grassetto';
            	 
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TVDTUM')) + ', Utente ' +record.get('TVUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return date_from_AS(value);	
            	}
            	 
            	 }
            	 ,{header: 'Utente', dataIndex: 'TVUSGE', width: 70, sortable : true,
            	  renderer: function(value, metaData, record){
            	  
            	  		if (record.get('TVUSGE') != record.get('TVUSUM')) 
            	          metaData.tdCls += ' grassetto';
            	  
                         q_tip = 'Modifica: ' + date_from_AS(record.get('TVDTUM')) + ', Utente ' +record.get('TVUSUM');
            			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
            		     return value;	
            	} 
            	 }
            	 ]}
                 	 
	                     
	                
	                
	         ], listeners: {
	         
	         selectionchange: function(selModel, selected) { 
	               
	               if(selected.length > 0){
		               var form_dx = this.up('form').up('panel').down('#dx_form');
		               form_dx.getForm().reset();
		               rec_index = selected[0].index;
		               form_dx.getForm().findField('rec_index').setValue(rec_index);
	                   acs_form_setValues(form_dx, selected[0].data);
	                   
	                   }
		          }
		   	 }
				 
			, viewConfig: {
		        getRowClass: function(record, index) {
		                   		
		        		           		
		           return '';																
		         }   
		    }		
					       
		
		
		}
		]}, 
		
	
		    
		
		   {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: <?php echo j($d_title); ?>,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [
 		           {
					name: 'rrn',
					fieldLabel : 'rrn',
					xtype: 'textfield',
					hidden : true							
				   }, 
				     {xtype : 'textfield',name : 'rec_index',hidden : true},
				    {
                		xtype: 'tabpanel',
                		items: [
                		
                		{
        				xtype: 'panel',
        				title: 'Dettagli', 
        				autoScroll : true,
        				frame : true,
        				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
        				//width: 165,
        				items: [
        				{
                		name: 'descrizione',
                		xtype: 'textfield',
                		fieldLabel: 'Descrizione',
                		anchor: '-15',	
                		maxLength : 30
                		},
        				
        				{
            		name: 'listino',
            		xtype: 'combo',
            		fieldLabel: 'Listino',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
               		allowBlank: false,	
               		queryMode: 'local',
            	    minChars: 1, 											
            	  	anchor: '-15',		
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [								    
            		     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('BITL'), ''); ?>	
            		    ]
            		}
            		,listeners: { 
            	 		beforequery: function (record) {
                     		record.query = new RegExp(record.query, 'i');
                     		record.forceAll = true;
            			 }
            		 }
            		
                  },
            	 {
        		name: 'valuta',
        		xtype: 'combo',
        		fieldLabel: 'Valuta',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
           		allowBlank: false,	
           		queryMode: 'local',
        	    minChars: 1, 											
        	  	anchor: '-15',		
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [								    
        		     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('VUVL'), ''); ?>	
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
               	{ 
        		xtype: 'fieldcontainer',
        		flex:1,
        		anchor: '-15',
        		layout: { 	type: 'hbox',
        				    pack: 'start',
        				    align: 'stretch'},						
        		items: [
        		
        		{
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	   , fieldLabel: 'Validit&agrave; iniziale'
        	   , name: 'val_ini_df'
        	   , format: 'd/m/y'							   
        	   , submitFormat: 'Ymd'
        	   , allowBlank: false
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        	//	, anchor: '-15'
        	   , flex :1.3
        	   
        	}, {
        	   xtype: 'datefield'
        	   , startDay: 1 //lun.
        	   , fieldLabel: 'Finale'
        	   , labelAlign : 'right'
        	   , labelWidth : 40
        	   , name: 'val_fin_df'
        	   , format: 'd/m/y'						   
        	   , submitFormat: 'Ymd'
        	   , allowBlank: false
        	   , listeners: {
        	       invalid: function (field, msg) {
        	       Ext.Msg.alert('', msg);}
        		}
        	   , anchor: '-15'
        	   , flex :0.9
        	}]},
        	 {
        		name: 'check_data',
        		xtype: 'combo',
        		fieldLabel: 'Check data',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
           		//allowBlank: false,	
           		queryMode: 'local',
        	    minChars: 1, 											
        	  	anchor: '-15',		
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [								    
        		     <?php echo acs_ar_to_select_json($main_module->find_TA_std('CCT08'), ''); ?>	
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
        	  {
        		name: 'gr_doc',
        		xtype: 'combo',
        		fieldLabel: 'Gruppo docum. 1',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -', 
           		queryMode: 'local',
        	    minChars: 1, 											
        	  	anchor: '-15',		
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [	
        		    
        		    <?php if($cfg_mod_Gest['gruppo_documenti_ob'] == 'Y')
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), ''); 
               		      else
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '', true, 'N', 'Y'); 
               		 ?>	
               		
        		    							    
        		     
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
                {
        		name: 'gr_doc2',
        		xtype: 'combo',
        		fieldLabel: 'Gruppo docum. 2',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
        		queryMode: 'local',
        	    minChars: 1, 											
        	  	anchor: '-15',		
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [	
        		    
        		    <?php if($cfg_mod_Gest['gruppo_documenti_ob'] == 'Y')
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), ''); 
               		      else
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '', true, 'N', 'Y'); 
               		 ?>	
               		
        		    							    
        		     
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },  {
        		name: 'gr_doc3',
        		xtype: 'combo',
        		fieldLabel: 'Gruppo docum. 3',
        		forceSelection: true,								
        		displayField: 'text',
        		valueField: 'id',								
        		emptyText: '- seleziona -',
        		queryMode: 'local',
        	    minChars: 1, 											
        	  	anchor: '-15',		
        		store: {
        			editable: false,
        			autoDestroy: true,
        		    fields: [{name:'id'}, {name:'text'}],
        		    data: [	
        		    
        		    <?php if($cfg_mod_Gest['gruppo_documenti_ob'] == 'Y')
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), ''); 
               		      else
               		          echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), '', true, 'N', 'Y'); 
               		 ?>	
               		
        		    							    
        		     
        		    ]
        		}
        		,listeners: { 
        	 		beforequery: function (record) {
                 		record.query = new RegExp(record.query, 'i');
                 		record.forceAll = true;
        			 }
        		 }
        		
              },
                 <?php		      
                  $proc = new ApiProc();
        		  echo $proc->get_json_response(
        		      extjs_combo_dom_ris(array(
        		          'initial_value' => array('dom_c'  => $m_params->var1,
        		                                   'ris_c'  => $m_params->van1,
        		                                   'tipo_c' => "",
        		                                   'out_d'  => $m_params->out_v1),
        		          'risposta_a_capo' => true,
        		          'label' =>'Variabile/(1)<br>Variante', 
        		          'file_TA' => 'sys',
        		          'tipo_opz_risposta' => array('output_domanda' => true,  
        	                 'tipo_cf' => '',
        		             'opzioni' => array(
        		                  array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
        		              )),
        		          'dom_cf'  => 'var1',  'ris_cf' => 'van1'))
        		  );
        		  echo ",";
        		  
        		  ?>
        		  
        		    { name: 'tipo1',
					  xtype: 'combo',
				      fieldLabel: 'Tipologia 1',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUTI'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 }
        		  
        		  
        		  <?php
        		  echo ",";
        		  echo $proc->get_json_response(
        		      extjs_combo_dom_ris(array(
        		          'initial_value' => array('dom_c' => $m_params->row->var2,
        		                                   'ris_c' => $m_params->row->van2,
        		                                   'out_d' => $m_params->row->out_v2),
        		          'risposta_a_capo' => true,
        		          'label' =>'Variabile/(2)<br>Variante',
        		          'file_TA' => 'sys',
        		          'tipo_opz_risposta' => array('output_domanda' => true, 
        		             'tipo_cf' => '',
        		              'opzioni' => array(
        		                  array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
        		              )
        		              ),
        		          'dom_cf'  => 'var2',  'ris_cf' => 'van2'))
        		      );
        		  echo ", ";
        		  ?>
        		  
        		   { name: 'tipo2',
					  xtype: 'combo',
				      fieldLabel: 'Tipologia 2',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_sys_TA('PUTI'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 }	
				]},
				
				{
				xtype: 'panel',
				title: 'Articolo', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false , pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
			     {
            		name: 'art_riga',
            		xtype: 'textfield',
            		fieldLabel: <?php echo j($art_title); ?>,
            		//anchor: '-15',	
            		maxLength : 15,
                    width: 200
            		},
				   {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
						 <?php write_numberfield_std('TVVALO', 'Valore', $m_params->row->TVVALO, 0, array("flex_width" =>"width: 180")) ?>	
				       , <?php write_combo_std('TVTPRZ', 'Tipo valore', $m_params->row->TVTPRZ, acs_ar_to_select_json($main_module->find_TA_std('STPRZ', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array("labelWidth" => 70, 'labelAlign' => 'right')) ?>			
				      
						
					]}
					 , <?php write_combo_std('TVTPRG', 'Tipo riga', $m_params->row->TVTPRG, acs_ar_to_select_json(find_TA_sys('BTRD', null, null, null, null, null, 0, '', 'Y', 'Y'), '')) ?>	
					 , <?php write_combo_std('TVTISE', 'Tipo importo', $m_params->row->TVTISE, acs_ar_to_select_json($main_module->find_TA_std('STPIS', null, 'N', 'N', null, null, null, 'N', 'Y'), '')) ?>							
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
						 <?php write_numberfield_std('TVISMI', 'Imp. selez. min.', $m_params->row->TVISMI, null, array('flex_width' => "width: 170")); ?>	
				       , <?php write_numberfield_std('TVISMA', 'max', $m_params->row->TVISMA, null, array('flex_width' => "width: 120", "labelWidth" => 50, 'labelAlign' => 'right')) ?>	
					   
					]},
					
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
						 <?php write_numberfield_std('TVIOMI', 'Imp. ordine min.', $m_params->row->TVIOMI, null, array('flex_width' => "width: 170")) ?>	
				        , <?php write_numberfield_std('TVIOMA', 'max', $m_params->row->TVIOMA, null, array('flex_width' => "width: 120", "labelWidth" => 50, 'labelAlign' => 'right')) ?>	
						
					]},
					
					{
						xtype: 'fieldcontainer',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						items: [
						  <?php write_combo_std('TVDIV1', 'Divisione 1', $m_params->row->TVDIV1, acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), '')) ?>
						, <?php write_combo_std('TVDIV2', 'Divisione 2', $m_params->row->TVDIV2, acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), '')) ?>
						, <?php write_combo_std('TVDIV3', 'Divisione 3', $m_params->row->TVDIV3, acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), '')) ?>
						
					]},
					
					<?php if($m_params->type == 'T'){ ?>
					 {   xtype: 'fieldcontainer',
    		          layout: 'hbox',
    		      	  margin : '0 0 0 105',
    		      	  anchor: '-15',	
    		          items: [
    						<?php for ($i = 1; $i<=15; $i++){?>
    						{ xtype: 'displayfield', value : '<?php echo $i; ?>', flex : 0.06, fieldStyle:'font-size:10px;'},
    						<?php }?>	
    						 ]
    				}, { 
						xtype: 'fieldcontainer',
						margin : '0 0 5 0',
						anchor: '-15',
						layout: { 	type: 'hbox',
									pack: 'start',
									align: 'stretch',
									frame: true},
						defaults: {xtype: 'textfield', flex: 0.06, margin : '0 1 0 0', maxLength: 1},
						items: [
	                        {xtype: 'label', text: 'Art. posizionale:', flex : 0.36},
							<?php for ($i = 0; $i <= 14; $i++){
							    $artp = "artp_{$i}";
							    ?>
								{ name: '<?php echo "artp_{$i}"; ?>', 
								  listeners: {
								    
									'change': function(field){
									 value = this.getValue().toString();
									if (value.length == 1) {
										var nextfield = field.nextSibling();
										if (!Ext.isEmpty(nextfield))
											nextfield.focus(true, 0);
									  } 
									  
									  field.setValue(field.getValue().toUpperCase());
										
								  }
								}
							  },
							<?php }?>
							
						]						
					},
					
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
						  <?php write_combo_std('TVECAM', 'Escl. rig. camp.', $m_params->row->TVECAM, acs_ar_to_select_json($main_module->find_TA_std('ARY', null, 'N', 'N', null, null, null, 'N', 'Y'), '')) ?>
						, <?php write_combo_std('TVEPRO', 'Escl. promozioni', $m_params->row->TVEPRO, acs_ar_to_select_json($main_module->find_TA_std('ARY', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array("labelAlign" => "right") ) ?>
						
					]}
					<?php }?>
				
				]},
				<?php if($m_params->type == 'T'){?>
				{
				xtype: 'panel',
				title: 'Classificazioni', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				   { name: 'TVTSEL',
					  xtype: 'combo',
				      fieldLabel: 'Tipo class. cliente',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',
        			  labelWidth : 110,							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_TA_std('STPSC', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
        			    ]
        			},listeners: { 
    			 		beforequery: function (record) {
    	         		record.query = new RegExp(record.query, 'i');
    	         		record.forceAll = true;
    					},
    					change: function(field,newVal) {	
                  		if (!Ext.isEmpty(newVal)){
                  		    <?php for($i = 1; $i <= 5; $i++){?>
                  		    var combo = this.up('panel').down('#<?php echo "combo{$i}" ?>');
                  		    combo.store.proxy.extraParams.selez_cli = newVal;
                        	combo.store.load();  
                  		    <?php }?>                     		 
                         	                        
                         }
                         },
                         render: function(combo) {	
                           if (!Ext.isEmpty(combo.value))
                             c_value = combo.value;
                           else c_value = '1';
                              <?php for($i = 1; $i <= 5; $i++){?>
                      		    var combo = this.up('panel').down('#<?php echo "combo{$i}" ?>');
                      		    combo.store.proxy.extraParams.selez_cli = c_value;
                            	combo.store.load();  
                      		  <?php }?>  
                           
                           
                         }
 					}
				 }	
				 , <?php write_combo_std('TVSSEL', 'Includi/Escludi', $m_params->row->TVSSEL, acs_ar_to_select_json($main_module->find_TA_std('CHKIE', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array("labelWidth" => 110)) ?>,
				 ,	
				 
				 {name: 'TVSEL1',
            		xtype: 'combo',
            		flex: 1,
            		itemId : 'combo1',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',	
            		fieldLabel : 'Classif. cliente',
            		labelWidth : 110,							
            		emptyText: '- seleziona -',
            	    store: {
        			        autoLoad: false,
        					proxy: {
        			            type: 'ajax',
        			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
        			            actionMethods: {
        				          read: 'POST'
        			        },
        	                	extraParams: {
        	    		    		selez_cli: '',
        	    				},				            
        			            doRequest: personalizza_extraParams_to_jsonData, 
        			            reader: {
        			            type: 'json',
        						method: 'POST',						            
        				            root: 'root'						            
        				        }
        			        },       
        					fields: ['id', 'text'],		             	
        	            },
            	          listeners: {
            	           afterrender: function(comp){
                                data = [
                                    {id: <?php echo j($m_params->row->TVSEL1)?>, text: <?php echo j($m_params->row->TVSEL1) ?>}
                                ];
                                comp.store.loadData(data);
                                comp.setValue(<?php echo j($m_params->row->TVSEL1)?>);
                            }
                           
                     }
            			
               		}
				 ,
				<?php for($i = 2; $i <= 5; $i++){
					$campo = "TVSEL{$i}";?>
				    {name: <?php echo j($campo); ?>,
            		xtype: 'combo',
            		margin : '0 0 5 115',
            		flex: 1,
            		itemId : <?php echo j("combo{$i}")?>,
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
            	    store: {
        			        autoLoad: false,
        					proxy: {
        			            type: 'ajax',
        			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
        			            actionMethods: {
        				          read: 'POST'
        			        },
        	                	extraParams: {
        	    		    		selez_cli: '',
        	    				},				            
        			            doRequest: personalizza_extraParams_to_jsonData, 
        			            reader: {
        			            type: 'json',
        						method: 'POST',						            
        				            root: 'root'						            
        				        }
        			        },       
        					fields: ['id', 'text'],		             	
        	            },
            	          listeners: {
            	          afterrender: function(comp){
                                data = [
                                    {id: <?php echo j($m_params->row->$campo)?>, text: <?php echo j($m_params->row->$campo) ?>}
                                ];
                                comp.store.loadData(data);
                                comp.setValue(<?php echo j($m_params->row->$campo)?>);
                            }
                           
                     }
            			
               		},
					<?php }?>
					
					{ name: 'TVTPME',
					  xtype: 'combo',
				      fieldLabel: 'Tipo class. articolo',
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',
        			  labelWidth : 110,							
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json($main_module->find_TA_std('TRAGR', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
        			    ]
        			},listeners: { 
    			 		beforequery: function (record) {
    	         		record.query = new RegExp(record.query, 'i');
    	         		record.forceAll = true;
    					},
    					change: function(field,newVal) {	
                  		if (!Ext.isEmpty(newVal)){
                  		    <?php for($i = 1; $i <= 5; $i++){?>
                  		    var combos = this.up('panel').down('#<?php echo "combos{$i}" ?>');
                  		    combos.store.proxy.extraParams.selez_art = newVal;
                        	combos.store.load();  
                  		    <?php }?>                     		 
                         	                        
                         }
                        },
                        render: function(combo) {	
                           if (!Ext.isEmpty(combo.value))
                             c_value = combo.value;
                           else c_value = 'C';
                           
                              <?php for($i = 1; $i <= 5; $i++){?>
                      		    var combos = this.up('panel').down('#<?php echo "combos{$i}" ?>');
                      		    combos.store.proxy.extraParams.selez_art = c_value;
                            	combos.store.load();  
                      		  <?php }?>  
                           
                           
                         }
 					}
				 }	
				 , <?php write_combo_std('TVSEME', 'Includi/Escludi', $m_params->row->TVSEME, acs_ar_to_select_json($main_module->find_TA_std('CHKIE', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array("labelWidth" => 110)) ?>,
				 ,	
				 
				 {name: 'TVMER1',
            		xtype: 'combo',
            		flex: 1,
            		itemId : 'combos1',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',	
            		fieldLabel : 'Classif. articolo',
            		labelWidth : 110,							
            		emptyText: '- seleziona -',
            	    store: {
        			        autoLoad: false,
        					proxy: {
        			            type: 'ajax',
        			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
        			            actionMethods: {
        				          read: 'POST'
        			        },
        	                	extraParams: {
        	    		    		selez_art: '',
        	    				},				            
        			            doRequest: personalizza_extraParams_to_jsonData, 
        			            reader: {
        			            type: 'json',
        						method: 'POST',						            
        				            root: 'root'						            
        				        }
        			        },       
        					fields: ['id', 'text'],		             	
        	            },
            	          listeners: {
            	                 afterrender: function(comp){
                                data = [
                                    {id: <?php echo j($m_params->row->TVMER1)?>, text: <?php echo j($m_params->row->TVMER1) ?>}
                                ];
                                comp.store.loadData(data);
                                comp.setValue(<?php echo j($m_params->row->TVMER1)?>);
                            }
                           
                     }
            			
               		}
				 ,
				<?php for($i = 2; $i <= 5; $i++){
					$campo = "TVMER{$i}";?>
				    {name: <?php echo j($campo); ?>,
            		xtype: 'combo',
            		margin : '0 0 5 115',
            		flex: 1,
            		itemId : <?php echo j("combos{$i}")?>,
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',								
            		emptyText: '- seleziona -',
            	    store: {
        			        autoLoad: false,
        					proxy: {
        			            type: 'ajax',
        			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
        			            actionMethods: {
        				          read: 'POST'
        			        },
        	                	extraParams: {
        	    		    		selez_cli: '',
        	    				},				            
        			            doRequest: personalizza_extraParams_to_jsonData, 
        			            reader: {
        			            type: 'json',
        						method: 'POST',						            
        				            root: 'root'						            
        				        }
        			        },       
        					fields: ['id', 'text'],		             	
        	            },
            	          listeners: {
            	                 afterrender: function(comp){
                                data = [
                                    {id: <?php echo j($m_params->row->$campo)?>, text: <?php echo j($m_params->row->$campo) ?>}
                                ];
                                comp.store.loadData(data);
                                comp.setValue(<?php echo j($m_params->row->$campo)?>);
                            }
                           
                     }
            			
               		},
					<?php }?>
					
				
				]},
				<?php }?>
				
				<?php if($m_params->cliente == ''){?>
				{
				xtype: 'panel',
				title: 'Agenti', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
				 <?php write_combo_std('TVSAGE', 'Incl./Escl.', $m_params->row->TVSAGE, acs_ar_to_select_json($main_module->find_TA_std('CHKIE', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array("labelWidth" => 60)) ?>,
				   {name: 'TVAGE1',
            		xtype: 'combo',
            		flex: 1,
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',	
            		fieldLabel : 'Agente',
            		labelWidth : 60,		
            		queryMode: 'local',
            		minChars: 1, 					
            		emptyText: '- seleziona -',
            	     store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
            			
               		},
				
				<?php for($i = 2; $i <= 5; $i++){ 
                    $campo = "TVAGE{$i}";?>
				 { name: <?php echo j($campo); ?>,
					  xtype: 'combo',
				      margin : '0 0 5 65',
            		  flex: 1,
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',						
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 },	
				<?php }?>
				
				]}
				<?php }?>
				
				<?php if($m_params->type != 'T'){?>
				, {
				xtype: 'panel',
				title: 'Pagamenti', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				//width: 165,
				items: [
				
				<?php write_combo_std('TVSPAG', 'Incl./Escl.', $m_params->row->TVSPAG, acs_ar_to_select_json($main_module->find_TA_std('CHKIE', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array("labelWidth" => 70)) ?>,
				
				   {name: 'TVPG01',
            		xtype: 'combo',
            		flex: 1,
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',	
            		fieldLabel : 'Pagamento',
            		labelWidth : 70,		
            		queryMode: 'local',
            		minChars: 1, 					
            		emptyText: '- seleziona -',
            	     store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json(find_TA_sys('CUCP', null, null, null, null, null, 0, "", 'Y', 'Y'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
            			
               		},
				
				<?php for($i = 2; $i <= 10; $i++){ 
				    if($i == 10)
				        $campo = "TVPG10";
				    else
                        $campo = "TVPG0{$i}";?>
				 { name: <?php echo j($campo); ?>,
					  xtype: 'combo',
				      margin : '0 0 5 75',
            		  flex: 1,
				      forceSelection: true,								
    			      displayField: 'text',
        			  valueField: 'id',						
        			  emptyText: '- seleziona -',
        		      queryMode: 'local',
                      minChars: 1,
                      anchor: '-15',
        			  store: {
        				editable: false,
        				autoDestroy: true,
        			    fields: [{name:'id'}, {name:'text'}],
        			    data: [								    
        			     <?php echo acs_ar_to_select_json(find_TA_sys('CUCP', null, null, null, null, null, 0, "", 'Y', 'Y'), '', true, 'N', 'Y'); ?>	
        			    ]
        			},listeners: { 
			 		beforequery: function (record) {
	         		record.query = new RegExp(record.query, 'i');
	         		record.forceAll = true;
					 }
 					}
				 },	
				<?php }?>
				
				]}
				<?php }?>
				
				]}
                			
                	 ],
			
			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     }, '->',
                 {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				c_art : '<?php echo $m_params->c_art; ?>',
	        				tipo_listino : '<?php echo$m_params->tipo_list; ?>',
	        				cliente : <?php echo j($m_params->cliente); ?>,
				        	type : <?php echo j($m_params->type); ?> 
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        form.getForm().reset();
					        grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values,
			        				cliente : <?php echo j($m_params->cliente); ?>,
				        			type : <?php echo j($m_params->type); ?> 
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
		  
			 	 }
					
						   
						
						
				
						

								
					
					
					 ],
					 
					
					
	}
	
]}


<?php

exit;
}

   
if ($_REQUEST['fn'] == 'get_data_tab'){
   
   //$where = "";
   if($m_params->selez_cli == 1)
      $where = " AND TAID = 'CUS1'";
   if($m_params->selez_cli == 2)
      $where = " AND TAID = 'CUS2'";
   if($m_params->selez_cli == 3)
      $where = " AND TAID = 'CUS3'";
   if($m_params->selez_cli == 4)
      $where = " AND TAID = 'CUS4'";
   
  if($m_params->selez_art == 'C')
      $where = " AND TAID = 'MUCM'";
  if($m_params->selez_art == 'G')
      $where = " AND TAID = 'MUGM'";
  if($m_params->selez_art == 'S')
      $where = " AND TAID = 'MUSM'";
  if($m_params->selez_art == 1)
      $where = " AND TAID = 'MUCS'";
  
       
       
   $sql = "SELECT *
           FROM {$cfg_mod_Admin['file_tab_sys']}
           WHERE TADT = '{$id_ditta_default}' {$where} AND TATP <> 'S' AND TALINV=''
           ORDER BY TANR, TADESC";
   
    
   $stmt = db2_prepare($conn, $sql);
   $result = db2_execute($stmt);
   
   $ret = array();
   while ($row = db2_fetch_assoc($stmt)) {
       
       $text = "[" . trim($row['TANR']) . "] " . trim($row['TADESC']);
       
       $ret[] = array( "id" 	=> trim($row['TANR']),
           "text" 	=> acs_u8e($text) );
   }

   echo acs_je($ret);
   exit;
}

