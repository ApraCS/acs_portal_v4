<?php

require_once("../../config.inc.php");
require_once("acs_panel_ins_new_anag_include.php");

ini_set('max_execution_time', 30000);

$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));

$m_params = acs_m_params_json_decode();


function get_gcrow_by_prog($id_prog){
    global $conn, $cfg_mod_Gest, $id_ditta_default;
    
    $sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG
					FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
					LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
						TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
					LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
						TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
					WHERE GCDT='{$id_ditta_default}' AND GCPROG = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_prog));
    $row = db2_fetch_assoc($stmt);
    $row['GCTRAS'] 	= trim($row['GCTRAS']);
    $row['GCCITI'] 	= trim($row['GCCITI']);
    $row['GCSEL2'] 	= trim($row['GCSEL2']);
    $row['cliente'] = trim($row['GCCDCF']);
    //$row = array_map('rtrim', $row);
    return $row;
}


function get_row_sconti_promotion_by_codcli($codcli){
    global $conn, $cfg_mod_Gest, $id_ditta_default;
    
    $sql = "SELECT RRN(TS) AS RRN_TS, TS.*
        FROM {$cfg_mod_Gest['file_tasco']} TS
        WHERE TS.TSDT = '{$id_ditta_default}'
        AND TS.TSTPLI='V' and TSTPTB='P'
        AND TSCLI = {$codcli} AND TSART = 'PROMOTION'
        ";
        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($codcli));
    $row = db2_fetch_assoc($stmt);
    return $row;
}


function get_gcrow_by_modan_default($modan){
    global $conn, $cfg_mod_Gest, $id_ditta_default;
    
    $id_ditta_default_W = trim($id_ditta_default) . 'W';    //i clienti "default" vengono generati sulla ditta 1W
        
    $sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG 
					FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            
                    INNER JOIN {$cfg_mod_Gest['file_tabelle']} TA_MODAN ON
                        TA_MODAN.TACOGE = GC.GCCDCF 

					LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
						TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
					LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
						TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'                    
					WHERE GCDT='{$id_ditta_default_W}' AND TA_MODAN.TADT = '{$id_ditta_default}' AND TA_MODAN.TAKEY1 = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($modan));
    $row = db2_fetch_assoc($stmt);
    $row['GCTRAS'] = trim($row['GCTRAS']);
    $row['GCCITI'] = trim($row['GCCITI']);
    $row['GCSEL2'] = trim($row['GCSEL2']);
    return $row;
}

function ar_standard_alternativa(){
	$ret = array();
	$ret[] = array('id' => 'S', 'text' => 'Standard');
	$ret[] = array('id' => 'A', 'text' => 'Alternativa');
	return $ret;
}

function ar_giorno_scarico(){
	$ret = array();
	$ret[] = array('id' => 'X', 'text' => 'Si');
	$ret[] = array('id' => 'M', 'text' => 'Mattino');
	$ret[] = array('id' => 'P', 'text' => 'Pomeriggio');
	return $ret;
}




function get_request_data($key_request){
	global $conn, $cfg_mod_Gest, $id_ditta_default;
	$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_cerved']}
			LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']}
			  ON TADT = '{$id_ditta_default}' AND PARAMETRO = TAMAIL AND TATAID = 'CRVDS'
			WHERE CODICE_RICHIESTA = ?";
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($key_request));
	
	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		$n = array();
		$n['parametro'] = acs_u8e(trim($row['PARAMETRO']));
		$n['des_parametro'] = acs_u8e(trim($row['TADESC']));
		$n['contenuto'] = acs_u8e(trim($row['CONTENUTO']));
		$n['codice_contenuto'] = acs_u8e(trim($row['CODICE_CONTENUTO']));
		$n['tipo_contenuto'] = acs_u8e(trim($row['TIPO_CONTENUTO']));
		$ret[] = $n;
	}
	
	return $ret;
}


// ******************************************************************************************
//  CANCELLA CONDIZIONE COMMERCIALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
    
     $rrn = $m_params->row->rrn;
     $sql = "DELETE FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC WHERE RRN(CC) = '{$rrn}'";
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $rowgc = get_gcrow_by_prog($m_params->row->prog);
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DELETE_BCCO',
            "vals" => array(
                "RICITI" =>  $m_params->row->codice,
                "RICLIE" =>  $rowgc['GCCDCF']
            )
            
        )
        );
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
            
}



// ******************************************************************************************
// IMPORT ANAGRAFICA DA GEST
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_import_anag'){
	
	$m_params = acs_m_params_json_decode();

	if (isset($m_params->gccdcf))
	    $cod_cli = sprintf("%09d", $m_params->gccdcf);
	else 
	    $cod_cli = sprintf("%09d", $m_params->form_values->f_cliente_cod);
	    
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'EXP_ANAG_CLI',
	        "vals" => array(
	            "RICLIE" => $cod_cli
	        )	        
	      )
	);
	
	$ret['id_prog'] = (int)$return_RI['RIPROG'];
	echo acs_je($ret);
	exit;
	
}



// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_ricalcola_iban'){
	
	require_once(dirname(__FILE__) . '/../../utility/php-iban-2.5.9/php-iban.php');
	
	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->form_values;
	
	//costruisco iban neutro
	$iban = "IT";
	$iban .= sprintf("%-2s", 'XX'); //CIN IBAN
	$iban .= sprintf("%-1s", 'Y'); //CIN BBAN
	$iban .= sprintf("%05d", $form_values->CCABI);
	$iban .= sprintf("%05d", $form_values->CCCAB);
	
	if (strlen(trim($form_values->CCCOCO)) < 12)
		$iban .= sprintf("%012d", $form_values->CCCOCO);
	else	
		$iban .= trim($form_values->CCCOCO);
	
	$iban = iban_set_nationalchecksum($iban);
	
	$ret = array();
	$ret['success'] = true;
	$ret['iban'] = $iban;
	
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verify_set_localita'){
	$m_params = acs_m_params_json_decode();

	$sql = "SELECT TA_COM.*, TA_REG.TADESC AS des_reg FROM {$cfg_mod_Gest['file_tabelle']} TA_COM			
				LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
					TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'	
				WHERE TA_COM.TATAID = 'ANCOM' AND TA_COM.TAKEY4 = '{$cfg_mod_Gest['cod_naz_italia']}' AND TA_COM.TAKEY2 = ? AND TA_COM.TADESC = ?";		
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->provincia, $m_params->comune));
	$row = db2_fetch_assoc($stmt);
	$ret = array();

	if ($row) {
		$ret['success'] = true;
		$ret['data'] = $row;
	}
	else
		$ret['success'] = false;
	
	echo acs_je($ret);
 exit;
}

// ******************************************************************************************
// VIEW GRID RESPONSE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'view_grid_response'){
	$m_params = acs_m_params_json_decode();	
	?>
	{"success":true, "items": [
			{
				xtype: 'grid',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
				store: {
						xtype: 'store',
						autoLoad:true,
						data: <?php echo acs_je(get_request_data($m_params->key_request)); ?>,
			        	fields: ['parametro', 'des_parametro', 'contenuto', 'codice_contenuto', 'tipo_contenuto']
				}, //store
		
				columns: [	
		    		  {text: 'Parametro', width: 90, 	dataIndex: 'des_parametro'}
		    		, {text: 'Contenuto', flex: 1, 		dataIndex: 'contenuto', renderer: function (value, metaData, rec){
		    		
		    				if (rec.get('tipo_contenuto') == 'D')
		    					return date_from_AS(value);
		    		
		    				if (Ext.isEmpty(rec.get('codice_contenuto')) == false){
		    					return '[' + rec.get('codice_contenuto') + '] ' + value;
		    				}
		    				 else return value;		    				
		    			}
		    		  }
				],
				enableSort: false

			, buttons: [
			
			<?php if ($m_params->view_type == 'base') { ?>		
							
				<?php if ($cerved_user_data['disabilita_call_dettagli'] != 'Y'){ ?>
						{
							xtype: 'button',
				            text: 'Dettagli', 
				            iconCls: 'icon-folder_open-32', scale: 'large',
				            handler: function() {
				            
								Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
										timeout: 2400000,
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved_by_company',
								        jsonData: {
								        	view_type: 'details',
								        	companyId: <?php echo $m_params->companyId; ?>
								        },	
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        						        
											Ext.getBody().unmask();
								            var jsonData = Ext.decode(result.responseText);
								            
											acs_show_win_std('Cerved response - Details', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
													{
														view_type: 'details',
										  				key_request: jsonData.key_request,
										  				companyId: jsonData.companyId
										  			}, 650, 550, {}, 'icon-globe-16');	
		
								        },
								        failure    : function(result, request){
											Ext.getBody().unmask();							        
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	
				            
				            }
				        }, 
				     <?php } ?>   
				        
				        {
							xtype: 'button',
				            text: 'Copia intestazione', 
				            iconCls: 'icon-keyboard-32', scale: 'large',
				            handler: function() {
				            
				            	t_win = this.up('window');
				            	from_win = Ext.getCmp(<?php echo j($m_params->from_window_id); ?>);
				            	form = from_win.down('form').getForm();
				            
				            	this.up('grid').store.each(function(rec)  
								{
								  
								  if (rec.get('parametro') == 'taxCode') form.findField('GCCDFI').setValue(rec.get('contenuto'));
								  
								  if (rec.get('parametro') == 'companyName') form.findField('GCDCON').setValue(rec.get('contenuto'));  
								  if (rec.get('parametro') == 'street') form.findField('GCINDI').setValue(rec.get('contenuto'));
								  if (rec.get('parametro') == 'postCode') form.findField('GCCAP').setValue(rec.get('contenuto'));
								  
								  //if (rec.get('parametro') == 'municipality') form.findField('GCTEL').setValue(rec.get('contenuto'));								  								  						           
								  //if (rec.get('parametro') == 'municipality') form.findField('GCLOCA').setValue(rec.get('contenuto'));								  								  						            								  								  						           						            						            						            								  
								  
								  if (rec.get('parametro') == 'municipality') 	tmp_comune = rec.get('contenuto');
								  if (rec.get('parametro') == 'province') 		tmp_provincia = rec.get('contenuto');
								  
								  if (rec.get('parametro') == 'certifiedEmail') form.findField('GCPEC').setValue(rec.get('contenuto'));
								  
								},this);
								
								Ext.Ajax.request({
								   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=verify_set_localita',
								   method: 'POST',
								   jsonData: {
								   		comune: tmp_comune,
								   		provincia: tmp_provincia
								   	}, 
								   
								   success: function(response, opts) {
								   	  Ext.getBody().unmask();
									  var jsonData = Ext.decode(response.responseText);									   	  
     								  if (jsonData.success == true) {
     								  	form.findField('GCLOCA').setValue(tmp_comune);
     								  	form.findField('GCPROV').setValue(tmp_provincia);
     								  	form.findField('GCNAZI').setValue(<?php echo j($cfg_mod_Gest['cod_naz_italia']); ?>); //nazione ITA in stosa?
     								  	form.findField('v_regione').setValue(jsonData.data.DES_REG);
     								  	t_win.close();
     								  }	else {
     								  	acs_show_msg_error('Localit&agrave;/Provincia non presente in anagrafica');
     								  }
								   }, 
								   failure: function(response, opts) {
								      Ext.getBody().unmask();
								      alert('error');
								   }
								});									
									
				            }
				           }				        
				        
				        
				        
				 <?php } ?>
				        
				    ]
											    
				    		
	 	}
	]
   }
	
	<?php
	exit;
}

// ******************************************************************************************
// CERVED
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved'){
    require_once '../base/cerved_call_api.php';
    $m_params = acs_m_params_json_decode();
    $ret = cerved_api_call_search($m_params->form_values->GCPIVA);
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
// CERVED (by company)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved_by_company'){
    require_once '../base/cerved_call_api.php';
	ini_set('max_execution_time', 300000);	
	$m_params = acs_m_params_json_decode();

	$service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";

	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'Codular Sample cURL Request'
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	$token = $resp_json->access_token;
	$token_type = $resp_json->token_type;

	// Close request to clear up some resources
	curl_close($curl);

	$taxCode = $m_params->form_values->GCPIVA;

	//RICHIEDO INFO AZIENDA
	$service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies/{$m_params->companyId}/{$m_params->view_type}";

	$headr = array();
	$headr[] = 'Accept: application/json';
	$headr[] = 'Authorization: '. $token_type . ' ' . $token;

	$curl = curl_init();

	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'php curl',
			//CURLOPT_HEADER => true, //per debug
			CURLINFO_HEADER_OUT => true,
			CURLOPT_HTTPHEADER => $headr
	));

	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	
	//scrivo i parametri ricevuti su IC0
	if ($m_params->view_type == 'details')
	    $key_request = cerved_api_call_write_parametri_cerved_details($resp_json);
	if ($m_params->view_type == 'scores')
	    $key_request = cerved_api_call_write_parametri_cerved_scores($resp_json);			
	
	// Close request to clear up some resources
	curl_close($curl);
	
	$ret['success'] = true;
	$ret['key_request'] = $key_request;
	$ret['companyId'] = $resp_json->companyInfo->companyId;	
	echo acs_je($ret);
	exit;
}

// ******************************************************************************************
// tree grid data
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
	ini_set('max_execution_time', 300);	
	$m_params = acs_m_params_json_decode();
	
	$sql_where = sql_where_params($m_params->form_ep);	
	
	if (isset($m_params->recenti) && (int)$m_params->recenti > 0) {
	    $m_limit = $m_params->recenti;
	} else {
	    $m_limit = 200;
	}
	
	
	$area_manager = $m_params->form_ep->CCARMA;
	$agente = $m_params->form_ep->CCAGE;
	
	if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
	    //elenco anagrafiche clienti
	    
	    
	    if(isset($area_manager) && count($area_manager) > 0 
	    || isset($agente) && count($agente) > 0 ){
	   
	       $where = '';
	       if(count($area_manager) > 0)
	           $where .= sql_where_by_combo_value('CCARMA', $area_manager);
	       if(count($agente) > 0){
	           if(count($agente) == 1)
	               $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
	           if(count($agente) > 1)
	               $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
	       }
	        
	        $sql_join = " INNER JOIN (
                	        SELECT CCPROG
                	        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
                	        WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
            	          ON CC.CCPROG = GC.GCPROG";
	       }
	
        $tab_schede = $main_module->find_TA_std('SCCLI');
        $ar_schede = array();
        
        foreach($tab_schede as $v){
            //if(trim($v['id']) != 'BRCF')
               $ar_schede[] = $v['id'];
        }      
       
        $sche_where = sql_where_by_combo_value('TA_SCHEDE.TAID' , $ar_schede);
                     
        $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_tab_sys']} TA_SCHEDE
        WHERE TA_SCHEDE.TADT = GC.GCDT AND TA_SCHEDE.TACOR1 = GC.GCCDCF {$sche_where}) AS NR_SCHEDE";
        
       // $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_tab_sys']} TA_BRCF
       // WHERE TA_BRCF.TADT = GC.GCDT AND TA_BRCF.TACOR1 = GC.GCCDCF AND TA_BRCF.TAID = 'BRCF' AND SUBSTRING(TA_BRCF.TAREST, 229, 2) = 'OV') AS NR_BRCF";
        
        $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_xspm']} TV
        WHERE TV.TVDT = GC.GCDT AND digits(TV.TVCCON) = GC.GCCDCF) AS NR_XSPM";
        
        $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_scala_premi']} SP
        WHERE SP.SPDT = GC.GCDT AND digits(SP.SPCCON) = GC.GCCDCF) AS NR_BUCF";
	    
	    $sql = "SELECT GC.*, CF.* , TA.TAFG02 AS GRUPPO_PAGAMENTO, C_CONTRACT
                {$sql_campi_select}
	            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
          	    LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
			     ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
                /*LEFT OUTER JOIN {$cfg_mod_Gest['file_testate']} TD
			     ON GC.GCDT = TD.TDDT AND GC.GCCDCF = TD.TDCCON*/
                LEFT OUTER JOIN(
                   SELECT COUNT(*) AS C_CONTRACT, TDCCON
                   FROM {$cfg_mod_Gest['file_testate_doc_gest']} 
                   WHERE TDDT = '{$id_ditta_default}' AND TDTIDO = 'VC' AND TDTPDO = 'CN'
                   GROUP BY TDCCON
                 ) TD
			     ON GC.GCCDCF = digits(TD.TDCCON) 
                LEFT OUTER JOIN {$cfg_mod_Gest['file_tabelle']} TA
			     ON GC.GCDT = TA.TADT AND TA.TATAID = 'MODAN' AND GC.GCDEFA = TA.TAKEY1 
                {$sql_join}
	            WHERE 1=1 /* GCFG01 NOT IN ('I', 'U', 'H') */ 
	            AND GCDT = '{$id_ditta_default}' AND GCTPAN = 'CLI' {$sql_where}
	            ORDER BY CFDTGE DESC
	            LIMIT {$m_limit}";
 
	}else{
	    //elenco destinazioni per cliente	    
	    $prog = $_REQUEST['node'];
	    
	    $rowProg = get_gcrow_by_prog($prog);
	       
	    
	    //prima rieseguo un refresh per sicurezza
	    $sh = new SpedHistory();
	    $return_RI = $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'EXP_ANAG_CLI',
	            "vals" => array(
	                "RICLIE" => $rowProg['GCCDCF']
	            )
	        )
	    );
	    
	    	    
	    $sql = "SELECT GC.*
	            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                WHERE 1=1 /* GCFG01 NOT IN ('I', 'U', 'H') */
	            AND GCDT = '{$id_ditta_default}' AND GCTPAN IN ('DES', 'PVEN', 'CLI22') AND GCPRAB = {$prog}
	            {$sql_where_des}
                ORDER BY GCTPAN ASC, GCTPIN ASC
	            LIMIT 100";
	    
	            
	    $sql_ta = "SELECT RRN(TA) AS RRN, TA.*
	               FROM {$cfg_mod_Gest['file_tab_sys']} TA
	               WHERE TADT = '{$id_ditta_default}' AND TAID = 'VUDE' AND TANR = ? AND TACOR1 = ?";
	    $stmt_ta = db2_prepare($conn, $sql_ta);
	    echo db2_stmt_errormsg();
	
}
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();
		
		$sql_c = "SELECT COUNT(*) AS C_ROW
            	  FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            	  WHERE GCDT = '{$id_ditta_default}' AND GCTPAN IN ('DES') AND GCPRAB = ?
            	  ";
		
		$stmt_c = db2_prepare($conn, $sql_c);
		echo db2_stmt_errormsg();
		
		while ($r = db2_fetch_assoc($stmt)) {

		    $tmp_ar_id = array();
		    
			
			$ar_r = &$ar;			
			$cod_liv0 = trim($r['GCPROG']); 	//Progressivo
			
			   $liv = $cod_liv0;
			   $tmp_ar_id[] = $liv;
			  if (!isset($ar_r["{$liv}"])){
			        $ar_new = array();
					$ar_new = $r;
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['liv_cod'] = $liv;
					$ar_new['liv_cod_out'] = $liv;				
					$ar_new['task'] = acs_u8e($r['GCDCON']);
					$ar_new['omp'] = $r['GCGMTP'];
					//$ar_new['cliente'] = trim($r['GCCDCF']);
					if(trim($r['GCNAZI']) != ''){
					   $naz = get_TA_sys('BNAZ', trim($r['GCNAZI']));
					   $ar_new['d_nazione'] = trim($naz['text']);
					}
					if(trim($r['GCTPAN']) != 'DES'){
					   $ha_commenti = $main_module->has_commento_cliente($r['GCCDCF']);
					   $ar_new['note'] =  $ha_commenti;
			        }else{
			            $ar_new['note'] =  "N";
			        }
					$ar_new['cliente'] =  $r['GCCDCF'];
					$ar_new['contract'] =  $r['C_CONTRACT'];
					//icona entry (per attivita' in corso sull'ordine)
					
					if(trim($r['GCCDFI']) == '')
					    $ar_new['cod_fi'] =  $r['GCPIVA'];
					else
					    $ar_new['cod_fi'] =  $r['GCCDFI'];
					if (in_array(trim($r['GCTPAN']), array('DES'))){ 
					   $ar_new['leaf'] = true;
					   $result = db2_execute($stmt_ta, array($r['GCCDCF'], $rowProg['GCCDCF']));
					   $r_ta = db2_fetch_assoc($stmt_ta);
				
					   $ar_new['data_ge'] = $r_ta['TADTGE'];
					   $ar_new['data_um'] = $r_ta['TADTUM'];
					   $ar_new['ute_ge'] = $r_ta['TAUSGE'];
					   $ar_new['ute_um'] = $r_ta['TAUSUM'];
					   $ar_new['rrn_ta'] = $r_ta['RRN'];
					   $ar_new['k_cli_des'] = implode("_", array($id_ditta_default, $rowProg['GCCDCF'], $r['GCCDCF']));
					   $ar_new['sosp'] = trim($r_ta['TATP']);
					   
					   $ar_new['t_indi'] = trim($r['GCTPIN']);
					   if($ar_new['t_indi'] == ''){
					       $row_d = $main_module->get_TA_std('INDI', 'D');
					       $ar_new['d_indi'] = $row_d['TADESC'];
					   }else{
					       $row_d = $main_module->get_TA_std('INDI', $ar_new['t_indi']);
					       $ar_new['d_indi'] = $row_d['TADESC'];
					   }
					      
					   $ar_new['crm1'] = trim($r['GCCRM1']);
					   
					   if(trim($r['GCTPDS']) == 'S')
					       $ar_new['des_std'] = trim($r['GCTPDS']);
					   
					   if(trim($r['GCTPAN']) == 'DES'){
					      
					       if($ar_new['t_indi'] == 'S' || $ar_new['t_indi'] == 'G'){
					           $ar_new['iconCls'] = 'icon-home-16';
					       }elseif($ar_new['t_indi'] == 'N'){
					           $ar_new['iconCls'] = 'icon-block_blue-16';
					       }elseif($ar_new['t_indi'] == 'A'){
					           $ar_new['iconCls'] = 'icon-briefcase-16';
					       }elseif($ar_new['t_indi'] == 'P' || $ar_new['t_indi'] == 'E'){
					           $ar_new['iconCls'] = 'icon-clienti-16';
					       }else{
					           $ar_new['iconCls'] = 'icon-delivery-16';
					       }
					   
					   
					   }
					   $ar_new['liv'] = 'liv_4';
					   $ar_new['out_codice'] =  $r['GCCDCF'];
				     
					}else{
					    $ar_new['data_ge'] = $r['CFDTGE'];
					    $ar_new['data_um'] = $r['CFDTUM'];
					    $ar_new['ute_ge'] = $r['CFUSGE'];
					    $ar_new['ute_um'] = $r['CFUSUM'];
					    $ar_new['g_paga'] = $r['GRUPPO_PAGAMENTO'];
					    $ar_new['ciclo']  = trim($r['GCCIVI']);
					    $ta_ciclo  = $main_module->find_TA_std('CHCVF', trim($r['GCCIVI']));
					    $ar_new['d_ciclo']  = "[".trim($r['GCCIVI'])."] ".$ta_ciclo[0]['text'];
					    
					    $ar_new['k_cli_des'] = implode("_", array($id_ditta_default, $r['GCCDCF'], ''));
					    $ar_new['schede'] = $r['NR_SCHEDE'] + $r['NR_XSPM']+ $r['NR_BUCF'];
					    $ar_new['sosp'] = trim($r['GCSOSP']);
					    $ar_new['liv'] = 'liv_2';
					    $result = db2_execute($stmt_c, array($r['GCPROG']));
					    $row_c = db2_fetch_assoc($stmt_c);
					    if($row_c['C_ROW'] == 0){
					        $ar_new['leaf'] = true;
					        $ar_new['iconCls'] = 'icon-folder_grey-16';
					    }    
					    
					    //prento tutto scadute e non 
					    
					    if($m_params->form_ep->f_cond == 'Y'){
					       $condizioni = $main_module->get_condizioni_commerciali_cliente('Y', $r, 'Y');
									    
					       $ar_new['c_cond'] = $condizioni['count'];
					       $ar_new['val_cond'] = $condizioni['max_val'];
					    }
					    
					    $sa = new SpedAssegnazioneClienti();
					    $title = "";
					    if ($sa->ha_entry_aperte_per_cliente(trim($r['GCCDCF']))){
					        $img_entry_name = "icone/16x16/arrivi_gray.png";
					        $title = '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';
					    }
					    
					    $ar_new['out_codice'] = $r['GCCDCF'].$title;
					  
					}
					
					//Se presente recupero anagrafica collegata
					if($r['GCANCO'] > 0){
					    $sqlAC = "SELECT *
					       FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
					       WHERE GCDT = '{$id_ditta_default}' AND GCCDCF = '{$r['GCANCO']}'";
					    $stmtAC = db2_prepare($conn, $sqlAC);
					    echo db2_stmt_errormsg();
					    $result = db2_execute($stmtAC);
					    $rowAC = db2_fetch_assoc($stmtAC);
					    $ar_new['tooltip_anagrafica_collegata'] = trim($rowAC['GCDCON']) . " - " . $r['GCANCO'];
					}
					
					
									   
					$ar_r["{$liv}"] = $ar_new;					
				}
				
				
				$ar_r = &$ar_r["{$liv}"];
				$ar_liv2 = &$ar_r;
	
	}
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	if ($_REQUEST['node'] != '' &&$_REQUEST['node'] != 'root'){
		//$ret = $ret[0]['children'][0]['children'][0]['children'];
	    $ar= $ar[$_REQUEST['node']]['children'];
	}	
	
	
	
	 echo acs_je(array('success' => true, 'children' => $ret));	
	
	exit;
} //get_json_data

// ******************************************************************************************
// INVIO A GESTIONALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_send_to_gest'){
	$m_params = acs_m_params_json_decode();
	
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $m_params->rec_dt);
	$cl_p .= sprintf("%010s", $m_params->rec_prog);
	
	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31O1C('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31O1C', $libreria_predefinita_EXE, $cl_in, null, null);
		//$call_return = $tkObj->PgmCall('UTIBR31G5', $libreria_predefinita_EXE, '', null, null);
	}
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);	
	
 exit;	
}

if ($_REQUEST['fn'] == 'exe_check'){
    
    $ret = array();
    
    if ($m_params->form_values->mode == 'NEW') {
        $p_iva 	= utf8_decode(trim($m_params->form_values->GCPIVA));
        $cod_f 	= utf8_decode(trim($m_params->form_values->GCCDFI));
        $sql_where = " AND ( 1 = 0 ";
        if($p_iva != '')
            $sql_where .= " OR GCPIVA = '{$p_iva}'";
            
        if($cod_f != '')
            $sql_where .= " OR GCCDFI = '{$cod_f}'";
          $sql_where .= " )";
        
        $sql = "SELECT *
                FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
                ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
                WHERE GCDT='{$id_ditta_default}' {$sql_where}";
                
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $text_error = "";
        while ($row = db2_fetch_assoc($stmt)) {
                               
            $cliente = "[".trim($row['GCCDCF'])."] ".trim($row['GCDCON']);
            $iva_cod = trim($row['GCPIVA'])." - ".trim($row['GCCDFI']);
            $immi = print_date($row['CFDTGE']);
            if(trim($row['GCSOSP']) == 'S')
                $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=15>";
            $text_error .= "<br> {$sosp}{$cliente} - {$iva_cod} - Immissione {$immi} ";
            
        }
            
        if($text_error != ''){
            $ret['msg_error'] = "<span style='font-weight: bold; font-size:14px;'>Partita iva/codice fiscale gi� in archivio:</span> {$text_error}";
            $ret['success'] = false;
        }else{
            $ret['success'] = true;
            
        }
         
    }
    
    echo acs_je($ret);
    return;
}

// ******************************************************************************************
// EXE SAVE NEW ANAGRAGICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_new_anag'){
	
	$m_params = acs_m_params_json_decode();	
	
	$ar_ins = array();
	$ret = array();
	
	$ar_ins['GCUSUM'] = trim($auth->get_user());
	$ar_ins['GCDTUM'] = oggi_AS_date();
	$ar_ins['GCORUM'] = oggi_AS_time();	
	
	
	$r_prov 	= $s->get_TA_std('ANPRO', trim($m_params->form_values->GCPROV), null, 'Y');
	$r_comune  	= $s->get_TA_std('ANCOM', trim($m_params->form_values->GCLOCA));
	$r_regione 	= $s->get_TA_std_by_des('ANREG', null, trim($m_params->form_values->v_regione));
	
	
	$ar_ins['GCDCON'] 	= utf8_decode(trim($m_params->form_values->GCDCON));
	$ar_ins['GCINDI'] 	= utf8_decode(trim($m_params->form_values->GCINDI));
	$ar_ins['GCCAP'] 	= trim($m_params->form_values->GCCAP);
	
	$ar_ins['GCTFIS'] 	= trim($m_params->form_values->GCTFIS);
	$ar_ins['GCCOGN'] 	= utf8_decode(trim($m_params->form_values->GCCOGN));
	$ar_ins['GCNOME'] 	= utf8_decode(trim($m_params->form_values->GCNOME));
	
	$ar_ins['GCKMIT'] 	= sql_f(trim($m_params->form_values->GCKMIT));
	
	//$ar_ins['GCLOCA'] 	= trim($r_comune['TADESC']);
	$ar_ins['GCLOCA']	= utf8_decode(trim($m_params->form_values->GCLOCA));
	$ar_ins['GCLOC2']	= utf8_decode(trim($m_params->form_values->GCLOC2));
	$ar_ins['GCPROV'] 	= utf8_decode(trim($m_params->form_values->GCPROV)); //trim($r_comune['TAKEY2']);
	$ar_ins['GCNAZI'] 	= utf8_decode(trim($m_params->form_values->GCNAZI));
	$ar_ins['GCCISN'] 	= trim($r_comune['TAKEY4']);   //Nazione
	
	if(trim($m_params->form_values->GCZONA) != '')
	   $ar_ins['GCZONA'] 	= utf8_decode(trim($m_params->form_values->GCZONA));   //Zona
       //$ar_ins['GCZONA'] 	= trim($r_regione['TANAZI']);   //Zona
	
	
	
	$ar_ins['GCPIVA'] 	= utf8_decode(trim($m_params->form_values->GCPIVA));
	$ar_ins['GCCDFI'] 	= utf8_decode(trim($m_params->form_values->GCCDFI));
		
	$ar_ins['GCMAIL'] 	= utf8_decode(trim($m_params->form_values->GCMAIL));
	$ar_ins['GCWWW'] 	= utf8_decode(trim($m_params->form_values->GCWWW));
	$ar_ins['GCRISC'] 	= utf8_decode(trim($m_params->form_values->GCRISC));
	
	if (isset($m_params->form_values->GCSIRE))
	    $ar_ins['GCSIRE'] 	= utf8_decode(trim($m_params->form_values->GCSIRE));
	
	if (isset($m_params->form_values->GCMAI1))
	   $ar_ins['GCMAI1'] 	= utf8_decode(trim($m_params->form_values->GCMAI1));
	if (isset($m_params->form_values->GCMAI2))
	   $ar_ins['GCMAI2'] 	= utf8_decode(trim($m_params->form_values->GCMAI2));
	if (isset($m_params->form_values->GCMAI3))
	   $ar_ins['GCMAI3'] 	= utf8_decode(trim($m_params->form_values->GCMAI3));
	if (isset($m_params->form_values->GCMAI4))
	   $ar_ins['GCMAI4'] 	= utf8_decode(trim($m_params->form_values->GCMAI4));
	if (isset($m_params->form_values->GCMAI5))
	   $ar_ins['GCMAI5'] 	= utf8_decode(trim($m_params->form_values->GCMAI5));
	if (isset($m_params->form_values->GCMAI6))
	   $ar_ins['GCMAI6'] 	= utf8_decode(trim($m_params->form_values->GCMAI6));
   if (isset($m_params->form_values->GCMAI7))
       $ar_ins['GCMAI7'] 	= utf8_decode(trim($m_params->form_values->GCMAI7));
   if (isset($m_params->form_values->GCMAI7))
       $ar_ins['GCMAI8'] 	= utf8_decode(trim($m_params->form_values->GCMAI8));
	
	$ar_ins['GCTEL'] 	= utf8_decode(trim($m_params->form_values->GCTEL));	
	$ar_ins['GCTEL2'] 	= utf8_decode(trim($m_params->form_values->GCTEL2));
	$ar_ins['GCTPIN'] 	    = utf8_decode(trim($m_params->form_values->GCTPIN));
	$ar_ins['GCFAX'] 	= utf8_decode(trim($m_params->form_values->GCFAX));
	
	//FATTURAZIONE ELETTRONICA
	$ar_ins['GCCSDI'] 	= utf8_decode(trim($m_params->form_values->GCCSDI));
	$ar_ins['GCPEC'] 	= utf8_decode(trim($m_params->form_values->GCPEC));
	
	
	//destinazione assegnata (prima su VUD2)
	$ar_ins['GCDSTA'] 	= utf8_decode(trim($m_params->form_values->GCDSTA));

	if($m_params->form_values->type == 'P' || trim($m_params->form_values->GCTPIN) == 'P')
	    $ar_ins['GCTPDS']  = 'A';
	else
	    $ar_ins['GCTPDS'] 	= utf8_decode(trim($m_params->form_values->GCTPDS));
	
	    
	if ($m_params->form_values->mode == 'NEW') {
		$ar_ins['GCDT'] 	= $id_ditta_default;
		$ar_ins['GCPROG'] 	= $main_module->next_num('ANAG_GC');
		
		$ar_ins['GCUSGE'] = trim($auth->get_user());
		$ar_ins['GCDTGE'] = oggi_AS_date();
		$ar_ins['GCORGE'] = oggi_AS_time();		
		
		//Itinerario, se impostato dalla provincia
		$ar_ins['GCCITI'] 	= trim($r_prov['TANAZI']);   	//Itinerario
		
		if ($m_params->form_values->tipo_anagrafica == 'DES'){
			$ar_ins['GCTPAN'] 	= 'DES';
			if($m_params->form_values->type == 'P')
			    $ar_ins['GCTPIN']  = 'P';
			else
			    $ar_ins['GCTPIN'] 	= 'D';
			   
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else if ($m_params->form_values->tipo_anagrafica == 'PVEN'){
		    $ar_ins['GCTPAN']   = 'CLI'; ## era PVEN
		    //$ar_ins['GCTPIN']   = '';    ## era P
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else {
			$ar_ins['GCTPAN'] 	= 'CLI';
			$ar_ins['GCDEFA'] 	= $m_params->open_request->modan;
		}

		//insert riga
		$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
		$ret['GCPROG'] = $ar_ins['GCPROG'];	
	
	}
	

	if ($m_params->form_values->mode == 'EDIT') {
		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
		          SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			      WHERE GCPROG = ?";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_ins,
				array($m_params->form_values->GCPROG)
				));
		echo db2_stmt_errormsg($stmt);
		
		$ret['success'] = true;
		$ret['GCPROG'] = $m_params->form_values->GCPROG;
	}

	
	
	
	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $ret['GCPROG'],
	            "RINOTE" => 'save_anag'
	        )
	    )
	);
	
	if ($m_params->form_values->mode == 'NEW') 
	    $prog = $ar_ins['GCPROG'];
	else
	    $prog = $m_params->form_values->GCPROG;
	
    $record = get_gcrow_by_prog($prog);
	
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'EXP_ANAG_CLI',
            "vals" => array(
                "RICLIE" => sprintf("%09d", $record['GCCDCF'])
            )
        )
        );
	
    $record = get_gcrow_by_prog($prog);
    $record = array_map('rtrim', $record);
    
    $ret['record'] = $record;	

	echo acs_je($ret);
 exit;
}

function exe_save_logis_add_field($ar, $m_params){
    
    global $cfg_mod_Gest;
    
	$ar['GCLING'] 	= utf8_decode($m_params->form_values->GCLING);
	$ar['GCSEL3'] 	= utf8_decode($m_params->form_values->GCSEL3);
	$ar['GCCFGC'] 	= utf8_decode($m_params->form_values->GCCFGC);
	$ar['GCSEDB'] 	= utf8_decode($m_params->form_values->GCSEDB);
	$ar['GCCFG1'] 	= utf8_decode($m_params->form_values->GCCFG1);
	$ar['GCCFG2'] 	= utf8_decode($m_params->form_values->GCCFG2);
	$ar['GCCFG3'] 	= utf8_decode($m_params->form_values->GCCFG3);
	$ar['GCCFG4'] 	= utf8_decode($m_params->form_values->GCCFG4);
	$ar['GCNAZO'] 	= utf8_decode($m_params->form_values->GCNAZO);
	$ar['GCPORT'] 	= utf8_decode($m_params->form_values->GCPORT);
	$ar['GCSPED'] 	= utf8_decode($m_params->form_values->GCSPED);
	$ar['GCTRAS'] 	= utf8_decode($m_params->form_values->GCTRAS);
	$ar['GCCITI'] 	= utf8_decode($m_params->form_values->GCCITI);
	$ar['GCFMNC'] 	= utf8_decode($m_params->form_values->GCFMNC);
	$ar['GCICON'] 	= utf8_decode($m_params->form_values->GCICON);
	$ar['GCIFAT'] 	= utf8_decode($m_params->form_values->GCIFAT);
	$ar['GCIPSP'] 	= utf8_decode($m_params->form_values->GCIPSP);
	$ar['GCGGS1'] 	= utf8_decode($m_params->form_values->GCGGS1);
	$ar['GCGGS2'] 	= utf8_decode($m_params->form_values->GCGGS2);
	$ar['GCGGS3'] 	= utf8_decode($m_params->form_values->GCGGS3);
	$ar['GCGGS4'] 	= utf8_decode($m_params->form_values->GCGGS4);
	$ar['GCGGS5'] 	= utf8_decode($m_params->form_values->GCGGS5);
	$ar['GCGGS6'] 	= utf8_decode($m_params->form_values->GCGGS6);
	$ar['GCNOSC'] 	= utf8_decode($m_params->form_values->GCNOSC);
	if(is_null($m_params->form_values->GCVT1))
	    $ar['GCVT1'] = 0;
	else 
	    $ar['GCVT1'] = $m_params->form_values->GCVT1;
	
	if(trim($m_params->form_values->GCVT1) == 0)
	    $ar['GCDVT1'] 	= '';
	else
	   $ar['GCDVT1'] 	= utf8_decode($m_params->form_values->GCDVT1);
	   if(is_null($m_params->form_values->GCVT2))
       $ar['GCVT2'] = 0;
    else
	   $ar['GCVT2'] 	= utf8_decode($m_params->form_values->GCVT2);
	if(trim($m_params->form_values->GCVT2) == 0)
	   $ar['GCDVT2'] 	= "";
	else 
	   $ar['GCDVT2'] 	= $m_params->form_values->GCDVT2;
	
	$ar['GCECON'] 	= utf8_decode($m_params->form_values->GCECON);
	$ar['GCCONA'] 	= utf8_decode($m_params->form_values->GCCONA);
	if($cfg_mod_Gest['gest_azienda_collegata']  == 'Y'){
	    $ar['GCANCO'] 	= utf8_decode(trim($m_params->form_values->GCANCO));
	}
	
	if($cfg_mod_Gest['destinazione_tipologia']  == 'Y'){
	    $ar['GCFLG1'] 	= utf8_decode(trim($m_params->form_values->GCFLG1));
	    $ar['GCFLG2'] 	= utf8_decode(trim($m_params->form_values->GCFLG2));
	}
	

	$ar['GCREDO'] 	= utf8_decode($m_params->form_values->GCREDO);
	
	return $ar;
}

function exe_save_crm_add_field($ar, $m_params){
        
    $ar['GCPROG'] 	= utf8_decode($m_params->form_values->GCPROG);
    $ar['GCGMTP'] 	= utf8_decode($m_params->form_values->GCGMTP);
    $ar['GCCATA'] 	= utf8_decode($m_params->form_values->GCCATA);
    $ar['GCLIST'] 	= utf8_decode($m_params->form_values->GCLIST);
    $ar['GCESPO'] 	= utf8_decode($m_params->form_values->GCESPO);
    $ar['GCNBOX'] 	= sql_f($m_params->form_values->GCNBOX);
    $ar['GCNVEN'] 	= sql_f($m_params->form_values->GCNVEN);
    $ar['GCNPVE'] 	= sql_f($m_params->form_values->GCNPVE);
    
    $ar['GCCIVI'] 	= utf8_decode($m_params->form_values->GCCIVI);
    $ar['GCORIG'] 	= utf8_decode($m_params->form_values->GCORIG);
    
    $ar['GCSWGB'] 	= utf8_decode($m_params->form_values->GCSWGB);
    $ar['GCPREM'] 	= utf8_decode($m_params->form_values->GCPREM);
    $ar['GCCRM1'] 	= utf8_decode($m_params->form_values->GCCRM1);
    $ar['GCCRM2'] 	= utf8_decode($m_params->form_values->GCCRM2);
    
    $ar['GCSEL1'] 	= utf8_decode($m_params->form_values->GCSEL1);
    $ar['GCSEL2'] 	= utf8_decode($m_params->form_values->GCSEL2);
    $ar['GCSEL3'] 	= utf8_decode($m_params->form_values->GCSEL3);
    $ar['GCSEL4'] 	= utf8_decode($m_params->form_values->GCSEL4);
    $ar['GCREFE'] 	= utf8_decode($m_params->form_values->GCREFE);
    
    $ar['GCFG03'] 	= utf8_decode($m_params->form_values->GCFG03);
    
    $ar['GCCAZ1'] 	= utf8_decode($m_params->form_values->GCCAZ1);
    $ar['GCCAZ2'] 	= utf8_decode($m_params->form_values->GCCAZ2);
    $ar['GCCAZ3'] 	= utf8_decode($m_params->form_values->GCCAZ3);
    $ar['GCCAZ4'] 	= utf8_decode($m_params->form_values->GCCAZ4);
    $ar['GCMERC'] 	= utf8_decode($m_params->form_values->GCMERC);
    
    if(strlen($m_params->form_values->GCTPIN) > 0)
        $ar['GCTPIN']   = utf8_decode(trim($m_params->form_values->GCTPIN));
   
    
    return $ar;
}


function exe_save_cc_add_field($ar, $m_params){
	$ar['CCAG1'] 	= utf8_decode($m_params->form_values->CCAG1);
	$ar['CCAG2'] 	= utf8_decode($m_params->form_values->CCAG2);
	$ar['CCAG3'] 	= utf8_decode($m_params->form_values->CCAG3);
	$ar['CCPA1'] 	= sql_f($m_params->form_values->CCPA1);
	$ar['CCPA2'] 	= sql_f($m_params->form_values->CCPA2);
	$ar['CCPA3'] 	= sql_f($m_params->form_values->CCPA3);
	if(trim($m_params->form_values->CCCCON) == 'STD')
	   $ar['CCARMA'] 	= $m_params->form_values->CCARMA;
	
	$ar['CCPAGA'] 	= utf8_decode(trim($m_params->form_values->CCPAGA));
	$ar['CCNGPA'] 	= sql_f($m_params->form_values->CCNGPA);
	$ar['CCMME1'] 	= sql_f($m_params->form_values->CCMME1);
	$ar['CCMME2'] 	= sql_f($m_params->form_values->CCMME2);
	$ar['CCGGE1'] 	= utf8_decode($m_params->form_values->CCGGE1);
	$ar['CCGGE2'] 	= utf8_decode($m_params->form_values->CCGGE2);
	$ar['CCTSIV'] 	= utf8_decode(trim($m_params->form_values->CCTSIV));
	
	$ar['CCREFE'] 	= utf8_decode(trim($m_params->form_values->CCREFE));
	$ar['CCLIST'] 	= utf8_decode(trim($m_params->form_values->CCLIST));
	$ar['CCCATR'] 	= utf8_decode(trim($m_params->form_values->CCCATR));
	$ar['CCSC1'] 	= sql_f($m_params->form_values->CCSC1);
	$ar['CCSC2'] 	= sql_f($m_params->form_values->CCSC2);
	$ar['CCSC3'] 	= sql_f($m_params->form_values->CCSC3);
	$ar['CCSC4'] 	= sql_f($m_params->form_values->CCSC4);
	$ar['CCSC5'] 	= sql_f($m_params->form_values->CCSC5);
	$ar['CCSC6'] 	= sql_f($m_params->form_values->CCSC6);
	$ar['CCSC7'] 	= sql_f($m_params->form_values->CCSC7);
	$ar['CCSC8'] 	= sql_f($m_params->form_values->CCSC8);
	
	$ar['CCTCON'] 	= utf8_decode($m_params->form_values->CCTCON);
	$ar['CCGRDO'] 	= utf8_decode($m_params->form_values->CCGRDO);
	$ar['CCDIVI'] 	= utf8_decode($m_params->form_values->CCDIVI);
	if(strlen($m_params->form_values->CCDTVI) > 0)
	   $ar['CCDTVI'] 	= $m_params->form_values->CCDTVI;
    if(strlen($m_params->form_values->CCDTVF) > 0)
	   $ar['CCDTVF'] 	= $m_params->form_values->CCDTVF;
	$ar['CCFGR1'] 	= utf8_decode($m_params->form_values->CCFGR1);
	$ar['CCFGR2'] 	= utf8_decode($m_params->form_values->CCFGR2);
	$ar['CCFGR3'] 	= utf8_decode($m_params->form_values->CCFGR3);
	$ar['CCDES1'] 	= utf8_decode($m_params->form_values->CCDES1);
	$ar['CCDES2'] 	= utf8_decode($m_params->form_values->CCDES2);
	$ar['CCDES3'] 	= utf8_decode($m_params->form_values->CCDES3);
	$ar['CCDES4'] 	= utf8_decode($m_params->form_values->CCDES4);
	$ar['CCDES5'] 	= utf8_decode($m_params->form_values->CCDES5);
	$ar['CCTMOD'] 	= utf8_decode($m_params->form_values->CCTMOD);
	$ar['CCCPRO'] 	= utf8_decode($m_params->form_values->CCCPRO);
	$ar['CCPRZO'] 	= utf8_decode($m_params->form_values->CCPRZO);
	$ar['CCSCIM'] 	= utf8_decode($m_params->form_values->CCSCIM);
	$ar['CCEINT'] 	= utf8_decode($m_params->form_values->CCEINT);
	$ar['CCAZSA'] 	= utf8_decode($m_params->form_values->CCAZSA);
	$ar['CCAZSD'] 	= utf8_decode($m_params->form_values->CCAZSD);
	$ar['CCAZPR'] 	= utf8_decode($m_params->form_values->CCAZPR);
	
	$ar['CCCATC'] 	= utf8_decode($m_params->form_values->CCCATC);
	$ar['CCCAPC'] 	= utf8_decode($m_params->form_values->CCCAPC);
	
	if (strlen(trim($m_params->form_values->CCVALU)) > 0)
	   $ar['CCVALU'] 	= $m_params->form_values->CCVALU;
	
	if (strlen(trim($m_params->form_values->CCABI)) > 0)
		$ar['CCABI'] 	= sprintf("%05s", trim($m_params->form_values->CCABI));
	else
		$ar['CCABI'] = '';
	
	if (strlen(trim($m_params->form_values->CCCAB)) > 0)
		$ar['CCCAB'] 	= sprintf("%05s", trim($m_params->form_values->CCCAB));
	else
		$ar['CCCAB'] = '';

	if (strlen(trim($m_params->form_values->CCCOCO)) > 0)
		if (strlen(trim($m_params->form_values->CCCOCO)) < 12)	
			$ar['CCCOCO'] 	= sprintf("%012d", $m_params->form_values->CCCOCO);
		else
			$ar['CCCOCO'] = trim($m_params->form_values->CCCOCO);
	else	
		$ar['CCCOCO'] = '';
    
    //if (strlen(trim($m_params->form_values->CCIBAN)) > 0)
	   $ar['CCIBAN'] 	= trim($m_params->form_values->CCIBAN);
	
	$ar['CCBANC'] 	= utf8_decode($m_params->form_values->CCBANC);
	   
	//NS Banca
	if (isset($m_params->form_values->CCBASF)) $ar['CCBASF'] 	= trim(acs_toDb($m_params->form_values->CCBASF));
	if (isset($m_params->form_values->CCABIF)) $ar['CCABIF'] 	= $m_params->form_values->CCABIF;
	if (isset($m_params->form_values->CCCABF)) $ar['CCCABF'] 	= $m_params->form_values->CCCABF;
	
  return $ar;
}



function exe_save_fido_add_field($ar, $m_params){
    if(strlen($m_params->form_values->CCDTVI) > 0)
	   $ar['CCDTVI'] 	= $m_params->form_values->CCDTVI;
	if(strlen($m_params->form_values->CCDTVF) > 0)
	   $ar['CCDTVF'] 	= $m_params->form_values->CCDTVF;
	if(trim($m_params->form_values->CCCESU) != '')
	   $ar['CCCESU'] 	= $m_params->form_values->CCCESU;
    if(trim($m_params->form_values->CCLEGA) != '')
        $ar['CCLEGA'] 	= $m_params->form_values->CCLEGA;
	$ar['CCBLOC'] 	= $m_params->form_values->CCBLOC;
	$ar['CCIMFD'] 	= sql_f($m_params->form_values->CCIMFD);
	
	$ar['CCCAL1'] 	= $m_params->form_values->CCCAL1;
    $ar['CCCAL2'] 	= $m_params->form_values->CCCAL2;
	$ar['CCPINC'] 	= sql_f($m_params->form_values->CCPINC);

	return $ar;
}

function exe_save_configLogis_add_field($ar, $m_params){
    global $cfg_mod_Gest;
    
    if(trim($m_params->form_values->GCZONA) != '')
        $ar['GCZONA'] 	= $m_params->form_values->GCZONA;
    if(trim($m_params->form_values->GCCITI) != '')
        $ar['GCCITI'] 	= $m_params->form_values->GCCITI;
    if(trim($m_params->form_values->GCAG1) != '')
        $ar['GCAG1'] 	= $m_params->form_values->GCAG1;
    if(trim($m_params->form_values->GCAG2) != '')
        $ar['GCAG2'] 	= $m_params->form_values->GCAG2;
    $ar['GCPA1'] 	= sql_f($m_params->form_values->GCPA1);
    $ar['GCPA2'] 	= sql_f($m_params->form_values->GCPA2);
    $ar['GCGGS1'] 	= utf8_decode($m_params->form_values->GCGGS1);
    $ar['GCGGS2'] 	= utf8_decode($m_params->form_values->GCGGS2);
    $ar['GCGGS3'] 	= utf8_decode($m_params->form_values->GCGGS3);
    $ar['GCGGS4'] 	= utf8_decode($m_params->form_values->GCGGS4);
    $ar['GCGGS5'] 	= utf8_decode($m_params->form_values->GCGGS5);
    $ar['GCGGS6'] 	= utf8_decode($m_params->form_values->GCGGS6);
    if($cfg_mod_Gest['destinazione_tipologia']  == 'Y'){
        $ar['GCFLG1'] 	= utf8_decode(trim($m_params->form_values->GCFLG1));
        $ar['GCFLG2'] 	= utf8_decode(trim($m_params->form_values->GCFLG2));
    }
    
    
    
    
    return $ar;
}



// ******************************************************************************************
// EXE SAVE CONDIZIONE COMMERCIALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_cc'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();
	

	if ($m_params->open_request->mode == 'NEW' || $m_params->open_request->mode == 'CREATE' || $m_params->open_request->mode == 'DUP') {
	    
	       
	        $sql_cc = "SELECT COUNT(*) AS C_ROW
	                   FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
	                   WHERE CCDT = '{$id_ditta_default}' AND CCPROG = '{$m_params->form_values->CCPROG}'
	                   AND CCSEZI = 'BCCO' AND CCCCON = '{$m_params->form_values->CCCCON}'";
	        
	        $stmt = db2_prepare($conn, $sql_cc);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt);
	        $row = db2_fetch_assoc($stmt);
	    	        
	        if($row['C_ROW'] > 0){
	            $ret['success'] = false;
	            $ret['msg_error'] = 'Codice condizione gi&agrave; esistente';
	            echo acs_je($ret);
	            return;
	        }
	        
	   

		$ar_ins['CCUSGE'] = trim($auth->get_user());
		$ar_ins['CCDTGE'] = oggi_AS_date();
		$ar_ins['CCORGE'] = oggi_AS_time();		
		
		$ar_ins['CCDT'] 	= $id_ditta_default;
		$ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
		$ar_ins['CCSEZI'] 	= 'BCCO';
		$ar_ins['CCCCON'] 	= utf8_decode(trim($m_params->form_values->CCCCON));
		
		//Descrizione separata in nome (30) + articolo (30)
		$ar_ins['CCDCON'] 	= sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON))) . 
		                      sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON_ART)));

		$ar_ins = exe_save_cc_add_field($ar_ins, $m_params);
		
		//insert riga
		$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}


	if ($m_params->open_request->mode == 'EDIT') {
		$ar_upd = exe_save_cc_add_field(array(), $m_params);
		
		$ar_upd['CCUSGE'] = trim($auth->get_user());
		$ar_upd['CCDTGE'] = oggi_AS_date();
		$ar_upd['CCORGE'] = oggi_AS_time();
		
		if (trim($m_params->form_values->CCCON) != 'STD'){
		    $ar_upd['CCDCON'] 	= sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON))) .
		                          sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON_ART)));		    		    
		}
		
		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
				SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";		

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, $m_params->form_values->CCSEZI, $m_params->form_values->CCCCON)
		));
		echo db2_stmt_errormsg($stmt);
		
		$ret['success'] = true;
	}
	

	//UPDATE GC0
	$ar_upd1 = array();
	if(isset($m_params->form_values->GCCATC) || isset($m_params->form_values->GCCACPC)
	    || isset($m_params->form_values->GCIVES)){
	        
        $ar_upd1['GCUSGE'] = trim($auth->get_user());
        $ar_upd1['GCDTGE'] = oggi_AS_date();
        $ar_upd1['GCORGE'] = oggi_AS_time();
        if(strlen($m_params->form_values->GCCATC) > 0)
            $ar_upd1['GCCATC'] = $m_params->form_values->GCCATC;
        if(strlen($m_params->form_values->GCCAPC) > 0)
            $ar_upd1['GCCAPC'] = $m_params->form_values->GCCAPC;
        if(strlen($m_params->form_values->GCIVES) > 0)
            $ar_upd1['GCIVES'] = $m_params->form_values->GCIVES;
        //update riga
        $sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
                SET " . create_name_field_by_ar_UPDATE($ar_upd1) . "
		        WHERE GCDT=? AND GCPROG=?";
	                    
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array_merge(
                $ar_upd1,
                array($id_ditta_default, $m_params->form_values->CCPROG)
                ));
        echo db2_stmt_errormsg($stmt);
	                    
	}
	
	
	//eventuale aggiornamento sconti promotion (in XX0S2TS0)
	$rowGC = get_gcrow_by_prog($m_params->form_values->CCPROG);
	$rowPromotion = get_row_sconti_promotion_by_codcli($rowGC['GCCDCF']);
	
	if ($rowPromotion){
	   //update
	   $ar_ins = array();
	   $ar_ins['TSUSUM'] = trim($auth->get_user());
	   $ar_ins['TSDTUM'] = oggi_AS_date();
	   
	   $ar_ins['TSSC1'] = sql_f($m_params->form_values->TSSC1);
	  /* $ar_ins['TSSC2'] = sql_f($m_params->form_values->TSSC2);
	   $ar_ins['TSSC3'] = sql_f($m_params->form_values->TSSC3);
	   $ar_ins['TSSC4'] = sql_f($m_params->form_values->TSSC4);*/
	   
	   $sql = "UPDATE {$cfg_mod_Gest['file_tasco']} TS
		          SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE RRN(TS) = {$rowPromotion['RRN_TS']}";
	   
	   $stmt = db2_prepare($conn, $sql);
	   echo db2_stmt_errormsg();
	   $result = db2_execute($stmt, $ar_ins);
	   echo db2_stmt_errormsg($stmt);	   
	   
	} else {
	    //Insert
	    if(trim($m_params->form_values->TSSC1) != '' && $m_params->form_values->TSSC1 > 0){
    	    $ar_ins = array();
    	    $ar_ins['TSDT'] = $id_ditta_default;
    	    $ar_ins['TSUSGE'] = trim($auth->get_user());
    	    $ar_ins['TSDTGE'] = oggi_AS_date();
    	    $ar_ins['TSUSUM'] = trim($auth->get_user());
    	    $ar_ins['TSDTUM'] = oggi_AS_date();
    	    
    	    $ar_ins['TSTPLI'] = 'V';
    	    $ar_ins['TSTPTB'] = 'P';
    	    $ar_ins['TSCLI'] = $rowGC['GCCDCF'];
    	    $ar_ins['TSART'] = 'PROMOTION';
    	    
    	    $ar_ins['TSSC1'] = sql_f($m_params->form_values->TSSC1);
    	  /*  $ar_ins['TSSC2'] = sql_f($m_params->form_values->TSSC2);
    	    $ar_ins['TSSC3'] = sql_f($m_params->form_values->TSSC3);
    	    $ar_ins['TSSC4'] = sql_f($m_params->form_values->TSSC4);*/
    	    
    	    //insert riga
    	    $sql = "INSERT INTO {$cfg_mod_Gest['file_tasco']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    	    
    	    $stmt = db2_prepare($conn, $sql);
    	    echo db2_stmt_errormsg();
    	    $result = db2_execute($stmt, $ar_ins);
    	    echo db2_stmt_errormsg($stmt);
	       }
	}
	
	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $m_params->form_values->CCPROG,
	            "RINOTE" => 'save_cc'
	        )
	    )
	    );
	
	echo acs_je($ret);
	
	exit;
}





// ******************************************************************************************
// EXE SAVE FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_fido'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();



	if ($m_params->open_request->mode == 'NEW') {

		$ar_ins['CCUSGE'] = trim($auth->get_user());
		$ar_ins['CCDTGE'] = oggi_AS_date();
		$ar_ins['CCORGE'] = oggi_AS_time();

		$ar_ins['CCDT'] 	= $id_ditta_default;
		$ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
		$ar_ins['CCSEZI'] 	= 'BFID';
		$ar_ins['CCCCON'] 	= utf8_decode(trim($m_params->form_values->CCCCON));
		$ar_ins['CCDCON'] 	= utf8_decode(trim($m_params->form_values->CCDCON));

		$ar_ins = exe_save_fido_add_field($ar_ins, $m_params);

		//insert riga
		$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}


	if ($m_params->open_request->mode == 'EDIT') {
		$ar_upd = exe_save_fido_add_field(array(), $m_params);

		$ar_upd['CCUSGE'] = trim($auth->get_user());
		$ar_upd['CCDTGE'] = oggi_AS_date();
		$ar_upd['CCORGE'] = oggi_AS_time();

		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
		SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, $m_params->form_values->CCSEZI, $m_params->form_values->CCCCON)
		));
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}
	
	
	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $m_params->form_values->CCPROG,
	            "RINOTE" => 'save_fido'
	        )
	    )
	    );
	

	echo acs_je($ret);
	exit;
}






// ******************************************************************************************
// EXE SAVE FORM LOGISTICA/CONFIGURAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_logis'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	$ar_upd = exe_save_logis_add_field(array(), $m_params);
	
	$ar_upd['GCUSGE'] = trim($auth->get_user());
	$ar_upd['GCDTGE'] = oggi_AS_date();
	$ar_upd['GCORGE'] = oggi_AS_time();
	
	
	//update riga
	$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE GCPROG=?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($m_params->form_values->GCPROG)
	));
	echo db2_stmt_errormsg($stmt);

	$ret['success'] = true;

	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $m_params->form_values->GCPROG
	        )
	    )
	    );
	
	
	$record = get_gcrow_by_prog($m_params->form_values->GCPROG);
	$record = array_map('rtrim', $record);	
	$ret['record'] = $record;	

	echo acs_je($ret);
	exit;
}

// ******************************************************************************************
// EXE SAVE FORM CRM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_crm'){
    
    $m_params = acs_m_params_json_decode();
    
    $ar_upd = array();
    $ret = array();

    $ar_upd = exe_save_crm_add_field($ar_upd, $m_params);
    
    $ar_upd['GCUSGE'] = trim($auth->get_user());
    $ar_upd['GCDTGE'] = oggi_AS_date();
    $ar_upd['GCORGE'] = oggi_AS_time();
    
    $ar_upd['GCUSUM'] = trim($auth->get_user());
    $ar_upd['GCDTUM'] = oggi_AS_date();
    $ar_upd['GCORUM'] = oggi_AS_time();


    //update riga
    $sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio_crm']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE GCPROG=?";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array_merge(
        $ar_upd,
        array($m_params->form_values->GCPROG)
        ));
    echo db2_stmt_errormsg($stmt);
    
    $ar_ACH = array();
    $ar_INS = array();
    foreach($m_params->form_values as $k => $v){
        if (substr($k, 0, 3) == 'ACH'){
            $k = str_replace("ACH", "f", $k);
            $ar_ACH[$k] = $v;
         }
        
        if (substr($k, 0, 3) == 'INS'){
            $k = str_replace("INS", "f", $k);
            $ar_INS[$k] = $v;
        }
      }
      
      $gc_row = get_gcrow_by_prog($m_params->GCPRAB);
      $my_row = get_gcrow_by_prog($m_params->form_values->GCPROG);
      if(trim($my_row['GCTPAN']) == 'DES')
          $rife = trim($gc_row['GCCDCF'])."_".trim($m_params->cliente);
      else
          $rife = trim($m_params->cliente);
      
      $main_module->exe_upd_commento_cliente($ar_ACH, 'CD', $rife, 'ACH');
      $main_module->exe_upd_commento_cliente($ar_INS, 'CD', $rife, 'INS');	
      
    
    $ret['success'] = true;
    
    //call AGG tramite RI
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory();
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_ANAG_CLI',
            "vals" => array(
                "RIPROG" => $m_params->form_values->GCPROG
            )
        )
        );
    
    
    $record = get_gcrow_by_prog($m_params->form_values->GCPROG);
    $record = array_map('rtrim', $record);
    $ret['record'] = $record;
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_save_config_logis'){
    
    $m_params = acs_m_params_json_decode();
    
    $ar_upd = array();
    $ret = array();
    
    $ar_upd = exe_save_configLogis_add_field($ar_upd, $m_params);
    
    $ar_upd['GCUSGE'] = trim($auth->get_user());
    $ar_upd['GCDTGE'] = oggi_AS_date();
    $ar_upd['GCORGE'] = oggi_AS_time();
    
    $ar_upd['GCUSUM'] = trim($auth->get_user());
    $ar_upd['GCDTUM'] = oggi_AS_date();
    $ar_upd['GCORUM'] = oggi_AS_time();
 
    //update riga
    $sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE GCPROG=?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array_merge(
        $ar_upd,
        array($m_params->form_values->GCPROG)
        ));
    echo db2_stmt_errormsg($stmt);
        
    $ret['success'] = true;
    
    //call AGG tramite RI
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory();
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_ANAG_CLI',
            "vals" => array(
                "RIPROG" => $m_params->form_values->GCPROG
            )
        )
        );
    
    $record = get_gcrow_by_prog($m_params->form_values->GCPROG);
    $record = array_map('rtrim', $record);
    $ret['record'] = $record;
    
    echo acs_je($ret);
    exit;
}



// ******************************************************************************************
// ELENCO CONDIZIONI COMMERCIALI PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_cc'){

	$m_params = acs_m_params_json_decode();
	
	if (strlen($m_params->rec_id) == 0){
	    echo acs_je(array());
	    exit;
	}
	
	$sql_where = "";
	$ar_ins = array();
	$ret = array();
	
	$id_ditta_default_W = trim($id_ditta_default) . 'W';
	$row_gc_modan = get_gcrow_by_modan_default($m_params->GCDEFA);
	

	/*$sql = "SELECT TA.*, AN.*, GC.*, '{$m_params->rec_id}' AS Q_REC_ID 
            FROM {$cfg_mod_Gest['file_tabelle']} TA
			INNER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}  AN ON
				CCDT = ? AND CCPROG = ? AND CCSEZI = 'BCCO' AND CCCCON = TAKEY2 
            LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                 ON CCDT = GCDT AND CCPROG = GCPROG
			WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ANCCO' AND TAKEY1 = ?
            ORDER BY
                CASE WHEN CCCCON = 'STD' THEN 0 ELSE 1 END,
                CCCCON
            ";
	
	$sqlD = "SELECT RRN(CC) AS RRN, CC.*
	         FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
	         WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id, $m_params->GCDEFA ));
	echo db2_stmt_errormsg($stmt);
	
	while ($row = db2_fetch_assoc($stmt)) {
	   
	    $n = array();
	    $n['codice'] = $row['TAKEY2'];
	    $n['descrizione'] = $row['TADESC'];
	    if(strlen($row['CCCCON']) > 0)
	        $n['caricata'] = $row['CCCCON'];
	    else
	        $n['caricata'] = "";
	    $n['prog'] = $m_params->rec_id;
	    $n['cdcf'] = $m_params->cdcf;
	    $n['ditta'] = $id_ditta_default;
	    $n['condizioni'] = 'BCCO';
	    $n['dettagli'] = "";
	    if(trim($row['CCAG1']) != ''){
	       $ta_sys_ag = find_TA_sys('CUAG', trim($row['CCAG1']));
	       $n['dettagli'] .= "Agente: ".$ta_sys_ag[0]['text']." [".trim($row['CCAG1'])."]";
	       //CCPA1
	    }
	    
	    if($row['CCPA1'] > 0){
	        if(trim($row['CCAG1']) != '')
	            $n['dettagli'] .= ", ". n($row['CCPA1'],0)."%";
	        else
	            $n['dettagli'] .= "Agente: , ". n($row['CCPA1'],0)."%";
	    }
	    
	    if(trim($row['CCREFE']) != ''){
	       $ta_sys_rf = find_TA_sys('BREF', trim($row['CCREFE']));
	       if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	       $n['dettagli'] .= "Referente: ".$ta_sys_rf[0]['text']." [".trim($row['CCREFE'])."]";
	    }
	    if(trim($row['CCPAGA']) != ''){
	       $ta_sys_pg = find_TA_sys('CUCP', trim($row['CCPAGA']));
	       if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	       $n['dettagli'] .= "Pagamento: ".$ta_sys_pg[0]['text']." [".trim($row['CCPAGA'])."]";
	    }
	    
	    //des_banca
	    if (strlen(trim($row['CCABI'])) > 0 && strlen(trim($row['CCCAB'])) > 0) {
	        $sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
	        $stmtB = db2_prepare($conn, $sql);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmtB, array($row['CCABI'], $row['CCCAB']));
	        $r_cab = db2_fetch_assoc($stmtB);
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Banca: ".trim($r_cab['XDSABI']) ."-".trim($r_cab['XDSCAB']);
	    }
	    
	    if(trim($row['CCSC1']) > 0){
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Sconti: ".n($row['CCSC1'],2)."+".n($row['CCSC2'],2)."+".n($row['CCSC3'],2)."+".n($row['CCSC4'],2);
	    }
	    
	  	 
	    if(trim($row['CCLIST']) != ''){
	        if(trim($row['CCSC1']) > 0)
	            $n['dettagli'] .= " [Listino ".trim($row['CCLIST']).", ".trim($row['CCVALU'])."]";
	        else
	            $n['dettagli'] .= "<br> [Listino ".trim($row['CCLIST']).", ".trim($row['CCVALU'])."]";
	    }
	    
	  
	    
	    if(trim($row['CCSC5']) > 0){
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Sc.doc: ".n($row['CCSC5'],2)."+".n($row['CCSC6'],2)."+".n($row['CCSC7'],2)."+".n($row['CCSC8'],2);
	    }
	    
	    if(trim($row['GCIVES']) != ''){
	        $ta_sys_as = find_TA_sys('CUAE', trim($row['GCIVES']));
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Assoggettamento: ".$ta_sys_as[0]['text']." [".trim($row['GCIVES'])."]";
	    }
	    
	   
	    
	    if($row['CCDTVI'] > 0 || $row['CCDTVF'] > 0)
	    $n['val'] = "Dal: ".print_date($row['CCDTVI']). "<br> Al: ".print_date($row['CCDTVF']);
	    if($row['CCDTVF'] > 0 && $row['CCDTVF'] < oggi_AS_date())
	        $n['scaduta'] = 'Y';

	  
	  if(trim($row['TAKEY2']) != 'STD'){
	   $stmtD = db2_prepare($conn, $sqlD);
	   echo db2_stmt_errormsg();
	   $result = db2_execute($stmtD, array($id_ditta_default_W, $row_gc_modan['GCPROG'], 'BCCO', trim($row['TAKEY2'])));
	   $r_def = db2_fetch_assoc($stmtD);
	   
	   $stmtC = db2_prepare($conn, $sqlD);
	   $result = db2_execute($stmtC, array($id_ditta_default, $m_params->rec_id, 'BCCO', trim($row['TAKEY2'])));
	   $r_cc = db2_fetch_assoc($stmtC);
	   $n['rrn'] = $r_cc['RRN'];
	  
      }
	  
	  
	  if(trim($row['CCDIVI']) == '' && trim($row['CCCCON']) != 'STD')
	      $divisione = trim($r_def['CCDIVI']);
	  else    
	      $divisione = trim($row['CCDIVI']);
	  
	  $ta_sys_div = get_TA_sys('DDOC', $divisione);
	   
	  if(trim($row['TAKEY2']) == 'STD')
	      $n['tipo'] = "1) Non condizionate";
      else
	      $n['tipo'] = "2) Condizionate [{$divisione}] ".$ta_sys_div['text'];
	    $ret[] = $n;
	}
	
	$codici = array();
	foreach($ret as $k => $v)
	    $codici[] = $v['codice'];

    if(count($codici) > 0)
        $sql_where .=   " AND CCCCON NOT IN (" . sql_t_IN($codici) . ") ";	*/	
    
   $sql_where .= sql_where_params_comm($m_params->filtri);
	    
	$sql_cc = "SELECT CC.*, RRN(CC) AS RRN, TA.TADESC AS D_CC
               FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
               LEFT OUTER JOIN {$cfg_mod_Gest['file_tabelle']} TA 
				 ON TA.TADT = CCDT AND TA.TATAID = 'ANCCO' AND CCCCON = TA.TAKEY2 AND TA.TAKEY1 = '{$m_params->GCDEFA}'
	           WHERE CCDT = '{$id_ditta_default}' AND CCPROG = '{$m_params->rec_id}' 
               AND CCSEZI = 'BCCO' {$sql_where}
               ORDER BY
               CASE WHEN CCDTVF < " . oggi_AS_date() . " THEN 9 ELSE 0 END,
               CCDTVI
            ";
		
	$stmt_cc = db2_prepare($conn, $sql_cc);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_cc);
	echo db2_stmt_errormsg($stmt_cc);
	
	
	while ($row_cc = db2_fetch_assoc($stmt_cc)) {
	    $n = array();
	    $n['rrn'] = $row_cc['RRN'];
	    $n['codice'] = $row_cc['CCCCON'];
	    if(trim($n['codice']) == 'STD') 
	       $n['descrizione'] = $row_cc['D_CC'];
	    else
	       $n['descrizione'] = $row_cc['CCDCON'];
	    $n['caricata'] = 'Y';
	    $n['prog'] = $row_cc['CCPROG'];
	    $n['ditta'] = $row_cc['CCDT'];
	    $n['condizioni'] = $row_cc['CCSEZI'];
	    $n['dettagli'] = "";
	    if(trim($row_cc['CCAG1']) != ''){
	        $ta_sys_ag = find_TA_sys('CUAG', trim($row_cc['CCAG1']));
	        $n['dettagli'] .= "Agente: ".$ta_sys_ag[0]['text']." [".trim($row_cc['CCAG1'])."]";
	    }
	    
	    
	    if($row_cc['CCPA1'] > 0){
	        if(trim($row_cc['CCAG1']) != '')
	            $n['dettagli'] .= ", ". n($row_cc['CCPA1'], 0)."%";
            else
                $n['dettagli'] .= "Agente: , ". n($row_cc['CCPA1'], 0)."%";
	    }
	    
	    
	    if(trim($row_cc['CCREFE']) != ''){
	        $ta_sys_rf = find_TA_sys('BREF', trim($row_cc['CCREFE']));
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Referente: ".$ta_sys_rf[0]['text']." [".trim($row_cc['CCREFE'])."]";
	    }
	    if(trim($row_cc['CCPAGA']) != ''){
	        $ta_sys_pg = find_TA_sys('CUCP', trim($row_cc['CCPAGA']));
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Pagamento: ".$ta_sys_pg[0]['text']." [".trim($row_cc['CCPAGA'])."]";
	    }
	   
	    //des_banca
	    if (strlen(trim($row_cc['CCABI'])) > 0 && strlen(trim($row_cc['CCCAB'])) > 0) {
	        $sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
	        $stmt = db2_prepare($conn, $sql);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt, array($row_cc['CCABI'], $row_cc['CCCAB']));
	        $r_cab = db2_fetch_assoc($stmt);
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Banca: ".trim($r_cab['XDSABI']) ."-".trim($r_cab['XDSCAB']);
	    }
	    
	    if(trim($row_cc['CCSC1']) > 0){
	        if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
	        $n['dettagli'] .= "Sconti: ".n($row_cc['CCSC1'],2)."+".n($row_cc['CCSC2'],2)."+".n($row_cc['CCSC3'],2)."+".n($row_cc['CCSC4'],2);
	    }
	    
	    if(trim($row_cc['CCLIST']) != ''){
	        if(trim($row_cc['CCSC1']) > 0)
	            $n['dettagli'] .= " [Listino ".trim($row_cc['CCLIST'])."]";
            else
                $n['dettagli'] .= "<br> [Listino ".trim($row_cc['CCLIST'])."]";
	    }
	    
	    if(trim($row_cc['CCCCON']) != 'STD')
	       $n['val'] = "Dal: ".print_date2($row_cc['CCDTVI']). "<br> Al: ".print_date2($row_cc['CCDTVF']);
	    
	    if($row_cc['CCDTVF'] > 0 && $row_cc['CCDTVF'] < oggi_AS_date())
	        $n['scaduta'] = 'Y';
	    
	    $divisione = trim($row_cc['CCDIVI']);
        
        
        if(trim($row_cc['CCCCON']) == 'STD')
            $n['tipo'] = "1) Non condizionate";
        else {
            if (strlen(trim($divisione)) > 0)
                $ta_sys_div = get_TA_sys('DDOC', $divisione);
            else 
                $ta_sys_div = array('text' => '');
            
            $n['tipo'] = "2) Condizionate - [".$divisione."] ".$ta_sys_div['text'];
        }
            
	    $ret[] = $n;
	}

	echo acs_je($ret);
	exit;
}







// ******************************************************************************************
// ELENCO CONDIZIONI AMMINISTRATIVE PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_fido'){

	$m_params = acs_m_params_json_decode();
	
	if (strlen($m_params->rec_id) == 0){
	    echo acs_je(array());
	    exit;
	}

	$ar_ins = array();
	$ret = array();

	$sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID FROM {$cfg_mod_Gest['file_tabelle']} TA
			LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}  AN ON
			CCDT = ? AND CCPROG = ? AND CCSEZI = 'BFID' AND CCCCON = TAKEY1
			WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ANC05'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id));
	echo db2_stmt_errormsg($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$ret[] = $row;
	}

	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// OPEN TAB
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){ ?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'fit'
	},
    <?php echo make_tab_closable(); ?>,	
    title: 'Anagrafica aziende',
    tbar: new Ext.Toolbar({
      items:[
            '<b>Gestione scheda anagrafica clienti</b>', '->',
            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').expandAll();}}
            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').collapseAll();}}
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
      ]            
    }),  
	
	
	items: [  
  		  <?php write_main_tree(array(), $m_params->form_open, $m_params->recenti); ?>
  	    , <?php // write_main_form(array()); ?>
  	]
  }
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// NUOVA ANAGRAFICA
// ******************************************************************************************
$m_params = acs_m_params_json_decode();
if ($_REQUEST['fn'] == 'new_anag_cli' /* && $m_params->i_e == 'I' */){ ?>
{
 success:true,  
 items: [
  	{
		xtype: 'tabpanel',
		GCPROG: <?php echo j($m_params->rec_id) ?>,
		std_caricata : false,
		original_record: [],
		
		<?php
		$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
		
		//se rientro in modifica, ricarico il record		
		if ($all_params['mode'] == 'EDIT') {
			$row = get_gcrow_by_prog($all_params['rec_id']);						
		} else {
		    //NEW
		    $row = array();
		    
		    //in base a MODAN recupero il cliente di default (TACOGE) sulla ditta 1W
		    // e da questo record prendo alcuni default
		    $rowD = get_gcrow_by_modan_default($all_params['modan']);
		    
		    if ($rowD){
		        $row['GCRISC'] = $rowD['GCRISC'];      //rischio		        
		    }
		    
		    if (strlen($row['GCNAZI']) == 0 && $m_params->i_e == 'I')
		        $row['GCNAZI'] = $cfg_mod_Gest['cod_naz_italia'];
		        
		}
		?>
		
		items: [
		
		   	<?php write_new_form_ITA(
    		   	    array(
    		   	        'tipo_anagrafica' => 'CLI'
    		   	    ),
		   			$all_params,
		   			$row
		   	); ?>, 	

			
			 <?php write_new_list_COMM(  //$all_params['mode'] == 'NEW'
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>,
			
	  	<?php write_new_form_LOGIS(
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>,
		   	
		   	<?php write_new_list_FIDO(
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>, 	
		   	
		    <?php write_new_form_CRM(
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>
		   	
		]
		
		,listeners : {
		   afterrender: function (comp) {
		   <?php  
		   if($all_params['mode'] == 'EDIT'){
		       $nrow = get_gcrow_by_prog($row['GCPROG']);
		       $title = "[".$nrow['GCCDCF'].", ".trim($nrow['GCCINT'])."] ".trim($nrow['GCDCON']);
		       ?>
		       
	           comp.up('window').setTitle('Modifica anagrafica ' + <?php echo j($title); ?>);	 				
		 	<?php }?>
		 	
		 	},
		 	
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
				
			},
		
		
		}
		
		
		, acs_actions : {
		   
		   exe_afterSave: function (m_window){
		        var tabPanel = m_window.down('tabpanel');
		        var activeTab = tabPanel.getActiveTab();
                var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                tabPanel.setActiveTab(activeTabIndex + 1);
                
                if(activeTabIndex < tabPanel.items.length - 1){
                	tabPanel.setActiveTab(activeTabIndex + 1);
                } else {
                	m_window.close();
					acs_show_msg_info('Salvataggio completato');
                }
                
           }
		   
		   
		   
		   }
		
	}
 ]
}  	
<?php exit; } ?>

<?php
// ******************************************************************************************
// NUOVA DESTINAZIONE - ITALIA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'new_dest' /*&& $m_params->i_e == 'I'*/){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
		$row = get_gcrow_by_prog($all_params['rec_id']);		
		$rowProg = get_gcrow_by_prog($row['GCPRAB']);
		$row['prog_cli'] = $rowProg['GCPROG'];
		$row['cod_cli'] = $rowProg['GCCDCF'];
		$row['cod_des'] = trim($row['GCCDCF']);
		$ret_des = $s->get_cliente_des_anag($id_ditta_default, $row['cod_cli'], trim($row['GCDSTA']));
		$row['IND_D'] = $ret_des['IND_D']; 
		$row['LOC_D'] = $ret_des['LOC_D']; 
		$row['CAP_D'] = $ret_des['CAP_D']; 
		$row['PRO_D'] = $ret_des['PRO_D']; 
	} else {
	    //NEW
	    $rowProg = get_gcrow_by_prog($all_params['parent_prog']);
	    $row = array();
	    $row['cod_cli'] = $rowProg['GCCDCF'];
	    $row['GCCDCF']  = $rowProg['GCCDCF'];
	    
	    if ($m_params->i_e == 'I')
	        $row['GCNAZI'] = $cfg_mod_Gest['cod_naz_italia'];
	}
	
	?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'DES'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>,
			   	
		   	
		   	  	 <?php write_new_form_ConfigLogis(
		   			array(),
			   	    array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
		   			$row
		   	    ); ?>
		   	    ,
		   	    <?php 
		   	      if($row['GCTPIN'] != 'D' && $all_params['type'] != 'D'){
			   	    write_new_form_CRM(
		   			array(),
			   	    array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
		   			$row
		   	    ); 
                }?>	 
		   	
		   	  
		], 	listeners : {
		   
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
		
	  , acs_actions : {
		   
		   exe_afterSave: function (m_window){
		        var tabPanel = m_window.down('tabpanel');
		        var activeTab = tabPanel.getActiveTab();
                var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                tabPanel.setActiveTab(activeTabIndex + 1);
                
                if(activeTabIndex < tabPanel.items.length - 1){
                	tabPanel.setActiveTab(activeTabIndex + 1);
                } else {
                	m_window.close();
					acs_show_msg_info('Salvataggio completato');
                }
                
           }
		   
		   
		   
		   }
	}
 ]
}
<?php exit; } ?>

<?php

//ESTEROOOOO
if ($_REQUEST['fn'] == 'new_pven' && $m_params->i_e == 'E'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
		$row = get_gcrow_by_prog($all_params['rec_id']);
	}	
	
?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'PVEN'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>,
			   	
			   	 <?php write_new_form_CRM(
		   			array(),
			   	     array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
		   			$row
		   	); ?>	  
		],  listeners : {
		   
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
	}
 ]
}  	
<?php exit; } ?>

<?php
if ($_REQUEST['fn'] == 'new_pven' && $m_params->i_e == 'I'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
		$row = get_gcrow_by_prog($all_params['rec_id']);
	} else {
	    //NEW
	    
	    if ($m_params->i_e == 'I')
	        $row['GCNAZI'] = $cfg_mod_Gest['cod_naz_italia'];
	}
	
	
?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'PVEN'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>,
			   	
			   	 <?php write_new_form_CRM(
		   			array(),
			   	     array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
		   			$row
		   	); ?>	  
		],
		 listeners : {
		   
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
	}
 ]
}  	
<?php exit; } ?>



<?php
// ******************************************************************************************
// OPEN FORM COMM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_COMM'){ 
    $all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
    ?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'panel',
		layout: 'fit',
		items: [
				
			   	<?php write_new_form_COMM(
			   			array(),
			   	        $all_params
			   	); ?>
		],
		  listeners : {
		   afterrender: function (comp) {
		   <?php  
		       $nrow = get_gcrow_by_prog($all_params['CCPROG']);
		       $title = "[".$nrow['GCCDCF'].", ".trim($nrow['GCCINT'])."] ".trim($nrow['GCDCON']);
		    ?>
		       
	           comp.up('window').setTitle(comp.up('window').title + ' ' + <?php echo j($title); ?>);	 				
		 	
		 	
		 	}

		}
	}

 ]

}  	
<?php exit; } ?>

<?php
// ******************************************************************************************
// OPEN FORM FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_FIDO'){ ?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'panel',
		layout: 'fit',
		items: [
				
			   	<?php write_new_form_FIDO(
			   			array(),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } ?>

<?php
/********************************************************************************** 
 WIDGET
 **********************************************************************************/
function write_main_tree($p, $form_ep, $recenti){
    global $s, $cfg_mod_Gest;	
?>	
			{
				xtype: 'treepanel',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        
		        rootVisible: false,
		        loadMask: true,
		        stateful: true,
        		stateId: 'panel-anag-cli',
        		stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					    fields: ['iconCls', 'trCls', 'task', 'liv', 'liv_data', 'liv_cod', 'liv_cod_out', 'liv_cod_qtip', 'd_indi',
					    	'GCDT', 'GCPROG', 'sosp', 'note', 'cliente', 'contract', 'GCUSUM', 'GCUSGE', 'GCDTGE', 'GCDTUM', 'des_std', 'omp',
					    	'GCDCON', 'GCMAIL', 'GCWWW', 'GCSIRE', 'GCMAI1', 'GCMAI2', 'GCMAI3', 'GCMAI4', 'GCMAI7', 'GCMAI8', 'GCTEL', 'GCTEL2', 'GCFAX', 'out_codice',
					    	'GCINDI', 'GCCDCF', 'GCCDFI', 'GCPIVA', 'GCLOCA', 'GCDSTA', 'GCTPAN', 'GCPROV', 'GCCATC', 'GCIVES', 'GCCAPC', 'schede',
					    	'CFUSUM', 'CFUSGE', 'CFDTGE', 'CFDTUM', 'cod_fi', 'GCCRM1', 'GCCAP', 'GCNAZI', 'd_nazione', 'rrn_ta', 't_indi', 'GCCIVI', 'ciclo', 'd_ciclo',
					    	'GCANCO', 'tooltip_anagrafica_collegata', 'data_ge', 'data_um', 'ute_ge', 'ute_um', 'c_cond', 'val_cond', 'g_paga', 'k_cli_des'
					    ],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							actionMethods: {read: 'POST'},
							timeout: 240000,
	                        reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }, 
	                        extraParams: {
	                          form_ep : <?php echo acs_je($form_ep); ?>,
	                          recenti : <?php echo j($recenti); ?>
							}
	                	    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),
	
	            multiSelect: false,
		        singleExpand: false,
		        
		        <?php  $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
		        <?php  $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>"; ?>
		        <?php  $schede = "<img src=" . img_path("icone/48x48/tag.png") . " height=20>"; ?>
		        <?php  $contr = "<img src=" . img_path("icone/48x48/blog.png") . " height=20>"; ?>
		
				columns: [	
				 {text: '<?php echo $sosp; ?>', width: 40, dataIndex: 'sosp', 
				  tooltip: 'Sospeso',
				  renderer: function(value, metaData, record){
				          if(record.get('liv') == 'liv_2'){
				               if(record.get('ciclo') != '')
				               	  metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('d_ciclo')) + '\"';
				               if(record.get('ciclo') == '2') 
    		    		 	       metaData.tdCls += ' sfondo_verde';
    	    		 	       if(record.get('ciclo') == '1' || record.get('ciclo') == '3') 
    		    		 	       metaData.tdCls += ' sfondo_arancione';
		    		 	   }    
		    			  if(record.get('sosp') == 'S'){ 
                              metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			      return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	   }else{
		    		 	   	  if(record.get('omp') == 'P')
		    		 	   	  return '<img src=<?php echo img_path("icone/48x48/gift.png") ?> width=18>';
		    		 	   }
		    		 
		    		 	   
					 }}
				   ,{
				    text: '<?php echo $note; ?>',
				    width: 32, 
				    tooltip: 'Blocco note',
				    tdCls: 'tdAction',  
				    dataIndex : 'note',       		       		        
					renderer: function(value, metaData, record){
    					if(record.get('liv') == 'liv_2'){
    						if (record.get('note') == true) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
    			    		if (record.get('note') == false) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
    			    	    if (record.get('note') == 'N') return '';
    			    	   
    			    	}
    		
			    	}
			    	}
			    	, {text: 'Tipo<br>Sede', tooltip : 'Tipo indirizzo', width: 40, dataIndex: 'iconCls',  xtype: 'treecolumn',
			    	   renderer: function(value, metaData, record){
			    	        if (record.get('des_std') == 'S') metaData.tdCls += ' sfondo_verde';			
			    	        return '';
                    	} }
                	, {text: 'Codice', width: 100, dataIndex: 'out_codice', tdCls: ' grassetto'}
                	, {text: '<?php  echo "<img src=" . img_path("icone/48x48/globe.png") . " height=20>"; ?>', width: 30, dataIndex: 'GCANCO', tooltip: 'Anagrafica collegata',
                    	 renderer: function(value, metaData, record){
                    	         if(record.get('GCANCO') > 0){
                        	          var q_tip = 'Anagrafica collegata:<br/> ' + record.get('tooltip_anagrafica_collegata');
                    			      metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';  	
                    	         }
                                 if(record.get('GCANCO') > 0)		    	
                    		     	return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=18>';
                    		     else
                    		     	return '';
                    	}
                	}
                	, {text: '<?php echo $schede; ?>', 	
        			   width: 30, 
        			   dataIndex: 'schede',
        			   tooltip: 'Scheda',
        			   renderer: function(value, p, record){
        				  if(record.get('schede') > 0)
        	    			 return '<img src=<?php echo img_path("icone/48x48/tag.png") ?> width=15>';
        	    		 	
        	    	   }
	    		     }
                	,{text: '<?php  echo "<img src=" . img_path("icone/48x48/shopping_bag.png") . " height=20>"; ?>', width: 30, dataIndex: 'condizioni', tooltip: 'Condizioni commerciali',
                    	 renderer: function(value, metaData, record){
                    	      if(record.get('c_cond') > 0 && record.get('val_cond') >= '<?php echo oggi_AS_date(); ?>')
                    	       		return '<img src=<?php echo img_path("icone/48x48/shopping_bag.png") ?> width=18>';
                    	       if(record.get('c_cond') > 0 && record.get('val_cond') < '<?php echo oggi_AS_date(); ?>')
                    	       		return '<img src=<?php echo img_path("icone/48x48/shopping_bag_gray.png") ?> width=18>';
                    	}
                	}
                    ,{text: '<?php echo $contr; ?>', width: 30, dataIndex: 'contract', tooltip: 'Contract',
                    	 renderer: function(value, metaData, record){
                    	  if(record.get('liv') == 'liv_2'){
                    	      if(record.get('contract') > 0)
                    	       		return '<img src=<?php echo img_path("icone/48x48/blog.png") ?> width=18>';
                    	   }  
                    	}
                	}
		    		, {text: 'Ragione sociale/Denominazione', flex: 1, dataIndex: 'task', menuDisabled: true, tdCls: ' grassetto'}
		    		, {text: 'Localit&agrave;', flex: 1, dataIndex: 'GCLOCA', tdCls: ' grassetto'}					
		    		, {text: 'Indirizzo', flex: 1, dataIndex: 'GCINDI'}
		    		, {text: 'Cap', width: 60, dataIndex: 'GCCAP'}
		    		, {text: 'Pr.', width: 40, dataIndex: 'GCPROV', tdCls: ' grassetto'}
		    		, {text: 'Naz.', width: 40, dataIndex: 'GCNAZI', tdCls: ' grassetto',
		    		   renderer: function(value, metaData, record){
    					      if (record.get('GCNAZI').trim() != '') 
                        	      metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('d_nazione')) + '"'; 
            	          	  
            	          	  return value;	
                    	}}
					, {text: 'Cod.Fiscale', width: 150, dataIndex: 'cod_fi',
    					 renderer: function(value, metaData, record){
    					      if (record.get('cod_fi').trim() != record.get('GCPIVA').trim()) 
                        	          	metaData.tdCls += ' grassetto';
            	          	  return value;	
                    	}}
					, {text: 'Partita IVA', width: 130, dataIndex: 'GCPIVA',
					     renderer: function(value, metaData, record){
					      if (record.get('GCPIVA').trim() != record.get('cod_fi').trim()) 
                    	          	metaData.tdCls += ' grassetto';
        	          	  if (record.get('GCPIVA').trim() == record.get('cod_fi').trim()) 
                    	          	metaData.tdCls += ' grassetto';
                          return value;	
                	}}
					, {header: 'Immissione',
                	 columns: [
                	 {header: 'Data', dataIndex: 'data_ge', renderer: date_from_AS, width: 70, sortable : true,
                	 renderer: function(value, metaData, record){
                	         if(record.get('data_um') > 0){
                    	         if (record.get('data_ge') != record.get('data_um')) 
                    	          	metaData.tdCls += ' grassetto';
                    	          	
                    	          q_tip = 'Modifica: ' + date_from_AS(record.get('data_um')) + ', Utente ' +record.get('ute_um');
                			      metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';  	
                	         }
                              			    	
                		     return date_from_AS(value);	
                	}
                	 
                	 }
                	 ,{header: 'Utente', dataIndex: 'ute_ge', width: 70, sortable : true,
                	  renderer: function(value, metaData, record){
                	  
                	  	
                	        if(!Ext.isEmpty(record.get('ute_um')) && record.get('ute_um').trim() != ''){
                	  			if (record.get('ute_ge') != record.get('ute_um')) 
                	          		metaData.tdCls += ' grassetto';
                	          		
                	          q_tip = 'Modifica: ' + date_from_AS(record.get('data_um')) + ', Utente ' +record.get('ute_um');
                			  metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';   		
                	        }  
                	  
                              			    	
                	    return value;	
                	} 
                	 }
                	 ]}
					
				],
				enableSort: true,
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
					    celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						    iEvent.stopEvent();
						    var col_name = iView.getGridColumns()[iColIdx].dataIndex,
						    	rec = iView.getRecord(iRowEl),
	            				my_tree = iView;
	            				
								my_listeners_upd_dest = {
	        					afterEditRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_tree.getTreeStore().load();		        						
	        						from_win.close();
					        		}, 
					        		
					             afterInsertRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_tree.getTreeStore().load();		        						
	        						from_win.close();
					        		} 
			    				};					  
						  
						  

							
							if(col_name != 'note' && col_name != 'condizioni' && col_name != 'contract'){	
										
								if (rec.get('GCTPAN').trim() == 'DES') {
									acs_show_win_std('Scheda ' + rec.get('d_indi') + ' ['  + rec.get('GCCDCF').trim() +']', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 750, 600, {}, 'icon-globe-16');							
								 	return;
								}
								
								if (rec.get('GCTPAN').trim() == 'PVEN') {
									acs_show_win_std('Modifica anagrafica', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 750, 550, {}, 'icon-globe-16');							
								 	return;
								}								
													  					
							 if(col_name == 'schede' && rec.get('GCTPAN').trim() == 'CLI'){
					   	     	acs_show_win_std('Seleziona scheda [' + rec.get('GCCDCF').trim() + '] ' + rec.get('task').trim(), 'acs_anag_cli_schede_aggiuntive_new.php?fn=open_form', {cliente: rec.get('cliente')}, 500, 400, null, 'icon-tag-16');		  					
							 	return false;
							 }						  					
													  					
							//nel caso di anagrafica cliente, prima richiedo aggiornamento della stessa
							if (rec.get('GCTPAN').trim() == 'CLI' && rec.get('GCCDCF').trim().length > 0){
						    	Ext.getBody().mask('Loading... ', 'loading').show();
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_import_anag',
									        timeout: 2400000,
									        jsonData: {
									        	gcprog: rec.get('GCPROG'),
									        	gccdcf: rec.get('GCCDCF').trim() 
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									        	Ext.getBody().unmask();
									            var jsonData = Ext.decode(result.responseText);
																	  						  	
												acs_show_win_std('Modifica anagrafica',  
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
													{
										  				mode: 'EDIT',
										  				modan: 'ToDo',
										  				i_e: 'I', //ToDo
										  				rec_id: jsonData.id_prog,
										  				gccdcf: rec.get('GCCDCF').trim(),
										  				g_paga : rec.get('g_paga')
										  			}, 750, 600, {}, 'icon-globe-16');						  
										  	
										  	return false;									            
									            									            
									        }, //success
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });  						    	
						    	
						    	return;
						    }							   					  					
													  					
													  					
													  						  	
								acs_show_win_std('Modifica anagrafica', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				i_e: 'I', //ToDo
						  				rec_id: rec.get('id'),
						  				gccdcf: rec.get('GCCDCF').trim()
						  			}, 750, 600, {}, 'icon-globe-16');						  
						  	
						  	return false;
						  	
						  	}
						  	
						  	
						  	if (rec.get('liv') == 'liv_2' && col_name == 'note')
								show_win_bl_cliente(rec.get('cliente'), rec.get('task'));
						    
						    if (rec.get('liv') == 'liv_2' && col_name == 'condizioni'){
						         acs_show_win_std('Condizioni commerciali cliente ' + rec.get('GCDCON'), 
									'acs_anag_cli_cc.php?fn=open_form', 
									{prog: rec.get('GCPROG')}, 800, 550, {}, 'icon-shopping_bag-16');
						    
						    }
						    
						    if (rec.get('liv') == 'liv_2' && col_name == 'contract'){
						          acs_show_win_std('Elenco contract [' + rec.get('GCCDCF').trim() + '] ' + rec.get('task').trim(), 
    				         	'../base/acs_seleziona_ordini_gest.php?fn=open', {
    				         		cod_cli: rec.get('GCCDCF'),
    				         		doc_gest_search: 'CONTRACT_abbina',
    				         		show_parameters: false,
    				         		show_det : 'Y',
    				         		auto_load: true
    				         	}, 1000, 550,  {}, 'icon-blog-16');
						    
						    }
						    
						    
						    
    					   		 
    					   	  
					   			
					   		 
						    
						
						  				
						  }
					    },
			            
						itemclick: function(view, record, item, index, e) {
								//form = this.up('panel').down('form');
								//console.log(record);
								//form.loadRecord(record);
						},			            
			          
			          
			          itemcontextmenu : function(grid, rec, node, index, event) {
			          		event.stopEvent();
				  													  
					 		var voci_menu = [];
				     		var row = rec.data;
				     		var m_grid = this;
					     		
			                id_selected = grid.getSelectionModel().getSelection();							
						    list_selected_id = [];
						    for (var i=0; i<id_selected.length; i++) 
							  list_selected_id.push(id_selected[i].data.cliente);
							
			                voci_menu.push({
			         		   text: 'Sospendi/Attiva',
			        		   iconCls : 'icon-divieto-16',          		
			        		   handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          rec.set('sosp', jsonData.new_value);	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
			    				
			    				if(rec.get('GCTPAN').trim() == 'CLI'){
			    				
			    				my_listeners_inserimento = {
		        					afterInsertRecord: function(from_win){	
		        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente
		        						//rec.store.treeStore.load({node: rec.parentNode});		        						
		        						from_win.close();
						        		}
				    				};	
			    		
			    		  voci_menu.push({
						         		text: 'Inserimento nuovo stato/attivit&agrave;',
						        		iconCls : 'icon-arrivi-16',          		
						        		handler: function() {
										acs_show_win_std('Nuovo stato/attivit&agrave;', 
											<?php echo j('acs_anag_cli_create_attivita.php'); ?>, 
											{	tipo_op: 'ANCLI',
												rif : 'CLI',
								  				list_selected_id: list_selected_id
								  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
								  							        		          		
						        		}
						    		});
						    		
						    		
						    		
						  		  voci_menu.push({
						         		text: 'Gestione stato/attivit&agrave; per cliente',
						        		iconCls : 'icon-arrivi-16',          		
						        		handler: function() {
										acs_show_panel_std( 
											<?php echo j('acs_panel_todolist.php?fn=open_panel'); ?>,
											null, {cliente: rec.get('cliente')} 
											);						        		          		
						        		}
						    		});	
						    		                        			  
                        			   voci_menu.push({
                        	      		text: 'Mezzi assegnati',
                        	    		iconCls : 'iconSpedizione',      		
                        	    		handler: function() {
                        	    			acs_show_win_std('Mezzi assegnati cliente '+rec.get('cliente'), 
                        						'../desk_vend/acs_mezzi_assegnati.php?fn=open_grid', 
                        						{k_cli_des: rec.get('k_cli_des')}, 600, 400, null, 'iconSpedizione');
                        	    		}
                        			  });
                        			  
                        			   <?php if($cfg_mod_Gest['gestione_avanzata_ciclo_vita'] == 'Y'){?>
                        			   voci_menu.push({
                        	      		text: 'Assegna ciclo di vita',
                        	    		iconCls : 'icon-blog_compose-16',      		
                        	    		handler: function() {
                        	    		    
                        	    		    my_listeners = {
            		        					afterAssegna: function(from_win, ciclo){	
            		        						rec.set('ciclo', ciclo);		        						
            		        						from_win.close();
            						        		}
            				    				};	
                        	    		    
                        	    			acs_show_win_std('Assegna ciclo di vita', 'acs_gestione_ciclo_vita.php?fn=change_ciclo', {prog : rec.get('GCPROG')}, 400, 150,  my_listeners, 'icon-blog_compose-16');	
                        	    		}
                        			  });
                        			  <?php }?>
						    		
						    
			    				}else{
			    				  if(rec.get('t_indi') == 'D' || rec.get('t_indi') == ''){
			    				
			    					 voci_menu.push({
                        	      		text: 'Mezzi assegnati',
                        	    		iconCls : 'iconSpedizione',      		
                        	    		handler: function() {
                        	    			acs_show_win_std('Mezzi assegnati cliente '+rec.get('cliente'), 
                        						'../desk_vend/acs_mezzi_assegnati.php?fn=open_grid', 
                        						{k_cli_des: rec.get('k_cli_des')}, 600, 400, null, 'iconSpedizione');
                        	    		}
                        			  });
			    				  }
			    				}
			    				
			    				
			    				
			    				
			    				
			    		    var menu = new Ext.menu.Menu({
        				    items: voci_menu
        					}).showAt(event.xy);	
			          			          				                          
			          },
			      			            	        
			        } //listeners
				

		, buttons: [
		     { xtype: 'tbfill' },		
			, {
	         	xtype: 'splitbutton',
	            text: 'Crea nuova anagrafica',
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	
		        	<?php foreach ($s->find_TA_std('MODAN', null, 'N', 'Y') as $ec) { ?> 	
						{
							xtype: 'button',
				            text: <?php echo j(trim($ec['text'])) ?>,
				            //iconCls: 'icon-save-32', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_inserimento = {
				        					afterInsertRecord: function(from_win, record){
				        						
				        						    var values = record;
				        						    values['out_codice'] = record.GCCDCF;
    					                            values['task'] = record.GCDCON;
    					                            values['liv'] = 'liv_2';
    					                            values['iconCls'] = 'icon-folder_grey-16';
    					                            values['id'] = record.GCPROG;
    					                          	var rootNode = my_tree.getRootNode();
													rootNode.insertChild(0, values); 
												    var new_rec = my_tree.store.getNodeById(values['id']);
												    my_tree.getView().select(new_rec);
			  					   	     			my_tree.getView().focusRow(new_rec);
				        						//my_tree.store.load();		        						
				        						
								        		}
						    				};					            
										acs_show_win_std('Immissione nuova anagrafica cliente', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
											{
								  				mode: 'NEW',
								  				modan: <?php echo j(trim($ec['id'])) ?>,
								  				i_e: <?php echo j(trim($ec['TAFG01'])) ?>,
								  				g_paga: <?php echo j(trim($ec['TAFG02'])) ?>
								  			}, 750, 600, my_listeners_inserimento, 'icon-globe-16');
				            
				            }
				        },
				       <?php } ?>  
				            	        	
		        	]
		        }
	        }, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>punto vendita',
	            iconCls: 'icon-button_grey_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				                        selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}
				            							  
						    				      
				            			
				            			var parent_prog = selected_record[0].get('id');		
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();	
				            			
				            		
				            			my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						//my_tree.store.load();		        						
				        						//////from_win.close();
								        		}
						    				};		
				            				            							            			
										acs_show_win_std('Nuovo punto vendita [' + cod_cli + '] ' + d_cli, 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				type : 'P',
								  				parent_prog: parent_prog
								  			}, 750, 600, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            		
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
				            			parent_prog = selected_record[0].get('id');	
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();			
				            			my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						//my_tree.store.load();		        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						////from_win.close();
								        		}
						    				};
				            				            							            			
										acs_show_win_std('Nuovo punto vendita [' + cod_cli + '] ' + d_cli,  
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				type : 'P',
								  				parent_prog: parent_prog
								  			}, 750, 600, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				}, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>destinazione',
	            iconCls: 'icon-button_black_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            			
				            		    selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}
				            			
				            			var parent_prog = selected_record[0].get('id');		
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();		            							            			
									    
									    my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						//my_tree.store.load();		        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						/////from_win.close();
								        		}
						    				};
									
										acs_show_win_std('Nuova destinazione [' + cod_cli + '] ' + d_cli, 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				parent_prog: parent_prog,
								  				type : 'D'
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            			         
						    		    selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
				            			var parent_prog = selected_record[0].get('id');		
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();		            							            			
									    
				            			
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						//my_tree.store.load();		        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						/////from_win.close();
								        		}
						    				};
				            				            							            			
										acs_show_win_std('Nuova destinazione [' + cod_cli + '] ' + d_cli, 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				parent_prog: parent_prog,
								  				type : 'D'
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				}
				
				<?php if($cfg_mod_Gest['gest_azienda_collegata']  != 'Y'){?>
				, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>&nbsp;anagrafica collegata',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}
				            			
				            			parent_prog = selected_record[0].get('id');			
				            			
										my_listeners_add_pven = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						//my_tree.store.load();		        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						from_win.close();
								        		}
						    				};
				            				            							            			
										acs_show_win_std('Nuova azienda collegata', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				parent_prog: parent_prog
								  			}, 650, 550, my_listeners_add_pven, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            		
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
				            			parent_prog = selected_record[0].get('id');	
				            			
				            			my_listeners_add_pven = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						//my_tree.store.load();		        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						from_win.close();
								        		}
						    				};
				            						            							            			
										acs_show_win_std('Nuova azienda collegata', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				parent_prog: parent_prog
								  			}, 650, 550, my_listeners_add_pven, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				}
				<?php }?>
		  
				           
         ]
			        
			        
			        
			, viewConfig: {
			        toggleOnDblClick: false,
			        getRowClass: function(record, index) {
			         ret = record.get('liv');
			           
			           if (record.get('trCls')=='grassetto')
					        return ret + ' grassetto';
			           
			           if (record.get('trCls')=='G')
					        return ret + ' colora_riga_grigio';
			           if (record.get('trCls')=='Y')
					        return ret + ' colora_riga_giallo';	
					   	           	
			           return ret;																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree











function write_new_list_COMM($p, $all_params, $row){
    global $s, $id_ditta_default;

	?>
			{
				xtype: 'grid',
				cls: '',
		        flex: 1,
		       	<?php
		       	if($all_params['mode'] == 'EDIT')
    			 	echo "disabled: false, ";
    			 else 
    			 	echo "disabled: true, "; 
    			?>       
		        loadMask: true,
		        title: 'Condizioni commerciali',
		        features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),
			    tbar: new Ext.Toolbar({
    	            items:[
    	            {xtype : 'displayfield',
    	            itemId : 'f_text',
    	            name : 'f_text'}
    	             ,'->',
    	            {text: '', 
    	             iconCls: 'icon-filter-16',
    	             handler: function(event, toolEl, panel){
    	                    var m_grid = this.up('grid');
    			          	acs_show_win_std('Filtri', 'acs_panel_ins_new_anag_form.php?fn=open_filtri_comm', {
    			          			
        			          	} , 400, 230, {
     		            		afterFilter: function(from_win, form_values){
     		            		     var testo = '';
     		            		     if(!Ext.isEmpty(form_values.val_ini))
     		            		     	var testo = testo + 'Validit&agrave; dal ' + date_from_AS(form_values.val_ini);
     		            			 if(!Ext.isEmpty(form_values.val_fin))
     		            			    var testo = testo + ' al ' + date_from_AS(form_values.val_fin); 
     		            			 if(!Ext.isEmpty(form_values.f_divisione))
     		            			 	var testo = testo + ' Divisione ' + form_values.f_divisione;
     		            			 if(!Ext.isEmpty(form_values.f_codice))
     		            			    var testo = testo + ' Codice ['+ form_values.f_codice + '] ';
     		            			 if(!Ext.isEmpty(form_values.f_descrizione))   
     		            			    var testo = testo + ' - descrizione: ' + form_values.f_descrizione ;
     		            			 if(!Ext.isEmpty(form_values.f_scadute))  
     		            			    var testo =  testo + ' ,escluse scadute da 30 gg';
     		            			 m_grid.down('#f_text').setValue(testo);
     		            			 m_grid.getStore().proxy.extraParams.filtri = form_values;
     		            			 m_grid.getStore().load();
     		            			 from_win.close();
         		            	}
     		            	   }, 'icon-search-16');
    			          
    		           		 
    		           		 }
    		           	 }
    	           
    	         ]            
  			   }),
			    store: {
					xtype: 'store',
					groupField: 'tipo',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_list_cc',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
			                    extraParams: {
			                            filtri : '',
										rec_id: <?php echo j($all_params['rec_id']) ?>,
										GCDEFA: <?php echo j($row['GCDEFA']) ?>,
										cdcf : <?php echo j($all_params['gccdcf']) ?>
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['rrn', 'cdcf', 'dettagli', 'tipo', 'codice', 'descrizione', 'caricata', 'prog', 'ditta', 'condizioni', 'val', 'scaduta']							
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		  {text: 'Codice', width: 50, dataIndex: 'codice',
		    		     renderer: function (value, metaData, rec){	
		    		       if(rec.get('codice').trim() == 'STD'){	    		
		    					metaData.tdCls += ' grassetto';
		    					metaData.tdCls += ' sfondo_grigio';
		    				}	
		    			  
		    			return value;
		    						    				
		    			}
		    		  
		    		  }
		    		, {text: 'Descrizione', width: 250, dataIndex: 'descrizione', 
		    		   renderer: function (value, metaData, rec){	
		    		       if(rec.get('caricata') != '')	    		
		    					metaData.tdCls += ' grassetto';
		    			   if(rec.get('scaduta') == 'Y'){
		    			    	metaData.tdCls += ' grassetto';
		    			    	return '<span style = "color: red;">'+ value +'</span>';
		    			   }
		    			   
		    			     if(rec.get('codice').trim() == 'STD'){	    		
		    					metaData.tdCls += ' grassetto';
		    					metaData.tdCls += ' sfondo_grigio';
		    				}
		    			
		    			return value;
		    						    				
		    			}}
		    		, {text: 'Validit&agrave;', width: 90, dataIndex: 'val'}
		    		, {text: 'Condizioni', flex: 1, dataIndex: 'dettagli'}
				],
				enableSort: true
				
				
	        	, listeners: {
	        	
	        	 activate: function(comp, eOpts) {
	        	  
	        	   var std_caricata = comp.up('window').down('tabpanel').std_caricata;
	        	   console.log(std_caricata);
	        	     <?php if($all_params['mode'] == 'NEW'){?>
				   if(std_caricata == false){
    				    var prog = comp.up('window').down('tabpanel').GCPROG;
    				    comp.up('window').down('tabpanel').std_caricata = true;
    				   
    					my_listeners_add_cc = {
    						afterEditRecord: function(from_win){	
    							//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
    							comp.store.load();		        						
    							from_win.close();
    						}
    		        		
        				};	
    				
    					acs_show_win_std('Modifica condizione commerciale', 
    									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
    					{
    		  				mode: 'EDIT',
    		  				modan: 'ToDo',
    		  				g_paga : <?php echo j($all_params['g_paga']); ?>,
    		  				CCDT: <?php echo j($id_ditta_default)?>,
    		  				CCSEZI: 'BCCO',
    		  				CCCCON: 'STD',
    		  				CCDCON: '',
    		  				CCPROG: prog,
    						GCDEFA:  <?php echo j($all_params['modan']) ?>,
    						cdcf : ''
    		  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');
				   }
				<?php }?>
                 },
	        	 
	        	 celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
	            			var my_grid = iView;
	            
							my_listeners_add_cc = {
	        					afterEditRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_grid.store.load();		        						
	        						var tabPanel = this.down('tabpanel');
                                    var activeTab = tabPanel.getActiveTab();
                                    var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                                    
                                    if(activeTabIndex < tabPanel.items.length - 1)
                                    	tabPanel.setActiveTab(activeTabIndex + 1);
                                    else
	        						    from_win.close();
					        		}
			    				};					  

			    			
			    			if (Ext.isEmpty(rec.get('caricata')) == false) {
			    				//modifica esistente
		            			acs_show_win_std('Modifica condizione commerciale', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
									{
						  				g_paga : <?php echo j($all_params['g_paga']); ?>,
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				CCDT: rec.get('ditta'),
						  				CCPROG: rec.get('prog'),
						  				CCSEZI: rec.get('condizioni'),
						  				CCCCON: rec.get('codice'),
						  				CCDCON: rec.get('descrizione'),
						  				cdcf : rec.get('cdcf'),
						  				GCDEFA: <?php echo j($row['GCDEFA']) ?>,
						  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');	
						  								  				    				
			    			}
			    			else {
			    				//crea nuovo
		            			acs_show_win_std('Crea nuova condizione commerciale', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
									{
						  				mode: 'NEW',
						  				modan: 'ToDo',
						  				g_paga : <?php echo j($all_params['g_paga']); ?>,
						  				CCDT: rec.get('ditta'),
						  				CCPROG: rec.get('prog'),
						  				CCSEZI: 'BCCO',
						  				CCCCON: rec.get('codice'),
						  				CCDCON: rec.get('descrizione'),
						  				cdcf : rec.get('cdcf'),
						  				GCDEFA: <?php echo j($row['GCDEFA']) ?>,
						  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');			    				
			    			}	
			    				
						  	
						  }
					  },
					  
					  			
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
							event.stopEvent();
							var voci_menu = [];
				          
				            voci_menu.push({
        				         		text: 'Duplica condizione',
        				        		iconCls : 'icon-folder_search-16',          		
        				        		handler: function() {
        				        		
        				        				my_listeners_add_cc = {
                	        					afterEditRecord: function(from_win){	
                	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
                	        						grid.store.load();		        						
                	        						var tabPanel = this.down('tabpanel');
                                                    var activeTab = tabPanel.getActiveTab();
                                                    var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                                                    
                                                    if(activeTabIndex < tabPanel.items.length - 1)
                                                    	tabPanel.setActiveTab(activeTabIndex + 1);
                                                    else
                	        						    from_win.close();
                					        		}
                			    				};	
        				        		
        				        				acs_show_win_std('Crea nuova condizione commerciale', 
            									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
            									{
            						  				mode: 'DUP',
            						  				modan: 'ToDo',
            						  				g_paga : <?php echo j($all_params['g_paga']); ?>,
            						  				CCDT: rec.get('ditta'),
            						  				CCPROG: rec.get('prog'),
            						  				CCSEZI: 'BCCO',
            						  				CCCCON: rec.get('codice'),
            						  				CCDCON: rec.get('descrizione'),
            						  				cdcf : rec.get('cdcf'),
            						  				GCDEFA: <?php echo j($row['GCDEFA']) ?>,
            						  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');	
        				        		}
        				    		});
				
				
				      voci_menu.push({
			         		text: 'Cancella condizione',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		
			        		 Ext.Msg.confirm('Richiesta conferma', 'Confermi cancellazione definitiva della condizione selezionata?' , function(btn, text){
            	   		       if (btn == 'yes'){
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
								        method     : 'POST',
					        			jsonData: {
					        				row : rec.data,
					        				
										},							        
								        success : function(result, request){
								        	grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
								    
								   }
            	   		          });		    
							  
				                }
			    		});
				  
				            var menu = new Ext.menu.Menu({
    				            items: voci_menu
					        }).showAt(event.xy);	
								
							
					
					}
				}
				

			, buttons: [
					{
						xtype: 'button',
			            text: 'Nuova condizione PREDEFINITA', 
			            iconCls: 'icon-sub_blue_add-32', scale: 'large',
			            handler: function() {
			            
	            			 var my_grid = this.up('grid');
	            
	            			 //grid selezione condizione di default
	            			 acs_show_win_std('Condizioni predefinite', 
							 'acs_panel_ins_new_anag_form.php?fn=open_filtra_CC', 
							 {g_paga : <?php echo j($all_params['g_paga']); ?>,
				  				CCPROG: my_grid.up('tabpanel').GCPROG,
				  				grid_id : my_grid.getId(),
				  				GCDEFA: <?php echo j($row['GCDEFA']) ?>
				  			 }, 420, 450, {}, 'icon-globe-16');
			            
			            }
			        }, {
						xtype: 'button',
			            text: 'Nuova condizione GENERICA', 
			            iconCls: 'icon-sub_grey_add-32', scale: 'large',
			            handler: function() {
			            
	            			 var my_grid = this.up('grid');
	            			 
	            			 var my_listeners_add_cc = {
	            			 	afterEditRecord: function(from_win){
	            			 		from_win.destroy();
	            			 		my_grid.store.load();
	            			 	}
	            			 };
	            
				  			 //crea nuovo partendo da 0
		            			acs_show_win_std('Crea nuova condizione commerciale', 
									'acs_panel_ins_new_anag_form.php?fn=open_form_COMM', 
									{
						  				mode: 'CREATE',
						  				modan: 'ToDo',						  				
						  				CCPROG: my_grid.up('tabpanel').GCPROG
						  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');	
			            
			            }
			        }
				          
				    ]
											    
				    		
	 	}
<?php
} //write_new_list_COMM







function write_new_list_FIDO($p, $all_params, $row){
	global $s;
	?>
			{
				xtype: 'grid',
				itemId: 'listFido',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        title: 'Dati amministrativi',
		        
				<?php
				 if ($all_params['mode'] == 'EDIT')
				 	echo "disabled: false, ";
				 else 
				 	echo "disabled: true, "; 
				?>           
		        
		        
		        
			store: {
					xtype: 'store',
					groupField: 'cod_web',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_list_fido',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
			                    extraParams: {
										rec_id: <?php echo j($all_params['rec_id']) ?>,
										GCDEFA: <?php echo j($row['GCDEFA']) ?>
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['Q_REC_ID', 'TADT', 'TAKEY1', 'TAKEY2', 'TADESC', 'TATAID', 'CCDT', 'CCCCON', 'CCDCON', 'CCPROG', 'CCSEZI', 'CCIMFD', 'CCDTVI', 'CCDTVF']							
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		{text: 'Codice', width: 90, dataIndex: 'TAKEY1'}
		    		, {text: 'Descrizione', flex: 1, dataIndex: 'TADESC'}
		    		, {text: 'Caricata', flex: 1, dataIndex: 'CCCCON', renderer: function (value, metaData, rec){		    		
		    				if (Ext.isEmpty(value) == false){
		    					return 'X';
		    				}		    				
		    			}}
		    		, {text: 'Importo fido', flex: 1, dataIndex: 'CCIMFD', renderer : floatRenderer2}
		    		, {text: 'Validit&agrave; inziale', flex: 1, dataIndex: 'CCDTVI', renderer : date_from_AS}
		    		, {text: 'Validit&agrave; finale', flex: 1, dataIndex: 'CCDTVF', renderer : date_from_AS}
				],
				enableSort: true
				
				
	        	, listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
	            			my_grid = iView;
	            
							my_listeners_add_cc = {
	        					afterEditRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_grid.store.load();		        						
	        						from_win.close();
					        		}
			    				};					  

			    			
			    			if (Ext.isEmpty(rec.get('CCCCON')) == false) {
			    				//modifica esistente
		            			acs_show_win_std('Modifica condizione amministrativa', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_FIDO', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				CCDT: rec.get('CCDT'),
						  				CCPROG: rec.get('CCPROG'),
						  				CCSEZI: rec.get('CCSEZI'),
						  				CCCCON: rec.get('CCCCON')
						  			}, 600, 400, my_listeners_add_cc, 'icon-globe-16');						  				    				
			    			}
			    			else {
			    				//crea nuovo
		            			acs_show_win_std('Crea nuova condizione amministrativa', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_FIDO', 
									{
						  				mode: 'NEW',
						  				modan: 'ToDo',
						  				CCDT: rec.get('TADT'),
						  				CCPROG: rec.get('Q_REC_ID'),
						  				CCSEZI: 'BFID',
						  				CCCCON: rec.get('TAKEY1'),
						  				CCDCON: rec.get('TADESC'),
						  			}, 600, 400, my_listeners_add_cc, 'icon-globe-16');			    				
			    			}	
			    				
						  	
						  }
					  }
				}
				

			, buttons: [
			
			
			          {
							xtype: 'button',
				            text: 'Chiudi', 
				            iconCls: 'icon-outbox-32', scale: 'large',
				            handler: function() {				            
				            			my_win = this.up('window');
				            			my_win.close();
				            }
				        }
				          
				    ]
											    
				    		
	 	}
<?php
} //write_new_list_FIDO

function write_main_form($p){
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            flex: 0.3,
            
            defaults:{ anchor: '-10' , xtype: 'textfield', labelAlign: 'top'},
            
            items: [
            	{name: 'GCDCON',	fieldLabel: 'Denominazione'},
            	{name: 'GCWW', 		fieldLabel: 'Sito web'},
            	{name: 'GCMAIL', 	fieldLabel: 'Email azienda'},
            	{name: 'GCMAI1', 	fieldLabel: 'Email - Conf. ordine'},
            	{name: 'GCMAI2', 	fieldLabel: 'Email - Fatt. vendita'},
            	{name: 'GCMAI3', 	fieldLabel: 'Email - Piani sped.'},
            	{name: 'GCMAI4', 	fieldLabel: 'Email - Resi'},
            	{name: 'GCTEL', 	fieldLabel: 'Telefono'},
            	{name: 'GCTEL2', 	fieldLabel: 'Telefono 2'},
            	{name: 'GCFAX', 	fieldLabel: 'Fax'},
            ],
			buttons: [],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_main_form



function write_new_form_CRM($p = array(), $request = array(), $r = array()){
    global $conn, $cfg_mod_Gest;
    global $s, $main_module;
    
    if ($request['mode'] == 'EDIT'){
        $r = get_gcrow_by_prog($request['rec_id']);        
    } else {
        //NUOVA
        $r = array(
        //'GCPROG' => $request['GCPROG']
        );
    }
   ?> 
    {
        xtype: 'form',
        itemId: 'form_CRM',
        bodyStyle: 'padding: 10px',
        bodyPadding: '0 0 0 3',
        frame: true,
        title: 'Classificazioni', //ex CRM
        autoScroll: true,
   
			<?php
			 if ($request['mode'] == 'EDIT')
			 	echo "disabled: false, ";
			 else 
			 	echo "disabled: true, "; 
			?>   
   
        items: [
        {xtype: 'hiddenfield', name: 'GCPROG', value: <?php echo j($r['GCPROG']); ?>},
        {
      			xtype: 'fieldset',
      			allowBlank: true,
      			layout: 'hbox',
      			items:
      			[
        		
        		
        		 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
    						 { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						margin : '0 20 0 0',
    						layout: { 	type: 'vbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
    						
    						<?php if(trim($r['GCTPAN']) != 'DES'){?>
    						<?php if($cfg_mod_Gest['tipo_indi_cliente'] == 'Y'){?>
    						  <?php write_combo_std('GCTPIN', 'Tipo indirizzo', $r['GCTPIN'], acs_ar_to_select_json($main_module->find_TA_std('INDI', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('labelWidth' => 90)) ?>, 
    						<?php }?>
    						
    						<?php if($cfg_mod_Gest['gestione_avanzata_ciclo_vita'] != 'Y'){?>
    						  <?php write_combo_std('GCCIVI', 'Ciclo di vita', $r['GCCIVI'], acs_ar_to_select_json($main_module->find_TA_std('CHCVF', null, 'N', 'N', null, null, null, 'N', 'Y'), '', 'R'), array('labelWidth' => 90) ) ?>,					
    						<?php }?>
    						  <?php write_combo_std('GCORIG', 'Origine', $r['GCORIG'], acs_ar_to_select_json(find_TA_sys('BORI', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'),  array('labelWidth' => 90) ) ?>	
    						
    						<?php }?>
    			     		
    						, <?php write_combo_std('GCCRM1', 'Class. CRM1', $r['GCCRM1'], acs_ar_to_select_json(find_TA_sys('BCR1', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 90)) ?>	
    						
							<?php if($cfg_mod_Gest['crm2_ob'] == 'Y')
            					$allowBlank = 'false';
            				else
            				    $allowBlank = 'true';
            				?>
    						
    						, <?php write_combo_std('GCCRM2', 'Class. CRM2', $r['GCCRM2'], acs_ar_to_select_json(find_TA_sys('BCR2', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 90, "allowBlank" => $allowBlank)) ?>	
    						
    						<?php if(trim($r['GCTPIN']) != 'P'){?>
    						, <?php write_combo_std('GCSEL1', 'Classific.1', $r['GCSEL1'], acs_ar_to_select_json(find_TA_sys('CUS1', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 90)) ?>	
    						, <?php write_combo_std('GCSEL2', 'Classific.2', $r['GCSEL2'], acs_ar_to_select_json(find_TA_sys('CUS2', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 90)) ?>	
    					    <?php }else{?>
                              , <?php write_combo_std('GCFG03', 'Visualizzazione Web', $r['GCFG03'], acs_ar_to_select_json($main_module->find_TA_std('CCT06', null, 'N', 'N', null, null, null, 'N', 'Y'), '', 'R'), array('labelWidth' => 90)) ?>	
    					    <?php }?>
    						]},
    						
    						 { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						layout: { 	type: 'vbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
    							
    							<?php if($cfg_mod_Gest["anag_cli_hide_CRM"] == 'Y' ){?>
    							
    							     <?php if(trim($r['GCTPAN']) == 'DES' || $request['type'] == 'P'){?>
    							    {
                      				xtype: 'checkbox',
                      				boxLabel: 'Ordini Materiali Promozionali',
                                    name: 'GCGMTP',
                                    width : 200,
                      				value: <?php echo j(trim(acs_u8e($r['GCGMTP']))); ?>,
                                    inputValue: 'P',
                                    listeners : {
                                    	'render': function(field){
                    						<?php if(trim(acs_u8e($r['GCGMTP'])) == 'P'){?>
                    							field.setValue(true);
                    						
                    						<?php }?>
                    						   
                					  }
                                
                                    }	
                      			   },	{
                  				xtype: 'checkbox',
                  				name: 'GCESPO',
                  				value: <?php echo j(trim(acs_u8e($r['GCESPO']))); ?>,
                  			    boxLabel: 'Espositori',
                                inputValue: 'Y'	,
                                 listeners : {
                                	'render': function(field){
                						<?php if(trim(acs_u8e($r['GCESPO'])) == 'Y'){?>
                							field.setValue(true);
                						
                						<?php }?>
                						   
                					  }
    
                                }
                  			}
    							     
    							     
    							     <?php }?>
    							
    							
    							<?php }else{?>
    							 { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						layout: { 	type: 'hbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
        						{
                  				xtype: 'checkbox',
                  				boxLabel: 'Ordini Materiali Promozionali',
                                name: 'GCGMTP',
                                width : 200,
                  				value: <?php echo j(trim(acs_u8e($r['GCGMTP']))); ?>,
                                inputValue: 'P',
                                listeners : {
                                	'render': function(field){
                						<?php if(trim(acs_u8e($r['GCGMTP'])) == 'P'){?>
                							field.setValue(true);
                						
                						<?php }?>
                						   
            					  }
                            
                                }	
                  			   },	{
              				xtype: 'checkbox',
              				name: 'GCLIST',
              				value: <?php echo j(trim(acs_u8e($r['GCLIST']))); ?>,
              				boxLabel: 'Listini',
                            inputValue: 'Y',
                            flex : 1,
                             listeners : {
                            	'render': function(field){
            						<?php if(trim(acs_u8e($r['GCLIST'])) == 'Y'){?>
            							field.setValue(true);
            						
            						<?php }?>
            						   
            					  }
                            
                                }		
                  			}
        						
    						]}, { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						layout: { 	type: 'hbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
        							{
                  				xtype: 'checkbox',
                  				name: 'GCESPO',
                  				value: <?php echo j(trim(acs_u8e($r['GCESPO']))); ?>,
                  			    boxLabel: 'Espositori',
                  			    width : 200,
                                inputValue: 'Y'	,
                                 listeners : {
                                	'render': function(field){
                						<?php if(trim(acs_u8e($r['GCESPO'])) == 'Y'){?>
                							field.setValue(true);
                						
                						<?php }?>
                						   
                					  }
    
                                }
                  			},{
                  				xtype: 'checkbox',
                  				name: 'GCCATA',
                  				value: <?php echo j(trim(acs_u8e($r['GCCATA']))); ?>,
                  				boxLabel: 'Cataloghi',
                                inputValue: 'Y',
                                listeners : {
                                	'render': function(field){
                						<?php if(trim(acs_u8e($r['GCCATA'])) == 'Y'){?>
            							field.setValue(true);
            						
                						<?php }?>
                						   
                    					  }
                                    
                                    }		
                      			}
        					
    						]}
    							
    							<?php }?>
    								
    						
    				     , 
    				      { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						layout: { 	type: 'hbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
    						 {
                  				xtype: 'checkbox',
                  				name: 'GCSWGB',
                  				value: <?php echo j(trim(acs_u8e($r['GCSWGB']))); ?>,
                  				boxLabel: 'Budget',
                  				margin : '0 0 5 0',
                                inputValue: 'Y',
                                 listeners : {
                                	'render': function(field){
                						<?php if(trim(acs_u8e($r['GCSWGB'])) == 'Y'){?>
                							field.setValue(true);
                						
                						<?php }?>
                						   
                					  }
                                
                                }		
                  			}
                  			
                  			, <?php write_combo_std('GCMERC', 'Fascia mercato', $r['GCMERC'], acs_ar_to_select_json($main_module->find_TA_std('FMERC', null, 'N', 'N', null, null, null, 'N', 'Y'), '', 'R'), array('labelWidth' => 110, 'labelAlign' => 'right')) ?>				          
    						
    						]}
    				     
    				     
    				    
              			
              			, <?php write_combo_std('GCPREM', 'Premi', $r['GCPREM'], acs_ar_to_select_json($main_module->find_TA_std('PREM', null, 'N', 'N', null, null, null, 'N', 'Y'), '', 'R', 'N', 'Y'),  array('labelWidth' => 75) ) ?>	   
              			
              			
              			<?php if(trim($r['GCTPAN']) == 'DES'){?>
              			, <?php write_combo_std('GCREFE', 'Referente', $r['GCREFE'], acs_ar_to_select_json(find_TA_sys('BREF', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 75)) ?>				          
    					<?php }?>
    					
    					<?php if(trim($r['GCTPIN']) != 'P'){
    					    if($cfg_mod_Gest["tab_GCSEL3"] == 'ANC01')
    					        $tab = $main_module->find_TA_std($cfg_mod_Gest["tab_GCSEL3"], null, 'N', 'N', null, null, null, 'N', 'Y');
    					    else 
    					        $tab = find_TA_sys($cfg_mod_Gest["tab_GCSEL3"], null, null, null, null, null, 0, '', 'Y', 'Y');
    					    ?>
    					, <?php write_combo_std('GCSEL3', 'Classific.3', $r['GCSEL3'], acs_ar_to_select_json($tab, '', 'R'), array('labelWidth' => 75)) ?>	
    				    , <?php write_combo_std('GCSEL4', 'Classific.4', $r['GCSEL4'], acs_ar_to_select_json(find_TA_sys('CUS4', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 75)) ?>			
    					<?php }?>
    					]}
						
						   
						]}
        
        		]
        },
        
        
        	
		<?php if( $cfg_mod_Gest["anag_cli_hide_CRM"] != 'Y' || 
		    ($cfg_mod_Gest["anag_cli_hide_CRM"] == 'Y' && trim($r['GCTPAN']) != 'CLI')){?>
    							
        
            {
      			xtype: 'fieldset',
      			allowBlank: true,
      			layout: 'vbox',
      			items:
      			[
      			{ 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
                {
              			xtype: 'numberfield',
              			fieldLabel: 'Nr. Box',
              			name: 'GCNBOX',
      					value: <?php echo j(trim(acs_u8e($r['GCNBOX']))); ?>,
              			labelWidth: 90,
              			allowBlank: true,
              			hideTrigger: true,
              			minValue: 0,
            			maxValue: 9999,
            			step: 1,
              			//flex: 1,
              	},
              	{
              			xtype: 'numberfield',
              			fieldLabel: 'Nr. Venditori',
              			name: 'GCNVEN',
      					value: <?php echo j(trim(acs_u8e($r['GCNVEN']))); ?>,
              			labelAlign: 'right',
              			labelWidth: 90,
              			allowBlank: true,
              			hideTrigger: true,
              			minValue: 0,
            			maxValue: 9999,
            			step: 1,
              			width : 215
              	},
              	{
              			xtype: 'numberfield',
              			fieldLabel: 'Nr. Punti Vendita',
              			name: 'GCNPVE',
      					value: <?php echo j(trim(acs_u8e($r['GCNPVE']))); ?>,
              			labelAlign: 'right',
              			labelWidth: 110,
              			allowBlank: true,
              			hideTrigger: true,
              			minValue: 0,
            			maxValue: 9999,
            		    step: 1,
              			width : 225
              	}
              	
              	]},
              	 { 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
						, <?php write_combo_std('GCCAZ1', 'Concorrente 1', $r['GCCAZ1'], acs_ar_to_select_json(find_TA_sys('BCES', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 90, 'flex_width' => 'width : 330')) ?>	
    				    , <?php write_combo_std('GCCAZ3', 'Concorrente 3', $r['GCCAZ3'], acs_ar_to_select_json(find_TA_sys('BCES', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right', 'flex_width' => 'width : 350')) ?>			
					
					
					]},{ 
					xtype: 'fieldcontainer',
					flex:1,
					layout: { 	type: 'hbox',
							    pack: 'start',
							    align: 'stretch'},						
					items: [
						, <?php write_combo_std('GCCAZ2', 'Concorrente 2', $r['GCCAZ2'], acs_ar_to_select_json(find_TA_sys('BCES', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelWidth' => 90, 'flex_width' => 'width : 330')) ?>	
    				    , <?php write_combo_std('GCCAZ4', 'Concorrente 4', $r['GCCAZ4'], acs_ar_to_select_json(find_TA_sys('BCES', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right', 'flex_width' => 'width : 350')) ?>			
					
					
					]}
              	
              	
              	]
        }, {
      			xtype: 'fieldset',
      			flex: 1,
      			autoScroll: true,
      			title: 'Insegna/Indicazioni logistiche',
      			layout: 'anchor',
      			items:
      			[
      			
      			  <?php 
      			  
          			  $gc_row = get_gcrow_by_prog($r['GCPRAB']);
          			  if(trim($r['GCTPAN']) == 'DES')
          			      $rife = trim($gc_row['GCCDCF'])."_".trim($r['GCCDCF']);
          			      else
          			          $rife = trim($r['GCCDCF']);
    		          $commento_txt = $main_module->get_commento_cliente($rife, 'INS', 'CD');
      			  
      			 
      			   for($i=0; $i<= 3;$i++){ ?>
                	
                	{
						name: 'INS_text_<?php echo $i ?>',
						xtype: 'textfield',
						fieldLabel: '',
					    anchor: '-15',
					    maxLength: 80,	
					  	value: <?php echo j(trim($commento_txt['testi'][$i])); ?>,							
					},	
					
					<?php } ?>	
      			
      			]
      	},
      	{
      			xtype: 'fieldset',
      			flex: 1,
      			title: 'Apertura - Orari/Segnalazioni',
      			layout: 'anchor',
      			items:
      			[
      			
      			   <?php 
      			   
      			   
    		           $gc_row = get_gcrow_by_prog($r['GCPRAB']);
    		           if(trim($r['GCTPAN']) == 'DES')
    		               $rife = trim($gc_row['GCCDCF'])."_".trim($r['GCCDCF']);
    	               else
    	                   $rife = trim($r['GCCDCF']);
      			       
    		           $commento_txt = $main_module->get_commento_cliente($rife, 'ACH', 'CD');
      			   
      			   
      			   for($i=0; $i<= 3;$i++){ ?>
                	
                	{
						name: 'ACH_text_<?php echo $i ?>',
						xtype: 'textfield',
						fieldLabel: '',
					    anchor: '-15',
					    maxLength: 80,	
					    flex: 1,				    
					    value: <?php echo j(trim($commento_txt['testi'][$i])); ?>,							
					},	
					
					<?php } ?>
      		
      			]
        }
        
        <?php }?>
       
        ],
		buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', 
	            scale: 'large',
	            handler: function() {
	                var form = this.up('form').getForm();
	            	var loc_win = this.up('window');	            		            	
					var tabPanel = loc_win.down('tabpanel');
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_crm',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	cliente : <?php echo j($r['GCCDCF']); ?>,
						        	open_request: <?php echo acs_je($request) ?>,
						        	GCPRAB : <?php echo j($r['GCPRAB']); ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            var record = jsonData.record;
						            
			                        tabpanel_load_record_in_form(tabPanel, record);	
			                        tabPanel.acs_actions.exe_afterSave(loc_win);
						            
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						            						
									 														            
						            
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],    
        
        }
<?php     


}

function write_new_form_ConfigLogis($p = array(), $request = array(), $r = array()){
    global $conn, $cfg_mod_Gest;
    global $s, $main_module;
    
    if ($request['mode'] == 'EDIT'){
        //recupero la riga da modificare
        $sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} WHERE GCPROG=?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($request['rec_id']));
        
        $ret = array();
        $r = db2_fetch_assoc($stmt);
        
        
    } else {
        //NUOVA
        $r = array(
        //'GCPROG' => $request['GCPROG']
        );
    }
    ?>
    {
        xtype: 'form',
        itemId: 'form_CL',
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 5 5',
        frame: true,
        title: 'Configurazione/logistica',
        autoScroll: true,
   
			<?php
			 if ($request['mode'] == 'EDIT')
			 	echo "disabled: false, ";
			 else 
			 	echo "disabled: true, "; 
			?>   
   
        items: [
        {xtype: 'hiddenfield', name: 'GCPROG', value: <?php echo j($r['GCPROG']); ?>},
         
         	     { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',						
						items: [
						  <?php write_combo_std('GCZONA', 'Zona', $r['GCZONA'], acs_ar_to_select_json(find_TA_sys('CUZO', null, null, null, null, null, 0, '', 'Y', 'Y'), '')) ?>					
    					, <?php write_combo_std('GCCITI', 'Itinerario', $r['GCCITI'], acs_ar_to_select_json(find_TA_sys('BITI', null, null, null, null, null, 0, '', 'Y', 'Y'), ''),  array('labelAlign' => 'right', "allowBlank" => 'false') ) ?>	
					]},
					
					<?php if($cfg_mod_Gest["destinazione_tipologia"] == 'Y'){ ?>
					   { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',						
						items: [
						  <?php write_combo_std('GCFLG1', 'Dest. x tipologia', $r['GCFLG1'], acs_ar_to_select_json($main_module->find_TA_std('DTIP', null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y'), array('flex_width' => "width: 230")) ?>					
    					, <?php write_combo_std('GCFLG2', '', $r['GCFLG2'], acs_ar_to_select_json($main_module->find_TA_std('DTIP', null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y'),  array('flex_width' => "width: 120", "margin" => "margin : '0 0 0 3',") ) ?>	
					]},
					<?php }?>
					
           			{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',						
						items: [
					    
						  <?php write_combo_std('GCAG1', 'Agente 1', $r['GCAG1'], acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), '')) ?>					
    					, <?php write_numberfield_std('GCPA1', '% provv.', $r['GCPA1'], 11, array('labelAlign' => 'right', 'flex_width' => 'flex:1') ) ?>	
					]},
					
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',						
						items: [
						  <?php write_combo_std('GCAG2', 'Agente 2', $r['GCAG2'], acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, " AND SUBSTRING(TAREST, 196, 1) IN ('', 'A', 'P')", 'Y', 'Y'), '')) ?>					
    					, <?php write_numberfield_std('GCPA2', '% provv.', $r['GCPA2'], 11, array('labelAlign' => 'right') ) ?>	
					]},
       
					{
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							margin : '10 0 0 0',
							defaults:{labelWidth: 30, flex: 1, labelAlign: 'top'},
							items: [
								<?php write_combo_std('GCGGS1', 'Lun', $r['GCGGS1'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS2', 'Mar', $r['GCGGS2'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>													 
							  , <?php write_combo_std('GCGGS3', 'Mer', $r['GCGGS3'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS4', 'Gio', $r['GCGGS4'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS5', 'Ven', $r['GCGGS5'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS6', 'Sab', $r['GCGGS6'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>

							]
						  }
    						
      	
       
        ],
		buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', 
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');
	            	var tabPanel = loc_win.down('tabpanel');
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_config_logis',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	cliente : <?php echo j($r['GCCDCF']); ?>,
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            var record = jsonData.record;
						            
			                        tabpanel_load_record_in_form(tabPanel, record);	
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						            tabPanel.acs_actions.exe_afterSave(loc_win);
     
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],    
        
        }
<?php     


}



function write_new_form_ITA($p = array(), $request = array(), $r = array()){
    global $s, $main_module, $cfg_mod_Gest, $id_ditta_default, $conn;
	
    if($p['tipo_anagrafica'] == 'DES'){
        
        if($request['mode'] == 'NEW'){
            $gc_row = get_gcrow_by_prog($request['parent_prog']);
           
            $sql = "SELECT COUNT(*) AS R_STD
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
            ON TA.TADT = GC.GCDT AND TAID = 'VUDE' AND TANR = GC.GCCDCF AND TACOR1 = '{$gc_row['GCCDCF']}'
            WHERE GCDT = '{$id_ditta_default}' AND GCPRAB = '{$request['parent_prog']}' AND GCTPDS = 'S'
            /*AND GCPROG <> '{$gc_row['GCPROG']}'*/ AND TATP <> 'S'";
            
        }else{
            $sql = "SELECT COUNT(*) AS R_STD
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
            ON TA.TADT = GC.GCDT AND TAID = 'VUDE' AND TANR = GC.GCCDCF AND TACOR1 = '{$r['cod_cli']}'
            WHERE GCDT = '{$id_ditta_default}' AND GCPRAB = '{$r['prog_cli']}' AND GCTPDS = 'S'
            AND GCPROG <> '{$r['GCPROG']}' AND TATP <> 'S'";
            
        }
       
    	$stmt = db2_prepare($conn, $sql);
    	echo db2_stmt_errormsg();
    	$result = db2_execute($stmt);
    	$row = db2_fetch_assoc($stmt);
        	
       }
    	
    
	
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '0 0 0 3',
            frame: true,
            title: 'Intestazione',
            autoScroll: true,
            
            flex: 0.7,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'mode',
	                	value: <?php echo j($request['mode']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'type',
	                	value: <?php echo j($request['type']) ?>
                	},{
	                	xtype: 'hidden',
	                	name: 'tipo_anagrafica',
	                	value: <?php echo j($p['tipo_anagrafica']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'tipologia',
	                	value: <?php echo j($cfg_mod_Gest['cod_naz_italia']); ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'GCPROG',
	                	value: <?php echo j(trim($r['GCPROG'])); ?>	
                	}
                	
                	
<?php if (in_array($p['tipo_anagrafica'], array('DES')) && (trim($r['GCTPIN']) == 'D' || trim($r['GCTPIN']) == '') && $request['type'] != 'P') { ?>
					, <?php write_combo_std('GCTPDS', 'Tipo destinazione', $r['GCTPDS'], acs_ar_to_select_json(ar_standard_alternativa(), '', 'R'), array() ) ?>
<?php } ?>       
                	, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
                	
                	
		                	{
		                		xtype: 'textfield', labelWidth: 130,
								name: 'GCDCON',
								flex: 1,
								fieldLabel: 'Denominazione',
							    maxLength: 90,
							    value: <?php echo j(trim(acs_u8e($r['GCDCON']))); ?>,
							    allowBlank: false								
							}, 
							
							<?php if($p['tipo_anagrafica'] != 'DES'){?>
							 {
										  
										xtype: 'displayfield',
										editable: false,
										fieldLabel: '',
										padding: '0 0 0 10',
									    value: <?php echo j("<img src=" . img_path("icone/48x48/user.png") . " width=16>"); ?>,
							}, {
		                            xtype: 'checkbox'
		                          , name: 'GCTFIS' 
		                          , boxLabel: 'Privato'
		                          , checked: <?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')) echo 'true'; else echo 'false'; ?>
		                          , inputValue: 'P'
		                       	  , listeners: {
								    change: function(checkbox, newValue, oldValue, eOpts) {
								    	var m_form = this.up('form');
								    	var fs_cognome_nome = m_form.down('#fs_cognome_nome');
								    	
			                    	    if (newValue == false){
			                    	    	fs_cognome_nome.hide();
			                    	    	m_form.getForm().findField('GCCOGN').disable();
			                    	    	m_form.getForm().findField('GCNOME').disable();
			                    	    	}
			                    	    else {
			                    	    	fs_cognome_nome.show();
			                    	    	m_form.getForm().findField('GCCOGN').enable();
			                    	    	m_form.getForm().findField('GCNOME').enable();
			                    	    }	
			                    	  
								    }
								}		                          
		                    }	
		                    <?php }?>	    							
							
						]
					}, 	
                	
                	
                	{
						xtype: 'fieldcontainer',
						itemId: 'fs_cognome_nome',
						
						<?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')){ ?>						 
							hidden: false,
						<?php } else { ?>
							hidden: true,
						<?php } ?>
						
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [					
							 {
								name: 'GCCOGN',
								fieldLabel: 'Cognome', labelWidth: 130,
							    maxLength: 40, allowBlank: false,
							    value: <?php echo j(trim(acs_u8e($r['GCCOGN']))); ?>,
								<?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')){ ?>						 
									disabled: false
								<?php } else { ?>
									disabled: true
								<?php } ?>
							},{
								name: 'GCNOME',
								fieldLabel: 'Nome',labelAlign: 'right',
								maxLength: 40, allowBlank: false,
								value: <?php echo j(trim(acs_u8e($r['GCNOME']))); ?>,
								<?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')){ ?>						 
									disabled: false
								<?php } else { ?>
									disabled: true
								<?php } ?>															
							}
						]
					},
					 {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							{
									name: 'GCINDI',
									fieldLabel: 'Indirizzo',
									maxLength: 60,
									value: <?php echo j(trim(acs_u8e($r['GCINDI']))); ?>							
							}, {
								name: 'GCCAP', labelWidth: 40,
								fieldLabel: 'Cap', labelAlign: 'right',
							    maxLength: 7,
							    flex : 0.2,
							    value: <?php echo j(trim(acs_u8e($r['GCCAP']))); ?>
							}
						]
					}					
					
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield'},
						items: [
							  
								{
									xtype: 'fieldcontainer', flex: 3,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'GCLOCA', flex: 0.6,
											fieldLabel: 'Localit&agrave;', 
											labelWidth: 130,
											maxLength: 60,
										    value: <?php echo j(trim(acs_u8e($r['GCLOCA']))); ?>,
										   	
										<?php if(($request['mode'] == 'EDIT' &&  (trim($r['GCNAZI']) == 'ITA' || trim($r['GCNAZI']) == 'IT' || trim($r['GCNAZI']) == ''))
										 || ($request['mode'] == 'NEW' && (trim($request['i_e']) == '' || $request['i_e'] == 'I'))){?>
										    	readOnly: true
										    <?php }?>
										    
										    
										    
										},										
										
										
										<?php 
										
										
										if(($request['mode'] == 'EDIT' &&  (trim($r['GCNAZI']) == 'ITA' || trim($r['GCNAZI']) == 'IT' || trim($r['GCNAZI']) == ''))
										 || ($request['mode'] == 'NEW' && (trim($request['i_e']) == '' || $request['i_e'] == 'I'))){?>
										{										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 5',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
    														    var my_listeners_search_loc = {
    									        					afterSelected: function(from_win, record_selected){
    									        					       console.log(record_selected);
    										        						m_form.findField('GCLOCA').setValue(record_selected.get('descr'));
    										        						m_form.findField('GCPROV').setValue(record_selected.get('COD_PRO'));
    										        						m_form.findField('v_regione').setValue(record_selected.get('DES_REG'));
    										        						m_form.findField('GCNAZI').setValue(record_selected.get('COD_NAZ'));
    										        						m_form.findField('GCZONA').setValue(record_selected.get('ZONA'));
    										        						from_win.close();
    														        		}										    
    														    };
    															
    															
    															acs_show_win_std('Seleziona localit&agrave;', 
    																'search_loc.php?fn=open_search', 
    																{
    																	gcnazi: m_form.findField('GCNAZI').getValue()
    																}, 450, 150, my_listeners_search_loc, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
										<?php }?>
										
										
										
										
								  ]
								}							  
							  	
								
								
								
						        
						        , {
									name: 'GCPROV', labelWidth: 45,
									width : 80,
									fieldLabel: 'Prov.', labelAlign: 'right',
								    maxLength: 2, readOnly: true,
								    value: <?php echo j(trim(acs_u8e($r['GCPROV']))); ?>,
							    	readOnly: true	
								}, {
									name: 'GCKMIT', width: 100, labelWidth: 60,
									fieldLabel: 'Km', labelAlign: 'right',
									margin : '0 0 0 2',
								    maxLength: 5, readOnly: false,
								    value: <?php echo j(trim(acs_u8e($r['GCKMIT']))); ?>
								}									    	
							
						]
					 }
					
					
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [					
							
							{
								name: 'GCLOC2',
								fieldLabel: '&nbsp;',	
								labelSeparator : '',	
								maxLength : 60,			    
							    value: <?php echo j(trim(acs_u8e($r['GCLOC2']))); ?>,
							    						
							}
							,
							 <?php 
							 
							     //Nazione
							 
							     //se sono in NEW (e Italia) il combo ha solo la nazione (Italia)
							     if ($request['mode'] =='NEW')
							         $filtra_nazione = $r['GCNAZI'];
							     else
							         $filtra_nazione = null;

							     write_combo_std('GCNAZI', 'Nazione', $r['GCNAZI'], acs_ar_to_select_json(find_TA_sys('BNAZ', $filtra_nazione, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right'))
							 
							 ?>
						]
					}
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
						
						<?php write_combo_std('GCZONA', 'Zona', trim($r['GCZONA']), acs_ar_to_select_json(find_TA_sys('CUZO', null, null, null, null, null, 0, '', 'Y', 'Y'), '')) ?>					
						, {
								name: 'v_regione',
								fieldLabel: 'Regione',	
								labelAlign : 'right',						    
							    value: <?php echo j(trim(acs_u8e($r['DES_REG']))); ?>,
							    readOnly: true							
							}
						]}
					
					
			<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [			    	
							{
								name: 'GCPIVA',
								fieldLabel: 'Partita IVA',
							    maxLength: 11,
							    value: <?php echo j(trim(acs_u8e($r['GCPIVA']))); ?>							
							}, {
								name: 'GCCDFI',
								fieldLabel: 'Codice fiscale', labelAlign: 'right',
								<?php if(trim($r['GCNAZI']) == 'ITA' || trim($r['GCNAZI']) == 'IT' || trim($r['GCNAZI']) == ''){?>
								 	maxLength: 18, //sempre lungo 18, poi in "Salva" al limite dico che deve essere max 16
								<?php }else{?>
									maxLength: 18,
								<?php }?>
							    value: <?php echo j(trim(acs_u8e($r['GCCDFI']))); ?>							
							} 
						 ]
						}
<?php } ?>
					
					
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							{
								name: 'GCTEL',
								fieldLabel: 'Telefono',
								maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCTEL']))); ?>							
							}, 
							<?php if($p['tipo_anagrafica'] != 'DES'){?>
							{
								name: 'GCTEL2',
								fieldLabel: 'Telefono 2', labelAlign: 'right',
							    maxLength: 20,
							    value: <?php echo j(trim(acs_u8e($r['GCTEL2']))); ?>
							}	
							<?php }else{
							
							    if($request['type'] == 'P')
							        $r['GCTPIN'] = 'P';
						        if($request['type'] == 'D')
						            $r['GCTPIN'] = 'D';
							   write_combo_std('GCTPIN', 'Tipo indirizzo', $r['GCTPIN'], acs_ar_to_select_json($main_module->find_TA_std('INDI', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('labelAlign' => 'right')) ?>	  
							<?php }?>						
							
						]
					}
					
					<?php if($request['type'] == 'P' || $r['GCTPIN'] == 'P'){?>
					
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
								{
    						name: 'GCWWW',
    						fieldLabel: 'Sito web PdV',
    					    maxLength: 60,
    					    value: <?php echo j(trim(acs_u8e($r['GCWWW']))); ?>							
    						}					
							
						]
					}
					
					<?php }?>
					
					
					
					<?php if($p['tipo_anagrafica'] != 'DES'){?>	
					, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [	
										
							{
								name: 'GCFAX',
								fieldLabel: 'Fax',
							    maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCFAX']))); ?>
							}
							
							
<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>							
							, {
								name: 'GCCKIN',
								fieldLabel: 'Check ordine', labelAlign: 'right',
							    maxLength: 20,
							    value: <?php echo j(trim(acs_u8e($r['GCCKIN']))); ?>
							}
<?php } ?>							
							
							
						]
					}
					
					
					,{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						//anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
						
						<?php write_combo_std('GCRISC', 'Rischio', $r['GCRISC'], acs_ar_to_select_json(find_TA_sys('CURI', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>
    						, {
    						name: 'GCCSDI',
    						fieldLabel: 'Codice SDI', 
    					    maxLength: 7,
    					    labelAlign: 'right',
    					    //width : 200,
    						value: <?php echo j(trim(acs_u8e($r['GCCSDI']))); ?>
    						}
						
						]
					},{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
    					{xtype: 'component',
                                flex : 1,
                            },
    						{
    						name: 'GCSIRE',
    						fieldLabel: 'Siren/Siret',
    					    maxLength: 15,
    					    labelAlign : 'right',
    					    value: <?php echo j(trim(acs_u8e($r['GCSIRE']))); ?>							
    						}
						
						]
					}, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						//anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
    						{
    						name: 'GCWWW',
    						fieldLabel: 'Sito web',
    					    maxLength: 60,
    					    value: <?php echo j(trim(acs_u8e($r['GCWWW']))); ?>							
    						}
    						
						
						]
					}
					<?php }?>
					
					, {
						xtype: 'fieldset', title: 'E-mail',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						
						
						<?php if($p['tipo_anagrafica'] == 'CLI' || $p['tipo_anagrafica'] == 'PVEN'){?>	
								{
									regex: <?php echo regex_multiemail(); ?>,
									name: 'GCMAIL',
									fieldLabel: 'Principale',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAIL']))); ?>							
								},
							<?php }?>
							
							
							
					<?php if($r['GCTPIN'] == 'P' || $request['type'] == 'P'){?>
					
						     {
									regex: <?php echo regex_multiemail(); ?>,
									name: 'GCMAI5',
									fieldLabel: 'Pubblica',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI5']))); ?>							
								}, {
									regex: <?php echo regex_multiemail(); ?>,
									name: 'GCMAI6',
									fieldLabel: 'Comunicaz. aziendali',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI6']))); ?>
								},
					
					
					
					<?php } ?>
					
							{
								regex: <?php echo regex_multiemail(); ?>,
								name: 'GCMAI1',
								fieldLabel: 'Conferme ordine',
							    maxLength: 60,
							    value: <?php echo j(trim(acs_u8e($r['GCMAI1']))); ?>							
							}
							
							
    						<?php if (in_array($p['tipo_anagrafica'], array('CLI')) || 
    					           ($r['GCTPIN'] == 'P' || $request['type'] == 'P')) { ?>								
    								, {
    									regex: <?php echo regex_multiemail(); ?>,
    									name: 'GCMAI2',
    									fieldLabel: 'Fatture vendita',
    								    maxLength: 60,
    								    value: <?php echo j(trim(acs_u8e($r['GCMAI2']))); ?>							
    								}
    						<?php } ?>		
							
							
								
							
								, {
									regex: <?php echo regex_multiemail(); ?>,
									name: 'GCMAI3',
									fieldLabel: 'Piani spedizione',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI3']))); ?>							
								}, {
									regex: <?php echo regex_multiemail(); ?>,
									name: 'GCMAI4',
									fieldLabel: 'Autorizzazione resi',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI4']))); ?>
								},
								{
									regex: <?php echo regex_multiemail(); ?>,
									name: 'GCMAI7',
									fieldLabel: 'Conferme Assistenza',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI7']))); ?>
								},
									{
									regex: <?php echo regex_multiemail(); ?>,
									name: 'GCMAI8',
									fieldLabel: 'Conferma Esposizioni',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI8']))); ?>
								}
								
								<?php if($p['tipo_anagrafica'] == 'CLI'){?>	
    								, {
    									regex: <?php echo regex_multiemail(); ?>,
    									name: 'GCPEC',
    									fieldLabel: 'Indirizzo PEC',
    								    maxLength: 120,
    								    value: <?php echo j(trim(acs_u8e($r['GCPEC']))); ?>							
    								}
								<?php }?>
		
				   							
								
						]
					  },
					  
			
					<?php if (in_array($p['tipo_anagrafica'], array('DES'))) {
					    ?>		
    						<?php write_combo_std('GCDSTA', 'Destinazione assegnata', $r['GCDSTA'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $r['cod_cli'], null, null, null, 0, " AND TANR <> '{$r['GCCDCF']}' AND SUBSTRING(TAREST, 163, 1) <> 'P'", 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 130, 'dest_change_list' => 'Y', 'show_det' => 'Y', 'cliente' => $r['cod_cli']) ) ?>,
    						
  					
						
					   , {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						defaults:{ labelWidth: 130, xtype: 'textfield', readOnly: true},
					    items: [
    						{
    						name: 'ind_std',
    						fieldLabel: 'Indirizzo',
    						flex: 2,
    					    value: <?php echo j(trim(acs_u8e($r['IND_D']))); ?>							
    						},{
    						name: 'cap_std',
    						fieldLabel: 'Cap.',
    						flex: 0.5,
    						labelWidth: 40,
    						labelAlign : 'right',
    					    value: <?php echo j(trim(acs_u8e($r['CAP_D']))); ?>							
    						}
						
						  ]
					   },
					    {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						defaults:{ labelWidth: 130, xtype: 'textfield', readOnly: true},
					    items: [
    						{
    						name: 'loc_std',
    						fieldLabel: 'Localit&agrave;',
    						flex: 2,
    					    value: <?php echo j(trim(acs_u8e($r['LOC_D']))); ?>							
    						},{
    						name: 'pro_std',
    						flex: 0.5,
    						labelWidth: 40,
    						fieldLabel: 'Prov.',
    						labelAlign : 'right',
    					    value: <?php echo j(trim(acs_u8e($r['PRO_D']))); ?>							
    						}
						
						  ]
					   }
						
						
						//]}
					<?php }?>
					
							
					
					
					
					],
					
					
					
					
		dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            fixed: true,
            items: [					
			{
		        xtype:'tbspacer',
		        flex:1
		    }
		    
		    
<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { 
    if($request['i_e'] == 'I' || $r['GCNAZI'] == 'ITA' || $r['GCNAZI'] == 'IT'){?>		    
			, {
	            text: 'Partita iva',
	            iconCls: 'icon-logo_cerved_q-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

	            	//verifico inserimento partita iva
	            	piva = form.findField('GCPIVA').getValue();
	            	if (Ext.isEmpty(piva) == true){
	            		acs_show_msg_error('Inserire Partita IVA');
	            		return false;
	            	}
	            	
	            	
	            	
					if(!Ext.isEmpty(piva)){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            
						            if(jsonData.success == false){
						               acs_show_msg_info(jsonData.error_msg, 'Continua');
	            		               return false;
	            		            }
						            
						            acs_show_win_std('CERVED - Fast check', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
											{
												view_type: 'base',
								  				key_request: jsonData.key_request,
								  				companyId: jsonData.companyId,
						        				from_window_id: loc_win.id
								  			}, 650, 550, {}, 'icon-logo_cerved_q-16');						            
						            
						            
						            //form.findField('GCDCON').setValue(jsonData.companyInfo[0].companyName);						            
						            //form.findField('GCCAP').setValue(jsonData.companyInfo[0].address[0].postCode);
						            //form.findField('GCINDI').setValue(jsonData.companyInfo[0].address[0].street);
						            //form.findField('f_comune').setValue(jsonData.companyInfo[0].address[0].municipality);						            						            						            

						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }            	                	                
	            }
	        }
<?php }} ?>	        
	        
	        
	        
	        
	        , {
	            text: 'Gmap',
	            iconCls: 'icon-gmaps_logo-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');
	            	
	            	//costruisco indirizzo
	            	ind = 'VIA INSALA, 236 - 61122 - PESARO';
	            	
					//$ar[$liv1_v]["val"][$liv2_v]["gmap_ind"]	= implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));
					
					f = [];
					//f.push(form.findField('f_nazione').getValue());
					f.push(form.findField('GCCAP').getValue());
					f.push(form.findField('GCPROV').getValue());
					f.push(form.findField('GCLOCA').getValue());
					f.push(form.findField('GCINDI').getValue());
					
					km_field_id = form.findField('GCKMIT').id;	            	
	            	
					gmapPopup("../desk_vend/gmap_route.php?km_field_id=" + km_field_id + "&ind=" + f.join());	            	
	            	
	            }
	         }
			, {
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var panel_form = this.up('form'),
	            		form = panel_form.getForm(),           	
	            		form_values = form.getValues();
	            	
	            	 <?php
	    	            if($row['R_STD'] > 0 && (trim($r['GCTPIN']) == 'D' || $request['type'] == 'D')){ ?>
	            	   
	            	    if(form.findField('GCTPDS').getValue() != 'null' && form.findField('GCTPDS').getValue() == 'S'){
	            	    	acs_show_msg_error('Gi&agrave; presente una destinazione Standard');
	            	    	return false;
	            	    }
	            	  <?php }?>
	            	
	            	
	            	
	            	 // ************************
	            	   //In base a tipologia (se EFFETTIVO) rendo obbligatorio alcuni campi
	            	   //   (GCSEL3 decodificato da ANC01)
	            	   
	            	   if (form_values.GCSEL3 == '020'){	//Cliente Effettivo
	            	   	if (form_values.GCTFIS == 'P'){
	            	   		//PRIVATO
	            	   		//Obbligatorio: Cognome, Nome, CF, Indirizzo, Localita, CAP, Provincia, Nazione, Telefono
	            	   		if (
	            	   			Ext.isEmpty(form_values.GCCOGN) ||
	            	   			Ext.isEmpty(form_values.GCNOME) ||
	            	   			Ext.isEmpty(form_values.GCCDFI) ||
	            	   			Ext.isEmpty(form_values.GCINDI) ||
	            	   			Ext.isEmpty(form_values.GCLOCA) ||
	            	   			Ext.isEmpty(form_values.GCCAP)  ||
	            	   			Ext.isEmpty(form_values.GCPROV) ||
	            	   			Ext.isEmpty(form_values.GCNAZI) ||
	            	   			Ext.isEmpty(form_values.GCTEL)
	            	   			){
	            	   			Ext.Msg.alert('Campi obbligatori non compilati', 'Per rendere Effettivo un cliente &egrave; obbligatorio indicare:<br/> Cognome, Nome, CF, Indirizzo, Localit&agrave;, CAP, Provincia, Nazione, Telefono');
	            	   			return false;
	            	   		}
	            	   	} else {
	            	   		//AZIENDA
	            	   		//Obbligatorio: Denominazione, CF, PIVA, Indirizzo, Localita, CAP, Provincia, Nazione, SDI o PEC, Telefono
	            	   		if (
	            	   			Ext.isEmpty(form_values.GCDCON) ||
	            	   			Ext.isEmpty(form_values.GCPIVA) ||	            	   				            	   			
	            	   			Ext.isEmpty(form_values.GCINDI) ||
	            	   			Ext.isEmpty(form_values.GCLOCA) ||
	            	   			Ext.isEmpty(form_values.GCCAP)  ||
	            	   			Ext.isEmpty(form_values.GCPROV) ||
	            	   			Ext.isEmpty(form_values.GCNAZI) ||
	            	   			Ext.isEmpty(form_values.GCTEL)  ||
	            	   			(
	            	   				Ext.isEmpty(form_values.GCCSDI) && Ext.isEmpty(form_values.GCPEC) 
	            	   			)
	            	   			){
	            	   			Ext.Msg.alert('Campi obbligatori non compilati', 'Per rendere Effettivo un cliente &egrave; obbligatorio indicare:<br/> Denominazione, CF, PIVA, Indirizzo, Localit&agrave;, CAP, Provincia, Nazione, SDI o PEC, Telefono');
	            	   			return false;
	            	   		}	            	   		
	            	   	}	            	   		            	   	            	   
	            	   } //if cliente effettivo	            	   
	            	   // ************************
	            	                
	            	     
                         if (Ext.isEmpty(form_values.GCNAZI) || (
                         	 form_values.GCNAZI.trim() == 'ITA' || 
                         	 form_values.GCNAZI.trim() == 'IT' ||
                         	 form_values.GCNAZI.trim() == '')) {
                                var cf = form_values.GCCDFI;
        						var controllo = 0;
        						 
        						if (!Ext.isEmpty(cf)) {
        						  if (cf.length > 16) {
        						  	Ext.Msg.alert('Errore', 'Il codice fiscale deve contenere al massimo 16 caratteri');
	            	   				return false;
        						  }
    								
        						  else
        							controllo =  ControllaCF(cf);
        						}
        						 
        					 	 
            					if(controllo == '1'){
                    	   			msg = 'Il codice fiscale deve contenere 16 tra lettere e cifre.';
                    	   		} else if(controllo == '2'){
                    	   			msg = 'Il codice fiscale non &egrave; valido.';
                    	   		}
                        	} else 	{   		
            	   		  	 //NON ITA
            	   		   	 var controllo = '';
            	   		    }
            	   		  
            	   		  
            	   		  if(controllo != ''){
            	   		  
            	   		  	Ext.Msg.confirm('Richiesta conferma', msg + '<br>Confermi l\'operazione?' , function(btn, text){
            	   		    	if (btn == 'yes')
            	   		    		panel_form.acs_actions.exe_form_submit(panel_form);            	   		                  	   		  	
            	   		  	}); //confirm            	   		  
            	   		  
            	   		  } else {            	   		  
            	   		  
            	   		  //controllo a buon fine
            	   		  
            	   		     Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
				      	    		 		Ext.Msg.confirm('Richiesta conferma', jsonData.msg_error + '<br><p style="text-align:center; font-weight: bold; font-size:14px;">Confermi l\'operazione?</p>' , function(btn, text){
                                	   		    	if (btn == 'yes')
                                	   		    		panel_form.acs_actions.exe_form_submit(panel_form);            	   		                  	   		  	
                            	   		  	}); //confirm 
				        			     }else{
				        			        panel_form.acs_actions.exe_form_submit(panel_form);   
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
            	   		  
            	   		  	 	            	   		 
            	   		  }
           	                
	            }
	        }	         
	         
	        ]// buttons
	       }
	      ]             
			
			
	    , acs_actions: {
	    	exe_form_submit: function(panel_form){
				var form = panel_form.getForm(),
				loc_win = panel_form.up('window');
		
         	   		  if(form.isValid()){
         	   		       Ext.Ajax.request({
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_new_anag',
						        jsonData: {
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>,
						        	cod_cli : <?php echo j($r['cod_cli'])?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						         var jsonData = Ext.decode(result.responseText);
						         if(jsonData.success == false){
        					      	    acs_show_msg_error(jsonData.msg_error);
        					      	    return false;
        					     }else{  
        					        
        					        var record = jsonData.record;
						            var tabPanel = loc_win.down('tabpanel');
						            						            
						            loc_win.fireEvent('afterInsertRecord', loc_win, record);
						            
						            tabpanel_load_record_in_form(tabPanel, record);
						            
						            tabPanel.GCPROG = jsonData.GCPROG;
						            form.findField('mode').setValue('EDIT');
						            form.findField('mode').resetOriginalValue(); //clear isDirty
						            
						            var title = 'Modifica anagrafica [' + record.GCCDCF + ', ' + record.GCCINT + '] ' +record.GCDCON;
						            loc_win.setTitle(title);
						            
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						            						            						            
									tabPanel.items.each(function(panelItem, index, totalCount){
										panelItem.setDisabled(false);
										
										//per aggiornare le condizioni commerciali: MIGLIORARE: TODO
										if (panelItem.xtype == 'grid'){
										  	panelItem.store.proxy.extraParams.GCDEFA = 'ITA';
											panelItem.store.proxy.extraParams.rec_id = jsonData.GCPROG;
											panelItem.store.proxy.extraParams.cdcf = record.GCCDCF;
											panelItem.store.load();
										}
										
										
										if (panelItem.itemId == 'form_LOGIS'){
										  panelItem.getForm().findField('GCPROG').setValue(jsonData.GCPROG);
										  panelItem.getForm().setValues(jsonData.record)
										}	
										if (panelItem.itemId == 'form_CRM'){
										  panelItem.getForm().findField('GCPROG').setValue(jsonData.GCPROG);											
									      panelItem.getForm().setValues(jsonData.record);
									    }
										
									});
									
									tabPanel.acs_actions.exe_afterSave(loc_win);
									
									}
						      },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    } //is valid		
		
			} //exe_form_submit
			
	    } //acs_actions		
			

				
				
        }
<?php
} //write_new_form_ITA



function write_new_form_COMM($p = array(), $request = array()){
    global $conn, $cfg_mod_Gest, $id_ditta_default;
    
    if ($request['mode'] == 'EDIT'|| $request['mode'] == 'DUP'){ //|| $request['mode'] == 'CREATE'
        //recupero la riga da modificare
        $sql = "SELECT RRN(CC) AS RRN, CC.*, GC.*, TA.TADESC AS DES1, TA.TADES2 AS DES2, 
            TA_NS_BANCA.TADESC AS DES1_NS_BANCA, TA_NS_BANCA.TADES2 AS DES2_NS_BANCA,
            TA_ANCCO.TADESC AS D_STD
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
        LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
        ON CCDT = GCDT AND CCPROG = GCPROG
        LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
            ON CCDT = TA.TADT AND TA.TAID = 'CUBA' AND TA.TANR = CCBANC
        LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA_NS_BANCA
            ON CCDT = TA_NS_BANCA.TADT AND TA_NS_BANCA.TAID = 'CUBA' AND TA_NS_BANCA.TANR = CCBASF
        LEFT OUTER JOIN {$cfg_mod_Gest['file_tabelle']} TA_ANCCO
        ON CCDT = TA_ANCCO.TADT AND TA_ANCCO.TATAID = 'ANCCO' AND TA_ANCCO.TAKEY1 = GC.GCDEFA AND TA_ANCCO.TAKEY2 = CCCCON
        WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($request['CCDT'], $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));
        
        $ret = array();
        $r = db2_fetch_assoc($stmt);
        $r['des_cuba'] = trim($r['DES1']).trim($r['DES2']);
        $r['des_ns_cuba'] = trim($r['DES1_NS_BANCA']).trim($r['DES2_NS_BANCA']);
        if(trim($r['CCCCON']) == 'STD')
            $r['CCDCON'] =  $r['D_STD'];
            
            //des_banca
            if (strlen(trim($r['CCABI'])) > 0 && strlen(trim($r['CCCAB'])) > 0) {
                $sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, array($r['CCABI'], $r['CCCAB']));
                $r_cab = db2_fetch_assoc($stmt);
                $des_banca = implode(" - ", array(trim($r_cab['XDSABI']), trim($r_cab['XDSCAB'])));
            }                        
            
            $ta_sys_pg = find_TA_sys('CUCP', trim($r['CCPAGA']));
            $r['des_paga'] = $ta_sys_pg[0]['text'];
            
            
            
		
	} else {
		
		
		//DALLA REGIONE PROVO A RECUPERARE IL REFERENTE (TATISP IN TATAID='ANREG')
		
			$sql = "SELECT TA_REG.TADESC AS DES_REG, TA_REG.TATISP AS REF_BY_REG, GCDEFA, GCCDCF 
					FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
					LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
						TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
					LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
						TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
					WHERE GCPROG = ?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($request['CCPROG']));
			$r_regione = db2_fetch_assoc($stmt);
						
		
		//In base al GCDEFA del cliente, verifico se questa condizione (codice) ha un default sulla ditta 1W.
		//Se trovo il default preimposto alcuni campi
		$row_gc = get_gcrow_by_prog($request['CCPROG']);		
		
		
        $row_gc_modan = get_gcrow_by_modan_default($row_gc['GCDEFA']);
        $id_ditta_default_W = trim($request['CCDT']) . 'W';
         
		
		$sql = "SELECT RRN(CC) AS RRN, CC.*, GC.*, TA.TADESC AS DES1, TA.TADES2 AS DES2, TA_ANCCO.TADESC AS D_STD
                FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
                LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                 ON CCDT = GCDT AND CCPROG = GCPROG
                LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
                 ON '{$request['CCDT']}' = TA.TADT AND TA.TAID = 'CUBA' AND TA.TANR = CCBANC
                LEFT OUTER JOIN {$cfg_mod_Gest['file_tabelle']} TA_ANCCO
                 ON '{$request['CCDT']}' = TA_ANCCO.TADT AND TA_ANCCO.TATAID = 'ANCCO' AND TA_ANCCO.TAKEY1 = GC.GCDEFA AND TA_ANCCO.TAKEY2 = CCCCON
                WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";		
		
				
		$stmtD = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmtD, array($id_ditta_default_W, $row_gc_modan['GCPROG'], $request['CCSEZI'], $request['CCCCON']));
		$r_def = db2_fetch_assoc($stmtD);

		if ($r_def){
		    $r = $r_def;      //parto da default
		} else {
		    $r = array();     
		}
		
		
		//NUOVA CONDIZIONE COMMERCIALE
		$r['CCDT']    = $request['CCDT'];
		$r['CCPROG']  = $request['CCPROG'];
		$r['CCSEZI']  = $request['CCSEZI'];
		$r['CCCCON']  = $request['CCCCON'];
		$r['CCDCON']  = $request['CCDCON'];
		$r['CCREFE']  = $r_regione['REF_BY_REG'];
		$r['GCDEFA']  = $r_regione['GCDEFA'];
		$r['GCCDCF']  = $r_regione['GCCDCF'];
		if($r['CCDTVI'] == 0)
		  $r['CCDTVI']  = oggi_AS_date();
	    if($r['CCDTVF'] == 0)
	      $r['CCDTVF']  = '20991231';

		
	} //NEW
	
	global $s, $main_module;
	?>
       
        {
            xtype: 'form',
            frame: true,
            disabled: false, //true,
            autoScroll: true,
            flex: 1,
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield' <?php echo $readOnly ?>},
            items: [
            
            	{xtype: 'tabpanel',
        		items: [
        		
        		 {  xtype : 'form',
        		   title : 'Dettagli',
        		   frame: true,
        		   items : [
        		
        		   
        		    {xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($r['CCDT']); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j($r['CCSEZI']); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($r['CCPROG']); ?>},
            		
            		<?php if($request['mode'] != 'CREATE' && $request['mode'] != 'DUP'){?>
            			{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j($r['CCCCON']); ?>},
            		<?php }?>
            		
            		<?php if (trim($r['CCCCON']) == 'STD'){ ?>
            		    {xtype: 'hiddenfield', name: 'CCDCON', value: <?php echo j($r['CCDCON']); ?>},
            		<?php } ?>
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							   	value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,							    
							    <?php if($request['mode'] != 'CREATE' && $request['mode'] != 'DUP'){ ?>
							    	disabled: true
							    <?php }?>
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    
							    //La STD non e' gestibile,
							    //le altre SI ma devo separare la descrizione in 30 (nome) + 30 (articolo)
							    
							    <?php if (trim($r['CCCCON']) == 'STD'){ ?>
							    	value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    	maxLength: 60,
							    	disabled: true,
							    <?php } else { ?>
							    	value: <?php echo j(trim(acs_u8e(substr($r['CCDCON'], 0, 30)))); ?>,
							    	maxLength: 30,
							    	disabled: false,							    	
							    <?php }?>						
							}
							
							
							<?php if (trim($r['CCCCON']) != 'STD'){ ?>
								, {
									name: 'CCDCON_ART',
									fieldLabel: 'Art.', flex: 0.8, labelAlign: 'right', labelWidth: 40,
									value: <?php echo j(trim(acs_u8e(substr($r['CCDCON'], 30, 30)))); ?>,
							    	maxLength: 30,
							    	disabled: false,									
								}
							<?php } ?>
							]
						}            
            
            
					, {
						xtype: 'fieldset', title: 'Coordinate bancarie/Pagamento',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						          {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'CCBANC',
											fieldLabel: 'Vostra banca', width: 150,
											maxLength: 3,
											readOnly : true, 
										    value: <?php echo j(trim(acs_u8e($r['CCBANC']))); ?>							
										},{
											name: 'des_cuba',
											fieldLabel: '', 
											flex: 1,
											readOnly : true,
										    maxLength: 160, 
										    value: <?php echo j(trim(acs_u8e($r['des_cuba']))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 5',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/sub_red_delete.png") . " width=16>"); ?>,
										    listeners: {
											            render: function( component ) {
											                m_form = this.up('form').getForm();
											                component.getEl().on('dblclick', function( event, el ) {
															
						        					 		m_form.findField('des_cuba').setValue('');
								        					m_form.findField('CCBANC').setValue('');
								        					m_form.findField('CCABI').setValue('');
								        					m_form.findField('CCCAB').setValue('');
								        					m_form.findField('des_banca').setValue('');
										        			       
										        				
															});										            
											             }
													}										    
										    
										    
										},{										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 5',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners = {
									        					afterSelected: function(from_win, row){
									        					 	m_form.findField('des_cuba').setValue(row.descrizione);
										        					m_form.findField('CCBANC').setValue(row.codice);
										        					m_form.findField('CCABI').setValue(row.abi);
										        					m_form.findField('CCCAB').setValue(row.cab);
										        					m_form.findField('des_banca').setValue(row.descrizione);
										        			        from_win.close();
										        				}										    
														    },
															acs_show_win_std('Ricerca banca codificata [CUBA]', 'acs_ricerca_tab_sys.php?fn=open_tab', {taid : 'CUBA'}, 1000, 400, my_listeners, 'icon-leaf-16');    
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								},	
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'CCABI',
											fieldLabel: 'Archivio sportelli',
										    maxLength: 5,
										    width: 180,
										    value: <?php echo j(trim(acs_u8e($r['CCABI']))); ?>							
										}, {
											name: 'CCCAB',
											fieldLabel: '', labelWidth: 0, width: 60,
										    maxLength: 5,
										    value: <?php echo j(trim(acs_u8e($r['CCCAB']))); ?>,
										    listeners: {
                                				'blur': function(field) {
                                					var form = this.up('form').getForm();
                                					var form_values = form.getValues();
                                					if(form_values.CCABI.trim() != ''){
                                					
                                    					Ext.Ajax.request({
                        								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_abicab',
                        								        method     : 'POST',
                        					        			jsonData: {
                        					        				form_values : form_values
                        										},							        
                        								        success : function(result, request){
                        								            var jsonData = Ext.decode(result.responseText);
                        								        	if(jsonData.success == false){
                                						           	    acs_show_msg_error(jsonData.msg_error);
                                	 					 			    return;
                                						            }else{
                                						               if(form_values.CCBANC.trim() == ''){
                                						               	  form.findField('CCBANC').setValue(jsonData.CCBANC);
										        					   	  form.findField('des_cuba').setValue(jsonData.des_cuba);
										        					   	  form.findField('des_banca').setValue(jsonData.des_cuba);
                                						            	}
                        								            }
                        					            		},
                        								        failure    : function(result, request){
                        								            Ext.Msg.alert('Message', 'No data to be loaded');
                        								        }
                        								    });
                    								  }
                                				}
                                			}							
										},{
											name: 'des_banca',
											fieldLabel: '', flex: 1,
										    maxLength: 160, 
										    readOnly : true,
										    value: <?php echo j(trim(acs_u8e($des_banca))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners= {
									        					afterSelected: function(from_win, form_values){
									        													        					
										        						m_form.findField('CCABI').setValue(form_values.f_abi);
										        						m_form.findField('CCCAB').setValue(form_values.f_cab);
										        						m_form.findField('des_banca').setValue(form_values.des_banca);
										        															        						
										        						  Ext.Ajax.request({
                                    								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_cuba',
                                    								        method     : 'POST',
                                    					        			jsonData: {
                                    					        				//rrn : <?php echo j($r['RRN'])?>,
                                    					        				form_values : form_values,
                                    					        				from_search : 'Y'
                                    										},							        
                                    								        success : function(result, request){
                                    								            var jsonData = Ext.decode(result.responseText);
                                    								        	m_form.findField('CCBANC').setValue(jsonData.CCBANC);
										        								m_form.findField('des_cuba').setValue(jsonData.des_cuba);
                                    								            from_win.close();
                                    					            		},
                                    								        failure    : function(result, request){
                                    								            Ext.Msg.alert('Message', 'No data to be loaded');
                                    								        }
                                    								    });
										        					
														        		}										    
														    },
															
															
															acs_show_win_std('Ricerca banca', 
																'search_bank.php?fn=open_search', 
																{}, 450, 300, my_listeners, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								}		
								
								, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [								
										 {
											name: 'CCCOCO',
											fieldLabel: 'Conto corrente',
										    maxLength: 20, flex: 0.8,
										    value: <?php echo j(trim(acs_u8e($r['CCCOCO']))); ?>							
										},
										{
											name: 'CCIBAN',
											fieldLabel: 'IBAN',
										    labelWidth: 40, maxLength: 34, flex: 1,
										    labelAlign : 'right',
										    value: <?php echo j(trim(acs_u8e($r['CCIBAN']))); ?>
										 }, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 5',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/button_blue_repeat.png") . " width=16>"); ?>,
										    listeners: {
											   render: function( component ) {
											   
											   		Ext.QuickTips.register({
                                                        target: component.getEl(),
                                                        text: 'Verifica IBAN',
                                                        enabled: true,
                                                        showDelay: 20,
                                                        trackMouse: true,
                                                        autoShow: true
                                                      });
											     
											        component.getEl().on('dblclick', function( event, el ) {
    												  	var form = component.up('form').getForm();
                                    					var form_values = form.getValues();
                                    					console.log(form_values);													Ext.Ajax.request({
                								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_iban',
                								        method     : 'POST',
                					        			jsonData: {
                					        				form_values : form_values
                										},							        
                								        success : function(result, request){
                								            var jsonData = Ext.decode(result.responseText);
                								        	if(jsonData.success == false){
                        						           	    acs_show_msg_error(jsonData.msg_error);
                        	 					 			    return;
                        						            }else{
                        						               if(jsonData.CCBANC == ''){
                                                                  Ext.Msg.confirm('Richiesta conferma', jsonData.msg_info , function(btn, text){																							    
                                								   if (btn == 'yes'){	
                                								        Ext.Ajax.request({
                                                					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_nuova_cuba',
                                                					        method     : 'POST',
                                                		        			jsonData: {
                                                		        			  abi : jsonData.abi,
                                                		        			  cab : jsonData.cab,
                                                		        			  des_cuba : jsonData.des_cuba
                                                		        			},							        
                                                					        success : function(result, request){
                                                					            var jsonData = Ext.decode(result.responseText);
                                                		            			form.findField('CCBANC').setValue(jsonData.CCBANC);
                                        						               	form.findField('CCABI').setValue(jsonData.CCABI);
                                        						               	form.findField('CCCAB').setValue(jsonData.CCCAB);
                								        					   	form.findField('des_cuba').setValue(jsonData.des_cuba);
                								        					   	form.findField('des_banca').setValue(jsonData.des_cuba);
                                                		            		   
                                                		            		},
                                                					        failure    : function(result, request){
                                                					            Ext.Msg.alert('Message', 'No data to be loaded');
                                                					        }
                                                					    });
                                								   
                                								      }	
                                                        						                    
                                									});
                        						               }else{
                        						               	  form.findField('CCBANC').setValue(jsonData.CCBANC);
                        						               	  form.findField('CCABI').setValue(jsonData.CCABI);
                        						               	  form.findField('CCCAB').setValue(jsonData.CCCAB);
								        					   	  form.findField('des_cuba').setValue(jsonData.des_cuba);
								        					   	  form.findField('des_banca').setValue(jsonData.des_cuba);
                        						            	}
                								            }
                					            		},
                								        failure    : function(result, request){
                								            Ext.Msg.alert('Message', 'No data to be loaded');
                								        }
                								    });															
															
															
															
														
															});
											             }
													}										    
										    
										    
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 5',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/button_black_repeat_dx.png") . " width=16>"); ?>,
										    listeners: {
											            render: function( component ) {
											            
    											            Ext.QuickTips.register({
                                                                target: component.getEl(),
                                                                text: 'Ricalcola IBAN',
                                                                enabled: true,
                                                                showDelay: 20,
                                                                trackMouse: true,
                                                                autoShow: true
                                                              });
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
															Ext.getBody().mask('Loading... ', 'loading').show();
															
															Ext.Ajax.request({
																	timeout: 2400000,
															        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ricalcola_iban',
															        jsonData: {
															        	form_values: m_form.getValues()
															        },	
															        method     : 'POST',
															        waitMsg    : 'Data loading',
															        success : function(result, request){
															        						        
																		Ext.getBody().unmask();
															            var jsonData = Ext.decode(result.responseText);
																		
																		m_form.findField('CCIBAN').setValue(jsonData.iban);
									
															        },
															        failure    : function(result, request){
																		Ext.getBody().unmask();							        
															            Ext.Msg.alert('Message', 'No data to be loaded');
															        }
															    });																
															
															
															
														
															});
											             }
													}										    
										    
										    
										}
										
										
									]
								},
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox'},
									anchor: '-10',
									items: [
									<?php 
									if (trim($r['CCCCON']) == 'STD') 
									   $allowBlank = 'false';
									else
									   $allowBlank = 'true';
									?>
									
									
								{
								    xtype : 'combo',
                                    name: 'CCPAGA',
                                    value: <?php echo j(trim($r['CCPAGA'])); ?>,
                                    flex : 1.5,
                                    fieldLabel: 'Pagamento',
                                    displayField: 'text',
                                    valueField: 'id',
                                    anchor: '-10',
                                    editable : true,
                                    forceSelection:true,
                                    allowBlank: <?php echo $allowBlank ?>,													
                                  	store : {
                        				autoLoad: true,
                        				pageSize: 1000,            	
                        				proxy: {
                        					type: 'ajax',
                        					url : 'acs_panel_ins_new_anag.php?fn=get_json_data_pagamento',
                        					reader: {
                        						type: 'json',
                        						method: 'POST',		
                        						root: 'root',
                        					},
                        					actionMethods: {
                        							  read: 'POST'
                        							},
                        					extraParams: {
                        								gruppo: <?php echo j($request['g_paga']); ?>
                        						   },               						        
                        					doRequest: personalizza_extraParams_to_jsonData
                        				},       
                        					fields: ['id', 'text', 'sosp', 'color']   
                        					<?php if(trim($r['CCPAGA']) != ''){?>
                            					,listeners: {
                            					   load: function(store, a, b, c) {
                                                        if(store.proxy.extraParams.gruppo != '')
                                                        	store.insert(0, {id: <?php echo j($r['CCPAGA'])?>, text: <?php echo j("[".trim($r['CCPAGA']). "] ".trim($r['des_paga'])) ?>});
                                                    }
                                                }    
                                            <?php }?> 	
                        			},
                                    tpl: [
                                    	 '<ul class="x-list-plain">',
                                            '<tpl for=".">',
                                            '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                                    		 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                                                   '</tpl>'
                                                     
                                                    ),
                                        
                                    	queryMode: 'local',
                                    	minChars: 1,	
                                        listeners: { 
                                        	   beforequery: function (record) {
                                        		record.query = new RegExp(record.query, 'i');
                                        		record.forceAll = true;
                                        	},
                                        
                                        	 
                                        	 select: function(combo, newValue, oldValue, eOpts) {
                                        	    if(combo.value == 'ALL'){
                                        	       combo.store.proxy.extraParams.gruppo = '';
                                        	       combo.setValue('');
                                        	       combo.store.load();
                                        	     
                                        	       //combo.focus();
                                        	      
                                        	    }
    											
    										 },
    										 afterrender: function(comp){
    										   <?php if(trim($r['CCPAGA']) != ''){?>
    		                                     data = [{id: <?php echo j($r['CCPAGA'])?>, text: <?php echo j("[".trim($r['CCPAGA']). "] ".trim($r['des_paga'])) ?>}
    		                                     ];
		                                         comp.store.loadData(data);
		                                         comp.setValue(<?php echo j($r['CCPAGA'])?>);
            		                           <?php }?>              
            		                          } 
                                       		   
                                    	} 					 
                                    }
                                     
									   , <?php write_combo_std('CCTSIV', 'Rata IVA', $r['CCTSIV'], acs_ar_to_select_json($main_module->find_TA_std('ANC03', null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y'), array('flex_width' => "flex: 1", 'labelAlign' => 'right') ) ?>
									]
								}, 
								
								<?php if (trim($r['CCCCON']) == 'STD'){ ?>
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									items: [										
										   {xtype: 'numberfield',
											name: 'CCNGPA',
											fieldLabel: 'GG inizio scad.',
										    maxLength: 5, flex: 1,
										    maxValue : 31,
										    hideTrigger : true,
										    value: <?php echo j(trim(acs_u8e($r['CCNGPA']))); ?>							
										}, {
										    xtype: 'numberfield',
											name: 'CCMME1',
											fieldLabel: '1&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    maxValue : 12,
										    hideTrigger : true,
										    value: <?php echo j(trim(acs_u8e($r['CCMME1']))); ?>							
										}, <?php write_combo_std('CCGGE1', '', $r['CCGGE1'], acs_ar_to_select_json($main_module->find_TA_std('ANC04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>
										
										, {
										    xtype: 'numberfield',
											name: 'CCMME2',
											fieldLabel: '2&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    maxValue : 12,
										    hideTrigger : true,
										    value: <?php echo j(trim(acs_u8e($r['CCMME2']))); ?>							
										}, <?php write_combo_std('CCGGE2', '', $r['CCGGE2'], acs_ar_to_select_json($main_module->find_TA_std('ANC04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>
									]
								}
								
								
								//NOSTRA BANCA
								, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'CCBASF',
											fieldLabel: 'Nostra banca', width: 150,
											maxLength: 3,
											readOnly : true, 
										    value: <?php echo j(trim(acs_u8e($r['CCBASF']))); ?>							
										},
										{xtype: 'hiddenfield', name: 'CCABIF',   value: <?php echo j($r['CCABIF']); ?>},
										{xtype: 'hiddenfield', name: 'CCCABF',   value: <?php echo j($r['CCCABF']); ?>},
										{
											name: 'des_ns_cuba',
											fieldLabel: '', 
											flex: 1,
											readOnly : true,
										    maxLength: 160, 
										    value: <?php echo j(trim(acs_u8e($r['des_ns_cuba']))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 5',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/sub_red_delete.png") . " width=16>"); ?>,
										    listeners: {
											            render: function( component ) {
											                m_form = this.up('form').getForm();
											                component.getEl().on('dblclick', function( event, el ) {
																m_form.findField('des_ns_cuba').setValue('');
									        					m_form.findField('CCBASF').setValue('');
									        					m_form.findField('CCABIF').setValue('');
									        					m_form.findField('CCCABF').setValue('');
										        			       
										        				
															});										            
											             }
													}										    
										    
										    
										},{										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 5',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners = {
									        					afterSelected: function(from_win, row){
									        					 	m_form.findField('des_ns_cuba').setValue(row.descrizione);
										        					m_form.findField('CCBASF').setValue(row.codice);
										        					m_form.findField('CCABIF').setValue(row.abi);
										        					m_form.findField('CCCABF').setValue(row.cab);
										        			        from_win.close();
										        				}										    
														    },
															acs_show_win_std('Ricerca banca codificata [CUBA]', 'acs_ricerca_tab_sys.php?fn=open_tab', {taid : 'CUBA'}, 1000, 400, my_listeners, 'icon-leaf-16');    
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								}								
								
								
								<?php }?>
						]
					}
					
					
					,
					
					
					{
						xtype: 'fieldset', title: 'Agenti',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
									
										<?php //write_combo_std('CCAG1', 'Agente (1)', $r['CCAG1'], acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), '', 'R'), array('transorm' => 'NO', 'flex_width' => "flex: 2") ) ?>
										
										{
								            xtype: 'combo',
											name: 'CCAG1',
											flex: 2,
											fieldLabel: 'Agente (1)',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, {name:'color'}, {name:'sosp'}, 'perc_provv', 'area_manager'],
												    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array(
												            'value'  => $r['CCAG1'],
												            'i_sosp' => 'N', 'cli_cc' => 'Y')), '', 'R'); ?>] 
											},
								            
								            value: '<?php echo trim($r['CCAG1']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            anchor: '100%',
								            queryMode: 'local',
     										minChars: 1,
     										tpl: [
                            				 '<ul class="x-list-plain">',
                                                '<tpl for=".">',
                                                '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                            				 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                            
                                                    '</tpl>'
                                             
                                            ),
									        listeners: {
									            select: function(combo, row, index) {
												   this.up('form').getForm().findField('CCPA1').setValue(row[0].data.perc_provv);
												   
													<?php if (trim($r['CCCCON']) == 'STD'){ ?>
														this.up('form').getForm().findField('CCARMA').setValue(row[0].data.area_manager);													
													<?php } ?>												   
												   								            
									            }, 
									              beforequery: function (record) {
                        	         				record.query = new RegExp(record.query, 'i');
                        	         				record.forceAll = true;
                        				 		}
									        }            
										
								        }										
										
										
										
									, {
											xtype: 'numberfield', hideTrigger:true,
											name: 'CCPA1',
											fieldLabel: '% provv.', labelAlign: 'right',
										    flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCPA1']))); ?>							
										}
									]
								}, 								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php //write_combo_std('CCAG2', 'Agente (2)', $r['CCAG2'], acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), '', 'R'), array('transorm' => 'NO', 'flex_width' => "flex: 2") ) ?>
										
										
										{
								            xtype: 'combo',
											name: 'CCAG2',
											flex: 2,
											fieldLabel: 'Agente (2)',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, 'perc_provv', {name:'color'}, {name:'sosp'}],
												    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('value'  => $r['CCAG2'], 'cli_cc' => 'Y')), '', 'R'); ?>] 
											},
								            
								            value: '<?php echo trim($r['CCAG2']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            anchor: '100%',
								            queryMode: 'local',
     										minChars: 1,
     										tpl: [
                            				 '<ul class="x-list-plain">',
                                                '<tpl for=".">',
                                                '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                            				 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                            
                                                    '</tpl>'
                                             
                                            ),
									        listeners: {
									            select: function(combo, row, index) {
												   this.up('form').getForm().findField('CCPA2').setValue(row[0].data.perc_provv);								            
									            },
									            
									            beforequery: function (record) {
                        	         				record.query = new RegExp(record.query, 'i');
                        	         				record.forceAll = true;
                        				 		}
									        }            
										
								        },
										
										
										{
											xtype: 'numberfield', hideTrigger:true,
											name: 'CCPA2',
											fieldLabel: '% provv.', labelAlign: 'right',
										    flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCPA2']))); ?>							
										}
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php //write_combo_std('CCAG3', 'Agente (3)', $r['CCAG3'], acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), '', 'R'), array('transorm' => 'NO', 'flex_width' => "flex: 2") ) ?>
										
										
										{
								            xtype: 'combo',
											name: 'CCAG3',
											flex: 2,
											fieldLabel: 'Agente (3)',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, 'perc_provv', {name:'color'}, {name:'sosp'}],
												    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('value'  => $r['CCAG3'], 'cli_cc' => 'Y')), '', 'R'); ?>] 
											},
								            
								            value: '<?php echo trim($r['CCAG3']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            anchor: '100%',
								            queryMode: 'local',
     										minChars: 1,
     										tpl: [
                            				 '<ul class="x-list-plain">',
                                                '<tpl for=".">',
                                                '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                            				 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                            
                                                    '</tpl>'
                                             
                                            ),
									        listeners: {
									            select: function(combo, row, index) {
												   this.up('form').getForm().findField('CCPA3').setValue(row[0].data.perc_provv);								            
									            }, beforequery: function (record) {
                        	         				record.query = new RegExp(record.query, 'i');
                        	         				record.forceAll = true;
                        				 		}
									        }            
										
								        },
										{
											xtype: 'numberfield', hideTrigger:true,
											name: 'CCPA3',
											fieldLabel: '% provv.', labelAlign: 'right',
										    flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCPA3']))); ?>							
										}
									]
								}
								
								<?php if (trim($r['CCCCON']) == 'STD'){ ?>
								, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [								
										{
								            xtype: 'combo',
											name: 'CCARMA',
											width : '67%',
											margin : '0 5 0 0',
											fieldLabel: 'Area manager',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, 'perc_provv'],
												    data: [
												     <?php if($cfg_mod_Gest['campo_obbligatorio'] != 'Y'){?>
        								            	 <?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('value'  => $r['CCARMA'], 'tipo' => 'area_manager')), '', 'R'); ?> 
        								            <?php }else{?> 
        								              <?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('value'  => $r['CCARMA'], 'tipo' => 'area_manager')), '', 'R', 'N', 'Y'); ?>
        								            <?php }?> 
												     ] 
											},
								            
								            value: '<?php echo trim($r['CCARMA']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            <?php if($cfg_mod_Gest['campo_obbligatorio'] != 'Y'){?>
								            	allowBlank: false 
								            <?php }else{?> 
								             	allowBlank: true 
								            <?php }?>    										
								        }
								    ]
								  }
								<?php } ?>
								
								
						]
					}
					, 	{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									items: [
										<?php write_combo_std('CCREFE', 'Referente', $r['CCREFE'], acs_ar_to_select_json(find_TA_sys('BREF', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 2.8") ) ?>,
									
									{
											name: 'CCSC1',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: 'Sconti righe',
											labelAlign : 'right', 
										    maxLength: 5, width : 145,
										    value: <?php echo j(trim(acs_u8e($r['CCSC1']))); ?>							
										}, {
											name: 'CCSC2',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC2']))); ?>							
										}, {
											name: 'CCSC3',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC3']))); ?>							
										}, {
											name: 'CCSC4',
											xtype: 'numberfield', hideTrigger:true,
										    labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC4']))); ?>							
										}
									
									]
								},		
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
									
									<?php write_combo_std('CCLIST', 'Listino', $r['CCLIST'], acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 2.8") ) ?>,
									
									{
											name: 'CCSC5',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: 'Sconti docum.',
											labelAlign : 'right', 
										    maxLength: 5, width : 145,
										    value: <?php echo j(trim(acs_u8e($r['CCSC5']))); ?>							
										}, {
											name: 'CCSC6',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC6']))); ?>							
										}, {
											name: 'CCSC7',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC7']))); ?>							
										}, {
											name: 'CCSC8',
											xtype: 'numberfield', hideTrigger:true,
										    labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC8']))); ?>							
										}
									  
									
									]
								}	,		
								
								<?php if (trim($r['CCCCON']) == 'STD'){ ?>
									 {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('GCIVES', 'Assoggettamento', $r['GCIVES'], acs_ar_to_select_json($main_module->find_TA_std('CUAE', null, 'N', 'N', null, null, null, 'N', 'Y', 'Y'), ''), array('flex_width' => " flex : 0.9") ) ?>,
										
										<?php if ($cfg_mod_Gest['gestisci_sconti_promotion'] == 'Y'){
										    
										    //recupero sconti promotion
										    $rowPromotion = get_row_sconti_promotion_by_codcli($r['GCCDCF']);
										    ?>
										    {xtype: 'hiddenfield', name: 'save_sconti_promotion',   value: 'Y'},
    										{
    											name: 'TSSC1',
    											xtype: 'numberfield', hideTrigger:true,
    											fieldLabel: 'Sc. Promotion',
    											labelAlign : 'right', 
    										    maxLength: 5, width : 145,
    										    value: <?php echo j($rowPromotion['TSSC1']); ?>							
    										},
    										{xtype: 'component', flex : 0.55}
    										
    										/*, {
    											name: 'TSSC2',
    											xtype: 'numberfield', hideTrigger:true,
    											labelWidth : 20,
    											fieldLabel: '+', labelAlign: 'right',
    										    maxLength: 5, width : 65,
    										    labelSeparator : '',
    										    value: <?php echo j($rowPromotion['TSSC2']); ?>							
    										}, {
    											name: 'TSSC3',
    											xtype: 'numberfield', hideTrigger:true,
    											labelWidth : 20,
    											fieldLabel: '+', labelAlign: 'right',
    										    maxLength: 5, width : 65,
    										    labelSeparator : '',
    										    value: <?php echo j($rowPromotion['TSSC3']); ?>							
    										}, {
    											name: 'TSSC4',
    											xtype: 'numberfield', hideTrigger:true,
    										    labelWidth : 20,
    											fieldLabel: '+', labelAlign: 'right',
    										    maxLength: 5, width : 65,
    										    labelSeparator : '',
    										    value: <?php echo j($rowPromotion['TSSC4']); ?>							
    										}	*/									
										<?php } ?>										
								    ]},
								    

									 {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('CCCATR', 'Causale vendita', $r['CCCATR'], acs_ar_to_select_json(find_TA_sys('VUCT', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 100) ) ?>,
										<?php write_combo_std('CCVALU', 'Valuta', $r['CCVALU'], acs_ar_to_select_json(find_TA_sys('VUVL', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'flex_width' => "width: 340", "allowBlank" => 'false') ) ?>,
								    ]},

								    
										
									
							    
								   , {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
									
									<?php write_combo_std('GCCATC', 'Cat. sconto', $r['GCCATC'], acs_ar_to_select_json(find_TA_sys('VUCS', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
									<?php write_combo_std('GCCAPC', 'Cat. provvigioni', $r['GCCAPC'], acs_ar_to_select_json(find_TA_sys('VUCP', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'flex_width' => "width: 340") ) ?>,
									
									]}
											
									
							<?php }?>
							
        		    <?php if(trim($request['CCCCON']) != 'STD'){   ?>
        		          , {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{xtype: 'textfield'},
							items: [
								{
                				   xtype: 'datefield'
                				   , startDay: 1 //lun.
                				   , fieldLabel: 'Validit&agrave; iniziale'
                				   , value: '<?php echo $r['CCDTVI']; ?>'
                				   , flex : 1
                				   , name: 'CCDTVI'
                				   , format: 'd/m/Y'
        				   		   , altFormats: 'Ymd|dmY'
        				           , submitFormat: 'Ymd'
                				   //, labelWidth : 110
                				   , allowBlank: false
                				  // , anchor: '-15'
                				   , listeners: {
                				       invalid: function (field, msg) {
                				       Ext.Msg.alert('', msg);}
                						}
                				}
                			  ,	<?php write_combo_std('CCDIVI', 'Divisione docum.', $r['CCDIVI'], acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 110, "labelAlign" => "right") ) ?>
							
							]}
					
					   , {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{xtype: 'textfield'},
							items: [
								{
                				   xtype: 'datefield'
                				   , startDay: 1 //lun.
                				   , fieldLabel: 'Validit� finale'
                				   , value: '<?php echo $r['CCDTVF']; ?>'
                				   , flex : 1
                				  // , labelWidth : 110
                				   , name: 'CCDTVF'
                				   , format: 'd/m/Y'
        				   		   , altFormats: 'Ymd|dmY'
                				   , submitFormat: 'Ymd'
                				   //, anchor: '-15'
                				   , allowBlank: false
                				   , listeners: {
                				       invalid: function (field, msg) {
                				       Ext.Msg.alert('', msg);}
                						}
                				}
                				
                				<?php
                				if($cfg_mod_Gest['gruppo_documenti_ob'] == 'Y')
                					$allowBlank = 'false';
                				else
                				    $allowBlank = 'true';
                				?>
                				
                			  , <?php write_combo_std('CCGRDO', 'Gruppo docum.', $r['CCGRDO'], acs_ar_to_select_json(find_TA_sys('RDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 110, "labelAlign" => "right", "allowBlank" => $allowBlank) ) ?>
							
							]},
							
							    
								   , {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
									
									<?php write_combo_std('CCCATC', 'Cat. sconto', $r['CCCATC'], acs_ar_to_select_json(find_TA_sys('VUCS', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
									<?php write_combo_std('CCCAPC', 'Cat. provvigioni', $r['CCCAPC'], acs_ar_to_select_json(find_TA_sys('VUCP', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'labelWidth' => 110, 'flex_width' => "flex : 1") ) ?>,
									
									]}
									
							<?php }?>
        		   
        		   ], listeners : {
        		   
        		   
						activate : function(subform){
							
            			    var main_form = subform.up('form');		
                            main_form.acs_actions.set_save_button(subform);
                
						
						}        		   
        		   }
        		
        		
        		}, 
        		
        	
        		
        		 <?php if(trim($request['CCCCON']) != 'STD'){ ?>
          
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            height : 470,
            title: 'Condizionamenti',
            autoScroll: true,
            //flex: 1,
            defaults:{ anchor: '-10' , labelWidth: 120},
            items: [
               {
				xtype: 'fieldcontainer',
				margin : '0 0 10 0',
				layout: {type: 'hbox', pack: 'start', align: 'stretch'},
				anchor: '-10',
				defaults:{xtype: 'textfield'},
				items: [								
					{
						name: 'CCCCON',
						fieldLabel: 'Codice',
					    labelWidth: 50, width: 130,
					   	value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,
					   	disabled : true					    
					}, {
						name: 'CCDCON',
						fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
					    value: <?php echo j(trim(acs_u8e(substr($r['CCDCON'], 0, 30)))); ?>,
					    disabled : true
				    }
					, {
						name: 'CCDCON_ART',
						fieldLabel: 'Art.', flex: 0.8, labelAlign: 'right', labelWidth: 40,
						value: <?php echo j(trim(acs_u8e(substr($r['CCDCON'], 30, 30)))); ?>,
						disabled : true									
					  }
				
					]
				}, { 
					xtype: 'fieldcontainer',
					layout: 'hbox',
					flex: 1,
					items: [
						<?php write_combo_std('CCTCON', 'Tipo condizione', $r['CCTCON'], acs_ar_to_select_json(find_TA_sys('TCOM', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
						<?php write_combo_std('CCFGR1', 'Tipologia ordini 1', $r['CCFGR1'], acs_ar_to_select_json($main_module->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
						
				
				]},
            		
					{ 
				xtype: 'fieldcontainer',
				layout: 'hbox',
				flex: 1,
				items: [
				<?php write_combo_std('CCCPRO', 'Compos. promo.', $r['CCCPRO'], acs_ar_to_select_json($main_module->find_TA_std('CCT01', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
				<?php write_combo_std('CCFGR2', 'Tipologia ordini 2', $r['CCFGR2'], acs_ar_to_select_json($main_module->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
				
				
				]},
					
				{ 
    				xtype: 'fieldcontainer',
    				layout: 'hbox',
    				flex: 1,
    				items: [
    				<?php write_combo_std('CCEINT', 'Elabor. interattiva', $r['CCEINT'], acs_ar_to_select_json($main_module->find_TA_std('CCT03', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
    				<?php write_combo_std('CCFGR3', 'Tipologia ordini 3', $r['CCFGR3'], acs_ar_to_select_json($main_module->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>
				
				]},
				{ 
    				xtype: 'fieldcontainer',
    				layout: 'hbox',
    				flex: 1,
    				items: [
    					<?php write_combo_std('CCSCIM', 'Sconto a importo', $r['CCSCIM'], acs_ar_to_select_json($main_module->find_TA_std('CCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
    					<?php write_combo_std('CCAZSA', 'Azzera % sc. art.', $r['CCAZSA'], acs_ar_to_select_json($main_module->find_TA_std('CCT04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
    			
				]},
				
				{ 
					xtype: 'fieldcontainer',
					layout: 'hbox',
					flex: 1,
					items: [
						
						<?php write_combo_std('CCPRZO', 'Prezzo obbligat.', $r['CCPRZO'], acs_ar_to_select_json($main_module->find_TA_std('CCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
						<?php write_combo_std('CCAZSD', 'Azzera % sc. doc.', $r['CCAZSD'], acs_ar_to_select_json($main_module->find_TA_std('CCT04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
				]},
				{ 
    				xtype: 'fieldcontainer',
    				layout: 'hbox',
    				flex: 1,
    				items: [
    					
					{xtype : 'component', flex : 1}
					, <?php write_combo_std('CCAZPR', 'Azzera provvigioni', $r['CCAZPR'], acs_ar_to_select_json($main_module->find_TA_std('CCT04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
			
				]},
				
				{
					xtype: 'fieldset', title: 'Seleziona destinazioni',
					layout: {type: 'vbox', pack: 'start', align: 'stretch'},
					anchor: '-10',
					defaults:{xtype: 'textfield'},
					items: [
					
					<?php 
					
					global $id_ditta_default;
					$where = "";
					if($cfg_mod_Gest["sel_destinazioni"] == 'P')
					    $where .= "AND SUBSTRING(TAREST, 163, 1) = 'P'";
					
					
					$sql = "SELECT SUBSTRING(TAREST, 91, 60) AS LOCA
					        FROM {$cfg_mod_Gest['file_tab_sys']} TA
				            WHERE TA.TADT = '{$id_ditta_default}' AND TA.TAID='VUDE' AND TA.TACOR1 = '{$r['GCCDCF']}'
                            {$where}				            
                            AND TA.TATP <>'S' AND TANR = ? ORDER BY TANR";
					
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					
					
					?>
					
					
					{ 
        				xtype: 'fieldcontainer',
        				layout: 'hbox',
        				flex: 1,
        				items: [
        				<?php write_combo_std('CCDES1', 'Destinazione 1', $r['CCDES1'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $r['GCCDCF'], null, null, null, 0, $where, 'Y', 'Y'), ''), array('flex_width' => "width: 330", 'labelWidth' => 110) ) ?>,
        				<?php if(trim($r['CCDES1']) != ''){
        				    $result = db2_execute($stmt, array($r['CCDES1']));
        				    $row = db2_fetch_assoc($stmt);?>
        				    
        				    {xtype : 'displayfield',
        				    margin : '0 0 0 20',
        				    value : <?php echo j($row['LOCA'])?>,
        				    flex : 1
        				    }
        				    
        				    <?php }?>
    				]},
    						{ 
        				xtype: 'fieldcontainer',
        				layout: 'hbox',
        				flex: 1,
        				items: [
        				
        				<?php write_combo_std('CCDES2', 'Destinazione 2', $r['CCDES2'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $r['GCCDCF'], null, null, null, 0, $where, 'Y', 'Y'), ''), array('flex_width' => "width: 330", 'labelWidth' => 110) ) ?>,
        				  <?php if(trim($r['CCDES2']) != ''){
        				    $result = db2_execute($stmt, array($r['CCDES2']));
        				    $row = db2_fetch_assoc($stmt);?>
        				    
        				    {xtype : 'displayfield',
        				    margin : '0 0 0 20',
        				    value : <?php echo j($row['LOCA'])?>,
        				    flex : 1
        				    }
        				    
        				    <?php }?>
    			     ]},
    						{ 
        				xtype: 'fieldcontainer',
        				layout: 'hbox',
        				flex: 1,
        				items: [
        				<?php write_combo_std('CCDES3', 'Destinazione 3', $r['CCDES3'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $r['GCCDCF'], null, null, null, 0, $where, 'Y', 'Y'), ''), array('flex_width' => "width: 330", 'labelWidth' => 110) ) ?>,
        				 <?php if(trim($r['CCDES3']) != ''){
        				    $result = db2_execute($stmt, array($r['CCDES3']));
        				    $row = db2_fetch_assoc($stmt);?>
        				    
        				    {xtype : 'displayfield',
        				    margin : '0 0 0 20',
        				    value : <?php echo j($row['LOCA'])?>,
        				    flex : 1
        				    }
        				    
        				    <?php }?>
    				]},
    						{ 
        				xtype: 'fieldcontainer',
        				layout: 'hbox',
        				flex: 1,
        				items: [
        				<?php write_combo_std('CCDES4', 'Destinazione 4', $r['CCDES4'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $r['GCCDCF'], null, null, null, 0, $where, 'Y', 'Y'), ''), array('flex_width' => "width: 330", 'labelWidth' => 110) ) ?>,
        				  <?php if(trim($r['CCDES4']) != ''){
        				    $result = db2_execute($stmt, array($r['CCDES4']));
        				    $row = db2_fetch_assoc($stmt);?>
        				    
        				    {xtype : 'displayfield',
        				    margin : '0 0 0 20',
        				    value : <?php echo j($row['LOCA'])?>,
        				    flex : 1
        				    }
        				    
        				    <?php }?>
    				]},	
					{ 
        				xtype: 'fieldcontainer',
        				layout: 'hbox',
        				flex: 1,
        				items: [
        				<?php write_combo_std('CCDES5', 'Destinazione 5', $r['CCDES5'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $r['GCCDCF'], null, null, null, 0, $where, 'Y', 'Y'), ''), array('flex_width' => "width: 330", 'labelWidth' => 110) ) ?>,
    				      <?php if(trim($r['CCDES5']) != ''){
        				    $result = db2_execute($stmt, array($r['CCDES5']));
        				    $row = db2_fetch_assoc($stmt);?>
        				    
        				    {xtype : 'displayfield',
        				    margin : '0 0 0 20',
        				    value : <?php echo j($row['LOCA'])?>,
        				    flex : 1
        				    }
        				    
        				    <?php }?>
    				
    				]},		
							
					]
				}		
            
            ], listeners : {
            
            
						activate : function(subform){
							
            			    var main_form = subform.up('form');		
                            main_form.acs_actions.set_save_button(subform);
                
						
						}  
            
            }
            
            }
            <?php }?>
        		]}
			
			],  //fine items form
			buttons: [
			
			  <?php if(trim($request['CCCCON']) == 'STD'){ ?>
				{
				 text : 'Listino a punti',
	             scale: 'medium',			                 
	             iconCls: 'icon-button_blue_play-24',
	             handler : function() {
	                 acs_show_win_std('Listino punti', 'acs_anag_cli_listino_punti.php?fn=open_panel', {cdcf : <?php echo j($r['GCCDCF']); ?>}, 1300, 500, null, 'icon-button_blue_play-16');	          	                	   
	         		
				 } //handler function()
				 
				 },
				 { xtype: 'tbfill'},
				<?php }?>
			
			<?php if($request['from_todo'] != 'Y'){?>
			   {
	            text: 'Salva',
	            iconCls: 'icon-save-24', scale: 'medium',
	            itemId : 'main_form_save_b',
	            width : 120,
	            handler: function() {
	            
	            	var panel_form = this.up('form'),
	            		form = panel_form.getForm(),           	
	            		form_values = form.getValues();
	                    loc_win = this.up('window');
	            	
	            	var tabPanel = loc_win.down('tabpanel');
					var activeTab = tabPanel.getActiveTab();
					var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
					var sub_form = activeTab.getForm();
					if(sub_form.isValid()){
                        if(activeTabIndex < tabPanel.items.length - 1){
                           tabPanel.setActiveTab(activeTabIndex + 1);
                        } else {
                            panel_form.acs_actions.exe_form_submit(panel_form); 
    		            } 
	            	}
					      	
	            	
	            } //handler
	        } //salva
	        <?php }?>
	        ]            
			
			
		 , acs_actions: {
		 
		   set_save_button : function(subform){
		   
		 		var tabPanel = subform.up('tabpanel');
				var activeTab = tabPanel.getActiveTab();
				var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
			    var main_form = tabPanel.up('form');	
		              
                if(activeTabIndex < tabPanel.items.length - 1){
                   main_form.down('#main_form_save_b').setText('Avanti');
                } else {
                   main_form.down('#main_form_save_b').setText('Salva');
	            } 
            	            	
						
		   
		   }
		 
	       , exe_form_submit: function(panel_form){
	    	
    	    	var form = panel_form.getForm(),
    		        loc_win = panel_form.up('window'),
    		        tabPanel = loc_win.down('tabpanel');
    	    	Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_cc',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>,
						        	GCDEFA : <?php echo j(trim($r['GCDEFA'])); ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        	Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            if(jsonData.success == false){
						            	acs_show_msg_error(jsonData.msg_error);
						            }else{
						            	loc_win.fireEvent('afterEditRecord', loc_win);
						            }
						
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });		
	    	
	    	}}	
				
        }
        
         
        
       
<?php
} //write_new_form_COMM


function write_new_form_FIDO($p = array(), $request = array()){
	global $conn, $cfg_mod_Gest;

	if ($request['mode'] == 'EDIT'){
		//recupero la riga da modificare
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($request['CCDT'], $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));

		$ret = array();
		$r = db2_fetch_assoc($stmt);

	} else {
		//NUOVA CONDIZIONE COMMERCIALE
		$r = array(	'CCDT' => $request['CCDT'],
				'CCPROG' => $request['CCPROG'],
				'CCSEZI' => $request['CCSEZI'],
				'CCCCON' => $request['CCCCON'],
				'CCDCON' => $request['CCDCON'],
		);
	}

	global $s, $main_module;
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            disabled: false, //true,
            autoScroll: true,
            
            flex: 1,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [
            		{xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($r['CCDT']); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j($r['CCSEZI']); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($r['CCPROG']); ?>},
            		{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j($r['CCCCON']); ?>},
            		{xtype: 'hiddenfield', name: 'CCDCON', value: <?php echo j($r['CCDCON']); ?>},
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							    value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,
							    disabled: true
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    maxLength: 5,
							    value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    disabled: true							
							}
							]
						},
						
						{
						xtype: 'fieldcontainer',
						layout: {type: 'vbox'},
						//anchor: '-10',
						defaults:{xtype: 'textfield' , width : 450, labelWidth : 220},
						items: [								
					      <?php write_numberfield_std('CCIMFD', 'Importo fido', trim($r['CCIMFD'])) ?>		
						, <?php write_datefield_std('CCDTVI', 'Data validit&agrave; iniziale', trim($r['CCDTVI'])) ?>
						, <?php write_datefield_std('CCDTVF', 'Data validit&agrave; finale', trim($r['CCDTVF'])) ?>
					    , <?php write_combo_std('CCBLOC', 'Blocco ordini', $r['CCBLOC'], acs_ar_to_select_json($main_module->find_TA_std('CCT06', null, 'N', 'N', null, null, null, 'N', 'Y'), '')) ?>		
					    , <?php write_combo_std('CCLEGA', 'Esposiz. sui collegati', $r['CCLEGA'], acs_ar_to_select_json($main_module->find_TA_std('CCT09', null, 'N', 'N', null, null, null, 'N', 'Y'), '')) ?>	
						, <?php write_combo_std('CCCESU', 'Check esubero su avanzamento ordini', $r['CCCESU'], acs_ar_to_select_json($main_module->find_TA_std('CCT07', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('labelWidth' => '220', 'flex' => '1')) ?>	
						
						, <?php write_combo_std('CCCAL1', 'Causale limitazione esposizione 1', $r['CCCAL1'], acs_ar_to_select_json(find_TA_sys('VUCT', null, null, null, null, null, 0, '', 'Y', 'Y'), '', true, 'N', 'Y'), array('labelWidth' => '220', 'flex' => '1')) ?>	
						, <?php write_combo_std('CCCAL2', 'Causale limitazione esposizione 2', $r['CCCAL2'], acs_ar_to_select_json(find_TA_sys('VUCT', null, null, null, null, null, 0, '', 'Y', 'Y'), '', true, 'N', 'Y'), array('labelWidth' => '220', 'flex' => '1')) ?>	
						, <?php write_numberfield_std('CCPINC', '% inclusione', trim($r['CCPINC'])) ?>	
							
							]
						}             
            
						
					
			],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');	            	
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_fido',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            
						            loc_win.fireEvent('afterEditRecord', loc_win);
						            
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],             
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
        }
<?php
} //write_new_form_FIDO

function write_new_form_LOGIS($p = array(), $request = array()){
    global $conn, $cfg_mod_Gest;
	global $s, $main_module;

	if ($request['mode'] == 'EDIT'){
		//recupero la riga da modificare
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} WHERE GCPROG=?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($request['rec_id']));

		$ret = array();
		$r = db2_fetch_assoc($stmt);
		$r['GCVT1_D'] = $main_module->decod_cliente($r['GCVT1'], 'F');
		$r['GCVT2_D'] = $main_module->decod_cliente($r['GCVT2'], 'F');
		
		$decod1 = get_TA_sys('VUDE', $r['GCDVT1'], $r['GCVT1']);
		$r['GCDVT1_D'] = "[".trim($r['GCDVT1'])."] ".trim($decod1['text']);
		
		$decod = get_TA_sys('VUDE', $r['GCDVT2'], $r['GCVT2']);
		$r['GCDVT2_D'] = "[".trim($r['GCDVT2'])."] ".trim($decod['text']);
		
	} else {
		//NUOVA
		$r = array(
				//'GCPROG' => $request['CCPROG']
		);
	}
	
		
	?>
        {
            xtype: 'form',
            itemId: 'form_LOGIS',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Configurazione/logistica',
            disabled: true,
            autoScroll: true,
            
			<?php
			 if ($request['mode'] == 'EDIT')
			 	echo "disabled: false, ";
			 else 
			 	echo "disabled: true, "; 
			?>           
            
            flex: 1,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [
            		{xtype: 'hiddenfield', name: 'GCPROG', value: <?php echo j($r['GCPROG']); ?>},            
					/*{
								name: 'GCDCON',
								fieldLabel: '', flex: 1,
							    value: <?php echo j(trim(acs_u8e($r['GCDCON']))); ?>,
							    disabled: true							
					}*/
					 {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
								<?php write_combo_std('GCLING', 'Lingua documenti', $r['GCLING'], acs_ar_to_select_json(find_TA_sys('VULN', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R') ) ?>					
                               ,<?php write_combo_std('GCPORT', 'Porto', $r['GCPORT'], acs_ar_to_select_json(find_TA_sys('CUPO', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>					
							]
						  },
						  
						 {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
							   <?php if($cfg_mod_Gest['nazione_operativa_ob'] == 'Y')
							         $allowBlank = 'false';
							    else $allowBlank = 'true';
							    
							    write_combo_std('GCNAZO', 'Nazione operativa', $r['GCNAZO'], acs_ar_to_select_json(find_TA_sys('BNAZ', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('allowBlank' => $allowBlank) ) ?>   ,<?php write_combo_std('GCSPED', 'Spedizioni', $r['GCSPED'], acs_ar_to_select_json(find_TA_sys('CUSP', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>					
							]
						  },
						  
						   {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
							   <?php write_checkboxgroup_std('GCFMNC', 'Forniture MTO', $r['GCFMNC'], array('Y' => 'Si') ) ?>
                              ,<?php write_combo_std('GCTRAS', 'Trasporto', trim($r['GCTRAS']), acs_ar_to_select_json(find_TA_sys('VUTR', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>					
							]
						  },
						   {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
							   {
                        			xtype: 'combo',
                        			name: 'GCVT1',
                        			fieldLabel: 'Vettore 1',
                        			allowBlank : true,	
                        			minChars: 2,	
                        			anchor: '-15',
                        			value : <?php echo j(trim($r['GCVT1'])); ?>,		
                        			store: {
                                    	pageSize: 1000,
                                    	proxy: {
                        		            type: 'ajax',
                        		            url : <?php echo acs_je('acs_get_select_json.php?select=search_fornitori'); ?>,
                        		            reader: {
                        		                type: 'json',
                        		                root: 'root',
                        		                totalProperty: 'totalCount'
                        		            }
                        		        },       
                        				fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
                                    },
                                    valueField: 'cod',                        
                                    displayField: 'descr',
                                    typeAhead: false,
                                    hideTrigger: true,
                                    anchor: '100%',
                                    listConfig: {
                                        loadingText: 'Searching...',
                                        emptyText: 'Nessun fornitore trovato',
                                        // Custom rendering template for each item
                                        getInnerTpl: function() {
                                            return '<div class=\"search-item\">' +
                                                '<h3><span>{descr}</span>[{cod}]</h3>' +
                         						' {out_ind}<br/>' +
                                                ' {out_loc}' +
                                            '</div>';
                                        	}                
                                        },
                        	            pageSize: 1000,
                        	              listeners: {
                                           afterrender: function(compo){
                                         	    var data = [{cod: <?php echo j($r['GCVT1'])?>, descr: <?php echo j(trim($r['GCVT1_D'])) ?>}
                                                 ];
                                                compo.store.loadData(data);
                                         		compo.setValue(<?php echo j(trim($r['GCVT1'])); ?>);
                                         	}
     									}
                        	     },
							   {
                        			xtype: 'combo',
                        			name: 'GCVT2',
                        			fieldLabel: 'Vettore 2',
                        			allowBlank : true,	
                        			labelAlign : 'right',
                        			minChars: 2,	
                        			anchor: '-15',
                        			value : <?php echo j(trim($r['GCVT2'])); ?>,		
                        			store: {
                                    	pageSize: 1000,
                                    	proxy: {
                        		            type: 'ajax',
                        		            url : <?php echo acs_je('acs_get_select_json.php?select=search_fornitori'); ?>,
                        		            reader: {
                        		                type: 'json',
                        		                root: 'root',
                        		                totalProperty: 'totalCount'
                        		            }
                        		        },       
                        				fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
                                    },
                                    valueField: 'cod',                        
                                    displayField: 'descr',
                                    typeAhead: false,
                                    hideTrigger: true,
                                    anchor: '100%',
                                    listConfig: {
                                        loadingText: 'Searching...',
                                        emptyText: 'Nessun fornitore trovato',
                                        // Custom rendering template for each item
                                        getInnerTpl: function() {
                                            return '<div class=\"search-item\">' +
                                                '<h3><span>{descr}</span>[{cod}]</h3>' +
                         						' {out_ind}<br/>' +
                                                ' {out_loc}' +
                                            '</div>';
                                        	}                
                                        },
                        	            pageSize: 1000,
                        	            listeners: {
                                         	afterrender: function(compo){
                                         	    var data = [{cod: <?php echo j($r['GCVT2'])?>, descr: <?php echo j(trim($r['GCVT2_D'])) ?>}
                                                 ];
                                                compo.store.loadData(data);
                                         		compo.setValue(<?php echo j(trim($r['GCVT2'])); ?>);
                                         	}
     									}
                        	          
                        
                                },
							  
							]
						  },
						  
						   {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120},
							items: [
							     
							{   xtype: 'textfield',
            				    name: 'GCDVT1',
                                hidden : true,
                                value : <?php echo j($r['GCDVT1']); ?>,
            				},
                            {   xtype: 'textfield',
            				    name: 'f_destinazione1',
            				    fieldLabel: 'Destin. vettore 1',
            				    flex : 1,
                             	value : <?php echo j($r['GCDVT1_D']); ?>,
                                readOnly : true
            				},
            				{										  
							xtype: 'displayfield',
							editable: false,
							fieldLabel: '',
							padding: '0 0 0 5',
						    value: <?php echo j("<img src=" . img_path("icone/48x48/sub_red_delete.png") . " width=16>"); ?>,
						    listeners: {
							            render: function( component ) {
							                m_form = this.up('form').getForm();
							                component.getEl().on('dblclick', function( event, el ) {
											
		        					 		m_form.findField('f_destinazione1').setValue('');
				        					m_form.findField('GCDVT1').setValue('');
						        			       
						        				
											});										            
							             }
									}										    
							 },
            				{										  
            				  xtype: 'displayfield',
            				  fieldLabel: '',
            				  padding: '0 0 0 5',
            				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
            				  listeners: {
            							render: function( component ) {
            							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('GCDVT1').setValue(record_selected.cod);	
												form.findField('f_destinazione1').setValue(record_selected.denom);
                                                from_win.close();
												}										    
									};
									
									var vettore1 = form.findField('GCVT1').value;
									
									acs_show_win_std('Seleziona destinazione', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente: vettore1, type : 'D'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
						},	
            				{   xtype: 'textfield',
            				    name: 'GCDVT2',
                                hidden : true,
                                value : <?php echo j($r['GCDVT2']); ?>,
            				},
                            {   xtype: 'textfield',
            				    name: 'f_destinazione',
            				    fieldLabel: 'Destin. vettore 2',
            				    labelAlign : 'right',
                             	//labelWidth : 120,
                             	flex : 1,
                             	value : <?php echo j($r['GCDVT2_D']); ?>,
                                readOnly : true
            				},
            				{										  
							xtype: 'displayfield',
							editable: false,
							fieldLabel: '',
							padding: '0 0 0 5',
						    value: <?php echo j("<img src=" . img_path("icone/48x48/sub_red_delete.png") . " width=16>"); ?>,
						    listeners: {
							            render: function( component ) {
							                m_form = this.up('form').getForm();
							                component.getEl().on('dblclick', function( event, el ) {
											
		        					 		m_form.findField('f_destinazione').setValue('');
				        					m_form.findField('GCDVT2').setValue('');
						        			       
						        				
											});										            
							             }
									}										    
							 },
            			    {										  
            				  xtype: 'displayfield',
            				  fieldLabel: '',
            				  padding: '0 0 0 5',
            				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
            				  listeners: {
            							render: function( component ) {
            							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('GCDVT2').setValue(record_selected.cod);	
												form.findField('f_destinazione').setValue(record_selected.denom);
                                                from_win.close();
												}										    
									};
									
									var vettore2 = form.findField('GCVT2').value;
									
									acs_show_win_std('Seleziona destinazione', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente: vettore2, type : 'D'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
						}
							
							]
						  },
						  {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120},
							items: [
							
								 <?php write_combo_std('GCCITI', 'Itinerario', trim($r['GCCITI']), acs_ar_to_select_json(find_TA_sys('BITI', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array("allowBlank" => 'false', 'labelWidth' => 120, 'flex_width' => "flex: 0.95") ) ?>					
							    ,{	xtype: 'displayfield',
									editable: false,
									fieldLabel: '',
									margin: '0 5 0 5',
									width :20,
									<?php 
									$row = get_TA_sys('ITAL', null, $request['gccdcf']);
									if($row != false && count($row) > 0){ ?>
										style: 'background : #A0DB8E;', 
									<?php }?>
									value: <?php echo j("<img src=" . img_path("icone/48x48/gear.png") . " width=16>"); ?>,
								    listeners: {
								            render: function( component ) {
								              Ext.QuickTips.register({
                                                    target: component.getEl(),
                                                    text: 'Visualizza ITAL',
                                                    enabled: true,
                                                    showDelay: 20,
                                                    trackMouse: true,
                                                    autoShow: true
                                                  });
								            
								            	m_form = this.up('form').getForm();
								                component.getEl().on('dblclick', function( event, el ) {
								                
								                acs_show_win_std('[ITAL] Itinerario alternativo', 'acs_tab_sys_ITAL.php?fn=open_panel', {taid: 'ITAL', tacor1 : <?php echo j($request['gccdcf']); ?>}, 1200, 400,
                    	        					null, 'icon-tag-16');	          	                	                
								                
												
												});
												
												
												
								             }
										}										    
										    
										    
										}
										
							 <?php if($cfg_mod_Gest['gest_azienda_collegata']  == 'Y'){?>
							    ,{
                        			xtype: 'combo',
                        			name: 'GCANCO',
                        			fieldLabel: 'Azienda collegata',
                        			labelAlign : 'right',
                        			allowBlank : true,	
                        			minChars: 2,	
                        			anchor: '-15',
                        			value : <?php echo j(trim($r['GCANCO'])); ?>,		
                        			store: {
                                    	pageSize: 1000,
                                    	proxy: {
                        		            type: 'ajax',
                        		            url : <?php echo acs_je('acs_get_select_json.php?select=search_cli_anag'); ?>,
                        		            reader: {
                        		                type: 'json',
                        		                root: 'root',
                        		                totalProperty: 'totalCount'
                        		            }
                        		        },       
                        				fields: ['cod', 'descr'],		             	
                                    },
                                    valueField: 'cod',                        
                                    displayField: 'cod',
                                    typeAhead: false,
                                    hideTrigger: true,
                                    anchor: '100%',
                                    listConfig: {
                                        loadingText: 'Searching...',
                                        emptyText: 'Nessun cliente trovato',
                                        // Custom rendering template for each item
                                        getInnerTpl: function() {
                                            return '<div class="search-item">' +
                                                '<h3><span>{descr}</span></h3>' +
                                                '[{cod}]' + 
                                            '</div>';
                                        	}                
                                        },
                        	            pageSize: 1000,
                        	            listeners : {
                            				'blur': function(field) {
                      				    		var value = field.value;
                      				    		console.log(value);
                      				    		       
                      				    		    Ext.Ajax.request({
                    							        url        : 'acs_panel_ins_new_anag.php?fn=check_azienda_collegata',
                     		                            timeout: 2400000,
                    							        method     : 'POST',
                    							        waitMsg    : 'Data loading',
                    	 								jsonData:  {
                    	 								     cliente : value
                    	 								 },  
                    							        success : function(result, request){
                    										jsonData = Ext.decode(result.responseText);
                     		                                if(jsonData.success == false){
                     		                                   acs_show_msg_error(jsonData.msg_error);
                     		                                   return;
                     		                                }
                    	 		
                    							        },
                    							        failure    : function(result, request){
                    							            Ext.Msg.alert('Message', 'No data to be loaded');
                    							        }
                    							    });
	                      				    	
                      				    		
                                		}
                                		}
                        
                                }
                                <?php }else{?>
                               , {xtype : 'component', flex : 1}
                                <?php }?>
							]},
							
					   <?php if($cfg_mod_Gest["destinazione_tipologia"] == 'Y'){ ?>
    					   { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
    						defaults:{labelWidth: 120},						
    						items: [
    						  <?php write_combo_std('GCFLG1', 'Dest. x tipologia', $r['GCFLG1'], acs_ar_to_select_json($main_module->find_TA_std('DTIP', null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y'), array('flex_width' => "width: 235")) ?>					
        					, <?php write_combo_std('GCFLG2', '', $r['GCFLG2'], acs_ar_to_select_json($main_module->find_TA_std('DTIP', null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y'),  array('flex_width' => "width: 105", "margin" => "margin : '0 0 0 5',") ) ?>	
    					    , <?php write_combo_std('GCREDO', 'Refer. order entry', $r['GCREDO'], acs_ar_to_select_json(find_TA_sys('BREF', null, null, null, null, null, 0, '', 'Y', 'Y'), '', true, 'N', 'Y'),  array('labelAlign' => 'right')) ?>	
    					]},
    					<?php }?>
						  
						   {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
								 <?php write_combo_std('GCSEDB', 'Variante distinta', $r['GCSEDB'], acs_ar_to_select_json(find_TA_sys('PUVN', null, null, '?S5', null, null, 0, '', 'Y', 'Y'), '', 'R') ) ?>
								,<?php write_combo_std('GCCFGC', 'Variante 1', $r['GCCFGC'], acs_ar_to_select_json(find_TA_sys('PUVN', null, null, '?CC', null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>
							]
						  },
						  {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
								 <?php write_combo_std('GCCFG1', 'Variante 2', $r['GCCFG1'], acs_ar_to_select_json(find_TA_sys('PUVN', null, null, '?CC', null, null, 0, '', 'Y', 'Y'), '', 'R') ) ?>
								,<?php write_combo_std('GCCFG2', 'Variante 3', $r['GCCFG2'], acs_ar_to_select_json(find_TA_sys('PUVN', null, null, '?CC', null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>
							]
						  },
						  {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
								 <?php write_combo_std('GCCFG3', 'Variante 4', $r['GCCFG3'], acs_ar_to_select_json(find_TA_sys('PUVN', null, null, '?CC', null, null, 0, '', 'Y', 'Y'), '', 'R') ) ?>
								,<?php write_combo_std('GCCFG4', 'Variante 5', $r['GCCFG4'], acs_ar_to_select_json(find_TA_sys('PUVN', null, null, '?CC', null, null, 0, '', 'Y', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>
							]
						  }
					
					
										
					
					, {
						xtype: 'fieldset', title: 'Regole di invio',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						
						
						{
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
								<?php write_combo_std('GCICON', 'Regola invio confer.', $r['GCICON'], acs_ar_to_select_json($main_module->find_sys_TA('BCKE', array('des2' => 'Y')), '', 'R') ) ?>						
								,<?php write_combo_std('GCECON', 'Emissione conferma', $r['GCECON'], acs_ar_to_select_json($main_module->find_TA_std('GCT01', null, 'N', 'N', null, null, null, 'N', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>
							]
						  },
						  {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120, flex: 1},
							items: [
								<?php write_combo_std('GCIFAT', 'Regola invio fatture',  $r['GCIFAT'], acs_ar_to_select_json(find_TA_sys('BCKD', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R') ) ?>
								,<?php write_combo_std('GCCONA', 'Conferma autom.', $r['GCCONA'], acs_ar_to_select_json($main_module->find_TA_std('GCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), '', 'R'), array('labelAlign' => 'right') ) ?>
							]
						  },
						
						 {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 120},
							items: [
								 <?php write_combo_std('GCIPSP', 'Regola invio piani di spedizione',  $r['GCIPSP'], acs_ar_to_select_json(find_TA_sys('BCKP', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R'), array('flex_width' => 'width : 330') ) ?>
							]
						  }
							
						  
						 
						
						]
					}
					
					, {
						xtype: 'fieldset', title: 'Informazioni di scarico',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 30, xtype: 'textfield'},
						items: [
						  {
							xtype: 'fieldcontainer',
							layout: {type: 'hbox', pack: 'start', align: 'stretch'},
							anchor: '-10',
							defaults:{labelWidth: 30, flex: 1, labelAlign: 'top'},
							items: [
								<?php write_combo_std('GCGGS1', 'Lun', $r['GCGGS1'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS2', 'Mar', $r['GCGGS2'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>													 
							  , <?php write_combo_std('GCGGS3', 'Mer', $r['GCGGS3'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS4', 'Gio', $r['GCGGS4'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS5', 'Ven', $r['GCGGS5'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>
							  , <?php write_combo_std('GCGGS6', 'Sab', $r['GCGGS6'], acs_ar_to_select_json(ar_giorno_scarico(), '', 'R'), array() ) ?>

							]
						  }, 
						  
						<?php write_textfield_std('GCNOSC', 'Note', trim($r['GCNOSC']), 30 ) ?>
						
						]
					}					
					
			],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');
	            	var tabPanel = loc_win.down('tabpanel');
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_logis',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            var record = jsonData.record;
						            tabpanel_load_record_in_form(tabPanel, record);						            
						            
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						            						
									tabPanel.acs_actions.exe_afterSave(loc_win);															            
						            
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],             
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
        }
<?php
} //write_new_form_LOGIS


if ($_REQUEST['fn'] == 'exe_sos_att'){
    
    $sosp = trim($m_params->row->sosp);
    
    if($sosp == "")
        $new_value = "S";
    else
        $new_value = "";
            
            if(trim($m_params->row->GCTPAN) == 'DES'){
                $ar_upd1 = array();
                $ar_upd1['TAUSUM'] = $auth->get_user();
                $ar_upd1['TADTUM'] = oggi_AS_date();
                $ar_upd1['TATP'] = $new_value;
                
                $sql_ta = "UPDATE {$cfg_mod_Gest['file_tab_sys']} TA
                        SET " . create_name_field_by_ar_UPDATE($ar_upd1) . "
                        WHERE  RRN(TA) ='{$m_params->row->rrn_ta}'";
                
                $stmt_ta = db2_prepare($conn, $sql_ta);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_ta, $ar_upd1);
           
            }else{
                
                //Su anagrafica cliente
                
                $ar_upd = array();
                $ar_upd['GCUSUM'] = $auth->get_user();
                $ar_upd['GCDTUM'] = oggi_AS_date();
                $ar_upd['GCSOSP'] = $new_value;
                
                $sql_gc = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE GCPROG = '{$m_params->row->liv_cod}'";
                
                $stmt_gc = db2_prepare($conn, $sql_gc);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_gc, $ar_upd);
                
                $ar_upd_cf = array();
                $ar_upd_cf['CFUSUM'] = $auth->get_user();
                $ar_upd_cf['CFDTUM'] = oggi_AS_date();
                $ar_upd_cf['CFFLG3'] = $new_value;
                $ar_upd_cf['CFFINT'] = "";
                
                $sql_cf = "UPDATE {$cfg_mod_Gest['file_anag_cli']} CF
                SET " . create_name_field_by_ar_UPDATE($ar_upd_cf) . "
                WHERE CFDT = '{$id_ditta_default}' AND CFCD = '{$m_params->row->GCCDCF}'";
                
                $stmt_cf = db2_prepare($conn, $sql_cf);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_cf, $ar_upd_cf);
                
                //call AGG tramite RI
                //NUOVA VERSIONE ATTRAVERSO RI
                $sh = new SpedHistory();
                $return_RI = $sh->crea(
                    'pers',
                    array(
                        "messaggio"	=> 'AGG_ANAG_GL',
                        "vals" => array(
                            "RIPROG" => $m_params->row->GCPROG
                        )
                    )
                );
                
            }
            
            
            $ret = array();
            $ret['new_value'] = $new_value;
            $ret['success'] = true;
            echo acs_je($ret);
            exit;
            
            
}

if($_REQUEST['fn'] == 'exe_check_abicab'){
    
    $ret = array();
   
    $abi = sprintf("%05s", $m_params->form_values->CCABI);
    $cab = sprintf("%05s", $m_params->form_values->CCCAB);
    $banca = $m_params->form_values->CCBANC;
    if(trim($banca) != '')
        $sql_where = " AND TANR = '{$banca}'";
    else{
        $sql_where = " AND SUBSTRING(TAREST, 61, 5) = '{$abi}' AND SUBSTRING(TAREST, 66, 5) = '{$cab}'";
    }

    $sql = "SELECT SUBSTRING(TAREST, 61, 5) AS ABI, SUBSTRING(TAREST, 66, 5) CAB, TANR,
            TADESC, TADES2
            FROM {$cfg_mod_Gest['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}' AND TAID = 'CUBA'
            {$sql_where}    
            ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if($row != false && count($row) > 0){
        if(trim($banca) == ''){
            $ret['CCBANC']   = trim($row['TANR']);
            $ret['des_cuba'] = trim($row['TADESC']).trim($row['TADES2']);
            $ret['success']  = true;
        }else{
            if($row['ABI'] != trim($abi) || $row['CAB'] != trim($cab)){
                $ret['success'] = false;
                $ret['msg_error'] = "ABI/CAB non corrispondono a quelli del codice banca";
            }
            
        }
    }else{
        $ret['CCBANC']   = "";
        $ret['des_cuba'] = "Abi/Cab non acquisito";
        $ret['success']  = true;
    }
 
   
    
    echo acs_je($ret);
    exit;
    
}

if($_REQUEST['fn'] == 'exe_check_iban'){
    
    $ret = array();
    
    $iban = trim($m_params->form_values->CCIBAN);
    $abi = substr($iban, 5, 5);
    $cab = substr($iban, 10, 5);
    
    if(!is_numeric($abi) || !is_numeric($cab)){
        
        $ret['msg_error'] = "IBAN non correttamente formattato";
        $ret['success']  = false;
       
    }else{
        
    
    $sql_where = " AND SUBSTRING(TAREST, 61, 5) = '{$abi}' AND SUBSTRING(TAREST, 66, 5) = '{$cab}'";
    
        
    $sql = "SELECT TANR, TADESC, TADES2
            FROM {$cfg_mod_Gest['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}' AND TAID = 'CUBA'
            {$sql_where}
            ";
          
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
        
    if($row != false && count($row) > 0){
            $ret['CCBANC']   = trim($row['TANR']);
            $ret['CCABI']   = $abi;
            $ret['CCCAB']   = $cab;
            $ret['des_cuba'] = trim($row['TADESC']).trim($row['TADES2']);
            $ret['success']  = true;
    }else{
       //controllare nello sportello
       
        $sql_ac = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
        $stmt_ac = db2_prepare($conn, $sql_ac);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_ac, array($abi, $cab));
        $r_cab = db2_fetch_assoc($stmt_ac);
        if($r_cab != false && count($r_cab) > 0){
            $ret['CCBANC']   = "";
            $ret['abi']    = $abi;
            $ret['cab']    = $cab;
            $ret['des_cuba'] = trim($r_cab['XDSABI']) ."-".trim($r_cab['XDSCAB']);
            $ret['msg_info'] = "Banca/Sportello non codificato in SV2: confermi inserimento?";
            $ret['success']  = true;
        }else{
            
            $ret['CCBANC']   = "";
            $ret['msg_error'] = "ABICAB non presente n� in SV2 n� su Sportelli bancari";
            $ret['success']  = false;
            
        }
      
       }
    }   
    echo acs_je($ret);
    exit;
        
}

if($_REQUEST['fn'] == 'exe_nuova_cuba'){
    
    $ar_ins = array();
    $sh = new SpedHistory();
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'NEW_COD_BANCA',
            "vals" => array()
        )
        );
    
    
    $ar_ins['TAUSGE'] 	= $auth->get_user();
    $ar_ins['TADTGE']   = oggi_AS_date();
    $ar_ins['TAUSUM'] 	= $auth->get_user();
    $ar_ins['TADTUM']   = oggi_AS_date();
    $ar_ins['TADT'] 	= $id_ditta_default;
    $ar_ins['TAID'] 	= 'CUBA';
    $ar_ins['TANR'] 	= $return_RI['RICITI'];
    $ar_ins['TADESC'] 	= substr($m_params->des_cuba, 0, 30);
    $ar_ins['TADES2'] 	= substr($m_params->des_cuba, 30, 30);
    $ar_ins['TAREST']   = sprintf("%-60s", '');
    $ar_ins['TAREST']   .= sprintf("%05s", $m_params->abi);
    $ar_ins['TAREST']   .= sprintf("%05s", $m_params->cab);
    
    
    $sql = "INSERT INTO {$cfg_mod_Gest['file_tab_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg();
    
    $sql = "SELECT TANR, TADESC, TADES2, SUBSTRING(TAREST, 61, 5) AS ABI,
            SUBSTRING(TAREST, 66, 5) AS CAB
            FROM {$cfg_mod_Gest['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}' AND TAID = 'CUBA'
            AND TANR = '{$return_RI['RICITI']}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    $ret = array();
    $ret['success'] = true;
    $ret['CCBANC']   = trim($row['TANR']);
    $ret['CCABI']    = trim($row['ABI']);
    $ret['CCCAB']    = trim($row['CAB']);
    $ret['des_cuba'] = trim($row['TADESC']).trim($row['TADES2']);
    echo acs_je($ret);
    exit;
    
}

if($_REQUEST['fn'] == 'exe_check_cuba'){
 
    $ret = array();
 
    $abi = sprintf("%05s", $m_params->form_values->f_abi);
    $cab = sprintf("%05s", $m_params->form_values->f_cab);
        
    $sql = "SELECT COUNT(*) AS C_ROW, TANR, TADESC, TADES2
            FROM {$cfg_mod_Gest['file_tab_sys']} TA
            WHERE TADT = '{$id_ditta_default}' AND TAID = 'CUBA'
            AND SUBSTRING(TAREST, 61, 5) = '{$abi}'
            AND SUBSTRING(TAREST, 66, 5) = '{$cab}'
            GROUP BY TANR, TADESC, TADES2";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if($row['C_ROW'] > 0){
        $ret['CCBANC']   = trim($row['TANR']);
        $ret['des_cuba'] = trim($row['TADESC']).trim($row['TADES2']);
   
    }else{
        $ret['CCBANC']   = "";
        $ret['des_cuba'] = "Abi/Cab non acquisito";
    }
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
 
}

if($_REQUEST['fn'] == 'get_values_new_dest'){
  
    $ar_values = array();
    $new_dest = $m_params->new_dest;
    $ret_des = $s->get_cliente_des_anag($id_ditta_default, $m_params->cliente, $new_dest);
    
    $ar_values['IND_D'] = $ret_des['IND_D'];
    $ar_values['LOC_D'] = $ret_des['LOC_D'];
    $ar_values['CAP_D'] = $ret_des['CAP_D'];
    $ar_values['PRO_D'] = $ret_des['PRO_D']; 
   
    $ret = array();
    $ret['success'] = true;
    $ret['rec'] = $ar_values;
    echo acs_je($ret);
    exit;

}


if($_REQUEST['fn'] == 'get_json_data_pagamento'){
        
    $ret = get_pagamenti($m_params);
 
    echo acs_je($ret);
    exit;
}

if($_REQUEST['fn'] == 'check_azienda_collegata'){
    
    $ret = array();
    
    $sql = "SELECT GCANCO, GCCDCF
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            WHERE GCDT='{$id_ditta_default}' AND GCCDCF = '{$m_params->cliente}'";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
        
    if($row['GCANCO'] > 0 && $row['GCANCO'] != $row['GCCDCF']){
        $ret['success'] = false; 
        $ret['msg_error'] = 'Impossibile collegare il cliente';
        
    }else{
        $ret['success'] = true;
    }
    
    
    echo acs_je($ret);
    exit;
}