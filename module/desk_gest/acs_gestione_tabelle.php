<?php

require_once "../../config.inc.php";
require_once("acs_gestione_tabelle_include.php");

$s = new Spedizioni(array('no_verify' => 'Y'));
$main_module = new DeskGest();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();


function m_get_fields($m_table_config, $m_table_config_altro = array()){
    $r = array();
    foreach ($m_table_config['fields'] as $kf=>$f)
        $r[] = $kf;
    if($m_table_config['TAID'] == 'CUCP' || $m_table_config['TAID'] == 'BCCG'){
        foreach ($m_table_config_altro['fields'] as $kf=>$f)
            $r[] = $kf;
    }
    
    if($m_table_config['TAID'] == 'CUAG'){
         $r[] = 'forn';
         $r[] = 'c_forn';
         $r[] = 'd_forn';
         $r[] = 'provvigione';
     
     }
     
     if($m_table_config['TAID'] == 'ITAL'){
         $r[] = 'd_itin';
     
     }
    
    
    if($m_table_config['TAID'] == 'XSPM'){
        $r[] = 'cliente';
        $r[] = 'd_cli';
        $r[] = 'sconti';
        $r[] = 'var1';
        $r[] = 'van1';
        $r[] = 'var2';
        $r[] = 'van2';
        $r[] = 't_var1';
        $r[] = 't_van1';
        $r[] = 't_var2';
        $r[] = 't_van2';
    }
    
    return acs_je($r);
}


function m_get_columns($m_table_config){
      
    foreach ($m_table_config['fields'] as $kf=>$f) {
        if(!isset($f['hidden']))
            $hidden = 'false';
        else 
            $hidden = $f['hidden'];
        
        if(!isset($f['c_width']))
            $width = "flex : 1";
        else
            $width = "width : {$f['c_width']}";
        
        if(isset($f['short']))
            $header = $f['short'];
        else
            $header = $f['label'];
        
        $renderer = "";
        if($kf == 'TATP'){
            $renderer = "renderer: function(value, metaData, record){
		    			  if(record.get('TATP') == 'S'){ 
                               metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			       return '<img src=" .  img_path("icone/48x48/divieto.png") . " width=15>';
		    		 	   }

                            if (record.get('esau') == 'E'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('In esaurimento') + '\"';
					   			return '<img src=". img_path("icone/48x48/clessidra.png") ." width=15>';}
							if (record.get('esau') == 'A'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('In avviamento') + '\"';
					   			return '<img src=".img_path("icone/48x48/label_blue_new.png") ."width=18>';}
				    		if (record.get('esau') == 'C'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('In codifica') + '\"';
					   			return '<img src= ". img_path("icone/48x48/design.png")." width=15>';}
                            if (record.get('esau') == 'R'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Riservato') + '\"';
					   			return '<img src= ". img_path("icone/48x48/folder_private.png")." width=15>';}
                            if (record.get('esau') == 'F'){	    			    	
					   			metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Fine codifica') + '\"';
					   			return '<img src= ". img_path("icone/48x48/button_blue_play.png")." width=15>';}
		    		 
		    		  }";
        }
        
        if($kf == 'abi'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('iban').trim() != ''){ 
                            metaData.tdCls += ' sfondo_verde';
		    			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Iban: '+record.get('iban')) + '\"';
                          }
                          return value;   
		    		  }";
            
            
        }
        
        if($kf == 'dtvi'){
            $renderer = "renderer: function(value, metaData, record){
                         if(record.get('scaduta') == 'Y'){
                            metaData.tdCls += ' sfondo_rosso ';
		    			  }
                          return value + ' - ' + record.get('dtvf');
		    		  }";
            
            
        }
        
        if($kf == 'tipo1' && $m_table_config['TAID'] == 'BCCG'){
            $renderer = "renderer: function(value, metaData, record){
                           return value + ', ' + record.get('tipo2') + ', ' + record.get('tipo3');
		    		  }";
            
            
        }
        
             
        $view = true;
        if($f['only_view'] == 'F')
            $view = false;
            
        if($view == true){
        
        ?>
		 {
			header: <?php echo j($header)?>,
		 	dataIndex: <?php echo j($kf)?>, 
		 	hidden: <?php echo $hidden ?>, 
		 	<?php if(isset($f['tooltip'])){?>
		 		tooltip: <?php echo j($f['tooltip']); ?>,
		 	<?php }?>	 
		    <?php echo $width ?>,
		 	filter: {type: 'string'}, filterable: true,
		 	<?php echo $renderer ?>
		 },		
		<?php
		
            }
	}
	
	?>
	
	
	 <?php if($m_table_config['TAID'] == 'CUAG'){?>
	    { header: 'Fornitore'
          , dataIndex: 'forn'
          , width: 80
          , filter: {type: 'string'}, filterable: true
          , renderer: function(value, metaData, record){
                 if(record.get('forn').trim() != '000000000')
    			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('d_forn')) + '\"';
                  
                  return value;   
    		  }
       
          },
	    { header: 'Provvigioni'
          , dataIndex: 'provvigione'
          , width: 100
          , filter: {type: 'string'}, filterable: true
       
          },
	 
	 <?php }?>
	 
	   <?php if($m_table_config['TAID'] == 'ITAL'){?>
	     { header: 'Itinerario'
          , dataIndex: 'd_itin'
          , width: 200
          , filter: {type: 'string'}, filterable: true
          },
	   <?php }?>
	 
	        
        <?php if($m_table_config['TAID'] == 'XSPM'){?>
         { header: 'Cliente'
          , dataIndex: 'cliente'
          , width: 80
          , filter: {type: 'string'}, filterable: true
          , renderer: function(value, metaData, record){
                 if(record.get('d_cli').trim() != '')
    			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('d_cli')) + '\"';
                  
                  return value;   
    		  }
       
          },
          { header: 'Sconti'
          , dataIndex: 'sconti'
          , width: 100
          , filter: {type: 'string'}, filterable: true
       
          },
        { header: 'Var. 1'
          , dataIndex: 'var1'
          , width: 50
          , filter: {type: 'string'}, filterable: true
          , renderer: function(value, metaData, record){
                 if(record.get('var1').trim() != '')
    			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('t_var1')) + '\"';
                  
                  return value;   
    		  }
          }
        
         ,{ header: 'Van. 1'
          , dataIndex: 'van1'
          , width: 50
          , filter: {type: 'string'}, filterable: true
          , renderer: function(value, metaData, record){
                 if(record.get('van1').trim() != '')
    			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('t_van1')) + '\"';
                  
                  return value;   
    		  }
          }
          ,{ header: 'Var. 2'
          , dataIndex: 'var2'
          , width: 50
          , filter: {type: 'string'}, filterable: true
          , renderer: function(value, metaData, record){
                 if(record.get('var2').trim() != '')
    			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('t_var2')) + '\"';
                  
                  return value;   
    		  }
          }
          ,{ header: 'Van. 2'
          , dataIndex: 'van2'
          , width: 50
          , filter: {type: 'string'}, filterable: true
          , renderer: function(value, metaData, record){
                 if(record.get('van2').trim() != '')
    			    metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('t_van2')) + '\"';
                  
                  return value;   
    		  }
          },
			        
        <?php }?>
	
	  {header: 'Immissione',
	 columns: [
	 {header: 'Data', dataIndex: 'TADTGE', renderer: date_from_AS, width: 70, sortable : true,
	 renderer: function(value, metaData, record){
	         if (record.get('TADTGE') != record.get('TADTUM')) 
	          metaData.tdCls += ' grassetto';
	 
             q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		     return date_from_AS(value);	
	}
	 
	 }
	 ,{header: 'Utente', dataIndex: 'TAUSGE', width: 70, sortable : true,
	  renderer: function(value, metaData, record){
	  
	  		if (record.get('TAUSGE') != record.get('TAUSUM')) 
	          metaData.tdCls += ' grassetto';
	  
             q_tip = 'Modifica: ' + date_from_AS(record.get('TADTUM')) + ', Utente ' +record.get('TAUSUM');
			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    			    	
		     return value;	
	} 
	 }
	 ]}
	 
	 <?php 
}


function m_get_comp_form($m_table_config, $m_params){
    global $main_module, $desk_art;
    
    $f_combo = "";
    $n_fc = '';
        
    foreach ($m_table_config['fields'] as $kf=>$f) {
        
        //hidden
        if(!isset($f['hidden']))
            $hidden = 'false';
        else
            $hidden = $f['hidden'];
        
        
        //width
        if(!isset($f['width']))
            $width = "flex : 1, anchor: '-15'";
        else
            $width = "width : {$f['width']}";
        
            
        //maxLength
        if(!isset($f['maxLength']))
            $maxlen = 100;
        else
            $maxlen = $f['maxLength'];
        
        if(!isset($f['allowBlank']))
            $allowBlank = 'true';
        else
            $allowBlank = $f['allowBlank']; 
        
        if(!isset($f['maskRe']))
            $maskRe = "";
        else
            $maskRe = "maskRe : {$f['maskRe']}, ";
        
        
        if(isset($f['upper'])){
            $upper = " 'change': function(field){
        				  field.setValue(field.getValue().toUpperCase());
      					}
 						 ";
        }else{
            $upper = "";
        }
        
        if(isset($m_params->c_art))
            $tacor1 = $m_params->c_art;
        else 
            $tacor1 = $m_params->tacor1;
                
        if(isset($f['check'])){
            $check = " , 'blur': function(field) {
      				    var form = this.up('form').getForm();
                        var grid = this.up('form').up('panel').down('grid');
                      
      					Ext.Ajax.request({
						        url        : '".$_SERVER['PHP_SELF']."?fn=exe_check',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				codice : field.getValue(),
                                    tacor2 : '".$m_params->tacor2."',
                                    tacor1 : '".$tacor1."'      

								},							        
						        success : function(result, request){
                                    var jsonData = Ext.decode(result.responseText);
							        if(jsonData.success == false){
					      	    		 acs_show_msg_error(jsonData.msg_error);
							        	 form.setValues(jsonData.record);
			        			   		 var rec_index = grid.getStore().findRecord('TANR', jsonData.record.TANR);
		  					   			 grid.getView().select(rec_index);
			  					   	     grid.getView().focusRow(rec_index);
				        			}
					      	    	
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
                	 		

            		}
 						 ";
        }else{
            $check = "";
        }
        
        
        
        //view
        $view = true;    
        if($f['only_view'] == 'C')
            $view = false;
        
        if($view == true){
     
            //xtype
            if(!isset($f['xtype'])){
                
                if($f['fieldcontainer'] == 'Y'){?>
          
  		 	 { 
				xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox',
						    pack: 'start',
						    align: 'stretch'},						
				items: [
				
				
				   
				]},
							
              
         <?php }else{ ?>
           {
            xtype : 'textfield',
		 	name: <?php echo j($kf)?>,
		 	fieldLabel: <?php echo j($f['label'])?>,
		 	labelWidth : 120,
		 	<?php echo $width ?>,
		 	maxLength : <?php echo $maxlen ?>,
		 	allowBlank : <?php echo $allowBlank ?>,
		 	<?php echo $maskRe ?>
		 	hidden : <?php echo $hidden ?>,
		 	listeners: {
		 	  <?php echo $upper ?>
		 	  <?php echo $check ?>
		 	}
		 	
		 	
		  },
              
              
       <?php   }
          
      
            }elseif($f['xtype'] == 'combo_tipo'){
                ?>
		     {
			xtype : 'combo',
		 	name: <?php echo j($kf)?>, 
		 	fieldLabel: <?php echo j($f['label'])?>,
		 	labelWidth : 120,
		 	<?php echo $width ?>,
		 	hidden : <?php echo $hidden ?>,
		 	allowBlank : <?php echo $allowBlank ?>,
			displayField: 'text',
		    valueField: 'id',	
		    value : '',						
			emptyText: '- seleziona -',
		   	queryMode: 'local',
     		minChars: 1,	
			store: {
			
    			editable: false,
    			autoDestroy: true,
    			fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		     	
    		     <?php 
    		     
    		     
    		     if(isset($f['tab_sys']) && strlen($f['tab_sys']) > 0){
    		         if(isset($f['tacor2'])) $tacor2 = $f['tacor2'];
    		         else $tacor2 = '';
    		         echo acs_ar_to_select_json(find_TA_sys($f['tab_sys'], null, null, $tacor2, null, null, 0, '', 'Y'), '', true, 'N', 'Y');
    		     }else if(isset($f['tab_std']) && strlen($f['tab_std']) > 0){
    		         echo acs_ar_to_select_json($main_module->find_TA_std($f['tab_std'], null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y');
    		     }else{
    		         
    		     
    		     if($kf == 'cond_pa')
    		         echo acs_ar_to_select_json($main_module->find_sys_TA('BPA4'), '');
		         if($kf == 'gr_ava')
		             echo acs_ar_to_select_json($main_module->find_sys_TA('BGAG'), '');
	             if($kf == 'gr_pro')
	                 echo acs_ar_to_select_json($main_module->find_sys_TA('BGPR'), '');
    		     if($kf == 'a_geo')
    		         echo acs_ar_to_select_json($main_module->find_sys_TA('BAGE'), ''); 
    		     if($kf == 'area_f1' || $kf == 'area_f2' || $kf == 'area_f3' || $kf == 'area_f4')
    		         echo acs_ar_to_select_json($main_module->find_TA_std('ARFIS', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
    		     if($kf == 'n_paga1' || $kf == 'n_paga2' || $kf == 'n_paga3')
		             echo acs_ar_to_select_json($main_module->find_sys_TA('CUCP'), ''); 
		         if($kf == 'gg_fisso_cli')
	                 echo acs_ar_to_select_json($main_module->find_TA_std('CCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'data_cons')
                     echo acs_ar_to_select_json($main_module->find_TA_std('DATAC', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'contr_ass')
                     echo acs_ar_to_select_json($main_module->find_TA_std('CCT05', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'doga1')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('BGDN'), ''); 
                 if($kf == 'riba')
                     echo acs_ar_to_select_json($main_module->find_TA_std('TRIBA', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'TACOR1')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('BITL'), ''); 
                 if($kf == 'TACOR2')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('VUVL'), ''); 
                 if($kf == 'azz_sc')
                     echo acs_ar_to_select_json($main_module->find_TA_std('CCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'check_data')
                     echo acs_ar_to_select_json($main_module->find_TA_std('CCT08', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'gr_doc')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('RDOC'), ''); 
                 if($kf == 'tipo') 
                     echo acs_ar_to_select_json($main_module->find_TA_std('AGE01', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'g_prov')
                     echo acs_ar_to_select_json($main_module->find_TA_std('AGE02', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'invio')
                     echo acs_ar_to_select_json($main_module->find_TA_std('AGE03', null, 'N', 'N', null, null, null, 'N', 'Y'), '');
                 if($kf == 'b2b')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), '');
                 if($kf == 'agenti')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('CGRA'), '');
                 if($kf == 'tivr')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('BTID'), '');
                 if($kf == 'divis')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('DDOC'), '');
                 if($kf == 'itin')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('BITI'), '');
                 if($kf == 'tpco1' || $kf == 'tpco2' || $kf == 'tpco3')
                     echo acs_ar_to_select_json($main_module->find_sys_TA('BDOC'), '');
                 if($kf == 'tp'){ ?>
                     {id : 'C', text : '[C] Cliente'},
                     {id : 'F', text : '[F] Fornitore'},
                 <?php }
                    if($kf == 'canc'){ ?>
                     {id : '',  text : '- vuoto -'},
                     {id : 'N', text : '[N] No'},
                 <?php }
                     if($kf == 'bmcf' || $kf == 'comu'){ ?>
                     {id : '',  text : '- vuoto -'},
                     {id : 'Y', text : '[Y] Yes'},
                 <?php }
                 
                     if($kf == 'vali'){ ?>
                     {id : '',  text : '- vuoto -'},
                     {id : 'A', text : '[A] Assicurato'},
                     {id : 'E', text : '[E] Entrambi'},
                     {id : 'S', text : '[S] Standard'},
                 <?php }
                     if($kf == 'vdoc'){ ?>
                     {id : '',  text : '- vuoto -'},
                     {id : 'L', text : '[L] Localit�'}
                 <?php  }
                 
                     }?>
    		    ]
			}, listeners: { 
			 		beforequery: function (record) {
    	         		record.query = new RegExp(record.query, 'i');
    	         		record.forceAll = true;
    				 },
                  change: function() {
                  if (this.getValue() === null) {
                    this.setValue('');
                  }
                }
    				 
   
    			
 			},	
		 	
		  },		
		  <?php }elseif($f['xtype'] == 'fieldcontainer'){ 
		      if($n_fc != $f['n_fc']) {?>
		       { 
				xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox',
						    pack: 'start',
						    align: 'stretch'},						
				items: [
				<?php } 
				    if($f['type'] == 'combo'){?>
				  {
        			xtype : 'combo',
        		 	name: <?php echo j($kf)?>, 
        		 	fieldLabel: <?php echo j($f['label'])?>,
        		 	 labelAlign : <?php echo j($f['labelAlign'])?>,
        		 	labelWidth : <?php echo j($f['labelWidth'])?>,
        		 	<?php echo $width ?>,
        		 	forceSelection: true,  							
        			displayField: 'text',
        		    valueField: 'id',							
        			emptyText: '- seleziona -',
        		   	queryMode: 'local',
             		minChars: 1,	
        			store: {
            			editable: false,
            			autoDestroy: true,
            			fields: [{name:'id'}, {name:'text'}],
            		    data: [	
            		    
            		    <?php
            		    if($kf == 'banca_ob' || $kf == 'contanti'){
            		      echo acs_ar_to_select_json($main_module->find_TA_std('CCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), ''); 
            		    }
            		    elseif($kf == 'l_ini' || $kf == 'l_fin'){  
    		               echo acs_ar_to_select_json(find_TA_sys('PUFI', null, null, null, null, null, 0, '', 'Y'), '');
            		    }elseif($kf == 'post'){?>
            		            {id : '' , text : '- vuoto -'},
            		     	    {id : 'Y', text : '[Y] Yes'}
            		    <?php }else{ ?>							    
            	    	   	 	{id : '>', text : '[>] Maggi.'},
            		     	    {id : '<', text : '[<] Minore'},
            		     	    {id : '=', text : '[=] Uguale'}
            		    <?php }?> 	    
            		    ]
        			}, listeners: { 
        			 		beforequery: function (record) {
            	         		record.query = new RegExp(record.query, 'i');
            	         		record.forceAll = true;
            				 }
         			}	
        		 	
        		  },
				
				
				<?php }elseif($f['type'] == 'number'){ 
				    if(isset($f['maxValue']))
				        $maxValue = "maxValue : {$f['maxValue']}, ";
				   
				    ?>
				
				   {
                    xtype : 'numberfield',
        		 	name: <?php echo j($kf)?>, 
        		 	labelWidth : 120,
    	 	        fieldLabel: <?php echo j($f['label'])?>,
    	 	        <?php echo $width ?>,
    	 	        <?php echo $maxValue ?>
    	 	        hideTrigger:true,
    	 	        decimalPrecision : 2,
    	 	        keyNavEnabled : false,
    				mouseWheelEnabled : false,
    	 	        labelAlign : <?php echo j($f['labelAlign'])?>,
        		 	labelWidth : <?php echo $f['labelWidth']; ?>  		 	
        		   },
				
				<?php }elseif($f['type'] == 'display'){ ?>
				
				{
                    xtype : 'displayfield',
        		 	name: <?php echo j($kf)?>, 
        		 	margin : '0 0 0 5',
        		 	anchor: '-5',
		 	        fieldLabel: <?php echo j($f['label'])?>,
		 	        labelSeparator : '',
	 	        	labelWidth : <?php echo j($f['labelWidth'])?>,
	 	        	<?php echo $width ?>,
	 	            labelAlign : <?php echo j($f['labelAlign'])?>
		 	     }
				
				
		        <?php }else{?>
			
				   {
                    xtype : 'textfield',
        		 	name: <?php echo j($kf)?>, 
		 	        fieldLabel: <?php echo j($f['label'])?>,
		 	        <?php echo $width ?>,
		 	        <?php echo $maskRe ?>
		 	        maxLength : <?php echo $maxlen ?>, 
		 	        labelAlign : <?php echo j($f['labelAlign'])?>,
        		 	labelWidth : <?php echo j($f['labelWidth'])?>,
        		 	<?php if($f['listener'] == 'Y'){?>
        		 	 listeners: {
        				'blur': function(field) {
        					var n_value = field.getValue();
        					if(n_value > 0){	
        						n_value = (n_value < 10 ? '0' : '') + n_value;
        						field.setValue(n_value);
        					}
        				}
            		}
        		 	<?php }?>
        		   },
				
			
	          <?php  
				}
	          if($f['close'] == 'Y') {  ?> ]},  <?php }
		
		  }elseif($f['xtype'] == 'datefield'){
		      
		      if($f['submit'] != '')
		          $submitFormat = $f['submit'];
	          else
	              $submitFormat = 'Ymd'
    
		      
		      ?>
		      {
			   xtype: 'datefield'
			   , startDay: 1 //lun.
			   , name: <?php echo j($kf)?>
		 	   , fieldLabel: <?php echo j($f['label'])?>
		 	   , <?php echo $width ?>
		 	   , labelWidth : 120
			   , format: 'd/m/y'
        	   , submitFormat: <?php echo j($submitFormat); ?>
			   , allowBlank: true
			   , anchor: '-15'
			   , listeners: {
			       invalid: function (field, msg) {
			       Ext.Msg.alert('', msg);
			       },
			       select : function(field){
			          console.log(field);
			       }
				}
			},
		   
		      
		      
		      <?php 
		      
		  }elseif($f['xtype'] == 'number'){
		  
		      if(isset($f['maxValue']))
		          $maxValue = "maxValue : {$f['maxValue']}, ";
		      ?>
		    {
                xtype : 'numberfield',
    		 	name: <?php echo j($kf)?>, 
    		 	labelWidth : 120,
	 	        fieldLabel: <?php echo j($f['label'])?>,
	 	        <?php echo $width ?>,
	 	        <?php echo $maxValue ?>
	 	        hideTrigger:true,
	 	        decimalPrecision : 2,
	 	        keyNavEnabled : false,
				mouseWheelEnabled : false,
	 	       // labelAlign : <?php echo j($f['labelAlign'])?>,
    		  
    		 	
    		   },
		  
		  
		  <?php }
		  
 
        }
        
            $n_fc = $f['n_fc'];
		
        
	}
}



if ($_REQUEST['fn'] == 'exe_check'){
    
    $ret = array();
    
    if(strlen(trim($m_params->tacor2)) > 0)
        $tacor2 = trim($m_params->tacor2);
    
    if(isset($m_params->tacor1) && strlen(trim($m_params->tacor1)) > 0)
        $tacor1 = trim($m_params->tacor1);
   
    if(isset($m_params->tacor2) && strlen(trim($m_params->tacor2)) > 0)
        $row = get_TA_sys($m_table_config['TAID'], $m_params->codice, null, $tacor2);
    else if(isset($m_params->tacor1) && strlen(trim($m_params->tacor1)) > 0)
        $row = get_TA_sys($m_table_config['TAID'], $m_params->codice, $tacor1);
    else
        $row = get_TA_sys($m_table_config['TAID'], $m_params->codice);

    
    if($row != false && count($row) > 0){
        $ret['msg_error'] = "Codice esistente";
        $ret['success'] = false;
        $sql_s = "SELECT *
        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_table_config['TAID']}'
        AND TANR = '{$m_params->codice}'";
        
        $stmt_s = db2_prepare($conn, $sql_s);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_s);
        echo db2_stmt_errormsg($stmt_s);
        $row_s = db2_fetch_assoc($stmt_s);
        $row_s = get_m_row($row_s, $m_table_config);
        $ret['record'] = $row_s;
    }else{
        $ret['success'] = true;
    }
  
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_sos_att'){
    
    $ar_upd = array();
    
    $tatp = trim($m_params->row->TATP);
  
    if($tatp == "")
        $new_value = "S";
    else
        $new_value = "";
    
        
    $ar_upd['TAUSUM'] = $auth->get_user();
    $ar_upd['TADTUM'] = oggi_AS_date();
    $ar_upd['TATP']   = $new_value;
    
    $sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(TA) = '{$m_params->row->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['new_value'] = $new_value;
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'exe_canc'){
    
        
    $sql = "DELETE FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
    WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
     
	$ret = array();
    $ret['success'] = true;
	echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ret = array();
    
    //se ho passato tacor2 il controllo dell'esistenza devo farlo anche in base a TACOR2
    if(isset($m_params->tacor2) && strlen($m_params->tacor2) > 0){
        $row = get_TA_sys($m_table_config['TAID'], $m_params->form_values->TANR, null, $m_params->tacor2);
    }elseif(isset($m_params->tacor1) && strlen($m_params->tacor1) > 0){ 
        $row = get_TA_sys($m_table_config['TAID'], $m_params->form_values->TANR, $m_params->tacor1);
    }else {
        $row = get_TA_sys($m_table_config['TAID'], $m_params->form_values->TANR);
    }
  
    if($row != false && count($row) > 0){
        $ret['success'] = false;
        $ret['msg_error'] = "Codice esistente";
        echo acs_je($ret);
        return;
    }

    
	$ar_ins = array();

	if(isset($m_table_config['fields']['TAREST'])){
	    if (!isset($m_params->form_values->TAREST) || !isset($m_table_config['TAREST'])){
	        $ret['success'] = false;
	        $ret['msg_error'] = "Gestione tabella non configurata correttamente(TAREST) <br> Contattare amministratore di sistema";
	        echo acs_je($ret);
	        return;
	    }
	}
	
	if(isset($m_table_config['fields']['TAREST'])){
	    $tarest = genera_TAREST($m_table_config, $m_params->form_values);
	    $ar_ins['TAREST'] 	= $tarest;
	}
	
	//ritorno codice banca 
	if($m_table_config['TAID'] == 'CUBA'){
	    
	    $sh = new SpedHistory();
	    $return_RI = $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'NEW_COD_BANCA',
	            "vals" => array()
	        )
	        );
	    $ar_ins['TANR'] = $return_RI['RICITI'];
	 
	}else{
	    $ar_ins['TANR'] = $m_params->form_values->TANR;
	}

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAUSUM'] 	= $auth->get_user();
	$ar_ins['TADTUM']   = oggi_AS_date();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TAID'] 	= $m_table_config['TAID'];
	
	if($m_table_config['TAID'] == 'ITAL')
	    $ar_ins['TACOR1'] 	= $m_params->tacor1;
	    
	
	if(isset($m_params->form_values->TACINT))
        $ar_ins['TACINT'] 	= $m_params->form_values->TACINT;
    if(isset($m_params->form_values->TACOR1))
        $ar_ins['TACOR1'] 	= $m_params->form_values->TACOR1;
    if(isset($m_params->form_values->TACOR2))
        $ar_ins['TACOR2'] 	= $m_params->form_values->TACOR2;
    if($m_table_config['TAID'] == 'CUBA'){
        
        $istituto = trim($m_params->form_values->TADESC);
        if(trim(substr($istituto, 0, 30)) != '')
            $ar_ins['TADESC'] 	= acs_toDb(substr($istituto, 0, 30));
        if(trim(substr($istituto, 30, 30)) != '')
            $ar_ins['TADES2'] 	= acs_toDb(substr($istituto, 30, 30));
        
    }else{
        if(isset($m_params->form_values->TADESC))
            $ar_ins['TADESC'] 	= acs_toDb(trim($m_params->form_values->TADESC));
        if(isset($m_params->form_values->TADES2))
            $ar_ins['TADES2'] 	= acs_toDb(trim($m_params->form_values->TADES2));
    }

    $sql = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";


    
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	echo db2_stmt_errormsg();
	
	if($m_table_config['TAID'] == 'BCCG'){
	    $ar_ins2['TAUSGE'] 	= $auth->get_user();
	    $ar_ins2['TADTGE']   = oggi_AS_date();
	    $ar_ins2['TAUSUM'] 	= $auth->get_user();
	    $ar_ins2['TADTUM']   = oggi_AS_date();
	    $ar_ins2['TADT'] 	= $id_ditta_default;
	    $ar_ins2['TAID'] 	= 'BCG2';
	    $ar_ins2['TANR']     = $m_params->form_values->TANR;
	    $tarest2 = genera_TAREST2($m_params->form_values);
	    $ar_ins2['TAORDI'] 	= substr($tarest2, 0, 5);
	    $ar_ins2['TADESC'] 	= substr($tarest2, 5, 30);
	    $ar_ins2['TADES2'] 	= substr($tarest2, 35, 30);
	    $ar_ins2['TAREST']  = substr($tarest2, 65);
	    
	    $sql_2 = "INSERT INTO {$cfg_mod_DeskArt['file_tabelle_sys']}(" . create_name_field_by_ar($ar_ins2) . ") VALUES (" . create_parameters_point_by_ar($ar_ins2) . ")";
	    
	    $stmt_2 = db2_prepare($conn, $sql_2);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt_2, $ar_ins2);
	    echo db2_stmt_errormsg();
	}
	
	
	if($m_table_config['TAID'] == 'BCCG'){
	    $sql_sel .= ", TA2.TAORDI AS TAORDI2, TA2.TADESC AS TADESC2, TA2.TADES2 AS TADES22, TA2.TAREST AS TAREST2";
	    $sql_join .= " LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA2
	    ON TA.TADT = TA2.TADT AND TA2.TAID = 'BCG2' AND TA.TANR = TA2.TANR";
	    
	}
	
	
	if($m_params->tacor2 != ''){
	    $sql_where .= " AND TACOR2 = '".trim($m_params->tacor2)."'";
	}
	
	if(isset($m_params->tacor1) && $m_params->tacor1 != ''){
	    $sql_where .= " AND TACOR1 = '".trim($m_params->tacor1)."'";
	}
	
	
	$sql_s = "SELECT RRN(TA) AS RRN, TA.* {$sql_sel}
	          FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
	          {$sql_join}
              WHERE TADT = '{$id_ditta_default}' AND TAID = '{$m_table_config['TAID']}'  
	          AND TANR = '{$ar_ins['TANR']}' {$sql_where}
              ";
	
	$stmt_s = db2_prepare($conn, $sql_s);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_s);
	echo db2_stmt_errormsg($stmt_s);
	$row = db2_fetch_assoc($stmt_s);
	$row = get_m_row($row, $m_table_config);

	
	$ret['success'] = true;
	$ret['record'] = $row;
	echo acs_je($ret);
    exit;
}




if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ret = array();
    $ar_upd = array();
    
    if(isset($m_table_config['fields']['TAREST'])){
        if (!isset($m_params->form_values->TAREST) || !isset($m_table_config['TAREST'])){
            $ret['success'] = false;
            $ret['msg_error'] = "Gestione tabella non configurata correttamente(TAREST) <br> Contattare amministratore di sistema";
            echo acs_je($ret);
            return;
        }
    }
    
    if(isset($m_table_config['fields']['TAREST'])){
        $tarest = genera_TAREST($m_table_config, $m_params->form_values);
        $ar_upd['TAREST'] 	= $tarest;
    }

	
	$ar_upd['TAUSUM'] 	= $auth->get_user();
	$ar_upd['TADTUM']   = oggi_AS_date();
	if(isset($m_params->form_values->TACINT))
	   $ar_upd['TACINT'] 	= $m_params->form_values->TACINT;
   if(isset($m_params->form_values->TACOR1))
       $ar_upd['TACOR1'] 	= $m_params->form_values->TACOR1;
   if(isset($m_params->form_values->TACOR2))
       $ar_upd['TACOR2'] 	= $m_params->form_values->TACOR2;
	if($m_table_config['TAID'] == 'CUBA'){
       $istituto = trim($m_params->form_values->TADESC);
       $ar_upd['TADESC'] 	= acs_toDb(substr($istituto, 0, 30));
       $ar_upd['TADES2'] 	= acs_toDb(substr($istituto, 30, 30));
	    
	}else{
	    if(isset($m_params->form_values->TADESC))
	       $ar_upd['TADESC'] 	= acs_toDb(trim($m_params->form_values->TADESC));
        if(isset($m_params->form_values->TADES2))
	       $ar_upd['TADES2'] 	= acs_toDb(trim($m_params->form_values->TADES2));
	}
	

	
	$sql = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
	SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
	WHERE RRN(TA) = '{$m_params->form_values->RRN}'";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_upd);
	echo db2_stmt_errormsg();
	
	if($m_table_config['TAID'] == 'BCCG'){
	   
	    $ar_upd2['TAUSUM'] 	= $auth->get_user();
	    $ar_upd2['TADTUM']   = oggi_AS_date();
	    $tarest2 = genera_TAREST2($m_params->form_values);
	    $ar_upd2['TAORDI'] 	= substr($tarest2, 0, 5);
	    $ar_upd2['TADESC'] 	= substr($tarest2, 5, 30);
	    $ar_upd2['TADES2'] 	= substr($tarest2, 35, 30);
	    $ar_upd2['TAREST']  = substr($tarest2, 65);
	    
	    $sql_2 = "UPDATE {$cfg_mod_DeskArt['file_tabelle_sys']} TA
        	      SET " . create_name_field_by_ar_UPDATE($ar_upd2) . "
        	      WHERE TADT = '{$id_ditta_default}' AND TAID = 'BCG2'
        	      AND TANR = '{$m_params->form_values->TANR}'";
	    
	    $stmt_2 = db2_prepare($conn, $sql_2);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt_2, $ar_upd2);
	    echo db2_stmt_errormsg();
	}
	
	
	if($m_table_config['TAID'] == 'BCCG'){
	    $sql_sel .= ", TA2.TAORDI AS TAORDI2, TA2.TADESC AS TADESC2, TA2.TADES2 AS TADES22, TA2.TAREST AS TAREST2";
	    $sql_join .= " LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA2
	    ON TA.TADT = TA2.TADT AND TA2.TAID = 'BCG2' AND TA.TANR = TA2.TANR";
	    
	}

	
	$sql_s = "SELECT RRN (TA) AS RRN, TA.* {$sql_sel}
	          FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
              {$sql_join}
	          WHERE RRN(TA) = '{$m_params->form_values->RRN}' {$sql_where}";
	
	$stmt_s = db2_prepare($conn, $sql_s);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_s);
	echo db2_stmt_errormsg($stmt_s);
	$row = db2_fetch_assoc($stmt_s);
	$row = get_m_row($row, $m_table_config);

	$ret['success'] = true;
	$ret['record'] = $row;
	echo acs_je($ret);
    exit;
}



if ($_REQUEST['fn'] == 'get_data_grid'){

    $sql_where = "";
    $sql_join = "";
    $sql_sel = "";
    $ar_lin = array();
    foreach($cfg_mod_DeskUtility['ar_lingue'] as $v){
        $ar_lin[] = $v['id'];
    }    
    
    if($m_params->tacor2 != ''){
        $sql_where .= " AND TACOR2 = '".trim($m_params->tacor2)."'";
        $sql_where_join = " AND TACOR2 = '".trim($m_params->tacor2)."'";
    }
    
    if(isset($m_params->tacor1) && $m_params->tacor1 != ''){
        $sql_where .= " AND TACOR1 = '".trim($m_params->tacor1)."'";
        $sql_where_join = " AND TACOR1 = '".trim($m_params->tacor1)."'";
    }
    
    if(isset($m_params->tanr) && $m_params->tanr != ''){
        $sql_where .= " AND TA.TANR = '".$m_params->tanr."'";
        $sql_where_join = " AND TANR = '".$m_params->tanr."'";
    }
    
    if($m_table_config['TAID'] == 'BCCG'){
        $sql_sel .= ", TA2.TAORDI AS TAORDI2, TA2.TADESC AS TADESC2, TA2.TADES2 AS TADES22, TA2.TAREST AS TAREST2";
        $sql_join .= " LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA2
                       ON TA.TADT = TA2.TADT AND TA2.TAID = 'BCG2' AND TA.TANR = TA2.TANR";
       
    }
    
       
	$sql = "SELECT RRN (TA) AS RRN, TA.*, TA_TRAD.C_ROW AS TRAD {$sql_sel}
	        FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA
	        {$sql_join}
	        LEFT  OUTER JOIN (
                SELECT COUNT(*) AS C_ROW, TANR
                FROM  {$cfg_mod_DeskArt['file_tabelle_sys']} TAT
                WHERE TAT.TADT = '{$id_ditta_default}' AND TAT.TAID = '{$m_table_config['TAID']}' {$sql_where_join} AND TAT.TALINV IN (" . sql_t_IN($ar_lin) . ")
                GROUP BY TAT.TANR) TA_TRAD
            ON TA.TANR = TA_TRAD.TANR 
	        WHERE TA.TADT='{$id_ditta_default}' AND TA.TALINV = '' AND TA.TAID = ?
            {$sql_where} ORDER BY TA.TANR, TA.TADESC";
	 
           
   	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_table_config['TAID']));

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {
	    
	    $row = get_m_row($row, $m_table_config);
	    $data[] = $row;
		
	}

	echo acs_je($data);

	exit();

}


if ($_REQUEST['fn'] == 'open_panel'){

    if(isset($m_params->c_art))
        $tacor1 = $m_params->c_art;
    else 
        $tacor1 = $m_params->tacor1;
    
    ?>

				
{"success":true, "items": [

   {
			xtype: 'panel',
			<?php if($m_table_config['TAID'] != 'ITAL'){
			  if($m_params->hide_grid != 'Y'){?>
    			title: '<?php echo "{$m_table_config['title_grid']}"; ?>',
            	<?php echo make_tab_closable(); ?>,
        	<?php }}?>
        	layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
	{
        
        xtype: 'grid',
        multiSelect : true,
        itemId : 'my_grid',
        <?php if($m_params->hide_grid == 'Y'){?>	
			hidden : true,
		<?php } ?>
       	flex : 0.7,
	    features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}]
			,store: {
						
						xtype: 'store',
						autoLoad: true,
					   	proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },							        
							        
							           extraParams: {
										 tacor2 : <?php echo j($m_params->tacor2); ?>,
										 tacor1 : <?php echo j($tacor1); ?>,
										 tanr : <?php echo j($m_params->tanr);?>
										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        		  fields: <?php 
		        		  if($m_table_config['TAID'] == 'CUCP' || $m_table_config['TAID'] == 'BCCG')
    		        		     echo m_get_fields($m_table_config, $m_table_config_altro);
    		        		 else
    		        		     echo m_get_fields($m_table_config);
		        		 ?>,
		        		
		        		},
		    		
			        columns: [ <?php m_get_columns($m_table_config) ?> ],  
					
						listeners: {
						
					      	 selectionchange: function(selModel, selected) { 
	                           if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().reset();
            		               rec_index = selected[0].index;
            		               form_dx.getForm().findField('rec_index').setValue(rec_index);
            	                   acs_form_setValues(form_dx, selected[0].data);
            	                 //  form_dx.getForm().setValues(selected[0].data);
            	                }
            	                
		       		  	 	 }
		       		  	 	 <?php if($m_params->autoload == 'firstRec'){?>
		       		  	 	 ,'afterrender': function(comp){
		       		  	 	    var win = comp.up('window');
								comp.store.on('load', function(store, records, options) {
								   var form_dx = this.up('panel').down('#dx_form');
            		               form_dx.getForm().setValues(records[0].data);
								}, comp);
    						}
    						<?php }?>
		       		  	 	,itemcontextmenu : function(grid, rec, node, index, event) {
				  				event.stopEvent();
				  													  
						 		var voci_menu = [];
					     		var row = rec.data;
					     		var m_grid = this;
					     		
					     		id_selected = grid.getSelectionModel().getSelection();
					     		list_selected_id = [];
					     		for (var i=0; i<id_selected.length; i++){
					     			list_selected_id.push({
					     			   RRN : id_selected[i].get('RRN'),
					     			   TAREST : id_selected[i].get('TAREST')});
								}
					     
					 	 voci_menu.push({
			         		text: 'Sospendi/Attiva',
			        		iconCls : 'icon-divieto-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_sos_att',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          rec.set('TATP', jsonData.new_value);	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    				});
			    				
			    				
        			    
        				      var menu = new Ext.menu.Menu({
        				            items: voci_menu
        					}).showAt(event.xy);	
			    	
			    	},celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	loc_win = this.up('window');
					  	

					  		
					  	}
					  	}
					
					
					},
						viewConfig: {
        		        getRowClass: function(record, index) {	
        		        
        		        if(record.get('TATP') == 'S'){
        		           return ' colora_riga_rosso';
                		 }
		        														
		         }   
		    }
       
        }  
						
		
		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		             <?php if($m_params->hide_grid != 'Y'){?>
 		                title: <?php echo j("{$m_table_config['title_form']}{$desc}"); ?>,
 		            <?php }?>
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            frame: true,
 		            items: [ 
     		          	
 		            <?php if($m_table_config['TAID'] == 'CUCP' || $m_table_config['TAID'] == 'BCCG'){ ?>
                	{
                		xtype: 'tabpanel',
                		items: [
                		
                		{
        				xtype: 'panel',
        				title: 'Dettagli', 
        				autoScroll : true,
        				frame : true,
        				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
        				//width: 165,
        				items: [
        				
                <?php }?>
 		            
 		            {xtype : 'textfield',name : 'rec_index',hidden : true},
                    <?php m_get_comp_form($m_table_config, $m_params) ?>
                    
                     <?php if($m_table_config['TAID'] == 'CUAG'){?>

				    {
    					name: 'forn',
    					xtype: 'textfield',
    					hidden : true,
    											
    				   },
    				    
    				    { xtype: 'fieldcontainer'
                          , layout:
                              {	type: 'hbox'
                              , pack: 'start'
                        	  , align: 'stretch'
                                }
                          , items: [ 
                             {
            				name: 'c_forn',
            				fieldLabel : 'Fornitore',
            				xtype: 'displayfield',
            				anchor: '-15',
            										
            			   }, 
            			   {xtype: 'component',
                                flex : 1,
                            },
                          { xtype: 'button'
                         , margin: '0 0 0 0'
                         , anchor: '-5'	
                         , scale: 'small'
                         , iconCls: 'icon-search-16'
                         , iconAlign: 'top'
                         , width: 25			
                         , handler : function() {
                               var m_form = this.up('form').getForm();
                               var my_listeners = 
                                 { afterSel: function(from_win, row) {
                                     m_form.findField('forn').setValue(row.CFCD);
                                     m_form.findField('c_forn').setValue('['+row.CFCD+']');
                                     m_form.findField('d_forn').show();
                                     m_form.findField('d_forn').setValue('<b>'+row.CFRGS1+'</b>');
                                     from_win.close();
                                   }
                                 }
                             
                               acs_show_win_std('Anagrafica clienti/fornitori'
                                               , '../desk_utility/acs_anag_ricerca_cli_for.php?fn=open_tab'
                                               , {cf: 'F'}, 600, 500, my_listeners
                                               , 'icon-search-16');
                             }
                                    
                        }
                          
                          ]
                         },
				   
			            {
            				name: 'd_forn',
            				fieldLabel : '&nbsp;',
            				labelSeparator : '',
            				xtype: 'displayfield',
            				anchor: '-15'							
            			   },
                
                <?php }?>
                    
                  <?php if($m_table_config['TAID'] == 'XSPM'){?>

				{
                    xtype: 'combo',
        			name: 'cliente',
        			labelWidth : 120,
        			fieldLabel: 'Cliente',
        			minChars: 1,
        			allowBlank : false,	
                    store: {
                    	pageSize: 1000,
                    	proxy: {
        		            type: 'ajax',
        		            url : <?php echo acs_je('acs_get_select_json.php?select=search_cli_anag'); ?>,
        		            reader: {
        		                type: 'json',
        		                root: 'root',
        		                totalProperty: 'totalCount'
        		            },
        		              actionMethods: { read: 'POST'},
        		              doRequest: personalizza_extraParams_to_jsonData
        		        }, 
        		        fields: ['cod', 'descr', 'out_loc'],		             	
                    },
                    valueField: 'cod',                        
                    displayField: 'descr',
                    typeAhead: false,
                    hideTrigger: true,
                    anchor: '-15',
                    listConfig: {
                        loadingText: 'Searching...',
                        emptyText: 'Nessun cliente trovato',
                       //Custom rendering template for each item
                        getInnerTpl: function() {
                            return '<div class="search-item">' +
                                '<h3><span>{descr}</span></h3>' +
                                '[{cod}] {out_loc}' + 
                            '</div>';
                        }                
                        
                    },
                    pageSize: 1000
                }, 
                  <?php		      
                  $proc = new ApiProc();
				  echo $proc->get_json_response(
				      extjs_combo_dom_ris(array(
				          'risposta_a_capo' => true,
				          'label' =>'Variabile/(1)<br>Variante', 
				          'file_TA' => 'sys',
				          'tipo_opz_risposta' => array('output_domanda' => true,  
			                 'tipo_cf' => '',
				             'opzioni' => array(
				                  array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
				              )),
				          'dom_cf'  => 'var1',  'ris_cf' => 'van1'))
				  );
				  echo ",";
				  echo $proc->get_json_response(
				      extjs_combo_dom_ris(array(
				          'risposta_a_capo' => true,
				          'label' =>'Variabile/(2)<br>Variante',
				          'file_TA' => 'sys',
				          'tipo_opz_risposta' => array('output_domanda' => true, 
				             'tipo_cf' => '',
				             'opzioni' => array(
				                  array('id' => '',  'text' => 'Variante', 'taid' => '*RIS*')
				              )),
				          'dom_cf'  => 'var2',  'ris_cf' => 'van2'))
				      );
				  }?>
                    
                    
	                <?php if($m_table_config['TAID'] == 'CUCP'){?>
		    ]},
		    
		        {
				xtype: 'panel',
				title: 'Tipo rate1', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				    if($m_table_config['TAID'] == 'CUCP'){ 
                        
                        m_get_comp_form($m_table_config_rate1, $m_params);
                
                    }?>
            
            	]},{
				xtype: 'panel',
				title: 'Tipo rate2', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				    if($m_table_config['TAID'] == 'CUCP'){ 
                        
				        m_get_comp_form($m_table_config_rate2, $m_params);
                
                    }?>
            
            	]},{
				xtype: 'panel',
				title: 'Tipo rate3', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				    if($m_table_config['TAID'] == 'CUCP'){ 
                        
				        m_get_comp_form($m_table_config_rate3, $m_params);
                
                    }?>
            
            	]}
		    
		    ]}
		    
		    
		    
		    ],
		    <?php }elseif($m_table_config['TAID'] == 'BCCG'){ ?>
		        
		           ]},
		    
		        {
				xtype: 'panel',
				title: 'Agenti', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				    if($m_table_config['TAID'] == 'BCCG'){ 
                        
				        m_get_comp_form($m_table_config_agenti, $m_params);
                
                    }?>
            
            	]},
            	
            	{
				xtype: 'panel',
				title: 'Nazione', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				    if($m_table_config['TAID'] == 'BCCG'){ 
                        
				        m_get_comp_form($m_table_config_nazione, $m_params);
                
                    }?>
            
            	]},
            	
            	{
				xtype: 'panel',
				title: 'Zona', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				    if($m_table_config['TAID'] == 'BCCG'){ 
                        
				        m_get_comp_form($m_table_config_zona, $m_params);
                
                    }?>
            
            	]},
            	
            	{
				xtype: 'panel',
				title: 'Provincia', 
				autoScroll : true,
				frame : true,
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				items: [
				
				<?php 
				    if($m_table_config['TAID'] == 'BCCG'){ 
                        
				        m_get_comp_form($m_table_config_prov, $m_params);
                
                    }?>
            
            	]},
		    
		    ]}
		    
		    
		    
		    ],
		        
		    <?php }else{?>
		    ],
		<?php }?>
		
			 <?php if($m_params->only_view != 'Y'){?>		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
			     
			     '->',
			     
			     <?php if($m_params->from_ricerca != 'Y'){?>
			     
                  {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
        		
        		      Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?' , function(btn, text){
            	   		if (btn == 'yes'){
        				       Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
									},							        
							         success : function(result, request){
							        	var jsonData = Ext.decode(result.responseText);
        					         	if(jsonData.success == false){
					      	    		 	acs_show_msg_error(jsonData.msg_error);
							        	 	return;
				        			     }else{
				        			        form.getForm().reset();
							        	    var rec_index = grid.getStore().findRecord('TANR', form_values.TANR);
							       		    grid.getStore().remove(rec_index);	
        					         	}
						
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
					
					    	}
            	   		  });		    
							    
				        }

			     },  
			     <?php }?>
			      {
                     xtype: 'button',
                     <?php if($m_params->from_ricerca == 'Y'){
                            if($m_params->modifica == 'Y'){?>
                             hidden : true,
                            <?php }?>
                     text: 'Salva',
                     <?php }else{?>
                    text: 'Crea',
                    <?php }?>
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form_values = this.up('form').getValues();
 			          var form = this.up('form').getForm();
 			          var m_form = this.up('form');
	       			  var grid = this.up('form').up('panel').down('grid'); 
	       			  
		       		  <?php  if($m_params->from_ricerca == 'Y'){?>
                           var loc_win = this.up('window'); 
                      <?php }?>
	       	
 			        
 			         //if (form.isValid()){
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				tacor2 : <?php echo j(trim($m_params->tacor2)); ?>,
	        				tacor1 : <?php echo j($tacor1); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        var new_rec = jsonData.record;
					        if(jsonData.success == false){
					      	    acs_show_msg_error(jsonData.msg_error);
					      	    return false;
					        }else{
					        	
					        	 <?php  if($m_params->from_ricerca == 'Y'){?>
							         loc_win.fireEvent('afterSave', loc_win, new_rec);
								<?php }else{?>
								     <?php if ($m_table_config['msg_on_save'] == true){ ?>
			        	    			acs_show_msg_info('Inserimento completato');
		        			         <?php } ?>
    					        	 grid.getStore().add(new_rec);
    					        	 var rec_index = grid.getStore().findRecord('TANR', new_rec.TANR);
    			  					 grid.getView().select(rec_index);
    			  					 grid.getView().focusRow(rec_index);
			  					 <?php }?>
			  			      }
					       },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		          // }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Salva',
                    <?php  if($m_params->from_ricerca == 'Y' && $m_params->aggiungi == 'Y'){?>
                        hidden : true,
                    <?php }?>
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		               		
 			       			var form_values = this.up('form').getValues();
 			       			var form = this.up('form').getForm();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			var index = grid.getStore().findExact('RRN', form_values.RRN); 			       		
 			       		    var record_grid = grid.getStore().getAt(index);
 			       		    
 			       			<?php  if($m_params->from_ricerca == 'Y'){?>
                               var loc_win = this.up('window'); 
                            <?php }?>
 			       			
 			       			
 			       		
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        var new_rec = jsonData.record;
							           if(jsonData.success == false){
					      	    			acs_show_msg_error(jsonData.msg_error);
					      	    		return false;
					        			}else{
					        			
					        			   <?php  if($m_params->from_ricerca == 'Y'){?>
							                  loc_win.fireEvent('afterSave', loc_win, new_rec);
								          <?php }else{?>
								           
    					        			   <?php if ($m_table_config['msg_on_save'] == true){ ?>
    					        			   		acs_show_msg_info('Salvataggio eseguito');
    					        			   <?php } ?>
    					        			    record_grid.set(new_rec);
					        			   <?php }?>
					        			   
					        			}
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			               
			            }

			     }
			     ]
		   }]
		   
		   <?php }?>
		   
			 	 }  
		 ],
					 
					
					
	}
]
}

<?php 

exit; 

}






