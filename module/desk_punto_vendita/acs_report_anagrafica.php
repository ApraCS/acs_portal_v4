<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));



if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          
                 {
				     name: 'f_data_da'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data immissione cliente da'
				   , labelAlign: 'left'
				   , labelWidth: 150
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
				}, {
				     name: 'f_data_a'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data immissione cliente a'
				   , labelAlign: 'left'
				   , labelWidth: 150
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
			}
	            ],
	            
				buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	}


if ($_REQUEST['fn'] == 'open_report'){
    
    
    
    $ar_email_to = array();
    
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u){
        $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }
    
    $ar_email_json = acs_je($ar_email_to);

?>


<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv3 td{font-weight: bold; font-size: 0.9em;}   
   tr.liv_data th{background-color: #333333; color: white;}
   tr.fattura_vendita td{font-style: italic}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
   
   div.with_todo_tooltip{text-align: right;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>
  
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<?php


//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$form_values_f =	strtr($_REQUEST['filter'], array('\"' => '"'));
$form_values_f = json_decode($form_values_f);

$ar = array();

$filter_where = '';

//controllo data
if (strlen($form_values->f_data_da) > 0)
    $filter_where .= " AND CFDTGE >= {$form_values->f_data_da}";

if (strlen($form_values->f_data_a) > 0)
    $filter_where .= " AND CFDTGE <= {$form_values->f_data_a}";

    $filter_where.= sql_where_by_combo_value('CFITIN', $form_values_f->f_sede);

    /*SELECT CF.*,  TD.M_SEDE AS SEDE
            FROM QS36F.XX0S2CF0 CF
           LEFT OUTER JOIN (
               SELECT MAX(TDITIN) AS M_SEDE, TDDT, TDCCON
               FROM QS36F.XX0S2TD0
               GROUP BY TDDT, TDCCON) TD
            ON CF.CFDT = TD.TDDT AND CF.CFCD = TD.TDCCON
            WHERE CFDT = '6 ' AND CFTICF = 'C' AND CF.CFITIN = ''*/
 


    $sql = "SELECT *
            FROM {$cfg_mod_DeskPVen['file_anag_cli']} CF
            WHERE CFDT = '$id_ditta_default' AND CFTICF = 'C' {$filter_where}
            ORDER BY CFDTGE, CFITIN, CFLOC1, CFRGS1";
     

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

while ($row = db2_fetch_assoc($stmt)) {
    
    
    $nr = array();
    if(trim($row['CFITIN']) != '')
        $nr['sede'] 	=  "[".trim($row['CFITIN']) ."] ".$s->decod_std('START', trim($row['CFITIN']));
    else
        $nr['sede'] = "";

    $nr['cliente']= $row['CFCD'];
    $nr['d_cli']= $row['CFRGS1'];
    $nr['data']= $row['CFDTGE'];
    $nr['loca']= $row['CFLOC1'];
    $ta_sys = find_TA_sys('BORI', trim($row['CFORIG']));
    if(trim($row['CFORIG']) != '')
        $nr['origine'] 	=  "[".trim($row['CFORIG'])."] ".$ta_sys[0]['text'];
    else
        $nr['origine'] = "";
            
    $ta_sys2 = find_TA_sys('CUS3', trim($row['CFSEL3']));
    if(trim($row['CFSEL3']) != '')
        $nr['tipologia'] 	=  $ta_sys2[0]['text'];
    else
        $nr['tipologia'] = "";
            
        $nr['utente']= $row['CFUSGE'];
    $ar[] = $nr;
	
	/*$tmp_ar_id = array();
	$ar_r= &$ar;

	
	$cod_liv0 = trim($row['CFITIN']); //SEDE
	$cod_liv1 = trim($row['CFCD']); //cliente

	//SEDE
	$liv=$cod_liv0;
	//$tmp_ar_id[] = $liv;
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = trim($row['CFITIN']);
		if(trim($row['CFITIN']) != "")
		   $ar_new['task'] 	= "[".trim($row['CFITIN']) ."] ".$s->decod_std('START', trim($row['CFITIN']));
		else 
		  $ar_new['task'] = ""; 	
		    
		 $ar_new['liv'] = 'liv_1';
	
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];


	//chiave cliente
	$liv=$cod_liv1;
	$ar_r=&$ar_r['children'];
	$tmp_ar_id[] = $liv;
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['task']= $row['CFCD'];
		$ar_new['d_cli']= $row['CFRGS1'];
		$ar_new['data']= $row['CFDTGE'];
		$ar_new['loca']= $row['CFLOC1'];
		$ta_sys = find_TA_sys('BORI', trim($row['CFORIG']));
		if(trim($row['CFORIG']) != '')
		  $ar_new['origine'] 	=  "[".trim($row['CFORIG'])."] ".$ta_sys[0]['text'];
		else
		  $ar_new['origine'] = "";
		
		 $ta_sys2 = find_TA_sys('CUS3', trim($row['CFSEL3']));
		if(trim($row['CFSEL3']) != '')
		  $ar_new['tipologia'] 	=  $ta_sys2[0]['text'];
		else
		  $ar_new['tipologia'] = "";
	
		$ar_new['utente']= $row['CFUSGE'];
		$ar_new['liv'] = 'liv_2';
		
	
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];*/
}




echo "<div id='my_content'>";

echo "
  		<div class=header_page>
			<H2>Registro anagrafiche</H2>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>";

echo "<table class=int1>";
echo "<tr class=liv_data><th>Punto vendita</th>
        <th>Cliente</th>
		<th>Denominazione</th>
		<th>Localit&agrave;</th>	
	 	<th>Origine</th>	
		<th>Codice classificazione</th>
        <th>Data immissione</th>	
		<th>Utente immissione</th>
		</tr>";


foreach($ar as $kar => $r){
	
		/*echo "<tr class=liv_totale><td>".$r['task']. "</td>
   		          <td>&nbsp;</td>
				  <td>&nbsp;</td>
			      <td>&nbsp;</td>
				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
                  <td>&nbsp;</td>
    		      </tr>";
	
		foreach($r['children'] as $kar1 => $r1){*/
			
			echo "<tr>
                  <td>".$r['sede']. "</td>
                  <td>".$r['cliente']. "</td>
   		          <td>".$r['d_cli']. "</td>
   				  <td>".$r['loca']. "</td>
			  	  <td>".$r['origine']. "</td>
				  <td>".$r['tipologia']. "</td>
                  <td>".print_date($r['data']). "</td>
                  <td>".$r['utente']. "</td>
    		      </tr>";
			
			
	
	
   //}

}

echo "</table>";


?>

</div>
</body>
</html>

<?php 
exit;
}

