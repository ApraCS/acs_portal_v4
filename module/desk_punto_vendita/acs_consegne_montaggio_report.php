<?php
require_once "../../config.inc.php";
require_once("acs_consegne_montaggio_include.php");

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$da_form = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
   
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          
                 {
				     name: 'f_data_da'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data iniziale'
				   , value: '<?php echo  print_date($m_params->filter->f_data, "%d/%m/%Y"); ?>'
				   , labelAlign: 'left'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
				}, {
				     name: 'f_data_a'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data finale'
				   , value: '<?php echo  print_date($m_params->filter->f_data_a, "%d/%m/%Y"); ?>'
				   , labelAlign: 'left'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
			}
	            ],
	            
				buttons: [
				
				{
	         	xtype: 'splitbutton',
	            text: 'Schede',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	{
		        	    xtype: 'button',
			            text: 'Scheda di carico',
				        iconCls: 'icon-print-16',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: 'acs_delivery_pos_carico_report.php?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        },{
		        	    xtype: 'button',
			            text: 'Scheda autorizzazione resi',
				        iconCls: 'icon-print-16',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: 'acs_delivery_pos_carico_report.php?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                            form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>',
		                        	resi : 'Y'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        	
		        	]}}
				
					
				   ,	{xtype: 'tbfill'},			
					{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}

// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
	
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD; font-weight: bold} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   tr.bold td{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values_f =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values =	strtr($_REQUEST['filter'], array('\"' => '"'));


$form_values = json_decode($form_values);
$form_values_f = json_decode($form_values_f);


$form_values->f_data_a = $form_values_f->f_data_a; 
$form_values->f_data = $form_values_f->f_data_da; 


$ar=crea_ar_tree_consegne_montaggio('', $form_values, 'Y'); //forza generazione completa

$sql = "SELECT * FROM {$cfg_mod_DeskPVen['fatture_anticipo']['file_testate']}
WHERE TFNRDO LIKE '{$m_params->open_request->chiave}%' ORDER BY TFDTGE DESC, TFORGE DESC FETCH FIRST 1 ROWS ONLY";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
$r = db2_fetch_assoc($stmt);
$data_ora = print_date($r['TFDTGE']) . " " . print_ora($r['TFORGE']);



echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>Programmazione consegne/montaggio</H2>
 		</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";

echo "<table class=int1>";

echo "<tr class='liv_data'>
		<th>Settimana</th>
		<th><img src=" . img_path("icone/48x48/divieto.png") . " height=20></th>
  		<th><img src=" . img_path("icone/48x48/power_black.png") . " height=20></th>
  		<th><img src=" . img_path("icone/48x48/info_blue.png") . " height=20></th>
		<th> Localit&agrave;/Riferimento </th>
		<th> Tp </th>
		<th> Data</th>
		<th> St </th>
		<th> Pr </th>
   		<th> Consegna richiesta </th>";
        if($js_parameters->only_view != 1){
   		   echo "<th> Importo ordini </th>";
        }
   		echo "<th> Fatture </th>";
   		if($js_parameters->only_view != 1){
   		   echo "<th> Importo merci </th>";
   		}
   		echo "</tr>";


foreach ($ar as $kar => $r){
	
	echo "<tr class ='liv3'> <td>".$r['task']." </td>";
    get_img_flag($r);
	echo " <td>&nbsp;</td>
  		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>
  		 <td>&nbsp;</td>
   		  <td>&nbsp;</td>";
	 if($js_parameters->only_view != 1){
   		  echo "<td class='number'>". n($r['importo'],2)."</td>";
     }
        echo "<td>&nbsp;</td>";
     if($js_parameters->only_view != 1){
    	echo "<td>&nbsp;</td>";
        }              
                            
  		echo "</tr>";
	
	
	
	foreach ($r['children'] as $kar1 => $r1){

	echo "<tr class ='liv2'> <td>".$r1['task']." </td>";
	get_img_flag($r1);
  	echo "<td>&nbsp;</td>
  		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>
  		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>";
  	if($js_parameters->only_view != 1){
   	    echo "<td class='number'>". n($r1['importo'],2)."</td>";
  	}    
    	  echo "<td>&nbsp;</td>";
    if($js_parameters->only_view != 1){
          echo "<td>&nbsp;</td>";
    }
       echo "</tr>";
		
	
	if(is_array($r1['children'] )){
		
		foreach ($r1['children'] as $kar2 => $r2){
			
			echo "<tr class='liv1'><td>".$r2['task'];
			get_img_flag($r2);
			echo "<td>".$r2['riferimento']."</td>
		  		  <td>&nbsp;</td>
		   		  <td>&nbsp;</td>
		   		   <td>&nbsp;</td>
		  		   <td>&nbsp;</td>
		   		  <td>&nbsp;</td>";
			if($js_parameters->only_view != 1){
		   		echo "<td class='number'>". n($r2['importo'],2)."</td>";
			}		
			     echo "<td>&nbsp;</td>";
		    if($js_parameters->only_view != 1){
			     echo "<td>&nbsp;</td>";
		    }    
			     echo "</tr>";
		
			foreach ($r2['children'] as $kar3 => $r3){
				
				echo "<tr class = 'bold'><td>".$r3['task'] . "</td>";
				get_img_flag($r3);
				echo "<td>".$r3['riferimento']."</td>
			  		  <td>&nbsp;</td>
			   		  <td>&nbsp;</td>
			   		  <td>&nbsp;</td>
			  		  <td>&nbsp;</td>
			   		  <td>&nbsp;</td>";
				if($js_parameters->only_view != 1){
			   		echo  "<td class='number'>". n($r3['importo'],2)."</td>";
				}
				echo "<td class='number'>". n($r3['fat_merce'],2)."</td>";
				if($js_parameters->only_view != 1){
				    echo "<td class='number'>". n($r3['importo_fat'],2)."</td>";
				}
				echo "</tr>";
	
				foreach ($r3['children'] as $kar4 => $r4){
				
					echo "<tr><td class = 'number'>".$r4['task'] . "</td>";
				    get_img_flag($r4);
					echo " <td>&nbsp;</td>
					  		  <td>".$r4['tipo']."</td>
					   		  <td>".print_date($r4['data_reg'])."</td>
					   		  <td>". $r4['stato']."</td>
					  		  <td>".$r4['priorita']."</td>
					   		  <td>".print_date($r4['cons_rich'])."</td>";
					if($js_parameters->only_view != 1){
					   		 echo "<td class='number'>". n($r4['importo'],2)."</td>";
					} 		 
					  	   	 echo "<td>&nbsp;</td>"; 
					 if($js_parameters->only_view != 1){
  		                     echo "<td>&nbsp;</td>";
					  }
                            
                            echo "</tr>";
				
				
				
				}
				
				
				
			}
			
		}
		
	}
	
	}
	
	
}
}


function get_img_flag($r){
	
	if($r['fl_cli_bloc'] > 0){
		echo "<td><img src=" . img_path("icone/48x48/divieto.png") . " height=15></td>";
	}else{
		echo "<td>&nbsp;</td>";
	}
	
	if($r['fl_bloc'] == 4){
		echo "<td><img src=" . img_path("icone/48x48/power_black_blue.png") . " height=15></td>";
	}else if($r['fl_bloc'] == 3){
		echo "<td><img src=" . img_path("icone/48x48/power_black.png") . " height=15></td>";
	}else if($r['fl_bloc'] == 2){
		echo "<td><img src=" . img_path("icone/48x48/power_blue.png") . " height=15></td>";
	}elseif($r['fl_bloc'] == 1){
		echo "<td><img src=" . img_path("icone/48x48/power_gray.png") . " height=15></td>";
	}else{
		echo "<td>&nbsp;</td>";
	}
	
	if($r['fl_art_manc'] == 5){
		echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
	}else if($r['fl_art_manc'] == 20){
	    echo "<td><img src=" . img_path("icone/48x48/shopping_cart_red.png") . " height=15></td>";
	}else if($r['fl_art_manc'] == 4){
		echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
	}else if($r['fl_art_manc'] == 3){
		echo "<td><img src=" . img_path("icone/48x48/shopping_cart.png") . " height=15></td>";
	}elseif($r['fl_art_manc'] == 2){
		echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
	}elseif($r['fl_art_manc'] == 1){
		echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
	}else{
		echo "<td>&nbsp;</td>";
	}
	

	
	

	
}







