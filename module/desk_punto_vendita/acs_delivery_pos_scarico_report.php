<?php
require_once "../../config.inc.php";
require_once("acs_consegne_montaggio_include.php");

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
    
    $week = $_REQUEST['settimana'];
    $year = $_REQUEST['anno'];
    
    
    $ar_email_to = array();
    
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u){
        $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }
    
    $ar_email_json = acs_je($ar_email_to);
    ?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   table.info tr,table.info td {border: 0px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD; font-weight: bold} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{background-color: #DDDDDD; font-weight: bold;}
   tr.bold td{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">


  function allegatiPopup(apri)
  {
      gmap_poup_stile = "top=10, left=10, width=600, height=600, status=no, menubar=no, toolbar=no scrollbars=no";
      window.open(apri, "", gmap_poup_stile);
  }
  
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);



if (isset($_REQUEST['rec_id'])){
    $ar_rec_id = explode("|", $_REQUEST['rec_id']);
    $ar_data_vettore = explode("_", $ar_rec_id[1]);
    $form_values->f_only_data =    $ar_data_vettore[0];
    $form_values->f_vettore = $ar_data_vettore[1];
}


$tot_generale = '';
$ar=crea_ar_tree_consegne_montaggio('', $form_values, 'Y'); //forza generazione completa

$data_ora =  print_date(oggi_AS_date())." ". print_ora(oggi_AS_time());

echo "<div id='my_content'>";



foreach ($ar as $kar => $r){
	
	foreach ($r['children'] as $kar1 => $r1){

	
	if(is_array($r1['children'] )){
		
		foreach ($r1['children'] as $kar2 => $r2){
			
		    echo "
		    <div class=header_page>
		    <H2>Scheda di scarico del ".$r2['data_vett']."</H2>
            <p class = 'number'>".$r2['vettore']." - Nr carico ".$r2['nr_carico']."</p>
		    </div>
		    <div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";
		    
		    echo "<table class=int1>";
		    
		    if($js_parameters->only_view == 1){ 
		        
		        echo "<tr class='liv_data'>
                  <th> Seq.</th>
		          <th>Data/Vettore/Cliente</th>
		          <th> Localit&agrave;/Riferimento <br> Informazioni logistiche</th>
		          <th> Telefono </th>
                  <th> Ordine </th>
                  <th> Data</th>
                  <th> Fattura </th>
                  <th> Data fattura </th>";
		      
		        echo "</tr>";
		        
		    }else{
		    
		    echo "<tr class='liv_data'>
                  <th rowspan=2> Seq.</th>
		          <th rowspan=2>Data/Vettore/Cliente</th>
		          <th rowspan=2> Localit&agrave;/Riferimento <br> Informazioni logistiche</th>
		          <th rowspan=2> Telefono </th>
                  <th rowspan=2> Ordine </th>
                  <th rowspan=2> Data</th>
                  <th rowspan=2> Fattura </th>
                  <th rowspan=2> Importo merce ivato </th>
                   <th rowspan=2> Netto a <br> pagare<br> </th>
                  <th colspan = 2 class='center'> Costo trasporto </th>
                  <tr class='liv_data'>
		                      <th>Carico cliente</th>
		                      <th>NS carico</th></tr>";
                
		    echo "</tr>";
		    }  
			foreach ($r2['children'] as $kar3 => $r3){
			    
			      echo "<tr><td valign=top><b>".$r3['seq_carico']."</b></td>
                        <td valign=top><b>".$r3['task'] . "</b></td>";
			    
				echo "<td>".$r3['riferimento']."<br>".$r3['indirizzo']."</td>
			  		 <td valign=top>".$r3['tel']."</td>
			   		  <td>&nbsp;</td>
			  		  <td>&nbsp;</td>
			   		  <td>&nbsp;</td>
			   		  <td>&nbsp;</td>";
				if($js_parameters->only_view != 1){ 
			     echo "<td>&nbsp;</td>";
				 echo "<td>&nbsp;</td>";
			     echo "<td>&nbsp;</td>";
				}
			     echo "</tr>";
	
			     $count = 0;
				foreach ($r3['children'] as $kar4 => $r4){
				    $count++;
				   
				     
				    
				    $commento_txt = $s->get_commento_ordine_by_k_ordine($r4['k_ordine'], 'DEL');
				    $annotazioni = array();
					echo "<tr><td>&nbsp;</td>";
					echo " <td>&nbsp;</td>";
					
					if($count == 1){
					    echo "<td>
                                <table class = 'info'>
                                <tr>";
					    
					    if(risposta_PVNOR_nota($id_ditta_default, $r4['k_ordine'], 'SEN') != '')
					        echo "<tr>";
					        echo "<td valign = top><b>".risposta_PVNOR_nota($id_ditta_default, $r4['k_ordine'], 'SEN')." <b></td>";
					        echo "</tr>";
					        
				        if(risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '01') != '')
				            echo "<tr>";
				            echo "<td valign=top>".decod_risposta('01', risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '01'))." ".risposta_PVNOR_nota($id_ditta_default, $r4['k_ordine'], '01')." </td>";
				            echo "</tr>";
			            if(risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '02') != '')
			                echo "<tr>";
			                echo "<td valign=top>".decod_risposta('02', risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '02'))." ".risposta_PVNOR_nota($id_ditta_default, $r4['k_ordine'], '02')." </td>";
			                echo "</tr>";
		                if(risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '03') != '')
		                    echo "<tr>";
		                    echo "<td valign=top>".decod_risposta('03', risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '03'))." ".risposta_PVNOR_nota($id_ditta_default, $r4['k_ordine'], '03')." </td>";
		                    echo "</tr>";
	                    if(risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '04') != '')
	                        echo "<tr>";
	                        echo "<td valign=top>".decod_risposta('04', risposta_PVNOR($id_ditta_default, $r4['k_ordine'], '04'))." ".risposta_PVNOR_nota($id_ditta_default, $r4['k_ordine'], '04')." </td>";
	                        echo "</tr>";
					                        
                        echo "</table></td>";
					    
					}else{
					    echo "<td>&nbsp;</td>";
					    
					}
					
					   
					   	  echo "<td>&nbsp;</td>
					   		  <td valign=top>".$r4['k_ord_report']."</td>
					  		  <td valign=top>".print_date($r4['data_reg'])."</td>";
					   	  if(strlen($r4['fattura']) > 0)
					   	  echo "<td valign=top>".$r4['fattura']." del ".print_date($r4['data_fat'])."
                                <br/>
                                <span class=noPrint>".print_pdf_link(get_path_attachment_fattura($r3['cod_cli'], $r4['k_ordine_fatt'])) ."</span>
                              </td>";
					   	  else 
					   	      echo "<td>&nbsp;</td>";
					   	  if($r4['imp_merce_ivato'] > 0)
					   		echo "<td class = 'number' valign=top>".n($r4['imp_merce_ivato'],2)."</td>";
					   	  else
					   	    echo "<td>&nbsp;</td>";
                          if($js_parameters->only_view != 1){ 
                           if($r4['imp_fatt'] > 0)
                              echo "<td class = 'number' valign=top>".n($r4['imp_fatt'],2)."</td>";
                           else
                              echo "<td>&nbsp;</td>";
                          
                              
                              if($r4['trasp_cli'] > 0)
                                  echo "<td class = 'number' valign=top>".n($r4['trasp_cli'],2)."</td>";
                              else
                               echo "<td>&nbsp;</td>";
                              
                               if($r4['trasp_ns'] > 0)
                                   echo "<td class = 'number' valign=top>".n($r4['trasp_ns'],2)."</td>";
                               else
                                   echo "<td>&nbsp;</td>";
                          }
                            echo "</tr>";
                            
                            for($i=1; $i<= count($commento_txt);$i++){ 
                                
                                $annotazioni[]  = trim($commento_txt[$i]['text']);
                                
                            }
                            
                            $ann = implode(", " , $annotazioni);
                            
                            if(count($commento_txt) > 0)
                                echo  "<tr><td colspan = 11>Annotazioni : ".$ann."</td></tr>";
                          
				
				
				}
				
			}
			
			if($js_parameters->only_view != 1){ 
			echo "<tr>
                  <td colspan = 8 ><b>TOTALE GENERALE</b></td>
                  <td class = number>".n($r2['imp_fatt'],2)."</td>
                  <td class = number>".n($r2['trasp_cli'],2)."</td>
                  <td class = number>".n($r2['trasp_ns'],2)."</td>
                  </tr>"	;
			}
			
			echo "</table>";
			echo "<div class=\"page-break\"></div>";
			
		}
		
	}
	
	}
	
	
}
}



function decod_risposta($domanda, $risposta){
    global $s;
    $ar=$s->get_TA_std('PRVAN', $domanda, $risposta);
    return $ar['TADESC'];
}

function risposta_PVNOR($ditta, $tddocu, $domanda){
    
    global $conn;
    global $cfg_mod_DeskPVen;
    
    $sql="SELECT * FROM {$cfg_mod_DeskPVen['file_tabelle']}
    WHERE TADT='{$ditta}' AND TATAID='PVNOR' AND TAINDI='{$tddocu}' AND TAKEY2='{$domanda}' ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    return $row['TAKEY3'];
    
}

function risposta_PVNOR_nota($ditta, $tddocu, $domanda){
    
    global $conn;
    global $cfg_mod_DeskPVen;
    
    $sql="SELECT * FROM {$cfg_mod_DeskPVen['file_tabelle']}
    WHERE TADT='{$ditta}' AND TATAID='PVNOR' AND TAINDI='{$tddocu}' AND TAKEY2='{$domanda}' ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    return $row['TAMAIL'];
    
}


