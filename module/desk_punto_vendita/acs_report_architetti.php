<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

function sum_columns_value(&$ar_r, $r){
	
	$ar_r['importo']  += $r['TDINFI']/122*100;
	$ar_r['imponibile']  += $r['TDINFI']/122*100;
	$ar_r['tot_doc']  += $r['TDINFI'];


}


?>


<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   

   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv3 td{font-weight: bold; font-size: 0.9em;}   
   tr.liv_data th{background-color: #333333; color: white;}
   tr.fattura_vendita td{font-style: italic}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
   
   div.with_todo_tooltip{text-align: right;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>
  
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<?php


//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$ar = array();

$filter_where = '';



//controllo data
if (strlen($form_values->f_data) > 0)
	$filter_where .= " AND TDODRE >= {$form_values->f_data}";

if (strlen($form_values->f_data_a) > 0)
	$filter_where .= " AND TDODRE <= {$form_values->f_data_a}";

	$filter_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
	$filter_where.= sql_where_by_combo_value('TD.TDOTPD', $form_values->f_tipo_ordine);
	$filter_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);
	$filter_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);
	$filter_where.= sql_where_by_combo_value('TD.TDSTAB', $form_values->f_sede);
	$filter_where.= sql_where_by_combo_value('TD.TDCAG2', $form_values->f_architetto);
	$filter_where.= sql_where_by_combo_value('TD.TDVETT', $form_values->f_vettore);

	$sql = "SELECT TDDOCU, TDOADO, TDONDO, TDODRE, TDOTPD, TDDOTD, TDSTAT, TDDSST, TDTIMP, TDTOTD,
	TDINFI, TDCAG2, TDDAG2, TDCCON, TDDCON, TDCLOR, TDSTAB, TDDT
	FROM  {$cfg_mod_DeskPVen['file_testate']} TD
	WHERE ". $s->get_where_std()." {$filter_where}
	ORDER BY TDDAG2, TDDCON, TDODRE DESC";


	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);



while ($row = db2_fetch_assoc($stmt)) {
	
	

	$tmp_ar_id = array();
	$ar_r= &$ar;

	//stacco dei livelli
	$cod_liv_tot = 'TOTALE';	//totale
	$cod_liv0 = trim($row['TDCAG2']); //architetto
	$cod_liv1 = trim($row['TDCCON']); //codice cliente
	$cod_liv2 = trim($row['TDDOCU']); //chiave documento

	

	//LIVELLO TOTALE
	$liv = $cod_liv_tot;
	if (!isset($ar_r["{$liv}"])){
		$ar_new = array();
		$ar_new['id'] = 'liv_totale';
		$ar_new['liv_cod'] = 'TOTALE';
		$ar_new['task'] = 'Totale';
		$ar_new['liv'] = 'liv_totale';
		$ar_r["{$liv}"] = $ar_new;
	}
	$ar_r = &$ar_r["{$liv}"];
	sum_columns_value($ar_r, $row);

	//ARCHITETTO
	$liv=$cod_liv0;
	$ar_r = &$ar_r['children'];
	$tmp_ar_id[] = $liv;
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['task'] = acs_u8e($row['TDDAG2']);
		$ar_new['liv'] = 'liv_1';
	
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];
	sum_columns_value($ar_r, $row);

	//creo cliente
	$liv=$cod_liv1;
	$ar_r=&$ar_r['children'];
	$tmp_ar_id[] = $liv;
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['task']= $row['TDDCON'];
		$ar_new['liv'] = 'liv_2';
			
		$ar_new['children'] = array();
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];
	sum_columns_value($ar_r, $row);


	//chiave documento
	$liv=$cod_liv2;
	$ar_r=&$ar_r['children'];
	$tmp_ar_id[] = $liv;
	$task=array();
	$task[0]=$row['TDOADO'];
	$task[1]=$row['TDONDO'];
	$task[2]=$row['TDDT'];
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['task']= implode("_", $task);
		$ar_new['data'] = $row['TDODRE'];
		$ar_new['tipologia'] = $row['TDCLOR'];
		$ar_new['sede'] = $row['TDSTAB'];
		$ar_new['stato']= "[".trim($row['TDSTAT'])."] ".$row['TDDSST'];
		$ar_new['tipo'] = "[".trim($row['TDOTPD'])."] ".$row['TDDOTD'];
		$ar_new['liv'] = 'liv_3';
		
	
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];
	sum_columns_value($ar_r, $row);
}




echo "<div id='my_content'>";

echo "
  		<div class=header_page>
			<H2>Elenco ordini per architetto</H2>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>";

echo "<table class=int1>";
echo "<tr><th>Architetto/Cliente/Ordine</th>
		<th>Data</th>
		<th>Stato</th>
		<th>Tipo</th>	
	 	<th>Tipologia</th>	
	 	<th>Sede</th>	
		<th class=number>Importo merce</th>
		<th class=number>Imponibile</th>
		<th class=number>Totale</th>
		</tr>";

foreach($ar as $kar => $r){
	
		echo "<tr class=liv_totale><td>".$r['liv_cod']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			      <td>&nbsp;</td>
				  <td>&nbsp;</td>
   		          <td class=number>".n($r['importo'])."</td>	
    		      <td class=number>".n($r['imponibile'])."</td>
    		      <td class=number>".n($r['tot_doc'])."</td>
   		 		  </tr>";
	
		foreach($r['children'] as $kar1 => $r1){
			
			echo "<tr class=liv3><td>".$r1['task']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			  	  <td>&nbsp;</td>
				  <td>&nbsp;</td>
   		          <td class=number>".n($r1['importo'])."</td>	
   		          <td class=number>".n($r1['imponibile'])."</td>
    		      <td class=number>".n($r1['tot_doc'])."</td>
   		          </tr>";
			
			
			foreach($r1['children'] as $kar2 => $r2){
					
				echo "<tr class=liv2><td>".$r2['task']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			      <td>&nbsp;</td>
				  <td>&nbsp;</td>
   		          <td class=number>".n($r2['importo'])."</td>	
 		          <td class=number>".n($r2['imponibile'])."</td>
    		      <td class=number>".n($r2['tot_doc'])."</td>
		          </tr>";
				
				if(is_array($r2['children'] )){
				
				foreach($r2['children'] as $kar3 => $r3){  //ORDINE
						
					echo "<tr><td>".$r3['task']. "</td>
   		         	 <td>".print_date($r3['data'])."</td>
   				 	 <td>".$r3['stato']."</td>
 				     <td>".$r3['tipo']."</td>
			         <td>".$r3['tipologia']."</td>
 				     <td>".$r3['sede']."</td>
				     <td class=number>".n($r3['importo'])."</td>	
 		          	 <td class=number>".n($r3['imponibile'])."</td>
    		         <td class=number>".n($r3['tot_doc'])."</td>
				     </tr>";
			
				} 
				
				}
			
		}
	
   }

}

echo "</table>";


?>

</div>
</body>
</html>

