<?php

require_once "../../config.inc.php";

ini_set('max_execution_time', 3000);

$main_module = new DeskPVen();
 
//funcioni generiche
switch($_REQUEST['fn']) {
	case "upd_commento_ordine": 
		$result = $main_module->exe_upd_commento_ordine($_REQUEST);		
		echo "{success: true}";
		exit();
		break;
    case "toggle_data_confermata":
		$result = $s->exe_toggle_data_conferma(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;
	case "exe_crea_segnalazione_arrivi":
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	    $result = $s->exe_crea_segnalazione_arrivi($_REQUEST);
	    echo $result;
	    exit();
	    break;
	case "exe_avanzamento_segnalazione_arrivi":
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	    $result = $s->exe_avanzamento_segnalazione_arrivi($_REQUEST);
	    echo $result;
	    exit();
	    break;
	case "exe_send_email":
	    include "send_email.php";
	    exit();
	    break;
	case "assegna_carico":
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	    $result = $s->exe_assegna_carico(acs_m_params_json_decode());
	    echo $result;
	    exit();
	    break;
	case "assegna_progressivo_carico":
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	    $result = $s->exe_assegna_progressivo_carico(acs_m_params_json_decode());
	    echo $result;
	    exit();
	    break;	
	case "sincronizza_ordine":
	    $s = new Spedizioni(array('no_verify' => 'Y'));
	    $result = $s->exe_sincronizza_ordine(acs_m_params_json_decode());
	    echo $result;
	    exit();
	    break;
	case "evidenza_generica":
	    $result = $main_module->exe_evidenza_generica(acs_m_params_json_decode());
	    echo $result;
	    exit();
	    break;
}
	
