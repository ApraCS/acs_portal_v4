<?php

require_once("../../config.inc.php");

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$m_params = acs_m_params_json_decode();

function sum_columns_value(&$ar_r, $r){
    
    $ar_r['importo'] += $r['TDTIMP'];
    $ar_r['count']++;
    
 
}

if ($_REQUEST['fn'] == 'exe_avanzamento_segnalazione_arrivi_json'){
    
    $result = $s->exe_avanzamento_segnalazione_arrivi_json(acs_m_params_json_decode());
    echo $result;
    exit();
}

// ******************************************************************************************
// RECUPERO FLAG RILASCIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_richiesta_record_rilasciato'){
   $prog = $m_params->prog;
    
    $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_assegna_ord']} 
            WHERE ASIDPR = ?";
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt, array($prog));
    
    $r = db2_fetch_assoc($stmt);
    $ret = array();
    $ret['success'] = true;
    $ret['ASFLRI'] = $r['ASFLRI'];
    echo acs_je($ret);
    exit;
} //get_json_data


if ($_REQUEST['fn'] == 'exe_visual'){
    
    $m_params = acs_m_params_json_decode();
    
    //$k_ordine = $m_params->k_ordine;
    $prog = $m_params->prog;
             
    if (trim($m_params->flag) == '')  //elseif (trim($m_params->flag) == 'Y') $m_flag = 'Z';
        $m_flag = 'Y';
    else
        $m_flag = '';
            
            
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_assegna_ord']}
            SET ASFVIU = '{$m_flag}'
            WHERE /*ASDOCU = '{$k_ordine}' AND*/ ASIDPR = '{$prog}'";
    
    //print_r($sql);
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    if($m_flag == 'Y'){
        $ar_ins = array();
        
        $ar_ins['AHUSGE'] 	= $auth->get_user();
        $ar_ins['AHDTGE']   = oggi_AS_date();
        $ar_ins['AHHMGE']   = oggi_AS_time();
        $ar_ins['AHDT'] 	= $id_ditta_default;
        $ar_ins['AHIDPR'] 	= $prog;
        $ar_ins['AHATES'] 	= 'VIS';
    
        $sql_h = "INSERT INTO {$cfg_mod_DeskPVen['file_history_att']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
      
        $stmt_h = db2_prepare($conn, $sql_h);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_h, $ar_ins);
    }
    
    
    $ret = array();
    $ret['success'] = true;
    $ret['flag'] = $m_flag;
    echo acs_je($ret);
    
    exit;
}

// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    $sql_where = "";
    
    if(isset($form_values->f_todo) && strlen($form_values->f_todo) > 0)
        $sql_where .= " AND ASCAAS = '{$form_values->f_todo}'";
        
    if(isset($form_values->f_utente_assegnato) && strlen($form_values->f_utente_assegnato) > 0)
        $sql_where .= " AND ASUSAT = '{$form_values->f_utente_assegnato}'";
            
    if(count($form_values->f_todo_ev) > 0){
        $show_rilav = 'Y';
        $sql_where .= " AND ASFLRI = 'Y'";
        $sql_where .= sql_where_by_combo_value('ASCARI', $form_values->f_todo_ev);
    }elseif(strlen($m_params->k_ordine) > 0){
        $sql_where .= " AND ASDOCU = '{$m_params->k_ordine}'";
    }else{
        $show_rilav = 'N';
        $sql_where .= " AND ASFLRI <> 'Y'";
    }
    
    if(isset($form_values->f_data_scad) && strlen($form_values->f_data_scad) > 0){
        $sql_where .= " AND ASDTSC <= {$form_values->f_data_scad}";
        
    }
            
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.*, TD.*, NT_MEMO.NTMEMO AS MEMO, UT.UTDESC AS D_UTENTE
            FROM {$cfg_mod_Spedizioni['file_assegna_ord']} ATT_OPEN
            INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ATTAV
                ON ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1
            INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
                ON ATT_OPEN.ASDOCU = TD.TDDOCU
            LEFT OUTER JOIN {$cfg_mod_Admin['file_utenti']} UT
                ON ATT_OPEN.ASUSAT = UT.UTCUTE
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
            ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
            WHERE TDDT = '{$id_ditta_default}'
            AND TA_ATTAV.TARIF1 = 'POS' {$sql_where}";
    
       
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    
 
            
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
     
        //stacco dei livelli
        $cod_liv0 = trim($row['ASCAAS']);
        $cod_liv1 = trim($row['ASUSAT']);
        $cod_liv2 = implode("|", array(trim($row['ASDOCU']), trim($row['ASIDPR'])));
        
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //CAUSALE
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = acs_u8e($row['TADESC']);
            $tmp_fl_date = get_fl_date(trim($row['ASDTSC']));
            $ar_new['fl_date'] = max($ar_new['fl_date'], $tmp_fl_date);
            $ar_new['liv'] = 'liv_0';
          
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //UTENTE ASSEGNATO
        $liv =$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] =  "[".trim($row['ASUSAT'])."] ".trim($row['D_UTENTE']);
            $tmp_fl_date = get_fl_date(trim($row['ASDTSC']));
            $ar_new['fl_date'] = max($ar_new['fl_date'], $tmp_fl_date);
            $ar_new['liv'] = 'liv_1';
    
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        //ORDINE
        $liv=$cod_liv2;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] =  $s->k_ordine_out($row['ASDOCU']);
            $ar_new['k_ordine'] =  $row['ASDOCU'];
            $ar_new['visual'] =  trim($row['ASFVIU']);
            $ar_new['flag'] =  trim($row['ASFLRI']);
            $ar_new['is_rilav'] =  $show_rilav;
            $ar_new['f_ril'] =  trim($row['ASFLNR']);
            $ar_new['prog'] =  $row['ASIDPR'];
            $ar_new['cliente'] =  $row['TDDCON'];
            $ar_new["riferimento"] 	=     trim($row['TDVSRF']);
            $ar_new["tipo"] 		=  $row['TDOTPD'];
            $ar_new["qtip_tipo"] 	=  acs_u8e($row['TDDOTD']);
            $ar_new["data_reg"]		=  $row['TDODRE'];
            $ar_new["cons_rich"]	=  $row['TDODER'];
            $ar_new["stato"]		=  $row['TDSTAT'];
            $ar_new["qtip_stato"]	=  acs_u8e($row['TDDSST']);
            $ar_new["importo"]		= $row['TDTIMP'];
            $ar_new['qtip_imm'] = print_date(trim($row['ASDTAS']))." - ".print_ora(trim($row['ASHMAS']));
            if($ar_new['flag']  == 'Y'){
                $causali_rilascio = $s->find_TA_std('RILAV', trim($row['ASCAAS']), 'N', 'N', trim($row['ASCARI']));
                $d_rilascio =  $causali_rilascio[0]['text'];
                $ar_new['qtip_imm'] .= "<br>".print_date(trim($row['ASDTRI'])).", [".trim($row['ASCARI'])."] ".$d_rilascio;
            }
            $ar_new['imm'] =  print_date(trim($row['ASDTAS']));
            $ar_new['ut_ins'] = trim($row['ASUSAS']);
            $ar_new['scadenza'] = trim($row['ASDTSC']);
            $tmp_fl_date = get_fl_date(trim($row['ASDTSC']));
            $ar_new['fl_date'] = max($ar_new['fl_date'], $tmp_fl_date);
            $ar_new['memo'] = trim($row['ASNOTE']);
            if(trim($row['MEMO']) != '')
                $ar_new["memo"] .=  "<br>" .trim($row['MEMO']);
            $ar_new['liv'] = 'liv_2';
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
        }
        
    }
    
    
    
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }

    
    echo acs_je(array('success' => true, 'children' => $ret));
   
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data_rilav'){
    
    $m_params = acs_m_params_json_decode();
    $where = "";
    $ar = array();
    
    if(strlen($m_params->takey1) > 0)
        $where .= " AND TAKEY1 = '{$m_params->takey1}' ";
        
        $sql = "SELECT *
        FROM {$cfg_mod_Spedizioni['file_tabelle']}
        WHERE TADT='{$id_ditta_default}' AND TATAID = 'RILAV'  {$where}";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        while ($row = db2_fetch_assoc($stmt)) {
            $r = array();
            $r['id'] 	= trim($row['TAKEY2']);
            $r['text'] = trim($row['TADESC']);
            $ar[] = $r;
        }
        
        
        echo acs_je($ar);
        exit;
        
}

if ($_REQUEST['fn'] == 'open_filtri'){
    
    
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
        $ar_users_json = acs_je($user_to);
        
        ?>
    
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            flex: 1,
	            items: [
	          
                  		 {
						name: 'f_todo',
						xtype: 'combo',
						flex : 1,
						fieldLabel: 'To Do',
						labelWidth : 110,
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: true,													
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php
							     // echo acs_ar_to_select_json($s->find_TA_std('ATTAV', N, 'Y'), "");
							     echo acs_ar_to_select_json($s->find_TA_std('ATTAV', null, 'Y', 'N', null, null, 'POS'), "");
							       ?>	
							    ] 
						},listeners: {
                    				change: function(field,newVal) {
                    				
                                    	 combo_todo_ev = this.up('form').down('#todo_ev');    
                                         combo_todo_ev.store.proxy.extraParams.takey1 = newVal;
                                    	 combo_todo_ev.store.load();                             
                             			
                             

                            }
                        }						 
					}, {
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente',
			            queryMode: 'local',
			            selectOnTab: false,
			            flex : 1,
			            name: 'f_utente_assegnato',
			            allowBlank: true,
						forceSelection: true,			            
						labelWidth : 110
			        }, {
						name: 'f_todo_ev',
						xtype: 'combo',
						itemId: 'todo_ev',
						fieldLabel: 'To Do evase',
						multiSelect : true, 
						labelWidth : 110,
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
						flex : 1,
					   	allowBlank: true,	
					   	store: {
                            autoLoad: true,
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
                                url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_rilav',
					            reader: {
					                type: 'json',
	                                method: 'POST',		
					                root: 'root',
					            },
                                actionMethods: {
    							          read: 'POST'
    							        },
                                extraParams: {
               		    		    		takey1: ''
               		    		       },               						        
				        		doRequest: personalizza_extraParams_to_jsonData
					        },       
								fields: ['id', 'text'],            	
			            }												
							 
					},
					{
					     name: 'f_data_scad'
					   , flex: 1                		
					   , xtype: 'datefield'
					  // , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
					   , startDay: 1 //lun.
					   , labelWidth : 110
					   , fieldLabel: 'Scadenza limite'
					   , format: 'd/m/Y'
					   , submitFormat: 'Ymd'
					   , allowBlank: true
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
            			 }
            		}
	            ],
	            
				buttons: [	
				{
			            text: 'Visualizza',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			              handler: function() {
	            
	            			var form = this.up('form').getForm();
	            			acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_panel', null, {
        		            	form_values: form.getValues()}, null, null);
        		         	  this.up('window').close();
			                   	                	                
	            		}
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
    
    <?php 
    
    exit;
}


if ($_REQUEST['fn'] == 'open_panel'){
?>

{"success":true, "items": [

        {
        xtype: 'treepanel' ,
        multiSelect: true,
        cls: 's_giallo',
        title: 'To Do list POS' ,
	    tbar: new Ext.Toolbar({
	            items:['<b> Attivit&agrave; utente di acquisizione/avanzamento ordini</b>', '->',
    	             {iconCls: 'icon-gear-16',
    	             text: 'CAUSALI', 
		           		handler: function(event, toolEl, panel){
			           		acs_show_panel_std('acs_gestione_attav.php?fn=open_grid', 'panel_gestione_attav', "");
			          
		           		 }
		           	 }
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,            
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['id', 'task', 'liv', 'cliente', 'flag', 'prog', 'imm', 'ut_ins', 'count',
                    'ut_ass', 'scadenza', 'riferimento', 'memo', 'tipo', 'qtip_tipo', 'data_reg', 'fl_date',
                    'cons_rich', 'stato', 'qtip_stato', 'importo', 'qtip_imm', 'k_ordine', 'f_ril', 'is_rilav', 'visual'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
						   form_values: <?php echo acs_je($m_params->form_values); ?>,
						   k_ordine : <?php echo j($m_params->k_ordine); ?>
						}, 
                        doRequest: personalizza_extraParams_to_jsonData  ,     
						reader: {
                            root: 'children'
                        }        				
                    }

                }),
    	    			
            columns: [{xtype: 'treecolumn', 
        	    		text: 'Causale/Utente/ordine', 	
        	    		width: 200,
        	    		dataIndex: 'task'
        	    		},{
			    		text: '<?php echo $cf5; ?>',
            			dataIndex: 'visual',
            			tooltip: 'Visualizzato',         			    
			    		width: 30,
		    			tdCls: 'tdAction',         			
            			menuDisabled: true, sortable: false,            		        
						renderer: function(value, p, record){
						    if(record.get('liv') == 'liv_2'){
								if (record.get('visual') == '') return '<img src=<?php echo img_path("icone/48x48/sub_blue_accept.png") ?> width=15>';   
			                	//if (record.get('visual') == 'Z') return '<img src=<?php echo img_path("icone/48x48/sticker_black.png") ?> width=15>';
							}
						}         			    
		  				},
        	    		{text: 'Cliente', width: 150, dataIndex: 'cliente'},	
	 					{text: 'Riferimento', width: 150, dataIndex: 'riferimento',
	 					renderer: function (value, metaData, record, row, col, store, gridView){						
						  if (record.get('liv') == "liv_1" || record.get('liv') == 'liv_0') return '[#' + record.get('count') +']';	    	
							return value;
							}},
	 					{text: 'Tp', width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd', 
	        	    		renderer: function (value, metaData, record, row, col, store, gridView){						
								if (record.get('qtip_tipo') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';								
							return value;			    
	    				}},
	    	    		{text: 'Data', 	width: 60, dataIndex: 'data_reg', renderer: date_from_AS},
	    	    		{text: 'St', width: 30, dataIndex: 'stato',
	    			      renderer: function(value, metaData, record){
	    			    	if (record.get('qtip_stato') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';	    			    
	    			    
							return value;
	    			    }	    	     
	    	   			},
	    	   			{text: 'Cons.<br>richiesta', width: 60, dataIndex: 'cons_rich', renderer: date_from_AS},
	 					{text: 'Importo', width: 90, dataIndex: 'importo', align: 'right', renderer: floatRenderer2},
	 					{text: 'Scadenza', width: 80, dataIndex: 'scadenza', 
	 					 renderer: function (value, metaData, record, row, col, store, gridView){
								
								if (record.get('fl_date') == 1)  metaData.tdCls += ' sfondo_giallo';							
								if (record.get('fl_date') == 2)  metaData.tdCls += ' sfondo_rosso';																	
						        
						       
    						return date_from_AS(value);	
    					}	
	 					
	 					},
	 					{text: 'Immessa', width: 120, dataIndex: 'imm',
	 					   renderer: function(value, metaData, record){
	    			    	if (record.get('qtip_imm') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_imm')) + '"';	    			    
	    			    
							return value;
	    			    }},	
	 					{text: 'Utente', width: 150, dataIndex: 'ut_ins'},	
	 					{text: 'Riferimento/Memo', flex:1, dataIndex: 'memo'},	
        	    		
    	    ],
    	     listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					     id_selected = grid.getSelectionModel().getSelection();
					     list_selected_id = [];
					     for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push({
							        id: id_selected[i].get('id'), 
									prog: id_selected[i].get('prog'), 
									f_ril: id_selected[i].get('f_ril'),
									k_ordine: id_selected[i].get('k_ordine')});
					     
					     if (rec.get('liv') == 'liv_2'){
					      
					    voci_menu.push({
			      		text: 'Avanzamento/Rilascio attivit&agrave;',
			    		iconCls: 'icon-arrivi-16',
			    		handler: function() {

  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('flag', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							        list_selected_id: list_selected_id, 
							        grid_id: grid.id},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });
					  
			<?php
            $causali_rilascio = $s->find_TA_std('RILAV', null, 'N', 'Y'); //recupero tutte le RILAV

            foreach($causali_rilascio as $ca) {
     
            ?>	

			
	if (rec.get('flag')!='Y' && rec.get('id').split("|")[0] == <?php echo j(trim($ca['id'])); ?>){ 		  
	
	voci_menu.push({
	      		text: <?php echo j($ca['text']); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {

				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('flag', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							           			list_selected_id: list_selected_id, grid_id: grid.id,
												auto_set_causale: <?php echo j(trim($ca['TAKEY2'])); ?>							        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
	    
						    
		
			    }
			  });
			  
			  
			  
			  
			  }
  <?php } ?>
  
  
  
  <?php 
  $js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
  
  if($js_parameters->only_view != 1){ ?>
			//VOCI MENU PER ORDINI NON EVASI -----------------------
	     	 voci_menu.push({
	         		text: 'Testata ordine',
	        		iconCls : 'icon-pencil-16',          		
	        		handler: function () {
	        			    	
			    			my_listeners = {
			    		  			afterOkSave: function(from_win, new_value){
		        						rec.set('tipo', new_value.TDOTPD);
		        						rec.set('riferimento', new_value.TDVSRF);
		        						rec.set('data_reg', new_value.TDODRE);
		        						from_win.close();  
						        		}
				    				};	
			    		  	 
			    		  	acs_show_win_std('Modifica informazioni di testata', 
		                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
		                				tddocu: rec.get('k_ordine')
									}, 800, 500, my_listeners, 'icon-shopping_cart_gray-16');
		                }
	    		});	  		
        <?php } ?>

		      	 voci_menu.push({
		         		text: 'Righe ordine',
		        		iconCls : 'icon-leaf-16',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
								  acs_show_win_std('Visualizza righe', 'acs_righe_ordine.php?fn=open_grid', {k_ordine: rec.get('k_ordine'), art_manc : rec.get('fl_art_manc')}, 900, 550, null, 'icon-leaf-16');
			                }
		    		});

			      	<?php 
			      	if($js_parameters->only_view != 1){ ?>

				   	 voci_menu.push({
				      		text: 'Documenti ordine',
				    		iconCls : 'iconScaricoIntermedio',      		
				    		handler: function() {
					    		console.log(rec.data);
								acs_show_win_std('Elenco documenti ordine', 'acs_documenti_cliente.php?fn=open_list', {
									k_ordine: rec.get('k_ordine')
								}, 1100, 600, {}, 'icon-leaf-16');
				    		}
						  });

					  <?php }?>
  
   	 			voci_menu.push({
		         		text: 'Stampa scheda ordine',
		        		iconCls : 'icon-print-16',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	window.open('acs_report_pdf.php?tipo_stampa=SCHEDA&k_ordine='+ rec.get('k_ordine'));
			                }
		    		});	
					  
					  
					     }
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	},
	        celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	
					   if (rec.get('liv') == 'liv_2' && col_name == 'visual'){
					  
						 
						  Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_visual',
						        method     : 'POST',
			        			jsonData: {
			        			    flag: rec.get('visual'),
			        			    prog: rec.get('prog'),
			        			    k_ordine: rec.get('k_ordine')
								},							        
						        success : function(result, request){
			            			jsonData = Ext.decode(result.responseText);
			            		 	rec.set('visual', jsonData.flag);
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	 

							
						}
						
					
	          
	            	}
	          
	           }
				 },
				 
				 viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('flag') == 'Y' && record.get('is_rilav') != 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }	
    	    
    	    
    	}	
	 	
	]  	
  }


<?php 
exit;
}


function get_fl_date($date){
    
       if (oggi_AS_date() == $date) return  1;
	   
	   if (oggi_AS_date() > $date) return 2;
	
	}