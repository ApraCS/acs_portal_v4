<?php
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

function get_tipo_scheda($name){
    global $n_ord;
    if (strstr( $name , "_MSGIN_" )) return "1. Messaggi ricevuti";
    if (strstr( $name , "_MSGOUT_" )) return "2. Messaggi inviati";
    
    if (substr($name, 0, 6) != $n_ord) return "3. Altro";
    return "4. Allegati grafici";
}

if ($_REQUEST['fn'] == 'get_data'){

    if(trim($m_params->tipo) == 'F')
        $img_dir = $cfg_mod_DeskPVen['allegati_root_F'];
    else
        $img_dir = $cfg_mod_DeskPVen['allegati_root_C'];
    
        $dir_ordine = $img_dir . $m_params->codice;  // 330120833
        
        $dir_ordine = "{$dir_ordine}/*.*";
            
            $ret = array();
            foreach (glob($dir_ordine) as $filename)
            {
                $nome_file_ar = explode('/', $filename);
                $nome_file = $nome_file_ar[4];
                $r = array();
                $r['DataCreazione'] = date ("d/m/Y", filemtime($filename));
                $r['IDOggetto']		 = acs_u8e($filename);
                $r['des_oggetto']	 = acs_u8e($nome_file); //basename($filename);
                $r['tipo_scheda']	 = get_tipo_scheda(basename($filename));
                $ret[] = $r;
            }
            
            echo acs_je($ret);
            exit;
    
}

if ($_REQUEST['fn'] == 'open_win'){
	$m_params = acs_m_params_json_decode();
	
	?>

{"success":true, "items": [

                new Ext.grid.GridPanel({
                    title: 'Allegati',
               		 store: new Ext.data.Store({
               									
               				autoLoad: true,				        
               	  			proxy: {
               							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data',
               							type: 'ajax',
               							reader: {
                						            type: 'json',
                									method: 'POST',						            
                						            root: 'root'						            
                						        },
               						      actionMethods: {
            							          read: 'POST'
            							        },
               						     extraParams: {
               		    		    		codice: '<?php echo $m_params->codice; ?>',
               		    		    		tipo : '<?php echo $m_params->tipo; ?>'
               		    				},               						        
				        			doRequest: personalizza_extraParams_to_jsonData
               						},
               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
               		     	groupField: 'tipo_scheda',               		     	
               			}),
    		    	features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),               			
                  						    						
                     columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],

			         listeners: {
				      activate: {
					      fn: function(e){
						   	if (this.store.proxy.extraParams.k_ordine != '')
							   	this.store.load();
					      }
				      },
					       
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  },
			   		  
			   		  itemcontextmenu : function(grid, rec, node, index, event) {
							event.stopEvent();							
							
		    				id_selected = grid.getSelectionModel().getSelection();
		    			  	list_selected_id = [];
		    			  	for (var i=0; i<id_selected.length; i++) 
		    				   list_selected_id.push({attachments: [id_selected[i].get('IDOggetto')]});
		    					  				
							var voci_menu = [];

							//k_ord = this.getStore().proxy.extraParams.k_ordine;
							
					           voci_menu.push({
					         		text: 'Invia tramite e-mail',
					        		iconCls : 'icon-email_compose-16',          		
					        		     handler: function() {
						        		     
					        		    	 acs_show_win_std('Email Composer', 
							        		    	 '../base/acs_email_composer.php?fn=open_email', 
							        		    	 {
						        		    	 		attachments: [rec.get('IDOggetto')], list_selected_id: list_selected_id
							        		    	 }, 
							        		    	 950, 500, null, 'icon-email_compose-16', 'Y');
								         }
					    		});


					           voci_menu.push({
					         		text: 'Cancella',
					        		iconCls : 'icon-sub_red_delete-16',          		
					        		     handler: function() {
						        		     
					        		    	 Ext.Msg.confirm('Richiesta conferma', 'Confermi la cancellazione degli allegati?', function(btn, text){																							    
													   if (btn == 'yes'){																	         	
														  
													    	 Ext.Ajax.request({
															        url        : '../base/file_utility.php?fn=exe_delete_file',
															        method     : 'POST',
												        			jsonData: {
												        			    list_selected_id: list_selected_id
																	},							        
															        success : function(result, request){
												            			grid.getStore().load();	
												            		 
												            		},
															        failure    : function(result, request){
															            Ext.Msg.alert('Message', 'No data to be loaded');
															        }
															    });					         	
											         	
														}
													   }); 
					        		    	 
								         }
					    		});


						      var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);						    		
							  
					  }
				         
			         }

                     }) 


	
]}


<?php 
exit;
}