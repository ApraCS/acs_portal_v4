<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

$list_selected_id = $m_params->selected_id;

$fl = $list_selected_id[0]; //primo record
$ar_selected_id = explode("_", $fl->k_ordine);
$year 	= $ar_selected_id[3];


//recupero l'anno per il carico dalla data di evasione programmata del primo ordine
try {
    $ord_tmp = $s->get_ordine_by_k_ordine($fl->k_ordine);
    $year = substr($ord_tmp['TDDTEP'], 0, 4);
} catch (Exception $e) {
    $year 	= $ar_selected_id[3];
}



function out_array_hidden_selected_id($list){
	$ret = array();
	foreach ($list as $l) {
		$ret[] = array('xtype' => "hidden", 'name'=>"list_selected_id", 'value'=>$l);
	}
	return implode(',' , array_map('json_encode', $ret));
}

if ($_REQUEST['fn'] == 'azzera_carico'){
    
    $tp_carico = 'CA';
    $nr_carico = 0;
    $error = "";
   
    
    foreach($m_params->list_ordini as $v){
        
     
        //CONTROLLO SU CARICO POSITIVO PROCEDO CON L'ASSEGNA
      
       $sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
                            TDSWSP = 'Y'
                            , TDTPCA = " . sql_t($tp_carico) . "
							, TDAACA = " . $m_params->form_values->f_anno . "
							, TDNRCA = {$nr_carico}
							, TDFN01 = 1
			WHERE TDDOCU = " . sql_t($v->k_ordine) . " AND " . $s->get_where_std();
									
									
	   $stmt_upd = db2_prepare($conn, $sql_upd);
	   echo db2_stmt_errormsg();
	   $result = db2_execute($stmt_upd);
									
	   $sh = new SpedHistory();
	   $sh->crea(
	       'assegna_carico',
	    array(
	        "k_ordine" 	=> $v->k_ordine,
	        "k_carico"	=> implode("_", array($m_params->form_values->f_anno, $tp_carico, $nr_carico)),
	        "seca"		=> ""
	    )
	    );
									
    }
    
    $ret = array();
    $ret["success"] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'assegna_carico'){
        
    $tp_carico = 'CA';
    $error = "";
    $segnala_errore = false;

    foreach($m_params->list_ordini as $v){

        $ord = $s->get_ordine_by_k_ordine($v->k_ordine);
        $data = $ord['TDDTEP'];
        
        
        $sql = "SELECT COUNT (DISTINCT(TDDTEP)) AS C_ROWS, TDDTEP
        FROM  {$cfg_mod_Spedizioni['file_testate']}
        WHERE TDDT = '{$id_ditta_default}'
        AND TDNRCA = {$m_params->form_values->f_carico}
        GROUP BY TDDTEP";
       
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if($row['C_ROWS'] > 1){
            
            $segnala_errore = true;
            $error = "Carico con anomalie";
            
        }else if($row['C_ROWS'] == 1){
            
           if(trim($row['TDDTEP']) != $data){
            $segnala_errore = true;
            $error = "Carico gi&agrave; esistente con ordini con data di evasione diversa";
           }
        }
        
        if( $segnala_errore == true){
            $ret = array();
            $ret["success"] = false;
            $ret["msg_error"] = $error;
            echo acs_je($ret);
            exit;  
        }

        //CONTROLLO SU CARICO POSITIVO PROCEDO CON L'ASSEGNA
        
        $sql_set = "";
        $vettore = $m_params->form_values->f_vettore;
        if(isset($vettore) && strlen(trim($vettore)) > 0)
            $sql_set .= ", TDVETT = '{$vettore}'";
        $trasp = $m_params->form_values->f_trasp;
        if(isset($trasp) && strlen(trim($trasp)) > 0)
            $sql_set .= ", TDVET1 = '{$trasp}'";
        
        if(trim($vettore) != '' || trim($trasp) != ''){
            
            $ex_vettore = trim($ord['TDVETT']);
            $ex_trasp = trim($ord['TDVET1']);
            
            $sh = new SpedHistory();
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'AGG_VETTORE',
                    "k_ordine"	=> $v->k_ordine,
                    "vals" => array("RIVETT" => trim($vettore),
                        "RICVES" => trim($trasp),
                        "RINOTR" => "AC, EX: {$ex_vettore}, {$ex_trasp}")
                       
                )
                );
            
        }    
    
 
    
    $sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']} SET
               TDSWSP = 'Y'
              , TDTPCA = " . sql_t($tp_carico) . "
									, TDAACA = " . $m_params->form_values->f_anno . "
									, TDNRCA = " . $m_params->form_values->f_carico . "
									, TDFN01 = 1
                                    {$sql_set}
									/*, TDSECA = " . sql_t($applica_seq_ord) . "*/
			WHERE TDDOCU = " . sql_t($v->k_ordine) . " AND " . $s->get_where_std();

    
    $stmt_upd = db2_prepare($conn, $sql_upd);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_upd);
    

    
    $sh = new SpedHistory();
    $sh->crea(
        'assegna_carico',
        array(
            "k_ordine" 	=> $v->k_ordine,
            "k_carico"	=> implode("_", array($m_params->form_values->f_anno, $tp_carico, $m_params->form_values->f_carico)),
            "seca"		=> ""
        )
        );
    
    }
    
    $ret = array();
    $ret["success"] = true;
    echo acs_je($ret);
    exit;  
}


if ($_REQUEST['fn'] == 'open_form'){
    
      
?>

{"success":true, "items": [

 					{ 
						xtype: 'fieldcontainer',
						margin : '5 5 5 5',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
        					 {
        			flex : 0.5,
        			xtype: 'grid',
        			title: '',
        	        loadMask: true,	
        	        autoScroll : true,
        	        store: {
        			xtype: 'store',
        			autoLoad:true,
        			data : [
        				
        			    <?php foreach($m_params->selected_id as $v){?>
        			 		{ordine : <?php echo j($v->ordine); ?>, cliente : <?php echo j($v->cliente); ?>, vettore : <?php echo j($v->vettore); ?>},
        	        	<?php  }?>
        			  		
        			  	],
        					
        				
                			fields: ['cliente', 'ordine', 'vettore']							
        							
        	}, //store
        		
        
        	      columns: [
        	       {
                    header   : 'Ordine',
                    dataIndex: 'ordine',
                    flex: 1
                    },
                    {
                    header   : 'Cliente',
                    dataIndex: 'cliente',
                    flex: 1
                    },{
                    header   : 'Vettore',
                    dataIndex: 'vettore',
                    flex: 1
                    }
                    
             ]
      		      },{
            xtype: 'form',
            flex : 0.5,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            
            items: [
             		{
						name: 'f_anno',
						xtype: 'hidden',
						value: '<?php echo $year ?>'							
					 }, {
						xtype: 'textfield',
						fieldLabel: 'Anno',
						readOnly : true,
						value: '<?php echo $year ?>',
					    anchor: '-15'							
					 },	{ 
						xtype: 'fieldcontainer',
						
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {
						name: 'f_carico',
						xtype: 'numberfield',
						fieldLabel: 'Carico',
						minValue: 1,
						flex : 1,
						hideTrigger : true,
						allowBlank : false,
						value: '',
					    anchor: '-15'							
					 }, {
						xtype: 'button',
			            text: 'Genera',
			            margin : '0 15 0 5',
			            anchor: '-15',
			            //iconCls: 'icon-address_book-32', 
			            //iconAlign: 'top',	
			            scale: 'small',		
			            handler: function() {
			            
			                   var form = this.up('form').getForm();
			            
			            		Ext.Ajax.request({
								        url        : 'acs_op_exe.php?fn=assegna_progressivo_carico',
								        jsonData: form.getValues(),
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){	            	  													        
								            var jsonData = Ext.decode(result.responseText);
								            form.findField("f_carico").setValue(jsonData.nr);	
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	   
			            
			            	  	
			            }
			        }
						
						   
						]
						
						}
					 
			,{
	            xtype:'fieldset',
	            title: 'Assegnazione su carico/spedizione aggiuntiva',
	            collapsible: false,
	            collapsed: false, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[	
	             {
								name: 'f_vettore',
								xtype: 'combo',
				            	anchor: '-15',
				            	flex: 1,
					            fieldLabel: 'Vettore/montatore',
					            labelWidth : 110,
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								//value: <?php echo j(trim($ord['TDVETT'])) ?>,
								forceSelection: true,
							   	allowBlank: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [
									     <?php echo acs_ar_to_select_json($s->find_TA_std('AUTR'), '') ?>
									    ]
								}
							},
							
							
							       {
								name: 'f_trasp',
								xtype: 'combo',
				            	anchor: '-15',
				            	flex: 1,
				            	labelWidth : 110,
					            fieldLabel: 'Trasportatore',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								//value: <?php echo j(trim($ord['TDVET1'])) ?>,
								forceSelection: true,
							   	allowBlank: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [
									     <?php echo acs_ar_to_select_json($s->find_TA_std('AVET'), '') ?>
									    ]
								}
							}
						

			]
		} 
		
		,<?php echo out_array_hidden_selected_id($list_selected_id) ?>
				],
			buttons: [
			{
	            text: 'Azzera',
	            handler: function() {
	            	var form = this.up('form').getForm();
		            loc_win = this.up('window');
		            
		             Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'azzeramento del carico?', function(btn, text){																							    
						if (btn == 'yes'){
		          
						 Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=azzera_carico',
						        jsonData:  {
						        	form_values : form.getValues(),
						        	list_ordini : <?php echo acs_je($m_params->selected_id); ?>,
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						            var jsonData = Ext.decode(result.responseText);
						            
						             if (jsonData.success == false){
							          	acs_show_msg_error(jsonData.msg_error);
									  	return false;
							         }else{
							         	loc_win.fireEvent('afterConf', loc_win);
							         }
						         
						        },
						        failure    : function(result, request){
						           Ext.Msg.alert('Message', 'No data to be loaded');
			                    }
						    });	  
						    }  	
						 }); 
            	                	                
	            }
	        }, '->',
			
			{
	            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
		            loc_win = this.up('window');
		          
		            	//Ext.getBody().mask('Loading... ', 'loading').show();	
						 if (form.isValid()){    
						 
						 Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=assegna_carico',
						        jsonData:  {
						        	form_values : form.getValues(),
						        	list_ordini : <?php echo acs_je($m_params->selected_id); ?>,
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						            var jsonData = Ext.decode(result.responseText);
						            
						             if (jsonData.success == false){
							          	acs_show_msg_error(jsonData.msg_error);
									  	return false;
							         }else{
							         	loc_win.fireEvent('afterConf', loc_win);
							         }
						         
						        },
						        failure    : function(result, request){
						           Ext.Msg.alert('Message', 'No data to be loaded');
			                    }
						    });	    	
		         
            			}
  
            	                	                
	            }
	        }],             
				
        }
						
						
						   
						]}

        
]}

<?php 

exit;
}