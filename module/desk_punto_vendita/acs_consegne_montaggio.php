<?php

require_once "../../config.inc.php";
require_once("acs_consegne_montaggio_include.php");

$main_module = new DeskPVen();
$cod_mod_provenienza = $main_module->get_cod_mod();
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_aggiungi_sequenza'){
    
    $m_params = acs_m_params_json_decode();
    

    foreach($m_params->form_values as $k=> $v){
        
        $sequenza = $m_params->form_values->$k;
        $ar_k = array();
        
       foreach($m_params->list_selected_id->$k->list_ord as $k2=> $v2){
            $ar_k[] = $v2->k_ordine;
            
            $sh = new SpedHistory();
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'ASS_SEQ_PV',
                    "k_ordine"	=> $v2->k_ordine,
                    "vals" => array(
                        "RISECA" => $v,
                    )
                    )
                );
            
            
        }
  
        $sql = "UPDATE {$cfg_mod_DeskPVen['file_testate']}
        SET TDSECA = '{$v}'
        WHERE TDDT = '{$id_ditta_default}' AND TDDOCU IN (". sql_t_IN($ar_k) . ")";
          
     
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
         

    }
 
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
        
    
}


// ******************************************************************************************
// EXE - spunta blu controllato
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_spunta_controllato'){
    
    $m_params = acs_m_params_json_decode();
    
    $k_ordine = $m_params->k_ordine;
    
    if ($m_params->flag == ' ')
        $m_flag = 'Y';
    else
        $m_flag = ' ';
            
    $sql = "UPDATE {$cfg_mod_DeskPVen['file_testate']}
            SET TDFG05 = '{$m_flag}'
            WHERE TDDT = '{$id_ditta_default}' AND TDDOCU = '{$k_ordine}'";
            
            //print_r($sql);
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
          
            $ret = array();
            $ret['success'] = true;
            $ret['flag'] = $m_flag;
            echo acs_je($ret);
            
            exit;
}


//**********************************************************
if ($_REQUEST['fn'] == 'exe_assegna_vettore'){
//**********************************************************
	  
    $sql = " SELECT *
    FROM {$cfg_mod_DeskPVen['file_tabelle']} TA
    WHERE TADT = '{$id_ditta_default}' AND TATAID = 'AUTR' AND TAKEY1 = '{$m_params->to_cod}'
	";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    if(trim($row['TACOGE']) == '')
        $trasp = "0";
    else{
        $trasp = $row['TACOGE'];
    }
    
    $sql_u = "UPDATE
    {$cfg_mod_DeskPVen['file_testate']} TD
    SET TDVETT=?, TDVET1 = ?
    WHERE TDDOCU IN( " . sql_t_IN($m_params->list_selected_id) . ")
	";
    $stmt_u = db2_prepare($conn, $sql_u);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_u, array($m_params->to_cod, $trasp));
    
  
   
	foreach($m_params->list_selected_id as $v){
	    
	$ord = $s->get_ordine_by_k_ordine($v);
	$ex_vettore = trim($ord['TDVETT']);
	$ex_trasp = trim($ord['TDVET1']);
	
	    
	$sh = new SpedHistory();
	$sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_VETTORE',
	        "k_ordine"	=> $v,
	        "vals" => array("RIVETT" => trim($m_params->to_cod),
	                        "RICVES" => $trasp,
	                        "RINOTR" => "AV, EX: {$ex_vettore}, {$ex_trasp}"
	                )
	    )
	    );
	
	}
				
	$ret = array('success' => true);
	echo acs_je($ret);
	exit;
}

//**********************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid_vettori'){
//**********************************************************
	//costruzione sql
	$sql = "SELECT TAKEY1 AS COD, TADESC AS DES
			FROM {$cfg_mod_DeskPVen['file_tabelle']} TA
			WHERE TADT = '{$id_ditta_default}' AND TATAID = 'AUTR'
			ORDER BY TADESC
	";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->cod_iti, $m_params->data));
	
	$ar = array();
	$ar_tot = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit;
}

//**********************************************************
if ($_REQUEST['fn'] == 'open_form_assegna_vettore'){
//**********************************************************
?>
{"success":true, "items": [
	{
		xtype: 'form',
		bodyStyle: 'padding: 10px',
		bodyPadding: '5 5 0',
		frame: true,
		title: '',
	
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		 
	
		items: [
		{
			xtype: 'grid',
			itemId: 'tabSelect',
			loadMask: true,
			flex: 1,
			features: [
			{
				ftype: 'filters',
				encode: false,
				local: true,
				filters: [
				{
					type: 'boolean',
					dataIndex: 'visible'
				}
				]
			}],
			selModel: {selType: 'checkboxmodel'},
			store: {
				xtype: 'store',
				autoLoad:true,
				proxy: {
					url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_vettori',
					method: 'POST',
					type: 'ajax',
						
					//Add these two properties
					actionMethods: {
						read: 'POST'
					},
						
					reader: {
						type: 'json',
						method: 'POST',
						root: 'root'
					}
					, doRequest: personalizza_extraParams_to_jsonData
					, extraParams: <?php echo acs_je($m_params); ?>
								},
									
								fields: ['COD', 'DES']
											
											
							}, //store
							multiSelect: false,
							
						
							columns: [
								{header: 'Codice', 		dataIndex: 'COD', width: 80,  filter: {type: 'string'}, filterable: true},						
								{header: 'Denominazione', 	dataIndex: 'DES', flex: 1,  filter: {type: 'string'}, filterable: true},
							]
							 
						}	
							 
							 
					],
				buttons: [{
		            text: 'Conferma',
		            iconCls: 'icon-windows-32',
		            scale: 'large',	            
		            handler: function() {
	
					var form_p = this.up('form');
					var tab_selecte = form_p.down('#tabSelect');
					var loc_win = this.up('window');
		            
					row_selected = tab_selecte.getSelectionModel().getSelection();   				
		   			selected_id = row_selected[0].data.COD;
	
	
					 Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_vettore',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        			    to_cod: selected_id,
	        			    list_selected_id: <?php echo acs_je($m_params->list_selected_id) ?>
						},							        
				        success : function(result, request){
				    		loc_win.fireEvent('onSelected', loc_win); 			            			
	            		},
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });	
					
		            } //handler
		        }
		        ],             
					
	        }
	]}
<?php	
	exit;
}




if ($_REQUEST['fn'] == 'get_parametri_form'){

	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';

	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
		$ar_area_spedizione_txt = implode(',', $ar_ar2);

			
		?>

{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: false,
	title: '',
    buttonAlign:'center',
	buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "DELIVERY_POS");  ?>
	{
			            text: 'Report',
			            iconCls: 'icon-print-32',
			            scale: 'large',
			            handler: function() {
		                    form = this.up('form').getForm();
		                    
		                      if (form.isValid()){
            	                
            	                acs_show_win_std('Filtro data', 'acs_consegne_montaggio_report.php?fn=open_form', {
		        				filter: form.getValues()
		    					},		        				 
		        				400, 200, {}, 'icon-print-16');	
		        				
		             
	                }         	                	     
            	                	                
	            }
			         },
			{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	             handler: function() {
	                form = this.up('form').getForm();
	                            	                
		               if (form.isValid()){	                	                
			               acs_show_panel_std('acs_consegne_montaggio.php?fn=open_tab', 'consegne-montaggio', {form_values: form.getValues()});
			               this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        }],             
            
            items: [
	            
	             {
					xtype: 'fieldset',
					layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
						{ 
            xtype: 'fieldcontainer',
            layout: { 	type: 'hbox',
            			pack: 'start',
            			align: 'stretch',
            			frame: true},	
            				
            items: [
            	    {
				     name: 'f_data'
				// , flex: 1                		
				   , xtype: 'datefield'
				   , margin: "10 0 0 10"               		
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data iniziale'
				   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
				   , labelAlign: 'left'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
				}, {
				     name: 'f_data_a'
				//   , flex: 1
				   , margin: "10 20 0 10"               		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data finale'
				   , labelWidth: 70
				   , labelAlign: 'right'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
            			}
            	]},
            			{
							name: 'f_sede',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Sede',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?>	
								    ] 
							}						 
							},
							
							
							{
							name: 'f_vettore',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Vettore',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('AUTR'), '') ?>	
								    ] 
							}						 
							},{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							 margin: "10 10 5 10",	
							 anchor: '-15',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							anchor: '-15',
							margin: "10 10 5 10",
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							},{
						flex: 1,
			            xtype: 'combo',
						name: 'f_cliente',
						fieldLabel: 'Cliente',
						 anchor: '-15',
						 margin: "10 20 5 10",	
						minChars: 2,			
            
			            store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_get_select_json.php?select=search_cli',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['cod', 'descr', 'out_loc', 'ditta_orig'],		             	
			            },
                        
						valueField: 'cod',                        
			            displayField: 'descr',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun cliente trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{descr}</span></h3>' +
			                        '[{cod}] {out_loc} {ditta_orig}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        	       },
        	       
        	       	{ 
            xtype: 'fieldcontainer',
            flex : 1,
            layout: { 	type: 'hbox',
            			pack: 'start',
            			align: 'stretch',
            			frame: true},	
            				
            items: [
            	   	{
					name: 'f_da_carico',
					xtype: 'numberfield',	
				    hideTrigger : true,
				    fieldLabel : 'Da carico',
				    anchor: '-15',
					margin: "5 40 5 10",	
				   },{
					name: 'f_a_carico',
					xtype: 'numberfield',	
					hideTrigger : true,
					fieldLabel : 'A carico',
					labelWidth: 70,
				    labelAlign: 'right',
				    margin: "5 0 5 10",	
				   
				 }
            	]},
        	       
        	       {
							name: 'f_evasi',
							xtype: 'radiogroup',
							fieldLabel: 'Ordini evasi',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						   	labelWidth: 110,
						    margin: "0 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: 'T'
		                          , width: 50
		                         <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] != 'Y') { ?>			                
			                      , checked: true
							<?php } ?>
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Da evadere'
		                          , inputValue: 'N'
								  <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') { ?>			                
			                      , checked: true
							      <?php } ?>
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Evasi'
								  , checked: false	
		                          , inputValue: 'Y'
		                          
		                        }]
						},{
							xtype: 'radiogroup',
							fieldLabel: 'Ordini controllati',
							labelWidth: 110,
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "0 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_controllati' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'Y'
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_controllati' 
		                          , boxLabel: 'Esclusi'
		                          , inputValue: 'N'
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_controllati' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: ''
		                          
		                        }]
						}
	             
	             ]
	             },                                     
            ]
        }

	
]}


<?php	
 exit;	
}



// ******************************************************************************************
// get_data_tree
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_data_tree'){

	$m_params = acs_m_params_json_decode();

	$form_values=$m_params->open_request->form_values;

	$ar = crea_ar_tree_consegne_montaggio($_REQUEST['node'], $form_values);

	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}

	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}



//**********************************************************
//DEFAULT: COSTRUZIONE TREE
//**********************************************************
if ($_REQUEST['fn'] == 'open_tab'){
?>
{"success":true, "items": [

        {
            xtype: 'treepanel' ,
	        title: 'Delivey POS' ,
	        tbar: new Ext.Toolbar({
	            items:['<b> Programmazione consegne/montaggio</b>', '->'
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	    
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,            
			multiSelect:true,
			singleExpand: false,
			stateful: true,
        	stateId: 'delivery_pos',
        	stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                                
				    fields: ['task', 'colli_tot', 'colli_disp', 'colli_non_disp', 'liv', 'tipo',
				    		'stato', 'data', 'riferimento', 'gg_ritardo', 'fl_cli_bloc', 'fl_bloc', 'fl_new',
				    		'fl_art_manc', 'art_da_prog', 'fl_da_prog', 'cod_iti', 'volume', 'importo', 'cod_cli',
				    		'fat_merce', 'importo_fat', 'k_ordine', 'controllato', 'data_atp', 'seq_carico',
				    		'raggr', 'data_reg', 'priorita', 'tp_pri', 'cons_rich', 'colli', 'colli_sped', 'volume', 'tdfg06',
				    		'ordine', 'cliente', 'vettore'
				    		],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tree',
						actionMethods: {read: 'POST'},
						
						extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
                        
                       	reader: {
                            root: 'children'
                        },
                       	                      
                    	doRequest: personalizza_extraParams_to_jsonData        				
                    }

                }),
                
                <?php 
                //img per intestazioni colonne flag
                $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>";
                $cf2 = "<img src=" . img_path("icone/48x48/power_black.png") . " height=25>";
                $cf4 = "<img src=" . img_path("icone/48x48/button_blue_pause.png") . " height=25>";
                $cf5 = "<img src=" . img_path("icone/48x48/sub_blue_accept.png") . " height=20>";
                $cf6 = "<img src=" . img_path("icone/48x48/comment_accept.png") . " height=25>";
                $cf7 = "<img src=" . img_path("icone/48x48/comments.png") . " height=25>";
                ?>
     
				columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            flex: 1,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Settimana',
			            renderer: function(value, metaData, record){
	
								if (record.get('liv') == 'liv_4')
				  	  				 metaData.tdCls += ' auto-height';	
				  	  				 
			  	  				if (record.get('liv') == 'liv_3')
				  	  			    metaData.tdCls += ' grassetto';			
								
								return value;	
							}
			               
			        }, 
			           {
				    text: '<?php echo $cf6; ?>',
				    width: 32, 
				    tooltip: 'Note consegna/installazione',
				    tdCls: 'tdAction',  
				    dataIndex : 'comm',       			
	    	        menuisabled: true, sortable: false,        		        
					renderer: function(value, p, record){
    					if(record.get('liv') == 'liv_4'){
    						if (record.get('comm') == 1) return '<img src=<?php echo img_path("icone/48x48/comment_accept.png") ?> width=18>';
    			    		if (record.get('comm') == 0) return '<img src=<?php echo img_path("icone/48x48/comment_accept_grey.png") ?> width=18>';
    			    	}
			    		
			    	}
			    	},
			    	 {
				    text: '<?php echo $cf7; ?>',
				    width: 32, 
				    tooltip: 'Note predisposizione carico',
				    tdCls: 'tdAction',  
				    dataIndex : 'comm_car',       			
	    	        menuisabled: true, sortable: false,        		        
					renderer: function(value, p, record){
    					if(record.get('liv') == 'liv_4'){
    						if (record.get('comm_car') == 1) return '<img src=<?php echo img_path("icone/48x48/comments.png") ?> width=18>';
    			    		if (record.get('comm_car') == 0) return '<img src=<?php echo img_path("icone/48x48/comments_grigio.png") ?> width=18>';
    			    	}
			    		
			    	}
			    	},
                  	{
				    text: '<?php echo $cf1; ?>',
				    width: 30, tooltip: 'Clienti bloccati',
				    tdCls: 'tdAction',         			
	    	        menuisabled: true, sortable: false,            		        
					renderer: function(value, p, record){if (record.get('fl_cli_bloc')>0) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
			    	},
			    	
			    	
					<?php
						//inclusione colonne flag ordini
						$js_include_flag_ordini = array('menuDisabled' => false, 'sortable' => true);
						require("../desk_vend/_include_js_column_flag_ordini.php"); 
					?>,		   
				    	
    			    	
    			    	{
			    		text: '<?php echo $cf4; ?>',
            			dataIndex: 'fl_da_prog', tooltip: 'Ordini da programmare',         			    
			    		width: 30,
		    			tdCls: 'tdAction',         			
            			menuDisabled: true, sortable: false,            		        
						renderer: function(value, p, record){if (record.get('fl_da_prog')>0) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';}         			    
		  				},
		  				
    			    	{
			    		text: '<?php echo $cf5; ?>',
            			dataIndex: 'controllato',
            			tooltip: 'Controllato',         			    
			    		width: 30,
		    			tdCls: 'tdAction',         			
            			menuDisabled: true, sortable: false,            		        
						renderer: function(value, p, record){if (record.get('controllato') == 'Y') return '<img src=<?php echo img_path("icone/48x48/sub_blue_accept.png") ?> width=18>';}         			    
		  				}, {
                         text: 'Seq', width: 30,
                         dataIndex: 'seq_carico',
                         },
            		  { header: 'Localita\'/Riferimento', 
                 		dataIndex: 'riferimento', 
                 		sortable: false,
                 		flex:1},
                 
                 
                 
    	{text: 'Tp', width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd', sortable: false, 
	    	    	renderer: function (value, metaData, record, row, col, store, gridView){						
					metaData.tdCls += ' ' + record.get('raggr');										
					return value;			    
			}},
	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', sortable: false, renderer: date_from_AS},
	    {text: 'St', 			width: 30, dataIndex: 'stato', sortable: false},	    	    
	    {text: 'Pr', 			width: 35, dataIndex: 'priorita', sortable: false, tdCls: 'tpPri',
				renderer: function (value, metaData, record, row, col, store, gridView){
					if (record.get('tp_pri') == 4)
						metaData.tdCls += ' tpSfondoRosa';

					if (record.get('tp_pri') == 6)
						metaData.tdCls += ' tpSfondoCelesteEl';					
					
					return value;
				    
	    			}},
		{text: 'Consegna<br>richiesta',	
		width: 60, 
		dataIndex: 'cons_rich',
		sortable: false,
		renderer: function (value, metaData, record, row, col, store, gridView){
			if(record.get('cons_rich') < record.get('data_atp'))
				metaData.tdCls += ' sfondo_rosso';
		
			return date_from_AS(value);
			}
		},	
		{text: 'ATP',	width: 60, dataIndex: 'data_atp', sortable: false, renderer: date_from_AS},	    	    	    
	    {text: 'Colli',			width: 35, dataIndex: 'colli', sortable: false, align: 'right'},	       
	    {text: 'D',	width: 30, tdCls: 'tdAction', sortable: false,
			    renderer: function(value, p, record){
			    	if (parseInt(record.get('colli_disp')) == 0 ) return '';
			    	if (parseInt(record.get('colli_disp')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none.png") ?> width=18>';			    	
			    	if (parseInt(record.get('colli_disp')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?> width=18>';			    	
			    	}},	    
	    {text: 'S',	width: 30, tdCls: 'tdAction', sortable: false,
			    renderer: function(value, p, record){
			    	if (parseInt(record.get('colli_sped')) == 0) return '';
			    	if (parseInt(record.get('colli_sped')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none_black.png") ?> width=18>';			    	
			    	if (parseInt(record.get('colli_sped')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_green.png") ?> width=18>';			    	
			    	
			    	}},			    		    
				    {
				    text: 'Volume',			
				    width: 50, 
				    dataIndex: 'volume',
				    sortable: false, 
				    align: 'right', 
				    renderer: floatRenderer2,
				    },
				    <?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>
				    {
                    header: 'Importo ordini', 
                 	dataIndex: 'importo', 
                 	align: 'right',
                 	sortable: false,
                  	renderer: floatRenderer2,
                  	width: 100},
                  	
                  	<?php }?>
                   {
                     header: 'Fatture', 
                     dataIndex: 'fat_merce', 
                     align: 'right',
                     sortable: false,
                     renderer: floatRenderer2,
                 	 width: 70},
                 	  <?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>
                     {
                     header: 'Importo merce', 
                	 dataIndex: 'importo_fat', 
                 	 align: 'right',
                 	 sortable: false,
                     renderer: floatRenderer2,
                     width: 100}
                     <?php }?>
                 
	         ],	//columns	
	         
	         listeners: {
	         
	         
	         
	         
		         itemcontextmenu : function(grid, rec, node, index, event) {
		           	var voci_menu = [];
		         	event.stopEvent();
					var grid = this;
					
					if (rec.get('liv') =='liv_2'){
					
					    voci_menu.push({
		          		text: 'Scheda scarico',
		        		iconCls : 'icon-leaf-16',	          		
		        		handler: function() {										
							window.open('acs_delivery_pos_scarico_report.php?fn=open_report&form_values=<?php echo json_encode($m_params->form_values);?>&rec_id=' + rec.get('id'));
		        			}
	    				});
	    			
	    			<?php if($js_parameters->only_view != 1){ ?>	
	    			 voci_menu.push({
		          		text: 'Assegna sequenza',
		        		iconCls : 'icon-sub_blue_add-16',	          		
		        		handler: function() {
		        		
		        		
					      list_selected_id = {};
					      list_ord = [];
						  rec.eachChild(function(rec_cli){
						        var list_ord =[];
								rec_cli.eachChild(function(rec_ord){
				   					list_ord.push({k_ordine: rec_ord.get('k_ordine')});				   				
				   				 }); 						     
                        		list_selected_id[rec_cli.get('cod_cli')] = {cod_cli: rec_cli.get('cod_cli'), d_cli : rec_cli.get('task'), seq : rec_cli.get('seq_carico'), list_ord : list_ord};
				   				
                              }); 
                              console.log(list_ord);
                              var my_listeners = {
    			    		  			afterOkSave: function(from_win){
    		        						grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};
                              
                              
                              acs_show_win_std('Aggiungi sequenza', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_seq', {
								list_selected_id: list_selected_id
							}, 400, 400, my_listeners, 'icon-sub_blue_add-16');
						 
						
		        			
		        											
		        			}
	    			});
	    			
	    			voci_menu.push({
		          		text: 'Assegna carico',
		        		iconCls: 'iconCarico',        		
		        		handler: function() {
						   
					        list_selected_id = [];
					    	rec.eachChild(function(rec_cli){
					               
						      rec_cli.eachChild(function(rec_ord){
						      console.log(rec_ord);
				   		   	  list_selected_id.push({k_ordine: rec_ord.get('k_ordine'), 
				   					                       ordine: rec_ord.get('ordine'),
				   					                       cliente: rec_ord.get('cliente'),
						   								   vettore : rec_ord.get('vettore')});				   				
				   				 }); 						     
                        	
                              }); 
                              
                                    	my_listeners = {
                						afterConf: function(from_win){
            		        				grid.getStore().load();
            		        			    from_win.close();  
            						     }
        		    				};		

                              
                              acs_show_win_std('Assegna nuovo numero carico', 'acs_form_json_assegna_carico.php?fn=open_form', {selected_id: list_selected_id}, 700, 250, my_listeners, 'iconCarico');
						 
						
		        			
		        											
		        			}
	    			});
	    			
	    			<?php }?>
					}
	 			
	 				<?php if($js_parameters->only_view != 1){ ?>
	    			//livello cliente
	    			if (rec.get('liv') == 'liv_3'){
	    		
				    voci_menu.push({
		          		text: 'Elenco documenti',
		        		iconCls : 'icon-leaf-16',	          		
		        		handler: function() {										
							acs_show_win_std('Elenco documenti cliente', 'acs_documenti_cliente.php?fn=open_list', {
								cliente_selected : rec.get('cod_cli')
							}, 1024, 600, {}, 'icon-leaf-16');
		        			}
	    			});
	    			
	    			
	    			  voci_menu.push({
		          		text: 'Report e/conto',
		        		iconCls : 'icon-print-16',	          		
		        		handler: function() {
		        			window.open('acs_report_ec.php?fn=open_report&tipo=001&f_cliente_cod='+rec.get('cod_cli'));
		        											
		        			}
	    			});
	    			
    				voci_menu.push({
        	      		text: 'Ordini aperti cliente',
        	    		iconCls : 'icon-exchange_black-16',      		
        	    		handler: function() {
        		    		console.log(rec.data);
        					acs_show_win_std('Elenco ordini aperti', 'acs_documenti_cliente.php?fn=open_list', {
        						cliente_selected : rec.get('cod_cli'), ord_aperti : 'Y'
        					}, 1024, 600, {}, 'icon-leaf-16');
        	    		}
        			  });
	    			
	    			}
	    			<?php }?>
	    			
	    			if (rec.get('k_ordine') != ''){
	    			
	    				<?php if($js_parameters->only_view != 1){ ?>
	    			
				    voci_menu.push({
		          		text: 'Assegna vettore/montatore',
		        		iconCls : 'iconSpedizione',	          		
		        		handler: function() {

						var msg_error = '';

						var list_selected_id = [];
					    	var rows = grid.getSelectionModel().getSelection();
				    		Ext.each(rows, function(item){
				    		     list_selected_id.push(item.get('k_ordine'));
				    		});
		    		
		        			my_listeners = {
		        					onSelected: function(fromWin){
		        							grid.getStore().load();
		        							fromWin.close();	
						        		}
				    				};												
							//assegna vettore					
		    			  	acs_show_win_std('Assegna vettore/montatore', 
		    			  		'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_assegna_vettore', 
		    			  		{		
		    			  		 list_selected_id: list_selected_id
		    			  		}, 600, 550, my_listeners, 'iconSpedizione');
		        			}
	    			});
	    			
	    			
	    			 
	    		voci_menu.push({
		      		text: 'Assegna carico',
		    		iconCls: 'iconCarico',
		    		handler: function() {		    
						
	                   	my_listeners = {
        						afterConf: function(from_win){
    		        				grid.getStore().load();
    		        			    from_win.close();  
    						     }
		    				};		

					  	id_selected = grid.getSelectionModel().getSelection();
					  	list_selected_id = [];
					  	var data_selected = 0;

					  	for (var i=0; i<id_selected.length; i++){ 
					  	
					  	 		if (id_selected[i].get('liv') != 'liv_4'){
									  acs_show_msg_error('Selezionare righe solo a livello ordine');
									  return false;
								 }
					  	
					  			if (id_selected[i].get('data') != data_selected && data_selected > 0){
									  acs_show_msg_error('Selezionare ordini con la stessa data di evasione programmmata');
									  return false;
								 }
								 
								
								 
						   list_selected_id.push(
						   		{k_ordine: id_selected[i].get('k_ordine'), 
						   		ordine: id_selected[i].get('ordine'),
						   		cliente: id_selected[i].get('cliente'),
						   		vettore : id_selected[i].get('vettore')});
						  	
						  	
						  	 data_selected = id_selected[i].get('data');
						   }

						acs_show_win_std('Assegna nuovo numero carico', 'acs_form_json_assegna_carico.php?fn=open_form', {selected_id: list_selected_id}, 700, 250, my_listeners, 'iconCarico')
					}	
				  });	
	    			
	    			
    	     	 	voci_menu.push({
    	         		text: 'Testata ordine',
    	        		iconCls : 'icon-pencil-16',          		
    	        		handler: function () {
    	        			    list_selected_id = grid.getSelectionModel().getSelection();
    			    		  	rec = list_selected_id[0];	
    			    		  	
    			    			var my_listeners = {
    			    		  			afterOkSave: function(from_win, new_value){
    		        						grid.getStore().load();
    		        						from_win.close();  
    						        		}
    				    				};	
    			    		  	 
    			    		  	acs_show_win_std('Modifica informazioni di testata', 
    		                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
    		                				tddocu: rec.get('k_ordine')
    									}, 800, 500, my_listeners, 'icon-shopping_cart_gray-16');
    		                }
    	    		});	   
    	    		
    	    		   	 voci_menu.push({
				      		text: 'Documenti ordine',
				    		iconCls : 'iconScaricoIntermedio',      		
				    		handler: function() {
					    		console.log(rec.data);
								acs_show_win_std('Elenco documenti ordine', 'acs_documenti_cliente.php?fn=open_list', {
									k_ordine: rec.get('k_ordine')
								}, 1100, 600, {}, 'icon-leaf-16');
				    		}
						  });
    	    		
    	    		<?php } ?>
    	    		
    	    		voci_menu.push({
		         		text: 'Righe ordine',
		        		iconCls : 'icon-leaf-16',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
								  acs_show_win_std('Visualizza righe', 'acs_righe_ordine.php?fn=open_grid', {k_ordine: rec.get('k_ordine'), art_manc : rec.get('fl_art_manc')}, 900, 550, null, 'icon-leaf-16');
			                }
		    		});
	    			
	    		<?php if($js_parameters->only_view != 1){ ?>
	    			
	    			voci_menu.push({
		         		text: 'Stampa contratto',
		        		iconCls : 'icon-print-16',          	 	
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	window.open('acs_report_pdf.php?k_ordine='+ rec.get('k_ordine'));
			                }
		    		});
		    		
		    		<?php }?>
		    		
		    		 	 voci_menu.push({
		         		text: 'Stampa scheda ordine',
		        		iconCls : 'icon-print-16',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	window.open('acs_report_pdf.php?tipo_stampa=SCHEDA&k_ordine='+ rec.get('k_ordine'));
			                }
		    		});	
		    	
		    <?php if($js_parameters->only_view != 1){ ?>
		    	if(rec.get('stato') == 'CC'){      
		    		voci_menu.push({
		         		text: 'Verbale di verifica',
		        		iconCls : 'icon-print-16',          	 	
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	window.open('acs_delivery_pos_verbale_verifica.php?k_ordine='+ rec.get('k_ordine'));
			                }
		    		});
		    		}
		    		 voci_menu.push({
		         		text: 'Genera fattura accompagnatoria',
		        		iconCls : 'icon-blog_compose-16',          		
						handler: function () {
							console.log(rec);
		        			  list_selected_id = grid.getSelectionModel().getSelection();
			    		  	  rec = list_selected_id[0];	 
							  acs_show_win_std('Fattura accompagnatoria', 'acs_fattura_accompagnatoria.php?fn=open_form', 
									  {k_ordine: rec.get('k_ordine')}, 650, 550, {
										  'afterSave': function(from_win){
											  grid.getStore().load();
											  from_win.close();											  
										  }
									  }, 'icon-leaf-16');
	
			                }
		    		});
		    		
		    		 voci_menu.push({
	         		text: 'Modifica stato', 
	        		iconCls : 'icon-clessidra-16',          	 	
	        		handler: function () {
	        		
	        		my_listeners = {
 		  			afterOkSave: function(from_win){
 		  			console.log(grid);
 						grid.getStore().treeStore.load();
 						from_win.close();  
			        		}
	    				};	
	        		
			         acs_show_win_std('Cambia stato', '../desk_vend/acs_gestione_stato_ordini.php?fn=change_state', {k_ordine: rec.get('k_ordine')}, 400, 220,  my_listeners, 'icon-clessidra-16');	
		           }
	    		});
	    		
	    	    		
	    	id_selected = grid.getSelectionModel().getSelection();
            list_selected_id = [];
		  	for (var i=0; i<id_selected.length; i++) 
			   list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});	
			   
		  	voci_menu.push({
	     		text: 'Evidenza generica',
	    		iconCls : 'icon-sub_blue_accept-16', 
	    		menu:{
	        	items:[
	                {  text: 'Rosso',
	                   iconCls : 'icon-sub_blue_accept-16',
	                   style: 'background-color: #F9BFC1;', 
	                   handler : function(){
	                     Ext.Ajax.request({
					        url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : '1'
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
					         var ar_value = jsonData.or_flag;
					         	for (var i=0; i<id_selected.length; i++) 
					        	 	for (var chiave in ar_value) {
						        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
											id_selected[i].set('tdfg06', ar_value[chiave].flag);  
						        	 }
					         }
							 },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                },
	            	{  text: 'Giallo',
	            	   iconCls : 'icon-sub_blue_accept-16', 
	            	   style: 'background-color: #F4E287;',
	            	        handler : function(){
	                     Ext.Ajax.request({
	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : '2'
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
						         var ar_value = jsonData.or_flag;
						         	for (var i=0; i<id_selected.length; i++) 
						        	 	for (var chiave in ar_value) {
							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
												id_selected[i].set('tdfg06', ar_value[chiave].flag);
							        	 }
						         }
					         
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                },{
	                    text: 'Verde',
	                    iconCls : 'icon-sub_blue_accept-16', 
	                    style: 'background-color: #A0DB8E;',
	                         handler : function(){
	                     Ext.Ajax.request({
	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : '3'
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
						         var ar_value = jsonData.or_flag;
						         	for (var i=0; i<id_selected.length; i++) 
						        	 	for (var chiave in ar_value) {
							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
												id_selected[i].set('tdfg06', ar_value[chiave].flag);
							        	 }
						         }
					         
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                },{
	                	text: 'Nessuno',
	                	iconCls : 'icon-sub_blue_accept-16', 
	                	     handler : function(){
	                     Ext.Ajax.request({
	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : ''
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
						         var ar_value = jsonData.or_flag;
						         	for (var i=0; i<id_selected.length; i++) 
						        	 	for (var chiave in ar_value) {
							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
												id_selected[i].set('tdfg06', ar_value[chiave].flag);
							        	 }
						         }
					         
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                }
	            ]
	        }        		
	    	
			});
	    		
	    		
	    	<?php } ?>
		    			
		    			}    	
		    			
		    			
		    		
		    					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);		    					    		
		    		
		         },	
	         
	         itemclick: function(view,rec,item,index,eventObj) {
	
			     
			          if (rec.get('liv') == 'liv_4')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	//var wd = Ext.getCmp('OrdPropertyGridDet');
			        	var wdi = Ext.getCmp('OrdPropertyGridImg');        	
			        	var wdc = Ext.getCmp('OrdPropertyCronologia');

			        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdi.store.load()
						wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdc.store.load()			        												        	
			        	
						Ext.Ajax.request({
						   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
					   }												        	
			         },
	         
	       		  celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();
					  	
					  		<?php if($js_parameters->only_view != 1){ ?>

					if (rec.get('liv') == 'liv_4' && col_name == 'comm'){
					
					acs_show_win_std('Annotazioni delivery POS consegna/installazione', 'acs_form_json_annotazioni_ordine.php', {from_del : 'Y', num_ann : 9, k_ordine : rec.get('k_ordine'), bl : '<?php echo $cfg_mod_DeskPVen['pv_commenti_delivery'];?>'}, 800, 400, null, 'icon-comment_edit-16');	
					

					}
					
					if (rec.get('liv') == 'liv_4' && col_name == 'comm_car'){
					
					acs_show_win_std('Annotazioni delivery POS predisposizione carico', 'acs_form_json_annotazioni_ordine.php', {from_del : 'Y', num_ann : 9, k_ordine : rec.get('k_ordine'), bl : 'CAR'}, 800, 400, null, 'icon-comment_edit-16');	
					

					}
					

					if (rec.get('liv') == 'liv_4' && col_name == 'controllato'){
					  
						 
						  Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_spunta_controllato',
						        method     : 'POST',
			        			jsonData: {
			        			    flag: rec.get('controllato'),
			        			    k_ordine: rec.get('k_ordine')
								},							        
						        success : function(result, request){
			            			jsonData = Ext.decode(result.responseText);
			            		 	rec.set('controllato', jsonData.flag);
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	 

							
						}
						
						
						
						if(rec.get('liv')=='liv_4' && col_name != 'controllato' && col_name != 'comm' && col_name != 'comm_car'){
						
							iEvent.preventDefault();
							acs_show_win_std('Segnalazione articoli critici', 'acs_art_critici.php?fn=open_art', {k_ordine: rec.get('k_ordine'), type: 'MTO'}, 950, 500, null, 'icon-shopping_cart_gray-16');	
							
								return false;
							
						}
						<?php }?>
					
						

					 }
					}
					
			},
	        
 			viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }
	        																			  			
	            
        }  

]
}


<?php 
exit;
}

if ($_REQUEST['fn'] == 'open_seq'){
    $m_params = acs_m_params_json_decode();
    

    foreach((array)$m_params->list_selected_id as $k =>$v){
        $ar_cli[$v->cod_cli] = trim($v->d_cli) ."_".$v->seq;
        
       
    }
    
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            autoScroll : true,
	            title: '',
	            
	            items: [
	          
	 	
	 	<?php foreach($ar_cli as $k => $v){
	 	    $seq_cli = explode("_", $v);
	 	    $cli =  trim($seq_cli[0]);
	 	    $seq = trim($seq_cli[1]);
	 	    $cliente = "[".$k."] ".$cli;?>
	 	
	 				 {
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{xtype : 'displayfield',
            	          		 name : 'f_cli',
            	          		 flex: 2,
            	          		 labelWidth : 60,
            	          		 value : <?php echo j($cliente)?>
            	          		
            	          		},{xtype : 'textfield',
            	          		 name : <?php echo j($k); ?>,
            	          		 margin : '0 0 0 10',
            	          		 flex: 1,
            	          		 fieldLabel: 'Seq.',
            	          		 labelWidth : 30,
            	          		 value : <?php echo j($seq); ?>,
            	          		 maxLength: 3
            	          		
            	          		}
						    
						
						]},
	 	
	 	<?php } ?>
	           
	          		
	            ],
	            
				buttons: [					
					{
			            text: 'Salva',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            var form = this.up('form');
			            var grid = this.up('form').down('grid');
						var loc_win = this.up('window');
						if (form.getForm().isValid()){	 
			                Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_sequenza',
						        jsonData: {
						        	form_values : form.getValues(),
						        	list_selected_id : <?php echo acs_je($m_params->list_selected_id) ?>
						        },
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						          loc_win.fireEvent('afterOkSave', loc_win);		            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			               }
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}