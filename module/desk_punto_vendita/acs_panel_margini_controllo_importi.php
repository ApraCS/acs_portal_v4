<?php

/* In apertura posso ricevere in m_params:
 k_ordine: es. 6 _VO_VO1_2017_000111 _ _6
 */

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));
$m_params = acs_m_params_json_decode();
$mod_js_parameters = $main_module->get_mod_parameters();



//****************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    //****************************************************
    
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_json_data($m_params)
    ));
    exit;
}




//****************************************************
// WIN PRINCIPALE
//****************************************************
//****************************************************
if ($_REQUEST['fn'] == 'open'){
    //****************************************************
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _pm_controllo_importi_main_win($m_params)
    ));
    exit;
}






function _pm_controllo_importi_main_win($m_params){
    $m_params->r_comm = "";
    
    $c = new Extjs_compo('grid');
    $c->set(array(
        'multiSelect' => false,
        'flex' => 1,
        'tbar' => extjs_code(
            " new Ext.Toolbar({
             items:['->',
             {   xtype: 'checkbox'
                 , margin : 2
                 , name: 'f_view'
                 , boxLabel: 'Righe commento'
                 , inputValue: 'Y'
                 , listeners: {change: function(comp, check){
                               var grid = this.up('grid');
                                 if(check == true){
                                     grid.store.proxy.extraParams.r_comm = comp.inputValue;
                                     grid.store.load();
                                 }else{
                                     grid.store.proxy.extraParams.r_comm = '';
                                     grid.store.load();
                                  }
                                }
                             }
             } ]})   "
            ),
        'columns'     => array(
            array('width' => 30, 'dataIndex' => 'tipologia',  'header' => 'Tp')
            , array('width' => 130, 'dataIndex' => 'documento',  'header' => 'Documento')
            , array('width' => 30, 'dataIndex' => 'stato',      'header' => 'St')
            , grid_column_h_date('Data', 'data_reg', 'w60', 'Data registrazione')
            , array('width' => 100, 'dataIndex' => 'cod_art',    'header' => 'Codice')
            , array('flex'  => 1  , 'dataIndex' => 'des_art',    'header' => 'Descrizione beni')
            , array('width' => 30 , 'dataIndex' => 'um',         'header' => 'UM')
            , grid_column_h_f2('Q.t&agrave;', 'qty', 'w40', 'Quantit&agrave;')
            , grid_column_h_f2('Importo&nbsp;<br>VENDITE', 'imp_ven', 'w80', null, array(
                'renderer' => extjs_code("
                function(value, metaData, record){
	    	      if(record.get('tp_row') == 'tot') metaData.tdCls += ' grassetto';
                     return  floatRenderer2(value);
                    
	    		}
              ")) )
            , grid_column_h_f2('Costo&nbsp;&nbsp;&nbsp;<br>PREVISTO', 'costo', 'w80', null, array(
                'renderer' => extjs_code("
                function(value, metaData, record){
	    	      if(record.get('tp_row') == 'tot') metaData.tdCls += ' grassetto';
                     return  floatRenderer2(value);
                    
	    		}
              ")))
            , grid_column_h_f2('Costo&nbsp;&nbsp;&nbsp;<br>EFFETTIVO', 'c_eff', 'w80', null, array(
                'renderer' => extjs_code("
                function(value, metaData, record){
	    	      if(record.get('tp_row') == 'tot') metaData.tdCls += ' grassetto';
                  if(record.get('fl_mts') == 'Y') metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('qtip_mts')) + '\"';
                     return  floatRenderer2(value);
                    
	    		}
              ")))
            
            , grid_column_h('Fornitore', 'forn', 'f1', 'DDT fornitore')
            , grid_column_h_img('comment_edit', 'note', 'Note di riga ddt', array(
                'renderer' => extjs_code("
                function(value, metaData, record){
	    	        if(record.get('riga') == 'DDT' && record.get('stato') != ''){
                    if (record.get('note') == 1) return '<img src=" .  img_path("icone/48x48/comment_edit_yellow.png") . " width=15>';
		     		if (record.get('note') == 0) return '<img src=" .  img_path("icone/48x48/comment_light.png") . " width=15>';
                 }
                    
                    
	    		}
              ")) )
            , grid_column_h_img('info_black', 'ddt_nc', 'Riga ddt anomala', array(
                'renderer' => extjs_code("
                function(value, metaData, record){
	    	      if(record.get('ddt_nc') == 'Y')
                    return '<img src=" .  img_path("icone/48x48/warning_red.png") . " width=15>';
                    
	    		}
              ")) )
            , grid_column_h_img('sub_black_accept', 's_black', 'Ddt controllato', array(
                'renderer' => extjs_code("
                function(value, metaData, record){
	    	        if (record.get('s_black') == 'Y') return '<img src=" .  img_path("icone/48x48/sub_black_accept.png") . " width=15>';
                    
	    		}
              ")) )
        ),
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('TDDOCU', 'documento', 'cod_art', 'des_art', 'um', 'qty', 'pr_unit', 'costo',
                'tipologia', 'stato', 'data_reg', 'imp_ven', 'c_eff', 'ddt_nc', 'forn', 'note',
                'riga', 'nrec', 'rife1', 's_black', 'tp_row', 'fl_mts', 'qtip_mts', 'k_ordine'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_json_data'), $m_params) ),
        'listeners'   => array(
            'itemclick' => extjs_code(grid_itemclick("
              //ToDo
                return false;
                var me = this,
                    m_panel  = this.up('#art_adv'),
                    form_add = m_panel.down('#form_add');
           ")),
            'celldblclick' => extjs_code(grid_celldblclick("
                var grid = this;
                if(col_name == 'note'){
                
                var my_listeners = {
                afterSave: function(from_win, jsonData){
                grid.getStore().load();
                from_win.close();
}
}
                
                acs_show_win_std('Blocco note di riga', '../desk_acq/acs_fatture_entrata.php?fn=blocco_note', {
                k_ordine: '{$m_params->k_ordine}',
                rife1 : rec.get('rife1'),
                nrec : rec.get('nrec'),
                only_view : 'Y'
}, 350, 350, my_listeners, 'icon-blog_compose-16');

}
")),            'itemcontextmenu' => extjs_code(grid_itemcontextmenu("
             event.stopEvent();
                var voci_menu = [];
                if(rec.get('tipologia') == 'AO')
                voci_menu.push({
	         			text: 'Modifica costo confermato',
	        			iconCls : 'icon-pencil-16',             		
	        			handler: function () {
                           var grid = view;
	                       my_listeners = {
				        					afterSave: function(from_win){
    				        					grid.getStore().load();
	 											from_win.close();
	 										}
										}
                            acs_show_win_std('Costo confermato', 'acs_panel_margini_controllo_importi.php?fn=open_form', {
                            row : rec.data
                            }, 300, 150, my_listeners, 'icon-pencil-16');
	        		  	
		                }
	    		});

                 var menu = new Ext.menu.Menu({
			            items: voci_menu
        					}).showAt(event.xy);   
            "))
        ),
        'viewConfig' => array(
            'getRowClass' =>extjs_code(
                "function(record, index) {
				if (record.get('ddt_nc') == 'Y') return ' colora_riga_rosso';
			    return '';
				}   "
                
                )
        )
        ));
    return $c->code();
}




/* ------------------------------------------------------------- */
function _get_json_data($m_params){
    /* ------------------------------------------------------------- */
    global $conn, $id_ditta_default, $cfg_mod_DeskPVen, $cfg_mod_DeskArt, $s, $main_module, $cfg_mod_DeskAcq;
    
    $sql_where = "";
    $sql_where.= sql_where_by_combo_value('TDDOCU', $m_params->k_ordine);
    
    
    //RIGHE ORDINE -------------
    if($cfg_mod_DeskPVen["RTMOLT_sottocampato_da"] != 0){
        $rtmolt = " case when trim(substr(rtfil1, ".$cfg_mod_DeskPVen["RTMOLT_sottocampato_da"].", 12))='' then 0 else
                    dec(substr(rtfil1, ".$cfg_mod_DeskPVen["RTMOLT_sottocampato_da"].", 12)) / 100000 end as rtmolt";
    }else{
        $rtmolt = "RTMOLT";
    }
    
    if(trim($m_params->r_comm) == ""){
        $sql_where .= " AND SUBSTRING(RD.RDART, 1, 1) <> '*'";
        $no_comm = "Y";
    }else{
        $sql_where .= "";
        $no_comm = "";
    }
    
    $sql = "SELECT TD.*, RDART, RDDART, RDRIGA, RDQTA, RDQTE, RDUM, RTPRZ, RTINFI, {$rtmolt}, RDNREC, RDSTAT, RDSTEV
    , MTNRDQ, MTAADQ, MTTIDQ, TD_ACQ.TDTPDO AS TIPO_ACQ
    FROM {$cfg_mod_DeskPVen['file_testate']} TD
    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
    ON TD.TDDT=RDDT AND TD.TDOTID=RDTIDO AND TD.TDOINU=RDINUM AND TD.TDOADO=RDAADO AND TD.TDONDO=RDNRDO
    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
    ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art']} AR
    ON AR.ARDT=RD.RDDT AND AR.ARART = RD.RDART
    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_proposte_MTO']} MT
    ON MTDT=TD.TDDT AND MTTIDO=TD.TDOTID AND MTINUM=TD.TDOINU AND MTAADO=TD.TDOADO AND MTNRDO=TD.TDONDO AND MTNREC = RDNREC AND MTSTAR <> 'R'
    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate_doc_gest']} TD_ACQ
    ON MT.MTDT=TD_ACQ.TDDT AND MT.MTTIDQ=TD_ACQ.TDTIDO AND MT.MTINUQ=TD_ACQ.TDINUM AND MT.MTAADQ=TD_ACQ.TDAADO AND MT.MTNRDQ=TD_ACQ.TDNRDO
    WHERE RD.RDART NOT IN('ACCONTO', 'CAPARRA') AND RD.RDTISR = '' AND RD.RDSRIG = 0
    {$sql_where}
    ORDER BY MTAADQ, MTNRDQ, RD.RDRIGA";
    
        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ret = array();
    $tot_vo = 0;
    $n_vo = 0;
    while ($row = db2_fetch_assoc($stmt)) {
        
        $n = array();
        $n['documento'] = "{$row['TDOADO']}_{$row['TDONDO']}_{$row['TDOTPD']}";
        $n['tipologia'] = $row['TDOTID'];
        $n['stato'] = $row['TDSTAT'];
        $n['data_reg'] = trim($row['TDODRE']);
        $n['cod_art']   = trim($row['RDART']); $n['des_art']   = trim($row['RDDART']);
        $n['um']        = trim($row['RDUM']);
        $numero = sprintf("%06s", $row['MTNRDQ']);
        if($row['MTAADQ'] > 0)
            $n['forn']  = "[{$row['MTTIDQ']}: {$row['MTAADQ']}_{$numero}_{$row['TIPO_ACQ']}]";
        if($row['RDQTA'] > 0) $n['qty'] = $row['RDQTA'];
        //$n['pr_unit']   = (float)$row['RTPRZ'];
        $n['imp_ven']    = (float)$row['RTINFI'];
        $n['costo']   = (float)$row['RTMOLT'];
        $tot_vo_rtmolt += (float)$row['RTMOLT'];
        $tot_vo_rtinfi += (float)$row['RTINFI'];
        $n_vo ++;
        $ret[] = $n;
    }
    if($n_vo > 0){
        $n = array();
        $n['documento'] = "<b>Totale Ordini vendita</b>";
        $n['costo'] = $tot_vo_rtmolt;
        $n['imp_ven'] = $tot_vo_rtinfi;
        $n['tp_row'] = "tot";
        $ret[] = $n;
    }

    
    //RIGHE ORDINE A FORNITORE -------------
    
    $sql = "SELECT TD.*, MT.*, TOVMAR, CFRGS1, TD_ACQ.TDSTAT AS STATO_ACQ, TD_ACQ.TDTPDO AS TIPO_ACQ, TD_ACQ.TDDTRG AS DATA
            FROM {$cfg_mod_DeskPVen['file_testate']} TD
            LEFT OUTER JOIN(
               SELECT MTDT, MTTIDQ, MTINUQ, MTAADQ, MTNRDQ, MTTIDO, MTINUM, MTAADO, MTNRDO, MTFORN
               FROM {$cfg_mod_DeskPVen['file_proposte_MTO']}
               WHERE MTTIDQ = 'AO' AND MTSTAR = ''
               GROUP BY MTDT, MTTIDQ, MTINUQ, MTAADQ, MTNRDQ,MTTIDO, MTINUM, MTAADO, MTNRDO, MTFORN) MT 
            ON MT.MTDT=TD.TDDT AND MT.MTTIDO=TD.TDOTID AND MT.MTINUM=TD.TDOINU AND MT.MTAADO=TD.TDOADO AND MT.MTNRDO=TD.TDONDO 
            LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate_gest_valuta']} TO1 
            ON TO1.TODT = MT.MTDT AND TO1.TOTIDO = MT.MTTIDQ AND  TO1.TOINUM = MT.MTINUQ AND TO1.TOAADO= MT.MTAADQ AND TO1.TONRDO = MT.MTNRDQ AND TO1.TOVALU = 'EUR'
            LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_cli']} CF
            ON MT.MTDT = CF.CFDT AND MT.MTFORN = CF.CFCD AND CFTICF = 'F'
            LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate_doc_gest']} TD_ACQ
	        ON MT.MTDT=TD_ACQ.TDDT AND MT.MTTIDQ=TD_ACQ.TDTIDO AND MT.MTINUQ=TD_ACQ.TDINUM AND MT.MTAADQ=TD_ACQ.TDAADO AND MT.MTNRDQ=TD_ACQ.TDNRDO   
            WHERE TDDOCU = '{$m_params->k_ordine}'
            ORDER BY MTAADQ, MTNRDQ";
    
    if($cfg_mod_DeskPVen["RTMOLT_sottocampato_da"] != 0){
        $rtmolt = " case when trim(substr(rtfil1, ".$cfg_mod_DeskPVen["RTMOLT_sottocampato_da"].", 12))='' then 0 else
                    dec(substr(rtfil1, ".$cfg_mod_DeskPVen["RTMOLT_sottocampato_da"].", 12)) / 100000 end";
        $sum_rtmolt = " SUM(FLOAT($rtmolt)) AS RTMOLT";
    }else{
        $sum_rtmolt = " SUM(RTMOLT) AS RTMOLT";
    }
    
    $sql_cp = "SELECT {$sum_rtmolt}
               FROM {$cfg_mod_DeskPVen['file_proposte_MTO']} MT
               LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
               ON MT.MTDT= RT.RTDT AND MT.MTTIDO = RT.RTTIDO AND MT.MTINUM = RT.RTINUM AND MT.MTAADO = RT.RTAADO AND MT.MTNRDO = RT.RTNRDO AND MT.MTNREC = RT.RTNREC AND RTVALU='EUR'           
               WHERE MTDT = ? AND MTTIDQ = ? AND MTINUQ = ? AND MTAADQ = ? AND MTNRDQ = ?
            ";
    
    $stmt_cp = db2_prepare($conn, $sql_cp);
    echo db2_stmt_errormsg();
       
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $tot_ao = 0;
    $tot_cp = 0;
    $n_ao = 0;
    while ($row = db2_fetch_assoc($stmt)) {
        
        $n = array();
        $numero = sprintf("%06s", $row['MTNRDQ']);
        $n['documento'] = "{$row['MTAADQ']}_{$numero}_{$row['TIPO_ACQ']}";
        $n['k_ordine'] = implode("_", array($row['MTDT'], $row['MTTIDQ'], $row['MTINUQ'], $row['MTAADQ'], $numero));
       
        $of = $s->k_ordine_td_decode_xx($n['k_ordine']);
        $result = db2_execute($stmt_cp, $of);
        $row_of = db2_fetch_assoc($stmt_cp);
        $n['costo']   = (float)$row_of['RTMOLT'];
             
        $n['tipologia'] =  trim($row['MTTIDQ']);
        $n['stato'] = $row['STATO_ACQ'];
        $n['data_reg'] = $row['DATA'];
        $n['c_eff'] = $row['TOVMAR'];
        $n['forn']  = $row['CFRGS1'];
        $tot_ao +=  $n['c_eff'];
        $tot_cp +=  $n['costo'];
        $n_ao ++;
        $ret[] = $n;
        
    }
    if($n_ao > 0){
        $n = array();
        $n['documento'] = "<b>Totale costo confermato</b>";
        $n['costo']  = $tot_cp;
        $n['c_eff']  = $tot_ao;
        $n['tp_row'] = "tot";
        $ret[] = $n;
    }
   
    
    //RIGHE CARRELLO
    $tot_ddt = 0;
    $n_ddt = 0;
    $ord_exp = explode('_', $m_params->k_ordine);
    
    if(trim($m_params->r_comm) == "")
        $mt_where .= " AND SUBSTRING(MT.MTART, 1, 1) <> '*' AND MT.MTSTAR NOT IN('R', 'C') ";
        
        
        $sql = "SELECT MT.*, RD.*, TD_DDT.TDTPDO AS TIPO, TD_DDT.TDSTAT AS STATO, TD_DDT.TDDTRG AS DATA,
        RT.RTINFI, CF.CFRGS1, RF.RFFG03,
        LI.LIPRZ, LI.LISC1, LI.LISC2, LI.LISC3, LI.LISC4, LI.LIFIL1,
        CF.CFSC1, CF.CFSC2, CF.CFSC3, CF.CFSC4,
        RD3.RDQTA3
        
        FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MT
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
        ON MTDT=RT.RTDT AND MTTIDE=RT.RTTIDO AND MTINUE=RT.RTINUM AND MTAADE=RT.RTAADO AND MTNRDE=RT.RTNRDO AND MTNREE=RT.RTNREC AND RT.RTVALU='EUR'
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate_doc_gest']} TD_DDT
        ON MTDT=TD_DDT.TDDT AND MTTIDE=TD_DDT.TDTIDO AND MTINUE=TD_DDT.TDINUM AND MTAADE=TD_DDT.TDAADO AND MTNRDE=TD_DDT.TDNRDO
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_cli']} CF
        ON MT.MTDT = CF.CFDT AND MT.MTFORN = CF.CFCD AND CFTICF = 'F'
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc']} RD
        ON RD.RDDT = MTDT AND RD.RDTIDO = MTTIDE AND RD.RDINUM = MTINUE AND RD.RDAADO = MTAADE AND RD.RDNRDO = MTNRDE AND RD.RDNREC = MTNREE
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_fatture']} RF
        ON RF.RFDT = MT.MTDT AND RF.RFTIDE = MT.MTTIDE AND RF.RFINUE = MT.MTINUE AND RF.RFAADE = MT.MTAADE AND RF.RFNRDE = digits(MT.MTNRDE)
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_listini']} LI
            ON  LI.LIDT = MT.MTDT AND LIVALU = 'EUR' AND LITPLI = 'A'  AND LILIST = 'EURO'
            AND  LI.LIART = MT.MTART  AND LI.LICCON = MT.MTFORN
            AND LI.LIVAR1 = '' AND LI.LIVAR2='' AND LI.LIVAR3='' AND LI.LIDIM1=0 AND LI.LIDIM2=0 AND LI.LIDIM3=0 AND LI.LIDIM4=0 AND LI.LIDIM5 =0
        
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe']} RD3
            ON RD3.RDDT = MTDT AND RD3.RDOTID = MTTIDO AND RD3.RDOINU = MTINUM AND RD3.RDOADO = MTAADO AND RD3.RDONDO = MTNRDO
            AND RD3.RDPMTO = SUBSTRING(MTFILL, 11, 8)
        
        WHERE MTDT = '{$ord_exp[0]}' AND MTTIDO = '{$ord_exp[1]}' AND MTINUM = '{$ord_exp[2]}'
            AND MTAADO = '{$ord_exp[3]}' AND MTNRDO = '{$ord_exp[4]}'            
        {$mt_where}";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        
        while ($row = db2_fetch_assoc($stmt)) {
            $n = array();
            
            if ($row['MTSTAR'] == 'M'){ //MTS
                $n['fl_mts'] = 'Y';
                $n['documento'] = 'Da listino';
                
                $n['c_eff_unit_lordo'] = (float)$row['LIPRZ']; //da listino: prezzo unit.
                
                //Sconti da listino
                $n['li_sc1'] = n($row['LISC1']);
                $n['li_sc2'] = n($row['LISC2']);
                $n['li_sc3'] = n($row['LISC3']);
                $n['li_sc4'] = n($row['LISC4']);
                $n['azzera_sconti'] = trim(substr($row['LIFIL1'], 2, 1));
                
                //Sconti da anag. fornitore
                $n['fo_sc1'] = n($row['CFSC1']);
                $n['fo_sc2'] = n($row['CFSC2']);
                $n['fo_sc3'] = n($row['CFSC3']);
                $n['fo_sc4'] = n($row['CFSC4']);
                
                //costo effettivo: applico sconti da listino/fornitore e moltip. per qta
                $n['c_eff'] = $row['RDQTA3'];
                
                
                $n['qtip_mts'] = "Sconti list. : {$n['li_sc1']}% {$n['li_sc2']}% {$n['li_sc3']}% {$n['li_sc4']}%
                <br>Sconti forn. : {$n['fo_sc1']}% {$n['fo_sc2']}% {$n['fo_sc3']}% {$n['fo_sc4']}%
                <br>Azzera sconti : {$n['azzera_sconti']}
                <br>Prezzo listino: {$n['c_eff_unit_lordo']}";
                
            }
            else {
                if($row['RDQTA'] > 0) $n['qty']  = $row['RDQTA'];
                if($row['MTAADE'] > 0)
                    $n['documento']=implode("_", array($row['MTAADE'], sprintf("%06s", $row['MTNRDE']), $row['TIPO']));
                $n['c_eff'] = (float)$row['RTINFI'];
            }
            
            $n['tipologia'] = $row['MTTIDE'];
            $n['riga'] = 'DDT';
            $n['stato'] = trim($row['STATO']);
            $n['data_reg'] = $row['DATA'];
            $n['cod_art']   = trim($row['MTART']);
            $n['des_art']   = trim($row['MTDART']);
            $n['qty']   = $row['MTQTCO'];
            $n['um']    = trim($row['MTUMCO']);
            
            $tot_ddt +=  $n['c_eff'];
            $n_ddt ++;
            if(trim($row['RDPRIO']) == 'NC')
                $n['ddt_nc'] = 'Y';
                
                $n['forn'] = trim($row['CFRGS1']);
                $n['nrec'] = sprintf("%06s", $row['MTNREE']);
                $n['rife1'] =  $row['MTTIDE'].$row['MTINUE'].$row['MTAADE'].sprintf("%06s", $row['MTNRDE']).sprintf("%06s", $row['MTNREE']);
                
                $desk_acq = new DeskAcq();
                $n['note']  = $desk_acq->has_commento_riga($n['rife1'] , 'PNC');
                $n['s_black']  = trim($row['RFFG03']);
                
                
                
                $ret[] = $n;
        } //while
        if($n_ddt > 0){
            $n = array();
            $n['documento'] = "<b>Totale Ddt fornitore</b>";
            $n['c_eff'] = $tot_ddt;
            $n['tp_row'] = "tot";
            $ret[] = $n;
        }
        
        //RIGHE FATTURE (ABBINATE A K_ORDINE)
        $tot_vf = 0;
        $n_vf = 0;
        $rows = $main_module->_get_rd_gest_abbinate_by_k_ordine($m_params->k_ordine, 'VF', $no_comm);
        foreach($rows as $row){
            $n = array();
            $n['documento'] = implode(' / ', array('Fattura', $row['RDNRDO'], $row['RDAADO']));
            $n['tipologia'] = $row['RDTIDO'];
            $n['stato'] = $row['TDSTAT'];
            $n['data_reg'] = trim($row['TDDTRG']);
            $n['cod_art']   = trim($row['RDART']); $n['des_art']   = trim($row['RDDART']);
            $n['um']        = trim($row['RDUM']);
            if($row['RDQTA'] > 0) $n['qty'] = $row['RDQTA'];
            $n['iva'] = $row['RTINFI'] * $row['RDALIV']/100;
            $n['imp_ven']    = (float)$row['RTINFI'] + $n['iva'];
            $tot_vf +=  $n['imp_ven'] ;
            $n_vf ++;
            $ret[] = $n;
        }
        if($n_vf > 0){
            $n = array();
            $n['documento'] = "<b>Totale Fatture vendita</b>";
            $n['tp_row'] = "tot";
            $n['imp_ven'] = $tot_vf;
            $ret[] = $n;
        }
        
        
        
        
        // FINANZIAMENTI!!!! ******************************
        $oe = $s->k_ordine_td_decode($m_params->k_ordine);
        
        $sql = "SELECT *
                FROM {$cfg_mod_DeskPVen['file_righe']}
                WHERE RDDT=" . sql_t($oe['TDDT']) . " AND RDOTID=" . sql_t($oe['TDOTID'])  . "
				  AND RDOINU=" . sql_t($oe['TDOINU']) . " AND RDOADO=" . sql_t($oe['TDOADO'])  . " AND RDONDO=" . sql_t($oe['TDONDO'])  . "
				  AND RDTPNO='SPECO' ";

        $stmtFin = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmtFin);
        
        $tot_fin = 0;
        $n_fin = 0;
        while ($row = db2_fetch_assoc($stmtFin)) {
            
            $n = array();
            $n['cod_art']   = trim($row['RDART']); 
            $n['des_art']   = trim(utf8_encode($row['RDDES1']));
           
            $n['c_eff']      = (float)$row['RDQTA3'];
            $tot_fin += $n['c_eff'];
            $n_fin ++;
            $ret[] = $n;
        }
        if($n_fin > 0){
            $n = array();
            $n['documento'] = "<b>Totale costi</b>";
            $n['c_eff'] = $tot_fin;
            $n['tp_row'] = "tot";
            $ret[] = $n;
        }
        
        
        
        /*
         $n = array();
         $n['des_art'] = "<b>Margine previsto</b>";
         $n['tp_row'] = "tot";
         $n['imp_ven'] = $tot_vo_rtinfi - $tot_vo_rtmolt;
         $ret[] = $n;
         
         $n = array();
         $n['des_art'] = "<b>Margine effettivo</b>";
         $n['tp_row'] = "tot";
         $n['imp_ven'] = $tot_vf - $tot_ddt;
         $ret[] = $n;
         */
        return grid_to_json_data($ret);
}



if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $m_params = acs_m_params_json_decode();
    
        
        $sh = new SpedHistory();
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'MOD_IMP_AO',
                "k_ordine"	=> $m_params->k_ordine,
                "vals" => array(
                    "RIIMPO" => sql_f($m_params->form_values->f_importo),
                )
            )
            );
 
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

//****************************************************
if ($_REQUEST['fn'] == 'open_form'){
    //****************************************************
 ?>
 {"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            
					buttons: ['->',
			         
			          {
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var loc_win = this.up('window');
			           		Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    k_ordine : '<?php echo $m_params->row->k_ordine; ?>', 
 			        			    form_values: form.getValues()
 								},							        
 						        success : function(result, request){
 						        	var jsonData = Ext.decode(result.responseText);
 						        	loc_win.fireEvent('afterSave', loc_win);
		 			              },
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
		 				   });	
		 	
			            }
			         }	
						], items: [
						       {						
									name: 'f_importo',
									xtype: 'numberfield',
									fieldLabel: 'Importo',
									value: '<?php echo $m_params->row->c_eff; ?>', 
									flex: 1,
									hideTrigger:true				
								 },
					
									 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}
 
 <?php 
    exit;
}
