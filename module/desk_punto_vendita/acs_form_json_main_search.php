<?php

require_once "../../config.inc.php";
$mod_provenienza = new DeskPVen();
$cod_mod_provenienza = $mod_provenienza->get_cod_mod();

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

//parametri del modulo (non legati al profilo)
$mod_js_parameters = $main_module->get_mod_parameters();

$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            autoScroll: true,
            url: 'abcdeeeee.php',
            
			buttonAlign:'center',            
            
            items: [
            
					 
					 
		 {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,			
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            url : <?php
		            		$cfg_mod = $s->get_cfg_mod();
		            		if ($cfg_mod['anagrafiche_cliente_da_modulo'] == 'Y')
		            			echo acs_je('acs_get_select_json.php?select=search_cli');
		            		else
		            			echo acs_je('acs_get_select_json.php?select=search_cli_anag');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc', 'tel', 'cod_fi'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            	var form = this.up('form').getForm();
	            	form.findField('f_destinazione_cod').setValue(''); 	            	
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc} Tel. {tel} [CF]:{cod_fi}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }
					 
					 
					 
					 
					 
		, {
            xtype: 'combo',
			name: 'f_destinazione_cod',
			fieldLabel: 'Destinazione',
			minChars: 2,			
            
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_cli_des_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            },
                 extraParams: {
        		  	cliente_cod: ''
        		 }		            
		        },       
				fields: ['cod', 'descr'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: false, //mostro la freccetta di selezione
            anchor: '100%',

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessuna destinazione trovata',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        }
        

					 
					 
				  ,{ 
						flex: 1,
						xtype: 'fieldcontainer',
						layout: 'hbox',						
						items: [{						
									name: 'f_riferimento',
									xtype: 'textfield',
									fieldLabel: 'Riferimento',
									value: '',
									//anchor: "-180", 
									flex: 1,					
								 }							
						
						, {
							flex: 1,
							name: 'f_stabilimento',
							xtype: 'combo',
							fieldLabel: 'Sede',
							labelAlign: 'right',
							multiSelect : true,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?> 	
								    ] 
								}						 
							}							 
						 
						]
					},
						 { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							labelAlign: 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							}
						
						]
					}
					
					
					
					/* priorita', modello */
					, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							flex: 1,						
							name: 'f_priorita',
							xtype: 'combo',
							fieldLabel: 'Priorit&agrave;',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_priorita(), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_modello',
							xtype: 'combo',
							fieldLabel: 'Modello',
							labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_modello(), '') ?> 
								    ] 
								}						 
							}
						
						]
					}
					

					/* referente ordine e cliente */
					, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							flex: 1,						
							name: 'f_referente_cliente',
							xtype: 'combo',
							fieldLabel: 'Referente cliente',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_referente_cliente(), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_referente_ordine',
							xtype: 'combo',
							fieldLabel: 'Referente ordine',
							labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_referente_ordine(), '') ?> 
								    ] 
								}						 
							}
						
						]
					}					
					
					
					/* conf.prog, hold */
					, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [
						 {
							flex: 1,							
							name: 'f_confermati',
							xtype: 'combo',
							fieldLabel: 'Conf.Progr.',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: 'T', text: 'Tutti'},
								     {id: 'C', text: 'Confermati'},
								     {id: 'N', text: 'Da confermare'}
								    ] 
								}						 
							}
						 , {
							flex: 1,							
							name: 'f_hold',
							xtype: 'combo',
							fieldLabel: 'Hold', labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: 'T', text: 'Tutti'},
								     {id: 'H', text: 'Solo Hold'},
								     {id: 'P', text: 'Hold esclusi'}
								    ] 
								}						 
							}								
						
						]
					}					
					
					
					/* num.ord / solo bloccati */					
					, { 
						flex: 1,
						xtype: 'fieldcontainer',
						layout: 'hbox',						
						items: [
						
						  {
						  	flex: 1,
							xtype: 'container',
							items: [
								{						
									name: 'f_num_ordine',
									xtype: 'textfield',
									fieldLabel: 'Nr. Ordine',
									value: '',
									margin : '0 0 0 0',
									anchor: "-180", width: 200					
								 }							
							]						  						  
						  } 
						
						, {
							flex: 1,							
							name: 'f_solo_bloccati',
							xtype: 'combo',
							fieldLabel: 'Bloccati', labelAlign: 'right',							
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	margin : '0 0 0 0',														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: 'T', text: 'Tutti'},
								     {id: '1', text: 'Solo bloccati'},
								     {id: 'N', text: 'Bloccati esclusi'}
								    ] 
								}						 
							}								 
						 
						]
					}

					 
			,{
	            xtype:'fieldset',
	            title: 'Selezione date',
	            collapsible: true,
	            collapsed: true, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[
					{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data programm.',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}

					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data spedizione',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_sped_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_sped_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
					
					
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data rilascio',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_ril_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_ril_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
										
					
					
					

					
					
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data ricezione',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_ricezione_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_ricezione_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
					

					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data conferma',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_conferma_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_conferma_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
						
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data scarico',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_scarico_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_scarico_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}						
						
						   
						
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data disponib.',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_disp_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_disp_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}		
					
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data evas. rich.',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_ev_rich_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_ev_rich_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , width: 220
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}						
									
												
						
						   
						            
	            ]
	        }		
	        
	     ,{
	            xtype:'fieldset',
	            title: 'Selezione carico',
	            collapsible: true,
	            collapsed: true, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[
	            
					 {
			            xtype: 'fieldset',
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding: 0px;',
						border: false,
						defaults: {
			                //anchor: '100%',
			                width: 75, labelWidth: 65
			            },
			            items: [{
		                    	xtype: 'label',
		                    	text: 'Carico spedizione',
		                    	width: 110
		                    }, {
			                boxLabel: 'Tutti',
			                name: 'f_carico_assegnato',
			                checked: true,			                
			                inputValue: 'T'
			            }, {
			                boxLabel: 'Con carico',
			                checked: false,			                
			                name: 'f_carico_assegnato',
			                inputValue: 'Y',
			                width: 95, labelWidth: 85
			            }, {
			                boxLabel: 'Senza',
			                checked: false,			                
			                name: 'f_carico_assegnato',
			                inputValue: 'N'		                
			            },  {
							name: 'f_num_carico',
							xtype: 'textfield',
							fieldLabel: 'Nr. Carico',
							value: '',
			                width: 130, labelWidth: 60							
						 }]
			        }, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',					
						items: [
						  {
                				name: 'f_tipo_ordine',
                				xtype: 'combo',				
                				flex: 1,
                				fieldLabel: 'Tipo ordine',
                				displayField: 'text',
                				valueField: 'id',
                				emptyText: '- seleziona -',
                				forceSelection: true,
                			   //	allowBlank: false,
                				store: {
                					autoLoad: true,
                					editable: false,
                					autoDestroy: true,
                				    fields: [{name:'id'}, {name:'text'}],
                				    data: [
                					    <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'VO',  null, null, 0, '', 'Y'), '') ?>
                					    ]
                				}
                
                				
                			}, 	
                			{
                			flex : 1,
							name: 'f_vettore',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Vettore',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,	
						    labelAlign: 'right',						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('AUTR'), '') ?>	
								    ] 
							}						 
							}							 
						 
						]
					},
					{
							name: 'f_ordini_evasi',
							xtype: 'radiogroup',
							fieldLabel: 'Ordini evasi',
							labelAlign: 'left',
						   	allowBlank: true,
						    items: [{
		                            xtype: 'radio'
		                          , name: 'f_ordini_evasi' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: 'T'
		                         // , width: 50
		                         <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] != 'Y') { ?>			                
			                      , checked: true
							<?php } ?>
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_ordini_evasi' 
		                          , boxLabel: 'Da evadere'
		                          , inputValue: 'N'
								  <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') { ?>			                
			                      , checked: true
							      <?php } ?>
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_ordini_evasi' 
		                          , boxLabel: 'Evasi'
								  , checked: false	
		                          , inputValue: 'Y'
		                          
		                        }]
						},
						{
							name: 'f_piva',
							xtype: 'radiogroup',
							fieldLabel: 'Clienti con P.IVA',
							labelAlign: 'left',
						   	allowBlank: true,
						    items: [{
		                            xtype: 'radio'
		                          , name: 'f_piva' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'T'
		                         // , width: 50
		                      		, checked: true
							
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_piva' 
		                          , boxLabel: 'Esclusi'
		                          , inputValue: 'N'
								
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_piva' 
		                          , boxLabel: 'Solo'
								  , checked: false	
		                          , inputValue: 'Y'
		                          
		                        }]
						}
	            ]
	         }				
					
					
										
					

					
					
				],
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "MAIN_SEARCH");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
					var form_values = form.getValues();

		                            //waitMsg:'Loading...',

		                            	mp = Ext.getCmp('m-panel');
         	
<?php
	    $ss .= "<span class=acs_button style=\"\">";
	    $ss .= "<img itemId=\"print\" src=" . img_path("icone/48x48/print.png") . " height=20>";
	    $ss .= "</span>";
	    
	    $ss = '';
?>	    
		                            	
/* ELENCO ORDINI */        
	var colElencoOrdiniSearch = [	
		{text: 'Punto vendita/Cliente-Destinazione &nbsp;&nbsp;<?php echo $ss; ?>', 		xtype: 'treecolumn', width: 250, dataIndex: 'task',
    		  menuDisabled: true, sortable: false,		
          	  renderer: function (value, metaData, record, row, col, store, gridView){						
  				if (record.get('liv') == 'liv_3' || record.get('liv') == 'liv_2') metaData.tdCls += ' auto-height';
  				
  	  			if (record.get('tooltip_ripro').length > 0)
	    		 	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_ripro')) + '"';
	    		 	  															
  				return value;			    
				}		
		},

		<?php
			//inclusione colonne flag ordini
			$js_include_flag_ordini = array('menuDisabled' => true, 'sortable' => true);
			require("../desk_vend/_include_js_column_flag_ordini.php"); 
					
		?>,	
			    	
			    	
		{text: 'Seq', 			width: 33, dataIndex: 'seq_carico',
    		  renderer: function(value, metaData, record){
        		  
		    	if (record.get('liv') == 'liv_2') return value;
		    		
		    	if (record.get('liv') == 'liv_3' && value== 'S'){ //contrassegno
	    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_seq_carico')) + '"';			    	
			    	 return '<img src=<?php echo img_path("icone/48x48/bandiera_grey.png") ?> width=18>';
		    	}
		    	if (record.get('liv') == 'liv_3' && value== 'I'){ //indice rottura
	    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_seq_carico')) + '"';			    	
			    	 return '<img src=<?php echo img_path("icone/48x48/sticker_black.png") ?> width=18>';
		    	}		    	
		    	if (record.get('liv') == 'liv_3' && value== 'SI'){ //contrassegno e indice rottura
	    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_seq_carico')) + '"';			    	
			    	 return '<img src=<?php echo img_path("icone/48x48/sticker_blue.png") ?> width=18>';
		    	}
		    	
		    	
		    		
		    	}		
		},
	    {text: 'Localita\'/Riferimento', 	flex: 1, dataIndex: 'riferimento'},
    	{text: 'Tp', 			width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd', 
	    	    	renderer: function (value, metaData, record, row, col, store, gridView){						
						metaData.tdCls += ' ' + record.get('raggr');	
						if (record.get('qtip_tipo') != "")	    			    	
							metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';
															
					return value;			    
			}
			  , filter: {type: 'string'}, filterable: true
			},
	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', renderer: date_from_AS},
	    {text: 'St', 			width: 30, dataIndex: 'stato',
	      renderer: function(value, metaData, record){
	    		if (record.get('qtip_stato') != "")	    			    	
					metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';	    			    
	    	   			    
			return value;
		}},	    	    
	    {text: 'Pr', 			width: 35, dataIndex: 'priorita', tdCls: 'tpPri',
				renderer: function (value, metaData, record, row, col, store, gridView){
					if (record.get('tp_pri') == 4)
						metaData.tdCls += ' tpSfondoRosa';

					if (record.get('tp_pri') == 6)
						metaData.tdCls += ' tpSfondoCelesteEl';	
						
				    if (record.get('qtip_pri') != "")	    			    	
						metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_pri')) + '"';				
					
					return value;
				    
	    			}},
		{text: 'Consegna<br>richiesta',	width: 60, dataIndex: 'cons_rich', renderer: date_from_AS},	    	    	    
	    {text: 'Colli',			width: 35, dataIndex: 'colli', align: 'right'},	       
	   
	    {text: 'D',	width: 30, tdCls: 'tdAction', 
            <?php if($cfg_info_columns_properties['info_hidden_colli_DS'] == 'Y'){?>
            hidden:true,
            <?php }?>
	    renderer: function(value, p, record){
	    	if (parseInt(record.get('colli_disp')) == 0 ) return '';
	    	if (parseInt(record.get('colli_disp')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none.png") ?> width=18>';			    	
	    	if (parseInt(record.get('colli_disp')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?> width=18>';			    	
	    	}},	    
	    {text: 'S',	width: 30, tdCls: 'tdAction',
	    <?php if($cfg_info_columns_properties['info_hidden_colli_DS'] == 'Y'){?>
            hidden:true,
            <?php }?>
			    renderer: function(value, p, record){
			    	if (parseInt(record.get('colli_sped')) == 0) return '';
			    	if (parseInt(record.get('colli_sped')) <  parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_none_black.png") ?> width=18>';			    	
			    	if (parseInt(record.get('colli_sped')) >= parseInt(record.get('colli'))) return '<img src=<?php echo img_path("icone/48x48/star_full_green.png") ?> width=18>';			    	
			    	
			    	}},
				
		<?php if($cfg_info_columns_properties['info_show_vettore'] == 'Y'){?>	
		 {text: 'Vettore/Trasp.', width: 80, dataIndex: 'vettore'},
		<?php } 
		if($cfg_info_columns_properties['info_show_architetto'] == 'Y'){?>    	
	    {text: 'Architetto', width: 80, dataIndex: 'architetto'},
		<?php } ?>  	
			    	
    	<?php if ($mod_js_parameters->v_pallet == 1){ ?>			    	
	    	{text: 'Pallet',			width: 30, dataIndex: 'pallet'},
	    <?php } ?>				    		    
	    {text: 'Volume',			width: 50, dataIndex: 'volume', align: 'right'},	    
		{text: 'Peso',				flex: 55, dataIndex: 'peso', align: 'right', hidden: true},

		<?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>		
			{text: 'Importo',			width: 80, dataIndex: 'importo', align: 'right'},
		<?php  } ?>
			
		{text: 'Dispon.<br>(ATP)',		width: 60, dataIndex: 'data_disp', renderer: date_from_AS},			
		{text: 'Disp.alla<br>spediz.',	width: 60, dataIndex: 'cons_prog', renderer: date_from_AS},		
		{text: 'Carico/<br>Spediz.',	width: 60, dataIndex: 'data_sped', renderer: date_from_AS},						
		{text: 'Cons.<br>entro',		width: 60, dataIndex: 'data_cons_cli', renderer: date_from_AS}
		
    	<?php if ($mod_js_parameters->v_gg_rit == 1){ ?>
    		,{text: 'GG<br>Rit.', width: 30, dataIndex: 'gg_rit'},
    	<?php } ?>				
	]
	



		                            	
		el = Ext.create('Ext.tree.Panel', {		
	        title: 'Info',
	        cod_iti: "",
	        
        	stateful: true,
        	stateId: 'info-std',
	        
	        
	        //selType: 'cellmodel',
	        <?php echo make_tab_closable(); ?>,
	        
	        tbar: new Ext.Toolbar({
	            items:['<b>Elenco ordini cliente per Punto vendita/Cliente/Destinazione/Data evasione programmata</b>', '->'
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	        
	        
	        cls: 'elenco_ordini',
	        collapsible: true,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    model: 'ModelOrdini',
				    
					listeners: {				
								beforeload: function(store, options) {
				        			Ext.getBody().mask('Loading... ', 'loading').show();
				        			},							
					            load: function () {
					            
					            	Ext.getBody().unmask();
					            }
					         },													    
				                                          
                    proxy: {
                        type: 'ajax',
                        url: 'acs_get_elenco_ordini_from_search_json.php',
                        timeout: 240000,
                        extraParams: {
                        	f_cliente_cod: form.findField("f_cliente_cod").getValue(),
                        	f_destinazione_cod: form.findField("f_destinazione_cod").getValue(),
                        	
                        	f_riferimento: form.findField("f_riferimento").getValue(),
                        	
                        	f_data_dal: form.findField("f_data_dal").getValue(),
                        	f_data_al: form.findField("f_data_al").getValue(),
                        	
                        	f_data_sped_dal: form.findField("f_data_sped_dal").getValue(),
                        	f_data_sped_al: form.findField("f_data_sped_al").getValue(),                        	
                        	
                        	f_data_ril_dal: form.findField("f_data_ril_dal").getValue(),
                        	f_data_ril_al: form.findField("f_data_ril_al").getValue(),
                        	                        	
                        	f_data_ricezione_dal: form.findField("f_data_ricezione_dal").getValue(),
                        	f_data_ricezione_al: form.findField("f_data_ricezione_al").getValue(),
                        	
                        	f_data_conferma_dal: form.findField("f_data_conferma_dal").getValue(),
                        	f_data_conferma_al: form.findField("f_data_conferma_al").getValue(),                        	
                        	
                        	f_data_scarico_dal: form.findField("f_data_scarico_dal").getValue(),
                        	f_data_scarico_al: form.findField("f_data_scarico_al").getValue(),                        	
                        	                        	
                        	f_data_disp_dal: form.findField("f_data_disp_dal").getValue(),
                        	f_data_disp_al: form.findField("f_data_disp_al").getValue(),
                        	
                        	f_data_ev_rich_dal: form.findField("f_data_ev_rich_dal").getValue(),
                        	f_data_ev_rich_al: form.findField("f_data_ev_rich_al").getValue(),
                        	                        	
                        	//f_data_ricezione: form.findField("f_data_ricezione").getValue(),                        	
                        	//f_data_conferma: form.findField("f_data_conferma").getValue(),
                        	
                        	f_carico_assegnato: form_values['carico_assegnato'],
                        	f_lotto_assegnato: form_values['lotto_assegnato'],
                        	f_proforma_assegnato: form_values['proforma_assegnato'],
                        	f_collo_disp: form_values['collo_disp'],
                        	f_collo_sped: form_values['collo_sped'],
                        	f_anomalie_evasione: form_values['anomalie_evasione'],                        	
                        	f_ordini_evasi: form_values['f_ordini_evasi'],
                        	f_indice_rottura_assegnato: form_values['indice_rottura_assegnato'],                        	
                        	
                        	f_area: form_values['f_area'],                        	                        	                        	
                        	//f_stabilimento: form_values['f_stabilimento'],
                        	f_stabilimento: form.findField("f_stabilimento").getValue().join(','),
                        	
                        	//f_itinerario: form_values['f_itinerario'],
                        
                        	//f_tipologia_ordine: form_values['f_tipologia_ordine'],
                        	f_tipologia_ordine: form.findField("f_tipologia_ordine").getValue().join(','),
                        	f_modello: form_values['f_modello'],
                        	f_referente_cliente:  form_values['f_referente_cliente'],
                        	f_referente_ordine:  form_values['f_referente_ordine'],
                        	//f_stato_ordine: form_values['f_stato_ordine'],
                        	f_stato_ordine: form.findField("f_stato_ordine").getValue().join(','),
                        	f_num_ordine: form_values['f_num_ordine'],                        	
                        	f_num_carico: form_values['f_num_carico'],
                        	f_num_lotto: form_values['f_num_lotto'],
                        	f_num_proforma: form_values['f_num_proforma'],
                        	f_indice_rottura: form_values['f_indice_rottura'],
							f_priorita: form_values['f_priorita'],
							f_solo_bloccati: form_values['f_solo_bloccati'],
							f_confermati: form_values['f_confermati'],
							f_hold: form_values['f_hold'],
							f_tipo_ordine: form_values['f_tipo_ordine'],
							f_vettore: form_values['f_vettore'],
							f_evasi: form_values['f_evasi'],
							f_piva: form_values['f_piva'],
							f_num_contract: form_values['f_num_contract'],
							f_mercato: form_values['f_mercato']
                        	},
                        reader: {
                            root: 'children'
                        }
                    }
                }),
	        multiSelect: true,
	        singleExpand: false,
	
			columns: colElencoOrdiniSearch,

	        listeners: {
	        
			  afterrender: function (comp) {
			  	//console.log(Ext.fly(this.columns[0].id).select('img').elements[0].getEl());
			  }, 		        
	        	
			  itemcontextmenu : function(grid, rec, node, index, event) {
		          if (rec.get('liv') == 'liv_0')			  	
					showMenu_liv_0(grid, rec, node, index, event);
		          if (rec.get('liv') == 'liv_1')			  	
					showMenu_liv_1(grid, rec, node, index, event);
		          if (rec.get('liv') == 'liv_2')			  	
					showMenu_liv_2(grid, rec, node, index, event);										
		          if (rec.get('liv') == 'liv_3')			  	
					showMenu_liv_3(grid, rec, node, index, event);					
					
					},

			  celldblclick: {								
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				  	col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
				  	rec = iView.getRecord(iRowEl);
				  	
				  	
    		          if (rec.get('liv') == 'liv_3' && ( col_name == 'task' )  && !Ext.isEmpty(rec.get('k_ordine')))
    		          {	
    		        	var w = Ext.getCmp('OrdPropertyGrid');
    		        	//var wd = Ext.getCmp('OrdPropertyGridDet');
    		        	var wdi = Ext.getCmp('OrdPropertyGridImg');    
    		        	var wdc = Ext.getCmp('OrdPropertyCronologia');    	
    	
    		        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
    					wdi.store.load();		        												        	
    					wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
    					wdc.store.load();					
    		        	
    					Ext.Ajax.request({
    					   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
    					   jsonData: { show: 'Y' },	
    					   success: function(response, opts) {
    					      var src = Ext.decode(response.responseText);
    					      w.setSource(src.riferimenti);
    					      Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);					      
    					   }
    					});
    				   }				  	
				  	
			    									
				    //DAY o HOLD			    									
			    	if (rec.get('liv')=='liv_3' && ( col_name =='cons_prog' ) ){
						mp = Ext.getCmp('m-panel');
						
						if (parseInt(rec.get('fl_da_prog')) == 1)
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?get_iti_by_par=Y&col_name=fl_da_prog&cod_iti=' + rec.get('cod_iti') + '&da_data=' + rec.get('cons_prog'), Ext.id())
				            ).show();						
						else
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_json.php?tipo_cal=iti_data&cod_iti=' + rec.get('cod_iti') + '&data=' + rec.get('cons_prog'), Ext.id())
				            ).show();
			            
						return false; //previene expand/collapse di default			    	
			    	}
			    	
			    	//GATE O HANGAR
			    	if (rec.get('liv')=='liv_3' && (col_name =='data_sped' ) ){
						mp = Ext.getCmp('m-panel');
						
						if (parseInt(rec.get('fl_da_prog')) == 1) //apro comunque HOLD
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?get_iti_by_par=Y&col_name=fl_da_prog&cod_iti=' + rec.get('cod_iti') + '&da_data=' + rec.get('cons_prog'), Ext.id())
				            ).show();						
						else if (rec.get('stato_sped') == 'DP') //apro HANGAR (sped da programmare)
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_panel_flights_el_ordini.php?&col_name=DP_val&cod_iti=' + rec.get('cod_iti') + '&da_data=' + rec.get('cons_prog'), Ext.id())
				            ).show();
				        else     											//apro GATE
							mp.add(
							  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_panel_flights_el_ordini.php?tipo_cal=iti_data&cod_iti=' + rec.get('cod_iti') + '&data=' + rec.get('data_sped'), Ext.id())							  
				            ).show();
			            
						return false; //previene expand/collapse di default			    	
			    	}			    	
			    									
			    														
					if (rec.get('liv')=='liv_2' && col_name=='tipo' && rec.get('tipo') != ''){
						iEvent.preventDefault();
						show_win_art_reso(rec.get('k_cli_des'));
						return false;
					}
												
					if (col_name=='art_mancanti' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();	
						acs_show_win_std('Segnalazione articoli critici', 'acs_art_critici.php?fn=open_art', {k_ordine: rec.get('k_ordine'), type: 'MTO'}, 950, 500, null, 'icon-shopping_cart_gray-16');					
						//show_win_art_critici(rec.get('k_ordine'), 'MTO');
						return false;							
					}	
					if (col_name=='art_da_prog' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();												
						show_win_art_critici(rec.get('k_ordine'), 'MTS');
						return false;							
					}														
					if (col_name=='colli' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();						
						show_win_colli_ord(rec, rec.get('k_ordine'));
						return false;
					}					
					if (col_name=='data_disp' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();						
					  	acs_show_win_std('Elenco fabbisogno materiali', 'acs_background_mrp_art.php?fn=fabbisogno_ordine', {k_ordine: rec.get('k_ordine')}, 900, 550, null, 'icon-shopping_cart_gray-16');
						return false;
					}															
					if (col_name=='riferimento' && rec.get('liv')=='liv_2'){
						iEvent.preventDefault();						
						gmapPopup("gmap.php?ind=" + rec.get('gmap_ind'));
						return false;						
					}
																					
				 }
				},										
	       
	        	
	        	
	        },
			    
			viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			        	v_o = record.get('liv');			        	
			        	v = record.get('liv');
			        	
			        	if (v_o=="liv_1" && record.get('n_carico')==0)
			        		v = v + ' no_carico'; 
			        	
			        	v = v + ' ' + record.get('tp_pri');												        	
			        	
			        	if (v_o=="liv_3"){
			        		v = v + ' ' + record.get('raggr');												        		
			        	} //ordini
			           return v;																
			         }   
			    }												    
			    
			    
			    																		                
	      });		                            	
		                            	
		                   
		                            	
		                            	
            		mp.add(el).show();		
		            this.up('window').close();            	                	                
	            }
	        },  {
	            text: 'Elenco',
	            iconCls: 'icon-bookmark-32',
	            scale: 'large',
	            handler: function() {
	                form = this.up('form').getForm();
	                if (form.isValid()){
	                  //per bug su nomi campi (in alcuni manca _f)
	                	form_values = form.getValues();
	                	
                        form_values['f_carico_assegnato'] = form_values['carico_assegnato'];
                        form_values['f_lotto_assegnato'] = form_values['lotto_assegnato'];                        
                        form_values['f_proforma_assegnato'] = form_values['proforma_assegnato'];
                        form_values['f_collo_disp'] = form_values['collo_disp'];
                        form_values['f_collo_sped'] = form_values['collo_sped'];
                        form_values['f_anomalie_evasione'] = form_values['anomalie_evasione'];
                        form_values['f_ordini_evasi'] = form_values['f_ordini_evasi'];
                        form_values['f_indice_rottura_assegnato'] = form_values['indice_rottura_assegnato'];
	                
	               
	                
		    			acs_show_win_std('Selezione ordini', 'acs_seleziona_ordini.php?fn=open_form', {
		        				solo_senza_carico: 'N',
		        				grid_id: null,
		        				tipo_elenco: 'DA_INFO',
			        			form_values: form_values
		    					},		        				 
		        				1024, 600, {}, 'icon-leaf-16');	                		                
	                }
	            }
	          },/* {
	            text: 'Report',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function() {
	                form = this.up('form').getForm();
	                
	                  //per bug su nomi campi (in alcuni manca _f)
	                	form_values = form.getValues();
                        form_values['f_carico_assegnato'] = form_values['carico_assegnato'];
                        form_values['f_lotto_assegnato'] = form_values['lotto_assegnato'];                        
                        form_values['f_proforma_assegnato'] = form_values['proforma_assegnato'];
                        form_values['f_collo_disp'] = form_values['collo_disp'];
                        form_values['f_collo_sped'] = form_values['collo_sped'];
                        form_values['f_anomalie_evasione'] = form_values['anomalie_evasione'];
                        form_values['f_ordini_evasi'] = form_values['f_ordini_evasi'];
                        form_values['f_indice_rottura_assegnato'] = form_values['indice_rottura_assegnato'];
	                
	                
	                if (form.isValid()){
	                
	                
	                acs_show_win_std('Dettagli report', 'acs_print_lista_elenco_INFO.php?fn=open_form', {
		        				solo_senza_carico: 'N',
		        				filter: form_values
		    					},		        				 
		        				400, 200, {}, 'icon-print-16');	
		        				
		             
	                }
	            }
	          } */
	          ],             
				
        }
]}