<?php

//DRY: anche in class DeskPVen
function get_doc_type_by_tipo($tipo){
	if (in_array($tipo, ar_tipo_by_doc_type('O')))
		return 'O'; //Ordine
	if (in_array($tipo, ar_tipo_by_doc_type('P')))
		return 'P'; //Ordine
}

function ar_tipo_by_doc_type($doc_type){
    global $cfg_mod_DeskPVen;
    return $cfg_mod_DeskPVen['ar_tipo_by_doc_type'][$doc_type];
}

function sql_where_by_doc_type($doc_type){
	$sql_where = '';
	//tipo ordine
	$sql_where .= " AND TDOTPD IN(" . sql_t_IN(ar_tipo_by_doc_type($doc_type)) . ")";

 return $sql_where;	
}




function get_acconto_ordine($k_ordine){
	global $conn, $cfg_mod_DeskPVen;

	$s = new Spedizioni(array('no_verify' => 'Y'));
	$oe = $s->k_ordine_td_decode_xx($k_ordine);
	
	$sql_a = "SELECT RT.RTPRZ AS ACCONTO
		FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
		INNER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
		ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
		WHERE RD.RDART = 'ACCONTO' AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
		AND RD.RDTISR = '' AND RD.RDSRIG = 0";
	
	$stmt_a = db2_prepare($conn, $sql_a);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_a, $oe);		
	$row_a = db2_fetch_assoc($stmt_a);
	return $row_a['ACCONTO'];
}


function get_caparra_ordine($k_ordine){
	global $conn, $cfg_mod_DeskPVen;
	
	global $backend_ERP;
	if ($backend_ERP == 'GL') return 0;

	$s = new Spedizioni(array('no_verify' => 'Y'));
	$oe = $s->k_ordine_td_decode_xx($k_ordine);

	$sql_a = "SELECT RT.RTPRZ AS CAPARRA
	FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
	INNER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
	ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
	WHERE RD.RDART = 'CAPARRA' AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
	AND RD.RDTISR = '' AND RD.RDSRIG = 0";

	$stmt_a = db2_prepare($conn, $sql_a);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_a, $oe);
	$row_a = db2_fetch_assoc($stmt_a);
	return $row_a['CAPARRA'];
}

function get_importi_ordine($k_ordine){
	global $conn, $cfg_mod_DeskPVen;
	
	$s = new Spedizioni(array('no_verify' => 'Y'));
	$oe = $s->k_ordine_td_decode_xx($k_ordine);
	
	$sql_imp = "SELECT *
				FROM {$cfg_mod_DeskPVen['file_testate_gest_valuta']}
				WHERE TODT = ? AND TOTIDO = ? AND TOINUM = ? AND TOAADO = ? AND TONRDO = ? AND TOVALU='EUR'";
	
	$stmt_imp = db2_prepare($conn, $sql_imp);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_imp, $oe);
	$row_imp = db2_fetch_assoc($stmt_imp);
	return $row_imp;
}


function risposta_PVNOR($ditta, $tddocu, $domanda){

	global $conn;
	global $cfg_mod_DeskPVen;

	$sql="SELECT * FROM {$cfg_mod_DeskPVen['file_tabelle']} 
			WHERE TADT='{$ditta}' AND TATAID='PVNOR' AND TAINDI='{$tddocu}' AND TAKEY2='{$domanda}' ";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);

	return $row['TAKEY3'];

}

function risposta_PVNOR_nota($ditta, $tddocu, $domanda){

	global $conn;
	global $cfg_mod_DeskPVen;

	$sql="SELECT * FROM {$cfg_mod_DeskPVen['file_tabelle']}
		  WHERE TADT='{$ditta}' AND TATAID='PVNOR' AND TAINDI='{$tddocu}' AND TAKEY2='{$domanda}' ";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);

	return $row['TAMAIL'];

}


function get_BREF_code($user){
	global $conn, $id_ditta_default;
	global $cfg_mod_Admin, $backend_ERP;
	
	$sql = "SELECT *
		FROM {$cfg_mod_Admin['file_tab_sys']}
		WHERE TADT = '$id_ditta_default' AND TAID = 'BREF' AND TATP <> 'S' AND TADESC=?
	";
	
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($user));
	$row = db2_fetch_assoc($stmt);
	
	return $row['TANR'];
}


function decode_PUVN_code($tab, $cod){
	global $conn, $id_ditta_default;
	global $cfg_mod_Admin, $backend_ERP;

	$sql = "SELECT *
			FROM {$cfg_mod_Admin['file_tab_sys']}
			WHERE TADT = '$id_ditta_default' AND TAID = 'PUVN' AND TACOR2 = ? AND TANR = ?
			";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($tab, $cod));
	$row = db2_fetch_assoc($stmt);

	return $row['TADESC'];
}

function get_TA_sys_by_code($tab, $cod){
	global $conn, $id_ditta_default;
	global $cfg_mod_Admin, $backend_ERP;

	$sql = "SELECT *
			FROM {$cfg_mod_Admin['file_tab_sys']}
			WHERE TADT = '$id_ditta_default' AND TAID = ? AND TANR = ?
	";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($tab, $cod));
	$row = db2_fetch_assoc($stmt);

	return $row;
}


