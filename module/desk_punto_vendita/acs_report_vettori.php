<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

function sum_columns_value(&$ar_r, $r){
	$ar_r['TFINFI'] += $r['TFINFI'];
	$ar_r['TFTIMP'] += $r['TFTIMP'];
	$ar_r['TFTOTD'] += $r['TFTOTD'];


}


function sum_columns_value_t(&$ar_r, $r){

    $ar_r['trasp_cli'] += $r['TOVSP1'];
    $ar_r['trasp_ns'] += $r['TOVSP2'];
    
    
}


?>


<html>
 <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
    .number{text-align: right;}
	.grassetto{font-weight: bold;}   
    .center{text-align: center;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv3 td{font-weight: bold; font-size: 0.9em;}   
   tr.liv_data th{background-color: #333333; color: white;}
   tr.fattura_vendita td{font-style: italic}
   
   tr.tr-cli td{font-weight: bold;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   
   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
   
   div.with_todo_tooltip{text-align: right;}
      
   table.int1 td, table.int1 th{vertical-align: top;}   
   
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
	   
      
  </style>
  
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>


 </head>
 <body>
 
 <div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<?php


//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$ar = array();

//$row['TACOGE_D'] = $s->decod_std('AVET', $row['TACOGE']);


//controllo data
if (strlen($form_values->f_data_fattura_da) > 0)
	$filter_where .= " AND TFDTRG >= {$form_values->f_data_fattura_da}";

if (strlen($form_values->f_data_fattura_a) > 0)
	$filter_where .= " AND TFDTRG <= {$form_values->f_data_fattura_a}";

	
  $filter_where.= sql_where_by_combo_value('TD.TDVETT', $form_values->f_vettore);
  $filter_where.= sql_where_by_combo_value('TFNAZI', $form_values->f_sede);
  $filter_where.= sql_where_by_combo_value('TFTPDO', $form_values->f_tipo_fatt);
  $filter_where.= sql_where_by_combo_value('TFCDIV', $form_values->f_divisione);

$sql = "SELECT TFTIDO, TFAADO, TFNRDO, TFCCON, TFDCON, TFDOCU, TFTOTD, TFTIMP, TFDTRG, 
        TFAADO, TFNRDO, TFTPDO, TFFG01, TFRGSV, TFINUM, TFAADO, TFTPDO, TFDTPD, TFDT, 
        TFSTAT, TFVETT, TFRGSV, TFINFI, TFNAZI, TDVETT
        FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
          ON TF.TFDT = TD.TDDT AND TF.TFDOAB = TD.TDDOCU
        WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VF' {$filter_where} ORDER BY TFVETT, TFRGSV";

/*print_r($sql);
exit;*/


//TO0 PER TRASPORTO
$sql_to = "SELECT *
FROM {$cfg_mod_DeskPVen['file_testate_gest_valuta']}
WHERE TODT = ? AND TOTIDO = ? AND TOINUM = ? AND TOAADO = ? AND TONRDO = ? AND TOVALU='EUR'";



$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);



while ($row = db2_fetch_assoc($stmt)) {
	
    $ar_new['k_ordine_fatt'] = implode("_", array($row['TFDT'], $row['TFTIDO'], $row['TFINUM'], $row['TFAADO'], $row['TFNRDO']));
    
    $oe = $s->k_ordine_td_decode_xx($ar_new['k_ordine_fatt']);
    
    //bug prepare multipla
    $stmt_to = db2_prepare($conn, $sql_to);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_to, $oe);
    $row_to = db2_fetch_assoc($stmt_to);
	

	$tmp_ar_id = array();
	$ar_r= &$ar;

	//stacco dei livelli
	$cod_liv_tot = 'TOTALE';	//totale
	$cod_liv0 = trim($row['TDVETT']); //vettore
	$cod_liv1 = trim($row['TFCCON']); //codice cliente
	$cod_liv2 = trim($row['TFDOCU']); //chiave documento

	

	//LIVELLO TOTALE
	$liv = $cod_liv_tot;
	if (!isset($ar_r["{$liv}"])){
		$ar_new = array();
		$ar_new['id'] = 'liv_totale';
		$ar_new['liv_cod'] = 'TOTALE';
		$ar_new['task'] = 'Totale';
		$ar_new['liv'] = 'liv_totale';
		$ar_r["{$liv}"] = $ar_new;
	}
	$ar_r = &$ar_r["{$liv}"];
	sum_columns_value($ar_r, $row);
	sum_columns_value_t($ar_r, $row_to);

	//creo la vettore
	$liv=$cod_liv0;
	$ar_r = &$ar_r['children'];
	$tmp_ar_id[] = $liv;
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['codice']=$liv;
		if (strlen(trim($liv)) == 0)
		    $d_vettore = 'Vettore non assegnato';
	    else
	        $d_vettore = $s->decod_std('AUTR', trim($liv));
	        $ar_new['task'] = $d_vettore;
		$ar_new['liv'] = 'liv_1';
	
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];
	sum_columns_value($ar_r, $row);
	sum_columns_value_t($ar_r, $row_to);

	//creo cliente
	$liv=$cod_liv1;
	$ar_r=&$ar_r['children'];
	$tmp_ar_id[] = $liv;
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['codice']=$liv;
		$ar_new['task']= $row['TFDCON'];
		
	
		$ar_new['liv'] = 'liv_2';
			
		$ar_new['children'] = array();
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];
	sum_columns_value($ar_r, $row);
	sum_columns_value_t($ar_r, $row_to);


	//chiave documento
	$liv=$cod_liv2;
	$ar_r=&$ar_r['children'];
	$tmp_ar_id[] = $liv;
	$task=array();
	$task[0]=$row['TFAADO'];
	$task[1]=$row['TFNRDO'];
	$task[2]=$row['TFDT'];
	if(!isset($ar_r[$liv])){
		$ar_new= array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['codice']=$row['TFTPDO'];
		$ar_new['task']= "[{$row['TFINUM']}] " .implode("_", $task);
		
		$ar_new['data'] = $row['TFDTRG'];
		$ar_new['liv'] = 'liv_3';
		$ar_new['liv_cod_qtip'] = acs_u8e($row['TFDTPD']);
		$ar_new['stato']= $row['TFFG01'];
		$ar_new['stato_out']= $row['TFSTAT'];
	
		$ar_r[$liv]=$ar_new;
	}

	$ar_r=&$ar_r[$liv];
	sum_columns_value($ar_r, $row);
	sum_columns_value_t($ar_r, $row_to);
}




echo "<div id='my_content'>";

echo "
  		<div class=header_page>
			<H2>Elenco fatture per vettore</H2>
 			<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>";

echo "<table class=int1>";
echo "<tr><th rowspan=2>Vettore/Cliente/Fattura</th>
		<th rowspan=2>Codice</th>
		<th rowspan=2>Data</th>
		<th rowspan=2>Stato</th>
		<th class=number rowspan=2>Importo merce</th>
		<th class=number rowspan=2>Imponibile</th>
		<th class=number rowspan=2>Totale</th>
        <th colspan = 2 class='center'> Trasporto </th>
        <tr>
        <th class=number>Carico cliente</th>
        <th class=number>NS carico</th></tr>
        </tr>
		</tr>";

foreach($ar as $kar => $r){
	
		echo "<tr class=liv_totale><td>".$r['liv_cod']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
   		          <td class=number>".n($r['TFINFI'])."</td>	
    		      <td class=number>".n($r['TFTIMP'])."</td>
    		      <td class=number>".n($r['TFTOTD'])."</td>
                   <td class=number>".n($r['trasp_cli'])."</td>
    		         <td class=number>".n($r['trasp_ns'])."</td>
   		 		  </tr>";
	
		foreach($r['children'] as $kar1 => $r1){
			
			echo "<tr class=liv3><td>".$r1['task']. "</td>
   		          <td>".$r1['codice']."</td>
   				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
   		          <td class=number>".n($r1['TFINFI'])."</td>	
   		          <td class=number>".n($r1['TFTIMP'])."</td>
    		      <td class=number>".n($r1['TFTOTD'])."</td>
                   <td class=number>".n($r1['trasp_cli'])."</td>
    		         <td class=number>".n($r1['trasp_ns'])."</td>
   		          </tr>";
			
			
			foreach($r1['children'] as $kar2 => $r2){
					
				echo "<tr class=liv2><td>".$r2['task']. "</td>
   		          <td>".$r2['codice']."</td>
   				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
   		          <td class=number>".n($r2['TFINFI'])."</td>	
 		          <td class=number>".n($r2['TFTIMP'])."</td>
    		      <td class=number>".n($r2['TFTOTD'])."</td>
                  <td class=number>".n($r2['trasp_cli'])."</td>
    		      <td class=number>".n($r2['trasp_ns'])."</td>
		          </tr>";
				
				if(is_array($r2['children'] )){
				
				foreach($r2['children'] as $kar3 => $r3){  //fattura
				 
					echo "<tr><td>".$r3['task']. "</td>
   		         	 <td>".$r3['codice']."</td>
   				 	 <td>".print_date($r3['data'])."</td>
 				     <td>".$r3['stato_out']."</td>
				     <td class=number>".n($r3['TFINFI'])."</td>	
 		          	 <td class=number>".n($r3['TFTIMP'])."</td>
    		         <td class=number>".n($r3['TFTOTD'])."</td>
                     <td class=number>".n($r3['trasp_cli'])."</td>
    		         <td class=number>".n($r3['trasp_ns'])."</td>
				     </tr>";
					
				/*	$ar_rf=crea_ar_tree_fatture_anticipo($r3['id'], $form_values);
			
					
					if(is_array($ar_rf)){
					
					foreach($ar_rf as $kar4 => $r4){
					
						echo "<tr class=fattura_vendita><td style=text-align:right>".$r4['task']. "</td>
   		         	 <td>".$r4['codice']."</td>
   				 	 <td>".print_date($r4['data'])."</td>
					 <td> &nbsp;</td>
   				 	 <td> &nbsp;</td>
   		          	 <td> &nbsp;</td>
   			      	 <td> &nbsp;</td>
   			      	 </tr>";
						}
					
					}*/
				
					
				}    //qui
				
				}
			
		}
	
   }

}

echo "</table>";


?>

</div>
</body>
</html>

