<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_DeskPVen['file_tabelle'],
    
    'TATAID' => 'CMAR',
    'descrizione' => 'Gestione causale marginalit&agrave;',
    
    'fields_key' => array('TAKEY1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice', 'fw' =>'width : 100'),
        'TADESC' => array('label'	=> 'Descrizione'),
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
