<?php
require_once "../../config.inc.php";
require_once("acs_consegne_montaggio_include.php");

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));


// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
	
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 5px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc; font-weight: bold}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   tr.livtot th{background-color: #cccccc; font-weight: bold}
   tr.bold td{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

//parametri per report
$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$filter_where = '';
//controllo data
if (strlen($form_values->f_data) > 0)
    $filter_where .= " AND TDODRE >= {$form_values->f_data}";
if (strlen($form_values->f_data_a) > 0)
    $filter_where .= " AND TDODRE <= {$form_values->f_data_a}";
        
$filter_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
$filter_where.= sql_where_by_combo_value('TD.TDOTPD', $form_values->f_tipo_ordine);
$filter_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);
$filter_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);
$filter_where.= sql_where_by_combo_value('TD.TDSTAB', $form_values->f_sede);
$filter_where.= sql_where_by_combo_value('TD.TDCAG2', $form_values->f_architetto);
$filter_where.= sql_where_by_combo_value('TD.TDVETT', $form_values->f_vettore);

if($_REQUEST['resi'] == 'Y')
    $filter_where .= " AND TDOTPD = 'MR'";
else
    $filter_where .= " AND TDOTPD = 'MA'";

$sql = "SELECT TD.*, RD.*
       FROM  {$cfg_mod_DeskPVen['file_testate']} TD
       LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
		 ON TDDT=RDDT AND TDOTID=RDTIDO AND TDOINU=RDINUM AND TDOADO=RDAADO AND TDONDO=RDNRDO
	   WHERE ". $s->get_where_std()." {$filter_where} 
       ORDER BY TDDCON";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
while($row = db2_fetch_assoc($stmt)){
    
    $tmp_ar_id = array();
    $ar_r= &$ar;
  
    $cod_liv0 = trim($row['TDSTAB']);  //SEDE
    $cod_liv1 = trim($row['TDCCON']);  //CLIENTE
    $cod_liv2 = trim($row['TDDOCU']);  //ORDINE
    $cod_liv3 = trim($row['RDRIGA']);  //ORDINE
   
  
    //SEDE
    $liv=$cod_liv0;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['codice'] = $liv;
        $ar_new['task'] = $s->decod_std('START', trim($row['TDSTAB']));
        
        
        $ar_r[$liv]=$ar_new;
    }
    
    $ar_r=&$ar_r[$liv];
    
   
    //CLIENTE
    $liv=$cod_liv1;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['codice'] = $liv;
        $ar_new['task'] = $row['TDDCON'];
        $ar_new['loca'] = acs_u8e(trim($s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD'])));
        
        
        $ar_r[$liv]=$ar_new;
    }
    
    $ar_r=&$ar_r[$liv];
    
    
 
    //ORDINE
    $liv=$cod_liv2;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['codice'] = $liv;
        if(trim($row['TDISON']) != '')
            $ar_new['causale'] = "[".trim($row['TDISON'])."] ".$s->decod_std('ISON', trim($row['TDISON']));
        $ar_new['task']  = trim($row['TDOADO'])."_".trim($row['TDONDO']);
        $ar_new['riferimento']	= acs_u8e(trim($row['TDVSRF']));
        $ar_new['k_ordine']		= trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
        $ar_new['data_ordine']	= trim($row['TDODRE']);
        $ar_new['stato'] 		= trim($row['TDDSST']). " [".trim($row['TDSTAT'])."]";
        $ar_new['refe'] 		= acs_u8e(trim($row['TDDORE']));
        $ar_new['vettore'] = $s->decod_std('AUTR', trim($row['TDVETT']));
        
        $ar_r[$liv]=$ar_new;
    }
  
    $ar_r=&$ar_r[$liv];
    
    //RIGA
    $liv=$cod_liv3;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['codice'] = $liv;
        $ar_new['articolo'] = trim($row['RDART']);
        $ar_new['task'] = "[".trim($row['RDART'])."] ".$row['RDDART'];
       
        
        
        $ar_r[$liv]=$ar_new;
    }
    
    
    
    
}

echo "<div id='my_content'>";

echo "<div class=header_page>";
if($_REQUEST['resi'] == 'Y')
    echo "<h2>Riepilogo resi</h2>";
else
    echo "<H2>Riepilogo assistenze</H2>";
 echo "</div>";

if($form_values->f_data != ''){
    echo "<p style = 'margin-left: 5px; '>Dal ".print_date($form_values->f_data)." al ".print_date($form_values->f_data_a)."</p>";
}

echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";

echo "<tr class='livtot'>
        <th> Causale</th>
		<th> Sede</th>
		<th> Nr assistenze </th>
        
        </tr>";

$ult_cs = "";

$sql_t = "SELECT COUNT(*) AS C_ROW, TDISON, TDSTAB
FROM  {$cfg_mod_DeskPVen['file_testate']} TD
WHERE ". $s->get_where_std()." {$filter_where} AND TDOTPD = 'MA'
GROUP BY TDISON, TDSTAB 
ORDER BY TDISON";

$stmt_t = db2_prepare($conn, $sql_t);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_t);

while($row_t = db2_fetch_assoc($stmt_t)){
    
    if($ult_cs != $row_t['TDISON']){
    

    if(trim($row_t['TDISON']) == '')
        $task = 'Non definita';
    else
        $task = "[".trim($row_t['TDISON'])."] ".$s->decod_std('ISON', trim($row_t['TDISON']));
    
    }else{
        $task = "";
    }     
        
   echo "<tr><td>".$task. "</td>
	    <td>".$s->decod_std('START', trim($row_t['TDSTAB']))."</td>
        <td>".$row_t['C_ROW']."</td>
        
        </tr>";

       $ult_cs = $row_t['TDISON'];
    
}



echo "</table>";
echo "<br>";
echo "<table class=int1>";

echo "<tr class='liv_data'>
        <th> Sede/Cliente/Ordine</th>
		<th> Localit&agrave;</th>
		<th> Riferimento </th>
        <th> Data ordine </th>
		<th> Stato ordine</th>
		<th> Causale</th>
        <th> Descrizione riga </th>
        <th> Referente ordine </th>
        <th> Vettore </th>
        </tr>";

foreach ($ar as $kar => $r){
    

    echo "<tr class ='liv3'> <td>".$r['task']." </td>";
    echo "<td colspan = 8 >&nbsp;</td>";
  	echo "</tr>";

	    foreach ($r['children'] as $kar1 => $r1){
	        
	        echo "<tr class ='liv1'> 
                 <td>".$r1['task']." </td>
                 <td>".$r1['loca']." </td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 
                 </tr>";
	    
	    foreach ($r1['children'] as $kar2 => $r2){
	
	        echo "<tr><td>".$r2['k_ordine']."</td>";
	        echo "<td>&nbsp;</td>
           		  <td>".$r2['riferimento']."</td>
           		  <td>".print_date($r2['data_ordine'])."</td>
          		  <td>".$r2['stato']."</td>
                  <td>".$r2['causale']."</td>
                  <td>&nbsp;</td>
                  <td>".$r2['refe']."</td>
                  <td>".$r2['vettore']."</td>";
          echo "</tr>";
          
          foreach ($r2['children'] as $kar3 => $r3){
              
              if($r3['articolo'] != '' && substr($r3['articolo'], 0, 1) != '*'){
             
                 echo "<tr><td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>".$r3['task']."</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>";
                
                  echo "</tr>";
              }
              
              
          }
          
	    }
	}
	}
	
}





