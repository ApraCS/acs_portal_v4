<?php

require_once "../../config.inc.php";
$mod_provenienza = new DeskPVen();
$cod_mod_provenienza = $mod_provenienza->get_cod_mod();

$s = new Spedizioni();
$cfg_mod = $s->get_cfg_mod();

$initial_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario
$m_week = strftime("%V", strtotime($initial_data_txt));
$m_year = strftime("%G", strtotime($initial_data_txt));

$ar_area_spedizione = $s->get_options_area_spedizione();
$ar_area_spedizione_txt = '';

$ar_ar2 = array();
foreach ($ar_area_spedizione as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";

	$ar_area_spedizione_txt = implode(',', $ar_ar2);

	$da_form = acs_m_params_json_decode();
	$m_params = acs_m_params_json_decode();

	if ($m_params->from == 'INFO'){
		$spedizione = array();
		$itin = new Itinerari;
	} else {
		//da DAY o LIST
		//recupero la spedizione
		$sottostringhe = explode("|", $da_form->rec_id);
		switch ($da_form->rec_liv){
			case "liv_0": 	//spedizione
				$m_sped = $sottostringhe[2];
				break;
			case "liv_1":
				$tmp_liv = $sottostringhe[2];
				$tmp_liv_exp = explode("___", $tmp_liv);
				$m_sped = $tmp_liv_exp[1];
				break;
		}
		$spedizione = $s->get_spedizione($m_sped);

		//dalla spedizione recupero l'itinerario
		$itin = new Itinerari;
		$itin->load_rec_data_by_k(array('TAKEY1' => $spedizione['CSCITI']));
	}



	// ******************************************************************************************
	// ELENCO CLIENTI
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'get_el_clienti'){

		switch ($da_form->rec_liv){
			case "liv_0": 	//spedizione
				$dt	  = $spedizione['CSDT'];
				$ar_clienti = $s->get_el_clienti_dest_by_sped($m_sped, $dt);
				break;
			case "liv_1":	//carico
				$tpca = $tmp_liv_exp[2];
				$aaca = $tmp_liv_exp[3];
				$nrca = $tmp_liv_exp[4];
				$dt	  = $spedizione['CSDT'];
				$ar_clienti = $s->get_el_clienti_dest_by_sped_car($m_sped, $tpca, $aaca, $nrca, $dt);
				break;
		}

		echo acs_je($ar_clienti);

		exit; }

		// ******************************************************************************************
		// ELENCO AGENTI
		// ******************************************************************************************
		if ($_REQUEST['fn'] == 'get_el_agenti'){

			switch ($da_form->rec_liv){
				case "liv_0": 	//spedizione
					$dt	  = $spedizione['CSDT'];
					$ar_clienti = $s->get_el_agenti_by_sped($m_sped, $dt);
					break;
				case "liv_1":	//carico
					$tpca = $tmp_liv_exp[2];
					$aaca = $tmp_liv_exp[3];
					$nrca = $tmp_liv_exp[4];
					$dt	  = $spedizione['CSDT'];
					$ar_clienti = $s->get_el_agenti_by_sped_car($m_sped, $tpca, $aaca, $nrca, $dt);
					break;
			}

			echo acs_je($ar_clienti);

			exit; }


// ******************************************************************************************
// DEFAULT: APERTURA FORM PARAMETRI
// ******************************************************************************************
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            url: 'acs_print_lista_consegne.php',
			buttons: [

				{
		            text: 'Visualizza',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		                this.up('form').submit({
		                url: 'acs_print_lista_consegne_programmate_INFO.php',
	                  	target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',
                        params: {
                        	form_values: Ext.encode(this.up('form').getValues())
                        }
	                  });
		                
		            }
		        } 
	        
	        
	        ],             
            
            items: [
            

				{
                	xtype: 'hidden',
                	name: 'from',
                	value: '<?php echo $m_params->from ?>'
                },
                {
                	xtype: 'hidden',
                	name: 'filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                }, {
                	xtype: 'hidden',
                	name: 'tipo',
                	value: 'lista_consegne_programmate_INFO'
                }
                , {
                	xtype: 'hidden',
                	name: 'rec_id',
                	value: '<?php echo $da_form->rec_id ?>'
                }, {
                	xtype: 'hidden',
                	name: 'rec_liv',
                	value: '<?php echo $da_form->rec_liv ?>'
                }, {                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 1',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa importi',
			                boxLabel: 'Si',
			                name: 'stampa_importi',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_importi',
			                inputValue: 'N'
			            }]
			        },  {
			            xtype: 'component',
			            width: 10
			        }, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a radio button
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [

			            <?php if ($cfg_mod['gestione_per_riga'] != 'Y') { ?>
			            
				            {
				                fieldLabel: 'Dettaglio ordini',
				                boxLabel: 'Si',
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'No',
				                checked: true,			                
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'N'
				            }
				         <?php } else { ?>
				            {
				                fieldLabel: 'Dettaglia per',
				                boxLabel: 'Ordine',
				                checked: true,
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'Riga',			                
				                name: 'stampa_dettaglio_ordini',
				                inputValue: 'R'
				            }				         
				         <?php } ?>  
			            
			            ]
			        }
			        
			        
			        ]			        
			        
			        
			        
			        
			        
			    }  
			    
			    
			    
			    
				,{                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 1',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa telefono',
			                boxLabel: 'Si',
			                name: 'stampa_tel',
			                checked: true,			                
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',			                
			                name: 'stampa_tel',
			                inputValue: 'N'
			            }]
			        }]			        
			        
			    }	
			  
            ]
        }

	
]}