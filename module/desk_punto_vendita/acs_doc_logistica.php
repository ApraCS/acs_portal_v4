<?php

require_once("../../config.inc.php");
require_once "acs_panel_protocollazione_include.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

function prepare_grid_row($row, $doc){
    
    $nr = array();
    if($doc == 'TD'){
        $nr['TDDOCU']	    = trim($row['TDDOCU']);
        $nr['num']		    = trim($row['TDONDO']);
        $nr['tipo']	        = trim($row['TDOTPD']);
        $nr['stato']	    = trim($row['TDSTAT']);
        $nr['c_des']	    = trim($row['TDDCON']);
        $nr['c_cod']	    = trim($row['TDCCON']);
        $nr['out_loc_dest'] = trim($row['TDDLOC']);
        $nr['riferimento']	= trim($row['TDVSRF']);
        if($row['TDHMIM']== 0){
            $nr['ora_generazione'] = $row['TDORGE'];
        }else{
            $nr['ora_generazione'] = $row['TDHMIM'];
        }
        $nr['dtrg']	      = trim($row['TDDTIM']);
        $nr['liv']	      = 'liv_totale';
    }
    
    if($doc == 'TF'){
        $nr['TDDOCU']	    = trim($row['TFDOCU']);
        $nr['num']		    = trim($row['TFNRDO']);
        $nr['tipo']	        = trim($row['TFTPDO']);
        $nr['stato']	    = trim($row['TFSTAT']);
        $nr['c_des']	    = trim($row['TFDCON']);
        $nr['c_cod']	    = trim($row['TFCCON']);
        $nr['out_loc_dest'] = trim($row['TFLOCA']);
        $nr['riferimento']	= trim($row['TFVSRF']);
        if($row['TDHMIM']== 0){
            $nr['ora_generazione'] = $row['TFORGE'];
        }else{
            $nr['ora_generazione'] = $row['TFHMIM'];
        }
        $nr['dtrg']	      = trim($row['TFDTIM']);
        $nr['liv']	      = 'liv_totale';
        
    }
    
    return $nr;
}

if ($_REQUEST['fn'] == 'get_grid_data_last_orders'){
    $m_params = acs_m_params_json_decode();
    
    
    
    if(!is_null($m_params->doc_type))
        $doc_type = $m_params->doc_type;
    else
        $doc_type = 'OT';
    
      
    
       
    
    $doc = '';
    if(in_array($doc_type, array('OT', 'IP'))){
        $doc = 'TD';
        
        if($doc_type == 'OT')
          $where_type = " AND TDOTPD IN ('OT', 'IC')";
        else
          $where_type = " AND TDOTPD = '{$doc_type}'";
        
        $sql = "SELECT TD.*
                FROM {$cfg_mod_DeskPVen['file_testate']} TD
                WHERE " . $s->get_where_std() ." {$where_type}
                AND TDSTAB = '" . $main_module->get_stabilimento_in_uso() . "'
                ORDER BY TDDTIM DESC, TDHMIM DESC
                FETCH FIRST 100 ROWS ONLY";
    }
    
    if(in_array($doc_type, array('OR', 'V1'))){
    $doc = 'TF';
    if($doc_type == 'V1')
        $where_type = " AND TFTPDO IN ('V1', 'V2', 'BV', 'T2')";
    else
        $where_type = " AND TFTPDO = '{$doc_type}'";
    
    $sql = "SELECT TF.*
        FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
        WHERE TFDT='{$id_ditta_default}' AND TFTIDO = 'VD' {$where_type}
        AND TFNAZI = '" . $main_module->get_stabilimento_in_uso() . "'
        ORDER BY TFDTIM DESC, TFHMIM DESC
        FETCH FIRST 100 ROWS ONLY";
        
    }
  
    
    $stmt= db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row= db2_fetch_assoc($stmt)){
        //per la tipologia ambiente left outer join con TA0
        
        $nr = prepare_grid_row($row, $doc);
        $ar[] = $nr;
        
    };
    
    
    echo acs_je($ar);
    
    exit;
}

if ($_REQUEST['fn'] == 'get_json_data_causale'){
    
    $ret = array();
    $m_params = acs_m_params_json_decode();
    
    if(strlen($m_params->documento) > 0)
        $sql_where = " AND TAKEY1= '{$m_params->documento}'";
            
    $sql = "SELECT TAKEY2, TADESC
            FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
            WHERE TADT = '{$id_ditta_default}' AND TATAID = 'DCLOC' {$sql_where}
            ORDER BY TADESC";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    while ($row = db2_fetch_assoc($stmt)){
        
        $ret[] = array("id"=>trim($row['TAKEY2']), "text" =>"[".trim($row['TAKEY2'])."] ".trim($row['TADESC']));
        
    }
    
    echo acs_je($ret);
    exit();
}

if ($_REQUEST['fn'] == 'get_json_data_elenco_fornitori'){
    
    $ret = array();
    $m_params = acs_m_params_json_decode();
    
    if(strlen($m_params->fornitore) > 0)
        $sql_where = " AND CFCD= '{$m_params->fornitore}'"; 
    
    if(strlen($_REQUEST['query']) > 0)
        $sql_where = " AND UPPER(CFRGS1)  LIKE '%" . strtoupper($_REQUEST['query']) . "%'";
    
    
    $sql = "SELECT CFCD, CFRGS1
    FROM {$cfg_mod_Spedizioni['file_anag_cli']} CF
    WHERE CFDT = '{$id_ditta_default}' {$sql_where}
    ORDER BY CFRGS1";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    while ($row = db2_fetch_assoc($stmt)){
        
        $ret[] = array("id"=>trim($row['CFCD']), "text" =>trim($row['CFRGS1']));
        
    }
    
    echo acs_je($ret);
    exit();
}


if ($_REQUEST['fn'] == 'exe_conferma_ordine'){
    
    $m_params = acs_m_params_json_decode();
    $use_session_history = microtime(true);
       
    foreach($m_params->form_values as $k => $v){
        
        if (substr($k, 0, 7) == 'f_text_'){
            
            $sh = new SpedHistory();
           $num_creato = $sh->crea(
                'pers',
                array(
                    "messaggio"	=> $m_params->msg_ri,
                    "use_session_history" 	=> $use_session_history,
                    "vals" => array(
                        "RIDART" => $v
                    )
                ));
        }
     }

         
     $sh = new SpedHistory();
     $num_creato =  $sh->crea(
         'pers',
         array(
             "messaggio"	=> $m_params->msg_ri,
             "end_session_history" 	=> $use_session_history,
             "vals" => array(
                 "RICLIE" => $m_params->form_values->f_intestatario,
                 "RICAGE" => $m_params->form_values->f_causale,
                 "RICITI" => $m_params->form_values->f_maga_prov,
                 "RIVETT" => $m_params->form_values->f_maga_dest,
                 "RIDTEP" => $m_params->form_values->f_data_reg,
                 "RIDTVA" => $m_params->form_values->f_data_cons,
                 "RISECA" => trim($main_module->get_stabilimento_in_uso()),
                 "RIDART" => $m_params->form_values->f_riferimento
             )
         ));
    
    $ret = array();
    $ret['success'] = true;
    $ret['ord_creato'] = $num_creato['RIDART'];
    echo acs_je($ret);
    exit();
    
}



// ******************************************************************************************
// APERTURA PREVENTIVI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_prev'){	?>
	

{"success":true, "items": [

        {
			xtype: 'panel',
			title: 'Documenti logistica',
			closable: true,	
		    layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
				
						
							{
						xtype: 'grid',
						title: '',
						flex:0.7,
						autoScroll: true,
				        loadMask: true,
				        features: [	
            				{
            					ftype: 'filters',
            					encode: false, 
            					local: true,   
            			   		 filters: [
            			       {
            			 		type: 'boolean',
            					dataIndex: 'visible'
            			     }
            			      ]
            			}],	
				        store: {
						//xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['TDDOCU', 'num', 'tipo', 'stato', 'c_des', 'c_cod', 'out_loc_dest', 'riferimento', 'ora_generazione', 'TDHMIM', 'dtrg', 'liv']							
									
			}, //store
				
		      columns: [	
			      
			      
			        /*{
	                header   : 'Ordine',
	                dataIndex: 'TDDOCU',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                },*/ {
	                header   : 'Ora',
	                dataIndex: 'ora_generazione',
				    renderer: time_from_AS, 
	                width : 40
	                },{
	                header   : 'Data',
	                dataIndex: 'dtrg',
                    renderer: date_from_AS,
	                width : 60
	                },{
	                header   : 'Numero',
	                dataIndex: 'num',
	                filter: {type: 'string'}, filterable: true,
	                width : 60
	                },{
	                header   : 'Tp',
	                dataIndex: 'tipo',
	                filter: {type: 'string'}, filterable: true,
	                width : 30
	                },{
	                header   : 'St',
	                dataIndex: 'stato',
	                filter: {type: 'string'}, filterable: true,
	                width : 30
	                },{
	                header   : 'Denominazione',
	                dataIndex: 'c_des',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1,
	                renderer: function (value, metaData, record, row, col, store, gridView){
						return value + ' [' + record.get('c_cod') + ']';
					}
	                },{
	                header   : 'Destinazione',
	                dataIndex: 'out_loc_dest',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                },{
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                filter: {type: 'string'}, filterable: true,
	                flex : 1
	                }, {
        	            xtype: 'actioncolumn',
        				text: '<img src=<?php echo img_path("icone/16x16/pencil.png") ?>>',
        	            width: 30, menuDisabled: true, sortable: false,
        	            items: [{
        	                icon:  <?php echo img_path("icone/16x16/pencil.png") ?>,
        	           	    tooltip: 'Modifica testata',
        	                handler: function (grid, rowIndex, colIndex) {
        	                    var rec = grid.getStore().getAt(rowIndex);
        	                    acs_show_win_std('Ordine protocollato numero ' + rec.get('num') + '. Modifica informazioni di testata', 
	                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
	                				doc_type: rec.get('tipo'),
	                				tddocu: rec.get('TDDOCU')
								}, 800, 500, {}, 'icon-shopping_cart_gray-16');
        	               
        	                }
        	            }]
	      		 	 },{
        	            xtype: 'actioncolumn',
        				text: '<img src=<?php echo img_path("icone/16x16/comment_edit.png") ?>>',
        	            width: 30, menuDisabled: true, sortable: false,
        	            items: [{
        	                icon:  <?php echo img_path("icone/16x16/comment_edit.png") ?>,
        	           	    tooltip: 'Annotazioni',
        	                handler: function (grid, rowIndex, colIndex) {
        	                    var rec = grid.getStore().getAt(rowIndex);
        	                    show_win_annotazioni_ordine_bl(rec.get('TDDOCU'));
        	                }
        	            }]
	      		  	}, {
        	            xtype: 'actioncolumn',
        				text: '<img src=<?php echo img_path("icone/16x16/leaf_1.png") ?>>',
        	            width: 30, menuDisabled: true, sortable: false,
        	            items: [{
        	                icon:  <?php echo img_path("icone/16x16/leaf_1.png") ?>,
        	                tooltip: 'Visualizza righe ordine',
        	                handler: function (grid, rowIndex, colIndex) {
        	                    var rec = grid.getStore().getAt(rowIndex);
        	                    acs_show_win_std('Visualizza righe' , 'acs_righe_ordine.php?fn=open_grid', {k_ordine: rec.get('TDDOCU'), from_ot : 'Y'}, 900, 550, null, 'icon-leaf-16');
        	               
        	                }
        	            }]
	      		  },{
        	            xtype: 'actioncolumn',
        				text: '<img src=<?php echo img_path("icone/16x16/print.png") ?>>',
        	            width: 30, menuDisabled: true, sortable: false,
        	            items: [{
        	                icon:  <?php echo img_path("icone/16x16/print.png") ?>,
        	                tooltip: 'Stampa ordine',
        	                handler: function (grid, rowIndex, colIndex) {
        	                   var rec = grid.getStore().getAt(rowIndex);
        	                   if(rec.get('tipo') == 'OR' || rec.get('tipo') == 'V1'){
        	                   
        	                      	Ext.Ajax.request({
							        	url        : 'acs_stampa_fattura.php?fn=exe_stampa_fattura',
								        method     : 'POST',
					        			jsonData: {
					        				k_doc_da_stampare: rec.get('TDDOCU'),
					        				intestatario : rec.get('c_cod'),
					        				from_dclog : 'Y'
										},							        
								        success : function(result, request){
								            var jsonData = Ext.decode(result.responseText);
            								var obj = jsonData.obj;
            								allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + obj);		        
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
					    	 });	
        	                   
        	                   }else{
        	                   		window.open('acs_report_pdf.php?tipo_stampa=SCHEDA&k_ordine='+ rec.get('TDDOCU'));
        	                   }
        	               
        	                }
        	            }]
	      		  }
	              
	                
	                
	         ], listeners: {
	         
	         
	       	/*  itemcontextmenu : function(grid, rec, node, index, event) {
							event.stopEvent();							
							id_selected = grid.getSelectionModel().getSelection();
		    			    				
							var voci_menu = [];


						      var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);						    		
							  
					  } */
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           
					           return '';																
					         }   
					    }
					       
		
		
		}
		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            msg_ri : '',
 		            title: 'Intestazione documento',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.3,
 		            tbar: new Ext.Toolbar({
	                items:[ '->',
	                    {
	            		 iconCls: 'icon-gear-16',
        	             text: '', 
        	             tooltip : 'Depositi',
		           		 handler: function(event, toolEl, panel){
        		           		 	acs_show_win_std('Gestione deposito', 'acs_gest_depos.php?fn=open_tab', {}, 950, 500, null, 'icon-gear-16');
        		            }
        		        }
	            	]            
	       			 }),	
 		            items: [
 		            
 		             {
            			name: 'f_tipo_doc',
            			anchor: '-15',
            			xtype: 'combo',
                    	anchor: '100%',
            			fieldLabel: 'Tipo documento',
            			displayField: 'text',
            			valueField: 'id',
            			emptyText: '- seleziona -',
            			forceSelection: true,
            		   	allowBlank: false,
            			store: {
            				autoLoad: true,
            				editable: false,
            				autoDestroy: true,
            			    fields: [{name:'id'}, {name:'text'}, {name:'TARIF1'}, {name:'TACOGE'}, {name:'TASTAL'}, {name:'TAKEY2'}],
            			    data: [
            				     <?php  echo acs_ar_to_select_json($main_module->find_TA_std('DCLOG', null, 'N', 'Y'), '') ?>
            				    ]
            			}, listeners: {
                			change: function(field,newVal) {	
              					var form  = this.up('form');   
              					form.msg_ri = field.store.data.map[newVal].data.TARIF1;  
              					var forn = field.store.data.map[newVal].data.TACOGE; 
              					var type_cf = field.store.data.map[newVal].data.TASTAL;  
              					var sede = field.store.data.map[newVal].data.TAKEY2;  
              					//se la sede del ta0 � uguale a quella di sessione non imposto il fornitore di default
              					var combo_for = this.up('form').down('#c_forn');
              					var combo_cli = this.up('form').down('#c_cli');  
              					if(type_cf == 'C'){
              					   combo_cli.show();
              					   combo_for.hide();
              					}else{
              					   combo_for.show();
              					   combo_cli.hide();
              					}   
              					var combo_causale = this.up('form').down('#causale'); 
              					combo_causale.store.proxy.extraParams.documento = newVal;
                            	combo_causale.store.load();   
              					 
              					if(forn.trim() != ''){
              						Ext.Ajax.request({
                    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco_fornitori',
                    				        timeout: 2400000,
                    				        method     : 'POST',
                    	        			jsonData: {
                    	        				fornitore : forn
                    	        			},							        
                    				        success : function(result, request){
                    					        var jsonData = Ext.decode(result.responseText);
                    					         
                    					          data = [
                                                        {id: jsonData[0].id, text: jsonData[0].text}
                                                    ];
                                                    combo_for.store.loadData(data);
                                                    combo_for.setValue(forn);
                       			   					combo_for.setReadOnly(true);
                    					    },
                    				        failure    : function(result, request){
                    				            Ext.Msg.alert('Message', 'No data to be loaded');
                    				        }
                    				    }); 
              					
                    
                       			}else{
                       				combo_for.setReadOnly(false);
                       				
                       			}  
                       			
                              }
                        },
            
            			
            		}, 
            		
            		{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
            			name: 'f_causale',
            			anchor: '-15',
            			flex : 1,
            			xtype: 'combo',
                    	anchor: '100%',
                    	itemId : 'causale',
            			fieldLabel: 'Causale',
            			displayField: 'text',
            			valueField: 'id',
            			emptyText: '- seleziona -',
            			forceSelection: true,
            		   //allowBlank: false,
            			store: {
            			        autoLoad: true,
            					proxy: {
            			            type: 'ajax',
            			            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_causale',
            			            actionMethods: {
            				          read: 'POST'
            			        },
            	                	extraParams: {
            	    		    		documento: '',
            	    			},				            
            			            doRequest: personalizza_extraParams_to_jsonData, 
            			            reader: {
            			            type: 'json',
            						method: 'POST',						            
            				            root: 'root'						            
            				        }
            			        },       
            					fields: ['id', 'text'],		             	
            	            }, listeners: {
                				beforequery: function (record) {
                    				record.query = new RegExp(record.query, 'i');
                    				record.forceAll = true;
                    		 }
                          }
            			
            		  }, {
                         xtype : 'button',
	            		 iconCls: 'icon-gear-16',
	            		 margin : '0 0 0 10',
	            		 scale : 'small',
	            		 tooltip : 'Causali DDT',
        		         handler: function(event, toolEl, panel){
        		           		 	acs_show_win_std('Gestione causali', 'acs_gest_causali_ddt.php?fn=open_tab', {}, 950, 500, null, 'icon-gear-16');
        		            }
        		        }
            
						
					]},
            		
            		 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
    						{
    						
    			            xtype: 'combo',
    			            flex:1, 
    			            itemId: 'c_forn',
    						name: 'f_intestatario',
    						fieldLabel: 'Intestatario doc.',
    						anchor: '-15',
    						minChars: 2,			
                			store: {
    			            	pageSize: 1000,            	
    							proxy: {
    					            type: 'ajax',
    					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco_fornitori',
    					            reader: {
    					                type: 'json',
    					                root: 'root',
    					                totalProperty: 'totalCount'
    					            }
    					        },       
    								fields: [{name:'id'}, {name:'text'}],            	
    			            },
    						valueField: 'id',                        
    			            displayField: 'text',
    			            typeAhead: false,
    			            hideTrigger: true,
    						listConfig: {
    			                loadingText: 'Searching...',
    			                emptyText: 'Nessun fornitore trovato',
                    
    			                // Custom rendering template for each item
    			                getInnerTpl: function() {
    			                    return '<div class="search-item">' +
    			                        '<h3><span>{text}</span></h3>' +
    			                        '[{id}] {text}' + 
    			                    '</div>';
    			                	}                
    		                
    		            	},
                
                			pageSize: 1000,
            			},
            			
            			{   xtype: 'combo',
    			            flex:1, 
    			            itemId: 'c_cli',
    						name: 'f_intestatario',
    						fieldLabel: 'Intestatario doc.',
    						hidden : true,
    						anchor: '-15',
    						minChars: 2,			
                			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_get_select_json.php?select=search_cli_anag',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['cod', 'descr', 'out_loc', 'ditta_orig'],		             	
			                },
    						valueField: 'cod',                        
			            	displayField: 'descr',
			            	typeAhead: false,
			            	hideTrigger: true,
		            		anchor: '100%',
							listConfig: {
			                	loadingText: 'Searching...',
			                	emptyText: 'Nessun cliente trovato',
                
			                	// Custom rendering template for each item
			                	getInnerTpl: function() {
			                    	return '<div class="search-item">' +
			                        '<h3><span>{descr}</span></h3>' +
			                        '[{cod}] {out_loc} {ditta_orig}' + 
			                    	'</div>';
			                	}                
		                
		            		},
                			pageSize: 1000,
            			},
            			{
            			xtype : 'button',
            			tooltip : 'Allegati',
            			margin : '0 0 0 10',
	            		text: 'Apri',
	            		iconCls: 'icon-button_blue_play-16',
	            		scale: 'small',	                     
	             		handler: function() {
	               	 	var form = this.up('form').getForm();
	                	var form_values = form.getValues();
	                	var intestatario = form_values.f_intestatario;
	                	   acs_show_win_std('Visualizza allegati', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_allegati', {cliente : intestatario}, 400, 400, null, 'icon-recycle_bin-16');	
			                           
	           			 }
	        
	        			}
						   
						]},
            	         {
            			name: 'f_maga_prov',
            			xtype: 'combo',
                    	anchor: '100%',
            			fieldLabel: 'Magazzino',
            			displayField: 'text',
            			valueField: 'id',
            			emptyText: '- seleziona -',
            			forceSelection: true,
            		   	allowBlank: true,
            
            			store: {
            				autoLoad: true,
            				editable: false,
            				autoDestroy: true,
            			    fields: [{name:'id'}, {name:'text'}],
            			    data: [
            			        <?php  echo acs_ar_to_select_json($main_module->find_TA_std('DEPOS', null, 'N', 'N', null,  null,  null,  'N', 'Y'), '') ?>
            				    ]
            			}
            
            			
            		},   {
            			name: 'f_maga_dest',
            			xtype: 'combo',
                    	anchor: '100%',
            			fieldLabel: 'Magaz. di dest.',
            			displayField: 'text',
            			valueField: 'id',
            			emptyText: '- seleziona -',
            			forceSelection: true,
            		   	allowBlank: true,
            			store: {
            				autoLoad: true,
            				editable: false,
            				autoDestroy: true,
            			    fields: [{name:'id'}, {name:'text'}],
            			    data: [
            				     <?php  echo acs_ar_to_select_json($main_module->find_TA_std('DEPOS', null, 'N', 'N', null,  null,  null,  'N', 'Y'), '') ?>
            				    ]
            			}
            
            			
            		}, 
            	    {
				   xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data registraz.'
				   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
				   , name: 'f_data_reg'
				   , format: 'd/m/y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '100%'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
						}
				},  {
				   xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Cons. richiesta'
				   , name: 'f_data_cons'
				   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
				   , format: 'd/m/y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '100%'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
						}
				},{
							xtype: 'textfield',
							fieldLabel: 'Riferimento',
							name: 'f_riferimento',
							flex :1,
							anchor: '-0',
							},	
							
									 {
					xtype: 'fieldset',
	                title: 'Annotazioni',
	                layout: 'anchor',
	                flex:1,
	                items: [
						<?php for($i=1; $i<= 10;$i++){ ?>
                	
                	{
						name: 'f_text_<?php echo $i ?>',
						xtype: 'textfield',
						fieldLabel: '',
						anchor: '-15',
					    maxLength: 80,					    
					    //value: <?php echo j(trim($commento_txt[$i])); ?>,							
					},	
					
					<?php }?>	
								
									
					 
	             ]}
				
								 
			 		           ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                    	{
                     xtype: 'button',
	            	 scale: 'medium',
					 text: '#100',
	            	 iconCls: 'icon-rss_black-24',                     
			            handler: function(a, b, c) {
			            	var m_grid = this.up('form').up('panel').down('grid');		
			            	var form = this.up('form').getForm();
			            	form_values = form.getValues();	
			            	console.log(form_values);	
							doc_type = form_values.f_tipo_doc;
				
	 			
	 				             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_grid_data_last_orders',
									        method     : 'POST',
						        			jsonData: {
												doc_type: doc_type
											},							        
									        success : function(response, opts){
									     
									       
									        jsonData = Ext.decode(response.responseText);
									        //console.log(jsonData);
									        
											Ext.each(jsonData, function(value) {
												m_grid.getStore().add(value);
											});
						            		
						            		  
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			            		
			            }
			     },
                '->',
		           
			      {
                     xtype: 'button',
                    text: 'Protocolla',
		            scale: 'medium',	                     
					iconCls: 'icon-button_blue_play-24',
		          	handler: function() {
		          	
		          	  var form = this.up('form').getForm();
		          	  var msg_ri = this.up('form').msg_ri;
	       			  var form_values = this.up('form').getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
	       			 
 			            if(form.isValid()){
     			          Ext.Ajax.request({
    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_ordine',
    				        timeout: 2400000,
    				        method     : 'POST',
    	        			jsonData: {
    	        				form_values : form_values,
    	        				msg_ri : msg_ri
    						},							        
    				        success : function(result, request){
    					        var jsonData = Ext.decode(result.responseText);
    					        var tddocu = jsonData.ord_creato;
    					        var ar_tddocu = tddocu.split('_'); 
    					        var values = {};
    					        values['TDDOCU'] = tddocu;
    					        values['tipo'] = form_values.f_tipo_doc;
    					        var ora = new Date().getHours() + '',
    					            min= new Date().getMinutes() + '',
				        			sec= new Date().getSeconds();
							    newDate = ora + min + sec
								values['ora_generazione'] = newDate; 
								values['c_cod'] = form_values.f_intestatario; 
								values['dtrg'] = form_values.f_data_reg; 
								values['riferimento'] = form_values.f_riferimento; 
								values['num'] = ar_tddocu[4].trim(); 
								console.log(values);
								console.log(grid);
    					    	grid.getStore().add(values);
    		        			
	
    		        			
    					    },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    }); 
		           }
			
			            }

			     }
			     ]
		   }]
			 	 }		
					 ]
		
	}
	
]}




<?php
exit;

}



if ($_REQUEST['fn'] == 'open_allegati'){
   $m_params = acs_m_params_json_decode(); ?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            autoScroll : true,
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            
            items: [
            
            {
						xtype: 'grid',
						title: 'Allegati',
						loadMask: true,	
						features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
							}),    
				        store: {
						xtype: 'store',
						autoLoad:true,
						proxy: {
   							url: 'acs_get_order_images.php',
   							type: 'ajax',
   							reader: {
   						      type: 'json',
   						      method: 'POST',		
   						      root: 'root'
   						     },
   						      actionMethods: {
							     read: 'POST'
							        },
   						     extraParams: {
   		    		    		cliente: '<?php echo $m_params->cliente; ?>'
   		    				},
   		    				doRequest: personalizza_extraParams_to_jsonData				        
   						},
							
        			fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
       		     	groupField: 'tipo_scheda',
									
					}, //store
				

			      columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }]
			             
			             , listeners: {
				     		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  }
				 }
				 
					       
		
		
		}
            ]
       
            
           
}
	
]}


<?php

exit;
}



