<?php
require_once "../../config.inc.php";
require_once "acs_panel_margini_include.php";
require_once "acs_panel_protocollazione_include.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          
                 {
							xtype: 'radiogroup',
							fieldLabel: 'Importi forniture',
							labelWidth: 130,
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "0 10 0 0",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_imp' 
		                          , boxLabel: 'Si'
		                          , inputValue: 'Y'
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_imp' 
		                          , boxLabel: 'No'
		                          , inputValue: 'N'
		                          
		                        }]
						},
						
						 {
						xtype: 'checkboxgroup',
						fieldLabel: 'Raggruppa per causale',
						labelWidth: 130,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_causale' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					}
						
	            ],
	            
				buttons: [	
					
					{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}



if ($_REQUEST['fn'] == 'open_report'){

    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u){
    	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }
    
    $ar_email_json = acs_je($ar_email_to);



?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold; font-size: 0.8em;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv1_1 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values_f =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values =	strtr($_REQUEST['filter'], array('\"' => '"'));


$form_values = json_decode($form_values);
$form_values_f = json_decode($form_values_f);

$ar=crea_ar_tree_margini($_REQUEST['node'], $form_values, 'N', 'Y', $form_values_f->f_causale); 

echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>Riepilogo marginalit&agrave; attesa ordini clienti</H2>
 		</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";

echo "<tr class='liv_data' ><th rowspan=2>Sede/Cliente/Ordine</th>
		<th rowspan=2>Data ordine</th>
		<th rowspan=2>Data fattura</th>
		<th rowspan=2> St.</th>
		<th rowspan=2><img src=" . img_path("icone/48x48/info_blue.png") . " height=20></th>
		<th rowspan=2 class=number> Importo ordine con IVA</th>
        <th rowspan=2 class=number> Importo forniture previsto senza iva</th>
		<th rowspan=2 class=number> Coeff. Preventivo</th>";
 
if($form_values_f->f_imp == 'Y'){
     echo "	<th rowspan=2 class=number> Importo forniture</th>
		<th rowspan=2 class=number> Coeff.</th>
		<th rowspan=2 class=number> Margine lordo</th>";
}

 echo " <th colspan =3 class='center'> Assistenze </th>
  		<tr class='liv_data'>
		<th>Numero</th>
		<th>Stato</th>
		<th>Data</th>";

foreach ($ar as $kar => $r){
    
    	echo "<tr class=liv_totale><td>".$r['liv_cod']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		           <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   				  <td class=number>".n($r['importo_ord'])."</td>
                  <td class=number>".n($r['importo_forn_pre'])."</td>
   		 		  <td class=number>".n($r['coeff_pre'])."%</td>";
	if($form_values_f->f_imp == 'Y'){
	        echo "<td class=number>".n($r['importo_forn'])."</td>
   		 		  <td class=number>".n($r['coeff'])."%</td>
   				  <td class=number>".n($r['margine'])."</td>";
	}
   		echo "    <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td></tr>";
	foreach ($r['children'] as $kar1 => $r1){
	    
	    echo "<tr class=liv2><td>".$r1['task']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   				  <td class=number>".n($r1['importo_ord'])."</td>
                  <td class=number>".n($r1['importo_forn_pre'])."</td>
   		 		  <td class=number>".n($r1['coeff_pre'])."%</td>";
	    if($form_values_f->f_imp == 'Y'){
    		echo "<td class=number>".n($r1['importo_forn'])."</td>
   		 		  <td class=number>".n($r1['coeff'])."%</td>
   				  <td class=number>".n($r1['margine'])."</td>";
	    }
   			echo "<td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td></tr>";
	
	foreach ($r1['children'] as $kar2 => $r2){
		
	    echo "<tr class='liv1_1'><td>".$r2['task']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		           <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   				  <td class=number>".n($r2['importo_ord'])."</td>
                  <td class=number>".n($r2['importo_forn_pre'])."</td>
   		 		  <td class=number>".n($r2['coeff_pre'])."%</td>";
	    if($form_values_f->f_imp == 'Y'){
    		echo "<td class=number>".n($r2['importo_forn'])."</td>
   		 		  <td class=number>".n($r2['coeff'])."%</td>
   				  <td class=number>".n($r2['margine'])."</td>";
	    }	
   			echo "<td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td></tr>";
		
		foreach ($r2['children'] as $kar3 => $r3){
			
		    echo "<tr class='liv1'><td>".$r3['task']. "</td>
   		          <td>&nbsp;</td>
   				  <td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td>
   				  <td class=number>".n($r3['importo_ord'])."</td>
                  <td class=number>".n($r3['importo_forn_pre'])."</td>
   		 		  <td class=number>".n($r3['coeff_pre'])."%</td>";
		    if($form_values_f->f_imp == 'Y'){
    		 echo "<td class=number>".n($r3['importo_forn'])."</td>
   		 		  <td class=number>".n($r3['coeff'])."%</td>
   				  <td class=number>".n($r3['margine'])."</td>";
		    }
   			echo "<td>&nbsp;</td>
   		          <td>&nbsp;</td>
    		      <td>&nbsp;</td></tr>";
		
			foreach ($r3['children'] as $kar4 => $r4){
			    echo "<tr>";
			    
			    $commento_txt = $s->get_commento_ordine_by_k_ordine($r4['k_ordine'], 'PRO');
			    $commenti = "";
			    for($i=1; $i<= 9;$i++){
			        if(trim($commento_txt[$i]['text']) != '')
			            $commenti .= trim($commento_txt[$i]['text'])."<br>";
			    }
			    
			    if($form_values_f->f_causale != 'Y' && strlen(trim($r4['c_marg'])) > 0)
			        echo "<td>".$r4['task']. " [".utf8_decode($r4['t_causale'])."]";
			    else
			         echo "<td>".$r4['task'];
			    
			    if(trim($commenti) != '')
			        echo "<br><div style= 'margin-left : 30%;'>".$commenti."</div>";
			    			             
			    echo "</td>";     
			    
   		        echo  "<td>".print_date($r4['data_ord'])."</td>
   				  <td>".print_date($r4['data_fatt'])."</td>
   		          <td>".$r4['stato']."</td>";
				
			    if($r4['art_mancanti'] == 4){
					echo "<td><img src=" . img_path("icone/48x48/info_blue.png") . " height=15></td>";
			    }else if($r4['art_mancanti'] == 3){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_gray.png") . " height=15></td>";
			    }else if($r4['art_mancanti'] == 2){
					echo "<td><img src=" . img_path("icone/48x48/shopping_cart_green.png") . " height=15></td>";
			    }elseif($r4['art_mancanti'] == 1){
					echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
   				
   				
				echo "<td class=number>".n($r4['importo_ord'])."</td>
                  <td class=number>".n($r4['importo_forn_pre'])."</td>
   		 		  <td class=number>".n($r4['coeff_pre'])."%</td>";
			if($form_values_f->f_imp == 'Y'){
        		echo "<td class=number>".n($r4['importo_forn'])."</td>
       		 		  <td class=number>".n($r4['coeff'])."%</td>
       				  <td class=number>".n($r4['margine'])."</td>";
    			}
   			echo "<td>".$r4['nr_ass']."</td>
   		          <td>".$r4['st_ass']."</td>
    		      <td>".print_date($r4['data_ass'])."</td></tr>";
				
				
			}
			
		}
	
	}
	}
	
}



}







