<?php
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


//*********************************************************************
if ($_REQUEST['fn'] == 'call_refresh_data_RI'){
//*********************************************************************
    $sh = new SpedHistory($m_DeskAcq);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'CHK_BC_ART',
            "vals" => array(
                "RIART" => trim($m_params->open_request->art)
            )
        )
    );
    echo acs_je(array('success' => true));
exit;
}


//*********************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
//*********************************************************************
    
    $nr = array();
    $ar = array();
    
    if (isset($m_params->cod_maga_selected))
        $ar_maga = $m_params->cod_maga_selected;
    else 
        $ar_maga = array($m_params->open_request->maga);
    
    $m_params = acs_m_params_json_decode();
    $oe = $s->k_ordine_td_decode_xx($m_params->open_request->k_ordine);
    

    $sql = "Select substr(TADES2, 4, 8) AS DATA_PARTENZA
            FROM {$cfg_mod_DeskPVen['file_tab_sys']} TA WHERE TA.TADT = '{$id_ditta_default}' AND TAID = 'MUDV'";
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $row_mudv = db2_fetch_assoc($stmt);
      
    
    $sql =    " Select T.MVDT  as DT, T.MVART  as ART, T.MVDEPO as DEPO , A.MEUBIC as UBIC,
                sum(T.MVQTA * A.MESEGN) as GIAC"
            . " from {$cfg_mod_DeskPVen['file_movimenti_gest']} T"
            . " inner join {$cfg_mod_DeskPVen['file_movimenti_gest_estensione']} A on(A.MEDT = T.MVDT and A.MENRRG = T.MVNRRG)"
            . " where T.MVDT=? AND MVART = ? AND MVDEPO IN (". sql_t_IN($ar_maga) . ") and A.MEUBIC <> '' 
                AND MVAARG*10000 + MVMMRG * 100 + MVGGRG >= {$row_mudv['DATA_PARTENZA']}"
            . " group by MVDT, MVART, MVDEPO, MEUBIC"
            . " having sum(T.MVQTA * A.MESEGN) <> 0"
            . " order by MVDEPO, MEUBIC";
            
 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array(
            trim($m_params->open_request->dt),
            trim($m_params->open_request->art)
            )
        );
    
    while($row = db2_fetch_assoc($stmt)){
        $nr['dt']         = trim($row['DT']);
        $nr['art']        = trim($row['ART']);
        $nr['maga_c']     = trim($row['DEPO']);
        $depo_sys = find_TA_sys('MUFD', $nr['maga_c']);
        $nr['maga_d']     = $depo_sys[0]['text'];
        $nr['ubicazione'] = trim($row['UBIC']);
        $nr['qty']        = n($row['GIAC'], 0);
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}



//*********************************************************************
if ($_REQUEST['fn'] == 'open_view_barcode'){
//*********************************************************************
$ar = array();
$rec = $m_params->rec;
$sql = "SELECT COUNT(BCQTA) AS BCQTA, BCCODE, BCFILL
        FROM {$cfg_mod_DeskPVen['file_ubicazioni']} UB
        WHERE BCDT = ? AND BCART = ? AND BCDEPO = ? AND BCUBIC = ?
        AND BCFINT = ''
        GROUP BY BCCODE, BCFILL
           ";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt, array($rec->dt, $rec->art, $rec->maga_c, $rec->ubicazione));
while($row = db2_fetch_assoc($stmt)){
    $nr['barcode']    = trim($row['BCCODE']);
    $nr['qty']        = $row['BCQTA'];
    $nr['filler']     = trim($row['BCFILL']);
    $ar[] = $nr;
}


?>
			{"success":true, "items": [
        			{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,	
				        store: {
        						xtype: 'store',
        						autoLoad:true,
        					 data : <?php echo acs_je($ar); ?>,
				   			fields: ['barcode', 'qty']
						}, //store
			      columns: [	
			        {header   : 'barcode',       dataIndex: 'barcode', flex: 1},
			        {header   : 'Quantit&agrave;', dataIndex: 'qty',  width: 100 , align: 'right'},
			        {header   : 'Filler',        dataIndex: 'filler', flex: 1 , hidden: true}
	              ]
	        
		} //grid
	
]}
<?php
exit;
}





//*********************************************************************
if ($_REQUEST['fn'] == 'open'){
//*********************************************************************    
    ?>

			{"success":true, "items": [
        			{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad: false,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['dt', 'art', 'maga_c', 'maga_d', 'ubicazione', 'qty']							
									
			}, //store
			      columns: [	
			        {header   : 'Magazzino',     dataIndex: 'maga_c', width:  80 },
			        {header   : 'Denominazione', dataIndex: 'maga_d', flex:    1 },
			        {header   : 'Ubicazione',    dataIndex: 'ubicazione', flex: 1 },
			        {header   : 'Quantit&agrave;', dataIndex: 'qty',  width: 100 , align: 'right'},
			        {
        	            xtype: 'actioncolumn',
        				text: '<img src=<?php echo img_path("icone/16x16/barcode.png") ?>>',
        	            width: 40, menuDisabled: true, sortable: false,
        	            items: [{
        	                icon: <?php echo img_path("icone/16x16/barcode.png") ?>,
        	                // Use a URL in the icon config
        	                tooltip: 'Visualizza barcode',
        	                handler: function (grid, rowIndex, colIndex) {
        	        			var rec = grid.getStore().getAt(rowIndex);	
         						acs_show_win_std('Elenco barcode per ubicazione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_view_barcode', {
					    		  		rec: rec.data}, 500, 400, null, 'icon-shopping_cart_gray-16');
        	                }
        	            }]
        	        }
	              ],
	              
	              
	        listeners: {
	          afterrender: function (comp) {
	          
                comp.up('window').setTitle(<?php echo j("Riepilogo ubicazioni per articolo {$m_params->art} - {$m_params->art_d}"); ?>);	          
	          
	          	// - richiamo l'aggiornamento dei dati
		    	 Ext.Ajax.request({
			        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_refresh_data_RI', 
			        method     : 'POST',
        			jsonData: {
        			    open_request: <?php echo acs_je($m_params) ?>
					},							        
			        success : function(result, request){
            			comp.getStore().load();
            		},
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });		          	
	          
	          }
	        } //listeners
	        
	        
           , dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                flex: 1,
                padding: 10,
                items: [										
				 {
		            xtype: 'combo',
		            flex: 1,
					fieldLabel: 'Deposito',
					labelAlign: 'right',
		            store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('MUFD'), '') ?> 	
								    ] 
							},
		            
		            value: <?php echo j($m_params->maga); ?>,          
					valueField: 'id',                       
		            displayField: 'text',
		            anchor: '100%',
		            multiSelect: true,
		            
			        listeners: {
			            change: function(field,newVal) {	      
			            				            
						   m_grid = this.up('window').down('grid');
						   m_grid.store.proxy.extraParams.cod_maga_selected = newVal;
						   m_grid.store.load();			            
			                  		            	
			            }
			        }            
				
		        }	        
	          ]
	         }
	       ]
	        
	        
		} //grid
	
]}


<?php 
}
