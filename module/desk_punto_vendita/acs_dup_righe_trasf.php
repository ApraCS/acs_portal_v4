<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'get_json_data'){
        
        
        $m_params = acs_m_params_json_decode();
        $k_ordine = $m_params->k_ord_dup;
        $oe = $s->k_ordine_td_decode_xx($k_ordine);
        
        $sql_where = "";
        $sql_where .= " AND RD.RDSTEV <>'S' AND RDQTA > RDQTE";
            
            $data = array();
            
            $sql = "SELECT RD.*, RTPRZ
            FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
            LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
            ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
            WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
            AND RD.RDTISR = '' AND RD.RDSRIG = 0 ";
       
                  
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $oe);
            
            while ($row = db2_fetch_assoc($stmt)) {
                $row['data_selezionata'] = sprintf("%08s", $dtep);
                $row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
                if ($row['data_selezionata'] == $row['data_consegna'])
                    $row['inDataSelezionata'] = 0;
                    else $row['inDataSelezionata'] = 1;
                    $row['RDART'] = acs_u8e($row['RDART']);
                    $row['RDDART'] = acs_u8e($row['RDDART']);
                    $row['RDNREC'] = acs_u8e($row['RDNREC']);
                    $row['RTPRZ'] = acs_u8e($row['RTPRZ']);
                    $row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
                    $row['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
                    $data[] = $row;
            }
            
            
            //metto prima le righe della data selezionata
            ////usort($data, "cmp_by_inDataSelezionata");
            
            $ord_exp = explode('_', $k_ordine);
            $anno = $ord_exp[3];
            $ord = $ord_exp[4];
            $ar_ord =  $s->get_ordine_by_k_docu($k_ordine);
            
            $m_ord = array(
                'TDONDO' => $ar_ord['TDONDO'],
                'TDOADO' => $ar_ord['TDOADO'],
                'TDDTDS' => print_date($ar_ord['TDDTDS'])
            );
            
            echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
            
            $appLog->save_db();
            exit();
    }
    
    
    if ($_REQUEST['fn'] == 'exe_duplica'){
        
        $m_params = acs_m_params_json_decode();
        $use_session_history = microtime(true);
        
        
        $list_righe=$m_params->righe;
   
            
            foreach ($list_righe as $v){
               
                $sh = new SpedHistory();
                $sh->crea(
                    'pers',
                    array(
                        "messaggio"	=> 'DUP_ORD_TRASF',
                        "k_ordine"		=> $m_params->k_ordine_dup,
                        "use_session_history" =>  $use_session_history,
                        "vals" => array(
                            "RINRCA"    => $v->nrec
                           
                         )
                        
                        
                        
                    )
                    );
            }
            
            
            $sh = new SpedHistory();
            $ret_RI  = $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'DUP_ORD_TRASF',
                    "k_ordine"		=> $m_params->k_ordine_trasf,
                    "end_session_history" =>  $use_session_history
                )
                );

        
        $ret['success'] = true;
        $ret['ret_RI'] = $ret_RI;
        echo acs_je($ret);
        exit;
    }
    
    if ($_REQUEST['fn'] == 'open_form'){
        
        $m_params = acs_m_params_json_decode();
        
        $r = $s->get_ordine_by_k_docu($k_ordine);
        
        ?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            autoScroll: true,
            layout: 'fit',
			  items: [
		
						
	        	  
	        	            
	             {
		xtype: 'gridpanel',
		name : 'f_righe',
		itemId: 'grid_righe',
		margin : '7 0 0 0',
		flex: 1,
		selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},
		stateful: true,
        stateId: 'seleziona-righe-ordini-222',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
		store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,
					loadMask: true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							type: 'ajax',
							reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        },
					        actionMethods: {
						          read: 'POST'
						        },
   						     extraParams: {
   		    		    		k_ord_dup: '<?php echo $m_params->k_ord_dup ?>'
   		    				},               						        
	        			doRequest: personalizza_extraParams_to_jsonData
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART', 'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE', 'RTPRZ', 'active'
	        			]
	    			}),
	    			
		        columns: [
					        
		             {
		                header   : '<br/>&nbsp;Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },			        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		             
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    		}}, {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>Prezzo',
		                dataIndex: 'RTPRZ', 
		                width    : 50,
		                align: 'right',
						renderer: floatRenderer2	                		                
		             }, {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }	        
		      
		        

		            
		            
		         ], 
		         
		         
		         dockedItems: [{
        			xtype: 'toolbar',
        			dock: 'bottom',
        		    items: [
						 '->',{
						 
						text: 'Duplica ordine cliente',
						iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	
			            	var grid = this.up('grid');
			            	var righe = grid.getSelectionModel().getSelection();
			                var button = this;
			            	button.disable();
			        
			            	
                          list_righe_selected = [];
                           for (var i=0; i<righe.length; i++) {
				            list_righe_selected.push({
				            	nrec: righe[i].get('RDNREC'),
				            	RDART: righe[i].get('RDART'),
				            	RDQTA: righe[i].get('RDQTA'),
				            	RTPRZ: righe[i].get('RTPRZ')
				            	});}
				            	
		            	  if (list_righe_selected.length == 0){
							  acs_show_msg_error('Selezionare almeno una riga');
							  button.enable();
							  return false;
						  }
			            	
			            	var loc_win = this.up('window');
							
							   Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
		 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_duplica',
		 						        method     : 'POST',
		 			        			jsonData: {
		 			        			   k_ordine_dup: '<?php echo $m_params->k_ord_dup; ?>',
		 			        			   k_ordine_trasf: '<?php echo $m_params->k_ordine_trasf; ?>',
		 			        			   righe: list_righe_selected
		 			        			
		 								},							        
		 						        success : function(result, request){	
		 						        jsonData = Ext.decode(result.responseText);
		 						         Ext.getBody().unmask();
     						    		 if (jsonData.ret_RI.RIESIT == 'W'){
    									 	acs_show_msg_error(jsonData.ret_RI.RINOTE);
    									 	return;
    									 }else{
		 						            if (!Ext.isEmpty(loc_win.events.aftersave))
										         loc_win.fireEvent('afterSave', loc_win);
										    else
										        loc_win.close();
									        }
		 			            		},
		 						        failure    : function(result, request){
		 						           Ext.getBody().unmask();
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });
							
	
			
			            }
			         }       
					 			
							 			       
			]
		}], viewConfig: {
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (rec.get('RDSTEV') == 'N') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';		        	
		        
		        
		         }   
		    }												    
			
		         
	}	  
			  ] //ITEM panel
			   
		}
]}
	
	<?php
	exit; 

	
	
}