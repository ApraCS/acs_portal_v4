<?php

require_once "../../config.inc.php";
require_once "acs_panel_protocollazione_include.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
//$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);


?>


<html>
 <head>

  <style>

   #logocliente {
     padding-top: 80px; 
     background-image: url(<?php echo "'" . "http://" . $_SERVER['HTTP_HOST'] . ROOT_PATH .  'personal/logo_cliente.png' . "'"; ?>); 
     background-repeat: no-repeat; background-size: auto 70px;
    }

   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
 
   .cella_gray{background-color: #D1CCBF;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 12px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}  
   
  table.intest td{border: 0px;} 
 
    
   tr.ag_liv_data th{background-color: #333333; color: white;}
   .grassetto th{font-weight: bold;}
    .box{width: 250px;
   		 height: 150;
   		 padding:10px;
   		 border: 1px solid;
   		 
   	  } 
   
    div#my_content h1{font-size: 22px; padding: 10px;}
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
   @media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
 @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'N';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 


		

<?php


$oe = $s->k_ordine_td_decode_xx($_REQUEST['k_ordine']);
$row= $s->get_ordine_by_k_docu($_REQUEST['k_ordine']);
$commento= $s->get_commento_ordine_by_k_ordine($_REQUEST['k_ordine'], $cfg_mod_DeskPVen['bl_commenti_ordine']);


$doc_type = get_doc_type_by_tipo($row['TDOTPD']);
$modello = '';

$ar = array();

$sql_rd = "SELECT RDART, RDDART, RDRIGA, RDQTA, RDUM, RTINFI 
			, AL.ALDAR1 AS TRAD_LNG_1, AL.ALDAR2 AS TRAD_LNG_2
			, AL.ALDAR3 AS TRAD_LNG_3, AL.ALDAR4 AS TRAD_LNG_4
			FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
				LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
					ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
				LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art_trad']} AL
					ON AL.ALDT=RD.RDDT AND AL.ALART=RD.RDART AND AL.ALLING='{$cfg_mod_DeskPVen['traduzione']}'
			WHERE RD.RDART NOT IN('ACCONTO', 'CAPARRA') AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
			AND RD.RDTISR = '' AND RD.RDSRIG = 0
			ORDER BY RD.RDRIGA";

$stmt_rd = db2_prepare($conn, $sql_rd);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_rd, $oe);



$ret = array(
		"descr" 	=> acs_u8e(trim($row['TDDDES'])),
		"out_loc"	=> acs_u8e(trim($row['TDDLOC']) . " " . trim($row['TDPROD'])),
		"out_ind"	=> acs_u8e(trim($row['TDIDES'])),
        "cap"	    => acs_u8e(trim($row['TDDCAP'])),
		"data_contratto"		=> acs_u8e(trim($row['TDODRE'])),
		"data_consegna"		    => acs_u8e(trim($row['TDDTEP'])),
		"data_consegna_con"		=> acs_u8e(trim($row['TDODER'])),
		"numero"		        => (int)trim($row['TDONDO']),
		"pagamento"             => acs_u8e($des_pag),
		"imponibile"		    => acs_u8e(trim($row['TDTIMP'])),
		"IVA"		            => acs_u8e(trim($row['TDTIVA'])),
		"email"                 => acs_u8e(trim($row['TDMAIL'])),
		"tot_imp"               => acs_u8e(trim($row['TDTOTD'])),
		"prior"                 => acs_u8e(trim($row['TDDPRI'])),
		"refe"                  => acs_u8e(trim($row['TDDORE'])),
		"rif"                   => acs_u8e(trim($row['TDVSRF'])),
		"tipo_ordine"           => acs_u8e(trim($row['TDOTPD'])),
        "desc_tipo_ordine"      => acs_u8e(trim($row['TDDOTD'])),
		"tipologia_ordine"      => acs_u8e(trim($row['TDCLOR'])),
		"tipol_amb"             => trim($row['TDCVN1']),
		"stab"                  => trim($row['TDSTAB']),
		//"tipol_amb"             => decode_PUVN_code('MOD', trim($row['TDCVN1']))

);

//ORDINI DI LAVORAZIONE (TRASFERIMENTO) TIPOLOGIA ORDINE L
$sql_xx = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD0
           WHERE TDDT = ? AND TDTIDO = ? AND TDINUM = ? AND TDAADO = ? AND TDNRDO = ?";


$stmt_xx = db2_prepare($conn, $sql_xx);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_xx, $oe);

$row_xx = db2_fetch_assoc($stmt_xx);
$ta0_dep_da = $main_module->get_TA_std('DEPOS', trim($row_xx['TDDEPO']));
$ta0_dep_a = $main_module->get_TA_std('DEPOS', trim($row_xx['TDDEPD']));


$ret_xx = array(

    "dep_da"    => acs_u8e(trim($row_xx['TDDEPO'])),
    "dep_da_descr" 	=> $ta0_dep_da['TADESC'],
    "dep_da_loc"	=> $ta0_dep_da['TALOCA']." ".$ta0_dep_da['TAPROV'],
    "dep_da_ind"	=> $ta0_dep_da['TAINDI'],
    "dep_da_cap"	=> $ta0_dep_da['TACAP'],
    "dep_a"		=> acs_u8e(trim($row_xx['TDDEPD'])),
    "dep_a_descr" 	=> $ta0_dep_a['TADESC'],
    "dep_a_loc"	=> $ta0_dep_a['TALOCA']." ".$ta0_dep_a['TAPROV'],
    "dep_a_ind"	=> $ta0_dep_a['TAINDI'],
    "dep_a_cap"	=> $ta0_dep_a['TACAP'],
       
        );
        


/**************** CREAZIONE AR RIGHE RIGHE   *********************************************************************************/
$c_righe = 0;
while($row_rd = db2_fetch_assoc($stmt_rd)){
		$c_righe++;
	
		$ar_new = array();
		$ar_new['cod_articolo']  = trim($row_rd['RDART']);
		
		$is_order_prec = 'N';
		//verifica se ordine precedente a modifica su gestione descrizioni
		if ($doc_type == 'P' && ($oe['TDOADO'] < 2017 || ($oe['TDOADO'] == 2017 && $oe['TDONDO']<='000058')))
			$is_order_prec = 'Y';
		if ($doc_type == 'O' && ($oe['TDOADO'] < 2017 || ($oe['TDOADO'] == 2017 && $oe['TDONDO']<='000331')))
			$is_order_prec = 'Y';
		
		if($is_order_prec == 'Y' && trim($row_rd['TRAD_LNG_1']) != ''){
			$ar_new['articolo'] = trim($row_rd['TRAD_LNG_1']);
			
			//dalla trad del primo articolo recupero il modello (se presente)
			if ($c_righe==1)
				$modello = $row_rd['TRAD_LNG_1'];
			
			//accodiamo altre parti della descrizione
			if(trim($row_rd['TRAD_LNG_2']) != '')
				$ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_2']);
			if(trim($row_rd['TRAD_LNG_3']) != '')
				$ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_3']);
			if(trim($row_rd['TRAD_LNG_4']) != '')
				$ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_4']);
			
		} else{
			$ar_new['articolo'] = trim($row_rd['RDDART']);
		}
		
		$ar_new['quant']  = $row_rd['RDQTA'];
		$ar_new['un_mis'] = trim($row_rd['RDUM']);
		$ar_new['prezzo'] = $row_rd['RTINFI'];
	
	$ar[] = $ar_new;
}

?>


<table class='intest'>
<tr><td style='width: 50%'><div id='logocliente'>&nbsp;</div></td>
<td> 
<p>Provenienza</p>
<hr>
<p><b><?php echo "[{$ret_xx['dep_da']}] {$ret_xx['dep_da_descr']}"?></b></p>
<p> <?php echo $ret_xx['dep_da_ind']; ?> </p>
<p> <?php echo $ret_xx['dep_da_cap'] ." ".$ret_xx['dep_da_loc']; ?> </p>

</tr>

<tr>
<td VALIGN="bottom">

<span style= 'font-weight: bold; font-size: 14px;'><?php echo $ret['desc_tipo_ordine']; ?></span>
<br>
<br>

Ordine numero  <span style= 'font-weight: bold; font-size: 14px;'> <?php echo "{$ret['tipo_ordine']}_{$ret['numero']}"; ?>
</span> del <b><?php echo print_date($ret['data_contratto']); ?></b> 
<br> Referente: <?php echo $ret['refe']; ?>
</td>


<td>
<p>Destinazione</p>
<hr>
<p><b><?php echo "[{$ret_xx['dep_a']}] {$ret_xx['dep_a_descr']}"?></b></p>
<p><?php echo $ret_xx['dep_a_ind']; ?> </p>
<p> <?php echo $ret_xx['dep_a_cap'] ." ".$ret_xx['dep_a_loc']; ?> </p>
</td>
</tr>
</table>

<?php 


echo "<br>";
echo "<br>";
echo "<table class='grassetto'>";
echo "<tr>";
echo "<tr><td colspan=4 class='cella_gray' style='text-align:center;'> <i> Riepilogo merce</i> </td></tr>";
echo "<th>Descrizione beni</th>";
echo "<th>UM</th>";
echo "<th>Quantit&agrave;</th>";



foreach ($ar as $k => $v){
	    echo "<tr>";

		if (trim($v['cod_articolo']) == '*'){
			echo "<td valign=top colspan=4>".$v['articolo']."</td>";
		} else {
			echo "<td valign=top>".$v['articolo']."</td>";
			echo "<td valign=top style='width:30px;'>".$v['un_mis']."</td>";
			echo "<td valign=top style='text-align:right; width:70px;'>".n($v['quant'],2)."</td>";

		}	
		echo "</tr>";
} 
echo "</table>";

?>

 </body>
</html>		