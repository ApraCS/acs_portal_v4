<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_genera_nota_credito'){
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'GEN_NOT_CRE',
            "k_ordine"	=> $m_params->k_doc,
            "vals" => array("RICITI" => $m_params->form_values->f_stato)
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'exe_mod_tra'){

    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'MOD_SPE_TRAS',
            "k_ordine"	=> $m_params->k_doc,
            "vals" => array("RIIMPO" => sql_f($m_params->form_values->f_trasp_cli),
                            "RIQTA" =>  sql_f($m_params->form_values->f_trasp_ns)
                             )
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'exe_mod_vet'){

    $sql= "SELECT TACOGE
    FROM  {$cfg_mod_DeskPVen['file_tabelle']} TA
    WHERE TADT = '{$id_ditta_default}' AND TATAID = 'AUTR' 
    AND TAKEY1 ='{$m_params->form_values->f_vettore}' ";
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    $trasportatore = $row['TACOGE'];
    
    $sql_u = "UPDATE
    {$cfg_mod_DeskPVen['file_testate']} TD
    SET TDVETT=?, TDVET1 = ?
    WHERE TDDOCU = '{$m_params->k_ordine}'
	";
    
    $stmt_u = db2_prepare($conn, $sql_u);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_u, array($m_params->form_values->f_vettore, $trasportatore));
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'MOD_VET_PVEN',
            "k_ordine"	=> $m_params->k_doc,
            "vals" => array("RIVETT" => $m_params->form_values->f_vettore,
                            "RICVES" => $trasportatore)
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if ($_REQUEST['fn'] == 'exe_ann_ddt'){
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ANN_DDT_ASS',
            "k_ordine"	=> $m_params->k_doc,
            "vals" => array("RICITI" => $m_params->form_values->f_stato)
       
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}

if ($_REQUEST['fn'] == 'get_json_data_list'){

    
    $cliente = $m_params->cliente;
    
    if(isset($m_params->k_ordine) && strlen($m_params->k_ordine) > 0){
        $sql_where_td = " AND TDDOCU = '{$m_params->k_ordine}'";
        $sql_where_tf = " AND TFDOAB ='{$m_params->k_ordine}'";
    }
    
    if(isset($cliente) && strlen($cliente) > 0){
        $sql_where_td = " AND TDCCON='{$cliente}'";
        $sql_where_tf = " AND TFCCON='{$cliente}'";
        
        if($m_params->ord_aperti == 'Y')
            $sql_where_td .= " AND TDCCON='{$cliente}' AND TDSTAT NOT IN ('CC','CT') AND TDOTPD <> 'MP'";
    }
  

	$ar = array();

	$sql = "SELECT TDDOCU, TDOADO, TDONDO, TDODRE, TDOTPD, TDDOTD, TDSTAT, TDDSST, TDTIMP, TDTOTD, 
	TDINFI, TDUSIM, TDFN11, TDFN19, TDABBI, TDCCON
	FROM  {$cfg_mod_DeskPVen['file_testate']}
	WHERE ". $s->get_where_std()."AND TDDT = '{$id_ditta_default}' {$sql_where_td}";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($row = db2_fetch_assoc($stmt)) {

		$nr = array();
	
		$nr['from_file'] = 'TD';
		$nr['k_doc']  	= trim($row['TDDOCU']);
		$nr['ord_fat']  = trim($row['TDOADO'])."_".trim($row['TDONDO']);
		$nr['data']  	= trim($row['TDODRE']);
		$nr['tipo']  	= trim($row['TDOTPD']);
		$nr['tipo_desc']  	= trim($row['TDDOTD']);
		$nr['stato']  	    = trim($row['TDSTAT']);
		$nr['stato_desc']   = trim($row['TDDSST']);
		$nr['caparra']  	= $row['TDINFI'] - $row['TDTOTD'];
		$nr['cliente']  	= $row['TDCCON'];
/*		
		$nr['importo']  	= $row['TDINFI']/122*100;
		$nr['imponibile']  	= $row['TDTIMP']/122*100;
		$nr['tot_doc']  	= trim($row['TDTOTD']);
*/		
		$nr['importo']  	= $row['TDINFI']/122*100;
		$nr['imponibile']  	= $row['TDINFI']/122*100;
		$nr['tot_doc']  	= trim($row['TDINFI']);
		
		$nr['utente']  	    = trim($row['TDUSIM']);
		$nr['doc_ab']  	    = trim($row['TDABBI']);
		$nr['iconCls_stato']= $s->get_iconCls(3, $row);
		$nr['totali'] = 'N';
		$ar[] = $nr;
	}

	
if($m_params->ord_aperti != 'Y'){
	$sql_f = "SELECT TFDOCU, TFAADO, TFNRDO, TFTPDO, TFDTPD, TFSTAT, TFDSST, TFINFI, TFTOTD, 
	TFTIMP, TFIMP5, TFDTRG, TFUSIM, TFDOAB, TFTIDO, TFNATD, TFDT, TFTIDO, TFINUM
	FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFDT='{$id_ditta_default}' {$sql_where_tf} ";


	$stmt_f = db2_prepare($conn, $sql_f);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_f);

	while ($row_f = db2_fetch_assoc($stmt_f)) {

		$nr = array();
		$nr['from_file'] = 'TF';
		$nr['k_doc']  	= trim($row_f['TFDOCU']);
		$nr['k_ordine_fatt'] = implode("_", array($row_f['TFDT'], $row_f['TFTIDO'], $row_f['TFINUM'], $row_f['TFAADO'], $row_f['TFNRDO']));
		$nr['ord_fat']	= trim($row_f['TFAADO'])."_".trim($row_f['TFNRDO']);
		$nr['data']  	= trim($row_f['TFDTRG']);
		$nr['tipo']  	= trim($row_f['TFTPDO']);
		$nr['tipo_desc']  	= trim($row_f['TFDTPD']);
		$nr['stato']  		= trim($row_f['TFSTAT']);
		$nr['stato_desc']   = trim($row_f['TFDSST']);
		$nr['importo']  	= $row_f['TFINFI'];
		$nr['imponibile']  	= $row_f['TFTIMP'];
		$nr['tot_doc']  	= $row_f['TFTOTD'];
		$nr['caparra']  	= $row_f['TFIMP5'];
		$nr['utente']  	    = trim($row_f['TFUSIM']);
		$nr['doc_ab']  	    = trim($row_f['TFDOAB']);
		$nr['tp']  	        = trim($row_f['TFTIDO']);
		$nr['tfnatd']  	    = trim($row_f['TFNATD']);
		$nr['totali'] = 'N';
		$ar[] = $nr;

	}
	
	if(isset($m_params->k_ordine) && strlen($m_params->k_ordine) > 0){
	
	$sql_ant = "SELECT SUM(TFTOTD) AS TOT_FAT
	FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VA' AND TFDOAB ='{$m_params->k_ordine}' ";
	
	$stmt_ant = db2_prepare($conn, $sql_ant);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_ant);
	$row_ant = db2_fetch_assoc($stmt_ant);
	
	
	$nr = array();
	$nr['doc_ab'] = "<b>Totale ORDINE</b>";
	$nr['tot_doc'] = n($ar[0]['tot_doc'],2);
	$nr['caparra'] = n($ar[0]['caparra'], 2);
	$nr['totali'] = 'Y';
	$ar[] = $nr;
	//creare riga totali nella grid
	
	$nr = array();
	$nr['doc_ab'] = "<b>Totale ANTICIPI</b>";
	$tot_fat = $row_ant['TOT_FAT'] + $ar[0]['caparra'];
	if($ar[0]['tot_doc'] > 0)
	   $perc = ($tot_fat / $ar[0]['tot_doc']) * 100;
	else 
	   $perc = 0;
	$nr['tot_doc'] = n($tot_fat, 2);
	$nr['caparra'] = "(". n($perc, 2)."%)";
	$nr['totali'] = 'Y';
	$ar[] = $nr;
	
	$sql_tot_fat = "SELECT SUM(TFTOTD) AS TOT_FAT, SUM(TFIMP5) AS CAPARRA
	FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFDT='{$id_ditta_default}' AND TFDOAB ='{$m_params->k_ordine}' AND TFTPDO <> 'RC'";
	
	$stmt_tot_fat = db2_prepare($conn, $sql_tot_fat);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_tot_fat);
	$row_tot_fat = db2_fetch_assoc($stmt_tot_fat);
	
	$nr = array();
	$nr['doc_ab'] = "<b>Residuo anticipi esclusi</b>";
	if($row_tot_fat['CAPARRA'] > 0)
	    $tot_fat = $row_tot_fat['TOT_FAT'] - $row_tot_fat['CAPARRA'];
	else
	    $tot_fat = $row_tot_fat['TOT_FAT'];
	$saldo = $ar[0]['tot_doc'] - $tot_fat - $ar[0]['caparra'];
	$nr['tot_doc'] = n($saldo, 2);
	$nr['totali'] = 'Y';
	$ar[] = $nr;
	
	}
	
}
	echo acs_je($ar);
	exit;

}

if ($_REQUEST['fn'] == 'open_list'){

	$cliente = $m_params->cliente_selected;
	$k_ordine = $m_params->k_ordine;
	
	if(isset($k_ordine) && strlen($k_ordine) > 0){
	    $sql_where = " AND TDDOCU = '{$k_ordine}'";
	}else{
	    $sql_where = " AND TDCCON='{$cliente}'";
	}

	$sql = "SELECT *
	FROM  {$cfg_mod_DeskPVen['file_testate']}
	WHERE ". $s->get_where_std()."AND TDDT = '{$id_ditta_default}' {$sql_where}";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$row = db2_fetch_assoc($stmt);
	
	?>


{"success":true, "items": [

			{
		     xtype: 'grid',
			 loadMask: true,
	         store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								    url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_list',
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
						           extraParams: {
										 cliente: '<?php echo $m_params->cliente_selected; ?>',
										 k_ordine: '<?php echo $m_params->k_ordine; ?>',
										 ord_aperti: '<?php echo $m_params->ord_aperti; ?>'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
							
		        			fields: [
		            			'k_doc', 'from_file', 'ord_fat', 'data', 'tipo', 'tipo_desc', 'stato', 'imponibile', 'importo', 
		            			'tot_doc', 'stato_desc', 'utente', 'iconCls_stato', 'caparra', 'doc_ab', 'tp', 'tfnatd', 
		            			'k_ordine_fatt', 'totali'
		            			]						
									
			}, //store
			
			      columns: [
			             {
			                header   : 'Documento',
			                dataIndex: 'ord_fat', 
			                flex: 1
			             },
			             
			          { 
			            dataIndex: 'iconCls_stato',
			            header: '<img src=<?php echo img_path("icone/48x48/lock.png") ?> width=20>', 
			            align: 'center',
			            width: 40,
			            renderer: function(value, metaData, record){
			            
						    	if (record.get('iconCls_stato') == 'iconEvasoParz') 
						    	return '<img src=<?php echo img_path("icone/48x48/lock_grey.png") ?> width=18>';
						    	    	
								if (record.get('iconCls_stato') == 'iconEvaso') 
								return '<img src=<?php echo img_path("icone/48x48/lock.png") ?> width=18>';	
											
						    }
			        },
				     
				     
				   	  {
			                header   : 'St',
			                dataIndex: 'stato', 
			                width: 30
			             },{
			                header   : 'Descrizione stato',
			                dataIndex: 'stato_desc', 
			                flex : 1
			             },{
			                header   : 'Tp',
			                dataIndex: 'tipo', 
			                width: 30
			             },{
			                header   : 'Descrizione tipo',
			                dataIndex: 'tipo_desc', 
			                flex : 1
			             }, {
			                header   : 'Data',
			                dataIndex: 'data', 
			                width: 60,
			                renderer: date_from_AS
			             },	{
			                header   : 'Merce',
			                dataIndex: 'importo', 
			                width: 60,
			                align: 'right', renderer: floatRenderer2
			             },{
			                header   : 'Imponibile',
			                dataIndex: 'imponibile', 
			                width: 70,
			                align: 'right', renderer: floatRenderer2
			             },{
			                header   : 'Totale',
			                dataIndex: 'tot_doc', 
			                width: 70,
			                align: 'right', 
			                renderer: function(value, p, record){
			                 	if(record.get('totali') != 'Y')
			                 	return floatRenderer2(value);
			                 	
			                 	return value;
			                	
			    			}	
			                
			             },	{
			                header   : 'Caparra',
			                dataIndex: 'caparra', 
			                width: 90,
			                align: 'right', renderer: floatRenderer2,
			                 renderer: function(value, p, record){
			                 	if(record.get('totali') != 'Y')
			                 	return floatRenderer2(value);
			                 	
			                 	return value;
			                	
			    			}
			             },{
			                header   : 'Utente',
			                dataIndex: 'utente', 
			                width: 70
			             },{
			                header   : 'Doc. abbinato',
			                dataIndex: 'doc_ab', 
			                width: 170
			             },        
			             	      
			             			        
			             
			            
			         ]
			         
			         , listeners: {		
	 				
			 			afterrender: function (comp) {			 			
		 					<?php if(isset($m_params->k_ordine) && strlen($m_params->k_ordine) > 0){
		 					    $cliente = "[".$row['TDCCON']."] ".($row['TDDCON']);?>
		 						comp.up('window').setTitle('Elenco documenti collegati all\'ordine  <?php echo "{$row['TDOADO']}_{$row['TDONDO']}_{$row['TDOTPD']} " ?>' + <?php echo j($cliente); ?>);	
		 					<?php }elseif($m_params->ord_aperti == 'Y'){ 
                                $cliente = "[".$row['TDCCON']."] ".($row['TDDCON']);?>
		 					    comp.up('window').setTitle(<?php echo j("Elenco ordini aperti cliente {$cliente}") ?>);	 				
		 					<?php }else{
                                $cliente = "[".$row['TDCCON']."] ".($row['TDDCON']);?>
		 					    comp.up('window').setTitle(<?php echo j("Elenco documenti cliente {$cliente}") ?>);	 				
		 					<?php }?>
			 			},
			 			
        			   <?php if($m_params->ord_aperti == 'Y'){?>
        			    itemclick: function(view,rec,item,index,eventObj) {
            	        	var w = Ext.getCmp('OrdPropertyGrid');
            	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
            	        	var wdi = Ext.getCmp('OrdPropertyGridImg');  
            	        	var wdc = Ext.getCmp('OrdPropertyCronologia');      	
            
            	        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_doc');
            				wdi.store.load();	        	
            	  			wdc.store.proxy.extraParams.k_ordine = rec.get('k_doc');
            				wdc.store.load();
            	        	
            				Ext.Ajax.request({
            				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_doc'),
            				   success: function(response, opts) {
            				      var src = Ext.decode(response.responseText);
            				      w.setSource(src.riferimenti);
            			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
            				      //wd.setSource(src.dettagli);
            				   }
            				});
            				
        				//files (uploaded) - ToDo: dry 
        				var wdu = Ext.getCmp('OrdPropertyGridFiles');
        				if (!Ext.isEmpty(wdu)){
        					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_doc')){				
        			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_doc');
        			        	if (wdu.isVisible())		        	
        							wdu.store.load();
        					}					
        				}				
        				
        	         },	
        	         <?php }?>
	         
	          itemcontextmenu : function(grid, rec, node, index, event) {
	  			event.stopEvent();				  													  
			  	var voci_menu = [];
			  	
			

			  	 if (rec.get('from_file') == 'TF'){
			  	
			  	  
			  	   if(rec.get('tipo') == 'RC'){
    			  	   voci_menu.push({
    	         		text: 'Ristampa documento',
    	        		iconCls : 'icon-print-16',          		
    	        		handler: function () {
    						window.open('acs_report_res_cap.php?k_ordine='+ rec.get('k_ordine_fatt') + '&doc_ab=' + rec.get('doc_ab'));
    		                }
    	    		  });
			  	   }else{
			  	   
    			  	   voci_menu.push({
    	         		text: 'Ristampa documento',
    	        		iconCls : 'icon-print-16',          		
    	        		handler: function () {
    						acs_show_win_std('Ristampa documento', 'acs_stampa_fattura.php?fn=open_form_simple', {k_doc_da_stampare: rec.get('k_doc')}, 400, 300, null, 'icon-print-16');
    		                }
    	    		  });
			  	   
			  	   
			  	   
			  	   }
			  	
			  	
			  	  
	    		  
	    		
	    		   if (rec.get('tp') == 'VF' && rec.get('tipo') != 'RC'){
		    		 voci_menu.push({
		         		text: 'Storna con nota credito',
		        		iconCls : 'icon-button_blue_play-16',          		
		        		handler: function () {
							acs_show_win_std('Seleziona stato contratto', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_stato', {k_doc: rec.get('k_doc')}, 300, 150, null, 'icon-button_blue_play-16');
			                }
		    		}); 
		    		
		    		  if (rec.get('tfnatd') == 'F'){
		    		 voci_menu.push({
		         		text: 'Modifica importi trasporto',
		        		iconCls : 'icon-pencil-16',          		
		        		handler: function () {
							acs_show_win_std('Modifica importi trasporto', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod_tra', {k_doc: rec.get('k_doc'), k_ordine_fatt : rec.get('k_ordine_fatt') }, 300, 150, null, 'icon-pencil-16');
			                }
		    		}); 
		    		
		    		   voci_menu.push({
	         		text: 'Modifica trasportatore e vettore',
	        		iconCls : 'icon-pencil-16',          		
	        		handler: function () {
						acs_show_win_std('Modifica trasportatore e vettore', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_mod_vet', {k_doc: rec.get('k_doc')}, 350, 150, null, 'icon-pencil-16');
		                }
	    		});
	    		  
	    		  }
	    		  
	    		
	    		  
	    		  }
	    		  
	    		   if (rec.get('tp') == 'VD'){
	    		   
	    		   voci_menu.push({
		         		text: 'Annulla ddt assistenza',
		        		iconCls : 'icon-sub_red_delete-16',          		
		        		handler: function () {
		        			acs_show_win_std('Seleziona stato', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_stato', {k_doc: rec.get('k_doc'), ann_ddt : 'Y'}, 300, 150, null, 'icon-button_blue_play-16');
		        		
			                }
		    		});
	    		   
	    		   }
	    		   
	    		  
	    		
	    		 
	    		  }
			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(event.xy);	
	    									  	
			 }
	 			 
	 			      }
	 			      
	 			      ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					          
					          if(record.get('totali') == 'Y')
					             return 'colora_riga_grigio';
					          					           		
					           return '';																
					         }   
					    }
         
	   		
			
		}//grid
		 
			
		
 				
	
     ]
        
 }
 
  <?php 
 exit;
 }
 
 if ($_REQUEST['fn'] == 'open_stato'){
     $m_params = acs_m_params_json_decode();
     
     ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
         			{
					flex: 1,							
					name: 'f_stato',
					xtype: 'combo',
					fieldLabel: 'Stato',
					anchor: '-15',
					margin: "10 10 5 10",
					labelWidth : 40,
					displayField: 'text',
					valueField: 'id',
					emptyText: '- seleziona -',
					forceSelection:true,
				   	allowBlank: true,
				   	//value: 'P3',														
					store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
						     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
						    ] 
						}						 
					}
        
	            ],
	            
				buttons: [	
        				{
        				<?php if($m_params->ann_ddt == 'Y'){?>
			            	text: 'Annulla',
			            	iconCls : 'icon-sub_red_delete-24',
			            <?php }else{?>
			            	text: 'Genera',
			            	iconCls: 'icon-print-24',	
			            <?php }?>
				        scale: 'medium',		            
			            handler: function() { 
			            var form = this.up('form');
			            var win = this.up('window');
			              Ext.Ajax.request({
			                 <?php if($m_params->ann_ddt == 'Y'){?>
			                    url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ann_ddt',
			                 <?php }else{?>
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_nota_credito',
						     <?php }?>
						        method     : 'POST',
			        			jsonData: {
			        			    form_values: form.getValues(),
			        			    k_doc: '<?php echo $m_params->k_doc; ?>'
								},							        
						        success : function(result, request){
			            			jsonData = Ext.decode(result.responseText);
			            			win.close();
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			                
			            }
			        }
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	
	if ($_REQUEST['fn'] == 'open_mod_tra'){
	    $m_params = acs_m_params_json_decode();
	    
	    $oe = $s->k_ordine_td_decode_xx($m_params->k_ordine_fatt);
	    
	    //TO0 PER TRASPORTO
	    $sql= "SELECT *
	    FROM {$cfg_mod_DeskPVen['file_testate_gest_valuta']}
	    WHERE TODT = ? AND TOTIDO = ? AND TOINUM = ? AND TOAADO = ? AND TONRDO = ? AND TOVALU='EUR'";
	   
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt, $oe);
	    $row = db2_fetch_assoc($stmt);
	    
	    $trasp_cli = trim($row['TOVSP1']);
	    $trasp_ns = trim($row['TOVSP2']);
	    
	    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
         			{
					name: 'f_trasp_cli',
					xtype: 'numberfield',	
				    hideTrigger : true,
				    fieldLabel : 'Carico cliente',
				    flex: 1,
				    value : '<?php echo $trasp_cli; ?>'
				   },{
					name: 'f_trasp_ns',
					xtype: 'numberfield',	
					hideTrigger : true,
					fieldLabel : 'NS carico',
					flex: 1,
					value : '<?php echo $trasp_ns; ?>'
				   
				 }
        
	            ],
	            
				buttons: [	
				{
			            text: 'Modifica',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() { 
			            var form = this.up('form');
			            var win = this.up('window');
			              Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_tra',
						        method     : 'POST',
			        			jsonData: {
			        			    form_values: form.getValues(),
			        			    k_doc: '<?php echo $m_params->k_doc; ?>'
								},							        
						        success : function(result, request){
			            			jsonData = Ext.decode(result.responseText);
			            			win.close();
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			                
			            }
			        }
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	

	
	if ($_REQUEST['fn'] == 'open_mod_vet'){
	    $m_params = acs_m_params_json_decode();
	 
	    $sql= "SELECT TDVETT, TDVET1, TDDOCU
	           FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
               LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
                ON TF.TFDT = TD.TDDT AND TF.TFDOAB = TD.TDDOCU
	           WHERE TFDT = '{$id_ditta_default}' AND TFDOCU ='$m_params->k_doc' ";
	    
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    $row = db2_fetch_assoc($stmt);
	    $vettore = trim($row['TDVETT']);
	    $k_ordine = trim($row['TDDOCU']);
	    
	    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
         			{
						name: 'f_vettore',
						xtype: 'combo',
						itemId: 'c_vett',
		            	anchor: '-15',
		            	flex: 1,
		            	labelWidth : 80,
			            fieldLabel: 'Vettore',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						value: <?php echo j($vettore) ?>,
						forceSelection: true,
					   	allowBlank: true,
				   		store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,
							    fields: ['id', 'text'],
							    data: [
								     <?php echo acs_ar_to_select_json($s->find_TA_std('AUTR'), '') ?>
								    ]
							}
					
					}
        
	            ],
	            
				buttons: [	
				{
			            text: 'Modifica',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() { 
			            var form = this.up('form');
			            var win = this.up('window');
			              Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_vet',
						        method     : 'POST',
			        			jsonData: {
			        			    form_values: form.getValues(),
			        			    k_doc: '<?php echo $m_params->k_doc; ?>',
			        			    k_ordine : '<?php echo $k_ordine; ?>'
								},							        
						        success : function(result, request){
			            			jsonData = Ext.decode(result.responseText);
			            			win.close();
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			                
			            }
			        }
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}