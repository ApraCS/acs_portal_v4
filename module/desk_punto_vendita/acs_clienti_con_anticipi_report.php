<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
 div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{font-weight: bold; font-size: 0.8em;} 
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;  font-weight: bold;} 
   tr.err td{background-color: #F4E287;}
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

function sum_columns_value(&$ar, $r){
   
    $ar['imponibile'] += $r['TFTIMP'];
    $ar['tot_doc'] += $r['TFTOTD'];
  
    return $ar;
}


$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);


    
$sql_where.= sql_where_by_combo_value('TF.TFCCON', $form_values->f_cliente_cod);
$sql_where.= sql_where_by_combo_value('TF.TFNAZI', $form_values->f_sede);
$sql_where.= sql_where_by_combo_value('TF.TFTPDO', $form_values->f_tipo_documento);
    
//controllo data
if (strlen($form_values->f_data_da) > 0)
   $sql_where .= " AND TF.TFDTRG >= {$form_values->f_data_da}";
if (strlen($form_values->f_data_a) > 0)
   $sql_where .= " AND TF.TFDTRG <= {$form_values->f_data_a}";
            
   //controllo data ordine
if (strlen($form_values->f_data_ordine_da) > 0)
   $sql_where .= " AND TDODRE >= {$form_values->f_data_ordine_da}";
if (strlen($form_values->f_data_ordine_a) > 0)
   $sql_where .= " AND TDODRE <= {$form_values->f_data_ordine_a}";
       
//controllo fattura(aperta/chiusa)
$filtro_res=$form_values->f_filtra_fatture;

if($filtro_res== "C"){
    $sql_where.=" AND TF.TFFG01 IN ('C','M')";
    
}else if ($filtro_res== "A"){
    
    $sql_where.=" AND TF.TFFG01 <>'C' AND TF.TFFG01 <> 'M'";
    
}
        

    $sql_t = "SELECT SUM(TFTIMP) AS TFTIMP, SUM(TFTOTD) AS TFTOTD, TFCPAG, TFDPAG
                FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
                LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
                    ON TF.TFDT = TD.TDDT AND TF.TFDOAB = TD.TDDOCU
                WHERE 1=1 ".$main_module->get_where_std_PV_TF()." AND TFDT='{$id_ditta_default}'
                AND TFTIDO='VA' {$sql_where} GROUP BY TFCPAG, TFDPAG ORDER BY TFCPAG, TFDPAG";

    $stmt_t = db2_prepare($conn, $sql_t);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_t);
    
    while($row_t = db2_fetch_assoc($stmt_t)){
        
        $ar_r2= &$ar2;
        
        $cod_liv0 = trim($row_t['TFCPAG']);
        
        //LIVELLO paga
        $liv = $cod_liv0;
        if (!isset($ar_r2["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = $liv;
            $ar_new['codice']=$liv;
            $ar_new['task'] = acs_u8e($row_t['TFDPAG']);
            $ar_new['imponibile']  	= $row_t['TFTIMP'];
            $ar_new['tot_doc']  	= $row_t['TFTOTD'];
            
            $ar_r2["{$liv}"] = $ar_new;
        }
        
    }
    
    $sql = "SELECT TFTIDO, TFAADO, TFNRDO, TFCCON, TFDCON, TFNAZI, TFDOCU,
            TFDNAZ, TFTOTD, TFTIMP, TFDTRG, TFAADO, TFNRDO, TFTPDO, TFFG01, TFRGSV,
            TFINUM, TFAADO, TFTPDO, TFDTPD, TFDT, TFSTAT, TFCPAG, TFDPAG
            FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF    
            LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
              ON TF.TFDT = TD.TDDT AND TF.TFDOAB = TD.TDDOCU
            WHERE 1=1 ".$main_module->get_where_std_PV_TF()." AND TFDT='{$id_ditta_default}'
            AND TFTIDO='VA' {$sql_where} ORDER BY TFDNAZ, TFDCON";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        $cod_liv_tot = 'TOTALE';	//totale
        $cod_liv0 = trim($row['TFNAZI']);
        $cod_liv1 = trim($row['TFCPAG']);
        $cod_liv2 = trim($row['TFDOCU']);
        
        
        //LIVELLO TOTALE
        $liv = $cod_liv_tot;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = 'liv_totale';
            $ar_new['liv_cod'] = 'TOTALE';
            $ar_new['task'] = 'Totale generale';
            $ar_new['liv'] = 'liv_totale';
            
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row);
        
        $liv=$cod_liv0;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new= array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice']=$liv;
            $ar_new['task'] = $s->decod_std('START', $liv);
            
            
            $ar_r[$liv]=$ar_new;
        }
        
        $ar_r=&$ar_r[$liv];
        sum_columns_value($ar_r, $row);
        
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new= array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice']=$liv;
            $ar_new['task'] = acs_u8e($row['TFDPAG']);
            
            
            $ar_r[$liv]=$ar_new;
        }
        
        $ar_r=&$ar_r[$liv];
        sum_columns_value($ar_r, $row);
        
        
        //creo FATTURA
        $liv=$cod_liv2;
        $ar_r=&$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new= array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice']=$liv;
            $ar_new['cod_cli']  =  $row['TFCCON'];
            $ar_new['cliente'] = trim($row['TFDCON']);
            $ar_new['k_ordine'] =  "[{$row['TFINUM']}] ".trim($row['TFAADO'])."_".trim($row['TFNRDO'])."_".trim($row['TFDT']);
            $ar_new['sede'] = trim($row['TFNAZI']);
            $ar_new['stato'] = trim($row['TFSTAT']);
            $ar_new['data']  	= trim($row['TFDTRG']);
            $ar_new['tipo'] = trim($row['TFTPDO']);
            $ar_new['imponibile']  	= $row['TFTIMP'];
            $ar_new['tot_doc']  	= $row['TFTOTD'];
          
            
            
            $ar_r[$liv]=$ar_new;
        }
       
    }

   

echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>Riepilogo fatture anticipo</H2>
		<div style=\"text-align: left;\">
		 Anticipi da: " .  print_date($form_values->f_data_da) . " a: " .  print_date($form_values->f_data_a) . "
			</div>
 		<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
		</div>";

echo "<table class=int1>";

echo "<tr class='liv_data' >
        <th>Sede/Cliente/Tipo pagamento</th>
  		<th>Codice</th>
        <th>Data</th>  		
        <th>Fattura</th>
        <th>Stato</th>
		<th>Tipo</th>
        <th>Imponibile</th>
        <th>Totale</th>
        </tr>";




if(is_array($ar)){
 foreach ($ar as $kar => $r){
    
  
    echo "<tr class=liv_totale><td>".$r['task']. "</td>
   		  <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class=number>".n($r['imponibile'],2)."</td>
            <td class=number>".n($r['tot_doc'],2)."</td>
            </tr>";
    
    
    
    foreach ($ar2 as $kart => $rt){
        
        if(trim($rt['task']) == '')
            $task = 'Non definito';
        else
             $task = $rt['task'];
                
        
             echo "<tr class=liv3><td>".$task. "</td>
            <td colspan = 5>&nbsp;</td>
   	        <td class=number>".n($rt['imponibile'],2)."</td>
            <td class=number>".n($rt['tot_doc'],2)."</td>
            </tr>";
        
    }
    
    foreach ($r['children'] as $kar1 => $r1){
        
        echo "<tr><td colspan = 10>&nbsp;</td></tr>";
    
            echo "<tr class = liv1>
                <td>".$r1['task']."</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class=number>".n($r1['imponibile'],2)."</td>
                <td class=number>".n($r1['tot_doc'],2)."</td>
                </tr>";
        
        foreach ($r1['children'] as $kar2 => $r2){
            
            if(trim($r2['task']) == '')
                $task = 'Non definito';
            else
                $task = $r2['task'];

            
            echo "<tr class = liv0>
                <td>".$task."</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class=number>".n($r2['imponibile'],2)."</td>
                <td class=number>".n($r2['tot_doc'],2)."</td>
                </tr>";
    
        foreach ($r2['children'] as $kar3 => $r3){

	            echo "<tr> <td>".$r3['cliente']."</td>";
                echo "  <td>".$r3['cod_cli']."</td>
                <td>".print_date($r3['data'])."</td>   			
                <td>".$r3['k_ordine']."</td>
                <td>".$r3['stato']."</td>
    			<td>".$r3['tipo']."</td>
    			<td class=number>".n($r3['imponibile'],2)."</td>
                <td class=number>".n($r3['tot_doc'],2)."</td>
            
   		  	</tr>";

    }
    }
    }
}
}

?>
</div>
</body>
</html>	

