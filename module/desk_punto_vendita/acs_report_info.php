<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$main_module = new DeskPVen();

$da_form = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'open_elenco_clienti'){

	// ******************************************************************************************
	// REPORT
	// ******************************************************************************************

	$ar_email_to = array();

	$users = new Users;
	$ar_users = $users->find_all();

	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}

	$ar_email_json = acs_je($ar_email_to);

	?>

<html>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}

    table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}

    td.titolo{font-weight:bold;}
  
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #ffffff; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     

</style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);

    Ext.onReady(function() {	
	});  

  </script>

</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content'>

<h2 style='margin-top: 10px; margin-bottom:10px; font-size: 20px; '>Rapporto registrazione documenti</h2>
<p align="right"> Data elaborazione:  <?php echo date('d/m/Y  H:i');?></p>

<table class=int1>


<?php

function sum_columns_value(&$ar, $r){
		
	$ar['importo'] += $r['TDINFI'];
	$ar['count']   += 1;
	
	
	return $ar;
}

$tot_generale=array();



	$sql = "SELECT TDDCON, TDDLOC, TDNAZD, TDPROD, TDDNAD, TDVSRF, TDOADO, TDONDO, TDOTPD, TDODRE, TDDSST, TDSTAT, TDUSIM, TDDVN1, TDINFI
			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
			WHERE " . $s->get_where_std() . " AND 1=1 
			ORDER BY TDODRE";

	
	$sql_where = "";
	$ar_sql = array();
	$m_params = acs_m_params_json_decode();
	
	if ($m_params->solo_senza_carico == 'Y')
		$sql_where .= " AND TDNRCA=0 ";
	
		
	$filtro = $s->get_elenco_ordini_create_filtro((array)json_decode($_REQUEST['filter']));  //filter
	$use_my_stmt = 'Y';
	$stmt 	= $s->get_elenco_ordini($filtro, 'N', 'search');

	
	
	if ($use_my_stmt != 'Y'){
		$sql = $sql . $sql_where;
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_sql);		
	}

	while ($row = db2_fetch_assoc($stmt)) {

		
		$tmp_ar_id = array();
		$ar_r= &$ar;
		
		$cod_liv0 = trim($row['TDSTAB']);  //STABILIMENTO
		
	
		//STABILIMENTO
		$liv =$cod_liv0;
		$tmp_ar_id[] = $liv;
		if (!isset($ar_r["{$liv}"])){
			$ar_new = array();
			$ar_new['id'] = implode("|", $tmp_ar_id);
			$ar_new['task'] = $liv;
			$ar_new['desc'] = $s->decod_std('START', trim($row['TDSTAB']));
			$ar_r["{$liv}"] = $ar_new;
		}
		$ar_r = &$ar_r["{$liv}"];
		sum_columns_value($ar_r, $row);
		$ar_r = &$ar_r['children'];
		
		$ar_new = array();
		$ar_new['id'] = implode("|", $tmp_ar_id);
		$ar_new['task'] = $liv;
		$ar_new['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
		$ar_new['localita'] 	= acs_u8e(trim($s->scrivi_rif_destinazione($row['TDDLOC'], $row['TDNAZD'], $row['TDPROD'], $row['TDDNAD'])));
		$ar_new['TDVSRF']		= acs_u8e(trim($row['TDVSRF']));
		$ar_new['k_ordine']		= trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
		$ar_new['data_ordine']	= trim($row['TDODRE']);
		$ar_new['stato'] 		= trim($row['TDDSST']). " [".trim($row['TDSTAT'])."]";
		$ar_new['refe'] 		= acs_u8e(trim($row['TDDORE']));
		$ar_new['var1']			= acs_u8e(trim($row['TDDVN1']));
		$ar_new['telefono']		= acs_u8e(trim($row['TDTEL']));
		//$ar_new['importo']  	= $row['TDINFI'];
		sum_columns_value($ar_new, $row);
		sum_columns_value($tot_generale, $row);
	
		
		
		$ar_r[]=$ar_new;
		
		
	
	}
	


foreach ($ar as $k0 => $v0){
	
	

	echo "<tr class=liv3 ><td colspan=10> Sede " .$v0['desc']."</td></tr>";
	?>
	
	<tr class=liv2>
	<td class=titolo>Cliente</td>
	<td class=titolo>Localit&agrave;</td>
	<td class=titolo>Riferimento</td>
	<td class=titolo>Telefono</td>
	<td class=titolo>Data ordine</td>
	<td class=titolo>Nr ordine</td>
	<td class=titolo>Stato ordine</td>
	<td class=titolo>Tipologia ambiente</td>
	<td class=titolo>Referente ordine</td>
	<td class=titolo>Importo</td>
	</tr>
	
	<?php 
	foreach ($v0['children'] as $k1 => $v1){

	?>

   <tr>
   <td><?php echo $v1['TDDCON']; ?></td>
   <td><?php echo $v1['localita']; ?></td>
   <td><?php echo $v1['TDVSRF']; ?></td>
   <td><?php echo $v1['telefono']; ?></td>
   <td><?php echo print_date($v1['data_ordine']); ?></td>
   <td><?php echo $v1['k_ordine']; ?></td>
   <td><?php echo $v1['stato']; ?></td>
   <td><?php echo $v1['var1']; ?></td>
   <td><?php echo $v1['refe']; ?></td>
   <td class=number><?php echo n($v1['importo'],2); ?></td>
   </tr>

<?php 
	}?>
	
	<tr class=liv_totale>
	<td colspan=5> Totale </td>
	<td class=number style='text-align:center;'> [ Nr documenti <?php echo $v0['count']; ?> ]</td>
	<td colspan=3>&nbsp;</td>
	<td class=number><?php echo n($v0['importo'],2); ?></td>
	</tr>


<?php 



}


?>

<tr><td colspan=9>&nbsp;</td></tr>

<tr class=liv2>
	<td colspan=5> <b>Totale generale</b> </td>
	<td class=number style='text-align:center;'>  <b>[ Nr Ordini <?php echo $tot_generale['count']; ?> ]</b></td>
	<td colspan=3>&nbsp;</td>
	<td class=number><b><?php echo n($tot_generale['importo'],2); ?></b></td>
	</tr>

 </table>

</div>
</body>
</html>
<?php 
  }
  
  if ($_REQUEST['fn'] == 'open_form'){
  	$m_params = acs_m_params_json_decode();
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            //url: 'acs_print_lista_consegne.php',
            
            items: [
				{
                	xtype: 'hidden',
                	name: 'filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                }            
            ],
            
			buttons: [					
				{
		            text: 'Visualizza',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		                this.up('form').submit({
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_elenco_clienti',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues())
	                        }
	                  });
	                  
	                  this.up('window').close();
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}
?>