<?php
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// ABBINA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_abbina_file'){
    
    $ret = array();
    $ar_ins = array();
    $oe = $s->k_ordine_td_decode_xx($m_params->k_ordine);
    
    $sql = "SELECT COUNT(*) AS C_ROW, RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC, RAPROG
           FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA
           WHERE RANREC = '{$m_params->row->num_ord}' AND RADT = ? AND RATIDO = ? AND RAINUM = ? AND RAAADO = ? AND RANRDO = ? 
           GROUP BY RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC, RAPROG
           ORDER BY RAPROG DESC";
   

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $oe);
    $row = db2_fetch_assoc($stmt);
    
    
    if($row['C_ROW'] > 0){
        $ar_ins['RAPROG'] = $row['RAPROG'] + 1;
    }else{
        $ar_ins['RAPROG'] = 1;
    }
    
    if(strlen($m_params->allegato_id) > 50){
        $ret['msg'] = 'Nome allegato superiore a 50 caratteri';
      }else{
    
    $ret['msg'] = "";
    //insert
    $ar_ins['RADT'] = $oe['TDDT'];
    $ar_ins['RATIDO'] = $oe['TDOTID'];
    $ar_ins['RAINUM'] = $oe['TDOINU'];
    $ar_ins['RAAADO'] = $oe['TDOADO'];
    $ar_ins['RANRDO'] = $oe['TDONDO'];
    $ar_ins['RANREC'] = $m_params->row->num_ord;
    $ar_ins['RAALOR'] = $m_params->allegato_id;
       
    $sql = "INSERT INTO {$cfg_mod_DeskPVen['file_allegati_righe']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    }
        
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

// ******************************************************************************************
// UPLOAD FILE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_upload_file'){
	$ret = array();
	
	if ($_REQUEST['save_in_tmp'] == 'Y'){
		$tmpfname = tempnam("/tmp", "email_att_");
		move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $tmpfname);
		$ret['success'] = true;
		$ret['tmp_path'] = $tmpfname;
		$ret['upload_file']	= $_FILES['file_to_upload'];
		$ret['new_name']	= $_REQUEST['f_name'];
		echo acs_je($ret);
		return;
	}
	
	$ord = $s->get_ordine_by_k_docu($_REQUEST['k_ordine']);

	//$img_dir = "/SV2/ORDINI/";	
	$img_dir = $cfg_mod_DeskPVen['allegati_root_C'];
	$dir_ordine = $img_dir . $ord['TDCCON'];
	
	if (!is_dir($dir_ordine))
		mkdir($dir_ordine);
	
		//$file_src = $dir_ordine . "/" . $_FILES['file_to_upload']['name'];
		$file_src = $dir_ordine . "/" .$_REQUEST['f_name'];
	
	   	//unlink($file_src);
	move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $file_src);
	
	if ($_REQUEST['from_righe'] == 'Y'){
	    	    
	    $ar_ins = array();
	    $oe = $s->k_ordine_td_decode_xx($_REQUEST['k_ordine']);
	    
	    $sql = "SELECT COUNT(*) AS C_ROW, RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC, RAPROG
	    FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA
	    WHERE RANREC = '{$_REQUEST['nrec']}' AND RADT = ? AND RATIDO = ? AND RAINUM = ? AND RAAADO = ? AND RANRDO = ?
	    GROUP BY RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC, RAPROG
	    ORDER BY RAPROG DESC";
	    
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, $oe);
	    $row = db2_fetch_assoc($stmt);
	    
	    
	    if($row['C_ROW'] > 0){
	        $ar_ins['RAPROG'] = $row['RAPROG'] + 1;
	    }else{
	        $ar_ins['RAPROG'] = 1;
	    }
	    
	    //insert
	    $ar_ins['RADT'] = $oe['TDDT'];
	    $ar_ins['RATIDO'] = $oe['TDOTID'];
	    $ar_ins['RAINUM'] = $oe['TDOINU'];
	    $ar_ins['RAAADO'] = $oe['TDOADO'];
	    $ar_ins['RANRDO'] = $oe['TDONDO'];
	    $ar_ins['RANREC'] = $_REQUEST['nrec'];
	    $ar_ins['RAALOR'] = $_REQUEST['f_name'];
	    	    
	    $sql = "INSERT INTO {$cfg_mod_DeskPVen['file_allegati_righe']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, $ar_ins);
	    echo db2_stmt_errormsg($stmt);
	    
	}
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;	
}

// ******************************************************************************************
// OPEN FORM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
    
    
	?>
 
 {"success":true, "items": [
        {
				xtype: 'panel',
				autoScroll : true,
				layout: {type: 'vbox', 
				border: false, pack: 'start', align: 'stretch'},
				
												
				items: [
				{
           
			xtype: 'form',
                        border: false,
                        bodyStyle: {
                            padding: '10px'
                        },
                        items: [
						{xtype: 'hiddenfield', name: 'k_ordine', value: <?php echo j($m_params->k_ordine) ?>},                        
						{xtype: 'hiddenfield', name: 'save_in_tmp', value: <?php echo j($m_params->save_in_tmp) ?>},
						{xtype: 'hiddenfield', name: 'from_righe', value: <?php echo j($m_params->from_righe) ?>},
						{xtype: 'hiddenfield', name: 'nrec', value: <?php echo j($m_params->row->num_ord) ?>},
                        {
                        	name: 'file_to_upload',
                            xtype: 'filefield',
                            labelWidth: 80,
                            fieldLabel: 'Seleziona file',
                            anchor: '100%',
                            allowBlank: false,
                            margin: 0,
                            listeners: {
                              'change': function(f, new_val){
                                    var t_filename = this.up('window').down('#filename');
                                    var ar_name = new_val.split('\\');
                                    t_filename.setValue(ar_name[2]);
                              }
            				}
							},
							{
                        	name: 'f_name',
                        	itemId : 'filename',
                            xtype: 'textfield',
                            labelWidth: 80,
                            maxLength : 50,
                            fieldLabel: 'Nome file',
                            anchor: '100%',
                            allowBlank: true,
                            margin : '5 0 0 0'
                            }
							
						],
							
							 buttons: [
							 {
                        text: 'Upload',
                        handler: function () {
                            var form = this.up('form').getForm();
                            var m_win = this.up('window');
 
                            if (!form.isValid()) return;
                            
                            if (!Ext.isEmpty(m_win.events.afterselect)){
								m_win.fireEvent('afterSelect', m_win, form.getValues());
						    	return;
						    }
 
                            form.submit({
                                url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upload_file',
                                waitMsg: 'Uploading your file(s)...',
                                success: function (f, a) {
                                    var data = a.result;
                                    m_win.fireEvent('afterUpload', m_win, data);
                                },
                                failure: function (f, a) {
                                    Ext.Msg.alert('Failure', a.result.msg || 'server error', function () {
                                        //win.close();
                                    });
                                }
                            });
                        }
                      }, {
                        text: 'Chiudi',
                        handler: function () {                       
                            this.up('window').close();
                        }
                    }]
          
			}
				
				<?php if($m_params->from_righe == 'Y'){?>
							
								, {
					xtype: 'fieldset',
	                title: 'Abbina allegato esistente',
	                autoScroll : true,
	                margin : '10 10 10 10',
	                layout: 'anchor',
					flex:1,
	                items: [
						
							{	xtype: 'grid',
							    autoScroll : true,
								title: '',
                        		flex: 1,
                                useArrows: true,
                                rootVisible: false,
                                loadMask: true,
                                
                        		store: {
                        				xtype: 'store',
                        				autoLoad:true,
                        				proxy: {
                        						url: 'acs_get_order_images.php',
                        						type: 'ajax',
                        						 actionMethods: {
                        					          read: 'POST'
                        					        },
                        						
                        						 extraParams: {
                        							k_ordine: <?php echo j($m_params->k_ordine) ?>
                        						}  ,
                        						doRequest: personalizza_extraParams_to_jsonData, 
                        				
                        						   reader: {
                        				            type: 'json',
                        							method: 'POST',						            
                        				            root: 'root'						            
                        				        }								
                        		
                        					},
                               		        fields: ['des_oggetto', 'IDOggetto'],
                               		     	
                        		}, //store
                        		                        
                    		     columns: [ {
                    	                header   : 'Allegato',
                    	                dataIndex: 'des_oggetto', 
                    	                flex     : 5
                    	             }, {
                        	            xtype: 'actioncolumn',
                        	            tooltip: 'Anteprima',
                        				text: '<?php echo "<img src=" . img_path("icone/16x16/search.png") . " height=16>"; ?>',
                        	            width: 30, menuDisabled: true, sortable: false,
                        	            items: [{
                        	                icon: <?php echo img_path("icone/16x16/search.png") ?>,
                        	                // Use a URL in the icon config
                        	                handler: function (grid, rowIndex, colIndex) {
                        	                    var rec = grid.getStore().getAt(rowIndex);
                        	                     allegatiPopup('acs_allegati_riga.php?fn=view_image&nome=' + rec.get('des_oggetto') + '&k_ordine=' + <?php echo j($m_params->k_ordine) ?>);
                        	                	
                        	                }
                        	            }]
                        	        }
                    	             ],
                    	             
                    	             listeners : {
                    	             
                    	               itemcontextmenu : function(grid, rec, node, index, event) {
        								event.stopEvent();	
        								 						
        										
        								var voci_menu = [];
                                        var m_win = this.up('window');
        							
        					         	 voci_menu.push({
        					         		text: 'Abbina allegato',
        					        		iconCls : 'icon-attachment-16',          		
        					        		     handler: function() {
        						        		    Ext.Ajax.request({
        									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_abbina_file',
        									        method     : 'POST',
        						        			jsonData: {
        						        				allegato_id : rec.get('des_oggetto'),
        						        				k_ordine: <?php echo j($m_params->k_ordine) ?>,
        						        				row : <?php echo acs_je($m_params->row) ?>,
        											},							        
        									        success : function(result, request){
        						            			var jsonData = Ext.decode(result.responseText);
        									            
        									            if(jsonData.msg != ''){
        									            	acs_show_msg_error(jsonData.msg);
        									            }else{
        						            				m_win.fireEvent('afterUpload', m_win);
        						            		   }
        						            		},
        									        failure    : function(result, request){
        									            Ext.Msg.alert('Message', 'No data to be loaded');
        									        }
        									    }); 
								         }
					    		});



						      var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);						    		
							  
					  }
                        	            
                        	             
                        	             }
                        
                        									    
                                		    		
                                }
        							   
         					
        					 
        	               ]}
							
							
							
							<?php }?>
					
				]
			}, 	
			]}


	
	<?php 
}