<?php

$s = new Spedizioni(array('no_verify' => 'Y'));

function sum_columns_value(&$ar_r, $r){
	global $s;

	$ar_r['colli_tot'] += $r['COLLI'];
	$ar_r['colli_disp'] +=$r['DISP'];
	$ar_r['importo'] +=$r['IMPORTO'];
	$ar_r['volume'] +=$r['VOLUME'];
	$ar_r['colli_non_disp'] += ($r['COLLI'] - $r['DISP']);

	$ar_r['fl_bloc'] = $s->get_fl_bloc($ar_r['fl_bloc'], $s->calc_fl_bloc($r));
	$ar_r['fl_new'] = $s->get_fl_new($r);
	$ar_r['fl_art_manc'] =max($ar_r['fl_art_manc'], $s->get_fl_art_manc($r));
	$ar_r['art_da_prog'] = max($ar_r['art_da_prog'], $s->get_art_da_prog($r));

	$ar_r['fl_da_prog'] += $r['TDFN04'];
	$ar_r['fl_cli_bloc'] += $r['TDFN02'];
	$ar_r['importo_fat'] += $r['importo_fat'];




}



function crea_ar_tree_consegne_montaggio($node, $form_values, $forza_generazione_completa = 'N'){
	
	$main_module = new DeskPVen();
	global $s, $cfg_mod_DeskPVen, $cfg_mod_Gest, $cfg_mod_Spedizioni, $conn, $id_ditta_default, $auth;

	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	
	$sql_where = parametri_sql_where($form_values);
	$sql_where_scaduti = parametri_sql_where($form_values, true);

	$data_limite = $form_values->f_data;
	
	if(strlen($form_values->f_data) > 0 && strlen($form_values->f_data_a) > 0 && $form_values->f_data == $form_values->f_data_a)
	    $expande_all = true;
	
	$ar = array();
			
	//FATTURATO MERCE, IMPORTO FATTURE
	$sql_f = "SELECT SUM(TFINFI) AS S_TFINFI, SUM(TFTIMP) AS S_TFTIMP, SUM(TFTANT) AS S_TFTANT
	FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VF' AND TFNATD='F'
	AND TFCCON = ?
	GROUP BY TFDT, TFCCON ";
	 
	$stmt_f = db2_prepare($conn, $sql_f);
	echo db2_stmt_errormsg();
	
	if($node == 'SCADUTI'){
			
	
		$sql= "SELECT TDCLOR, TDODRE, TDOPRI, TDODER, TDTOCO, TDCOPR, TDCOSP, TDVETT, TDGGRI, TDSTAT, TDDSST, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ,
		TDDOCU, TDOADO, TDONDO, TDOTPD, TDDTEP, TDDATP, TDVET1, TDTPCA, TDAACA, TDNRCA, TDPROF, TDFN11, TDFN19,
		SUM(TDTOCO) AS COLLI, SUM (TDCOPR) DISP,
		SUM(TDVOLU) AS VOLUME, SUM(TDTIMP) AS IMPORTO,
		TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS, TDFN15, TDFN04, TDCITI,
		TDDTNEW, TDFN14, TDDTAR, TDHMAR, TDFN13, TDTCNS, TDFLSV, TDFLSV, TDFG05, TDSECA, TDFG06
		FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
		   ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
		$sql_where_scaduti AND TDDTEP < $data_limite
		GROUP BY TDDTEP, TDVETT, TDVET1, TDGGRI, TDCITI, TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS,
		TDFN15, TDFN04, TDSTAT, TDDSST, TDCCON, TDDCON, TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ, TDFN11, TDFN19,
		TDDTNEW, TDFN14, TDDTAR, TDHMAR, TDFN13, TDTCNS, TDFLSV, TDFLSV, TDDOCU, TDOADO, TDONDO, TDOTPD, TDFG06,
        TDCLOR, TDODRE, TDOPRI, TDODER, TDTOCO, TDCOPR, TDCOSP, TDFG05, TDDATP, TDSECA, TDTPCA, TDAACA, TDNRCA, TDPROF
        ORDER BY TDSECA, TDGGRI, TDDTEP";
		
	
		$stmt= db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
			
			
		//FATTURATO MERCE, IMPORTO FATTURE
		$sql_f = "SELECT SUM(TFINFI) AS S_TFINFI, SUM(TFTIMP) AS S_TFTIMP, SUM(TFTANT) AS S_TFTANT
		FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
		WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VA' AND TFTPDO NOT IN ('NC', 'MN')
		AND TFCCON = ?
		GROUP BY TFDT, TFCCON ";
	
		$stmt_f = db2_prepare($conn, $sql_f);
		echo db2_stmt_errormsg();
	
			
		while ($r = db2_fetch_assoc($stmt)) {
	
			$tmp_ar_id = array('SCADUTI');
			$ar_new = array();
			$ar_r= &$ar;
	
			if($r['TDGGRI'] >=0 && $r['TDGGRI']<=5){
				$cod_liv1= "D_0_A_5";
				$ar_new['task'] = 'Da meno di 5 gg';
			}
			if($r['TDGGRI'] >5 && $r['TDGGRI']<=10){
				$cod_liv1= "D_5_A_10";
				$ar_new['task'] = 'Da meno di 10 gg';
			}
			if($r['TDGGRI'] >10 && $r['TDGGRI']<=15){
				$cod_liv1= "D_10_A_15";
				$ar_new['task'] = 'Da meno di 15 gg';
			}
			if($r['TDGGRI'] >15 && $r['TDGGRI']<=20){
				$cod_liv1= "D_15_A_20";
				$ar_new['task'] = 'Da meno di 20 gg';
			}
			if($r['TDGGRI'] >20 && $r['TDGGRI']<=25){
				$cod_liv1= "D_20_A_25";
				$ar_new['task'] = 'Da meno di 25 gg';
			}
			if($r['TDGGRI'] >22 && $r['TDGGRI']<=30){
				$cod_liv1= "D_25_A_30";
				$ar_new['task'] = 'Da meno di 30 gg';
			}
			if($r['TDGGRI'] >30){
				$cod_liv1= "D_30_A_9999";
				$ar_new['task'] = 'Da oltre 30 gg';
			}
			
			//NON STATTO PIU' PER GG RIT
			$data_vet= array();
			$data_vet[0]=trim($r['TDDTEP']);
			$data_vet[1]=trim($r['TDVETT']);
			$cod_liv1 = implode("_", $data_vet); //DATA VETTORE
			setlocale(LC_TIME, 'it_IT');
			
			if (strlen(trim($data_vet[1])) == 0)
			    $d_vettore = 'Vettore non assegnato';
			else
			    $d_vettore = $s->decod_std('AUTR', trim($data_vet[1]));
			
			$ar_new['task'] = implode(" - ", array(
			     ucfirst(strftime("%a %d/%m/%y", strtotime($data_vet[0]))),
			     $d_vettore)); //DATA VETTORE			
				
			$cliente_dest=array();
			$cliente_dest[0]=trim($r['TDCCON']);
			$cliente_dest[1]=trim($r['TDCDES']);
			$cliente_dest[2]=trim($r['TDSECA']);
	
			$cod_liv2 = implode("_", $cliente_dest); //CLIENTE_DESTINAZIONE
				
			$cod_liv3=$r['TDDOCU'];
				
				
	
			//$ar_r=&$ar_r['children'];
			$liv =$cod_liv1;
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
					
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['liv'] = 'liv_2';
				$ar_new['gg_ritardo'] =  $r['TDGGRI'];
				 
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			sum_columns_value($ar_r, $r);
				
				
			$ar_r=&$ar_r['children'];
			$liv =$cod_liv2;
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['task'] = $r['TDDCON'];
				$ar_new['data'] = $r['TDDTEP'];
				$ar_new['cod_cli'] = $r['TDCCON'];
				$ar_new['cod_iti'] = $r['TDCITI'];
				$ar_new['seq_carico'] = $r['TDSECA'];
				$ar_new['riferimento'] =$s->scrivi_rif_destinazione($r['TDLOCA'], $r['TDNAZI'], $r['TDPROV'], $r['TDDNAZ']);
				//$ar_new['comm']  = has_commento_ordine_delivery_from_cli($ar_new['cod_cli']);
	
				//FATTURA e IMPORTO MERCE
				$result = db2_execute($stmt_f, array($r['TDCCON']));
				$row_f = db2_fetch_assoc($stmt_f);
				$ar_new['fat_merce'] = $row_f['S_TFINFI'] - $row_f['S_TFTANT'];
				$ar_new['importo_fat'] = $row_f['S_TFTIMP'] - $row_f['S_TFTANT'];
	
				$ar_new['stato'] = $r['TDSTAT'];
				$ar_new['gg_ritardo'] =  $r['TDGGRI'];
				$ar_new['liv'] = 'liv_3';
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			sum_columns_value($ar_r, $r);
				
			$ar_r=&$ar_r['children'];
			$liv =$cod_liv3;
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['iconCls']= $s->get_iconCls(3, $r);
				$ar_new['task'] = trim($r['TDOADO'])."_".trim($r['TDONDO']);
				$ar_new['k_ordine'] = trim($r['TDDOCU']);
				
	           if($js_parameters->only_view != 1){
				$ha_commenti = $s->has_commento_ordine_by_k_ordine($ar_new['k_ordine']);
				
				if ($ha_commenti == FALSE)
				    $img_com_name = "icone/16x16/comment_light.png";
				else
				    $img_com_name = "icone/16x16/comment_edit_yellow.png";
	                  
				$ar_new['task'] .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine_bl(\'' . $ar_new['k_ordine'] . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
	           }
	           
	           
				$ar_new['comm']  = has_commento_ordine_delivery($ar_new['k_ordine']);
				$ar_new['comm_car']  = has_commento_ordine_delivery($ar_new['k_ordine'], 'CAR');
				$ar_new['stato'] = $r['TDSTAT'];
				$ar_new['ordine'] = trim($r['TDOADO'])."_".trim($r['TDONDO']);
				$ar_new['cliente'] = $r['TDDCON'];
				$ar_new['tipo'] = $r['TDOTPD'];
				$ar_new['gg_ritardo'] =  $r['TDGGRI'];
				$ar_new['raggr'] = trim($r['TDCLOR']);
				$ar_new['data_reg'] = trim($r['TDODRE']);
				$ar_new['priorita'] 	= trim($r['TDOPRI']);
				$ar_new['tp_pri'] 		= trim($s->get_tp_pri(trim($r['TDOPRI']), $r));
				$ar_new['cons_rich'] 	= $r['TDODER'];
				$ar_new['data_atp'] 	= $r['TDDATP'];
				$ar_new['colli'] 		= $r['TDTOCO'];
				$ar_new['colli_disp'] 	= $r['TDCOPR'];
				$ar_new['controllato'] 	= $r['TDFG05'];
				$ar_new['tdfg06'] 	    = $r['TDFG06'];
				$ar_new['descr'] 	= $r['RDDART'];
				$ar_new['um'] 	    = $r['RDUM'];
				$ar_new['quant'] 	= $r['RDQTA'];
				$ar_new['liv'] = 'liv_4';
				$ar_new['leaf']=true;
				$ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			sum_columns_value($ar_r, $r);
	
	
	
		}
			
		$ret = array();
		foreach($ar as $kar => $v){
			$ret[] = array_values_recursive($ar[$kar]);
		}
			
		echo acs_je(array('success' => true, 'children' => $ret));
			
		exit;
	
	}
	
	
	
	
	
	
	if ($forza_generazione_completa != 'Y' && ($node == '' || $node == 'root')){
	
		$sql_group_by.=" GROUP BY CS.CSAARG, CS.CSNRSE, TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO,
		                            TDFMTS, TDFN15, TDFN04, TDFG05, TDDATP, TDSECA,  TDTPCA, TDAACA, TDNRCA,
			                		TDDTNEW, TDFN14, TDDTAR, TDHMAR, TDFN13, TDTCNS, TDFLSV, TDFLSV, TDIDES,
							    	TDCLOR, TDODRE, TDOPRI, TDODER, TDTOCO, TDCOPR, TDCOSP, TDVOLU, TDVSRF,
                                    CFTEL, CFTEX, TDPROF, TDFN11, TDFN19, TDFG06
			                		ORDER BY CS.CSAARG, CS.CSNRSE, TDSECA";
		$data= "";
		$vettore = "";
		$tddocu = "";
		$cliente= "";
		$dest= "";
		$stato= "";
		$cod_iti= "";
	
	}
	else{
		
		if ($forza_generazione_completa != 'Y'){
			$anno_set= explode("_", $node);
			$anno= $anno_set[0];
			$set= explode("|", $anno_set[1]);
			$sql_where.=" AND CS.CSAARG=$anno AND CS.CSNRSE=$set[0]";
		}
		
		$sql_group_by.=" GROUP BY CS.CSAARG, CS.CSNRSE, TDBLEV, TDCITI, TDBLOC, TDFN02, TDFMTO, TDDTCF,
									TDMTOO, TDFMTS, TDFN15, TDFN04, TDDTEP, TDVETT, TDVET1, TDSTAT, TDDSST, TDCCON, TDDCON,
									TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ, TDONDO, TDDOCU, TDOADO, TDOTPD,
									TDDTNEW, TDFN14, TDDTAR, TDHMAR, TDFN13, TDTCNS, TDFLSV, TDFLSV, TDDATP, TDSECA,
									TDCLOR, TDODRE, TDOPRI, TDODER, TDTOCO, TDCOPR, TDCOSP, TDVOLU, TDVSRF, TDFG05,
                                    TDIDES, CFTEL, CFTEX, TDTPCA, TDAACA, TDNRCA, TDPROF, TDFN11, TDFN19, TDFG06							
                                    ORDER BY TDDTEP, TDAACA, TDNRCA, TDSECA, TDDCON, TDCLOR DESC";
		$data= ", TDDTEP";
		$vettore = ", TDVETT, TDVET1";
		$tddocu = ", TDDOCU, TDOADO, TDONDO, TDOTPD";
		$cod_iti= ", TDCITI";
		$cliente= ", TDCCON, TDDCON";
		$dest= ", TDCDES, TDLOCA, TDNAZI, TDPROV, TDDNAZ";
		$stato= ", TDSTAT, TDDSST";
			
			
	}
	
	
	//prima riga scaduti totali
	if ($node == '' || $node == 'root'){
			
		$ar['TOTALE']=array();
		$ar['TOTALE']['id'] = "TOTALE";
		$ar['TOTALE']['task'] = "TOTALE";
		$ar['TOTALE']['liv'] = 'liv_totale';
		$ar['TOTALE']['expanded']= true;
			
	
		$sql_1="SELECT SUM(TDTOCO) AS COLLI, SUM (TDCOPR) DISP,
		SUM(TDFN02) AS TDFN02, SUM(TDFN04) AS TDFN04,
		SUM(TDVOLU) AS VOLUME, SUM(TDTIMP) AS IMPORTO
		FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
		ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
		$sql_where_scaduti AND TDDTEP < $data_limite";
	
	
		$stmt_1 = db2_prepare($conn, $sql_1);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt_1);
		$r_1= db2_fetch_assoc($stmt_1);
			
		$ar_new = array();
		$ar_new['id'] = 'SCADUTI';
		$ar_new['colli_tot'] += $r_1['COLLI'];
		$ar_new['colli_disp'] +=$r_1['DISP'];
		$ar_new['colli_non_disp'] += ($r_1['COLLI'] - $r_1['DISP']);
		$ar_new['importo'] +=$r_1['IMPORTO'];
		$ar_new['volume'] +=$r_1['VOLUME'];
		$ar_new['fl_da_prog'] += $r_1['TDFN04'];
		$ar_new['fl_cli_bloc'] += $r_1['TDFN02'];
		$ar_new['task'] = 'Scaduti';
		$ar_new['liv'] = 'liv_1';
		$ar['TOTALE']['children']["SCADUTI"] = $ar_new;
			
		sum_columns_value($ar['TOTALE'], $r_1);
			
	}
	
	
		
		
		
	$sql= "SELECT CS.CSAARG, CS.CSNRSE $data $vettore $tddocu $cliente $dest $stato $cod_iti,
	SUM(TDTOCO) AS COLLI, SUM (TDCOPR) AS DISP,
	SUM(TDVOLU) AS VOLUME, SUM(TDTIMP) AS IMPORTO,
	TDBLEV, TDBLOC, TDFN02, TDFMTO, TDDTCF, TDMTOO, TDFMTS, TDFN15, TDFN04,
	TDDTNEW, TDFN14, TDFN11, TDFN19, TDDTAR, TDHMAR, TDFN13, TDTCNS, TDFLSV, TDFLSV, TDFG05, TDSECA,
	TDCLOR, TDODRE, TDOPRI, TDODER, TDTOCO, TDCOPR, TDCOSP, TDVOLU, TDVSRF, TDDATP,
    TDIDES, CFTEL, CFTEX,  TDTPCA, TDAACA, TDNRCA, TDPROF, TDFG06
    FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
	   ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
	   ON CF.CFDT = TD.TDDT AND CF.CFCD = TD.TDCCON
	$sql_where AND TDDTEP >= $data_limite $sql_group_by";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	//FATTURA VF eslcudo le note credito con tfnatd = 'f'
	$sql_f1 = "SELECT *
	FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VF' AND TFNATD = 'F' AND TFDOAB=? ORDER BY TFDTRG DESC";
	
	$stmt_f1 = db2_prepare($conn, $sql_f1);
	echo db2_stmt_errormsg();
	
	//VD BOLLA PER ASSISTENZE
	$sql_vd = "SELECT *
	FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VD' AND TFSTAT <> 'AN' AND TFDOAB=? ORDER BY TFDTRG DESC";
	
	$stmt_vd = db2_prepare($conn, $sql_vd);
	echo db2_stmt_errormsg();
	
	//TO0 PER TRASPORTO
	$sql_to = "SELECT *
	FROM {$cfg_mod_DeskPVen['file_testate_gest_valuta']}
    WHERE TODT = ? AND TOTIDO = ? AND TOINUM = ? AND TOAADO = ? AND TONRDO = ? AND TOVALU='EUR'";
	
	$stmt_to = db2_prepare($conn, $sql_to);
	echo db2_stmt_errormsg();

	
	
	
	while ($r = db2_fetch_assoc($stmt)) {
			
		$tmp_ar_id = array();
	
		$ar_r= &$ar;
			
	
		//stacco dei livelli
		$anno_set= array();
		$anno_set[0]=trim($r['CSAARG']);
		$anno_set[1]=trim($r['CSNRSE']);
		
			
		$cod_totale ="TOTALE"; //totale generale
	//	if(isset($form_values->f_report_carico) && $form_values->f_report_carico != 'Y')
		 $cod_liv0 = implode("_", $anno_set); //NUMERO ANNO_SETTIMANA
			
		$data_vet= array();
		$data_vet[0]=trim($r['TDDTEP']);
		$data_vet[1]=trim($r['TDVETT']);
		$data_vet[2]=trim($r['TDTPCA']);
		$data_vet[3]=trim($r['TDAACA']);
		$data_vet[4]=trim($r['TDNRCA']);
		$cod_liv1 = implode("_", $data_vet); //DATA VETTORE
			
		//decodifica vettore
		if (strlen(trim($data_vet[1])) == 0)
			$d_vettore = 'Vettore non assegnato';
		else
			$d_vettore = $s->decod_std('AUTR', trim($data_vet[1]));
					
		$cliente_dest=array();
		$cliente_dest[0]=trim($r['TDCCON']);
		$cliente_dest[1]=trim($r['TDCDES']);
		$cliente_dest[2]=trim($r['TDSECA']);
			
		$cod_liv2 = implode("_", $cliente_dest); //CLIENTE_DESTINAZIONE
		$cod_liv3 =trim($r['TDDOCU']); //ordine
					
					
				$liv =$cod_totale;
				//$tmp_ar_id[] = $liv;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
				}
					
				//facciamo sempre perche' TOTALE viene creato da SCADUTI
					
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $r);
				
				
				$ar_r=&$ar_r['children'];
				$liv =$cod_liv0;
				$tmp_ar_id[] = $liv;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					if ($ar_new['prod']==''){
						$ar_new['prod']=0;
					}
					$anno=trim($r['CSAARG']);
					$sett=trim($r['CSNRSE']);
					$ar_new['task'] = $anno." Settimana <b>".$sett."</b>";
					$ar_new['liv'] = 'liv_1';
					$ar_new['expanded'] = $expande_all;
					$ar_r["{$liv}"] = $ar_new;
	
				}
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $r);
				
	
				if ($forza_generazione_completa != 'Y' && ($node == '' || $node == 'root')){
					continue;
				}
					
				//data_vettore (tipo_anno_numero carico)
				$ar_r=&$ar_r['children'];
				$liv =$cod_liv1;
				$tmp_ar_id[] = $liv;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					setlocale(LC_TIME, 'it_IT');
					$ar_new['task'] =  ucfirst(strftime("%a %d/%m", strtotime($data_vet[0]))) .' - '.sprintf("%06s", $data_vet[4]). ' - ' . $d_vettore;
					$ar_new['data_vett'] = print_date($r['TDDTEP']);
					$ar_new['bar_data_vett'] = trim($r['TDDTEP']).trim($r['TDVET1']);
					$ar_new['bar_anno_numero'] = $data_vet[3]. sprintf("%06s", $data_vet[4]);
					$ar_new['nr_carico'] = sprintf("%06s", $data_vet[4]);
					$ar_new['expanded'] = $expande_all;
					
					if(trim($r['TDVETT']) == '')
					    $ar_new['vettore'] = 'Vettore non assegnato';
					else
					   $ar_new['vettore'] =$s->decod_std('AUTR', trim($r['TDVETT']));
					 
					$ar_new['liv'] = 'liv_2';
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r_data_vett = &$ar_r["{$liv}"];
				
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $r);
					
				//cliente_destinazione
				$ar_r=&$ar_r['children'];
				$liv =$cod_liv2;
				$tmp_ar_id[] = $liv;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['task'] = $r['TDDCON'];
					$ar_new['cod_cli'] = $r['TDCCON'];
					$ar_new['seq_carico'] = $r['TDSECA'];
					if(trim($r['CFTEX'])!='')
					    $ar_new['tel'] 	= $r['CFTEL']." <br> ".$r['CFTEX'];
					else
					    $ar_new['tel'] 	= $r['CFTEL'];
					//FATTURA e IMPORTO MERCE
					$result = db2_execute($stmt_f, array($r['TDCCON']));
					$row_f = db2_fetch_assoc($stmt_f);
					$ar_new['fat_merce'] = $row_f['S_TFINFI'] - $row_f['S_TFTANT'];
					$ar_new['importo_fat'] = $row_f['S_TFTIMP'] - $row_f['S_TFTANT'];
					
	                $ar_new['data'] = $r['TDDTEP'];
					$ar_new['riferimento'] =$s->scrivi_rif_destinazione($r['TDLOCA'], $r['TDNAZI'], $r['TDPROV'], $r['TDDNAZ']);
					$ar_new['indirizzo'] = $r['TDIDES'];
					$ar_new['cod_iti'] = $r['TDCITI'];
					$ar_new['liv'] = 'liv_3';
					$ar_new['expanded'] = $expande_all;
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $r);
	
	
				//ORDINE
				$ar_r=&$ar_r['children'];
				$liv =$cod_liv3;
				$tmp_ar_id[] = $liv;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['k_ordine'] = trim($r['TDDOCU']);
					$ar_new['iconCls']= $s->get_iconCls(3, $r);
					$ar_new['fl_evaso'] = $r['TDFN11'];
					$ar_new['task'] = trim($r['TDOADO'])."_".trim($r['TDONDO'])."_".trim($r['TDOTPD']);
					
	               if($js_parameters->only_view != 1){
					$ha_commenti = $s->has_commento_ordine_by_k_ordine($ar_new['k_ordine']);
					
					if ($ha_commenti == FALSE)
					    $img_com_name = "icone/16x16/comment_light.png";
					else
					    $img_com_name = "icone/16x16/comment_edit_yellow.png";
					
					$ar_new['task'] .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine_bl(\'' . $ar_new['k_ordine'] . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
	               }
					
	                $ar_new['comm']  = has_commento_ordine_delivery($ar_new['k_ordine']);
	                $ar_new['comm_car']  = has_commento_ordine_delivery($ar_new['k_ordine'], 'CAR');
					$ar_new['k_ord_report'] = trim($r['TDOADO'])."_".trim($r['TDONDO'])."_".trim($r['TDOTPD']);
					//$ar_new['barcode'] = trim($r['TDOTPD']).trim($r['TDOADO']).trim($r['TDONDO']);
					$ar_new['data'] = $r['TDDTEP'];
					$ar_new['ordine'] = trim($r['TDOADO'])."_".trim($r['TDONDO']);
					$ar_new['cliente'] = $r['TDDCON'];
					if(trim($r['TDVETT']) == '')
					    $ar_new['vettore'] = 'Vettore non assegnato';
				    else
				        $ar_new['vettore'] =$s->decod_std('AUTR', trim($r['TDVETT']));
					$ar_new['proforma'] = $r['TDPROF'];
					$ar_new['stato'] = $r['TDSTAT'];
					$ar_new['tipo'] = $r['TDOTPD'];
					$ar_new['cod_cli'] = $r['TDCCON'];
					$ar_new['nr_carico'] = sprintf("%06s", $data_vet[4]);
					$ar_new['liv'] = 'liv_4';
					$ar_new['leaf']=true;
	
					//dry
					$ar_new['riferimento']	= trim($row['TDVSRF']);
					$ar_new['raggr'] 		= trim($r['TDCLOR']);
					$ar_new['data_reg'] 	= trim($r['TDODRE']);
					$ar_new['priorita'] 	= trim($r['TDOPRI']);
					$ar_new['tp_pri'] 		= trim($s->get_tp_pri(trim($r['TDOPRI']), $r));
					$ar_new['cons_rich'] 	= $r['TDODER'];
					$ar_new['data_atp'] 	= $r['TDDATP'];
					$ar_new['colli'] 		= $r['TDTOCO'];
					$ar_new['colli_disp'] 	= $r['TDCOPR'];
					$ar_new['colli_sped'] 	= $r['TDCOSP'];
					$ar_new['controllato'] 	= $r['TDFG05'];
					$ar_new['tdfg06'] 	    = $r['TDFG06'];
					
					//BOLLA VD (ASSISTENTE E ORDINI DI TRASFERIMENTO)
					if(in_array($ar_new['tipo'], $cfg_mod_DeskPVen["tipo_ord_scarico"])){
					    
					    $stmt_fb = db2_prepare($conn, $sql_vd);
					    echo db2_stmt_errormsg();
					    //////////// $stmt_fb = $stmt_vd;
					    
					} else {
					    
					    //FATTURA VF e resto
					    $stmt_fb = db2_prepare($conn, $sql_f1);
					    echo db2_stmt_errormsg();
					    //////////// $stmt_fb = $stmt_f1;
					    
					}
					
					/*if($ar_new['tipo'] == 'MA' || $ar_new['tipo'] == 'OT'){
					  
					    $stmt_fb = $stmt_vd;
					}else{
					    
					    $stmt_fb = $stmt_f1;
					}
					*/
					
					$result = db2_execute($stmt_fb, array($r['TDDOCU']));
					$row_fb = db2_fetch_assoc($stmt_fb);
					if(trim($row_fb['TFAADO']) != ''){
					    $ar_new['fattura'] = trim($row_fb['TFAADO'])."_".trim($row_fb['TFNRDO'])."_".trim($row_fb['TFTPDO']);
					    $ar_new['k_ordine_fatt'] = implode("_", array($row_fb['TFDT'], $row_fb['TFTIDO'], $row_fb['TFINUM'], $row_fb['TFAADO'], $row_fb['TFNRDO']));
					}
					$ar_new['data_fat'] = trim($row_fb['TFDTRG']);
					$ar_new['imp_merce_ivato'] = $row_fb['TFINFI'] + $row_fb['TFINFI']*22/100;
					if($row_fb['TFTOTD'] > 0){
					    $ar_r_data_vett['imp_fatt'] += $row_fb['TFTOTD'] - $row_fb['TFIMP5'];
					}
					$ar_new['imp_fatt'] = $row_fb['TFTOTD'] - $row_fb['TFIMP5'];
				  				   
				    $oe = $s->k_ordine_td_decode_xx($ar_new['k_ordine_fatt']);
				   
				    $result = db2_execute($stmt_to, $oe);
				    $row_to = db2_fetch_assoc($stmt_to);
				    
				    if(trim($row_to['TOASP1']) != ''){
				        $ar_new['trasp_cli'] = trim($row_to['TOVSP1']);
				        if($row_to['TOVSP1'] > 0){
				            $ar_r_data_vett['trasp_cli'] += trim($row_to['TOVSP1']);
				        }
				       
				    }  
				    
				    if(trim($row_to['TOASP2']) != ''){
				        $ar_new['trasp_ns'] = trim($row_to['TOVSP2']);
				        if($row_to['TOVSP2'] > 0){
				            $ar_r_data_vett['trasp_ns'] += trim($row_to['TOVSP2']);
				        }
				    }
				   
				    
				    
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $r);
				
				
	
	} //while
	
	
	if ($node != '' && $node != 'root'){
		$ar= $ar['TOTALE']['children'][$node]['children'];
	}
	
	
	return $ar;
}


function parametri_sql_where($form_values, $per_scaduti = false){
  
	global $s;

	$sql_where.= " WHERE " . $s->get_where_std();

	$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);

	$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);

	$sql_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);

	$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);

	$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values ->f_cliente);
	
	$sql_where.= sql_where_by_combo_value('TD.TDSTAB', $form_values->f_sede);
	
	$sql_where.= sql_where_by_combo_value('TD.TDVETT', $form_values->f_vettore);
	
	if (strlen($form_values->f_only_data) > 0)
	   $sql_where.= sql_where_by_combo_value('TD.TDDTEP', $form_values->f_only_data);
	
   if($form_values->f_resi == 'Y')
       $sql_where.= " AND TDOTPD = 'MR'";
   else 
	   $sql_where.= " AND TDOTPD NOT IN ('MP', 'MR')";
	
	
	//controllo data
	// per recupero dati per scaduti ignoro il filtro per data
	if($per_scaduti != true){ 
        if (strlen($form_values->f_data) > 0)
            $sql_where .= " AND TD.TDDTEP >= {$form_values->f_data}";
        if (strlen($form_values->f_data_a) > 0)
            $sql_where .= " AND TD.TDDTEP <= {$form_values->f_data_a}";
	}
    
    //filtro carico
    if (strlen($form_values->f_da_carico) > 0)
        $sql_where .= " AND TD.TDNRCA >= {$form_values->f_da_carico}";
    if (strlen($form_values->f_a_carico) > 0)
        $sql_where .= " AND TD.TDNRCA <= {$form_values->f_a_carico}";
    
    

	//controllo HOLD
	$filtro_hold=$form_values->f_hold;

	if($filtro_hold== "Y")
		$sql_where.=" AND TDSWSP = 'Y'";
	if($filtro_hold== "N")
		$sql_where.=" AND TDSWSP = 'N'";

	$filtro_evasi=$form_values->f_evasi;

//solo ordini evasi
	if ($filtro_evasi == "Y"){
		$sql_where.= " AND TDFN11 = 1 ";
	}
	//solo ordini da evadere
	if ($filtro_evasi == "N"){
		$sql_where.= " AND TDFN11 = 0 ";
	}
	
	$filtro_controllati=$form_values->f_controllati;
	
	if ($filtro_controllati == "Y"){
	    $sql_where.= " AND TDFG05 = 'Y' ";
	}elseif($filtro_controllati == "N"){
	    $sql_where.= " AND TDFG05 = '' ";
	}
			
	return $sql_where;
}



function has_commento_ordine_delivery($k_ord, $bl = null){
    global $conn;
    global $cfg_mod_DeskPVen, $s;
      
    if (is_null($bl))
        $bl = $cfg_mod_DeskPVen['pv_commenti_delivery'];
        
        $oe = $s->k_ordine_td_decode($k_ord);
        
        $sql = "SELECT count(*) AS NR
        FROM {$cfg_mod_DeskPVen['file_righe']}
        WHERE RDDT=" . sql_t($oe['TDDT']) . " AND RDOTID=" . sql_t($oe['TDOTID'])  . "
				  AND RDOINU=" . sql_t($oe['TDOINU']) . " AND RDOADO=" . sql_t($oe['TDOADO'])  . " AND RDONDO=" . sql_t($oe['TDONDO'])  . "
				  AND RDTPNO=" . sql_t($bl) . "
			";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if ($row['NR'] > 0) return 1;
        else return 0;
}




function get_path_attachment_fattura($cod_cli, $k_fattura){
    global $cfg_mod_DeskPVen;
    //0013942/FTCLI-6-001015-CF1-2018-MIDI-900013942-08-06-2018-F.PDF (fattura) 
    //900016568/DTCLI-6-000130-BT1-2018-MIDI-900016568-07-11-2018-F.PDF
    $ar_fattura = explode('_', $k_fattura);
    $tmp_fatt_path = implode('-', array(
        trim($ar_fattura[0]), //ditta
        trim($ar_fattura[4]), //ordine
        trim($ar_fattura[2]), //indice numerazione
        $ar_fattura[3] //anno
     )); 
    
    $img_dir = $cfg_mod_DeskPVen['allegati_root_C'] . trim($cod_cli);    
    $search_path = "{$img_dir}/*-{$tmp_fatt_path}-*";
    
    $ret = array();
    foreach (glob($search_path) as $filename)    
    {
        $nome_file_ar = explode('/', $filename);
        $nome_file = end($nome_file_ar);
        $str_file = explode('-', $nome_file);
        $ret[$str_file[0]] = $filename;
    }    
    
    return $ret;    
    
}