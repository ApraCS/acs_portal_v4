<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$m_params = acs_m_params_json_decode();
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
$mod_js_parameters = $main_module->get_mod_parameters();

if ($_REQUEST['fn'] == 'exe_aggiungi_articolo'){

	$m_params = acs_m_params_json_decode();
	$sql_where = "";
	$cod = $m_params->codice;
	$ord = $s->get_ordine_by_k_ordine($m_params->k_ordine);
	$tipologia = trim($ord['TDCLOR']);
	if(isset($mod_js_parameters->Ass_tipo_parte) && $tipologia == 'A')
	    $sql_where .= " AND ARTPAR = '{$mod_js_parameters->Ass_tipo_parte}'";
	   
	
	
	$ret = array();
	
	if($m_params->esclusi_sosp == 'Y'){
	    $select = ", ARSOSP";
	    $groupby = " GROUP BY ARSOSP";
	}

	//print_r(strtoupper($m_params->codice));
	if($cod != '*' && strtoupper($cod) != 'NOTE'){
	
		$sql="SELECT COUNT(*) AS C_ART {$select}
		FROM {$cfg_mod_DeskPVen['file_anag_art']} AR
		WHERE ARDT = '$id_ditta_default'  {$sql_where}
		AND ARFOR1 <> 0 AND ARART ='{$m_params->codice}' {$groupby}";
		
	
		
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$row = db2_fetch_assoc($stmt);
		
		if($row['C_ART'] == 0){
			$ret['success'] = false;
			$ret['message_cod'] = 'art_not_found';
			echo acs_je($ret);
			exit;
		}
		
		
		
		if($m_params->esclusi_sosp == 'Y'){
		
		if($row['ARSOSP'] == 'S'){
		    $ret['success'] = false;
		    $ret['message_cod'] = 'art_sospeso';
		    echo acs_je($ret);
		    exit;
		}

		}
	}
	
	$sh = new SpedHistory();
	$sh->crea(
			'aggiungi_articoli',
			array(
					"k_ordine"			=> $m_params->k_ordine,
					"codice" 			=> $m_params->codice,
					"quant"		        => $m_params->quant,
					"prezzo"		    => $m_params->prezzo,
					"articolo" 			=> $m_params->articolo
	
			)
			);

	
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}

if ($_REQUEST['fn'] == 'exe_cancella_articoli'){
	
	$use_session_history = microtime(true);

	$m_params = acs_m_params_json_decode();
	
	$row = $m_params->rows;
	$sh = new SpedHistory();
			$sh->crea(
					'cancella_articoli',
					array(
							"k_ordine"			=> $m_params->k_ordine,
					         "nrec"		        => $row->num_ord,
					         "codice" 			=> $row->codice,
					         "articolo"		    => $row->articolo,
						
					)
					);
			

			
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_articoli'){

	$m_params = acs_m_params_json_decode();

	$mod_rows= $m_params->mod_rows;

	foreach ($mod_rows as $v){

		if(isset($v)){

			$k_ordine = $m_params->k_ordine;

			if($v->prezzo == ""){
				$v->prezzo=0;
			}

			$sh = new SpedHistory();
			$sh->crea(
					'modifica_articoli',
					array(
							"k_ordine"			=> $k_ordine,
							"codice" 			=> $v->codice,
							"riga" 			    => $v->riga,
						    "articolo"		    => $v->articolo,
							"quant"		        => $v->quant,
							"prezzo"		    => $v->prezzo,
							"nrec"		        => $v->num_ord,
							"stato" 			=> $v->stato
					)
					);

		}

	}

	exit;
}


// ******************************************************************************************
// modifica riga
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_ass_costo_pre'){
    
    $m_params = acs_m_params_json_decode();

    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ASS_CST_PRV',
            "k_ordine"      => $m_params->k_ordine,
            "vals" => array(
                "RIIMPO" 	=> sql_f($m_params->form_values->f_costo_pre),
                "RIART" 	=> $m_params->form_values->f_c_art,
                "RIDART" 	=> $m_params->form_values->f_d_art,
                "RINRCA" 	=> $m_params->form_values->f_nrec,
                'RIIVES'  => "L"
            ),
        )
        );
    
  
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'get_json_data'){

	$m_params = acs_m_params_json_decode();
	$k_ordine= $m_params->k_ordine;
	$ar = array();

	$oe = $s->k_ordine_td_decode_xx($k_ordine);
	$ord = $s->get_ordine_by_k_docu($k_ordine);
	
	if($cfg_mod_DeskPVen["RTPUNT_sottocampato_da"] != 0){
	    $rtpunt = " case when trim(substr(rtfil1, ".$cfg_mod_DeskPVen["RTPUNT_sottocampato_da"].", 15))='' then 0 else
                dec(substr(rtfil1, ".$cfg_mod_DeskPVen["RTPUNT_sottocampato_da"].", 15)) / 100000 end as RTPUNT ";
	}else{
	    $rtpunt = "RTPUNT";
	}

		$sql = "SELECT RDART, RDDART, RDRIGA, RDQTA, RDQTE, RDUM, RTPRZ, RDNREC, RDSTAT, RDSTEV, RDDEPO,
                 $rtpunt,  RA.N_ROW AS N_AL, SUBSTR(TA_ON.TAREST, 61, 60) AS EMAIL_ON, SUBSTR(TA_AO.TAREST, 41, 60) AS EMAIL_AO 
                FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
	 		    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
	    			ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'								
	    	    LEFT OUTER JOIN (
                       SELECT COUNT(*) AS N_ROW, RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC
                       FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA 
                       GROUP BY RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC) RA
                ON RD.RDDT=RA.RADT AND RD.RDTIDO = RA.RATIDO AND RD.RDINUM = RA.RAINUM AND RD.RDAADO = RA.RAAADO AND RD.RDNRDO = RA.RANRDO AND RD.RDNREC=RA.RANREC                
                LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art']} AR
	    		  ON AR.ARDT=RD.RDDT AND AR.ARART = RD.RDART	  
	    		LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tab_sys']} TA_ON
			      ON TA_ON.TADT = RD.RDDT AND TA_ON.TACOR1 = DIGITS(AR.ARFOR1) AND TA_ON.TAID = 'BWWW' AND TA_ON.TANR = '*ON' 
                LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tab_sys']} TA_AO
			      ON TA_AO.TADT = RD.RDDT AND TA_AO.TACOR1 = DIGITS(AR.ARFOR1) AND TA_AO.TAID = 'BRCF' AND TA_AO.TANR = '@A0'             
                WHERE RD.RDART NOT IN('ACCONTO', 'CAPARRA') AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
				AND RD.RDTISR = '' AND RD.RDSRIG = 0
	 			ORDER BY RD.RDRIGA";
       
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $oe);
		
		while($row = db2_fetch_assoc($stmt)){
			$nr = array();
			$nr['codice']	    = trim($row['RDART']);
			$nr['articolo']	    = utf8_encode(trim($row['RDDART']));
			$nr['riga']	        = trim($row['RDRIGA']);
			$nr['costo_pre']	= trim($row['RTPUNT']);
			$nr['TDFG04']	    = trim($ord['TDFG04']);  //(f_spunta) ordine controllato (ad esempio non faccio fare Assegna costo preventivo)
			if($nr['codice']=='*'){
				$nr['quant']=0;
				$nr['q_evasa']=0;
			}else{
				$nr['quant']	    = trim($row['RDQTA']);
				$nr['q_evasa']	    = trim($row['RDQTE']);
			}
			$nr['un_mis']	    = trim($row['RDUM']);
			$nr['prezzo']	    = (float)$row['RTPRZ'];
			$nr['depo']	    =  trim($row['RDDEPO']);
			if($nr['codice']=='*'){
				$nr['stato']= '';
			}else{
				$nr['stato']	    = $s->decod_std('STRIG', trim($row['RDSTAT']));
			}
			$nr['num_ord']	    = trim($row['RDNREC']);
			$nr['allegati']	    = trim($row['N_AL']);
			$nr['RDSTEV']	    = trim($row['RDSTEV']);
			
			if(trim($row['EMAIL_AO']) != '')
			    $nr['email'] = trim($row['EMAIL_AO']);
            else
                $nr['email'] = trim($row['EMAIL_ON']) ;
            
			$ar[] = $nr;

		}

		echo acs_je($ar);

		exit;
}

$permesso_modifica_prezzi = $main_module->permessi_modifiche_prezzi($m_params->k_ordine);


if ($_REQUEST['fn'] == 'open_grid'){
    
    $row   = $s->get_ordine_by_k_ordine($m_params->k_ordine);
    
    if(isset($m_params->disab)){
        
        $disab = $m_params->disab;
        $no_opz = 'Y';
        
    }else{
        
        if($auth->is_admin()){
            $disab = 'N';
        }else{

            $stato = trim($row['TDSTAT']);
            
            $sql = "SELECT TAFG02 FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
            WHERE TADT = '{$id_ditta_default}' AND TATAID = 'CHSTF' AND TAKEY1 = '{$stato}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            echo db2_stmt_errormsg();
            $row = db2_fetch_assoc($stmt);
            $disab = trim($row['TAFG02']);
        }
    }
     

?>


{"success":true, "items": [

			{
			xtype: 'grid',
	         loadMask: true,
	         
	         <?php 
	         
	         if($js_parameters->only_view != 1){
	         	if($no_opz != 'Y'){
	         		if($auth->is_admin() ||$m_params->art_manc != 4 || $m_params->art_manc != 3 || $m_params->art_manc != 2 ){ ?>	
				         plugins: [
					          Ext.create('Ext.grid.plugin.CellEditing', {
					            clicksToEdit: 1,
					               listeners: {
					              'beforeedit': function(e, eEv) {
					                 var rec = eEv.record;
					               		                
									if (rec.data.RDSTEV.trim() == 'N')		                
					                	return false;
					               	/*if (rec.data.codice.substring(0, 1)  != '*')		                
					                	return false;*/
					                
					              },
					              'edit': function(e) {
					                //this.isEditAllowed = false;
					              }
					            }
					          })
					      ],
		      <?php }}}?>
			   features: [
	
			{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
		}],
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							       },
							        
							        
							           extraParams: {
										 
										 k_ordine: <?php echo j($m_params->k_ordine) ?>
			        				},
			        				
			        			   doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root',
						            idProperty: 'order'						            
						        }
							},
							
		        			fields: ['codice', 'articolo', 'un_mis', 'riga', 'quant', 'q_evasa', 'depo',
		        			'prezzo', 'num_ord', 'stato', 'costo_pre', 'allegati', 'email', 'RDSTEV', 'TDFG04']							
									
			}, //store
					<?php $cf1 = "<img src=" . img_path("icone/48x48/currency_black_pound.png") . " height=20>"; ?>	
					<?php $cf2 = "<img src=" . img_path("icone/48x48/camera.png") . " height=20>"; ?>
			      columns: [	
			      
			       {
	                header   : 'numero',
	                dataIndex: 'num_ord',
	                hidden: true
	                },
			      {
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width: 50,
	                editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }
	                },
			      {
	                header   : 'Codice',
	                dataIndex: 'codice',
	                width: 120,               
          			filter: {type: 'string'}, 
	                filterable: true
	                },
	                {
	                header   : 'Descrizione beni',
	                dataIndex: 'articolo',
	                filter: {type: 'string'}, filterable: true,
	                flex: 1,
	            
	                editor: {
			                xtype: 'textfield',
			                allowBlank: true,
			                maxLength: 50
			            }
	                },
	               
	                {
	                header   : 'UM',
	                dataIndex: 'un_mis',
	                width: 50	             
	                },
	                {
	                header   : 'Quantit&agrave;',
	                dataIndex: 'quant',
	                renderer: floatRenderer2,
	                align: 'right',
	                width: 90,
	                 <?php if($permesso_modifica_prezzi === true && $disab != 'Y'){?>
	                editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }
			            
			          <?php }?>
	                },
	                {
	                header   : 'Evasa',
	                dataIndex: 'q_evasa',
	                renderer: floatRenderer2,
	                align: 'right',
	                width: 90
	                },
	                <?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>		                
	                {
	                header   : 'Prezzo',
	                dataIndex: 'prezzo',
	                renderer: floatRenderer2,
	                 align: 'right',
	                width: 60,
	                <?php if($permesso_modifica_prezzi === true && $disab != 'Y'){?>
	                editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }
			         <?php }?>
	                },
	                <?php }?>
	                 {
	                header   : 'Deposito',
	                dataIndex: 'depo',
	                width: 60
	                },
	                {
	                header   : 'Stato riga',
	                dataIndex: 'stato',
	                width: 100,
	                editor: {
			                xtype: 'combobox',
			                allowBlank: true,
			                valueField: 'id',
			                displayField: 'text',
			                	store: {
								fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('STRIG'), '') ?>	
								    ] 
							}	
			            }
	                },
	                
	                {text: '<?php echo $cf1; ?>',	
		                width: 30, tdCls: 'tdAction',
		                dataIndex: 'costo_pre', 
		        		tooltip: 'Costo preventivo',		        			    	     
		    			renderer: function(value, p, record){
		    			  if (record.get('costo_pre') != 0) return '<img src=<?php echo img_path("icone/48x48/currency_black_pound.png") ?> width=18>';
		    		  }},
		    		  
		    		    {text: '<?php echo $cf2; ?>', 	
        				width: 30,
        				align: 'center', 
        				dataIndex: 'allegati',
        				tooltip: 'Allegati',		        			    	     
        		    	renderer: function(value, metaData, record){
        		    			  if (record.get('allegati') > 0){
				    				 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode("[# " +record.get('allegati') + "]") + '"';			    	
					    	    		
					    	    	 return '<img src=<?php echo img_path("icone/48x48/camera.png") ?> width=15>';
					    	    
					    	     }	
        		    		 
        		    		  }
        		        }
	               
	           
	        
	         ] ,
	         
	        		listeners: {
	        		
	        			afterrender: function (comp) {
			 			
			 					
			 					comp.up('window').setTitle('Visualizza righe ordine  <?php echo "{$row['TDOADO']}_{$row['TDONDO']}_{$row['TDOTPD']} " ?>');	
			 										 	
								
			 			},
	        		
	        		<?php if($no_opz != 'Y'){ ?>
	        		
	        		itemcontextmenu : function(grid, rec, node, index, event) {	
								event.stopEvent();
								var grid = this;
			    				var rows = grid.getSelectionModel().getSelection()[0].data.num_ord;
			    				
								var voci_menu = [];
								
								
								<?php if($disab != 'Y'){ ?>
								
								if (
									parseFloat(rec.get('prezzo')) == 0 ||
									<?php if($permesso_modifica_prezzi === true){?>
									 1==1
									<?php } else { ?>
									 1==2
									<?php } ?>
								){
												
						            voci_menu.push({
							         		text: 'Cancella riga',
							        		iconCls : 'icon-sub_red_delete-16',          		
						        		     handler: function() {						       
					             
									             	Ext.Ajax.request({
												        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cancella_articoli',
												        timeout: 2400000,
												        method     : 'POST',
									        			jsonData: {
									        				rows : rec.data,
									        				k_ordine: <?php echo j($m_params->k_ordine) ?>
														},							        
												        success : function(result, request){
									            			grid.getStore().load();	
									            		   
									            		},
												        failure    : function(result, request){
												            Ext.Msg.alert('Message', 'No data to be loaded');
												        }
												    });
			        				
					            			}
							    	});
							    
							    }	
							     <?php }?>
							    
							    	
				           voci_menu.push({
				         		text: 'Assegna costo preventivo',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		     
				        		     
                                    if (rec.get('TDFG04').trim() != ''){ 
                                      acs_show_msg_error('Opzione non disponibile! (Margine ordine controllato!)');
                                      return false;
                                    }
				        		     
				        		    my_listeners = {
				        			    afterModArt: function(from_win){
				        				
		        						grid.getStore().load();
	 									from_win.close();
	 											
								        	}
										}
				        	
			            acs_show_win_std('Assegna/modifica costo preventivo', 'acs_righe_ordine.php?fn=form_mod_art', {row: rec.data, k_ordine: <?php echo j($m_params->k_ordine) ?>}, 500, 150, my_listeners, 'icon-pencil-16');          		
			       
			       
			            }
				    		});
				    		
				    		
				    		 voci_menu.push({
				         		text: 'Upload/abbina',
				        		iconCls : 'icon-search-16',          		
				        		     handler: function() {
				        		     
				        		   	acs_show_win_std('Upload/abbina', 'acs_upload.php?fn=open_form', {row: rec.data, from_righe : 'Y', k_ordine: <?php echo j($m_params->k_ordine) ?>} , 600, 350, {
         		            		afterUpload: function(from_win){
         		            			grid.store.load();
         		            			from_win.close()
             		            	}
         		            	}, 'icon-search-16');
			       
			       
			            }
				    		});
				    		
				    		   voci_menu.push({
					         		text: 'Invia e-mail',
					        		iconCls : 'icon-email_compose-16',          		
					        		     handler: function() {
						        		     
					        		    	 acs_show_win_std('Email Composer', 
							        		    	 '../base/acs_email_composer.php?fn=open_email', 
							        		    	 {
						        		    	 		row: rec.data, k_ordine: <?php echo j($m_params->k_ordine) ?>, from_righe : 'Y'
							        		    	 }, 
							        		    	 950, 500, null, 'icon-email_compose-16', 'Y');
								         }
					    		});

                  <?php
                  $js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
                  if ($js_parameters->mod_depo == 1){  	?>
                           if (rec.get('q_evasa') == 0){
	  						  voci_menu.push({
					         		text: 'Modifica deposito',
					        		iconCls : 'icon-pencil-16',          		
				        		    handler: function() {
					        		     
					        		     my_listeners = {
    				        			    afterConfirm: function(from_win){
    				        					grid.getStore().load();
    	 										from_win.close();
    	 											
    								        }
										 }
						        		     
				        		    	 acs_show_win_std('Seleziona deposito', 
						        		    	 'acs_modifica_deposito.php?fn=open_form', 
						        		    	 {
					        		    	 		row: rec.data, k_ordine: <?php echo j($m_params->k_ordine) ?>
						        		    	 }, 
						        		    	 400, 150, my_listeners, 'icon-pencil-16');
								         }
					    		});
							}
							
							<?php }?>
				  
						        var menu = new Ext.menu.Menu({
						            items: voci_menu
							    }).showAt(event.xy);	
					
						},
						
						<?php }?>
					
					celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					
					    if(rec.get('allegati') != '' && col_name == 'allegati')
            				acs_show_win_std('Allegati di riga ', 'acs_allegati_riga.php?fn=open_tab', {row: rec.data, k_ordine: <?php echo j($m_params->k_ordine) ?>}, 450, 400, null, 'icon-camera-16');
					   
					    	
	            	}
	          
	           }, 	beforeedit: function(editor, e, a, b){
							var grid = editor.grid;
							var record= e.record;	
							var col = e.column.dataIndex;
							if(col == 'articolo'){
								if(record.get('codice').substring(0, 1)  != '*')
								return false;
							}
						    
							
							 
            		}, //after edit	  
					
					} //listeners
				, viewConfig: {
		        getRowClass: function(rec, index) {
					if (rec.get('RDSTEV') == 'N') return 'rigaRevocata';
		         
		         }   
		    },	
			    
			     <?php if($js_parameters->only_view != 1  && $disab != 'Y'){ ?>
	         
	          dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-shopping_setup-32',
                     text: 'Elenco articoli',
			            handler: function() {
			            
			             var grid = this.up('grid');
			             
			             //salvare prima le modifiche delle righe per non perderle
			             var rows_modified = grid.getStore().getUpdatedRecords();
                         var list_rows_modified = [];
                            
                         for (var i=0; i<rows_modified.length; i++) {
			             list_rows_modified.push(rows_modified[i].data);}
			             
			             
			             Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_articoli',
						        method     : 'POST',
			        			jsonData: {
			        				mod_rows : list_rows_modified,
			        				k_ordine: <?php echo j($m_params->k_ordine) ?>
								},							        
						        success : function(result, request){
			            			console.log('salvataggio completato');			            		   
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			            
			            				my_listeners = {
				        					afterSelectArt: function(from_win, list_selected_art){
				        					
				        					    Ext.Ajax.request({
                        				        url        : 'acs_elenco_articoli.php?fn=exe_conferma_articoli',
                        				        method     : 'POST',
                        	        			jsonData: {
                        	        			    articoli : list_selected_art,
                        	        			    esclusi_sosp: 'Y',
                                                    k_ordine:  <?php echo j($m_params->k_ordine); ?>
                        						},							        
                        				        success : function(result, request){
                        				        	 jsonData = Ext.decode(result.responseText);
                        				        	 
                        				        	 if(jsonData.success == false && jsonData.message_cod =='art_sospeso'){
                    		            		   		acs_show_msg_error('Articolo inserito sospeso');
                    		            		   		return;
                    		            		     }
                        				        	 
                        				        	 if (!Ext.isEmpty(jsonData.message))
                        				        	  alert(jsonData.message);
                        				        	 else
                        				        	  alert('Inserimento riuscito');
                        				        	   
                        				             grid.getStore().load();                        				             
                        				             from_win.down('#form_add').getForm().reset(); 	
                        				        },
                        				        failure    : function(result, request){
                        				            Ext.Msg.alert('Message', 'No data to be loaded');
                        				        }
                        				    });	
				        					
								        	}
										}
										
							<?php if($m_params->from_ot == 'Y')
							         $prezzo_non_obb = true;
							     else
							         $prezzo_non_obb = false;
							?>
										
	        				acs_show_win_std('Visualizza articoli', '../base/acs_elenco_articoli_adv.php', {
	        					k_ordine: <?php echo j($m_params->k_ordine); ?>, 
	        					esclusi_sosp: 'Y',
	        					abilita_inserimento_in_ordine: true,
	        					prezzo_non_obb : <?php echo j($prezzo_non_obb); ?>,
	        				    ricerca_solo_fornitore : 'Y'
	        					}, 1100, 550, my_listeners, 'icon-shopping_setup-16');
			            }
			     },
			     
			     { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
						{
	                name   : 'f_codice',
	                id : 'Codice',
	                xtype : 'textfield',
	                flex: 1,
	                maxLength: 30,
	                fieldLabel: 'Codice',
	                labelAlign: 'right',
	                margin: '0 10 0 100'
	                
	                }, 
	                {
	                xtype: 'button',
	            	text: 'Aggiungi articolo',
	                handler: function() {
	                
	                var grid = this.up('grid');
	                cod = Ext.getCmp('Codice').getValue();
	                
	                //salvare prima le modifiche delle righe per non perderle
			             var rows_modified = grid.getStore().getUpdatedRecords();
                         var list_rows_modified = [];
                            
                         for (var i=0; i<rows_modified.length; i++) {
			             list_rows_modified.push(rows_modified[i].data);}
			             
			             
			             Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_articoli',
						        method     : 'POST',
			        			jsonData: {
			        				mod_rows : list_rows_modified,
			        				k_ordine: <?php echo j($m_params->k_ordine) ?>
								},							        
						        success : function(result, request){
			            			console.log('salvataggio completato');			            		   
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
	               
						Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_articolo',
					        method     : 'POST',
		        			jsonData: {
		        			    codice : cod,
		        			    quant : 1,
								prezzo : 0,
								articolo : '',
								esclusi_sosp: 'Y',
		        				k_ordine: <?php echo j($m_params->k_ordine) ?>
							},							        
					        success : function(result, request){
		            			
		            		   var jsonData = Ext.decode(result.responseText);
		            		   if(jsonData.success == true){
		            		     grid.getStore().load();
		            		     Ext.getCmp('Codice').setValue(''); 
		            		     return;
		            		     }
		            		    if(jsonData.success == false && jsonData.message_cod =='art_not_found'){
		            		   
		            		    my_listeners = {
			        					afterAddArt: function(from_win){
			        					    grid.getStore().load();	 
 											from_win.close();
 											
							        	}
								}
								
				<?php if($m_params->from_ot == 'Y')
				         $prezzo_non_obb = true;
				     else
				         $prezzo_non_obb = false;
				?>				
					
				my_listeners = {
    					afterSelectArt: function(from_win, list_selected_art){
    					
    					    Ext.Ajax.request({
    				        url        : 'acs_elenco_articoli.php?fn=exe_conferma_articoli',
    				        method     : 'POST',
    	        			jsonData: {
    	        			    articoli : list_selected_art,
    	        			    esclusi_sosp: 'Y',
                                k_ordine:  <?php echo j($m_params->k_ordine); ?>
    						},							        
    				        success : function(result, request){
    				        	 jsonData = Ext.decode(result.responseText);
    				        	 
    				        	 if(jsonData.success == false && jsonData.message_cod =='art_sospeso'){
		            		   		acs_show_msg_error('Articolo inserito sospeso');
		            		   		return;
		            		     }
    				        	 
    				        	 if (!Ext.isEmpty(jsonData.message))
    				        	  alert(jsonData.message);
    				        	 else
    				        	  alert('Inserimento riuscito');
    				        	   
    				             grid.getStore().load();                        				             
    				             from_win.down('#form_add').getForm().reset(); 	
    				        },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    });	
    					
			        	}
					}	
								
				acs_show_win_std('Visualizza articoli', '../base/acs_elenco_articoli_adv.php', {
	        					k_ordine: <?php echo j($m_params->k_ordine); ?>, 
	        					esclusi_sosp: 'Y',
	        					abilita_inserimento_in_ordine: true,
	        					prezzo_non_obb : <?php echo j($prezzo_non_obb); ?>,
	        					ricerca_solo_fornitore : 'Y'
	        					}, 900, 550, my_listeners, 'icon-shopping_setup-16');
		            		    }

		            		    
		            		    if(jsonData.success == false && jsonData.message_cod =='art_sospeso'){
		            		   		acs_show_msg_error('Articolo inserito sospeso');
		            		    }
		            		  
		            		   	
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
			            
			            
			            }
	                
	                }
						
						]
					 }, '->' 
					 
					 <?php if($m_params->from_ot == 'Y'){?>
					 
					 , {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-button_blue_play-32',
                     text: 'Duplica ordine cliente',
                     handler: function() {
			             
			             var window = this.up('grid').up('window');
			             var grid = this.up('grid');
			             acs_show_win_std('Ricerca ordini di vendita', '../desk_utility/acs_ricerca_ordini_gestionale.php?fn=ricerca_ordini_grid', {k_ordine: <?php echo j($m_params->k_ordine); ?>, from_ot : <?php echo j($m_params->from_ot); ?>}, 800, 450, null, 'icon-folder_search-16');
			             window.close();
			      		
			            }
			          }
					 
					 <?php }?>
					  
					 ,{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-save-32',
                     text: 'Salva',
                     itemId: 'bottom_save',
			         handler: function() {
			             
			             var window = this.up('grid').up('window');
			             var grid = this.up('grid');
			             
			              var rows_modified = grid.getStore().getUpdatedRecords();
                          var list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified.length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_articoli',
									        method     : 'POST',
						        			jsonData: {
						        				mod_rows : list_rows_modified,
						        				k_ordine: <?php echo j($m_params->k_ordine) ?>
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            			 window.close();
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
			            
			            		
	        				
			            }
			     }
			     
			     ]
		   }]
	   		<?php }?>
			
		}//grid
		 
			
		
 				
	
     ]
        
 }
 
 
 <?php 
 exit;
}


if ($_REQUEST['fn'] == 'form_mod_art'){
    
    $m_params = acs_m_params_json_decode();

    ?>
 	
 	 
  {"success":true, "items": [
         {
 		            xtype: 'form',
 		            //id: 'm_form',
 		            bodyStyle: 'padding: 10px',
 		            bodyPadding: '5 5 0',
 		            frame: false,
 		            //title: 'Modifica articolo',
 		            url: 'acs_op_exe.php',
 		            
 					buttons: [{
 			            text: 'Salva',
 			            iconCls: 'icon-button_blue_play-32',
 			            scale: 'large',
 			            handler: function() {
 			            	var form = this.up('form').getForm();
 			            	var form_values = form.getValues();
 							 var loc_win = this.up('window');
 							 
 						
 							  Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ass_costo_pre',
									        method     : 'POST',
						        			jsonData: {
						        				form_values: form.getValues(),
						        				k_ordine: '<?php echo $m_params->k_ordine; ?>'
						        				
											},							        
									        success : function(result, request){
									           loc_win.fireEvent('afterModArt', loc_win);		
						            	    								        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
 						
 			
 			            }
 			         }],   		            
 		            
 		            items: [   	 { 
 						xtype: 'fieldcontainer',
 						layout: { 	type: 'hbox',
 								    pack: 'start',
 								    align: 'stretch'},						
 						items: [
 								{						
									name: 'f_nrec',
									value: '<?php echo $m_params->row->num_ord?>',
									hidden: true,
									xtype: 'textfield',
									fieldLabel: 'num',
									readOnly: true
								 },
						        {						
									name: 'f_rig',
									xtype: 'displayfield',
									fieldLabel: 'Riga',
									value: '<?php echo $m_params->row->riga?>',
									labelWidth: 30,
									margin: '0 10 0 0'
								 },
 								 {						
 									name: 'f_c_art',
 									xtype: 'displayfield',
 									fieldLabel: 'Articolo',
 									value: '<?php echo $m_params->row->codice?>',
 									labelWidth: 60,
 									margin: '0 10 0 0'				
 								 },{						
 									name: 'f_d_art',
 									xtype: 'displayfield',
 									value: <?php echo j($m_params->row->articolo) ?>,
 												
 								 }
 						
 						    ]},{						
 									name: 'f_costo_pre',
 									xtype: 'numberfield',
 									decimalPrecision: 2,
 									fieldLabel: 'Costo preventivo di riga lordo',
 									value: '<?php echo $m_params->row->costo_pre?>',
 								   //anchor: "-180", 
 									flex: 1,
 									hideTrigger:true				
 								 }		
 					 
 										 
 		           ]
 		              }
 		
 			]
 }	
 	
 	
 	
 <?php
 exit;
}