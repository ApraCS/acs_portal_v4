<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'exe_registra'){

	$m_params = acs_m_params_json_decode();

	$use_session_history = microtime(true);
	$form_values=$m_params->form_values;
	
		$ri_msg = 'GEN_NOTACR';
		$sh = new SpedHistory();
		$sh->crea(
				'pers',
				array(
						"messaggio"	=> $ri_msg,
						"k_ordine"      => $m_params->k_ordine,
						"use_session_history" 	=> $use_session_history,
						"vals" => array(
							"RIART" 	=> '*',
							"RIDART" 	=> $form_values->f_note_1						
						),
				)
		);
		$sh = new SpedHistory();
		$sh->crea(
				'pers',
				array(
						"messaggio"	=> $ri_msg,
						"k_ordine"      => $m_params->k_ordine,
						"use_session_history" 	=> $use_session_history,
						"vals" => array(
								"RIART" 	=> '*',
								"RIDART" 	=> $form_values->f_note_2
						),
				)
		);
		$sh = new SpedHistory();
		$sh->crea(
				'pers',
				array(
						"messaggio"	=> $ri_msg,
						"k_ordine"      => $m_params->k_ordine,
						"use_session_history" 	=> $use_session_history,
						"vals" => array(
								"RIART" 	=> '*',
								"RIDART" 	=> $form_values->f_note_3
						),
				)
		);
		$sh = new SpedHistory();
		$sh->crea(
				'pers',
				array(
						"messaggio"	=> $ri_msg,
						"k_ordine"      => $m_params->k_ordine,
						"use_session_history" 	=> $use_session_history,
						"vals" => array(
								"RIART" 	=> '*',
								"RIDART" 	=> $form_values->f_note_4
						),
				)
		);
		$sh = new SpedHistory();
		$sh->crea(
				'pers',
				array(
						"messaggio"	=> $ri_msg,
						"k_ordine"      => $m_params->k_ordine,
						"use_session_history" 	=> $use_session_history,
						"vals" => array(
								"RIART" 	=> '*',
								"RIDART" 	=> $form_values->f_note_5
						),
				)
		);
		
		$sh = new SpedHistory();
		$sh->crea(
				'pers',
				array(
						"messaggio"				=> $ri_msg,
						"k_ordine"      		=> $m_params->k_ordine,
						"end_session_history" 	=> $use_session_history, //va in RINOTR
						"vals" => array(
								"RIDTEP" 		=> (int)$form_values->f_data,
								"RIIMPO"		=> (float)sql_f($form_values->f_importo),								
						),
				)
		);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'open_form'){
	$m_params = acs_m_params_json_decode();
	
	$initial_data_txt = $s->get_initial_calendar_date();

	
	?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            //url: 'acs_print_lista_consegne.php',
            
            items: [
				{
                	xtype: 'hidden',
                	name: 'filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                },
                
                {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , width: 250
						   , value: '<?php echo $initial_data_txt ?>'
			
						} , {
		                    xtype: 'numberfield',
		                    name: 'f_importo',
		                    hideTrigger:true,
		                    fieldLabel: 'Importo con IVA',
		                    width: 250
		                } ,
		            	 {						 
							 xtype: 'fieldset',
							 title: 'Note',
							 layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'
								   },		   					
							items: [
						 
								 {
										name: 'f_note_1',
										xtype: 'textfield',
										maxLength: 50,
									    anchor: '-15',	
									 },{
										name: 'f_note_2',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 },{
										name: 'f_note_3',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 },{
										name: 'f_note_4',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 },{
										name: 'f_note_5',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 }
							]
						}
            ],
            
			buttons: [					
				{
		            text: 'Genera NOTA STORNO ANTICIPO',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		            
		            var form=this.up('form').getForm();
		            var win=this.up('window');
		            var button = this;
			        button.disable();
		            if(form.isValid()){
		        
		                            Ext.getBody().mask('Loading... ', 'loading').show();
                                 	Ext.Ajax.request({
							        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_registra',
								        method     : 'POST',
					        			jsonData: {
					        				form_values: form.getValues(),
					        				k_ordine: <?php echo j($m_params->k_ordine) ?>
										},							        
								        success : function(result, request){
								        	Ext.getBody().unmask();
        	 						        win.close();									        
								        },
								        failure    : function(result, request){
								            Ext.getBody().unmask();
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
					    });	
	                  }
	             
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}
?>