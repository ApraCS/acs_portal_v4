<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
//$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

function ar_tipo_by_doc_type($doc_type){
    global $cfg_mod_DeskPVen;
    return $cfg_mod_DeskPVen['ar_tipo_by_doc_type'][$doc_type];
}

//DRY: anche in class DeskPVen
function get_doc_type_by_tipo($tipo){
   if (in_array($tipo, ar_tipo_by_doc_type('O')))
      return 'O'; //Ordine
   if (in_array($tipo, ar_tipo_by_doc_type('P')))
      return 'P'; //Ordine
}


?>


<html>
 <head>

  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
 
   .cella_gray{background-color: #D1CCBF;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 12px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}  
   
  table.intest td{border: 0px;} 
 
    
   tr.ag_liv_data th{background-color: #333333; color: white;}
   .grassetto th{font-weight: bold;}
    .box{width: 250px;
   		 height: 150;
   		 padding:10px;
   		 border: 1px solid;
   		 
   	  } 
   
    div#my_content h1{font-size: 22px; padding: 10px;}
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
   @media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
 @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	
	  
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'N';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 


		

<?php


$oe = $s->k_ordine_td_decode_xx($_REQUEST['k_ordine']);
$row= $s->get_ordine_by_k_docu($_REQUEST['k_ordine']);

$doc_type = get_doc_type_by_tipo($row['TDOTPD']);

$ar = array();

$sql_cli = "SELECT *
		FROM {$cfg_mod_Spedizioni['file_anag_cli']} {$join_riservatezza}
		WHERE CFCD= '{$row['TDCCON']}'
 		AND CFDT = '{$row['TDDT']}' AND CFTICF = 'C' AND CFFLG3 = ''
 	    ORDER BY CFRGS1, CFRGS2";


$stmt_cli = db2_prepare($conn, $sql_cli);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_cli);

$row_cli = db2_fetch_assoc($stmt_cli);

$ret_cli = array(
		"descr" 	=> acs_u8e(trim($row_cli['CFRGS1']) . " " . trim($row_cli['CFRGS2'])),
		"cod_cli" 	=> acs_u8e(trim($row_cli['CFCD'])),
		"out_loc"	=> acs_u8e(trim($row_cli['CFLOC1']) . " " . trim($row_cli['CFPROV'])),
		"out_ind"	=> acs_u8e(trim($row_cli['CFIND1']) . " " . trim($row_cli['CFIND2'])),
		"cap"		=> acs_u8e(trim($row_cli['CFCAP'])),
		"tel"		=> acs_u8e(trim($row_cli['CFTEL'])),
		"tel2"		=> acs_u8e(trim($row_cli['CFTEX'])),
		"p_iva"		=> acs_u8e(trim($row_cli['CFPIVA'])),
		"cod_fisc"  => acs_u8e(trim($row_cli['CFCDFI'])),
);


$ret = array(
		"descr" 	=> acs_u8e(trim($row['TDDDES'])),
		"out_loc"	=> acs_u8e(trim($row['TDDLOC']) . " " . trim($row['TDPROD'])),
		"out_ind"	=> acs_u8e(trim($row['TDIDES'])),
        "cap"	    => acs_u8e(trim($row['TDDCAP'])),
		"data_contratto"		=> acs_u8e(trim($row['TDODRE'])),
		"data_consegna"		    => acs_u8e(trim($row['TDDTEP'])),
		"data_consegna_con"		=> acs_u8e(trim($row['TDODER'])),
		"numero"		        => (int)trim($row['TDONDO']),
	    "email"                 => acs_u8e(trim($row['TDMAIL'])),
	    "refe"                  => acs_u8e(trim($row['TDDORE'])),
		"rif"                   => acs_u8e(trim($row['TDVSRF'])),
		"stab"                  => trim($row['TDSTAB']),
		

);

?>

<table class='intest'>
<tr> <td style='width: 50%'><img src=<?php echo logo_path(); ?> height=70></td>
<td> 
<p>Spett. </p>
<hr size=1px;>
<p><b><?php echo $ret_cli['descr']; ?></b></p>
<p> <?php echo $ret_cli['out_ind']; ?> </p>
<p> <?php echo $ret_cli['cap'] ." ".$ret_cli['out_loc']; ?> </p>


<br>
</td>
</tr>

<tr>
<td VALIGN="bottom">

Punto vendita <?php echo $s->decod_std('START', trim($ret['stab'])); ?>
<br>
<br>
<b> <?php echo $doc_type=='P' ? 'Proposta': $_REQUEST['tipo_stampa'] != 'SCHEDA' ? 'Contratto' : 'Ordine' ?></b>



numero <span style= 'font-weight: bold; font-size: 14px;'><?php echo $ret['numero']; ?>
</span> del <b><?php echo print_date($ret['data_contratto']); ?></b> 
<br> Referente: <?php echo $ret['refe']; ?>
<br> Cod. cliente: <?php echo $ret_cli['cod_cli']; ?>
</td>

<td>
<p>Destinazione</p>
<hr size=1px;>
<p><?php echo $ret['descr']; ?>  </p>
<p><?php echo $ret['out_ind']; ?> </p>
<p> <?php echo $ret['cap'] ." ".$ret['out_loc']; ?> </p>
</td>

</tr>
</table>

<?php 

echo "<br>";

echo "<table>";

	
	echo "<tr>";
	if($ret_cli['p_iva'] != 0){
	
		echo "<td>Codice Fiscale ".$ret_cli['cod_fisc']." P.IVA ".$ret_cli['p_iva']."</td>";
	
	}else{
	
		echo "<td>&nbsp;Codice Fiscale ".$ret_cli['cod_fisc']."</td>";
	}
	
	echo "<td style= 'width: 50%;'>Riferimento <b>".$ret['rif']."</b></td>";
	echo "</tr>";
	echo "<tr>";
	if(trim($ret_cli['tel2']) == ''){
		echo "<td>Telefono ".$ret_cli['tel']."</td>";
	}else{
		echo "<td>Telefono ".$ret_cli['tel']." - ".$ret_cli['tel2']."</td>";
	}
	echo "<td>Email ".$ret['email']."</td>";
	echo "</tr>";

echo "</table>";

?>

	<h2 style = 'text-align : center;'>VERBALE DI VERIFICA</h2>
	<br>
	<div style='width:100%; height:40%; border:1px solid black;'></div>	
	<br>
	<p><b>NOTE: </b>  <span style='color:gray;'>___________________________________________________________________________________________________________  </span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________ </span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________</span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________</span></p>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>		
	<p style = 'text-align : center; width : 30%'>  FIRMA MONTATORE </p>
	    <br/>&nbsp;<br/>
	<p>  _________________________________ </p>




