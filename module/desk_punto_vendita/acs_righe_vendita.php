<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$m_params = acs_m_params_json_decode();
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
$mod_js_parameters = $main_module->get_mod_parameters();



//****************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    //****************************************************
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_json_data($m_params)
    ));
    exit;
}



//****************************************************
// WIN PRINCIPALE
//****************************************************
//****************************************************
if ($_REQUEST['fn'] == 'open_grid'){
    //****************************************************
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _righe_vendita_main_win($m_params)
    ));
    exit;
}

function _righe_vendita_main_win($m_params){
    $c = new Extjs_compo('grid');
    $c->set(array(
        'multiSelect' => false,
        'autoScroll' => true,
        'flex' => 1,
        'columns'     => array(
              grid_column_h('Ordine', 'ordine',  'w80', 'Ordine di vendita')
            , grid_column_h_date('Data', 'data_reg', null, 'Data registrazione')
            , grid_column_h('Tp', 'tipo', 'w40', 'Tipo ordine',
                array(
                    'renderer' => extjs_code("
                        function(value, metaData, record){
                            if (record.get('qtip_tipo') != '')
	    	                  metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '\"';
                        
                            return value;
                            } ")
                )
                )
            , grid_column_h('St', 'stato', 'w40', 'Stato ordine',
                array(
                    'renderer' => extjs_code("
                        function(value, metaData, record){
                            if (record.get('qtip_stato') != '')	   
	    	                  metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(record.get('qtip_stato')) + '\"';	    			    				
	    		         
                            return value;
                            } ")
                     )
                 )
            , grid_column_h('Deposito', 'deposito', 'f1')
            , grid_column_h('Cliente', 'cliente', 'f1')
            , grid_column_h('UM', 'un_mis', 'w40', 'Unit&agrave; di misura')
            , grid_column_h_f2('Qta', 'quant', 'w60', 'Quantit&agrave;')
            , grid_column_h_date('Data evas.', 'data_ev', null, 'Data evasione')
           
        ),
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('k_ordine', 'ordine', 'codice', 'articolo', 'un_mis', 'quant', 'deposito', 'stato', 'qtip_stato', 'tipo', 'qtip_tipo', 'cliente', 'data_reg', 'data_ev'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_json_data'), $m_params) ),
       
    ));
    return $c->code();
}

/* ------------------------------------------------------------- */
function _get_json_data($m_params){
    /* ------------------------------------------------------------- */
    global $conn, $id_ditta_default, $cfg_mod_DeskPVen, $cfg_mod_Spedizioni, $s;
    $c_art = $m_params->c_art;
    $ar = array();
    
    
    $sql = "SELECT RD.*, TD.*
    FROM {$cfg_mod_DeskPVen['file_righe_doc']} RD
    INNER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
      ON TD.TDDT = RD.RDDT AND TD.TDOTID = RD.RDTIDO AND TD.TDOINU = RD.RDINUM AND TD.TDOADO = RD.RDAADO AND TD.TDONDO = RD.RDNRDO  
    INNER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA
      ON TD.TDDT = TA.TADT AND TATAID = 'CHSTF' AND TD.TDSTAT = TA.TAKEY1 AND TAFG03 = 'Y' 
    WHERE TDDT = '{$id_ditta_default}' AND RDART = '{$c_art}' AND RDSTEV <>'S' AND RDQTA > RDQTE
    AND TDOTID = 'VO' AND TDOTPD <> 'MP'
    ORDER BY TDDTEP";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        
        if(trim($row['RDAADO']) != 0){
            $nr['k_ordine'] = implode("_", array($row['RDDT'], $row['RDTIDO'], $row['RDINUM'], $row['RDAADO'], sprintf("%06s", $row['RDNRDO'])));
            $nr['ordine']	= implode("_", array($row['RDAADO'], sprintf("%06s", $row['RDNRDO'])));
        }
        
        $nr['codice']	    = trim($row['RDART']);
        $nr['articolo']	    = utf8_encode(trim($row['RDDART']));
        $nr['un_mis']	    = trim($row['RDUM']);
        if($row['RDQTA'] >= 0)
            $nr['quant']	= trim($row['RDQTA']);
        $ta_sys = find_TA_sys('MUFD', trim($row['RDDEPO']));
        $nr['deposito']	    = "[".trim($row['RDDEPO'])."] ".$ta_sys[0]['text'];
        $nr['stato']	    = trim($row['TDSTAT']);
        $nr['qtip_stato']   = trim($row['TDDSST']);
        $nr['cliente']	    = trim($row['TDDCON']);
        $nr['data_reg']	    = trim($row['TDODRE']);
        $nr['data_ev']	    = trim($row['TDDTEP']);
        $nr['tipo']	        = trim($row['TDOTPD']);
        $nr['qtip_tipo']	= trim($row['TDDOTD']);
        
        $ar[] = $nr;
        
    }
    return grid_to_json_data($ar);
}

