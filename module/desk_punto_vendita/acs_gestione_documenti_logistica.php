<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'exe_canc_detail'){
    
    $m_params = acs_m_params_json_decode();
 
    
    $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_tabelle']} TA WHERE RRN(TA) = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->RRN));
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}





if ($_REQUEST['fn'] == 'exe_inserisci_causale'){

	$m_params = acs_m_params_json_decode();

	$codice= $m_params->form_values->f_codice;

	$descr= $m_params->form_values->f_descr;

	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'DCLOG';
	$ar_ins['TAKEY1'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();


	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}


if ($_REQUEST['fn'] == 'exe_modifica_causale'){

	$m_params = acs_m_params_json_decode();

	$mod_rows= $m_params->mod_rows;

	foreach ($mod_rows as $v){

		if(isset($v)){
				
			$ar_ins = array();

			$ar_ins['TAKEY1'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TARIF1'] 	= trim($v->msg_ri);
			$ar_ins['TASTAL'] 	= trim($v->cod_int);
			$ar_ins['TACOGE'] 	= trim($v->int_std);
			$ar_ins['TAKEY2'] 	= trim($v->sede);
				
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} TA
			SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			WHERE RRN(TA) = '$v->rrn' ";
				
				

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
				
			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);
				
			//exit;

		}

	}

	exit;
}


if ($_REQUEST['fn'] == 'get_data_grid'){


	$sql = "SELECT RRN (TA) AS RRN, TA.*
	       FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
	       WHERE TATAID='DCLOG' AND TADT='{$id_ditta_default}'";

	/*print_r($sql);
	 exit;*/

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['codice'] = acs_u8e(trim($row['TAKEY1']));
		$row['sede'] = acs_u8e(trim($row['TAKEY2']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['msg_ri'] = acs_u8e(trim($row['TARIF1']));
		$row['int_std'] = acs_u8e(trim($row['TACOGE']));
		$row['cod_int'] = acs_u8e(trim($row['TASTAL']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));


		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}


if ($_REQUEST['fn'] == 'open_grid'){

	?>

				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'Gestione causali DDT',
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],

					store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'codice', 'descr', 'rrn', 'msg_ri', 'int_std', 'cod_int', 'sede'
		        			]
		    			},
		    		
			        columns: [
			       				        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             }, { 
			                header   : 'Sede',
			                dataIndex: 'sede', 
			                tooltip: '(TAKEY2)',
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             },
			             { 
			                header   : 'Tipo intestatario',
			                tooltip: '(TASTAL) F/C',
			                dataIndex: 'cod_int', 
			                flex:1,
			                editor: {
			                  xtype: 'textfield',
			                   allowBlank: true
			           	  }
			             }, { 
			                header   : 'Intestatario default',
			                tooltip: 'TACOGE',
			                dataIndex: 'int_std', 
			                flex:1,
			                editor: {
			                  xtype: 'textfield',
			                   allowBlank: true
			           	  }
			             },
			             { 
			                header   : 'Messaggio RI',
			                tooltip: 'TARIF1',
			                dataIndex: 'msg_ri', 
			                flex:1,
			                editor: {
			                  xtype: 'textfield',
			                   allowBlank: true
			           	  }
			             }
			            
			            
			         ],  
					
						listeners: {
						
						
						/*itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
				
			           voci_menu.push({
				         		text: 'Stati associati',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Stati consentiti', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_detail', {statof: rec.get('codice')}, 900, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}*/
					
					}, 
					
					 viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
					            console.log(m_grid);
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_causale',
											        method     : 'POST',
								        			jsonData: {
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												      m_grid.getStore().load();
												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
					   ]
					}, '->' ,
			     
			     {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			                console.log(list_rows_modified);
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_causale',
									        method     : 'POST',
						        			jsonData: {
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php 

exit; 

}

