<?php

function sum_columns_value(&$ar_r, $r, $form_values){
    
       
	$ar_r['importo_ord'] += $r['IMP_ORD'];
	$ar_r['importo_forn'] += $r['IMP_FORN'] + $r['IMP_FINANZIAMENTI'];
	$ar_r['importo_forn_pre'] += $r['IMP_FORN_PRE_RD'];
	$ar_r['importo_IVA'] += $r['IMP_IVA'];
	

	
	//se tutti gli ordini sono spuntanti.... passo la spunta a livello superiore
	if (strlen($r['TDFG04']) == '')
	    $record_spunta = 'N';
	else
	    $record_spunta = $r['TDFG04'];
	
	if (!isset($ar_r['f_spunta'])) 
	    $ar_r['f_spunta'] = $r['TDFG04'];
	    
	$ar_r['f_spunta'] = min($ar_r['f_spunta'], $record_spunta);
	
	if($ar_r['liv']=='liv_2' || $ar_r['liv']=='liv_3'){
    	if (strlen($r['TDISON']) == '')
    	    $record_mar = 'N';
        else
            $record_mar = $r['TDISON'];
            
       if (!isset($ar_r['s_causale']))
           $ar_r['s_causale'] = $r['TDISON'];
            
       $ar_r['s_causale'] = min($ar_r['s_causale'], $record_mar);
	}
	
	//scostamento livello ordine
	/*if($ar_r['liv'] == 'liv_3' && $ar_r['importo_forn_pre'] > 0){
	    $ar_r['scostamento'] = ($ar_r['importo_forn'] - $ar_r['importo_forn_pre'])/$ar_r['importo_forn_pre']*100;
	}*/

	//al momento il campo IVA non viene gestita (quindi TDTIVA = 0) quindi la scorporo io
	$ar_r['margine'] = $ar_r['importo_ord']*100/122 - $ar_r['importo_forn'];
	//$ar_r['margine'] = $ar_r['importo_ord'] - $ar_r['importo_forn'] - $ar_r['importo_IVA'];

	$ar_r['S_RDFG04_Y'] += $r['S_RDFG04_Y'];	
	
	
	if($ar_r['importo_forn']>0){
	    $perc = (100 - $form_values->f_trasp)/100;
	    $ar_r['coeff'] = ($ar_r['importo_ord'] * $perc)  / $ar_r['importo_forn'];
	}
	
	if($ar_r['importo_forn_pre']>0){
	    $perc = (100 - $form_values->f_trasp)/100;
	    $ar_r['coeff_pre'] =  ($ar_r['importo_ord'] * $perc) / $ar_r['importo_forn_pre'];
	    
	}
}


function crea_ar_tree_margini($node, $form_values, $escludi_icone = 'N', $from_report = 'N', $ragg_causale = 'N'){
	global $cfg_mod_DeskPVen, $cfg_mod_Spedizioni, $conn, $id_ditta_default;
	
	$s = new Spedizioni(array('no_verify' => 'Y'));

	$ar = array();
	$ret = array();
	$stato = array();

		 
    	if($cfg_mod_DeskPVen["RTPUNT_sottocampato_da"] != 0){
    	    $rtpunt = " case when trim(substr(rtfil1, ".$cfg_mod_DeskPVen["RTPUNT_sottocampato_da"].", 15))='' then 0 else
                    dec(substr(rtfil1, ".$cfg_mod_DeskPVen["RTPUNT_sottocampato_da"].", 15)) / 100000 end ";
    	}else{
    	    $rtpunt = "RTPUNT";
    	}
	
    	if($cfg_mod_DeskPVen["RTMOLT_sottocampato_da"] != 0){
    	    $rtmolt = " case when trim(substr(rtfil1, ".$cfg_mod_DeskPVen["RTMOLT_sottocampato_da"].", 12))='' then 0 else
                    dec(substr(rtfil1, ".$cfg_mod_DeskPVen["RTMOLT_sottocampato_da"].", 12)) / 100000 end ";
    	}else{
    	    $rtmolt = "RTMOLT";
    	}
    	
	
	   $info_importo_field = $cfg_mod_DeskPVen['info_importo_field'];

	   $sql_where .= " WHERE  1=1 and TD.TDOTPD = 'MO' AND TD.TDSWPP='Y' AND TD.TDDT='$id_ditta_default'";
		
	    $sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values->f_cliente_cod);
		$sql_where.= sql_where_by_combo_value('TD.TDSTAB', $form_values->f_sede);
		$sql_where.= sql_where_by_combo_value('TD.TDCORE', $form_values->f_referente);
		$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);
		$sql_where.= sql_where_by_combo_value('TD.TDISON', $form_values->f_causale);
			
		//controllo data ordine reg
		if (strlen($form_values->f_data_da) > 0)
			$sql_where .= " AND TD.TDODRE >= {$form_values->f_data_da}";
		if (strlen($form_values->f_data_a) > 0)
			$sql_where .= " AND TD.TDODRE <= {$form_values->f_data_a}";
		

		//controllo data fattura
		if (strlen($form_values->f_data_fattura_da) > 0)
			$sql_where .= " AND TD.TDDTFA >= {$form_values->f_data_fattura_da}";
		if (strlen($form_values->f_data_fattura_a) > 0)
			$sql_where .= " AND TD.TDDTFA <= {$form_values->f_data_fattura_a}";
		
		if ($form_values->f_con_costo_preventivo == 'Y')
		    $sql_where .= " AND TD.TDFN20 = 1";
		if ($form_values->f_con_costo_preventivo == 'N')
		    $sql_where .= " AND TD.TDFN20 = 0";
						
		if($form_values->f_margine == 'Y')
		    $sql_where .= " AND TD.TDFG04 = 'Y'";
	    if($form_values->f_margine == 'Z')
	        $sql_where .= " AND TD.TDFG04 = 'Z'";
        if($form_values->f_margine == 'V')
            $sql_where .= " AND TD.TDFG04 NOT IN ('Y', 'Z')";
	       
        if(isset($form_values->f_todo) && strlen($form_values->f_todo) > 0){
           $join = "INNER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} ATT_OPEN
                    ON ATT_OPEN.ASDOCU = TD.TDDOCU";
            
           $sql_where .= " AND ASCAAS = '{$form_values->f_todo}' AND ASFLRI <> 'Y'";
         
        }else{
            $join = "";
        }   
			
		//creo struttura sede/cliente con importo ordini
		$sql= "SELECT TD.TDSTAB, TD.TDCORE, TD.TDCCON, TD.TDDCON, TD.TDDOCU, TD.TDODRE, TD.TDFN20, TD.TDSTAT, TD.TDOADO, TD.TDONDO, TD.TDOTPD,
				TDA2.C_ROW_ASS AS ROW_A, TDA2.DATA_ASS AS DATA_A, TDA2.TDSTAT AS ST_ASS, TD.TDFMTO, TD.TDDTCF, TD.TDMTOO, TD.TDDTFA,
                TD.TDFN11, TD.TDFN19, TD.TDSWSP, TD.TDFN06, TD.TDISON,
				SUM(RD.RDQTA3) AS IMP_FORN, SUM(RDF.RDQTA3_FINANZIAMENTI) AS IMP_FINANZIAMENTI, 
                SUM($info_importo_field) AS IMP_ORD, SUM(TD.TDTIVA) AS IMP_IVA, SUM(TDA2.RDQTA3) AS IMP_FORN_A,
				SUM(RD.S_RDFG04_Y) AS S_RDFG04_Y, TD.TDFG04, SUM(RD.RDCSPV) AS IMP_FORN_PRE, SUM(RDCP.S_RTMOLT) AS IMP_FORN_PRE_RD
				FROM {$cfg_mod_Spedizioni['file_testate']} TD

                    /* Outer Join con righe MTO */ 
					LEFT OUTER JOIN (
						SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA3) AS RDQTA3, SUM(RDCSPV) AS RDCSPV,
								SUM(CASE WHEN RDFG04='Y' THEN 1 ELSE 0 END) AS S_RDFG04_Y
						FROM {$cfg_mod_Spedizioni['file_righe']} RDO
						WHERE RDO.RDTPNO='MTO' 
						GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO) RD
					ON TD.TDDT=RD.RDDT AND TDOTID=RD.RDOTID AND TDOINU=RD.RDOINU AND TDOADO=RD.RDOADO AND TDONDO=RD.RDONDO

                    /* Outer Join con righe SPECO per costo finanziamenti  */ 
					LEFT OUTER JOIN (
						SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA3) AS RDQTA3_FINANZIAMENTI								
						FROM {$cfg_mod_Spedizioni['file_righe']}
						WHERE RDTPNO='SPECO' 
						GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO) RDF
					ON TD.TDDT=RDF.RDDT AND TDOTID=RDF.RDOTID AND TDOINU=RDF.RDOINU AND TDOADO=RDF.RDOADO AND TDONDO=RDF.RDONDO
					
                    /* Outer Join con RD/RT gest per recupero costo preventivo */
                    LEFT OUTER JOIN (
                        SELECT RDCPT.RDDT, RDCPT.RDTIDO, RDCPT.RDINUM, RDCPT.RDAADO, RDCPT.RDNRDO, SUM($rtpunt) AS S_RTPUNT, SUM($rtmolt) AS S_RTMOLT
                        FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RDCPT
        	 			LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RTCPT
        	    			ON RTCPT.RTDT=RDCPT.RDDT AND RDCPT.RDTIDO = RTCPT.RTTIDO AND RDCPT.RDINUM = RTCPT.RTINUM AND RDCPT.RDAADO = RTCPT.RTAADO AND RDCPT.RDNRDO = RTCPT.RTNRDO AND RDCPT.RDNREC=RTCPT.RTNREC AND RTCPT.RTVALU='EUR'								
        				WHERE RDCPT.RDART NOT IN('ACCONTO', 'CAPARRA') 
        				AND RDCPT.RDTISR = '' AND RDCPT.RDSRIG = 0
                        GROUP BY RDCPT.RDDT, RDCPT.RDTIDO, RDCPT.RDINUM, RDCPT.RDAADO, RDCPT.RDNRDO
                    ) RDCP
                    ON RDCP.RDDT = TD.TDDT AND RDCP.RDTIDO = TD.TDOTID AND RDCP.RDINUM = TD.TDOINU AND RDCP.RDAADO = TD.TDOADO AND RDCP.RDNRDO = TD.TDONDO


                    /* Outer Join con testate/righe ordini di assistenza relativi */
					LEFT OUTER JOIN (
						SELECT TDA.TDDT, TDA.TDABBI, MAX(TDSTAT) AS TDSTAT, COUNT(*) AS C_ROW_ASS, MAX(TDA.TDDTEP) AS DATA_ASS, SUM(RD_A.RDQTA3) AS RDQTA3
						FROM {$cfg_mod_Spedizioni['file_testate']} TDA
							LEFT OUTER JOIN (
								SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA3) AS RDQTA3
								FROM {$cfg_mod_Spedizioni['file_righe']} RDO_A
								WHERE RDO_A.RDTPNO='MTO' 
								GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO) RD_A
							ON TDA.TDDT=RD_A.RDDT AND TDA.TDOTID=RD_A.RDOTID AND TDA.TDOINU=RD_A.RDOINU AND TDA.TDOADO=RD_A.RDOADO AND TDA.TDONDO=RD_A.RDONDO
						WHERE TDA.TDCLOR='A' 
						GROUP BY TDA.TDDT, TDA.TDABBI) TDA2 
					ON TDA2.TDDT=TD.TDDT AND TDA2.TDABBI = CONCAT(TD.TDOADO, CONCAT('.', CONCAT(TRIM(TD.TDONDO), CONCAT('_', TD.TDOTPD))))					
					{$join}
					{$sql_where}
					
                    GROUP BY TD.TDDTFA, TD.TDFMTO, TD.TDDTCF, TD.TDMTOO, TDA2.C_ROW_ASS,TDA2.DATA_ASS,
					TDA2.TDSTAT, TD.TDSTAB, TD.TDCORE, TD.TDCCON, TD.TDDCON, TD.TDDOCU, TD.TDODRE, TD.TDFN20, TD.TDSTAT, 
					TD.TDOADO, TD.TDONDO, TD.TDOTPD, TD.TDFG04, TD.TDFN11, TD.TDFN19, TD.TDSWSP, TD.TDFN06, TD.TDISON
				    ORDER BY TD.TDSTAB, TD.TDCORE, TD.TDDCON, TD.TDCCON";
					
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		
		$sql_a = "SELECT TDSTAT FROM {$cfg_mod_Spedizioni['file_testate']} TD
				WHERE TDDT={$id_ditta_default} AND TDSWPP='Y' AND TDCLOR = 'A' AND TDABBI = ?";
		
		$stmt_a = db2_prepare($conn, $sql_a);
		echo db2_stmt_errormsg();
		$count_ordine = 0;
	    while ($row = db2_fetch_assoc($stmt)) {

			$tmp_ar_id = array();
			$ar_r= &$ar;
			//stacco dei livelli
			$cli_liv_tot = 'TOTALE';			//totale
			
			if($ragg_causale == 'Y'){
			    
			    $cod_liv0 = trim($row['TDISON']); 	//CAUSALE MARGINALITA'
			    if($from_report == 'Y')
			        $cod_liv0_1 = implode("_", array(trim($row['TDSTAB']), trim($row['TDCORE']))); //REFERENTE_PUNTO VENDITA
			    
			}else{
			    
			    $cod_liv0 = trim($row['TDSTAB']); 	//sede
			    if($from_report == 'Y')
			        $cod_liv0_1 = trim($row['TDCORE']); //REFERENTE
			    
			}
			
			$cod_liv1 = trim($row['TDCCON']); 	//codice cliente
			$cod_liv2 = trim($row['TDDOCU']);   //ORDINE
			 
			//LIVELLO TOTALE
			$liv = $cod_liv_tot;
			if (!isset($ar_r["{$liv}"])){
				$ar_new = array();
				$ar_new['id'] = 'liv_totale';
				$ar_new['liv_cod'] = 'TOTALE';
				$ar_new['task'] = 'Totale';
				$ar_new['liv'] = 'liv_totale';
				$ar_new['expanded'] = true;
				$ar_r["{$liv}"] = $ar_new;
			}
			
			if ((int)$row['ROW_A'] > 0)
				$ar_r[$liv]['nr_ass']+=$row['ROW_A'];
			
			$ar_r = &$ar_r["{$liv}"];
			sum_columns_value($ar_r, $row, $form_values);
			 
			//creo la sede
			$liv=$cod_liv0;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if(!isset($ar_r[$liv])){
				$ar_new= array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['codice']=$liv;
				if($ragg_causale == 'Y')
				    $ar_new['task'] = $s->decod_std('CMAR', trim($row['TDISON']));
				else
				    $ar_new['task'] = $s->decod_std('START', trim($row['TDSTAB']));
				$ar_new['liv'] = 'liv_1';
				$ar_r[$liv]=$ar_new;
			}
			
			if ((int)$row['ROW_A'] > 0)
				$ar_r[$liv]['nr_ass']+=$row['ROW_A'];
			 
			$ar_r=&$ar_r[$liv];
			sum_columns_value($ar_r, $row, $form_values);
			
			
			if($from_report == 'Y'){
			//creo referente
			$liv=$cod_liv0_1;
			$ar_r = &$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if(!isset($ar_r[$liv])){
			    $ar_new= array();
			    $ar_new['id'] = implode("|", $tmp_ar_id);
			    $ar_new['codice']=$liv;
			   
			    $ta_sys =  get_TA_sys_by_code('BREF', trim($row['TDCORE']));
			    $pv = $s->decod_std('START', trim($row['TDSTAB']));
			    
			    if($ragg_causale == 'Y')
			        $ar_new['task'] = $pv ." - ". $ta_sys['TADESC'];
		        else
		            $ar_new['task'] = $ta_sys['TADESC'];
			    
			    $ar_new['liv'] = 'liv_1_1';
			    $ar_r[$liv]=$ar_new;
			}
			
			if ((int)$row['ROW_A'] > 0)
			    $ar_r[$liv]['nr_ass']+=$row['ROW_A'];
			    
			    $ar_r=&$ar_r[$liv];
			    sum_columns_value($ar_r, $row, $form_values);
			}
			 
			//creo cliente
			$liv=$cod_liv1;
			$ar_r=&$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if(!isset($ar_r[$liv])){
				$ar_new= array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['codice']=$liv;
				///$ar_new['task'] = trim($row['TDDCON']);
				$ar_new['liv'] = 'liv_2';
				$ar_new['ord'] = $row['TDOADO'].".".trim($row['TDONDO'])."_".$row['TDOTPD'];
				$result = db2_execute($stmt_a, array($ar_new['ord']));
				$ar_st = array();
				while($row_a = db2_fetch_assoc($stmt_a))
					$ar_st[$row_a['TDSTAT']]++;				
				
				$ar_new['st_ass'] = out_list_value_from_array($ar_st);
				$ar_new['data_ass']= max($ar_new['data_ass'], $row['DATA_A']);
				$ar_r[$liv]=$ar_new;
			}
			$t_ar_liv_cli = &$ar_r[$liv];  //memorizzo puntatore a livello cliente
			if ((int)$row['ROW_A'] > 0)
				$ar_r[$liv]['nr_ass']+=$row['ROW_A'];
			
			$ar_r[$liv]['liv_nr_ord']+=1;
			$ar_r[$liv]['liv_nr_ord_costo_prev']+=$row['TDFN20'];
			
			$ar_r[$liv]['task'] = trim($row['TDDCON']);
			if ($escludi_icone != 'Y'){
    			if ($ar_r[$liv]['liv_nr_ord_costo_prev'] == $ar_r[$liv]['liv_nr_ord']) //tutti costi prev caricati
    			    $ar_r[$liv]['task'] .= '<span style="display: inline; float: right;"><img class="cell-img" src=' . img_path("icone/16x16/currency_black_pound.png") . '></span>';
    			else if ($ar_r[$liv]['liv_nr_ord_costo_prev'] > 0) //alcuni costi prev caricati
    			    $ar_r[$liv]['task'] .= '<span style="display: inline; float: right;"><img class="cell-img" src=' . img_path("icone/16x16/currency_grey_pound.png") . '></span>';
			}			
			
			$ar_r=&$ar_r[$liv];
			sum_columns_value($ar_r, $row, $form_values);
			
			
			//ordine
			$liv=$cod_liv2;
			$ar_r=&$ar_r['children'];
			$tmp_ar_id[] = $liv;
			if(!isset($ar_r[$liv])){
				$ar_new= array();
				$ar_new['id'] = implode("|", $tmp_ar_id);
				$ar_new['codice']=$liv;
				$ar_new['k_ordine'] = $liv;
				
				$ar_new['iconCls']= $s->get_iconCls(3, $row);  //fl_cons_conf = TDFN06
				$ar_new['task'] = implode('_', array($row['TDOADO'], $row['TDONDO'], $row['TDOTPD']));
				
				$ar_new['liv_nr_ord_costo_prev'] = $row['TDFN20'];
								
				if ($escludi_icone != 'Y'){
				
				$ha_commenti = $s->has_commento_ordine_by_k_ordine($ar_new['k_ordine']);
				if ($ha_commenti == FALSE)
				     $img_com_name = "icone/16x16/comment_light.png";
				else
				     $img_com_name = "icone/16x16/comment_edit_yellow.png";
				            
				    $ar_new['task'] .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_ordine_bl(\'' . $ar_new['k_ordine'] . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
				
				
			    if (trim($row['TDFN20']) == 1)
			        $ar_new['task'] .= '<span style="display: inline; float: right;"><img class="cell-img" src=' . img_path("icone/16x16/currency_black_pound.png") . '></span>';
				    
		       
		        //icona entry (per attivita' in corso sull'ordine)
		        $sa = new SpedAssegnazioneOrdini();
		        if ($sa->ha_entry_aperte_per_ordine($ar_new['k_ordine'])){
		            $img_entry_name = "icone/16x16/arrivi_gray.png";
		            $ar_new['task'] .= '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><a href="javascript:show_win_entry_ordine(\'' . $ar_new['k_ordine'] . '\', \'' . $cod_liv0 . '\', \'' . $cod_liv1 . '\')";><img class="cell-img" src=' . img_path($img_entry_name) . '></a> </span>';
		        }	
		    
			
				}
				
				$ar_new['comm']  = has_commento_ordine_profit($ar_new['k_ordine'], 'PRO');
				$ar_new['ord'] = $row['TDOADO']."_".trim($row['TDONDO'])."_".$row['TDOTPD'];
				$ar_new['nr_ass']=$row['ROW_A'];
				$ar_new['data_ass']=$row['DATA_A'];
				$ar_new['imp_ass']= $row['IMP_FORN_A'];
				
				//STATO ASSISTENZA
				$ar_new['st_ass'] = $row['ST_ASS'];
				if ($ar_new['nr_ass'] > 1){
					$ar_new['st_ass'] = '---';
					$result = db2_execute($stmt_a, array($ar_new['ord']));
					$ar_st = array();
					while($row_a = db2_fetch_assoc($stmt_a))
						$ar_st[$row_a['TDSTAT']]++;					
					
					$ar_new['st_ass'] = out_list_value_from_array($ar_st);
					
				}	
				$ar_new['art_mancanti'] = $s->get_fl_art_manc($row);
				$ar_new['data_ord']=$row['TDODRE'];
				$ar_new['data_fatt']=$row['TDDTFA'];
				$ar_new['stato']=$row['TDSTAT'];
				
				if(trim($row['TDISON']) != ''){
				    $ar_new['c_marg'] = trim($row['TDISON']);
				    $ar_new['t_causale'] = acs_u8e($s->decod_std('CMAR', trim($row['TDISON'])));
				}    
				
				
				
				//$ar_new['f_spunta'] = $row['TDFG04'];
				//$ar_new['f_spunta_black'] = $row['TDFG04'];
				$ar_new['liv'] = 'liv_3';
				if($row['IMP_FORN_PRE_RD'] > 0){
				    $ar_new['value_scost'] = ($row['IMP_FORN']-$row['IMP_FORN_PRE_RD'])/$row['IMP_FORN_PRE_RD']*100;
				    if($ar_new['value_scost'] > (float)$form_values->f_scost || $ar_new['value_scost'] < - (float)$form_values->f_scost){
				    $ar_new['segnala_sfora_scost'] = 'Y';
				    $t_ar_liv_cli['segnala_sfora_scost'] = 'Y';
				    }
				}
				$ar_new['leaf']=true;
				$ar_r[$liv]=$ar_new;
			}
			
			
			
			$ar_r=&$ar_r[$liv];
			sum_columns_value($ar_r, $row, $form_values);

		} //while

	return $ar;
}

function has_commento_ordine_profit($k_ord, $bl){
    global $conn;
    global $cfg_mod_DeskPVen, $s;
    
   /* if (is_null($bl))
        $bl = $cfg_mod_DeskPVen['pv_commenti_delivery'];*/
        
        $oe = $s->k_ordine_td_decode($k_ord);
        
        $sql = "SELECT count(*) AS NR
        FROM {$cfg_mod_DeskPVen['file_righe']}
        WHERE RDDT=" . sql_t($oe['TDDT']) . " AND RDOTID=" . sql_t($oe['TDOTID'])  . "
				  AND RDOINU=" . sql_t($oe['TDOINU']) . " AND RDOADO=" . sql_t($oe['TDOADO'])  . " AND RDONDO=" . sql_t($oe['TDONDO'])  . "
				  AND RDTPNO=" . sql_t($bl) . "
			";
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if ($row['NR'] > 0) return 1;
        else return 0;
}


