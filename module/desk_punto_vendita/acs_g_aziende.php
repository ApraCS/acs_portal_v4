<?php 

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();


$m_table_config = array(
		'tab_name' =>  $cfg_mod_Spedizioni['file_tabelle'],
		
		'TATAID' => 'AZIEN',
		'descrizione' => 'Lista aziende',
		
		'fields_key' => array('TAKEY1'),
		
		'fields' => array(				
				'TAKEY1' => array('label'	=> 'Azienda'),
				'TADESC' => array('label'	=> 'Descrizione'),
				'TAINDI' => array('label'	=> 'Indirizzo'),
				'TACAP'  => array('label'	=> 'CAP'),
				'TALOCA' => array('label'	=> 'Localit&agrave;'),
				'TAPROV' => array('label'	=> 'Provincia'),
				'TANAZI' => array('label'	=> 'Nazione'),
				'TATELE' => array('label'	=> 'Telefono'),
				'TAMAIL' => array('label'	=> 'Email'),
				
		)
		
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid.php';
