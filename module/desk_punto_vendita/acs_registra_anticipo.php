<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'exe_check_cliente'){
    
    $m_params = acs_m_params_json_decode();
    $ord = $s->get_ordine_by_k_docu($m_params->k_ordine);
    
    $anagrafica_cliente_con_dati_completi = $main_module->anagrafica_cliente_con_dati_completi($ord['TDCCON']);
   
    if ($anagrafica_cliente_con_dati_completi['success'] == false){
        $ret = $anagrafica_cliente_con_dati_completi;
        echo acs_je($ret);
        return;
        
    }
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit();
    
}

if ($_REQUEST['fn'] == 'exe_registra_anticipo'){

	$m_params = acs_m_params_json_decode();

	$use_session_history = microtime(true);
	$form_values=$m_params->form_values;
	
	if($m_params->res_cap == 'Y')
	    $msg_ri = 'restituzione_caparra';
    else 
	    $msg_ri = 'registra_anticipo';
	
		$sh = new SpedHistory();
		$sh->crea(
		        $msg_ri,
				array(
							
						"nota" 		    => '*',  //la mia nuova preferenza
						"text_nota" 	=> $form_values->f_note_1,
						"k_ordine"      => $m_params->k_ordine, //la preferenza precedente
						"use_session_history" 	=> $use_session_history

				)
				);
	
		$sh = new SpedHistory();
		$sh->crea(
		        $msg_ri,
				array(
						"nota" 		    => '*',
						"text_nota" 	=> $form_values->f_note_2,  //la mia nuova preferenza
						"k_ordine"      => $m_params->k_ordine, //la preferenza precedente
						"use_session_history" 	=> $use_session_history
		
				)
				);
		

		$sh = new SpedHistory();
		$sh->crea(
		        $msg_ri,
				array(
						"nota" 		    => '*',
						"text_nota" 	=> $form_values->f_note_3,  //la mia nuova preferenza
						"k_ordine"      => $m_params->k_ordine, //la preferenza precedente
						"use_session_history" 	=> $use_session_history
		
				)
				);
		

		$sh = new SpedHistory();
		$sh->crea(
		        $msg_ri,
				array(
						"nota" 		    => '*',
						"text_nota" 	=> $form_values->f_note_4,  //la mia nuova preferenza
						"k_ordine"      => $m_params->k_ordine, //la preferenza precedente
						"use_session_history" 	=> $use_session_history
		
				)
				);
		

		$sh = new SpedHistory();
		$sh->crea(
		        $msg_ri,
				array(
						"nota" 		    => '*',
						"text_nota"     => $form_values->f_note_5,  //la mia nuova preferenza
						"k_ordine"      => $m_params->k_ordine, //la preferenza precedente
						"use_session_history" 	=> $use_session_history
		
				)
				);
		
		$sh = new SpedHistory();
		$ret_RI = $sh->crea(
		        $msg_ri,
				array(
							
						"data" 			=> $form_values->f_data,  
						"tipo_paga" 	=> $form_values->f_pagamento,
						"flag_saldo" 	=> $form_values->f_saldo,
						"importo"		=> sql_f($form_values->f_importo), 
						"k_ordine"      => $m_params->k_ordine, 
						"end_session_history" =>  $use_session_history
		
				)
				);

	$ret = array();
	$ret['success'] = true;
	$ret['ret_RI'] = $ret_RI;
	echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'open_form'){
	$m_params = acs_m_params_json_decode();
	$ord = $s->get_ordine_by_k_docu($m_params->k_ordine);
	$initial_data_txt = $s->get_initial_calendar_date();
	
	$row_at = $s->get_TA_std('USATT', $auth->get_user(), 'MODAF');
	
	if(count($row_at) > 0)
	    $mod_data = 'Y';
    else
        $mod_data = 'N';

	?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            //url: 'acs_print_lista_consegne.php',
            
            items: [
				{
                	xtype: 'hidden',
                	name: 'filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                },
                
                <?php if($mod_data == 'Y'){?>
                		{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , width: 250
						   , value: '<?php echo $initial_data_txt ?>'
			
						} ,
				<?php }else{?>	
				  		 {xtype: 'displayfield',
						  fieldLabel : 'Data',
						  name: 'f_data_video',
						  value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
						  },
						  {xtype: 'hiddenfield',
						  name: 'f_data',
						  value: '<?php echo oggi_AS_date(); ?>'
						  },
						
				
				<?php }?>	
						
						
						 {
		                    xtype: 'numberfield',
		                    name: 'f_importo',
		                    hideTrigger:true,
		                    fieldLabel: 'Importo con IVA',
		                    width: 250,
		                    <?php if($m_params->res_cap == 'Y'){?>
		                    listeners : {
		                          'blur': function(field) {
		                            var value = field.getValue();
		                             if(value > <?php echo j($m_params->caparra) ?>){
		                                acs_show_msg_error('Importo maggiore caparra disponibile');
    									field.setValue('');
    									return false;
		                             }
		                          }
		                    
		                    }
		                    <?php }?>
		                },{
							name: 'f_pagamento',
							xtype: 'combo',
							fieldLabel: 'Pagamento', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							flex: 1,
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php //echo acs_ar_to_select_json($s->get_options_pagamento(), '') ?>
								     <?php echo acs_ar_to_select_json(find_TA_sys('CUCP'), '') ?> 	
								    ] 
							},
							<?php if($m_params->res_cap != 'Y'){?>
								value: <?php echo j(trim($ord['TDCPAG'])) ?>
							<?php }?>
					} 
		                	
		                	<?php if($m_params->res_cap != 'Y'){?>
		                
					     ,{				 
							xtype: 'checkboxgroup',
							fieldLabel: 'A saldo',
							labelAlign: 'left',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_saldo' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'Y'
		                        }]														
						 }
						<?php }?> 
						 
						, {						 
							 xtype: 'fieldset',
							 title: 'Note',
							 layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'
								   },		   					
							items: [
						 
								 {
										name: 'f_note_1',
										xtype: 'textfield',
										maxLength: 50,
									    anchor: '-15',	
									 },{
										name: 'f_note_2',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 },{
										name: 'f_note_3',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 },{
										name: 'f_note_4',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 },{
										name: 'f_note_5',
										xtype: 'textfield',
										//fieldLabel: 'Note',
									    anchor: '-15',	
									    maxLength: 50
									 }
							]
						}
            ],
            
			buttons: [		
						
				{
		           
		            <?php if($m_params->res_cap == 'Y' && $m_params->garanzia != 'Y'){?>
		             text: 'Genera RESTITUZIONE CAPARRA',
					<?php }elseif($m_params->res_cap == 'Y' && $m_params->garanzia == 'Y'){?>
			         text: 'Genera RESTITUZIONE GARANZIA',
			         <?php }else{?>
			         text: 'Genera FATTURA ANTICIPO',
					<?php }?>
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		            
		            var form=this.up('form').getForm();
		            var win=this.up('window');
		            var button = this;
			        button.disable();
			      
			          if (Ext.isEmpty(form.findField('f_importo').getValue())) {
		            	acs_show_msg_error('Inserire importo'); 
		            	button.enable();
		            	return false;
		            }

		            if (parseFloat(form.findField('f_importo').getValue()) > parseFloat(<?php echo $ord['TDTOTD'] ?>)) {
		            	acs_show_msg_error('L\'importo indicato non pu&ograve; essere superiore a quello dell\'ordine'); 
		            	button.enable();
		            	return false;
		            }
		           
		            if(form.isValid()){
		                            Ext.getBody().mask('Loading... ', 'loading').show();
                                 	Ext.Ajax.request({
							        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_registra_anticipo',
								        method     : 'POST',
					        			jsonData: {
					        				form_values: form.getValues(),
					        				k_ordine: <?php echo j($m_params->k_ordine) ?>,
					        				res_cap :  <?php echo j($m_params->res_cap) ?>
										},							        
								        success : function(result, request){
					            			 Ext.getBody().unmask();
											 jsonData = Ext.decode(result.responseText);
											 if (jsonData.ret_RI.RIESIT == 'W'){
											 	acs_show_msg_error(jsonData.ret_RI.RINOTE);
											 	button.enable();
											 	return;
											 }
					            			 win.close();									        
								        },
								        failure    : function(result, request){
								        	Ext.getBody().unmask();
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
					    });	
	                  }
	             
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}
?>