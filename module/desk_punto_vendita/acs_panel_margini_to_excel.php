<?php
require_once "../../config.inc.php";
require_once "acs_panel_margini_include.php";
require_once "acs_panel_protocollazione_include.php";

ini_set('max_execution_time', 6000);

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));


$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

header ("Content-Type: application/vnd.ms-excel");
header ("Content-Disposition: inline; filename=profit_to_excel");

$ar=crea_ar_tree_margini($_REQUEST['node'], $form_values, 'Y', 'Y');

?>
<html lang=it><head>
<title>Titolo</title></head>
<body>
<table border="1">
 <tr>
  <th>Sede</th>
  <th>Referente</th>
  <th>Cliente</th>
  <th>Ordine</th>
  <th>Data fattura</th>
  <th>Stato</th>
  <th>Importo ordine con IVA</th>
  <th>Importo forniture previsto senza IVA</th>
  <th>Coeff. Preventivo</th>
  <th>Importo forniture</th>
  <th>Coeff.</th>
  <th>Margine lordo</th>
  <th>Presenza importo fornitura</th>
 </tr>
<?php

foreach ($ar as $kar => $r){
  foreach ($r['children'] as $kar1 => $r1){
    foreach ($r1['children'] as $kar2 => $r2){
        foreach ($r2['children'] as $kar3 => $r3){
          foreach ($r3['children'] as $kar4 => $r4){
              echo "<tr>";
              echo "<td>" . $r1['task'] . "</td>";
              echo "<td>" . $r2['task'] . "</td>";
              echo "<td>" . $r3['task'] . "</td>";
              echo "<td>" . $r4['task'] . "</td>";
              echo "<td>" . print_date($r4['data_ord']) . "</td>";
              echo "<td>" . $r4['stato'] . "</td>";
              echo "<td>" . n($r4['importo_ord']) . "</td>";
              echo "<td>" . n($r4['importo_forn_pre']) . "</td>";
              echo "<td>" . n($r4['coeff_pre']) . "</td>";
              echo "<td>" . n($r4['importo_forn']) . "</td>";
              echo "<td>" . n($r4['coeff']) . "</td>";
              echo "<td>" . n($r4['margine']) . "</td>";
              echo "<td>" . n($r4['liv_nr_ord_costo_prev']) . "</td>";
              
              echo "</tr>";
          }
      }
  }
  }
}

?>
</table>
</body></html>