<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$m_DeskAcq = new DeskAcq(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

//test develop
/*************************************************************************
 * SALVA QUANTITA'
 *************************************************************************/
if ($_REQUEST['fn'] == 'exe_save_quant'){

	/*
	 Viene passato l'elenco delle chiavi ditta/articolo e relativa qta ordinata
	 1) aggirno le qta' su wmaf20 (usando i campi 20)
	 */

	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$ar_ord = array();

	$m_params = acs_m_params_json_decode();
	$list_selected_id = $m_params->list_selected_id; //elenco chiavi di fabbisogno

	
	if ($m_params->field_name == 'QTA_RIORD'){
		
		$sql = "UPDATE {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
				SET M2QDOR=?, M2FLOR = ?
				WHERE RRN(W2) = ? ";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		
		foreach($list_selected_id as $kr => $r){
		
			if($r->QTA_RIORD == 0){
				$stato='';
			} else {
				$stato='H';
			}
		
			$r = (array)$r;
			$dt 	= trim($r['DT']);
			$art	= trim($r['ARTICOLO_C']);
			$prog	= trim($r['PROGRESSIVO']);
			$qta	= (float)$r['QTA_RIORD'];
			$result = db2_execute($stmt, array($qta, $stato, $r['RRN_ID']));
			echo db2_stmt_errormsg($stmt);		
		}		
	} //QTA_RIORD
	
	if ($m_params->field_name == 'PRZ_LIST'){
	
		$sql = "UPDATE {$cfg_mod_DeskAcq['file_wizard_MTS_2']}
				SET M2PRZ=?
				WHERE RRN(W2) = ? ";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
	
		foreach($list_selected_id as $kr => $r){
	
			$r = (array)$r;
			$dt 	= trim($r['DT']);
			$art	= trim($r['ARTICOLO_C']);
			$prog	= trim($r['PROGRESSIVO']);
			$result = db2_execute($stmt, array((float)$r['PRZ_LIST'], $r['RRN_ID']));
			echo db2_stmt_errormsg($stmt);
			
			//scrivo RI
			$sh->crea(
					'pers',
					array(
							"messaggio"				=> 'INS_LIST_ACQ',
							"vals" => array(
									"RIART" 		=> $art,
									"RIPROG" 		=> $prog,                       //RIPROG=0???? SERVE RRN_ID???
									"RIPRZ" 		=> (float)$r['PRZ_LIST']
							),
					)
					);
		}
	} //QTA_RIORD
	
	


	$ret['success'] = true;
	
	echo acs_je($ret);
	exit;

}


/*************************************************************************
 * GENERAZIONE ORDINI MTO
 *************************************************************************/
if ($_REQUEST['fn'] == 'exe_generazione_ordini'){

	/*
	 Viene passato l'elenco delle chiavi ditta/articolo e relativa qta ordinata
	 1) aggirno le qta' su wmaf20 (usando i campi 20)
	 2) creo una riga su RI per ogni fornitore/data
	 */

	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$ar_ord = array();

	$m_params = acs_m_params_json_decode();
	$list_selected_id = $m_params->list_selected_id; //elenco chiavi di fabbisogno

	$sql = "UPDATE {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
			SET M2QDOR=?, M2FLOR = 'I', M2DTEO=?, M2PRZ=?
			WHERE RRN(W2) = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	
	foreach($list_selected_id as $kr => $r){
		$r = (array)$r;
		$dt 	= trim($r['DT']);
		$art	= trim($r['ARTICOLO_C']);
		$prog	= trim($r['PROGRESSIVO']);
		$qta	= (float)$r['QTA_RIORD'];
		$cons	= (string)$r['CONSEGNA_STD'];
		$forn	= (string)$r['FORNITORE_C'];
		$prz	= (float)$r['PRZ_LIST'];
		$result = db2_execute($stmt, array($qta, $cons, $prz, $r['RRN_ID']));
		echo db2_stmt_errormsg($stmt);

		//mi creo un array per ditta/fornitore/consegna
		$ar_ord[$dt][$forn][$cons] +=1;
		
		/*print_r($r);
		exit;*/
	}


	//Prima di generare gli ordini, recupero l'elenco delle righe interessate (perche' dovro' fornire un report)
	//TODO: gestire la multisessione?
	$sql2 = "SELECT * FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} WHERE M2FLOR = 'I'";
	$stmt2 = db2_prepare($conn, $sql2);
	$result2 = db2_execute($stmt2);
	while ($r = db2_fetch_assoc($stmt2)) {
		$ret_row[] = $r;
	}


	$tipologia = $m_params->tipologia;

				//scrivo RI per il fornitore/data inline
				//HISTORY
				
				$sh = new SpedHistory($m_DeskAcq);   //$m_DeskAcq
				$sh->crea(
						'dora_gen_ord_mts',
						array(
								"ditta" => $dt,
								"fornitore" => $forn,
								"tipologia" => $tipologia,
								"msg" => $m_params->msg_magazzino
								
						)
						);
				
		
	


	$ret = array();
	$ret['success']	= true;
	$ret['el_row']	= $ret_row;
	echo acs_je($ret);

	exit;
}



// ******************************************************************************************
// exe modifica punto riordino su articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'grid_conferma_ordine'){
	$m_params = acs_m_params_json_decode();

	
	?>

{"success":true, "items":

{
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    dockedItems: [{
			xtype: 'toolbar'
					, dock: 'bottom'
							, items: [
							
							{ xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-box_open-32',
					                     text: '<b>Genera movimentazioni</b>',
								            handler: function() {
			            
			            	list_selected_id = [];
							grid = this.up('grid');
							m_win = this.up('window');
														
							grid.getStore().each(function(rec) {  
							  if (rec.get('OLD_RIORD').trim() == 'H' && parseFloat(rec.get('QTA_RIORD')) > 0)
							  	list_selected_id.push(rec.data); 
							},this);
							
							tipol = Ext.getCmp('id_caus').getValue();
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione del movimento di magazzino?', function(btn){
							   if(btn === 'yes'){
							    Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
							            method: 'POST',
					        			jsonData: {
					        			list_selected_id: list_selected_id, 
					        			tipologia: tipol,
					        			msg_magazzino: 'GEN_MOV_MTS'
					        			
					        			},						            
							            success: function ( result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                
							                	m_win.fireEvent('afterExecute');
							                	m_win.close();
							                
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });
										            
							     
								         	}
								       }  
									  , {
							  	id: 'id_caus',				  
								name: 'f_caus',
								xtype: 'combo',
								fieldLabel: 'Causale',
								labelAlign: 'right',
								labelWidth: 40,
								width: 200,
								displayField: 'text',
							    valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: false,
							   	allowBlank: true,														
					            	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [		
							        <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('WMTSC'), '') ?>	
								    ] 
							   }	
							    						 
							  }  , '->',       
					 				{ 
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-shopping_cart_green-32',
					                     text: '<b>Genera ordini</b>',
								            handler: function() {
			            
			            	list_selected_id = [];
							grid = this.up('grid');
							m_win = this.up('window');
							
							
														
							grid.getStore().each(function(rec) {  
							  if (rec.get('OLD_RIORD').trim() == 'H' && parseFloat(rec.get('QTA_RIORD')) > 0)
							  	list_selected_id.push(rec.data); 
							
							if (rec.get('CONSEGNA_STD').trim() == ''){
								acs_show_msg_error('Inserire una data in Consegna richiesta');
							   	return false;
							  	}
							},this);
							
							tipol = Ext.getCmp('id_tip').getValue();
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione degli ordini a fornitore?', function(btn){
							   if(btn === 'yes'){
							    Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
							            method: 'POST',
					        			jsonData: {
					        			list_selected_id: list_selected_id, 
					        			tipologia: tipol
					      
					        			},						            
							            success: function ( result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                
							                	m_win.fireEvent('afterExecute');
							                	m_win.close();
							                
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });
										            
							     
								         	}
								       }  
									  , {
							  	id: 'id_tip',					  
								name: 'f_tipol',
								xtype: 'combo',
								fieldLabel: 'Tipologia',
								labelWidth: 50,
								width: 200,
								margin: '0 5 0 0',
								labelAlign: 'right',
								displayField: 'text',
							    valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: false,
							   	allowBlank: true,														
					            	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [		
							        <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('WMTST'), '') ?>	
								    ] 
							   }	
							    						 
							  }      			       
			]
		}],	                    
		
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		            listeners: {
		              'edit': function(editor, e, opts) {
		              	
		              	//se una data, converto il valore in Ymd
		              	if (e.field == 'CONSEGNA_STD'){
		              		e.record.set(e.field, Ext.Date.format(e.value, 'Ymd'));
		              	}
		              
		             	//////e.grid.store.save();
		             	
		             	//Forzoicalcolo totale riga
		             	e.record.set('PRZ_LIST_TOT');
		             	
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],	    
	    
		features: [
					{ftype: 'summary'}, {
			            id: 'group',
			            ftype: 'groupingsummary',
			            groupHeaderTpl: '{[values.rows[0].data.FORNITORE_D]} [{name}]',
			            hideGroupedHeader: false
			        },					
						
			{
				ftype: 'filters',
				encode: false, //settingsGrid.store.commitChanges() json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_conferma_grid', 
								   method: 'POST',								
								   type: 'ajax',
    								actionMethods: {
										     read: 'POST'
										    },
				          extraParams: {
								 
								 selected_forn: <?php echo j($m_params->fornitore)?>
							
			        			},
			     
			    doRequest: personalizza_extraParams_to_jsonData, 								
	
							       
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root',
						            idProperty: 'order'						            
						        }
							},
			groupField: 'FORNITORE_C',
				
			fields: ['RRN_ID', 'DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'UM', 'PROGRESSIVO', 
						{name: 'DISPONIBILITA', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'}, 
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'QTA_RIORD', 'PRZ_LIST', 
						{name: 'PRZ_LIST_TOT', type: 'float',
							convert: function(val,row) {
    							return parseFloat(row.get('QTA_RIORD')) * parseFloat(row.get('PRZ_LIST'));
    						}
    					}	
					]
						
		}, //store	    
	    			 		
		columns: [				    
				  	{header: 'Codice', width: 100, dataIndex: 'ARTICOLO_C', filter: {type: 'string'}}
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}, 	
				  		 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					           return 'Totale ordine a fornitore'; 
					     }
					}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', tdCls: 'grassetto', 
				  	
					renderer: function (value, metaData, record, row, col, store, gridView){
							if (record.get('OLD_RIORD').trim() != '')
			                		metaData.tdCls += ' grassetto sfondo_grigio';
			                	
			                	return floatRenderer0N(value);
			        },				  	
				  	
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  
				   }
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS, tdCls: 'grassetto',
					editor: {
			                xtype: 'datefield',
			                allowBlank: false			                
			            }				  				   
				  
				  }
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', renderer: floatRenderer2N, tdCls: 'grassetto',
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  				   
				  }				  
				  , {header: 'Importo',	 	width: 72, dataIndex: 'PRZ_LIST_TOT', align: 'right', renderer: floatRenderer2N,
							 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
								            return floatRenderer2(value); 
								        }				  
				  }				  
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    }
		    
	    
		    
    							
	}    
    

}


<?php exit; }

// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$m_where = '';
	
	$selected_id = $_REQUEST['selected_id'];
	
	$m_where .= sql_where_by_combo_value('M2FOR1', $selected_id);
	
	/*if (count($selected_id) > 0 && strlen($_REQUEST['selected_id']) > 0){
		$m_where .= " AND M2FOR1 = '{$selected_id}' ";
	}*/
	
	
	if(isset($js_parameters->depo) && count($js_parameters->depo) > 0)
	    $m_where .= sql_where_by_combo_value('M2MAGA', $js_parameters->depo);
	
	//GRUPPO
	if (strlen(trim($_REQUEST['f_gruppo'])) > 0)
		$m_where .= " AND m2cgru = '" . trim($_REQUEST['f_gruppo']) . "'";
		
	//sottoGRUPPO
	if (strlen(trim($_REQUEST['f_sottogruppo'])) > 0)
		$m_where .= " AND m2csgr = '" . trim($_REQUEST['f_sottogruppo']) . "'";
		
	if (strlen(trim($_REQUEST['f_deposito'])) > 0)
	    $m_where .= " AND M2MAGA = '" . trim($_REQUEST['f_deposito']) . "'";
	
		
	if (strlen(trim($_REQUEST['f_cod_art'])) > 0)
	    $m_where .= " AND W2.M2ART LIKE '%" . trim($_REQUEST['f_cod_art']) . "%'";
		
    if (strlen(trim($_REQUEST['f_des_art'])) > 0)
        $m_where .= " AND W2.M2DART LIKE '%" . trim($_REQUEST['f_des_art']) . "%'";
   
    //SOSPESI
    if (strlen(trim($_REQUEST['f_sospesi'])) > 0)
	  $m_where .= " AND (M2SOSP <> 'S' OR (M2QGIA <> 0 OR M2QORD <> 0 OR M2QFAB <> 0))";
		    
	
	  
  $sql_r = "SELECT SUM(M3QTA) Q_RIS 
            FROM {$cfg_mod_Spedizioni['file_dettaglio_wizard']} M3
            WHERE M3DT = '{$id_ditta_default}' AND M3FG01 = 'R' AND M3TRIG = 'I'
            AND M3ART = ? AND M3MAGA = ?";
  
  $stmt_r = db2_prepare($conn, $sql_r);
  echo db2_stmt_errormsg();	
	
  $sql = "SELECT MAX(RRN(W2)) AS RRN_ID, M2DVN1, M2SOSP, M2MAGA, M2DVN2, M2DVN3, M2DVN4, M2DVN5, M2QDOR, M2FLOR, M2QMAN, M2LOTT, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
    	 W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2RGS1, W2.M2DTEO, W2.M2ART, W2.M2PROG, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM
    	 FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
         LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
    	 ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE
    	 WHERE 1=1  AND M2DT = '{$id_ditta_default}' {$m_where}
    	 GROUP BY
    	 M2DVN1, M2SOSP, M2MAGA, M2DVN2, M2DVN3, M2DVN4, M2DVN5, M2QDOR, M2FLOR, M2QMAN, M2LOTT, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
    	 W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2RGS1, W2.M2DTEO, W2.M2ART, W2.M2PROG, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM,
    	 TA_ITIN.TASITI
         ORDER BY W2.M2ART, W2.M2MAGA";
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while ($r = db2_fetch_assoc($stmt)) {
	    
	
		
	    $r['DT'] 			= acs_u8e($r['M2DT']);
		$r['FORNITORE_C'] 	= acs_u8e($r['M2FOR1']);
		
		if(trim($r['M2DVN1'])!=''){
			$r['VARIANTI'] 	    = acs_u8e($r['M2DVN1']);
		}
		
		if(trim($r['M2DVN2'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN2']);
		}
		
		if(trim($r['M2DVN3'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN3']);
		}
		
		if(trim($r['M2DVN4'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN4']);
		}
		
		if(trim($r['M2DVN5'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN5']);
		}
		
		
		$ta_sys = find_TA_sys('MUFD', trim($r['M2MAGA']));
		$r['DEPOSITO'] 	=  "[".trim($r['M2MAGA'])."] ".$ta_sys[0]['text'];
		$r['maga'] 	= trim($r['M2MAGA']);
		$r['FORNITORE_D'] 	= acs_u8e($r['M2RGS1']);
		$r['ARTICOLO_C'] 	= acs_u8e($r['M2ART']);
		if(trim($r['M2SOSP']) == 'S')
		  $r['TASK_ART'] = acs_u8e($r['M2ART']).'<span style="display: inline; float: right;"><img class="cell-img" src=' . img_path("icone/16x16/divieto.png") . '></span>';
		else 
		  $r['TASK_ART'] = acs_u8e($r['M2ART']);
		
		$r['ARTICOLO_D'] 	= acs_u8e($r['M2DART']);
		$r['PROGRESSIVO'] 	= $r['M2PROG'];
		$r['UM'] 			= acs_u8e($r['M2UM']);
		$r['GIACENZA'] 		= acs_u8e($r['M2QGIA']);
		$r['ORDINATO'] 		= acs_u8e($r['M2QORD']);
		$r['IMPEGNATO'] 	= $r['M2QFAB'];
		$r['SCORTA'] 		= acs_u8e($r['M2SMIC']);

		$r['LOTTO_RIORD']	= (int)$r['M2LOTT'];

		$r['CONSEGNA_STD']	= acs_u8e($r['M2DTEO']);
		$r['ULTIMO_IMP']	= acs_u8e($r['M2UIMP']);
		
		if (trim($_REQUEST['f_ordinato']) == 'Y')
		    $r['DISPONIBILITA']	= acs_u8e((float)$r['M2QGIA'] - (float)$r['M2QFAB']);
		else
		    $r['DISPONIBILITA']	= acs_u8e((float)$r['M2QGIA'] + (float)$r['M2QORD'] - (float)$r['M2QFAB']);
		    
		    
	    $r['DISPONIBILITA_MANCANTE']	= $r['M2QMAN'];

		//valore di scostamento dalla disponibilita'
		if ((int)$r['LOTTO_RIORD'] != 0){
			$r['SCOST_RIORD'] = ( (float)$r['DISPONIBILITA'] - (float)$r['LOTTO_RIORD'] ) / (float)$r['LOTTO_RIORD'] * 100;
		}


		$r['QTA_RIORD']		= $r['M2QDOR'];
		$r['OLD_RIORD']		= $r['M2FLOR'];
		$r['PRZ_LIST']		= $r['M2PRZ'];
		
		$result = db2_execute($stmt_r, array($r['M2ART'], $r['M2MAGA']));
		$row_r = db2_fetch_assoc($stmt_r);
		$r['QTA_RIS'] = $row_r['Q_RIS'];
		
		$ar_ret[] = $r;
		
	   //}
	}


	echo acs_je($ar_ret);

	exit();
}



// ******************************************************************************************
// DATI PER GRID FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_fornitore'){
    global $cfg_mod_DeskAcq;
    $sql_where = "";
    
	if(isset($js_parameters->depo) && count($js_parameters->depo) > 0)
	    $sql_where.= sql_where_by_combo_value('M2MAGA', $js_parameters->depo);
	
	$sql = "SELECT DISTINCT M2FOR1, M2RGS1 FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']}
	          WHERE 1=1 AND M2DT = '{$id_ditta_default}' {$sql_where}";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}




//CAMPI CORRISPONDENTI NON TROVATI IN M2
// ******************************************************************************************
// DATI PER SELECT GRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_gruppo_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;
	
	$sql = "SELECT DISTINCT m2cgru as id, m2dgru as text FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2 WHERE m2cgru <> ''";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


//CAMPI CORRISPONDENTI NON TROVARI IN M2
// ******************************************************************************************
// DATI PER SELECT SOTTOGRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_sottogruppo_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;	

	$sql = "SELECT DISTINCT m2csgr as id, m2dsgr as text FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2 WHERE m2csgr <> ''";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}

// ******************************************************************************************
// DATI PER DEPOSITO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_deposito'){
    
    global $cfg_mod_DeskAcq;
    
    $sql_where = "";
    
    if(isset($js_parameters->depo) && count($js_parameters->depo) > 0)
        $sql_where.= sql_where_by_combo_value('M2MAGA', $js_parameters->depo);
    
    $sql = "SELECT DISTINCT M2MAGA FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2 
            WHERE M2DT = '{$id_ditta_default}' AND M2MAGA <> ''  {$sql_where}";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ar = array();
    
    while ($r = db2_fetch_assoc($stmt)) {
        
        $ta_sys = find_TA_sys('MUFD', trim($r['M2MAGA']));
        $ret[] = array("id"=>trim($r['M2MAGA']), "text" =>"[".trim($r['M2MAGA'])."] ".$ta_sys[0]['text']);
        
       
    }
    
    echo acs_je($ret);
    exit();
}

// ******************************************************************************************
// ARTICOLI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_articoli'){
    
    $ar = array();
    $m_params = acs_m_params_json_decode();
   
    $sql = "SELECT M2ART, M2DART, M2FOR1
    FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2 
    WHERE M2DT = '$id_ditta_default'
    AND UPPER(M2ART) LIKE '%" . strtoupper($_REQUEST['query']) . "%'
    ORDER BY M2ART";
    
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $text = "[" . trim($row['M2ART']) . "] " . trim($row['M2DART']);
        
        $ret[] = array( "id" 	 => trim($row['M2ART']),
                        "text" 	 => acs_u8e($text),
                        "M2FOR1" => acs_u8e(trim($row['M2FOR1']))
        );
    }

    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'get_json_data_conferma_grid'){

	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$m_params = acs_m_params_json_decode();
	
	$sql = "SELECT RRN(W2) AS RRN_ID, W2.*
	FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
    WHERE M2FOR1= {$m_params->selected_forn} AND M2FLOR='H' ";

	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($r = db2_fetch_assoc($stmt)) {
		$r['DT'] 			= acs_u8e($r['M2DT']);
		$r['FORNITORE_C'] 	= acs_u8e($r['M2FOR1']);
        $r['FORNITORE_D'] 	= acs_u8e($r['M2RGS1']);
		$r['ARTICOLO_C'] 	= acs_u8e($r['M2ART']);
		$r['ARTICOLO_D'] 	= acs_u8e($r['M2DART']);
		$r['PROGRESSIVO'] 	= $r['M2PROG'];
		$r['UM'] 			= acs_u8e($r['M2UM']);
	    $r['CONSEGNA_STD']	= acs_u8e($r['M2DTEO']);
		
		$r['QTA_RIORD']		= $r['M2QDOR'];
		$r['OLD_RIORD']		= $r['M2FLOR'];
		$r['PRZ_LIST']		= $r['M2PRZ'];
		$ar_ret[] = $r;
	}


	echo acs_je($ar_ret);

	exit();
}


// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA (APERTA IN DEFAULT)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
//            title: 'Selezione dati',
            
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
			            
            
            items: [
  			 	    {					
						xtype: 'grid',
						flex: 1,
						id: 'tab-tipo-stato',
						loadMask: true,
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},
     					
						features: [
						{
							ftype: 'filters',
							// encode and local configuration options defined previously for easier reuse
							encode: false, // json encode the filter query
							local: true,   // defaults to false (remote filtering)
					
							// Filters are most naturally placed in the column definition, but can also be added here.
						filters: [
						{
							type: 'boolean',
							dataIndex: 'visible'
						}
						]
						}],     					
     					
									
						selModel: {selType: 'checkboxmodel', mode: 'SINGLE'},				
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fornitore',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
								
						
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['M2FOR1', 'M2RGS1']
										
										
						}, //store
			
						columns: [{header: 'Fornitore', dataIndex: 'M2FOR1', flex: 2, filter: {type: 'string'}, filterable: true},
								  {header: 'Denominazione', dataIndex: 'M2RGS1', flex: 8, filter: {type: 'string'}, filterable: true},
								  {
				               xtype:'actioncolumn', 
				               header: 'PDF',			            
				               width:50,
		            		   items: [{
		                		 icon: <?php echo img_path("icone/16x16/print.png") ?>,

		                		margin: "0 0 0 30",
		                		tooltip: 'Visualizza report',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			acs_show_win_std('Elenco allegati', 'acs_allegati_anagrafica.php?fn=open_win', {codice: rec.get('M2FOR1'), tipo : 'F'}, 1024, 600, null, 'icon-leaf-16');
		                		}
		                	  }
		                	]
		                }
								  
								  
								  ]
						 
					}            
            
            
            		, {            		
						margin: "0 0 0 2",            		
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			//height: 50,
            			
			            layout: {
			                type: 'vbox',
			                align: 'stretch',
			                pack  : 'start'
			     		},            			
            			
     					items: [
     					
					//SELECT PER GRUPPO E SOTTOGRUPPO
					{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						     {
								margin: "0 10 0 10",
								flex: 1,					
								name: 'f_gruppo',
								xtype: 'combo',
								fieldLabel: 'Gruppo fornitura',
								labelAlign: 'top',
								//width: 290,
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  },{
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_sottogruppo',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_sottogruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  }
						
						]}, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						     {
								margin: "0 10 0 10",							  
								name: 'f_des_art',
								xtype: 'textfield',
								fieldLabel: 'Descrizione articolo',
								labelAlign: 'top',
								flex: 1,
								
							  },    {
							  margin: "0 10 0 10",
						flex: 1,
			            xtype: 'combo',
						name: 'f_cod_art',
						fieldLabel: 'Articolo',
						labelAlign: 'top',
						anchor: '-15',
						minChars: 2,			
						store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_articoli',
					            actionMethods: {
						          read: 'POST'
						        },
					            extraParams: {
				    		    		gruppo: '',
				    				},				            
						        doRequest: personalizza_extraParams_to_jsonData, 
					            reader: {
					                type: 'json',
					                root: 'root',
					                method: 'POST',	
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}, 'M2FOR1'],            	
			            },
                      
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun articolo trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {text}' + 
			                    '</div>';
			                	}                
		                
		            	},
		            	   listeners: {
				            
					        select: function(field,newVal) {
					        
					          var fornitore = newVal[0].get('M2FOR1');
					          var tab_for = this.up('form').down('#tab-tipo-stato');
					          ar_for = tab_for.store.data.items;
					          for (var i=0; i< ar_for.length; i++){
        	    			  		if(tab_for.store.data.items[i].data.M2FOR1 == fornitore)
        	    			  		   tab_for.selModel.doSelect(tab_for.store.data.items[i]);
        	    			  } 
					
					        }
				        }, 
            
            			pageSize: 1000,
        		}
						
						]},
						{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						     {
						flex: 1,
			            xtype: 'combo',
						name: 'f_fornitore',
						fieldLabel: 'Fornitore',
						labelAlign: 'top',
						anchor: '-15',
						margin: "0 10 0 10",		
						minChars: 1,			
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_gestione_articoli_form.php?fn=get_json_data_fornitori',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            },
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {text}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
        		} , {
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_deposito',
								multiSelect : true,
								xtype: 'combo',
								fieldLabel: 'Deposito',
								labelAlign: 'top',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					       	            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_deposito',
							            reader: {
        					                type: 'json',
        					                root: 'root',
        					                totalProperty: 'totalCount'
    					            }
							        },       
									fields: [{name:'id'}, {name:'text'}],     	             	
					            },
							    						 
							  }  
						
						]},
						
						
						{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   	 {
							name: 'f_sospesi',
							xtype: 'checkboxgroup',
							fieldLabel: '',
							margin: "0 80 5 10",
						//	labelAlign: 'right',
							//labelWidth : 210,
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_sospesi' 
		                          , boxLabel: 'Escludi sospesi non movimentati'
		                          , labelWidth : 210
		                          , inputValue: 'S'
		                          , checked : true
		                        }]														
							},
							
								 {
							name: 'f_ordinato',
							xtype: 'checkboxgroup',
							fieldLabel: '',
							margin: "0 10 5 10",
						//	labelAlign: 'right',
							//labelWidth : 210,
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_ordinato' 
		                          , boxLabel: 'Escludi ordinato da disponibilit&agrave;'
		                          , labelWidth : 210
		                          , inputValue: 'Y'
		                          , checked : true
		                        }]														
							}
						]}
						
						
						
					
						  
					
     					]
     				}            
            
	
						 
				],
			buttons: [
			
/*
			 {
	            text: 'Anagrafica articoli (OLD)',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
    				var form_p = this.up('form');
      				m_values = form_p.getValues();
				    acs_show_win_std('Visualizza articoli', 'acs_elenco_articoli.php', {
    				    fornitore : m_values.f_fornitore, 
    				    codice: m_values.f_cod_art, 
    				    d_art: m_values.f_des_art, 
    				    gruppo: m_values.f_gruppo,
    				    sottogruppo: m_values.f_sottogruppo,
    				    from_mts: 'Y'}, 900, 550, null, 'icon-shopping_setup-16');
	            
            	                	                
	            }
	        },
*/
	        
	        
	        {
	            text: 'Anagrafica articoli',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
    				var form_p = this.up('form');
      				m_values = form_p.getValues();
				    acs_show_win_std('Visualizza articoli', '../base/acs_elenco_articoli_adv.php', {
    				    fornitore : m_values.f_fornitore, 
    				    codice: m_values.f_cod_art, 
    				    d_art: m_values.f_des_art, 
    				    gruppo: m_values.f_gruppo,
    				    sottogruppo: m_values.f_sottogruppo,
    				    from_mts: 'Y',
    				    ricerca_solo_fornitore : 'Y',
    				    open_on_dblclick: 'background_mrp_from_wizard'}, 900, 550, null, 'icon-shopping_setup-16');
	            
            	                	                
	            }
	        },{ xtype: 'tbfill' },
	        
<!-- 	        { text: 'Report ordini fornitori', -->
<!-- 	          iconCls: 'icon-print-32', -->
<!-- 	          scale: 'large',	             -->
<!-- 	          handler: function() { -->
<!-- 	          	form = this.up('form').getForm(); -->
                  
<!--                   if (form.isValid()){ -->
<!--                   	acs_show_win_std('Filtro report ordini fornitore aperti', -->
<!--                    					 'acs_report_ordini_fornitori_aperti.php?fn=open_form', -->
<!--                    					 { filter: form.getValues() }, -->
<!--                    					 400, 250, {}, 'icon-print-16'); -->
<!--                    					 	}	 -->
<!--      		      				}  -->
<!-- 		     }, -->
	        
	        
	        {
	            text: 'Ubicazioni',
	            iconCls: 'icon-print-32', scale: 'large',	            
	            handler: function() {
                    var form = this.up('form').getForm();
                    if (form.findField('f_deposito').getValue()=='') {
                         acs_show_msg_error('Selezionare un deposito');
                         return false;
                       }       
                    if (form.isValid()){
                                acs_show_win_std('Filtri', 'acs_report_ubicazioni.php?fn=open_filtri', {
		        				filter: form.getValues()
		    					},		        				 
		        				300, 200, {}, 'icon-print-16');	
		        	    }   
                    
                    /*this.up('form').submit({
                      url: 'acs_report_ubicazioni.php',
                      target: '_blank',
                      standardSubmit: true,
                      method: 'POST',
                      params: {
                        form_values: Ext.encode(form.getValues()),    
                      }
          			});*/
	            }
	        },
	        
	        {
	            text: 'Reminder MTO/MTS',
	            iconCls: 'icon-info_blue-32',
	            scale: 'large',	            
	            handler: function() {
	            	this.up('window').close();
    				acs_show_win_std('Controllo/sollecito forniture MTO/MTS', 'acs_sollecito_forniture.php?fn=get_parametri_form', null, 610, 380, null, 'iconInfoGray')                	                
	            }
	        },
	        
				
			
			
			 {
	            text: 'Visualizza MTS',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

				var m_win  = this.up('window');
				var form_p = this.up('form');
				m_values = form_p.getValues();
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
	            id_selected = tab_tipo_stato.getSelectionModel().getSelection()[0].data.M2FOR1;
  					
   				m_values['selected_id'] = id_selected;
			  
				//carico la form dal json ricevuto da php e la inserisco nel m-panel
				Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_grid',
				        method     : 'POST',
				        jsonData: m_values,
						waitMsg    : 'Data loading',
				        success : function(result, request){			        	
				        	
							mp = Ext.getCmp('m-panel');					        					        
							mp.remove('wizard-mts');					        
				            var jsonData = Ext.decode(result.responseText);
				            
				            mp.add(jsonData.items);
				            mp.doLayout();
				            mp.items.last().show();
 							m_win.close();
						             
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				        				        
				    });
				    
	            
            	                	                
	            }
	        }

	    
	        
	        
	       ],             
				
        }
]}
<?php			            
exit;
}



/***********************************************************************************
 * GRID
 ***********************************************************************************/
if ($_REQUEST['fn'] == 'get_json_grid'){

	$m_params = acs_m_params_json_decode();

	//recuper data/ora ultimo aggiornamento
	$sql = "SELECT min(M2DTGE) as M2DTGE, min(M2ORGE) AS M2ORGE FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']}";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$r = db2_fetch_assoc($stmt);
	if ($r != false){
		$situazione_txt = " [Situazione al " . print_date($r['M2DTGE']) . " " . print_ora($r['M2ORGE']) . "]";
	} else
		$situazione_txt = '';


		?>

{"success":true, "items":

  {
    xtype: 'panel',	    
	title: <?php echo acs_je('Wizard MTS | ' . $m_params->selected_id)?>,
	closable: true,
	 	
    layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},    
    items: [
    
    {
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    stateful: true,
    	stateId: 'pv_wizard_mts',
    	stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
	    
        dockedItems: [{
            xtype: 'toolbar'
            , dock: 'top'
            , items: [{xtype: 'label', cls: 'strong', text: 'Controllo disponibilita/riordino articoli a magazzino' + <?php echo j($situazione_txt); ?>}, {xtype: 'tbfill'}
            
				, {
                     xtype: 'button',
                     text: '',
			            handler: function() {
			         	}
			       }
			       
			      <?php  if ($js_parameters->only_view  != 1){?>
			       
 				, {
                     xtype: 'button',
	            	 //scale: 'medium',
	            	 iconCls: 'icon-shopping_cart_green-16',
                     text: 'Conferma riordino/movimentazione',
			            handler: function() {
			            
			            mm_grid = this.up('grid');
							
							//apro form per richiesta conferma e possibilita' modifica prezzo e data consegna
							                   	my_listeners = {
						        					afterExecute: function(){
						        						mm_grid.store.reload();						        									            
										        		}
								    				};							
							
							
							acs_show_win_std('Riepilogo articoli da riordinare/movimentare per fornitore', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_conferma_ordine', {fornitore: '<?php echo $m_params->selected_id ?>'}, 800, 500, my_listeners, 'icon-shopping_cart_gray-16');
							return false;
							
							
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione degli ordini a fornitore?', function(btn){
							   if(btn === 'yes'){
									Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
							            method: 'POST',
					        			jsonData: {list_selected_id: list_selected_id},						            
							            success: function ( result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                
							                //apro un report passando le righe interessate
												var mapForm = document.createElement("form");
												mapForm.target = "_blank";    
												mapForm.method = "POST";
												mapForm.action = '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report';
												
												// Create an input
												var mapInput = document.createElement("input");
												mapInput.type = "hidden";
												mapInput.name = "jsonData";
												mapInput.value = JSON.stringify(jsonData);
												
												// Add the input to the form
												mapForm.appendChild(mapInput);
												
												// Add the form to dom
												document.body.appendChild(mapForm);
												
												// Just submit
												mapForm.submit();							                
							                
							                
							                grid.store.load()
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });
										            
			         	}
			       }  			       
			       
			       <?php }?>
			       
			]
		}],	                    
		
	    
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		              clicksToEdit: 1,
		            listeners: {
		              'beforeedit': function(e, eEv, a, b, c, d) {
		                var me = this;
		                rec = eEv.record;		                
		                
		                //decido, per colonna, quando e' possibile modificare il valore
		                
		                if (eEv.field == 'QTA_RIORD'){
							if (rec.data.OLD_RIORD.trim() == '' || rec.data.OLD_RIORD == 'H')		                
			                	return true;
			                else
			                	return false;		                
		                }

		                if (eEv.field == 'PRZ_LIST'){
		                	if (rec.get('PRZ_LIST') != 0)	//modifiche ammesse solo se il valore di partenza e' 0
		                		return false;
		                	return true;
		                }	
		              		                

		              },
		              'edit': function(e) {
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],	    
	    
		features: [
			{
				ftype: 'filters',
				encode: false, // json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
				method: 'POST',
				type: 'ajax',
	            timeout: 120000,
	            autoSave: true,
	            autoSync: true,
	            actionMethods: {
							     read: 'POST'
							    },
				extraParams: {
								 f_cod_art: <?php echo j($m_params->f_cod_art) ?>,
								 f_des_art: <?php echo j($m_params->f_des_art) ?>,
								 selected_id: <?php echo j($m_params->selected_id) ?>,
								 f_gruppo: <?php echo j($m_params->f_gruppo) ?>,
								 f_sottogruppo: <?php echo j($m_params->f_sottogruppo) ?>,
								 f_deposito: <?php echo acs_je($m_params->f_deposito) ?>,
								 f_sospesi: <?php echo j($m_params->f_sospesi) ?>,
								 f_ordinato: <?php echo j($m_params->f_ordinato) ?>
			        			},
			     
			    doRequest: personalizza_extraParams_to_jsonData, 								
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				}				
			},
				
			fields: ['RRN_ID', 'DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'DEPOSITO', 'maga', 'UM', 'VARIANTI', 'PROGRESSIVO',
						{name: 'DISPONIBILITA', type: 'int'}, {name: 'DISPONIBILITA_MANCANTE', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'},
						{name: 'QTA_RIS', type: 'float'},  
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'QTA_RIORD', 'PRZ_LIST', 'TASK_ART']
						
		}, //store	    
	    			 		
		columns: [		
				   {header: 'Codice', 
				   width: 100, 
				   dataIndex: 'TASK_ART', 
				   filter: {type: 'string'},
				   renderer: function (value, metaData, record, row, col, store, gridView){
	            	    metaData.tdCls += ' auto-height';
		            	return value;
	               }
				   
				   }
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}}
				  , {header: 'Deposito', flex: 1, dataIndex: 'DEPOSITO', filter: {type: 'string'}}
				  , {header: 'Varianti', flex: 1, dataIndex: 'VARIANTI', filter: {type: 'string'}}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Disponib.',	width: 72, dataIndex: 'DISPONIBILITA', align: 'right',
					    renderer: function (value, metaData, record, row, col, store, gridView){
			                	metaData.tdCls += ' grassetto';

					  				if (parseFloat(value) < 0)
					  					 metaData.tdCls += ' sfondo_rosso';					  					 														  
					  				else if (parseFloat(value) == 0 || parseFloat(record.get('DISPONIBILITA_MANCANTE')) > 0 )
					  					 metaData.tdCls += ' sfondo_giallo';					  					 
					  				if (parseFloat(value) > 0)
					  					 metaData.tdCls += ' sfondo_verde';					  					 
			                			                
					  			return floatRenderer0N(value);									
						} 
				    }				  
				  , {header: 'Giacenza',	width: 72, dataIndex: 'GIACENZA', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Ordinato',	width: 72, dataIndex: 'ORDINATO', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Impegnato',	width: 72, dataIndex: 'IMPEGNATO', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Scorta',	 	width: 72, dataIndex: 'SCORTA'	, align: 'right', renderer: floatRenderer0N}				  
				  , {header: 'Riservati',	width: 72, dataIndex: 'QTA_RIS'	, align: 'right', renderer: floatRenderer0N}				  				  				  				  				  
				  , {header: 'Punto di<br/>riordino',	width: 72, dataIndex: 'SCOST_RIORD', align: 'right',

						renderer: function (value, metaData, record, row, col, store, gridView){
								if (parseFloat(value) != 0){
				                	if (value < -5)
				                		metaData.tdCls += ' grassetto sfondo_rosso';
									else if (value <= 5)				                	
										metaData.tdCls += ' grassetto sfondo_giallo';
								}		
				                	
				                	return floatRenderer0N(record.get('LOTTO_RIORD'));
				        },				  	
				  	 
				  	}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', 
				  	
						renderer: function (value, metaData, record, row, col, store, gridView){
					
					            if (record.get('OLD_RIORD').trim() == 'H' || record.get('OLD_RIORD').trim() == ''){
					             metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';
					            }
			                	else {
			                		metaData.tdCls += ' grassetto sfondo_grigio';
		                		}
			                	
			                	return floatRenderer0N(value);
			        },				  	
				  	
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  
				   }
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS}
				  , {header: 'Ultimo<br/>impegno',	width: 90, dataIndex: 'ULTIMO_IMP', renderer: date_from_AS}
				 
				 <?php if($js_parameters->hide_price != 1){?>
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', 	
				  		renderer: function (value, metaData, record, row, col, store, gridView){
				  			metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';
				  			return floatRenderer2N(value);
				  		},
						editor: {
			                xtype: 'numberfield', 
			                allowBlank: true
			            }				  
				  }
				  
				  <?php }?>
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    },
		    
		    
         listeners: {
         
         
         
				itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						  var voci_menu = [];
						  voci_menu.push({
			         		text: 'Visualizza ubicazioni',
			        		iconCls : 'icon-shopping_cart_gray-16',          		
			        		handler: function () {
			        			    list_selected_id = grid.getSelectionModel().getSelection();
					    		  	rec = list_selected_id[0];
					    		  	acs_show_win_std('Loading...', 'acs_ubicazioni_per_articolo.php?fn=open', {
					    		  		dt : rec.get('DT'), 
					    		  		art: rec.get('ARTICOLO_C'),
					    		  		art_d: rec.get('ARTICOLO_D'),
					    		  		maga : rec.get('maga')}, 800, 600, null, 'icon-shopping_cart_gray-16');
				                }
			    		  });
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			   },
         
         
         
         
   		  celldblclick: {								
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
			  	rec = iView.getRecord(iRowEl);
		
				 
				  acs_show_win_std('Loading...', 'acs_background_mrp_art.php', {dt: rec.get('DT'), rdart: rec.get('ARTICOLO_C'), from_wz : 'Y', maga : rec.get('maga'), disp_maga : rec.get('DISPONIBILITA')}, 1200, 600, null, 'icon-shopping_cart_gray-16');
			  }
   		  },
   		  
   		         edit: function(editor, e, a, b, c, d){
			                   			
                 			var field_name = e.field;
                 
                 			list_selected_id = [];
							grid = editor.grid;
							record= e.record;
												
							list_selected_id.push(record.data);
							
							
							if (field_name == 'QTA_RIORD'){
								var old_value = record.raw.QTA_RIORD;
								var new_value =  record.data.QTA_RIORD;							
							}
							
							if (field_name == 'PRZ_LIST'){
								var old_value = record.raw.PRZ_LIST;
								var new_value =  record.data.PRZ_LIST;							
							}							
							
							if (old_value == new_value)
								return false;
							
							Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_quant',
							            method: 'POST',
					        			jsonData: {
					        				list_selected_id: list_selected_id,
					        				field_name: field_name
					        			},						            
							            success: function (result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                    //grid.store.reload();	
							          							    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
                  

            }
	         
         }		    
		    
    							
	}    
    
    ] //items
  } //main panel 

}
<?php
exit; }
?>