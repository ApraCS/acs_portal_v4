<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
//$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

if ($_REQUEST['fn'] == 'open_filtri'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	            
	           	    {
							name: 'f_barcode',
							xtype: 'checkboxgroup',
							fieldLabel: '',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_barcode' 
		                          , boxLabel: 'Includi Barcode'
		                          , labelWidth : 210
		                          , inputValue: 'Y'
		                          , checked : true
		                        }]														
							},
							
							  	    {
							name: 'f_ubi_blank',
							xtype: 'checkboxgroup',
							fieldLabel: '',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_ubi_blank' 
		                          , boxLabel: 'Includi ubicazioni vuote'
		                          , labelWidth : 210
		                          , inputValue: 'Y'
		                          , checked : false
		                        }]														
							}
							
						  , { name: 'f_est_dati_pivot'
						    , xtype: 'checkboxgroup'
						    , fieldLabel: ''
						    , allowBlank: true
						    , items: [
					          { xtype: 'checkbox'
		                      , name: 'f_est_dati_pivot' 
		                      , boxLabel: 'Estrazione dati pivot'
		                      , labelWidth : 210
		                      , inputValue: 'Y'
		                      , checked : true
		                      }]														
							}
							
					      , {
							xtype: 'radiogroup',
							fieldLabel: '',
							labelWidth: 110,
						   	allowBlank: true,
						   	anchor: '-15',
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_visual' 
		                          , boxLabel: 'Ubicazioni/Articolo'
		                          , inputValue: 'UA'
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_visual' 
		                          , boxLabel: 'Articolo/Ubicazioni'
		                          , inputValue: 'AU'
		                          
		                        }]
						}
                 
	            ],
	            
				buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        } 
		        
		        ]            
	           
	}
		
  ]}
	
<?php 
exit;
}

if($_REQUEST['fn'] == 'open_report'){
    
$ar_email_to = array();
$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}
$ar_email_json = acs_je($ar_email_to);

$filter =	strtr($_REQUEST['filter'], array('\"' => '"'));
$filter = json_decode($filter);

$deposito = $filter->f_deposito;
$articolo = $filter->f_cod_art;

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);
$barcode = $form_values->f_barcode;
$ubi_blank = $form_values->f_ubi_blank;
$stacco = $form_values->f_visual;
$estrazione_dati_pivot = $form_values->f_est_dati_pivot;

$nr = array();
$ar = array();

$lettura = $main_module->_get_ubicazione($deposito, '', $stacco, $ubi_blank);

foreach ($lettura as $k => $row){
    
    $ord_acq_ar = $main_module->_get_oa_from_bc($row['BC']);
    $ord_cli_ar = $main_module->_get_oc_from_oa_gest($ord_acq_ar);
    $tmp_ar_id = array();
    $ar_r= &$ar; // assegnazione per riferimento! se modificata variabile $ar_r,
    // allora modificata anche $ar perch� assegnata per riferimento &
    
    //stacco dei livelli
    $cod_liv_tot = 'TOTALE';			//totale
    
    if($stacco == 'UA'){
        $cod_liv0 = trim($row['UBIC']); 	//ubicazione
        $cod_liv1 = trim($row['ART']); //articolo
    } else { $cod_liv0 = trim($row['ART']); 	//articolo
             $cod_liv1 = trim($row['UBIC']);  //ubicazione
           }
    
      
    $cod_liv2 = trim($row['BC']);   //barcode
    
    //LIVELLO TOTALE
    $liv = $cod_liv_tot;
    if (!isset($ar_r["{$liv}"])){
        $ar_new = array();
        $ar_new['id']       = 'liv_totale';
        $ar_new['liv_cod']  = 'TOTALE';
        $ar_new['task']     = 'Totale';
        $ar_new['liv']      = 'liv_totale';
        $ar_new['expanded'] = true;
        $ar_r["{$liv}"]     = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    $ar_r['giacenza'] += $row['GIAC'];
    
    //--- livello per ubicazione
    $liv=$cod_liv0;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        
        $ar_new['articolo'] = trim($row['ART']);
        $ar_new['descrizione'] = trim($row['DART']);
        $ar_new['maga_c']     = trim($row['DEPO']);
        $ta0  = find_TA_sys('MUFD',trim($row['DEPO']));
        $ar_new['magazzino']     = "[".trim($row['DEPO'])."] ".$ta0[0]['text'];
        $ar_new['ubicazione'] = trim($row['UBIC']);
        $ar_liv0 = &$ar_r[$liv];
        $ar_r[$liv]=$ar_new;
    } 
     
     $ar_r=&$ar_r[$liv];
     $ar_r['giacenza'] += $row['GIAC'];
     
                
    //--- livello per articolo
    $liv=$cod_liv1;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['articolo'] = trim($row['ART']);
        $ar_new['descrizione'] = trim($row['DART']);
        $ar_new['ubicazione'] = trim($row['UBIC']);
        $ar_new['maga_c']     = trim($row['DEPO']);
        $ta0  = find_TA_sys('MUFD',trim($row['DEPO']));
        //$ar_new['giacenza'] = trim($row['GIAC']);
        $ar_new['magazzino']     = "[".trim($row['DEPO'])."] ".$ta0[0]['text'];
        $ar_liv1 = &$ar_r[$liv];
        $ar_r[$liv]=$ar_new;
    }
    $ar_r=&$ar_r[$liv];
    $ar_r['giacenza'] += $row['GIAC'];
    
    //--- livello per barcode
    $liv=$cod_liv2;
    $ar_r=&$ar_r['children'];
    $tmp_ar_id[] = $liv;
    if(!isset($ar_r[$liv])){
        $ar_new= array();
        $ar_new['articolo'] = trim($row['ART']);
        $ar_new['descrizione'] = trim($row['DART']);
        $ar_new['barcode']    = trim($row['BC']);
        $ar_new['ubicazione'] = trim($row['UBIC']);
        $ar_new['magazzino']     = "[".trim($row['DEPO'])."] ".$ta0[0]['text'];
        $ar_new['ordine_forn'] = trim($ord_acq_ar['ordine_forn']);
        $ar_new['ordine_clie'] = trim($ord_cli_ar['ordine_clie']);
        $ar_new['vsriferimento'] = trim($ord_cli_ar['VSRF_CLI']);
        $ar_new['cliente'] ='';
        if(trim($ord_cli_ar['CCON_CLI']) != '')
          $ar_new['cliente'] = "[".trim($ord_cli_ar['CCON_CLI'])."]".trim($ord_cli_ar['DCON_CLI']);

        $ar_liv0['contatore'] ++;
        $ar_liv1['contatore'] ++;
        $ar_r[$liv]=$ar_new;
    } 
            
    $ar_r=&$ar_r[$liv];
    $ar_r['giacenza'] = $row['GIAC'];
            
            
}
        
?>


<html>
 <head>

  <style>

   #logocliente {
     padding-top: 80px; 
     background-image: url(<?php echo "'" . "http://" . $_SERVER['HTTP_HOST'] . ROOT_PATH .  'personal/logo_cliente.png' . "'"; ?>); 
     background-repeat: no-repeat; background-size: auto 70px;
    }

  
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   	
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
 
   .cella_gray{background-color: #D1CCBF;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 12px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #DDDDDD;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}  
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
  table.intest td{border: 0px;} 
 
    
   tr.ag_liv_data th{background-color: #cccccc; color: white;}
   .grassetto th{font-weight: bold;}
    .box{width: 250px;
   		 height: 150;
   		 padding:10px;
   		 border: 1px solid;
   		 
   	  } 
   
    div#my_content h1{font-size: 22px; padding: 10px;}
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
   @media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
 @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 
<?php

echo "<div id='my_content'>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<h1>Ubicazioni per articolo dal ". print_date($row['data_partenza']) ."</h1>";


echo "<table class=int1>";

echo "<tr class='liv_data'>";
echo "<th>Ubicazione</th>";
echo "<th>Articolo</th>";
echo "<th>Descrizione</th>";
echo "<th>Barcode</th>";
echo "<th>Magazzino</th>";
echo "<th>Quantit&agrave;</th>";
echo "<th>Ord.forn./Prenot.merce</th>";
echo "<th>Ord.cliente</th>";
echo "<th>Vs.rif.</th>";
echo "<th>Cliente</th>";

foreach ($ar as $kar => $r){
    
    foreach ($r['children'] as $kar1 => $r1){
      if(trim($estrazione_dati_pivot) == false){
        if($stacco == 'UA'){
            echo   "<tr class = ag_liv2>
                    <td>".$r1['ubicazione']. "</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class=number>".n($r1['giacenza'])."</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    </tr>";
        } else { echo  "<tr  class=ag_liv2>
                        <td>&nbsp;</td>
                        <td>".$r1['articolo']. "</td>
                        <td>".$r1['descrizione']. "</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class=number>".n($r1['giacenza'])."</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        </tr>";
                }
     }
     
        foreach ($r1['children'] as $kar2 => $r2){
          if(trim($estrazione_dati_pivot) == false){
            if($stacco == 'UA'){
            echo "<tr  class=ag_liv0>
                  <td>".$r2['ubicazione']. "</td>
                  <td>".$r2['articolo']. "</td>
                  <td>".$r2['descrizione']. "</td>
                  <td>&nbsp;</td>
                  <td>".$r2['magazzino']. "</td>
                  <td class=number>".n($r2['giacenza'])."</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>";
            } else { echo "<tr  class=ag_liv0>
                           <td>".$r2['ubicazione']. "</td>
                           <td>&nbsp</td>
                           <td>&nbsp</td>
                           <td>&nbsp;</td>
                           <td>".$r2['magazzino']. "</td>
                           <td class=number>".n($r2['giacenza'])."</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           </tr>";
                    }
          }
                    
            if(trim($barcode) == 'Y'){
                foreach ($r2['children'] as $kar3 => $r3){
                   echo "<tr class=liv2>
                        <td>".$r3['ubicazione']."</td>
                        <td>".$r3['articolo']. "</td>
                        <td>".$r3['descrizione']. "</td>
                        <td>'".$r3['barcode']."</td>
                        <td>".$r3['magazzino']. "</td>
                        <td class=number>".n($r3['giacenza'])."</td>
                        <td>".$r3['ordine_forn']."</td>
                        <td>".$r3['ordine_clie']."</td>
                        <td>".$r3['vsriferimento']."</td>
                        <td>".$r3['cliente']."</td>
                        </tr>";
                }
            }
          if(trim($estrazione_dati_pivot) == false){
            echo "<tr><td colspan = 8>&nbsp;</td></tr>";
          }
        }
        //echo "<tr><td colspan = 8>&nbsp;</td></tr>";
    }
}

echo "</table>";
?>

</div>
</body>
</html>	

<?php 

exit;
}	