<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

ini_set('max_execution_time', 300000);

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
  div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   div.header_page h3{font-size: 18px; font-weight: bold;}
   div.header_page h2{font-size: 16px; font-weight: bold;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
   tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{font-weight: bold; font-size: 0.8em;} 
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD; font-weight: bold;} 
   tr.err td{background-color: #F4E287;}
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

function sum_columns_value(&$ar, $r, $t_caparre){

    if($r['TDINFI'] - $r['TDTOTD'] + $r['CAP_RC'] - $r['TOT_RC'] != 0 && ($t_caparre == 'Y' || ($r['TDINFI'] - $r['TDTOTD'] - $r['TFIMP5'] + $r['CAP_RC'] - $r['TOT_RC']) > 0)){
 
    $ar['importo'] += $r['TDINFI'];
    $ar['caparra'] += $r['TDINFI'] - $r['TDTOTD'] + $r['CAP_RC'] - $r['TOT_RC'];
    $ar['tfimp5'] += $r['TFIMP5'];
    $ar['cap_sca'] += $r['TDINFI'] - $r['TDTOTD'] - $r['TFIMP5'] + $r['CAP_RC'] - $r['TOT_RC'];
    
   }
    return $ar;
}


$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$sql_where.= sql_where_by_combo_value('TDCCON', $form_values->f_cliente_cod);
$sql_where.= sql_where_by_combo_value('TDSTAB', $form_values->f_sede);
$sql_where.= sql_where_by_combo_value('TDSTAT', $form_values->f_stato_ordine);


//controllo data ordine
if (strlen($form_values->f_data_ordine_da) > 0)
    $sql_where.= " AND TDODRE >= {$form_values->f_data_ordine_da}";
if (strlen($form_values->f_data_ordine_a) > 0)
    $sql_where.= " AND TDODRE <= {$form_values->f_data_ordine_a}";


$sql_where .= sql_where_by_combo_value('TDCARP', $form_values->f_tipo_pagamento);
    
$sql_where_tf.= sql_where_by_combo_value('TF2.TFCCON', $form_values->f_cliente_cod);
$sql_where_tf.= sql_where_by_combo_value('TF2.TFNAZI', $form_values->f_sede);
$sql_where_tf.= sql_where_by_combo_value('TF2.TFTPDO', $form_values->f_tipo_documento);



$sql_where_rc.= sql_where_by_combo_value('RC2.TFCCON', $form_values->f_cliente_cod);
$sql_where_rc.= sql_where_by_combo_value('RC2.TFNAZI', $form_values->f_sede);

$sql_where_rc_3.= sql_where_by_combo_value('RC3.TFCCON', $form_values->f_cliente_cod);
$sql_where_rc_3.= sql_where_by_combo_value('RC3.TFNAZI', $form_values->f_sede);

//controllo data
if (strlen($form_values->f_data_da) > 0)
   $sql_where_tf .= " AND TF2.TFDTRG >= {$form_values->f_data_da}";
if (strlen($form_values->f_data_a) > 0)
   $sql_where_tf .= " AND TF2.TFDTRG <= {$form_values->f_data_a}";
            
            
//controllo fattura(aperta/chiusa)
$filtro_res=$form_values->f_filtra_fatture;

if($filtro_res== "C"){
    $sql_where_tf.=" AND TF2.TFFG01 IN ('C','M')";
    $sql_where_rc.=" AND RC2.TFFG01 IN ('C','M')";
    $sql_where_rc_3.=" AND RC3.TFFG01 IN ('C','M')";
    
}else if ($filtro_res== "A"){
    
    $sql_where_tf.=" AND TF2.TFFG01 <>'C' AND TF2.TFFG01 <> 'M'";
    $sql_where_rc.=" AND RC2.TFFG01 <>'C' AND RC2.TFFG01 <> 'M'";
    $sql_where_rc_3.=" AND RC3.TFFG01 <>'C' AND RC3.TFFG01 <> 'M'";
}
        

if(strlen($form_values->f_data_a) > 0)
    $sql_where_rc_3 .= "AND RC3.TFDTRG > {$form_values->f_data_a}";
else
    $sql_where_rc_3 .= "AND RC3.TFDTRG > 99999999";

if(strlen($form_values->f_data_a) > 0)
    $sql_where_rc .= "AND RC2.TFDTRG <= {$form_values->f_data_a}"; 

$sql_t = "SELECT TD.*, TF.TFIMP5 AS TFIMP5, RC.TFTOTD AS TOT_RC, RC_C.TFTOTD AS CAP_RC
            FROM  {$cfg_mod_DeskPVen['file_testate']} TD
            /* FATTURE IN BASE A FILTRI UTENTE */            
            LEFT OUTER JOIN(
                SELECT SUM (TF2.TFIMP5) AS TFIMP5, TF2.TFDOAB
                FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF2
                WHERE TF2.TFDT = '{$id_ditta_default}' AND TF2.TFTIDO='VF' {$sql_where_tf}
                GROUP BY TF2.TFDOAB) TF
            ON TD.TDDOCU = TF.TFDOAB 
           /* RC IN STATO NC CHE VANNO A SCALARE LE CAPARRE */
            LEFT OUTER JOIN(
                SELECT SUM (RC2.TFTOTD) AS TFTOTD, RC2.TFDOAB
                FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} RC2
    	        WHERE RC2.TFDT = '{$id_ditta_default}' AND RC2.TFTIDO='VF' AND RC2.TFTPDO = 'RC' AND RC2.TFSTAT = 'NC'
                {$sql_where_rc}               
            GROUP BY RC2.TFDOAB) RC
            ON TD.TDDOCU = RC.TFDOAB
           /* RC CON DATA > FILTRO FATTURE PER AUMENTARE CAPARRA*/
            LEFT OUTER JOIN(
                SELECT SUM (RC3.TFTOTD) AS TFTOTD, RC3.TFDOAB
                FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} RC3
                WHERE RC3.TFDT = '{$id_ditta_default}' AND RC3.TFTIDO='VF' AND RC3.TFTPDO = 'RC' AND RC3.TFSTAT <> 'NC' 
            {$sql_where_rc_3}
            GROUP BY RC3.TFDOAB) RC_C
            ON TD.TDDOCU = RC_C.TFDOAB

            WHERE ". $s->get_where_std()."AND TDOTPD='MO'
            {$sql_where}
            ORDER BY TDCARP";
    
$stmt_t = db2_prepare($conn, $sql_t);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_t);



$sql_rc = " SELECT SUM(TFTOTD) AS CAP_RC
            FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
            WHERE TFTIDO='VF' AND TFTPDO = 'RC' AND TFDOAB = ?
            {$where}";

$stmt_rc = db2_prepare($conn, $sql_rc);
echo db2_stmt_errormsg();

while($row_t = db2_fetch_assoc($stmt_t)){
    
    /*$result = db2_execute($stmt_rc, array($row_t['TDDOCU']));
    $row_rc = db2_fetch_assoc($stmt_rc);
    $row_t['cap_rc'] = $row_rc['CAP_RC'];*/
    
    $ar_r2= &$ar2;
    
    $cod_liv1 = trim($row_t['TDCARP']);
  
    $liv=$cod_liv1;
    if(!isset($ar_r2[$liv])){
        $ar_new= array();
        $ar_new['id'] = $liv;
        $ar_new['codice']=$liv;
        $ar_new['task'] = $liv . ' - ' . $s->decod_std('TPCAP', $liv);
        
        
        $ar_r2[$liv]=$ar_new;
    }
    
    $ar_r2=&$ar_r2[$liv];
    sum_columns_value($ar_r2, $row_t, $_REQUEST['t_caparre']);
    
    
}
    
    $sql = "SELECT TD.*, TF.TFIMP5 AS TFIMP5, RC.TFTOTD AS TOT_RC, RC_C.TFTOTD AS CAP_RC
            FROM  {$cfg_mod_DeskPVen['file_testate']} TD
            /* FATTURE IN BASE A FILTRI UTENTE */                   
            LEFT OUTER JOIN(
                    SELECT SUM (TF2.TFIMP5) AS TFIMP5, TF2.TFDOAB
                    FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF2
    		        WHERE  TF2.TFDT = '{$id_ditta_default}' AND TF2.TFTIDO='VF' {$sql_where_tf}
                    GROUP BY TF2.TFDOAB) TF
                ON TD.TDDOCU = TF.TFDOAB
           /* RC IN STATO NC CHE VANNO A SCALARE LE CAPARRE */
            LEFT OUTER JOIN(
                SELECT SUM (RC2.TFTOTD) AS TFTOTD, RC2.TFDOAB
                FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} RC2
    	        WHERE RC2.TFDT = '{$id_ditta_default}' AND RC2.TFTIDO='VF' AND RC2.TFTPDO = 'RC' AND RC2.TFSTAT = 'NC' 
                {$sql_where_rc}               
            GROUP BY RC2.TFDOAB) RC
            ON TD.TDDOCU = RC.TFDOAB
           /* RC CON DATA > FILTRO FATTURE PER AUMENTARE CAPARRA*/
            LEFT OUTER JOIN(
                SELECT SUM (RC3.TFTOTD) AS TFTOTD, RC3.TFDOAB
                FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} RC3
                WHERE RC3.TFDT = '{$id_ditta_default}' AND RC3.TFTIDO='VF' AND RC3.TFTPDO = 'RC' AND RC3.TFSTAT <> 'NC'
            {$sql_where_rc_3}           
            GROUP BY RC3.TFDOAB) RC_C
            ON TD.TDDOCU = RC_C.TFDOAB
            WHERE ". $s->get_where_std()."AND TDOTPD='MO'
            {$sql_where} 
            ORDER BY TDSTAB, TDCARP, TDDCON";
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    
    while($row = db2_fetch_assoc($stmt)){
            
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        $cod_liv_tot = 'TOTALE';	//totale
        $cod_liv0 = trim($row['TDSTAB']);
        $cod_liv1 = trim($row['TDCARP']);
        $cod_liv2 = trim($row['TDDOCU']);
       
        
        //LIVELLO TOTALE
        $liv = $cod_liv_tot;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new['id'] = 'liv_totale';
            $ar_new['liv_cod'] = 'TOTALE';
            $ar_new['task'] = 'Totale generale';
            $ar_new['liv'] = 'liv_totale';
           
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        sum_columns_value($ar_r, $row, $_REQUEST['t_caparre']);
        
        $liv=$cod_liv0;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new= array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice']=$liv;
            $ar_new['task'] = $s->decod_std('START', trim($row['TDSTAB']));
          
            $ar_r[$liv]=$ar_new;
        }
        
        $ar_r=&$ar_r[$liv];
        sum_columns_value($ar_r, $row, $_REQUEST['t_caparre']);
        
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new= array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice']=$liv;
            $ar_new['task'] = $s->decod_std('TPCAP', $liv);
            $ar_new['cap_rc'] =  $row['CAP_RC'];
            
            $ar_r[$liv]=$ar_new;
        }
        
        $ar_r=&$ar_r[$liv];
        sum_columns_value($ar_r, $row, $_REQUEST['t_caparre']);
        
        
        //creo ordine
        $liv=$cod_liv2;
        $ar_r=&$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new= array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['codice']=$liv;
            $ar_new['cod_cli']  =  $row['TDCCON'];
            $ar_new['cliente'] = trim($row['TDDCON']);
            $ar_new['k_ord'] = trim($row['TDDOCU']);
            $ar_new['k_ordine'] = trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
            $ar_new['sede'] = trim($row['TDSTAB']);
            $ar_new['stato'] = trim($row['TDSTAT']);
            $ar_new['data']  	= trim($row['TDODRE']);
            $ar_new['tipo'] = trim($row['TDOTPD']);
            $ar_new['importo'] = $row['TDINFI'];
            $ar_new['tipo_caparra'] = $row['TDCARP'];
            $ar_new['caparra'] = $row['TDINFI'] - $row['TDTOTD'] - $row['TOT_RC'];
            $ar_new['tfimp5'] = $row['TFIMP5'];
            $ar_new['cap_sca'] = $row['TDINFI'] - $row['TDTOTD'] - $row['TOT_RC'] - $row['TFIMP5'];
            
            $ar_new['CAP_RC'] = $row['CAP_RC'];
            $ar_new['caparra'] += $ar_new['CAP_RC'];
            $ar_new['cap_sca'] += $ar_new['CAP_RC'];
            
            $ar_r[$liv]=$ar_new;
        }
       
    }
  
    
echo "<div id='my_content'>";

echo "
	<div class=header_page>";
if($_REQUEST['t_caparre'] == 'Y')
	echo "<H2>Riepilogo caparre</H2>";
else
    echo "<H2>Riepilogo caparre da scalare</H2>";
	echo "<div style=\"text-align: left;\">
	Ordini da: " .  print_date($form_values->f_data_ordine_da) . " a " .  print_date($form_values->f_data_ordine_a) . "
	- Fatture da: " .  print_date($form_values->f_data_da) . " a " .  print_date($form_values->f_data_a) . "
		</div>
	<div style=\"text-align: right;\">Data elaborazione: " .  Date('d/m/Y H:i') . " </div>
	</div>";
echo "<table class=int1>";

echo "<tr class='liv_data' >
        <th>Sede/Cliente/Tipo pagamento</th>
  		<th>Codice</th>
        <th>Data</th>  		
        <th>Ordine</th>
        <th>Stato</th>
		<th>Tipo</th>
        <th>Importo</th>
        <th>Caparra</th>
        <th>Caparra scalata</th>
		<th>Caparra da scalare</th>
        </tr>";

if(is_array($ar)){
    foreach ($ar as $kar => $r){
    
  
    echo "<tr class=liv_totale><td>".$r['task']. "</td>
   		  <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class=number>".n($r['importo'],2)."</td>
            <td class=number>".n($r['caparra'],2)."</td>
            <td class=number>".n($r['tfimp5'],2)."</td>
            <td class=number>".n($r['cap_sca'],2)."</td>
            </tr>";
    
    foreach ($ar2 as $kart => $rt){
        
        //se passo il parametro t_caparre = Y faccio vedere tutte le caparre, anche quelle pagate e chiuse
        if($rt['caparra'] != 0 && ($_REQUEST['t_caparre'] == 'Y' || $rt['cap_sca'] > 0)){
         
             if(trim($rt['task']) == '')
                 $task = 'Non definito';
             else
                 $task = $rt['task'];
              echo "<tr class = liv3><td>".$task. "</td>
           		    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class=number>".n($rt['importo'],2)."</td>
                    <td class=number>".n($rt['caparra'],2)."</td>
                    <td class=number>".n($rt['tfimp5'],2)."</td>
                    <td class=number>".n($rt['cap_sca'],2)."</td>
                    </tr>";
                }
 
    }

    
    foreach ($r['children'] as $kar1 => $r1){
        
       if($r1['caparra'] != 0 && ($_REQUEST['t_caparre'] == 'Y' || $r1['cap_sca'] > 0)){
            echo "<tr><td colspan = 10>&nbsp;</td></tr>";
            echo "<tr class = liv1>
                <td>".$r1['task']."</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class=number>".n($r1['importo'],2)."</td>
                <td class=number>".n($r1['caparra'],2)."</td>
                <td class=number>".n($r1['tfimp5'],2)."</td>
                <td class=number>".n($r1['cap_sca'],2)."</td>
                </tr>";
             
        }
        
        foreach ($r1['children'] as $kar2 => $r2){
            
           if($r2['caparra'] != 0 && ($_REQUEST['t_caparre'] == 'Y' || $r2['cap_sca'] > 0)){
                
                if(trim($r2['task']) == '')
                    $task = 'Non definito';
                else
                    $task = $r2['task'];
            
            echo "<tr class = liv0>
                <td>".$task."</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class=number>".n($r2['importo'],2)."</td>
                <td class=number>".n($r2['caparra'],2)."</td>
                <td class=number>".n($r2['tfimp5'],2)."</td>
                <td class=number>".n($r2['cap_sca'],2)."</td>
                </tr>";
    
            }
        foreach ($r2['children'] as $kar3 => $r3){
    
           // $tipo_paga_caparra = $s->get_TA_std('TPCAP', trim($r3['tipo_caparra']));
    
           if($r3['caparra'] != 0 && ($_REQUEST['t_caparre'] == 'Y' || $r3['cap_sca'] > 0)){
               
               	
                echo "<tr> <td>".$r3['cliente']."</td>";
                echo "  <td>".$r3['cod_cli']."</td>
                <td>".print_date($r3['data'])."</td>   			
                <td>".$r3['k_ordine']."</td>
                <td>".$r3['stato']."</td>
    			<td>".$r3['tipo']."</td>
    			<td class=number>".n($r3['importo'],2)."</td>
                <td class=number>".n($r3['caparra'],2)."</td>
                <td class=number>".n($r3['tfimp5'],2)."</td>
    		    <td class=number>".n($r3['cap_sca'],2)."</td>
            
   		  	</tr>";
    
    }
    
    }
    }
    }
}
}

?>
</div>
</body>
</html>	

