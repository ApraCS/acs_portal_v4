<?php

require_once("../../config.inc.php");
require_once("acs_panel_ins_new_anag_include.php");

ini_set('max_execution_time', 30000);

$deskGest = new DeskGest(array('abilita_su_modulo' => 'DESK_PVEN'));
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));
$main_module = new DeskPVen();
$m_params = acs_m_params_json_decode();
$sede = $main_module->get_stabilimento_in_uso();

function get_gcrow_by_prog($id_prog){
    global $conn, $cfg_mod_Gest, $id_ditta_default;
    
    $sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG
    FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
    LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
    TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
    LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
    TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
    WHERE GCDT='{$id_ditta_default}' AND GCPROG = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_prog));
    $row = db2_fetch_assoc($stmt);
    $row['GCTRAS'] = trim($row['GCTRAS']);
    $row['GCCITI'] = trim($row['GCCITI']);
    $row['GCSEL2'] = trim($row['GCSEL2']);
    //$row = array_map('rtrim', $row);
    return $row;
}


function get_ccrow_by_prog($id_prog){
    global $conn, $cfg_mod_Gest, $id_ditta_default;
    $sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC WHERE CCDT='{$id_ditta_default}' AND CCPROG=?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_prog));
    $row = db2_fetch_assoc($stmt);
    
    //des_banca
    if (strlen(trim($row['CCABI'])) > 0 && strlen(trim($row['CCCAB'])) > 0) {
        $sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($r['CCABI'], $r['CCCAB']));
        $r_cab = db2_fetch_assoc($stmt);
        $des_banca = implode(" - ", array(trim($r_cab['XDSABI']), trim($r_cab['XDSCAB'])));
        
        $row['des_banca'] = $des_banca;
    }
   
    return $row;
}


function ar_standard_alternativa(){
	$ret = array();
	$ret[] = array('id' => 'S', 'text' => 'Standard');
	$ret[] = array('id' => 'A', 'text' => 'Alternativa');
	return $ret;
}


function ar_giorno_scarico(){
	$ret = array();
	$ret[] = array('id' => 'X', 'text' => 'Si');
	$ret[] = array('id' => 'M', 'text' => 'Mattino');
	$ret[] = array('id' => 'P', 'text' => 'Pomeriggio');
	return $ret;
}

function insert_parametro_in_db($stmt, $ar_ins, $parametro, $contenuto, $code, $seq, $tipo_contenuto = ''){
	$ar_ins['PARAMETRO'] = acs_u8e($parametro);
	$ar_ins['CONTENUTO'] = acs_u8e($contenuto);
	$ar_ins['CODICE_CONTENUTO'] = acs_u8e($code);
	$ar_ins['TIPO_CONTENUTO'] = $tipo_contenuto;
	$ar_ins['ICSEQU'] 	 = $seq;
	db2_execute($stmt, $ar_ins); echo db2_stmt_errormsg($stmt);
}



function write_parametri_cerved_base($resp_json){
	global $auth, $conn, $cfg_mod_Gest;

	$ci = $resp_json->companyInfo[0];

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'BASE';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'companyName', $ci->companyName, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'street', $ci->address[0]->street, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'postCode', $ci->address[0]->postCode, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'province', $ci->address[0]->province, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'municipality', $ci->address[0]->municipality, $ci->address[0]->municipalityCode, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'activityStatus', $ci->activityStatusDescription, $ci->activityStatus, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'companyType', $ci->companyType, $ci->companyType, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'companyForm ', $ci->companyForm->description, $ci->companyForm->code, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'registrationDate ', strftime('%Y%m%d', strtotime($ci->registrationDate)), '', ++$seq, 'D');
	insert_parametro_in_db($stmt, $ar_ins, 'ateco07  ', $ci->ateco07Description, $ci->ateco07, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'reaCode  ', implode(' ', array($ci->reaCode->coCProvinceCode, $ci->reaCode->reano)), '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'taxCode  ', $ci->taxCode, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'vatRegistrationNo  ', $ci->vatRegistrationNo, '', ++$seq);

	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}




function write_parametri_cerved_details($resp_json){
	global $auth, $conn, $cfg_mod_Gest;

	$ci = $resp_json->companyInfo;

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'DETAILS';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'telephone', 	$ci->telephone, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'email', 		$ci->email, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'certifiedEmail',$ci->certifiedEmail, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'activityStartDate', strftime('%Y%m%d', strtotime($ci->activityStartDate)), '', ++$seq, 'D');
	insert_parametro_in_db($stmt, $ar_ins, 'paidUpEquityCapital', $ci->paidUpEquityCapital, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'sales', $ci->balanceDetails->sales, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'grossProfit', $ci->balanceDetails->grossProfit, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'profitAndLoss', $ci->balanceDetails->profitAndLoss, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'numberOfEmployes', $ci->balanceDetails->numberOfEmployes, '', ++$seq);

	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}





function write_parametri_cerved_scores($resp_json){
	global $auth, $conn, $cfg_mod_Gest;

	$ci = $resp_json;

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'SCORES';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'negativeEventsGrading', 	implode(', ', array(
			$ci->negativeEventsGrading->grading,
			$ci->negativeEventsGrading->descriptiveGradingSynthesisCode,
			$ci->negativeEventsGrading->subScoreClass
	)), $ci->negativeEventsGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'economicalFinancialGrading', 	implode(', ', array(
			$ci->economicalFinancialGrading->grading,
			$ci->economicalFinancialGrading->descriptiveGradingSynthesisCode,
			$ci->economicalFinancialGrading->subScoreClass
	)), $ci->economicalFinancialGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'paylineGrading', 	implode(', ', array(
			$ci->paylineGrading->grading,
			$ci->paylineGrading->descriptiveGradingSynthesisCode,
			$ci->paylineGrading->subScoreClass
	)), $ci->paylineGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'cervedGroupScore', 	implode(', ', array(
			$ci->cervedGroupScore->grading,
			$ci->cervedGroupScore->descriptiveGradingSynthesisCode,
			$ci->cervedGroupScore->icon
	)), $ci->cervedGroupScore->available, ++$seq);


	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}




function get_request_data($key_request){
	global $conn, $cfg_mod_Gest;
	$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_cerved']}
			LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']}
			ON PARAMETRO = TAMAIL AND TATAID = 'CRVDS'
			WHERE CODICE_RICHIESTA = ?";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($key_request));

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		$n = array();
		$n['parametro'] = acs_u8e(trim($row['PARAMETRO']));
		$n['des_parametro'] = acs_u8e(trim($row['TADESC']));
		$n['contenuto'] = acs_u8e(trim($row['CONTENUTO']));
		$n['codice_contenuto'] = acs_u8e(trim($row['CODICE_CONTENUTO']));
		$n['tipo_contenuto'] = acs_u8e(trim($row['TIPO_CONTENUTO']));
		$ret[] = $n;
	}

	return $ret;
}

// ******************************************************************************************
// IMPORT ANAGRAFICA DA GEST
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_import_anag'){
   
    if (isset($m_params->gccdcf))
        $cod_cli = sprintf("%09d", $m_params->gccdcf);
    else
        $cod_cli = sprintf("%09d", $m_params->form_values->f_cliente_cod);
            
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory();
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'EXP_ANAG_CLI',
            "vals" => array(
                "RICLIE" => $cod_cli
            )
        )
        );
            
    $ret['id_prog'] = (int)$return_RI['RIPROG'];
    echo acs_je($ret);
    exit;
            
}

// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_ricalcola_iban'){
	
	require_once(dirname(__FILE__) . '/../../utility/php-iban-2.5.9/php-iban.php');
	
	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->form_values;
	
	//costruisco iban neutro
	$iban = "IT";
	$iban .= sprintf("%-2s", 'XX'); //CIN IBAN
	$iban .= sprintf("%-1s", 'Y'); //CIN BBAN
	$iban .= sprintf("%05d", $form_values->CCABI);
	$iban .= sprintf("%05d", $form_values->CCCAB);
	
	if (strlen(trim($form_values->CCCOCO)) < 12)
		$iban .= sprintf("%012d", $form_values->CCCOCO);
	else	
		$iban .= trim($form_values->CCCOCO);
	
	$iban = iban_set_nationalchecksum($iban);
	
	$ret = array();
	$ret['success'] = true;
	$ret['iban'] = $iban;
	
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verify_set_localita'){
	$m_params = acs_m_params_json_decode();

	$sql = "SELECT TA_COM.*, TA_REG.TADESC AS des_reg FROM {$cfg_mod_Gest['file_tabelle']} TA_COM			
				LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
					TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'	
				WHERE TA_COM.TATAID = 'ANCOM' AND TA_COM.TAKEY4 = '{$cfg_mod_Gest['cod_naz_italia']}' AND TA_COM.TAKEY2 = ? AND TA_COM.TADESC = ?";		
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->provincia, $m_params->comune));
	$row = db2_fetch_assoc($stmt);
	$ret = array();

	if ($row) {
		$ret['success'] = true;
		$ret['data'] = $row;
	}
	else
		$ret['success'] = false;
	
	echo acs_je($ret);
 exit;
}

// ******************************************************************************************
// VIEW GRID RESPONSE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'view_grid_response'){
	$m_params = acs_m_params_json_decode();	
	?>
	{"success":true, "items": [
			{
				xtype: 'grid',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
				store: {
						xtype: 'store',
						autoLoad:true,
						data: <?php echo acs_je(get_request_data($m_params->key_request)); ?>,
			        	fields: ['parametro', 'des_parametro', 'contenuto', 'codice_contenuto', 'tipo_contenuto']
				}, //store
		
				columns: [	
		    		  {text: 'Parametro', width: 90, 	dataIndex: 'des_parametro'}
		    		, {text: 'Contenuto', flex: 1, 		dataIndex: 'contenuto', renderer: function (value, metaData, rec){
		    		
		    				if (rec.get('tipo_contenuto') == 'D')
		    					return date_from_AS(value);
		    		
		    				if (Ext.isEmpty(rec.get('codice_contenuto')) == false){
		    					return '[' + rec.get('codice_contenuto') + '] ' + value;
		    				}
		    				 else return value;		    				
		    			}
		    		  }
				],
				enableSort: false

			, buttons: [
			
			<?php if ($m_params->view_type == 'base') { ?>		
							
						{
							xtype: 'button',
				            text: 'Dettagli', 
				            iconCls: 'icon-folder_open-32', scale: 'large',
				            handler: function() {
				            
								Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
										timeout: 2400000,
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved_by_company',
								        jsonData: {
								        	view_type: 'details',
								        	companyId: <?php echo $m_params->companyId; ?>
								        },	
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        						        
											Ext.getBody().unmask();
								            var jsonData = Ext.decode(result.responseText);
								            
											acs_show_win_std('Cerved response - Details', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
													{
														view_type: 'details',
										  				key_request: jsonData.key_request,
										  				companyId: jsonData.companyId
										  			}, 750, 550, {}, 'icon-globe-16');	
		
								        },
								        failure    : function(result, request){
											Ext.getBody().unmask();							        
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	
				            
				            }
				        }
				        
				        
				        
				        , {
							xtype: 'button',
				            text: 'Scores', 
				            iconCls: 'icon-certificate-32', scale: 'large',
				            handler: function() {
				            
								Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
										timeout: 2400000,
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved_by_company',
								        jsonData: {
								        	view_type: 'scores',
								        	companyId: <?php echo $m_params->companyId; ?>
								        },	
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        						        
											Ext.getBody().unmask();
								            var jsonData = Ext.decode(result.responseText);
								            
											acs_show_win_std('Cerved response - Scores', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
													{
														view_type: 'scores',
										  				key_request: jsonData.key_request,
										  				companyId: jsonData.companyId
										  			}, 750, 550, {}, 'icon-globe-16');	
		
								        },
								        failure    : function(result, request){
											Ext.getBody().unmask();							        
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	
				            
				            }
				        }
				        
				        , {
							xtype: 'button',
				            text: 'Copia intestazione', 
				            iconCls: 'icon-keyboard-32', scale: 'large',
				            handler: function() {
				            
				            	t_win = this.up('window');
				            	from_win = Ext.getCmp(<?php echo j($m_params->from_window_id); ?>);
				            	form = from_win.down('form').getForm();
				            
				            	this.up('grid').store.each(function(rec)  
								{
								  
								  if (rec.get('parametro') == 'taxCode') form.findField('GCCDFI').setValue(rec.get('contenuto'));
								  
								  if (rec.get('parametro') == 'companyName') form.findField('GCDCON').setValue(rec.get('contenuto'));  
								  if (rec.get('parametro') == 'street') form.findField('GCINDI').setValue(rec.get('contenuto'));
								  if (rec.get('parametro') == 'postCode') form.findField('GCCAP').setValue(rec.get('contenuto'));
								  
								  //if (rec.get('parametro') == 'municipality') form.findField('GCTEL').setValue(rec.get('contenuto'));								  								  						           
								  //if (rec.get('parametro') == 'municipality') form.findField('GCLOCA').setValue(rec.get('contenuto'));								  								  						            								  								  						           						            						            						            								  
								  
								  if (rec.get('parametro') == 'municipality') 	tmp_comune = rec.get('contenuto');
								  if (rec.get('parametro') == 'province') 		tmp_provincia = rec.get('contenuto');
								  
								},this);
								
								Ext.Ajax.request({
								   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=verify_set_localita',
								   method: 'POST',
								   jsonData: {
								   		comune: tmp_comune,
								   		provincia: tmp_provincia
								   	}, 
								   
								   success: function(response, opts) {
								   	  Ext.getBody().unmask();
									  var jsonData = Ext.decode(response.responseText);									   	  
     								  if (jsonData.success == true) {
     								  	form.findField('GCLOCA').setValue(tmp_comune);
     								  	form.findField('GCPROV').setValue(tmp_provincia);
     								  	form.findField('GCNAZI').setValue(<?php echo j($cfg_mod_Gest['cod_naz_italia']); ?>);
     								  	form.findField('v_regione').setValue(jsonData.data.DES_REG);
     								  	t_win.close();
     								  }	else {
     								  	acs_show_msg_error('Localit&agrave;/Provincia non presente in anagrafica');
     								  }
								   }, 
								   failure: function(response, opts) {
								      Ext.getBody().unmask();
								      alert('error');
								   }
								});									
									
				            }
				           }				        
				        
				        
				        
				 <?php } ?>
				        
				    ]
											    
				    		
	 	}
	]
   }
	
	<?php
	exit;
}





// ******************************************************************************************
// CERVED
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved'){
$m_params = acs_m_params_json_decode();

$service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";

$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $service_url,
		CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_USERAGENT => 'Codular Sample cURL Request'
));
// Send the request & save response to $resp
$resp = curl_exec($curl);

$resp_json = json_decode($resp);
$token = $resp_json->access_token;
$token_type = $resp_json->token_type;

// Close request to clear up some resources
curl_close($curl);

$taxCode = $m_params->form_values->GCPIVA; 

//RICHIEDO INFO AZIENDA
$service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies?&taxCode={$taxCode}&registered=N";

$headr = array();
$headr[] = 'Accept: application/json';
$headr[] = 'Authorization: '. $token_type . ' ' . $token;

$curl = curl_init();

// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $service_url,
		CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_USERAGENT => 'php curl',
		//CURLOPT_HEADER => true, //per debug
		CURLINFO_HEADER_OUT => true,
		CURLOPT_HTTPHEADER => $headr
));

// Send the request & save response to $resp
$resp = curl_exec($curl);

$headerSent = curl_getinfo($curl, CURLINFO_HEADER_OUT );

$resp_json = json_decode($resp);

//scrivo i parametri ricevuti su IC0
$key_request = write_parametri_cerved_base($resp_json);

// Close request to clear up some resources
curl_close($curl);

$ret['success'] = true;
$ret['key_request'] = $key_request;
$ret['companyId'] = $resp_json->companyInfo[0]->companyId;
echo acs_je($ret);
exit;
}





// ******************************************************************************************
// CERVED (by company)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved_by_company'){
	ini_set('max_execution_time', 300000);	
	$m_params = acs_m_params_json_decode();

	$service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";

	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'Codular Sample cURL Request'
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	$token = $resp_json->access_token;
	$token_type = $resp_json->token_type;

	// Close request to clear up some resources
	curl_close($curl);

	$taxCode = $m_params->form_values->GCPIVA;

	//RICHIEDO INFO AZIENDA
	$service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies/{$m_params->companyId}/{$m_params->view_type}";

	$headr = array();
	$headr[] = 'Accept: application/json';
	$headr[] = 'Authorization: '. $token_type . ' ' . $token;

	$curl = curl_init();

	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'php curl',
			//CURLOPT_HEADER => true, //per debug
			CURLINFO_HEADER_OUT => true,
			CURLOPT_HTTPHEADER => $headr
	));

	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	
	//scrivo i parametri ricevuti su IC0
	if ($m_params->view_type == 'details')
		$key_request = write_parametri_cerved_details($resp_json);
	if ($m_params->view_type == 'scores')
		$key_request = write_parametri_cerved_scores($resp_json);			
	
	// Close request to clear up some resources
	curl_close($curl);
	
	$ret['success'] = true;
	$ret['key_request'] = $key_request;
	$ret['companyId'] = $resp_json->companyInfo->companyId;	
	echo acs_je($ret);
	exit;
}
	

// ******************************************************************************************
// tree grid data
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    
    $sql_where = sql_where_params($m_params->form_ep);
    
    
    $area_manager = $m_params->form_ep->CCARMA;
    $agente = $m_params->form_ep->CCAGE;
    
    if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
        //elenco anagrafiche clienti
        
        
        if(isset($area_manager) && count($area_manager) > 0
        || isset($agente) && count($agente) > 0 ){
            
            $where = '';
            if(count($area_manager) > 0)
                $where .= sql_where_by_combo_value('CCARMA', $area_manager);
            if(count($agente) > 0){
                if(count($agente) == 1)
                    $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
                if(count($agente) > 1)
                    $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
            }
                
        $sql_join = " INNER JOIN (
                      SELECT CCPROG
                      FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
                      WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
                      ON CC.CCPROG = GC.GCPROG";
        }
        
        $tab_schede = $main_module->find_TA_std('SCCLI');
        $ar_schede = array();
        
        foreach($tab_schede as $v)
            $ar_schede[] = $v['id'];
        $sche_where = sql_where_by_combo_value('TA_SCHEDE.TAID' , $ar_schede);
            
        $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_tab_sys']} TA_SCHEDE
                                WHERE TA_SCHEDE.TADT = GC.GCDT AND TA_SCHEDE.TACOR1 = GC.GCCDCF {$sche_where}) AS NR_SCHEDE";
            
        $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_xspm']} TV
                            WHERE TV.TVDT = GC.GCDT AND digits(TV.TVCCON) = GC.GCCDCF) AS NR_XSPM";
            
        $sql = "SELECT GC.*, CF.* , TA.TAFG02 AS GRUPPO_PAGAMENTO {$sql_campi_select}
                FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
                ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
                LEFT OUTER JOIN {$cfg_mod_Gest['file_tabelle']} TA
                ON GC.GCDT = TA.TADT AND TA.TATAID = 'MODAN' AND GC.GCDEFA = TA.TAKEY1
                {$sql_join}
                WHERE GCFG01 <> 'A'
                AND GCDT = '{$id_ditta_default}' AND GCTPAN = 'CLI' {$sql_where}
                ORDER BY CFDTGE DESC
                LIMIT 50";
            
    }else{
        //elenco destinazioni per cliente
        $prog = $_REQUEST['node'];
        
        $rowProg = get_gcrow_by_prog($prog);
        
        
        //prima rieseguo un refresh per sicurezza
        $sh = new SpedHistory();
        $return_RI = $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'EXP_ANAG_CLI',
                "vals" => array(
                    "RICLIE" => $rowProg['GCCDCF']
                )
            )
            );
        
        
        $sql = "SELECT GC.*
                FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                WHERE 1=1 AND GCFG01 <> 'A'
                AND GCDT = '{$id_ditta_default}' AND GCTPAN IN ('DES') AND GCPRAB = {$prog}
                {$sql_where_des}
                ORDER BY GCTPAN ASC, GCTPIN ASC
                LIMIT 100";
        
        
        $sql_ta = "SELECT RRN(TA) AS RRN, TA.*
                   FROM {$cfg_mod_Gest['file_tab_sys']} TA
                   WHERE TADT = '{$id_ditta_default}' AND TAID = 'VUDE' AND TANR = ? AND TACOR1 = ?";
                   
        $stmt_ta = db2_prepare($conn, $sql_ta);
        echo db2_stmt_errormsg();
        
    }
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar = array();
    
    $sql_c = "SELECT COUNT(*) AS C_ROW
    FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
    WHERE GCDT = '{$id_ditta_default}' AND GCTPAN IN ('DES', 'PVEN', 'CLI222') AND GCPRAB = ?
    ";
    
    $stmt_c = db2_prepare($conn, $sql_c);
    echo db2_stmt_errormsg();
    
    while ($r = db2_fetch_assoc($stmt)) {
        
        $tmp_ar_id = array();
        
        
        $ar_r = &$ar;
        $cod_liv0 = trim($r['GCPROG']); 	//Progressivo
        
        $liv = $cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = array();
            $ar_new = $r;
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['liv_cod'] = $liv;
            $ar_new['liv_cod_out'] = $liv;
            $ar_new['task'] = acs_u8e($r['GCDCON']);
            $ar_new['omp'] = $r['GCGMTP'];
            //$ar_new['cliente'] = trim($r['GCCDCF']);
            if(trim($r['GCNAZI']) != ''){
                $naz = get_TA_sys('BNAZ', trim($r['GCNAZI']));
                $ar_new['d_nazione'] = trim($naz['text']);
            }
            if(trim($r['GCTPAN']) != 'DES'){
                $ha_commenti = $deskGest->has_commento_cliente($r['GCCDCF']);
                $ar_new['note'] =  $ha_commenti;
            }else{
                $ar_new['note'] =  "N";
            }
            $ar_new['cliente'] =  $r['GCCDCF'];
            //icona entry (per attivita' in corso sull'ordine)
            
            if(trim($r['GCCDFI']) == '')
                $ar_new['cod_fi'] =  $r['GCPIVA'];
                else
                    $ar_new['cod_fi'] =  $r['GCCDFI'];
                    if (in_array(trim($r['GCTPAN']), array('DES', 'PVEN'))){
                        $ar_new['leaf'] = true;
                        $result = db2_execute($stmt_ta, array($r['GCCDCF'], $rowProg['GCCDCF']));
                        $r_ta = db2_fetch_assoc($stmt_ta);
                        
                        $ar_new['data_ge'] = $r_ta['TADTGE'];
                        $ar_new['data_um'] = $r_ta['TADTUM'];
                        $ar_new['ute_ge'] = $r_ta['TAUSGE'];
                        $ar_new['ute_um'] = $r_ta['TAUSUM'];
                        $ar_new['rrn_ta'] = $r_ta['RRN'];
                        
                        $ar_new['sosp'] = trim($r_ta['TATP']);
                        
                        $ar_new['t_indi'] = trim($r['GCTPIN']);
                        if($ar_new['t_indi'] == ''){
                            $row_d = $deskGest->get_TA_std('INDI', 'D');
                            $ar_new['d_indi'] = $row_d['TADESC'];
                        }else{
                            $row_d = $deskGest->get_TA_std('INDI', $ar_new['t_indi']);
                            $ar_new['d_indi'] = $row_d['TADESC'];
                        }
                        
                        $ar_new['crm1'] = trim($r['GCCRM1']);
                        
                        if(trim($r['GCTPDS']) == 'S')
                            $ar_new['des_std'] = trim($r['GCTPDS']);
                            
                            if(trim($r['GCTPAN']) == 'DES'){
                                
                                if($ar_new['t_indi'] == 'S' || $ar_new['t_indi'] == 'G'){
                                    $ar_new['iconCls'] = 'icon-home-16';
                                }elseif($ar_new['t_indi'] == 'N'){
                                    $ar_new['iconCls'] = 'icon-block_blue-16';
                                }elseif($ar_new['t_indi'] == 'A'){
                                    $ar_new['iconCls'] = 'icon-briefcase-16';
                                }elseif($ar_new['t_indi'] == 'P' || $ar_new['t_indi'] == 'E'){
                                    $ar_new['iconCls'] = 'icon-clienti-16';
                                }else{
                                    $ar_new['iconCls'] = 'icon-delivery-16';
                                }
                                
                                
                            }
                            $ar_new['liv'] = 'liv_4';
                            $ar_new['out_codice'] =  $r['GCCDCF'];
                            
                    }else{
                        $ar_new['data_ge'] = $r['CFDTGE'];
                        $ar_new['data_um'] = $r['CFDTUM'];
                        $ar_new['ute_ge'] = $r['CFUSGE'];
                        $ar_new['ute_um'] = $r['CFUSUM'];
                        $ar_new['g_paga'] = $r['GRUPPO_PAGAMENTO'];
                        $ar_new['schede'] = $r['NR_SCHEDE'] + $r['NR_XSPM'];
                        $ar_new['sosp'] = trim($r['GCSOSP']);
                        $ar_new['liv'] = 'liv_2';
                        $result = db2_execute($stmt_c, array($r['GCPROG']));
                        $row_c = db2_fetch_assoc($stmt_c);
                        if($row_c['C_ROW'] == 0){
                            $ar_new['leaf'] = true;
                            $ar_new['iconCls'] = 'icon-folder_grey-16';
                        }
                        
                        //prento tutto scadute e non
                        
                        if($m_params->form_ep->f_cond == 'Y'){
                            $condizioni = $deskGest->get_condizioni_commerciali_cliente('Y', $r, 'Y');
                            
                            $ar_new['c_cond'] = $condizioni['count'];
                            $ar_new['val_cond'] = $condizioni['max_val'];
                        }
                        
                        $sa = new SpedAssegnazioneClienti();
                        $title = "";
                        if ($sa->ha_entry_aperte_per_cliente(trim($r['GCCDCF']))){
                            $img_entry_name = "icone/16x16/arrivi_gray.png";
                            $title = '<span style="display: inline; float: right; padding-right: 3px; padding-top: 2px;"><img class="cell-img" src=' . img_path($img_entry_name) . '></span>';
                        }
                        
                        $ar_new['out_codice'] = $r['GCCDCF'].$title;
                        
                    }
                    
                    //Se presente recupero anagrafica collegata
                    if($r['GCANCO'] > 0){
                        $sqlAC = "SELECT *
                        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
                        WHERE GCDT = '{$id_ditta_default}' AND GCCDCF = '{$r['GCANCO']}'";
                        $stmtAC = db2_prepare($conn, $sqlAC);
                        echo db2_stmt_errormsg();
                        $result = db2_execute($stmtAC);
                        $rowAC = db2_fetch_assoc($stmtAC);
                        $ar_new['tooltip_anagrafica_collegata'] = trim($rowAC['GCDCON']) . " - " . $r['GCANCO'];
                    }
                    
                    
                    
                    $ar_r["{$liv}"] = $ar_new;
        }
        
        
        $ar_r = &$ar_r["{$liv}"];
        $ar_liv2 = &$ar_r;
        
    }
    
    $ret = array();
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    if ($_REQUEST['node'] != '' &&$_REQUEST['node'] != 'root'){
        //$ret = $ret[0]['children'][0]['children'][0]['children'];
        $ar= $ar[$_REQUEST['node']]['children'];
    }
    
    
    
    echo acs_je(array('success' => true, 'children' => $ret));
    
    exit;
} //get_json_data




// ******************************************************************************************
// EXE SAVE NEW ANAGRAGICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_new_anag'){
	
	$m_params = acs_m_params_json_decode();	
	
	$ar_ins = array();
	$ret = array();
	
	$ar_ins['GCUSUM'] = trim($auth->get_user());
	$ar_ins['GCDTUM'] = oggi_AS_date();
	$ar_ins['GCORUM'] = oggi_AS_time();	

	$r_comune  	= $s->get_TA_std('ANCOM', trim($m_params->form_values->GCLOCA));
	$r_regione 	= $s->get_TA_std_by_des('ANREG', trim($m_params->form_values->v_regione));
	$r_prov 	= $s->get_TA_std('ANPRO', trim($m_params->form_values->GCPROV));
	
	$ar_ins['GCUSGE'] = substr(trim($auth->get_user()), 0, 8);
	$ar_ins['GCDTGE'] = oggi_AS_date();
	$ar_ins['GCORGE'] = oggi_AS_time();	
	
	$ar_ins['GCCITI'] = $main_module->get_stabilimento_in_uso();
	$ar_ins['GCAG1'] = $main_module->get_stabilimento_in_uso();
	
	$ar_ins['GCDCON'] 	= utf8_decode(trim($m_params->form_values->GCDCON));
	$ar_ins['GCINDI'] 	= utf8_decode(trim($m_params->form_values->GCINDI));
	$ar_ins['GCCAP'] 	= trim($m_params->form_values->GCCAP);
	
	
	$ar_ins['GCTFIS'] 	= trim($m_params->form_values->GCTFIS);
	$ar_ins['GCCOGN'] 	= utf8_decode(trim($m_params->form_values->GCCOGN));
	$ar_ins['GCNOME'] 	= utf8_decode(trim($m_params->form_values->GCNOME));
	
	
	
	$ar_ins['GCKMIT'] 	= sql_f(trim($m_params->form_values->GCKMIT));
	
	//$ar_ins['GCLOCA'] 	= trim($r_comune['TADESC']);
	$ar_ins['GCLOCA']	= utf8_decode(trim($m_params->form_values->GCLOCA));
	$ar_ins['GCPROV'] 	= utf8_decode(trim($m_params->form_values->GCPROV)); //trim($r_comune['TAKEY2']);
	$ar_ins['GCNAZI'] 	= utf8_decode(trim($m_params->form_values->GCNAZI));
	$ar_ins['GCCISN'] 	= trim($r_comune['TAKEY4']);   //Nazione
	
	$ar_ins['GCZONA'] 	= trim($r_regione['TANAZI']);   //Zona
	//$ar_ins['GCCITI'] 	= trim($r_prov['TANAZI']);   	//Itinerario
	
	$ar_ins['GCPIVA'] 	= utf8_decode(trim($m_params->form_values->GCPIVA));
	$ar_ins['GCCDFI'] 	= utf8_decode(trim($m_params->form_values->GCCDFI));
	
	$ar_ins['GCMAIL'] 	= utf8_decode(trim($m_params->form_values->GCMAIL));
	$ar_ins['GCWWW'] 	= utf8_decode(trim($m_params->form_values->GCWWW));
	$ar_ins['GCMAI1'] 	= utf8_decode(trim($m_params->form_values->GCMAI1));
	$ar_ins['GCMAI2'] 	= utf8_decode(trim($m_params->form_values->GCMAI2));
	$ar_ins['GCMAI3'] 	= utf8_decode(trim($m_params->form_values->GCMAI3));
	$ar_ins['GCMAI4'] 	= utf8_decode(trim($m_params->form_values->GCMAI4));
	
	$ar_ins['GCSEL3'] 	= utf8_decode(trim($m_params->form_values->GCSEL3));   //tipologia
	$ar_ins['GCORIG'] 	= utf8_decode(trim($m_params->form_values->GCORIG));   //origine
	
	$ar_ins['GCTEL'] 	= utf8_decode(trim($m_params->form_values->GCTEL));	
	$ar_ins['GCTEL2'] 	= utf8_decode(trim($m_params->form_values->GCTEL2));
	$ar_ins['GCFAX'] 	= utf8_decode(trim($m_params->form_values->GCFAX));
	//FATTURAZIONE ELETTRONICA
	$ar_ins['GCCSDI'] 	= utf8_decode(trim($m_params->form_values->GCCSDI));
	$ar_ins['GCPEC'] 	= utf8_decode(trim($m_params->form_values->GCPEC));
	
	$ar_ins['GCIVES'] 	= utf8_decode(trim($m_params->form_values->GCIVES));
	
	//destinazione standard/alternativa
	$ar_ins['GCTPDS'] 	= utf8_decode(trim($m_params->form_values->GCTPDS));


	if ($m_params->form_values->mode == 'NEW') {
		$ar_ins['GCDT'] 	= $id_ditta_default;
		$ar_ins['GCPROG'] 	= $s->next_num('ANAG_GC');
		
		if ($m_params->form_values->tipo_anagrafica == 'DES'){
			$ar_ins['GCTPAN'] 	= 'DES';
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else if ($m_params->form_values->tipo_anagrafica == 'PVEN'){
			$ar_ins['GCTPAN'] 	= 'PVEN';
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else {
			$ar_ins['GCTPAN'] 	= 'CLI';
			$ar_ins['GCDEFA'] 	= $m_params->open_request->modan;
		}
		
		
		//insert riga
		$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

	    $ret['success'] = true;
	    $ret['GCPROG'] = $ar_ins['GCPROG'];
	    
	    $ret['gc_row'] = get_gcrow_by_prog($ar_ins['GCPROG']);
		$ret['cc_row'] = get_ccrow_by_prog($ar_ins['GCPROG']);
	}
	

	if ($m_params->form_values->mode == 'EDIT') {
		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_ins,
				array($m_params->form_values->GCPROG)
				));
		echo db2_stmt_errormsg($stmt);
		
		$ret['success'] = true;
		$ret['GCPROG'] = $m_params->form_values->GCPROG;
		
	}
	
	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $ret['GCPROG'],
	            "RINOTE" => 'save_anag'
	        )
	    )
	    );
	
	if ($m_params->form_values->mode == 'NEW')
	    $prog = $ar_ins['GCPROG'];
    else
        $prog = $m_params->form_values->GCPROG;
	        
    $record = get_gcrow_by_prog($prog);
    
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'EXP_ANAG_CLI',
            "vals" => array(
                "RICLIE" => sprintf("%09d", $record['GCCDCF'])
            )
        )
        );
    
    $record = get_gcrow_by_prog($prog);
    $record = array_map('rtrim', $record);
    $ret['record'] = $record;	
    echo acs_je($ret);
    exit;
}




function exe_save_logis_add_field($ar, $m_params){
	$ar['GCLING'] 	= utf8_decode($m_params->form_values->GCLING);
	$ar['GCSEL3'] 	= utf8_decode($m_params->form_values->GCSEL3);
	$ar['GCCFGC'] 	= utf8_decode($m_params->form_values->GCCFGC);
	$ar['GCNAZO'] 	= utf8_decode($m_params->form_values->GCNAZO);
	$ar['GCFMNC'] 	= utf8_decode($m_params->form_values->GCFMNC);
	$ar['GCICON'] 	= utf8_decode($m_params->form_values->GCICON);
	$ar['GCIFAT'] 	= utf8_decode($m_params->form_values->GCIFAT);
	$ar['GCIPSP'] 	= utf8_decode($m_params->form_values->GCIPSP);
	$ar['GCGGS1'] 	= utf8_decode($m_params->form_values->GCGGS1);
	$ar['GCGGS2'] 	= utf8_decode($m_params->form_values->GCGGS2);
	$ar['GCGGS3'] 	= utf8_decode($m_params->form_values->GCGGS3);
	$ar['GCGGS4'] 	= utf8_decode($m_params->form_values->GCGGS4);
	$ar['GCGGS5'] 	= utf8_decode($m_params->form_values->GCGGS5);
	$ar['GCGGS6'] 	= utf8_decode($m_params->form_values->GCGGS6);
	$ar['GCNOSC'] 	= utf8_decode($m_params->form_values->GCNOSC);	
	return $ar;
}




function exe_save_cc_add_field($ar, $m_params){
	$ar['CCAG1'] 	= utf8_decode($m_params->form_values->CCAG1);
	$ar['CCAG2'] 	= utf8_decode($m_params->form_values->CCAG2);
	$ar['CCAG3'] 	= utf8_decode($m_params->form_values->CCAG3);
	$ar['CCPA1'] 	= sql_f($m_params->form_values->CCPA1);
	$ar['CCPA2'] 	= sql_f($m_params->form_values->CCPA2);
	$ar['CCPA3'] 	= sql_f($m_params->form_values->CCPA3);
	
	$ar['CCPAGA'] 	= utf8_decode(trim($m_params->form_values->CCPAGA));
	$ar['CCNGPA'] 	= sql_f($m_params->form_values->CCNGPA);
	$ar['CCMME1'] 	= sql_f($m_params->form_values->CCMME1);
	$ar['CCMME2'] 	= sql_f($m_params->form_values->CCMME2);
	$ar['CCGGE1'] 	= sql_f($m_params->form_values->CCGGE1);
	$ar['CCGGE2'] 	= sql_f($m_params->form_values->CCGGE2);
	$ar['CCTSIV'] 	= utf8_decode(trim($m_params->form_values->CCTSIV));
	
	$ar['CCREFE'] 	= utf8_decode(trim($m_params->form_values->CCREFE));
	$ar['CCLIST'] 	= utf8_decode(trim($m_params->form_values->CCLIST));
	$ar['CCCATR'] 	= utf8_decode(trim($m_params->form_values->CCCATR));
	$ar['CCSC1'] 	= sql_f($m_params->form_values->CCSC1);
	$ar['CCSC2'] 	= sql_f($m_params->form_values->CCSC2);
	$ar['CCSC3'] 	= sql_f($m_params->form_values->CCSC3);
	$ar['CCSC4'] 	= sql_f($m_params->form_values->CCSC4);
	
	if (strlen(trim($m_params->form_values->CCABI)) > 0)
		$ar['CCABI'] 	= sprintf("%05s", trim($m_params->form_values->CCABI));
	else
		$ar['CCABI'] = '';
	
	if (strlen(trim($m_params->form_values->CCCAB)) > 0)
		$ar['CCCAB'] 	= sprintf("%05s", trim($m_params->form_values->CCCAB));
	else
		$ar['CCCAB'] = '';

	if (strlen(trim($m_params->form_values->CCCOCO)) > 0)
		if (strlen(trim($m_params->form_values->CCCOCO)) < 12)	
			$ar['CCCOCO'] 	= sprintf("%012d", $m_params->form_values->CCCOCO);
		else
			$ar['CCCOCO'] = trim($m_params->form_values->CCCOCO);
	else	
		$ar['CCCOCO'] = '';
    
	if (strlen(trim($m_params->form_values->CCIBAN)) > 0)
	   $ar['CCIBAN'] = $m_params->form_values->CCIBAN;	
	
	
  return $ar;
}



function exe_save_fido_add_field($ar, $m_params){
	$ar['CCDTVI'] 	= $m_params->form_values->CCDTVI;
	$ar['CCDTVF'] 	= $m_params->form_values->CCDTVF;
	$ar['CCIMFD'] 	= sql_f($m_params->form_values->CCIMFD);

	return $ar;
}



// ******************************************************************************************
// EXE SAVE CONDIZIONE COMMERCIALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_cc'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	   
	   //verifiche in base al pagamento
	    $cfg_mod = $main_module->get_cfg_mod();
	    $sql = "SELECT * FROM {$cfg_mod['file_tab_sys']} TA WHERE TAID = ? AND TADT = ? AND TANR=? ORDER BY TADESC";
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    db2_execute($stmt, array('CUCP', $id_ditta_default, $m_params->form_values->CCPAGA));
      	$r_pag = db2_fetch_assoc($stmt);
      	/* come da richiesta del cliente, inibito il controllo 
      	if (trim($r_pag['TACINT']) == 'BON' && trim($m_params->form_values->CCIBAN)==''){
            $ret['success'] = false;
            $ret['error_msg'] = "IBAN obbligatorio per pagamenti di tipo bonifico";
            echo acs_je($ret);
            exit;
        } */
        
        /* come da richiesta del cliente, inibito il controllo 
        if (trim($m_params->form_values->CCABI)=='' || trim($m_params->form_values->CCCAB)==''){
            $ret['success'] = false;
            $ret['error_msg'] = "ABI / CAB obbligaotori";
            echo acs_je($ret);
            exit;
        } */
        
		
		//se non esiste la condizione standard la creo
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? AND CCSEZI='BCCO' AND CCCCON = 'STD'";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($id_ditta_default, $m_params->form_values->CCPROG));
		echo db2_stmt_errormsg($stmt);
		
		$row = db2_fetch_assoc($stmt);
		if (!$row){
			$ar_ins['CCUSGE'] = trim($auth->get_user());
			$ar_ins['CCDTGE'] = oggi_AS_date();
			$ar_ins['CCORGE'] = oggi_AS_time();
		
			$ar_ins['CCDT'] 	= $id_ditta_default;
			$ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
			$ar_ins['CCSEZI'] 	= 'BCCO';
			$ar_ins['CCCCON'] 	= 'STD';
		
			$ar_ins = exe_save_cc_add_field($ar_ins, $m_params);
			
						
			//insert riga
			$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
			echo db2_stmt_errormsg($stmt);
		}
		
		
		$ar_upd = exe_save_cc_add_field(array(), $m_params);
		
		$ar_upd['CCUSGE'] = trim($auth->get_user());
		$ar_upd['CCDTGE'] = oggi_AS_date();
		$ar_upd['CCORGE'] = oggi_AS_time();		
		
		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
				SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";		

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, 'BCCO', 'STD')
		));
		echo db2_stmt_errormsg($stmt);
		
		//call AGG tramite RI
		//NUOVA VERSIONE ATTRAVERSO RI
		$sh = new SpedHistory();
		$return_RI = $sh->crea(
		    'pers',
		    array(
		        "messaggio"	=> 'AGG_ANAG_CLI',
		        "vals" => array(
		            "RIPROG" => $m_params->form_values->CCPROG,
		            "RINOTE" => 'save_cc'
		        )
		    )
		    );

	$ret['success'] = true;
    echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// EXE SAVE FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_fido'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();



	if ($m_params->open_request->mode == 'NEW') {

		$ar_ins['CCUSGE'] = trim($auth->get_user());
		$ar_ins['CCDTGE'] = oggi_AS_date();
		$ar_ins['CCORGE'] = oggi_AS_time();

		$ar_ins['CCDT'] 	= $id_ditta_default;
		$ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
		$ar_ins['CCSEZI'] 	= 'BFID';
		$ar_ins['CCCCON'] 	= utf8_decode(trim($m_params->form_values->CCCCON));
		$ar_ins['CCDCON'] 	= utf8_decode(trim($m_params->form_values->CCDCON));

		$ar_ins = exe_save_fido_add_field($ar_ins, $m_params);

		//insert riga
		$sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}


	if ($m_params->open_request->mode == 'EDIT') {
		$ar_upd = exe_save_fido_add_field(array(), $m_params);

		$ar_upd['CCUSGE'] = trim($auth->get_user());
		$ar_upd['CCDTGE'] = oggi_AS_date();
		$ar_upd['CCORGE'] = oggi_AS_time();

		//update riga
		$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
		SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, $m_params->form_values->CCSEZI, $m_params->form_values->CCCCON)
		));
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}

	
	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $m_params->form_values->CCPROG,
	            "RINOTE" => 'save_fido'
	        )
	    )
	    );
	
	echo acs_je($ret);
	exit;
}

// ******************************************************************************************
// EXE SAVE FORM LOGISTICA/CONFIGURAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_logis'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	$ar_upd = exe_save_logis_add_field(array(), $m_params);
	
	$ar_upd['GCUSGE'] = trim($auth->get_user());
	$ar_upd['GCDTGE'] = oggi_AS_date();
	$ar_upd['GCORGE'] = oggi_AS_time();
	
	
	//update riga
	$sql = "UPDATE {$cfg_mod_Gest['ins_new_anag']['file_appoggio']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE GCDT='$id_ditta_default' AND GCPROG=?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($m_params->form_values->GCPROG)
	));
	echo db2_stmt_errormsg($stmt);

	$ret['success'] = true;
	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory();
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $m_params->form_values->GCPROG
	        )
	    )
	    );
	
	
	$record = get_gcrow_by_prog($m_params->form_values->GCPROG);
	$record = array_map('rtrim', $record);
	$ret['record'] = $record;
	
	echo acs_je($ret);
    exit;
}


// ******************************************************************************************
// ELENCO CONDIZIONI COMMERCIALI PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_cc'){

	$m_params = acs_m_params_json_decode();
	
	$ar_ins = array();
	$ret = array();

	$sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID FROM {$cfg_mod_Gest['file_tabelle']} TA
			LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}  AN ON
				CCDT = ? AND CCPROG = ? AND CCSEZI = 'BCCO' AND CCCCON = TAKEY2 
			WHERE TATAID = 'ANCCO' AND TAKEY1 = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id, $m_params->GCDEFA ));
	echo db2_stmt_errormsg($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$ret[] = $row;
	}

	echo acs_je($ret);
	exit;
}







// ******************************************************************************************
// ELENCO CONDIZIONI AMMINISTRATIVE PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_fido'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	$sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID FROM {$cfg_mod_Gest['file_tabelle']} TA
			LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}  AN ON
			CCDT = ? AND CCPROG = ? AND CCSEZI = 'BFID' AND CCCCON = TAKEY1
			WHERE TATAID = 'ANC05'";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id));
	echo db2_stmt_errormsg($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$ret[] = $row;
	}

	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// OPEN TAB
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){ 

	?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'hbox',
	    align: 'stretch',
	    pack : 'start',
	},
    <?php echo make_tab_closable(); ?>,	
    title: 'Customer POS',
    tbar: new Ext.Toolbar({
      items:[
            '<b>Inserimento/Manutenzione anagrafica clienti punto vendita</b>', '->',
            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').expandAll();}}
            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').collapseAll();}}
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
      ]            
    }),  
	
	
	items: [  
  		  <?php write_main_tree(array(), $sede, $m_params->form_open, $m_params->recenti);  ?>
  	    , <?php // write_main_form(array()); ?>
  	]
  }
 ]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// NUOVA ANAGRAFICA
// ******************************************************************************************
$m_params = acs_m_params_json_decode();
if ($_REQUEST['fn'] == 'new_anag_cli'){ ?>
{
 success:true,  
 items: [
  	{
		xtype: 'tabpanel',
		
		<?php
		$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
		
		//se rientro in modifica, ricarico il record		
		if ($all_params['mode'] == 'EDIT') {
		    $row = get_gcrow_by_prog($all_params['rec_id']);	
		}
		?>
			
		
		items: [
		
		   	<?php write_new_form_ITA(
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>, 	

		      	<?php write_new_form_COMM(
			   			array(),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())
			   	); ?>
		   	
		   	
		],listeners : {
		   afterrender: function (comp) {
		   <?php  
		   if($all_params['mode'] == 'EDIT'){
		       $nrow = get_gcrow_by_prog($row['GCPROG']);
		       $title = "[".$nrow['GCCDCF'].", ".trim($nrow['GCCINT'])."] ".trim($nrow['GCDCON']);
		       ?>
		       
	           comp.up('window').setTitle('Modifica anagrafica ' + <?php echo j($title); ?>);	 				
		 	<?php }?>
		 	
		 	},
		 	
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
		
		
		, acs_actions : {
		   
		   exe_afterSave: function (m_window){
		        var tabPanel = m_window.down('tabpanel');
		        var activeTab = tabPanel.getActiveTab();
                var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                tabPanel.setActiveTab(activeTabIndex + 1);
                
                if(activeTabIndex < tabPanel.items.length - 1){
                	tabPanel.setActiveTab(activeTabIndex + 1);
                } else {
                	m_window.close();
					acs_show_msg_info('Salvataggio completato');
                }
                
           }
		   
		   
		   
		   }
	}
 ]
}  	
<?php exit; } ?>
<?php
// ******************************************************************************************
// NUOVA DESTINAZIONE 
// ******************************************************************************************
if ($_REQUEST['fn'] == 'new_dest'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
	    /*$row = get_gcrow_by_prog($all_params['rec_id']);
	    $rowProg = get_gcrow_by_prog($row['GCPRAB']);
	    $row['prog_cli'] = $rowProg['GCPROG'];
	    $row['cod_cli'] = $rowProg['GCCDCF'];
	    $row['cod_des'] = trim($row['GCCDCF']);
	    $ret_des = $s->get_cliente_des_anag($id_ditta_default, $row['cod_cli'], trim($row['GCDSTA']));
	    $row['IND_D'] = $ret_des['IND_D'];
	    $row['LOC_D'] = $ret_des['LOC_D'];
	    $row['CAP_D'] = $ret_des['CAP_D'];
	    $row['PRO_D'] = $ret_des['PRO_D']; */
	}
	
	?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'DES'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>
		]
		
		, 	listeners : {
		   
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
		
	  , acs_actions : {
		   
		   exe_afterSave: function (m_window){
		        var tabPanel = m_window.down('tabpanel');
		        var activeTab = tabPanel.getActiveTab();
                var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                tabPanel.setActiveTab(activeTabIndex + 1);
                
                if(activeTabIndex < tabPanel.items.length - 1){
                	tabPanel.setActiveTab(activeTabIndex + 1);
                } else {
                	m_window.close();
					acs_show_msg_info('Salvataggio completato');
                }
                
           }
		   
		   
		   
		   }
	}
 ]
}  	
<?php exit; } ?>

<?php
if ($_REQUEST['fn'] == 'new_pven' && $m_params->i_e == 'I'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
	    $row = get_gcrow_by_prog($all_params['rec_id']);
	}
	
	
?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'PVEN'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>
		]
		, listeners : {
		   
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
	}
 ]
}  	
<?php exit; } ?>




<?php
// ******************************************************************************************
// OPEN FORM FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_FIDO'){ ?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'panel',
		layout: 'fit',
		items: [
				
			   	<?php write_new_form_FIDO(
			   			array(),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } ?>



<?php

/********************************************************************************** 
 WIDGET
 **********************************************************************************/
function write_main_tree($p, $sede, $form_ep, $recenti){
	global $s;	
	
?>	
			{
				xtype: 'treepanel',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					      fields: ['iconCls', 'trCls', 'task', 'liv', 'liv_data', 'liv_cod', 'liv_cod_out', 'liv_cod_qtip', 'd_indi',
					    	'GCDT', 'GCPROG', 'sosp', 'note', 'cliente', 'GCUSUM', 'GCUSGE', 'GCDTGE', 'GCDTUM', 'des_std', 'omp',
					    	'GCDCON', 'GCMAIL', 'GCWWW', 'GCSIRE', 'GCMAI1', 'GCMAI2', 'GCMAI3', 'GCMAI4', 'GCTEL', 'GCTEL2', 'GCFAX', 'out_codice',
					    	'GCINDI', 'GCCDCF', 'GCCDFI', 'GCPIVA', 'GCLOCA', 'GCDSTA', 'GCTPAN', 'GCPROV', 'GCCATC', 'GCIVES', 'GCCAPC', 'schede',
					    	'CFUSUM', 'CFUSGE', 'CFDTGE', 'CFDTUM', 'cod_fi', 'GCCRM1', 'GCCAP', 'GCNAZI', 'd_nazione', 'rrn_ta',
					    	'GCANCO', 'tooltip_anagrafica_collegata', 'data_ge', 'data_um', 'ute_ge', 'ute_um', 'c_cond', 'val_cond', 'g_paga'
					    ],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							actionMethods: {read: 'POST'},
							timeout: 240000,
	                        reader: {
					            type: 'json',
								method: 'POST',						            
					            root: 'children'						            
					        }, 
	                        extraParams: {
	                          form_ep : <?php echo acs_je($form_ep); ?>,
	                          recenti : <?php echo j($recenti); ?>
							}
	                	    , doRequest: personalizza_extraParams_to_jsonData         				
	                    }
	                }),
	
	            multiSelect: false,
		        singleExpand: false,
		        
		        <?php  $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
		        <?php  $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>"; ?>
		        <?php  $schede = "<img src=" . img_path("icone/48x48/tag.png") . " height=20>"; ?>
		
				columns: [	
				  {text: '<?php echo $sosp; ?>', width: 40, dataIndex: 'sosp', 
				  tooltip: 'Sospeso',
				  renderer: function(value, metaData, record){
		    			  if(record.get('sosp') == 'S'){ 
                              metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			      return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	   }else{
		    		 	   	  if(record.get('omp') == 'P')
		    		 	   	  return '<img src=<?php echo img_path("icone/48x48/gift.png") ?> width=18>';
		    		 	   }
					 }}
				   ,{
				    text: '<?php echo $note; ?>',
				    width: 32, 
				    tooltip: 'Blocco note',
				    tdCls: 'tdAction',  
				    dataIndex : 'note',       		       		        
					renderer: function(value, metaData, record){
    					if(record.get('liv') == 'liv_2'){
    						if (record.get('note') == true) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
    			    		if (record.get('note') == false) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
    			    	    if (record.get('note') == 'N') return '';
    			    	   
    			    	}
    		
			    	}
			    	}
			    	, {text: 'Tipo<br>Sede', tooltip : 'Tipo indirizzo', width: 40, dataIndex: 'iconCls',  xtype: 'treecolumn',
			    	   renderer: function(value, metaData, record){
			    	        if (record.get('des_std') == 'S') metaData.tdCls += ' sfondo_verde';			
			    	        return '';
                    	} }
                	, {text: 'Codice', width: 100, dataIndex: 'out_codice', tdCls: ' grassetto'}
                	, {text: '<?php  echo "<img src=" . img_path("icone/48x48/globe.png") . " height=20>"; ?>', width: 30, dataIndex: 'GCANCO', tooltip: 'Anagrafica collegata',
                    	 renderer: function(value, metaData, record){
                    	         if(record.get('GCANCO') > 0){
                        	          var q_tip = 'Anagrafica collegata:<br/> ' + record.get('tooltip_anagrafica_collegata');
                    			      metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';  	
                    	         }
                                 if(record.get('GCANCO') > 0)		    	
                    		     	return '<img src=<?php echo img_path("icone/48x48/globe.png") ?> width=18>';
                    		     else
                    		     	return '';
                    	}
                	}
                	, {text: '<?php echo $schede; ?>', 	
        			   width: 30, 
        			   dataIndex: 'schede',
        			   tooltip: 'Scheda',
        			   renderer: function(value, p, record){
        				  if(record.get('schede') > 0)
        	    			 return '<img src=<?php echo img_path("icone/48x48/tag.png") ?> width=15>';
        	    		 	
        	    	   }
	    		     }
                	,{text: '<?php  echo "<img src=" . img_path("icone/48x48/shopping_bag.png") . " height=20>"; ?>', width: 30, dataIndex: 'condizioni', tooltip: 'Condizioni commerciali',
                    	 renderer: function(value, metaData, record){
                    	      if(record.get('c_cond') > 0 && record.get('val_cond') >= '<?php echo oggi_AS_date(); ?>')
                    	       		return '<img src=<?php echo img_path("icone/48x48/shopping_bag.png") ?> width=18>';
                    	       if(record.get('c_cond') > 0 && record.get('val_cond') < '<?php echo oggi_AS_date(); ?>')
                    	       		return '<img src=<?php echo img_path("icone/48x48/shopping_bag_gray.png") ?> width=18>';
                    	}
                	}
		    		, {text: 'Ragione sociale/Denominazione', flex: 1, xtype: 'treecolumn', dataIndex: 'task', menuDisabled: true}
		        	, {text: 'Localit&agrave;', flex: 1, dataIndex: 'GCLOCA'}					
		    		, {text: 'Indirizzo', flex: 1, dataIndex: 'GCINDI'}
		    		, {text: 'Cap', width: 60, dataIndex: 'GCCAP'}
		    		, {text: 'Pr.', width: 40, dataIndex: 'GCPROV', tdCls: ' grassetto'}
		    		, {text: 'Naz.', width: 40, dataIndex: 'GCNAZI', tdCls: ' grassetto',
		    		   renderer: function(value, metaData, record){
    					      if (record.get('GCNAZI').trim() != '') 
                        	      metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('d_nazione')) + '"'; 
            	          	  
            	          	  return value;	
                    	}}
					, {text: 'Cod.Fiscale', width: 150, dataIndex: 'cod_fi',
    					 renderer: function(value, metaData, record){
    					      if (record.get('cod_fi').trim() != record.get('GCPIVA').trim()) 
                        	          	metaData.tdCls += ' grassetto';
            	          	  return value;	
                    	}}
					, {text: 'Partita IVA', width: 130, dataIndex: 'GCPIVA',
					     renderer: function(value, metaData, record){
					      if (record.get('GCPIVA').trim() != record.get('cod_fi').trim()) 
                    	          	metaData.tdCls += ' grassetto';
        	          	  if (record.get('GCPIVA').trim() == record.get('cod_fi').trim()) 
                    	          	metaData.tdCls += ' grassetto';
                          return value;	
                	}}
					, {header: 'Immissione',
                	 columns: [
                	 {header: 'Data', dataIndex: 'data_ge', renderer: date_from_AS, width: 70, sortable : true,
                	 renderer: function(value, metaData, record){
                	         if(record.get('data_um') > 0){
                    	         if (record.get('data_ge') != record.get('data_um')) 
                    	          	metaData.tdCls += ' grassetto';
                    	          	
                    	          q_tip = 'Modifica: ' + date_from_AS(record.get('data_um')) + ', Utente ' +record.get('ute_um');
                			      metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';  	
                	         }
                              			    	
                		     return date_from_AS(value);	
                	}
                	 
                	 }
                	 ,{header: 'Utente', dataIndex: 'ute_ge', width: 70, sortable : true,
                	  renderer: function(value, metaData, record){
                	  
                	  	
                	        if(!Ext.isEmpty(record.get('ute_um')) && record.get('ute_um').trim() != ''){
                	  			if (record.get('ute_ge') != record.get('ute_um')) 
                	          		metaData.tdCls += ' grassetto';
                	          		
                	          q_tip = 'Modifica: ' + date_from_AS(record.get('data_um')) + ', Utente ' +record.get('ute_um');
                			  metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';   		
                	        }  
                	  
                              			    	
                	    return value;	
                	} 
                	 }
                	 ]}
				],
				enableSort: true,
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
					    celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						     iEvent.stopEvent();
						     var col_name = iView.getGridColumns()[iColIdx].dataIndex,
						    	 rec = iView.getRecord(iRowEl),
	            				 my_tree = iView;
							  
							   my_listeners_upd_dest = {
	        					afterEditRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_tree.getTreeStore().load();		        						
	        						from_win.close();
					        		}, 
					        		
					             afterInsertRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_tree.getTreeStore().load();		        						
	        						from_win.close();
					        		} 
			    				};					  

						    
							if(col_name != 'note' && col_name != 'condizioni'){	
										
								if (rec.get('GCTPAN').trim() == 'DES') {
									acs_show_win_std('Scheda ' + rec.get('d_indi') + ' ['  + rec.get('GCCDCF').trim() +']', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 750, 550, {}, 'icon-globe-16');							
								 	return;
								}
								
								if (rec.get('GCTPAN').trim() == 'PVEN') {
									acs_show_win_std('Modifica anagrafica', 
										'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_pven', 
										{
							  				mode: 'EDIT',
							  				modan: 'ToDo',
							  				i_e: 'I', //ToDo
							  				rec_id: rec.get('GCPROG')
							  			}, 750, 550, {}, 'icon-globe-16');							
								 	return;
								}								
													  					
							 if(col_name == 'schede' && rec.get('GCTPAN').trim() == 'CLI'){
					   	     	acs_show_win_std('Seleziona scheda [' + rec.get('GCCDCF').trim() + '] ' + rec.get('task').trim(), 'acs_anag_cli_schede_aggiuntive_new.php?fn=open_form', {cliente: rec.get('cliente')}, 500, 400, null, 'icon-tag-16');		  					
							 	return false;
							 }						  					
													  					
							//nel caso di anagrafica cliente, prima richiedo aggiornamento della stessa
							if (rec.get('GCTPAN').trim() == 'CLI' && rec.get('GCCDCF').trim().length > 0){
						    	Ext.getBody().mask('Loading... ', 'loading').show();
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_import_anag',
									        timeout: 2400000,
									        jsonData: {
									        	gcprog: rec.get('GCPROG'),
									        	gccdcf: rec.get('GCCDCF').trim() 
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									        	Ext.getBody().unmask();
									            var jsonData = Ext.decode(result.responseText);
																	  						  	
												acs_show_win_std('Modifica anagrafica',  
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
													{
										  				mode: 'EDIT',
										  				modan: 'ToDo',
										  				i_e: 'I', //ToDo
										  				rec_id: jsonData.id_prog,
										  				gccdcf: rec.get('GCCDCF').trim(),
										  				g_paga : rec.get('g_paga')
										  			}, 750, 600, {}, 'icon-globe-16');						  
										  	
										  	return false;									            
									            									            
									        }, //success
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });  						    	
						    	
						    	return;
						    }							   					  					
													  					
													  					
													  						  	
								acs_show_win_std('Modifica anagrafica', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				i_e: 'I', //ToDo
						  				rec_id: rec.get('id'),
						  				gccdcf: rec.get('GCCDCF').trim()
						  			}, 650, 550, {}, 'icon-globe-16');						  
						  	
						  	return false;
						  	
						  	}
						  	
						  	if (rec.get('liv') == 'liv_2' && col_name == 'note')
								//show_win_bl_cliente(rec.get('cliente'), rec.get('task'));
						    
						    if (rec.get('liv') == 'liv_2' && col_name == 'condizioni'){
						         acs_show_win_std('Condizioni commerciali cliente ' + rec.get('GCDCON'), 
									'../desk_gest/acs_anag_cli_cc.php?fn=open_form', 
									{prog: rec.get('GCPROG')}, 800, 550, {}, 'icon-shopping_bag-16');
						    
						    }
						  				
						  }
					    },
			            
						itemclick: function(view, record, item, index, e) {
								//form = this.up('panel').down('form');
								//console.log(record);
								//form.loadRecord(record);
						},			            
			          
			          
			          itemcontextmenu : function(grid, rec, node, index, event) {			          				                          
			          }
			            	        
			        } //listeners
				

		, buttons: [
		 	/*{
							xtype: 'button',
				            text: 'Interrogazione/manutenzione anagrafica cliente', 
				            iconCls: 'icon-clienti-32', scale: 'large',
				            handler: function() {				            			
								acs_show_win_std('Ricerca cliente', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=search_anag_cli', 
									{tree_id: this.up('treepanel').id}, 650, 350, {}, 'icon-globe-16');
				            }
				},*/
		   { xtype: 'tbfill' },		
		
			, {
	         	xtype: 'splitbutton',
	            text: 'Crea nuova anagrafica',    //privato azienda
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	
		        	<?php 
		        	
		        	foreach ($s->find_TA_std('MODAP', null, 'N', 'Y') as $ec) {
		           		
		 				//print_r($ec);
		        		
		        		if(trim($ec['TARIF2']) != $sede)
		           			continue;
		           		
		        		?> 	
						{
							xtype: 'button',
				            text: <?php echo j(trim($ec['text'])) ?>,
				            //iconCls: 'icon-save-32', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										my_listeners_inserimento = {
				        					afterInsertRecord: function(from_win, record){	
				        							var values = record;
				        						    values['out_codice'] = record.GCCDCF;
    					                            values['task'] = record.GCDCON;
    					                            values['liv'] = 'liv_2';
    					                            values['iconCls'] = 'icon-folder_grey-16';
    					                            values['id'] = record.GCPROG;
    					                          	var rootNode = my_tree.getRootNode();
													rootNode.insertChild(0, values); 
												    var new_rec = my_tree.store.getNodeById(values['id']);
												    my_tree.getView().select(new_rec);
			  					   	     			my_tree.getView().focusRow(new_rec);
				        						//my_tree.store.load();	
								        		}
						    				};					            
										acs_show_win_std('Immissione nuova anagrafica cliente', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
											{
								  				mode: 'NEW',
								  				modan: <?php echo j(trim($ec['id'])) ?>,
								  				i_e: <?php echo j(trim($ec['TAFG01'])) ?>,
								  				g_paga: <?php echo j(trim($ec['TAFG02'])) ?>
								  			}, 750, 600, my_listeners_inserimento, 'icon-globe-16');
				            
				            }
				        },
				       <?php } ?>  
				            	        	
		        	]
		        }
	        }, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>destinazione',
	            iconCls: 'icon-button_black_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				                	
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}
				            			
				            		    var parent_prog = selected_record[0].get('id');		
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();		            							            			
									    
									    my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						//my_tree.store.load();		        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						/////from_win.close();
								        		}
						    				};			            							            			
										acs_show_win_std('Nuova destinazione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				parent_prog: parent_prog,
								  				type : 'D'
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            			         
						    		    selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'CLI'){
				            				acs_show_msg_error('Selezionare un singolo cliente dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
				            			var parent_prog = selected_record[0].get('id');		
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();		            							            			
									    
				            			
										my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						//my_tree.store.load();		        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						/////from_win.close();
								        		}
						    				};
						    							            							            			
										acs_show_win_std('Nuova destinazione', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				parent_prog: parent_prog,
								  				type : 'D'
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				}      
         ]
			        
			        
			        
			, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');

			           if (record.get('trCls')=='grassetto')
					        return ret + ' grassetto';
			           
			           if (record.get('trCls')=='G')
					        return ret + ' colora_riga_grigio';
			           if (record.get('trCls')=='Y')
					        return ret + ' colora_riga_giallo';					        
			           		        	
			           return ret;																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree



function write_main_form($p){
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            flex: 0.3,
            
            defaults:{ anchor: '-10' , xtype: 'textfield', labelAlign: 'top'},
            
            items: [
            	{name: 'GCDCON',	fieldLabel: 'Denominazione'},
            	{name: 'GCWW', 		fieldLabel: 'Sito web'},
            	{name: 'GCMAIL', 	fieldLabel: 'Email azienda'},
            	{name: 'GCMAI1', 	fieldLabel: 'Email - Conf. ordine'},
            	{name: 'GCMAI2', 	fieldLabel: 'Email - Fatt. vendita'},
            	{name: 'GCMAI3', 	fieldLabel: 'Email - Piani sped.'},
            	{name: 'GCMAI4', 	fieldLabel: 'Email - Resi'},
            	{name: 'GCTEL', 	fieldLabel: 'Telefono'},
            	{name: 'GCTEL2', 	fieldLabel: 'Telefono 2'},
            	{name: 'GCFAX', 	fieldLabel: 'Fax'},
            ],
			buttons: [],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_main_form







function write_new_form_ITA($p = array(), $request = array(), $r = array()){
	global $s, $main_module;
	if($p['tipo_anagrafica'] == 'DES'){
	    
	    if($request['mode'] == 'NEW'){
	        $gc_row = get_gcrow_by_prog($request['parent_prog']);
	        
	        $sql = "SELECT COUNT(*) AS R_STD
	        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
	        LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
	        ON TA.TADT = GC.GCDT AND TAID = 'VUDE' AND TANR = GC.GCCDCF AND TACOR1 = '{$gc_row['GCCDCF']}'
	        WHERE GCDT = '{$id_ditta_default}' AND GCPRAB = '{$request['parent_prog']}' AND GCTPDS = 'S'
	        /*AND GCPROG <> '{$gc_row['GCPROG']}'*/ AND TATP <> 'S'";
	        
	    }else{
	        $sql = "SELECT COUNT(*) AS R_STD
	        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
	        LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
	        ON TA.TADT = GC.GCDT AND TAID = 'VUDE' AND TANR = GC.GCCDCF AND TACOR1 = '{$r['cod_cli']}'
	        WHERE GCDT = '{$id_ditta_default}' AND GCPRAB = '{$r['prog_cli']}' AND GCTPDS = 'S'
	        AND GCPROG <> '{$r['GCPROG']}' AND TATP <> 'S'";
	        
	    }
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt);
	    $row = db2_fetch_assoc($stmt);
	    
	}
	
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '0 0 0 3',
            frame: true,
            title: 'Intestazione',
            autoScroll: true,
            
            flex: 0.7,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'mode',
	                	value: <?php echo j($request['mode']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'type',
	                	value: <?php echo j($request['type']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'tipo_anagrafica',
	                	value: <?php echo j($p['tipo_anagrafica']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'tipologia',
	                	value: <?php echo j($cfg_mod_Gest['cod_naz_italia']); ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'GCPROG',
	                	value: <?php echo j(trim($r['GCPROG'])); ?>	
                	}
                	
                	
<?php if (in_array($p['tipo_anagrafica'], array('DES'))) { ?>
					, <?php write_combo_std('GCTPDS', 'Tipo destinazione', $r['GCTPDS'], acs_ar_to_select_json(ar_standard_alternativa(), '', 'R'), array() ) ?>
<?php } ?>                	
                	
                	

                	, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						items: [
                	
                	
		                	{
		                		xtype: 'textfield', labelWidth: 130,
								name: 'GCDCON',
								flex: 1,
								fieldLabel: 'Denominazione',
							    maxLength: 100,
							    value: <?php echo j(trim(acs_u8e($r['GCDCON']))); ?>,
							    listeners:{
                                   change:function(field){
                                        field.setValue(field.getValue().toUpperCase());
                                   }
                               }								
							}, 
							
							<?php if($p['tipo_anagrafica'] != 'DES'){?>
							 {
										  
										xtype: 'displayfield',
										editable: false,
										fieldLabel: '',
										padding: '0 0 0 10',
									    value: <?php echo j("<img src=" . img_path("icone/48x48/user.png") . " width=16>"); ?>,
							}, {
		                            xtype: 'checkbox'
		                          , name: 'GCTFIS' 
		                          , boxLabel: 'Privato'
		                          , checked: <?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')) echo 'true'; else echo 'false'; ?>
		                          , inputValue: 'P'
		                       	  , listeners: {
								    change: function(checkbox, newValue, oldValue, eOpts) {
								    	var m_form = this.up('form');
								    	var fs_cognome_nome = m_form.down('#fs_cognome_nome');
								    	
			                    	    if (newValue == false){
			                    	    	fs_cognome_nome.hide();
			                    	    	m_form.getForm().findField('GCCOGN').disable();
			                    	    	m_form.getForm().findField('GCNOME').disable();
			                    	    	}
			                    	    else {
			                    	    	fs_cognome_nome.show();
			                    	    	m_form.getForm().findField('GCCOGN').enable();
			                    	    	m_form.getForm().findField('GCNOME').enable();
			                    	    }	
			                    	  
								    }
								}		                          
		                    }
		                    <?php }?>		    							
							
						]
					}, 	
					
					
					
					
					{
						xtype: 'fieldcontainer',
						itemId: 'fs_cognome_nome',
						
						<?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')){ ?>						 
							hidden: false,
						<?php } else { ?>
							hidden: true,
						<?php } ?>
						
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [					
							 {
								name: 'GCCOGN',
								fieldLabel: 'Cognome', labelWidth: 130,
							    maxLength: 40, allowBlank: false,
							    value: <?php echo j(trim(acs_u8e($r['GCCOGN']))); ?>,
								<?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')){ ?>						 
									disabled: false,
								<?php } else { ?>
									disabled: true,
								<?php } ?>
								 listeners:{
                                   change:function(field){
                                        field.setValue(field.getValue().toUpperCase());
                                   }
                               }	
							},{
								name: 'GCNOME',
								fieldLabel: 'Nome',labelAlign: 'right',
								maxLength: 40, allowBlank: false,
								value: <?php echo j(trim(acs_u8e($r['GCNOME']))); ?>,
								<?php if ($r['GCTFIS']=='P' || ($request['mode'] == 'NEW' && substr($request['modan'], 0, 2) == 'PR')){ ?>						 
									disabled: false,
								<?php } else { ?>
									disabled: true,
								<?php } ?>	
								 listeners:{
                                   change:function(field){
                                        field.setValue(field.getValue().toUpperCase());
                                   }
                               }															
							}
						]
					},
					
					
					
					
					
					{
									name: 'GCINDI',
									fieldLabel: 'Indirizzo',
									maxLength: 100,
									value: <?php echo j(trim(acs_u8e($r['GCINDI']))); ?>,
									listeners:{
                                   		change:function(field){
                                        field.setValue(field.getValue().toUpperCase());
                                   }
                               }								
							},
					 {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							  
								{
									xtype: 'fieldcontainer', flex: 3,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'GCLOCA', flex: 1,
											fieldLabel: 'Localit&agrave;', labelWidth: 130,
										    value: <?php echo j(trim(acs_u8e($r['GCLOCA']))); ?>,
										    listeners:{
                                               change:function(field){
                                                    field.setValue(field.getValue().toUpperCase());
                                               }
                                            },	
										    <?php if(trim($request['i_e']) == '' || $request['i_e'] == 'I'){?>
										    readOnly: true
										    <?php }?>
										}, 
										
										<?php if(trim($request['i_e']) == '' || $request['i_e'] == 'I'){?>
																				   
										{										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners_search_loc = {
									        					afterSelected: function(from_win, record_selected){
										        						m_form.findField('GCLOCA').setValue(record_selected.get('descr'));
										        						m_form.findField('GCPROV').setValue(record_selected.get('COD_PRO'));
										        						m_form.findField('v_regione').setValue(record_selected.get('DES_REG'));
										        						m_form.findField('GCNAZI').setValue(record_selected.get('COD_NAZ'));
										        						from_win.close();
														        		}										    
														    },
															
															
															acs_show_win_std('Seleziona localit&agrave;', 
																'search_loc.php?fn=open_search', 
																{}, 450, 150, my_listeners_search_loc, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
										
										<?php }?>
								  ]
								}							  
							  	
								
								
								
						        
						        , {
									name: 'GCCAP', width: 110, labelWidth: 70,
									fieldLabel: 'Cap', labelAlign: 'right',
								    maxLength: 5,
								    listeners:{
                                       change:function(field){
                                            field.setValue(field.getValue().toUpperCase());
                                       }
                                    },
								    value: <?php echo j(trim(acs_u8e($r['GCCAP']))); ?>
								}								    	
							
						]
					 }, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							  
								{
									xtype: 'fieldcontainer', flex: 3,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'v_regione',  flex: 1,
											fieldLabel: 'Regione',
										    labelWidth: 130,
										    listeners:{
                                               change:function(field){
                                                    field.setValue(field.getValue().toUpperCase());
                                               }
                                            },
										    value: <?php echo j(trim(acs_u8e($r['DES_REG']))); ?>,
										    <?php if(trim($request['i_e']) == '' || $request['i_e'] == 'I'){?>
										    readOnly: true
										    <?php }?>
										}
								  ]
								}							  
							  	
						        
						        , {
									name: 'GCPROV', width: 110, labelWidth: 70,
									fieldLabel: 'Prov.', labelAlign: 'right',
								    maxLength: 5,
								    listeners:{
                                       change:function(field){
                                            field.setValue(field.getValue().toUpperCase());
                                       }
                                    },
								    <?php if(trim($request['i_e']) == '' || $request['i_e'] == 'I'){?>
								    readOnly: true,
								    <?php }?>
								    value: <?php echo j(trim(acs_u8e($r['GCPROV']))); ?>,
							    	<?php if(trim($request['i_e']) == '' || $request['i_e'] == 'I'){?>
								    readOnly: true
								    <?php }?>
								}	
							
						]
					 },
					 
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [					
							 {
								name: 'GCNAZI',
								fieldLabel: 'Nazione', labelWidth: 130,
							    maxLength: 2,
							    listeners:{
                                       change:function(field){
                                            field.setValue(field.getValue().toUpperCase());
                                       }
                                    },
							    value: <?php echo j(trim(acs_u8e($r['GCNAZI']))); ?>,
							    <?php if(trim($request['i_e']) == '' || $request['i_e'] == 'I'){?>
							    readOnly: true
							    <?php }?>							
							},{
								name: 'GCTEL',
								fieldLabel: 'Telefono 1',labelAlign: 'right',
								maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCTEL']))); ?>							
							}
						]
					},{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [

							<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>						
							 {
								name: 'GCCDFI',
								fieldLabel: 'Codice fiscale',
							    maxLength: 100,
							    allowBlank: true,
							    listeners:{
                                       change:function(field){
                                            field.setValue(field.getValue().toUpperCase());
                                       }
                                    },
							    value: <?php echo j(trim(acs_u8e($r['GCCDFI']))); ?>							
							} ,
							<?php }
	                        if($p['tipo_anagrafica'] != 'DES'){?>
							 {
								name: 'GCTEL2',
								fieldLabel: 'Telefono 2', labelAlign: 'right',
							    maxLength: 20,
							    value: <?php echo j(trim(acs_u8e($r['GCTEL2']))); ?>
							}
							<?php }?>
						]
					},{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [

							<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>						
								{
								name: 'GCPIVA',
								fieldLabel: 'P. IVA',
							    maxLength: 100,
							    value: <?php echo j(trim(acs_u8e($r['GCPIVA']))); ?>							
							} ,
							<?php } 
	                        if($p['tipo_anagrafica'] != 'DES'){?>
							 {
								name: 'GCFAX',
								fieldLabel: 'Fax', labelAlign: 'right',
							    maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCFAX']))); ?>
							}
							<?php }?>
						]
					}
					
				   <?php if($p['tipo_anagrafica'] != 'DES'){?>
					,{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						//anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield'},
						items: [

							 {
								name: 'GCCSDI',
								fieldLabel: 'Codice SDI', 
								minLength: 7,
							    maxLength: 7,
							    width : 200,
								value: <?php echo j(trim(acs_u8e($r['GCCSDI']))); ?>
							},
							
							 {
    							name: 'GCIVES',
    							xtype: 'combo',
    			            	anchor: '-15',
    			            	flex : 1,
    			            	labelAlign: 'right',
				    			margin : '0 0 0 150',
    							fieldLabel: 'Aliquota IVA',
    							value: <?php echo j(trim(acs_u8e($r['GCIVES']))); ?>,
    							displayField: 'text',
    							valueField: 'id',
    							emptyText: '- seleziona -',
    							forceSelection: true,
    						   	allowBlank: true,
    							store: {
    								autoLoad: true,
    								editable: false,
    								autoDestroy: true,
    							    fields: [{name:'id'}, {name:'text'}],
    							    data: [
    							    <?php echo acs_ar_to_select_json(find_TA_sys('CUAE', null, null, null, null, null, 0, "", 'Y'), '') ?>
    								    ]
    							}
    				
    						}
						
						]
					}
					<?php }?>
					, {
						xtype: 'fieldset', title: 'E-mail',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						
							<?php if($p['tipo_anagrafica'] != 'DES'){?>
								{
									vtype: 'email',
									name: 'GCMAIL',
									fieldLabel: 'Principale',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAIL']))); ?>							
								}
								<?php }?>
								
								
<?php if (!in_array($p['tipo_anagrafica'], array('PVEN'))) { ?>								
								, {
									vtype: 'email',
									name: 'GCMAI1',
									fieldLabel: 'Contratto/Ordine',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI1']))); ?>							
								}
					<?php if (!in_array($p['tipo_anagrafica'], array('DES'))) { ?>								
								, {
									vtype: 'email',
									name: 'GCMAI2',
									fieldLabel: 'Fattura',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI2']))); ?>							
								}
					<?php } ?>
								
							
<?php } ?>			
					<?php if($p['tipo_anagrafica'] != 'DES'){?>
								,{
									vtype: 'email',
									name: 'GCPEC',
									fieldLabel: 'Indirizzo PEC',
								    maxLength: 120,
								    value: <?php echo j(trim(acs_u8e($r['GCPEC']))); ?>							
								}
								<?php }?>
			
								
						]
					  }
					  
					  
					  <?php if (!in_array($p['tipo_anagrafica'], array('DES'))) { ?>								
						
						
                         ,{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130},
						items: [
							  
							 <?php write_combo_std('GCSEL3', 'Tipologia', $r['GCSEL3'], acs_ar_to_select_json($main_module->find_TA_std('ANC01'), '', 'R') ) ?>
                           , <?php write_combo_std('GCORIG', 'Origine', $r['GCORIG'], acs_ar_to_select_json(find_TA_sys('BORI'), ''), array("labelAlign" => "right",  "allowBlank" => "false")  ) ?>							 
							
						]
					 }

						  , <?php write_textarea_std('GCNOTE', 'Note', trim($r['GCNOTE']), 240 ) ?>

								
							
					<?php } ?>
					  	
					
					
							
					
					
					
					],
					
					
					
					
		dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            fixed: true,
            items: [					
			{
		        xtype:'tbspacer',
		        flex:1
		    } 
	        , {
	            text: 'Gmap',
	            iconCls: 'icon-gmaps_logo-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	
	            	//costruisco indirizzo
	            	ind = 'VIA INSALA, 236 - 61122 - PESARO';
	            	
					//$ar[$liv1_v]["val"][$liv2_v]["gmap_ind"]	= implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));
					
					f = [];
					//f.push(form.findField('f_nazione').getValue());
					f.push(form.findField('GCCAP').getValue());
					f.push(form.findField('GCPROV').getValue());
					f.push(form.findField('GCLOCA').getValue());
					f.push(form.findField('GCINDI').getValue());
					
					km_field_id = form.findField('GCKMIT').id;	            	
	            	
					gmapPopup("../desk_vend/gmap_route.php?km_field_id=" + km_field_id + "&ind=" + f.join());	            	
	            	
	            }
	         }
/*	         
	         , {
	            text: 'Percorso',
	            iconCls: 'icon-pneumatico-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	
	            	//costruisco indirizzo
					f = [];
					//f.push(form.findField('f_nazione').getValue());
					f.push(form.findField('GCCAP').getValue());
					f.push(form.findField('GCPROV').getValue());
					f.push(form.findField('GCLOCA').getValue());
					f.push(form.findField('GCINDI').getValue());
					gmapPopup("../desk_vend/gmap_percorso_single.php?ind=" + f.join());	            	
	            	
	            }
	         }
*/	         
	         
			, {
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	
                        
                    form_values = form.getValues();
                    var cf = form_values.GCCDFI;
					
					var controllo = 0;
					 if (Ext.isEmpty(cf) == false) controllo =  ControllaCF(cf);
	            	
            	   if(controllo == '1'){
            	   		msg = 'Il codice fiscale deve contenere 16 tra lettere e cifre';
            	   		 Ext.Msg.alert('Message', msg);
            	    
            	   }else if(controllo == '2'){
            	   		msg = 'Il codice fiscale non &egrave; valido';
            	   		 Ext.Msg.alert('Message', msg);
            	   }else{
	            	   
	            	   // ************************
	            	   //In base a tipologia (se EFFETTIVO) rendo obbligatorio alcuni campi
	            	   //   (GCSEL3 decodificato da ANC01)
	            	   
	            	   if (form_values.GCSEL3 == '020'){	//Cliente Effettivo
	            	   	if (form_values.GCTFIS == 'P'){
	            	   		//PRIVATO
	            	   		//Obbligatorio: Cognome, Nome, CF, Indirizzo, Localita, CAP, Provincia, Nazione, Telefono
	            	   		if (
	            	   			Ext.isEmpty(form_values.GCCOGN) ||
	            	   			Ext.isEmpty(form_values.GCNOME) ||
	            	   			Ext.isEmpty(form_values.GCCDFI) ||
	            	   			Ext.isEmpty(form_values.GCINDI) ||
	            	   			Ext.isEmpty(form_values.GCLOCA) ||
	            	   			Ext.isEmpty(form_values.GCCAP) ||
	            	   			Ext.isEmpty(form_values.GCPROV) ||
	            	   			Ext.isEmpty(form_values.GCNAZI) ||
	            	   			Ext.isEmpty(form_values.GCTEL)
	            	   			){
	            	   			Ext.Msg.alert('Campi obbligatori non compilati', 'Per rendere Effettivo un cliente &egrave; obbligatorio indicare:<br/> Cognome, Nome, CF, Indirizzo, Localit&agrave;, CAP, Provincia, Nazione, Telefono');
	            	   			return false;
	            	   		}
	            	   	} else {
	            	   		//AZIENDA
	            	   		//Obbligatorio: Denominazione, CF, PIVA, Indirizzo, Localita, CAP, Provincia, Nazione, SDI o PEC, Telefono
	            	   		if (
	            	   			Ext.isEmpty(form_values.GCDCON) ||
	            	   			Ext.isEmpty(form_values.GCPIVA) ||	            	   				            	   			
	            	   			Ext.isEmpty(form_values.GCINDI) ||
	            	   			Ext.isEmpty(form_values.GCLOCA) ||
	            	   			Ext.isEmpty(form_values.GCCAP)  ||
	            	   			Ext.isEmpty(form_values.GCPROV) ||
	            	   			Ext.isEmpty(form_values.GCNAZI) ||
	            	   			Ext.isEmpty(form_values.GCTEL) ||
	            	   			(
	            	   				Ext.isEmpty(form_values.GCCSDI) && Ext.isEmpty(form_values.GCPEC) 
	            	   			)
	            	   			){
	            	   			Ext.Msg.alert('Campi obbligatori non compilati', 'Per rendere Effettivo un cliente &egrave; obbligatorio indicare:<br/> Denominazione, CF, PIVA, Indirizzo, Localit&agrave;, CAP, Provincia, Nazione, SDI o PEC, Telefono');
	            	   			return false;
	            	   		}	            	   		
	            	   	}	            	   		            	   	            	   
	            	   } //if cliente effettivo	            	   
	            	   // ************************
	            	   
	            	    panel_form.acs_actions.exe_form_submit(panel_form);  
	            	   }
	            	

					          	                	                
	            }
	        }	         
	         
	        ]// buttons
	       }
	      ]             
			
		  , acs_actions: {
	    	exe_form_submit: function(panel_form){
				var form = panel_form.getForm(),
				loc_win = panel_form.up('window');
		
         	   		  if(form.isValid()){
							  Ext.Ajax.request({
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_new_anag',
						        jsonData: {
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>,
						        	cod_cli : <?php echo j($r['cod_cli'])?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						            var jsonData = Ext.decode(result.responseText);
						            var record = jsonData.record;
						            var tabPanel = loc_win.down('tabpanel');
						            						            
						            loc_win.fireEvent('afterInsertRecord', loc_win, record);
						            
						            tabpanel_load_record_in_form(tabPanel, record);
						            
						            tabPanel.GCPROG = jsonData.GCPROG;
						            form.findField('mode').setValue('EDIT');
						            form.setValues(jsonData.gc_row);
						            form.findField('mode').resetOriginalValue(); //clear isDirty
						            
						            var title = 'Modifica anagrafica [' + record.GCCDCF + ', ' + record.GCCINT + '] ' +record.GCDCON;
						            loc_win.setTitle(title);
						            
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						            						            						            
									tabPanel.items.each(function(panelItem, index, totalCount){
										panelItem.setDisabled(false);
										
										//per aggiornare le condizioni commerciali: MIGLIORARE: TODO
										if (panelItem.xtype == 'grid'){
											panelItem.store.proxy.extraParams.GCDEFA = 'ITA';
											panelItem.store.proxy.extraParams.rec_id = jsonData.GCPROG;
											panelItem.store.proxy.extraParams.cdcf = record.GCCDCF;
											panelItem.store.load();
										}
										
											//per aggiornare le condizioni commerciali: MIGLIORARE: TODO
										if (panelItem.itemId == 'form_COMM'){
											panelItem.getForm().findField('CCPROG').setValue(jsonData.GCPROG);
											panelItem.getForm().setValues(jsonData.cc_row);
											panelItem.disabled = false;
										}
										
										
										
										if (panelItem.itemId == 'form_LOGIS'){
										  panelItem.getForm().findField('GCPROG').setValue(jsonData.GCPROG);
										  panelItem.getForm().setValues(jsonData.record)
										}	
									
										
									});
									
									tabPanel.acs_actions.exe_afterSave(loc_win);
									
						            	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    } //is valid		
		
			} //exe_form_submit
			
	    } //acs_actions				
			
				
				
        }
<?php
} //write_new_form_ITA



function write_new_form_COMM($p = array(), $request = array()){
	global $conn, $cfg_mod_Gest, $id_ditta_default;

	if ($request['mode'] == 'EDIT'){
		//recupero la riga da modificare
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? /*AND CCSEZI=? AND CCCCON=?*/";		
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($id_ditta_default, $request['rec_id']));
		//$result = db2_execute($stmt, array($id_ditta_default, $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));
		
		$ret = array();
		$r = db2_fetch_assoc($stmt);
		
		//des_banca
		if (strlen(trim($r['CCABI'])) > 0 && strlen(trim($r['CCCAB'])) > 0) {				
			$sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();		
			$result = db2_execute($stmt, array($r['CCABI'], $r['CCCAB']));
			$r_cab = db2_fetch_assoc($stmt);
			$des_banca = implode(" - ", array(trim($r_cab['XDSABI']), trim($r_cab['XDSCAB'])));
		}
		
		
	} else {
		
		
		//DALLA REGIONE PROVO A RECUPERARE IL REFERENTE (TATISP IN TATAID='ANREG')
		
			$sql = "SELECT TA_REG.TADESC AS DES_REG, TA_REG.TATISP AS REF_BY_REG 
					FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
					LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
						TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
					LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
						TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
					WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($request['CCPROG']));
			$r_regione = db2_fetch_assoc($stmt);
		
		//NUOVA CONDIZIONE COMMERCIALE
		$r = array(	'CCDT' => $request['CCDT'], 
		            'CCPROG' => $request['CCPROG'], 
					'CCSEZI' => $request['CCSEZI'], 
					'CCCCON' => $request['CCCCON'],
					'CCDCON' => $request['CCDCON'],
					'CCREFE' => $r_regione['REF_BY_REG']
		);
	}
	
	global $s, $main_module, $id_ditta_default;
	?>
        {
            xtype: 'form',
     		itemId: 'form_COMM',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Modifica condizione commerciale',
            <?php if ($request['mode'] == 'EDIT'){?>
            disabled: false,
            <?php }else{?>
            disabled: true,
            <?php }?>
            autoScroll: true,
            flex: 1,
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [
            		{xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($id_ditta_default); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j('BCCO'); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($request['rec_id']); ?>},
            		{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j('STD'); ?>},
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							    value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,
							    disabled: true
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    maxLength: 5,
							    value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    disabled: true							
							}
							]
						} ,
						{
						xtype: 'fieldset', title: 'Pagamento',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('CCPAGA', 'Pagamento', $r['CCPAGA'], acs_ar_to_select_json($main_module->find_sys_TA('CUCP'), ''), array('flex_width' => "flex: 2") ) ?>
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [										
										{
											name: 'CCNGPA',
											fieldLabel: 'GG inizio scad.',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCNGPA']))); ?>							
										}, {
											name: 'CCMME1',
											fieldLabel: '1&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCMME1']))); ?>							
										}, <?php write_combo_std('CCGGE1', '', $r['CCGGE1'], acs_ar_to_select_json($main_module->find_TA_std('ANC04'), ''), array('flex_width' => "flex: 1") ) ?>
										
										, {
											name: 'CCMME2',
											fieldLabel: '2&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCMME2']))); ?>							
										}, <?php write_combo_std('CCGGE2', '', $r['CCGGE2'], acs_ar_to_select_json($main_module->find_TA_std('ANC04'), ''), array('flex_width' => "flex: 1") ) ?>
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [										
										<?php write_combo_std('CCTSIV', 'Tipo scad. iva', $r['CCTSIV'], acs_ar_to_select_json($main_module->find_TA_std('ANC03'), ''), array('flex_width' => "flex: 2") ) ?>
									]
								}
						]
					}	           
            
            <?php if(1==1 || $request['i_e'] == 'I'){?>
					, {
						xtype: 'fieldset', title: 'Coordinate bancarie',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'des_banca',
											fieldLabel: 'Banca', labelWidth: 120, flex: 1,
										    maxLength: 160, 
										    value: <?php echo j(trim(acs_u8e($des_banca))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners= {
									        					afterSelected: function(from_win, form_values){
										        						m_form.findField('CCABI').setValue(form_values.f_abi);
										        						m_form.findField('CCCAB').setValue(form_values.f_cab);
										        						m_form.findField('des_banca').setValue(form_values.des_banca);
										        						from_win.close();
														        		}										    
														    },
															
															
															acs_show_win_std('Ricerca banca', 
																'search_bank.php?fn=open_search', 
																{}, 450, 300, my_listeners, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								}		
								
								, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [								
										{
											name: 'CCABI',
											fieldLabel: 'ABI / CAB / CC',
										    maxLength: 5,
										    labelWidth: 120, width: 180,
										    value: <?php echo j(trim(acs_u8e($r['CCABI']))); ?>							
										}, {
											name: 'CCCAB',
											fieldLabel: '', labelWidth: 0, width: 60,
										    maxLength: 5,
										    value: <?php echo j(trim(acs_u8e($r['CCCAB']))); ?>							
										}, {
											name: 'CCCOCO',
											fieldLabel: '',
										    maxLength: 20, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCCOCO']))); ?>							
										}
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [{
											name: 'CCIBAN',
											fieldLabel: 'Iban',
										    labelWidth: 120, maxLength: 34, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCIBAN']))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/button_black_repeat_dx.png") . " width=16>"); ?>,
										    
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
															Ext.getBody().mask('Loading... ', 'loading').show();
															
															Ext.Ajax.request({
																	timeout: 2400000,
															        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ricalcola_iban',
															        jsonData: {
															        	form_values: m_form.getValues()
															        },	
															        method     : 'POST',
															        waitMsg    : 'Data loading',
															        success : function(result, request){
															        						        
																		Ext.getBody().unmask();
															            var jsonData = Ext.decode(result.responseText);
																		
																		m_form.findField('CCIBAN').setValue(jsonData.iban);
									
															        },
															        failure    : function(result, request){
																		Ext.getBody().unmask();							        
															            Ext.Msg.alert('Message', 'No data to be loaded');
															        }
															    });																
															
															
															
														
															});
											             }
													}										    
										    
										    
										}
									]
								}
						]
					}
					
				<?php }?>
					, {
						xtype: 'fieldset', title: 'Vendita',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('CCREFE', 'Referente', $r['CCREFE'], acs_ar_to_select_json($main_module->find_sys_TA('BREF'), ''), array('flex_width' => "flex: 2") ) ?>,
									]
								},									
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('CCCATR', 'Causale vendita', $r['CCCATR'], acs_ar_to_select_json($main_module->find_sys_TA('VUCT'), ''), array('flex_width' => "flex: 2") ) ?>,
										<?php write_combo_std('CCLIST', 'Listino', $r['CCLIST'], acs_ar_to_select_json($main_module->find_sys_TA('BITL'), ''), array('flex_width' => "flex: 2", 'labelAlign' => 'right') ) ?>
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [										
										{
											name: 'CCSC1',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: 'Sconto comm. 1',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC1']))); ?>							
										}, {
											name: 'CCSC2',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: '2', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC2']))); ?>							
										}, {
											name: 'CCSC3',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: '3', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC3']))); ?>							
										}, {
											name: 'CCSC4',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: '4', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCSC4']))); ?>							
										}
									]
								}
						]
					}						
					
					
            
					
			],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            		var panel_form = this.up('form'),
	            		form = panel_form.getForm(),           	
	            		form_values = form.getValues();
	                    loc_win = this.up('window');
	            	
	            	var tabPanel = loc_win.down('tabpanel');
					var activeTab = tabPanel.getActiveTab();
					var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
					
                    if(activeTabIndex < tabPanel.items.length - 1){
                        panel_form.acs_actions.exe_form_submit(panel_form); 
                    } else {
                    
                      if(form.isValid())
						 panel_form.acs_actions.exe_form_submit(panel_form); 
		
                    }
	            	            	
	            	
	            } //handler
	        } //salva
	        ]             
			
		 , acs_actions: {
	    	exe_form_submit: function(panel_form){
	    	
    	    	var form = panel_form.getForm(),
    		        loc_win = panel_form.up('window');
    	    	
    	    	Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_cc',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>,
						        	GCDEFA : <?php echo j(trim($r['GCDEFA'])); ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        	Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            if(jsonData.success == false){
						            	acs_show_msg_error(jsonData.msg_error);
						            }else{
						            	loc_win.fireEvent('afterEditRecord', loc_win);
						            }
						
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });		
	    	
	    	}}			
			
				
				
        }
<?php
} //write_new_form_COMM







function write_new_form_FIDO($p = array(), $request = array()){
	global $conn, $cfg_mod_Gest;

	if ($request['mode'] == 'EDIT'){
		//recupero la riga da modificare
		$sql = "SELECT * FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($request['CCDT'], $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));

		$ret = array();
		$r = db2_fetch_assoc($stmt);

	} else {
		//NUOVA CONDIZIONE COMMERCIALE
		$r = array(	'CCDT' => $request['CCDT'],
				'CCPROG' => $request['CCPROG'],
				'CCSEZI' => $request['CCSEZI'],
				'CCCCON' => $request['CCCCON'],
				'CCDCON' => $request['CCDCON'],
		);
	}

	global $s, $main_module;
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            disabled: false, //true,
            autoScroll: true,
            
            flex: 1,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [
            		{xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($r['CCDT']); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j($r['CCSEZI']); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($r['CCPROG']); ?>},
            		{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j($r['CCCCON']); ?>},
            		{xtype: 'hiddenfield', name: 'CCDCON', value: <?php echo j($r['CCDCON']); ?>},
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							    value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,
							    disabled: true
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    maxLength: 5,
							    value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    disabled: true							
							}
							]
						}            
            
						, <?php write_numberfield_std('CCIMFD', 'Importo fido', trim($r['CCIMFD'])) ?>		
						, <?php write_datefield_std('CCDTVI', 'Data validit&agrave; iniziale', trim($r['CCDTVI'])) ?>
						, <?php write_datefield_std('CCDTVF', 'Data validit&agrave; finale', trim($r['CCDTVF'])) ?>
					
									
            
					
			],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');	            	
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_fido',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            
						            loc_win.fireEvent('afterEditRecord', loc_win);
						            
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_new_form_FIDO




