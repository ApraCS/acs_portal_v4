<?php
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_modifica_deposito'){
    $ret = array();
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'MOD_DEP_RIGA',
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array(
                "RICITI" => $m_params->deposito,
                "RIDTEP" => $m_params->row->num_ord,
                "RIART" => $m_params->row->codice,
                "RIDART" => $m_params->row->articolo,
            )
        ));
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}




if ($_REQUEST['fn'] == 'open_form'){ ?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: true,
					buttons: ['->'
			         
			         , {
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var loc_win = this.up('window');
			            	
			          			if (!form.isValid()) return false;
							
								Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_deposito',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    k_ordine : <?php echo j($m_params->k_ordine); ?>,
 			        			    row : <?php echo acs_je($m_params->row); ?>,
 			        			    deposito: form.getValues().f_deposito
 								},							        
 						        success : function(result, request){
 						        	var jsonData = Ext.decode(result.responseText);
 						        	loc_win.fireEvent('afterConfirm', loc_win);
		 			            		 
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
		 			
		
			
			            }
			         }
					 ], items: [
					
						{
							name: 'f_deposito',
							xtype: 'combo',
							fieldLabel: 'Deposito',
							labelWidth : 80,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',		
						    value : <?php echo j($m_params->row->depo); ?>,			   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('MUFD', null, null, null, null, null, 0, "", 'Y'), '') ?>
								    ] 
							}
							
							
												 
						}
						
				
										 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit; 

	
	
}
