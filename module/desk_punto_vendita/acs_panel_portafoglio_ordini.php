<?php

require_once "../../config.inc.php";
require_once "acs_panel_protocollazione_include.php";

$main_module = new DeskPVen();
$cod_mod_provenienza = $main_module->get_cod_mod();
$s = new Spedizioni(array('no_verify' => 'Y'));
//$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

if ($_REQUEST['fn'] == 'open_report'){

	
	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}
	
	$ar_email_json = acs_je($ar_email_to);
	
	//parametri per report
	$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
	$form_values = json_decode($form_values);
	
	
	$ar_ret_gen = get_json_data_chart2($form_values, 'TOTALE_GENERALE');
	$totale_generale = $ar_ret_gen[0];
	
?>	
	<html>
	<head>
	
	<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
	  
	  <style>
	   div#my_content h1{font-size: 22px; padding: 5px;}
	   div.header_page{border: 1px solid grey; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
	   div.header_page h3{font-size: 18px; font-weight: bold;}
	   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
	   table{border-collapse:collapse; width: 100%;}
	   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
	    .number{text-align: right;}
		.grassetto{font-weight: bold;}
		   
	
	   /*tr.liv_totale td{background-color: #cccccc; font-weight: bold;}*/
	   tr.liv_totale td{font-weight: bold;}
	   
	   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
	   tr.liv2 td{background-color: #cccccc;}   
	   tr.liv1 td{background-color: #ffffff;}   
	   tr.liv_data th{background-color: #333333; color: white;}
	   
	   tr.tr-cli td{font-weight: bold;}
	   
	   table.int0{margin-bottom: 20px;}
	   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
	   
	   div.cli_note{font-style: italic; padding-left: 25px; font-size: 0.9em;}
	      
	   table.int1 td, table.int1 th{vertical-align: top;}   
	   
		@media print 
		{
		    .noPrint{display:none;}
		}   
	      
	  </style>
	
	  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
	  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
	  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  
	
	  <script type="text/javascript">
	 
		Ext.Loader.setConfig({
		    enabled: true
		});Ext.Loader.setPath('Ext.ux', '../ux');
	
	    Ext.require(['*']);
	    
	    
	    
	Ext.onReady(function() {
	
		//apertura automatica finestra invio email
		<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
			Ext.get('p_send_email').dom.click();
		<?php } ?>
		
		});    
	    
	
	  </script>
	
	
	 </head>
	 <body>
	 
	 
	 <div class="page-utility noPrint">
	<?php 
				$bt_fascetta_print = 'Y';
				$bt_fascetta_email = 'Y';
				$bt_fascetta_excel = 'Y';
				$bt_fascetta_close = 'Y';
				include  "../../templates/bottoni_fascetta.php";
	?>	
	</div>
	
	 <div id='my_content'>
	 
	 <?php if($_REQUEST['ordini'] == 'Y'){?>
	 <h1>Riepilogo portafoglio <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo "righe "; ?>ordini clienti </h1>
		<?php if (strlen($form_values->f_data) > 0 || strlen($form_values->f_data_a) > 0) { ?>
	 		<div>&nbsp;&nbsp;Data: dal <?php echo print_date($form_values->f_data); ?> al <?php echo print_date($form_values->f_data_a); ?></div>		
		<?php } ?>	 
	 	<div style="text-align: right;">Data elaborazione: <?php echo Date('d/m/Y H:i'); ?></div>
	 <br/>
	 
	
	 	<table class="int1" width="100%">
			<tr>
			 <td><?php echo print_table_stabilimenti(); ?></td>
			  <td><?php echo print_table_referente_ordine(); ?></td>
			 <td><?php echo print_table_localita(); ?></td>
			
			 </tr>
			
			<tr>
			 <td colspan=3>&nbsp;</td>		 
			</tr>		
			
			<tr>
			 <td>
			 	<?php echo print_table_tree_tipologia_tipo(); ?>		 	
			 </td>
			 <td><?php echo print_table_tree_stato_tipologia(); ?></td>
			 <td><?php echo print_table_variante1(); ?></td>		 
			</tr>
	
		</table>
	
		
		<br/>
		
		<?php }?>
		
		
	<?php
	/************************************************************************
	 * 	FATTURATO CLIENTI
	 ************************************************************************/
	
	$ar_ret_gen = get_json_data_chart_fatt($form_values, 'TOTALE_GENERALE');
	$totale_generale = $ar_ret_gen[0];
	
	?>	
		
	
	 <?php if($_REQUEST['fatture'] == 'Y'){?>
		
	 <h1>Riepilogo fatturato clienti </h1>
		<?php if (strlen($form_values->f_data_fattura) > 0 || strlen($form_values->f_data_fattura_a) > 0) { ?>
	 		<div>&nbsp;&nbsp;Data: dal <?php echo print_date($form_values->f_data_fattura_da); ?> al <?php echo print_date($form_values->f_data_fattura_a); ?></div>		
		<?php } ?>	 
	 <br/>
	
	 	<table class="int1" width="100%">
			<tr>
			 <td><?php echo print_table_stabilimenti_fatt(); ?></td>
			  <td><?php echo print_table_referente_ordine_fatt(); ?></td>
			 <td><?php echo print_table_localita_fatt(); ?></td>			
			 </tr>
			
			<tr>
			 <td colspan=3>&nbsp;</td>		 
			</tr>		
	
		</table>
	
		
		<br/>		
		<?php }?>
		
		
		
		
		
		
		
		
		
	 </div>
	
	 </body>
	</html>
	<?php

	 
}



if ($_REQUEST['fn'] == 'open_form'){
	$m_params = acs_m_params_json_decode();
	?>

{"success":true, "items": [

        {
				xtype: 'form',
				bodyStyle: 'padding: 5px',
				frame: true,
				rowspan: 1,
				flex: 1,
				title: '',
				items: [
					{
						xtype: 'fieldset',
						title: '',
						flex: 1,
						layout: 'vbox',
						border: false,
						 items: [
						 
						 
						 { 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [	
						 {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data reg.ord. da'
						   , name: 'f_data'
						   
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						 
			
						},{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data reg.ord. a'
						   , name: 'f_data_a'
						   , format: 'd/m/Y'
						   , labelAlign: 'right'	
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						
						}
								
							   ]
							  }	
							 
						   
						 , { 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [	
								 {
								   xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Data fattura da'
								   , name: 'f_data_fattura_da'
								   
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '-15'
								 
					
								},{
								   xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Data fattura a'
								   , name: 'f_data_fattura_a'
								   , format: 'd/m/Y'
								   , labelAlign: 'right'	
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								
								}						   
						   	]
						  },
							{ 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [	
					
							   {
								name: 'f_stato_ordine',
								xtype: 'combo',
								fieldLabel: 'Stato ordine',
								labelAlign: 'right',
								displayField: 'text',
								labelAlign:'left',
															
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
								multiSelect: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								    
										 <?php echo acs_ar_to_select_json($s->get_options_stati_ordine('Y'), '') ?> 	
										] 
									}					 
								},{
								name: 'f_tipo_ordine',
								xtype: 'combo',
								fieldLabel: 'Tipo ordine',
								flex: 1,
								displayField: 'text',
								labelAlign: 'right',								
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,														
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								    
										 <?php echo acs_ar_to_select_json($s->options_select_tipi_ordine('Y'), '') ?> 	
										] 
									}							 
								}
								
							   ]
							  }	
								

							  
							, { 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [							
							  
								
							  {
								name: 'f_tipologia_ordine',
								xtype: 'combo',
								fieldLabel: 'Tipologia ordine',
								flex: 1,
								displayField: 'text',
								labelAlign:'left',
															
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								     	
										 <?php echo acs_ar_to_select_json($s->find_TA_std('TIPOV'), "", true, 'N', 'N', 'Y'); ?>	
										] 
									}							 
								}, {
								name: 'f_sede',
								xtype: 'combo',
								fieldLabel: 'Sede',
								flex: 2,
								displayField: 'text',
								labelAlign:'right',
								flex: 1,								
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,														
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								     	
										  <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?>		
										] 
									}							 
								}
							]
						   }, { 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [							
							  
								
							  {
								name: 'f_architetto',
								xtype: 'combo',
								fieldLabel: 'Architetto',
								flex: 1,
								displayField: 'text',
								labelAlign:'left',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								     	
										 <?php echo acs_ar_to_select_json($s->find_TA_std('ARCHI'), "", true, 'N', 'N', 'Y'); ?>	
										] 
									}							 
								},
								
							  {
								name: 'f_vettore',
								xtype: 'combo',
								fieldLabel: 'Vettore',
								flex: 2,
								displayField: 'text',
								labelAlign:'right',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								     	
										 <?php echo acs_ar_to_select_json($s->find_TA_std('AUTR'), "", true, 'N', 'N', 'Y'); ?>	
										] 
									}							 
								}
							]
						   },  { 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [							
							  
								
							  {
								name: 'f_tipo_fatt',
								xtype: 'combo',
								fieldLabel: 'Tipo fattura',
								flex: 1,
								displayField: 'text',
								labelAlign:'left',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								    
									     <?php echo acs_ar_to_select_json(get_ar_tipo_documento(), ""); ?>	
									    ] 
									}							 
								},  {
								name: 'f_divisione',
								xtype: 'combo',
								fieldLabel: 'Divisione',
								flex: 1,
								displayField: 'text',
								labelAlign:'right',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,
								store: {
    								autoLoad: true,
    								editable: false,
    								autoDestroy: true,	 
    							    fields: [{name:'id'}, {name:'text'}],
    							    data: [								    
    								     <?php  echo acs_ar_to_select_json($main_module->find_sys_TA('DDOC'), '');  ?>	
    								    ] 
							   }							 
								},
								
							
							]
						   }
						   
						   
						  
						  
					 ]
					 },
					 
					 	
					 ],
					 
					 		buttons: [
				<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "PORTAF_ORD");  ?>
				{
	         	xtype: 'splitbutton',
	            text: 'Reports',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
					{
					xtype: 'button',
		            scale: 'medium',
		            text: 'Registro anagrafiche',	            
			        scale: 'large',            
		            handler: function() {
		            	form = this.up('form').getForm();
		                 if (form.isValid()){
        	                acs_show_win_std('Registro anagrafiche', 'acs_report_anagrafica.php?fn=open_form', {
	        				filter: form.getValues()
	    					},		        				 
	        				400, 200, {}, 'icon-print-16');	
		        	} 
		            
		                
		            }
		        },{
					xtype: 'button',
		            scale: 'medium',
		            text: 'Report architetti',		            
			        handler: function() {
		                this.up('form').submit({
	                        url: 'acs_report_architetti.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues())
	                        }
	                  });
	                  
	                  this.up('window').close();
		                
		            }
		        },{
					xtype: 'button',
		            scale: 'medium',
		            text: 'Report assistenze',	            
			        scale: 'large',            
		             handler: function() {
			                this.up('form').submit({
		                        url: 'acs_causali_assistenza_report.php?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues())
		                        }
		                  });
		                  
		                  this.up('window').close();
			         }
		        }, {
					xtype: 'button',
		            scale: 'medium',
		            text: 'Report resi',	            
			        scale: 'large',            
		             handler: function() {
			                this.up('form').submit({
		                        url: 'acs_causali_assistenza_report.php?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	resi : 'Y'
		                        }
		                  });
		                  
		                  this.up('window').close();
			         }
		        }, {
					xtype: 'button',
		            scale: 'medium',
		            text: 'Report vettori',	            
			        scale: 'large',            
		            handler: function() {
		                this.up('form').submit({
	                        url: 'acs_report_vettori.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues())
	                        }
	                  });
	                  
	                  this.up('window').close();
		                
		            }
		        },{
					xtype: 'button',
		            scale: 'medium',
		            text: 'Riepilogo portafoglio',
			        handler: function() {
			            var form_values = this.up('form').getValues();
			            if(form_values.f_data == 0 && form_values.f_data_a == 0){
			              acs_show_msg_error('Selezionare data ordine');
					      return;
			            }
			          
			            
		                this.up('form').submit({
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues()),
	                        	ordini : 'Y'
	                        }
	                  });
	                  
	                  this.up('window').close();
		                
		            }
		        },{
					xtype: 'button',
		            scale: 'medium',
		            text: 'Riepilogo fatturato',
			        handler: function() {
			            var form_values = this.up('form').getValues();
			            if(form_values.f_data_fattura_da == 0 && form_values.f_data_fattura_a == 0){
			              acs_show_msg_error('Selezionare data fattura');
					      return;
			            }
			          
			            
		                this.up('form').submit({
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues()),
	                        	fatture : 'Y'
	                        }
	                  });
	                  
	                  this.up('window').close();
		                
		            }
		        }
				      
				            	        	
		        	]
		        }
	        }
	        
	        
	        ]  
					
					
	}
	
]}


<?php 
}


function print_table_stabilimenti(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'tree_divisione_sede');
	//$ar_ret = get_json_data_chart2($form_values, 'TDSTAB');

	$ret = "";
	//$ret .= print_tabella('Sede', $ar_ret);
	$ret .= print_tabella_tree('Divisione / Sede', $ar_ret);
	return $ret;
	
}




function print_table_referente_cliente(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TDDOCL');

	$ret = "";
	$ret .= print_tabella('Referente cliente', $ar_ret, 'N');
	return $ret;
}

function print_table_localita(){

	global $form_values;
	//$ar_ret = get_json_data_chart2($form_values, 'TDLOCA');
	$ar_ret = get_json_data_chart2($form_values, 'tree_localita', 0, 'Y');

	$ret = "";
	$ret .= print_tabella_tree('Regione/Provincia/Localit&agrave;', $ar_ret, 'Y');
	//$ret .= print_tabella('Localit&agrave;', $ar_ret, 'N');
	return $ret;
}



function print_table_referente_ordine(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TDDORE');

	$ret = "";
	$ret .= print_tabella('Referente ordine', $ar_ret, 'N');
	return $ret;
}


function print_table_variante1(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TDDVN1');

	$ret = "";
	$ret .= print_tabella('Variante', $ar_ret, 'N');
	return $ret;
}


function print_table_tree_tipologia_tipo(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'tree_tipologia_tipo');

	$ret = "";
	$ret .= print_tabella_tree('Tipologia / Tipo ordine cliente', $ar_ret);
	return $ret;
}


function print_table_tree_stato_tipologia(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'tree_stato_tipologia');

	$ret = "";
	$ret .= print_tabella_tree('Stato / Tipologia ordine cliente', $ar_ret);
	return $ret;
}


function print_table_trasportatori(){

	global $form_values;
	$ar_ret = get_json_data_chart2($form_values, 'TDVET1');

	$ret = "";
	$ret .= print_tabella('Trasportatori', $ar_ret);
	return $ret;
}






function print_table_stabilimenti_fatt(){

	global $form_values;
	$ar_ret = get_json_data_chart_fatt($form_values, 'tree_stab_tipologia');

	$ret = "";
	$ret .= print_tabella_tree('Divisione/Sede', $ar_ret, 'Y');
	return $ret;
}
function print_table_referente_ordine_fatt(){

	global $form_values;
	$ar_ret = get_json_data_chart_fatt($form_values, 'referente');

	$ret = "";
	$ret .= print_tabella('Referente ordine', $ar_ret, 'N');
	return $ret;
}
function print_table_localita_fatt(){

	global $form_values;
	$ar_ret = get_json_data_chart_fatt($form_values, 'tree_localita', 0 , 'Y');

	$ret = "";
	//$ret .= print_tabella('Localit&agrave;', $ar_ret, 'Y');
	$ret .= print_tabella_tree('Regione/Provincia/Localit&agrave;', $ar_ret, 'Y');
	return $ret;
}








function print_tabella($title, $ar_ret, $stampa_cod = 'Y', $top = 0){
	global $totale_generale;
	 
	 
	//scorro per tot import e nr
	$tot_imp_rec = $tot_nr_rec = 0; //somma di quelli che mostro (per devo aggiungere ALTRO)
	foreach($ar_ret as $r){
		$tot_imp_rec += $r['S_IMP'];
		$tot_nr_rec  += $r['NUM_ORD'];
	}


	//i totali li prendo da tot_generale
	$tot_imp = $totale_generale['S_IMP'];
	$tot_nr  = $totale_generale['NUM_ORD'];
	 
	if ($top > 0){
		//creo riga "altro"
		$ar_ret[] = array(	'TDASPE' => 'ALTRO',
				'TDASPE_D' => 'ALTRO',
				'S_IMP' => ($tot_imp - $tot_imp_rec),
				'NUM_ORD' => ($tot_nr - $tot_nr_rec)
		);
	}
	 
	 
	 
	$ret = "";
	$ret .= "
		 <table>

		  <tr class=liv3>
			 <td colspan=6>" . $title . "</td>
	   	  </tr>

		  <tr>
	 		";
	 
	if ($stampa_cod == 'Y')
		$ret .= "
	 		<th>Cod</th>
	 		<th>Descrizione</th>
	 		";
		else
			$ret .= "<th colspan=2>Descrizione</th>";
			 

	 	$ret .= "
		   <th class=number>Importo</th>
		   <th class=number>%</th>
		   <th class=number>Nr</th>
		   <th class=number>%</th>
		  </tr>
		 ";
	 	 
	 	foreach($ar_ret as $r){

	 		$r_perc_imp = $r_perc_nr = 0;
	 		if ($tot_imp != 0)
	 			$r_perc_imp = ($r['S_IMP'] / $tot_imp) * 100;
	 		if ($tot_nr != 0)
	 			$r_perc_nr = ($r['NUM_ORD'] / $tot_nr) * 100;

	 				$ret .= "
				<tr>
				";

	 				if ($stampa_cod == 'Y')
	 					$ret .= "
	 					<td>{$r['TDASPE']}</td>
	 					<td>{$r['TDASPE_D']}</td>
	 					";
	 					else
	 						$ret .= "<td colspan=2>{$r['TDASPE_D']}</td>";

	 						$ret .= "
				 <td class=number>" . n($r['S_IMP']) . "</td>
				 <td class=number>" . n($r_perc_imp) . "</td>
				 <td class=number>" . n($r['NUM_ORD'], 0) . "</td>
				 <td class=number>" . n($r_perc_nr) . "</td>
				</tr>
			";
	 	}
	 	 

	 	//totale
	 	$ret .= "
	 	  <tr class=liv_totale>
	 		<td colspan=2 class=number>Totale</td>
	 		<td class=number>" . n($tot_imp) . "</td>
			<td class=number>&nbsp;</td>
			<td class=number>" . n($tot_nr, 0) . "</td>
			<td class=number>&nbsp;</td>
		  </tr>
		";
	 	 
	 	 
	 	$ret .= "
		 </table>
		";
	 	return $ret;
}







function print_tabella_tree($title, $ar_ret, $liv1 = 'N'){
	 
	//scorro per tot import e nr
	$tot_imp = $tot_nr = 0;
	foreach($ar_ret as $r){
		$tot_imp += $r['S_IMP'];
		$tot_nr  += $r['NUM_ORD'];
	}
	
	/*echo "<pre>";
	print_r($ar_ret);
	exit;*/


	//creo alberatura
	$ar = array();
	foreach($ar_ret as $row){

		$s_ar = &$ar;

	   //stacco per TDASPE (campo1_name)
		$r_livs['liv1_v'] = trim($row['TDASPE']);
		
		$liv = $r_livs['liv1_v'];
		if (is_null($s_ar[$liv])){
			$s_ar[$liv] = $row;
		} else {
			//incremento
			$s_ar[$liv]['S_IMP'] += $row['S_IMP'];
			$s_ar[$liv]['NUM_ORD'] += $row['NUM_ORD'];
		}
		 
		$s_ar = &$s_ar[$liv]['children'];
		
		if($liv1 == 'Y'){
    		 $r_livs['liv1d_v'] = trim($row['CAMPO1_NAME']);
    		
    		$liv = $r_livs['liv1d_v'];
    		if (is_null($s_ar[$liv])){
    		    $s_ar[$liv] = $row;
    		} else {
    		    //incremento
    		    $s_ar[$liv]['S_IMP'] += $row['S_IMP'];
    		    $s_ar[$liv]['NUM_ORD'] += $row['NUM_ORD'];
    		}
    		
    		$s_ar = &$s_ar[$liv]['children'];
    		
		}
		
		if (is_null($s_ar[trim($row['CAMPO2_NAME'])]))
			$s_ar[trim($row['CAMPO2_NAME'])] = $row;
		else {
			$s_ar[trim($row['CAMPO2_NAME'])]['S_IMP'] += $row['S_IMP'];
			$s_ar[trim($row['CAMPO2_NAME'])]['NUM_ORD'] += $row['NUM_ORD'];
		}

	}
	 
	 


	$ret = "";
	$ret .= "
		 <table>

		  <tr class=liv3>
			 <td colspan=6>" . $title . "</td>
	   	  </tr>

		  <tr>
		   <th>Cod</th>
		   <th>Descrizione</th>
		   <th class=number>Importo</th>
		   <th class=number>%</th>
		   <th class=number>Nr</th>
		   <th class=number>%</th>
		  </tr>
		 ";

	foreach($ar as $r){
	  
	 	
		$r_perc_imp = $r_perc_nr = 0;
		if ($tot_imp != 0)
			$r_perc_imp = ($r['S_IMP'] / $tot_imp) * 100;
			if ($tot_nr != 0)
				$r_perc_nr = ($r['NUM_ORD'] / $tot_nr) * 100;
					
	 		$ret .= "
	 		<tr class=liv2>
	 		<td>{$r['TDASPE']}</td>
	 		<td>{$r['TDASPE_D']}</td>
	 		<td class=number>" . n($r['S_IMP']) . "</td>
				 <td class=number>" . n($r_perc_imp) . "</td>
				 <td class=number>" . n($r['NUM_ORD'], 0) . "</td>
				 <td class=number>" . n($r_perc_nr) . "</td>
				</tr>
			";
	 		
	 		//livello1 
	 		if($liv1 == 'Y'){
    	 		foreach($r['children'] as $r1){
    	 		    
    	 		    $r1_perc_imp = $r1_perc_nr = 0;
    	 		    if ($tot_imp != 0)
    	 		        $r1_perc_imp = ($r1['S_IMP'] / $tot_imp) * 100;
    	 		        if ($tot_nr != 0)
    	 		            $r1_perc_nr = ($r1['NUM_ORD'] / $tot_nr) * 100;
    	 		            
    	 		            $ret .= "
    	 		            <tr class=tr-cli>
    	 		            <td>{$r1['CAMPO1_NAME']}</td>
    	 		            <td>{$r1['CAMPO1_DES']}</td>
    	 		            <td class=number>" . n($r1['S_IMP']) . "</td>
    				 <td class=number>" . n($r1_perc_imp) . "</td>
    				 <td class=number>" . n($r1['NUM_ORD'], 0) . "</td>
    				 <td class=number>" . n($r1_perc_nr) . "</td>
    				</tr>
    			";
    	 		            
 		            //livello2
 		            foreach($r1['children'] as $r2){
 		                
 		                $r2_perc_imp = $r2_perc_nr = 0;
 		                if ($tot_imp != 0)
 		                    $r2_perc_imp = ($r2['S_IMP'] / $tot_imp) * 100;
 		                    if ($tot_nr != 0)
 		                        $r2_perc_nr = ($r2['NUM_ORD'] / $tot_nr) * 100;
 		                        
 		                        $ret .= "
 		                        <tr>
 		                        <td>{$r2['CAMPO2_NAME']}</td>
 		                        <td>{$r2['CAMPO2_DES']}</td>
 		                        <td class=number>" . n($r2['S_IMP']) . "</td>
            				 <td class=number>" . n($r2_perc_imp) . "</td>
            				 <td class=number>" . n($r2['NUM_ORD'], 0) . "</td>
            				 <td class=number>" . n($r2_perc_nr) . "</td>
            				</tr>
            			    ";
 		            } //livello2
    	 		}
    	 		    
	 		}else{
	 		    //livello2
	 		    foreach($r['children'] as $r2){
	 		        
	 		        $r2_perc_imp = $r2_perc_nr = 0;
	 		        if ($tot_imp != 0)
	 		            $r2_perc_imp = ($r2['S_IMP'] / $tot_imp) * 100;
	 		            if ($tot_nr != 0)
	 		                $r2_perc_nr = ($r2['NUM_ORD'] / $tot_nr) * 100;
	 		                
	 		                $ret .= "
	 		                <tr>
	 		                <td>{$r2['CAMPO2_NAME']}</td>
	 		                <td>{$r2['CAMPO2_DES']}</td>
	 		                <td class=number>" . n($r2['S_IMP']) . "</td>
            				 <td class=number>" . n($r2_perc_imp) . "</td>
            				 <td class=number>" . n($r2['NUM_ORD'], 0) . "</td>
            				 <td class=number>" . n($r2_perc_nr) . "</td>
            				</tr>
            			    ";
	 		    } //livello2
	 		    
	 		    
	 		    
	 		}
    	 		    
    	 		    
    	 		

	 		

	 	

	} //livello1


	//totale
	$ret .= "
	 	  <tr class=liv_totale>
	 		<td colspan=2 class=number>Totale</td>
	 		<td class=number>" . n($tot_imp) . "</td>
			<td class=number>&nbsp;</td>
			<td class=number>" . n($tot_nr, 0) . "</td>
			<td class=number>&nbsp;</td>
		  </tr>
		";
	 
	 
	$ret .= "
		 </table>
		";
	return $ret;
}





function get_json_data_chart2($form_values, $campo, $top = 0, $regione = 'N'){
	global $cfg_mod_DeskPVen, $main_module, $s, $conn;
	
	$filter_where = '';
	
	//controllo data
	    if (strlen($form_values->f_data) > 0)
	    	$filter_where .= " AND TDODRE >= {$form_values->f_data}";
		
		if (strlen($form_values->f_data_a) > 0)
			$filter_where .= " AND TDODRE <= {$form_values->f_data_a}";

		if (isset($form_values->f_tipo_ordine) && count($form_values->f_tipo_ordine) > 0)
		   $filter_where .= " AND TDOTPD IN (" . sql_t_IN($form_values->f_tipo_ordine) . ")";

	 	if (isset($form_values->f_stato_ordine) && count($form_values->f_stato_ordine) > 0)
	 	$filter_where .= " AND TDSTAT IN( " . sql_t_IN($form_values->f_stato_ordine) . ")";

 		if (isset($form_values->f_tipologia_ordine) && count($form_values->f_tipologia_ordine) > 0)
 		$filter_where .= " AND TDCLOR IN( " . sql_t_IN($form_values->f_tipologia_ordine) . ")";
	 		 
 		if (isset($form_values->tipo) && strlen($form_values->tipo) > 0 && $form_values->tipo != 'tutti'){
	 			if ($form_values->tipo == 1) //solo
	 				$filter_where .= " AND ( TDFN02 = " . sql_t($form_values->tipo) . " OR TDFN03 = " . sql_t($form_values->tipo) . ") ";
 				if ($form_values->tipo == 0) //esclusi
 					$filter_where .= " AND ( TDFN02 = " . sql_t($form_values->tipo) . " AND TDFN03 = " . sql_t($form_values->tipo) . ") ";
	 		}

	 		
	 		$filter_where.= sql_where_by_combo_value('TDCDIV', $form_values->f_divisione);
 		    $filter_where.= sql_where_by_combo_value('TDSTAB', $form_values->f_sede);
  			$filter_where.= sql_where_by_combo_value('TDCAG2', $form_values->f_architetto);
 			$filter_where.= sql_where_by_combo_value('TDVETT', $form_values->f_vettore);
 			

	 			switch ($campo){
	 				case 'TDSTAB':
	 				case 'TDCITI':
	 				case 'TDVET1':
	 				case 'TDDVN1':
	 				case 'TDCAVE':
	 				case 'TDDOCL':
	 					$campo_name = $campo;
	 					$campo_des = $campo;
	 					break;
	 				case 'TDDORE':
	 					$campo_name = $campo;
	 					$campo_des = $campo;
	 					break;
	 				case 'TDCAG1':
	 					$campo_name = $campo;
	 					$campo_des  = 'TDDAG1';
	 					break;
 					case 'TDLOCA':
 						$campo_name = $campo;
 						$campo_des  = 'TDLOCA';
 						break;
	 				case 'TDCCON':
	 					$campo_name = $campo;
	 					$campo_des  = 'TDDCON';
	 					break;
	 				case 'TDCPAG':
	 					$campo_name = $campo;
	 					$campo_des  = 'TDDPAG';
	 					break;
	 				case 'TDNAZI':
	 					$campo_name = $campo;
	 					$campo_des  = 'TDDNAZ';
	 					break;
	 				case 'TDCDIV':
	 					$campo_name = $campo;
	 					$campo_des  = 'TDDDIV';
	 					break;
	 				case 'tree_tipologia_tipo':
	 					$campo_name  = 'TDCLOR';
	 					$campo_des   = 'TDCLOR';
	 					$campo2_name = 'TDOTPD';
	 					$campo2_des  = 'TDDOTD';
	 					break;
	 				case 'tree_stato_tipologia':
	 					$campo_name  = 'TDSTAT';
	 					$campo_des   = 'TDDSST';
	 					$campo2_name  = 'TDCLOR';
	 					$campo2_des   = 'TDCLOR';
	 					break;
	 				case 'tree_divisione_sede':
	 				    $campo_name  = 'TDCDIV';
	 				    $campo_des   = 'TDDDIV';
	 				    $campo2_name  = 'TDSTAB';
	 				    $campo2_des   = 'TDSTAB';
	 				    break;
	 				case 'tree_localita':
	 				    $campo_name  = 'TDNAZI';
	 				    $campo_des   = 'TDDNAZ';
	 				    $campo1_name  = 'TDPROV';
	 				    $campo1_des   = 'TDPROV';
	 				    $campo2_name  = 'TDLOCA';
	 				    $campo2_des   = 'TDLOCA';
	 				    break;
	 				case 'TOTALE_GENERALE':
	 					$campo_name  = 'TDDT';
	 					$campo_des   = 'TDDT';
	 					break;
	 				default:
	 					$campo_name = 'TAASPE';
	 					$campo_des = 'TAASPE';
	 					break;
	 			}


	 			//programmati o hold?
	 				if ($form_values->programmati_hold == 'HOLD')
	 				$filter_where .= " AND TDSWSP = 'N' ";	//hold
 					else if ($form_values->programmati_hold == 'PROG')
 					$filter_where .= " AND TDSWSP = 'Y' ";	//tutti
 					/*else
 						$filter_where .= " AND 1=1 ";	//programmati*/

     					$campo1_select = $campo1_group = '';
     					if (isset($campo1_name)){
     					    $campo1_select = "{$campo1_name} AS CAMPO1_NAME, {$campo1_des} AS CAMPO1_DES,";
     					    $campo1_group  = ", {$campo1_name}, {$campo1_des}";
     					}
 					
 						$campo2_select = $campo2_group = '';
 						if (isset($campo2_name)){
 							$campo2_select = "{$campo2_name} AS CAMPO2_NAME, {$campo2_des} AS CAMPO2_DES,";
 							$campo2_group  = ", {$campo2_name}, {$campo2_des}";
 						}
 						
 						if ($regione == 'Y'){
 						    $campo_reg_select = "TA_REG.TADESC AS REGIONE, TA_PROV.TADESC AS PROVINCIA, ";
 						    $join_reg = "LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA_COM
 						                     ON TA_COM.TAKEY4 = TD.TDNAZI AND TA_COM.TAKEY2 = TD.TDPROV AND TA_COM.TADESC = TDLOCA AND TA_COM.TATAID = 'ANCOM'
 						                 LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA_REG
 						                     ON TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
                                         LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA_PROV
 						                     ON TA_PROV.TAKEY1 = TD.TDPROV AND TA_PROV.TATAID = 'ANPRO'";
 						    
 						    $campo_reg_group  = ", TA_REG.TADESC, TA_PROV.TADESC";
 						}
 						

	 						$sql = "
	 						SELECT {$campo_name} AS TDASPE, {$campo_des} AS TDASPE_D, 
	 						{$campo1_select} {$campo2_select} {$campo_reg_select}
	 						COUNT(*) AS NUM_ORD, SUM(" . $cfg_mod_DeskPVen['info_importo_field'] . ") AS S_IMP
                            FROM {$cfg_mod_DeskPVen['file_testate']} TD
	 						INNER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA_ITIN
	 						    ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
 						   {$join_reg}
                            WHERE" .   $s->get_where_std()." AND 1=1 {$filter_where} 
	 						GROUP BY {$campo_name}, {$campo_des} {$campo1_group} {$campo2_group} {$campo_reg_group}
	 						ORDER BY SUM(TDTIMP) DESC, COUNT(*) DESC
	 						";
							

	 						if ($top > 0)
	 							$sql .= " FETCH FIRST {$top} ROWS ONLY ";

	 							$stmt = db2_prepare($conn, $sql);
	 							echo db2_stmt_errormsg();
	 							$result = db2_execute($stmt);
	 					
	 							$ar_records = array();
	 							while ($r = db2_fetch_assoc($stmt)){
	 							    
	 								//decodifico descrizione
	 								if ($campo == 'TDSTAB')
	 									$r['TDASPE_D'] = $s->decod_std('START', $r['TDASPE']);
 									else if ($campo == 'TDCITI')
 										$r['TDASPE_D'] = $s->decod_std('ITIN', $r['TDASPE']);
 									else if ($campo == 'TDVET1')
 										$r['TDASPE_D'] = $s->decod_std('AVET', $r['TDASPE']);
 									else if ($campo == 'TDCAG1')
 										$r['TDASPE_D'] = $r['TDASPE_D'];
	 								else if ($campo == 'TDCDIV')
	 									$r['TDASPE_D'] = $r['TDASPE_D'];
	 								else if ($campo == 'TDCCON')
	 									$r['TDASPE_D'] = $r['TDASPE_D'];
	 								else if ($campo == 'TDCPAG')
	 									$r['TDASPE_D'] = $r['TDASPE_D'];
	 								else if ($campo == 'TDNAZI')
	 									$r['TDASPE_D'] = $r['TDASPE_D'];
	 								else if ($campo == 'TDCAVE'){
	 									$r['TDASPE_D'] = $s->decod_std('MERCA', $r['TDASPE']);
	 									}
 									else if ($campo == 'tree_tipologia_tipo'){
 										$r['TDASPE_D'] = $s->decod_std('TIPOV', $r['TDASPE']);
 										}
 									else if ($campo == 'tree_stato_tipologia'){
 										$r['CAMPO2_DES'] = $s->decod_std('TIPOV', $r['CAMPO2_NAME']);
 										}
									else if ($campo == 'tree_divisione_sede'){
									    $r['CAMPO2_DES'] = $s->decod_std('START', $r['CAMPO2_NAME']);
									}else if ($campo == 'tree_localita'){
									    $r['TDASPE'] = "[".trim($r['TDASPE'])."] ".$r['REGIONE'];
									    $r['TDASPE_D'] = '';
									    $r['CAMPO1_DES'] = $r['PROVINCIA'];
									}
 									else
 										$r['TDASPE_D'] = $s->decod_std('ASPE', $r['TDASPE']);

 									$ar_records[] = $r;
	 							}

	 							$tot_ar_ret = 0;
	 							foreach($ar_records as $tot_ar)
	 								$tot_ar_ret += $tot_ar['NUM_ORD'];

	 								$ar_ret = array();
	 								foreach($ar_records as $ar1){
	 									//$ar1['NUM_ORD'] = round(($ar1['NUM_ORD'] * 100 / $tot_ar_ret),2);
	 									$ar_ret[] = $ar1;
	 								}
	 								return $ar_ret;
} //get_json_data_chart2



function get_ar_tipo_documento(){
    global $id_ditta_default;
    global $conn, $cfg_mod_Gest;
    global $main_module;
    $cfg_mod = $main_module->get_cfg_mod();
    $sql = "SELECT TFTPDO AS COD, TFDTPD AS DES
    FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']}
    WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VF'
    GROUP BY TFTPDO, TFDTPD ORDER BY TFDTPD";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $ar = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ar[] = array("id" => trim($r['COD']), "text" => "[" . trim($r['COD'] . "] ".$r['DES']));
    }
    return $ar;
}

function get_json_data_chart_fatt($form_values, $campo, $top = 0, $regione = 'N'){
	global $cfg_mod_DeskPVen, $main_module, $s, $conn, $id_ditta_default;
	
	$filter_where = '';

	//controllo data
	if (strlen($form_values->f_data_fattura_da) > 0)
		$filter_where .= " AND TFDTRG >= {$form_values->f_data_fattura_da}";

	if (strlen($form_values->f_data_fattura_a) > 0)
		$filter_where .= " AND TFDTRG <= {$form_values->f_data_fattura_a}";

		//$filter_where.= sql_where_by_combo_value('TFVETT', $form_values->f_vettore);
		$filter_where.= sql_where_by_combo_value('TD.TDVETT', $form_values->f_vettore);

	switch ($campo){
		case 'tree_stab_tipologia':
			$campo_name  =  'TFCDIV';
			$campo_des   =  'TFDDIV';
			$campo1_name  = 'TFARMA';
			$campo1_des   = 'TFDARM';
			$campo2_name  = 'TFTPDO';
			$campo2_des   = 'TFDTPD';
			break;
		case 'tree_localita':
		    $campo_name  = 'TDNAZI';
		    $campo_des   = 'TDDNAZ';
		    $campo1_name  = 'TFPROV';
		    $campo1_des   = 'TFPROV';
		    $campo2_name  = 'TFLOCA';
		    $campo2_des   = 'TFLOCA';
		    break;
		case 'TFARMA':
			$campo_name  = 'TFARMA';
			$campo_des   = 'TFDARM';
			break;
		case 'referente':
			$campo_name  = 'TFCODO';
			$campo_des   = 'TFDCDO';
			break;
		case 'localita':
			$campo_name = 'TFLOCA';
			$campo_des  = 'TFLOCA';
			break;
		case 'TOTALE_GENERALE':
			$campo_name  = 'TFDT';
			$campo_des   = 'TFDT';
			break;
	}

	$campo1_select = $campo1_group = '';
	if (isset($campo1_name)){
	    $campo1_select = "{$campo1_name} AS CAMPO1_NAME, {$campo1_des} AS CAMPO1_DES,";
	    $campo1_group  = ", {$campo1_name}, {$campo1_des}";
	}
	
	$campo2_select = $campo2_group = '';
	if (isset($campo2_name)){
		$campo2_select = "{$campo2_name} AS CAMPO2_NAME, {$campo2_des} AS CAMPO2_DES,";
		$campo2_group  = ", {$campo2_name}, {$campo2_des}";
	}
	
	
	if ($regione == 'Y'){
	    $campo_reg_select = "TA_REG.TADESC AS REGIONE, TA_PROV.TADESC AS PROVINCIA, ";
	    $join_reg = "LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA_COM
            	    ON TA_COM.TAKEY4 = TD.TDNAZI AND TA_COM.TAKEY2 = TF.TFPROV AND TA_COM.TADESC = TF.TFLOCA AND TA_COM.TATAID = 'ANCOM'
            	    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA_REG
            	    ON TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
            	    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA_PROV
            	    ON TA_PROV.TAKEY1 = TF.TFPROV AND TA_PROV.TATAID = 'ANPRO'";
	    
	    $campo_reg_group  = ", TA_REG.TADESC, TA_PROV.TADESC";
	}

	$sql = "
	        SELECT {$campo_name} AS TDASPE, {$campo_des} AS TDASPE_D, 
	        {$campo1_select} {$campo2_select} {$campo_reg_select}
			COUNT(*) AS NUM_ORD, SUM(TFINFI) AS S_IMP
			FROM {$cfg_mod_DeskPVen['fatture_anticipo']['file_testate']} TF
			LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
            ON TF.TFDT = TD.TDDT AND TF.TFDOAB = TD.TDDOCU
            {$join_reg}
			WHERE TFDT = '{$id_ditta_default}' AND TFTIDO='VF'
			{$filter_where}
			GROUP BY {$campo_name}, {$campo_des} {$campo1_group} {$campo2_group} {$campo_reg_group}
			ORDER BY SUM(TFINFI) DESC, COUNT(*) DESC
			";

			if ($top > 0)
				$sql .= " FETCH FIRST {$top} ROWS ONLY ";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			

			$ar_records = array();
			while ($r = db2_fetch_assoc($stmt)){

				//decodificare descrizione se necessario
				
			    if ($campo == 'tree_localita'){
			        $r['TDASPE'] = "[".trim($r['TDASPE'])."] ".$r['REGIONE'];
			        $r['TDASPE_D'] = '';
			        $r['CAMPO1_DES'] = $r['PROVINCIA'];
			    }
				
				$ar_records[] = $r;
			}
			

			$tot_ar_ret = 0;
			foreach($ar_records as $tot_ar)
				$tot_ar_ret += 1; $tot_ar['NUM_ORD'];

				
			$ar_ret = array();
			foreach($ar_records as $ar1){
				$ar_ret[] = $ar1;
			}
	return $ar_ret;
} //get_json_data_chart2