<?php

require_once("../../config.inc.php"); 
require_once("acs_panel_fatture_anticipo_include.php");

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$cod_mod_provenienza = $main_module->get_cod_mod();


//elenco nazioni combo
function get_ar_nazioni(){
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFNAZI, TFDNAZ
			FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
			WHERE TFTIDO='VA'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['TFNAZI']), "text" => $r['TFDNAZ']);
	}
	return $ar;
}

function get_ar_tipo_documento(){
	global $id_ditta_default;
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFTPDO AS COD, TFDTPD AS DES 
			FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']} 
			WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VA'
			GROUP BY TFTPDO, TFDTPD ORDER BY TFDTPD";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES'] . " [" . trim($r['COD'] . "]"));
	}
	return $ar;
}

function get_ar_stato_fattura(){
	global $id_ditta_default;
	global $conn, $cfg_mod_Gest;
	global $main_module;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql = "SELECT TFSTAT AS COD, TFDSST AS DES
			FROM {$cfg_mod_Gest['fatture_anticipo']['file_testate']}
			WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VA'
			GROUP BY TFSTAT, TFDSST ORDER BY TFDSST";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = array("id" => trim($r['COD']), "text" => $r['DES'] . " [" . trim($r['COD'] . "]"));
	}
	return $ar;
}


if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();
	
	$ar=crea_ar_tree_fatture_anticipo($_REQUEST['node'], $m_params->open_request->form_values);
	
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}

	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}




// ******************************************************************************************
// INVIO A GESTIONALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_send_to_gest'){
	$m_params = acs_m_params_json_decode();

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $id_ditta_default);
	$cl_p .= sprintf("%-3s", '*ST');

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31P1('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31P1', $libreria_predefinita_EXE, $cl_in, null, null);
		
		//$call_return = $tkObj->PgmCall('UTIBR31G5', $libreria_predefinita_EXE, '', null, null);
	}

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}
####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TFCCON, TFDCON
	FROM {$cfg_mod['fatture_anticipo']['file_testate']}
	WHERE TFDT='{$id_ditta_default}' AND UPPER(
	REPLACE(REPLACE(TFDCON, '.', ''), ' ', '')
	) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
					GROUP BY TFCCON, TFDCON
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array("cod"=> trim($row['TFCCON']),"descr"=> acs_u8e(trim($row['TFDCON'])));
	}

	echo acs_je($ret);

	exit;
}




if ($_REQUEST['fn'] == 'open_parameters'){
	?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "PV_FAT_ANT");  ?>{
			            text: 'Aggiorna',
			            iconCls: 'icon-button_black_repeat_dx-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=exe_send_to_gest', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }, {
	            text: 'Rapporto<br>sincronizzazione',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
	         
							acs_show_panel_std('acs_panel_rapporto_sincronizzazione.php?fn=open_tab', 'panel_fatture_anticipo', {chiave: 'ANT_REPORT'});
							this.up('window').close();
			
	            }
	        }, {
	         	xtype: 'splitbutton',
	            text: 'Reports',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	
		        		{
	                     xtype: 'button',
		            	 scale: 'medium',
	                     text: 'Riepilogo anticipi da scalare',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_panel_fatture_anticipo_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        },{
	                     xtype: 'button',
		            	 scale: 'medium',
	                     text: 'Riepilogo fatture anticipo emesse',
				            handler: function() {
				            
			                form = this.up('form').getForm();
			                
			                if (form.isValid()){	                	                
				                form.submit({
				                		url: 'acs_clienti_con_anticipi_report.php',
						                standardSubmit: true,
				                        method: 'POST',
										target : '_blank',
										params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
				                });
			                }								            
				            
				         }
				        },{
						xtype : 'button',
			            text: 'Riepilogo caparre da scalare',
			            //iconCls: 'icon-print-32',
			            scale: 'medium',
			            handler: function() {
		                    form = this.up('form').getForm();
	                        this.up('form').submit({
	                        url: 'acs_panel_fatture_anticipo_caparre_report.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',                        
	                        params: {
	                            form_values: Ext.encode(form.getValues()),

	                         }
                  			});            	                	     
            	                	                
	            }
			         }, {
						xtype : 'button',
			            text: 'Riepilogo caparre incassate',
			            //iconCls: 'icon-print-32',
			            scale: 'medium',
			            handler: function() {
		                    form = this.up('form').getForm();
	                        this.up('form').submit({
	                        url: 'acs_panel_fatture_anticipo_caparre_report.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',                        
	                        params: {
	                            form_values: Ext.encode(form.getValues()),
	                            t_caparre : 'Y'

	                         }
                  			});            	                	     
            	                	                
	            }
			         }, {
						xtype : 'button',
			            text: 'Riepilogo fatture di anticipo/caparre detratte',
			            //iconCls: 'icon-print-32',
			            scale: 'medium',
			            handler: function() {
		                    form = this.up('form').getForm();
	                        this.up('form').submit({
	                        url: 'acs_fatt_ant_caparre_detratte_report.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',                        
	                        params: {
	                            form_values: Ext.encode(form.getValues()),
	                            t_caparre : 'Y'

	                         }
                  			});            	                	     
            	                	                
	            }
			         }
				      
				            	        	
		        	]
		        }
	        }, {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_fatture_anticipo.php?fn=open_tab', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }],   		            
		            
		            items: [
		         	  	{
           				 xtype: 'combo',
						name: 'f_cliente_cod',
						fieldLabel: 'Cliente',
						minChars: 2,	
						anchor: '-15',		
           				margin: "20 25 10 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		$cfg_mod = $main_module->get_cfg_mod();
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }, {
							name: 'f_sede',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Sede',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?>	
								    ] 
							}						 
							}, {
								name: 'f_tipo_documento',
								xtype: 'combo',
								multiSelect: true,
								fieldLabel: 'Tipo fattura anticipo',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,		
							    anchor: '-15',
							    margin: "10 10 10 10",						   													
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
									     <?php echo acs_ar_to_select_json(get_ar_tipo_documento(), ""); ?>	
									    ] 
								}						 
							}, {
								name: 'f_tipo_pagamento',
								xtype: 'combo',
								multiSelect: true,
								fieldLabel: 'Tipo pagamento caparra',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,		
							    anchor: '-15',
							    margin: "10 10 10 10",						   													
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
									     <?php echo acs_ar_to_select_json($main_module->find_TA_std('TPCAP', null, 'N', 'N', null, null, null, 'N', 'Y'), ""); ?>	
									    ] 
								}						 
							}, {
							name: 'f_stato_ordine',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Stato ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 10 10",
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?>	
								    ] 
							}						 
						}, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "10 10 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data fatture da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "10 25 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data fatture a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_ordine_da'
							   , margin: "10 10 10 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data ordine da'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_ordine_a'
							   , margin: "10 25 10 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data a'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }
					 
						
						
						,{
							name: 'f_filtra_fatture',
							xtype: 'radiogroup',
							fieldLabel: 'Fatture',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "10 10 10 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Tutte'
		                          , inputValue: 'T'
		                          , width: 50
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Chiuse'
		                          , inputValue: 'C'
		                          , checked: false
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_filtra_fatture' 
		                          , boxLabel: 'Aperte'
		                          , inputValue: 'A'
		                          , checked: false
		                        }]
						},  {				 
							xtype: 'checkboxgroup',
							margin: "10 10 10 10",
							fieldLabel: '',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_filtro_ordini' 
		                          , boxLabel: 'Solo clienti con ordini'
		                          , checked: false
		                          , inputValue: 'Y'
		                        },{
		                            xtype: 'checkbox'
		                          , name: 'f_filtro_anticipi' 
		                          , boxLabel: 'Solo clienti con anticipi'
		                          , checked: false
		                          , inputValue: 'Y'
		                        }]														
						 } 
										 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}
	]
}		
	
		
<?php
	exit;
}



if ($_REQUEST['fn'] == 'open_tab'){
	$m_params = acs_m_params_json_decode();
	?>

{"success":true, "items": [

        {
            xtype: 'treepanel',
	        title: 'Prepayment',
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Interrogazioni fatture di anticipo e relative detrazioni</b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),             
			
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'codice', 'rif', 'data', 'TFTOTD', 'TFTIMP', 'detratto', 
				    'residuo', 'residuo_ord', 'stato', 'liv', 'liv_cod_qtip', 'importo_ord', 
				    'caparra', 'stato_out', 'perc', 'fat_merce', 'importo_fat', 'imp_note_credito', 
				    'tipo_caparra', 'TFIMP5', 'c_paga', 'd_paga'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
					
                        extraParams: {
                                    open_request: <?php echo acs_je($m_params) ?>
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData, 
								

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }       				
                    }

                }),
				

		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            flex: 3,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Sede/Cliente/Fattura di anticipo'
			        }, { 
			            dataIndex: 'codice',
			            header: 'Codice', 
			            flex: 1,
			            renderer: function(value, metaData, record){
						
					    	if (record.get('liv_cod_qtip') != ''){
				    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('liv_cod_qtip')) + '"';			    	
					    	}						
						
						 return value;
						}
			        },{ 
			            dataIndex: 'data',
			            header: 'Data', 
			            renderer: date_from_AS,
			            width: 60
			        },  { 
			            dataIndex: 'importo_ord',
			            header: 'Imp. ordini', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'd_paga',
			            header: 'Pagamento', 
			            flex: 1,

			        },{ 
			            dataIndex: 'stato_out',
			            header: 'Stato', 
			             width: 70
			          
			        },{ 
			            dataIndex: 'caparra',
			            header: 'Caparra', 
			            width: 70,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'residuo_ord',
			            header: 'Residuo <br>ordini', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'perc',
			            header: '% Residuo', 
			            flex: 1,
			            align: 'right', 
			            renderer: perc2
			        },{ 
			            dataIndex: 'TFTOTD',
			            header: 'Tot. anticipi', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'TFTIMP',
			            header: 'Imponibile <br>anticipi', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, { 
			            dataIndex: 'detratto',
			            header: 'Anticipi <br> scalati ', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'residuo',
			            header: 'Residuo <br> anticipi', 
			            flex: 1,
			            align: 'right', 
			            //coloro la cella
			            renderer: function (value, metaData, record, row, col, store, gridView){

                               /* if (parseFloat(record.get('residuo')) > 0){
                                    metaData.tdCls += ' sfondo_rosso';
                                    }*/
                            //ritorno il valore da visualizzare (con eventuale formattazione data, numero, ....)
                            return floatRenderer2(value); }
			        }, { 
			            dataIndex: 'stato',
			            header: 'St.', 
			            align: 'center',
			            width: 40,
			            renderer: function(value, metaData, record){
			            
						    	if (record.get('stato') == 'C') 
						    	return '<img src=<?php echo img_path("icone/48x48/lock_grey.png") ?> width=18>';
						    	    	
								if (record.get('stato') == 'M') 
								return '<img src=<?php echo img_path("icone/48x48/lock.png") ?> width=18>';	
											
						    }
			        },
			        { 
			            dataIndex: 'fat_merce',
			            header: 'Fatturato <br> merce', 
			            flex: 1,
			            align: 'right', 
			            //coloro la cella
			            renderer: function (value, metaData, record, row, col, store, gridView){

                            return floatRenderer2(value); }
			        },
			        { 
			            dataIndex: 'importo_fat',
			            header: 'Importo <br> fatture', 
			            flex: 1,
			            align: 'right', 
			            //coloro la cella
			            renderer: function (value, metaData, record, row, col, store, gridView){

                              
                            return floatRenderer2(value); }
			        },
			        { 
			            dataIndex: 'imp_note_credito',
			            header: 'Importo <br> note credito', 
			            flex: 1,
			            align: 'right', 
			            //coloro la cella
			            renderer: function (value, metaData, record, row, col, store, gridView){

                            return floatRenderer2(value); }
			        }
			     
	         ],
	         
	          listeners: {
	          
	           beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                        load: function () {
                          Ext.getBody().unmask();
                        },
                        
                   celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();

		
						
						if ( rec.get('liv')=='liv_2'){
						
							iEvent.preventDefault();
							acs_show_win_std('Elenco documenti cliente', 'acs_documenti_cliente.php?fn=open_list', {
								cliente_selected : rec.get('codice')
							}, 1024, 600, {}, 'icon-leaf-16');
							
								return false;
							
						}
						
						

					 }
					}
	         , itemcontextmenu : function(grid, rec, node, index, event) {
		           	var voci_menu = [];
		         	event.stopEvent();
					var grid = this;
					
						//livello cliente
	    			if (rec.get('liv') == 'liv_2'){
	    		
	    			
	    			
	    			  voci_menu.push({
		          		text: 'Report e/conto',
		        		iconCls : 'icon-print-16',	          		
		        		handler: function() {
		        			window.open('acs_report_ec.php?fn=open_report&tipo=001&f_cliente_cod='+rec.get('codice'));
		        											
		        			}
	    			});
	    			
	    			}
	    			
	    						
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);
					
					}
	          }	
	         
	         
	         , viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }																				  			
	            
        }  

]
}

<?php exit; }

