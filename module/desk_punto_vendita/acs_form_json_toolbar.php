<?php
require_once "../../config.inc.php";


?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			cls: 'acs_toolbar',
			tbar: [
					{
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Avanzate',
			            items: [{
			                xtype:'splitbutton',
			                iconCls: 'icon-unlock-32',
			                text: 'Avanzate',
			                scale: 'large',
			                iconAlign: 'top',
			                arrowAlign:'bottom',
			                menu: {
			                	xtype: 'menu',
		        				items: [	
		        				  	
						{
							xtype: 'button',
				            text: 'Gestione cambio stati',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  acs_show_panel_std('../desk_vend/acs_gestione_stato_ordini.php?fn=open_grid', 'panel_gestione_stati', "");	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        }, 
				        //COMMENTATA, USARE SOLO PER INSERIRE NUOVI DOCUMENTI
				        {
							xtype: 'button',
				            text: 'Gestione documenti logistica',
				            //iconCls: 'icon-print-32', 
				            scale: 'small',
				            handler: function() {
				            	  acs_show_panel_std('acs_gestione_documenti_logistica.php?fn=open_grid', 'panel_gestione_doc_ddt', "");	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        }
		        				 ]
			                
			                }
			            }]
			        }, {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Base',
			            items: [
			     
			     

// ***************************************************************************************
// Vettori
// ***************************************************************************************
			            {
			                text: 'Vettori<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-user_group-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Vettori(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "vettori") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "vettori") ?>
								<?php echo $cl->out_Writer_Model("vettori") ?>
								<?php echo $cl->out_Writer_Store("vettori") ?>
								<?php echo $cl->out_Writer_sotto_main("vettori") ?>
								<?php echo $cl->out_Writer_main("Lista vettori", "vettori") ?>								
								<?php echo $cl->out_Writer_window("Tabella vettori", "SETUP_VETTORI") ?>							

								} //handler function()
			            }, 
			            
// ***************************************************************************************
// Trasportatori
// ***************************************************************************************
			            {
			                text: 'Aziende',
			                scale: 'large',			                 
			                iconCls: 'icon-address_book-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new Trasportatori(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent('Y'), "trasportatori") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "trasportatori") ?>
								<?php echo $cl->out_Writer_Model("trasportatori") ?>
								<?php echo $cl->out_Writer_Store("trasportatori") ?>
								<?php echo $cl->out_Writer_sotto_main("trasportatori") ?>
								<?php echo $cl->out_Writer_main("Lista aziende", "trasportatori") ?>								
								<?php echo $cl->out_Writer_window("Tabella aziende", "SETUP_TRASPORTATORI") ?>							

								} //handler function()
			            },   
			         
				        
				          {
							xtype: 'button',
				            text: 'Architetti',
				            iconCls: 'icon-design-32', 
				            iconAlign: 'top',	
				            scale: 'large',		
				            handler: function() {
				            	  	acs_show_win_std('Tabella architetti', 
										'acs_g_architetti.php?fn=open_grid', {tataid : 'ARCHI'}, 950, 500, null, 'icon-design-16', 'Y');
	          	                	                
				           			panel = this.up('menu').up('buttongroup').up('panel').up('window');
				           			panel.close();
				            }
				        }
			            
			        
		            
			    	]
			   }
			]
		}
	]		
}            