<?php

require_once "../../config.inc.php";


$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$mod_js_parameters = $main_module->get_mod_parameters();

if ($_REQUEST['fn'] == 'exe_conferma_articoli'){
    $ret_esito = '';

	$m_params = acs_m_params_json_decode();
	
	$sql_where = "";
	$ord = $s->get_ordine_by_k_ordine($m_params->k_ordine);
	$tipologia = trim($ord['TDCLOR']);
	if(isset($mod_js_parameters->Ass_tipo_parte) && $tipologia == 'A')
	    $sql_where .= " AND ARTPAR = '{$mod_js_parameters->Ass_tipo_parte}'";
	    
	
	$k_ordine = $m_params->k_ordine;
	$articoli= $m_params->articoli;

	foreach ($articoli as $v){
		
		if(isset($v)){
		    
		    
		    if($m_params->esclusi_sosp == 'Y'){
		        $select = ", ARSOSP";
		        $groupby = " GROUP BY ARSOSP";
		    }
		    
	
	        $sql="SELECT COUNT(*) AS C_ART {$select}
	        FROM {$cfg_mod_DeskPVen['file_anag_art']} AR
	        WHERE ARDT = '$id_ditta_default' {$sql_where}
	        AND ARFOR1 <> 0 AND ARART ='{$v->codice}' {$groupby}";
	        
		        
	        $stmt = db2_prepare($conn, $sql);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt);
	        $row = db2_fetch_assoc($stmt);
	   
	        if($m_params->esclusi_sosp == 'Y'){
	            
	            if($row['ARSOSP'] == 'S'){
	                $ret['success'] = false;
	                $ret['message_cod'] = 'art_sospeso';
	                echo acs_je($ret);
	                exit;
	            }
	            
	        }
		

		if($v->prezzo == ""){
			$v->prezzo=0;
		}
		
		$sh = new SpedHistory();
		$ret_RI = $sh->crea(
				'aggiungi_articoli',
				array(
						"k_ordine"			=> $k_ordine,
						"codice" 			=> $v->codice,
						"articolo"		    => $v->articolo,
						"quant"		        => $v->quant,
						"prezzo"		    => $v->prezzo
				)
				);

        if (strlen(trim($ret_RI['RINOTE'])) > 0)
		      $ret_esito = trim($ret_RI['RINOTE']);
		}
		
		
		
		if(count($v->note) > 0){
		    
		    for($i=1; $i <= 5 ; $i++){
		        
		        $nota = "nota_{$i}";
		        if(strlen($v->note[0]->$nota) > 0){
		            
		            $sh = new SpedHistory();
		            $sh->crea(
		                'aggiungi_articoli',
		                array(
		                    "k_ordine"		=> $k_ordine,
		                    "codice" 		=> "*",
		                    "articolo"		=> $v->note[0]->$nota,
		                    "quant"		    => 0,
		                    "prezzo"		=> 0
		                )
		                );
		        }
		    }
		    
		}
		

	   }
	   

	

	   $ret = array();
	   $ret['success'] = true;
	   
	   if (strlen($ret_esito) > 0)
	     $ret['message'] = $ret_esito;
	   
	   echo acs_je($ret);
	   exit();
}



if ($_REQUEST['fn'] == 'get_json_data_articoli'){

	$m_params = acs_m_params_json_decode();
	
	
	$codice= $m_params->open_request->codice;
	$fornitore= $m_params->open_request->fornitore;
	$d_art= $m_params->open_request->d_art;
	$gruppo= $m_params->open_request->gruppo;
	$sottogruppo= $m_params->open_request->sottogruppo;
	
	if(strlen($codice) > 0){
	    $where_art .= "AND ARART LIKE '%{$codice}%'";
	}
	
	if(strlen($_REQUEST['query']) > 0){
	    $where_art .= "AND ARART LIKE '%{$_REQUEST['query']}%'";
	}
	
	
	if(strlen($d_art) > 0){
	    $where_art .= "AND ARDART LIKE '%{$d_art}%'";
	}

	if(strlen($fornitore) > 0){
	    $where_art .= "AND ARFOR1 = '{$fornitore}'";
	}
	
	if(strlen($gruppo) > 0){
	    $where_art .= "AND ARGRME = '{$gruppo}'";
	}
	
	
	if(strlen($sottogruppo) > 0){
	    $where_art .= "AND ARSGME = '{$sottogruppo}'";
	}
	
	if($m_params->open_request->esclusi_sosp == 'Y')
	    $where_art .= " AND ARSOSP <> 'S'";
	
    //da config.inc.php
    if (isset($cfg_mod_DeskPVen["add_where_to_elenco_articoli"]))
        $where_art .= " " . $cfg_mod_DeskPVen["add_where_to_elenco_articoli"] . " ";
	    
	$sql="SELECT ARART, ARDART, ARVDI2, ARFOR1, ARCLME, CF.CFRGS1 AS D_FORN, ARTPAR
			FROM {$cfg_mod_DeskPVen['file_anag_art']} AR 
			LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_cli']} CF
				ON CF.CFDT = AR.ARDT AND CF.CFCD = AR.ARFOR1
			WHERE ARDT = '{$id_ditta_default}' 
				  AND ARFOR1 <> 0 $where_art
			";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['codice']	    = trim($row['ARART']);
		$nr['articolo']  	= trim($row['ARDART']);
		$nr['fornitore']  	= trim($row['D_FORN']);
		$nr['groupfield']   = trim($row['ARCLME'])."_".trim($row['ARFOR1']);
		
		$ta_sys = find_TA_sys('MUCM', trim($row['ARCLME']));
		$nr['header']       = "Classe : {$ta_sys[0]['text']} - Fornitore: {$row['D_FORN']}"; 
		$nr['tipo_parte']  	= trim($row['ARTPAR']);
		$ar[] = $nr;

	}

	echo acs_je($ar);
	exit;
}



$m_params = acs_m_params_json_decode();
?>


{"success":true, "items": [

			{
			xtype: 'grid',
	        loadMask: true,	
	        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
		    stateful: true,
            stateId: 'seleziona-elenco-articoli',
            stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
            features: [
              
              {ftype: 'grouping',  startCollapsed: true, groupHeaderTpl: '{[values.rows[0].data.header]}',},
              {ftype: 'filters',
                autoReload: false,
                local: true,
                filters: [{
                    dataIndex: 'articolo',
                    type: 'string'
                }, {
                    dataIndex: 'fornitore',
                    type: 'string'
                }]
            }],
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_articoli', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['codice', 'articolo', 'quant', 'prezzo', 'fornitore', 'tipo_parte', 'groupfield', 'header'],
		        			groupField: 'groupfield', 
		        		  
			}, //store
				
			      columns: [	
			      {
	                header   : 'Codice',
	                dataIndex: 'codice',
	                flex: 1,
	                },
	                {
	                header   : 'Articolo',
	                dataIndex: 'articolo',
	                flex: 1,
	                },
	              /*  {
	                header   : 'Tipo parte',
	                dataIndex: 'tipo_parte',
	                flex: 1,
					//filter: {type: 'string'}, filterable: true,
	                
	                },*/
	                <?php if($m_params->from_mts != 'Y'){ ?>
	                {
	                header   : 'Quantit&agrave;',
	                dataIndex: 'quant',
	                flex: 1,	                
	                align: 'right',
	                editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }
	                },  
	                {
	                header   : 'Prezzo',
	                dataIndex: 'prezzo',
	                align: 'right',
	                flex: 1,
	                editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }
	                }
	               <?php }else{?>
	          		
	          		{
	                header   : 'Fornitore',
	                dataIndex: 'fornitore',
	                flex: 1,
				//	filter: {type: 'string'}, filterable: true,
	                
	                }
	          	
	          	 <?php }?>
	        
	         ] ,
	         
	           <?php if($m_params->from_mts != 'Y'){ ?>
	           
	           listeners: {
                    groupclick: function (view, node, group, e, eOpts) {
                        view.features[0].collapseAll();
                        view.features[0].expand(group);
                    }
                },
	           
	          dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-print-32',
                     text: 'Conferma articoli',
			            handler: function() {
			        
			            var loc_win = this.up('window');
			            var grid = this.up('grid');
			            articoli_selected = grid.getStore().getUpdatedRecords();
                     
                         list_selected_art = [];
                           
                            for (var i=0; i<articoli_selected.length; i++) {
                            
                            	if(articoli_selected[i].get('quant') > 0){
                            
				            	list_selected_art.push({
				            	codice: articoli_selected[i].get('codice'),
				            	descr: articoli_selected[i].get('articolo'),
				            	quant: articoli_selected[i].get('quant'),
				            	prezzo: articoli_selected[i].get('prezzo'),
				            	tipo_parte: articoli_selected[i].get('tipo_parte'),
				            	
				            	});
				            	}
				            	
				            }
				            
				            
				            if (!Ext.isEmpty(loc_win.events.afterselectart))
								loc_win.fireEvent('afterSelectArt', loc_win, list_selected_art);
						     else{
						     
    						     Ext.Ajax.request({
    						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_articoli',
    						        method     : 'POST',
    			        			jsonData: {
    			        			    articoli : list_selected_art,
    			        				k_ordine: <?php echo j($m_params->k_ordine) ?>
    								},							        
    						        success : function(result, request){
    						             loc_win.fireEvent('afterAddArt', loc_win);
    						        },
    						        failure    : function(result, request){
    						            Ext.Msg.alert('Message', 'No data to be loaded');
    						        }
    						    });	
						     
						     }
				    
				    
				         
				    
				
			         
                   
	        				
			            }
			     }
			     
			     
			     ]
		   }]
	   	<?php }else{?>	
	   	  listeners: {
	         
	           celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	
					  	if (col_name=='codice'){
							iEvent.preventDefault();	
							acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: <?php echo $id_ditta_default; ?>, rdart: rec.get('codice')}, 1200, 600, null, 'icon-shopping_cart_gray-16');
							
							return false;							
					   }
	          
	            	}
	          
	           }
	         		
    
	         
	         
	         }
	         <?php }?>
			
		}//grid
		 
			
		
 				
	
     ]
        
 }