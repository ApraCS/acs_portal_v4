<?php 
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// elenco stabilimenti selezionabili da utente
// ******************************************************************************************


if ($_REQUEST['fn'] == 'get_parametri_form'){
	
	?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            
            items: [{
					     name: 'f_data'
					   , flex: 1                		
					   , xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Data registrazione'
					   , labelAlign: 'left'
					   , labelWidth: 120
					   , format: 'd/m/Y'
					   , submitFormat: 'Ymd'
					   , allowBlank: false
					   , anchor: '-15'
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
				 }
					}  ,{
							name: 'f_sede',
							xtype: 'combo',
							fieldLabel: 'Sede',
							displayField: 'des',
							valueField: 'cod',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    labelWidth: 120	,
						    value : '<?php echo $main_module->get_stabilimento_in_uso($auth)?>',				   													
								store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'cod'}, {name:'des'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->get_stabilimenti(), '') ?>	
								    ] 
							},				 
							}     
            ],
            
			buttons: [					
				{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	             handler: function() {
	                form = this.up('form').getForm();
	                            	                
		               if (form.isValid()){	                	                
			               acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_grid', 'allegati-documenti', {form_values: form.getValues()});
			               this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        
	        }
	        ]            
            
           
}
	
]}


<?php

exit;
}



if ($_REQUEST['fn'] == 'get_json_data_grid'){

	
	$ar = array();
	
	if(strlen($m_params->data)>0)
	    $sql_where .= " AND TFDTRG = '{$m_params->data}'";
	if(strlen($m_params->sede)>0)
	    $sql_where .= " AND TFNAZI = '{$m_params->sede}'";

	$sql = "SELECT TFDT, TFDOCU, TFCCON, TFDCON, TFAADO, TFNRDO, TFTPDO,  
            TFDTPD, TFDTRG, TFORGE, TFNAZI, TFTOTD, TFTIMP
	        FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
	        WHERE TFDT='{$id_ditta_default}' {$sql_where}";
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($row = db2_fetch_assoc($stmt)) {

	    //print_r($row); exit;
	    
		$nr = array();
		$nr['sede']  	    =  $s->decod_std('START', trim($row['TFNAZI'])); 
		$nr['k_doc']  	    = trim($row['TFDOCU']);
		$nr['doc_name']  	= implode("_", array($row['TFAADO'], $row['TFNRDO']));
		$nr['cod_cli']  	= trim($row['TFCCON']);
		$nr['cliente']  	= trim($row['TFDCON']);
		$nr['tipo']  	    = "[".trim($row['TFTPDO'])."] ".trim($row['TFDTPD']);
		$nr['totale']  	    = $row['TFTOTD'];
		$nr['imponibile']  	= $row['TFTIMP'];
		$nr['data_gen']  	= trim($row['TFDTRG']);
		$nr['ora_gen']  	= trim($row['TFORGE']);
		$nr['spunta_stampa']= '';
		
		//recupero allegato
		/* CONFIG */
		//$img_dir = "/SV2/ORDINI/";
		$img_dir = $cfg_mod_DeskPVen['allegati_root_C'];
		
		$dir_ordine = $img_dir . $row['TFCCON'];
		$dir_ordine = "{$dir_ordine}/FTCLI*" . trim($row['TFDT']) . "*" . trim($row['TFNRDO']) . "*" . trim($row['TFAADO']) . "*.*";
		
		$ret = array();
		foreach (glob($dir_ordine) as $filename)
		{
		    $nome_file_ar = explode('/', $filename);
		    $nome_file = end($nome_file_ar);
		    $r = array();
		    $r['DataCreazione'] = date ("d/m/Y H:d", filemtime($filename));
		    $r['IDOggetto']		 = acs_u8e($filename);
		    $r['des_oggetto']	 = acs_u8e($nome_file); //basename($filename);
		    //$r['tipo_scheda']	 = get_tipo_scheda(basename($filename));
		    
		    $nr['allegato_name'] = $nome_file;
		    $nr['allegato_path'] = $filename;
		    $nr['allegato_data'] = $r['DataCreazione'];
		}
		

		
		

		$ar[] = $nr;

	}

	echo acs_je($ar);
	exit;

}


if ($_REQUEST['fn'] == 'open_grid'){


	?>


{"success":true, "items": [

			{
		     xtype: 'grid',
		     title: 'Attachment',
			 loadMask: true,
			 tbar: new Ext.Toolbar({
	            items:['<b>Allegati/Documenti cartella cliente del <?php echo print_date($m_params->form_values->f_data); ?> </b>', '->'
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	    
	        <?php echo make_tab_closable(); ?>,
	        features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}],
	         store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								    url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 data: '<?php echo $m_params->form_values->f_data; ?>',
										 sede: '<?php echo $m_params->form_values->f_sede; ?>'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
							
		        			fields: [
		            			'sede','k_doc', 'doc_name', 'tipo', 'importo',  'cliente', 'cod_cli', 'data_gen', 'ora_gen', 'spunta_stampa'
		            			,'allegato_name', 'allegato_path', 'allegato_data', 'totale', 'imponibile'
		            			]						
									
			}, //store
			
			<?php $cf1 = "<img src=" . img_path("icone/48x48/sub_blue_accept.png") . " height=20>"; ?>
			
			      columns: [
			          {
			                header   : 'Sede',
			                dataIndex: 'sede', 
			                width: 120,
			                filter: {type: 'string'}, filterable: true
			             },
			       		{
			                header   : 'Data',
			                dataIndex: 'data_gen', 
			                width: 70,
			                renderer: date_from_AS,
			                filter: {type: 'string'}, filterable: true
			             }, {
			                header   : 'Tipo elenco documenti cliente',
			                dataIndex: 'tipo', 
			                flex: 1,
			                filter: {type: 'string'}, filterable: true
			             }, {
			                header   : 'Documento',
			                dataIndex: 'doc_name', 
			                width: 100,
			                filter: {type: 'string'}, filterable: true
			             },
			       		{
			                header   : 'Codice cliente',
			                dataIndex: 'cod_cli', 
			                width: 90,
			                filter: {type: 'string'}, filterable: true
			             },
			              {
			                header   : 'Denominazione cliente',
			                dataIndex: 'cliente', 
			                flex: 1,
			                filter: {type: 'string'}, filterable: true
			             },{
			                header   : 'Totale',
			                dataIndex: 'totale', 
			                width: 100,
			                align: 'right', 
			                renderer: floatRenderer2,
			                filter: {type: 'string'}, filterable: true
			             },{
			                header   : 'Imponibile',
			                dataIndex: 'imponibile', 
			                width: 100,
			                align: 'right', 
			                renderer: floatRenderer2,
			                filter: {type: 'string'}, filterable: true
			             },{
			                header   : 'Allegato',
			                dataIndex: 'allegato_name', 
			                flex: 1,
			                filter: {type: 'string'}, filterable: true
			             },{
			                header   : 'Data allegato',
			                dataIndex: 'allegato_data', 
			                width: 120,
			                filter: {type: 'string'}, filterable: true
			             },{
			               text: '<?php echo $cf1; ?>',
			               width     : 30,
			               tdCls: 'tdAction',         			
                           menuDisabled: true, sortable: false,  
                           dataIndex: 'spunta_stampa',          		        
			               renderer: function(value, p, record){
			               if (record.get('spunta_stampa') == 'Y') return '<img src=<?php echo img_path("icone/48x48/sub_blue_accept.png") ?> width=15>';}   
			                
		             }        
			             
			            
			         ]
			         
			         , listeners: {		
	 				
			 				
			 				
		       			 itemcontextmenu : function(grid, rec, node, index, event) {
				  			event.stopEvent();				  													  
						  	var voci_menu = [];

						  	
						  	 voci_menu.push({
				         		text: 'Elenco generale documenti cliente',
				        		iconCls : 'icon-leaf-16',          		
				        		handler: function () {
									acs_show_win_std('Elenco documenti cliente', 'acs_documenti_cliente.php?fn=open_list', {cliente_selected: rec.get('cod_cli')}, 1024, 600, null, 'icon-leaf-16');
					                }
				    		});
				    		
				    		 voci_menu.push({
				         		text: 'Elenco allegati cliente',
				        		iconCls : 'icon-leaf-16',          		
				        		handler: function () {
									acs_show_win_std('Elenco allegati cliente', 'acs_allegati_cliente.php?fn=open_win', {cliente_selected: rec.get('cod_cli')}, 1024, 600, null, 'icon-leaf-16');
					                }
				    		});
				    		
						      var menu = new Ext.menu.Menu({
						            items: voci_menu
							}).showAt(event.xy);	
				    									  	
						 },   
						 
					 celldblclick: {
		           
			           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			           
			           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
						  	rec = iView.getRecord(iRowEl);
						 
						   
						   if (col_name == 'spunta_stampa'){
						   			if(rec.get('spunta_stampa') == '')
						  				rec.set('spunta_stampa', 'Y');
							 	    else
							 			rec.set('spunta_stampa', '');
							
							 /* Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_spunta_blu',
							        method     : 'POST',
				        			jsonData: {
				        			    flag: rec.get('f_spunta'),
				        			    k_ordine: rec.get('k_ordine')
									},							        
							        success : function(result, request){
				            			jsonData = Ext.decode(result.responseText);
				            		 	rec.set('f_spunta', jsonData.flag);
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	 */
	
								
							} //spunta
							
							if (col_name == 'allegato_name' && Ext.isEmpty(rec.get('allegato_path')) == false){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('allegato_path'));
							 }							
	          
	            	}
	          
	           }
	 			 
	 			      }
	 			      
	 			      ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					          
					          					           		
					           return '';																
					         }   
					    }
         
	   		
			
		}//grid
		 
			
		
 				
	
     ]
        
 }
 
  <?php 
 exit;
 }