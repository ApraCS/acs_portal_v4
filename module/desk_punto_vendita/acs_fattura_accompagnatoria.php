<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$raw_post_data = acs_raw_post_data();
if (strlen($raw_post_data) > 0)
{
	$m_params = acs_m_params_json_decode();
	$k_ordine = $m_params->k_ordine;
}

if (isset($_REQUEST['k_ordine']))
	$k_ordine = $_REQUEST['k_ordine'];

	$oe = $s->k_ordine_td_decode_xx($k_ordine);


if ($_REQUEST['fn'] == 'get_json_data'){
	
	
    $m_params = acs_m_params_json_decode();
    
    $sql_where = "";
    if($m_params->ord_ass != 'Y' && $m_params->ord_reso != 'Y' && $m_params->ord_comp != 'Y')
        $sql_where = " AND RD.RDSTEV <>'S' AND RDQTA > RDQTE";
    
	$data = array();

	$sql = "SELECT RD.*, RTPRZ, RTINFI
	        FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
	        ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
	        WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
	        AND RD.RDTISR = '' AND RD.RDSRIG = 0 ";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $oe);

	while ($row = db2_fetch_assoc($stmt)) {
		$row['data_selezionata'] = sprintf("%08s", $dtep);
		$row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
		if ($row['data_selezionata'] == $row['data_consegna'])
			$row['inDataSelezionata'] = 0;
		else $row['inDataSelezionata'] = 1;
			$row['RDART'] = acs_u8e($row['RDART']);
			$row['RDDART'] = acs_u8e($row['RDDART']);
			$row['RDNREC'] = acs_u8e($row['RDNREC']);
			$row['RTPRZ'] = acs_u8e($row['RTPRZ']);
			$row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
			$row['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
			$data[] = $row;
	}


	//metto prima le righe della data selezionata
	////usort($data, "cmp_by_inDataSelezionata");
		
	$ord_exp = explode('_', $n_ord);
	$anno = $ord_exp[3];
	$ord = $ord_exp[4];
	$ar_ord =  $s->get_ordine_by_k_docu($n_ord);

	$m_ord = array(
			'TDONDO' => $ar_ord['TDONDO'],
			'TDOADO' => $ar_ord['TDOADO'],
			'TDDTDS' => print_date($ar_ord['TDDTDS'])
	);
		
	echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";

	$appLog->save_db();
	exit();
}


if ($_REQUEST['fn'] == 'exe_genera_fatt'){
    $ret_RI = array();

	$m_params = acs_m_params_json_decode();
	$use_session_history = microtime(true);
		
	$list_righe=$m_params->righe;
	
	if($m_params->ord_ass != 'Y' && $m_params->ord_reso != 'Y' && $m_params->ord_comp != 'Y'){
	
	$sh = new SpedHistory();
	$sh->crea(
			'genera_fatt_acc',
			array(
					"k_ordine"		=> $m_params->k_ordine,
					"use_session_history" =>  $use_session_history,
					"nota" 		    => '*',  //la mia nuova preferenza
					"tipologia"     => 'N',
					"text_nota" 	=> $m_params->form_values->f_note_1
					
			)
			);
	
	$sh = new SpedHistory();
	$sh->crea(
			'genera_fatt_acc',
			array(
					"k_ordine"		=> $m_params->k_ordine,
					"use_session_history" =>  $use_session_history,
					"nota" 		    => '*',  //la mia nuova preferenza
					"tipologia"     => 'N',
					"text_nota" 	=>$m_params->form_values->f_note_2
						
			)
			);
	
	$sh = new SpedHistory();
	$sh->crea(
			'genera_fatt_acc',
			array(
					"k_ordine"		=> $m_params->k_ordine,
					"use_session_history" =>  $use_session_history,
					"nota" 		    => '*',  //la mia nuova preferenza
					"tipologia"     => 'N',
					"text_nota" 	=> $m_params->form_values->f_note_3
						
			)
			);
	
	$sh = new SpedHistory();
	$sh->crea(
			'genera_fatt_acc',
			array(
					"k_ordine"		=> $m_params->k_ordine,
					"use_session_history" =>  $use_session_history,
					"nota" 		    => '*',  //la mia nuova preferenza
					"tipologia"     => 'N',
					"text_nota" 	=> $m_params->form_values->f_note_4
						
			)
			);
	
	$sh = new SpedHistory();
	$sh->crea(
			'genera_fatt_acc',
			array(
					"k_ordine"		=> $m_params->k_ordine,
					"use_session_history" =>  $use_session_history,
					"nota" 		    => '*',  //la mia nuova preferenza
					"tipologia"     => 'N',
					"text_nota" 	=> $m_params->form_values->f_note_5
						
			)
			);
	
	
	if(strlen($m_params->form_values->f_parz) >0){
	
		foreach ($list_righe as $v){
		
		
			$sh = new SpedHistory();
			$sh->crea(
					'genera_fatt_acc',
					array(
							"k_ordine"		=> $m_params->k_ordine,
							"use_session_history" =>  $use_session_history,
							"nrec"              =>  $v->nrec,
							"tipologia"         =>  'R',
							
							
					
					)
			);
		}
	
	}
	
	$sh = new SpedHistory();
	$ret_RI = $sh->crea(
			'genera_fatt_acc',
			array(
					"k_ordine"		=> $m_params->k_ordine,
					"data" 			=> $m_params->form_values->f_data,
					"tipo_paga" 	=> $m_params->form_values->f_pagamento,
					"trasp" 	    => $m_params->form_values->f_trasp,
					"causale" 	    => $m_params->form_values->f_causale,
					"flag_parz"		=> $m_params->form_values->f_parz,
			        "fatt_corr"		=> $m_params->form_values->f_fatt_c,
			        "a_mezzo"       => $m_params->form_values->f_a_mezzo,
    			    "imp_trasp_cli"		=> sql_f($m_params->form_values->f_trasp_cli),
			        "imp_trasp_ns"		=> sql_f($m_params->form_values->f_trasp_ns),
					"end_session_history" =>  $use_session_history
			)
			);
	
	}elseif($m_params->ord_reso == 'Y'){
	    
	    foreach ($list_righe as $v){
	        
	        $sh = new SpedHistory();
	        $sh->crea(
	            'pers',
	            array(
	                "messaggio"	=> 'GEN_ORD_RESO',
	                "k_ordine"	=> $m_params->k_ordine,
	                "use_session_history" =>  $use_session_history,
	                "vals" => array("RINRCA" => $v->nrec)
	                
	                
	            )
	            );

	    }
	    
	    
	    $sh = new SpedHistory();
	    $ret_RI= $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'GEN_ORD_RESO',
	            "k_ordine"	=> $m_params->k_ordine,
	            "end_session_history" =>  $use_session_history,
	            "vals" => array("RICITI" => $m_params->causale,
	                            "RIVETT" => $m_params->prior)
	            
	            
	        )
	        );
	 
	    
	}elseif($m_params->ord_ass == 'Y'){

	    foreach ($list_righe as $v){
	
	        if($v->nrec == 0){
	            $articolo = $v->RDART;
	            $quant = $v->RDQTA;
	            $prezzo = $v->RTPRZ;
	         }else{
	             $articolo = "";
	             $quant = 0;
	             $prezzo = 0;
	             
	         }
	      
	        $sh = new SpedHistory();
	        $sh->crea(
	            'crea_ordine_assistenza',
	            array(
	                "k_ordine"		=> $m_params->k_ordine,
	                "use_session_history" =>  $use_session_history,
	                "nrec"              =>  $v->nrec,
	                "articolo"     => $articolo,
	                "quant"        => $quant,
	                "prezzo"       => $prezzo
	                

	            )
	            );
	    }

	
	$sh = new SpedHistory();
	$ret_RI = $sh->crea(
	    'crea_ordine_assistenza',
	    array(
	        "k_ordine"		=> $m_params->k_ordine,
	        "causale"           =>  $m_params->causale,
	        "prior"           =>  $m_params->prior,
	        "end_session_history" =>  $use_session_history
	    )
	    );
	
	
	if($m_params->reso == 'Y'){
	    $use_session_history = microtime(true);
	    foreach ($list_righe as $v){
	        
	        
	        if($v->nrec == 0){
	            $articolo = $v->RDART;
	            $quant = $v->RDQTA;
	            $prezzo = $v->RTPRZ;
	        }else{
	            $articolo = "";
	            $quant = 0;
	            $prezzo = 0;
	            
	        }
	        
	        $sh = new SpedHistory();
	        $sh->crea(
	            'pers',
	            array(
	                "messaggio"	=> 'GEN_ORD_RESO',
	                "k_ordine"	=> $m_params->k_ordine,
	                "use_session_history" =>  $use_session_history,
	                "vals" => array("RINRCA" => $v->nrec,
	                                "RIART" =>$articolo, 
	                                "RIQTA" => $quant,
	                                "RIIMPO" => $prezzo
	                )
	            )
	            );
	        
	    }
	    
	    
	    $sh = new SpedHistory();
	    $ret_RI = $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'GEN_ORD_RESO',
	            "k_ordine"	=> $m_params->k_ordine,
	            "end_session_history" =>  $use_session_history,
	            "vals" => array("RICITI" => $m_params->causale,
	                "RIVETT" => $m_params->prior)
	        )
	        );
	    
	}
	    
	    
	}else{
	  
	    foreach ($list_righe as $v){
	        
	        $sh = new SpedHistory();
	        $sh->crea(
	            'pers',
	            array(
	                "messaggio"	=> 'GEN_ORD_COMP',
	                "k_ordine"	=> $m_params->k_ordine,
	                "use_session_history" =>  $use_session_history,
	                "vals" => array("RINRCA" => $v->nrec)
	                
	                
	            )
	            );
	        
	    }
	    
	    
	    $sh = new SpedHistory();
	    $ret_RI = $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'GEN_ORD_COMP',
	            "k_ordine"	=> $m_params->k_ordine,
	            "end_session_history" =>  $use_session_history,
	            "vals" => array("RIDTVA" => $m_params->data_reg,
	                            "RIDTEP" => $m_params->cons_prog)
	        )
	        );
	    
	}

	$ret['success'] = true;
	$ret['ret_RI'] = $ret_RI;
	echo acs_je($ret);
	exit;
}

if ($_REQUEST['fn'] == 'open_form'){

	$m_params = acs_m_params_json_decode();
	
	$r = $s->get_ordine_by_k_docu($k_ordine);
	
	$row_at = $s->get_TA_std('USATT', $auth->get_user(), 'MODAF');
	
	if(count($row_at) > 0)
	    $mod_data = 'Y';
	else
	    $mod_data = 'N';
	
	

	?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            autoScroll: true,
            layout: 'fit',
			  items: [
			
			  {  xtype: 'form',
		            flex: 1,
					layout: { 	type: 'vbox',
								pack: 'start',
							    align: 'stretch'},		            
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		             <?php if($m_params->ord_ass != 'Y' && $m_params->ord_reso != 'Y' && $m_params->ord_comp != 'Y'){?>
					buttons: [
					{
			            text: 'Genera FATTURA',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var grid = this.up('form').down('grid');
			            	var righe = grid.getSelectionModel().getSelection();
			            	var button = this;
			            	var importo = 0;
			            	button.disable();
                         
                          list_righe_selected = [];
                           for (var i=0; i<righe.length; i++) {
                           	list_righe_selected.push({
				            	nrec: righe[i].get('RDNREC')				            	
				            });}
				           
				           
    				       if (form.getValues().f_parz == 'Y'){
    				        	//obbligatorio almeno una riga
    				        	if (Ext.isEmpty(list_righe_selected)){
    				        		acs_show_msg_error('Selezionare almeno una riga ordine');
    				        		button.enable();
    				        		return false;
    				        	}
    				        	
    				        	for (var i=0; i<righe.length; i++) {
    				        	  if(parseFloat(righe[i].get('RDQTE')) <  parseFloat(righe[i].get('RDQTA')))
                            			importo += parseFloat(righe[i].get('RTINFI'));
                           				            	
				                }
				       
    				        	
    				        }else{
    				         var all_righe = grid.getStore().getRange();
    				           for (var i=0; i<all_righe.length; i++) {
    				                  if(parseFloat(all_righe[i].get('RDQTE')) < parseFloat(all_righe[i].get('RDQTA')))
                            			importo += parseFloat(all_righe[i].get('RTINFI'));
                           				            	
				                }
				                
    				    
    				        }
    				        
    				       
    				       <?php if(!in_array(trim($r['TDOTPD']), array('MA', 'OT', 'MR'))){  ?>
    				        
    				        if(importo == 0){
				            	acs_show_msg_error('Importo merce a zero!');
				            	button.enable();
    				        	return false;
				            }
				            
    				        <?php }?>
    				        
    				      

			            	var loc_win = this.up('window');
			            	
			            	
			            		if (!form.isValid()){
			            			 button.enable();
			            			 return false;
			            		}
							
							    Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
		 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_fatt',
		 						        method     : 'POST',
		 			        			jsonData: {
		 			        			   k_ordine: '<?php echo $m_params->k_ordine?>',
		 			        			   righe: list_righe_selected,
		 			        			   form_values: form.getValues(),
		 			        			   ord_ass : '<?php echo $m_params->ord_ass ?>',
		 			        			   ord_reso : '<?php echo $m_params->ord_reso ?>'
		 								},							        
		 						        success : function(result, request){
		 						        
        	 						        Ext.getBody().unmask();
        	 						        
											 jsonData = Ext.decode(result.responseText);
											 if (jsonData.ret_RI.RIESIT == 'W'){
											 	acs_show_msg_error(jsonData.ret_RI.RINOTE);
											 	button.enable();
											 	return;
											 }        	 						        
        	 						         	
										    if (!Ext.isEmpty(loc_win.events.aftersave))
										         loc_win.fireEvent('afterSave', loc_win);
										    else
										        loc_win.close();
		 			            		},
		 						        failure    : function(result, request){
		 						             Ext.getBody().unmask();
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });	
							
		
			
			            }
			         }
				
						
						],
						
						<?php }?>
						
						 items: [
						 
						  <?php if($m_params->ord_ass == 'Y'){ ?>
        	        	  { 
        	        	  html: '<h1><center>Genera ASSISTENZA</center></h1>',
        	        	 
        	        	  },
        	        	  <?php }elseif($m_params->ord_reso == 'Y'){ ?> 
        	        	  
        	        	  { 
        	        	  html: '<h1><center>Genera RESO </center></h1>',
        	        	 
        	        	  },
        	        	  
        	        	  <?php }elseif($m_params->ord_comp == 'Y'){?>
        	        	  
        	        	    { 
        	        	  html: '<h1><center>Genera COMPLETAMENTO </center></h1>',
        	        	 
        	        	  },
        	        	  
        	        	  <?php }
			
						
        	        	  if($m_params->ord_ass != 'Y' && $m_params->ord_reso != 'Y' && $m_params->ord_comp != 'Y'){?>
							{ 
						xtype: 'container',
						margin: '0 0 0 0',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'
								 },						
						items: [
						
				
						
						     {
							 xtype: 'fieldset',
							 title: 'Info',
							 margin: '0 0 0 0',
							 flex: 1,
							 layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'
								   },		   					
						items: [
						
						<?php if($mod_data == 'Y'){?>
						    {
							     name: 'f_data'
							   //, margin: "10 10 10 10"   
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data fattura'
							   , labelWidth: 90
							   , value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   //, anchor: '-15'
							   , width: 220
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
								},
								
						<?php }else{?>
						
						  {xtype: 'displayfield',
						  fieldLabel : 'Data fattura',
						  labelWidth: 90,
						  name: 'f_data_video',
						  value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
						  },
						  {xtype: 'hiddenfield',
						  name: 'f_data',
						  value: '<?php echo oggi_AS_date() ?>'
						  },
						
						<?php }?>		
		               	 	{
							name: 'f_pagamento',
							xtype: 'combo',
							fieldLabel: 'Pagamento', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							width: '100%',
						    //margin: "10 10 10 10",
						 	labelWidth: 90,
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php //echo acs_ar_to_select_json($s->get_options_pagamento(), '') ?>
								     <?php echo acs_ar_to_select_json(find_TA_sys('CUCP'), '') ?> 	
								    ] 
							},
							value: <?php echo j(trim($r['TDCPAG'])) ?>
					},
					
						 {
						xtype: 'fieldcontainer',
						flex: 1, 
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'
								   },						
						items: [
						
							{
							margin: "0 130 0 0" ,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Parziale',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 90,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_parz' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'Y'
		                          , listeners: {
								    change: function(checkbox, newValue, oldValue, eOpts) {
								    	var m_form = this.up('form');
								    	var fs_vettore = m_form.down('#grid_righe');
								    	
			                    	    if (newValue == true)
			                    	    	fs_vettore.enable();
			                    	    else
			                    	    	fs_vettore.disable();	
			                    	  
								    }
								}
		                        }]														
						 },{
							//margin: "10 10 10 10" ,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Fattura corrispettivo',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 120,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_fatt_c' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'C'
		                       
		                        }]														
						 }
						    
						
						]}
					
					
				,{
							name: 'f_trasp',
							xtype: 'combo',
							fieldLabel: 'Trasportatore', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							width: '100%',
						    //margin: "10 10 10 10",
							labelWidth: 90,	
							forceSelection:true,
						   	allowBlank: true,	
						   	value: <?php echo j(trim($r['TDVET1'])) ?>,													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('AVET'), ''); ?>	
								    ] 
							}
						
						},{
							name: 'f_causale',
							xtype: 'combo',
							fieldLabel: 'Causale', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							width: '100%',
						    //margin: "10 10 10 10",
							labelWidth: 90,	
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('VUCT'), ''); ?>	
								    ] 
							}
						
						}
						
						]}, {
						//xtype: 'fieldcontainer',
			            xtype:'fieldset',
			            title: 'Note',						
						margin: '0 0 0 0',
						flex: 1,
						layout: { 	type: 'vbox',									
						            pack: 'start',
								    align: 'stretch'},						
						items: [
						      {
								name: 'f_note_1',
							    xtype: 'textfield',
								//fieldLabel: 'Note',
								//labelWidth: 40,
								maxLength: 50,
							    //margin: "0 0 0 10"
							 },{
								name: 'f_note_2',
								xtype: 'textfield',
								//margin: "10 0 0 50", 
							    maxLength: 50
							 },{
								name: 'f_note_3',
								xtype: 'textfield',	
							    maxLength: 50
							 },{
								name: 'f_note_4',
								xtype: 'textfield',		
							    maxLength: 50
							 },{
								name: 'f_note_5',
								xtype: 'textfield',	
							    maxLength: 50
							 }
						
						]}
							
						
						]},
						
						{
					xtype: 'fieldset',
	                title: 'Trasporto',
	                layout: 'anchor',
	                margin: '0 0 0 0',
	                style: 'padding:5px',
                    bodyStyle: 'padding:5px',
					items: [
						
						
						{
							 xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    {
								name: 'f_trasp_cli',
								xtype: 'numberfield',	
							    hideTrigger : true,
							    fieldLabel : 'Carico cliente',
							    flex: 1, labelWidth: 80,
							    margin : '0 10 0 5'
							 }, {
								name: 'f_trasp_ns',
								xtype: 'numberfield',	
								hideTrigger : true,
								fieldLabel : 'NS carico',
								flex: 1, labelWidth: 60, labelAlign: 'right',
								margin : '0 5 0 10'							   
							 }, {
        							name: 'f_a_mezzo',
        							xtype: 'combo',
        							fieldLabel: 'A mezzo', labelAlign: 'right',
        							displayField: 'text',
        							valueField: 'id',
        							anchor: '-15',
        							flex: 3,
        						 	labelWidth: 90,
        							forceSelection:true,
        						   	allowBlank: false,														
        							store: {
        								autoLoad: true,
        								editable: false,
        								autoDestroy: true,	 
        							    fields: [{name:'id'}, {name:'text'}],
        							    data: [								    
        								     <?php echo acs_ar_to_select_json(find_TA_sys('VUTR'), '') ?> 	
        								    ] 
        							},
        							value: <?php echo j($cfg_mod_DeskPVen['gen_fat_acc_vet_def']) ?>
        					}
						
						]}	   
 					
					 
	             ]}, 
	        	  <?php } ?>
	        	  
	        	            
	             {
		xtype: 'gridpanel',
		name : 'f_righe',
		itemId: 'grid_righe',
		<?php if($m_params->ord_ass != 'Y' && $m_params->ord_reso != 'Y' && $m_params->ord_comp != 'Y'){?>
		disabled: true,
		<?php }?>
		margin : '7 0 0 0',
		flex: 1,
		selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},
		stateful: true,
        stateId: 'seleziona-righe-ordini-222',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
		
  	    store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,
					loadMask: true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>',
							type: 'ajax',
							reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        },
					        actionMethods: {
						          read: 'POST'
						        },
   						     extraParams: {
   		    		    		ord_ass: '<?php echo $m_params->ord_ass ?>',
   		    		    		ord_reso: '<?php echo $m_params->ord_reso ?>',
   		    		    		ord_comp: '<?php echo $m_params->ord_comp ?>'
   		    				},               						        
	        			doRequest: personalizza_extraParams_to_jsonData
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART', 'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE', 'RTPRZ', 'active', 'RTINFI'
	        			]
	    			}),
	    			
		        columns: [
					        
		             {
		                header   : '<br/>&nbsp;Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },			        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		             
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    		}}, {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>Prezzo',
		                dataIndex: 'RTPRZ', 
		                width    : 50,
		                align: 'right',
						renderer: floatRenderer2	                		                
		             }, {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }	        
		      
		        

		            
		            
		         ], 
		         
		         <?php if($m_params->ord_ass == 'Y' || $m_params->ord_reso == 'Y'  || $m_params->ord_comp == 'Y'){?>
		         dockedItems: [{
        			xtype: 'toolbar',
        			dock: 'bottom',
        		    items: [
        		    
            		    <?php if($m_params->ord_comp == 'Y'){
            		    
            		        $row =  $s->get_ordine_by_k_ordine($m_params->k_ordine);
            		        
            		        ?>
            		    
            		    {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data reg.'
						   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
						   , name: 'f_data_reg'
						   , itemId : 'data_reg'
						   , labelWidth : 60
						   , width : 170
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						
			
						},{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Consegna prog.'
						   , value: '<?php echo  print_date($row['TDDTEP'], "%d/%m/%Y"); ?>'
						   , name: 'f_cons_prog'
						   , itemId : 'cons_prog'
						   , format: 'd/m/Y'
						   , width : 200
						   , labelWidth : 90
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
			
						},
            		    
            		    <?php }else{?>
        		    
        						{
							    itemId : 'causale',	  
								name: 'f_caus_ass',
								xtype: 'combo',
								fieldLabel: 'Causale assistenza',
								width: 200,
								displayField: 'text',
								margin : '0 10 0 0',
							    valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: false,
								value: <?php echo j(trim($r['TDISON'])) ?>, 
							   	allowBlank: true,														
					            	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [		
							        <?php echo acs_ar_to_select_json($main_module->find_TA_std('ISON'), '') ?>	
								    ] 
							   }	
							    						 
							  }  ,{
							    itemId : 'prior',	  
								name: 'f_prior',
								xtype: 'combo',
								fieldLabel: 'Priorit&agrave;',
								labelWidth : 40,
								width: 170,
								displayField: 'text',
							    valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: false,
								value: <?php echo j(trim($r['TDOPRI'])) ?>, 
							   	allowBlank: true,														
					            	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [
									     <?php echo acs_ar_to_select_json(find_TA_sys('BPRI', null, null, 'VO', null, null, 1), '') ?>
									    ]
							   }	
							    						 
							  }, 
							  
							  <?php if($m_params->ord_ass == 'Y'){?>
							  {
													 
							xtype: 'checkboxgroup',
							fieldLabel: 'Genera reso',
							labelAlign: 'right',
							itemId : 'g_reso',
		                    allowBlank: true,
						   	labelWidth: 65,
						   	margin : '0 10 0 0',
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_reso' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'Y'
		                       
		                        }]														
						 }, 
						 '->',{
                             xtype: 'button',
        	            	 scale: 'large',
        	            	 iconCls: 'icon-shopping_setup-32',
                             text: 'Aggiungi articoli',
        			            handler: function() {
        			            
        			             var grid = this.up('grid');
        			             var row_len = grid.getStore().getRange().length;
        			            
        			            				my_listeners = {
        				        					afterSelectArt: function(from_win, new_art){
        				        					
        				        					  for (var i=0; i<new_art.length; i++) {
                                                        var values = {RDART: new_art[i].codice, RDDART: new_art[i].descr, RDQTA : new_art[i].quant, RTPRZ: new_art[i].prezzo, RDNREC : 0};
    		        									grid.getStore().add(values);
    		        								  }
                                                        var new_len = grid.getSelectionModel().store.data.length;
                                                        
                                                        for (var i= row_len; i< new_len; i++) {
                                                         	grid.getSelectionModel().select(i, true); 
                                                        }
                                                        
                                                        from_win.close();
        	 											
        								        	}
        										}
        	        				acs_show_win_std('Visualizza articoli', '../base/acs_elenco_articoli_adv.php', {
        	        					k_ordine: <?php echo j($m_params->k_ordine); ?>, 
        	        					esclusi_sosp: 'Y', 
        	        					ord_ass : 'Y',
        	        					abilita_inserimento_in_ordine: true,
        	        					ricerca_solo_fornitore : 'Y'
        	        					}, 900, 550, my_listeners, 'icon-shopping_setup-16');
        			            }
        			     },
						 <?php }
						 
						 }?>
						 
						 '->',{
						 
						  <?php if($m_params->ord_ass == 'Y'){ ?>
						     text: 'Genera ASSISTENZA',
						  <?php }elseif($m_params->ord_reso == 'Y'){?>
			            	 text: 'Genera RESO',
			              <?php }else{ ?> 
			                 text: 'Genera COMPLETAMENTO',
			              <?php }?>
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	
			            	var grid = this.up('grid');
			            	var righe = grid.getSelectionModel().getSelection();
			            	var causale = '';
			            	var prior = '';
			            	var reso = '';
			            	var data_reg = '';
			            	var cons_prog = '';
			            	
			            	 <?php if($m_params->ord_comp == 'Y'){ ?>
			            	   data_reg = this.up('window').down('#data_reg').getSubmitValue();
			            	   cons_prog = this.up('window').down('#cons_prog').getSubmitValue();
			            	  
			            	 <?php }?>
			            	 
			            	 <?php if($m_params->ord_ass == 'Y'){ ?>
			            	   causale = this.up('window').down('#causale').getValue();
			            	   prior = this.up('window').down('#prior').getValue();
			            	   reso = this.up('window').down('#g_reso').getValue();
			            	 <?php }?>
			            	 
			            	  <?php if($m_params->ord_reso == 'Y'){ ?>
			            	   causale = this.up('window').down('#causale').getValue();
			            	   prior = this.up('window').down('#prior').getValue();
			            	 <?php }?>
			            	           	
			            	
			            	var button = this;
			            	button.disable();
			        
			            	
                          list_righe_selected = [];
                           for (var i=0; i<righe.length; i++) {
				            list_righe_selected.push({
				            	nrec: righe[i].get('RDNREC'),
				            	RDART: righe[i].get('RDART'),
				            	RDQTA: righe[i].get('RDQTA'),
				            	RTPRZ: righe[i].get('RTPRZ')
				            	});}
				            	
		            	  if (list_righe_selected.length == 0){
							  acs_show_msg_error('Selezionare almeno una riga');
							  button.enable();
							  return false;
						  }
			            	
			            	var loc_win = this.up('window');
							
							   Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
		 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_fatt',
		 						        method     : 'POST',
		 			        			jsonData: {
		 			        			   k_ordine: '<?php echo $m_params->k_ordine?>',
		 			        			   righe: list_righe_selected,
		 			        			   ord_ass : '<?php echo $m_params->ord_ass ?>',
		 			        			   ord_reso : '<?php echo $m_params->ord_reso ?>',
		 			        			   ord_comp : '<?php echo $m_params->ord_comp ?>',
		 			        			   causale : causale,
		 			        			   prior : prior,
		 			        			   reso : reso.f_reso,
		 			        			   data_reg  : data_reg,
		 			        			   cons_prog : cons_prog
		 								},							        
		 						        success : function(result, request){	
		 						        
		 						            Ext.getBody().unmask();
        	 						       					        	
										    if (!Ext.isEmpty(loc_win.events.aftersave))
										         loc_win.fireEvent('afterSave', loc_win);
										    else
										        loc_win.close();
		 			            		},
		 						        failure    : function(result, request){
		 						           Ext.getBody().unmask();
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });
							
	
			
			            }
			         }       
					 			
							 			       
			]
		}],	 
		
		<?php }?> 	    					
		
		listeners: {		
	 			afterrender: function (comp) {
	 				//comp.up('window').setWidth(950);
	 				//comp.up('window').setHeight(500);
					//window_to_center(comp.up('window'));
					<?php if($m_params->ord_ass != 'Y' && $m_params->ord_reso != 'Y' && $m_params->ord_comp != 'Y'){?>
					comp.up('window').setTitle('Genera fattura accompagniatoria: <?php echo " ordine {$r['TDOADO']}_{$r['TDONDO']} {$r['TDPROG']} {$r['TDOTPD']}"; ?>');	 				
					<?php }?>	 			
	 			},
	 		
 				beforeselect: function(grid, record, index, eOpts) {
   				   //controllo possibilita' di spuntare su ogni singola riga
 				
			       <?php if($m_params->ord_ass != 'Y' && $m_params->ord_reso != 'Y'){?>
					if(record.get('RDSTEV') == 'N') return false;
        			if(parseFloat(record.get('RDQTA')) <= parseFloat(record.get('RDQTE')) || record.get('RDSTEV') == 'S') return false;
				   <?php } ?>
				   	 
        		} //beforeselect
       		
	
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (rec.get('RDSTEV') == 'N') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';		        	
		        
		        
		         }   
		    }												    
			
		         
	}
									 

										 
		           ]}  //form
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit; 

	
	
}