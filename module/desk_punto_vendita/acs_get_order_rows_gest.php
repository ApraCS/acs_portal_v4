<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

	$raw_post_data = acs_raw_post_data();
	if (strlen($raw_post_data) > 0)
	{	
		$m_params = acs_m_params_json_decode();
		$k_ordine = $m_params->k_ordine;
	}

	if (isset($_REQUEST['k_ordine']))
		$k_ordine = $_REQUEST['k_ordine'];

	$oe = $s->k_ordine_td_decode_xx($k_ordine);
	
	if(isset($k_ordine) && $k_ordine != '')
	  $row  = $s->get_ordine_by_k_ordine($k_ordine);

	  //non trovo il documento sul TD0, ad esempio perche' parliamo di ordini di acquisto
	  // ho rddes2 = "2017_000984_FF_AD1_AD   �                       "
	  if ($row==false){
	  	$rddes2_ar = explode("_", $m_params->rddes2);
	  	$tipologia_doc = $rddes2_ar[2];
	  } else {
	  	$tipologia_doc = $row['TDOTPD'];
	  }
	
	// ******************************************************************************************
	// call avanza (per riga)
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_richiesta_avanza'){
		$m_params = acs_m_params_json_decode();		
		$ret = array();
		
		$ao = new SpedAssegnazioneOrdini();
		//($causale_rilascio, $ar_par, $nrec = 0){
		$ao->avanza($m_params->set_causale, array('prog' => $m_params->prog), $m_params->nrec);
		
		$ret['success'] = true;
		echo acs_je($ret);
		exit;
	}
	
	// ******************************************************************************************
	// modifica riga
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_modifica_articoli_MTO'){
	    
		$m_params = acs_m_params_json_decode();
	
		$use_session_history = microtime(true);
	
			$sh = new SpedHistory();
			
			if($m_params->magazzino == 'Y'){
				$msg = 'modifica_ord_mag_mto';
			}else{
			    $msg = 'modifica_ord_mto';
			}
			
			$sh->crea(
				$msg,
					array(
							"k_ordine"				=> $m_params->k_ordine,
							"nrec" 		    	    => $m_params->articoli->f_nrec,
					        "check" 	    	    => $m_params->articoli->f_ck_r,
							"prezzo"		    	=> sql_f($m_params->articoli->f_prezzo),
							"use_session_history" 	=> $use_session_history
								
					)
					);

		$sh = new SpedHistory();
		$sh->crea(
				$msg,
				array(
						"k_ordine"	  => $m_params->k_ordine,
						"end_session_history" => $use_session_history
				)
				);
	
		$ret = array();
		$ret['success'] = true;
		echo acs_je($ret);
		exit;
	}
	
	
// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
	$sql_select = "";
	$sql_join = "";
	
	if($m_params->open_request->righe_doc == 'Y' || $m_params->open_request->righe_ddt == 'Y'){
	    
	    $sql_select = ", L_ACQ.NR_LA AS NR_LA, TDCCON";
	    $sql_join = "
	                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD
                    ON TD.TDDT=RD.RDDT AND TD.TDTIDO=RD.RDTIDO AND TD.TDINUM=RD.RDINUM AND TD.TDAADO=RD.RDAADO AND TD.TDNRDO=RD.RDNRDO
	                LEFT OUTER JOIN (
                       SELECT COUNT(*) AS NR_LA, LIDT, LITPLI, LIART, LICCON, LILIST, LIVALU
                       FROM {$cfg_mod_DeskPVen['file_listini']}
                       GROUP BY LIART, LIDT, LITPLI, LICCON, LILIST, LIVALU) L_ACQ
                 	ON L_ACQ.LIDT = RD.RDDT AND L_ACQ.LITPLI = 'A' AND L_ACQ.LIART = RD.RDART AND L_ACQ.LICCON = TD.TDCCON AND L_ACQ.LILIST = RD.RDLIST AND L_ACQ.LIVALU = 'EUR'";
	}
	
	$sql = "SELECT RD.*, RTPRZ, RTIMPR {$sql_select}
	FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
	LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
	ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
	{$sql_join}
    WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
	AND RD.RDTISR = '' AND RD.RDSRIG = 0";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $oe);

	while ($row = db2_fetch_assoc($stmt)) {
		$row['data_selezionata'] = sprintf("%08s", $dtep);
		$row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
		if ($row['data_selezionata'] == $row['data_consegna'])
			$row['inDataSelezionata'] = 0;
			else $row['inDataSelezionata'] = 1;
			$row['RDART'] = acs_u8e(trim($row['RDART']));
			$row['RDDART'] = acs_u8e($row['RDDART']);
			$row['RDNREC'] = acs_u8e($row['RDNREC']);
			$row['RTPRZ'] = acs_u8e($row['RTPRZ']);
			$row['list_acq'] = $row['NR_LA'];
			$row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
			$row['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
			$data[] = $row;
	}
	

			//metto prima le righe della data selezionata
				////usort($data, "cmp_by_inDataSelezionata");
			
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $s->get_ordine_by_k_docu($n_ord);

			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		
exit();				
}

if ($_REQUEST['fn'] == 'open_grid'){
?>


{"success": true, "items":
	{
		xtype: 'gridpanel',
		
		stateful: true,
        stateId: 'seleziona-righe-ordini',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
	    store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,
					loadMask: true,	
					proxy: {
						url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>',
							type: 'ajax',
							 actionMethods: {
						          read: 'POST'
						        },
						        
						         extraParams: {
									 open_request: <?php echo acs_je($m_params); ?>
		        				},
		        				
		        				doRequest: personalizza_extraParams_to_jsonData, 
					
							   reader: {
					            type: 'json',
								method: 'POST',						            
					            root: 'root'						            
					        }
						},			        
  				/*	proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>',
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						},*/
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART', 'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO', 'RDSC1', 'RDSC2', 'RDSC3', 'RDSC4'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE', 'RTPRZ', 'RTIMPR', 'list_acq', 'TDCCON', 'RDLIST'
	        			]
	    			}),
	    			
	    			 <?php $la = "<img src=" . img_path("icone/48x48/currency_blue_dollar.png") . " height=24>"; ?>
	    			
		        columns: [
		
		        	{
		                header   : '<br/>&nbsp;Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
						  					return date_from_AS(value);			    
										}		                
		                 
		             },		        
		             {
		                header   : '<br/>&nbsp;Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },			        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		             
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
		    			    	}}			    			             
		             
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             },
		                <?php  if ($js_parameters->only_view  != 1){?>
		              {
		                header   : '<br/>Prezzo',
		                <?php if($m_params->magazzino == 'Y'){?>
		                dataIndex: 'RTIMPR',
		                <?php }else{?>
		                dataIndex: 'RTPRZ',
		                <?php }?> 
		                width    : 50,
		                align: 'right',
						renderer: floatRenderer2	                		                
		             },
		             <?php }?>
		              {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;S',
		                dataIndex: 'RDSTEV', 
		                width    : 20,
		                align: 'right'
		             }, {
		                header   : '<br/>&nbsp;T.R.',
		                dataIndex: 'RDT	PRI', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Pr.',
		                dataIndex: 'RDPRIO', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;St.',
		                dataIndex: 'RDSTAT', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Carico',
		                dataIndex: 'RDNRCA', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRCA') == 0) return ''; //non mostro																											
						  					return rec.get('RDTICA') + "_" + rec.get('RDNRCA');			    
										}		                
		             }
		             
		             
				<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             , {
		                header   : '<br/>&nbsp;Commessa',
		                dataIndex: 'COMMESSA', 
		                width    : 80
		             }				
		        <?php } else { ?>
		             , {
		                header   : '<br/>&nbsp;Lotto',
		                dataIndex: 'RDNRLO', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRLO') == 0) return ''; //non mostro																											
						  					return rec.get('RDTPLO') + "_" + rec.get('RDNRLO');			    
										}
		             }		        
		        <?php } ?> 
		        
		        
<?php if (strlen($m_params->prog) > 0) { ?>
		             , {
		                header   : '<br/>&nbsp;Causale',
		                dataIndex: 'CAUSALE', 
		                width    : 80
		             }
<?php }         
if($m_params->righe_doc == 'Y' || $m_params->righe_ddt == 'Y'){?>
        
        , {text: '<?php echo $la; ?>', 
		  width: 30, 
		  dataIndex: 'list_acq',
		  tooltip: 'Listini acquisto',
		  renderer: function(value, p, record){
			  if(record.get('list_acq') > 0) return '<img src=<?php echo img_path("icone/48x48/currency_blue_dollar.png") ?> width=15>';
		  }},
        
        <?php }?>
            
		            
		         ]	    					
		
		, listeners: {		
	 			afterrender: function (comp) {
	 				//comp.up('window').setWidth(950);
	 				//comp.up('window').setHeight(500);
					//window_to_center(comp.up('window'));
					<?php if($m_params->righe_doc == 'Y'){?>
						comp.up('window').setTitle('<?php echo "Righe documento di acquisto {$oe['TDOADO']}_{$oe['TDONDO']}_{$tipologia_doc}"; ?>');	 				
					<?php }else if($m_params->righe_ddt == 'Y'){ ?>
					    comp.up('window').setTitle('<?php echo "Righe DDT fornitore {$oe['TDOADO']}_{$oe['TDONDO']}_{$tipologia_doc}"; ?>');	 				
					<?php }else{?>
						comp.up('window').setTitle('<?php echo "Righe ordine {$oe['TDOADO']}_{$oe['TDONDO']}_{$tipologia_doc} "; ?>');	 				
	 				<?php }?>
	 			},
	 			
				  celldblclick2222: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	rec = iView.getRecord(iRowEl);
					  	acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('RDDT'), rdart: rec.get('RDART')}, 1100, 600, null, 'icon-shopping_cart_gray-16');
					  }
				  }
				
				 , itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      row = rec.data;			      		 
				      var voci_menu = [];
				      
				      
   					 voci_menu.push({
				         		text: 'Modifica riga documento',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		     
				        		    my_listeners = {
				        			    afterModArt: function(from_win){
				        				console.log('mylist')
		        						grid.getStore().load();
	 									from_win.close();
	 											
								        	}
										}
				        	
				         <? if($m_params->f_reminder == 'Y'){?>
				        	 acs_show_win_std('Modifica riga ordine fornitore', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_mod_art', {row: row, k_ordine: '<?php echo $k_ordine; ?>', magazzino : '<?php echo $m_params->magazzino; ?>', f_reminder : '<?php echo $m_params->f_reminder ?>'}, 500, 250, my_listeners, 'icon-pencil-16');          		
				         <?php }else{?>
				         	acs_show_win_std('Modifica articolo', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_mod_art', {row: row, k_ordine: '<?php echo $k_ordine; ?>', magazzino : '<?php echo $m_params->magazzino; ?>' }, 500, 200, my_listeners, 'icon-pencil-16');          		
				         <?php }?>   
				            
				            
			           
			       
			       
			            }
				    		});
				    	
				    	//if(rec.get('list_acq') > 0)	
				    	if (rec.get('RDART').substr(0,1) != '*'){
				    	voci_menu.push({
				         		text: 'Aggiorna/genera listino',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		     
				        		    my_listeners = {
				        			    afterList: function(from_win){
				        				grid.getStore().load();
	 									from_win.close();
	 											
								        	}
										}
				        	
			            acs_show_win_std('Aggiorna/genera listino', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_list', {row: row, k_ordine: '<?php echo $k_ordine; ?>'}, 500, 200, my_listeners, 'icon-pencil-16');          		
			       
			       
			            }
				    		});
					}
				
			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(event.xy);
				      
				      
				 } //itemcontextmenu
	
	 			 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';		        	
		         }   
		    }												    
			
		         
	}
}


<?php 
exit;

}
if ($_REQUEST['fn'] == 'form_mod_art'){

	$m_params = acs_m_params_json_decode();
	
	if($m_params->art_mto == 'Y' && $m_params->from_reminder != 'Y'){
	
	$sql = "SELECT MT.*, RTIMPR FROM 
	       {$cfg_mod_DeskAcq['file_proposte_MTO']} MT
	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
	           ON RT.RTDT= MT.MTDT AND MT.MTTIDO = RT.RTTIDO AND MT.MTINUM = RT.RTINUM AND MT.MTAADO = RT.RTAADO AND MT.MTNRDO = RT.RTNRDO AND MT.MTNWRE=RT.RTNREC AND RTVALU='EUR'
            WHERE MTDT = '{$m_params->row->RDDT}' AND MTTIDO = '{$m_params->row->RDOTID}' AND MTINUM = '{$m_params->row->RDOINU}'
            AND MTAADO = '{$m_params->row->RDOADO}' AND MTNRDO = '{$m_params->row->RDONDO}'
            AND SUBSTRING(MTFILL, 11, 8) = '{$m_params->row->RDPMTO}'";
	 
	       
   $stmt = db2_prepare($conn, $sql);
   echo db2_stmt_errormsg();
   $result = db2_execute($stmt);
   $row = db2_fetch_assoc($stmt);
   
   $nrec = $row['MTNWRE'];
   $riga = $row['MTRIGA'];
   $c_art = $row['MTART'];
   $d_art = $row['MTDART'];
   $prezzo = $row['RTIMPR'];
	}elseif($m_params->art_mto == 'Y' && $m_params->from_reminder == 'Y'){
	    $nrec = $m_params->row->nrec;
	    $riga = $m_params->row->riga;
	    $c_art = $m_params->row->c_art;
	    $d_art = $m_params->row->d_art;
	    $prezzo = $m_params->row->prezzo;

    }else{
       
   $nrec = $m_params->row->RDNREC;
   $riga =  $m_params->row->RDRIGA;
   $c_art = $m_params->row->RDART;
   $d_art = $m_params->row->RDDART;
   $sc1  = $m_params->row->RDSC1;
   $sc2  = $m_params->row->RDSC2;
   $sc3  = $m_params->row->RDSC3;
   $sc4  = $m_params->row->RDSC4;
   $qta_or  = $m_params->row->RDQTA;
   $qta_ev = $m_params->row->RDQTE;
   $rdstev = $m_params->row->RDSTEV;
   if($m_params->row->RDSTEV == 'S')
       $qta_re = 0;
   else 
       $qta_re  = $m_params->row->residuo;

   if($m_params->magazzino == 'Y')
      $prezzo = $m_params->row->RTIMPR;
   else    
      $prezzo = $m_params->row->RTPRZ;
 }
	
	

	?>
 	
 	 
  {"success":true, "items": [
         {
 		            xtype: 'form',
 		            //id: 'm_form',
 		            bodyStyle: 'padding: 10px',
 		            bodyPadding: '5 5 0',
 		            frame: false,
 		            url: 'acs_op_exe.php',
 		            
 					buttons: [{
 			            text: 'Salva',
 			            iconCls: 'icon-button_blue_play-32',
 			            scale: 'large',
 			            handler: function() {
 			            	var form = this.up('form').getForm();
 			            	var form_values = form.getValues();
 							 var loc_win = this.up('window');
 							 
 						
 							  Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_articoli_MTO',
									        method     : 'POST',
						        			jsonData: {
						        				articoli: form.getValues(),
						        				k_ordine: '<?php echo $m_params->k_ordine; ?>',
						        				magazzino: '<?php echo $m_params->magazzino; ?>'
											},							        
									        success : function(result, request){
									           loc_win.fireEvent('afterModArt', loc_win);		
						            	    								        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
 						
 			
 			            }
 			         }],   		            
 		            
 		            items: [  { 
 						xtype: 'fieldcontainer',
 						layout: { 	type: 'hbox',
 								    pack: 'start',
 								    align: 'stretch'},						
 						items: [
 								
								 
								 { xtype: 'displayfield', value : 'Riga:', margin : '0 50 0 0' },
								 { xtype: 'displayfield', value : 'Articolo:', margin : '0 75 0 0' },
								 { xtype: 'displayfield', value : 'Descrizione:', margin : '0 0 0 0' },
						    
 						
 						    ]},  { 
         						xtype: 'fieldcontainer',
         						layout: { 	type: 'hbox',
         								    pack: 'start',
         								    align: 'stretch'},						
         						items: [
 								{						
									name: 'f_nrec',
									value: '<?php echo $nrec; ?>',
									hidden: true,
									xtype: 'textfield',
									fieldLabel: 'num',
									readOnly: true
								 },
							
						        {						
									name: 'f_rig',
									xtype: 'displayfield',
									fieldLabel: '',
									value: '<?php echo $riga; ?>',
									labelWidth: 30,
									margin : '0 60 0 0' 
								 },
 								 {						
 									name: 'f_art',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: '<?php echo $c_art; ?>',
 									labelWidth: 60,
 									margin: '0 55 10 0'				
 								 },{						
 									name: 'f_art',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: <?php echo j($d_art); ?>,
 												
 								 }
 						
 						    ]},
 						    
 						     { 
						xtype: 'fieldcontainer',
						
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
								{						
 									name: 'f_prezzo',
 									xtype: 'numberfield',
 									<?php if($m_params->art_mto == 'Y'){ ?>
 										fieldLabel: 'Costo',
 									<?php }else{ ?>
 										fieldLabel: 'Prezzo',
 									<?php }?>
 									value: '<?php echo $prezzo; ?>',
 									//anchor: "-180", 
 									//flex: 1,
 									width : 180,
 									labelWidth: 70,
 									hideTrigger:true				
 								 }
 								 
 							
 								 
 			<?php if($m_params->art_mto != 'Y' && $m_params->from_reminder != 'Y' && $m_params->magazzino != 'Y'){?>
 								, { 
         						xtype: 'fieldcontainer',
         						labelWidth : 70,
         						margin : '0 0 0 10',
         						fieldLabel : 'Sconti',
         						layout: { 	type: 'hbox',
         								    pack: 'start',
         								    align: 'stretch'},						
         						items: [
 								
						        {						
									name: 'f_sc1',
									xtype: 'displayfield',
									fieldLabel: '',
									value: '<?php echo n_auto($sc1); ?> %',
									labelWidth: 30,
									margin : '0 30 0 0' 
								 },
 								 {						
 									name: 'f_sc2',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: '<?php echo n_auto($sc2); ?> %',
 									labelWidth: 30,
 									margin: '0 30 0 0'				
 								 },
 								 {						
 									name: 'f_sc3',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: '<?php echo n_auto($sc3); ?> %',
 									labelWidth: 30,
 									margin: '0 30 0 0'				
 								 },
 								 {						
 									name: 'f_sc4',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: '<?php echo n_auto($sc4); ?> %',
 									labelWidth: 30,
 									margin: '0 30 0 0'				
 								 },
 						
 						    ]}	
 						    

 						    	
 					      <?php 
 					      if($m_params->f_reminder == 'Y'){?>
 					      
 					      
 					       		, {   name: 'f_q_or',
									xtype: 'displayfield',
									fieldLabel: 'Q.t&agrave; ordinata',
									value: '<?php echo n_auto($qta_or); ?>',
									width: 150,
									//margin : '0 0 0 0' 
								 },
								 
 					       		{   name: 'f_q_ev',
									xtype: 'displayfield',
									fieldLabel: 'Q.t&agrave; evasa',
									value: '<?php echo n_auto($qta_ev); ?>',
									width: 150,
									//margin : '0 0 0 0' 
								 },
								 
								  { 
            						xtype: 'fieldcontainer',
            						layout: { 	type: 'hbox',
            								    pack: 'start',
            								    align: 'stretch'},						
            						items: [
            						
            							{   name: 'f_q_re',
    									xtype: 'displayfield',
    									width: 150,
    									fieldLabel: 'Q.t&agrave; residua',
										value: '<?php echo n_auto($qta_re); ?>',
										
										 },
										{
            											 
            							xtype: 'checkboxgroup',
            							fieldLabel: 'Chiudi q.t&agrave; residua',
            							allowBlank: true,
            						   	labelWidth: 130, 
            						   	items: [{
            		                            xtype: 'checkbox'
            		                          , name: 'f_ck_r' 
            		                          , boxLabel: ''
            		                          , checked: false
            		                          , inputValue: 'Y'
            		                       
            		                        }]														
            						 }
            								 
            						
            						
            						   
            						]}
								 
								 
 					       	
								 
						 	<?php }}?>
						
						
						  ]} 
						
 						    
 						    
 						    
 						    
 						    
 						    
 						    
 						    
 						    
 						    
 										 
 		           ]
 		              }
 		
 			]
 }	
 	
 	
 	
 <?php 
 exit;
 }
 
 if ($_REQUEST['fn'] == 'exe_list_acq'){
     
     $m_params = acs_m_params_json_decode();
     
     $data['LIUSUM'] = $auth->get_user();
     $data['LIDTUM'] = oggi_AS_date();
     $data['LIPRZ'] = sql_f($m_params->form_values->f_prezzo);
     $data['LISC1'] = sql_f($m_params->form_values->f_sc1);
     $data['LISC2'] = sql_f($m_params->form_values->f_sc2);
     $data['LISC3'] = sql_f($m_params->form_values->f_sc3);
     $data['LISC4'] = sql_f($m_params->form_values->f_sc4);

     if($m_params->listino > 0){
         //update
     
         $sql = "UPDATE {$cfg_mod_DeskUtility['file_listini']} LI
         SET " . create_name_field_by_ar_UPDATE($data) . "
         WHERE LIDT = '{$id_ditta_default}' AND LIART = '{$m_params->c_art}'
         AND LICCON = '{$m_params->forn}' AND LILIST = '{$m_params->list}'
         AND LITPLI = 'A' AND LIVALU = 'EUR'";
    
         $stmt = db2_prepare($conn, $sql);
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt, $data);
         echo db2_stmt_errormsg($stmt);
         
         
     }else{
         //insert
         
         $data['LIUSGE'] = $auth->get_user();
         $data['LIDTGE'] = oggi_AS_date();
         $data['LIDT'] = $id_ditta_default;
         $data['LIART'] = $m_params->c_art;
         $data['LICCON'] = $m_params->forn;
         $data['LILIST'] = $m_params->list;
         $data['LITPLI'] = 'A';
         $data['LIVALU'] = 'EUR';
         

         $sql = "INSERT INTO {$cfg_mod_DeskUtility['file_listini']}(" . create_name_field_by_ar($data) . ") VALUES (" . create_parameters_point_by_ar($data) . ")";
         $stmt = db2_prepare($conn, $sql);
         
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt, $data);
         echo db2_stmt_errormsg($stmt);
         
     }
     
     $ret = array();
     $ret['success'] = true;
     echo acs_je($ret);
     exit;
 }
 
 if ($_REQUEST['fn'] == 'open_list'){
     
     $m_params = acs_m_params_json_decode();
     $c_art = $m_params->row->RDART;
     $forn = $m_params->row->TDCCON;
     $list = $m_params->row->RDLIST;
     
     
     $sql = "SELECT LIART, LILIST, LIPRZ, LIUM, LICCON, CF_FORNITORE.CFRGS1 AS D_FORNITORE, LISC1, LISC2, LISC3, LISC4
             FROM {$cfg_mod_DeskUtility['file_listini']} LI
             LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR
             ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART
             LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
             ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
             WHERE LIDT = '{$id_ditta_default}' AND LIART = '{$c_art}' AND LICCON = '{$forn}'
             AND LITPLI = 'A' AND LILIST = '{$list}' AND LIVALU = 'EUR'";
    
     
     
     $stmt = db2_prepare($conn, $sql);
     echo db2_stmt_errormsg();
     $result = db2_execute($stmt);
     $row = db2_fetch_assoc($stmt);
     
 
     ?>
 	
 	 
  {"success":true, "items": [
         {
 		            xtype: 'form',
 		            //id: 'm_form',
 		            bodyStyle: 'padding: 10px',
 		            bodyPadding: '5 5 0',
 		            frame: false,
 		            //title: 'Modifica articolo',
 		            url: 'acs_op_exe.php',
 		            
 					buttons: [{
 			            text: 'Salva',
 			            iconCls: 'icon-button_blue_play-32',
 			            scale: 'large',
 			            handler: function() {
 			            	var form = this.up('form').getForm();
 			            	var form_values = form.getValues();
 							 var loc_win = this.up('window');
 							 
 						
 							  Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_list_acq',
									        method     : 'POST',
						        			jsonData: {
						        				form_values: form.getValues(),
						        				k_ordine: '<?php echo $m_params->k_ordine; ?>',
						        				listino : '<?php echo $m_params->row->list_acq; ?>',
						        				c_art : '<?php echo $m_params->row->RDART; ?>',
                                                forn : '<?php echo $m_params->row->TDCCON; ?>',
                                                list : '<?php echo $m_params->row->RDLIST; ?>'
											},							        
									        success : function(result, request){
									           loc_win.fireEvent('afterList', loc_win);		
						            	    								        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
 						
 			
 			            }
 			         }],   		            
 		            
 		            items: [  { 
 						xtype: 'fieldcontainer',
 						layout: { 	type: 'hbox',
 								    pack: 'start',
 								    align: 'stretch'},						
 						items: [
 								
								 
								 { xtype: 'displayfield', value : 'Fornitore:', margin :'0 80 0 0'},
								 { xtype: 'displayfield', value : 'Listino:', margin : '0 70 0 0'},
								 { xtype: 'displayfield', value : 'Articolo:'},
						    
 						
 						    ]},  { 
         						xtype: 'fieldcontainer',
         						layout: { 	type: 'hbox',
         								    pack: 'start',
         								    align: 'stretch'},						
         						items: [
 														
						        {						
									name: 'f_fornitore',
									xtype: 'displayfield',
									fieldLabel: '',
									value: '<?php echo $forn; ?>',
									margin : '0 70 0 0' 
								 },{						
 									name: 'f_list',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: '<?php echo $list; ?>',
 									//labelWidth: 60,
 									margin: '0 70 0 0'				
 								 },
 								 {						
 									name: 'f_art',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: '<?php echo $c_art; ?>',
 									labelWidth: 60,
 									//margin: '0 55 10 0'				
 								 }/*,{						
 									name: 'f_art',
 									xtype: 'displayfield',
 									fieldLabel: '',
 									value: <?php echo j($d_art); ?>,
 												
 								 }*/
 						
 						    ]},{						
 									name: 'f_prezzo',
 									xtype: 'numberfield',
 									fieldLabel: 'Prezzo',
 									value: '<?php echo n($row['LIPRZ'], 2); ?>',
 									width : 150,
 									labelWidth: 50,
 									hideTrigger:true				
 								 }
 								 
 								, { 
         						xtype: 'fieldcontainer',
         						layout: { 	type: 'hbox',
         								    pack: 'start',
         								    align: 'stretch'},						
         						items: [
 								
						        {						
									name: 'f_sc1',
									xtype: 'textfield',
									fieldLabel: 'Sc. 1',
									value: '<?php echo n_auto($row['LISC1']); ?>',
									labelWidth: 50,
									width : 110,
								 },
 								 {						
 									name: 'f_sc2',
 									xtype: 'textfield',
 									fieldLabel: 'Sc. 2',
 									labelAlign : 'right',
 									value: '<?php echo n_auto($row['LISC2']); ?>',
 									labelWidth: 40,
 									width : 110,				
 								 },
 								 {						
 									name: 'f_sc3',
 									xtype: 'textfield',
 									fieldLabel: 'Sc. 3',
 									labelAlign : 'right',
 									value: '<?php echo n_auto($row['LISC3']); ?>',
 									labelWidth: 40,
 									width : 110,				
 								 },
 								 {						
 									name: 'f_sc4',
 									xtype: 'textfield',
 									fieldLabel: 'Sc. 4',
 									labelAlign : 'right',
 									value: '<?php echo n_auto($row['LISC4']); ?>',
 									labelWidth: 40,
 									width : 110,				
 								 },
 						
 						    ]}		
 					     
 										 
 		           ]
 		              }
 		
 			]
 }	
 	
 	
 	
 <?php 
 exit;
 }