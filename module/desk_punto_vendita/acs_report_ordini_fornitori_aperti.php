<?php

require_once "../../config.inc.php";

//$main_module = new DeskUtility();
$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$da_form = acs_m_params_json_decode();


function sum_columns_value(&$ar_r, $r){    
    $ar_r['RTINFI'] += $r['RTINFI'];
}


if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode(); 
  
    ?>
	
	{"success":true, "items": [
		{ xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
			items: [
				 {  name: 'f_datacons_da'
    			     , flex: 1                		
    				 , xtype: 'datefield'
    				 , startDay: 1 //lun.
    				 , fieldLabel: 'Data consegna iniziale'
    				 , labelAlign: 'left'
    				 , format: 'd/m/Y'
    				 , submitFormat: 'Ymd'
    				 , allowBlank: false
    				 , anchor: '-15'
    				 , labelWidth: 130
    				 , value: '<?php echo  print_date($m_params->filter->f_cons_for_da, "%d/%m/%Y"); ?>'
    				 , listeners: {
    				 	invalid: function (field, msg) {
    				 	  Ext.Msg.alert('', msg);}
    			       }
    				}
			 , {
				     name: 'f_datacons_a'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data consegna finale'
				   , labelAlign: 'left'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: false
				   , anchor: '-15'
				   , labelWidth: 130
				    , value: '<?php echo  print_date($m_params->filter->f_cons_for_a, "%d/%m/%Y"); ?>'
				   , listeners: {
				       invalid: function (field, msg) {
				         Ext.Msg.alert('', msg);}
			 }
			}
	            ],
	            
				buttons: [	
					
					{ text: 'Report',
				      iconCls: 'icon-print-32',		            
				      scale: 'large',		            
			          handler: function() {
			          	form = this.up('form').getForm();
			          	if (form.isValid()){
    			          	this.up('form').submit({
                            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
                            target: '_blank', 
                            standardSubmit: true,
                            method: 'POST',
                            params: {
     	                        	  form_values: Ext.encode(this.up('form').getValues()),
    		                          filter : '<?php echo json_encode($m_params->filter); ?>'
    		                        }
    		                  });
    		                this.up('window').close();
		                } <!-- end if -->
		              } <!-- end handler -->
			        } <!-- end button report --> 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}

// ******************************************************************************************
// DATI PER FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_fornitore'){
    
    //costruzione sql
    global $id_ditta_default;
    global $cfg_mod_DeskPVen;
    global $cfg_mod_Spedizioni;
    
    
    $sql_where = ""; 
    $sql_select = " CLF.CFRGS1, TD0.TDCCON";
    $sql_where = " TD0.TDDT='{$id_ditta_default}' and TD0.TDTIDO='AO' and TD0.TDSTAT not in('CC','AN')";
    $sql_join2  = " left outer join {$cfg_mod_DeskPVen['file_anag_cli']} CLF
              on(CLF.CFDT=TD0.TDDT and trim(CLF.CFCD)=trim(TD0.TDCCON) and CLF.CFTICF='F')";
    $sql_orderby = "order by CLF.CFRGS1, TD0.TDCCON";
    
    $sql = "select distinct {$sql_select} from {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD0
        {$sql_join2} where {$sql_where} {$sql_orderby}
            FETCH FIRST 2000 ROWS ONLY";
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ar = array();
    
    while ($r = db2_fetch_assoc($stmt)) {
        /*$ar['ID'] = $r['TDCCON'];
        $ar['TEXT'] =  "[".trim($r['TDCCON'])."] ".trim($r['CFRGS1']);
        $ar[] = $r;*/
        $ret[] = array("ID"=>trim($r['TDCCON']), "TEXT" =>"[".trim($r['TDCCON'])."] ".trim($r['CFRGS1']));
        }
    //$ret = array("options" => $ar);
    echo acs_je($ret);
    exit();
}

// ******************************************************************************************
// DATI PER STATO ORDINE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_statoord'){
    
    //costruzione sql
    global $id_ditta_default;
    global $cfg_mod_DeskPVen;
    global $cfg_mod_Spedizioni;
    
    $sql_where = "";
    
    $sql_select = " BSTA.TADESC as BSTADESC, TD0.TDSTAT as TDSTAT";
    $sql_where = " TD0.TDDT='{$id_ditta_default}' and TD0.TDTIDO='AO' and TD0.TDSTAT not in('CC','AN')";
    $sql_join4 = " left outer join {$cfg_mod_DeskPVen['file_tab_sys']} BSTA
              on(BSTA.TADT=TD0.TDDT and BSTA.TAID='BSTA' and trim(BSTA.TANR)=trim(TD0.TDSTAT) and trim(BSTA.TACOR2)=trim(TD0.TDTIDO))";
    $sql_orderby = "order by BSTA.TADESC, TD0.TDSTAT";
    
    $sql = "select distinct {$sql_select} from {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD0
        {$sql_join4} where {$sql_where} {$sql_orderby}
            FETCH FIRST 2000 ROWS ONLY";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        $ar = array();
        
        while ($r = db2_fetch_assoc($stmt)) {
            if (trim($r['BSTADESC']) =='')
                $r['BSTADESC']='-';
            
            /*$ar['ID'] = $r['TDSTAT'];
            //print_r($r['TDSTAT']);
            $ar['TEXT'] =  "[".trim($r['TDSTAT'])."] ".trim($r['BSTADESC']);
            $ar[] = $r;*/
            
            $ret[] = array("ID"=>trim($r['TDSTAT']), "TEXT" =>"[".trim($r['TDSTAT'])."] ".trim($r['BSTADESC']));
        }
        //$ret = array("options" => $ar);
        
        echo acs_je($ret);
        exit();
}

// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){ ?>
<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   .azzurro {background-color: #86CFF4;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values_filter = strtr($_REQUEST['filter'], array('\"' => '"'));
$form_values_filter = json_decode($form_values_filter);
$form_values        = strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values        = json_decode($form_values);
$sql_where = "";

$sql_select = " TD0.TDDT, TD0.TDTIDO, TD0.TDINUM, TD0.TDAADO, TD0.TDNRDO, TD0.TDTPDO";
$sql_select.= ",TD0.TDAG1, TD0.TDDTEP, TD0.TDCCON, TD0.TDSTAT, TD0.TDSTAT, TD0.TDDTRG, TD0.TDVSRF";
$sql_select.= ", CLF.CFRGS1, CUAG.TADESC as CUAGDESC, BSTA.TADESC as BSTADESC";
$sql_select.= ", RD0.RDART, trim(RD0.RDUM), RD0.RDUM, RD0.RDQTA, AR0.ARDART, RT0.RTPRZ, RT0.RTINFI "; 

$sql_where .= " TD0.TDDT='{$id_ditta_default}' and TD0.TDTIDO='AO' and TD0.TDSTAT not in('CC','AN')"; 
$sql_where .= sql_where_by_combo_value('MT.MTFORN', $form_values_filter->f_fornitore);
$sql_where .= sql_where_by_combo_value('MT.MTAVAN', $form_values_filter->f_stato_fornitore);
$sql_where .= sql_where_by_combo_value('TD0.TDAG1', $form_values_filter->f_sede);

$data_cons_da = $form_values->f_datacons_da;
$data_cons_a = $form_values->f_datacons_a;
if($data_cons_da != 0)
    $sql_where .= " and TD0.TDDTEP >= $data_cons_da ";
if($data_cons_a != 0)
    $sql_where .= " and TD0.TDDTEP <= $data_cons_a ";
//print_r($sql_where); exit;
$sql_join1 = " left outer join {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD0 
              on(RD0.RDDT=TD0.TDDT and RD0.RDTIDO=TD0.TDTIDO and RD0.RDINUM=TD0.TDINUM and RD0.RDAADO=TD0.TDAADO and RD0.RDNRDO=TD0.TDNRDO
and RD0.RDART not like '*%')";
$sql_join2 = " left outer join {$cfg_mod_DeskPVen['file_anag_cli']} CLF 
              on(CLF.CFDT=TD0.TDDT and trim(CLF.CFCD)=trim(TD0.TDCCON))";
$sql_join3 = " left outer join {$cfg_mod_DeskPVen['file_tab_sys']} CUAG 
              on(CUAG.TADT=TD0.TDDT and CUAG.TAID='CUAG' and trim(CUAG.TANR)=trim(TD0.TDAG1))";
$sql_join4 = " left outer join {$cfg_mod_DeskPVen['file_tab_sys']} BSTA 
              on(BSTA.TADT=TD0.TDDT and BSTA.TAID='BSTA' and trim(BSTA.TANR)=trim(TD0.TDSTAT) and trim(BSTA.TACOR2)=trim(TD0.TDTIDO))";
$sql_join5 = " left outer join {$cfg_mod_DeskPVen['file_anag_art']} AR0 
              on(AR0.ARDT=TD0.TDDT and AR0.ARART=RD0.RDART)";
$sql_join6 = " left outer join {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT0 
              on(RT0.RTDT=TD0.TDDT and RT0.RTTIDO=TD0.TDTIDO and RT0.RTINUM=TD0.TDINUM and RT0.RTAADO=TD0.TDAADO and RT0.RTNRDO=TD0.TDNRDO and RT0.RTNREC=RD0.RDNREC AND RT0.RTVALU='EUR')";

$sql_orderby = "order by TD0.TDDTEP, CLF.CFRGS1, TD0.TDAADO, TD0.TDAADO";

$sql = "SELECT {$sql_select} 
        FROM {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD0
        {$sql_join1} {$sql_join2} {$sql_join3} {$sql_join4} {$sql_join5} {$sql_join6}
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_proposte_MTO']} MT
        ON MT.MTDT=TD0.TDDT AND MT.MTTIDQ=TD0.TDTIDO AND MT.MTINUQ=TD0.TDINUM AND MT.MTAADQ=TD0.TDAADO AND MT.MTNRDQ=TD0.TDNRDO
        WHERE {$sql_where} {$sql_orderby}
        FETCH FIRST 2000 ROWS ONLY";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$ar = array();

while($row = db2_fetch_assoc($stmt)){
        
    $tmp_ar_id = array();
    $ar_r= &$ar;
    
    $cod_livT = 'TOTALE';           //data ev richiesta
    $cod_liv0 = $row['TDDTEP'];     //data ev richiesta
    $cod_liv1 = $row['TDCCON'];     //cod fornitore
    $cod_liv2 = implode("_", array($row['TDAADO'], $row['TDNRDO'], $row['TDTPDO']));     //cod fornitore
    
    
    //livello totale
    $liv =$cod_livT;
    $tmp_ar_id[] = $liv; //TOTALE
    if (!isset($ar_r["{$liv}"])){
        $ar_new = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['task'] = $liv;
        $ar_new['liv_cod'] = $liv;
        $ar_new['liv'] = 'liv_totale';
        $ar_new['children'] = array();
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    sum_columns_value($ar_r, $row);
    $ar_r = &$ar_r['children'];
    
    
    //stacco per data
    $liv =$cod_liv0;
    $tmp_ar_id[] = $liv; //data
    if (!isset($ar_r["{$liv}"])){
        $ar_new = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);        
        $ar_new['task'] = $liv;        
        $ar_new['liv_cod'] = $liv;
        $ar_new['liv'] = 'liv_1';
        $ar_new['children'] = array();
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    sum_columns_value($ar_r, $row);
    $ar_r = &$ar_r['children'];
    
    //stacco per fornitore
    $liv =$cod_liv1;
    $tmp_ar_id[] = $liv; //fornitore
    if (!isset($ar_r["{$liv}"])){
        $ar_new = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['liv_cod'] = $liv;
        $ar_new['task'] = $row['CFRGS1'];
        $ar_new['liv'] = 'liv_2';
        $ar_new['children'] = array();
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    sum_columns_value($ar_r, $row);
    $ar_r = &$ar_r['children'];
    
    
    //stacco per ordine
    $liv =$cod_liv2;
    $tmp_ar_id[] = $liv; //ordine
    if (!isset($ar_r["{$liv}"])){
        $ar_new = array();
        $ar_new['id'] = implode("|", $tmp_ar_id);
        $ar_new['liv_cod'] = $liv;
        $ar_new['task'] = $liv;
        $ar_new['liv'] = 'liv_3';
        $ar_new['children'] = array();
        $ar_new['TDDTRG'] = $row['TDDTRG'];
        $ar_new['TDAG1'] = $row['TDAG1'];
        $ar_new['CUAGDESC'] = $row['CUAGDESC'];
        $ar_new['TDSTAT'] = $row['TDSTAT'];
        $ar_new['BSTADESC'] = $row['BSTADESC'];
        $ar_new['TDVSRF'] = $row['TDVSRF'];
        $ar_r["{$liv}"] = $ar_new;
    }
    $ar_r = &$ar_r["{$liv}"];
    sum_columns_value($ar_r, $row);
    $ar_r = &$ar_r['children'];
    
    
    //accodo la row nell'array
    $ar_r[] = $row;
    
} //while

echo "<div id='my_content'>";
echo "<div class=header_page>";
echo "<H2>Riepilogo consegne per data/fornitore</H2>";
echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";
echo "<table class=int1>";
echo "<tr class='liv_data'>";

$rowspan='';
$colspan='';

foreach($ar as $ktot => $totale){
    //liv totale    
    echo "<tr class='liv_totale'>
             <td valign='top' colspan=5>Totale generale</td>
             <td valign='top'class='number'>".n($totale['RTINFI'],2)."</td>
           </tr>";
    
    //liv data
    foreach($totale['children'] as $kdata => $data){
        
        //riga bianca
        echo "<tr><td valign='top' colspan=5>&nbsp;</td></tr>";
        
        echo "<tr class='liv3'>
              <td valign='top' colspan=5>Data consegna " . print_date($kdata) . "</td>
              <td valign='top'class='number'>".n($data['RTINFI'],2)."</td>
              </tr>";

        echo "<tr>
              <th valign='top' width=350>Ordine</th>
              <th valign='top' width=350>Stato ordine/Descrizione articolo</th>
    
              <th valign='top' align=right width=180>Sede/Codice articolo</th>
              <th valign='top' width=20>UM</th>
              <th valign='top' width=180>Riferimento/Qta</th>
              <th valign='top' align=right width=150>Importo</th>
              </tr>";
        
        //liv fornitore
        foreach($data['children'] as $kfor => $forn){
            /*echo "<pre>"; echo $kfor;*/
            echo "<tr class='liv2'>
                  <td valign='top' colspan=5>".'Fornitore ['.$forn['liv_cod'].'] '.$forn['task']."</td>
                  <td valign='top'class='number'>".n($forn['RTINFI'],2)."</td>
                  </tr>";
    
            
            //liv ordini
            foreach($forn['children'] as $kord => $ord){
                /*echo "<pre>"; echo $kord;*/ 
                echo "<tr class='liv1'>
                      <td valign='top'>Ordine " . $ord['liv_cod'] . ' del ' . print_date($ord['TDDTRG']) . "</td>
                      <td valign='top'colspan=1>".'['.$ord['TDSTAT'].'] '.$ord['BSTADESC']."</td>
                       
                      <td valign='top'>".'['.$ord['TDAG1'].'] '.$ord['CUAGDESC']."</td>
                      <td>&nbsp;</td>
                      <td valign='top' colspan=1>".$ord['TDVSRF']."</td>
                      <td valign='top'class='number'>".n($ord['RTINFI'],2)."</td>
                      </tr>";
                
                //righe
                foreach($ord['children'] as $krig => $rig){
                    echo "<tr class='liv4'>
                          <td>&nbsp;</td>
                          <td valign='top'>". $rig['ARDART'] . "</td>
                          <td valign='top'>". $rig['RDART'] . "</td>
                          <td valign='top'>". $rig['RDUM'] . "</td>
                          <td valign='top' class='number'>". $rig['RDQTA'] . "</td>
                          <td valign='top'class='number'>".n($rig['RTINFI'],2)."</td>
                          </tr>";}
            
            }
        }
    }
}
    


?>
</div>
</body>
</html>	
<?php }
?>