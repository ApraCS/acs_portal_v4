<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$ar_email_to = array();
$ar_email_json = acs_je($ar_email_to);

?>


<html>
 <head>

  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data td{background-color: #333333; color: white; font-weight: bold;}
   
   div#my_content h1{font-size: 22px; padding: 5px;}   
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'N';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 

<?php

function sum_columns_value(&$ar, $r){
    
    if($r['IMP_DDT'] > 0)
        $ar['importo'] += $r['IMP_DDT'];
    else
        $ar['importo'] += $r['IMP_ACQ'];
    
      return $ar;
}


$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$sql_where.= sql_where_by_combo_value('TD.TDSTAB', $form_values->f_sede);

$sql_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);

$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);

$sql_where.= sql_where_by_combo_value('MTFORN', $form_values->f_fornitore);

$sql_where.= sql_where_by_combo_value('MTAVAN', $form_values->f_stato_fornitore);

$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values->f_cliente);

$sql_where.= sql_where_by_combo_value('MTART', $form_values->f_cod_art);

if (strlen($form_values->f_cons_cli_da) > 0)
    $sql_where .= " AND TD.TDDTEP >= {$form_values->f_cons_cli_da}";
if (strlen($form_values->f_cons_cli_a) > 0)
    $sql_where .= " AND TD.TDDTEP <= {$form_values->f_cons_cli_a}";
    
if (strlen($form_values->f_cons_for_da) > 0)
    $sql_where .= " AND TD_ACQ.TDDTEP >= {$form_values->f_cons_for_da}";
if (strlen($form_values->f_cons_for_a) > 0)
    $sql_where .= " AND TD_ACQ.TDDTEP <= {$form_values->f_cons_for_a}";

//solo ordini evasi
if ($form_values->f_evasi == "Y"){
    $sql_where.= " AND TD.TDFN11 = 1 ";
}
//solo ordini da evadere
if ($form_values->f_evasi == "N"){
    $sql_where.= " AND TD.TDFN11 = 0 ";
}

$sql = "SELECT MT.*, TD.*, CFRGS1, ARFLR3, 
        TD_ACQ.TDSTAT AS STATO_ACQ, TD_ACQ.TDTPDO AS TIPO_ACQ, TD_ACQ.TDDTEP AS DATA_OF,
        TD_DDT.TDSTAT AS STATO_DDT, TD_DDT.TDTPDO AS TIPO_DDT, TD_DDT.TDDTEP AS DATA_DDT,
        RT_ACQ.RTINFI AS IMP_ACQ, RT_DDT.RTINFI AS IMP_DDT, TA.TADESC AS D_STMTO
        FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MT
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
        ON MTDT=TD.TDDT AND MTTIDO=TD.TDOTID AND MTINUM=TD.TDOINU AND MTAADO=TD.TDOADO AND MTNRDO=TD.TDONDO
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD_ACQ
        ON MTDT=TD_ACQ.TDDT AND MTTIDQ=TD_ACQ.TDTIDO AND MTINUQ=TD_ACQ.TDINUM AND MTAADQ=TD_ACQ.TDAADO AND MTNRDQ=TD_ACQ.TDNRDO
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD_DDT
        ON MTDT=TD_DDT.TDDT AND MTTIDE=TD_DDT.TDTIDO AND MTINUE=TD_DDT.TDINUM AND MTAADE=TD_DDT.TDAADO AND MTNRDE=TD_DDT.TDNRDO
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
        ON MT.MTDT = CF.CFDT AND MT.MTFORN = CF.CFCD AND CFTICF = 'F'
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT_ACQ
        ON RT_ACQ.RTDT = MTDT AND RT_ACQ.RTTIDO = MTTIDQ AND RT_ACQ.RTINUM = MTINUQ AND RT_ACQ.RTAADO = MTAADQ AND RT_ACQ.RTNRDO = MTNRDQ  AND RT_ACQ.RTNREC = MTNREQ  AND MTAVAN = 'P' AND RT_ACQ.RTVALU='EUR'
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT_DDT
        ON RT_DDT.RTDT = MTDT AND RT_DDT.RTTIDO = MTTIDE AND RT_DDT.RTINUM = MTINUE AND RT_DDT.RTAADO = MTAADE AND RT_DDT.RTNRDO = MTNRDE AND RT_DDT.RTNREC = MTNREE AND MTAVAN = 'E' AND RT_DDT.RTVALU='EUR'
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA
	    ON MTDT=TA.TADT AND TA.TATAID = 'STMTO'AND TA.TAKEY1 = MTAVAN  
	    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art']} AR 
	    ON AR.ARDT = MTDT AND AR.ARART = MTART       
        WHERE MTDT = '{$id_ditta_default}' AND MTSTAR NOT IN ('R', 'C') {$sql_where}
        ORDER BY TA.TAPESO, MTDTATP";

	
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();		
$result = db2_execute($stmt);

$ar = array();

/***********************************************************************************************
 *  ELENCO 
 ***********************************************************************************************/
 
if (1 == 1) {

 echo "<h1>Riepilogo avanzamento forniture</h1>";

 while ($row = db2_fetch_assoc($stmt)) {
		
		
		$tmp_ar_id = array();
		$ar_r= &$ar;
		
		$cod_liv0 = trim($row['MTFORN']);  //FORNITORE
		$cod_liv1 = trim($row['MTAVAN']);  //STATO MTO
		$cod_liv2 = implode("_", array(trim($row['TDDOCU']), $row['MTART']));  //DOCUMENTO_ARTICOLO
		
		//FORNITORE
		$liv=$cod_liv0;
		if(!isset($ar_r[$liv])){
		    $ar_new= array();
		    $ar_new['id'] = implode("|", $tmp_ar_id);
		    $ar_new['codice'] = $liv;
		    $ar_new['task'] = trim($row['CFRGS1']);
		 
		    $ar_r[$liv]=$ar_new;
		}
		
		$ar_r=&$ar_r[$liv];
		sum_columns_value($ar_r, $row);
		
		 $ar_r["val"]['TDDTEP'][$row['TDDTEP']] +=1;
		 $ar_r["val"]['TDCCON'][$row['TDCCON']] +=1;
		 $ar_r["val"]['TDONDO'][$row['TDONDO']] +=1;
		 $ar_r["val"]['MTART'][$row['MTART']] +=1;

		//STATO MTO
		$liv=$cod_liv1;
		$ar_r = &$ar_r['children'];
		$tmp_ar_id[] = $liv;
		if(!isset($ar_r[$liv])){
		    $ar_new= array();
		    $ar_new['id'] = implode("|", $tmp_ar_id);
		    $ar_new['codice']=$liv;
		    $ar_new['task'] =  $row['D_STMTO'];

		    $ar_r[$liv]=$ar_new;
		}
		
		$ar_r=&$ar_r[$liv];
		sum_columns_value($ar_r, $row);
	
		
		//DOCUMENTO_ARTICOLO
		$liv=$cod_liv2;
		$ar_r=&$ar_r['children'];
		$tmp_ar_id[] = $liv;
		if(!isset($ar_r[$liv])){
		    $ar_new= array();
		    $ar_new['id'] = implode("|", $tmp_ar_id);
		    $ar_new['codice']=$liv;
		    $ar_new['c_art']  	= trim($row['MTART']);
		    $ar_new['d_art']  	= trim($row['MTDART']);
		    $ar_new['c_cli'] 	= trim($row['TDCCON']);
		    $ar_new['d_cli'] 	= trim($row['TDDCON']);
		    $ar_new['MTAADQ'] 	= trim($row['MTAADQ']); 
		    $ar_new['MTAADE'] 	= trim($row['MTAADE']);
		    $ar_new['ord_cli'] 	= implode('_' , array(trim($row['TDOADO']), trim($row['TDONDO']), trim($row['TDOTPD']))) . " [" . trim($row['TDSTAT']) . "]";
		    $ar_new['ord_for'] 	= implode("_", array($row['MTAADQ'], $row['MTNRDQ'], $row['TIPO_ACQ'])). " [".$row['STATO_ACQ']."]";
		    $ar_new['ddt_ent'] 	= implode("_", array($row['MTAADE'], $row['MTNRDE'], $row['TIPO_DDT']));
		    
		    if(trim($row['MTAVAN']) == 'A')
		        $ar_new['data'] = trim($row['MTDTATP']);
		    else
		        $ar_new['data']  = trim($row['DATA_OF']);
		    
		    $ar_new['data_ddt'] = trim($row['DATA_DDT']);
		  
		    $ar_new['data_cons'] = trim($row['TDDTEP']);
		    
		    if(trim($row['MTSTAR']) == 'M')
		      $ar_new['st_reg'] = 'D';
		    
		    if(trim($row['ARFLR3']) == 'M')
		      $ar_new['mts'] = 'S';
		    
		    if($row['IMP_DDT'] > 0)
		        $ar_new['importo'] = $row['IMP_DDT'];
		     else
		         $ar_new['importo'] = $row['IMP_ACQ'];
	
		    $ar_r[$liv]=$ar_new;
		}
	
 } 

 foreach ($ar as $kar => $r){
     echo "<h2>Fornitore " . $r['task'] . "</h2>";

	echo "<table>";
	
	    echo "<th>Cliente<BR> (# " . count($r['val']['TDCCON']) . ")</th>";
		echo "<th>Denominazione</th>";		
		echo "<th>Ordine<br>Cliente<BR> (# " . count($r['val']['TDONDO']) . ")</th>";
		echo "<th>Cons.<br>ord.cli.<BR> (# " . count($r['val']['TDDTEP']) . ")</th>";
		echo "<th>Articolo<BR> (# " . count($r['val']['MTART']) . ")</th>";
		echo "<th>Da Mag.</th>";
		echo "<th>Disp.a mag.</th>";		
		echo "<th>Descrizione<BR>&nbsp;</th>";		
		echo "<th>Ordine<br>fornitore</th>";
		echo "<th>Data<br>consegna OF</th>";
		echo "<th>DDT<br>entrata</th>";
		echo "<th>Data<br>DDT</th>";
		echo "<th>Importo</th>";		
		
		echo "<tr class = 'ag_liv_data'>
              <td colspan=12>TOTALE</td>
              <td class = 'number'>" . n($r['importo'] , 2) . "</td>
              </tr>";
	
		foreach ($r['children'] as $kar1 => $r1){
		    
		    
		    echo "<tr class = 'liv1'>
                  <td colspan=12>" . $r1['task'] . "</td>
                  <td class = 'number'>" . n($r1['importo'] , 2) . "</td>
                  </tr>";
		    
		    foreach ($r1['children'] as $kar2 => $r2){
	    
		
		        if(trim($r2['MTAADQ']) != 0)
		            $ord_forn = $r2['ord_for'];
        		else
        		    $ord_forn = "&nbsp;";
		
		        if(trim($r2['MTAADE']) != 0)
		            $ddt_ent = $r2['ddt_ent'];
                else
                    $ddt_ent = "&nbsp;";
  
		echo "<tr>";
		echo "<td>" . trim($r2['c_cli']) .  "</td>";			
		echo "<td>" . trim($r2['d_cli']) .  "</td>";			
		echo "<td>" . $r2['ord_cli']."</td>";
		echo "<td>" . print_date($r2['data_cons']) . "</td>";
		echo "<td>" . trim($r2['c_art']) .  "</td>";
		echo "<td>" . trim($r2['mts']) .  "</td>";
		echo "<td>" . trim($r2['st_reg']) .  "</td>";
		echo "<td>" . trim($r2['d_art']) .  "</td>";			
		echo "<td width = '120px'>" . $ord_forn . "</td>";
		echo "<td>" . print_date($r2['data']) . "</td>";
		echo "<td>" . $ddt_ent . "</td>";
		echo "<td>" . print_date($r2['data_ddt']) . "</td>";
		echo  "<td class = 'number'>" . n($r2['importo'] , 2) . "</td>";
		echo "</tr>";
	
	}
		}
	
	echo "</table>";
} 

 
 
} 

 
 		
?>
 </div>
 </body>
</html>		