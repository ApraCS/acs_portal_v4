<?php

require_once("../../config.inc.php");
/* TODO: verificare permessi Admin */

$m_params = acs_m_params_json_decode();

$m_table_config = array(
    'tab_name' =>  $cfg_mod_DeskPVen['file_tabelle'],
    
    'TATAID' => 'DEPOS',
    'descrizione' => 'Gestione deposito',
    
    'fields_key' => array('TAKEY1'),
    
    'fields' => array(
        'TAKEY1' => array('label'	=> 'Codice', 'fw' =>'width : 100'),
        'TADESC' => array('label'	=> 'Descrizione'),
        'TAINDI' => array('label'	=> 'Indirizzo'),
        'TACAP' => array('label'	=> 'CAP'),
        'TALOCA' => array('label'	=> 'Localit&agrave;'),
        'TAPROV' => array('label'	=> 'Provincia'),
        'TANAZI' => array('label'	=> 'Nazione'),
        'TAFG01' => array('label'	=> 'Gestione logistica'),
    )
    
);

require ROOT_ABS_PATH . 'module/base/_gest_tataid_2.php';
