<?php

require_once "../../config.inc.php";
$mod_provenienza = new DeskPVen();
$cod_mod_provenienza = $mod_provenienza->get_cod_mod();
$info_importo_field = $cfg_mod_DeskPVen['info_importo_field'];


$s = new Spedizioni(array('no_verify' => 'Y'));
$main_module = new Spedizioni();

set_time_limit(240);


$ar_email_to = array();
$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");
$ar_email_to[] = array(trim($vettore->rec_data['TAMAIL']), "VETTORE " . j(trim($vettore->rec_data['TADESC'])) . " (" .  trim($vettore->rec_data['TAMAIL']) . ")");

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

if ($is_linux == 'Y')
    $_REQUEST['filter'] = strtr($_REQUEST['filter'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
    
$filtro = $s->get_elenco_ordini_create_filtro((array)json_decode($_REQUEST['filter']));  //filter
$stmt 	= $s->get_elenco_ordini($filtro, 'N', 'search');
?>
<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   
   table.int0{margin-bottom: 20px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   h1.page_title{font-size: 18px; padding-top: 10px; padding-bottom: 10px;}
   
   table.tab_itinerario{margin: 0px 0px 50px 0px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
 
<?php
 
    $liv0_in_linea = null;
	$ar = array();
	$tot_generale=array();
	
	while ($r = db2_fetch_assoc($stmt)) {

		//$liv0 = implode("|", array($r['TDDTEP'], $r['CSCITI'], $r['CSCVET'], $r['CSCAUT'], $r['CSCCON']));
		$liv0 = implode("|", array($r['TDSTAB']));
		//$liv1 = implode("|", array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));
		$liv1 = 'START';		
		$liv2 = implode("|", array($r['TDCCON'], $r['TDCDES']));		
		$liv3 = implode("|", array($r['TDDT'], $r['TDOTID'], $r['TDOINU'], $r['TDOADO'], $r['TDONDO']));		
		
		//stabilimento
		$t_ar = &$ar;
		$l = $liv0;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$s->decod_std('START', trim($r['TDSTAB'])), "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r); 					  								  
			$t_ar = &$t_ar[$l]['children'];
		
		//carico
		//non statto piu' per carico... ma lo uso come totale per itinerario
		$l = $liv1;
		if (!isset($t_ar[$l])){
 			//$itinerario = new Itinerari();
			//$itinerario->load_rec_data_by_k(array("TAKEY1"=>$r['TDCITI']));

 			$t_ar[$l] = array("cod" => $l, "descr"=>$s->decod_std('START', trim($r['TDSTAB'])), "record" => $r,
 								  "val" => array(), "children"=>array());
		}
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);
			$t_ar[$l]['carico'] = $s->get_carico_td($r);
			$t_ar[$l]['orario_carico'] = $r['CSHMPG'];			 								  
			$t_ar = &$t_ar[$l]['children'];
		 								  								  
		//cliente|dest
		$l = $liv2;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);								  
			$t_ar = &$t_ar[$l]['children'];								  								  

		//ordine
		$l = $liv3;
		if (!isset($t_ar[$l])) 				
 			$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
 								  "val" => array(), "children"=>array());
			$t_ar[$l]['val'] = somma_valori($t_ar[$l]['val'], $r);	
			
			$t_ar = &$t_ar[$l]['children'];		
			
			$tot_generale=somma_valori($tot_generale, $r);
		
	} //while

	//print_r($t_ar);

//	echo "<pre>"; print_r($ar); echo "</pre>";exit();
	
//STAMPO
 $cl_liv_cont = 0;
 if ($_REQUEST['stampa_dettaglio_ordini'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 $liv2_row_cl = ++$cl_liv_cont; 
 $liv1_row_cl = ++$cl_liv_cont;
 
echo "<div id='my_content'>"; 

if ($_REQUEST['stampa_dettaglio_ordini'] == "Y"){
	echo "<H1 class='page_title'>Riepilogo clienti/documenti per sede </H1>";
}else{
	echo "<H1 class='page_title'>Riepilogo clienti per sede</H1>";
}

echo liv0_intestazione_open($l0['record']);

foreach ($ar as $kl0 => $l0){


	  		foreach ($l0['children'] as $kl1 => $l1){
				echo liv1_intestazione_open($l1, $liv1_row_cl);	  			
			
			  		foreach ($l1['children'] as $kl2 => $l2){
						echo liv2_intestazione_open($l2, $liv2_row_cl);
						
						if ($_REQUEST['stampa_dettaglio_ordini']=="Y")
				  		foreach ($l2['children'] as $kl3 => $l3)
							echo liv3_intestazione_open($l3, $liv3_row_cl);			
							
					}			
			}		
	
	
}

if ($_REQUEST['stampa_importi']=='Y'){
	
	
	if ($_REQUEST['stampa_dettaglio_ordini']=="Y"){
		
		if ($_REQUEST['stampa_tel']=='Y'){
			$n_col_span = 7;
			$n_col_riga = 11;
			$td= "<td colspan=3>&nbsp;</td>";
		}else{
			$n_col_span = 5;
			$n_col_riga = 9;
			$td= "<td colspan=3>&nbsp;</td>";
		}
		
		
	}else{
	
			if ($_REQUEST['stampa_tel']=='Y'){
				$n_col_span = 7;
				$n_col_riga = 8;
			}else{
			   $n_col_span = 5;
			   $n_col_riga = 6;
			}
	
	}

echo "<tr><td colspan={$n_col_riga}>&nbsp;</td></tr>";
echo "<tr><td colspan={$n_col_span} ><b>Totale generale</b></td><td class=number><b>".n($tot_generale['IMPORTO'],2)."</b></td>".$td."</tr>";
}

echo liv0_intestazione_close($l0['record']);
echo "</div>";

 		
?>

 </body>
</html>







<?php




function somma_valori($ar, $r){
	
	global $info_importo_field;
	if (isset($info_importo_field)==false || strlen($info_importo_field) == 0)
		$info_importo_field = 'TDTIMP';

 $ar['IMPORTO'] += $r[$info_importo_field]; 

 return $ar;  
}

function liv3_intestazione_open($l, $cl_liv){


	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 5;
	else
		$n_col_span = 4;
	$sr = new SpedAutorizzazioniDeroghe();
	if ($sr->ha_RIPRO_by_k_ordine($l['record']['TDDOCU'])){
		$ar_tooltip_ripro = $sr->tooltip_RIPRO_by_k_ordine($l['record']['TDDOCU']);
		$txt_riprogrammazione = "<BR>" .  implode("<br>", $ar_tooltip_ripro);		
	}


	
	$ret = "
		<tr class=liv{$cl_liv}>";
		if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span = 7;
		else
		$n_col_span = 5;
		
		$ret.="<td colspan={$n_col_span} class=number>" . $l['record']['TDDVN1'] . "</td>";
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'], 2) . "</TD>";
		
		$ret.="<td>" . implode("_", array($l['record']['TDOADO'],  $l['record']['TDONDO'], $l['record']['TDOTPD'])) . " [{$l['record']['TDSTAT']}] </TD>
		 <td>" . print_date($l['record']['TDODRE']) . "</TD>
		 <td>" . $l['record']['TDVSRF'] . "{$txt_riprogrammazione}</TD>";			

	
	$ret .= "</tr>";
	

	
 return $ret;
}

function liv2_intestazione_open($l, $cl_liv){


	$ret = "
		<tr class=liv{$cl_liv}>
		 <td colspan=1>" . $l['record']['TDDCON'] . "</TD>
		 <td colspan=1>" . $l['record']['TDDLOC'] . "</TD>		 
		 <td colspan=1>" . $l['record']['TDPROD'] . "</TD>		 
		 <td colspan=1>" . $l['record']['TDDCAP'] . "</TD>
		 <td colspan=1>" . $l['record']['TDIDES'] . "</TD>";
	
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<td colspan=1>" . $l['record']['TDTEL'] . "</TD><td colspan=1>" . $l['record']['TDMAIL'] . "</TD> ";	
		 		 
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'], 2) . "</TD>";		 
	
	if ($_REQUEST['stampa_dettaglio_ordini']=='Y')
		$ret .= "<td>&nbsp</TD><td>&nbsp</TD><td>&nbsp</TD>";
	
	$ret .= "</tr>";
 return $ret;
}



function liv1_intestazione_open($l, $cl_liv){

	if ($l['orario_carico'] > 0)
		$orario_carico = ' [Orario: ' . print_ora($l['orario_carico']) . "]";
	else $orario_carico = '';

	if (strlen(trim($l['carico']['PSNOTE'])) > 0)
		$note_carico = "&nbsp;&nbsp;&nbsp;<b> [" . trim($l['carico']['PSNOTE']) . "]</b>";
	else $note_carico = '';

	if ($_REQUEST['stampa_tel']=='Y')
		$n_col_span =6;
	else
		$n_col_span = 5;
	
	if ($_REQUEST['stampa_dettaglio_ordini']=='Y' && $_REQUEST['stampa_tel']=='Y')
		$n_col_span =7;
	
	if ($_REQUEST['stampa_dettaglio_ordini']!='Y' && $_REQUEST['stampa_importi']!='Y' && $_REQUEST['stampa_tel']=='Y')
		$n_col_span =7;
	
	if ($_REQUEST['stampa_importi']=='Y' && $_REQUEST['stampa_tel']=='Y')
			$n_col_span =7;
	
	//riga vuota
	$ret ="<tr><td colspan=11>&nbsp;</td></tr>";
	
	$ret .= "
		<tr class=liv{$cl_liv}>
		 <td colspan={$n_col_span}>Sede " . $l['descr'] . "</TD>
		
		 ";
	
	
	if ($_REQUEST['stampa_importi']=='Y') $ret .= "<td colspan=1 class=number>" . n($l['val']['IMPORTO'],2) . "</TD>";		 

	if ($_REQUEST['stampa_dettaglio_ordini']=='Y')
		$ret .= "<td colspan=3 class=number>&nbsp</TD>";
	
	$ret .="</tr>";
 return $ret;
}



function liv0_intestazione_open($r){
	global $s, $spedizione, $itinerario;

	
	$ret = "
  		
			<table class='int1 tab_itinerario'>
			 <tr>
			  <th>Cliente</th>
			  <th>Localit&agrave;</th>
			  <th>Pr</th>			  			  
			  <th>Cap</th>
			  <th>Indirizzo</th>";

	
	if ($_REQUEST['stampa_tel']=='Y')  $ret .= "<th>Telefono</TH><th>Email</TH> ";	
		  
	if ($_REQUEST['stampa_importi']=='Y') $ret .="<th>Importo</th>";
	
	if ($_REQUEST['stampa_dettaglio_ordini']=="Y"){
	
	$ret .= "
	<th>Ordine</th>
	<th>Data</th>
	<th>Riferimento</th>";
	
	}
	
	$ret .= "</tr>";	
 return $ret;	
}

function liv0_intestazione_close($r){
	return "</table>";
}



?>		