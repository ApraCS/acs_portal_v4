<?php
require_once("../../config.inc.php");
$_module_descr = "Desktop POS";

$main_module = new DeskPVen();

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}


$ar_email_json = acs_je($ar_email_to);

?>



<html>
<head>
<title>ACS Portal_PVen</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />

<!-- <link rel="stylesheet" type="text/css" href="resources/css/calendar.css" /> -->
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/extensible-all.css" />
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/calendar.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">

 /* EVIDENZIAZIONE RIGA SELEZIONATA */
 .x-panel-body.highlight_row_selected .x-grid-row-selected .x-grid-cell, 
 .x-panel-body.highlight_row_selected .x-grid-row-selected .x-grid-rowwrap-div {
    background-color: yellow !important;
}

 /* REGOLE RIORDINO */
 .x-grid-cell.lu{background-color: #dadada;} 
 .x-grid-cell.ma{background-color: #e5e5e5;} 
 .x-grid-cell.me{background-color: #eeeeee;} 
 .x-grid-cell.gi{background-color: #FFFFFF;} 
 .x-grid-cell.ve{background-color: #F4EDAF;} 
 .x-grid-cell.sa{background-color: #FFFFFF;}
 
 .x-grid-cell.attivi{background-color: #A0DB8E;}
 .x-grid-cell.elaborazione-old-1{background-color: #e5e5e5;} 
 .x-grid-cell.elaborazione-old-2{background-color: #F9BFC1;} 

.x-panel td.x-grid-cell.festivo{opacity: 0.6;}

.x-panel td.x-grid-cell.con_carico_1{background-color: #909090; color: white; font-weight: bold;} /* SOLO ALCUNI HANNO IL CARICO */
.x-panel td.x-grid-cell.con_carico_2{background-color: #3c3c3c; color: white; font-weight: bold;}  /* HANNO TUTTI IL CARICO */
.x-panel td.x-grid-cell.con_carico_3xxx{background-color: #F9827F; font-weight: bold;}  /* NON CARICO. CON DATE CONF. */
.x-panel td.x-grid-cell.con_carico_4xxx{background-color: #F9BFC1; font-weight: bold;}  /* NON CARICO. CON DATE TASS. */
.x-panel td.x-grid-cell.con_carico_5xxx{background-color: #CEEA82; font-weight: bold;}  /* ALCUNI EVASI */
.x-panel td.x-grid-cell.con_carico_6{background-color: #8CD600; font-weight: bold;}  /* TUTTI EVASI */


.x-panel .x-column-header-text{font-weight: bold;}


.x-panel.cls_tonalita_verde .x-panel-header { 
 background-image: -moz-linear-gradient(center top , #8EDD65, #8EDD65 45%, #8EDD65 46%, #8EDD65 50%, #8EDD65 51%, #8EDD65);
 background-image: -webkit-linear-gradient(top,#8EDD65,#8EDD65 45%,#8EDD65 46%,#8EDD65 50%,#8EDD65 51%,#8EDD65); 
 }
 
 .x-panel.cls_tonalita_verde .x-panel-body { 
  background:  #C2E189; 
 }
 
 .x-panel.cls_tonalita_arancione .x-panel-header { 
 background-image: -moz-linear-gradient(center top , #FFB549, #FFB549 45%, #FFB549 46%, #FFB549 50%, #FFB549 51%, #FFB549);
 background-image: -webkit-linear-gradient(top, #FFB549, #FFB549 45%, #FFB549 46%, #FFB549 50%, #FFB549 51%, #FFB549); 
 }
  
.x-panel.cls_tonalita_arancione .x-panel-body { 
 background: #F8E08E; 
 }

tr.liv_totale td.x-grid-cell{background-color: #F4EDAF; border: 1px solid white;}
tr.liv_totale td.x-grid-cell.festivo{opacity: 0.6;}
tr.liv_0 td.x-grid-cell{border: 1px solid white; font-weight: bold;}
tr.liv_1 td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: bold;}
tr.liv_1_no_b td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: normal;}
tr.liv_1.no_carico td.x-grid-cell{background-color: #C4D8E2}

.x-panel.elenco_ordini tr.liv_1 td.x-grid-cell{font-weight: normal;}
.x-panel.elenco_ordini tr.liv_2 td.x-grid-cell{font-weight: bold;}

tr.liv_totale.dett_selezionato td.x-grid-cell{background-color: #F4ED7C; font-weight: bold;}

/*TIPO ORDINE (raggr)*/
.x-grid-cell.tipoOrd.O {background-color: #93C6E0;} /* mostre */
.x-grid-cell.tipoOrd.S {background-color: #D3BFB7;} /* materiale promozionale */
.x-grid-cell.tipoOrd.R {background-color: #E2D67C;} /* composizioni promozionali */
.x-grid-cell.tipoOrd.L {background-color: white;}   /* assistenze */


/*RIGHE ORDINE*/
.x-grid-row.rigaRevocata .x-grid-cell{background-color: #F9BFC1}
.x-grid-row.rigaChiusa .x-grid-cell{background-color: #CEEA82}
.x-grid-row.rigaParziale .x-grid-cell{background-color: #F4EDAF}
.x-grid-row.rigaNonSelezionata .x-grid-cell{background-color: #cccccc}


/*TIPO PRIORITA*/
 .x-grid-cell.tpSfondoRosa{background-color: #F9BFC1;}
 .x-grid-cell.tpSfondoGrigio{background-color: #cecece;}
 .x-grid-cell.tpSfondoCelesteEl{background-color: #99D6DD;}
 

/*ICONE*/
.iconSpedizione {background-image: url(<?php echo img_path("icone/16x16/spedizione.png") ?>) !important;}
.iconConf {background-repeat: no-repeat;
    background-image: url(<?php echo img_path("icone/16x16/puntina_rossa.png") ?>) !important;
}
.iconPrint {background-image: url(<?php echo img_path("icone/16x16/print.png") ?>) !important;}
.iconClienti {background-image: url(<?php echo img_path("icone/16x16/clienti.png") ?>) !important;}
 
/* celle classe ABC */
.x-panel.x-table-index .x-panel-body {
		text-align: center;
    	background-color: #D3D3D3;
		font-size: 18;
		/*font-weight: bold;*/
}
 .x-panel.x-table-index.leg .x-panel-body {		
		font-size: 14;
}
.x-panel.x-table-index .x-panel-body span {font-size: 10px;}
 
/* bordi laterali tabella non visualizzabili */
.table-border {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	text-align: right;
}
.table-border-final {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	border-right: 1px solid #99BBE8;
	text-align: right;
}

a.view_dett_class_gc{text-decoration: none;}
 

.x-toolbar .strong{font-weight: bold;}

.x-grid-cell-inner p.sottoriga-liv_2{text-indent: 70px}



/* ANALISI BILANCIO */
.x-grid-row.S1 .x-grid-cell{background-color: #99CCCC; padding-top: 3px; padding-bottom: 3px; }
.x-grid-row.S2 .x-grid-cell{background-color: #99B3CC; font-weight: bold; padding-top: 4px; padding-bottom: 4px;}
.x-grid-row.dett .x-grid-cell{background-color: #FFFFE0; font-weight: normal;}
.x-grid-row .x-grid-cell.tot{font-weight: bold;}
/* cal: dettaglio per mese/giorno */
.x-grid-row.cal .x-grid-cell{padding-top: 1px; padding-bottom: 0px; border: 1px solid #f9f9f9;}
.x-grid-row.cal .x-grid-cell.day{background-color: #84E2A8} /* giorno calendario */
.x-grid-row.cal .x-grid-cell.day.alt{background-color: #B5E8BF} /* alterno colori colonne mese */
.x-grid-row.cal .x-grid-cell.day.gw6{background-color: #F9DDD6} /* SABATO */
.x-grid-row.cal .x-grid-cell.day.gw0{background-color: #F9BA82} /* DOMENICA */
.x-grid-row.cal .x-grid-cell.tot{background-color: #D8DDCE} /* TOTALI */

/* FLUSSI DI CASSA  ----------------------------------------- */
.x-panel.flussi_cassa_tree_ .x-grid-table{border-collapse: collapse;}
.x-panel.flussi_cassa_tree_ .x-grid-row.S1 .x-grid-cell{border: 1px solid gray; font-weight: bold;}
.x-panel.flussi_cassa_tree_ .x-grid-row.dett .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_ .x-grid-row.S2 .x-grid-cell{border: 1px solid gray; font-weight: bold;}

.x-panel.flussi_cassa_tree_ .x-grid-row.S1 .x-grid-cell.scaduto{background-color: #F99B0C;} /* colonna scaduto: arancione SI */
.x-panel.flussi_cassa_tree_ .x-grid-row.dett .x-grid-cell.scaduto{background-color: #F2CE68;} /* colonna scaduto: arancione dett */

.x-panel.flussi_cassa_tree_ .x-grid-row.S1 .x-grid-cell.oltre{background-color: #E8DD21;} /* colonna oltre: giallo SI */
.x-panel.flussi_cassa_tree_ .x-grid-row.dett .x-grid-cell.oltre{background-color: #F4EDAF;} /* colonna oltre: giallo dett */

/* tree scaduto */
.x-panel.flussi_cassa_tree_scaduto .x-grid-table{border-collapse: collapse;}
.x-panel.flussi_cassa_tree_scaduto .x-grid-row.S1 .x-grid-cell{border: 1px solid gray; font-weight: bold; background-color: #F99B0C;}
.x-panel.flussi_cassa_tree_scaduto .x-grid-row.dett .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_scaduto .x-grid-row.S2 .x-grid-cell{border: 1px solid gray;}

/* tree oltre */
.x-panel.flussi_cassa_tree_oltre .x-grid-table{border-collapse: collapse;}
.x-panel.flussi_cassa_tree_oltre .x-grid-row.S1 .x-grid-cell{border: 1px solid gray; font-weight: bold; background-color: #E8DD21;}
.x-panel.flussi_cassa_tree_oltre .x-grid-row.dett .x-grid-cell{border: 1px solid gray;}
.x-panel.flussi_cassa_tree_oltre .x-grid-row.S2 .x-grid-cell{border: 1px solid gray;}


/* PROVVIGIONI */
.x-grid-row.liv_0 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_totale .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_1 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_2 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_3 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_4 .x-grid-cell.con_maturato{background-color: #a0db8e} /* verdino */
.x-grid-row.liv_0_2 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_totale.flight .x-grid-cell{background-color: #FFD69B; border: 1px solid white;}

.x-grid-row.liv_0 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_totale .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_1 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_2 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_3 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_4 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C} /* arancio */
.x-grid-row.liv_0_2 .x-grid-cell.con_maturato_parziale{background-color: #F9E04C;} /* arancio */

.x-grid-row.liv_1 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_2 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_3 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */
.x-grid-row.liv_4 .x-grid-cell.con_maturato{background-color: #a0db8e;} /* verdino */


.x-grid-row.liv_totale.MATURATO.dett_selezionato .x-grid-cell{background-color: #FFB777; border: 1px solid white;}
.x-grid-row.liv_totale.MATURATO .x-grid-cell{background-color: #FFD69B; border: 1px solid white;}

.x-grid-row.liv_totale.LIQUIDATO.dett_selezionato .x-grid-cell{background-color: #15b25e; border: 1px solid white;}
.x-grid-row.liv_totale.LIQUIDATO .x-grid-cell{background-color: #69935e; border: 1px solid white;}
.iconChartBar {background-image: url(<?php echo img_path("icone/16x16/chart_bar.png") ?>) !important;}
</style>



<script type="text/javascript" src="../../../extjs/ext-all.js"></script>

<script type="text/javascript" src="../../js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
<script type="text/javascript" src="../../js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>


<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>

<?php if (!isset($disabilita_gmap) || $disabilita_gmap != 'Y'){ ?>
	<script type="text/javascript" src="<?php echo site_protocol();?>maps.google.com/maps/api/js?sensor=false"></script>	
	<script src=<?php echo acs_url("js/gmaps.js") ?>></script>
<?php } ?>


<script src=<?php echo acs_url("js/acs_js.js") ?>></script>



<script type="text/javascript">



	function gmapPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=400, height=400, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}


	function allegatiPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=600, height=600, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}	


	var refresh_stato_aggiornamento = {
        run: doRefreshStatoAggiornamento,
        interval: 90 * 1000 //millisecondi
      }

	function doRefreshStatoAggiornamento() {
        Ext.Ajax.request({
            url : 'get_stato_aggiornamento.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btStatoAggiornamento = document.getElementById('bt-stato-aggiornamento-esito');
                btStatoAggiornamento.innerHTML = jsonData.html;
            },
            failure: function ( result, request) {
                console.log('failed');
            }
        });
	}


	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
            //'Ext.calendar': 'src',
	        "Extensible": "../../js/extensible-1.5.2/src", 
			"Extensible.example": "../../js/extensible-1.5.2/examples"	                   
        }	    
	});Ext.Loader.setPath('Ext.ux', '../../ux');


    Ext.require(['*'
                 , 'Ext.ux.grid.FiltersFeature'                 
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
                 , 'Extensible.calendar.data.MemoryEventStore'
                 , 'Extensible.calendar.CalendarPanel'  
                 , 'Extensible.example.calendar.data.Events'
				 , 'Extensible.calendar.data.CalendarMappings'
    			 , 'Extensible.calendar.data.EventMappings'
    			 , 'Ext.ux.grid.Printer'
      			 , 'Ext.ux.RowExpander' 
                 ]);

//    ,
//    'Ext.calendar.data.MemoryCalendarStore',
//    'Ext.calendar.data.MemoryEventStore',
//    'Ext.calendar.data.Events',
//    'Ext.calendar.data.Calendars'	  




    

    Ext.define('ModelOrdini', {
        extend: 'Ext.data.Model',
        fields: [
        			{name: 'task'}, {name: 'n_carico'}, {name: 'k_carico'}, {name: 'sped_id'}, {name: 'cod_iti'}, {name: 'dt_orig'},
        			{name: 'k_ordine'}, {name: 'prog'}, {name: 'n_O'}, {name: 'n_M'}, {name: 'n_P'}, {name: 'n_CAV'}, {name: 'n_A'}, {name: 'n_CV'},
        			{name: 'liv'}, {name: 'liv_cod'}, {name: 'n_stadio'}, {name: 'row_cls'},
					{name: 'cliente'}, {name: 'k_cli_des'}, {name: 'gmap_ind'}, {name: 'scarico_intermedio'},
					{name: 'nr'}, {name: 'rec_stato'},
					{name: 'volume'},{name: 'pallet'},{name: 'peso'},
					{name: 'volume_disp'},{name: 'pallet_disp'},{name: 'peso_disp'},
					{name: 'colli'},{name: 'colli_disp'},{name: 'colli_sped'},{name: 'data_disp'},{name: 'data_sped'},{name: 'stato_sped'}, {name: 'data_cons_cli'}, {name: 'data_conf_ord'}, {name: 'fl_data_disp'},
					{name: 'riferimento'}, {name: 'vettore'}, {name: 'architetto'},
					{name: 'tipo'},{name: 'raggr'},{name: 'nr_fatt'},
					{name: 'cons_rich'},					
					{name: 'priorita'},{name: 'tp_pri'},
					{name: 'seq_carico'}, {name: 'tooltip_seq_carico'}, {name: 'dirty_seq_carico'}, {name: 'tooltip_ripro'},
					{name: 'stato'}, {name: 'cod_pagamento'}, {name: 'des_pagamento'}, {name: 'preferenza'}, {name: 'preferenza_anag'},
					{name: 'cons_conf'},{name: 'cons_prog'}, {name: 'cons_prog_stadio'},{name: 'ind_modif'},
					{name: 'data_reg'}, {name: 'data_scadenza'}, {name: 'data_emiss_OF'},
					{name: 'importo'}, {name: 'gg_rit'},
					{name: 'fl_evaso'}, {name : 'TDFN19'}, {name: 'fl_bloc'}, {name: 'fl_cli_bloc'}, {name: 'fl_art_manc'}, 
					{name: 'fl_da_prog'}, {name: 'art_da_prog'}, {name: 'fl_new'}, {name : 'tdfg06'}, {name : 'caparra'}, {name : 'tp_cap'},
					{name: 'qtip_tipo'}, {name: 'qtip_stato'}, {name: 'qtip_pri'}
        ]
    });    



    Ext.define('RowCalendar', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task',     type: 'string'},
            {name: 'liv',      type: 'string'},            
            {name: 'flag1',    type: 'string'},            
            {name: 'user',     type: 'string'},
            {name: 'duration', type: 'string'},
            {name: 'done',     type: 'boolean'},
            {name: 'da_data',  type: 'float'}, {name: 'itin'}, {name: 'sped_id'}, {name: 'k_cliente'},            
            {name: 'd_1'}, {name: 'd_1_f'}, {name: 'd_1_t'}, {name: 'd_1_d'},
            {name: 'd_2'}, {name: 'd_2_f'}, {name: 'd_2_t'}, {name: 'd_2_d'},
            {name: 'd_3'}, {name: 'd_3_f'}, {name: 'd_3_t'}, {name: 'd_3_d'},
            {name: 'd_4'}, {name: 'd_4_f'}, {name: 'd_4_t'}, {name: 'd_4_d'},
            {name: 'd_5'}, {name: 'd_5_f'}, {name: 'd_5_t'}, {name: 'd_5_d'},
            {name: 'd_6'}, {name: 'd_6_f'}, {name: 'd_6_t'}, {name: 'd_6_d'},
            {name: 'd_7'}, {name: 'd_7_f'}, {name: 'd_7_t'}, {name: 'd_7_d'},
            {name: 'd_8'}, {name: 'd_8_f'}, {name: 'd_8_t'}, {name: 'd_8_d'},
            {name: 'd_9'}, {name: 'd_9_f'}, {name: 'd_9_t'}, {name: 'd_9_d'},
            {name: 'd_10'}, {name: 'd_10_f'}, {name: 'd_10_t'}, {name: 'd_10_d'},
            {name: 'd_11'}, {name: 'd_11_f'}, {name: 'd_11_t'}, {name: 'd_11_d'},
            {name: 'd_12'}, {name: 'd_12_f'}, {name: 'd_12_t'}, {name: 'd_12_d'},
            {name: 'd_13'}, {name: 'd_13_f'}, {name: 'd_13_t'}, {name: 'd_13_d'},
            {name: 'd_14'}, {name: 'd_14_f'}, {name: 'd_14_t'}, {name: 'd_14_d'},
			{name: 'fl_bloc'}, {name: 'fl_ord_bloc'}, {name: 'fl_art_mancanti'}, {name: 'fl_da_prog'}, {name: 'ha_contratti'}, {name: 'ha_proposte_MTO'}                                   
        ]
    });    

    //Model per albero itinerari
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task', type: 'string'}, {name: 'user', type: 'string'}, 'liv', 'sped_id', 'k_cliente', 'data'
        ]
    });    


    Ext.define('Ux.InputTextMask', {
   	   constructor: function(mask, clearWhenInvalid) {
   	     if(clearWhenInvalid === undefined)
   	         this.clearWhenInvalid = true;
   	      else
   	         this.clearWhenInvalid = clearWhenInvalid;
   	      this.rawMask = mask;
   	      this.viewMask = '';
   	      this.maskArray = new Array();
   	      var mai = 0;
   	      var regexp = '';
   	      for(var i=0; i<mask.length; i++){
   	         if(regexp){
   	            if(regexp == 'X'){
   	               regexp = '';
   	            }
   	           if(mask.charAt(i) == 'X'){
   	               this.maskArray[mai] = regexp;
   	               mai++;
   	               regexp = '';
   	            } else {
   	               regexp += mask.charAt(i);
   	           }
   	         } else if(mask.charAt(i) == 'X'){
   	            regexp += 'X';
   	            this.viewMask += '_';
   	         } else if(mask.charAt(i) == '9' || mask.charAt(i) == 'L' || mask.charAt(i) == 'l' || mask.charAt(i) == 'A') {
   	            this.viewMask += '_';
   	            this.maskArray[mai] = mask.charAt(i);
   	            mai++;
   	         } else {
   	            this.viewMask += mask.charAt(i);
   	            this.maskArray[mai] = RegExp.escape(mask.charAt(i));
   	            mai++;
   	         }
   	      }

   	      this.specialChars = this.viewMask.replace(/(L|l|9|A|_|X)/g,'');
   	      return this;
   	   },

   	   init: function(field) {
   	      this.field = field;

   	      if (field.rendered){
   	         this.assignEl();
   	      } else {
   	        field.on('render', this.assignEl, this);
   	     }

   	      field.on('blur',this.removeValueWhenInvalid, this);
   	      field.on('focus',this.processMaskFocus, this);
   	   },

   	   assignEl: function() {
   	      this.inputTextElement = this.field.inputEl.dom;
   	      this.field.inputEl.on('keypress', this.processKeyPress, this);
   	      this.field.inputEl.on('keydown', this.processKeyDown, this);
   	      if(Ext.isSafari || Ext.isIE){
   	         this.field.inputEl.on('paste',this.startTask,this);
   	         this.field.inputEl.on('cut',this.startTask,this);
   	      }
   	      if(Ext.isGecko || Ext.isOpera){
   	         this.field.inputEl.on('mousedown',this.setPreviousValue,this);
   	      }
   	      if(Ext.isGecko){
   	        this.field.inputEl.on('input',this.onInput,this);
   	      }
   	      if(Ext.isOpera){
   	        this.field.inputEl.on('input',this.onInputOpera,this);
   	      }
   	   },
   	   onInput: function(){
   	      this.startTask(false);
   	   },
   	   onInputOpera: function(){
   	      if(!this.prevValueOpera){
   	         this.startTask(false);
   	      }else{
   	         this.manageBackspaceAndDeleteOpera();
   	      }
   	   },

   	   manageBackspaceAndDeleteOpera: function(){
   	      this.inputTextElement.value=this.prevValueOpera.cursorPos.previousValue;
   	      this.manageTheText(this.prevValueOpera.keycode,this.prevValueOpera.cursorPos);
   	      this.prevValueOpera=null;
   	   },

   	   setPreviousValue: function(event){
   	      this.oldCursorPos=this.getCursorPosition();
   	   },

   	   getValidatedKey: function(keycode, cursorPosition) {
   	      var maskKey = this.maskArray[cursorPosition.start];
   	      if(maskKey == '9'){
   	         return keycode.pressedKey.match(/[0-9]/);
   	      } else if(maskKey == 'L'){
   	         return (keycode.pressedKey.match(/[A-Za-z]/))? keycode.pressedKey.toUpperCase(): null;
   	      } else if(maskKey == 'l'){
   	         return (keycode.pressedKey.match(/[A-Za-z]/))? keycode.pressedKey.toLowerCase(): null;
   	      } else if(maskKey == 'A'){
   	         return keycode.pressedKey.match(/[A-Za-z0-9]/);
   	      } else if(maskKey){
   	         return (keycode.pressedKey.match(new RegExp(maskKey)));
   	      }
   	      return(null);
   	   },

   	   removeValueWhenInvalid: function() {
   	      if(this.clearWhenInvalid && this.inputTextElement.value.indexOf('_') > -1){
   	         this.inputTextElement.value = '';
   	      }
   	   },

   	   managePaste: function() {
   	      if(this.oldCursorPos==null){
   	        return;
   	      }
   	      var valuePasted=this.inputTextElement.value.substring(this.oldCursorPos.start,this.inputTextElement.value.length-(this.oldCursorPos.previousValue.length-this.oldCursorPos.end));
   	      if(this.oldCursorPos.start<this.oldCursorPos.end){
   	         this.oldCursorPos.previousValue =
   	            this.oldCursorPos.previousValue.substring(0,this.oldCursorPos.start)+
   	            this.viewMask.substring(this.oldCursorPos.start,this.oldCursorPos.end)+
   	            this.oldCursorPos.previousValue.substring(this.oldCursorPos.end,this.oldCursorPos.previousValue.length);
   	         valuePasted=valuePasted.substr(0,this.oldCursorPos.end-this.oldCursorPos.start);
   	      }
   	      this.inputTextElement.value=this.oldCursorPos.previousValue;
   	      keycode = {
   	         unicode: '',
   	         isShiftPressed: false,
   	         isTab: false,
   	         isBackspace: false,
   	         isLeftOrRightArrow: false,
   	         isDelete: false,
   	         pressedKey: ''
   	      }
   	      var charOk=false;
   	      for(var i=0;i<valuePasted.length;i++){
   	         keycode.pressedKey=valuePasted.substr(i,1);
   	         keycode.unicode=valuePasted.charCodeAt(i);
   	         this.oldCursorPos=this.skipMaskCharacters(keycode,this.oldCursorPos);
   	         if(this.oldCursorPos===false){
   	            break;
   	         }
   	         if(this.injectValue(keycode,this.oldCursorPos)){
   	            charOk=true;
   	            this.moveCursorToPosition(keycode, this.oldCursorPos);
   	            this.oldCursorPos.previousValue=this.inputTextElement.value;
   	            this.oldCursorPos.start=this.oldCursorPos.start+1;
   	         }
   	      }
   	      if(!charOk && this.oldCursorPos!==false){
   	         this.moveCursorToPosition(null, this.oldCursorPos);
   	      }
   	      this.oldCursorPos=null;
   	   },

   	   processKeyDown: function(e){
   	      this.processMaskFormatting(e,'keydown');
   	   },

   	   processKeyPress: function(e){
   	      this.processMaskFormatting(e,'keypress');
   	   },

   	   startTask: function(setOldCursor){
   	      if(this.task==undefined){
   	         this.task=new Ext.util.DelayedTask(this.managePaste,this);
   	     }
   	      if(setOldCursor!== false){
   	         this.oldCursorPos=this.getCursorPosition();
   	     }
   	     this.task.delay(0);
   	   },

   	   skipMaskCharacters: function(keycode, cursorPos){
   	      if(cursorPos.start!=cursorPos.end && (keycode.isDelete || keycode.isBackspace))
   	         return(cursorPos);
   	      while(this.specialChars.match(RegExp.escape(this.viewMask.charAt(((keycode.isBackspace)? cursorPos.start-1: cursorPos.start))))){
   	         if(keycode.isBackspace) {
   	            cursorPos.dec();
   	         } else {
   	            cursorPos.inc();
   	         }
   	         if(cursorPos.start >= cursorPos.previousValue.length || cursorPos.start < 0){
   	            return false;
   	         }
   	      }
   	      return(cursorPos);
   	   },

   	   isManagedByKeyDown: function(keycode){
   	      if(keycode.isDelete || keycode.isBackspace){
   	         return(true);
   	      }
   	      return(false);
   	   },

   	   processMaskFormatting: function(e, type) {
   	      this.oldCursorPos=null;
   	      var cursorPos = this.getCursorPosition();
   	      var keycode = this.getKeyCode(e, type);
   	      if(keycode.unicode==0){//?? sometimes on Safari
   	         return;
   	      }
   	      if((keycode.unicode==67 || keycode.unicode==99) && e.ctrlKey){//Ctrl+c, let's the browser manage it!
   	         return;
   	      }
   	      if((keycode.unicode==88 || keycode.unicode==120) && e.ctrlKey){//Ctrl+x, manage paste
   	         this.startTask();
   	         return;
   	     }
   	      if((keycode.unicode==86 || keycode.unicode==118) && e.ctrlKey){//Ctrl+v, manage paste....
   	         this.startTask();
   	        return;
   	      }
   	     if((keycode.isBackspace || keycode.isDelete) && Ext.isOpera){
   	        this.prevValueOpera={cursorPos: cursorPos, keycode: keycode};
   	        return;
   	     }
   	      if(type=='keydown' && !this.isManagedByKeyDown(keycode)){
   	         return true;
   	      }
   	      if(type=='keypress' && this.isManagedByKeyDown(keycode)){
   	        return true;
   	     }
   	     if(this.handleEventBubble(e, keycode, type)){
   	        return true;
   	     }
   	     return(this.manageTheText(keycode, cursorPos));
   	   },

   	   manageTheText: function(keycode, cursorPos){
   	      if(this.inputTextElement.value.length === 0){
   	         this.inputTextElement.value = this.viewMask;
   	      }
   	      cursorPos=this.skipMaskCharacters(keycode, cursorPos);
   	      if(cursorPos===false){
   	         return false;
   	     }
   	    if(this.injectValue(keycode, cursorPos)){
   	        this.moveCursorToPosition(keycode, cursorPos);
   	      }
   	      return(false);
   	   },

   	   processMaskFocus: function(){
   	      if(this.inputTextElement.value.length == 0){
   	         var cursorPos = this.getCursorPosition();
   	         this.inputTextElement.value = this.viewMask;
   	         this.moveCursorToPosition(null, cursorPos);
   	      }
   	   },

   	   isManagedByBrowser: function(keyEvent, keycode, type){
   	      if(((type=='keypress' && keyEvent.charCode===0) ||
   	         type=='keydown') && (keycode.unicode==Ext.EventObject.TAB ||
   	         keycode.unicode==Ext.EventObject.RETURN ||
   	         keycode.unicode==Ext.EventObject.ENTER ||
   	         keycode.unicode==Ext.EventObject.SHIFT ||
   	         keycode.unicode==Ext.EventObject.CONTROL ||
   	         keycode.unicode==Ext.EventObject.ESC ||
   	         keycode.unicode==Ext.EventObject.PAGEUP ||
   	         keycode.unicode==Ext.EventObject.PAGEDOWN ||
   	         keycode.unicode==Ext.EventObject.END ||
   	         keycode.unicode==Ext.EventObject.HOME ||
   	         keycode.unicode==Ext.EventObject.LEFT ||
   	         keycode.unicode==Ext.EventObject.UP ||
   	        keycode.unicode==Ext.EventObject.RIGHT ||
   	        keycode.unicode==Ext.EventObject.DOWN)){
   	            return(true);
   	      }
   	      return(false);
   	   },

   	   handleEventBubble: function(keyEvent, keycode, type) {
   	      try {
   	         if(keycode && this.isManagedByBrowser(keyEvent, keycode, type)){
   	            return true;
   	         }
   	         keyEvent.stopEvent();
   	         return false;
   	      } catch(e) {
   	         alert(e.message);
   	      }
   	   },

   	  getCursorPosition: function() {
   	      var s, e, r;
   	      if(this.inputTextElement.createTextRange){
   	         r = document.selection.createRange().duplicate();
   	         r.moveEnd('character', this.inputTextElement.value.length);
   	         if(r.text === ''){
   	            s = this.inputTextElement.value.length;
   	         } else {
   	            s = this.inputTextElement.value.lastIndexOf(r.text);
   	         }
   	         r = document.selection.createRange().duplicate();
   	         r.moveStart('character', -this.inputTextElement.value.length);
   	         e = r.text.length;
   	      } else {
   	         s = this.inputTextElement.selectionStart;
   	         e = this.inputTextElement.selectionEnd;
   	      }
   	      return this.CursorPosition(s, e, r, this.inputTextElement.value);
   	   },

   	   moveCursorToPosition: function(keycode, cursorPosition) {
   	      var p = (!keycode || (keycode && keycode.isBackspace ))? cursorPosition.start: cursorPosition.start + 1;
   	      if(this.inputTextElement.createTextRange){
   	         cursorPosition.range.move('character', p);
   	         cursorPosition.range.select();
   	      } else {
   	         this.inputTextElement.selectionStart = p;
   	         this.inputTextElement.selectionEnd = p;
   	      }
   	   },

   	   injectValue: function(keycode, cursorPosition) {
   	      if (!keycode.isDelete && keycode.unicode == cursorPosition.previousValue.charCodeAt(cursorPosition.start))
   	         return true;
   	      var key;
   	      if(!keycode.isDelete && !keycode.isBackspace){
   	         key=this.getValidatedKey(keycode, cursorPosition);
   	      } else {
   	         if(cursorPosition.start == cursorPosition.end){
   	            key='_';
   	            if(keycode.isBackspace){
   	               cursorPosition.dec();
   	            }
   	         } else {
   	            key=this.viewMask.substring(cursorPosition.start,cursorPosition.end);
   	         }
   	}
   	      if(key){
   	         this.inputTextElement.value = cursorPosition.previousValue.substring(0,cursorPosition.start)
   	            + key +
   	            cursorPosition.previousValue.substring(cursorPosition.start + key.length,cursorPosition.previousValue.length);
   	         return true;
   	      }
   	      return false;
   	   },

   	   getKeyCode: function(onKeyDownEvent, type) {
   	     var keycode = {};
   	      keycode.unicode = onKeyDownEvent.getKey();
   	      keycode.isShiftPressed = onKeyDownEvent.shiftKey;

   	      keycode.isDelete = ((onKeyDownEvent.getKey() == Ext.EventObject.DELETE && type=='keydown') || ( type=='keypress' && onKeyDownEvent.charCode===0 && onKeyDownEvent.keyCode == Ext.EventObject.DELETE))? true: false;
   	      keycode.isTab = (onKeyDownEvent.getKey() == Ext.EventObject.TAB)? true: false;
   	     keycode.isBackspace = (onKeyDownEvent.getKey() == Ext.EventObject.BACKSPACE)? true: false;
   	      keycode.isLeftOrRightArrow = (onKeyDownEvent.getKey() == Ext.EventObject.LEFT || onKeyDownEvent.getKey() == Ext.EventObject.RIGHT)? true: false;
   	      keycode.pressedKey = String.fromCharCode(keycode.unicode);
   	      return(keycode);
   	   },

   	  CursorPosition: function(start, end, range, previousValue) {
   	      var cursorPosition = {};
   	      cursorPosition.start = isNaN(start)? 0: start;
   	      cursorPosition.end = isNaN(end)? 0: end;
   	      cursorPosition.range = range;
   	     cursorPosition.previousValue = previousValue;
   	      cursorPosition.inc = function(){cursorPosition.start++;cursorPosition.end++;};
   	    cursorPosition.dec = function(){cursorPosition.start--;cursorPosition.end--;};
   	      return(cursorPosition);
   	  }
   	});

    
	//TODO: da spostare in PLAN
	function getCellClass (val, meta, rec, rowIndex, colIndex, store) {
		idx = colIndex - 6; //colonna5 corrisponde a d_1
		 
		if (rec.get('d_' + idx + '_f') == 'F' || rec.get('d_' + idx + '_f') == 'S' || rec.get('d_' + idx + '_f') == 'D'){
			meta.tdCls += ' festivo';
		}
		if (rec.get('d_' + idx + '_t') == '1'){
			meta.tdCls += ' con_carico_1';
		}		
		if (rec.get('d_' + idx + '_t') == '2'){
			meta.tdCls += ' con_carico_2';
		}
		if (rec.get('d_' + idx + '_t') == '3'){
			meta.tdCls += ' con_carico_3';
		}		
		if (rec.get('d_' + idx + '_t') == '4'){
			meta.tdCls += ' con_carico_4';
		}		
		if (rec.get('d_' + idx + '_t') == '5'){
			meta.tdCls += ' con_carico_5';
		}
		if (rec.get('d_' + idx + '_t') == '6'){
			meta.tdCls += ' con_carico_6';
		}				
		
		if ((rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3') && rec.get('d_' + idx).length > 0 && rec.get('d_' + idx + '_d').length > 0)		
			return val += "<span class=c_disp>/" + rec.get('d_' + idx + '_d') + "</span>";
		else
			return val;	
	}

    //PANEL fo_presca
    function show_fo_presca(){
       	   acs_show_win_std('Prenotazione scarichi', 'acs_panel_fo_presca.php?fn=get_open_form', null, 530, 420);	
    }


    function show_scelta_stabilimento(){
   	   acs_show_win_std('Seleziona sede', 'acs_seleziona_stabilimento.php?fn=get_open_form', null, 530, 420, null, 'icon-home-16');        
    }    


    function show_win_dett_art(c_art, rec){
    	var tp = Ext.getCmp('tp-dett-art');
    	if (tp) {
          //refresh dati
    	  tp.loadArt(c_art);
    	} else {
   		  acs_show_win_std('Dettaglio articolo', 'acs_win_dettaglio_articoli.php?fn=open_win', 
		    {c_art : rec.get('codice'), d_art : rec.get('task')}, 700, 600, null, 'icon-leaf-16');        	
    	}
    	
    }


    function show_win_art_critici(ord, type){
		// create and show window
		var win = new Ext.Window({
		  width: 950
		, height: 500
		, minWidth: 600
		, minHeight: 400
		, plain: true
		, title: 'Segnalazione articoli critici'
		, layout: 'fit'
		, border: true
		, closable: true
		, items:  
			new Ext.grid.GridPanel({
				
			        listeners: {
			        	
							  celldblclick: {								
								  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){									  
								  	rec = iView.getRecord(iRowEl);								  	
								  	show_win_disponibilita(rec.get('ditta_origine'), rec.get('RDART'), rec, rec.get('RDTPNO'));
								  }
							  }	  
					},

				
					store: new Ext.data.Store({
						
						listeners: {
						            load: function () {
						                win.setTitle('Segnalazione articoli critici ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO + ' - Disponibilit&agrave; prevista: ' + this.proxy.reader.jsonData.ordine.TDDTDS);
						            }
						         },
						
						groupField: 'RDTPNO',			
						autoLoad:true,				        
	  					proxy: {
								url: 'acs_get_order_rows.php?type=' + type + '&nord=' + ord,
								type: 'ajax',
								reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
		        			fields: [
		            			'ditta_origine', 'TDDTEP', 'RDFMAN', 'RDDTDS', 'RDDFOR', 'RDDTEP', 'RDTPNO', 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDART', 'RDDES1', 'RDDES2', 'RDUM', 'RDDT', {name: 'RDQTA', type: 'float'}
		        			]
		    			}),
		    		features: new Ext.create('Ext.grid.feature.Grouping',{
        							groupHeaderTpl: 'Tipo: {name}',
        							hideGroupedHeader: true
    						}),
    						
					viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('RDFMAN')=='M')
					           		return ' segnala_riga_rosso';
					           if (record.get('RDFMAN')=='C')
					           		return ' segnala_riga_giallo';				           		
					           if (record.get('RDDTDS')==record.get('TDDTEP'))
					           		return ' segnala_riga_viola';					           		
					           return '';																
					         }   
					    },    						
    						
			        columns: [
			             {
			                header   : 'Disp. Sped.',
			                dataIndex: 'RDDTDS', 
			                width     : 70,
			                renderer: date_from_AS
			             },			        
			             {
			                header   : 'Stato',
			                dataIndex: 'RDRIFE', 
			                width     : 95
			             }, {
			                header   : 'Fornitore',
			                dataIndex: 'RDDFOR', 
			                flex    : 50
			             }, {
			                header   : 'Descrizione',
			                dataIndex: 'RDDES1', 
			                flex    : 150,
			                renderer: function(value, p, record){
			                	return record.get('RDDES1') + " [" + record.get('RDART') + "]"
			    			}			                			                
			             }, {
			                header   : 'Um',
			                dataIndex: 'RDUM', 
			                width    : 30
			             }, {
			                header   : 'Q.t&agrave;',
			                dataIndex: 'RDQTA', 
			                width    : 50,
			                align: 'right', renderer: floatRenderer2
			             }, {
			                header   : 'Consegna',
			                dataIndex: 'RDDTEP', 
			                flex    : 30,
			                renderer: date_from_AS
			             }, {
			                header   : 'Documento',
			                dataIndex: 'RDDES2', 
			                flex    : 60
			             }
			            
			         ]})   

		});

		win.show();
}



    
    function show_win_elenco_stampa(grid, rec, node, index, event){	
            	// create and show window
    			print_w = new Ext.Window({
    					  width: 600
    					, height: 400
    					, minWidth: 300
    					, minHeight: 300
    					, plain: true
    					, title: 'Parametri report scarichi'
    					, layout: 'fit'
    					, border: true
    					, closable: true										
    				});	
    			print_w.show();

    				//carico la form dal json ricevuto da php
    				Ext.Ajax.request({
    				        url        : 'acs_get_elenco_stampa_form.php',
    				        method     : 'POST',
    				        waitMsg    : 'Data loading',
    				        jsonData: {rec_id: rec.get('id'), rec_liv: rec.get('liv')}, 
    				        success : function(result, request){
    				            var jsonData = Ext.decode(result.responseText);
    				            print_w.add(jsonData.items);
    				            print_w.doLayout();				            
    				        },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    });		
    } //show_win_elenco_stampa


    function showMenu_liv_0(grid, rec, node, index, event) {
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);	      
	      var voci_menu = [];

	      <?php 
	    	//in base al profilo
	    	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	    	if ($js_parameters->gest_SPED != 0){
	       ?>
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	          grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HANGAR' ||
	          grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'GATE')  
		      voci_menu.push({
	          		text: 'Modifica spedizione',
	        		iconCls : 'iconSpedizione',	          		
	        		handler: function() {
	        			show_win_upd_spedizione(grid, rec, node, index, event);
	        		}
	    		});

	  	<?php } ?>	


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
	    	  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){ 		
		    voci_menu.push({
        		text: 'Report scarichi',
      		iconCls : 'iconPrint',          		
      		handler: function() {
      			show_win_elenco_stampa(grid, rec, node, index, event);
      		}
  		});
	      }

	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD'){	 
	 	      voci_menu.push({
	       		text: 'Report scarichi (hold)',
	     		iconCls : 'iconPrint',      		
	     		handler: function() {
	     			show_win_elenco_stampa_hold(grid, rec, node, index, event);
	     		}
	 		  });
	      }	  
	      
	  		

	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){ 		
	  		    voci_menu.push({
	          		text: 'Report per Sede/Cliente',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {
				
	        			acs_show_win_std('Scelta parametri', 'acs_get_elenco_stampa_form.php', {
		        					from: 'INFO',
		        					desk: 'PVEN',
		        					record: rec.data,
		        					filter: grid.initialConfig.store.proxy.extraParams
		        				}, 400, 250, null, 'icon-print-16'); 	        			
	        		}
	    		},{
	          		text: 'Rapporto registrazione documenti',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {

	        			acs_show_win_std('Conferma report', 'acs_report_info.php?fn=open_form', {
      					from: 'INFO',
      					record: rec.data,
      					filter: grid.initialConfig.store.proxy.extraParams
      				},330, 100, null, 'icon-print-16'); 	   
				
   			
	        		}
	    		});
		      }			      

		  

		 <?php if ($auth->is_operatore_aziendale() == 1){ ?>		 
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY')  
		      voci_menu.push({
	          		text: 'Composizione spedizioni',
	        		iconCls : 'iconPuzzle',	          		
	        		handler: function() {
	        			show_win_progetta_spedizioni(grid, rec, node, index, event);
	        		}
	    		});
		  <?php } ?>


	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY')  
		      voci_menu.push({
	          		text: 'Sincronizza',
	        		iconCls : 'iconRefresh',	          		
	        		handler: function() {

	        	    	id_selected = grid.getSelectionModel().getSelection();	        			
				    	list_selected_id = [];
				    	for (var i=0; i<id_selected.length; i++)
				  		   list_selected_id.push(id_selected[i].data);
			
				  		if (list_selected_id.length > 1){
				  			acs_show_msg_error('Selezionare una singola spedizione');
				  			return false;			
				  		}

	        			
	       			 Ext.Ajax.request({
	      			   url: 'acs_op_exe.php?fn=sincronizza_spedizione',
	      			   method: 'POST',
	      			   jsonData: {sped_id: list_selected_id[0]['sped_id']}, 

	      			   success: function(response, opts) {
	      			   	grid.store.treeStore.load();
	      			   }
	      			});

	        			
	        		}
	    		});



	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
	     		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){
	  		  voci_menu.push({
	        		text: 'Google maps',
	       		iconCls : 'icon-gmaps_logo-16',          		
	       		handler: function() {
	       			sped_id	= rec.get('sped_id');
	       			carico	= rec.get('k_carico');

	       			data = grid.getStore().treeStore.proxy.reader.jsonData.data;
	       			gmapPopup('gmap_sped.php?only_view=Y&data=' + data + '&sped_car=' + sped_id + '|' + carico + '&itin_id=' + grid.getStore().treeStore.proxy.reader.jsonData.cod_iti);
	           		
	       		} //handler google maps
	   		});         
	      }
		      
	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		}).showAt(event.xy);
}


function showMenu_liv_1(grid, rec, node, index, event) {
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);

	      var voci_menu = [];	      

	  <?php 
	  	   	//in base al profilo
	     	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	  	   	if ($js_parameters->gest_CAR != 0){
	   ?>
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && record.data.n_carico > 0)	      
	      voci_menu.push({	      	
    		text: 'Modifica carico',
  		iconCls : 'iconCarico',      		
  		handler: function() {
  			show_win_upd_carico(grid, rec, node, index, event);
  		}
		  });

/*	  <?php if (strlen($js_parameters->ass_SPED) == 0 || $js_parameters->ass_SPED != 0){ ?>	      
		//2014-11-17: (per Ciesse): assegna spedizione anche su carico
		// (per spostare un intero carico in un'altra spedizione
		// - devo aver selezionato un'unica riga
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && record.data.n_carico > 0)		
			  voci_menu.push({
	      		text: 'Assegna prod./dispon. alla spedizione', //DA CARICO
	    		iconCls: 'iconSpedizione',
	    		handler: function() {

	    			id_selected = grid.getSelectionModel().getSelection();		
	    	    	if (id_selected.length > 1){
	    	    		acs_show_msg_error('Selezionare una singola riga carico.');
	    	    		return false;
	    	    	}
	    			
		    		
	    			show_win_assegna_data(grid, rec, node, index, event);
	    		}
			  });
		<?php } ?>*/


		  

	 <?php } ?>	  

   if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' &&
  		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){	 
	      voci_menu.push({
    		text: 'Report scarichi',
  		iconCls : 'iconPrint',      		
  		handler: function() {
  			show_win_elenco_stampa(grid, rec, node, index, event);
  		}
		  });
   }	  
   

   if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' &&
  		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD' ){     
		  voci_menu.push({
       		text: 'Report carico',
      		iconCls : 'iconPrint',          		
      		handler: function() {
      			acs_show_win_std('Report carico', 'acs_report_carico.php?fn=get_json_form', {rec_id: rec.get('id'), rec_liv: rec.get('liv')}, null, null, null, 'iconPrint');          		
      		}
  		});
   }


   //selezione ordini per gestione veloce data evasione programmata
   if ( (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
  		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD') &&
      	parseInt(rec.get('n_carico')) == 0){
	      voci_menu.push({
    		text: 'Seleziona ordini',
  		iconCls : 'icon-leaf-16',      		
  		handler: function() {
  			acs_show_win_std('Selezione ordini', 'acs_seleziona_ordini.php?fn=open_form', {
      				solo_senza_carico: 'Y',
      				grid_id: grid.id,
      				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco,
      				sped_id: rec.get('sped_id'), n_carico: 0, cod_iti: grid.getStore().treeStore.proxy.reader.jsonData.cod_iti}, 
      				1024, 600, {}, 'icon-leaf-16');
  		}
		  });
   }	  
   
   <?php if ($auth->is_operatore_aziendale() == 1){ ?>
   //Report personalizzati el_ordini
   if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
  		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIS' ){     
		  voci_menu.push({
      		text: 'Report Personalizzati',
     		iconCls : 'iconPrint',          		
     		handler: function() {
     			acs_show_win_std('Report personalizzati', '<?php echo ROOT_PATH; ?>personal/dove_report_personalizzati_el_ordini.php?fn=get_json_form', {rec_id: rec.get('id'), rec_liv: rec.get('liv'), rec: rec.data}, null, null, null, 'iconPrint');          		
     		}
 		});
  }
  <?php } ?>
   


   if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
  		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){
		  voci_menu.push({
     		text: 'Google maps',
    		iconCls : 'icon-gmaps_logo-16',          		
    		handler: function() {

    			sped_id	= rec.get('sped_id');
    			carico	= rec.get('k_carico');
    			//recuper la data da id
    			data = rec.get('id').split('|')[2].split('_')[0];
    			gmapPopup('gmap_sped.php?only_view=Y&data=' + data + '&sped_car=' + sped_id + '|' + carico + '&itin_id=' + grid.getStore().treeStore.proxy.reader.jsonData.cod_iti);
        		
    		} //handler google maps
		});         
   }

   


   if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'INFO' && 
  		 grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco != 'HOLD'){
		  voci_menu.push({
     		text: 'Verbale di carico',
    		iconCls : 'icon-pencil-16',          		
    		handler: function() {

    			carico	= rec.get('k_carico');
				carico_exp = carico.split("_")
    			
			    Ext.Msg.confirm('Richiesta conferma', 'Confermi generazione verbale di carico?', function(btn, text){
				    
				      if (btn == 'yes'){

				    	  wait_win = new Ext.Window({
							  width: 400
							, height: 500
							, plain: true
							, title: 'Elaborazione verbale di carico'
							, layout: 'vbox'
							, border: true
							, closable: false
							, modal: true
							, items: [
							   {				
								xtype:'image', height: 400,
								src: <?php echo img_path("elaborazione.gif") ?>
							   }, {
								   flex: 1, width: '100%', align: 'center',
								   html: '<center>Avvio procedura<br/><br/><b>Attendere prego</b></center>',												   
							   }
							]
						});	
						wait_win.show();					      
	
					        Ext.Ajax.request({
					        	url        : '../desk_firma/index.php?fn=open_doc',
					        	jsonData   : {form_values: {
						        					f_chiave: pad(carico_exp[0], 4) + pad(carico_exp[2], 6),
							        				f_user: <?php echo j($auth->get_user()); ?>,
									        		check_disabled: 'Y'								        					
					        					} 
						        },
					            method: 'POST',
					            success: function ( result, request) {
					                jsonData = Ext.decode(result.responseText);
					                if (jsonData.success == true) {
					                    //acs_show_msg_info('Attendere elaborazione Verbale di carico');
					                    wait_win.down('panel').update('<center>Richiesta inviata a gestionale<br/><br/><b>Attendere prego</b></center>');					                    

					                    //chiudo dopo 60 secondi
					                    setTimeout(function(){
						                    wait_win.close();
						                }, 60*1000);
					                    
					                } else {
						                wait_win.close();
										acs_show_msg_error('Errore: ' + jsonData.message);
					                }	
					                    
					            },
					            failure: function ( result, request) {
						            wait_win.close();
					            	acs_show_msg_error('Errore: ' + jsonData.message);
					            }
					        });

					       	
				      } else {
				        //nothing
				      }
				    });
    			
        		
    		} //handler google maps
		});         
   }
   
	      
	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		  }).showAt(event.xy);
}




function showMenu_liv_2(grid, rec, node, index, event) {
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);		  
	      var voci_menu = [];

   		  <?php 
				  //in base al profilo
				  echo "is_operatore_aziendale = {$auth->is_operatore_aziendale()};";				  
			  ?>

	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO')	      
		      voci_menu.push({
	      		text: 'Documenti cliente',
	    		iconCls : 'iconScaricoIntermedio',      		
	    		handler: function() {
		    		console.log(rec.data);
					acs_show_win_std('Elenco documenti cliente', 'acs_documenti_cliente.php?fn=open_list', {
						cliente_selected : rec.get('k_cli_des').split('_')[1]
					}, 1024, 600, {}, 'icon-leaf-16');
	    		}
			  });

	      voci_menu.push({
	      		text: 'Ordini aperti cliente',
	    		iconCls : 'icon-exchange_black-16',      		
	    		handler: function() {
		    		console.log(rec.data);
					acs_show_win_std('Elenco ordini aperti', 'acs_documenti_cliente.php?fn=open_list', {
						cliente_selected : rec.get('k_cli_des').split('_')[1], ord_aperti : 'Y'
					}, 1024, 600, {}, 'icon-leaf-16');
	    		}
			  });
	      
	      
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		}).showAt(event.xy);
}




function showMenu_liv_3(grid, rec, node, index, event) {

	var raggr_voci_menu = {
			'print': {text: 'Stampa documenti', iconCls: 'icon-print-16'},
			'genera': {text: 'Genera ordine collegato', iconCls: 'icon-leaf-16'},
		};	
		
	<?php $js_parameters = $auth->get_module_parameters($main_module->get_cod_mod()); ?>
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);
	      var voci_menu = [];


	      //se ha selezionato un ordine evaso non permetto nessuna operazione
	    	id_selected = grid.getSelectionModel().getSelection();		
	    	num_evasi = 0;
	    	for (var i=0; i<id_selected.length; i++)
	  		   num_evasi += parseFloat(id_selected[i].data.fl_evaso);
	    	
			//VOCI MENU PER ORDINI EVASI -----------------------	    	
	  		if (num_evasi > 0){  

		  <?php if($js_parameters->only_view != 1){ ?>
			add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
	         		text: 'Stampa CONTRATTO',
	        		iconCls : 'icon-print-16',          		
	        		handler: function () {
	        			    list_selected_id = grid.getSelectionModel().getSelection();
			    		  	rec = list_selected_id[0];	 
			    		    if(rec.get('raggr').trim() == 'L')
			    	        	window.open('acs_report_ordini_lavorazione.php?k_ordine='+ rec.get('k_ordine'));
				    	    else 
			    		  	    window.open('acs_report_pdf.php?k_ordine='+ rec.get('k_ordine'));
			    		  
		                }
	    		});
           <?php } ?>
          	add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
	         		text: 'Stampa SCHEDA ordine',
	        		iconCls : 'icon-print-16',          		
	        		handler: function () {
	        			    list_selected_id = grid.getSelectionModel().getSelection();
			    		  	rec = list_selected_id[0];	 
			    		  	window.open('acs_report_pdf.php?tipo_stampa=SCHEDA&k_ordine='+ rec.get('k_ordine'));
		                }
	    		});	 

		      	<?php if($js_parameters->only_view != 1){ ?>
	    		if(rec.get('stato') == 'CC'){
	    	 	add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
	         		text: 'Verbale di verifica',
	        		iconCls : 'icon-print-16',          	 	
	        		handler: function () {
	        			    list_selected_id = grid.getSelectionModel().getSelection();
			    		  	rec = list_selected_id[0];	 
			    		  	window.open('acs_delivery_pos_verbale_verifica.php?k_ordine='+ rec.get('k_ordine'));
		                }
	    		});
			
	    		}
	    		<?php } 
	    		
	    		if($js_parameters->only_view != 1){ ?>
	    		add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
	         		text: 'Stampa FATTURA',
	        		iconCls : 'icon-print-16',          		
	        		handler: function () {
	        			    list_selected_id = grid.getSelectionModel().getSelection();
			    		  	rec = list_selected_id[0];	 
			    		  	 acs_show_win_std('Stampa fattura', 'acs_stampa_fattura.php?fn=open_form', {tipo_doc: 'F', k_ordine: rec.get('k_ordine')}, 400, 400, null, 'icon-print-16');
		                }
	    		});
	    		<?php } 
	    		
	    		if($js_parameters->only_view != 1){ ?>
	    		add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
	         		text: 'Stampa DDT',
	        		iconCls : 'icon-print-16',          		
	        		handler: function () {
	        			    list_selected_id = grid.getSelectionModel().getSelection();
			    		  	rec = list_selected_id[0];	 
			    		  	 acs_show_win_std('Stampa ddt', 'acs_stampa_fattura.php?fn=open_form', {tipo_doc: 'B', k_ordine: rec.get('k_ordine')}, 400, 400, null, 'icon-print-16');
		                }
	    		});
                <?php } 
              
              if($js_parameters->only_view != 1){ ?>
                voci_menu.push({
                    text: 'Righe ordine',
                    iconCls : 'icon-leaf-16',
                    handler: function () {
                        list_selected_id = grid.getSelectionModel().getSelection();
                        rec = list_selected_id[0];
                        acs_show_win_std('Visualizza righe', 'acs_righe_ordine.php?fn=open_grid', {k_ordine: rec.get('k_ordine'), art_manc : rec.get('fl_art_manc')}, 1100, 550, null, 'icon-leaf-16');
                    }
                });

                <?php }
                
			   if($js_parameters->only_view != 1){ ?>

				   	 voci_menu.push({
				      		text: 'Documenti ordine',
				    		iconCls : 'iconScaricoIntermedio',      		
				    		handler: function() {
					    		console.log(rec.data);
								acs_show_win_std('Elenco documenti ordine', 'acs_documenti_cliente.php?fn=open_list', {
									k_ordine: rec.get('k_ordine')
								}, 1100, 600, {}, 'icon-leaf-16');
				    		}
						  });

					  <? }
                
                if($js_parameters->only_view != 1){ ?>

		      	if (rec.get('tipo') == 'MO' || rec.get('tipo') == 'MA' || rec.get('tipo') == 'ME')
		      		add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
			          		text: 'Genera ASSISTENZA',
			        		iconCls : 'iconRefresh',
			        		handler: function () {
								 
	  							  acs_show_win_std('Genera ordine assistenza', 'acs_fattura_accompagnatoria.php?fn=open_form', 
	  									  {k_ordine: rec.get('k_ordine') , ord_ass : 'Y'}, 900, 400, null, 'icon-leaf-16');
	  	
	  			                }	          		
			        		
		    		});


		    	add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
	          		text: 'Genera RESO',
	        		iconCls : 'icon-button_black_play-16',	          		
	           		handler: function () {
						 
						  acs_show_win_std('Genera ordine di reso', 'acs_fattura_accompagnatoria.php?fn=open_form', 
								  {k_ordine: rec.get('k_ordine') , ord_reso : 'Y'}, 650, 400, null, 'icon-leaf-16');

		                }	    
    			});




		      	if (rec.get('caparra') > 0)
		      	  	if(rec.get('tp_cap') == 'G')
			      		add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
	    	          		text: 'Genera RESTITUZIONE GARANZIA',
	    	        		iconCls : 'icon-currency_black_dollar-16',	          		
	    	           		handler: function () {	    						 
	    		           		 acs_show_win_std('Genera restituzione garanzia', 'acs_registra_anticipo.php?fn=open_form', {k_ordine: rec.get('k_ordine'), res_cap : 'Y', garanzia : 'Y', caparra : rec.get('caparra')}, 550, 450, null, 'icon-currency_black_dollar-16');	    
	    		                }	    
	        		    });
	    		

		

        <?php } 
        if($js_parameters->gen_NC == 1){
          if($js_parameters->only_view != 1){ ?>
	      	 if (rec.get('tipo') == 'MO')
		     	 voci_menu.push({
		         		text: 'Genera nota storno anticipo',
		        		iconCls : 'icon-listino',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	acs_show_win_std('Genera nota storno anticipo', 'acs_genera_nota_di_credito.php?fn=open_form', {k_ordine: rec.get('k_ordine')}, 550, 450, null, 'icon-listino');
			                }
		    		});
	    		
	    		<?php } }
	    		
	    		if($js_parameters->only_view != 1){ ?>

	    		voci_menu.push({
	         		text: 'Inserimento nuovo stato/attivit&agrave;',
	        		iconCls : 'icon-arrivi-16',          		
	        		handler: function() {

	        			id_selected = grid.getSelectionModel().getSelection();

	        		  	list_selected_id = [];
	        		  	for (var i=0; i<id_selected.length; i++) 
	        			   list_selected_id.push({k_ordine : id_selected[i].data.k_ordine});

	        		  	my_listeners_inserimento = {
	        					afterInsertRecord: function(from_win){	
	        						from_win.close();
	    			        		}
	    	    				};	

	            		
	    				acs_show_win_std('Nuovo stato/attivit&agrave;', 
	    					<?php echo j('acs_form_json_create_entry.php'); ?>, 
	    					{	//tipo_op: 'ANART',
	    						rif : 'POS',
	    		  				list_selected_id: list_selected_id
	    		  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
	    		  							        		          		
	        		}
	    		});
	    		
	    		
	    		
	    		  voci_menu.push({
	         		text: 'Gestione stato/attivit&agrave; per cliente',
	        		iconCls : 'icon-arrivi-16',          		
	        		handler: function() {
	    				acs_show_panel_std( 
	    					<?php echo j('acs_panel_todolist.php?fn=open_panel'); ?>,
	    					null, {k_ordine: rec.get('k_ordine')} 
	    					);						        		          		
	        		}
	    		});	



	      
	    		
	                <?php } 
	    		
	    		
	    		
	    		
	    		
	    if($js_parameters->only_view != 1){ ?>
	  		 voci_menu.push({
	         		text: 'Altre voci menu <br> bloccate perch&egrave; <br> l\'ordine risulta evaso',
	        		iconCls : 'icon-warning_red-16',          		
	        		/*handler: function() {

		    	          		
	        		}*/
	    		});
				<?php } ?>
			      
	          var menu = new Ext.menu.Menu({
	            items: voci_menu
		     }).showAt(event.xy);	

			     return false;		
	  		}	  //sara ordini evasi      


	  		<?php if($js_parameters->only_view != 1){ ?>
			//VOCI MENU PER ORDINI NON EVASI -----------------------
			
	  		voci_menu.push({
    	         		text: 'Testata ordine',
    	        		iconCls : 'icon-pencil-16',          		
    	        		handler: function () {
    	        			    list_selected_id = grid.getSelectionModel().getSelection();
    			    		  	rec = list_selected_id[0];	
    			    		  	
    			    			my_listeners = {
    			    		  			afterOkSave: function(from_win, new_value){
    		        						rec.set('tipo', new_value.TDOTPD);
    		        						rec.set('riferimento', new_value.TDVSRF);
    		        						rec.set('data_reg', new_value.TDODRE);
    		        						rec.set('priorita', new_value.TDOPRI);
    		        						from_win.close();  
    						        		}
    				    				};	
    			    		  	 
    			    		  	acs_show_win_std('Modifica informazioni di testata', 
    		                			'acs_panel_protocollazione_mod_testata.php?fn=open_form', {
    		                				tddocu: rec.get('k_ordine')
    									}, 800, 500, my_listeners, 'icon-shopping_cart_gray-16');
    		                }
    	    		});	  		
        <?php } ?>


     	           voci_menu.push({
		         		text: 'Righe ordine',
		        		iconCls : 'icon-leaf-16',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
								  acs_show_win_std('Visualizza righe', 'acs_righe_ordine.php?fn=open_grid', {k_ordine: rec.get('k_ordine'), art_manc : rec.get('fl_art_manc')}, 1100, 550, null, 'icon-leaf-16');
			                }
		    		});

			      	<?php 
			      	if($js_parameters->only_view != 1){ ?>

				   	 voci_menu.push({
				      		text: 'Documenti ordine',
				    		iconCls : 'iconScaricoIntermedio',      		
				    		handler: function() {
					    		console.log(rec.data);
								acs_show_win_std('Elenco documenti ordine', 'acs_documenti_cliente.php?fn=open_list', {
									k_ordine: rec.get('k_ordine')
								}, 1100, 600, {}, 'icon-leaf-16');
				    		}
						  });

					  <? }
			      	
			      	
			      	if($js_parameters->only_view != 1){ ?>			      	
			      	add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
		         		text: 'Stampa CONTRATTO',
		        		iconCls : 'icon-print-16',          	 	
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    	        if(rec.get('raggr').trim() == 'L')
				    	        	window.open('acs_report_ordini_lavorazione.php?k_ordine='+ rec.get('k_ordine'));
					    	    else 
				    		  	    window.open('acs_report_pdf.php?k_ordine='+ rec.get('k_ordine'));
			                }
		    		});
                <?php } ?>
                add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
		         		text: 'Stampa SCHEDA ordine',
		        		iconCls : 'icon-print-16',          		
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	window.open('acs_report_pdf.php?tipo_stampa=SCHEDA&k_ordine='+ rec.get('k_ordine'));
			                }
		    		});	

			      	<?php if($js_parameters->only_view != 1){ ?>
			      	if(rec.get('stato') == 'CC'){
			      	add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
		         		text: 'Verbale di verifica',
		        		iconCls : 'icon-print-16',          	 	
		        		handler: function () {
		        			    list_selected_id = grid.getSelectionModel().getSelection();
				    		  	rec = list_selected_id[0];	 
				    		  	window.open('acs_delivery_pos_verbale_verifica.php?k_ordine='+ rec.get('k_ordine'));
			                }
		    		});
			      	}
                    <?php } 
                  if($js_parameters->only_view != 1){ ?>
                  	voci_menu.push({
		         		text: 'Genera fattura accompagnatoria',
		        		iconCls : 'icon-blog_compose-16',          		
						handler: function () {
							console.log(rec);
		        			  list_selected_id = grid.getSelectionModel().getSelection();
			    		  	  rec = list_selected_id[0];



			    				Ext.Ajax.request({
						        	url        : 'acs_registra_anticipo.php?fn=exe_check_cliente',
							        method     : 'POST',
				        			jsonData: {
				        				k_ordine: rec.get('k_ordine')
									},							        
							        success : function(result, request){
								  		  var jsonData = Ext.decode(result.responseText);

			                                if (jsonData.success == false){
			                                    acs_show_msg_error(jsonData.message, 'Dati obbligatori mancanti');
			                                    return;
			                                }else{

			                                	  acs_show_win_std('Fattura accompagnatoria', 'acs_fattura_accompagnatoria.php?fn=open_form', 
			        									  {k_ordine: rec.get('k_ordine')}, 650, 550, {
			        										  'afterSave': function(from_win){
			        											  grid.getStore().treeStore.load();
			        											  from_win.close();											  
			        										  }
			        									  }, 'icon-leaf-16');
				                            }
										  							        
							        },
							        failure    : function(result, request){
							        	Ext.getBody().unmask();
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
				    });	



			    		  	  	 
							
	
			                }
		    		});
            <?php } 
            if($js_parameters->gen_NC == 1){
                if($js_parameters->only_view != 1){ ?>
		      	 if (rec.get('tipo') == 'MO')
		      		 voci_menu.push({
			         		text: 'Genera nota storno anticipo',
			        		iconCls : 'icon-listino',          		
			        		handler: function () {
			        			    list_selected_id = grid.getSelectionModel().getSelection();
					    		  	rec = list_selected_id[0];	 
					    		  	acs_show_win_std('Genera nota storno anticipo', 'acs_genera_nota_di_credito.php?fn=open_form', {k_ordine: rec.get('k_ordine')}, 550, 450, null, 'icon-listino');
				                }
			    		});
                <?php } }
                if($js_parameters->only_view != 1){ ?>
		      	 if (rec.get('tipo') == 'MO')
			     	 voci_menu.push({
			         		text: 'Registra anticipo',
			        		iconCls : 'icon-listino',          		
			        		handler: function () {
			        			    list_selected_id = grid.getSelectionModel().getSelection();
					    		  	rec = list_selected_id[0];	

					    			Ext.Ajax.request({
							        	url        : 'acs_registra_anticipo.php?fn=exe_check_cliente',
								        method     : 'POST',
					        			jsonData: {
					        				k_ordine: rec.get('k_ordine')
										},							        
								        success : function(result, request){
									  		  var jsonData = Ext.decode(result.responseText);

				                                if (jsonData.success == false){
				                                    acs_show_msg_error(jsonData.message, 'Dati obbligatori mancanti');
				                                    return;
				                                }else{

				                                	 acs_show_win_std('Registra anticipo', 'acs_registra_anticipo.php?fn=open_form', {k_ordine: rec.get('k_ordine')}, 550, 450, null, 'icon-listino');
					                            }
											  							        
								        },
								        failure    : function(result, request){
								        	Ext.getBody().unmask();
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
					    });	
     								
					    		  	
				                }
			    		});
		      	 
            <?php } ?>

            
		      	 if (rec.get('nr_fatt') == 0) {
			      	 //se non ho ancora la fattura accompagnatoria...

		      	 }else{
			      		<?php if($js_parameters->only_view != 1){ ?>
					//se ho la fattura/boola accompagnatoria
			      		add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
			         		text: 'Stampa FATTURA',
			        		iconCls : 'icon-print-16',          		
			        		handler: function () {
			        			    list_selected_id = grid.getSelectionModel().getSelection();
					    		  	rec = list_selected_id[0];	 
					    		  	 acs_show_win_std('Stampa fattura', 'acs_stampa_fattura.php?fn=open_form', {k_ordine: rec.get('k_ordine')}, 400, 400, null, 'icon-print-16');
				                }
			    		});
			    		<?php } 
			    		
			    		if($js_parameters->only_view != 1){ ?>
			    		add_in_submenu(voci_menu, raggr_voci_menu, 'print', {
			         		text: 'Stampa DDT',
			        		iconCls : 'icon-print-16',          		
			        		handler: function () {
			        			    list_selected_id = grid.getSelectionModel().getSelection();
					    		  	rec = list_selected_id[0];	 
					    		  	 acs_show_win_std('Stampa bolla', 'acs_stampa_fattura.php?fn=open_form', {tipo_doc: 'B', k_ordine: rec.get('k_ordine')}, 400, 400, null, 'icon-print-16');
				                }
			    		});
			    		
                    <?php } ?>
			      	 }
			<?php if($js_parameters->only_view != 1){ ?>
		      	if (rec.get('tipo') == 'MO' || rec.get('tipo') == 'MA' || rec.get('tipo') == 'ME')
		      		add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
		          		text: 'Genera ASSISTENZA',
		        		iconCls : 'iconRefresh',	          		
		           		handler: function () {
							 
							  acs_show_win_std('Genera ordine assistenza', 'acs_fattura_accompagnatoria.php?fn=open_form', 
									  {k_ordine: rec.get('k_ordine') , ord_ass : 'Y'}, 900, 400, null, 'icon-leaf-16');
	
			                }	    
	    		});

		      	add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
		          		text: 'Genera RESO',
		        		iconCls : 'icon-button_black_play-16',	          		
		           		handler: function () {
							 
							  acs_show_win_std('Genera ordine di reso', 'acs_fattura_accompagnatoria.php?fn=open_form', 
									  {k_ordine: rec.get('k_ordine') , ord_reso : 'Y'}, 650, 400, null, 'icon-leaf-16');
	
			                }	    
	    		});


		      	add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
		          		text: 'Genera COMPLETAMENTO',
		        		iconCls : 'icon-button_blue_play-16',	          		
		           		handler: function () {
							 
							  acs_show_win_std('Genera ordine completamento', 'acs_fattura_accompagnatoria.php?fn=open_form', 
									  {k_ordine: rec.get('k_ordine') , ord_comp : 'Y'}, 650, 400, null, 'icon-leaf-16');
	
			                }	    
	    		});
	    		
		      	if (rec.get('caparra') > 0){

		      	  	if(rec.get('tp_cap') == 'G'){
			      		add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
	    	          		text: 'Genera RESTITUZIONE GARANZIA',
	    	        		iconCls : 'icon-currency_black_dollar-16',	          		
	    	           		handler: function () {
	    						 
	    		           		 acs_show_win_std('Genera restituzione garanzia', 'acs_registra_anticipo.php?fn=open_form', {k_ordine: rec.get('k_ordine'), res_cap : 'Y', garanzia : 'Y', caparra : rec.get('caparra')}, 550, 450, null, 'icon-currency_black_dollar-16');
	    
	    		                }	    
	        		    });

				    }else{

						if(rec.get('TDFN19') != 1){

							add_in_submenu(voci_menu, raggr_voci_menu, 'genera', {
		    	          		text: 'Genera RESTITUZIONE CAPARRA',
		    	        		iconCls : 'icon-currency_black_dollar-16',	          		
		    	           		handler: function () {
		    						 
		    		           		 acs_show_win_std('Genera restituzione caparra', 'acs_registra_anticipo.php?fn=open_form', {k_ordine: rec.get('k_ordine'), res_cap : 'Y', caparra : rec.get('caparra')}, 550, 450, null, 'icon-currency_black_dollar-16');
		    
		    		                }	    
		        		    });


					    }

					}


			   }
		    
<?php } ?>

		      	 

		  <?php 
				  //in base al profilo
				  $js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
				  if ($js_parameters->sped_INFO == 1)
				  	echo "ut_sped_INFO = 1;";
				  else
				  	echo "ut_sped_INFO = 0;";

				  echo "is_operatore_aziendale = {$auth->is_operatore_aziendale()};";				  
			  ?>
	      
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	    	  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD' ||
	    	  (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO' && ut_sped_INFO == 1 && parseFloat(rec.get('n_carico')) == 0))

	    	  <?php if($js_parameters->only_view != 1){ ?>
	 	  	 <?php if (strlen($js_parameters->ass_SPED) == 0 || $js_parameters->ass_SPED != 0){ ?>	  	      
				  voci_menu.push({
		      		text: 'Assegna prod./dispon. alla spedizione', //DA ORDINE
		    		iconCls: 'iconSpedizione',
		    		handler: function() {

				    	  //in INFO faccio gestire un record alla volta,
				    	  // a meno che siano tutti hold e stesso itinerario
				    	  if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){	    	  
						    	id_selected = grid.getSelectionModel().getSelection();

								num_hold = 0;
								num_con_carico = 0;
								list_itinerari = {};
						    	list_selected_id = [];
						    	for (var i=0; i<id_selected.length; i++){

							       if (id_selected[i].get('liv') != 'liv_3'){
							    	   acs_show_msg_error('Selezionare solo righe a livello ordine');
							  			return false;								       
							       }

							       //verifico gli hold
							       num_hold += parseInt(id_selected[i].get('fl_da_prog'))
							       list_itinerari[id_selected[i].get('cod_iti')] = 1;
						  		   list_selected_id.push(id_selected[i].data);

						  		   //verifico quelli con carico
						 		   k_carico_exp = id_selected[i].data.k_carico.split('_');	
								   if (k_carico_exp[2] > 0)
									   num_con_carico += 1;		  		   
						  		   
						    	} //per ogni ordine

								//2014-10-24: non verifico piu' che siano tutti hold, ma che non abbiano il carico
						  		//if (list_selected_id.length > 1 && (num_hold != list_selected_id.length || Object.keys(list_itinerari).length > 1)){
						  		if (list_selected_id.length > 1 && (num_con_carico > 0 || Object.keys(list_itinerari).length > 1)){
						  			acs_show_msg_error('Selezionare un singolo ordine<br>oppure solo ordini dello stesso itinerario senza carico assegnato');
						  			return false;			
						  		}
				    	  } //se INFO		

			    		
		    			show_win_assegna_data(grid, rec, node, index, event);
		    		}
				  });

				  
			<?php } ?>
			<?php } ?>

			  <?php if($js_parameters->only_view != 1){ ?>
			 voci_menu.push({
	         		text: 'Modifica stato', 
	        		iconCls : 'icon-clessidra-16',          	 	
	        		handler: function () {
	        		
	        		my_listeners = {
 		  			afterOkSave: function(from_win){
 		  			console.log(grid);
 						grid.getStore().treeStore.load();
 						from_win.close();  
			        		}
	    				};	
	        		
			         acs_show_win_std('Cambia stato', '../desk_vend/acs_gestione_stato_ordini.php?fn=change_state', {k_ordine: rec.get('k_ordine')}, 400, 220,  my_listeners, 'icon-clessidra-16');	
		           }
	    		});
        <?php } 
        
	   if($js_parameters->only_view != 1){ ?>
			 voci_menu.push({
	         		text: 'Modifica sede', 
	        		iconCls : 'icon-home-16',          	 	
	        		handler: function () {
	        		
	        		my_listeners = {
 		  			afterOkSave: function(from_win){
 		  			console.log(grid);
 						grid.getStore().treeStore.load();
 						from_win.close();  
			        		}
	    				};	
	        		
	        		acs_show_win_std('Seleziona sede', 'acs_seleziona_stabilimento.php?fn=get_open_form', {k_ordine: rec.get('k_ordine')}, 530, 420, my_listeners, 'icon-home-16');        
		           }
	    		});
        <?php } 
        
  if($js_parameters->only_view != 1){ ?>

		voci_menu.push({
     		text: 'Inserimento nuovo stato/attivit&agrave;',
    		iconCls : 'icon-arrivi-16',          		
    		handler: function() {

    			id_selected = grid.getSelectionModel().getSelection();

    		  	list_selected_id = [];
    		  	for (var i=0; i<id_selected.length; i++) 
    			   list_selected_id.push({k_ordine : id_selected[i].data.k_ordine});

    		  	my_listeners_inserimento = {
    					afterInsertRecord: function(from_win){	
    						from_win.close();
			        		}
	    				};	

        		
				acs_show_win_std('Nuovo stato/attivit&agrave;', 
					<?php echo j('acs_form_json_create_entry.php'); ?>, 
					{	//tipo_op: 'ANART',
						rif : 'POS',
		  				list_selected_id: list_selected_id
		  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
		  							        		          		
    		}
		});
		
		
		
		  voci_menu.push({
     		text: 'Gestione stato/attivit&agrave; per cliente',
    		iconCls : 'icon-arrivi-16',          		
    		handler: function() {
				acs_show_panel_std( 
					<?php echo j('acs_panel_todolist.php?fn=open_panel'); ?>,
					null, {k_ordine: rec.get('k_ordine')} 
					);						        		          		
    		}
		});	



  
		
            <?php } 
		
 if($js_parameters->only_view != 1){ ?>

	      if (
	    	      (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && is_operatore_aziendale==1) ||
		    	  (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO' && ut_sped_INFO == 1 && parseFloat(rec.get('n_carico')) == 0))	      
		  voci_menu.push({
    		text: 'Attiva/Disattiva conferma data programmata',
  		iconCls : 'iconConf',            		
  		handler: function() {

		  	id_selected = grid.getSelectionModel().getSelection();

		  	list_selected_id = [];
		  	for (var i=0; i<id_selected.length; i++) 
			   list_selected_id.push(id_selected[i].data.k_ordine);


	    	  //in INFO faccio gestire un record alla volta
	    	  if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO'){	    	  		
			  		if (list_selected_id.length > 1){
			  			acs_show_msg_error('Selezionare un singolo valore');
			  			return false;			
			  		}
	    	  }		


	    	  //non si puo' se ha scelto ordini non omogenei
		    	id_selected = grid.getSelectionModel().getSelection();		
		    	num_con_data = 0;
		    	num_senza_data = 0;
		    	for (var i=0; i<id_selected.length; i++){
		  		   if (parseFloat(id_selected[i].data.cons_conf) > 0)
			  		   num_con_data +=1;
		  		   else
		  			 	num_senza_data += 1;
		    	}

		  		if (num_con_data > 0 && num_senza_data >0){
		  			acs_show_msg_error('Operazione non ammessa. La selezione comprende alcuni ordini con data confermata e alcuni ordini senza data confermata');
		  			return false;			
		  		}	      


		  	//non si puo' se ho un ordine bloccato senza data
		    	id_selected = grid.getSelectionModel().getSelection();		
		    	for (var i=0; i<id_selected.length; i++){
		  		   if (parseFloat(id_selected[i].data.cons_conf) == 0 && parseFloat(id_selected[i].data.fl_bloc) > 1){
		  			 acs_show_msg_error('Operazione non ammessa. Non e\' possibile confermare la data su un ordine bloccato');
		  			 return false;
		  		   }
		    	}



		    //non si puo' se ho un ordine senza spedizione
	    	id_selected = grid.getSelectionModel().getSelection();		
	    	for (var i=0; i<id_selected.length; i++){
	  		   if (id_selected[i].data.sped_id == "" || parseFloat(id_selected[i].data.sped_id) == 0){
	  			  acs_show_msg_error('Operazione non ammessa. Non e\' possibile confermare la data su un ordine senza numero di spedizione');
	  			  return false;
	  		   }
	    	}		    	

	    	//devo aver selezionato solo righe a livello ordine	    	
	    	id_selected = grid.getSelectionModel().getSelection();		
	    	for (var i=0; i<id_selected.length; i++){
			  if (id_selected[i].data.liv != 'liv_3') {
				  acs_show_msg_error('Selezionare solo righe a livello ordine');
				  return false;
			  }
	    	}			
	    	
		  	
			 Ext.Ajax.request({
			   url: 'acs_op_exe.php?fn=toggle_data_confermata',
			   method: 'POST',
			   jsonData: {list_selected_id: list_selected_id}, 

			   success: function(response, opts) {
				   				node_expanded = [];				   
		  						for (var i=0; i<id_selected.length; i++){
			  						node_liv1 = id_selected[i].parentNode; //.parentNode;

			  						if (node_liv1 != null)
			  						if (Ext.Array.contains(node_expanded, node_liv1.id) == false){
		                                node_liv1.collapse();
		                                node_liv1.data.loaded = false; //forzo aggiornamento
		                                node_liv1.expand();
		                                node_expanded = node_liv1.id; 			  						
			  						}    
	                                
		  						} 
			   	
			   }
			});

  		}
		});
		  
<?php } ?>

	      if (parseFloat(rec.get('data_conf_ord')) > 0 ||
	    	      rec.get('fl_art_manc') == 2 ||
	    	      rec.get('fl_art_manc') == 3 ||
	    	      rec.get('fl_art_manc') == 4 ||
	    	      rec.get('fl_art_manc') == 20 ||
	    	      (parseInt(rec.get('sped_id')) > 0 && parseInt(rec.get('cons_prog_stadio')) == 1) //se in data ICE, non HOLD 
	    	      )  //grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY'


		    <?php if($js_parameters->only_view != 1){ ?>
	      if (parseFloat(rec.get('data_conf_ord')) > 0 && is_operatore_aziendale==1)  //grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY'
			  voci_menu.push({
		      		text: 'Sollecito riprogrammazione',
		    		iconCls: 'iconArtCritici',
		    		handler: function() {

		    		  	id_selected = grid.getSelectionModel().getSelection();

		    		  	list_selected_id = [];
		    		  	for (var i=0; i<id_selected.length; i++) 
		    			   list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});	    		
	
		    			//apro form per richiesta parametri
						var mw = new Ext.Window({
						  width: 800
						, height: 380
						, minWidth: 300
						, minHeight: 300
						, plain: true
						, title: 'Sollecito riprogrammazione'
						, iconCls: 'iconArtCritici'			
						, layout: 'fit'
						, border: true
						, closable: true								
						});				    			
		    			mw.show();
		    			
						//carico la form dal json ricevuto da php
						Ext.Ajax.request({
						        url        : 'acs_form_json_create_entry.php',
						        jsonData: {tipo_op: 'RIPRO', list_selected_id: list_selected_id},
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						            var jsonData = Ext.decode(result.responseText);
						            mw.add(jsonData.items);
						            mw.doLayout();				            
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });				    			
						    	    							    			
		    		}
				  });
	      	
<?php } 
if($js_parameters->only_view != 1){ ?>
	      if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' && is_operatore_aziendale==1)	      
		      voci_menu.push({
	      		text: 'Assegna destinazione cliente',
	    		iconCls : 'iconDestinazioneFinale',      		
	    		handler: function() {
	    			show_win_assegna_destinazione_finale(grid, rec, node, index, event);
	    		}
			  });		  
			<?php } 
			
	if($js_parameters->only_view != 1){ ?>
		  id_selected = grid.getSelectionModel().getSelection();	      
		  if (id_selected.length == 1)  		  
	      if (parseFloat(rec.get('data_conf_ord')) > 0  && parseFloat(rec.get('fl_evaso')) == 0 && is_operatore_aziendale==1)
			  voci_menu.push({
		      		text: 'Forza invio conferma d\'ordine',
		    		iconCls: 'icon-print-16',
		    		handler: function() {

			   			 Ext.Ajax.request({
				  			   url: 'acs_op_exe.php?fn=forza_invio_conferma_ordine',
				  			   method: 'POST',
				  			   jsonData: {list_selected_id: id_selected[0].data.k_ordine}, 
		
				  			   success: function(response, opts) {
				 	  			  acs_show_msg_info('Richiesta forzatura invio');				  			   	
				  			   }
				  			});	

			    		
		    		} //hanler function
			  });
	      <?php } ?>


	      <?php if($js_parameters->only_view != 1){ ?>
	      //Report personalizzati el_ordini
	      if (is_operatore_aziendale==1 && (
	    	      grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY' ||
	    		  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIST')
		      ){     
	  		  voci_menu.push({
	         		text: 'Report Personalizzati',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {
	        			acs_show_win_std('Report personalizzati', '<?php echo ROOT_PATH; ?>personal/dove_report_personalizzati_el_ordini.php?fn=get_json_form', {rec_id: rec.get('id'), rec_liv: rec.get('liv'), rec: rec.data}, null, null, null, 'iconPrint');          		
	        		}
	    		});
	      }     
	      <?php } ?>

	      voci_menu.push({
	    		text: 'Sincronizza',
	  		iconCls : 'iconRefresh',	          		
	  		handler: function() {

	  			
	  			 Ext.Ajax.request({
	  			   url: 'acs_op_exe.php?fn=sincronizza_ordine',
	  			   method: 'POST',
	  			   jsonData: {k_ordine: rec.get('k_ordine')}, 

	  			   success: function(response, opts) {
	  			   			grid.store.treeStore.load();
	  			   }
	  			});

	  			
	  		}
	  		});

		  	id_selected = grid.getSelectionModel().getSelection();

		  	list_selected_id = [];
		  	for (var i=0; i<id_selected.length; i++) 
			   list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});	
			   
		  	voci_menu.push({
	     		text: 'Evidenza generica',
	    		iconCls : 'icon-sub_blue_accept-16', 
	    		menu:{
	        	items:[
	                {  text: 'Rosso',
	                   iconCls : 'icon-sub_blue_accept-16',
	                   style: 'background-color: #F9BFC1;', 
	                   handler : function(){
	                     Ext.Ajax.request({
					        url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : '1'
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
					         var ar_value = jsonData.or_flag;
					         	for (var i=0; i<id_selected.length; i++) 
					        	 	for (var chiave in ar_value) {
						        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
											id_selected[i].set('tdfg06', ar_value[chiave].flag);
						        	 }
					         }
							 },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                },
	            	{  text: 'Giallo',
	            	   iconCls : 'icon-sub_blue_accept-16', 
	            	   style: 'background-color: #F4E287;',
	            	        handler : function(){
	                     Ext.Ajax.request({
	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : '2'
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
						         var ar_value = jsonData.or_flag;
						         	for (var i=0; i<id_selected.length; i++) 
						        	 	for (var chiave in ar_value) {
							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
												id_selected[i].set('tdfg06', ar_value[chiave].flag);
							        	 }
						         }
					         
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                },{
	                    text: 'Verde',
	                    iconCls : 'icon-sub_blue_accept-16', 
	                    style: 'background-color: #A0DB8E;',
	                         handler : function(){
	                     Ext.Ajax.request({
	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : '3'
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
						         var ar_value = jsonData.or_flag;
						         	for (var i=0; i<id_selected.length; i++) 
						        	 	for (var chiave in ar_value) {
							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
												id_selected[i].set('tdfg06', ar_value[chiave].flag);
							        	 }
						         }
					         
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                },{
	                	text: 'Nessuno',
	                	iconCls : 'icon-sub_blue_accept-16', 
	                	     handler : function(){
	                     Ext.Ajax.request({
	                    	 url        : 'acs_op_exe.php?fn=evidenza_generica',
					        timeout: 2400000,
					        method     : 'POST',
		        			jsonData: {
		        				list_selected_id: list_selected_id,
		        				flag : ''
							},							        
					        success : function(result, request){
					         var jsonData = Ext.decode(result.responseText);
					         if(jsonData.success == true){
						         var ar_value = jsonData.or_flag;
						         	for (var i=0; i<id_selected.length; i++) 
						        	 	for (var chiave in ar_value) {
							        	  if(id_selected[i].get('k_ordine') == ar_value[chiave].k_ordine)
												id_selected[i].set('tdfg06', ar_value[chiave].flag);
							        	 }
						         }
					         
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });
	                   }
	                }
	            ]
	        }        		
	    	
			});
			
  
	      var menu = new Ext.menu.Menu({
	            items: voci_menu
		}).showAt(event.xy);
}
    
    
function show_win_assegna_data(grid, rec, node, index, event, confermaData){

	if(typeof(confermaData)==='undefined') confermaData = '';
	
  	id_selected = grid.getSelectionModel().getSelection();

  	list_selected_id = [];
  	for (var i=0; i<id_selected.length; i++){
		  //devo prendere solo livelli ordine o cliente
		  if (id_selected[i].data.liv != 'liv_2' && id_selected[i].data.liv != 'liv_3' && 
				  ( id_selected[i].data.liv == 'liv_1' && id_selected.length > 1 )
			  ) {
			  acs_show_msg_error('Selezionare solo righe a livello ordine o cliente o una singola riga carico');
			  return false;
		  }

		record_data = id_selected[i].data;
		record_data['riferimento'] = ''; //lo pulisco perche' se c'e' una \ va non e' piu' un json valido  
	 	//list_selected_id.push(id_selected[i].data);
		list_selected_id.push(record_data);		  
  	}


<?php if ($cfg_mod_Spedizioni['ASS_SPED_disabilita_verifica_colli_spuntati'] != 'Y'){ ?>  	
  	//impedisco se ha colli spuntati
  	for (var i=0; i<id_selected.length; i++){
		  //devo prendere solo livelli ordine o cliente
		  if (parseInt(id_selected[i].data.colli_sped) > 0) {
			  acs_show_msg_error('Operazione non ammessa con colli spuntati');
			  return false;
		  } 	  	 
	}
<? } ?>	
  	


	  //in INFO e HOLD non si puo' assegnare spedizione a clienti/ordini bloccati o evasi
	  if (grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'INFO' ||
			  grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'HOLD'){	    	  
	    	id_selected = grid.getSelectionModel().getSelection();

	    	
	    	
	    	for (var i=0; i<id_selected.length; i++)
	  		if (
	  		  		//se non sono un amministratore di modulo segnalo anomalia su ordine bloccato
	  		  		<?php if ($js_parameters->p_mod_param != 'Y'){ ?>
	  		  			parseFloat(id_selected[i].get('fl_bloc')) > 1 ||
	  		  		<?php } ?>
	  		  		 
	  		  		parseFloat(id_selected[i].get('fl_evaso')) > 0  ||
	  				parseFloat(id_selected[i].get('fl_cli_bloc')) > 0
	  				//id_selected[i].parentNode.get('iconCls') == 'iconBlocco'
		  				){
	  					acs_show_msg_error('Operazione non ammessa su ordini/clienti bloccati o evasi');
	  			  		return false;			
	  		}
	  }
	
		// create and show window
		print_w = new Ext.Window({
		  width: 1050
		, height: 400
		, minWidth: 500
		, minHeight: 300
		, plain: true
		, title: 'Assegna produzione/disponibilit&agrave; alla spedizione'
    	, iconCls: 'iconFabbrica'		
		, layout: 'fit'
		, border: true
		, closable: true  
		});
		print_w.show();

		//carico la form dal json ricevuto da php
		Ext.Ajax.request({
		        url        : 'acs_form_json_assegna_spedizione.php',
		        method     : 'POST',
		        waitMsg    : 'Data loading',
		        jsonData: {list_selected_id: list_selected_id, cod_iti: grid.initialConfig.grid.cod_iti, grid_id: grid.id, confermaData: confermaData}, 
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            print_w.add(jsonData.items);
		            print_w.doLayout();				            
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		});		
}


    function show_win_righe_ord(ord, k_ord){
 	   acs_show_win_std('Righe ordine', <?php echo acs_url('module/desk_acq/acs_get_order_rows.php') ?>, {k_ord: k_ord, dtep: ord.get('cons_prog')}, 1150, 480);        
    }


    function show_win_bl_articolo(c_art){	
    	// create and show window
    	print_w = new Ext.Window({
    			  width: 400
    			, height: 400
    			, plain: false
    			, title: 'Blocco note articolo ' +c_art
    			, layout: 'fit'
    			, border: true
    			, closable: true
    			, maximizable: false										
    		});	
    	print_w.show();

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : '../desk_utility/acs_anag_art_note.php?fn=open_bl&c_art=' + c_art,
    		        method     : 'POST',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            print_w.add(jsonData.items);
    		            print_w.doLayout();				            
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });		
    } //show_win_annotazioni_articolo


    // show/refresh win dettaglio articoli
    function show_win_dett_art(c_art, rec){
    	var tp = Ext.getCmp('tp-dett-art');
    	if (tp) {
          //refresh dati
    	  tp.loadArt(c_art);
    	} else {
   		  acs_show_win_std('Dettaglio articolo', 'acs_win_dettaglio_articoli.php?fn=open_win', 
		    {c_art : rec.get('codice'), d_art : rec.get('task'), from_pv : 'Y'}, 700, 600, null, 'icon-leaf-16');        	
    	}
    	
    }


    function show_win_crt_promemoria(){

    var f = Ext.create('Ext.form.Panel', {
           frame:true,
           title: '',
           bodyStyle:'padding:5px 5px 0',
           width: 600,
           url: 'acs_op_exe.php',
           fieldDefaults: {
               labelAlign: 'top',
               msgTarget: 'side'
           },

           items: [{
           	xtype: 'hidden',
           	name: 'fn',
           	value: 'crt_promemoria'
           	}, {
                   xtype: 'combo',
                   anchor: '100%',                
                   store: Ext.create('Ext.data.ArrayStore', {
                       fields: [ 'email', 'descr' ],
                       data: <?php echo $ar_email_json ?>
                   }),
                   displayField: 'descr',
                   valueField: 'email',
                   value: '<?php global $auth; echo $auth->get_email(); ?>',
                   fieldLabel: 'A',
                   queryMode: 'local',
                   selectOnTab: false,
                   allowBlank: false,
                   name: 'to'
               },
               {
               xtype: 'container',
               anchor: '100%',
               layout:'column',
               items:[{
                   xtype: 'container',
                   columnWidth:.5,
                   layout: 'anchor',
                   items: [{
                       xtype:'datefield'
   					   , startDay: 1 //lun.
   					   , fieldLabel: 'Data'
   					   , name: 'f_data'
   					   , format: 'd/m/Y'
   					   , submitFormat: 'Ymd'
   					   , allowBlank: false
                          , anchor:'96%'
                          , minValue: new Date()
                   }]
               },{
                   xtype: 'container',
                   columnWidth:.5,
                   layout: 'anchor',
                   items: [{
                       xtype:'timefield'
   					   , fieldLabel: 'Ora'
   					   , name: 'f_ora'
   					   , format: 'H:i'						   
   					   , submitFormat: 'Hi'
   					   , allowBlank: false
                       , anchor:'96%'
                   }]
               }]
           }, {
               xtype: 'textfield',
               name: 'f_oggetto',
               fieldLabel: 'Oggetto',
               anchor: '96%',
   		    allowBlank: false
           }
           , {
               //xtype: 'htmleditor',
               xtype: 'textareafield',
               name: 'f_text',
               fieldLabel: 'Testo del messaggio',
               height: 200,
               anchor: '96%',
   		    allowBlank: false            
           }],

           buttons: [{
               text: 'Conferma',
   	            handler: function() {
   	            	var form = this.up('form').getForm();
   	            	var m_win = this.up('window');	            	
   	                form.submit(
   								{
   		                            //waitMsg:'Loading...',
   		                            success: function(form,action) {
   										m_win.close();		                            	
   		                            },
   		                            failure: function(form,action){
   		                            }
   		                        }	                	
   	                );
   		            //this.up('window').close();            	                	                
   	             }            
           		},{
               text: 'Annulla'
           }]
       });


   		// create and show window
   		var win = new Ext.Window({
   		  width: 800
   		, height: 380
   		, minWidth: 300
   		, minHeight: 300
   		, plain: true
   		, title: 'Registra promemoria via email con data/ora invio'
   		, iconCls: 'icon-clock-16'			
   		, layout: 'fit'
   		, border: true
   		, closable: true
   		, items: f
   		});
   		win.show();  

   }
    



	function show_win_annotazioni_docu_for(k_ordine){	
		// create and show window
		print_w = new Ext.Window({
				  width: 800
				, height: 400
				, minWidth: 300
				, minHeight: 400
				, plain: false
				, title: 'Annotazioni documenti fornitore'
				, layout: 'fit'
				, border: true
				, closable: true
				, maximizable: false										
			});	
		print_w.show();

			//carico la form dal json ricevuto da php
			Ext.Ajax.request({
			        url        : '../desk_acq/acs_form_json_annotazioni_ordine.php?fn=open_bl&k_ordine=' + k_ordine,
			        method     : 'POST',
			        waitMsg    : 'Data loading',
			        success : function(result, request){
			            var jsonData = Ext.decode(result.responseText);
			            print_w.add(jsonData.items);
			            print_w.doLayout();				            
			        },
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });		
	} 
    
    function show_win_annotazioni_ordine_bl(k_ordine, bl){	
    	// create and show window
    	print_w = new Ext.Window({
  		  width: 800
			, height: 500
			, minWidth: 400
			, minHeight: 400
			, plain: false
			, title: 'Annotazioni ordine'
			, layout: 'fit'
			, border: true
			, closable: true
			, maximizable: false											
    		});	
    	print_w.show();

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_form_json_annotazioni_ordine.php?k_ordine=' + k_ordine + '&num_ann=15', // + '&bl=' + bl,
    		        method     : 'POST',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            print_w.add(jsonData.items);
    		            print_w.doLayout();				            
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });		
    } //show_win_annotazioni_ordine


    function show_win_annotazioni_ordine(k_ordine, liv1, liv2){	
    	// create and show window
		print_w = new Ext.Window({
				  width: 800
				, height: 500
				, minWidth: 400
				, minHeight: 400
				, plain: false
				, title: 'Annotazioni ordine'
				, layout: 'fit'
				, border: true
				, closable: true
				, maximizable: false	
										
			});	
		print_w.show();

			//carico la form dal json ricevuto da php
			Ext.Ajax.request({
			        url        : 'acs_form_json_annotazioni_ordine.php?k_ordine=' + k_ordine + '&liv1=' + liv1 + '&liv2=' + liv2 + '&num_ann=15',
			        method     : 'POST',
			        waitMsg    : 'Data loading',
			        success : function(result, request){
			            var jsonData = Ext.decode(result.responseText);
			            print_w.add(jsonData.items);
			            print_w.doLayout();				            
			        },
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });		
	} //show_win_annotazioni_ordine    





    function show_win_entry_ordine(k_ordine, liv1, liv2){	

    	var win = new Ext.Window({
    		  width: 900
    		, height: 500
    		, minWidth: 600
    		, minHeight: 400
    		, plain: true
    		, title: 'Riepilogo attivit&agrave; acquisizione/avanzamento ordine aperte'
    		, layout: 'fit'
    		, border: true
    		, closable: true
    		, items:  
    			new Ext.grid.GridPanel({
    				
    					store: new Ext.data.Store({
    						
    						listeners: {
    						            load: function () {
    						                win.setTitle('Riepilogo attivit&agrave; acquisizione/avanzamento ordine aperte ' + this.proxy.reader.jsonData.ordine.TDOADO + "_" +  this.proxy.reader.jsonData.ordine.TDONDO);
    						            }
    						         },
    						
    						groupField: 'RDTPNO',			
    						autoLoad:true,				        
    	  					proxy: {
    								url: 'acs_get_order_entry.php?nord=' + k_ordine,
    								type: 'ajax',
    								reader: {
    						            type: 'json',
    						            root: 'root'
    						        }, 
    						        extraParams: {}
    							},
    		        			fields: [
    		            			'ASCAAS', 'ASUSAS', 'ASDTAS', 'ASHMAS', 'ASNOTE', 'ASUSAT', 'ASDTVI', 'ASDTSC', 'ASCAAS_OUT', 'ASDTRI', 'ASHMRI', 'ASCARI', 'ASCARI_OUT', 'ASNORI'
    		        			]
    		    			}),


    		    	        tbar: new Ext.Toolbar({
    		    	            items:[
    							{
    								name: 'f_anche_rilasciate',
    								xtype: 'checkboxgroup',
    								fieldLabel: 'Anche rilasciate',
    								labelAlign: 'right',
    							   	allowBlank: true,
    							   	items: [{								   	
    							            xtype: 'checkbox'
    								      , width: 250
    								      , labelWidth: 170
    							          , name: 'f_anche_rilasciate' 
    							          , boxLabel: ''
    							          , inputValue: 'Y'		                          
    							        }],
    							    listeners: {
    							    	change: function(checkbox, checked){
    										   m_grid = this.up('window').down('grid');
    										   m_grid.store.proxy.extraParams.anche_rilasciate = checked
    										   m_grid.store.load();			            						            			 	            					            					                    		
    							    	}
    							    }    														
    							}				    	            
    		    	         ]            
    		    	        }),		    			
      						
    			        columns: [
    			             {
    			                header   : 'Causale',
    			                dataIndex: 'ASCAAS_OUT', 
    			                flex: 1, 
    			                renderer: function(value, p, record){
    				                if (record.get('ASDTRI') > 0){
    					                //info rilascio
    					                value += '<BR> -> ' + record.get('ASCARI_OUT');
    				                }
    				                return value;
    			                }
    			             }, {
    				            header   : 'Assegnata da',
    				            dataIndex: 'ASUSAS', 
    				            flex: 1
    				         }, {
    					            header   : 'Assegnata il',
    					            dataIndex: 'ASDTAS', 
    					            flex: 1,
    					            renderer: function(value, p, record){
    						            value = datetime_from_AS(record.get('ASDTAS'), record.get('ASHMAS'));

    						            if (record.get('ASDTRI') > 0){
    						                //info rilascio
    						                value += '<BR>' + datetime_from_AS(record.get('ASDTRI'), record.get('ASHMRI'));
    					                }
    						            
    					            	return value;
    					            }					            
    					     }, {
    					            header   : 'Note',
    					            dataIndex: 'ASNOTE', 
    					            flex: 1,
    					            renderer: function(value, p, record){
    						            if (record.get('ASDTRI') > 0){
    						                //info rilascio
    						                value += '<BR>' + record.get('ASNORI');
    					                }
    						            
    					            	return value;
    					            }				
    					            
    					     }, {
    					            header   : 'Assegnata a',
    					            dataIndex: 'ASUSAT', 
    					            flex: 1
    					     }, {
    					            header   : 'Notificata dal',
    					            dataIndex: 'ASDTVI', 
    					            flex: 1,
    					            renderer: date_from_AS
    					     }, {
    					            header   : 'Scadenza',
    					            dataIndex: 'ASDTSC', 
    					            flex: 1,
    					            renderer: date_from_AS
    					            
    					     }
    			            
    			         ]})   

    		});
    	win.show();
    	
    } //show_win_entry_ordine
    
	


    

	 Ext.apply(Ext.form.field.VTypes,{
			password : function(val, field) {
			    
			    if (field.initialPassField) {
			        var pwd = Ext.getCmp(field.initialPassField);
			        
			        return (val == pwd.getValue());
			    }
			    return true;
			},
					
			passwordText : 'Passwords non coincidenti'
			});


    Ext.onReady(function() {

        Ext.QuickTips.init();






    Extensible.calendar.data.EventMappings = {
        // These are the same fields as defined in the standard EventRecord object but the
        // names and mappings have all been customized. Note that the name of each field
        // definition object (e.g., 'EventId') should NOT be changed for the default fields
        // as it is the key used to access the field data programmatically.
        EventId:     {name: 'EventId', mapping:'id', type:'int'}, // int by default
        CalendarId:  {name: 'CalendarId', mapping: 'cid', type: 'string'}, // int by default
        Title:       {name: 'Title', mapping: 'title'},
        StartDate:   {name: 'StartDate', mapping: 'start', type: 'date', dateFormat: 'c'},
        EndDate:     {name: 'EndDate', mapping: 'end', type: 'date', dateFormat: 'c'},
        RRule:       {name: 'RecurRule', mapping: 'rrule'},
        Location:    {name: 'Location', mapping: 'loc'},
        Notes:       {name: 'Notes', mapping: 'notes'},
        Url:         {name: 'Url', mapping: 'url'},
        IsAllDay:    {name: 'IsAllDay', mapping: 'ad', type: 'boolean'},
        Reminder:    {name: 'Reminder', mapping: 'rem'},        


        //Acs
        Risorsa: 	  {name:'Risorsa', mapping: 'risorsa', type: 'string'},
        Stabilimento: {name:'Stabilimento', mapping: 'stabilimento', type: 'string'},
        Trasp:		  {name:'Trasp', mapping: 'trasp', type: 'string'},
        Porta:    	  {name:'Porta', mapping: 'porta'},        
        
        
        // We can also add some new fields that do not exist in the standard EventRecord:
        CreatedBy:   {name: 'CreatedBy', mapping: 'created_by'},
        IsPrivate:   {name: 'Private', mapping:'private', type:'boolean'}
        
    };
    // Don't forget to reconfigure!
    Extensible.calendar.data.EventModel.reconfigure();






 /* PER DRAG & DROP **********/
    /*
     * Currently the calendar drop zone expects certain drag data to be present, so if dragging
     * from the grid, default the data as required for it to work
     */
    var nodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag1';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
        }
    };
    
    /*
     * Need to hook into the drop to provide a custom mapping between the existing grid
     * record being dropped and the new calendar record being added
     */
    var nodeDropInterceptor = function(n, dd, e, data){
        if(n && data){
            if(typeof(data.type) === 'undefined'){
                var rec = Ext.create('Extensible.calendar.data.EventModel'),
                    M = Extensible.calendar.data.EventMappings;
                
                // These are the fields passed from the grid's record
                //var gridData = data.selections[0].data;
                var gridData = data.records[0].data
                rec.data[M.Title.name] = 'salvataggio...';
                rec.data[M.CalendarId.name] = gridData.cid;
                
                // You need to provide whatever default date range logic might apply here:
                rec.data[M.StartDate.name] = n.date;
                rec.data[M.EndDate.name] = Extensible.Date.add(n.date, { hours: 1 });;
                rec.data[M.IsAllDay.name] = false;

                rec.data[M.CreatedBy.name] = gridData.TDNBOC;  //lo appoggio qui altrimenti non mi esegui il create               
                rec.data[M.Stabilimento.name] = gridData.TDSTAB;
                rec.data[M.Porta.name] = this.view.store.proxy.extraParams.f_porta;
                
                // save the evrnt and clean up the view
                ///////this.view.onEventDrop(rec, n.date);
                this.view.store.add(rec.data);                                                
                this.onCalendarDragComplete();
				this.view.fireEvent('eventadd');
                return false; //per non eseguire onDrop Normale
            }
        }
    };
    
    /*
     * Day/Week view require special logic when dragging over to create a 
     * drag shim sized to the event being dragged
     */
    var dayViewNodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag2';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
    
            var dayCol = e.getTarget('.ext-cal-day-col', 5, true);
            if (dayCol) {
                var box = {
                    height: Extensible.calendar.view.Day.prototype.hourHeight,
                    width: dayCol.getWidth(),
                    y: n.timeBox.y,
                    x: n.el.getLeft()
                }
                this.shim(n.date, box);
            }
        }
    };
    
    // Apply the interceptor functions to each class:
    var dropZoneProto = Extensible.calendar.dd.DropZone.prototype,
        dayDropZoneProto = Extensible.calendar.dd.DayDropZone.prototype 
    
    dropZoneProto.onNodeOver = Ext.Function.createInterceptor(dropZoneProto.onNodeOver, nodeOverInterceptor);
    dropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dropZoneProto.onNodeDrop, nodeDropInterceptor);
    
    dayDropZoneProto.onNodeOver = Ext.Function.createInterceptor(dayDropZoneProto.onNodeOver, dayViewNodeOverInterceptor);
    dayDropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dayDropZoneProto.onNodeDrop, nodeDropInterceptor);



    
    
    /*
     * This is a simple override required for dropping from outside the calendar. By default it
     * assumes that any drag originated within itelf, so a drag would be a move of an existing event.
     * This is no longer the case and it must check to see what the record state is.
     */
    Extensible.calendar.view.AbstractCalendar.override({
        onEventDrop : function(rec, dt){
            if (rec.phantom) {
                this.onEventAdd(null, rec);
            }
            else {
                this.moveEvent(rec, dt);
            }
        }
    });    	


	//disabilito il menu sugli eventi
	Extensible.calendar.menu.Event.override({
		showForEvent: function(rec, el, xy){
				return false;
		}
	});
    

	//formato data negli header
	Extensible.calendar.template.BoxLayout.prototype.singleDayDateFormat = 'd / m';	

    
    //PER RIMUOVERE L'ORARIO NEGLI EVENTI
    Extensible.calendar.view.DayBody.override({
    getTemplateEventData : function(evt){
        var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            colorCls = 'x-cal-default',
            title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ',
            recurring = evt[M.RRule.name] != '';
        
        this.getTemplateEventBox(evt);
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, 
                    evt[M.CalendarId.name]);
                
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '') + (Ext.isIE || Ext.isOpera ? '-x' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
        data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        data.Title =  (!title || title.length == 0 ? this.defaultEventTitleText : title);
        return Ext.applyIf(data, evt);
    }    
    });
    
  






    //COMBO: possibilita di rimuovere valore selezionato (con forceSelection lasciava sempre l'ultimo)
    Ext.form.field.ComboBox.override({
     beforeBlur: function(){
         var value = this.getRawValue();
         if(value == ''){
             this.lastSelection = [];
         }
         this.doQueryTask.cancel();
         this.assertValue();
     }
    });
    




            

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            // create instance immediately
            Ext.create('Ext.Component', {
                region: 'north',
                id: 'page-header',
                height: 110, // give north and south regions a height
                contentEl: 'header'
            }),{
                xtype: 'tabpanel',
                region: 'east',
                title: 'Dettagli ordine',
                id: 'OrdPropertiesTab',
                animCollapse: true,
                collapsible: true,
                collapsed: true,
                split: true,
                width: 295, // give east and west regions a width
                minSize: 175,
                maxSize: 400,
                margins: '0 5 0 0',
                activeTab: 0,
                tabPosition: 'bottom',

        		imposta_title: function(tddocu){
        				this.setTitle(tddocu);
        			},
                
                items: [Ext.create('Ext.grid.PropertyGrid', {
                        title: 'Riferimenti',
                        id: 'OrdPropertyGrid',
                        closable: false,
                        source: {},
                        disableSelection: true,
                        listeners: {
    					    'beforeedit': {
            					fn: function () {
                					return false;
            						}
        						}
    					}
                    }),

                    new Ext.grid.GridPanel({
                        title: 'Cronologia',
                        id: 'OrdPropertyCronologia',
                  		 store: new Ext.data.Store({
                  									
                  				autoLoad: false,				        
                  	  			proxy: {
                  							url: 'acs_get_order_cronologia.php',
                  							type: 'ajax',
                  							reader: {
                  						      type: 'json',
                  						      root: 'root'
                  						     },
                  						     extraParams: {
                  		    		    		k_ordine: ''
                  		    				}               						        
   				        
                  						},
                  		        fields: ['data', 'utente', 'attivita', 'stato_data'],
                  		     	groupField: 'data',               		     	
                  			}),
       		    	features: new Ext.create('Ext.grid.feature.Grouping',{
   						groupHeaderTpl: 'Data: {[date_from_AS(values.name)]}',
   						hideGroupedHeader: false
   					}),               			
                     						    						
                        columns: [{
   			                header   : 'Utente',
   			                dataIndex: 'utente', 
   			                flex     : 3
   			             }, {
   				                header   : 'Attivit&agrave;',
   				                dataIndex: 'attivita', 
   				                width    : 70
   				         }, {
   			                header   : 'Stato / Evas. Prog.',
   			                dataIndex: 'stato_data', 
   			                flex     : 5
   			             }],

                        }),                      
                    



                    
                    new Ext.grid.GridPanel({
                     title: 'Allegati',
                     id: 'OrdPropertyGridImg',
                     multiSelect: true,
                     tbar: new Ext.Toolbar({
     		            items:[
     		            	'->', '<b>Upload</b>', 
     		            {iconCls: 'icon-search-16', 
         		            handler: function(event, toolEl, panel){
             		            k_ordine=this.up('grid').store.proxy.extraParams.k_ordine;
             		            var m_grid = this.up('grid');
         		            	acs_show_win_std('Upload', 'acs_upload.php?fn=open_form', {k_ordine: k_ordine} , 600, 200, {
         		            		afterUpload: function(from_win){
         		            			m_grid.store.load();
         		            			from_win.close()
             		            	}
         		            	}, 'icon-search-16');

             		            }}
     		       		
     		         ]            
     		        }),   
               		 store: new Ext.data.Store({
               									
               				autoLoad: false,				        
               	  			proxy: {
               	  						timeout: 240000,
               							url: 'acs_get_order_images.php',
               							type: 'ajax',
               							reader: {
               						      type: 'json',
               						      root: 'root'
               						     },
               						     extraParams: {
               		    		    		k_ordine: ''
               		    				}               						        
				        
               						},
               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
               		     	groupField: 'tipo_scheda',               		     	
               			}),
    		    	features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),               			
                  						    						
                     columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],

			         listeners: {
				      activate: {
					      fn: function(e){
						   	if (this.store.proxy.extraParams.k_ordine != '')
							   	this.store.load();
					      }
				      },
					       
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  },

					  itemcontextmenu : function(grid, rec, node, index, event) {

						  
							event.stopEvent();							
							var grid = this;		             		
		    				//rows = grid.getSelectionModel().getSelection()[0].data.num_ord;		
							
		    				id_selected = grid.getSelectionModel().getSelection();
		    			  	list_selected_id = [];
		    			  	for (var i=0; i<id_selected.length; i++) 
		    				   list_selected_id.push({attachments: [id_selected[i].get('IDOggetto')]});
		    					  				
							var voci_menu = [];

							k_ord = this.getStore().proxy.extraParams.k_ordine;
							
					           voci_menu.push({
					         		text: 'Invia tramite e-mail',
					        		iconCls : 'icon-email_compose-16',          		
					        		     handler: function() {
						        		     
					        		    	 acs_show_win_std('Email Composer', 
							        		    	 '../base/acs_email_composer.php?fn=open_email', 
							        		    	 {
						        		    	 		attachments: [rec.get('IDOggetto')], k_ordine: k_ord, list_selected_id: list_selected_id
							        		    	 }, 
							        		    	 950, 500, null, 'icon-email_compose-16', 'Y');
								         }
					    		});


					           voci_menu.push({
					         		text: 'Cancella',
					        		iconCls : 'icon-sub_red_delete-16',          		
					        		     handler: function() {
						        		     
					        		    	 Ext.Msg.confirm('Richiesta conferma', 'Confermi la cancellazione degli allegati?', function(btn, text){																							    
													   if (btn == 'yes'){																	         	
														  
													    	 Ext.Ajax.request({
															        url        : '../base/file_utility.php?fn=exe_delete_file',
															        method     : 'POST',
												        			jsonData: {
												        			    k_ordine: k_ord,
												        			    list_selected_id: list_selected_id
																	},							        
															        success : function(result, request){
												            			grid.getStore().load();	
												            		 
												            		},
															        failure    : function(result, request){
															            Ext.Msg.alert('Message', 'No data to be loaded');
															        }
															    });					         	
											         	
														}
													   }); 
					        		    	 
								         }
					    		});


						      var menu = new Ext.menu.Menu({
						            items: voci_menu
							       }).showAt(event.xy);						    		
							  
					  }
			   		  
				         
			         } //listeners

                     })                      







                    ]
            },

            
            Ext.create('Ext.tab.Panel', {
            	id: 'm-panel',
        		cls: 'supply_desk_main_panel',            	            	
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 1,     // first tab initially active
                items: [
			                                   
                             					                                                
                ]
            })]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------
      	
		if (Ext.get("user-profile") != null){      	
	      	Ext.get("user-profile").on('click', function(){
	
	  		  acs_show_win_std('Cambia password', 
						'../base/acs_change_password.php?fn=change_password', 
						{}, 
						400, 350, {}, 'icon-pencil-16');	
	
			  });		
		}
		
      	
<?php if ($auth->is_sec_admin()) { ?>

		<?php
		 global $youtube_video_main_ID;
		 if (isset($youtube_video_main_ID)){
		 ?>
		 Ext.get("logo_cliente").on('click', function(){

		 	new Ext.Window({
		 	    title : "Info",
		 	    width : 300,
		 	    height: 300,
		 	    layout : 'fit',
		 	    items : [{
		 	        xtype : "component",
		 	        id    : 'iframe-win',
		 	        autoEl : {
		 	            tag : "iframe",
		 	            src : "http://www.youtube.com/embed/<?php echo $youtube_video_main_ID; ?>?autoplay=1" 
		 	        }
		 	    }]
		 	}).show();
		 	// Call below code through your any srcUpdate function.
		 	// Ext.getDom('iframe-win').src = "http://www.yahoo.com";
		  });

		 <?php } ?>


		Ext.get("main-site-help").on('click', function(){
			print_w = new Ext.Window({
					  width: 600
					, height: 300
					, minWidth: 300
					, minHeight: 300
					, plain: true
					, title: 'Pannello di amministrazione'
					, layout: 'fit'
					, border: true
					, closable: true										
				});	
			print_w.show(); 
			
				//carico la form dal json ricevuto da php
				Ext.Ajax.request({
				        url        : '../../module/admin/acs_form_json_amm_pannello.php',
				        method     : 'GET',
				        waitMsg    : 'Data loading',
				        success : function(result, request){
				            var jsonData = Ext.decode(result.responseText);
				            print_w.add(jsonData.items);
				            print_w.doLayout();
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    });			
			   	
    });      	
<?php } ?>      	
      	
      	
      	
        //-----------------------------------------------   
        if (Ext.get("main-site-home") != null){

        	Ext.QuickTips.register({
   			target: "main-site-home",
   			title: 'Home Page',
   			text: 'Chiude il desktop e torna alla pagina iniziale '
   		});         
        }



        <?php if ($js_parameters->p_solo_inter != 'Y'){ ?>     
           //-----------------------------------------------   
           if (Ext.get("main-site-tools") != null){

           	Ext.QuickTips.register({
      			target: "main-site-tool",
      			title: '(WIP)',
      			text: 'Modifica parametri elaborazione utente corrente'
      		});         
               
              Ext.get("main-site-tools").on('click', function(){
              	
      			print_w = new Ext.Window({
      					  width: 600
      					, height: 330
      					, minWidth: 300
      					, minHeight: 300
      					, plain: true
      					, title: 'Strumenti di supervisione ambiente'
      					, layout: 'fit'
      					, border: true
      					, closable: true										
      				});	
      			print_w.show();

      				//carico la form dal json ricevuto da php
      				Ext.Ajax.request({
      				        url        : 'acs_history_log.php?fn=get_parametri_form',
      				        method     : 'GET',
      				        waitMsg    : 'Data loading',
      				        success : function(result, request){
      				            var jsonData = Ext.decode(result.responseText);
      				            print_w.add(jsonData.items);
      				            print_w.doLayout();				            
      				        },
      				        failure    : function(result, request){
      				            Ext.Msg.alert('Message', 'No data to be loaded');
      				        }
      				    });			
              });
      	}         
        <?php } ?>	
           
      

        <?php if ($js_parameters->p_disabilita_Info != 'Y'){ ?>     
        //-----------------------------------------------   
        if (Ext.get("main-site-search") != null){

        	Ext.QuickTips.register({
   			target: "main-site-search",
   			title: 'Info',
   			text: 'Interrogazione ordini da programmare/programmati '
   		});         
            
           Ext.get("main-site-search").on('click', function(){
           	
   			print_w = new Ext.Window({
   					  width: 650
   					, height: 400
   					, minWidth: 300
   					, minHeight: 300
   					, plain: true
   					, title: 'Parametri interrogazione ordini da evadere'
   					, layout: 'fit'
   					, border: true
   					, closable: true
   					, iconCls: 'icon-search-16'
   					, tools: [
   						{
   						    type:'help',
   						    tooltip: 'Help',
   						    handler: function(event, toolEl, panel){
   						        show_win_help('MAIN_SEARCH');
   						    }
   						}								
   					]															
   				});	
   			print_w.show();

   				//carico la form dal json ricevuto da php
   				Ext.Ajax.request({
   				        url        : 'acs_form_json_main_search.php',
   				        method     : 'GET',
   				        waitMsg    : 'Data loading',
   				        success : function(result, request){
   				            var jsonData = Ext.decode(result.responseText);
   				            print_w.add(jsonData.items);
   				            print_w.doLayout();				            
   				        },
   				        failure    : function(result, request){
   				            Ext.Msg.alert('Message', 'No data to be loaded');
   				        }
   				    });			
           });
   	}
    <?php } ?>	    

        
    	//Inserimento anagrafiche gestionali
        if (Ext.get("bt-ins_anagrafiche") != null){
        	Ext.QuickTips.register({
    			target: "bt-ins_anagrafiche",
    			title: 'Customer POS',
    			text: 'Inserimento/Manutenzione anagrafica clienti POS'
    		});
                    
            Ext.get("bt-ins_anagrafiche").on('click', function(){
	        	acs_show_win_std('Parametri selezione anagrafiche clienti', 'acs_panel_ins_new_anag_form.php?fn=open_form_filtri', 
	    	        	null, 950, 500, null, 'iconClienti');
					        		          		
	        });	    	        	
        }   






		// Inserimento protocollazione PREVENTIVI      	
        if (Ext.get("bt-protocollazione-prev") != null){
        	Ext.QuickTips.register({
    			target: "bt-protocollazione-prev",
    			title: 'Prospect POS',
    			text: 'Inserimento/Manutenzione proposte di vendita'
    		});
                    
	        Ext.get("bt-protocollazione-prev").on('click', function(){

	        	mp = Ext.getCmp('m-panel');
	        	av_prot = Ext.getCmp('panel-protocollazione-prev');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {

	        		//carico la form dal json ricevuto da php
	        		Ext.Ajax.request({
	        		        url        : 'acs_panel_protocollazione.php?fn=open_prev',
	        		        method     : 'GET',
	        		        waitMsg    : 'Data loading',
	        		        success : function(result, request){
	        		            var jsonData = Ext.decode(result.responseText);
	        		            mp.add(jsonData.items);
	        		            mp.doLayout();
	        		            mp.show();
	        		            av_prot = Ext.getCmp('panel-protocollazione-prev').show();        		            
	        		        },
	        		        failure    : function(result, request){
	        		            Ext.Msg.alert('Message', 'No data to be loaded');
	        		        }
	        		    });

	        	}

	        	
	        });
        }          


		// Inserimento protocollazione ORDINI      	
        if (Ext.get("bt-protocollazione") != null){
        	Ext.QuickTips.register({
    			target: "bt-protocollazione",
    			title: 'Heading POS',
    			text: 'Inserimento/manutenzione ordini clienti POS'
    		});
                    
	        Ext.get("bt-protocollazione").on('click', function(){

	        	mp = Ext.getCmp('m-panel');
	        	av_prot = Ext.getCmp('panel-protocollazione');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {

	        		//carico la form dal json ricevuto da php
	        		Ext.Ajax.request({
	        		        url        : 'acs_panel_protocollazione.php',
	        		        method     : 'GET',
	        		        waitMsg    : 'Data loading',
	        		        success : function(result, request){
	        		            var jsonData = Ext.decode(result.responseText);
	        		            mp.add(jsonData.items);
	        		            mp.doLayout();
	        		            mp.show();
	        		            av_prot = Ext.getCmp('panel-protocollazione').show();        		            
	        		        },
	        		        failure    : function(result, request){
	        		            Ext.Msg.alert('Message', 'No data to be loaded');
	        		        }
	        		    });

	        	}

	        	
	        });
        }          


     // Inserimento portafoglio ordini    	
        if (Ext.get("bt-portafoglio-ordini") != null){
        	Ext.QuickTips.register({
    			target: "bt-portafoglio-ordini",
    			title: 'Report',
    			text: 'Riepilogo portafoglio clienti'
    		});
                    
	        Ext.get("bt-portafoglio-ordini").on('click', function(){

	        	av_prot = Ext.getCmp('panel-portafoglio-ordini');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {

	        		acs_show_win_std('Riepilogo portafoglio ordini/fatturato clienti', 'acs_panel_portafoglio_ordini.php?fn=open_form', null, 600, 300, null, 'iconChartBar')            

	        	}

	        	
	        });
        }  

        // anzianita_stato    	
        if (Ext.get("bt-anzianita_stato") != null){
        	Ext.QuickTips.register({
    			target: "bt-anzianita_stato",
    			title: 'Aging',
    			text: 'Analisi anzianit&agrave; stato ordini'
    		});
                    
	        Ext.get("bt-anzianita_stato").on('click', function(){

	        	av_prot = Ext.getCmp('panel-anzianita_stato');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {

	        		acs_show_win_std('Analisi anzianit&agrave; stato ordini','acs_analisi_anzianita_stato_ordini.php?fn=open_parameters', {show: 'Y'}, 600, 300, {}, 'icon-clessidra-16');

	        	}

	        	
	        });
        }  

        //anticipi

        if (Ext.get("bt-anticipi") != null){
        	Ext.QuickTips.register({
    			target: "bt-anticipi",
    			title: 'Prepayment',
    			text: 'Interrogazione e controllo pagamenti anticipati'
    		});
                    
	        Ext.get("bt-anticipi").on('click', function(){

	        	av_prot = Ext.getCmp('panel-anticipi');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {

	        		acs_show_win_std('Interrogazione anticipi/detrazioni','acs_panel_fatture_anticipo.php?fn=open_parameters', {}, 700, 500, {}, 'icon-credit_cards-16');					        		          		

	        	}

	        	
	        });
        }  

        //margini

        if (Ext.get("bt-margini") != null){
        	Ext.QuickTips.register({
    			target: "bt-margini",
    			title: 'Profit',
    			text: 'Analisi margine per ordine cliente'
    		});
                    
	        Ext.get("bt-margini").on('click', function(){

	        	av_prot = Ext.getCmp('panel-margini');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {

	        		acs_show_win_std('Parametri riepilogo margini ordine cliente','acs_panel_margini.php?fn=open_parameters', {}, 700, 370, {}, 'icon-margini-16');					        		          		

	        	}

	        	
	        });
        }  

        // ------------------- WIZARD MTS ------------------------
   	 // Riordino materiali promozionali   
        if (Ext.get("bt-wizard_MTS") != null){

        	Ext.QuickTips.register({
   			target: "bt-wizard_MTS",
   			title: 'Wizard MTS',
   			text: 'Fornitori riordino articoli MTS'
   		});         
            
           Ext.get("bt-wizard_MTS").on('click', function(){
        	   acs_show_win_std('Fornitori riordino articoli MTS', 'acs_wizard_MTS.php?fn=open_form', null, 600, 600, null, 'icon-shopping_basket_2-16');	
           });
   	}


        // ------------------- CONSEGNE MONTAGGIO ------------------------
      	 // Riordino materiali promozionali   
           if (Ext.get("bt-consegne_montaggio") != null){

           	Ext.QuickTips.register({
      			target: "bt-consegne_montaggio",
      			title: 'Delivery POS',
      			text: 'Programmazione consegne/montaggio'
      		});         
               
              Ext.get("bt-consegne_montaggio").on('click', function(){
           	   acs_show_win_std('Delivery POS', 'acs_consegne_montaggio.php?fn=get_parametri_form', null, 600, 400, null, 'icon-calendar-16');	
              });
      	}

     // ------------------- ALLEGATI/DOCUMENTI CLIENTE------------------------
        	 // Riordino materiali promozionali   
           if (Ext.get("bt-allegati_doc_cli") != null){

             	Ext.QuickTips.register({
        			target: "bt-allegati_doc_cli",
        			title: 'Attachment',
        			text: 'Allegati/Documenti cartella cliente'
        		});         
                 
                Ext.get("bt-allegati_doc_cli").on('click', function(){
             	   acs_show_win_std('Attachment', 'acs_attachment_cli.php?fn=get_parametri_form', null, 350, 150, null, 'icon-attachment-16');	
                });
        	}


           //*********************
           // ARRIVI
           //*********************
                   if (Ext.get("bt-arrivi") != null){
                   	Ext.QuickTips.register({
               			target: "bt-arrivi",
               			title: 'To Do List',
               			text: 'Attivit&agrave; utente di acquisizione/avanzamento ordini'
               		});            
           	        Ext.get("bt-arrivi").on('click', function(){ 
           	        	acs_show_win_std('Parametri interrogazione To Do list', 'acs_panel_todolist.php?fn=open_filtri', null , 300, 220, {}, 'icon-arrivi-16');                         
           	        	
           	     		});	    
                   }

                   // ------------------- ORDINE DI RESO------------------------
                  
                   if (Ext.get("bt-doc_log") != null){

                     	Ext.QuickTips.register({
                			target: "bt-doc_log",
                			title: 'Documenti logistica',
                			text: 'Documenti logistica'
                		});         
                         
                        Ext.get("bt-doc_log").on('click', function(){
                        	acs_show_panel_std('acs_doc_logistica.php?fn=open_prev');
                     	 
                        });
                	}	

                  	//Anagrafica articoli
                   if (Ext.get("bt-anag-art") != null){
                   	Ext.QuickTips.register({
               			target: "bt-anag-art",
               			title: 'Anagrafica articoli',
               			text: 'Gestione anagrafica articoli'
               		});
                               
           	        Ext.get("bt-anag-art").on('click', function(){
           	        	window.open ('../desk_utility/', "mywindow");	
               	      /*acs_show_win_std('Parametri selezione anagrafica articoli', '../desk_utility/acs_panel_anag_art.php?fn=open_form_filtri', 
           	    	  {from_pv : 'Y'}, 800, 430, null, 'icon-barcode');*/
           	        	
           	        });
                   }   


                   // ------------------- FATTURE ENTRATE ------------------------
                	 // Riordino materiali promozionali   
                     if (Ext.get("bt-fatture_entrata") != null){

                     	Ext.QuickTips.register({
                			target: "bt-fatture_entrata",
                			title: 'Documenti fornitore',
                			text: 'Controllo fatturazione documenti fornitore'
                		});         
                         
                        Ext.get("bt-fatture_entrata").on('click', function(){
                     	   acs_show_win_std('Documenti fornitore', <?php echo acs_url('module/desk_acq/acs_fatture_entrata.php?fn=open_form') ?>, null, 800, 350, null, 'icon-listino');	
                        });
                	}  

                

        if (Ext.get("bt-call-pgm") != null){
        	Ext.QuickTips.register({
    			target: "bt-call-pgm",
    			title: 'Start',
    			text: 'Avvio elaborazioni non interattive'
    		});
                    
	        Ext.get("bt-call-pgm").on('click', function(){

	        	av_prot = Ext.getCmp('call-pgm');

	        	if (av_prot){
	        		//av_prot.store.reload();
	        		av_prot.show();		    	
	        	} else {
					acs_show_win_std('Avvio processi batch',<?php echo acs_url('module/base/acs_form_json_call_pgm.php?mod=PVEN') ?>);					        		          		
				}

	        	
	        });
        }  





        if (Ext.get("bt-toolbar") != null){
         	Ext.QuickTips.register({
    			target: "bt-toolbar",
    			title: 'Setup',
    			text: 'Manutenzione voci/parametri gestionali di base'
    		});
                
    	    Ext.get("bt-toolbar").on('click', function(){
    				print_w = new Ext.Window({
    						  width: 1160
    						, height: 300
    						, minWidth: 300
    						, minHeight: 300
    						, plain: true
    						, title: 'Manutenzione voci/parametri gestionali di base'
    						, layout: 'fit'
    						, border: true
    						, closable: true
    						, scrollable: 'auto'
    						, tools: [
    									{
    									    type:'help',
    									    tooltip: 'Help',
    									    handler: function(event, toolEl, panel){
    									        show_win_help('SETUP_MAIN');
    									    }
    									}								
    								]										
    					});	
    				print_w.show(); 
    				
    					//carico la form dal json ricevuto da php
    					Ext.Ajax.request({
    					        url        : 'acs_form_json_toolbar.php',
    					        method     : 'GET',
    					        waitMsg    : 'Data loading',
    					        success : function(result, request){
    					            var jsonData = Ext.decode(result.responseText);
    					            print_w.add(jsonData.items);
    					            print_w.doLayout();
    					        },
    					        failure    : function(result, request){
    					            Ext.Msg.alert('Message', 'No data to be loaded');
    					        }
    					    });						   	
    	    });
        }




        

  		//schedulo la richiesta dell'ora ultimo aggiornamento
   		Ext.TaskManager.start(refresh_stato_aggiornamento);

   		//tooltip stato aggiornamento
   	    if (Ext.get("bt-stato-aggiornamento") != null){
   	     	Ext.QuickTips.register({
   				target: "bt-stato-aggiornamento",
   				title: 'Synchro',
   				text: <?php echo j($main_module->get_orari_aggiornamento()); ?>
   			});
   	    }


	
		
     <?php  
     
     
     $ar_sedi=  $main_module->get_stabilimenti();
     
     if (count($ar_sedi)>1 && $_SESSION[$main_module->get_cod_mod]['stabilimento_attivo_selezionato'] != 'Y'){
     
     ?>
     acs_show_win_std('Seleziona sede', 'acs_seleziona_stabilimento.php?fn=get_open_form', null, 530, 420, null, 'icon-home-16');
     return;
   
    <?php }?>
        
     		
    	//apro le finestre in automatico

		//se l'utente ha delle ToDo aperte... apro Todo
		<?php		
		$ao = new SpedAssegnazioneOrdini;
		if ($ao->ha_entry_aperte_per_utente($auth->get_user())){
		?>
    		acs_show_panel_std('acs_panel_todolist.php?fn=open_panel', null, {
        			form_values: {f_todo_ev: []}}, null, null);
		<?php } ?>	
        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
