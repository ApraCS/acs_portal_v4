<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'exe_genera_prebolla'){
    
    $list = $m_params->list_selected_ord;
    $use_session_history = microtime(true);

    foreach ($list as $v){
       
        if(trim($v->rdrife) == 'Da ricevere'){
            $rddes2 = $v->rddes2;
            $rdart = $v->rdart;
        }else{
            $rddes2 = "";
            $rdart = "";
        }
        
        $sh = new SpedHistory();
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'GEN_PRB_ACQ',
                "use_session_history" 	=> $use_session_history,
                "k_ordine"	=> $v->k_ordine,
                "vals" => array(
                    "RICLIE" => sprintf("%8s", trim($v->k_prog_MTO)),
                    "RIQTA" =>  sql_f($v->qta),
                    "RIART" =>  $rdart,
                    "RIDART" => $rddes2
                )
            )
            );
        
    }
            
    $sh = new SpedHistory();
    $num_creato = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'GEN_PRB_ACQ',
            "end_session_history" 	=> $use_session_history,
            "k_ordine"	=> '',
            "vals" => array(
                "RINRDO" => $m_params->rif_forn->f_num_rif,
                "RIDTEP" => $m_params->rif_forn->f_data_rif,
                "RIDTVA" => $m_params->rif_forn->f_data_reg,
                "RIDT"  =>  $id_ditta_default,
            )
        )
        );
            
    $ret = array();
    $ret['success'] = true;
    $ret['num_creato'] = 'Generata prebolla numero ' .trim($num_creato['RIAUTO']);
    echo acs_je($ret);
    exit();
            
}



if ($_REQUEST['fn'] == 'get_json_data_conferma_grid'){
    
    $list = $m_params->list_selected_ord;
    $ar = array();
    
    foreach($list as $v){
        
        $oe = $s->k_ordine_td_decode($v->k_ordine);
                
        $sql = "SELECT AR.*, CF_FORNITORE.CFRGS1 AS D_FOR, RDQTA
                FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
                LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art']} AR
	    		  ON AR.ARDT=RD.RDDT AND AR.ARART = RD.RDART
                LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_cli']} CF_FORNITORE
                    ON CF_FORNITORE.CFDT = AR.ARDT AND CF_FORNITORE.CFCD = AR.ARFOR1
                WHERE RD.RDART NOT IN('ACCONTO', 'CAPARRA') AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
				AND RD.RDTISR = '' AND RD.RDSRIG = 0 AND RDART = '{$v->c_art}'
                ";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $oe);
        
        while ($r = db2_fetch_assoc($stmt)) {
            
            $nr = array();
            $nr['k_ordine']	        = $v->k_ordine;
            $nr['ordine']           = $oe['TDOADO']."_".$oe['TDONDO'];	        
            $nr['c_art']	        = trim($r['ARART']);
            $nr['d_art']	        = trim($r['ARDART']);
            $nr['FORNITORE_C'] 	    = acs_u8e($r['ARFOR1']);
            $nr['FORNITORE_D'] 	    = acs_u8e($r['D_FOR']);
            $nr['UM']	            = trim($r['ARUMTE']);
            $nr['qta']		        = $r['RDQTA'];
            //$nr['PRZ_LIST']	        = '';
            $nr['fl_mts']	        = $v->fl_mts;
            $nr['k_prog_MTO']	        = $v->k_prog_MTO;
            $ar[] = $nr;
            
        }
        
    }
    
     
    
    
    echo acs_je($ar);
    
    exit();
}



// ******************************************************************************************
// exe modifica punto riordino su articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'grid_conferma_ordine'){
    $m_params = acs_m_params_json_decode();
    
    
    ?>

{"success":true, "items":

{
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    dockedItems: [{
			xtype: 'toolbar'
					, dock: 'bottom'
							, items: [
							 '->',       
			 				{ 
			                     xtype: 'button',
				            	 scale: 'large',
				            	 iconCls: 'icon-currency_black_euro-32',
			                     text: 'Genera prebolla',
						            handler: function() {
						               var grid = this.up('grid');
						               var loc_win = this.up('window');
						               var rows = grid.getStore().getRange();
                        			   list_selected_ord = [];
                                       
                                         for (var i=0; i<rows.length; i++) {
            				            list_selected_ord.push({
            				            	k_ordine: rows[i].get('k_ordine'),
            				            	k_prog_MTO: rows[i].get('k_prog_MTO'),
            				                c_art: rows[i].get('c_art'),
            				                d_art: rows[i].get('d_art'),
            				                qta: rows[i].get('qta'),
            				            	});}
            				            	
            				            	 my_listeners = {
				        			    		afterGenera: function(from_win){
				        			    		var grid_origine = Ext.getCmp('<?php echo $m_params->grid_id ?>');
				        						grid_origine.getStore().load();
	 											from_win.close();
	 											loc_win.close();
	 											
								        	}
										}
            				            	
						            acs_show_win_std('Prebolla entrata', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_fattura_forn', {list_selected_ord: list_selected_ord}, 500, 220, my_listeners, 'icon-listino');         
	            
							 }
					 } 			       
			]
		}],	                    
		
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		            listeners:{
		            	beforeedit: function(cellEditor, context, eOpts){
		            	    var record = context.record;
		            		if (record.get('fl_mts') != 'Y')
		            			return false;
		            	}
					 }
		          })
		      	],
		features: [
					{ftype: 'summary'}, {
			            id: 'group',
			            ftype: 'groupingsummary',
			            groupHeaderTpl: '{[values.rows[0].data.FORNITORE_D]} [{name}]',
			            hideGroupedHeader: false
			        },					
						
			{
				ftype: 'filters',
				encode: false, 
				local: true,   
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_conferma_grid', 
								   method: 'POST',								
								   type: 'ajax',
    								actionMethods: {
										     read: 'POST'
										    },
				          extraParams: {
								 
								 list_selected_ord: <?php echo acs_je($m_params->list_selected_ord)?>
							
			        			},
			     
			    		  doRequest: personalizza_extraParams_to_jsonData, 								
	
							       
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root',
						            idProperty: 'order'						            
						        }
							},
			groupField: 'FORNITORE_C',
				
			fields: ['k_prog_MTO', 'fl_mts', 'ordine', 'k_ordine', 'FORNITORE_C', 'FORNITORE_D', 'c_art', 'd_art', 'UM',  
						{name: 'qta', type: 'float'},  
							
					]
						
		}, //store	    
	    			 		
		columns: [	
		            {header: 'Ordine',	 width: 100, dataIndex: 'ordine'}			    
				  ,	{header: 'Codice', width: 100, dataIndex: 'c_art', filter: {type: 'string'}}
				  , {header: 'Articolo', flex: 1, dataIndex: 'd_art', filter: {type: 'string'}, 	
				  		 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					           return 'Totale'; 
					     }
					}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'qta', tdCls: 'grassetto', 
				  	renderer: floatRenderer0N,	  	
				  	editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            },
		            summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
								            return floatRenderer2(value); 
								        }				  
				   }
				 			  
    	], 
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    }
		    
	    
		    
    							
	}    
    

}


<?php exit; }

if ($_REQUEST['fn'] == 'get_fattura_forn'){ ?>
 
 {"success":true, "items": [
 
         {
             xtype: 'panel',
             title: '',
             autoScroll: true, 
             layout: 'fit',
 			  items: [
 			   { 
 						xtype: 'fieldcontainer',
 						layout: { 	type: 'vbox',
 								    pack: 'start',
 								    align: 'stretch'},						
 						items: [
 			  
 			   {
 		            xtype: 'form',
 		            bodyStyle: 'padding: 10px',
 		            bodyPadding: '5 5 0',
 		            frame: false,
 		            title: '',
 		            url: 'acs_op_exe.php',
 		            
 					buttons: [{
 			            text: 'Genera',
 			            iconCls: 'icon-button_blue_play-32',
 			            scale: 'large',
 			            handler: function() {
 			            	var form = this.up('form').getForm();
 							 var loc_win = this.up('window');
 							 var button = this;
			            	 button.disable();
 							 
 							 Ext.getBody().mask('Loading... ', 'loading').show();	
 							 Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_prebolla',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    rif_forn: form.getValues(),
 			        			    list_selected_ord: <?php echo acs_je($m_params->list_selected_ord); ?>,
 								},							        
 						        success : function(result, request){
 						         	Ext.getBody().unmask();
 						         	var jsonData = Ext.decode(result.responseText);
 						         	Ext.Msg.alert('Message', jsonData.num_creato);
 						            loc_win.fireEvent('afterGenera', loc_win);
									 
 			            		},
 						        failure    : function(result, request){
 						            Ext.getBody().unmask();
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
 						
 			
 			            }
 			         }],   		            
 		            
 		            items: [   {
 									xtype: 'fieldset',
 					                title: 'Riferimenti fornitore',
 					                defaultType: 'textfield',
 					                layout: 'anchor',
 					                items: [{
 										name: 'f_num_rif',
 										xtype: 'textfield',
 										fieldLabel: 'Numero',
 										value: '',
 									    anchor: '-15'							
 									 }, {
 									     name: 'f_data_rif'
 									   , flex: 1                		
 									   , xtype: 'datefield'
 									   , startDay: 1 //lun.
 									   , fieldLabel: 'Data fornitore'
 									   , labelAlign: 'left'
 									   , format: 'd/m/Y'
 									   , submitFormat: 'Ymd'
 									   , allowBlank: true
 									   , anchor: '-15'
 									   , listeners: {
 									       invalid: function (field, msg) {
 									       Ext.Msg.alert('', msg);}
 								 }
 									}, {
 									     name: 'f_data_reg'
 									   , flex: 1                		
 									   , xtype: 'datefield'
 									   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
 									   , startDay: 1 //lun.
 									   , fieldLabel: 'Data registraz.'
 									   , labelAlign: 'left'
 									   , format: 'd/m/Y'
 									   , submitFormat: 'Ymd'
 									   , allowBlank: true
 									   , anchor: '-15'
 									   , listeners: {
 									       invalid: function (field, msg) {
 									       Ext.Msg.alert('', msg);}
 								 }
 							}
 						
 										
 							
 									]
 					            },			 
 		   							 
 		           ]
 		              }	
 			
 		              
 		              	]}
 			  
 			  ] //ITEM panel
 			   
 		}
 	]
 }	
 	
 		
 <?php
 	exit;
 }
 