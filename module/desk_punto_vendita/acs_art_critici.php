<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();
//$m_params = json_decode(file_get_contents("php://input")); //php7
//$m_params = acs_m_params_decode(); //php7

$k_ordine = $m_params->k_ordine;

if ($_REQUEST['fn'] == 'exe_genera_fattura'){

 $list_ddt=$m_params->list_ddt;
 $use_session_history = microtime(true);
 
 foreach ($list_ddt as $v){
	    
	    if(trim($v->rdrife) == 'Da ricevere'){
	        $rddes2 = $v->rddes2;
	        $rdart = $v->rdart;
	    }else{
	        $rddes2 = "";
	        $rdart = "";
	    }
	  
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'GEN_DDT_ACQ',
            "use_session_history" 	=> $use_session_history,
            "k_ordine"	=> $v->k_ordine,
            "vals" => array(
                "RICLIE" => sprintf("%8s", trim($v->k_prog_MTO)),
                "RIART" =>  $rdart,
                "RIDART" => $rddes2
            )
        )
        );
   
	}
	
	$sh = new SpedHistory();
	$sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'GEN_DDT_ACQ',
	        "end_session_history" 	=> $use_session_history,
	        "k_ordine"	=> '',
	        "vals" => array(
	            "RINRDO" => $m_params->rif_forn->f_num_rif,
	            "RIDTEP" => $m_params->rif_forn->f_data_rif,
	            "RIDTVA" => $m_params->rif_forn->f_data_reg,
	            "RIDT"  =>  $id_ditta_default,
	        )
	    )
	    );

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit();
	
}


if ($_REQUEST['fn'] == 'exe_conferma_articoli_MTO'){

	$m_params = acs_m_params_json_decode();
	
	$all_ord=$m_params->all_ord;
	
	$use_session_history = microtime(true);
	
	
	foreach ($all_ord as $v){
			
	
		$sh = new SpedHistory();
		$sh->crea(
				'conferma_fornitura',
				array(
						"k_ordine"		=> $v->k_ordine, //il tddocu
						"use_session_history" => $use_session_history,
						"num_prog"		=>  $v->k_prog_MTO,
				        "articolo"		=>  $v->articolo,
						"tipo_op"		=>  $v->tipo_op
	
							
				)
	
				);
	
	}
	
	
	$sh = new SpedHistory();
	$sh->crea(
			'conferma_fornitura',
			array(
					"k_ordine"		=> '', //il tddocu
					"tipo_op"		=>  $v->tipo_op,
					"ditta"		=>  $id_ditta_default,
					"end_session_history" => $use_session_history
			)
			);
	exit;
}


if ($_REQUEST['fn'] == 'exe_save_stato'){

	$m_params = acs_m_params_json_decode();

		$sh = new SpedHistory();
		$sh->crea(
				'salva_stato',
				array(
						"k_ordine"			=> $k_ordine,
						"stato" 			=> $m_params->stato,
						"RDPMTO"			=> $m_params->RDPMTO 
				)
			);
	
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'exe_canc_ord_acq'){
    
    $m_params = acs_m_params_json_decode();
 
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DEL_ORD_MTO',
            "k_ordine"	=> $k_ordine,
            "vals" => array("RICLIE" => $m_params->prog,
                            "RIART" => $m_params->c_art,
                            "RIDART" => $m_params->d_art,
                            "RINOTE" => $m_params->ord_acq
            )
        )
       );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'exe_rip_forn'){
    
    $m_params = acs_m_params_json_decode();
    
    
    $sh = new SpedHistory();
    $sh->crea(
        'ripristino_fornitura',
        array(
            "k_ordine"			=> $k_ordine,
            "RDPMTO"			=> $m_params->RDPMTO
        )
        );

    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'open_art'){

$oe   = $s->k_ordine_td_decode_xx($m_params->k_ordine);
$row  = $s->get_ordine_by_k_ordine($m_params->k_ordine);
$ord  = $row;
$data = print_date($ord['TDDTDS']);

?>


{"success":true, "items": [

			{
		     xtype: 'grid',
		     selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
		      plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		            listeners:{
			        	afteredit: function(cellEditor, context, eOpts){
				        	var value = context.value;
				        	var grid = context.grid;
				        	var record = context.record;			        	
			        	   	Ext.Ajax.request({
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_stato',
					            method: 'POST',
			        		    jsonData: {
					      			   stato : value,
									   k_ordine: '<?php echo $m_params->k_ordine; ?>',
									   RDPMTO: record.get('RDPMTO') 					      			  
					      			   }, 					            
					            success: function ( result, request) {
					                var jsonData = Ext.decode(result.responseText);
					                record.set('stato_MTO', value);
					   						                															
					            },
					            failure: function (result, request) {
					            }
					        });
			        	}
					 }
		          })
		      ],
	         loadMask: true,
	         features: [
		       new Ext.create('Ext.grid.feature.Grouping',{
			   groupHeaderTpl: 'Tipo: {name}',
        	   hideGroupedHeader: true
		     })],
			store: {
						xtype: 'store',
						groupField: 'RDTPNO',
						autoLoad:true,
			
	  							proxy: {
								   url: 'acs_get_order_rows.php?type=<?php echo $m_params->type; ?>&nord=<?php echo $m_params->k_ordine; ?>', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
						            root: 'root'
						        }
							},
							
		        			fields: [
		            			'ditta_origine', 'TDDTEP', 'RDFG02', 'RDFG03', 'RDFMAN', 'RDDTDS', 'RDDFOR', 
		            			'RDDTEP', 'RDTPNO', 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDART',
		            			 'RDDES1', 'RDDES2', 'RDUM', 'RDDT', {name: 'RDQTA', type: 'float'},  'RDFG05',
		            			 'RDPMTO', 'k_ordine', 'RDFG01', 'RDQTA3', 'k_ord_art', 'stato_MTO', 'RDFG04',
		            			 'TDDEPO', 'maga', 'disponibilita', 'RDPMTO',  'RDOTID', 'RDOINU',
		            			 'RDUBIC', 'RDFU01'
		            			 
		        			]						
									
			}, //store
			
			<?php $cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>"; ?>
				

			      columns: [
			             {
			                header   : 'Disp. Sped.',
			                dataIndex: 'RDDTDS', 
			                width     : 70,
			                renderer: date_from_AS
			             },			        
			             {
			                header   : 'Stato',
			                dataIndex: 'RDRIFE', 
			                width     : 75
			             },{
			                text: '<?php echo $cf1; ?>',
			                width     : 35,
			                renderer: function(value, p, record){
			                if (record.get('RDFG02')== 'Y') 
			                return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
			                }
			             }, {
			                header   : 'Fornitore',
			                dataIndex: 'RDDFOR', 
			                flex    : 50
			             }, {
			                header   : 'Descrizione',
			                dataIndex: 'RDDES1', 
			                flex    : 150,
			                renderer: function(value, p, record){
			                	return record.get('RDDES1') + " [" + record.get('RDART') + "]"
			    			}			                			                
			             },{
			                header   : 'Um',
			                dataIndex: 'RDUM', 
			                width    : 30
			             }, {
			                header   : 'Q.t&agrave;',
			                dataIndex: 'RDQTA', 
			                width    : 45,
			                align: 'right', renderer: floatRenderer2
			             }, {
			                header   : 'Consegna',
			                dataIndex: 'RDDTEP', 
			                width: 70,
			                renderer: date_from_AS
			             },  {
			                header   : 'Dep.',
			                dataIndex: 'maga', 
			                tooltip: 'Deposito',
			                width: 40
			             },  {
			                header   : 'Ubicaz.',
			                dataIndex: 'RDUBIC', 
			                width: 50
			             },{
			                header   : 'Documento',
			                dataIndex: 'RDDES2',
			                flex    : 60,
			                renderer: function(value, metaData, record){
			                
        		                if (record.get('RDDES2') != "")	    			    	
        							metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('RDDES2')) + '"';	    			    
			                	if (record.get('RDFG04') == 'Y') metaData.tdCls += ' tpSfondoRosa'; //evidenza prezzo a 0
			                	
			                	return value;
			    			}
			             },  {
			                header   : 'OS',
			                dataIndex: 'RDFG05', 
			                flex    : 20,
			                renderer: function(value, metaData, record){
			                
			                if (record.get('RDFU01').trim() == 'P') {
			                	   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Prenotato') + '"';
			                	return 'P';
			                }	
			                
			                 if (record.get('RDFU01').trim() == 'R') {
			                	   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Riservato') + '"';
			                	return 'R';
			                }	
			                
		                	if (record.get('RDFG05').trim() == 'M') 
		                	   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Gestione magazzino') + '"';
			                	
			                	return value;
			    			}
			             }, {
	                		header   : 'Stato riga',
			                dataIndex: 'stato_MTO',
			                width: 100,
			                editor: {
				                xtype: 'combobox',
				                allowBlank: true,
				                valueField: 'id',
				                displayField: 'text',
				                	store: {
									fields: [{name:'id'}, {name:'text'}],
								    data: [								    
									     <?php echo acs_ar_to_select_json(find_TA_std('MTOST'), '') ?>	
									    ] 
								},	
								  listeners: {
            			            change: function(field,newVal, oldValue) {	      
            			            				            
            						   var m_grid = this.up('window').down('grid');
            						   var rows = m_grid.getSelectionModel().getSelection()[0].data;
            						   var maga = rows.maga;
            			            						   
            						   <?php 
            						   
            						   $sql = "SELECT TAKEY1
            						           FROM {$cfg_mod_DeskPVen['file_tabelle']} TA
            						           WHERE TADT='{$id_ditta_default}' 
                                               AND TATAID = 'DEPOS' AND TAFG01 = 'Y'";
            						   
            						   $stmt = db2_prepare($conn, $sql);
            						   echo db2_stmt_errormsg();
            						   $result = db2_execute($stmt);
            						   
            						   $ar_dep = "[";
            						   while($row = db2_fetch_assoc($stmt)){
            					            $dep = trim($row['TAKEY1']);
            						        $ar_dep .= "'{$dep}', ";
            						   }
            						   
            						   $ar_dep .= "]";
            						  
            						   ?>
            						   ar_mag_con_logistica = <?php echo j($ar_dep); ?>;
            						   
            						   if(ar_mag_con_logistica.includes(maga) && newVal == 3){
            						       acs_show_msg_error('Impossibile selezione il valore "A magazzino" per il deposito ' + maga);
									       field.value = oldValue;
									       return;
            						   }  
            			                  		            	
            			            }
            			        }
			           	    } 
	                }
			            
			         ]
			         
			         , listeners: {		
	 				
			 			afterrender: function (comp) {
							comp.up('window').setTitle('<?php echo "Riepilogo forniture ordine cliente {$oe['TDOADO']}_{$oe['TDONDO']}_{$ord['TDOTPD']}, Deposito {$ord['TDDEPO']} - Disponibilit&agrave; prevista {$data}"; ?>');	 				
			 			},
			 				
			 					
					/*	beforeselect: function(grid, record, index, eOpts) {
						
            			if (record.get('RDFG01').trim()=='O' || record.get('RDFG01').trim()=='D' || record.get('RDFG02').trim()=='Y') {//replace this with your logic.
               		         // Ext.Msg.alert('Message', 'articolo bloccato o gi&agrave; ordinato');
               		         alert('Articolo ordinato o bloccato');
               				 return false;
               				 
            			}
       				 } */
       				  celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	
					  	if (col_name=='RDDES1'){
							iEvent.preventDefault();	
							
								
			        		  my_listeners = {
				        			    afterBackgr: function(from_win){
				        				iView.getStore().load();
	 									from_win.close();
	 							    }
							  }
							  
						
							
							if(rec.get('RDFG05') == 'M'){
								acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt : rec.get('RDDT'), rdart: rec.get('RDART'), from_wz : 'Y', maga : rec.get('maga'), disp_maga : rec.get('disponibilita'), k_ordine : '<?php echo $m_params->k_ordine; ?>'}, 1200, 600, my_listeners, 'icon-shopping_cart_gray-16');
							}else{
								acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt : rec.get('RDDT'), rdart: rec.get('RDART')}, 1200, 600, null, 'icon-shopping_cart_gray-16');
							}							
					   }
	          
	            	}
	          
	           },
		       			 itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						  var voci_menu = [];
					      row = rec.data.k_ord_art;
						
					  	 voci_menu.push({
			         		text: 'Righe ordine fornitore',
			        		iconCls : 'icon-folder_search-16',          		
			        		handler: function () {
					    		  acs_show_win_std('Righe ordine fornitore', 
				                			'acs_get_order_rows_gest.php?fn=open_grid', {
				                             k_ordine: row, righe_doc : 'Y',
				                             rddes2: rec.get('RDDES2')
									}, 900, 600, {}, 'icon-shopping_cart_gray-16');
				                }
			    		});	 
			    		
			    		<?php if($auth->is_admin()){?>
			    		
			    		 	 voci_menu.push({
			         		text: 'Costo articolo ordine cliente',
			        		iconCls : 'icon-folder_search-16',          		
			        		handler: function () {
			        		
			        		  my_listeners = {
				        			    afterModArt: function(from_win){
				        				grid.getStore().load();
	 									from_win.close();
	 											
								        	}
										}
			        		
			        		acs_show_win_std('Costo articolo ordine cliente ' + '<?php echo "{$oe['TDOADO']}_{$oe['TDONDO']}_{$ord['TDOTPD']}"; ?>', 'acs_get_order_rows_gest.php?fn=form_mod_art', {row: rec.data,  k_ordine: '<?php echo $m_params->k_ordine; ?>', magazzino : 'Y', art_mto : 'Y'}, 500, 170, my_listeners, 'icon-pencil-16');          		
			        		
					    		/* acs_show_win_std('Righe ordine', 
				                			'acs_get_order_rows_gest.php?fn=open_grid', {
				                            k_ordine: '<?php echo $m_params->k_ordine; ?>', magazzino : 'Y',
				                 	}, 800, 600, {}, 'icon-shopping_cart_gray-16');*/
								
				                }
			    		});	
			    		
			    		
			    		if(rec.get('RDFG03') == '3'){
			    		
			    		 voci_menu.push({
			         		text: 'Ripristino fornitura',
			        		iconCls : 'icon-button_blue_play-16',          		
			        		handler: function () {
			        				
										Ext.Ajax.request({
										   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_rip_forn',
										   method: 'POST',
										   jsonData: { 
						        				RDPMTO : rec.get('RDPMTO'),
						        				k_ordine : '<?php echo $k_ordine; ?>'
						        				 }, 
										   
										   success: function(response, opts) {
										   	  
											  grid.getStore().load();									   	  
										   }, 
										   failure: function(response, opts) {
										      Ext.Msg.alert('Message', 'No data to be loaded');
										   }
										});	

					    		 
				                }
			    		});
			    		
			    		}
			    		
			    		 voci_menu.push({
			         		text: 'Cancella ordine acquisto',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		
			        		Ext.Msg.confirm('Conferma cancellazione', 'Confermi l\'operazione?', function(btn, text){																							    
								   if (btn == 'yes'){					        		
				        		
										Ext.Ajax.request({
										   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_ord_acq',
										   method: 'POST',
										   jsonData: { 
						        				prog : rec.get('RDPMTO'),
						        				c_art : rec.get('RDART'),
						        				d_art : rec.get('RDDART'),
						        				ord_acq : rec.get('RDDES2'),
						        				k_ordine : '<?php echo $k_ordine; ?>'
						        				 }, 
										   
										   success: function(response, opts) {
										   	  
											  grid.getStore().load();									   	  
										   }, 
										   failure: function(response, opts) {
										      Ext.Msg.alert('Message', 'No data to be loaded');
										   }
										});	

									  }
									});
					    		 
				                }
			    		});	 
			    		
			    		
			    		
			    		
			    		
			    		<?php }?>  		

				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	 					
	 			 
	 			      }
	 			      
	 			      ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('RDFMAN')=='M')
					           		return ' segnala_riga_rosso';
					           if (record.get('RDFMAN')=='C')
					           		return ' segnala_riga_giallo';				           		
					           if (record.get('RDDTDS')==record.get('TDDTEP'))
					           		return ' segnala_riga_viola';	
					          					           		
					           return '';																
					         }   
					    },	dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [  {
                     xtype: 'button',
                    text: 'Genera ddt',
		            iconCls: 'icon-currency_black_euro-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                   var ord = this.up('grid');
                   win = this.up('window');
                            
                           ordine_selected = ord.getSelectionModel().getSelection();
                         
                            list_selected_ord = [];
                           
                            for (var i=0; i<ordine_selected.length; i++) {
				            list_selected_ord.push({
				            	k_ordine: ordine_selected[i].get('k_ordine'),
				            	k_prog_MTO: ordine_selected[i].get('RDPMTO'),
				            	flag_1: ordine_selected[i].get('RDFG01'),
				            	flag_M: ordine_selected[i].get('RDFG05'),
				            	rdrife: ordine_selected[i].get('RDRIFE'),
				            	rdart: ordine_selected[i].get('RDART'),
				            	rddes2: ordine_selected[i].get('RDDES2'),
				            	tipo_op: 'CON'
				            	});}
				          
				            if (list_selected_ord.length == 0){
									  acs_show_msg_error('Selezionare almeno un ordine');
									  return false;
								  }
						
						for (var chiave in list_selected_ord) {
						        if (list_selected_ord[chiave].flag_1 != 'O'){
									  acs_show_msg_error('Articolo con ddt');
									  return false;
								  }
								  
								  if (list_selected_ord[chiave].flag_M == 'M'){
									  acs_show_msg_error('Articolo gestito a magazzino');
									  return false;
								  }
						   
						   }
								
								
								acs_show_win_std('Ddt entrata', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_fattura_forn', {list_ddt: list_selected_ord}, 500, 220, {}, 'icon-listino');         
			
			            }

			     }, '->',
			     
			     {
                     xtype: 'button',
                    text: 'Conferma fornitura',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                   var ord = this.up('grid');
                   win = this.up('window');
                            
                           ordine_selected = ord.getSelectionModel().getSelection();
                            list_selected_ord = [];
                            
                            for (var i=0; i<ordine_selected.length; i++) {
				            list_selected_ord.push({
				            	k_ordine: ordine_selected[i].get('k_ordine'),
				            	k_prog_MTO: ordine_selected[i].get('RDPMTO'),
				            	articolo: ordine_selected[i].get('RDART'),
				            	flag_1: ordine_selected[i].get('RDFG01'),
				            	flag_2: ordine_selected[i].get('RDFG02'),
				            	flag_M: ordine_selected[i].get('RDFG05'),
				            	tipo_op: 'CON'
				            	});}
				            
				            if (list_selected_ord.length == 0){
									  acs_show_msg_error('Selezionare almeno un ordine');
									  return false;
								  }
								  
								  		  
						   for (var chiave in list_selected_ord) {
						   
						        if (list_selected_ord[chiave].flag_M == 'M'){
									  acs_show_msg_error('Articolo gestito a magazzino');
									  return false;
								  }
						        if (list_selected_ord[chiave].flag_1 == 'O' || list_selected_ord[chiave].flag_1 == 'D' || list_selected_ord[chiave].flag_2 == 'Y'){
									  acs_show_msg_error('Articolo ordinato o bloccato');
									  return false;
								  }
								  
								
						   
						   }
								  
								  
								   Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_articoli_MTO',
									        method     : 'POST',
						        			jsonData: {
						        				all_ord: list_selected_ord
											},							        
									        success : function(result, request){
									        win.close(); 	
						            	    //ord.getStore().load();									        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
			
			            }

			     }]
		   }]
         
	   		
			
		}//grid
		 
			
		
 				
	
     ]
        
 }
 
  <?php 
 exit;
 }
 
 if ($_REQUEST['fn'] == 'get_fattura_forn'){
 
 
 	$m_params = acs_m_params_json_decode();
 
 	?>
 
 {"success":true, "items": [
 
         {
             xtype: 'panel',
             title: '',
             autoScroll: true, 
             layout: 'fit',
 			  items: [
 			   { 
 						xtype: 'fieldcontainer',
 						layout: { 	type: 'vbox',
 								    pack: 'start',
 								    align: 'stretch'},						
 						items: [
 			  
 			   {
 		            xtype: 'form',
 		            bodyStyle: 'padding: 10px',
 		            bodyPadding: '5 5 0',
 		            frame: false,
 		            title: '',
 		            url: 'acs_op_exe.php',
 		            
 					buttons: [{
 			            text: 'Genera',
 			            iconCls: 'icon-button_blue_play-32',
 			            scale: 'large',
 			            handler: function() {
 			            	var form = this.up('form').getForm();
 							 var loc_win = this.up('window');
 							 var button = this;
			            	 button.disable();
 							 
 							 Ext.getBody().mask('Loading... ', 'loading').show();	
 							 Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_fattura',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    rif_forn: form.getValues(),
 			        			    list_ddt: <?php echo acs_je($m_params->list_ddt); ?>
 								},							        
 						        success : function(result, request){
 						         	Ext.getBody().unmask();
 						         	
 						          if (!Ext.isEmpty(loc_win.events.aftergenera))
										loc_win.fireEvent('afterGenera', loc_win);
								 else
		            	 			loc_win.close();
			            	
 			            		 
 			            		},
 						        failure    : function(result, request){
 						            Ext.getBody().unmask();
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
 						
 			
 			            }
 			         }],   		            
 		            
 		            items: [   {
 									xtype: 'fieldset',
 					                title: 'Riferimenti fornitore',
 					                defaultType: 'textfield',
 					                layout: 'anchor',
 					                items: [{
 										name: 'f_num_rif',
 										xtype: 'textfield',
 										fieldLabel: 'Numero',
 										value: '',
 									    anchor: '-15'							
 									 }, {
 									     name: 'f_data_rif'
 									   , flex: 1                		
 									   , xtype: 'datefield'
 									   , startDay: 1 //lun.
 									   , fieldLabel: 'Data fornitore'
 									   , labelAlign: 'left'
 									   , format: 'd/m/Y'
 									   , submitFormat: 'Ymd'
 									   , allowBlank: true
 									   , anchor: '-15'
 									   , listeners: {
 									       invalid: function (field, msg) {
 									       Ext.Msg.alert('', msg);}
 								 }
 									}, {
 									     name: 'f_data_reg'
 									   , flex: 1                		
 									   , xtype: 'datefield'
 									   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
 									   , startDay: 1 //lun.
 									   , fieldLabel: 'Data registraz.'
 									   , labelAlign: 'left'
 									   , format: 'd/m/Y'
 									   , submitFormat: 'Ymd'
 									   , allowBlank: true
 									   , anchor: '-15'
 									   , listeners: {
 									       invalid: function (field, msg) {
 									       Ext.Msg.alert('', msg);}
 								 }
 							}
 						
 										
 							
 									]
 					            },			 
 		         	   
 					
 					 
 										 
 		           ]
 		              }	, //fine primo form		  
 			
 		              
 		              	]}
 			  
 			  ] //ITEM panel
 			   
 		}
 	]
 }	
 	
 		
 <?php
 	exit;
 }
 