<?php

function sum_columns_value(&$ar_r, $r){
	$ar_r['TFTOTD'] += $r['TFTOTD'];
	$ar_r['TFTIMP'] += $r['TFTIMP'];
	$ar_r['detratto'] += $r['detratto'];
	
	
	//if ($r['TFFG01']!= 'C' && $r['TFFG01'] != 'M' )
	$ar_r['residuo'] += $r['TFTIMP']+$r['detratto'];
	
	if($ar_r['importo_ord'] == 0){
		$ar_r['residuo_ord'] = 0;
	}else{
		$ar_r['residuo_ord'] = $ar_r['importo_ord'] - $ar_r['caparra'] -  $ar_r['TFTOTD'];
	
	}

	if ($ar_r['importo_ord']>0){
		$ar_r['perc'] = $ar_r['residuo_ord']/$ar_r['importo_ord'];
	}else{
		$ar_r['perc']=0;
	}
	

}


function sum_columns_value_imp(&$ar_r, $r){
	
	$ar_r['importo_ord'] += $r['S_IMPORTO'];
	$ar_r['caparra'] += $r['S_CAPARRA'];

	if($ar_r['importo_ord'] == 0){
		$ar_r['residuo_ord'] = 0;
	}else{
		$ar_r['residuo_ord'] = $ar_r['importo_ord'] - $ar_r['caparra'] -  $ar_r['TFTOTD'];
		
	}
	
	if ($ar_r['importo_ord']>0){
		$ar_r['perc'] = $ar_r['residuo_ord']/$ar_r['importo_ord'];
	}else{
		$ar_r['perc']=0;
	}
	
	
	if($ar_r['liv']=='liv_2'){
	
		
		if(!isset($ar_r['stato']))
			$ar_r['stato']= array();
				
		if (!in_array($r['TDSTAT'], $ar_r['stato'])) {
			array_push($ar_r['stato'], $r['TDSTAT']);
		}
		
		
		$ar_r['stato_out']= implode(', ' , $ar_r['stato']);
	}
}


function detratto($r, $stmt){

	$fat= $r['TFDOCU'];
	$result = db2_execute($stmt, array($fat));
	$row = db2_fetch_assoc($stmt);
	return array($row['S_RFIMPP'], $row['N_RFIMPP']);
}


function crea_ar_tree_fatture_anticipo($node, $form_values){
	global $cfg_mod_DeskPVen, $cfg_mod_Gest, $conn, $id_ditta_default;
	
	$main_module = new DeskPVen();
	$s = new Spedizioni(array('no_verify' => 'Y'));
	
	$ar = array();
	$ret = array();
	$stato = array();
	
	//FATTURATO MERCE, IMPORTO FATTURE
	$sql_f = "SELECT SUM(TFINFI) AS S_TFINFI, SUM(TFTOTD) AS S_TFTOTD
				FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
				WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VF' AND TFNATD='F'
				AND TFCCON = ?
				GROUP BY TFDT, TFCCON ";
	
	$stmt_f = db2_prepare($conn, $sql_f);
	echo db2_stmt_errormsg();
	
		
	//IMPORTO NOTE CREDITO
	$sql_nc = "SELECT  SUM(TFTOTD) AS S_TFTOTD
				FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
				WHERE TFDT='{$id_ditta_default}' AND TFTIDO='VF' AND TFNATD='C'
				AND TFCCON = ?
				GROUP BY TFDT, TFCCON ";
	
	$stmt_nc = db2_prepare($conn, $sql_nc);
	echo db2_stmt_errormsg();
	
	
  if ($node == '' || $node == 'root'){
	
    $sql_where_imp.= sql_where_by_combo_value('TDCCON', $form_values->f_cliente_cod);
    $sql_where_imp.= sql_where_by_combo_value('TDSTAB', $form_values->f_sede);
    $sql_where_imp.= sql_where_by_combo_value('TDSTAT', $form_values->f_stato_ordine);
    
    //controllo data ordine
    if (strlen($form_values->f_data_ordine_da) > 0)
    	$sql_where_imp .= " AND TDODRE >= {$form_values->f_data_ordine_da}";
    if (strlen($form_values->f_data_ordine_a) > 0)
    	$sql_where_imp .= " AND TDODRE <= {$form_values->f_data_ordine_a}";
    
    //*********************************
    //ELABORAZIONE SU ORDINI	
    //*********************************
  	//creo struttura sede/cliente con importo ordini
  	$sql = "SELECT TDCARP, TDSTAB, TDCCON, TDDCON, TDSTAT, SUM({$cfg_mod_DeskPVen['info_importo_field']}) AS S_IMPORTO, 
  			SUM(TDINFI - TDTOTD) AS S_CAPARRA 
		  	FROM  {$cfg_mod_DeskPVen['file_testate']}
  			WHERE ". $s->get_where_std()."AND TDOTPD='MO' {$sql_where_imp}
  			GROUP BY TDSTAB, TDCCON, TDDCON, TDSTAT, TDCARP
  			ORDER BY TDSTAB, TDDCON";
  	
  	  	
  	$stmt = db2_prepare($conn, $sql);
  	echo db2_stmt_errormsg();
  	$result = db2_execute($stmt);
  	
  	
  	while ($row = db2_fetch_assoc($stmt)) {
  		
  		$tmp_ar_id = array();
  		$ar_r= &$ar;
  	
  		//stacco dei livelli
  		$cod_liv_tot = 'TOTALE';			//totale
  		$cod_liv0 = trim($row['TDSTAB']); 	//sede
  		$cod_liv1 = trim($row['TDCCON']); 	//codice cliente
  		
  	
  		
  		//LIVELLO TOTALE
  		$liv = $cod_liv_tot;
  		if (!isset($ar_r["{$liv}"])){
  			$ar_new = array();
  			$ar_new['id'] = 'liv_totale';
  			$ar_new['liv_cod'] = 'TOTALE';
  			$ar_new['task'] = 'Totale';
  			$ar_new['liv'] = 'liv_totale';
  			$ar_new['expanded'] = true;
  			$ar_r["{$liv}"] = $ar_new;
  		}
  		$ar_r = &$ar_r["{$liv}"];
  		sum_columns_value_imp($ar_r, $row);
  		
  		//creo la sede
  		$liv=$cod_liv0;
  		$ar_r = &$ar_r['children'];
  		
  		$tmp_ar_id[] = $liv;
  		if(!isset($ar_r[$liv])){
  			$ar_new= array();
  			$ar_new['id'] = implode("|", $tmp_ar_id);
  			$ar_new['codice']=$liv;
  			$ar_new['task'] = $s->decod_std('START', trim($row['TDSTAB']));
  			$ar_new['liv'] = 'liv_1';
  			$ar_new['expanded'] =$livello1_expanded;
  			
  			
  			$ar_r[$liv]=$ar_new;
  		}
  	
  		$ar_sede = &$ar_r[$liv];
  		$ar_r=&$ar_r[$liv];
  		
  		sum_columns_value_imp($ar_r, $row);
  		
/*  	
  		if ($form_values->f_filtro_anticipi == 'Y'){
  		    if (!isset($ar_r[$cod_liv_tot]["children"][$cod_liv0]["children"][$cod_liv1])){
  		        continue;
  		    }
  		}
*/  		
  	
  		//creo cliente
  		$liv=$cod_liv1;
  		$ar_r=&$ar_r['children'];
  		$tmp_ar_id[] = $liv;
  		if(!isset($ar_r[$liv])){
  			$ar_new= array();
  			$ar_new['id'] = implode("|", $tmp_ar_id);
  			$ar_new['codice']=$liv;
  			$ar_new['task'] = $row['TDDCON'];
  			$ar_new['tipo_caparra'] = $row['TDCARP'];
  			$ar_new['liv'] = 'liv_2';
  			$ar_new['expanded'] =$livello2_expanded;
  			$ar_new['children'] = array();
  			$ar_new['leaf'] = true;
  			
  			//FATTURATO MERCE, IMPORTO FATTURE
  			$result = db2_execute($stmt_f, array($liv));
  			$row_f = db2_fetch_assoc($stmt_f);
  			$ar_new['fat_merce'] =  $row_f['S_TFINFI'];
  			$ar_new['importo_fat'] = $row_f['S_TFTOTD'];
  			
  				
  			//IMPORTO NOTE CREDITO
  			$result = db2_execute($stmt_nc, array($liv));
  			$row_nc = db2_fetch_assoc($stmt_nc);
  			$ar_new['imp_note_credito'] =  $row_nc['S_TFTOTD'];
  		
  				
  			//aggiorno valori sede
  			$ar_sede['fat_merce'] 			+= $ar_new['fat_merce'];
  			$ar_sede['importo_fat'] 		+= $ar_new['importo_fat'];
  			$ar_sede['imp_note_credito'] 	+= $ar_new['imp_note_credito'];
  			
  			
  			//aggiorno valori su liv totale
  			$ar['TOTALE']['fat_merce'] 			+= $ar_new['fat_merce'];
  			$ar['TOTALE']['importo_fat'] 		+= $ar_new['importo_fat'];
  			$ar['TOTALE']['imp_note_credito'] 	+= $ar_new['imp_note_credito'];
  			
  			$ar_r[$liv]=$ar_new;
  		}
  	
  		$ar_r=&$ar_r[$liv];
  		sum_columns_value_imp($ar_r, $row);

  	}
  	
  	
  	 
  	
  	//*********************************
  	//ELABORAZIONE SU FATTURE
  	//*********************************
  	
  	
	$sql_where.= sql_where_by_combo_value('TFCCON', $form_values->f_cliente_cod);
	$sql_where.= sql_where_by_combo_value('TFNAZI', $form_values->f_sede);	
	$sql_where.= sql_where_by_combo_value('TFTPDO', $form_values->f_tipo_documento);


	//controllo data
	if (strlen($form_values->f_data_da) > 0){
		$sql_where .= " AND TF.TFDTRG >= {$form_values->f_data_da}";
		$sql_where_rf = " AND RFDTRG >= {$form_values->f_data_da}";
	}	
	if (strlen($form_values->f_data_a) > 0){
		$sql_where .= " AND TF.TFDTRG <= {$form_values->f_data_a}";
		$sql_where_rf = " AND RFDTRG <= {$form_values->f_data_a}";
	}
	
	//controllo data ordine
	if (strlen($form_values->f_data_ordine_da) > 0)
	    $sql_where .= " AND TDODRE >= {$form_values->f_data_ordine_da}";
    if (strlen($form_values->f_data_ordine_a) > 0)
        $sql_where .= " AND TDODRE <= {$form_values->f_data_ordine_a}";
				
			//controllo fattura(aperta/chiusa)
		$filtro_res=$form_values->f_filtra_fatture;

		if($filtro_res== "C"){
			$sql_where.=" AND TFFG01 IN ('C','M')";
				
		}else if ($filtro_res== "A"){
				
			$sql_where.=" AND TFFG01 <>'C' AND TFFG01 <> 'M'";
		}


		$livello1_expanded=false;
		$livello2_expanded=false;

	
		if (is_array($form_values->f_sede)){
			if (count($form_values->f_sede) > 0)
				$livello1_expanded=true;
		}

		if (strlen($form_values->f_cliente_cod)>0){
			$livello1_expanded=true;
			$livello2_expanded=true;
		}
		

			$sql = "SELECT TFTIDO, TFAADO, TFNRDO, TFCCON, TFDCON, TFNAZI, TFDOCU,
					TFDNAZ, TFTOTD, TFTIMP, TFDTRG, TFAADO, TFNRDO, TFTPDO, TFFG01, TFRGSV,
					TFINUM, TFAADO, TFTPDO, TFDTPD, TFDT, TFSTAT, TFCPAG, TFDPAG
					FROM  {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF
                    LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
                        ON TF.TFDT = TD.TDDT AND TF.TFDOAB = TD.TDDOCU
                    WHERE 1=1 ".$main_module->get_where_std_PV_TF()." AND TFDT='{$id_ditta_default}' 
					AND TFTIDO='VA' {$sql_where} ORDER BY TFDNAZ, TFDCON";
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			$sql= "SELECT sum(RFIMPP) as S_RFIMPP, count(*) as N_RFIMPP, RFDOCU
					FROM {$cfg_mod_Gest['fatture_anticipo']['file_righe']} RF
					WHERE RFDT = '{$id_ditta_default}' AND RFDOCU= ? {$sql_where_rf}
                    GROUP BY RFDOCU";
			
	        $stmt_detratto = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			
			while ($row = db2_fetch_assoc($stmt)) {
			
				$tmp_ar_id = array();
				$ar_r= &$ar;

				//stacco dei livelli
				$cod_liv_tot = 'TOTALE';	//totale
				$cod_liv0 = trim($row['TFNAZI']); //nazione
				$cod_liv1 = trim($row['TFCCON']); //codice cliente
				$cod_liv2 = trim($row['TFDOCU']); //chiave documento
				

				$ar_detratto=detratto($row, $stmt_detratto);
				$row['detratto'] = $ar_detratto[0];
				
				
				//solo per i clienti estratti in base alla data ordine
				if ($form_values->f_filtro_ordini == 'Y'){
					if (!isset($ar_r[$cod_liv_tot]["children"][$cod_liv0]["children"][$cod_liv1])){
						continue;
					}
				}

				//LIVELLO TOTALE
				$liv = $cod_liv_tot;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = 'liv_totale';
					$ar_new['liv_cod'] = 'TOTALE';
					$ar_new['task'] = 'Totale';
					$ar_new['liv'] = 'liv_totale';
					$ar_new['expanded'] = true;
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $row);

				//creo la nazione
				$liv=$cod_liv0;
				$ar_r = &$ar_r['children'];
				$tmp_ar_id[] = $liv;
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['codice']=$liv;
					$ar_new['task'] = acs_u8e($row['TFDNAZ']);
					$ar_new['liv'] = 'liv_1';
					$ar_new['expanded'] =$livello1_expanded;
					$ar_r[$liv]=$ar_new;
				}

				$ar_r=&$ar_r[$liv];
				sum_columns_value($ar_r, $row);

				//creo cliente
				$liv=$cod_liv1;
				$ar_r=&$ar_r['children'];
				$tmp_ar_id[] = $liv;
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['codice']=$liv;
					$ar_new['task']= $row['TFDCON'];
					
					$ar_new['liv'] = 'liv_2';
					$ar_new['expanded'] =$livello2_expanded;
					
					//FATTURATO MERCE, IMPORTO FATTURE
					$result = db2_execute($stmt_f, array($row['TFCCON']));
					$row_f = db2_fetch_assoc($stmt_f);
					$ar_new['fat_merce'] = $row_f['S_TFINFI'];
					$ar_new['importo_fat'] = $row_f['S_TFTOTD'];
					
					//IMPORTO NOTE CREDITO
					$result = db2_execute($stmt_nc, array($row['TFCCON']));
					$row_nc = db2_fetch_assoc($stmt_nc);
					$ar_new['imp_note_credito'] = $row_nc['S_TFTOTD'];
					$result = db2_execute($stmt_f, array($liv));
					$row_f = db2_fetch_assoc($stmt_f);
					$ar_new['fat_merce'] =  $row_f['S_TFINFI'];
					
					//aggiorno valori su liv totale
					$ar['TOTALE']['fat_merce'] 			+= $ar_new['fat_merce'];
					$ar['TOTALE']['importo_fat'] 		+= $ar_new['importo_fat']; 
					$ar['TOTALE']['imp_note_credito'] 	+= $ar_new['imp_note_credito'];
					
					$ar_new['children'] = array();
					$ar_r[$liv]=$ar_new;
				}

				$ar_r=&$ar_r[$liv];
				$ar_r['leaf'] = false;
				sum_columns_value($ar_r, $row);


				//chiave documento
				$liv=$cod_liv2;
				$ar_r=&$ar_r['children'];
				$tmp_ar_id[] = $liv;
				$task=array();
				$task[0]=$row['TFAADO'];
			    $task[1]=$row['TFNRDO'];
			    $task[2]=$row['TFDT'];
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['codice']=$row['TFTPDO'];
					$ar_new['task']= "[{$row['TFINUM']}] " .implode("_", $task);
					$ar_new['data'] = $row['TFDTRG'];
					$ar_new['c_paga'] = $row['TFCPAG'];
					$ar_new['d_paga'] = $row['TFDPAG'];
					$ar_new['rif'] = trim($row['TFRGSV']);
					$ar_new['liv'] = 'liv_3';
					$ar_new['liv_cod_qtip'] = acs_u8e($row['TFDTPD']);
					$ar_new['stato']= $row['TFFG01'];
					$ar_new['stato_out']= $row['TFSTAT'];
					if($ar_detratto[1]==0){
						$ar_new['leaf']=true;
					}
					
					$ar_r[$liv]=$ar_new;
				}
				
				$ar_r=&$ar_r[$liv];
				sum_columns_value($ar_r, $row);
			}

		} 
		else {
		    
		    //controllo data
		    if (strlen($form_values->f_data_da) > 0)
		        $sql_where_rf = " AND RFDTRG >= {$form_values->f_data_da}";
		    
		    if (strlen($form_values->f_data_a) > 0)
		        $sql_where_rf = " AND RFDTRG <= {$form_values->f_data_a}";
		    
		
			$value = explode("|", $node);
			$value[0]; //id_nazione
			$value[1]; //id_cliente
			$value[2]; //id_fattura
				
		
			$sql= "SELECT * FROM {$cfg_mod_Gest['fatture_anticipo']['file_righe']} RF
			 		LEFT OUTER JOIN {$cfg_mod_Gest['fatture_anticipo']['file_testate']} TF_RIF
			 		  ON RF.RFPECO = TF_RIF.TFDOCU
					WHERE RFDT = '{$id_ditta_default}' AND RFDOCU= ".sql_t($value[2]). "
                    {$sql_where_rf}
					ORDER BY RFDTRG";
          			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
		
			while ($r = db2_fetch_assoc($stmt)){
		
				$task=array();
				$task[0]=$r['RFAADO'];
				$task[1]=$r['RFNRDO'];
				$task[2]=$r['RFDT'];
					
				$ar_new = array();
				$ar_new['id'] = implode("|", array($node, $r['RFNOTE']));
				$ar_new['codice']='';
				$ar_new['task']= $r['RFNOTE'];

				if(substr(trim($r['RFNOTE']), -2, 2) == 'CN')
				    $r['RFIMPP'] = 0;//$r['RFIMPP'] * (-1);
				    
				$ar_new['detratto']=$r['RFIMPP'];
				$ar_new['data']=$r['RFDTRG'];
				$ar_new['liv'] = 'liv_4';
				$ar_new['leaf']=true;
				$ar_new['caparra']=(float)$r['TFIMP5'];
				$ar_new['stato_out']= $r['RFCAUS'];
				$ar[] = $ar_new;
		
			}
		}	
		
		

  
 		 return $ar;
}