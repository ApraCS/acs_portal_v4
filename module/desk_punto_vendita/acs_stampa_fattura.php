<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

function get_obj_attachment_fattura($cod_cli, $k_fattura){
    global $cfg_mod_DeskPVen;
    //0013942/FTCLI-6-001015-CF1-2018-MIDI-900013942-08-06-2018-F.PDF (fattura)
    //900016568/DTCLI-6-000130-BT1-2018-MIDI-900016568-07-11-2018-F.PDF
    $ar_fattura = explode('_', $k_fattura);
    $tmp_fatt_path = implode('-', array(
        trim($ar_fattura[0]), //ditta
        trim($ar_fattura[4]), //ordine
        trim($ar_fattura[2]), //indice numerazione
        $ar_fattura[3] //anno
    ));
    
    $img_dir = $cfg_mod_DeskPVen['allegati_root_C'] . trim($cod_cli);
    $search_path = "{$img_dir}/*-{$tmp_fatt_path}-*";
    
    /*$ret = array();
    foreach (glob($search_path) as $filename)
    {
        $nome_file_ar = explode('/', $filename);
        $nome_file = end($nome_file_ar);
        $str_file = explode('-', $nome_file);
        $ret[$str_file[0]] = $filename;
    }*/
    
    return glob($search_path);
    
}


/*************************************************************************
 * GET JSON DATA DOCUMENTI
 *************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data_documenti'){
	$ret = array();
	$ord = $s->get_ordine_by_k_docu($m_params->k_ordine);
	
	if ($m_params->tipo_doc == 'B')
		$tipologia = 'VD';
	else 
		$tipologia = 'VF';
	
	$sql = "SELECT * FROM  {$cfg_mod_DeskPVen['fatture_anticipo']['file_testate']} 
			WHERE TFDT = ? AND TFTIDO = ? AND TFDOAB = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $tipologia, $m_params->k_ordine));
	
	while ($r = db2_fetch_assoc($stmt)) {
		$r['DES'] = implode("_", array($r['TFAADO'], $r['TFNRDO'], $r['TFTIDO']));
		$ret[] = array_map('utf8_encode', $r);
	}
	
	echo acs_je($ret);
	exit;
}


/*************************************************************************
 * EXE STAMPA FATTURA
 *************************************************************************/
if ($_REQUEST['fn'] == 'exe_stampa_fattura'){

	$form_values=$m_params->form_values;

	$r = $s->get_ordine_by_k_docu($m_params->k_ordine);
	
	if (strlen($m_params->k_doc_da_stampare) > 0)
		$k_doc = $m_params->k_doc_da_stampare;
	else {
		if ($m_params->tipo_doc == 'B') //bolla
			$k_doc = implode("_", array($id_ditta_default, $r['TDTIDE'], $r['TDINDE'], $r['TDAADE'], sprintf("%06s", $r['TDNRDE'])));
		else
			$k_doc = implode("_", array($id_ditta_default, $r['TDTIFA'], $r['TDINFA'], $r['TDAAFA'], sprintf("%06s", $r['TDNRFA'])));
	}

	$sh = new SpedHistory();
	$sh->crea(
			'stampa_fatt_ven',
			array(
					"k_fattura" => $k_doc,
					"verbale" 	=> $form_values->f_verbale,
					"scheda" 	=> $form_values->f_scheda
				)
			);

	if($m_params->from_dclog)
	   $obj = get_obj_attachment_fattura($m_params->intestatario, $m_params->k_doc_da_stampare);
	else
	   $obj = "";
	    
	$ret = array();
	$ret['success'] = true;
	$ret['obj'] = $obj;
	echo acs_je($ret);
	exit;
}







/*************************************************************************
 * OPEN FORM SIMPLE (ricevo gia' il doc da stampare)
 *************************************************************************/
if ($_REQUEST['fn'] == 'open_form_simple'){
	?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
            
            items: [
						{				 
							xtype: 'checkboxgroup',
							fieldLabel: 'Verbale installazione',
							labelAlign: 'left',
							labelWidth: 130,
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_verbale' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'Y'
		                        }]														
						 }, {				 
							xtype: 'checkboxgroup',
							fieldLabel: 'Scheda trasportatore',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 130,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_scheda' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'Y'
		                        }]														
						 }
						 
            ],
            
			buttons: [					
				{
		            text: 'Stampa',
			        iconCls: 'icon-print-24',		            
			        scale: 'medium',		            
		            handler: function() {
		            
		            form=this.up('form').getForm();
		            win=this.up('window');
		            if(form.isValid()){
		            
		            	var form_p = this.up('form');

		            	id_selected = <?php echo j($m_params->k_doc_da_stampare)?>;
		        
                                 	Ext.Ajax.request({
							        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_stampa_fattura',
								        method     : 'POST',
					        			jsonData: {
					        				form_values: form.getValues(),
					        				k_doc_da_stampare: id_selected
										},							        
								        success : function(result, request){
					            			 win.close();									        
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
					    });	
	                  }
	             
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}





/*************************************************************************
 * OPEN FORM (con grid per scelta fattura)
 *************************************************************************/
if ($_REQUEST['fn'] == 'open_form'){
	$initial_data_txt = $s->get_initial_calendar_date();

	?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
            
            items: [
						{				 
							xtype: 'checkboxgroup',
							fieldLabel: 'Verbale installazione',
							labelAlign: 'left',
							labelWidth: 130,
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_verbale' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'Y'
		                        }]														
						 }, {				 
							xtype: 'checkboxgroup',
							fieldLabel: 'Scheda trasportatore',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 130,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_scheda' 
		                          , boxLabel: ''
		                          , checked: false
		                          , inputValue: 'Y'
		                        }]														
						 }
						 
						 
					, {					
						xtype: 'grid',
						flex: 1,
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},
									
						selModel: {selType: 'checkboxmodel', mode: 'SINGLE'},				
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_documenti',
			        			extraParams: {
			        				k_ordine: <?php echo j($m_params->k_ordine) ?>,
			        				tipo_doc: <?php echo j($m_params->tipo_doc) ?>
								},								
								method: 'POST',
								type: 'ajax',
					
								actionMethods: {read: 'POST'},
								doRequest: personalizza_extraParams_to_jsonData,														
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['TFDOCU', 'TFDTRG', 'DES']										
										
						}, //store
			
						columns: [
							{header: 'Documento', dataIndex: 'DES', flex: 1},
							{header: 'Data', dataIndex: 'TFDTRG', width: 90, renderer: date_from_AS}
						]
						 
					}						 
						 
						 
						 
						 
            ],
            
			buttons: [					
				{
		            text: 'Stampa',
			        iconCls: 'icon-print-24',		            
			        scale: 'medium',		            
		            handler: function() {
		            
		            form=this.up('form').getForm();
		            win=this.up('window');
		            if(form.isValid()){
		            
		            	var form_p = this.up('form');
						var tab_tipo_stato = form_p.down('grid');
						
						if (Ext.isEmpty(tab_tipo_stato.getSelectionModel().getSelection())){
							acs_show_msg_error('Selezionare il documento da stampare');
							return false;
						}
						
						
		            	id_selected = tab_tipo_stato.getSelectionModel().getSelection()[0].data.TFDOCU;
		        
                                 	Ext.Ajax.request({
							        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_stampa_fattura',
								        method     : 'POST',
					        			jsonData: {
					        				form_values: form.getValues(),
					        				k_ordine: <?php echo j($m_params->k_ordine) ?>,
					        				k_doc_da_stampare: id_selected,
					        				tipo_doc: <?php echo j($m_params->tipo_doc) ?>
										},							        
								        success : function(result, request){
					            			 win.close();									        
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
					    });	
	                  }
	             
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}
?>