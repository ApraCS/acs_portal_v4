<?php
require_once "../../config.inc.php";
require_once("acs_panel_ins_new_anag.php");
		
$main_module = new DeskGest();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_GEST'));

$m_params = acs_m_params_json_decode();	

function format_periodo($periodo){
    
    $format = "";
    
   if(strlen($periodo) == 6){
       $format = ucfirst(print_date($periodo . '01', "%b.'%y"));
   }elseif(strlen($periodo) > 6){
       $format = ucfirst(print_date($periodo, "%d/%m"));
  }else{
        $format = $periodo;
  }
    
    return $format;
}


function _sql_where_by_periodo($params){
    
    //$tipo_periodo = 'ANNO', $data_riferimento
    if (!isset($params->tipo_periodo))
        $tipo_periodo = 'ANNO';
    else
        $tipo_periodo = $params->tipo_periodo;
    
    if (!isset($params->form_values->data_riferimento))
      $data_riferimento = oggi_AS_date();
    else
      $data_riferimento = $params->form_values->data_riferimento;
                

    //print_date($data, $format="%d/%m/%y")
    $data_iniziale_strtotime = print_date($data_riferimento, "%Y-%m-%d");
    
    if ($tipo_periodo == 'MESE') {
        $sql_where_by_periodo = ' AND substr(CFDTGE, 1, 6)>=' . date('Ym', strtotime("{$data_iniziale_strtotime} -13 month"));
    }elseif ($tipo_periodo == 'GIORNO') {
        $sql_where_by_periodo = ' AND substr(CFDTGE, 1, 8)>=' . date('Ymd', strtotime("{$data_iniziale_strtotime} -28 day"));
    }
     else { //anno (default)
       // $sql_where_by_periodo = ' AND substr(ARDTGE, 1, 4)>=' . date('Y', strtotime("{$data_iniziale_strtotime} -12 year"));
    }
    $sql_where_by_periodo .= " AND CFDTGE <= " . $data_riferimento;
    return $sql_where_by_periodo;
}


//******************************************
//  GRAFICO: DATI: TIPO DI RIORDINO PER ANNO
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart'){
    
    if ($m_params->tipo_periodo == 'MESE') {
        $sql_field_periodo = "SUBSTR(CFDTGE, 1, 6)";
    } elseif ($m_params->tipo_periodo == 'GIORNO') {
        $sql_field_periodo = "SUBSTR(CFDTGE, 1, 8)";
    }
    else { //anno (default)
        $sql_field_periodo = "SUBSTR(CFDTGE, 1, 4)";
    }
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    
    
    $area_manager = $m_params->open_request->form_open->CCARMA;
    $agente = $m_params->open_request->form_open->CCAGE;
    
    if(count($area_manager) > 0 || count($agente) > 0 ){
        
        $where = '';
        if(count($area_manager) > 0)
            $where .= sql_where_by_combo_value('CCARMA', $area_manager);
        if(count($agente) > 0){
            if(count($agente) == 1)
                $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
            if(count($agente) > 1)
                $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
        }
            
        $sql_join = " INNER JOIN (
        SELECT CCPROG
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
        WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
        ON CC.CCPROG = GC.GCPROG";
    }
    
    $sql_where = sql_where_params($m_params->open_request->form_open);

    $sql = "SELECT (CASE WHEN GCNAZI='IT' THEN 'ITA' ELSE 'EST' END) AS NAZI,
            {$sql_field_periodo} AS PERIODO, COUNT(*) AS NUM_ART
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            {$sql_join}
	        LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
			  ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
            WHERE GCDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            GROUP BY  CASE WHEN GCNAZI='IT' THEN 'ITA' ELSE 'EST' END, {$sql_field_periodo}
            ORDER BY PERIODO
            ";
             
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ar_records = array();
    $ar_tipo = array();
    $ar_fields = array();
    while ($r = db2_fetch_assoc($stmt)){
        if (isset($ar_records[($r['PERIODO'])]) === 'undefined'){
              $ar_records[($r['PERIODO'])] = array();
        }
               
        $ar_records[($r['PERIODO'])]['PERIODO'] =  format_periodo($r['PERIODO']);
        $ar_records[($r['PERIODO'])]['TOTALE'] += $r['NUM_ART'];
        $ar_records[($r['PERIODO'])][($r['NAZI'])] = $r['NUM_ART'];
        
        if (!(in_array($r['NAZI'], $ar_tipo))){
            array_push($ar_tipo, $r['NAZI']);
            array_push($ar_fields, $r['NAZI']);
        }
    }
    
    array_push($ar_fields, "PERIODO");
    array_push($ar_fields, "TOTALE");        
    
    $ar_ret = array();
    foreach($ar_records as $ar1){
        for($i=0; $i<count($ar_tipo); $i++){
            if (!($ar1[($ar_tipo[$i])] > 0))
                $ar1[($ar_tipo[$i])] = 0;
        }
        $ar_ret[] = $ar1;
    }
    
    $fields = acs_je($ar_fields);
    echo acs_je($ar_ret);
    exit;
}


//******************************************
//  GRAFICO: DATI: CICLO DI VITA
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_ciclo_di_vita'){
    
  $sql_where_by_periodo = _sql_where_by_periodo($m_params);
  
  $area_manager = $m_params->open_request->form_open->CCARMA;
  $agente = $m_params->open_request->form_open->CCAGE;
  
  if(count($area_manager) > 0 || count($agente) > 0 ){
      
      $where = '';
      if(count($area_manager) > 0)
          $where .= sql_where_by_combo_value('CCARMA', $area_manager);
      if(count($agente) > 0){
          if(count($agente) == 1)
              $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
          if(count($agente) > 1)
              $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
      }
      
      $sql_join = " INNER JOIN (
      SELECT CCPROG
      FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
      WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
      ON CC.CCPROG = GC.GCPROG";
  }
  
    
  $sql_where = sql_where_params($m_params->open_request->form_open);
  
  $sql = "SELECT GCSOSP, GCCIVI, COUNT(*) AS NREF
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
        {$sql_join}
        LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
		ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
        WHERE GCDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
        GROUP BY GCSOSP, GCCIVI
        ORDER BY GCSOSP, GCCIVI
        ";
            
  $stmt = db2_prepare($conn, $sql);
  $result = db2_execute($stmt);
        
  $ret = array();
  $ret_tmp = array();
  while ($r = db2_fetch_assoc($stmt)) {
    if ($r['GCSOSP'] == 'S')
      $ret_tmp['S'] += $r['NREF'];
    else
      $ret_tmp[$r['GCCIVI']] += $r['NREF'];
  }
            
            function decod_ciclo_vita($v){
                switch(trim($v)){
                    case '001':     return 'Preventivazione';
                    case '002': 	return 'Acquisizione';
                    case '003': 	return 'Codifica';
                    case '004': 	return 'Inserimento';
                    case '005': 	return 'Regime';
                    case '006': 	return 'Sviluppo';
                    case '007': 	return 'Uscita';
                    default: return $v;
                }
            }
            
            function decod_k_ciclo_vita($v){
                switch(trim($v)){
                    case '': 	return 'Attivi';
                    case 'S': 	return 'Sospesi';
                    default: return $v;
                }
            }
            
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("des_name" => decod_ciclo_vita($k), "name" => decod_k_ciclo_vita($k), "referenze" => $ar);
    }
    
    echo acs_je($ret);
    exit;
}




//******************************************
//  GRAFICO: AGENTE
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_agente'){
    
  $sql_where_by_periodo = _sql_where_by_periodo($m_params);
  $sql_where = sql_where_params($m_params->open_request->form_open);
   
  $agente = $m_params->open_request->form_open->CCAGE;
  $area_manager = $m_params->open_request->form_open->CCARMA;
  if(count($agente) > 0)
      $sql_where .= sql_where_by_combo_value('CCAG1', $agente);
  if(count($area_manager) > 0)
      $sql_where .= sql_where_by_combo_value('CCARMA', $area_manager);

  $sql = "SELECT CCAG1, COUNT(*) AS NREF
          FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
          INNER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
            ON CCDT = GCDT AND CCPROG = GCPROG AND CCSEZI = 'BCCO' AND CCCCON = 'STD' 
          LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
		    ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
          WHERE GCDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
          GROUP BY CCAG1
          ORDER BY CCAG1
            ";
  
  $stmt = db2_prepare($conn, $sql);
  $result = db2_execute($stmt);

  $ret = array();
  $ret_tmp = array();
  while ($r = db2_fetch_assoc($stmt)) {
    $ret_tmp[$r['CCAG1']] += $r['NREF'];
  }
            
  function decod_agente($v){
      $agente = get_TA_sys('CUAG', trim($v));
      return trim($agente['text']);
  }
  
            
  foreach($ret_tmp as $k => $ar){
      $ret[] = array("name" => decod_agente($k), "referenze" => $ar);
  }
  echo acs_je($ret);
  exit;
}


//******************************************
//  GRAFICO: LISTINO
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_listino'){
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    $sql_where = sql_where_params($m_params->open_request->form_open);
    
    $area_manager = $m_params->open_request->form_open->CCARMA;
    $agente = $m_params->open_request->form_open->CCAGE;
    if(count($area_manager) > 0 || count($agente) > 0 ){
        
        $where = '';
        if(count($area_manager) > 0)
            $where .= sql_where_by_combo_value('CCARMA', $area_manager);
        if(count($agente) > 0){
            if(count($agente) == 1)
                $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
            if(count($agente) > 1)
                $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
        }
            
        $sql_join = " INNER JOIN (
        SELECT CCPROG
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
        WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
        ON CC.CCPROG = GC.GCPROG";
    }
    
    $sql = "SELECT GCLIST, COUNT(*) AS NREF
    FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
    {$sql_join}
    LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
		ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
    WHERE GCDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
    GROUP BY GCLIST
    ORDER BY GCLIST
    ";
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['GCLIST']] += $r['NREF'];
    }
    
    function decod_tipo_riordino($v){
        switch(trim($v)){
            case 'L':	return 'Listino';
            default: return $v;
        }
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_tipo_riordino($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}

//******************************************
//  GRAFICO: PROVINCIA
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_provincia'){
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    $sql_where = sql_where_params($m_params->open_request->form_open);
    
    $area_manager = $m_params->open_request->form_open->CCARMA;
    $agente = $m_params->open_request->form_open->CCAGE;

    if(count($area_manager) > 0 || count($agente) > 0 ){
            
        $where = '';
        if(count($area_manager) > 0)
            $where .= sql_where_by_combo_value('CCARMA', $area_manager);
        if(count($agente) > 0){
            if(count($agente) == 1)
                $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
            if(count($agente) > 1)
                $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
        }
                
        $sql_join = " INNER JOIN (
        SELECT CCPROG
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
        WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
        ON CC.CCPROG = GC.GCPROG";
        }
    
    
    
    $sql = "SELECT GCPROV,
          /* (CASE WHEN GCPROV <> '' THEN 'PROV' ELSE 'NAZI' END) AS TIPO,*/
           (CASE WHEN GCPROV <> '' THEN GCPROV ELSE GCNAZI END) AS PROV_NAZ, 
            COUNT(*) AS NREF
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            {$sql_join}
            LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
            	ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
            WHERE GCDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            GROUP BY GCPROV, CASE WHEN GCPROV <> '' THEN GCPROV ELSE GCNAZI END
            ORDER BY GCPROV, CASE WHEN GCPROV <> '' THEN GCPROV ELSE GCNAZI END
            ";
   
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['PROV_NAZ']] += $r['NREF'];
    }
    
    function decod_provincia($v){
        global $s;
            $row = $s->get_TA_std('ANPRO', $v, null, 'Y');
            $decod = $row['TADESC'];
            if(is_null($decod)){
                $row = get_TA_sys('BNAZ', trim($v));
                $decod =  trim($row['text']);
            }
               
        return $decod;
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_provincia($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}


//******************************************
//  GRAFICO: DATI: UTENTE DI IMMISSIONE
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_utente_immissione'){
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    $sql_where = sql_where_params($m_params->open_request->form_open);
    
    $area_manager = $m_params->open_request->form_open->CCARMA;
    $agente = $m_params->open_request->form_open->CCAGE;
    
    if(count($area_manager) > 0 || count($agente) > 0 ){
        
        $where = '';
        if(count($area_manager) > 0)
            $where .= sql_where_by_combo_value('CCARMA', $area_manager);
        if(count($agente) > 0){
            if(count($agente) == 1)
                $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
            if(count($agente) > 1)
                $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
        }
            
            $sql_join = " INNER JOIN (
            SELECT CCPROG
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
            WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
            ON CC.CCPROG = GC.GCPROG";
    }
    
    $sql = "SELECT CFUSGE, COUNT(*) AS NREF
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            {$sql_join}
          	LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
			  ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
            WHERE GCDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            GROUP BY CFUSGE
            ORDER BY CFUSGE
            ";
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['CFUSGE']] += $r['NREF'];
    }
    
    function decod_utente_immissione($v){
        return $v;
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_utente_immissione($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}

//******************************************
//  GRAFICO: NAZIONE
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_nazione'){
    
    $sql_where_by_periodo = _sql_where_by_periodo($m_params);
    $sql_where = sql_where_params($m_params->open_request->form_open);
    
    $area_manager = $m_params->open_request->form_open->CCARMA;
    $agente = $m_params->open_request->form_open->CCAGE;
    if(count($area_manager) > 0 || count($agente) > 0 ){
        
        $where = '';
        if(count($area_manager) > 0)
            $where .= sql_where_by_combo_value('CCARMA', $area_manager);
        if(count($agente) > 0){
            if(count($agente) == 1)
                $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
            if(count($agente) > 1)
                $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
        }
        
        $sql_join = " INNER JOIN (
        SELECT CCPROG
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
        WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
        ON CC.CCPROG = GC.GCPROG";
    }
    
    $sql = "SELECT GCNAZI, COUNT(*) AS NREF
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
            {$sql_join}
            LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
			  ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
            WHERE GCDT = '{$id_ditta_default}' {$sql_where} {$sql_where_by_periodo}
            GROUP BY GCNAZI
            ORDER BY GCNAZI
            ";
    //print_r($sql); exit;
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret_tmp = array();
    while ($r = db2_fetch_assoc($stmt)) {
        $ret_tmp[$r['GCNAZI']] += $r['NREF'];
    }
    
    function decod_nazione($v){
        $naz = get_TA_sys('BNAZ', trim($v));
        return trim($naz['text']);
    }
    
    foreach($ret_tmp as $k => $ar){
        $ret[] = array("name" => decod_nazione($k), "referenze" => $ar);
    }
    echo acs_je($ret);
    exit;
}


//i dati di codifica vengono elaborati direttamente qui
$sql_where = sql_where_params($m_params->form_open);
$area_manager = $m_params->form_open->CCARMA;
$agente = $m_params->form_open->CCAGE;

if(count($area_manager) > 0 || count($agente) > 0 ){
    
    $where = '';
    if(count($area_manager) > 0)
        $where .= sql_where_by_combo_value('CCARMA', $area_manager);
    if(count($agente) > 0){
        if(count($agente) == 1)
            $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
        if(count($agente) > 1)
            $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
    }
        
        $sql_join = " INNER JOIN (
        SELECT CCPROG
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
        WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
        ON CC.CCPROG = GC.GCPROG";
}

$sql = "SELECT (CASE WHEN GCNAZI='IT' THEN 'ITA' ELSE 'EST' END) AS NAZI,
        SUBSTR(CFDTGE, 1, 4) AS ANNO, COUNT(*) AS NUM_ART 
        FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
        {$sql_join}
	    LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
			  ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
        WHERE 1=1 {$sql_where}
        GROUP BY CASE WHEN GCNAZI='IT' THEN 'ITA' ELSE 'EST' END, SUBSTR(CFDTGE, 1, 4)
        ORDER BY ANNO
  ";
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$ar_records = array();
$ar_tipo = array();
$ar_fields = array();
while ($r = db2_fetch_assoc($stmt)){	
	if (isset($ar_records[($r['PERIODO'])]) === 'undefined'){
		$ar_records[($r['PERIODO'])] = array();    		 		
	}
	$ar_records[($r['PERIODO'])]['PERIODO'] = $r['PERIODO'];
	$ar_records[($r['PERIODO'])]['TOTALE'] += $r['NUM_ART'];
	$ar_records[($r['PERIODO'])][($r['NAZI'])] = $r['NUM_ART'];
	
	if (!(in_array($r['NAZI'], $ar_tipo))){
		array_push($ar_tipo, $r['NAZI']);
		array_push($ar_fields, $r['NAZI']);
	} 
}

array_push($ar_fields, "PERIODO");
array_push($ar_fields, "TOTALE");



$ar_ret = array();
foreach($ar_records as $ar1){
	for($i=0; $i<count($ar_tipo); $i++){
		if (!($ar1[($ar_tipo[$i])] > 0))
			$ar1[($ar_tipo[$i])] = 0;
	}
	$ar_ret[] = $ar1;
}


$tipo_appr = acs_je($ar_ret);
$fields = acs_je($ar_fields);

?>


{"success":true, "items": [
		 {
			xtype: 'container',
			title: '',
	 		layout: {
				type: 'hbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [
			
			
				{			
				xtype: 'panel',
				
				buttons: [
				
				{
					xtype: 'form',
	            	bodyStyle: 'padding: 10px',
	            	bodyPadding: '5 5 0',
	            	frame: true,
	            	autoScroll : true,
	            	title: '',
					items: [
        				{
        				   xtype: 'datefield'
        				   , startDay: 1 //lun.
        				   , fieldLabel: 'Data iniziale'
        				   , name: 'data_riferimento'
        				   , format: 'd/m/Y'
        				   , submitFormat: 'Ymd'
        				   , allowBlank: true
        				   , anchor: '-15'
        				   , value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
        				   , listeners: {
        				       invalid: function (field, msg) {
        				       Ext.Msg.alert('', msg);}				       
        				   }
        				}
        			]
        		}										
				
				
				,{ xtype: 'tbfill' },
				  {
			            text: 'Anni',
			            scale: 'medium',	                     
			            handler: function() {
			            	var gs = this.up('window').query('chart');
			            	var form = this.up('container').down('form').getForm();
			            	Ext.each(gs, function(g) {
			            			var st = g.getStore();
			            			st.proxy.extraParams.tipo_periodo = 'ANNO';
			            			st.proxy.extraParams.form_values = form.getValues();
									st.load();
					        	});
			            }
			       }, {
			            text: 'Ultimi 13 mesi',
			            scale: 'medium',	                     
			            handler: function() {
			            	var gs = this.up('window').query('chart');
			            	var form = this.up('container').down('form').getForm();
			            	Ext.each(gs, function(g) {
			            			var st = g.getStore();
			            			st.proxy.extraParams.tipo_periodo = 'MESE';
			            			st.proxy.extraParams.form_values = form.getValues();									
									st.load();
					        	});			            
			            }
			       }, {
			            text: 'Ultime 4 settimane',
			            scale: 'medium',	                     
			            handler: function() {
			            	var gs = this.up('window').query('chart');
			            	var form = this.up('container').down('form').getForm();
			            	Ext.each(gs, function(g) {
			            			var st = g.getStore();
			            			st.proxy.extraParams.tipo_periodo = 'GIORNO';
			            			st.proxy.extraParams.form_values = form.getValues();									
									st.load();
					        	});			            
			            }
			       }		 
				],
				title: '',
				flex: 25,
				layout: 'fit',
				items: [
				
				
				
				 {
					xtype: 'chart',
					id: 'chartCmp2',
					animate: true,
					shadow: true,
					flex: 0.8, 
					height: 300,
					legend: {
						position: 'bottom'
					},
					
					listeners: {					
			 			afterrender: function (comp) {
			 				Ext.getBody().unmask();
			 			}
			 		},						
					
					
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									type: 'json',
									read: 'POST'
								},
					
								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
								
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'							
								},
								doRequest: personalizza_extraParams_to_jsonData
								
							},
		
							fields: <?php echo acs_je($ar_fields) ?>,
										
						}, //store					
					
										
					store222222: {
						fields: <?php echo ($fields) ?>,
						data: <?php echo($tipo_appr)?>
					},					
					axes: [{
						type: 'Numeric',
						position: 'left',
						fields: ['TOTALE'],
						title: 'Nr clienti IMMESSI - Italia/Estero',
						grid: true
					}, {
						type: 'Category',
						position: 'bottom',
						fields: ['PERIODO'],
						title: false
					}],
					series: [{
						type: 'column',
						axis: 'left',
						gutter: 80,
						xField: ['PERIODO'],
						yField: ['TOTALE'],
						stacked: true,
							tips: {
								trackMouse: true,
								width: 65,
								height: 28,
								renderer: function(storeItem, item) {
									this.setTitle(String(item.value[1]));
								}
							}						
					}, 
					<?php
					
					for($i=0; $i<count($ar_tipo); $i++){
						echo "
						{
							type: 'line',
							axis: 'left',
							smooth: true,								
							xField: ['PERIODO'],
							yField: ['$ar_tipo[$i]'],
							tips: {
								trackMouse: true,
								width: 65,
								height: 28,
								renderer: function(storeItem, item) {
									this.setTitle(String(item.value[1]));
								}
							}
						}, 
						"; 
					}?>
					]
				}
				
				]
			}
			
		, {
			xtype: 'container',
			width: 400,
			title: '',
	 		layout: {
				type: 'vbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [
					{
					 xtype:  'tabpanel',
					 height: 300,
					 items: [
                        <!-- --------------------- -->
                        <!-- grafico ciclo di vita -->
                        <!-- --------------------- -->  
    					{
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Ciclo di vita',					 
    					 items: 	  				  		
    						{
    			            xtype: 'chart',
    			            animate: true,
    			            id: 'classi_giacenze_chart_ciclo_vita',
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_ciclo_di_vita',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    								
    							},
    		
    							fields: ['name', 'des_name', 'referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			            	field: 'des_name',
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('des_name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'des_name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial',
    								renderer: function (label){
    									// this will change the text displayed on the pie
    									var cmp = Ext.getCmp('classi_giacenze_chart_ciclo_vita'); // id of the chart
    									var index = cmp.store.findExact('des_name', label); // the field containing the current label
    									var data = cmp.store.getAt(index).data;
    									return data.name; // the field containing the label to display on the chart
    								}			                    
    			                }
    			            }]
    			        }	  			
    	  			}
                        <!-- ---------------------------------- -->
                        <!-- grafico per tipo approvigionamento -->
                        <!-- ---------------------------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Listino',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'listino',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_listino&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			}   <!-- ---------------------------------- -->
                        <!-- grafico per tipo approvigionamento -->
                        <!-- ---------------------------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Province',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_tipo_parte',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_provincia&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			         /*   legend: {
    			                position: 'right'
    			            },*/
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end tipo parte -->
				] <!-- end items tab panel  -->
			   } <!-- end tab panel  -->
				  , {
					 xtype:  'tabpanel',
					 height: 300,
					 items: [
					    <!-- ------------------------- -->
                        <!-- grafico per tipo riordino -->
                        <!-- ------------------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Agente',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_tipo_riordino',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_agente&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end tipo riordino -->
                        <!-- ----------------- -->
                        <!-- utente immissione -->
                        <!-- ----------------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Utente immissione',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_utente_immissione',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_utente_immissione&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			            legend: {
    			                position: 'right'
    			            },
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end utente immissione -->				
                        <!-- --------- -->
                        <!-- Fornitore -->
                        <!-- --------- -->  
    				  , {
    					 xtype: 'panel',
    					 layout: 'fit',
    					 flex: 1,
    			         title: 'Nazione',					 
    					 items:	  				  			
    					  {
    			            xtype: 'chart',
    			            animate: true,
    			            width: 200,
    			            id: 'classi_giacenze_chart_fornitore',			            
    
    						store: {
    							xtype: 'store',
    							autoLoad:true,	
    							proxy: {
    								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_nazione&from_ws=N',
    								method: 'POST',
    								type: 'ajax',
    					
    								//Add these two properties
    								actionMethods: {
    									type: 'json',
    									read: 'POST'
    								},
    					
    								extraParams: {open_request: <?php echo acs_raw_post_data(); ?>},
    								
    								reader: {
    									type: 'json',
    									method: 'POST',
    									root: 'root'							
    								},
    								doRequest: personalizza_extraParams_to_jsonData
    							},
    		
    							fields: ['name','referenze']
    										
    						}, //store			            
    			            
    			            
    			            shadow: true,
    			          
    			            insetPadding: 10,
    			            theme: 'Base:gradients',
    			            series: [{
    			                type: 'pie',
    			                field: 'referenze',
    			                showInLegend: true,
    			                donut: true,
    			                tips: {
    			                  trackMouse: true,
    			                  width: 200,
    			                  height: 28,
    			                  renderer: function(storeItem, item) {
    			                    //calculate percentage.
    			                    var total = 0;
    			                    storeItem.store.each(function(rec) {
    			                        total += rec.get('referenze');
    			                    });
    			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
    			                  }
    			                },
    			                highlight: {
    			                  segment: {
    			                    margin: 20
    			                  }
    			                },
    			                label: {
    			                    field: 'name',
    			                    display: 'rotate',
    			                    contrast: true,
    			                    font: '11px Arial'
    			                }
    			            }]
    			        }	 	  			
    	  			} <!-- end utente fornitore -->				
					 
				] <!-- end items tab panel  -->
			   } <!-- end tab panel  -->
			
			]
		}				
			
			]
        }
      
	]
}