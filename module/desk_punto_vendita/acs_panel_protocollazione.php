<?php

require_once("../../config.inc.php");
require_once "acs_panel_protocollazione_include.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$mod_provenienza = new DeskPVen();
$cod_mod_provenienza = $mod_provenienza->get_cod_mod();

/* ESEMPIO DI FILE BAT
 ----------------------------------------
 @echo off
 k:
 cd k:\
 cd k:\Apra\Apra\Sma1Bar

 set /p parametri_sma1bar=< %1%

 Sma1Bar.exe %parametri_sma1bar%
 ----------------------------------------
 */


function prepare_grid_row($row){
	$nr = array();
	$nr['tddocu']	        = trim($row['TDDOCU']);
	$nr['f_cliente_des']	= trim($row['TDDCON']);
	$nr['f_cliente_cod']	= trim($row['TDCCON']);
	$nr['nrdo']			    = trim($row['TDONDO']);
	
	$nr['f_destinazione_des']	    = trim($row['TDDDES']);
	$nr['out_loc_dest']	            = trim($row['TDDLOC']);
	
	if($row['TDHMIM']== 0){
		$nr['ora_generazione']	        = $row['TDORGE'];
	}else{
		$nr['ora_generazione']	        = $row['TDHMIM'];
	}
	
	$nr['vsrf']	      = trim($row['TDVSRF']);
	$nr['liv']	      = 'liv_totale';
	//$nr['dtrg']	      = trim($row['TDODRE']);
	$nr['dtrg']	      = trim($row['TDDTIM']);
	$nr['stdo']	      = trim($row['TDSTAT']);
	$nr['tpdo_des']	  = trim($row['TDDOTD']);
	$nr['tpdo']	      = trim($row['TDOTPD']);
	$nr['mode_des']	  = decode_PUVN_code('MOD', trim($row['TDCVN1']));
	
    return $nr;
}


// ******************************************************************************************
// exe protocollazione
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_protocollazione'){
	ini_set('max_execution_time', 3000);
	$pt = new SpedProtocollazionePVen();
	$result = $pt->exe_protocollazione(acs_m_params_json_decode());
	echo $result;
	exit();	
}


// ******************************************************************************************
// duplica preventivo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_duplica_preventivo'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();
	
	$sh = new SpedHistory();
	$num_creato = $sh->crea(
			'duplica_preventivo',
			array(
					"k_ordine"			=> $m_params->k_ordine					
				)
			);
	
	$ret = array();
	$ret['success'] = true;
	$ret['num_creato'] = trim($num_creato);
	echo acs_je($ret);
	exit();
}

// ******************************************************************************************
// duplica preventivo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_converti_in_ordine'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();

	$ord = $s->get_ordine_by_k_docu($m_params->k_ordine);
	$anagrafica_cliente_con_dati_completi = $main_module->anagrafica_cliente_con_dati_completi($ord['TDCCON']);
	if ($anagrafica_cliente_con_dati_completi['success'] == false){
	    $ret = $anagrafica_cliente_con_dati_completi;
	    echo acs_je($ret);
	    return;
	}
	
	
	
	
	$sh = new SpedHistory();
	$num_creato = $sh->crea(
			'preventivo_to_ordine',
			array(
					"k_ordine"			=> $m_params->k_ordine
			)
			);
	$ret = array();
	$ret['success'] = true;
	$ret['num_creato'] = trim($num_creato);
	echo acs_je($ret);
	exit();
}



// ******************************************************************************************
// duplica preventivo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_crea_ordine_assistenza'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();

	$sh = new SpedHistory();
	$ret_RI = $sh->crea(
			'crea_ordine_assistenza',
			array(
					"k_ordine"			=> $m_params->k_ordine
			)
			);
	$ret = array();
	$ret['success'] = true;
	$ret['num_creato'] = trim($ret_RI['RIAUTO']);
	echo acs_je($ret);
	exit();
}




if ($_REQUEST['fn'] == 'get_grid_data_find_order'){
	$m_params = acs_m_params_json_decode();	
	$sql_where = sql_where_by_doc_type($m_params->doc_type);
	
	$sql= "SELECT TD.* FROM {$cfg_mod_DeskPVen['file_testate']} TD
			WHERE " . $s->get_where_std() . " 
			  AND TDOADO = " . $m_params->form_values->f_anno . " 
			  AND TDONDO = " . sql_t($m_params->form_values->f_numero) . 
			  $sql_where . "			  
			/* AND TD.TDUSOE = " . sql_t($auth->get_user()) . "*/
			";
	
	//echo $sql;
	$stmt= db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row= db2_fetch_assoc($stmt)){	
		$nr = prepare_grid_row($row);
		$ar[] = $nr;
	};
	
	
	echo acs_je($ar);
	
	exit;
}



if ($_REQUEST['fn'] == 'get_grid_data_last_orders'){
	$m_params = acs_m_params_json_decode();

/*	
	$sql= "SELECT TD.*, TA_TIPO.TADESC AS TIPOL
		FROM {$cfg_mod_DeskPVen['file_testate']} TD
	 		LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tab_sys']} TA_TIPO 
	 			ON TD.TDDT=TA_TIPO.TADT AND TA_TIPO.TAID='PUVN' AND TA_TIPO.TACOR2 = 'MOD'  
		WHERE TD.TDUSOE = " . sql_t($auth->get_user()) . "		
	FETCH FIRST 30 ROWS ONLY";
*/
	
	$sql_where = '';

	//tipo ordine (ORD o PREV)
	$sql_where .= sql_where_by_doc_type($m_params->doc_type);
	
	$sql_where .= " AND TDSTAB = '" . $main_module->get_stabilimento_in_uso() . "'";
	
	$sql= "SELECT TD.*
		FROM {$cfg_mod_DeskPVen['file_testate']} TD
		WHERE " . $s->get_where_std() . $sql_where . " 
		/* AND TD.TDUSOE = " . sql_t($auth->get_user()) . "*/
		ORDER BY TDDTIM DESC, TDHMIM DESC
		FETCH FIRST 30 ROWS ONLY";

	$stmt= db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row= db2_fetch_assoc($stmt)){		
		//per la tipologia ambiente left outer join con TA0
	
		$nr = prepare_grid_row($row);		
		$ar[] = $nr;

	};
	

	echo acs_je($ar);
	
	exit;
}


// ******************************************************************************************
// download file .bat per esecuzione sma1bar
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_bat_file'){

	header("Content-Description: Sma1bar open file");
	header("Content-Type: application/octet-stream");
	header("Content-disposition: attachment; filename=\"abcde.sma1bar\"");

	$nrdo = $_REQUEST['nrdo'];
	$nrdo_1 = substr($nrdo, 0, 2);
	$nrdo_2 = substr($nrdo, 2, 2);

	//echo "ORD1224;44444;13;14;15;ORD";
	echo implode(";", array($nrdo, "NWO", $nrdo, $nrdo_1, $nrdo_2, $nrdo));
	exit;
}



// ******************************************************************************************
// EXE modifica info testata (pagamento)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_info_testata'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= $id_ditta_default; //ditta
	$cl_p .= str_repeat("0", 97); //non servono tutti gli altri parametri
	$cl_p .= sprintf("%-50s", trim($m_params->TDDOCU));
	$cl_p .= sprintf("%-1s", 'M'); //modifica
	$cl_p .= sprintf("%-3s", trim($m_params->f_pagamento));
	$cl_p .= sprintf("%-30s", ''); //progetto grafico
	$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_invio_conferma));
	$cl_p .= sprintf("%-1s", ''); //trim($m_params->f_attesa_conferma));
	$cl_p .= sprintf("%-1s", trim($m_params->f_preferenza));
	$cl_p .= sprintf("%-10s", trim(substr($auth->get_user(), 0, 10)));

	$cl_p .= sprintf("%012d", $m_params->f_acconto) . '000'; //acconto 12+3
	$cl_p .= sprintf("%012d", $m_params->f_caparra) . '000'; //caparra 12+3
	$cl_p .= sprintf("%012d", $m_params->f_imp_trasp) . '000'; //caparra 12+3
	$cl_p .= sprintf("%-3s", trim($m_params->f_referente));
	
	$cl_p .= sprintf("%-1s",  $m_params->doc_type); //O=ordine, P=preventivo
	$cl_p .= sprintf("%0-8s", $m_params->f_data_validita); //data validita
	
	$cl_p .= sprintf("%-3s", '');		   //stabilimento (qui non gestito)
	
	$cl_p .= sprintf("%0-8s", '');							//data evasione programmata (qui non gestita)
	$cl_p .= sprintf("%-3s",  $m_params->f_tipo_pag_cap);	//tipo pagamento caparra
	$cl_p .= sprintf("%-9s", '');		   //Trasportatore (qui non gestito)
	$cl_p .= sprintf("%-3s", '');			//Architetto (qui non gestito)
	//salvo in TA0 i parametri personalizzati
	
	foreach ($m_params as $p => $pv) {
		if (substr($p, 0, 7) == "f_pers_") {
			
			$id_dom = substr($p, 7);
			$f_nota = "f_nota_$id_dom";
			$ar_ins = array();
			$ar_ins['TADT'] 	= $id_ditta_default;
			$ar_ins['TATAID'] 	= 'PVNOR';
			$ar_ins['TAKEY1'] 	= $s->next_num('PVNOR');
			$ar_ins['TAINDI'] 	= $m_params->TDDOCU;
			$ar_ins['TAMAIL'] 	= $m_params->$f_nota;
			
			$ar_ins['TAKEY2'] 	= substr($p, 7); //DOMANDA
			$ar_ins['TAKEY3'] 	= $pv; //RISPOSTA

			$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
			echo db2_stmt_errormsg($stmt);			
		}
	}

	$cl_in 	= array();
	$cl_out = array();

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21H5C('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('UR21H5C', $libreria_predefinita_EXE, $cl_in, null, null);
		$tkObj->disconnect();
	}


	$ret = array();
	if ($call_return){
		$ret['success'] = true;
		$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
	} else {
		$ret['success'] = false;
	}

	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// FORM modifica info testata (pagamento, stato, ...)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'modifica_info_testata'){
	$m_params = acs_m_params_json_decode();
	$ord = $s->get_ordine_by_k_docu($m_params->tddocu);
	?>
{"success":true, "items": [
        {
            xtype: 'form',
            autoScroll: true,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            layout: {
			    type: 'vbox',
			    align: 'stretch',
			    pack : 'start',
			},
			
			
			buttons: [{
		            text: 'Salva',
		            iconCls: 'icon-windows-32',
		            scale: 'large',
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	var loc_win = this.up('window');
		            	
 						if(form.isValid()){
 						
 						
 							if (
 									parseInt(form.findField('f_caparra').getValue()) > 0 &&
 									Ext.isEmpty(form.findField('f_tipo_pag_cap').getValue())
 								){
 								acs_show_msg_error('Tipo pagamento caparra obbligatorio');
 								return false;
 							} 						
 						
	 						//Call protocollazione
							Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_info_testata',
							        timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText);
	 									loc_win.fireEvent('afterUpdateRecord', loc_win);
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							            loc_win.fireEvent('afterUpdateRecord', loc_win);
							        }
							  });			
 						}

		            }
		        }
		    ],
			
			items: [
            		{
                		xtype: 'hidden',
                		name: 'TDDOCU',
                		value: '<?php echo $ord['TDDOCU'] ?>'
                	}, {
							name: 'f_pagamento',
							xtype: 'combo',
							fieldLabel: 'Pagamento', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [<?php echo acs_ar_to_select_json(find_TA_sys('CUCP'), '') ?>] 
							},
							value: <?php echo j(trim($ord['TDCPAG'])) ?>
					}, {
								name: 'f_tipo_pag_cap',
								xtype: 'combo',
				            	anchor: '-15',
					            fieldLabel: 'Tipo pag. caparra',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								value: <?php echo j(trim($ord['TDCARP'])) ?>,
								forceSelection: true,
							   	allowBlank: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}, 'TAFG02'],
								    data: [
									     <?php echo acs_ar_to_select_json($s->find_TA_std('TPCAP', null, 'N', 'Y', null, null, null, 'N', 'Y'), '') ?>
									    ]
								},
									tpl: [
                				 '<ul class="x-list-plain">',
                                    '<tpl for=".">',
                                    '<tpl if="TAFG02 === \'S\'">' +
                                    '<li class="x-boundlist-item listItmes" style="background-color:#F9BFC1">{text}</li>',
                                    '</tpl>',
                                    '<tpl if="TAFG02 !== \'S\'">' +
                                    '<li class="x-boundlist-item listItmes">{text}</li>',
                                    '</tpl>',
                                    '</tpl>',
                                    '</ul>'
                				 ],
                				   // template for the content inside text field
                                    displayTpl: Ext.create('Ext.XTemplate',
                                           '<tpl for=".">',
                                                '{text}',
                    
                                            '</tpl>'
                                     
                                    ),
								listeners: {
				            		 beforeselect: function(combo, record, index ) {	            	
										if (record.get('TAFG02') == 'S') return false;
								
				            }
				          },
							},   
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
						/*{
		                name   : 'f_acconto',
		                xtype : 'textfield',
		                
		                //dataIndex: 'articolo',
		                flex: 1,
		                margin: "0 10 0 0",
		                fieldLabel: 'Acconto',
		                labelAlign: 'left',
		                anchor: '-15'
	                }, */
	                {
		                name   : 'f_caparra',
		                xtype : 'textfield',
		                //dataIndex: 'articolo',
		                flex: 1,
		                fieldLabel: 'Caparra',
		                labelAlign: 'left',
		                anchor: '-15'
	                }
						
						]
					 },
					 
				/*	  {
							name: 'f_preferenza',
							xtype: 'combo',
							fieldLabel: 'Preferenza', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: '', text: ''}, {id: 'A', text: 'A'}, {id: 'D', text: 'D'}
								    ] 
							},
							value: <?php echo j(trim($ord['TDUSOE'])) ?>
					}, */	
					
					{
		                name   : 'f_imp_trasp',
		                xtype : 'textfield',		                
		                fieldLabel: 'Imp. trasp.',
		                labelAlign: 'left',
		                width: '100'
	                }, {
							name: 'f_referente',
							xtype: 'combo',
							fieldLabel: 'Referente', 
							labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [<?php echo acs_ar_to_select_json(find_TA_sys('BREF'), '') ?>] 
							},
							value: <?php echo j(trim($ord['TDCORE'])) ?>
					}
				
<?php if ($m_params->doc_type == 'P') {?>		
			   ,  {
				     name: 'f_data_validita'
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data validit&agrave;'
				   , anchor: '-15'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: false
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
				   , value: '<?php echo print_date($ord['TDDTVA'], "%d/%m/%Y"); ?>'
				}
<?php } ?>				 
				
				
				, {
							xtype: 'displayfield',
							fieldLabel: 'Stato ordine',
							value: <?php echo j(trim($ord['TDDSST'])) ?>		
							
					}, {
							xtype: 'displayfield',
							fieldLabel: 'Stato cliente',
							value: <?php echo j(trim($ord['TDDSRI'])) ?>		
							
					},
					
					
					<?php 
					
					$sql="SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']}
							WHERE TADT='$id_ditta_default' AND TATAID='PRVAR' ORDER BY TAKEY1";
					
					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					
					while($row = db2_fetch_assoc($stmt)){  ?>
				
						
						
						 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
							{
							name: 'f_pers_<?php echo $row['TAKEY1']; ?>',
							value: <?php echo j(trim($row['TACOGE']))?>,
							xtype: 'combo',
							multiSelect: false,
							flex: 1,
							labelAlign: 'left',
							fieldLabel: '<?php echo $row['TADESC']; ?>',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    // margin: "10 10 0 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json($s->find_TA_std('PRVAN', $row['TAKEY1'], 5), '') ?> 
								    ] 
							}						 
						},  {
	                name   : 'f_nota_<?php echo $row['TAKEY1']; ?>',
	                xtype : 'textfield',
	                //dataIndex: 'articolo',
	                flex: 1,
	                margin: "0 10 0 10",
	                labelAlign: 'left',
	                anchor: '-15'
	                }
						
						]
					 },
						
						
						 
				<?php } ?>
					
					
					
					
			]
		}
	]
}
<?php
 exit;
}






// ******************************************************************************************
// APERTURA PREVENTIVI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_prev'){
	
	$addmenu = "
		      voci_menu.push({
	          		text: 'Converti in ordine',
	        		iconCls : 'iconRefresh',	          		
	        		handler: function() {
	        			
					   Ext.Msg.confirm('Richiesta conferma', 'Confermi esecuzione?', function(btn, text){																							    
					   if (btn == 'yes'){																	         	
			         	
			         	Ext.getBody().mask('Retrieving image', 'loading').show();
						Ext.Ajax.request({
						   url        : '" . $_SERVER['PHP_SELF'] ."?fn=exe_converti_in_ordine',
						   method: 'POST',
						   jsonData: {
						   	k_ordine: rec.get('tddocu') 
						   }, 
						   
						   success: function(result, request) {
						   	  Ext.getBody().unmask();
							  var jsonData = Ext.decode(result.responseText);

                                if (jsonData.success == false){
                                    acs_show_msg_error(jsonData.message, 'Dati obbligatori mancanti');
                                    return;
                                }
							  
			 					acs_show_msg_info('Geneato ordine ' + jsonData.num_creato);
								return false;
						   },									
			
			
						   failure: function(response, opts) {
						      Ext.getBody().unmask();
						      alert('error in get image');
						   }
						});						         	
			         	
						}
					   });  
        		}
    		});
			
			
		      voci_menu.push({
	          		text: 'Duplica preventivo',
	        		iconCls : 'iconRefresh',	          		
	        		handler: function(a,b,c,d,e,f) {
			
					   //m_grid = this.up('form').up('form').up('panel').down('grid');			
	        			
					   Ext.Msg.confirm('Richiesta conferma', 'Confermi esecuzione?', function(btn, text){																							    
					   if (btn == 'yes'){																	         	
			         	
			         	Ext.getBody().mask('Retrieving image', 'loading').show();
						Ext.Ajax.request({
						   url        : '" . $_SERVER['PHP_SELF'] ."?fn=exe_duplica_preventivo',
						   method: 'POST',
						   jsonData: {
						   	k_ordine: rec.get('tddocu') 
						   }, 
						   
						   success: function(result, request) {
						   	  Ext.getBody().unmask();
							  var jsonData = Ext.decode(result.responseText);
							  
			 					acs_show_msg_info('Geneato preventivo ' + jsonData.num_creato);
								return false;
			
			
			
 				             	Ext.Ajax.request({
								        url        : 'acs_panel_protocollazione.php?fn=get_grid_data_find_order',
								        method     : 'POST',
					        			jsonData: {
											doc_type: 'P',
											form_values: {f_anno: 2017, f_numero: jsonData.num_creato}
										},							        
								        success : function(response, opts){
								       
									        jsonData = Ext.decode(response.responseText);
									        
											Ext.each(jsonData, function(value) {
												m_grid.getStore().add(value);
											});
					            		  
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });		
		
		
		
		
		
		
		
						   }, 
						   failure: function(response, opts) {
						      Ext.getBody().unmask();
						      alert('error in get image');
						   }
						});						         	
			         	
						}
					   });  
        		}
    		});				
			
 		";
	?>
	

{"success":true, "items": [
	{
		id: 'panel-protocollazione-prev',
		title: 'Prospect POS',
		xtype: 'panel',
        loadMask: true,
        closable: true,		
        layout: 'fit',
        
        doc_type: 'P',		//PREVENTIVO        

		listeners: {
		
	 			afterrender: function (comp) {	 			
	 						
								<?php $cl = new SpedProtocollazionePVen(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent('P', 'Intestazione nuova proposta', 'cls_tonalita_arancione'), "spedprotocollazione") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "spedprotocollazione", 'cls_tonalita_arancione') ?>
								<?php echo $cl->out_Writer_Model("spedprotocollazione") ?>
								<?php echo $cl->out_Writer_Store("spedprotocollazione") ?>
								<?php echo $cl->out_Writer_sotto_main("spedprotocollazione", 0.4) ?>
								<?php echo $cl->out_Writer_main("Elenco proposte d'ordine immesse", "spedprotocollazione",
											"", 1, 'false', null, "", $addmenu) ?>								

								comp.add(main);
								comp.doLayout();
									 			
	 				}
	 	} 	
	 	
	 	 
	      
		
	}
]
}




<?php
exit;


/* PROTOCOLLAZIONE */
}
?>
{"success":true, "items": [
	{
		id: 'panel-protocollazione',
		title: 'Heading POS',
		xtype: 'panel',
        loadMask: true,
        closable: true,		
        layout: 'fit',
        
        doc_type: 'O', //Ordine

		listeners: {
		
	 			afterrender: function (comp) {
	 			
	 						
								<?php $cl = new SpedProtocollazionePVen(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent('O', 'Intestazione nuovo ordine', 'cls_tonalita_verde'), "spedprotocollazione") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "spedprotocollazione", 'cls_tonalita_verde') ?>
								<?php echo $cl->out_Writer_Model("spedprotocollazione") ?>
								<?php echo $cl->out_Writer_Store("spedprotocollazione") ?>
								<?php echo $cl->out_Writer_sotto_main("spedprotocollazione", 0.4) ?>
								<?php echo $cl->out_Writer_main("Elenco ordini protocollati", "spedprotocollazione") ?>

								comp.add(main);
								comp.doLayout();
									 			
	 				}
	 	}
	 	
	 	 
	      
		
	}
]
}