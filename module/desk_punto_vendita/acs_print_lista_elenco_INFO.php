<?php
require_once "../../config.inc.php";
$mod_provenienza = new DeskPVen();
$cod_mod_provenienza = $mod_provenienza->get_cod_mod();

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

set_time_limit(240);

if ($_REQUEST['fn'] == 'open_report'){


	$ar_email_to = array();
	$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");
	$ar_email_to[] = array(trim($vettore->rec_data['TAMAIL']), "VETTORE " . j(trim($vettore->rec_data['TADESC'])) . " (" .  trim($vettore->rec_data['TAMAIL']) . ")");

	$users = new Users;
	$ar_users = $users->find_all();

	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}

	$ar_email_json = acs_je($ar_email_to);


	if ($is_linux == 'Y')
		$_REQUEST['form_values'] = strtr($_REQUEST['form_values'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));


		$filter = (array)json_decode($_REQUEST['f_filter']);

		$filtro = array();
		$filtro["cliente"] = $filter['f_cliente_cod'];

		if (strlen($filter['f_destinazione_cod']) > 0)
			$filtro["destinazione"] = $filter['f_destinazione_cod'];

			$filtro["riferimento"] = $filter['f_riferimento'];
			$filtro["agente"] = $filter['f_agente'];

			//data programmata
		if (strlen($filter['f_data_dal']) > 0)
			$filtro["data_dal"] = date('Ymd', strtotime($filter['f_data_dal']));
		if (strlen($filter['f_data_al']) > 0)
			$filtro["data_al"] = date('Ymd', strtotime($filter['f_data_al']));

					//data spedizione
		if (strlen($filter['f_data_sped_dal']) > 0)
			$filtro["data_sped_dal"] = date('Ymd', strtotime($filter['f_data_sped_dal']));
		if (strlen($filter['f_data_sped_al']) > 0)
			$filtro["data_sped_al"] = date('Ymd', strtotime($filter['f_data_sped_al']));


							//data rilascio
		if (strlen($filter['f_data_ril_dal']) > 0)
			$filtro["data_ril_dal"] = date('Ymd', strtotime($filter['f_data_ril_dal']));
		if (strlen($filter['f_data_ril_al']) > 0)
			$filtro["data_ril_al"] = date('Ymd', strtotime($filter['f_data_ril_al']));

//AREA
		if (strlen(trim($filter['f_area'])) > 0)
			$filtro['area'] = $filter['f_area'];

	//data ricezione (dal, al)
		if (strlen($filter['f_data_ricezione_dal']) > 0)
			$filtro["data_ricezione_dal"] = date('Ymd', strtotime($filter['f_data_ricezione_dal']));
		if (strlen($filter['f_data_ricezione_al']) > 0)
			$filtro["data_ricezione_al"] = date('Ymd', strtotime($filter['f_data_ricezione_al']));

			//data conferma (dal, al)
		if (strlen($filter['f_data_conferma_dal']) > 0)
			$filtro["data_conferma_dal"] = date('Ymd', strtotime($filter['f_data_conferma_dal']));
		if (strlen($filter['f_data_conferma_al']) > 0)
			$filtro["data_conferma_al"] = date('Ymd', strtotime($filter['f_data_conferma_al']));


		if (strlen($filter['f_data_ricezione']) > 0)
			$filtro["data_ricezione"] = date('Ymd', strtotime($filter['f_data_ricezione']));
		if (strlen($filter['f_data_conferma']) > 0)
			$filtro["data_conferma"] = date('Ymd', strtotime($filter['f_data_conferma']));

			
		//data disponibilita (dal, al)
		if (strlen($filter['f_data_disp_dal']) > 0)
			$filtro["data_disp_dal"] = date('Ymd', strtotime($filter['f_data_disp_dal']));
		if (strlen($filter['f_data_disp_al']) > 0)
			$filtro["data_disp_al"] = date('Ymd', strtotime($filter['f_data_disp_al']));
		
		//data evasione richiesta (dal, al)
		if (strlen($filter['f_data_ev_rich_dal']) > 0)
			$filtro["data_ev_rich_dal"] = date('Ymd', strtotime($filter['f_data_ev_rich_dal']));
		if (strlen($filter['f_data_ev_rich_al']) > 0)
			$filtro["data_ev_rich_al"] = date('Ymd', strtotime($filter['f_data_ev_rich_al']));
								
			
			
			$filtro['carico_assegnato'] = $filter['f_carico_assegnato'];
			$filtro['lotto_assegnato'] 	= $filter['f_lotto_assegnato'];
			$filtro['proforma_assegnato'] 	= $filter['f_proforma_assegnato'];

			$filtro['collo_disp'] = $filter['f_collo_disp'];
			$filtro['collo_sped'] = $filter['f_collo_sped'];

			$filtro['anomalie_evasione']= $filter['f_anomalie_evasione'];
			$filtro['ordini_evasi'] 	= $filter['f_ordini_evasi'];
			$filtro['divisione'] 		= $filter['f_divisione'];
			$filtro['modello'] 			= $filter['f_modello'];
			$filtro['num_ordine'] 		= $filter['f_num_ordine'];
			$filtro['num_carico'] 		= $filter['f_num_carico'];
			$filtro['num_lotto'] 		= $filter['f_num_lotto'];
			$filtro['num_proforma'] 	= $filter['f_num_proforma'];
			$filtro['indice_rottura'] 	= $filter['f_indice_rottura'];
			$filtro['indice_rottura_assegnato'] 	= $filter['indice_rottura_assegnato'];
			$filtro['itinerario'] 		= $filter['f_itinerario'];
			$filtro['tipologia_ordine'] = $filter['f_tipologia_ordine'];
			$filtro['stabilimento']     = $filter['f_stabilimento'];
			$filtro['stato_ordine'] 	= $filter['f_stato_ordine'];
			$filtro['priorita'] 		= $filter['f_priorita'];
			$filtro['solo_bloccati']	= $filter['f_solo_bloccati'];
			$filtro['confermati']		= $filter['f_confermati'];
			$filtro['hold']				= $filter['f_hold'];

			$stmt 	= $s->get_elenco_ordini($filtro, 'N', 'search', 'TDDTEP', true);

			?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   table.int1 th.int_data{font-weight: bold; font-size: 13px;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
   tr.ag_liv2 td{background-color: #DDDDDD;}   
   tr.ag_liv_el_cliente td{background-color: #ffffff; font-weight: bold;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white;}   
   tr.ag_liv_area th{background-color: #b0b0b0; color: black;}
   tr.ag_liv1{background-color: #f3f3f3;}
   tr.ag_liv1 td{font-weight: bold;}
   span.sceltadata{font-size: 0.6em; font-weight: normal;}
   span.denominazione_cliente{font-weight: bold;}   
   
   h2.acs_report_title{font-size: 18px; padding: 10px;}
   
   //div#my_content h2{font-size: 1.6em; padding: 7px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
 
 
<?php


if($_REQUEST['f_logo']=='Y'){
	
	echo "<img src=". logo_report_path()." height=70>";
	
}


if($_REQUEST['f_sceltadata'] == 'TDDTEP' ){
	$campo_data = "TDDTEP";
	if($_REQUEST['f_bilingue']=='Y'){
		echo "<h2 class='acs_report_title'> Riferimenti in consegna/Loading Plan </h2>";
	}else{	
		echo "<h2 class='acs_report_title'>Riepilogo ordini per data evasione programmata " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";
	}
	
}else{
	$campo_data = "TDDTSP";
	
	if($_REQUEST['f_bilingue']=='Y'){
		echo "<h2 class='acs_report_title'> Rifermenti in consegna/Loading Plan </h2>";
	}else{
		echo "<h2 class='acs_report_title'>Riepilogo ordini per data spedizione " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";
	}
	
	
}


$campo_ora = sceltacampo_ora($campo_data);

$_REQUEST['el_dettaglio_per_cliente'] = 'Y';
$_REQUEST['el_dettaglio_per_ordine'] = 'Y';
$_REQUEST['dettaglio_per_carico'] = 'N';
$_REQUEST['mostra_importo'] = 'Y';
 



$ar_tot["TOTALI"] = array();

while ($r = db2_fetch_assoc($stmt)) {
	
	$f_data = $r[$campo_data];
	$f_liv0 = trim($r["TDASPE"]); 					$d_liv1 = trim($r["TDASPE"]);
	$f_liv1 = implode("_", array($r["TDCITI"])); 	$d_liv1 = $s->decod_std('ITIN', $r['TDCITI']);
	$f_liv2 = implode("_", array($r["TDCCON"], $r["TDCDES"]));					
	$f_liv3 = trim($r['TDDOCU']);					$d_liv2 = trim($r['TDDOCU']);
	$f_liv4 = trim($r['RDART']);					$d_liv4 = trim($r['RDDART']);

	$r['S_IMPORTO'] = $r['TDTIMP'];
	
	

	$tmp_ar = &$ar_tot["TOTALI"];
	
	$tmp_ar['IMPORTO'] += $r['S_IMPORTO'] ;
	$tmp_ar['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
		
	
	
	if (!isset($ar[$f_data]))
		$ar[$f_data] = array("cod" => $r[$campo_data], "descr"=>$f_data,
				"val" => array(), "children"=>array());

		$d_ar = &$ar[$f_data]['children'];
			
		$tmp_ar = &$ar[$f_data];
		$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
		$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;


		//liv0
		$cod_liv = $f_liv0;
		if (!isset($d_ar[$cod_liv]))
			$d_ar[$cod_liv] = array("cod" => $cod_liv, "descr"=>$s->decod_std('ASPE', $cod_liv),
					"val" => array(),
					"children"=>array());
				
		 $tmp_ar = &$d_ar[$cod_liv];
		 $tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
		 	
		 $d_ar = &$tmp_ar['children'];

		 	
		 //liv1
		 $cod_liv = $f_liv1;
		 if (!isset($d_ar[$cod_liv]))
				$d_ar[$cod_liv] = array("cod" => $cod_liv, "descr"=>$cod_liv,
						"val" => array(),
						"itinerario" => $s->decod_std('ITIN', $r['TDCITI']),
						"vettore" => $s->decod_std('AUTR', trim($r['CSCVET'])),
						"vmc"		=> $s->des_vmc_sp($r),
						"TDDTEP" => $r['TDDTEP'],
						"TDNBOC" => $r['TDNBOC'], "CSNSPC" => $r['CSNSPC'],
						"CSTISP" => $r['CSTISP'], "CSTITR" => $r['CSTITR'],
						"CSDTIC" => $r['CSDTIC'], "CSHMIC" => $r['CSHMIC'],
						"CSKMTR" => $r['CSKMTR'], "CSPORT" => $r['CSPORT'],
						"children"=>array());

				$tmp_ar = &$d_ar[$cod_liv];
				$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
				$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;

				$d_ar = &$tmp_ar['children'];
					

				//liv2 //CLIENTE
				$cod_liv = $f_liv2;
				if (!isset($d_ar[$cod_liv]))
					$d_ar[$cod_liv] = array("cod" => $r[$f_liv2], 
							"descr"=>trim($r['TDDCON']) . " (" . implode(", ", array(trim($r['TDDLOC']), trim($r['TDIDES']), trim($r['TDPROD']), trim($r['TDNAZD']))) . ")",
							"note_carico" => note_carico($r),
							
							$campo_data => $r[$campo_data],
				
							/*"TDTPCA" => $r['TDTPCA'],
							"TDAACA" => $r['TDAACA'],
							"TDNRCA" => $r['TDNRCA'],*/
							"val"  => array(), "children"=>array());
					$tmp_ar = &$d_ar[$cod_liv];
					$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
					$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
					$d_ar = &$tmp_ar['children'];
										

					//liv3 //ORDINE
					$cod_liv = $f_liv3;
					if (!isset($d_ar[$cod_liv]))
						$d_ar[$cod_liv] = array("cod" => $r[$f_liv2], 
								"descr"=>trim($r['TDOADO']) . "_" . trim($r['TDONDO']) . " " . trim($r['TDMODI']) . " " . trim($r['TDOTPD']), 
								$campo_data => $r[$campo_data],
								//"TDTPCA" => $r['TDTPCA'],
								//"TDAACA" => $r['TDAACA'],
								//"TDNRCA" => $r['TDNRCA'],
								"val"  => array(), "children"=>array());
						$tmp_ar = &$d_ar[$cod_liv];
						$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
						$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
						$d_ar = &$tmp_ar['children'];
						
				    //liv4 //ARTICOLO
					$cod_liv = $f_liv4;
					if (!isset($d_ar[$cod_liv]))
						$d_ar[$cod_liv] = array("cod" => $r[$f_liv4],
								"descr"=> $r[$d_liv4],
								$campo_data => $r[$campo_data],
								"RDDART" => $r['RDDART'],
								"RDQTA"  => $r['RDQTA'],
								"RDUM"   => $r['RDUM'],
								"RTINFI" => $r['RTINFI'],
								"val"  => array(), "children"=>array());
								$tmp_ar = &$d_ar[$cod_liv];
								$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
								$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
					
						$tmp_ar["row"] = $r;
							

} //while


//echo "<pre>";print_r($ar);exit;





echo "<table class=int1>";

if($_REQUEST['f_bilingue']=='Y'){
	
	
	echo "
					<tr>
					 <th>Cliente/Customer</th>
		   		     <th>Ordine/Order</th>
		   		     <th>Articolo</th>
   					 <th>UM</th>
		   			";
	
	if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
		echo "
		   					 <th >Codice</th>
							 <th >Descrizione</th>
							 <th >UM</th>
							 <th class=number>Q.t&agrave;</th>";
	}
	echo "<th class=number>Importo/Amount</th>";
	echo "<th class=number>Quantit&agrave;</th>";
	
	
}else{

		echo "
					<tr>
					 <th>Data/Area/Itinerario/Cliente</th>
		   		     <th>Ordine</th>
		   		     <th>Articolo</th>
   		 			 <th>UM</th>
		   			";
		   			
		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			echo "
		   					 <th >Codice</th>
							 <th >Descrizione</th>
							 <th >UM</th>
							 <th class=number>Q.t&agrave;</th>";
		}   			
		echo "
							
		   					 <th class=number>Importo</th>";	
		echo "<th class=number>Quantit&agrave;</th>";

}

echo "</tr>";




$cl_liv_cont = 0;
if ($_REQUEST['dettaglio_per_carico'] == "Y") $liv2_row_cl = ++$cl_liv_cont;
$liv1_row_cl = ++$cl_liv_cont;



$liv_data_colspan = 3;
$liv_area_colspan = 3;
$r_colspan_carico = 3;
$col_dettaglio    = 4;
if ($_REQUEST['dettaglio_per_carico'] == "Y" || $_REQUEST['el_dettaglio_per_cliente'] == "Y") $r_colspan = 4; else $r_colspan=3;
if ($_REQUEST['dettaglia_km_sped'] == "Y") {$r_colspan++; $r_colspan_carico++;}
if ($_REQUEST['mostra_tipologie'] == "Y") {$r_colspan+=2; $r_colspan_carico+=2;}


if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
	$liv_data_colspan +=4;
}


foreach ($ar as $kgg => $gg){

	echo "<tr class=ag_liv_data><th colspan={$liv_data_colspan}>" . ucfirst(print_date($gg['cod'], "%A %d/%m")) . "</th>";
	echo "<th>&nbsp;</th>";
	echo "<th class=number>" . n($gg['val']['IMPORTO'], 0) . "</th>";
	echo "<th>&nbsp;</th>";
	echo "</tr>";
	 
	 
	foreach ($gg['children'] as $kl0 => $l0){

		//AREA SPEDIZIONE
		$_REQUEST['dettaglio_per_area_spedizione'] = 'Y';
		if ($_REQUEST['dettaglio_per_area_spedizione'] == "Y"){

			echo "<tr class=ag_liv_area><th colspan={$liv_data_colspan}>" . ucfirst($l0['descr']) . "</th>";
			echo "<th>&nbsp;</th>";
			echo "<th class=number>" . n($l0['val']['IMPORTO'], 0) . "</th>";
			echo "<th>&nbsp;</th>";
			echo "</tr>";
		}

		 
		usort($l0['children'], "cmp_el_l1");

		foreach ($l0['children'] as $kl1 => $l1){

			//ITINERARIO
			echo "<tr class=ag_liv{$liv1_row_cl}>
					<td colspan={$liv_data_colspan}>" . $l1['itinerario'] . "</td>";
			 
						echo "<td>&nbsp;</td>";
						echo "<td class=number>" . n($l1['val']['IMPORTO'], 0) . "</td>";
						echo "<td>&nbsp;</td>";
				  		echo "</tr>";

				  			// CLIENTE
				  			foreach ($l1['children'] as $kl2 => $l2){
						  		  echo "<tr class=ag_liv_el_cliente>
						  		  
						  		  <td colspan={$liv_data_colspan}>" . $l2['descr'] . "</td>";
						  		  echo "<td>&nbsp;</td>";
						  						 
									echo "<td class=number>" . n($l2['val']['IMPORTO'], 0) . "</td>";
									echo "<td>&nbsp;</td>";
						  		    echo "</tr>";



			  		    //DOCUMENTI
			  			if ($_REQUEST['el_dettaglio_per_cliente'] == "Y"){			  			
			  			global $id_ditta_default;
			  						  			
			  		    foreach ($l2['children'] as $kl3 => $l3){
			  		    			
			  		    		echo "<tr class=ag_liv{$liv2_row_cl}>
			  		    					<td>" . trim($l3['row']['TDVSRF']) . "</td>			  		    					
			  		    					<td>" . $l3['descr'] . "</td>";
			  		    					
			  		    						echo "<td>&nbsp;</td>";
			  		    						echo "<td>&nbsp;</td>";

			  		    					if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			  		    						echo "
													<td class=number2>" . trim($l3['row']['RDART']) . "</td>
					 								<td class=number2>" . trim($l3['row']['RDDES1']) . "</td>
					 								<td class=number>"  . trim($l3['row']['RDUM']) . "</td>
					 								<td class=number>"  . n($l3['row']['RDQTA'], 0) . "</td>";
			  		    					}
			  		    					

											echo "<td class=number>" . n($l3['val']['IMPORTO'], 0) . "</td>";			
											echo "<td>&nbsp;</td>";
											echo "</tr>";
			  		    					
											echo "</tr>";
											
											foreach ($l3['children'] as $kl4 => $l4){
												
												echo "<tr><td colspan=2>&nbsp;</td>";
												
												echo "<td>" . $l4['RDDART'] . "</td>";
												echo "<td>" . $l4['RDUM'] . "</td>";
												echo "<td class=number>" . n($l4['RTINFI'], 0) . "</td>";
												echo "<td class=number>" . n($l4['RDQTA'], 0) . "</td>";
												echo "</tr>";
												
											}


											}
									}

					}

					//stamp l'eventuale riga collegata
					if ($_REQUEST['dettaglio_per_carico'] == "Y" && $l1['CSNSPC'] > 0){
													$col_dettaglio_coll = $col_dettaglio +1;
													echo "<tr class=ag_liv{$liv2_row_cl}>
													<td colspan={$r_colspan_carico} align=right>
													<img src=" . img_path("icone/48x48/link_blu.png") . " width=18> Spedizione collegata:</td>
													<td colspan={$col_dettaglio_coll}>" .  $s->get_el_carichi_by_sped($l1['CSNSPC'], 'Y', 'N', 'Y') . "</td>";
													echo "</tr>";
													}
														
														

													}
													}
													}


	//STAMPO TOTALE
	echo "
		<tr class=liv_totale>
				<td colspan={$liv_data_colspan}>TOTALE GENERALE</td>
				<td class=number>" . n($ar_tot['TOTALI']['IMPORTO'], 0) . "</td>";
	echo "</tr>";


	
}	
	
if ($_REQUEST['fn'] == 'open_form'){
		$m_params = acs_m_params_json_decode();
		?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            //url: 'acs_print_lista_consegne.php',
	            
	            items: [
	            	{
                	xtype: 'hidden',
                	name: 'f_filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                }  ,
					{
                    xtype: 'radiogroup',
                    anchor: '100%',
                    fieldLabel: 'Data',
                    items: [
                        {
                            xtype: 'radio'
                          , name: 'f_sceltadata' 
                          , boxLabel: 'Programmata'
                          , inputValue: 'TDDTEP'
                          , checked: true
                        },
                        {
                            xtype: 'radio'
                          , name: 'f_sceltadata' 
                          , boxLabel: 'Spedizione'
                          , inputValue: 'TDDTSP'                          
                        }
                   ]
                } ,
                {
							flex: 1,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Bilingue',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_bilingue' 
		                          , boxLabel: ''
		                          ,checked: false
		                          , inputValue: 'Y'
		                        }]														
						 },  {
							flex: 1,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Logo',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_logo' 
		                          , boxLabel: ''
		                          ,checked: false
		                          , inputValue: 'Y'
		                        }]														
						 }         
	            ],
	            
				buttons: [					
					{
			            text: 'Visualizza',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues())
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	

	function add_descr_report($desc){
		if (strlen($desc) > 0)
			return ' - ' . $desc;
			else return '';
				
	}
	
	
	function sceltadata($campo_data){
		return "(Data programmazione)";
	}
	
	function sceltacampo_ora($campo_data){
		return "SP.CSHMPG";
	}
	
	
	
	function note_carico($row){
		return "";
	}
	
	
	function out_cliente($row){
		$ret = "<span class=denominazione_cliente>{$row['TDDCON']}</span>";
	
		if ($row['DEST_NAZ_C'] == "ITA"  || $row['DEST_NAZ_C'] == "IT"){
			$ind = "{$row['DEST_LOCA']} ({$row['DEST_PROV']})";
		} else {
			$ind = "{$row['DEST_LOCA']} ({$row['DEST_NAZ_D']})";
		}
	
		return implode(", ", array($ret, $ind));
	}
	
	
		function cmp_el_l1($a, $b)
		{
		//elenco: ordini it/ve per data
	
		if ($_REQUEST['evidenzia_inizio_carico'] == 'Y'){
			return strcmp("{$a['CSDTIC']}", "{$b['CSDTIC']}");
			if ($a['CSDTIC'] == 0) $a_cmp = $a['DATA'];
			else $a_cmp = $a['CSDTIC'];
			if ($b['CSDTIC'] == 0) $b_cmp = $b['DATA'];
			else $b_cmp = $b['CSDTIC'];
	
			return strcmp($a_cmp . sprintf("%06s", $a['CSHMIC']), $b_cmp . sprintf("%06s", $b['CSHMIC']));
	
		}
	
		return strcmp(strtoupper($a["itinerario"] . $a["vmc"] . $a["CSHMPG"]) , strtoupper($b["itinerario"] . $b["vmc"] . $b["CSHMPG"]));
	}
	
	


?>