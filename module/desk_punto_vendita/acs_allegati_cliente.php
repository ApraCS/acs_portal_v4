<?php
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'open_win'){
	$m_params = acs_m_params_json_decode();
	
	//$oe = $s->get_ordine_by_k_docu($m_params->k_ordine);


	?>

{"success":true, "items": [

    {
                xtype: 'tabpanel',
                region: 'east',
                title: '',
                margins: '0 5 0 0',
                tabPosition: 'bottom',

        		imposta_title: function(tddocu){
        				this.setTitle(tddocu);
        			},
                
                items: [

                    new Ext.grid.GridPanel({
                    title: 'Allegati',
               		 store: new Ext.data.Store({
               									
               				autoLoad: true,				        
               	  			proxy: {
               							url: 'acs_get_order_images.php',
               							type: 'ajax',
               							reader: {
                						            type: 'json',
                									method: 'POST',						            
                						            root: 'root'						            
                						        },
               						      actionMethods: {
            							          read: 'POST'
            							        },
               						     extraParams: {
               		    		    		cliente: '<?php echo $m_params->cliente_selected; ?>'
               		    				},               						        
				        			doRequest: personalizza_extraParams_to_jsonData
               						},
               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
               		     	groupField: 'tipo_scheda',               		     	
               			}),
    		    	features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),               			
                  						    						
                     columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],

			         listeners: {
				      activate: {
					      fn: function(e){
						   	if (this.store.proxy.extraParams.k_ordine != '')
							   	this.store.load();
					      }
				      },
					       
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  }
				         
			         }

                     }) 


                    ]
            }
	
]}


<?php 
exit;
}