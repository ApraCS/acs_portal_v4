<?php
require_once("../../config.inc.php");
require_once("acs_panel_margini_include.php");
require_once "acs_panel_protocollazione_include.php";

$main_module = new DeskPVen();
$cod_mod_provenienza = $main_module->get_cod_mod();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'get_json_data'){

	$m_params = acs_m_params_json_decode();

	$ar=crea_ar_tree_margini($_REQUEST['node'], $m_params->open_request->form_values);
	
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}

	echo acs_je(array('success' => true, 'children' => $ret));
	exit;
}

####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
	####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();

	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT TDCCON, TDDCON
	FROM {$cfg_mod_DeskPVen['file_testate']}
	WHERE UPPER(
	REPLACE(REPLACE(TDDCON, '.', ''), ' ', '')
	) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			AND ". $s->get_where_std()." GROUP BY TDCCON, TDDCON
 				";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ret = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$ret[] = array("cod"=> trim($row['TDCCON']),"descr"=> acs_u8e(trim($row['TDDCON'])));
	}

	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'open_parameters'){
	?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
			            text: 'Analisi di bilancio',
			            iconCls: 'icon-bilancio2-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
						    acs_show_win_std('Analisi riclassificazione dati di bilancio', '../desk_gest/acs_panel_abi_start.php?fn=open_scelta_ceal', {}, 600, 400, {}, 'icon-bilancio2-16');	        	
							this.up('window').close();
			
			            }
			         }, '->', {
			            text: 'Report',
			            iconCls: 'icon-print-32',
			            scale: 'large',
			            handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
            	                
            	                acs_show_win_std('Report marginalit&agrave;', 'acs_panel_margini_report.php?fn=open_form', {
		        				filter: form.getValues()
		    					},		        				 
		        				300, 150, {}, 'icon-print-16');	
		        			}  
	                        /*this.up('form').submit({
    	                        url: 'acs_panel_margini_report.php',
    	                        target: '_blank', 
    	                        standardSubmit: true,
    	                        method: 'POST',                        
    	                        params: {
    	                            form_values: Ext.encode(form.getValues())
    						    }
                  			});    */        	                	                
	            		}
			         } , {
			            text: 'Excel',
			            iconCls: 'icon-inbox-32',
			            scale: 'large',
			            handler: function() {
		                    form = this.up('form').getForm();
	                        this.up('form').submit({
    	                        url: 'acs_panel_margini_to_excel.php',
    	                        target: '_blank', 
    	                        standardSubmit: true,
    	                        method: 'POST',                        
    	                        params: {
    	                            form_values: Ext.encode(form.getValues())
    						    }
                  			});			            
			            }
			         }, {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
							
							acs_show_panel_std('acs_panel_margini.php?fn=open_tab', 'panel_fatture_anticipo', {form_values: form.getValues()});
							this.up('window').close();
			
			            }
			         }],   		            
		            
		            items: [
		         	  	{
           				 xtype: 'combo',
						name: 'f_cliente_cod',
						fieldLabel: 'Cliente',
						labelWidth: 110,
						minChars: 2,	
						anchor: '-15',		
           				margin: "5 25 5 10",
            
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            
		            url : <?php
		            		$cfg_mod = $main_module->get_cfg_mod();
		            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
		            		?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

       		 }, {
							name: 'f_sede',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Sede',
							displayField: 'text',
							labelWidth: 110,
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?>	
								    ] 
							}						 
						},{
							name: 'f_referente',
							xtype: 'combo',
							fieldLabel: 'Referente', 
							displayField: 'text',
							valueField: 'id',
							labelWidth: 110,
							forceSelection:true,
						   	allowBlank: true,	
						   	margin: "5 10 5 10",
						   	anchor: '-15',													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [<?php echo acs_ar_to_select_json(find_TA_sys('BREF'), '') ?>] 
							}
							},
						 {
							name: 'f_stato_ordine',
							xtype: 'combo',
							multiSelect: true,
							labelWidth: 110,
							fieldLabel: 'Stati ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "5 10 0 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ]
							}						 
						},
						{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "5 10 0 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data ordine iniziale'
							   , labelWidth: 110
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "5 25 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data ordine finale'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_fattura_da'
							   , margin: "0 10 0 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data fatt. iniziale'
							   , labelWidth: 110
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_fattura_a'
							   , margin: "0 25 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data fatt. finale'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 },
					 {
							xtype: 'radiogroup',
							fieldLabel: 'Costo preventivo',
							labelWidth: 110,
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "0 10 5 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_con_costo_preventivo' 
		                          , boxLabel: 'Con'
		                          , inputValue: 'Y'
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_con_costo_preventivo' 
		                          , boxLabel: 'Senza'
		                          , inputValue: 'N'
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_con_costo_preventivo' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: ''
		                          , checked: true
		                          
		                        }]
						},  
    					
    					{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						 {name: 'f_scost',
    					 xtype: 'numberfield',
    					 fieldLabel: '% Scost.segnalata',
    					 labelWidth: 110,
    					 margin: "0 20 0 10",
    					 value : 5 
    					 },
    					  <?php $cf1 = "<img src=" . img_path("icone/48x48/sub_blue_accept.png") . " height=15>"; ?> 
    					  <?php $cf2 = "<img src=" . img_path("icone/48x48/sticker_black.png") . " height=15>"; ?> 
    					 {
							xtype: 'radiogroup',
							fieldLabel: 'Check margine',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    items: [{
		                            xtype: 'radio'
		                          , name: 'f_margine' 
		                          , boxLabel: '<?php echo $cf1?>'
		                          , width : 50
		                          , margin : '0 0 0 0'
		                          , inputValue: 'Y'
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_margine' 
		                          , margin : '0 0 0 0'
		                          , width : 50
		                          , boxLabel: '<?php echo $cf2?>'
		                          , inputValue: 'Z'
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_margine' 
		                          , width : 50
		                          , margin : '3 5 0 0'
		                          , boxLabel: 'Tutti'
		                          , inputValue: ''
		                          , checked: true
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_margine' 
		                          , width : 60
		                          , margin : '3 0 0 0'
		                          , boxLabel: 'Vuoto'
		                          , inputValue: 'V'
		                          
		                        }]
						}, 
						   
						]},
    					
    					{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
    					  {name: 'f_trasp',
        					 xtype: 'numberfield',
        					 fieldLabel: '% Trasporto',
        					 labelWidth: 110,
        					 margin: "0 10 0 10",
        					 value : 10 
    					 },{
							name: 'f_causale',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Causale margine',
							displayField: 'text',
							labelWidth: 110,
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "0 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_TA_std('CMAR'), '', true, 'N', 'Y') ?>	
								    ] 
							}						 
						}
    					 
    					 ]},
    					 
    					 	 {
						name: 'f_todo',
						xtype: 'combo',
						fieldLabel: 'To Do',
						labelWidth: 110,
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: true,	
					  	//anchor: '-15',
					    margin: "0 0 0 10",	
					    width : 280,											
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php
							     // echo acs_ar_to_select_json($s->find_TA_std('ATTAV', N, 'Y'), "");
							     echo acs_ar_to_select_json($s->find_TA_std('ATTAV', null, 'Y', 'N', null, null, 'POS'), "");
							       ?>	
							    ] 
						}						 
					}
    					 
										 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}
	]
}		
	
		
<?php
	exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
	$m_params = acs_m_params_json_decode();
	
	?>

{"success":true, "items": [

        {
            xtype: 'treepanel',
	        title: 'Profit',
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        tbar: new Ext.Toolbar({
		            items:[
		            '<b>Riepilogo margine su ordini clienti/forniture collegate</b>', '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
            		, {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}		            
		       		<?php echo make_tbar_closable() ?>
		         ]            
		        }),             
			
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    fields: ['task', 'importo_ord', 'importo_forn', 'margine', 'stato', 'data_ord', 'liv', 'ord',
				     		 'st_ass', 'data_ass', 'nr_ass', 'art_mancanti', 'k_ordine', 'coeff', 'data_fatt', 'imp_ass',
				     		 'S_RDFG04_Y', 'f_spunta' , 'coeff_pre', 'importo_forn_pre', 'segnala_sfora_scost', 'f_spunta_black',
				      	     'comm', 's_causale', 't_causale', 'c_marg'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
					
                        extraParams: {
                                    open_request: <?php echo acs_je($m_params) ?>
                                }, 
                                doRequest: personalizza_extraParams_to_jsonData, 
								

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						        }       				
                    }

                }),
                
       <?php $cf1 = "<img src=" . img_path("icone/48x48/sub_blue_accept.png") . " height=20>"; ?>
       <?php $cf2 = "<img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=20>"; ?>
	   <?php $comm = "<img src=" . img_path("icone/48x48/comment_accept.png") . " height=25>"; ?>

		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'task', 
			            flex: 3,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Sede/Cliente/Ordine',
			            renderer: function (value, metaData, record, row, col, store, gridView){
			            	if (record.get('liv') == 'liv_2' || record.get('liv') == 'liv_3')
  	  				 			metaData.tdCls += ' auto-height';
			            	return value;
			            }
			        }, {
				    text: '<?php echo $comm; ?>',
				    width: 32, 
				    tooltip: 'Annotazioni controllo margine',
				    tdCls: 'tdAction',  
				    dataIndex : 'comm',       			
	    	        menuisabled: true, sortable: false,        		        
					renderer: function(value, p, record){
    					if(record.get('liv') == 'liv_3'){
    						if (record.get('comm') == 1) return '<img src=<?php echo img_path("icone/48x48/comment_accept.png") ?> width=18>';
    			    		if (record.get('comm') == 0) return '<img src=<?php echo img_path("icone/48x48/comment_accept_grey.png") ?> width=18>';
    			    	}
			    		
			    	}
			    	}, {
			               text: '<?php echo $cf1; ?>',
			               width     : 30,
			               tooltip: 'Ordini con controllo preventivo (spunta blu)<br/>Ordini con controllo effettivo (sticker nero)',
			               tdCls: 'tdAction',         			
                           menuDisabled: true, sortable: false,  
                           dataIndex: 'f_spunta',          		        
			               renderer: function(value, metaData, record){
			                if (record.get('s_causale').trim() != ''){   
			                	metaData.tdCls += ' sfondo_giallo';
			                	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('t_causale')) + '"';	    			    
			                }	
			                if (record.get('f_spunta') == 'Y') return '<img src=<?php echo img_path("icone/48x48/sub_blue_accept.png") ?> width=15>';   
			                if (record.get('f_spunta') == 'Z') return '<img src=<?php echo img_path("icone/48x48/sticker_black.png") ?> width=15>';
						   }			                
		             },
		             /*{
			               text: '<?php echo $cf2; ?>',
			               width     : 30,
			               tdCls: 'tdAction',         			
                           menuDisabled: true, sortable: false,  
                           dataIndex: 'f_spunta_black',          		        
			               renderer: function(value, p, record){
			               if (record.get('f_spunta_black') == 'Y') return '<img src=<?php echo img_path("icone/48x48/sub_black_accept.png") ?> width=15>';}   
			                
		             },*/
		             
		             { 
			            dataIndex: 'data_ord',
			            header: 'Data ordine', 
			            renderer: date_from_AS,
			            width: 80
			        }, { 
			            dataIndex: 'data_fatt',
			            header: 'Data fattura', 
			            renderer: date_from_AS,
			            width: 80
			        }, { 
			            dataIndex: 'stato',
			            header: 'St.', 
			            width: 40
			        }, 
			        
			        {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', tooltip: 'Articoli MTO',  
    	    		dataIndex: 'art_mancanti',	    
    			    renderer: function(value, metaData, record){
    			    	if (record.get('S_RDFG04_Y') > 0) metaData.tdCls += ' sfondo_rosso'; //evidenza prezzo a 0
    			    
    			    	if (record.get('art_mancanti')==4) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
    			    	if (record.get('art_mancanti')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
    			    	if (record.get('art_mancanti')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
    			    	if (record.get('art_mancanti')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';			    	
    			    	}}
    			    	
    			    	,{
    			    	dataIndex: 'importo_ord',
			            header: 'Importo ordine<br/>con IVA', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'importo_forn_pre',
			            header: 'Imp. forn. previsto <br>senza iva', 
			            width: 120,
			            align: 'right',
			            renderer: function(value, metaData, record){
			            
			            if(record.get('segnala_sfora_scost') == 'Y'){
			            	
            			       metaData.tdCls += ' sfondo_giallo';		
			            
			            }
			            return floatRenderer2(value);	
							}
			        },{ 
			            dataIndex: 'coeff_pre',
			            header: 'Coeff. <br>Preventivo', 
			            width: 80,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'importo_forn',
			            header: 'Imp. forniture<br/>senza IVA', 
			            flex: 1,
			            align: 'right', 
			            renderer: floatRenderer2
			        },{ 
			            dataIndex: 'coeff',
			            header: 'Coeff.', 
			            width: 80,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, { 
			            dataIndex: 'margine',
			            header: 'Margine lordo', 
			            width: 100,
			            align: 'right', 
			            renderer: floatRenderer2
			        }, {header: 'Assistenze',
                    	columns: [
                      		{header: 'Numero', dataIndex: 'nr_ass', width: 70, align: 'right'},
                     		{header: 'Stato', dataIndex: 'st_ass', width: 70},
                     		{header: 'Data', dataIndex: 'data_ass', width: 70,  renderer: date_from_AS}
                     		//,{header: 'Importo forniture', dataIndex: 'imp_ass', width: 70, align: 'right', renderer: floatRenderer2},
                  				]
                 		}
			     
	         ]	,
	         
	          listeners: {
	          
	            beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                        load: function () {
                          Ext.getBody().unmask();
                        }, 
	         
	           celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	
					  	if (col_name=='art_mancanti' && rec.get('liv')=='liv_3'){
							iEvent.preventDefault();	
							acs_show_win_std('Segnalazione articoli critici', 'acs_art_critici.php?fn=open_art', {k_ordine: rec.get('k_ordine'), type: 'MTO'}, 950, 500, null, 'icon-shopping_cart_gray-16');					
							
							return false;							
					   }
					   
					   if (rec.get('liv') == 'liv_3' && col_name == 'f_spunta'){
					  
						 
						  Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_spunta_blu',
						        method     : 'POST',
			        			jsonData: {
			        			    flag: rec.get('f_spunta'),
			        			    k_ordine: rec.get('k_ordine')
								},							        
						        success : function(result, request){
			            			jsonData = Ext.decode(result.responseText);
			            		 	rec.set('f_spunta', jsonData.flag);
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	 

							
						}
						
						 if (rec.get('liv') == 'liv_3' && col_name == 'f_spunta_black'){
					  
						 
						  Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_spunta_black',
						        method     : 'POST',
			        			jsonData: {
			        			    flag: rec.get('f_spunta_black'),
			        			    k_ordine: rec.get('k_ordine')
								},							        
						        success : function(result, request){
			            			jsonData = Ext.decode(result.responseText);
			            		 	rec.set('f_spunta_black', jsonData.flag);
			            		},
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	 

							
						}
						
						if (rec.get('liv') == 'liv_3' && col_name == 'comm'){
					
							acs_show_win_std('Annotazioni controllo margine ' + rec.get('ord'), 'acs_form_json_annotazioni_ordine.php', {num_ann : 9, k_ordine : rec.get('k_ordine'), bl : 'PRO'}, 800, 400, null, 'icon-comment_edit-16');	
						}
	          
	            	}
	          
	           },  itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						  var voci_menu = [];
					      
					if(rec.get('liv') == 'liv_3'){
					  	 voci_menu.push({
			         		text: 'Stampa contratto',
			        		iconCls : 'icon-print-16',          		
			        		handler: function () {
			        			    list_selected_id = grid.getSelectionModel().getSelection();
					    		  	rec = list_selected_id[0];	 
					    		  	window.open('acs_report_pdf.php?k_ordine='+ rec.get('k_ordine'));
				                }
			    		});
			    		 	
					  	 voci_menu.push({
			         		text: 'Scheda controllo importi',
			        		iconCls : 'icon-listino',          		
			        		handler: function () {
		        			  var list_selected_id = grid.getSelectionModel().getSelection(),
				    		    rec = list_selected_id[0],
				    		    k_ordine = rec.get('k_ordine');				    		  
                	          acs_show_win_std('Scheda controllo importi ordine ' +rec.get('ord'), 'acs_panel_margini_controllo_importi.php?fn=open', {
    		        			k_ordine: k_ordine
    		    				}, 1100, 550, {}, 'icon-listino');					    		  	
    				        }
			    		});
			    		
			    		 voci_menu.push({
			         		text: 'Causale marginalit&agrave;',
			        		iconCls : 'iconRefresh',          		
			        		handler: function () {
		        			  var list_selected_id = grid.getSelectionModel().getSelection(),
				    		    rec = list_selected_id[0],
				    		    k_ordine = rec.get('k_ordine');				    		  
                	          acs_show_win_std('Causale marginalit&agrave;', 'acs_panel_margini.php?fn=open_form', {
    		        			k_ordine: k_ordine, causale : rec.get('c_marg')
    		    				}, 350, 150, {}, 'iconRefresh');					    		  	
    				        }
			    		});
			    		
			    		voci_menu.push({
     					text: 'Inserimento nuovo stato/attivit&agrave;',
    					iconCls : 'icon-arrivi-16',          		
    					handler: function() {

            			id_selected = grid.getSelectionModel().getSelection();
        
            		  	list_selected_id = [];
            		  	for (var i=0; i<id_selected.length; i++) 
            			   list_selected_id.push({k_ordine : id_selected[i].data.k_ordine});
        
            		  	my_listeners_inserimento = {
            					afterInsertRecord: function(from_win){	
            						from_win.close();
        			        		}
        	    				};	
        
                		
        				acs_show_win_std('Nuovo stato/attivit&agrave;', 
        					<?php echo j('acs_form_json_create_entry.php'); ?>, 
        					{	//tipo_op: 'ANART',
        						rif : 'POS',
        		  				list_selected_id: list_selected_id
        		  			}, 500, 500, my_listeners_inserimento, 'icon-arrivi-16');	
        		  							        		          		
            		}
        		});
			    		 	
			    		 			
					}
					
						
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			   },  itemclick: function(view,rec,item,index,eventObj) {
	
			     
			          if (rec.get('liv') == 'liv_3')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	//var wd = Ext.getCmp('OrdPropertyGridDet');
			        	var wdi = Ext.getCmp('OrdPropertyGridImg');        	
			        	var wdc = Ext.getCmp('OrdPropertyCronologia');

			        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdi.store.load()
						wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdc.store.load()			        												        	
			        	
						Ext.Ajax.request({
						   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
					   }												        	
			         }
	         
	         
	         }
	         , viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           
			    
			           return ret;																
			         }   
			    }																				  			
	            
        }  

]
}

<?php exit; }


// ******************************************************************************************
// EXE - spunta blu)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_spunta_blu'){

	$m_params = acs_m_params_json_decode();

	$k_ordine = $m_params->k_ordine;
	
	//TDFG04 = ''  -> non controllato
	//TDFG04 = 'Y' -> costo preventivo controllato (spunta blu)    -> ordine accettato
	//TDGG04 = 'Z' -> costo effettivo controllato  (sticker_black) -> contrallate ddt/fatture -> ordine chiuso

	if (trim($m_params->flag) == '' || trim($m_params->flag) == 'N')
		$m_flag = 'Y';
    elseif (trim($m_params->flag) == 'Y')
        $m_flag = 'Z';
	else 
		$m_flag = ' ';


	$sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
	       SET TDFG04 = '{$m_flag}'
		  WHERE TDDT = '{$id_ditta_default}' AND TDDOCU = '{$k_ordine}'";
	
	//print_r($sql);

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);



	$ret = array();
	$ret['success'] = true;
	$ret['flag'] = $m_flag;
	echo acs_je($ret);

	exit;
}
// ******************************************************************************************
// EXE - spunta black)
// ******************************************************************************************
/*if ($_REQUEST['fn'] == 'exe_spunta_black'){
    
    $m_params = acs_m_params_json_decode();
    
    $k_ordine = $m_params->k_ordine;
    
    if ($m_params->flag == ' ')
        $m_flag = 'Y';
    else
        $m_flag = ' ';
            
            
            
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_testate']}
            SET TDFG05 = '{$m_flag}'
            WHERE TDDT = '{$id_ditta_default}' AND TDDOCU = '{$k_ordine}'";
            
            //print_r($sql);
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
            
            
            $ret = array();
            $ret['success'] = true;
            $ret['flag'] = $m_flag;
            echo acs_je($ret);
            
            exit;
}*/

    
if ($_REQUEST['fn'] == 'exe_genera'){
    
    $ret = array();
    ini_set('max_execution_time', 3000);
    $m_params = acs_m_params_json_decode();
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ASS_CAU_NC',
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array("RICITI" => trim($m_params->form_values->f_causale))
        )
        );
        
    
    
    echo acs_je($ret);
    exit;
        
}
    
if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    ?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
        
            items: [
						
						{
					    xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{  
							name: 'f_causale',
							xtype: 'combo',
							flex : 1,	
							labelWidth : 50,
							fieldLabel: 'Causale',
							displayField: 'text',
							margin : '0 10 0 0',
						    valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: false,
							allowBlank: true,	
							value : <?php echo j($m_params->causale); ?>, 													
				            	store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [		
						        <?php echo acs_ar_to_select_json($main_module->find_TA_std('CMAR'), '', true, 'N', 'Y') ?>	
							    ] 
						   }	
						    						 
						  },
						  {
						 xtype: 'button',
						 margin: '0 0 0 5',
			             scale: 'small',			                 
			             iconCls: 'icon-gear-16',
			             iconAlign: 'top',			                
			             handler : function() {
									acs_show_win_std('Gestione causale', 'acs_gest_causale.php?fn=open_tab', {}, 600, 500, null, 'icon-gear-16');
								} //handler function()
						 
						 } 
						 
						 ]}
						 
            ],
            
			buttons: [					
				{
		            text: 'Conferma',
			        iconCls: 'icon-button_blue_play-32',		            
			        scale: 'large',	            
		            handler: function() {
		            
		            var form = this.up('form').getForm();
		            var win = this.up('window');
		            if(form.isValid()){
		            	Ext.Ajax.request({
				        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera',
					        method     : 'POST',
		        			jsonData: {
		        				form_values: form.getValues(),
		        				k_ordine: <?php echo j($m_params->k_ordine)?>
							},							        
					        success : function(result, request){
		            			 win.close();									        
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });	
	                  }
	             
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
exit;
}
