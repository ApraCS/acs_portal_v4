<?php

require_once "../../config.inc.php";


$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// modifico stabilimento ordine
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_stabilimento_ordine'){
    
    
   $sql = "SELECT TDSTAB AS SEDE
           FROM {$cfg_mod_DeskPVen['file_testate']} TD
           WHERE TDDT = '{$id_ditta_default}' AND TDDOCU = '{$m_params->k_ordine}'";
   
   $stmt = db2_prepare($conn, $sql);
   echo db2_stmt_errormsg();
   $result = db2_execute($stmt);
   $row = db2_fetch_assoc($stmt);

   if(trim($row['SEDE']) != $m_params->rec->cod){
   
        
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'MOD_DOC_PVEN',
            "k_ordine"	=> $m_params->k_ordine,
            "vals" => array("RICITI" => $m_params->rec->cod)
            
            
        )
        );
    
   }else{
       
       $ret = array('msg' => 'no refresh');
   }
    
   $ret = array('success' => true);
    echo acs_je($ret);
    exit();
}


// ******************************************************************************************
// modifico stabilimento attivo (in sessione)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_stabilimento_attivo'){
	$ar_stab = $main_module->set_stabilimento($m_params->cod);
	$ret = array('success' => true);
	echo acs_je($ret);
	exit();
}


// ******************************************************************************************
// elenco stabilimenti selezionabili da utente
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$ar_stab = $main_module->get_stabilimenti();
	echo acs_je($ar_stab);
	exit();
}



?>


{"success":true, "items": [

			{
			 xtype: 'grid',
	         loadMask: true,	
	
			 store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							       },
			        				
			        			   doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root',
						            idProperty: 'order'						            
						        }
							},
							
		        			fields: ['cod', 'des']							
									
			}, //store
				

			      columns: [	
			      
			       {
	                header   : 'codice',
	                dataIndex: 'cod',
	                width: 100
	                },
			      {
	                header   : 'Descrizione',
	                dataIndex: 'des',
	                flex: 1
	                }
	        
		         ] ,
	         
        		listeners: {
        		
        					  celldblclick: {								
								  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){									  
								  	rec = iView.getRecord(iRowEl);
								  	var win = this.up('window');	
								  	
								  	<?php if(isset($m_params->k_ordine) && strlen($m_params->k_ordine) > 0){?>
								  	
    								  	Ext.Ajax.request({
    									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_stabilimento_ordine',
    									        timeout: 2400000,
    									        method     : 'POST',
    									        waitMsg    : 'Data loading',
    			 								jsonData:  {
    			 										rec : rec.data,
    			 										k_ordine : <?php echo j($m_params->k_ordine); ?>
    			 								
    			 								},  
    									        success : function(result, request){
    									       		jsonData = Ext.decode(result.responseText);
    									       		if(jsonData.msg != ''){
    									       		   win.close();
    									       		}else{
                            							win.fireEvent('afterOkSave', win);
                            						}
    									        },
    									        failure    : function(result, request){
    									            Ext.Msg.alert('Message', 'No data to be loaded');
    									        }
    									  });	
								  	
								  	<?php }else{?>
								  	
    								  	Ext.Ajax.request({
    									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_stabilimento_attivo',
    									        timeout: 2400000,
    									        method     : 'POST',
    									        waitMsg    : 'Data loading',
    			 								jsonData:  rec.data,  
    									        success : function(result, request){
    												location.reload(); 
    												//win.close();
    									        },
    									        failure    : function(result, request){
    									            Ext.Msg.alert('Message', 'No data to be loaded');
    									            loc_win.fireEvent('afterUpdateRecord', loc_win);
    									        }
    									  });	
								  	
								  	
								  	<?php }?>							  	
								  	
																  	
								  	
								  }
							  }
        		
				}, 
	   		
			
		}//grid
 				
	
     ]
        
 }