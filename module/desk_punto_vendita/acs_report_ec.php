<?php
require_once "../../config.inc.php";
set_time_limit(240);
$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// FORM richiesta parametro
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
	
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['todo_params'] = strtr($_REQUEST['todo_params'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));	
?>	
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
            
            items: [        
						 {
				            xtype: 'combo',
							name: 'f_cliente_cod',
							fieldLabel: 'Cliente',
							minChars: 2,			
				            
				            store: {
				            	pageSize: 1000,
				            	
								proxy: {
						            type: 'ajax',
						            url : 'acs_get_select_json.php?select=search_cli_anag',
						            reader: {
						                type: 'json',
						                root: 'root',
						                totalProperty: 'totalCount'
						            }
						        },       
								fields: ['cod', 'descr', 'out_loc'],		             	
				            },
				                        
							valueField: 'cod',                        
				            displayField: 'descr',
				            typeAhead: false,
				            hideTrigger: true,
				            anchor: '100%',
				            
					        listeners: {
					            change: function(field,newVal) {	            	
					            }
					        },            
				
				            listConfig: {
				                loadingText: 'Searching...',
				                emptyText: 'Nessun cliente trovato',
				                
				
				                // Custom rendering template for each item
				                getInnerTpl: function() {
				                    return '<div class="search-item">' +
				                        '<h3><span>{descr}</span></h3>' +
				                        '[{cod}] {out_loc}' + 
				                    '</div>';
				                }                
				                
				            },
				            
				            pageSize: 1000
				
				        }            
				],
			buttons: [{
				            text: 'Visualizza',
					        iconCls: 'icon-folder_search-24',		            
					        scale: 'medium',		            
				            handler: function() {
				                this.up('form').submit({
			                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
			                        params: {todo_params: <?php echo j(acs_je($m_params->todo_params)); ?>},
			                        target: '_blank', 
			                        standardSubmit: true,
			                        method: 'POST'
			                  });
				                
				            }
				        }]             
				
        }
]}	
<?php	
	exit;
} //get_json_data 
?>
<?php
// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){

	
	
	
?>
<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
div#my_content h1{font-size: 22px; padding: 5px;}
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table.acs_report{border-collapse:collapse; width: 95%; margin-left: 20px; margin-right: 20px;}
table.acs_report td, table.acs_report th{border-top: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}

/* gestione righe partite */
table.acs_report tr.r1 td{border-top: 1px solid black; font-weight: bold; background-color: #f0f0f0;}
h2.r1{font-size: 2em;}


tr.contatti td{border-top: 0px;}

   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<?php

#eseguo la call per la generazione dei dati
$dt = $id_ditta_default;
$cod_cli = $_REQUEST['f_cliente_cod'];
$def_prt = $_REQUEST['tipo']; //rappresenta la memorizzazione su Sv2



//costruzione del parametro
$cl_p = str_repeat(" ", 246);
$cl_p .= sprintf("%-2s", $dt);
$cl_p .= sprintf("%09s", $cod_cli);
$cl_p .= sprintf("%-3s", $def_prt);

//apro una nuova connessione NON permanente per avere una qtemp personale
$namingMode = DB2_I5_NAMING_ON; // ON or OFF ??????
$conn_qtemp = db2_connect($i5_host, $i5_user, $i5_psw);

//Non uso ToolKit perche' altrimenti non condivide la QTEMP
$qry1	 =	"CALL {$cfg_mod_DeskPVen['report_ec']}.DC0P32B('{$cl_p}')"; 
$stmt1   = 	db2_prepare($conn_qtemp, $qry1);
echo db2_stmt_errormsg();
$result1 = 	db2_execute($stmt1);



//recupero i dati generati (INTESTAZIONI)
$sql = "SELECT * FROM QTEMP.WBR31G60 WHERE WSTP<>'' order by WSPROG";
$stmt = db2_prepare($conn_qtemp, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

$ar_h2 = array();
while ($r = db2_fetch_assoc($stmt)) {

	if ($r['WSTP'] == 'I3'){
		$tel = trim($r['WSNOTA']);
		continue;
	}
	if ($r['WSTP'] == 'I4'){
		$fax = trim($r['WSNOTA']);
		continue;
	}
	if ($r['WSTP'] == 'I5'){
		$email = trim($r['WSNOTA']);
		continue;
	}

	if ($r['WSTP'] == 'I1')
		$r_cls = 'r1';
	else
		$r_cls = '';

	$ar_h2[] =  "<h2 class={$r_cls}>" . trim($r['WSNOTA']) . "</h2>";
}


//aggiungo anche l'indirizzo del cliente

$ar_email_to[] = array(trim($email), "CLIENTE " . j(trim($email)) . " (" .  trim($email) . ")");
$ar_email_json = acs_je($ar_email_to);



?> 
 
 
 
 
 
 
 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<div id='my_content'>



<H3 class=acs_report>
	CONTABILITA' CLIENTI - ESTRATTO CONTO
</H3>

<span style="float: right">Data/ora emissione: <?php echo date('d/m/Y H:i')?></span>

	<TABLE class=acs_report width="100%">
	<tr><td colspan=3>
	<?php
		foreach($ar_h2 as $h2) echo $h2; 
	 ?>
	</td></tr>

	<tr><td colspan=3>&nbsp;</td></tr>
	</TABLE>
	<br/>
	
	
<?php	
//recupero i dati generati (RIGHE E/C)
	$sql = "SELECT * FROM QTEMP.WBR31G60 WHERE WSTP='' order by WSPROG";	
	$stmt = db2_prepare($conn_qtemp, $sql);
	echo db2_stmt_errormsg();	
	$result = db2_execute($stmt);
	
	?>

	
	
	
	
	<table class=acs_report width="100%">
	
	<TR>
	 <TH>Rif.E/conto</TH>
	 <TH>Documento</TH>
	 <TH>Data</TH>
	 <th class=number>Importo</th>
	 <th>Descrizione</th>
	 <th>Scadenza</th>
	 <th>Note</th>
	</TR>
	
	<?php
	
	while ($r = db2_fetch_assoc($stmt)) {
		
		//prima riga di partita
		if ($r['WSTRIG'] == 'N'){
			
			//prima creo una riga vuota di separazione
			echo "<tr class=r_sep><td colspan=7>&nbsp;</td></tr>";
			
			$tr_cls = 'r1';
			$td_rif_ec = trim($r['WSRESE']) . " " . trim($r['WSRTPP']) . " " . n($r['WSRNRP'], 0, 'N', 'Y');
		}
		else {
			$tr_cls = '';
			$td_rif_ec = '&nbsp;';
			
		}
		
		if(trim($r['WSESER']) == 2999){
		    $rif = "&nbsp;";
		    $doc = "&nbsp;";
		    $sca = "&nbsp;";
		    $note = "&nbsp;";
		}else{
		    
		    $rif = $td_rif_ec;
		    $doc = trim($r['WSESER']) . " " . trim($r['WSTPPA']) . " " . $r['WSNRPA'];
		    $sca = print_date($r['WSDTSC']);
		    $note = trim($r['WSNOT2']);
		    
		}	
		
		echo "<tr class=\"{$tr_cls}\">";
 		 echo "<td>" . $td_rif_ec . "</td>";
 		 echo "<td>" . trim($r['WSESER']) . " " . trim($r['WSTPPA']) . " " . n($r['WSNRPA'], 0, 'N', 'Y') . "</td>";
 		 echo "<td>" . print_date($r['WSDTPA']) . "</td>";
 		 echo "<td class=number>" . n($r['WSIMP'],2) . $r['WSDA'] . "</td>";
		 echo "<td>" . $r['WSDESC'] . "</td>";
		 echo "<td>" . print_date($r['WSDTSC']) . "</td>";
		 echo "<td>" . $r['WSNOT2'] . "</td>";		 
		echo "</tr>";
	}
	
	?>
	</table>
	
 <?php
//stampo il report 

 
  db2_close($conn_qtemp);
 exit;	
}
?>