<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());



function parametri_sql_where($form_values){

	global $s;

	$sql_where.= sql_where_by_combo_value('TD.TDSTAB', $form_values->f_sede);
	
	$sql_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);

	$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);

	$sql_where.= sql_where_by_combo_value('MTFORN', $form_values->f_fornitore);

	$sql_where.= sql_where_by_combo_value('MTAVAN', $form_values->f_stato_fornitore);
	
	$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values->f_cliente);
	
	$sql_where.= sql_where_by_combo_value('MTART', $form_values->f_cod_art);
	
	
	if (strlen($form_values->f_cons_cli_da) > 0)
	    $sql_where .= " AND TD.TDDTEP >= {$form_values->f_cons_cli_da}";
	if (strlen($form_values->f_cons_cli_a) > 0)
	    $sql_where .= " AND TD.TDDTEP <= {$form_values->f_cons_cli_a}";
	
    if (strlen($form_values->f_cons_for_da) > 0)
        $sql_where .= " AND TD_ACQ.TDDTEP >= {$form_values->f_cons_for_da}";
    if (strlen($form_values->f_cons_for_a) > 0)
        $sql_where .= " AND TD_ACQ.TDDTEP <= {$form_values->f_cons_for_a}";
	    
    //solo ordini evasi
    if ($form_values->f_evasi == "Y"){
        $sql_where.= " AND TD.TDFN11 = 1 ";
    }
    //solo ordini da evadere
    if ($form_values->f_evasi == "N"){
        $sql_where.= " AND TD.TDFN11 = 0 ";
    }

    return $sql_where;
}


if ($_REQUEST['fn'] == 'exe_conferma_fornitura'){

	$m_params = acs_m_params_json_decode();

	$all_ord=$m_params->all_ord;

	$use_session_history = microtime(true);


	foreach ($all_ord as $v){
		 

		$sh = new SpedHistory();
		$sh->crea(
				'conferma_fornitura',
				array(
						"k_ordine"		=> $v->k_ordine, //il tddocu
						"use_session_history" => $use_session_history,
						"num_prog"		=>  $v->k_prog_MTO,
						"tipo_op"		=>  $v->tipo_op

							
				)

				);

	}


	$sh = new SpedHistory();
	$sh->crea(
			'conferma_fornitura',
			array(
					"k_ordine"		=> '', //il tddocu
					"tipo_op"		=>  $v->tipo_op,
					"end_session_history" => $use_session_history
			)
			);
	exit;
}





if ($_REQUEST['fn'] == 'get_json_data_stato_ordine'){

	global $cfg_mod_Spedizioni;
	global $s;

	$m_params = acs_m_params_json_decode();

	$divisione_selected=$m_params->divisione;
	
    echo acs_je($s->get_options_stati_ordine('Y', array('divisione' => $divisione_selected)));
	
    exit();
}


if ($_REQUEST['fn'] == 'get_json_data_tipologia'){

	global $cfg_mod_Spedizioni;
	global $s;

	$m_params = acs_m_params_json_decode();

	$divisione_selected=$m_params->divisione;

	echo acs_je(find_TA_std('TIPOV', null, 1, null, null, null, null, array('divisione' => $divisione_selected)));

	exit();
}



if ($_REQUEST['fn'] == 'get_json_data_elenco_fornitori'){

	global $cfg_mod_Spedizioni;

	$ret = array();

	$m_params = acs_m_params_json_decode();
	
	if(strlen($_REQUEST['query']) > 0){
	    $sql_where = " AND UPPER(CFRGS1)  LIKE '%" . strtoupper($_REQUEST['query']) . "%'";
	    
	}
	
    $sql = "SELECT DISTINCT MTFORN, CFRGS1
	        FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MT
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
               ON MT.MTDT = CF.CFDT AND MT.MTFORN = CF.CFCD AND CFTICF = 'F'
            WHERE MTDT = '{$id_ditta_default}' {$sql_where}
            ORDER BY CFRGS1";

    
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);


	while ($row = db2_fetch_assoc($stmt)){
			
		$ret[] = array("id"=>trim($row['MTFORN']), "text" =>trim($row['CFRGS1']));
		
	}

	echo acs_je($ret);
	exit();
}


if ($_REQUEST['fn'] == 'get_parametri_form'){

	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';

	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
		$ar_area_spedizione_txt = implode(',', $ar_ar2);

			
		?>

{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: false,
	autoScroll:true,
	title: '',
	buttonAlign:'center',
	buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "REMINDER");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	             handler: function() {
	                form = this.up('form').getForm();
	                   	                
		               if (form.isValid()){
		               
		              if (Ext.isEmpty(form.findField("f_fornitore").getValue())){
		              
		              
		              	/*acs_show_msg_error('Inserire un fornitore per procedere');
		              	return false;*/
		              
        	              acs_show_win_std('Elenco fornitori',
                         'acs_sollecito_forniture.php?fn=open_elenco',
                          { form_values: form.getValues() },
                          400, 400, {}, 'icon-leaf-16');
                          this.up('window').close(); 
		            
		              }else{
		              	   acs_show_panel_std('acs_sollecito_forniture.php?fn=open_tab', 'sollecito_forniture', {form_values: form.getValues()});
			               this.up('window').close(); 
		              }
		               	                	                
			              	  
		                }	
	                
	                               
	            }
	        }
    			, { xtype:'splitbutton',
                     text: 'Report',
                      iconCls: 'icon-print-32',
                    scale: 'large',
                    handler: function(){ this.maybeShowMenu(); },
                    menu: { xtype: 'menu', 
    		        		items: [
    	        
                    	        	  { text: 'Avanzamento forniture',
                        	            iconCls: 'icon-print-16',
                        	            scale: 'large',	            
                        	            handler: function() {
                        	                     form = this.up('form').getForm();
                                                 this.up('form').submit({
                                                 url: 'acs_sollecito_forniture_report.php',
                                                 target: '_blank', 
                                                 standardSubmit: true,
                                                 method: 'POST',                        
                                                 params: {
                                                    form_values: Ext.encode(form.getValues()),
                        							//list_selected_stato: Ext.encode(list_selected_id)
                        					    },
                                          });}}          	                
                        	            
                    	            , { text: 'Riepilogo consegne',
                    	                iconCls: 'icon-print-16',
	          							scale: 'large',	            
	          							handler: function() {
	          							form = this.up('form').getForm();
                  						if (form.isValid()){
                      						acs_show_win_std('Riepilogo consegne per data/fornitore',
                       					    'acs_report_ordini_fornitori_aperti.php?fn=open_form',
                       					 	{ filter: form.getValues() },
                       					 	400, 150, {}, 'icon-print-16');
                   					 	}	
     		      					  }} 
                        	            ]}} <!-- end items split button/end menu/end splitbutton -->     
    	                      ], <!-- items buttons -->
                
            items: [
	            
	             {
					xtype: 'fieldset',
					layout: 'anchor',
					
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
		             
		                 {
							flex: 1,
							name: 'f_sede',
							xtype: 'combo',
							fieldLabel: 'Sede',
							multiSelect : true,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	anchor: '-5',
						    margin: "5 10 5 10",															
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('START'), '') ?> 	
								    ] 
								}						 
							},
	             		{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							margin: "5 10 5 10",	
						    anchor: '-5',
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							anchor: '-5',
							margin: "5 10 5 10",
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_stato_ordine',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								},
								   extraParams: {
        		 				},
        		 					doRequest: personalizza_extraParams_to_jsonData        		
							},
							
							fields: [{name:'id'}, {name:'text'}],
							}					 
							},
							 {
							flex: 1,
							name: 'f_stato_fornitore',
							xtype: 'combo',
							fieldLabel: 'Stato forniture',
							multiSelect : true,
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	anchor: '-5',
						    margin: "5 10 5 10",															
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('STMTO'), '') ?> 	
								    ] 
								}
								
								,listeners: {
            					
            					change: function(field,newVal) {
	            				var form = this.up('form').getForm();
	            				var data_cli = this.up('window').down('#data_cli');
	            				var data_for = this.up('window').down('#data_for');
	            				console.log(newVal);
	            				if(newVal == 'P'){
	            				  data_for.enable();
	            				}
	            				
	            				if(newVal != 'P'){
	            				  data_for.disable();
	            				}
	            				
	            					}
         				}						 
							},
							{
						
			            xtype: 'combo',
						name: 'f_fornitore',
						fieldLabel: 'Fornitore',
						anchor: '-15',
						margin: "5 15 5 10",	
						minChars: 2,	
						forceSelection:true,		
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_elenco_fornitori',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['id', 'text'],		             	
			            },
                        
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}]' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        	       },{
						
			            xtype: 'combo',
						name: 'f_cliente',
						fieldLabel: 'Cliente',
						anchor: '-15',
						margin: "5 15 5 10",	
						minChars: 2,	
						forceSelection:true,		
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_get_select_json.php?select=search_cli',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['cod', 'descr', 'out_loc', 'ditta_orig'],		             	
			            },
                        
						valueField: 'cod',                        
			            displayField: 'descr',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun cliente trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{descr}</span></h3>' +
			                        '[{cod}] {out_loc} {ditta_orig}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        	       }, {
						
			            xtype: 'combo',
						name: 'f_cod_art',
						fieldLabel: 'Codice articolo',
						anchor: '-15',
						margin: "5 15 5 10",	
						minChars: 2,	
						forceSelection:true,		
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : 'acs_elenco_articoli.php?fn=get_json_data_articoli',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['codice', 'articolo'],		             	
			            },
                        
						valueField: 'codice',                        
			            displayField: 'codice',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun articolo trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{codice}</span></h3>' +
			                        '[{articolo}]' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000
        	       }, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: '',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , margin : '0 0 0 10'
						   , fieldLabel: 'Data cons. ordine cliente dal'
						   , labelWidth: 170
						   , name: 'f_cons_cli_da'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , margin : '0 30 0 5'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelWidth: 10
						   , labelAlign: 'right'
						   , name: 'f_cons_cli_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}, 	  	{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						itemId : 'data_for',
						disabled : true,
						fieldLabel: '',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , margin : '0 0 0 10'
						   , fieldLabel: 'Data cons. ordine fornitore dal'
						   , labelWidth: 170
						   , name: 'f_cons_for_da'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , margin : '0 30 0 5'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelWidth: 10
						   , labelAlign: 'right'
						   , name: 'f_cons_for_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}, {
							name: 'f_evasi',
							xtype: 'radiogroup',
							margin: "5 0 5 10",
							fieldLabel: 'Ordini evasi',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: 'T'
							
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Da evadere'
		                          , inputValue: 'N'
								  , checked: true
							    
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Evasi'
								  , checked: false	
		                          , inputValue: 'Y'
		                          , width: 50
		                        }]
						}
			
	             ]
	             },                                     
            ]
        }

	
]}

<?php	
 exit;	
}




if ($_REQUEST['fn'] == 'get_data_grid'){

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;
	
	if(isset($m_params->open_request->fornitore) && strlen($m_params->open_request->fornitore) > 0){
	    $form_values->f_fornitore = $m_params->open_request->fornitore;
	}
	
	 $sql_where = parametri_sql_where($form_values);
	 

	 $sql = "SELECT MT.*, TD.*, RD.RDSTAT AS ST_RIGA, RD.RDART, (RD.RDQTA - RD.RDQTE) as QTRES, RD.RDQTA, RD.RDQTE,
            TD_ACQ.TDSTAT AS STATO_ACQ, TD_ACQ.TDTPDO AS TIPO_ACQ, TD_ACQ.TDDTEP AS DATA_OF, TD_ACQ.TDDEPO AS MAGA_ACQ,
            TD_DDT.TDSTAT AS STATO_DDT, TD_DDT.TDTPDO AS TIPO_DDT, RT.RTIMPR AS PREZZO, AR.ARFLR3
          FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MT
	      LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
	        ON MTDT=TD.TDDT AND MTTIDO=TD.TDOTID AND MTINUM=TD.TDOINU AND MTAADO=TD.TDOADO AND MTNRDO=TD.TDONDO
          LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD_ACQ
	        ON MTDT=TD_ACQ.TDDT AND MTTIDQ=TD_ACQ.TDTIDO AND MTINUQ=TD_ACQ.TDINUM AND MTAADQ=TD_ACQ.TDAADO AND MTNRDQ=TD_ACQ.TDNRDO
	      LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD_DDT
	        ON MTDT=TD_DDT.TDDT AND MTTIDE=TD_DDT.TDTIDO AND MTINUE=TD_DDT.TDINUM AND MTAADE=TD_DDT.TDAADO AND MTNRDE=TD_DDT.TDNRDO    
    	  LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
            ON MTDT=RT.RTDT AND MTTIDO=RT.RTTIDO AND MTINUM=RT.RTINUM AND MTAADO=RT.RTAADO AND MTNRDO=RT.RTNRDO AND MTNWRE=RT.RTNREC AND RT.RTVALU='EUR'
          LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc']} RD
            ON RD.RDDT = MTDT AND RD.RDTIDO = MTTIDO AND RD.RDINUM = MTINUM AND RD.RDAADO = MTAADO AND RD.RDNRDO = MTNRDO AND RD.RDNREC = MTNWRE
          LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art']} AR
            ON AR.ARDT = MT.MTDT AND AR.ARART = MT.MTART
          WHERE MTDT = '{$id_ditta_default}' AND MTSTAR NOT IN ('R', 'C') {$sql_where}
    	  ORDER BY TD.TDDTEP, TDDCON, TDDOCU";
	 
	
	 $stmt = db2_prepare($conn, $sql);
	 echo db2_stmt_errormsg();
	 $result = db2_execute($stmt);
	 
	 $data = array();
	 
	 while ($row = db2_fetch_assoc($stmt)) {
	 	
	    $row['c_art'] = acs_u8e($row['MTART']);
	 	$row['d_art'] = acs_u8e($row['MTDART']);
	 	$row['k_ordine'] = $row['TDDOCU']; 
	 	
	 	if(trim($row['MTAADO']) != 0)
	 	     $row['ord_cli']=implode("_", array($row['MTAADO'], $row['MTNRDO'], $row['TDOTPD']));
	 	else 
	 	    $row['ord_cli'] = "";
	 	if(trim($row['MTAADQ']) != 0)
	 	     $row['ord_for']=implode("_", array($row['MTAADQ'], $row['MTNRDQ'], $row['TIPO_ACQ']));
 	     else
 	         $row['ord_for'] = "";
 	         //6 _VO_VO1_2017_000016         _          _6     sprintf("%08s", trim($prog))
         
 	     $row['k_ordine_for'] = implode("_", array($row['MTDT'], $row['MTTIDQ'], $row['MTINUQ'], $row['MTAADQ'], $row['MTNRDQ']));
 	    
 	     $row['k_ordine_cli'] = implode("_", array($row['MTDT'], $row['MTTIDO'], $row['MTINUM'], $row['MTAADO'], sprintf("%06s", $row['MTNRDO'])));
 	     $row["fattura0"] =  $row['MTAADQ'];
 	     $row["bolla0"] = $row['MTAADE'];
 	     //k_ordine_bolla
 	     $row['k_ordine_bolla'] = implode("_", array($row['MTDT'], $row['MTTIDE'], $row['MTINUE'], $row['MTAADE'], $row['MTNRDE']));
 	         
	 	if(trim($row['MTAADE']) != 0)
	 	     $row['ddt_ent']=implode("_", array($row['MTAADE'], $row['MTNRDE'], $row['TIPO_DDT'])). " [".$row['STATO_DDT']."]";
 	     else
 	         $row['ddt_ent'] = "";
	 	$ordine_exp = $s->k_ordine_td_decode_xx($row['TDDOCU']);
	 	$row['ditta_origine'] = $ordine_exp['TDDT'];
	 	$row["raggr"] =  $row['TDCLOR'];
	 	$row["data"] =  $row['TDDTEP'];
	 	$row["data_of"] =  $row['DATA_OF'];
	 	$row['qtip_tipo'] = acs_u8e($row['TDDOTD']);
	 	$row['stato'] = $row['TDSTAT'];
	 	$row['st_reg'] = trim($row['MTSTAR']);
	 	$row['a_mag'] = trim($row['ARFLR3']);
	 	$row['st_mto'] = trim($row['MTAVAN']);
	 	$row['maga'] = trim($row['MTDEPO']);
	 	$row['maga_acq'] = trim($row['MAGA_ACQ']);
	 	if(trim($row['MTSTAR']) == 'M')
	 	    $row['d_st_mto'] = 'Disponibile';
	 	else
	 	    $row['d_st_mto'] = $s->decod_std('STMTO', trim($row['MTAVAN'])); 
	 	$row['qtip_stato'] = acs_u8e($row['TDDSST']);
	 	$row['riga'] = acs_u8e($row['MTRIGA']);
	 	$row['nrec'] = acs_u8e($row['MTNWRE']);
	 	$row['prezzo'] = acs_u8e($row['PREZZO']);
	 	$row['prog'] = substr($row['MTFILL'], 10, 8);
	 	$row['st_riga'] = acs_u8e($row['ST_RIGA']);
	 	$row['st_acq'] = $row['STATO_ACQ'];
	 	$ta_sys = find_TA_sys('BSTA', trim($row['STATO_ACQ']));
	 	$row['qtip_st_acq'] = $ta_sys[0]['text'];
	 	
	 	
        // giacenza 
	 	$row_p = $main_module->_gest_abilita_ris_mat( $row['RDART'], ($row['RDQTA']-$row['RDQTE'])
	 	, $row['MTTIDO'], $row['MTINUM'], $row['MTAADO'], $row['MTNRDO']);
	 	$row['bc'] = $row_p['bc'];
	 	$row['fl_mts'] = $row_p['fl_mts'];
	 	$row['giac'] = $row_p['giac'];
	 	$row['qta_gia_ris'] = $row_p['qta_gia_riservata'];
	 	$row['abilita_ris_mat'] = $row_p['abilita_ris_mat'];
	 	
	 	$data[] = $row;
	 }

	 if (count($form_values->f_stato_fornitore) == 0 || in_array('P', $form_values->f_stato_fornitore)) {
	
	     
	  $sql_m3 = "SELECT M3.*, TD.TDDTEP AS DATA_OF 
                FROM {$cfg_mod_Spedizioni['file_dettaglio_wizard']} M3
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD
	               ON M3DT=TD.TDDT AND M3TIDO=TD.TDTIDO AND M3INUM=TD.TDINUM AND M3AADO=TD.TDAADO AND M3NRDO=TD.TDNRDO
	            WHERE M3DT = '{$id_ditta_default}' AND M3CCON = '{$form_values->f_fornitore}'
                {$sql_where}";
	 	
	 $stmt_m3 = db2_prepare($conn, $sql_m3);
	 echo db2_stmt_errormsg();
	 $result = db2_execute($stmt_m3);
	 while ($r_m3 = db2_fetch_assoc($stmt_m3)) {
	     
	     $r_m3['c_art'] = acs_u8e($r_m3['M3ART']);
	     $r_m3['d_art'] = acs_u8e($r_m3['M3DART']);
	     $r_m3['st_mto'] = 'P'; 
	     $r_m3['st_acq'] = $r_m3['M3STDO'];
	     $r_m3["data_of"] =  $r_m3['DATA_OF'];
	     $r_m3["fattura0"] =  $r_m3['M3AADO'];
	     $r_m3['ord_for']=implode("_", array($r_m3['M3AADO'], $r_m3['M3NRDO'], $r_m3['M3TPDO']));
	     $r_m3['k_ordine_for'] = implode("_", array($r_m3['M3DT'], $r_m3['M3TIDO'], $r_m3['M3INUM'], $r_m3['M3AADO'], $r_m3['M3NRDO']));
	     $r_m3['d_st_mto'] = 'Ordinato';
	     $data[] = $r_m3;
	 }
	 
}
	 
	 echo acs_je($data);
	
	 exit();
				
}


$m_params = acs_m_params_json_decode();
if(isset($m_params->fornitore) && strlen($m_params->fornitore) > 0){
    $cod_forn = $m_params->fornitore;
}else{
    $cod_forn= $m_params->form_values->f_fornitore;
}

$sql_desc_forn = "SELECT RDDFOR
FROM {$cfg_mod_Spedizioni['file_righe']}
WHERE RDFORN = '{$cod_forn}' FETCH FIRST 1 ROWS ONLY";

$stmt_desc_forn = db2_prepare($conn, $sql_desc_forn);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_desc_forn);

$row_desc_forn = db2_fetch_assoc($stmt_desc_forn);
$denom_forn= $row_desc_forn['RDDFOR'];



if ($_REQUEST['fn'] == 'open_tab'){
			?>
				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'Reminder',
        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE',  ignoreRightMouseSelection: true},
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
		tbar: new Ext.Toolbar({
	            items:['<b>Gestione/controllo riordini - Fornitore: <?php echo $denom_forn; ?> </b> ' , '->'
	               , {iconCls: 'icon-leaf-16',
	            	    text: 'Elenco fornitori', 
		           		handler: function(event, toolEl, panel){
		           	 	var m_form_win = Ext.getCmp('<?php echo $m_params->win_id; ?>');
			           		m_form_win.show();
		           	 	}
		           	 }
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		           	<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
      
					store: {
						
						xtype: 'store',
						autoLoad:true,
					  //  groupField: 'RDTPNO',			
										        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			 'st_mto', 'd_st_mto', 'ditta_origine', 'k_ordine', 'ord_cli', 'ord_for', 'data_of', 'st_acq',
		            			 'ddt_ent', 'c_art', 'd_art', 'TDDCON', 'TDVSRF', 'data', 'riga', 'nrec', 'prezzo', 'st_riga',
		            			 'TDOTPD', 'raggr', 'qtip_tipo', 'stato', 'qtip_stato', 'k_ordine_for', 'k_ordine_bolla',
		            			 'fattura0', 'bolla0', 'prog', 'st_reg', 'a_mag', 'maga', 'maga_acq', 'qtip_st_acq'
		            			 , 'bc', 'giac', 'fl_mts', 'RDART', 'qta_gia_ris', 'abilita_ris_mat' , 'QTRES', 'RDQTA', 'RDQTE', 'k_ordine_cli'
		            			 , 'MTTIDO', 'MTINUM', 'MTAADO', 'MTNRDO'
		        			]
		    			},
		    		
			        columns: [
       { header   : 'Art'
       , dataIndex: 'RDART'
       , width    : 50
       , hidden   : true
       } 
     
     , { header   : 'Bc'
       , dataIndex: 'bc'
       , width    : 30
       , hidden   : true
       }
     , { header   : 'MTS'
       , dataIndex: 'fl_mts'
       , width    : 20
       , hidden   : true
       }
     , { header   : 'Abilita opz ris mat'
       , dataIndex: 'abilita_ris_mat'
       , width    : 20
       , hidden   : true
       }
     , { header   : 'Qta'
       , dataIndex: 'RDQTA'
       , width    : 50
       , hidden   : true
       }
     , { header   : 'Evasa'
       , dataIndex: 'RDQTE'
       , width    : 50
       , hidden   : true
       }
     , { header   : 'Residuo'
       , dataIndex: 'QTRES'
       , width    : 50
       , hidden   : true
       }
     , { header   : 'Giacenza'
       , dataIndex: 'giac'
       , width    : 50
       , hidden   : true
       }
     , { header   : 'Gia ris'
       , dataIndex: 'qta_gia_ris'
       , width    : 50
       , hidden   : true
       } 
     ,
			       		 {
			                header   : 'Stato',
			                dataIndex: 'd_st_mto', 
			                width     : 95,
			                filter: {type: 'string'}, filterable: true
			             },{ 
			                header   : 'Ordine fornitore',
			                dataIndex: 'ord_for', 
			                width     : 150,
			                filter: {type: 'string'}, filterable: true
			             }, {header: 'St', 			width: 30, dataIndex: 'st_acq', 
	    			     filter: {type: 'string'}, filterable: true,
	    			    renderer: function(value, metaData, record){
	    			    
							if (record.get('qtip_st_acq') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_st_acq')) + '"';	    			    
	    			    
							if (value == 'V') return '';
							if (value == 'R') return '';
							if (value == 'G') return '';
							if (value == 'B') return '';
							return value;
	    			    }	   	     
	    	    		},{
			                header   : 'Consegna',
			                dataIndex: 'data_of', 
			                filter: {type: 'string'}, filterable: true,
			                width    : 70,
			                renderer: date_from_AS
			            
			             },
			             { 
			                header   : 'Ordine cliente',
			                dataIndex: 'ord_cli', 
			                width     : 150,
			                filter: {type: 'string'}, filterable: true
			             }, {header: 'St', 			width: 30, dataIndex: 'stato', 
	    			     filter: {type: 'string'}, filterable: true,
	    			    renderer: function(value, metaData, record){
	    			    
							if (record.get('qtip_stato') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';	    			    
	    			    
							if (value == 'V') return '';
							if (value == 'R') return '';
							if (value == 'G') return '';
							if (value == 'B') return '';
							return value;
	    			    }	   	     
	    	    		},
	    	    		  { header: 'Tp',
			          		width: 30, 
			          		dataIndex: 'TDOTPD', 
			          		tdCls: 'tipoOrd', 
			          		hidden : true,
			          		filter: {type: 'string'}, filterable: true,
	        	     		renderer: function (value, metaData, record, row, col, store, gridView){						
							metaData.tdCls += ' ' + record.get('raggr');
								if (record.get('qtip_tipo') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';								
																			
							return value;			    
	    			}
	    			},
			             
			             {
			                header   : 'Data',
			                dataIndex: 'data', 
			                filter: {type: 'string'}, filterable: true,
			                width    : 70,
			                renderer: date_from_AS
			            
			             },
			              {
			                header   : 'Cliente',
			                dataIndex: 'TDDCON', 
			                flex    : 130,
			                filter: {type: 'string'}, filterable: true
			             }, {
			                header   : 'Riferimento',
			                dataIndex: 'TDVSRF', 
			                flex    : 80,
			                filter: {type: 'string'}, filterable: true
			             }, {
			                header   : 'Descrizione articolo',
			                dataIndex: 'd_art', 
			                flex    : 150,
			                filter: {type: 'string'}, filterable: true,
			                renderer: function(value, p, record){
			                	return record.get('d_art') + " [" + record.get('c_art') + "]";
			    			}		                			                
			             }
			             
			             ,{
			                header   : 'DDT Entrata',
			                dataIndex: 'ddt_ent', 
			                filter: {type: 'string'}, filterable: true,
			                width    : 150,
			            
			             }
			            
			         ],  

						listeners: {
					
					
						  celldblclick: {								
							  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){									  
							  	
							  	var rec = iView.getRecord(iRowEl);
							    var col_name = iView.getGridColumns()[iColIdx].dataIndex;	
							  	
							  	if(rec.get('a_mag') == 'M')
							  		acs_show_win_std('Loading.', 'acs_background_mrp_art.php', {dt: rec.get('ditta_origine'), rdart: rec.get('c_art'), from_wz : 'Y', maga : rec.get('maga'), k_ordine : rec.get('k_ordine_cli')}, 1100, 600, null, 'icon-shopping_cart_gray-16');								  	
							  	
							  	//if(col_name == 'd_art')
							  	//	acs_show_win_std('Loading.', 'acs_background_mrp_art.php', {dt: rec.get('ditta_origine'), rdart: rec.get('c_art')}, 1100, 600, null, 'icon-shopping_cart_gray-16');								  	
							  	
							  }
						  }	
						  
						  , itemcontextmenu : function(grid, rec, node, index, event) {
				  			event.stopEvent();
				  													  
						  var voci_menu = [];
					      row = rec.data.k_ord_art;
					      
					        <?php  if ($js_parameters->only_view  != 1){?>
					      				      
					       voci_menu.push({
			         		text: 'Costo articolo ordine cliente',
			        		iconCls : 'icon-folder_search-16',          		
			        		handler: function () {
			        		
			        		  my_listeners = {
				        			    afterModArt: function(from_win){
				        				grid.getStore().load();
	 									from_win.close();
	 											
								        	}
										}
			        		
			        		acs_show_win_std('Costo articolo ordine cliente ' + rec.get('ord_cli'), 'acs_get_order_rows_gest.php?fn=form_mod_art', {row: rec.data,  k_ordine: rec.get('k_ordine'), magazzino : 'Y', art_mto : 'Y', from_reminder : 'Y'}, 500, 170, my_listeners, 'icon-pencil-16');          		
			        		
								
				                }
			    		}); 
			    		
			    		<?php }?>
					      
					
					      
					       if(rec.get('st_mto') == 'P'){
					      //ORDINATO
					      
					        if(rec.get('fattura0') != 0){
					      	 voci_menu.push({
    			         		text: 'Righe ordine fornitore',
    			        		iconCls : 'icon-leaf-16',          		
    			        		handler: function () {
    					    		  acs_show_win_std('Righe ordine fornitore', 
    				                			'acs_get_order_rows_gest.php?fn=open_grid', {
    				                             k_ordine: rec.get('k_ordine_for'), righe_doc : 'Y',
    				                             rddes2: rec.get('ord_for'),
    				                             f_reminder : 'Y'
    									}, 900, 600, {}, 'icon-shopping_cart_gray-16');
    				                }
    			    		});	
    			    	  }
    			    	  
    			    	    <?php  if ($js_parameters->only_view  != 1){?>
    			    	
    			    		 	 voci_menu.push({
    			         		text: 'Modifica globale ordine',
    			        		iconCls : 'icon-pencil-16',          		
    			        		handler: function () {
    			        		
    			        			my_listeners = {
                     		  			afterOkSave: function(from_win){
                     		  				grid.getStore().load();
                     						from_win.close();  
                    			        		}
                    	    				};	
	        		
			         			acs_show_win_std('Ordine fornitore ' + rec.get('ord_for'), '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_stato_data', {k_ordine : rec.get('k_ordine'), prog : rec.get('prog'), old_st : rec.get('st_acq'), data_c: rec.get('data_of'), old_mag: rec.get('maga_acq')}, 600, 250,  my_listeners, 'icon-pencil-16');	
    				                }
    			    		});	
    			    		
    			    	
    			    		
					      
					       voci_menu.push({
			         		text: 'Cancella ordine acquisto',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		
			        		Ext.Msg.confirm('Conferma cancellazione', 'Confermi l\'operazione?', function(btn, text){																							    
								   if (btn == 'yes'){					        		
				        		
										Ext.Ajax.request({
										   url        : 'acs_art_critici.php?fn=exe_canc_ord_acq',
										   method: 'POST',
										   jsonData: { 
						        				prog : rec.get('prog'),
						        				k_ordine : rec.get('k_ordine_for')
						        				 }, 
										   
										   success: function(response, opts) {
										   	  
											  grid.getStore().load();									   	  
										   }, 
										   failure: function(response, opts) {
										      Ext.Msg.alert('Message', 'No data to be loaded');
										   }
										});	

									  }
									});
					    		 
				                }
			    		});
			    		
			    		<?php }?>
					      }
					      
					      if(rec.get('st_mto') == 'E'){
					  	 voci_menu.push({
			         		text: 'Righe ordine fornitore',
			        		iconCls : 'icon-leaf-16',          		
			        		handler: function () {
					    		  acs_show_win_std('Righe ordine fornitore', 
				                			'acs_get_order_rows_gest.php?fn=open_grid', {
				                             k_ordine: rec.get('k_ordine_for'), righe_doc : 'Y',
				                             rddes2: rec.get('k_ordine_for'),
				                             f_reminder : 'Y'
									}, 900, 600, {}, 'icon-shopping_cart_gray-16');
				                }
			    		});	
			    		
			    		  if(rec.get('bolla0') != 0){
					      	 voci_menu.push({
    			         		text: 'Righe DDT fornitore',
    			        		iconCls : 'icon-leaf-16',          		
    			        		handler: function () {
    					    		  acs_show_win_std('Righe DDT fornitore', 
    				                			'acs_get_order_rows_gest.php?fn=open_grid', {
    				                             k_ordine: rec.get('k_ordine_bolla'), righe_ddt : 'Y',
    				                             rddes2: rec.get('k_ordine_bolla')
    									}, 900, 600, {}, 'icon-shopping_cart_gray-16');
    				                }
    			    		});	
    			    	  }
			    		}
			    		
			    <?php  if ($js_parameters->only_view  != 1){?>			
			    		
        if(rec.get('fl_mts').trim() == 'Y'
            && rec.get('abilita_ris_mat').trim() =='Y'){
                voci_menu.push({
                    text: 'Prenota materiale'
                        , iconCls : 'icon-shopping_cart_gray-16'
                            , handler: function () { Ext.Msg.confirm('Richiesta conferma'
                                , 'Confermi la generazione documento di prenotazione materiale?'
                                , function(btn, text){ if (btn == 'yes'){
                                    Ext.Ajax.request(
                                    { url     : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_gen_doc_pren'
                                        , method  : 'POST'
                                            , jsonData: { row : rec.data
                                                        , k_ordine: rec.get('k_ordine_for')
                                                        }
                                            , success : function(result, request){ console.log('success');
                                                var jsonData = Ext.decode(result.responseText);
                                                rec.set('abilita_ris_mat', jsonData.ris);
                                                }
                                            , failure : function(result, request){ Ext.Msg.alert('Message', 'No data to be loaded');}
                                    });
                                    
                                }});}
                });
            }
            
            <?php }?>
			    		  		

				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}  
			    	
			      , cellclick : function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
				rec = iView.getRecord(iRowEl);								
				col_name = iView.getGridColumns()[iColIdx].dataIndex;
	
			     
			          if (col_name  == 'ord_cli')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	//var wd = Ext.getCmp('OrdPropertyGridDet');
			        	var wdi = Ext.getCmp('OrdPropertyGridImg');        	
			        	var wdc = Ext.getCmp('OrdPropertyCronologia');

			        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdi.store.load()
						wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdc.store.load()			        												        	
			        	
						Ext.Ajax.request({
						   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
					   }												        	
			         }
							  
						}, viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					          if (record.get('fl_mts')=='Y' && record.get('abilita_ris_mat')=='Y')
					           	return ' colora_riga_verde';
					          if(record.get('a_mag') == 'M')
					          	return ' colora_riga_giallo';
					          /* if (record.get('RDFMAN')=='M')
					           		return ' segnala_riga_rosso';
					           if (record.get('RDFMAN')=='C')
					           		return ' segnala_riga_giallo';				           		
					           if (record.get('RDDTDS')==record.get('TDDTEP'))
					           		return ' segnala_riga_viola';	
					           if (record.get('RDDES2').trim()!='')
					           		return ' segnala_riga_disabled';	*/			           		
					           return '';																
					         }   
					    },
					    
					      <?php  if ($js_parameters->only_view  != 1){?>
					    
			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [  {
                     xtype: 'button',
                    text: 'Genera ddt',
		            iconCls: 'icon-currency_black_euro-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                   var ord = this.up('grid');
                   win = this.up('window');
                            
                           ordine_selected = ord.getSelectionModel().getSelection();
                         
                            list_selected_ord = [];
                           
                            for (var i=0; i<ordine_selected.length; i++) {
				            list_selected_ord.push({
				            	k_ordine: ordine_selected[i].get('k_ordine'),
				            	k_prog_MTO: ordine_selected[i].get('prog'),
				            	flag_1: ordine_selected[i].get('st_mto'),
				            	flag_M: ordine_selected[i].get('st_reg'),
				            	A_MAG: ordine_selected[i].get('a_mag'),
				            	tipo_op: 'CON'
				            	});}
				          
				            if (list_selected_ord.length == 0){
									  acs_show_msg_error('Selezionare almeno un ordine');
									  return false;
								  }
						
							for (var chiave in list_selected_ord) {
						          if (list_selected_ord[chiave].flag_1 != 'P'){
									  acs_show_msg_error('Articolo con ddt');
									  return false;
								  }
								  
								  if (list_selected_ord[chiave].A_MAG == 'M' || (list_selected_ord[chiave].flag_M == 'M' && list_selected_ord[chiave].flag_1 == 'A')){
									  acs_show_msg_error('Articolo gestito a magazzino');
									  return false;
								  }
						   
						   }
						   
						   
								    my_listeners = {
				        			    afterGenera: function(from_win){
				        				ord.getStore().load();
	 									from_win.close();
	 											
								        	}
										}
								  
								acs_show_win_std('Ddt entrata', 'acs_art_critici.php?fn=get_fattura_forn', {list_ddt: list_selected_ord}, 500, 220, my_listeners, 'icon-listino');         
			
			            }

			     },{
                     xtype: 'button',
                    text: 'Genera prebolla entrata',
		            iconCls: 'icon-currency_black_euro-32',
		            scale: 'large',	                     
				    handler: function() {
				    
                   		var ord = this.up('grid');
                   		win = this.up('window');
                        ordine_selected = ord.getSelectionModel().getSelection();
                        list_selected_ord = [];
                           
                            for (var i=0; i<ordine_selected.length; i++) {
				            list_selected_ord.push({
				            	k_ordine: ordine_selected[i].get('k_ordine'),
				            	k_prog_MTO: ordine_selected[i].get('prog'),
				            	flag_1: ordine_selected[i].get('st_mto'),
				            	flag_M: ordine_selected[i].get('st_reg'),
				            	A_MAG: ordine_selected[i].get('a_mag'),
				            	c_art: ordine_selected[i].get('c_art'),
				            	fl_mts: ordine_selected[i].get('fl_mts'),
				            	});}
				          				          
				            if (list_selected_ord.length == 0){
									  acs_show_msg_error('Selezionare almeno un ordine');
									  return false;
								  }
						
							/*for (var chiave in list_selected_ord) {
						         if (list_selected_ord[chiave].flag_1 != 'P'){
		 								  acs_show_msg_error('Articolo con ddt');
									  return false;
								  }
								  
								  if (list_selected_ord[chiave].A_MAG == 'M' || (list_selected_ord[chiave].flag_M == 'M' && list_selected_ord[chiave].flag_1 == 'A')){
									  acs_show_msg_error('Articolo gestito a magazzino');
									  return false;
								  }
						    }*/
						    
						    acs_show_win_std('Prebolla entrata','acs_genera_prebolla.php?fn=grid_conferma_ordine', {list_selected_ord : list_selected_ord, grid_id : ord.getId() }, 800, 500, null, 'icon-shopping_cart_gray-16');
			            }

			     }, '->',
			     
			     {
                     xtype: 'button',
                    text: 'Conferma fornitura',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                   var ord = this.up('grid');
                   var win = this.up('window');
                            
                           ordine_selected = ord.getSelectionModel().getSelection();
                            list_selected_ord = [];
                                                     
                            for (var i=0; i<ordine_selected.length; i++) {
				            list_selected_ord.push({
				            	k_ordine: ordine_selected[i].get('k_ordine'),
				            	k_prog_MTO: ordine_selected[i].get('prog'),
				            	flag_1: ordine_selected[i].get('st_mto'),
				            	flag_2: ordine_selected[i].get('st_riga'),
				            	flag_M: ordine_selected[i].get('st_reg'),
				                A_MAG: ordine_selected[i].get('a_mag'),
				            	tipo_op: 'CON'
				            	});}
				            
				            if (list_selected_ord.length == 0){
									  acs_show_msg_error('Selezionare almeno un ordine');
									  return false;
								  }
						 
						 
					  		  var k_ord = '';
						   for (var chiave in list_selected_ord) {
						       if(k_ord != ''){
				            	if (list_selected_ord[chiave].k_ordine != k_ord){
							  		acs_show_msg_error('Selezionare lo stesso ordine di vendita');
							  	    return false;
						  		}
        						}
							  k_ord = list_selected_ord[chiave].k_ordine;	
						   
						        if (list_selected_ord[chiave].A_MAG == 'M' || list_selected_ord[chiave].flag_M == 'M'){
									  acs_show_msg_error('Articolo gestito a magazzino');
									  return false;
								  }
								  
								 
						        if (list_selected_ord[chiave].flag_1 == 'P' || list_selected_ord[chiave].flag_1 == 'E' || list_selected_ord[chiave].flag_2 == 'SO'){
									  acs_show_msg_error('Articolo ordinato o bloccato');
									  return false;
								  }
								  
								
						   
						   }
								  
								  
								   Ext.Ajax.request({
									        url        : 'acs_art_critici.php?fn=exe_conferma_articoli_MTO',
									        method     : 'POST',
						        			jsonData: {
						        				all_ord: list_selected_ord
											},							        
									        success : function(result, request){
									        //win.close(); 	
						            	      ord.getStore().load();									        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
			
			            }

			     }]
		   }]
          <?php }?>
	        																			  			
	            
        }  

]
}

<?php 
exit;
}

if ($_REQUEST['fn'] == 'exe_cambia_stato_data'){
   
    $m_params = acs_m_params_json_decode();
   // $all_ord=$m_params->all_ord;
    $use_session_history = microtime(true);
    
    //foreach ($all_ord as $v){
        $sh = new SpedHistory();
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'MOD_ORD_ACQ',
                "k_ordine"	=> $m_params->k_ordine,
                "use_session_history" => $use_session_history,
                "vals" => array("RICLIE"  =>  $m_params->prog)
            )
            );
   // }
    
    $sh = new SpedHistory();
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'MOD_ORD_ACQ',
            "k_ordine"	=> $m_params->k_ordine,
            "end_session_history" => $use_session_history,
            "vals" => array("RIDTEP" => sql_f($m_params->form_values->f_data),
                            "RISTDO" => $m_params->old_st,   //vecchio stato
                            "RISTNW" => $m_params->form_values->f_stato,
                            "RICITI" => $m_params->form_values->f_depo,
                            //"RICLIE"  =>  $m_params->prog
                            )   //nuovo stato
        )
        );
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'open_stato_data'){

	$m_params = acs_m_params_json_decode();

	$ta_sys = find_TA_sys('BSTA', trim($m_params->old_st));
	$stato = "Stato corrente: <br><b>[".trim($m_params->old_st)."] ".$ta_sys[0]['text']."</b>";
	
	$ta_sys = find_TA_sys('MUFD', trim($m_params->old_mag));
	$deposito = "Deposito corrente: <br> <b>[".trim($m_params->old_mag)."] ".$ta_sys[0]['text']."</b>";
	
	?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [
			  
			  
			  {
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: ['->',
			     
			          {
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var loc_win = this.up('window');
			            	
			            
			            	
							
								Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_stato_data',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    old_st : '<?php echo $m_params->old_st; ?>',
 			        			    k_ordine : '<?php echo $m_params->k_ordine; ?>',
 			        			    prog : '<?php echo $m_params->prog; ?>',
 			        			    form_values: form.getValues(),
 			        			    //all_ord: <?php echo acs_je($m_params->list_selected_ord); ?>
 			        			    
 								},							        
 						        success : function(result, request){
 						        	var jsonData = Ext.decode(result.responseText);
 						        	loc_win.fireEvent('afterOkSave', loc_win);
		 			            		 
		 			            		},
		 						        failure    : function(result, request){
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });	
							
		
			
			            }
			         }
					
						
						], items: [
						
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
							name: 'f_stato_old',
							xtype: 'displayfield',
							value : <?php echo j($stato)?>,
							fieldLabel: '',
							anchor: '-15',
							width : 260
							// margin : '0 90 0 0',
						 		 
						},
						
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
							xtype: 'displayfield',
							value  : 'Nuovo stato',
						
						},
						
						{
							name: 'f_stato',
							xtype: 'combo',
							fieldLabel: '',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin : '0 0 0 0',
						  	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('BSTA', null, null, 'AO', null, null, 0, '', 'Y'), '') ?> 	
								    ] 
								}			 
							}
						   
						]}
						
							
						
						   
						]},
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
							name: 'f_data_old',
							xtype: 'displayfield',
							value: 'Consegna corrente: <br><b><?php echo  print_date($m_params->data_c, "%d/%m/%Y"); ?></b>',
							fieldLabel: '',
							anchor: '-15',
							width : 260
						 		 
						},
						
						 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
							xtype: 'displayfield',
							value  : 'Nuova data consegna',
						
						},
						{
						     name: 'f_data'
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: ''
						   , width : 80
						   , labelAlign: 'left'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin : '0 0 0 0'
						   , minValue: new Date()
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
					 			}
 					      }
						   
						]}
						
						
						]}, 
					
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
							name: 'f_dep_old',
							xtype: 'displayfield',
							value : <?php echo j($deposito)?>,
							fieldLabel: '',
							anchor: '-15',
							width : 260
						 		 
						},
						
						 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
							xtype: 'displayfield',
							value  : 'Nuovo deposito',
						
						},
						
						{
							name: 'f_depo',
							xtype: 'combo',
							fieldLabel: '',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin : '0 0 0 0',
						  	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_sys('MUFD', null, null, null, null, null, 0, '', 'Y'), '') ?> 	
								    ] 
								}			 
							}
						
						   
						]}
						
						
							
						   
						]}				 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_gen_doc_pren'){
    
    $m_params = acs_m_params_json_decode();
    $rdart=$m_params->row->RDART;
    $rdqta=$m_params->row->RDQTA;
    $rdqte=$m_params->row->RDQTE;
    $mttido=$m_params->row->MTTIDO;
    $mtinum=$m_params->row->MTINUM;
    $mtaado=$m_params->row->MTAADO;
    $mtnrdo=$m_params->row->MTNRDO;
    
    
    //print_r('<pre>Aa'); print_r($m_params); print_r('<br>');
    $row_p = $main_module->_gest_abilita_ris_mat($rdart,$rdqta-$rdqte);
    if ($row_p['fl_mts'] == 'Y' && $row_p['abilita_ris_mat'] == 'Y') {
        $sh = new SpedHistory();
        $sh->crea( 'pers'
            ,  array( "messaggio"	=> 'GEN_DOC_PREN'
                , "k_ordine"	=> $m_params->row->k_ordine_cli
                , "vals" => array("RIART" =>$m_params->row->RDART )
                
            )
            );
        
        
        $ret['success'] = true;
        //$ret['abilita_ris_mat'] = 'N';
        
        if (!is_null($mttido) && strlen(trim($mttido)) > 0) {
            $v_m3 = $main_module->_verifica_m3($mttido, $mtinum, $mtaado, $mtnrdo);
            if ($v_m3['M3FG01'] =='P' || $v_m3['M3FG01'] =='R') {
                $ret['abilita_ris_mat'] = 'N';
            }
        }
        echo acs_je($ret);
        
    }
    exit;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//**********************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid_forn'){
    //**********************************************************
    
    $m_params = acs_m_params_json_decode();
    $sql_where = parametri_sql_where($m_params->form_values);
    
    
    //inserire join con TD
    $sql = "SELECT COUNT(MTART) AS ARTICOLI, MTFORN AS C_FOR, CFRGS1 AS D_FOR
            FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MT
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
	        ON MTDT=TD.TDDT AND MTTIDO=TD.TDOTID AND MTINUM=TD.TDOINU AND MTAADO=TD.TDOADO AND MTNRDO=TD.TDONDO
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD_ACQ
	        ON MTDT=TD_ACQ.TDDT AND MTTIDQ=TD_ACQ.TDTIDO AND MTINUQ=TD_ACQ.TDINUM AND MTAADQ=TD_ACQ.TDAADO AND MTNRDQ=TD_ACQ.TDNRDO            
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
            ON MT.MTDT = CF.CFDT AND MT.MTFORN = CF.CFCD AND CFTICF = 'F'
            WHERE MTDT = '{$id_ditta_default}' AND MTSTAR NOT IN ('R', 'C') 
            {$sql_where} GROUP BY MTFORN, CFRGS1 ORDER BY CFRGS1";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);

    if(isset($m_params->form_values->f_cod_art) && trim($m_params->form_values->f_cod_art) != '')
        $where = " AND M3DT = '{$m_params->form_values->f_cod_art}'";
    
   $sql_m3 = "SELECT COUNT(*) AS ARTICOLI
               FROM {$cfg_mod_Spedizioni['file_dettaglio_wizard']} M3
               WHERE M3DT = '{$id_ditta_default}' {$where} AND M3CCON = ? 
               ";
    
    $stmt_m3 = db2_prepare($conn, $sql_m3);
    echo db2_stmt_errormsg();
    
    
    $ar = array();
    $ar_tot = array();
    
    while ($r = db2_fetch_assoc($stmt)){
       
        $result = db2_execute($stmt_m3, array($r['C_FOR']));
        $r_3 = db2_fetch_assoc($stmt_m3);
        if (count($m_params->form_values->f_stato_fornitore) == 0 || in_array('P', $m_params->form_values->f_stato_fornitore)) {
            $r['ARTICOLI'] = $r['ARTICOLI'] + $r_3['ARTICOLI'];
        }
        $ar[] = $r;
        
    }
    echo acs_je($ar);
    exit;
}


if ($_REQUEST['fn'] == 'open_elenco'){
    
    $m_params = acs_m_params_json_decode();
    
    ?>
{"success":true, "items": [
	{
		xtype: 'form',
		bodyStyle: 'padding: 10px',
		bodyPadding: '5 5 0',
		frame: true,
		title: '',
	
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		 
	
		items: [
		{
			xtype: 'grid',
			itemId: 'tabSelect',
			loadMask: true,
			flex: 1,
			features: [
			{
				ftype: 'filters',
				encode: false,
				local: true,
				filters: [
				{
					type: 'boolean',
					dataIndex: 'visible'
				}
				]
			}],
			selModel: {selType: 'checkboxmodel'},
			store: {
				xtype: 'store',
				autoLoad:true,
				proxy: {
					url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_forn',
					method: 'POST',
					type: 'ajax',
						
					//Add these two properties
					actionMethods: {
						read: 'POST'
					},
						
					reader: {
						type: 'json',
						method: 'POST',
						root: 'root'
					}
					, doRequest: personalizza_extraParams_to_jsonData
					, extraParams: <?php echo acs_je($m_params); ?>
								},
									
								fields: ['C_FOR', 'D_FOR', 'ARTICOLI']
											
											
							}, //store
							multiSelect: false,
							
						
							columns: [
								{header: 'Fornitore', 	dataIndex: 'D_FOR', flex: 1,  filter: {type: 'string'}, filterable: true},
								{header: 'Articoli', 	dataIndex: 'ARTICOLI', flex: 1},
							]
							 
							,listeners: {
							
							  afterrender: function(comp) {
			        			win = comp.up('window');
			        			win.tools.close.hide();
			        			var myCloseTool = Ext.create('Ext.panel.Tool', {
			            			type: 'close',
			            			handler: function() {
			               			win.hide();
			              
			            		}
			        			})
			       			 win.header.insert(3, myCloseTool); // number depends on other items in header
			    				
			   				 }
							 	
							 	}
							 	
						 	,viewConfig :
                                {
                               // enableTextSelection: true
                                
                                }
						}	
							 
							 
					],
				buttons: [{
		            text: 'Conferma',
		            iconCls: 'icon-windows-32',
		            scale: 'large',	            
		            handler: function() {
		            
		            var win_id = this.up('window').getId();
	
					var form_p = this.up('form');
					var tab_selected = form_p.down('#tabSelect');
					var loc_win = this.up('window');
		            
					row_selected = tab_selected.getSelectionModel().getSelection();   				
		   			for_id = row_selected[0].data.C_FOR;
		   			
					acs_show_panel_std('acs_sollecito_forniture.php?fn=open_tab', 'sollecito_forniture', {form_values: <?php echo acs_je($m_params->form_values); ?>, fornitore : for_id, win_id: win_id});
			        loc_win.hide(); 
					
		            } //handler
		        }
		        ],             
					
	        }
	]}
<?php	
	exit;

}
