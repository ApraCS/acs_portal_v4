<?php

require_once "../../config.inc.php";
require_once "acs_panel_protocollazione_include.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));
//$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);


?>


<html>
 <head>

  <style>

   #logocliente {
     padding-top: 80px; 
     background-image: url(<?php echo "'" . "http://" . $_SERVER['HTTP_HOST'] . ROOT_PATH .  'personal/logo_cliente.png' . "'"; ?>); 
     background-repeat: no-repeat; background-size: auto 70px;
    }

   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
 
   .cella_gray{background-color: #D1CCBF;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 12px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}  
   
  table.intest td{border: 0px;} 
 
    
   tr.ag_liv_data th{background-color: #333333; color: white;}
   .grassetto th{font-weight: bold;}
    .box{width: 250px;
   		 height: 150;
   		 padding:10px;
   		 border: 1px solid;
   		 
   	  } 
   
    div#my_content h1{font-size: 22px; padding: 10px;}
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
   @media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
 @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'N';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 


		

<?php


$oe = $s->k_ordine_td_decode_xx($_REQUEST['k_ordine']);
$row= $s->get_ordine_by_k_docu($_REQUEST['k_ordine']);
$commento= $s->get_commento_ordine_by_k_ordine($_REQUEST['k_ordine'], $cfg_mod_DeskPVen['bl_commenti_ordine']);


$doc_type = get_doc_type_by_tipo($row['TDOTPD']);
$modello = '';

$ar = array();

$sql_rd = "SELECT RDART, RDDART, RDRIGA, RDQTA, RDUM, RDTPIN, RDANIN, RDNRIN, RTINFI 
			, AL.ALDAR1 AS TRAD_LNG_1, AL.ALDAR2 AS TRAD_LNG_2
			, AL.ALDAR3 AS TRAD_LNG_3, AL.ALDAR4 AS TRAD_LNG_4
			FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
				LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
					ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
				LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art_trad']} AL
					ON AL.ALDT=RD.RDDT AND AL.ALART=RD.RDART AND AL.ALLING='{$cfg_mod_DeskPVen['traduzione']}'
			WHERE RD.RDART NOT IN('ACCONTO', 'CAPARRA') AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
			AND RD.RDTISR = '' AND RD.RDSRIG = 0
			ORDER BY RD.RDRIGA";

$stmt_rd = db2_prepare($conn, $sql_rd);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_rd, $oe);

 
$sql_c = "SELECT RT.RTPRZ AS CAPARRA
			FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
			INNER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
			ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
			WHERE RD.RDART = 'CAPARRA' AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
			AND RD.RDTISR = '' AND RD.RDSRIG = 0";

$stmt_c = db2_prepare($conn, $sql_c);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_c, $oe);

$row_c = db2_fetch_assoc($stmt_c);

$sql_a = "SELECT RT.RTPRZ AS ACCONTO
			FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
			INNER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
				ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
			WHERE RD.RDART = 'ACCONTO' AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
				AND RD.RDTISR = '' AND RD.RDSRIG = 0";

$stmt_a = db2_prepare($conn, $sql_a);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_a, $oe);


$row_a = db2_fetch_assoc($stmt_a);


$sql_imp = "SELECT TOIMPO1, TOVTRA, TOCTRA
			FROM {$cfg_mod_DeskPVen['file_testate_gest_valuta']}
			
			WHERE TODT = ? AND TOTIDO = ? AND TOINUM = ? AND TOAADO = ? AND TONRDO = ? AND TOVALU='EUR'";


$stmt_imp = db2_prepare($conn, $sql_imp);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_imp, $oe);

$row_imp = db2_fetch_assoc($stmt_imp);

$des_pag_ar_cap = $s->get_TA_std('TPCAP', trim($row['TDCARP']));
$des_pag_cap = trim($des_pag_ar_cap['TADESC']);
$flag_g = trim($des_pag_ar_cap['TAFG01']);

if($flag_g == 'G'){
	$tot_finale = $row_imp['TOIMPO1'] + $row_a['ACCONTO'];
}else{
	$tot_finale = $row_imp['TOIMPO1'] + $row_a['ACCONTO'] + $row_c['CAPARRA'];
}

$ret_imp = array(
		"imp_totale" 	=> $tot_finale,
		"saldo"	        => $row_imp['TOIMPO1'],
		"imp_trasp"	    => $row_imp['TOVTRA'],
		"imp_trasp_2"	=> $row_imp['TOCTRA']
		
);



/*if(trim(risposta_PVNOR($row['TDDT'], $_REQUEST['k_ordine'], '06'))=='EM')
	$ret_imp["imp_trasp"] = $row_imp['TOCTRA'];*/



$sql_cli = "SELECT *
		FROM {$cfg_mod_Spedizioni['file_anag_cli']} {$join_riservatezza}
		WHERE CFCD= '{$row['TDCCON']}'
 		AND CFDT = '{$row['TDDT']}' AND CFTICF = 'C' AND CFFLG3 = ''
 	    ORDER BY CFRGS1, CFRGS2";


$stmt_cli = db2_prepare($conn, $sql_cli);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_cli);

$row_cli = db2_fetch_assoc($stmt_cli);

// CFNAZ

$ret_cli = array(
		"descr" 	=> acs_u8e(trim($row_cli['CFRGS1']) . " " . trim($row_cli['CFRGS2'])),
		"cod_cli" 	=> acs_u8e(trim($row_cli['CFCD'])),
		"out_loc"	=> acs_u8e(trim($row_cli['CFLOC1']) . " " . trim($row_cli['CFPROV'])),
		"out_ind"	=> acs_u8e(trim($row_cli['CFIND1']) . " " . trim($row_cli['CFIND2'])),
		"cap"		=> acs_u8e(trim($row_cli['CFCAP'])),
		"tel"		=> acs_u8e(trim($row_cli['CFTEL'])),
		"tel2"		=> acs_u8e(trim($row_cli['CFTEX'])),
		"p_iva"		=> acs_u8e(trim($row_cli['CFPIVA'])),
		"cod_fisc"  => acs_u8e(trim($row_cli['CFCDFI'])),
);


$des_pag_ar = get_TA_sys_by_code('CUCP', trim($row['TDCPAG']));
$des_pag = trim($des_pag_ar['TADESC']);



if (trim($des_pag_ar['TADES2']) != '')
	$des_pag .= "<br/>" . trim($des_pag_ar['TADES2']);

$ret = array(
		"descr" 	=> acs_u8e(trim($row['TDDDES'])),
		"out_loc"	=> acs_u8e(trim($row['TDDLOC']) . " " . trim($row['TDPROD'])),
		"out_ind"	=> acs_u8e(trim($row['TDIDES'])),
        "cap"	    => acs_u8e(trim($row['TDDCAP'])),
		"data_contratto"		=> acs_u8e(trim($row['TDODRE'])),
		"data_consegna"		    => acs_u8e(trim($row['TDDTEP'])),
		"data_consegna_con"		=> acs_u8e(trim($row['TDODER'])),
		"numero"		        => (int)trim($row['TDONDO']),
		"pagamento"             => acs_u8e($des_pag),
		"imponibile"		    => acs_u8e(trim($row['TDTIMP'])),
		"IVA"		            => acs_u8e(trim($row['TDTIVA'])),
		"email"                 => acs_u8e(trim($row['TDMAIL'])),
		"tot_imp"               => acs_u8e(trim($row['TDTOTD'])),
		"prior"                 => acs_u8e(trim($row['TDDPRI'])),
		"refe"                  => acs_u8e(trim($row['TDDORE'])),
		"rif"                   => acs_u8e(trim($row['TDVSRF'])),
		"tipo_ordine"           => acs_u8e(trim($row['TDOTPD'])),
		"tipologia_ordine"      => acs_u8e(trim($row['TDCLOR'])),
		"tipol_amb"             => trim($row['TDCVN1']),
		"stab"                  => trim($row['TDSTAB']),
        "divisione"             => trim($row['TDCDIV']),
		"tipo_pag_cap"          => acs_u8e($des_pag_cap)
		//"tipol_amb"             => decode_PUVN_code('MOD', trim($row['TDCVN1']))

);

        

function decod_risposta($domanda, $risposta){
	global $s;	
	$ar=$s->get_TA_std('PRVAN', $domanda, $risposta);
	return $ar['TADESC'];	
}


/**************** CREAZIONE AR RIGHE RIGHE   *********************************************************************************/
$c_righe = 0;
while($row_rd = db2_fetch_assoc($stmt_rd)){
		$c_righe++;
	
		$ar_new = array();
		$ar_new['cod_articolo']  = trim($row_rd['RDART']);
		
		$is_order_prec = 'N';
		//verifica se ordine precedente a modifica su gestione descrizioni
		if ($doc_type == 'P' && ($oe['TDOADO'] < 2017 || ($oe['TDOADO'] == 2017 && $oe['TDONDO']<='000058')))
			$is_order_prec = 'Y';
		if ($doc_type == 'O' && ($oe['TDOADO'] < 2017 || ($oe['TDOADO'] == 2017 && $oe['TDONDO']<='000331')))
			$is_order_prec = 'Y';
		
		if($is_order_prec == 'Y' && trim($row_rd['TRAD_LNG_1']) != ''){
			$ar_new['articolo'] = trim($row_rd['TRAD_LNG_1']);
			
			//dalla trad del primo articolo recupero il modello (se presente)
			if ($c_righe==1)
				$modello = $row_rd['TRAD_LNG_1'];
			
			//accodiamo altre parti della descrizione
			if(trim($row_rd['TRAD_LNG_2']) != '')
				$ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_2']);
			if(trim($row_rd['TRAD_LNG_3']) != '')
				$ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_3']);
			if(trim($row_rd['TRAD_LNG_4']) != '')
				$ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_4']);
			
		} else{
			$ar_new['articolo'] = trim($row_rd['RDDART']);
		}
		$ar_new['ord_ven']  = $row_rd['RDTPIN']."_".$row_rd['RDANIN']."_".sprintf("%06s", $row_rd['RDNRIN']);
		$ar_new['quant']  = $row_rd['RDQTA'];
		$ar_new['un_mis'] = trim($row_rd['RDUM']);
		$ar_new['prezzo'] = $row_rd['RTINFI'];
	
	$ar[] = $ar_new;
}

$stampa_info = 'Y';
$stampa='N';
 include "acs_report_include.php"; 


echo "<br>";
echo "<br>";
echo "<table class='grassetto'>";
echo "<tr>";
echo "<tr><td colspan=4 class='cella_gray' style='text-align:center;'> <i> Riepilogo merce</i> </td></tr>";
echo "<th>Descrizione beni</th>";
echo "<th>UM</th>";
echo "<th>Quantit&agrave;</th>";

if(trim($ret['tipologia_ordine']) !='A' && $_REQUEST['tipo_stampa'] != 'SCHEDA') 
	echo "<th>Importo</th>";
echo"</tr>";


foreach ($ar as $k => $v){
	    echo "<tr>";

		if (trim($v['cod_articolo']) == '*'){
			echo "<td valign=top colspan=4>".$v['articolo']."</td>";
		} else {
		    echo "<td valign=top><b>".$v['articolo']." [".$v['cod_articolo']."]</b>";
		    if(trim($ret['tipo_ordine']) == 'OT')
		        echo "<br>".$v['ord_ven'];  
            echo "</td>";
			echo "<td valign=top style='width:30px;'>".$v['un_mis']."</td>";
			echo "<td valign=top style='text-align:right; width:70px;'>".n($v['quant'],2)."</td>";
			if(trim($ret['tipologia_ordine']) !='A' && $_REQUEST['tipo_stampa'] != 'SCHEDA')
			 	echo "<td valign=top style='text-align:right;'>".n($v['prezzo'],2)."</td>";
		}	
		echo "</tr>";
} 
echo "</table>";

echo "<br/><br/>";


//*********************************************************************************
if ($_REQUEST['tipo_stampa'] != 'SCHEDA'){
	
	if(trim($ret['tipologia_ordine']) =='A'){
		echo "<p> Pagamento: ".$ret['pagamento']."</p>";
	}else{
	
	echo "<table class='totale'>";
	echo "<tr><td style = 'width: 60%; border: 0px;'>Pagamento: ".$ret['pagamento']."</td><th class='cella_gray'><b>Importo totale IVA inclusa</b></th><td class='cella_gray' style='text-align:right;'><b>".n($ret_imp['imp_totale'], 2)."</b></td></tr>";
	
	
	
	if ($doc_type != 'P'){
		if ((float)$row_c['CAPARRA'] <> 0)
			echo "<tr><td style = 'border: 0px;'>&nbsp;</td><th>Caparra (".$ret['tipo_pag_cap'].")</th><td style='text-align:right;'>".n($row_c['CAPARRA'],2)."</td></tr>";
		if ((float)$row_a['ACCONTO'] <> 0)
			echo "<tr><td style = 'border: 0px;'>&nbsp;</td><th>Acconto</th><td style='text-align:right;'>".n($row_a['ACCONTO'],2)."</td></tr>";
		if ((float)$row_c['CAPARRA'] <> 0 || (float)$row_a['ACCONTO'] <> 0)
			echo "<tr><td style = 'border: 0px;'>&nbsp;</td><th><b>Saldo </b></th><td style='text-align:right;'>".n($ret_imp['saldo'],2)."</td></tr>";
		
	}	
	
	echo "</table>";
	
	echo "<br>";
	echo "<br>";
	
	echo "<table class='totale'>";
	
	if($ret_imp['imp_trasp'] > 0){
	echo "<tr><td style = 'width: 60%; border: 0px;'>&nbsp;</td><th><b>Importo trasporto </b></th><td style='text-align:right;'>".n($ret_imp['imp_trasp'],2)."</td></tr>";
	}
	
	
	echo "<tr>";
	echo "<td colspan= 3 style = 'border: 0px;'>&nbsp;</td>";
	echo "</tr>";
	echo "<tr><td style = 'width: 60%; border: 0px;'>&nbsp;</td><th th class='cella_gray'><b>";
	echo $doc_type=='P' ? 'Totale proposta': 'Totale vs. dare<br/></b>Il saldo va effettuato entro 4gg prima della consegna';
	echo "</b></th>";
	
	if(trim(risposta_PVNOR($row['TDDT'], $_REQUEST['k_ordine'], '06'))=='EM')	
		echo "<td class='cella_gray' style='text-align:right;'><b>".n($ret_imp['saldo'],2)."</b></td></tr>";	
	else 
		echo "<td class='cella_gray' style='text-align:right;'><b>".n($ret['tot_imp'],2)."</b></td></tr>";
	
	
	if($ret_imp['imp_trasp_2'] > 0){
		echo "<tr>";
		echo "<td colspan= 3 style = 'border: 0px;'>&nbsp;</td>";
		echo "</tr>";
		echo "<tr><td style = 'width: 60%; border: 0px;'>&nbsp;</td><th><b>Importo trasporto da pagare a montatore</b></th><td style='text-align:right;'>".n($ret_imp['imp_trasp_2'],2)."</td></tr>";
	}
		
		
	
	echo "</table>";
	
	}
	
	echo "<br>";
	echo "<br>";
	echo "<p>&nbsp;Termini di consegna ";
	echo $doc_type=='P' ? 'prospettati': 'concordati';
	echo ":</p>";
	if(trim($ret['tipo_ordine']) != 'MO'){
		echo "<p>&nbsp;".$ret['prior']." ".print_date($ret['data_consegna'])."</p>";
	}else{
		echo "<p>&nbsp;consegna della merce entro 30/40 gg lavorativi dal momento in cui viene effettuato il saldo del 30% dell'importo totale.</p>";
	}
	echo "<br>";
	echo "<br>";
	

} //not SCHEDA

	echo "<table>";
	echo "<tr>";
	echo "<th><b> Annotazioni</b></th>";
	echo "<tr><td>";
	foreach ($commento as $c => $t){
		
		echo $t['text']."<br>";
		
	}
	echo "</td></tr>";
	echo "</table>";
	echo "<br>";
	
	//*********************************************************************************
	if ($_REQUEST['tipo_stampa'] != 'SCHEDA'){
	?>
	
	<?php if ($doc_type != 'P' && trim($ret['tipologia_ordine']) != 'A'){ ?>
	<table width="100%" class="intest">
	 <tr>
	   <td width="33%" align=center>
	    PARTE VENDITRICE
	    <br/>&nbsp;<br/>
	    _________________________________
	   </td>
	
	   <td width="33%">&nbsp;</td>
	
	   <td width="33%" align=center>
	    L'ACQUIRENTE
	    <br/>&nbsp;<br/>
	    _________________________________
	   </td>   
	 </tr>
	</table>
	
	<br>
	<br>
	
	<div class=page-break></div>
	<p style = 'font-size: 8px;'>
	L'Acquirente, presa visione delle Condizioni Generali di Vendita apposte sul retro del presente contratto, ai sensi e per gli effetti degli articoli 1341 e 1342 del Codice Civile, approva specificatamente le clausole qui di seguito richiamate: 1. Disposizioni generali; 2. Conclusione del contratto; 3. Caparra confirmatoria, acconto e saldo; 4. Variazioni e modifiche degli ordini; 5. Caratteristiche tecniche e campioni illustrativi; 6. Conformitą dei prodotti e conformitą e variazioni rispetto ai campioni; 7. Misure, rilievi e sue variazioni; 8. Consegna dei beni; 9. Servizio di trasporto e montaggio; 10. Prezzo e sue variazioni; 11. Modalitą di pagamento; 12. Penale; 13. Difetti di conformitą e garanzia; 14. Diritto di recesso; 15. Tutela dei dati personali; 16. Foro competente; 17. Disposizioni finali e specifica approvazione delle clausole.
    </p>
	<table width="100%" class="intest">
	 <tr>
	   <td width="22%" align=center>
	     <br><br>
	     Luogo__________, data___/___/________
	    
	   </td>
	
	   <td width="10%">&nbsp;</td>
	
	   <td width="22%" align=center>
	    L'ACQUIRENTE
	    <br/>&nbsp;<br/>
	    _________________________________
	   </td>   
	 </tr>
	</table>
	 <br>
	<?php if (trim($ret['tipo_ordine']) !='MA' || trim($ret['tipo_ordine']) !='ME' ){ 
	   $divisione = $ret['divisione']; 
	   $data = oggi_AS_date();
    ?>
	
	
	
		<p style='text-align:center'> <b>Contratto numero <span style= 'font-weight: bold; font-size: 14px;'><?php echo $ret['numero'] ?></span> del <?php echo print_date($ret['data_contratto']); ?></b></p>
		<p style="text-align: center">
		    <div style='
		    	height: 850px;
		    	width: 700px; 
		    	margin-right: auto; margin-left: auto;
   				background-image: url(<?php echo '"' . "http://" . $_SERVER['HTTP_HOST'] . ROOT_PATH .  "personal/condizioni_generali_{$divisione}.png?data={$data}" . '"'; ?>);   		
		   		background-repeat: no-repeat;
		   		background-size: 700px 850px;'>
 			</div>
		</p>
	
	<?php 
	}?>
	
	<?php $stampa='Y';
	$stampa_info = 'Y';
	if (trim($ret['tipo_ordine']) =='MO' && trim($ret['tipol_amb']) != '30'){ ?>
	
	<div class=page-break></div>	
		<?php include "acs_report_include.php";?>
	<br>
	<br>	
	<div style='width:100%; height:40%; border:1px solid black;'></div>	
	<br>
	<p><b>NOTE: </b>  <span style='color:gray;'>___________________________________________________________________________________________________________  </span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________ </span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________</span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________</span></p>
	
	<?php }

} //if tipo_stampa != 'SCHEDA
//***************************************************


?>	

</div>

<?php } ?>
 </body>
</html>		