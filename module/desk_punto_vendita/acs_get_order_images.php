<?php

require_once "../../config.inc.php";
$mod_provenienza = new DeskPVen();
$cod_mod_provenienza = $mod_provenienza->get_cod_mod();
///////require_once("../desk_vend/acs_get_order_images.php");

$m_params = acs_m_params_json_decode();


if (isset($_REQUEST['function']) && $_REQUEST['function'] == 'view_image'){
	//mi viene passato il path del file... lo apro e ne restituisco il contenuto
	$handle = fopen(utf8_decode($_REQUEST['IDOggetto']), "rb");
	$cont_file = fread($handle, filesize($_REQUEST['IDOggetto']));
	
	$path_info = pathinfo($_REQUEST['IDOggetto']);
	$tipo_estensione = $path_info['extension'];
	
	switch(strtolower($tipo_estensione)){
		case 'pdf':
			header('Content-type: application/pdf');
			echo $cont_file;
			break;
		case 'tif':
		case 'tiff':
			header('Content-type: image/tiff');
			echo $cont_file;
			break;
		case 'png':
			header('Content-type: image/png');
			echo $cont_file;
			break;
		case 'jpg':
			header('Content-type: image/jpeg');
			echo $cont_file;
			break;
		default:
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
			header("Content-disposition: attachment; filename=\"" . preg_replace('/^.+[\\\\\\/]/', '', $path_info['basename']) . "\"");
			echo $cont_file;
			break;
	}
	
	exit;
}






		$s = new Spedizioni(array('no_verify' => 'Y'));
		$ord = $s->get_ordine_by_k_docu($_REQUEST['k_ordine']);




		$n_docu = $_REQUEST['ord'];
		$n_docu_exp = explode("_", $n_docu);
		
		/* CONFIG */
		//$img_dir = "/SV2/ORDINI/";
		$img_dir = $cfg_mod_DeskPVen['allegati_root_C'];

		if(isset($_REQUEST['k_ordine']))
		  $dir_ordine = $img_dir . $ord['TDCCON'];
		else 
		  $dir_ordine = $img_dir . $m_params->cliente;
		    
		$dir_ordine = "{$dir_ordine}/*.*";

		$ret = array();
		foreach (glob($dir_ordine) as $filename)
		{
			$nome_file_ar = explode('/', $filename);
			$nome_file = end($nome_file_ar);
			$r = array();
			$r['DataCreazione'] = date ("d/m/Y", filemtime($filename));
			$r['IDOggetto']		 = acs_u8e($filename);
			$r['des_oggetto']	 = basename($filename);
			//$r['des_oggetto']	 = acs_u8e($nome_file);
			
			$r['tipo_scheda'] = get_tipo_scheda(basename($filename));
			$ret[] = $r;
		}

		echo acs_je($ret);




		function get_tipo_scheda($name){
			global $n_ord;
			if (strstr( $name , "_MSGIN_" )) return "1. Messaggi ricevuti";
			if (strstr( $name , "_MSGOUT_" )) return "2. Messaggi inviati";

			if (substr($name, 0, 6) != $n_ord) return "3. Altro";
			return "4. Allegati grafici";
		}


/*
		function print_date($data, $format="%d/%m/%y"){
			if (strlen(trim($data))==0 || $data=="0") return "";
			$oldLocale = setlocale(LC_TIME, 'it_IT');
			$dd = strftime($format, strtotime($data));
			setlocale(LC_TIME, $oldLocale);
			return $dd;
		}
*/
