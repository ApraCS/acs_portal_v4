<?php
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_rimuovi'){
    
    $m_params = acs_m_params_json_decode();
    $rrn= $m_params->row->rrn;
    
    $sql = "DELETE FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA WHERE RRN(RA) = '{$rrn}'";
      
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);

    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}


if ($_REQUEST['fn'] == 'view_image'){
    
    $ord = $s->get_ordine_by_k_docu($_REQUEST['k_ordine']);
    
    //$path = "/SV2/ORDINI/". $ord['TDCCON'] ."/".$_REQUEST['nome'];
    $path = $cfg_mod_DeskPVen['allegati_root_C']. $ord['TDCCON'] ."/".$_REQUEST['nome'];
    
    // /SV2/ORDINI/900000001/IBM I Supply Desk.pdf
    // /SV2/ORDINI/900000001/IBM I Supply Desk.pdf 
    /*print_r($path);
    exit;*/
    
    //mi viene passato il path del file... lo apro e ne restituisco il contenuto
    $handle = fopen($path, "rb");
    $cont_file = fread($handle, filesize($path));
    $path_info = pathinfo($path);
    
    
    $tipo_estensione = $path_info['extension'];
    
    switch(strtolower($tipo_estensione)){
        case 'pdf':
            header('Content-type: application/pdf');
            echo $cont_file;
            break;
        case 'tif':
        case 'tiff':
            header('Content-type: image/tiff');
            echo $cont_file;
            break;
        case 'png':
            header('Content-type: image/png');
            echo $cont_file;
            break;
        case 'jpg':
            header('Content-type: image/jpeg');
            echo $cont_file;
            break;
        default:
            header("Content-Description: File Transfer");
            header("Content-Type: application/octet-stream");
            header("Content-disposition: attachment; filename=\"" . preg_replace('/^.+[\\\\\\/]/', '', $path_info['basename']) . "\"");
            echo $cont_file;
            break;
    }
    
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $nr = array();
    $ar = array();
    
    $m_params = acs_m_params_json_decode();
    $oe = $s->k_ordine_td_decode_xx($m_params->open_request->k_ordine);
    
    $sql = "SELECT RRN(RA) AS RRN, RAALOR
    FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA
    WHERE RANREC = '{$m_params->open_request->row->num_ord}' AND RADT = ? AND RATIDO = ? AND RAINUM = ? AND RAAADO = ? AND RANRDO = ? ";
     
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $oe);
    
    while($row = db2_fetch_assoc($stmt)){
       
       
        $nr['nome'] = trim($row['RAALOR']);
        $nr['rrn']  = trim($row['RRN']);
     
        
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>

{"success":true, "items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
				items: [
						{
						xtype: 'grid',
						title: '',
						flex:0.7,
				        loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['nome', 'rrn']							
									
			}, //store
				
				<?php $cf1 = "<img src=" . img_path("icone/48x48/camera.png") . " height=18>"; ?>

			      columns: [	
			      {
	                header   : 'Allegato',
	                dataIndex: 'nome',
	                flex: 1
	                }
	                
	         ], listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					  	 voci_menu.push({
			         		text: 'Rimuovi abbinamento',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_rimuovi',
								        timeout: 2400000,
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        			
										},							        
								        success : function(result, request){
					            			grid.getStore().load();	
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
			    		});	 
			    	
			    		
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	         
	           , celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);
							  allegatiPopup('<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_image&nome=' + rec.get('nome') + '&k_ordine=' + <?php echo j($m_params->k_ordine) ?>);
							 
							  							  
							 
						  }
			   		  }
				  
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           /*if (record.get('stato') == 'C')
					           		return 'colora_riga_grigio';*/
					           return '';																
					         }   
					    }
					       
		
		
		}
					 ],
					 
					
					
	}
	
]}


<?php 
}
