<?php

require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data_fornitori'){
    
    global $cfg_mod_Spedizioni;
  
    $sql = "SELECT DISTINCT RDFORN, RDDFOR
    FROM {$cfg_mod_Spedizioni['file_righe']}
    WHERE UPPER(RDDFOR) LIKE '%" . strtoupper($_REQUEST['query']) . "%'
    AND RDDT = '{$id_ditta_default}' ORDER BY RDDFOR";
    
    //print_r($sql);
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    
    while ($row = db2_fetch_assoc($stmt)){
        
        $ret[] = array("id"=>trim($row['RDFORN']), "text" =>trim($row['RDDFOR']));
    }
    
    echo acs_je($ret);
    exit();
}

if ($_REQUEST['fn'] == 'get_json_data_articoli'){
    
    $ar = array();
    $m_params = acs_m_params_json_decode();
 
    
    
    if(strlen($m_params->fornitore)>0){
        $where_art .= "AND ARFOR1 = '{$m_params->fornitore}'";
    }else{
        $where_art .= " ";
    }
     
        
        $sql = "SELECT ARART, ARDART
        FROM {$cfg_mod_DeskPVen['file_anag_art']} AR
        WHERE ARDT = '$id_ditta_default' {$where_art}
        ORDER BY ARART";
        
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        $ret = array();
        while ($row = db2_fetch_assoc($stmt)) {
            
            $text = "[" . $row['ARART'] . "] " . $row['ARDART'];
            
            $ret[] = array( "id" 	=> $row['ARART'],
                "text" 	=> acs_u8e($text) );
        }
        
        
    
    
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'get_parametri_form'){
	
	?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            
            items: [{
						flex: 1,
			            xtype: 'combo',
						name: 'f_fornitore',
						fieldLabel: 'Fornitore',
						anchor: '-15',
						margin: "10 20 10 10",	
						minChars: 2,			
            
			            store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fornitori',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}],            	
			            },
                         listeners: {
                    	change: function(field,newVal) {	
                      		if (!Ext.isEmpty(newVal)){
                            	combo_art = this.up('form').down('#cod_art');                      		 
                             	combo_art.store.proxy.extraParams.fornitore = newVal;
                            	combo_art.store.load();                             
                             }
                             

                            }
                        },
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            anchor: '100%',

            
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {text}' + 
			                    '</div>';
			                	}                
		                
		            	},
            
            			pageSize: 1000,
        		},	{
									name: 'f_articolo',
									xtype: 'combo',
									itemId: 'cod_art',
									fieldLabel: 'Articolo',
									forceSelection: true,								
									displayField: 'text',
									valueField: 'id',								
									emptyText: '- seleziona -',
									forceSelection: true,
							   		anchor: '-15',
							   		margin: "10 10 10 10",	
							   		flex: 1,
								    store: {
							                autoLoad: true,
											proxy: {
									            type: 'ajax',
									            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_articoli',
									            actionMethods: {
            							          read: 'POST'
            							        },
							                	extraParams: {
							    		    		fornitore: '',
							    				},				            
									            doRequest: personalizza_extraParams_to_jsonData, 
									            reader: {
                						            type: 'json',
                									method: 'POST',						            
                						            root: 'root'						            
                						        }
									        },       
											fields: ['id', 'text'],		             	
							            }
								
					 }       
            ],
            
			buttons: [					
				{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	             handler: function() {
	                form = this.up('form').getForm();
	                form_values = form.getValues();
	                articolo = form_values.f_articolo;                
		               if (form.isValid()){	                	                
			                 acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: '<?php echo $id_ditta_default; ?>', rdart: articolo}, 1200, 600, null, 'icon-shopping_cart_gray-16');
			               this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        
	        }
	        ]            
            
           
}
	
]}


<?php

exit;
}
