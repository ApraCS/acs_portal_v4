<?php

require_once "../../config.inc.php";
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

$tp_tab = $m_params->open_parameters->tp_tab;



if ($_REQUEST['fn'] == 'exe_canc'){
    
    $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
    WHERE RRN(TA) = '{$m_params->form_values->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ar_ins = array();
    
    $ar_ins['TAUSGE'] 	= $auth->get_user();
    $ar_ins['TADTGE']   = oggi_AS_date();
    $ar_ins['TAORGE']   = oggi_AS_time();

    
    $ar_ins['TADT'] 	= $id_ditta_default;
    $ar_ins['TATAID'] 	= 'ARCHI';
    $ar_ins['TAKEY1'] 	= $m_params->form_values->TAKEY1;
    $ar_ins['TADESC'] 	= $m_params->form_values->TADESC;
    $ar_ins['TAINDI'] 	= $m_params->form_values->TAINDI;
    $ar_ins['TACAP'] 	= $m_params->form_values->TACAP;
    $ar_ins['TALOCA'] 	= $m_params->form_values->TALOCA;
    $ar_ins['TAPROV'] 	= $m_params->form_values->TAPROV;
    $ar_ins['TANAZI'] 	= $m_params->form_values->TANAZI;
    $ar_ins['TATELE'] 	= $m_params->form_values->TATELE;
    $ar_ins['TAMAIL'] 	= $m_params->form_values->TAMAIL;
     
        
    $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
        
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_modifica'){
    
    $ar_upd = array();
    
    $ar_upd['TAKEY1'] 	= $m_params->form_values->TAKEY1;
    $ar_upd['TADESC'] 	= $m_params->form_values->TADESC;
    $ar_upd['TAINDI'] 	= $m_params->form_values->TAINDI;
    $ar_upd['TACAP'] 	= $m_params->form_values->TACAP;
    $ar_upd['TALOCA'] 	= $m_params->form_values->TALOCA;
    $ar_upd['TAPROV'] 	= $m_params->form_values->TAPROV;
    $ar_upd['TANAZI'] 	= $m_params->form_values->TANAZI;
    $ar_upd['TATELE'] 	= $m_params->form_values->TATELE;
    $ar_upd['TAMAIL'] 	= $m_params->form_values->TAMAIL;
    
   
    
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} TA
    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
    WHERE RRN(TA) = '{$m_params->form_values->rrn}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}



if ($_REQUEST['fn'] == 'get_data_grid'){
    
       
        $sql = "SELECT RRN (TA) AS RRN, TADT, TATAID, TADESC, TAKEY1,
                TAINDI, TACAP, TALOCA, TAPROV, TANAZI, TATELE, TAMAIL
                FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
                WHERE TADT='{$id_ditta_default}' AND TATAID = '{$m_params->tataid}' ";
        
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        
        $data = array();
        
        while ($row = db2_fetch_assoc($stmt)) {
            
            $row['TATAID'] = acs_u8e(trim($row['TATAID']));
            $row['TAKEY1'] = acs_u8e(trim($row['TAKEY1']));
            $row['TADESC'] = acs_u8e(trim($row['TADESC']));
            $row['TAINDI'] = acs_u8e(trim($row['TAINDI']));
            $row['TACAP'] = acs_u8e(trim($row['TACAP']));
            $row['TALOCA'] = acs_u8e(trim($row['TALOCA']));
            $row['TAPROV'] = acs_u8e(trim($row['TAPROV']));
            $row['TANAZI'] = acs_u8e(trim($row['TANAZI']));
            $row['TATELE'] = acs_u8e(trim($row['TATELE']));
            $row['TAMAIL'] = acs_u8e(trim($row['TAMAIL']));
            $row['rrn'] = acs_u8e(trim($row['RRN']));
            
            $data[] = $row;
        }
        
        echo acs_je($data);
        
        exit();
        
}


if ($_REQUEST['fn'] == 'open_grid'){
    
    ?>

				
{"success":true, "items": [

   {
			xtype: 'panel',
			title: 'Lista Architetti',
        	<?php echo make_tab_closable(); ?>,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				
	{
        
        xtype: 'grid',
        flex : 0.5,
	    features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}]
			,store: {
						
						xtype: 'store',
						autoLoad:true,
					   	proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },							        
							        
							           extraParams: {
										 tataid : '<?php echo $m_params->tataid; ?>',
										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			 'rrn', 'TATAID', 'TAKEY1', 'TADESC', 'TAINDI', 'TACAP', 'TALOCA', 'TAPROV', 'TANAZI', 'TATELE', 'TAMAIL' 
		        			]
		    			},
		    		
			        columns: [
			       
			             { 
			                header   : 'Codice',
			                dataIndex: 'TAKEY1', 
			                flex    : 1,
			                
			           		 filter: {type: 'string'}, filterable: true
			             },{ 
			                header   : 'Descrizione',
			                dataIndex: 'TADESC', 
			                flex    : 1,
			                
			           		 filter: {type: 'string'}, filterable: true
			             }
			             
			            
			         ],  
					
						listeners: {
						
					      	 selectionchange: function(selModel, selected) { 
	               
            	               if(selected.length > 0){
            		               var form_dx = this.up('panel').down('#dx_form');
            		             
            		               form_dx.getForm().reset();
            	                   form_dx.getForm().setValues(selected[0].data);
	                 			}
		       		  	 
		       		  	 },itemclick: function(view,rec,item,index,eventObj) {
	
			     					  this.up('panel').down('#save').enable();							        	
			        	 },
					
					
					}
       
        }  
							
					       
		
		
		, {
 		            xtype: 'form',
 		            itemId: 'dx_form',
 		            autoScroll : true,
 		            title: 'Architetto',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            flex:0.5,
 		            frame: true,
 		            items: [ {
        					name: 'rrn',
        					fieldLabel : 'rrn',
        					xtype: 'textfield',
        					hidden : true						
        				   }, {
        					name: 'TATAID',
        					fieldLabel : 'TATAID',
        					xtype: 'textfield',
        					hidden : true						
        				   }, {
        					name: 'TAKEY1',
        					fieldLabel : 'Codice',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },  {
        					name: 'TADESC',
        					fieldLabel : 'Descrizione',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },{
        					name: 'TAINDI',
        					fieldLabel : 'Indirizzo',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },{
        					name: 'TACAP',
        					fieldLabel : 'CAP',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },{
        					name: 'TALOCA',
        					fieldLabel : 'Localit&agrave;',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },{
        					name: 'TAPROV',
        					fieldLabel : 'Provincia',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },{
        					name: 'TANAZI',
        					fieldLabel : 'Nazione',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },{
        					name: 'TATELE',
        					fieldLabel : 'Telefono',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   },{
        					name: 'TAMAIL',
        					fieldLabel : 'email',
        					xtype: 'textfield',
        					anchor: '-15'							
        				   }
			 		 
			 		 ],
			 		           
			 		           
			 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
               
			     
			     '->',
			     {
                     xtype: 'button',
                    text: 'Salva',
                    itemId : 'save',
                    disabled: true,
		            scale: 'small',	                     
					iconCls: 'icon-save',
		           	handler: function() {
		               		var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('form').up('panel').down('grid'); 
 			       			
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.getStore().load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
			
			            }

			     },  
			      {
                     xtype: 'button',
                    text: 'Crea',
		            scale: 'small',	                     
					iconCls: 'icon-user-add',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        grid.getStore().load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           
			
			            }

			     }, {
                     xtype: 'button',
                    text: 'Reset',
		            scale: 'small',	                     
					iconCls: 'icon-reset',
		            handler: function() {
		            	 this.up('form').getForm().reset();
	       			     }

			     }, {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-delete',
		            handler: function() {
		            
		              var form = this.up('form');
		              var form_values = form.getValues();
	       			  var grid = this.up('form').up('panel').down('grid');
        		
        				Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
							        method     : 'POST',
				        			jsonData: {
										form_values: form_values
									},							        
							        success : function(response, opts){
							        	form.getForm().reset();
							       		grid.getStore().load();
							       		
								 
				            		},
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				        }

			     }
			      
			     ]
		   }]
			 	 }
		 ],
					 
					
					
	}
]
}

<?php 

exit; 

}



