<?php
	
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors','On');
	
	//forzo anche senza sessione
	require_once('../../auth.php');
	session_start();
	$auth = Auth::get_session_auth();
	if ($auth->is_authenticate('Y') == 0) {
		$auth->set_authenticate('FIRMA');
	}
	
	require_once("../../config.inc.php");	
	$_module_descr = "Firma documenti";
			
	
	function check_decod_numero_carico($input, $check_disabled = 'N'){
		//2016003133ABCDE		
		
		if ($check_disabled != 'Y')
			if (strlen($input) != 14) return false;
		
		$ar = str_split($input, 2);
		$n = (int)$ar[4]*398 + (int)$ar[2]*147 + (int)$ar[3]*386 + (int)$ar[0]*421 + (int)$ar[1]*297;
		
		$n = $n % 10000;
		$n = sprintf("%04s", $n);
		
		$ar_cd = str_split($n, 2);
		
		if ($check_disabled != 'Y')
			if ((int)$ar_cd[0] != (int)$ar[5] || (int)$ar_cd[1] != (int)$ar[6]) return false;
				
		return substr($input, 0, 10);
	}
	
	
	
	//TODO: DRY (in report personalizzati)
	function exe_call_UR21H8($sped_id, $carico_in, $tddocu = '', $tipo_report = '', $cliente = '', $dest = ''){
		global $id_ditta_default, $useToolkit, $conn;
		global $libreria_predefinita, $libreria_predefinita_EXE;
		global $tkObj;
	
		$carico_exp 	= explode("_", $carico_in);
		$k_carico_out 	= implode("", array(trim($carico_exp[1]), $carico_exp[0], sprintf("%06s", $carico_exp[2])));
	
		//costruzione del parametro
		$cl_p = str_repeat(" ", 246);
		$cl_p .= sprintf("%-2s", $id_ditta_default);
		$cl_p .= sprintf("%09s", $sped_id);
		$cl_p .= sprintf("%12s", $k_carico_out);
		$cl_p .= sprintf("%-1s", "N");
		$cl_p .= sprintf("%-1s", " ");
	
		$cl_p .= sprintf("%-2s", $id_ditta_default);
	
		//user (10)
		global $auth;
		$cl_p .= sprintf("%-10s", trim($auth->get_user()));
	
		//tipo report (30)
		$cl_p .= sprintf("%-30s", $tipo_report);
	
		//tddocu (50)
		$cl_p .= sprintf("%-50s", trim($tddocu));
	
		//cliente/destinazione
		$cl_p .= sprintf("%-9s", trim($cliente));
		$cl_p .= sprintf("%-9s", trim($dest));
		
		//pcprog (output)
		$cl_p .= '00000000';
		
		//tipo spedizione
		$cl_p .= 'F';        //per TDNBOF (Flight)                                                                                                                                                                                                                                         1 VO   0000000000070013241   000000000  2016083100000000000000005 DSTD Y    N    N    N    N    N    N    N    N    N    N    N    N    N    N    N    N    N    N    N    00000000';

	
		if ($useToolkit == 'N'){
			//per test in Apra
			$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21H8C('{$cl_p}')";
			$stmt1   = 	db2_prepare($conn, $qry1);
			$result1 = 	db2_execute($stmt1);
			$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
			//FINE test per Apra
		} else {
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
	
			$cl_in 	= array();
			$cl_out = array();			
			
			$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
			$call_return = $tkObj->PgmCall('UR21H8', $libreria_predefinita_EXE, $cl_in, null, null);
			
			return substr($call_return['io_param']['LK-AREA'], 381, 8);
		}
	
	
	}	
	
	
	
	
	
	
	class DeskFirma {
	
		private $mod_cod = "DESK_FIRMA";
		private $mod_dir = "desk_firma";
	
	
		function __construct($parameters = array()) {
			return true; //TODO!!!!!!
			global $auth;
			$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
	
			if (is_null($mod_permitted) || !$mod_permitted){
	
				//se mi ha passato un secondo modulo testo i permessi
				if (isset($parameters['abilita_su_modulo'])){
					$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
					if (is_null($mod_permitted) || !$mod_permitted){
						die("\nNon hai i permessi!!");
					}
				} else
					die("\nNon hai i permessi!!!");
			}
		}
		
		public function get_cod_mod(){
			return $this->mod_cod;
		}

		
		public function fascetta_function(){
			global $auth, $main_module;
			return '';			
		}		
		
		
	}	
	
	
	
	$main_module = new DeskFirma();
	
	
	// ******************************************************************************************
	// open_doc TRAMITE INSERT SQL
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'open_doc'){
		
/*		
		echo acs_je(array('success' => true));
		exit;
*/		
		
		
		
		$m_params = acs_m_params_json_decode();
	
		//decofidico numero di carico (comprensivo di carico)
		$f_carico = $m_params->form_values->f_chiave;
		$search_carico = check_decod_numero_carico($f_carico, $m_params->form_values->check_disabled);
		if ($search_carico == false) {
			$ret = array();
			$ret['success'] = false;
			$ret['message'] = 'Barcode non corretto';
			echo acs_je($ret); exit;
		}
		$k_carico_in    = implode("_", array(substr($search_carico, 0, 4), 'CA', substr($search_carico, 4, 6)));
		$k_carico_out 	= implode("-", array('CA', substr($search_carico, 0, 4), sprintf("%06s", substr($search_carico, 4, 6))));
		
		//eseguo 		
		$ret_pcprog = exe_call_UR21H8(0, $k_carico_in, '', 'GENERA_VDC_CARICO.dsm');
		
		$ret_pcprog = (int)$ret_pcprog;
		
		if ($ret_pcprog == 0) {
			$ret = array();
			$ret['success'] = false;
			$ret['message'] = 'Errore nel ritorno di PCPROG';
			echo acs_je($ret); exit;			
		}

/*		
		ini_set("mssql.textlimit" , "2147483647");
		ini_set("mssql.textsize" , "2147483647");
		ini_set("odbc.defaultlrl", "100K");
		
		$link = mssql_connect('192.168.1.238', 'SV2', 'CUCINESTOSA');
*/

		//mssql
		$host 	= $mssql_stampe['host'];
		$dbname = $mssql_stampe['dbname'];
		$usr 	= $mssql_stampe['usr'];
		$psw  	= $mssql_stampe['psw'];
		$db = new PDO("dblib:host={$host};dbname={$dbname};charset=utf8",$usr,$psw);		
		
		$sql_ins = "INSERT INTO [DesmosSTOSA].[dbo].[COMANDI_UTENTE]
           ([Utente],[NomeBatch],[Stato],[DataOraInserimento],[Lancio],[Filtro1],[ValFiltro1])
			(SELECT 'SRVDESMOSETK1','GENERA_VDC_CARICO.dsm','0',getdate(),'{$ret_pcprog}','Desmos_Printer_Path','C:\desmos\stosa\Utenti\\" . $m_params->form_values->f_user . "\')
		";
 
		//$ret = mssql_query($sql_ins);
		$stmt=$db->prepare($sql_ins);
		$ret = $stmt->execute();
	
		$ret = array();
		$ret['success'] = true;
		echo acs_je($ret);
		exit;
	}
	
	
	
	
	// ******************************************************************************************
	// open_doc WEB SERVICE ADIUTO
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'open_doc_WS_ADIUTO'){
		$m_params = acs_m_params_json_decode();		
		
		//decofidico numero di carico (comprensivo di carico)
		$f_carico = $m_params->form_values->f_chiave;
		$search_carico = check_decod_numero_carico($f_carico);
		if ($search_carico == false) {
			$ret = array();
			$ret['success'] = false;
			$ret['message'] = 'Barcode non corretto';
			echo acs_je($ret); exit;
		}		
		
		$client = new SoapClient("http://srvdocu:8080//adiJed/services/SDKService?wsdl");
		
		$params = array(
				"username" => "webservices",
				"password" => "Apra!206!Adiut0"
		);
		$response = $client->__soapCall("remoteLogin", array($params));		
		$token = $response->loginResult;				
		
		$params = array(
				"session-id"	=> $token,
				"family" 		=> "Verbale di carico",
				"fields"		=> array("Numero di Carico"),
				"values"		=> array($search_carico)
		);

		
		$response = $client->executeQueryWithRight($params);
		
		//print_r($response);
		
		$xml = simplexml_load_string($response->results);
		$json = acs_je($xml);
		$array = json_decode($json, TRUE);
		
		$n_rec = $array['@attributes']['tot_ele'];
		
		if ($n_rec == 0) {
			$ret = array();
			$ret['success'] = false;
			$ret['message'] = 'Documento non trovato';
			echo acs_je($ret); exit;
		}
		
		if ($n_rec > 1) {
			$ret = array();
			$ret['success'] = false;
			$ret['message'] = 'Piu\' documenti trovati';
			echo acs_je($ret); exit;
		}
				
		
		$id_doc 		= $array['record']['dato'][0];	
		$nome_file 		= $array['record']['dato'][7];		
		
		$params = array(
				"idunivoco"	=> $id_doc				
		);
		$response = $client->getContent($params);				
		
		$tmpfname = "/SV2/ADIUTO/{$m_params->form_values->f_user}/{$nome_file}";
		$fp = fopen($tmpfname, 'w');
		fwrite($fp, $response->content);
		fclose($fp);
		
		$ret = array();
		$ret['success'] = true;
		$ret['file_path'] = $tmpfname;
		echo acs_je($ret);
		exit;
	}
	
	
			
?>



<html>
<head>
<title>ACS Portal_Gest</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">
.icon-folder_search-64 {background-image: url("../../images/icone/64x64/folder_search.png") !important;}


.x-btn-default-large-icon-text-left .x-btn-icon {width: 64px;}
.x-ie6 .x-btn-default-large-icon-text-left .x-btn-icon, .x-quirks .x-btn-default-large-icon-text-left .x-btn-icon {height: 64px}

/* NASCONDO ICONE */
#main-site-home, #main-site-search, #main-site-tools, #main-site-help {	visibility: hidden; }

</style>



<script type="text/javascript" src="../../../extjs/ext-all.js"></script>
<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>

<script src=<?php echo acs_url("js/acs_js.js") ?>></script>



<script type="text/javascript">

	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {       
        }	    
	}); Ext.Loader.setPath('Ext.ux', '../../ux');


    Ext.require(['*'
                 , 'Ext.ux.grid.FiltersFeature'                 
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
    			 , 'Ext.ux.grid.Printer'
      			 , 'Ext.ux.RowExpander' 
                 ]);



    Ext.onReady(function() {

        Ext.QuickTips.init();

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            layout: 'border',
            items: [ 
                    
	            Ext.create('Ext.Component', {
	                region: 'north',
	                id: 'page-header',
	                height: 110, // give north and south regions a height
	                contentEl: 'header'
	            }),
            
	            Ext.create('Ext.tab.Panel', {
	            	id: 'm-panel',
	        		cls: 'supply_desk_main_panel',            	            	
	                region: 'center', // a center region is ALWAYS required for border layout
	                deferredRender: false,
	                activeTab: 1,     // first tab initially active
	                items: [

						{
						    xtype: 'form',
						    bodyStyle: 'padding: 30px',
						    bodyPadding: '30 30 30 30',
						    margin: "10 10 10 10",
						    border: true,
						    frame: false,
						    title: 'Firma',
						    flex: 1,
						    layout: {
						    	type: 'vbox',
						    	align: 'center',
						    	pack: 'center'
						    },

						    start_submit: function() {
			                    form = this.getForm();
								  if (form.isValid()) {


										wait_win = new Ext.Window({
											  width: 400
											, height: 500
											, plain: true
											, title: 'Elaborazione verbale di carico'
											, layout: 'vbox'
											, border: true
											, closable: false
											, modal: true
											, items: [
											   {				
												xtype:'image', height: 400,
												src: <?php echo img_path("elaborazione.gif") ?>
											   }, {
												   flex: 1, width: '100%', align: 'center',
												   html: '<center>Avvio procedura<br/><br/><b>Attendere prego</b></center>',												   
											   }
											]
										});	
										wait_win.show();

									f_values = form.getValues();
			                    	//form.findField('f_chiave').setValue('');
			                    	//form.findField('f_chiave').focus();									
			                    	
									  
							        Ext.Ajax.request({
							        	url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_doc',
							        	jsonData   : {form_values: f_values},
							            method: 'POST',
							            success: function ( result, request) {
							                jsonData = Ext.decode(result.responseText);
							                if (jsonData.success == true) {
							                    //acs_show_msg_info('Attendere elaborazione Verbale di carico');
							                    
							                    wait_win.down('panel').update('<center>Richiesta inviata a gestionale<br/><br/><b>Attendere prego</b></center>');

							                    //chiudo dopo 30 secondi
							                    setTimeout(function(){	
													//window.close();
								                    wait_win.close();
								                    window.location.reload();								                    
								                }, 20*1000);
							                    
							                } else {
								                wait_win.close();
												acs_show_msg_error('Errore: ' + jsonData.message);
												setTimeout(function(){								                    							                    	
								                    window.location.reload();								                    
								                }, 5*1000);												
												
							                }	
							                    
							            },
							            failure: function ( result, request) {
								            wait_win.close();
							            	acs_show_msg_error('Errore: ' + jsonData.message);
											setTimeout(function(){								                    							                    	
							                    window.location.reload();								                    
							                }, 5*1000);							            	
							            }
							        });
							        
		        				} //if isValid()
			        		},


						    items: [
								{
									name: 'f_user',
									xtype: 'hidden',
									value: <?php echo j($_REQUEST['user'])?>,
								},


								{
									xtype: 'fieldcontainer',
									layout: {
									    type: 'hbox',
									    align: 'stretch'
									},
									items: [

										{
											name: 'f_chiave',
											xtype: 'textfield',
											hasfocus:true,
											fieldLabel: 'Barcode', labelAlign: 'top',
											bodyPadding: '30 30 30 30',
											bodyStyle: 'padding: 30px',
										    maxLength: 80,
										    fieldStyle: 'font-size: 60px; text-transform: uppercase;',
										    height: 100, width: 600,
										    enableKeyEvents: true,
										    listeners: {                   
								                'keyup': function(field,event){
								                    //if (event.getKey() == event.ENTER && 1==2){
								                    if (field.getValue().length == 14){
								                        //this.up('form').start_submit();
								                        //Ext.getCmp('firma_button').focus();								                    							                    	
									                    Ext.getCmp('firma_button').fireHandler();								                    
								                    }
								                 }, 
									             'afterrender': function(field) {
									                    field.focus();
									                }
								            }						
										}
										
										
										, {
											xtype: 'button',
											id: 'firma_button',
											iconCls: 'icon-folder_search-64',
											scale: 'large', width: 150,
											autoWidth : true,
											autoHeight : true,
											bodyPadding: '30 30 30 30',
											bodyStyle: 'padding: 30px',
											margin: "23 10 2 10",
											text: 'Start',
											handler: function() {
										    	var form = this.up('form');
												form.start_submit();										    	
											}
											
										}
											
									]
								}



								
								
							]


						}
				                                   
	                             					                                                
	                ]
	            })

            ]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------
      	
      	
        //--------------------------------------------------------------------------------        
  
  		//schedulo la richiesta dell'ora ultimo aggiornamento
   		/////Ext.TaskManager.start(refresh_stato_aggiornamento);	
     		

        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
