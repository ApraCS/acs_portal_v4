<?php 
	require_once("../../config.inc.php");
	$_module_descr = "Desktop Ordini di Acquisto";
			
	$main_module = new DeskAcq();
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}
	
	$ar_email_json = acs_je($ar_email_to);	
		
?>



<html>
<head>
<title>ACS Portal_DOrA</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />

<!-- <link rel="stylesheet" type="text/css" href="resources/css/calendar.css" /> -->
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/extensible-all.css" />
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/calendar.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">

 /* EVIDENZIAZIONE RIGA SELEZIONATA */
 .x-grid-row-selected .x-grid-cell, .x-grid-row-selected .x-grid-rowwrap-div {
    background-color: yellow !important;
}
 
 

 /* REGOLE RIORDINO */
 .x-grid-cell.lu{background-color: #dadada;} 
 .x-grid-cell.ma{background-color: #e5e5e5;} 
 .x-grid-cell.me{background-color: #eeeeee;} 
 .x-grid-cell.gi{background-color: #FFFFFF;} 
 .x-grid-cell.ve{background-color: #F4EDAF;} 
 .x-grid-cell.sa{background-color: #FFFFFF;}
 
 .x-grid-cell.attivi{background-color: #A0DB8E;}
 .x-grid-cell.elaborazione-old-1{background-color: #e5e5e5;} 
 .x-grid-cell.elaborazione-old-2{background-color: #F9BFC1;} 

.x-panel td.x-grid-cell.festivo{opacity: 0.6;}

.x-panel td.x-grid-cell.con_carico_1{background-color: #909090; color: white; font-weight: bold;} /* SOLO ALCUNI HANNO IL CARICO */
.x-panel td.x-grid-cell.con_carico_2{background-color: #3c3c3c; color: white; font-weight: bold;}  /* HANNO TUTTI IL CARICO */
.x-panel td.x-grid-cell.con_carico_3xxx{background-color: #F9827F; font-weight: bold;}  /* NON CARICO. CON DATE CONF. */
.x-panel td.x-grid-cell.con_carico_4xxx{background-color: #F9BFC1; font-weight: bold;}  /* NON CARICO. CON DATE TASS. */
.x-panel td.x-grid-cell.con_carico_5xxx{background-color: #CEEA82; font-weight: bold;}  /* ALCUNI EVASI */
.x-panel td.x-grid-cell.con_carico_6{background-color: #8CD600; font-weight: bold;}  /* TUTTI EVASI */
.x-grid-row.liv_2 .x-grid-cell.disponibile{background-color: #A0DB8E;}

.x-panel .x-column-header-text{font-weight: bold;}


tr.liv_totale td.x-grid-cell{background-color: #F4EDAF; border: 1px solid white;}
tr.liv_totale td.x-grid-cell.festivo{opacity: 0.6;}
tr.liv_0 td.x-grid-cell{border: 1px solid white; font-weight: bold;}
tr.liv_1 td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: bold;}
tr.liv_1_no_b td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: normal;}
tr.liv_1.no_carico td.x-grid-cell{background-color: #C4D8E2}

.x-panel.elenco_ordini tr.liv_1 td.x-grid-cell{font-weight: normal;}
.x-panel.elenco_ordini tr.liv_2 td.x-grid-cell{font-weight: bold;}

tr.liv_totale.dett_selezionato td.x-grid-cell{background-color: #F4ED7C; font-weight: bold;}

/*TIPO ORDINE (raggr)*/
.x-grid-cell.tipoOrd.O {background-color: #93C6E0;} /* mostre */
.x-grid-cell.tipoOrd.S {background-color: #D3BFB7;} /* materiale promozionale */
.x-grid-cell.tipoOrd.R {background-color: #E2D67C;} /* composizioni promozionali */
.x-grid-cell.tipoOrd.L {background-color: white;}   /* assistenze */


/*RIGHE ORDINE*/
.x-grid-row.rigaRevocata .x-grid-cell{background-color: #F9BFC1} 
.x-grid-row.rigaChiusa .x-grid-cell{background-color: #CEEA82}
.x-grid-row.rigaParziale .x-grid-cell{background-color: #F4EDAF}
.x-grid-row.rigaNonSelezionata .x-grid-cell{background-color: #cccccc}
.x-grid-row .x-grid-cell.font-normal{font-weight: normal;}

/*TIPO PRIORITA*/
 .x-grid-cell.tpSfondoRosa{background-color: #F9BFC1;}
 .x-grid-cell.tpSfondoCelesteEl{background-color: #99D6DD;}


/*ICONE*/
.iconSpedizione {background-image: url(<?php echo img_path("icone/16x16/spedizione.png") ?>) !important;}
 
/* celle classe ABC */
.x-panel.x-table-index .x-panel-body {
		text-align: center;
    	background-color: #D3D3D3;
		font-size: 18;
		/*font-weight: bold;*/
}
 .x-panel.x-table-index.leg .x-panel-body {		
		font-size: 14;
}
.x-panel.x-table-index .x-panel-body span {font-size: 10px;}
 
/* bordi laterali tabella non visualizzabili */
.table-border {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	text-align: right;
}
.table-border-final {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	border-right: 1px solid #99BBE8;
	text-align: right;
}

a.view_dett_class_gc{text-decoration: none;}
 
/* GRADIAZIONE CELLE GIACENZE/CONSUMI */
.x-panel.GPACPA .x-panel-body-default{background-color: #6BC9DB;} 

.x-panel.GPACPB .x-panel-body-default{background-color: #99D6DD;}
.x-panel.GPBCPA .x-panel-body-default{background-color: #99D6DD;}
.x-panel.GPBCPB .x-panel-body-default{background-color: #99D6DD;}

.x-panel.GPCCPA .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPCCPB .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPCCPC .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPBCPC .x-panel-body-default{background-color: #BAE0E0;}
.x-panel.GPACPC .x-panel-body-default{background-color: #BAE0E0;}

.x-panel.GPZCPA .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPB .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPC .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPD .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPZCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPDCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPCCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPBCPZ .x-panel-body-default{background-color: #F4EDAF;}
.x-panel.GPACPZ .x-panel-body-default{background-color: #F4EDAF;}

.x-toolbar .strong{font-weight: bold;}

.x-grid-cell-inner p.sottoriga-liv_2{text-indent: 70px}

</style>

<script type="text/javascript" src="../../../extjs/ext-all.js"></script>

<script type="text/javascript" src="../../js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
<script type="text/javascript" src="../../js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>


<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src=<?php echo acs_url("js/gmaps.js") ?>></script>
<script src=<?php echo acs_url("js/acs_js.js") ?>></script>



<script type="text/javascript">



	var refresh_stato_aggiornamento = {
        run: doRefreshStatoAggiornamento,
        interval: 30 * 1000 //millisecondi
      }

	function doRefreshStatoAggiornamento() {
        Ext.Ajax.request({
            url : 'get_stato_aggiornamento.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btStatoAggiornamento = document.getElementById('bt-stato-aggiornamento-esito');
                btStatoAggiornamento.innerHTML = jsonData.html;
            },
            failure: function ( result, request) {
                console.log('failed');
            }
        });
	}


	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
            //'Ext.calendar': 'src',
	        "Extensible": "../../js/extensible-1.5.2/src", 
			"Extensible.example": "../../js/extensible-1.5.2/examples"	                   
        }	    
	});Ext.Loader.setPath('Ext.ux', 'ux');


    Ext.require(['*', 'Ext.ux.GMapPanel', 'Ext.ux.grid.FiltersFeature', 'Ext.selection.CheckboxModel'
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
                 , 'Extensible.calendar.data.MemoryEventStore'
                 , 'Extensible.calendar.CalendarPanel'  
                 , 'Extensible.example.calendar.data.Events'
				 , 'Extensible.calendar.data.CalendarMappings',
    			 , 'Extensible.calendar.data.EventMappings'                 
                 ]);

//    ,
//    'Ext.calendar.data.MemoryCalendarStore',
//    'Ext.calendar.data.MemoryEventStore',
//    'Ext.calendar.data.Events',
//    'Ext.calendar.data.Calendars'	  




    

    Ext.define('ModelOrdini', {
        extend: 'Ext.data.Model',
        fields: [
        			{name: 'task'}, {name: 'n_carico'}, {name: 'k_carico'}, {name: 'sped_id'}, {name: 'cod_iti'},
        			{name: 'k_ordine'}, {name: 'prog'}, {name: 'n_O'}, {name: 'n_M'}, {name: 'n_P'}, {name: 'n_CAV'},
        			{name: 'liv'},
					{name: 'cliente'}, {name: 'k_cli_des'}, {name: 'gmap_ind'}, {name: 'scarico_intermedio'},
					{name: 'nr'},
					{name: 'volume'},{name: 'pallet'},{name: 'peso'},
					{name: 'volume_disp'},{name: 'pallet_disp'},{name: 'peso_disp'},
					{name: 'colli'},{name: 'colli_disp'},{name: 'colli_sped'},{name: 'data_disp'}, {name: 'data_cons_cli'}, {name: 'data_conf_ord'}, {name: 'fl_data_disp'},
					{name: 'riferimento'},
					{name: 'tipo'},{name: 'raggr'},
					{name: 'cons_rich'},					
					{name: 'priorita'},{name: 'tp_pri'},
					{name: 'seq_carico'},					
					{name: 'stato'},
					{name: 'cons_conf'},{name: 'cons_prog'},{name: 'ind_modif'},
					{name: 'data_reg'},{name: 'data_scadenza'},					
					{name: 'importo'}, {name: 'referenze'}, {name: 'referenze_cons'},
					{name: 'fl_evaso'}, {name: 'fl_ref'}, {name: 'fl_bloc'}, {name: 'fl_art_manc'}, {name: 'fl_da_prog'}, {name: 'art_da_prog'}, {name: 'fl_new'}
        ]
    });


    Ext.define('RowCalendar', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task',     type: 'string'},
            {name: 'liv',      type: 'string'},            
            {name: 'flag1',    type: 'string'},            
            {name: 'user',     type: 'string'},
            {name: 'duration', type: 'string'},
            {name: 'done',     type: 'boolean'},
            {name: 'da_data',  type: 'float'}, {name: 'itin'}, {name: 'sped_id'}, {name: 'k_cliente'},            
            {name: 'd_1'}, {name: 'd_1_f'}, {name: 'd_1_t'}, {name: 'd_1_d'},
            {name: 'd_2'}, {name: 'd_2_f'}, {name: 'd_2_t'}, {name: 'd_2_d'},
            {name: 'd_3'}, {name: 'd_3_f'}, {name: 'd_3_t'}, {name: 'd_3_d'},
            {name: 'd_4'}, {name: 'd_4_f'}, {name: 'd_4_t'}, {name: 'd_4_d'},
            {name: 'd_5'}, {name: 'd_5_f'}, {name: 'd_5_t'}, {name: 'd_5_d'},
            {name: 'd_6'}, {name: 'd_6_f'}, {name: 'd_6_t'}, {name: 'd_6_d'},
            {name: 'd_7'}, {name: 'd_7_f'}, {name: 'd_7_t'}, {name: 'd_7_d'},
            {name: 'd_8'}, {name: 'd_8_f'}, {name: 'd_8_t'}, {name: 'd_8_d'},
            {name: 'd_9'}, {name: 'd_9_f'}, {name: 'd_9_t'}, {name: 'd_9_d'},
            {name: 'd_10'}, {name: 'd_10_f'}, {name: 'd_10_t'}, {name: 'd_10_d'},
            {name: 'd_11'}, {name: 'd_11_f'}, {name: 'd_11_t'}, {name: 'd_11_d'},
            {name: 'd_12'}, {name: 'd_12_f'}, {name: 'd_12_t'}, {name: 'd_12_d'},
            {name: 'd_13'}, {name: 'd_13_f'}, {name: 'd_13_t'}, {name: 'd_13_d'},
            {name: 'd_14'}, {name: 'd_14_f'}, {name: 'd_14_t'}, {name: 'd_14_d'},
			{name: 'fl_bloc'}, {name: 'fl_ord_bloc'}, {name: 'fl_art_mancanti'}, {name: 'fl_da_prog'}, {name: 'ha_contratti'}, {name: 'ha_proposte_MTO'}                                   
        ]
    });    

    //Model per albero itinerari
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task', type: 'string'}, {name: 'user', type: 'string'}, 'liv', 'sped_id', 'k_cliente', 'data'
        ]
    });    


    /*Ext.define('Teller.ext.CurrencyField', {	
    	extend: 'Ext.form.field.Number',	
    	alias: 'widget.currencyfield', 	
    	hideTrigger: true, 	
    		setValue: function (v) {		
    			this.callParent(arguments); 		
    			if (!Ext.isEmpty(this.getValue())) {			
    			this.setRawValue(Ext.util.Format.currency(this.getValue()));		
    		}}, 
    		removeFormat: function (v) {		
    			if (Ext.isEmpty(v)) {		
    			return '';		
    			} else {	
    			v = v.toString().replace(Ext.util.Format.currencySign, '').replace(Ext.util.Format.thousandSeparator, '');			

    			if (v % 1 === 0) {	// Return value formatted with no precision since there are no digits after the decimal			
    			return Ext.util.Format.number(v, '0');			
    			} else {	// Return value formatted with precision of two digits since there are digits after the decimal				
    			return Ext.util.Format.number(v, '0.00');			}	
    			}	
    		}, 	

    	 //Override parseValue to remove the currency format	
    		parseValue: function (v) {		
    			return this.callParent([this.removeFormat(v)]);	
    		}, 	

    	 //Remove the format before validating the value	
    		getErrors: function (v) {		
    			return this.callParent([this.removeFormat(v)]);	
    		}, 	
    // Override getSubmitData to remove the currency format on the value that will be passed out from the getValues method of the form 
    		getSubmitData: function () {	
    			var returnObject = {};	
    			returnObject[this.name] = this.removeFormat(this.callParent(arguments)[this.name]); 	
    			return returnObject;	
    			}, 	
    	 //Override preFocus to remove the format during edit	
    		preFocus: function () {		
    			this.setRawValue(this.removeFormat(this.getRawValue())); 		
    			this.callParent(arguments);	
    		}
    	 });
*/
    
	//TODO: da spostare in PLAN
	function getCellClass (val, meta, rec, rowIndex, colIndex, store) {
		idx = colIndex - 6; //colonna5 corrisponde a d_1
		 
		if (rec.get('d_' + idx + '_f') == 'F' || rec.get('d_' + idx + '_f') == 'S' || rec.get('d_' + idx + '_f') == 'D'){
			meta.tdCls += ' festivo';
		}
		if (rec.get('d_' + idx + '_t') == '1'){
			meta.tdCls += ' con_carico_1';
		}		
		if (rec.get('d_' + idx + '_t') == '2'){
			meta.tdCls += ' con_carico_2';
		}
		if (rec.get('d_' + idx + '_t') == '3'){
			meta.tdCls += ' con_carico_3';
		}		
		if (rec.get('d_' + idx + '_t') == '4'){
			meta.tdCls += ' con_carico_4';
		}		
		if (rec.get('d_' + idx + '_t') == '5'){
			meta.tdCls += ' con_carico_5';
		}
		if (rec.get('d_' + idx + '_t') == '6'){
			meta.tdCls += ' con_carico_6';
		}				
		
		if ((rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3') && rec.get('d_' + idx).length > 0 && rec.get('d_' + idx + '_d').length > 0)		
			return val += "<span class=c_disp>/" + rec.get('d_' + idx + '_d') + "</span>";
		else
			return val;	
	}

    
	function show_win_annotazioni_docu_for(k_ordine){	
		// create and show window
		print_w = new Ext.Window({
				  width: 800
				, height: 400
				, minWidth: 300
				, minHeight: 400
				, plain: false
				, title: 'Annotazioni documenti fornitore'
				, layout: 'fit'
				, border: true
				, closable: true
				, maximizable: false										
			});	
		print_w.show();

			//carico la form dal json ricevuto da php
			Ext.Ajax.request({
			        url        : 'acs_form_json_annotazioni_ordine.php?fn=open_bl&k_ordine=' + k_ordine,
			        method     : 'POST',
			        waitMsg    : 'Data loading',
			        success : function(result, request){
			            var jsonData = Ext.decode(result.responseText);
			            print_w.add(jsonData.items);
			            print_w.doLayout();				            
			        },
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });		
	} 
    

    //PLAN ARRIVI
    function show_el_arrivi(){
    	mp = Ext.getCmp('m-panel');
    	arrivi = Ext.getCmp('panel-arrivi');

    	if (arrivi){
        	arrivi.store.reload();
        	arrivi.show();		    	
    	} else {

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_arrivi_json_plan.php',
    		        method     : 'GET',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            mp.add(jsonData.items);
    		            mp.doLayout();
    		            mp.show();
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });

    	}
    }



    //PANEL fo_presca
    function show_fo_presca(){
       	   acs_show_win_std('Prenotazione scarichi', 'acs_panel_fo_presca.php?fn=get_open_form', null, 530, 420);	
    }
    
    function allegatiPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=600, height=600, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}
    


    function show_win_righe_ord(ord, k_ord){
 	   acs_show_win_std('Righe ordine', <?php echo acs_url('module/desk_acq/acs_get_order_rows.php') ?>, {k_ord: k_ord, dtep: ord.get('cons_prog')}, 1150, 480);        
    }


	function gmapPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=400, height=400, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}
    


    Ext.onReady(function() {

        Ext.QuickTips.init();






    Extensible.calendar.data.EventMappings = {
        // These are the same fields as defined in the standard EventRecord object but the
        // names and mappings have all been customized. Note that the name of each field
        // definition object (e.g., 'EventId') should NOT be changed for the default fields
        // as it is the key used to access the field data programmatically.
        EventId:     {name: 'EventId', mapping:'id', type:'int'}, // int by default
        CalendarId:  {name: 'CalendarId', mapping: 'cid', type: 'string'}, // int by default
        Title:       {name: 'Title', mapping: 'title'},
        StartDate:   {name: 'StartDate', mapping: 'start', type: 'date', dateFormat: 'c'},
        EndDate:     {name: 'EndDate', mapping: 'end', type: 'date', dateFormat: 'c'},
        RRule:       {name: 'RecurRule', mapping: 'rrule'},
        Location:    {name: 'Location', mapping: 'loc'},
        Notes:       {name: 'Notes', mapping: 'notes'},
        Url:         {name: 'Url', mapping: 'url'},
        IsAllDay:    {name: 'IsAllDay', mapping: 'ad', type: 'boolean'},
        Reminder:    {name: 'Reminder', mapping: 'rem'},        


        //Acs
        Risorsa: 	  {name:'Risorsa', mapping: 'risorsa', type: 'string'},
        Stabilimento: {name:'Stabilimento', mapping: 'stabilimento', type: 'string'},
        Trasp:		  {name:'Trasp', mapping: 'trasp', type: 'string'},
        Porta:    	  {name:'Porta', mapping: 'porta'},        
        
        
        // We can also add some new fields that do not exist in the standard EventRecord:
        CreatedBy:   {name: 'CreatedBy', mapping: 'created_by'},
        IsPrivate:   {name: 'Private', mapping:'private', type:'boolean'}
        
    };
    // Don't forget to reconfigure!
    Extensible.calendar.data.EventModel.reconfigure();






 /* PER DRAG & DROP **********/
    /*
     * Currently the calendar drop zone expects certain drag data to be present, so if dragging
     * from the grid, default the data as required for it to work
     */
    var nodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag1';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
        }
    };
    
    /*
     * Need to hook into the drop to provide a custom mapping between the existing grid
     * record being dropped and the new calendar record being added
     */
    var nodeDropInterceptor = function(n, dd, e, data){
        if(n && data){
            if(typeof(data.type) === 'undefined'){
                var rec = Ext.create('Extensible.calendar.data.EventModel'),
                    M = Extensible.calendar.data.EventMappings;
                
                // These are the fields passed from the grid's record
                //var gridData = data.selections[0].data;
                var gridData = data.records[0].data
                rec.data[M.Title.name] = 'salvataggio...';
                rec.data[M.CalendarId.name] = gridData.cid;
                
                // You need to provide whatever default date range logic might apply here:
                rec.data[M.StartDate.name] = n.date;
                rec.data[M.EndDate.name] = Extensible.Date.add(n.date, { hours: 1 });;
                rec.data[M.IsAllDay.name] = false;

                rec.data[M.CreatedBy.name] = gridData.TDNBOC;  //lo appoggio qui altrimenti non mi esegui il create               
                rec.data[M.Stabilimento.name] = gridData.TDSTAB;
                rec.data[M.Porta.name] = this.view.store.proxy.extraParams.f_porta;
                
                // save the evrnt and clean up the view
                ///////this.view.onEventDrop(rec, n.date);
                this.view.store.add(rec.data);                                                
                this.onCalendarDragComplete();
				this.view.fireEvent('eventadd');
                return false; //per non eseguire onDrop Normale
            }
        }
    };
    
    /*
     * Day/Week view require special logic when dragging over to create a 
     * drag shim sized to the event being dragged
     */
    var dayViewNodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag2';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
    
            var dayCol = e.getTarget('.ext-cal-day-col', 5, true);
            if (dayCol) {
                var box = {
                    height: Extensible.calendar.view.Day.prototype.hourHeight,
                    width: dayCol.getWidth(),
                    y: n.timeBox.y,
                    x: n.el.getLeft()
                }
                this.shim(n.date, box);
            }
        }
    };
    
    // Apply the interceptor functions to each class:
    var dropZoneProto = Extensible.calendar.dd.DropZone.prototype,
        dayDropZoneProto = Extensible.calendar.dd.DayDropZone.prototype 
    
    dropZoneProto.onNodeOver = Ext.Function.createInterceptor(dropZoneProto.onNodeOver, nodeOverInterceptor);
    dropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dropZoneProto.onNodeDrop, nodeDropInterceptor);
    
    dayDropZoneProto.onNodeOver = Ext.Function.createInterceptor(dayDropZoneProto.onNodeOver, dayViewNodeOverInterceptor);
    dayDropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dayDropZoneProto.onNodeDrop, nodeDropInterceptor);



    
    
    /*
     * This is a simple override required for dropping from outside the calendar. By default it
     * assumes that any drag originated within itelf, so a drag would be a move of an existing event.
     * This is no longer the case and it must check to see what the record state is.
     */
    Extensible.calendar.view.AbstractCalendar.override({
        onEventDrop : function(rec, dt){
        	console.log('index.php - onEventDrop override');
            if (rec.phantom) {
                this.onEventAdd(null, rec);
            }
            else {
                this.moveEvent(rec, dt);
            }
        }
    });   

    //COMBO: possibilita di rimuovere valore selezionato (con forceSelection lasciava sempre l'ultimo)
    Ext.form.field.ComboBox.override({
     beforeBlur: function(){
         var value = this.getRawValue();
         if(value == ''){
             this.lastSelection = [];
         }
         this.doQueryTask.cancel();
         this.assertValue();
     }
    }); 	


	//disabilito il menu sugli eventi
	Extensible.calendar.menu.Event.override({
		showForEvent: function(rec, el, xy){
				return false;
		}
	});
    

	//formato data negli header
	Extensible.calendar.template.BoxLayout.prototype.singleDayDateFormat = 'd / m';	

    
    //PER RIMUOVERE L'ORARIO NEGLI EVENTI
    Extensible.calendar.view.DayBody.override({
    getTemplateEventData : function(evt){
        var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            colorCls = 'x-cal-default',
            title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ',
            recurring = evt[M.RRule.name] != '';
        
        this.getTemplateEventBox(evt);
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, 
                    evt[M.CalendarId.name]);
                
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '') + (Ext.isIE || Ext.isOpera ? '-x' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
        data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        data.Title =  (!title || title.length == 0 ? this.defaultEventTitleText : title);
        return Ext.applyIf(data, evt);
    }    
    });
    
  











            

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            // create instance immediately
            Ext.create('Ext.Component', {
                region: 'north',
                id: 'page-header',
                height: 110, // give north and south regions a height
                contentEl: 'header'
            }), {
                // lazily created panel (xtype:'panel' is default)
                region: 'south',
                contentEl: 'south',
                split: true,
                height: 300,
                minSize: 100,
                maxSize: 500,
                collapsible: true,
                collapsed: true,
                title: 'Legenda',
                margins: '0 0 0 0'
            }, {
                xtype: 'tabpanel',
                region: 'east',
                title: 'Dettagli ordine',
                id: 'OrdPropertiesTab',                 
                animCollapse: true,
                collapsible: true,
                collapsed: true,
                split: true,
                width: 295, // give east and west regions a width
                minSize: 175,
                maxSize: 400,
                margins: '0 5 0 0',
                activeTab: 0,
                tabPosition: 'bottom',

        		imposta_title: function(titolo){
    				this.setTitle(titolo);
    			},
                
                
                items: [Ext.create('Ext.grid.PropertyGrid', {
                        title: 'Riferimenti',
                        id: 'OrdPropertyGrid',
                        closable: false,
                        source: {},
                        disableSelection: true
                    }),


                    new Ext.grid.GridPanel({
                        title: 'Cronologia',
                        id: 'OrdPropertyCronologia',
                  		 store: new Ext.data.Store({
                  									
                  				autoLoad: false,				        
                  	  			proxy: {
                  							url: 'acs_get_order_cronologia.php',
                  							type: 'ajax',
                  							reader: {
                  						      type: 'json',
                  						      root: 'root'
                  						     },
                  						     extraParams: {
                  		    		    		k_ordine: ''
                  		    				}               						        
   				        
                  						},
                  		        fields: ['data', 'utente', 'attivita', 'stato_data'],
                  		     	groupField: 'data',               		     	
                  			}),
       		    	features: new Ext.create('Ext.grid.feature.Grouping',{
   						groupHeaderTpl: 'Data: {[date_from_AS(values.name)]}',
   						hideGroupedHeader: false
   					}),               			
                     						    						
                        columns: [{
   			                header   : 'Utente',
   			                dataIndex: 'utente', 
   			                flex     : 3
   			             }, {
   				                header   : 'Attivit&agrave;',
   				                dataIndex: 'attivita', 
   				                width    : 70
   				         }, {
   			                header   : 'Stato / Evas. Prog.',
   			                dataIndex: 'stato_data', 
   			                flex     : 5
   			             }],

                        }),                      
                    



                    
                    new Ext.grid.GridPanel({
                     title: 'Allegati',
                     id: 'OrdPropertyGridImg',
               		 store: new Ext.data.Store({
               									
               				autoLoad: false,				        
               	  			proxy: {
               							url: 'acs_get_order_images.php',
               							type: 'ajax',
               							reader: {
               						      type: 'json',
               						      root: 'root'
               						     },
               						     extraParams: {
               		    		    		k_ordine: ''
               		    				}               						        
				        
               						},
               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
               		     	groupField: 'tipo_scheda',               		     	
               			}),
    		    	features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),               			
                  						    						
                     columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],

			         listeners: {
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  }
				         
			         }

                     })                      







                    ]
            }, {
                region: 'west',
                stateId: 'navigation-panel',
                id: 'west-panel',
                title: 'Indice consegne',
                split: true,
                width: 200,
                minWidth: 175,
                maxWidth: 400,
                collapsible: true,
                collapsed: true,
                animCollapse: true,
                margins: '0 0 0 5',
                layout: 'accordion'
            },
            
            
            Ext.create('Ext.tab.Panel', {
            	id: 'm-panel',
        		cls: 'supply_desk_main_panel',            	            	
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 1,     // first tab initially active
                items: [
			                                   
                             					                                                
                ]
            })]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------
      	
      	// ------------------- REFRESH ------------------------     
        if (Ext.get("bt-page-refresh") != null){
         	Ext.QuickTips.register({
    			target: "bt-page-refresh",
    			title: 'Refresh',
    			text: 'Chiusura/riapertura desktop con rigenerazione finestra dati'
    		});         
        
    	    Ext.get("bt-page-refresh").on('click', function(){
    	    	window.location.reload();
    		});
        }


      	// ------------------- CALL BATCH ------------------------        
        if (Ext.get("bt-call-pgm") != null) {
          	Ext.QuickTips.register({
   			target: "bt-call-pgm",
   			title: 'Start',
   			text: 'Avvio elaborazioni non interattive'
   		});    	 
            
           Ext.get("bt-call-pgm").on('click', function(){
        	   acs_show_win_std('Avvio processi batch', <?php echo acs_url('module/base/acs_form_json_call_pgm.php?mod=DORA') ?>);	
           });        

        }   


      	// ------------------- REGOLE RIORDINO FORNITORI ------------------------        
        if (Ext.get("bt-regole-riordino") != null) {
          	Ext.QuickTips.register({
   			target: "bt-regole-riordino",
   			title: 'ATP Setup',
   			text: 'Regole riordino forniture'
   		});    	 
            
           Ext.get("bt-regole-riordino").on('click', function(){
        	   acs_show_win_std('Regole riordino forniture', <?php echo acs_url('module/desk_acq/acs_regole_riordino.php') ?>, null, 600, 600, null, 'icon-shopping_setup-16');	
           });        
        }   
        
      

	     // ------------------- PLAN ARRIVI ------------------------
             if (Ext.get("bt-partenze") != null){
             	Ext.QuickTips.register({
         			target: "bt-partenze",
         			title: 'Plan',
         			text: 'Calendario consegne programmate/da programmare'
         		});            
     	        Ext.get("bt-partenze").on('click', function(){             
     	     		show_el_arrivi();
     	     	});	    
             }



    	     // ------------------- PRENOTAZIONE SCARICO FORNITORI (fo_presca) ---------------
                 if (Ext.get("bt-fo_presca") != null){
                 	Ext.QuickTips.register({
             			target: "bt-fo_presca",
             			title: 'Download',
             			text: 'Programma ubicazione/orario consegne fornitori'
             		});            
         	        Ext.get("bt-fo_presca").on('click', function(){             
         	     		show_fo_presca();
         	     	});	    
                 }
             
    	     
    	 // ------------------- GESTIONE TABELLE ------------------------             
             if (Ext.get("bt-toolbar") != null){
              	Ext.QuickTips.register({
         			target: "bt-toolbar",
        			title: 'Setup',
        			text: 'Manutenzione voci/parametri gestionali di base'
         		});
                     
                Ext.get("bt-toolbar").on('click', function(){
             	   acs_show_win_std('Gestione tabelle', <?php echo acs_url('module/desk_acq/acs_form_json_toolbar.php') ?>);	
                });
             }

             // ------------------- RICERCA ORDINI FORNITORE ------------------------             
             if (Ext.get("bt-ricerca") != null){
              	Ext.QuickTips.register({
         			target: "bt-ricerca",
        			title: 'Gestione ordini',
        			text: 'Avanzamento ordini fornitore'
         		});
                     
                Ext.get("bt-ricerca").on('click', function(){
             	   acs_show_win_std(null, <?php echo acs_url('module/desk_acq/acs_ordini_fornitore.php?fn=open_form') ?>);	
                });
             }
             
             //*********************
             // ARRIVI
             //*********************
             if (Ext.get("bt-arrivi") != null){
             	Ext.QuickTips.register({
         			target: "bt-arrivi",
         			title: 'To Do List',
         			text: 'Attivit&agrave; utente di acquisizione/avanzamento ordini'
         		});            
     	        Ext.get("bt-arrivi").on('click', function(){             
     	        	acs_show_win_std('Parametri interrogazione To Do list', 'acs_panel_todolist.php?fn=open_filtri', null , 500, 250, {}, 'icon-arrivi-16');    	            
     	     	});	    
             }



             //-----------------------------------------------   
             if (Ext.get("main-site-logout") != null){

             	Ext.QuickTips.register({
        			target: "main-site-logout",
        			title: 'Logout',
        			text: 'Chiude il desktop e scollega utente corrente'
        		});         
             }     
             


          <?php if ($js_parameters->p_solo_inter != 'Y'){ ?>     
             //-----------------------------------------------   
             if (Ext.get("main-site-tools") != null){

             	Ext.QuickTips.register({
        			target: "main-site-tool",
        			title: '(WIP)',
        			text: 'Modifica parametri elaborazione utente corrente'
        		});         
                 
                Ext.get("main-site-tools").on('click', function(){
                	
        			print_w = new Ext.Window({
        					  width: 600
        					, height: 330
        					, minWidth: 300
        					, minHeight: 300
        					, plain: true
        					, title: 'Strumenti di supervisione ambiente'
        					, layout: 'fit'
        					, border: true
        					, closable: true										
        				});	
        			print_w.show();

        				//carico la form dal json ricevuto da php
        				Ext.Ajax.request({
        				        url        : 'acs_history_log.php?fn=get_parametri_form',
        				        method     : 'GET',
        				        waitMsg    : 'Data loading',
        				        success : function(result, request){
        				            var jsonData = Ext.decode(result.responseText);
        				            print_w.add(jsonData.items);
        				            print_w.doLayout();				            
        				        },
        				        failure    : function(result, request){
        				            Ext.Msg.alert('Message', 'No data to be loaded');
        				        }
        				    });			
                });
        	}         
          <?php } ?>	
             






        	 

          <?php if ($js_parameters->p_disabilita_Info != 'Y'){ ?> 	     
        	 // ------------------- RICERCA ORDINI ------------------------   
             if (Ext.get("main-site-search") != null){

             	Ext.QuickTips.register({
        			target: "main-site-search",
        			title: 'Info',
        			text: 'Interrogazione ordini'
        		});         
                 
                Ext.get("main-site-search").on('click', function(){
              	   acs_show_win_std('Parametri interrogazione ordini', <?php echo acs_url('module/desk_acq/acs_form_json_main_search.php') ?>, null, 600, 500);
                });
        	}
         <?php } ?>	


        	 // ------------------- BACKGROUND MRP ----------------------
             if (Ext.get("bt-call-background") != null){
             	Ext.QuickTips.register({
         			target: "bt-call-background",
         			title: 'Background MRP',
         			text: 'Segnalazioni riordino materiali'
         		});            
     	        Ext.get("bt-call-background").on('click', function(){
             	   acs_show_win_std('Segnalazioni riordino materiali', <?php echo acs_url('module/desk_acq/acs_panel_background_mrp.php?fn=get_json_form') ?>, null, 600, 600, null, 'icon-warning_black-16');
     	     	});	    
             }       
        	


        	 // ------------------- GIFT ------------------------
        	 // Riordino materiali promozionali   
             if (Ext.get("bt-gift") != null){

             	Ext.QuickTips.register({
        			target: "bt-gift",
        			title: 'Marketing',
        			text: 'Riordino materiali promozionali'
        		});         
                 
                Ext.get("bt-gift").on('click', function(){
              	   acs_show_panel_std('acs_panel_riordino_materiali_promo.php?fn=get_json_grid', 'panel-riordino_materiali_promo');              	   
                });
        	}
        	 

        	 // ------------------- WIZARD MTS ------------------------
        	 // Riordino materiali promozionali   
             if (Ext.get("bt-wizard_MTS") != null){

             	Ext.QuickTips.register({
        			target: "bt-wizard_MTS",
        			title: 'Wizard MTS',
        			text: 'Wizard riordino articoli MTS'
        		});         
                 
                Ext.get("bt-wizard_MTS").on('click', function(){
             	   acs_show_win_std('Wizard riordino articoli MTS', <?php echo acs_url('module/desk_acq/acs_wizard_MTS.php?fn=open_form') ?>, null, 600, 600, null, 'icon-shopping_basket_2-16');	
                });
        	}


        	 // ------------------- PLAN DISPONIBILITA' ARTICOLI ------------------------   
             if (Ext.get("bt-plan-dispo") != null){

             	Ext.QuickTips.register({
        			target: "bt-plan-dispo",
        			title: 'Analisi disponibilit&agrave;',
        			text: 'Planning analisi disponibilit&agrave; articoli'
        		});         
                 
                Ext.get("bt-plan-dispo").on('click', function(){
             	   acs_show_win_std(null, <?php echo acs_url('module/desk_acq/acs_panel_plan_dispo_art.php?fn=open_form') ?>);	
                });
        	}
        	 
        	 // ------------------- REPORT ------------------------   
             if (Ext.get("bt-print") != null){

             	Ext.QuickTips.register({
        			target: "bt-print",
        			title: 'Report',
        			text: 'Emissione report'
        		});         
                 
                Ext.get("bt-print").on('click', function(){
              	   acs_show_win_std('Report settimanali', <?php echo acs_url('module/desk_acq/acs_get_print_form.php') ?>, null, 600, 600);
                });
        	}             


             // ------------------- GRAFICI / STATISTICHE ------------------------
             if (Ext.get("bt-grafici") != null){
             	Ext.QuickTips.register({
         			target: "bt-grafici",
         			title: 'Analysis',
         			text: 'Grafici e report direzionali'
         		});
                         
     	        Ext.get("bt-grafici").on('click', function(){
     				acs_show_win_std('Grafici e report direzionali', 'acs_json_menu_statistiche.php', null, 900, 250, null, 'icon-grafici-16')	        	
     	        });
             }   

             // ------------------- FATTURE ENTRATE ------------------------
        	 // Riordino materiali promozionali   
             if (Ext.get("bt-fatture_entrata") != null){

             	Ext.QuickTips.register({
        			target: "bt-fatture_entrata",
        			title: 'Documenti fornitore',
        			text: 'Controllo fatturazione documenti fornitore'
        		});         
                 
                Ext.get("bt-fatture_entrata").on('click', function(){
             	   acs_show_win_std('Documenti fornitore', <?php echo acs_url('module/desk_acq/acs_fatture_entrata.php?fn=open_form') ?>, null, 800, 350, null, 'icon-listino');	
                });
        	}         
        	
	 
			//tooltip stato aggiornamento
             if (Ext.get("bt-stato-aggiornamento") != null){
              	Ext.QuickTips.register({
         			target: "bt-stato-aggiornamento",
         			title: 'Synchro',
         			text: <?php echo j($main_module->get_orari_aggiornamento()); ?>
         		});
             }


    	     
		//---------------------------------------------------------------
		//carico gli indici
		//---------------------------------------------------------------
    		mp_indici = Ext.getCmp('west-panel');
    				
     		Ext.Ajax.request({
		        url        : 'acs_arrivi_json_indici.php',
		        method     : 'GET',
		        waitMsg    : 'Data loading',
		        success : function(result, request){
		            var jsonData = Ext.decode(result.responseText);
		            mp_indici.add(jsonData.items);
		            mp_indici.doLayout();
		        },
		        failure    : function(result, request){
		            Ext.Msg.alert('Message', 'No data to be loaded');
		        }
		    });     		
            	

  		//schedulo la richiesta dell'ora ultimo aggiornamento
   		Ext.TaskManager.start(refresh_stato_aggiornamento);	
     		

        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
