<?php
require_once "../../config.inc.php";
require_once "acs_fatture_entrata_include.php";


$m_DeskAcq = new DeskAcq();

$main_module=new DeskAcq();


$da_form = acs_m_params_json_decode();

// ******************************************************************************************
// REPORT
// ******************************************************************************************
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

if(isset($_REQUEST['value_fat_non_cont']) && $_REQUEST['value_fat_non_cont'] == 'Y'){
    $form_values->f_non_cont = 'Y';
    $fat_nc = 'Y';
}


if(isset($_REQUEST['ddt_nf']) && $_REQUEST['ddt_nf'] == 'Y')
    $ddt_nf = $_REQUEST['ddt_nf'];
else
    $ddt_nf = 'N';

    $ar=crea_ar_tree_fatture_entrata('', $form_values, 'Y', 'Y', $ddt_nf); //forza generazione completa

$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
WHERE TFNRDO LIKE '{$m_params->open_request->chiave}%' ORDER BY TFDTGE DESC, TFORGE DESC FETCH FIRST 1 ROWS ONLY";


$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);
$r = db2_fetch_assoc($stmt);
$data_ora = print_date($r['TFDTGE']) . " " . print_ora($r['TFORGE']);

echo "<div id='my_content'>";
echo "<div class=header_page>";

    if($ddt_nf == 'Y')
	    echo "<H2>Riepilogo documenti fornitore da non fatturare</H2>";
	elseif($ddt_nf != 'Y' && $fat_nc == 'Y')
	    echo "<H2>Controllo fatture non contabilizzate</H2>";
	else 
	    echo "<H2>Controllo fatturazione documenti fornitore</H2>";
 		
 		echo"</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";

echo "<table class=int1>";

echo "<tr class='liv_data'>";

    if($ddt_nf == 'Y')
	   echo "<th rowspan=2>Anno/Mese/Fornitore/Ddt entrata</th>";
	else	
	    echo "<th rowspan=2>Anno/Mese/Fornitore/Fattura/Ddt entrata</th>";
		
		echo "<th rowspan=2>Data</th>
		<th rowspan=2>St.</th>
  		<th rowspan=2><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=20></th>
  		<th rowspan=2><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=20></th>
  		<th rowspan=2><img src=" . img_path("icone/48x48/info_gray.png") . " height=20></th>
		<th colspan =2 class='center'> Riferimenti fornitore </th>";

if($ddt_nf != 'Y' && $fat_nc != 'Y')
		echo "<th colspan =3 class='center'> Fattura fornitore </th>";
   
		echo "<th colspan =3 class='center'> Ddt entrata</th>";

if($ddt_nf != 'Y' && $fat_nc != 'Y')
		echo "<th colspan =3 class='center'> Fatture da pervenire</th>";
   
		echo "<tr class='liv_data'>
		<th>Numero</th>
		<th>Data</th>";
	
if($ddt_nf != 'Y' && $fat_nc != 'Y')
	    echo "<th>Merce</th><th>Imponibile</th><th>Tot. doc</th>";
		
		echo "<th>Merce</th><th>Imponibile</th><th>Tot. doc</th>";
	
if($ddt_nf != 'Y' && $fat_nc != 'Y')
		echo "<th>Merce</th><th>Imponibile</th><th>Tot. doc</th>";
		
		echo"</tr></tr>";

foreach ($ar as $kar => $r){
	
	echo "<tr class ='liv3'> <td colspan= 8>".$r['descr']." [#".$r['num_sottoliv']."]</td>";
  	
	if($ddt_nf != 'Y' && $fat_nc != 'Y'){
  		 echo "<td class='grassetto'>".n($r['fat_merce'],2)."</td>
  		  <td class='number'>".n($r['fat_imp'],2)."</td>
   		  <td class='number'>".n($r['fat_totd'],2)."</td>";
    }
   		 echo "<td class='grassetto'>". n($r['ddt_merce'],2)."</td>
  		  <td class='number'>".n($r['ddt_imp'],2)."</td>
   		  <td class='number'>".n($r['ddt_totd'],2)."</td>";
   		 
   		 if($ddt_nf != 'Y' && $fat_nc != 'Y'){
   		 echo "<td class='grassetto'>".n($r['res_merce'],2)."</td>
  		  <td class='number'>".n($r['res_imp'],2)."</td>
   		  <td class='number'>".n($r['res_totd'],2)."</td>";
   	 }
  		echo "</tr>";
	
	foreach ($r['children'] as $kar1 => $r1){
		
	/*if($r['fat_merce'] == $r['ddt_merce'] && $r['fattura0'] != 'Y')
		 $class = "class = 'verde'";
		
	if($r['fat_merce'] != $r['ddt_merce'] && $r['fattura0'] != 'Y')
		 	$class = "class = 'giallo'";*/
	
	
	
		echo "<tr class='liv2'><td style='font-size:12px;'><b>".$r1['descr']."</b></td>
  		  <td>[#".$r1['num_sottoliv']."]</td>
   		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>
   		  <td>&nbsp;</td>";
	if($ddt_nf != 'Y' && $fat_nc != 'Y'){
   		 echo "<td class='grassetto'>".n($r1['fat_merce'],2)."</td>
  		  <td class='number'>".n($r1['fat_imp'],2)."</td>
   		  <td class='number'>".n($r1['fat_totd'],2)."</td>";
     }
   		  echo "<td class='grassetto'>". n($r1['ddt_merce'],2)."</td>
  		  <td class='number'>".n($r1['ddt_imp'],2)."</td>
   		  <td class='number'>".n($r1['ddt_totd'],2)."</td>";
   		  
   	if($ddt_nf != 'Y' && $fat_nc != 'Y'){
   		  echo "<td class='grassetto'>".n($r1['res_merce'],2)."</td>
  		  <td class='number'>".n($r1['res_imp'],2)."</td>
   		  <td class='number'>".n($r1['res_totd'],2)."</td>";
   		  }
  		echo "</tr>";
		
		
		foreach ($r1['children'] as $kar2 => $r2){
		    
		    if($ddt_nf != 'Y' && $fat_nc != 'Y'){
			
			$commento= $main_module->get_commento_ordine_by_k_ordine($r2['k_ordine_fatt']);  //'1 _AD_AD1_2017_000013'
		
			echo "<tr  class='liv1'><td>".$r2['descr'];
			
			foreach ($commento as $c => $t){
					
				echo "<br>".$t['text'];
					
			}
			
			echo "</td>
		  		  <td>".print_date($r2['data_reg'])."</td>
   		          <td>".$r2['stato']."</td>
 				  <td>&nbsp;</td>";
   		  		 if($r3['s_black'] > 0){
		   		  echo "<td><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=15></td>";
		   		}else{
		   		  echo "<td>&nbsp;</td>";
		   		}
   		 		 echo " <td>&nbsp;</td>
		   		  <td>".$r2['num_forn']."</td>
		   		  <td>".print_date($r2['data_forn'])."</td>";
   		 		
   		 		if($ddt_nf != 'Y' && $fat_nc != 'Y'){
		   		 echo "<td class='grassetto'>".n($r2['fat_merce'],2)."</td>
		  		  <td class='number'>".n($r2['fat_imp'],2)."</td>
		   		  <td class='number'>".n($r2['fat_totd'],2)."</td>";
   		 		 }
		   		echo "<td class='grassetto'>". n($r2['ddt_merce'],2)."</td>
		  		  <td class='number'>".n($r2['ddt_imp'],2)."</td>
		   		  <td class='number'>".n($r2['ddt_totd'],2)."</td>";
		   		if($ddt_nf != 'Y' && $fat_nc != 'Y'){
		   		 echo "<td class='grassetto'>".n($r2['res_merce'],2)."</td>
		  		  <td class='number'>".n($r2['res_imp'],2)."</td>
		   		  <td class='number'>".n($r2['res_totd'],2)."</td>";
		   		}
		  		echo "</tr>";
		  		
		    }
		
			foreach ($r2['children'] as $kar3 => $r3){
				
				$commento= $main_module->get_commento_ordine_by_k_ordine($r3['k_ordine_bolla']);  //'1 _AD_AD1_2017_000013'
				
				echo "<tr><td>".$r3['descr'];
					
				foreach ($commento as $c => $t){
						
					echo "<br>".$t['text'];
						
				}
			
				echo "</td>
		  		  <td>".print_date($r3['data_reg'])."</td>
   		          <td>".$r3['stato']."</td>";
				
				if($r3['p_red'] > 0){
					echo "<td><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
		   		
		   		if($r3['s_black'] > 0){
		   		  echo "<td><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=15></td>";
		   		}else{
		   		  echo "<td>&nbsp;</td>";
		   		}
		   		if($r3['p_zero'] > 0){
		   			echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
		   		}else{
		   			echo "<td>&nbsp;</td>";
		   		}
		   		 
		   		 echo "<td>".$r3['num_forn']."</td>
		   		  <td>".print_date($r3['data_forn'])."</td>";

		   		 if($ddt_nf != 'Y' && $fat_nc != 'Y'){
		   		  echo "<td>&nbsp;</td>
		  		  <td>&nbsp;</td>
		   		  <td>&nbsp;</td>";
		   		 }
		   		 echo "<td class='grassetto'>". n($r3['ddt_merce'],2)."</td>
		  		  <td class='number'>".n($r3['ddt_imp'],2)."</td>
		   		  <td class='number'>".n($r3['ddt_totd'],2)."</td>";
		   		 if($ddt_nf != 'Y' && $fat_nc != 'Y'){
		   		  echo "<td>&nbsp;</td>
		  		  <td>&nbsp;</td>
		   		  <td>&nbsp;</td>";
		   		 }
		  		echo "</tr>";
				
				
				
			}
			
		}
	
	}
	
	
}











