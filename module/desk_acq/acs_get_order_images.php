<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();


if ($cfg_mod_DeskAcq["visualizza_order_img"] == "N"){
	$ret = array('root' => array());
	echo acs_je($ret);
	exit;
}


//interrogo le immagini dell'ordine su sql-server

ini_set("mssql.textlimit" , "2147483647");
ini_set("mssql.textsize" , "2147483647");
ini_set("odbc.defaultlrl", "100K");

$link = mssql_connect('192.168.1.238', 'SV2', 'CUCINESTOSA');




function acs_gzdecode($data){
	$g=tempnam('/tmp','view_allegati_');
	@file_put_contents($g,$data, FILE_BINARY);
	
	$zip = new ZipArchive;
	if ($zip->open($g) === TRUE) {		
		$entry = $zip->getNameIndex($i);	
		$zip->extractTo('/tmp');
		$zip->close();
		unlink($g);		

		$filename = '/tmp/' . $entry;
		return $filename;		
	} else {
		unlink($g);
		echo 'failed';
		exit;		
	}
	
}


//******************************************************************************************
//visualizzo singola immagine
//******************************************************************************************
if ($_REQUEST['function'] == 'view_image'){

	
	$sql = "SELECT O.OGGETTO, O.TIPOOGGETTO, O.DESOGGETTO
			FROM [DB-Gestore-Stosa].dbo.[OGGETTI] O 
			WHERE O.IDOGGETTO = " . sql_t($_REQUEST['IDOggetto']) ;
	
	$stmt = mssql_query($sql);
	$row = mssql_fetch_array($stmt);

	//decomprimo l'immagine su disco
	$filename = acs_gzdecode($row['OGGETTO']);	
	
	if (!$filename){
		//e' andata in errore la decompressione
	}
	
	$handle = fopen($filename, "rb");	
	
	switch(strtolower($row['TIPOOGGETTO'])){
		case 'pdf': 						
			header('Content-type: application/pdf');						
			echo fread($handle, filesize($filename));		
		break;
		case 'tif':
		case 'tiff': 
			header('Content-type: image/tiff');
			echo fread($handle, filesize($filename));
			break;		
		case 'png':
			header('Content-type: image/png');
			echo fread($handle, filesize($filename));
			break;							
		default:
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
			header("Content-disposition: attachment; filename=\"{$row['DESOGGETTO']}\"");
			echo fread($handle, filesize($filename));
			break;			
	}
	
	unlink($filename);	
	exit();
}


//******************************************************************************************
// ELENCO allegati ordine
//******************************************************************************************

$data = array();


$oe = $s->k_ordine_td_decode($_REQUEST['k_ordine']);

$sql = "

DECLARE @ORDINE AS VARCHAR (6)
SET @ORDINE=" . sql_t($oe['TDONDO']) . "

SELECT
	IDOggetto = O.IDOGGETTO,
	NumeroOrdine = M.NORDINE,
	Azione = A.AZIONE,
	CASE
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%comp.png' THEN 'Disegno Composizione'
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%top%.png' THEN 'Top'
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%mens%.png' THEN 'Mensole'
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%zocalz%.png' THEN 'Zoccoli'
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%cor%.png' THEN 'Cornici'
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%fm%.png' THEN 'Fuori Misura'
		WHEN A.AZIONE = 'DWG' AND O.DESOGGETTO LIKE '%.d%' THEN 'Disegni DWG'
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGIN[_]%.%' THEN 'Doc inviati (inviati dal cliente)'
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGOUT[_]CON%.%' THEN 'Doc ricevuti (conferme d''ordine)'
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGOUT[_]CHI%.%' THEN 'Chiarimenti'
		WHEN A.AZIONE = 'Allegati' AND O.DESOGGETTO LIKE '%.pdf' THEN 'Doc inviati (inviati dal cliente)'
		--WHEN A.AZIONE = 'Allegati' AND O.DESOGGETTO LIKE '%allegati.pdf' THEN 'Documenti TGEO'
		--Istruzioni tecniche di montaggio: sorgenti da definire
		ELSE 'Altro'
	END AS TipoScheda,
	CASE
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%comp.png' THEN 3
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%top%.png' THEN 4
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%mens%.png' THEN 5
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%zocalz%.png' THEN 6
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%cor%.png' THEN 7
		WHEN A.AZIONE = 'STAMPE' AND O.DESOGGETTO LIKE '%fm%.png' THEN 8
		WHEN A.AZIONE = 'DWG' AND O.DESOGGETTO LIKE '%.d%' THEN 9
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGIN[_]%.%' THEN 1
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGOUT[_]CON%.%' THEN 2
		WHEN A.AZIONE = 'FAX' AND O.DESOGGETTO LIKE '%[_]MSGOUT[_]CHI%.%' THEN 0
		WHEN A.AZIONE = 'Allegati' AND O.DESOGGETTO LIKE '%.pdf' THEN 1
		--WHEN A.AZIONE = 'Allegati' AND O.DESOGGETTO LIKE '%allegati.pdf' THEN 'Documenti TGEO'
		--Istruzioni tecniche di montaggio: sorgenti da definire
		ELSE 10
	END AS TipoSchedaID,
	Bloccato = A.BLOCCATO,
	Stato = S.STATO,
	Nota = S.NOTA,
	/* Oggetto = O.OGGETTO, */
	TipoOggetto = O.TIPOOGGETTO,
	DesOggetto = O.DESOGGETTO,
	DataCreazione = S.DATACREAZIONE

FROM [DB-Gestore-Stosa].dbo.[MASTER] M
	INNER JOIN [DB-Gestore-Stosa].dbo.[AZIONI] A ON M.IDMASTER = A.IDMASTER
	INNER JOIN [DB-Gestore-Stosa].dbo.[STATI] S ON A.IDAZIONE = S.IDAZIONE
	INNER JOIN [DB-Gestore-Stosa].dbo.[OGGETTI] O ON O.IDSTATO = S.IDSTATO
WHERE
	M.NORDINE=@ORDINE
	AND
	(A.AZIONE IN ('Allegati', 'FAX', 'DWG') OR
	(A.AZIONE = 'STAMPE' AND O.DESOGGETTO NOT LIKE '%.emf' AND O.DESOGGETTO NOT LIKE '%[_]assoconf%' AND O.DESOGGETTO NOT LIKE '%[_]compbasi%' AND O.DESOGGETTO NOT LIKE '%[_]tops%')
	AND A.IDAZIONE= (select MAX(Z.IDAZIONE)
			from [DB-Gestore-Stosa].[dbo].[MASTER] as A1
			inner join [DB-Gestore-Stosa].[dbo].[AZIONI] as Z
			ON (A1.IDMASTER=Z.IDMASTER )
			where A1.NORDINE=M.NORDINE AND Z.AZIONE='STAMPE'))
ORDER BY TipoSchedaID, DataCreazione
		";



$stmt = mssql_query($sql);
while ($row = mssql_fetch_array($stmt)){
	$data[] = array('tipo_scheda' => sprintf("%02s", $row['TipoSchedaID']) . " ". $row['TipoScheda'],
					'des_oggetto' => $row['DesOggetto'],
					'IDOggetto'	  => $row['IDOggetto'],
					'DataCreazione'	  => print_date($row['DataCreazione'])			
	);	
}

$ret = array('root' => $data);
echo acs_je($ret);

$appLog->save_db();

?>
