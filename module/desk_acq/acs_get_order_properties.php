<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();

	$sottostringhe = explode("|", $_REQUEST["m_id"]);
	$n_ord = $sottostringhe[6];


	$r = $main_module->get_ordine_by_k_ordine($_REQUEST["m_id"]);
	 
	if ($r['TDDTEPI'] != $r['TDDTEPF'] && $r['TDDTEPF'] != 0)
		$seconda_data_confermata = " (Ult.  " . print_date($r['TDDTEPF']) . ")";
	else
		$seconda_data_confermata = ''; 

echo '{
	"title": "Dettaglio ordine ' . $r['TDOTPD'] . '_' . $r['TDOADO'] . '_' . $r['TDONDO'] . trim($r['TDMODI']) . " " . trim($r['TDISON']) . '",		
	"riferimenti": {
	    "Tipo": "' . $r['TDDOTD'] . '",
	    "Divisione": "' . $r['TDDDIV'] . '",
	    "Stato": "' . $r['TDDSST'] . '",	    						
		"Varianti": ' . j($r['TDDVN1']) .  ',';

		if (strlen(trim($r['TDDVN2'])) > 0) echo '"\'\'\'": ' .j($r['TDDVN2']) . ',';
		if (strlen(trim($r['TDDVN3'])) > 0) echo '"\'\'": ' .j($r['TDDVN3']) . ',';				
		
  echo '
	    "Stato fornitore": "' . $r['TDDSRI'] . '",
	    "Denominazione": ' . j($r['TDDCON']) . ',
	    "E-mail": ' . j($r['TDMAIL']) . ',
	    "Telefono": ' . j($r['TDTEL']) . ',	    		
	    "Destinazione ": ' . j($r['TDINDI']) . ',';
             
          if (trim($r['TDNAZI']) == 'ITA' || trim($r['TDNAZI']) == 'IT')
		  	echo '"Localita\'": ' . j(implode(', ', array_map('trim', array($r['TDLOCA'], $r['TDCAP'], "( {$r['TDPROV']} )")))) ;
		  else
		  	echo '"Localita\'": ' . j(implode(', ', array_map('trim', array($r['TDLOCA'], $r['TDNAZ'])))) ;
  
  
  echo '
	    , "Agente": "' . $r['TDDAG1'] . '",
	    "Referente fornitore": "' . $r['TDDOCL'] . '",
	    "Referente ordine": "' . $r['TDDORE'] . '",
	    "Fine attesa conf.": "' . print_date($r['TDDTAR']) . ' ' . print_ora($r['TDHMAR']) . '",
	    "Cod. Fornitore/Destin.": ' . j($r['TDCCON'] . ' - ' . $r['TDCDES']) . ',';
  
  
		  echo '"----------": "---------------------------",';	
		  echo '"Rif.Scarico": "' . $r['TDRFCA'] . '(Sped. # ' . $r['TDNBOC'] . ')",';
		  if (strlen(trim($r['TDABBI'])) > 0) echo '"Ordine abbinato": "' . $r['TDABBI'] . '",';
		  
		  echo '"Disponib. alla sped.": "' . print_date($r['TDDTDS']) . '"';  
  
		
		
	if ($r['TDTDES'] == "1" || $r['TDTDES'] == "2"){    	    
	    echo ', "Scarico": ' . j($r['TDDDES'] . " ( {$r['TDCDES']} )");
	    
		if (trim($r['TDNAZD']) == 'ITA')
			$ar_dest = array($r['TDDCAP'], $r['TDDLOC'], "( {$r['TDPROD']} )" );
		else
			$ar_dest = array($r['TDDLOC'], $r['TDDNAD']);	
		
		echo '	    	    
	    , "Indirizzo scar.": ' . j(implode(',', array_map('trim', array($r['TDIDES'])))) . '	    
	    , "Localita scar.": ' . j(implode(', ', array_map('trim', $ar_dest)));	    		
	}	

	
  echo '	
	  , "Carico": "' . implode("_", array($r['TDAACA'], $r['TDTPCA'], $r['TDNRCA'])) . ' Seq. ' . $r['TDSECA'] . '",
		"Data rilascio": "' . print_date($r['TDDTRP']) . '",
		"Lotto": "' . $r['TDAALO'] . '_' . $r['TDNRLO'] . '_' . $r['TDTPLO'] . ' Seq.' . $r['TDSELO'] . '",
		"Spunta disponib.": "' . print_date($r['TDDTSPI']) . ' / ' . print_ora($r['TDHMSPI']) . ' - ' . print_date($r['TDDTSPF']) . ' / ' . print_ora($r['TDHMSPF']) . '",
		"Spunta spedizione": "' . print_date($r['TDDTSSI']) . ' / ' . print_ora($r['TDHMSSI']) . ' - ' . print_date($r['TDDTSSF']) . ' / ' . print_ora($r['TDHMSSF']) . '",
		"Proforma": "' . $r['TDPROF'] . '",
		"Doc trasporto": "' . $r['TDAADE'] . '_' . $r['TDNRDE'] . '_' . $r['TDTPDE'] . ' del ' . print_date($r['TDDTRE']) . '",
		"Immesso il": "' . print_date($r['TDDTEV']) . ' , ' . print_ora($r['TDHMEV']) . ' , ' . $r['TDUSEV'] . '",
		"Trasportatore 1": "' . $r['TDDVE1'] . '",
		"Trasportatore 2": "' . $r['TDDVE2'] . '"';	
	
echo '				
	 },

	"dettagli": {
		"Ordine": "' . $r['TDOTPD'] . '_' . $r['TDOADO'] . '_' . $r['TDONDO'] . trim($r['TDMODI']) . " " . trim($r['TDISON']) . '",
				
		"Protocollo": "' . print_date($r['TDDTIM']) . ' ' . $r['TDHMIM'] . ' ' . $r['TDUSIM'] .  '",				
		"Assegnato": "' . print_date($r['TDDTAS']) . ' ' . print_ora($r['TDHMAS']) . ' ' . $r['TDUSAS'] .  '",
	    "Order entry": "' . print_date($r['TDDTIO']) . ' ' . print_ora($r['TDHMIO']) . ' ' . $r['TDUSIO'] . '",	    				
	    "Ultima modifica": "' . print_date($r['TDDTOE']) . ' ' . print_ora($r['TDOROE'])  . ' ' . $r['TDUSOE'] . '",    
	    "Controllo": "' . print_date($r['TDDTCO']) . ' ' . print_ora($r['TDHMCO'])  . ' ' . $r['TDUSCO'] . '",	    

	    "Confermato": "' . print_date($r['TDDTCF']) . ' ' . print_ora($r['TDHMCF'])  . '",
	    "Evas. confermata": "' . print_date($r['TDDTEPI']) . $seconda_data_confermata . '",			    
        
	    "Blocco/Sblocco Amm": "' . print_date($r['TDDTBLO']) . ' / ' . print_date($r['TDDTSBO']) . '",
	    "Blocco/Sblocco Com": "' . print_date($r['TDDTBLC']) . ' / ' . print_date($r['TDDTSBC']) . '",	    			           	    
				

	 },	        
}                            
';

$appLog->save_db();




 


?>
