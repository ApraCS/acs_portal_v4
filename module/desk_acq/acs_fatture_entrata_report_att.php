<?php
require_once "../../config.inc.php";
require_once "acs_fatture_entrata_include.php";

$main_module=new DeskAcq();

$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    ?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [
				{
			            xtype: 'combo',
						name: 'f_forn',
						fieldLabel: 'Fornitore',
						anchor: '-15',
						flex : 1 ,  
					    labelWidth : 120,
						minChars: 2,			
            			store: {
			            	pageSize: 1000,            	
							proxy: {
					            type: 'ajax',
					            url : '../base/acs_get_anagrafica.php?fn=get_data&cod=F',
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
								fields: [{name:'id'}, {name:'text'}, {name:'loca'}, {name:'color'}],            	
			            },
						valueField: 'id',                        
			            displayField: 'text',
			            typeAhead: false,
			            hideTrigger: true,
			            triggerAction: 'all',
			            allowBlank : false,
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun fornitore trovato',
                
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item" style="background-color:{color}">' +
			                        '<h3><span>{text}</span></h3>' +
			                        '[{id}] {loca}' + 
			                    '</div>';
			                	}                
		                
		            	},
		            	pageSize: 1000,
		                 listeners: {
                            beforequery: function(qeb){
                           
                               qeb.combo.getStore().removeAll();
								
							}
                        }
						}  ,
						
			{ xtype: 'fieldcontainer'
    	     , layout: 
    	       { type: 'hbox'
    	       , pack: 'start'
    	       , align: 'stretch'
    		   }
    		 , items: [
        	       { name: 'f_data_in'  
        		   , width : 300
        		   , labelWidth : 120 	
        		   , xtype: 'datefield'
        		   , startDay: 1 //lun.
        		   , fieldLabel: 'Data fattura iniziale'
        		   , labelAlign: 'left'
        		   , format: 'd/m/Y'
        		   , submitFormat: 'Ymd'
        		   , anchor: '-15'
        		   }
        		 , { name: 'f_data_fin'
        		   , margin: "0 15 0 10"						     
        		   , flex : 1   						                     		
        		   , xtype: 'datefield'
        		   , labelWidth :30
        		   , startDay: 1 //lun.
        		   , fieldLabel: 'finale'
        		   , labelAlign: 'right'						   
        		   , format: 'd/m/Y'
        		   , submitFormat: 'Ymd'
        		   , anchor: '-15'
        		   }]}  ,
        		    {
					name: 'f_stato_fat',
					xtype: 'combo',
					labelWidth : 120,
					width : 100,
					multiSelect : true,
					fieldLabel: 'Stato fattura',
					displayField: 'text',
					valueField: 'id',
					emptyText: '- seleziona -',
					forceSelection: true,
				   	allowBlank: true,		
				    anchor: '-15',
				    margin : '0 0 5 0',
				  	store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
						     <?php echo acs_ar_to_select_json(find_TA_sys('BSTA', null, null, 'AF', null, null, 0, '', 'Y'), '') ?> 	
						    ] 
						}			 
					},
					{
					name: 'f_stato_ddt',
					xtype: 'combo',
					labelWidth : 120,
					width : 100,
					multiSelect : true,
					fieldLabel: 'Stato ddt',
					displayField: 'text',
					valueField: 'id',
					emptyText: '- seleziona -',
					forceSelection: true,
				   	allowBlank: true,		
				    anchor: '-15',
				    margin : '0 0 0 0',
				  	store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [								    
						     <?php echo acs_ar_to_select_json(find_TA_sys('BSTA', null, null, 'AD', null, null, 0, '', 'Y'), '') ?> 	
						    ] 
						}			 
					}      
            ],
            
			buttons: [					
				{
		            text: 'Report',
			        iconCls: 'icon-print-24',		            
			        scale: 'medium',		            
		            handler: function() {
		                var form = this.up('form');
		                if(form.getForm().isValid()){
		                form.submit({
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues())
	                        }
	                  });
	                  
	                  this.up('window').close();
		                }
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}


// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
    
    $ar_email_to = array();
    
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u){
    	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$ar=crea_ar_tree_fatture_entrata('', $form_values, 'Y', 'Y', 'N'); //forza generazione completa

$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
        WHERE TFNRDO LIKE '{$m_params->open_request->chiave}%' 
        AND TFCCON = '{$form_values->f_forn}'
        ORDER BY TFDTGE DESC, TFORGE DESC FETCH FIRST 1 ROWS ONLY";


$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);
$r = db2_fetch_assoc($stmt);
$data_ora = print_date($r['TFDTGE']) . " " . print_ora($r['TFORGE']);

echo "<div id='my_content'>";
echo "<div class=header_page>";

echo "<H2>Riepilogo attivit� controllo fatturazione forniture {$r['TFDCON']}</H2>";
 		
 		echo"</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";

echo "<table class=int1>";

echo "<tr class='liv_data'>";
echo "<th rowspan=2>Anno/Mese/Fattura</th>";
		
		echo "<th rowspan=2>Data</th>
		<th rowspan=2>St.</th>
  		<th rowspan=2><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=20></th>
  		<th rowspan=2><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=20></th>
  		<th rowspan=2><img src=" . img_path("icone/48x48/info_gray.png") . " height=20></th>
		<th colspan =2 class='center'> Riferimenti fornitore </th>";
		echo "<th rowspan =2>Imponibile</th>";
		/*echo "<th rowspan =2>Divisione</th>";*/
		echo "<th rowspan =2>Ordine fornitore</th>";
		echo "<th rowspan =2>Data</th>";
		echo "<th rowspan =2>Imponbile ordine for.</th>";
		echo "<th rowspan =2>Ordine vendita</th>";
		echo "<th rowspan =2>ToDo Controllo costo confermato</th>";
		echo "<th rowspan =2>ToDo Richiesta modifica importo fornitura</th>";
		echo "<tr class='liv_data'>
		      <th>Numero</th>
		      <th>Data</th>";

	    echo"</tr>";
	    
	    
	    
	    $sql_f = "SELECT TD_ACQ.*, MT.*, TDOADO, TDONDO, TDOTPD, TDDOCU, TOTIMP 
                 FROM {$cfg_mod_DeskPVen['file_righe_doc']} RD
	             LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD_ACQ
	               ON RD.RDDT=TD_ACQ.TDDT AND RD.RDTIOR=TD_ACQ.TDTIDO AND RD.RDINOR =TD_ACQ.TDINUM AND RD.RDANOR =TD_ACQ.TDAADO AND RD.RDNROR=TD_ACQ.TDNRDO
                 LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_proposte_MTO']} MT
                   ON RD.RDDT = MT.MTDT AND RD.RDTIOR = MT.MTTIDQ AND RD.RDINOR = MT.MTINUQ AND RD.RDANOR = MT.MTAADQ AND RD.RDNROR = MT.MTNRDQ
                 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
	               ON MTDT=TD.TDDT AND MTTIDO=TD.TDOTID AND MTINUM=TD.TDOINU AND MTAADO=TD.TDOADO AND MTNRDO=TD.TDONDO                
                 LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_testate_gest_valuta']} TO1 
                  ON TO1.TODT = MT.MTDT AND TO1.TOTIDO = MT.MTTIDQ AND  TO1.TOINUM = MT.MTINUQ AND TO1.TOAADO= MT.MTAADQ AND TO1.TONRDO = MT.MTNRDQ AND TO1.TOVALU = 'EUR'
                 WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
                ";
	    
	    $stmt_f = db2_prepare($conn, $sql_f);
	  
	   

	    foreach ($ar as $kar => $r){
	        foreach ($r['children'] as $kar1 => $r1){
	            
	            echo "<tr class ='liv3'> <td colspan= 8>".$r['descr']." [#".$r1['num_sottoliv']."]</td>";
	            echo "<td class='number'>".n($r['fat_imp'],2)."</td>";
	            echo "<td colspan = 6>&nbsp;</td>";
	            echo "</tr>";
	            
	    
		foreach ($r1['children'] as $kar2 => $r2){
		    
		    if($ddt_nf != 'Y' && $fat_nc != 'Y'){
			
			echo "<tr  class='liv1'><td>".$r2['descr'];
						
			echo "</td>
		  		  <td>".print_date($r2['data_reg'])."</td>
   		          <td>".$r2['stato']."</td>
 				  <td>&nbsp;</td>";
   		  		 if($r3['s_black'] > 0){
		   		  echo "<td><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=15></td>";
		   		}else{
		   		  echo "<td>&nbsp;</td>";
		   		}
   		 		 echo " <td>&nbsp;</td>
		   		  <td>".$r2['num_forn']."</td>
		   		  <td>".print_date($r2['data_forn'])."</td>";
   		 		
   		 		echo "<td class='number'>".n($r2['fat_imp'],2)."</td>";
   		 		echo "<td colspan = 7>&nbsp;</td>";
		  		echo "</tr>";
		  		
		    }
		
		    //bolla
			foreach ($r2['children'] as $kar3 => $r3){
			    
			    $k_ordine_bolla = $r3['k_ordine_bolla'];
			    $oe = $s->k_ordine_td_decode_xx($k_ordine_bolla);
			    $result = db2_execute($stmt_f, $oe);
			    $rf = db2_fetch_assoc($stmt_f);
			    $ord_forn =  $rf['TDAADO']."_".sprintf("%06s", $rf['TDNRDO'])."_".$rf['TDTPDO'];
			    $ord_forn .= " [{$rf['TDSTAT']}]";
			    $ord_ven =  $rf['TDOADO']."_".sprintf("%06s", $rf['TDONDO'])."_".$rf['TDOTPD'];
			    $data_forn = print_date($rf['TDDTRG']);
			    echo "<tr><td>".$r3['descr'];
					
				echo "</td>
		  		  <td>".print_date($r3['data_reg'])."</td>
   		          <td>".$r3['stato']."</td>";
				
				if($r3['p_red'] > 0){
					echo "<td><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=15></td>";
				}else{
					echo "<td>&nbsp;</td>";
				}
		   		
		   		if($r3['s_black'] > 0){
		   		  echo "<td><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=15></td>";
		   		}else{
		   		  echo "<td>&nbsp;</td>";
		   		}
		   		if($r3['p_zero'] > 0){
		   			echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
		   		}else{
		   			echo "<td>&nbsp;</td>";
		   		}
		   		 
		   		 echo "<td>".$r3['num_forn']."</td>
		   		  <td>".print_date($r3['data_forn'])."</td>";

		   		 echo "<td class='number'>".n($r3['ddt_imp'],2)."</td>";
		   		// echo "<td>&nbsp;</td>";
		   		 echo "<td>{$ord_forn}</td>";
		   		 echo "<td>{$data_forn}</td>";
		   		 echo "<td>".n($rf['TOTIMP'], 2)."</td>";
		   		 echo "<td>{$ord_ven}</td>";
		   		 echo "<td>";
		   		 if($rf['TDOADO'] > 0){
		   		 $sql_todo = "SELECT ATT_OPEN.*, NT_MEMO.NTMEMO AS MEMO
		   		              FROM {$cfg_mod_Spedizioni['file_assegna_ord']} ATT_OPEN
                              LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
                                 ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
                              WHERE ATT_OPEN.ASDOCU = '{$rf['TDDOCU']}' AND ATT_OPEN.ASCAAS = 'CK_CST_CF'
		   		              ";
		   		 
		   		 
		   		 
		   		 $stmt_todo = db2_prepare($conn, $sql_todo);
		   		 $result = db2_execute($stmt_todo);
		   		
		   		 while($rt = db2_fetch_assoc($stmt_todo)){
		   		     $todo = print_date(trim($rt['ASDTAS'])).", ";
		   		     $todo .= trim($rt['ASNOTE']);
		   		     if(trim($rt['MEMO']) != '')
		   		         $todo .=  " " .trim($rt['MEMO']);
		   		     if(trim($rt['ASFLRI']) == 'Y'){
		   		         $causali_rilascio = $s->find_TA_std('RILAV', trim($rt['ASCAAS']), 'N', 'N', trim($rt['ASCARI'])); 
		   		         $d_rilascio =  $causali_rilascio[0]['text'];
		   		         $todo .= "<br>".print_date(trim($rt['ASDTRI'])).", [".trim($rt['ASCARI'])."] ".$d_rilascio.", ".trim($rt['ASNORI']);
		   		     }
		   		         echo $todo."<br><br>";
	   		     }
		   		 }
				 echo "</td>";  
				 echo "<td>";
				 if($rf['TDOADO'] > 0){
    				 $sql_todo2 = "SELECT ATT_OPEN.*, NT_MEMO.NTMEMO AS MEMO
                    				 FROM {$cfg_mod_Spedizioni['file_assegna_ord']} ATT_OPEN
                    				 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
                    				 ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
                    				 WHERE ATT_OPEN.ASDOCU = '{$rf['TDDOCU']}' AND ATT_OPEN.ASCAAS = 'RC_MOD_CF'
                                     ";
    				
    				 
    				 $stmt_todo2 = db2_prepare($conn, $sql_todo2);
    				 $result = db2_execute($stmt_todo2);
    				
    				 while($rt2 = db2_fetch_assoc($stmt_todo2)){
    				     $todo2 = print_date(trim($rt2['ASDTAS'])).", ";
    				     $todo2 .= trim($rt2['ASNOTE']);
    				     if(trim($rt2['MEMO']) != '')
    				         $todo2 .=  " " .trim($rt2['MEMO']);
    			         if(trim($rt2['ASFLRI']) == 'Y')
    			             $todo2 .= " - Rilascio: ".print_date(trim($rt['ASDTRI'])).", ".trim($rt['ASCARI']).", ".trim($rt['ASNORI']);
    		             echo $todo2."<br><br>";
    				 }
			  }
			  
				 echo "</td>";  
				 
		   		 
		   		 echo "</tr>";
				
				
				
			}
			
		}
	
	}
	
	
}


}


