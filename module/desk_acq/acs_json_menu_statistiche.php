<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
 
?>

{"success":true, "items": [
{
	            xtype: 'panel',
	            title: '',
	            layout: 'fit',
				cls: 'acs_toolbar',
				tbar: [{
				            xtype: 'buttongroup',
				            columns: 50,
				            title: 'Statistiche',
				            items: [{
				                text: 'Analisi rivendite promozionali',
				                scale: 'large',			                 
				                iconCls: 'icon-grafici-32',
				                iconAlign: 'top',			                
				                 handler : function() {				                	 
				                	 acs_show_win_std('Analisi rivendite promozionali', 'acs_panel_rivendite_promozionali.php?fn=open_form', null, 600, 600, null, 'icon-grafici-16', 'N', 'N');
				                 	 this.up('window').close();				                	 
									} //handler function()
				            } 
						    ]
				}]
			}
]
}	