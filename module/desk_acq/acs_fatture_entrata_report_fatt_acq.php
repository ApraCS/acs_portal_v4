<?php
require_once "../../config.inc.php";
require_once "acs_fatture_entrata_include.php";


$m_DeskAcq = new DeskAcq();

$main_module=new DeskAcq();

$da_form = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          
                 {
				     name: 'f_data_in'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data iniziale'
				   , labelAlign: 'left'
				   , labelWidth: 150
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 		 }
			}, {
				     name: 'f_data_fin'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data finale'
				   , labelAlign: 'left'
				   , labelWidth: 150
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 		 }
			   }, {
                     xtype: 'checkbox'
                   , flex: 1.5
                   , name: 'f_filtra_fatture' 
                   , boxLabel: 'Solo fatture controllate'
                   , checked: false
                   , inputValue: 'Y'
                  }
	            ],
	            
				buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	
	if ($_REQUEST['fn'] == 'open_report'){

// ******************************************************************************************
// REPORT
// ******************************************************************************************
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   tr.liv_tot td{ background-color: #DDDDDD; font-weight: bold;} 
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);


$form_values_f = strtr($_REQUEST['filter'], array('\"' => '"'));
$form_values_f = json_decode($form_values_f);

$form_values->f_forn = $form_values_f->f_forn;

 $ddt_nf = 'N';

//*** SQL WHERE ***//
$sql_where  = parametri_sql_where($form_values);

$sql_dataora= "Select * from {$cfg_mod_DeskAcq['file_testate_fatture']}
        where TFNRDO like '{$m_params->open_request->chiave}%' order by TFDTGE desc, TFORGE desc FETCH FIRST 1 ROWS ONLY";
$stmt_dataora = db2_prepare($conn, $sql_dataora);
$result_dataora = db2_execute($stmt_dataora);
$r_dataora = db2_fetch_assoc($stmt_dataora);
$data_ora = print_date($r_dataora['TFDTGE']) . " " . print_ora($r_dataora['TFORGE']);

//**************************************//
//*** Estrazione per totali generali ***//
$sql_loj1 = " left outer join
                ( Select RFDOCU, SUM (RFTOTD) as RFTOTD, SUM (RFINFI) as RFINFI,
                    SUM (RFTIMP) as RFTIMP, Count(RFRIGA) as NR_RFRIGA
                from {$cfg_mod_DeskAcq['file_righe_fatture']}
                group by RFDOCU       
                ) 
              RF on TFDOCU=RF.RFDOCU ";

/*$sql_loj2 = " left outer join
                (Select count(RDRIGA) as NR_RG_DDT,
                 RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                 from {$cfg_mod_DeskAcq['file_righe_doc_gest']} where 1=1
                 group by RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                 order by RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO)
              RD on RD.RDDT = RF.RFDT AND RD.RDTIDO = RF.RFTIDE AND RD.RDINUM = RF.RFINUE AND 
                    RD.RDAADO = RF.RFAADE AND RD.RDNRDO = SUBSTRING(RF.RFNRDE, 1, 6)";*/

$sql_gen = "Select count(distinct (TFCCON)) as NR_FORN, TFDT,
                   SUM(CASE WHEN TFAADO = 0 THEN 0 ELSE TFTIMP END) AS TOT_TFTIMP,
                  /* Sum(TFTIMP) as TOT_TFTIMP,*/
                   Sum(TFTOTC-TFTOTD) as TOT_SCOST,
                   Sum(TFVVAR) as TOT_TFVVAR, SUM(TFTOTC) as TOT_TFTOTC, SUM(TFTOTD) as TOT_TFTOTD, 
                   SUM(TFVVAR) as TOT_TFVVAR, SUM (RF.RFTIMP) as TOT_RFTIMP, SUM(NR_RFRIGA) as TOT_NR_DDT
                 /* , SUM(NR_RG_DDT) as TOT_NR_RG_DDT*/
             from {$cfg_mod_DeskAcq['file_testate_fatture']} TF
            {$sql_loj1}
        
            where 1=1 {$sql_where} {$sql_where_var}
            group by TFDT";
     
$stmt_gen = db2_prepare($conn, $sql_gen);
echo db2_stmt_errormsg();
$result_gen = db2_execute($stmt_gen);
$row_gen = db2_fetch_assoc($stmt_gen);


//************************************//
//*** Estrazione testata fornitore ***//
$sql_where_var ='';
$sql_forn = "Select Distinct TF.TFDCON, TF.TFCCON from {$cfg_mod_DeskAcq['file_testate_fatture']} TF
where TFFU02 = '' {$sql_where} {$sql_where_var}  group by TF.TFDCON, TF.TFCCON order by TF.TFDCON, TF.TFCCON";
$stmt_forn = db2_prepare($conn, $sql_forn);
echo db2_stmt_errormsg();
// print_r($sql_forn); exit;

//************************************************//
//*** Estrazione importi fatture/ddt per fornitore ***//
$sql_where_var  = " and TFCCON = ? ";
$sql_loj1 = " left outer join
                ( Select distinct RFDOCU, /*RFDT, RFTIDE, RFINUE, RFAADE, RFNRDE,*/
                  SUM (RFTOTD) as RFTOTD, SUM (RFINFI) as RFINFI, SUM (RFTIMP) as RFTIMP, Count(RFRIGA) as NR_RFRIGA 
                  from {$cfg_mod_DeskAcq['file_righe_fatture']} where 1=1 
                  group by RFDOCU)  
              RF on TFDOCU=RF.RFDOCU ";

/*$sql_loj2 = " left outer join 
                (Select count(RDRIGA) as NR_RG_DDT,
                 RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                 from {$cfg_mod_DeskAcq['file_righe_doc_gest']} where 1=1
                 group by RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                 order by RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO) 
              RD on RD.RDDT = RF.RFDT AND RD.RDTIDO = RF.RFTIDE AND RD.RDINUM = RF.RFINUE AND 
                    RD.RDAADO = RF.RFAADE AND RD.RDNRDO = SUBSTRING(RF.RFNRDE, 1, 6)";*/


$sql_dett = "Select TFDT, TFCCON, TFDOCU, TFTIDO, TFINUM, TFAADO, TFNRDO, TFUSIM,
                    Sum(TFTIMP) as TFTIMP, SUM(TFTOTC) as TFTOTC, 
                    SUM(TFTOTD) as TFTOTD, SUM(TFVVAR) as TFVVAR, SUM (RF.RFTIMP) as RFTIMP,
                    SUM(NR_RFRIGA) as NR_DDT /*, SUM(NR_RG_DDT) as NR_RG_DDT*/
             from {$cfg_mod_DeskAcq['file_testate_fatture']} TF
            {$sql_loj1} 
           
            where TFFU02 = '' {$sql_where} {$sql_where_var}

            group by TFDT, TFCCON, TFDOCU, TFTIDO, TFINUM, TFAADO, TFNRDO, TFUSIM
            order by TFDT, TFCCON, TFDOCU, TFTIDO, TFINUM, TFAADO, TFNRDO";
            //            group by TFDT, TFCCON
            //            order by TFDT, TFCCON"; 

$stmt_dett = db2_prepare($conn, $sql_dett);
echo db2_stmt_errormsg();

//**********************************************************//
//*** Estrazione per totale imponibile note di varazione ***//
if($ddt_nf == 'N'){
    if ($form_values->f_ddt_nf == 'NF'){
        $sql_where_rf .= " and RF.RFSTAT <> 'NF' "; }
} else { $sql_where_rf .= " and RF.RFSTAT = 'NF' "; }

$form_values->report_ddt_variazioni_attese = 'Y';
$sql_where  = parametri_sql_where($form_values);

$sql_where_var ='';
$sql_where_var  = " and TFFU01='N' and TFFLAV<>9 and TFFG03 = ''";
$sql_loj1 = " left outer join
                (Select RFDOCU, SUM (RFTOTD) as RFTOTD, SUM (RFINFI) as RFINFI, SUM (RFTIMP) as RFTIMP
                from {$cfg_mod_DeskAcq['file_righe_fatture']} where 1=1 $sql_where_rf group by RFDOCU)
                RF on TFDOCU=RF.RFDOCU";
$sql_tot_imp_nt_var ="Select SUM (RF.RFTIMP) as TOT_RFTIMP from {$cfg_mod_DeskAcq['file_testate_fatture']} TF
                  {$sql_loj1}
                  where TFFU02 = '' {$sql_where} {$sql_where_var}";
                  // print_r($sql_imp_nt_var); exit;
$stmt_tot_imp_nt_var = db2_prepare($conn, $sql_tot_imp_nt_var);
echo db2_stmt_errormsg();
$result_tot_imp_nt_var = db2_execute($stmt_tot_imp_nt_var);
$row_gen_nt = db2_fetch_assoc($stmt_tot_imp_nt_var);
// print_r($row_gen_nt);exit;
// print_r($sql_tot_imp_nt_var);exit;

//*******************************************************************//
//*** Estrazione imponibile note variazione fatture per fornitore ***//
if($ddt_nf == 'N'){
    if ($form_values->f_ddt_nf == 'NF'){
        $sql_where_rf .= " and RF.RFSTAT <> 'NF' "; }
} else { $sql_where_rf .= " and RF.RFSTAT = 'NF' "; }

$form_values->report_ddt_variazioni_attese = 'Y';
$sql_where  = parametri_sql_where($form_values);

$sql_where_var ='';
$sql_where_var  = " and TFCCON = ? and TFFU01='N' and TFFLAV<>9 and TFFG03 = ''";
$sql_loj1 = " left outer join 
                (Select RFDOCU, sum (RFTOTD) as RFTOTD, sum (RFINFI) as RFINFI, sum (RFTIMP) as RFTIMP,
                 (case when sum (RFTIMP)>0 then 1 else 0 end) as NR_DOC_VAR
                from {$cfg_mod_DeskAcq['file_righe_fatture']} where 1=1 $sql_where_rf group by RFDOCU)
                RF on TFDOCU=RF.RFDOCU";
$sql_imp_nt_var ="Select sum (RF.RFTIMP) as RFTIMP, sum (NR_DOC_VAR) as NR_DOC_VAR from {$cfg_mod_DeskAcq['file_testate_fatture']} TF
                  {$sql_loj1} 
                  where TFFU02 = '' {$sql_where} {$sql_where_var}";
// print_r($sql_imp_nt_var); exit;
$stmt_imp_nt_var = db2_prepare($conn, $sql_imp_nt_var);
echo db2_stmt_errormsg();

            
//*** Emissione report ***//
    echo "<div id='my_content'>";
    echo "<div class=header_page>";
    echo "<H2>Analisi documenti fatturazione acquisti (Dal ".print_date($form_values->f_data_in)." al ".print_date($form_values->f_data_fin).")</H2>";
    
    echo"</div>
   <div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";
    echo "<table class=int1>";
    echo "<tr class='liv_data'>";
    $rs='2';
    $wd_1=80;
    $wd_2=50;
    $wd_3=100;
    echo "<th rowspan=$rs>Fornitore #".n($row_gen['NR_FORN'],0)."</th>";
    echo "<th colspan=3>Imponibile fatture</th>";
    echo "<th colspan=3>Scostamento</th>";
    echo "<th colspan=2>Variazione</th>";
    echo "<th colspan=2>Imponibile note variazione</th>";
    echo "<th colspan=6>Ddt</th>";
        echo "<tr  class='liv_data'>";
        echo "<th width=$wd_1>Importo</th>";
        echo "<th width=$wd_2>% Inc.</th>";
        echo "<th width=$wd_2>nr.doc.</th>";
        echo "<th width=$wd_1>Importo</th>";
        echo "<th width=$wd_2>% Inc.</th>";
        echo "<th width=$wd_2>nr.doc.</th>";
        echo "<th width=$wd_1>Importo</th>";
        echo "<th width=$wd_2>nr.doc.</th>";
        echo "<th width=$wd_1>Importo</th>";
        echo "<th width=$wd_2>nr.doc.</th>";
        echo "<th width=$wd_1>Imponibile</th>";
        echo "<th width=$wd_2>nr.doc.</th>";
        echo "<th width=$wd_2>nr.rg.</th>";
        echo "<th width=$wd_2>media rg.</th>";
        echo "<th width=$wd_2>% Inc.</th>";

        echo "<th width=$wd_3>Utenti su TF</th>";
        echo "</tr>";
        

    echo "</tr>";
    
$ddt_tot = 0;
$tftimp_tot = 0;
$scost_tot  = 0;
$rftimp_tot = 0;
$variaz_tot = 0;
//$tot_ddt    = 0;
$nt_var_tot = 0;

$result_forn = db2_execute($stmt_forn);
while ($row_forn = db2_fetch_assoc($stmt_forn)) {

    $result_imp_nt_var = db2_execute($stmt_imp_nt_var, array($row_forn['TFCCON']));
    $row_imp_nt_var = db2_fetch_assoc($stmt_imp_nt_var);
    $nt_var_tot += $row_imp_nt_var['NR_DOC_VAR'];
    //print_r($row_imp_nt_var."</br>");
    
    // *** Record fornitore + imponibile note variazione ***/
    
    $tftimp_perc = 0;
    $tftimp_forn = 0;
    $tftimp_nume = 0;
    $scost       = 0;
    $scost_perc  = 0;
    $scost_nume  = 0;
    $variaz      = 0;
    $variaz_nume = 0;
    $rftimp_forn = 0;
    $rftimp_rg_perc = 0;
    $ddt_nume    = 0;
    $rg_ddt_nume = 0;
    $rg_ddt_medi = 0;
    $ar_utenti   = array();
    
    $result_dett = db2_execute($stmt_dett, array($row_forn['TFCCON']));
       
    while ($row_dett = db2_fetch_assoc($stmt_dett)) {
        
        if($row_dett['TFAADO'] != 0)
            $tftimp_forn += $row_dett['TFTIMP'];
           
            $tftimp_perc += ($tftimp_forn / ($row_gen['TOT_TFTIMP'] / 100));
       // $tftimp_forn += $row_dett['TFTIMP'];
        $scost       += ($row_dett['TFTOTC'] - $row_dett['TFTOTD']);
        $scost_perc  += ($row_dett['TFTOTC']-$row_dett['TFTOTD']) / ($row_gen['TOT_SCOST'] /100);
        $variaz      += $row_dett['TFVVAR'];
        $rftimp_forn += $row_dett['RFTIMP'];
        
       // $rftimp_rg_perc += ($row_dett['NR_RG_DDT'] / ($row_gen['TOT_NR_RG_DDT'] / 100));
        //print_r($row_dett['RFTIMP']);
        
        $ar_utenti[$row_dett['TFUSIM']] = $row_dett['TFUSIM'];
        if ($row_dett['TFNRDO'] >0) {
            if ($row_dett['TFTIMP'] >0) {
                $tftimp_nume += 1;
                $tftimp_tot += 1; }
            if (($row_dett['TFTOTC'] - $row_dett['TFTOTD']) !=0) {
                $scost_nume += 1;
                $scost_tot += 1; }
            if ($row_dett['TFVVAR'] >0) {
                $variaz_nume += 1;
                $variaz_tot += 1; }
        }
        //if ($row_dett['RFTIMP'] >0) {
            $ddt_nume     += $row_dett['NR_DDT'];
            $rg_ddt_nume  += $row_dett['NR_RG_DDT'];
        //    $tot_ddt += 1; 
        //}
    }
        $rg_ddt_medi = $rg_ddt_nume / $ddt_nume;
        $ddt_tot += $row_dett['RFTIMP'];
        
        ob_start();
        echo "<tr>";
        echo "<td >". trim(acs_u8e($row_forn['TFDCON'])) .'['.$row_forn['TFCCON']."]</td>";
        
        echo "<td class = number>".n($tftimp_forn,2) ."</td>";
        echo "<td class = number>".n($tftimp_perc,2) ." %</td>";
        echo "<td class = number>".n($tftimp_nume,0) ."</td>";
        echo "<td class = number>".n($scost,2)."</td>";
        echo "<td class = number>".n($scost_perc,2) ." %</td>";
        echo "<td class = number>".n($scost_nume,0) ."</td>";
        echo "<td class = number>".n($variaz,2) ."</td>";
        echo "<td class = number>".n($variaz_nume,0) ."</td>";
        echo "<td class = number>".n($row_imp_nt_var['RFTIMP'],2) ."</td>";
        echo "<td class = number>".n($row_imp_nt_var['NR_DOC_VAR'],0) ."</td>";
        echo "<td class = number>".n($rftimp_forn,2) ."</td>";
        echo "<td class = number>".n($ddt_nume,0) ."</td>";
        echo "<td class = number>".n($rg_ddt_nume, 0) ."</td>";
        echo "<td class = number>".n($rg_ddt_medi, 0) ."</td>";
        echo "<td class = number>".n($rftimp_rg_perc,2) ." %</td>";
        echo "<td class = number>" . implode(",", $ar_utenti) ."</td>";
        $output .= ob_get_contents();
        ob_end_clean();
        
}
    //*** Record di totali ***//
    
if($row_gen['TFAADO'] != 0)
    $tot_gen_tftimp = $row_gen['TOT_TFTIMP'];
    
    echo "<tr class = liv_tot>";
    echo "<td> Totale generale</td>";
    echo "<td class = number>".n($row_gen['TOT_TFTIMP'],2)."</td>";
    echo "<td>&nbsp;</td>";
    echo "<td class = number>".n($tftimp_tot,0) ."</td>";
    echo "<td class = number>".n($row_gen['TOT_SCOST'],2)."</td>";
    echo "<td>&nbsp;</td>";
    echo "<td class = number>".n($scost_tot,0) ."</td>";

    echo "<td class = number>".n($row_gen['TOT_TFVVAR'],2)."</td>";
    echo "<td class = number>".n($variaz_tot,0) ."</td>";
    echo "<td class = number>".n($row_gen_nt['TOT_RFTIMP'],2) ."</td>";
    echo "<td class = number>".n($nt_var_tot,0) ."</td>";
    echo "<td class = number>".n($row_gen['TOT_RFTIMP'],2)."</td>";
    echo "<td class = number>".n($row_gen['TOT_NR_DDT'],0) ."</td>";
    echo "<td class = number>".n($row_gen['TOT_NR_RG_DDT'],0) ."</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
    echo "<td>&nbsp;</td>";
}

echo $output;



