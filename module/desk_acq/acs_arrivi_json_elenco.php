<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();

$raw_post_data = acs_raw_post_data();


if (strlen($raw_post_data) > 0)
{	
	$m_params = acs_m_params_json_decode();
	$itin 		= $m_params->itin;
	$da_data 	= $m_params->da_data;
	$col_name	= $m_params->col_name;
	$cliente	= $m_params->cliente;
	$from_type	= $m_params->from_type;
	$data		= $m_params->data;
} else {

	//filtro a monte (in base a dove ho fatto click)
	$sottostringhe = explode("|", $_REQUEST["m_id"]);
	
	$itin 		= $_REQUEST['itin'];
	$cliente 	= $_REQUEST['cliente'];
	$da_data 	= $_REQUEST['da_data'];
	$col_name	= $_REQUEST['col_name'];
	$from_type	= $_REQUEST['from_type'];
	$data		= $_REQUEST['data'];	
}

if (trim($itin) != '')
	$desc_itin = $main_module->decod_std('ITIN', $itin);
else $desc_itin = '';

$filtro = array();
$filtro["itinerario"] 	= $itin;
$filtro["cliente"] 		= $cliente;

$titolo_panel = "Elenco ";
$tipo_elenco = "LIST";

if ($from_type == 'INDICI'){
	//singolo giorno da indici	
	$filtro["data_spedizione"] 		= $data;
	$filtro["a_data_spedizione"] 	= $data;
	$tipo_elenco = "DAY";		
	$m_data_per_panel = date('d/m', strtotime($data));	

} elseif ($from_type == 'MAIN_SEARCH') { //INFO
	
	$filtro["cliente"] = $_REQUEST['f_cliente_cod'];
	
	if (strlen($_REQUEST['f_destinazione_cod']) > 0)
		$filtro["destinazione"] = $_REQUEST['f_destinazione_cod'];
	
	$filtro["riferimento"] = $_REQUEST['f_riferimento'];
	
	//data programmata
	if (strlen($_REQUEST['f_data_dal']) > 0)
		$filtro["data_dal"] = date('Ymd', strtotime($_REQUEST['f_data_dal']));
	if (strlen($_REQUEST['f_data_al']) > 0)
		$filtro["data_al"] = date('Ymd', strtotime($_REQUEST['f_data_al']));
	
	//data spedizione
	if (strlen($_REQUEST['f_data_sped_dal']) > 0)
		$filtro["data_sped_dal"] = date('Ymd', strtotime($_REQUEST['f_data_sped_dal']));
	if (strlen($_REQUEST['f_data_sped_al']) > 0)
		$filtro["data_sped_al"] = date('Ymd', strtotime($_REQUEST['f_data_sped_al']));
	
	
	//data rilascio
	if (strlen($_REQUEST['f_data_ril_dal']) > 0)
		$filtro["data_ril_dal"] = date('Ymd', strtotime($_REQUEST['f_data_ril_dal']));
	if (strlen($_REQUEST['f_data_ril_al']) > 0)
		$filtro["data_ril_al"] = date('Ymd', strtotime($_REQUEST['f_data_ril_al']));
	
	//data ricezione	
	if (strlen($_REQUEST['f_data_ricezione_dal']) > 0)
		$filtro["data_ricezione_dal"] = date('Ymd', strtotime($_REQUEST['f_data_ricezione_dal']));
	if (strlen($_REQUEST['f_data_ricezione_al']) > 0)
		$filtro["data_ricezione_al"] = date('Ymd', strtotime($_REQUEST['f_data_ricezione_al']));	
	
	if (strlen($_REQUEST['f_data_conferma']) > 0)
		$filtro["data_conferma"] = date('Ymd', strtotime($_REQUEST['f_data_conferma']));
	
	$filtro['carico_assegnato'] = $_REQUEST['f_carico_assegnato'];
	$filtro['lotto_assegnato'] 	= $_REQUEST['f_lotto_assegnato'];
	
	$filtro['collo_disp'] = $_REQUEST['f_collo_disp'];
	$filtro['collo_sped'] = $_REQUEST['f_collo_sped'];
	
	$filtro['anomalie_evasione']= $_REQUEST['f_anomalie_evasione'];
	$filtro['ordini_evasi'] 	= $_REQUEST['f_ordini_evasi'];
	$filtro['divisione'] 		= $_REQUEST['f_divisione'];
	$filtro['num_ordine'] 		= $_REQUEST['f_num_ordine'];
	$filtro['num_carico'] 		= $_REQUEST['f_num_carico'];
	$filtro['num_lotto'] 		= $_REQUEST['f_num_lotto'];
	$filtro['itinerario'] 		= $_REQUEST['f_itinerario'];
	$filtro['tipo_ordine'] 		= $_REQUEST['f_tipo_ordine'];
	$filtro['stato_ordine'] 	= $_REQUEST['f_stato_ordine'];	
	$filtro['priorita'] 		= $_REQUEST['f_priorita'];
	$filtro['solo_bloccati']	= $_REQUEST['f_solo_bloccati'];
	
	$tipo_elenco = 'INFO';
	
	
	
} else { 	//selezione da PLAN
	if (strlen($da_data)==8){		
		if (substr($col_name, 0, 2)=="d_")
		{ //ho selezionato un giorno		
			$add_day = (int)substr($col_name, 2, 2) - 1;
			$m_data = date('Ymd', strtotime($da_data . " +{$add_day} days"));
			$m_data_per_panel = date('d/m', strtotime($da_data . " +{$add_day} days"));
			$m_data_to = $m_data; //solo un giorno
			$tipo_elenco = "DAY";
		}
		elseif (trim($col_name)=='fl_da_prog')
		{ //ordini non programmabili (TDSWSP=='N') ... non filtro per data
			$filtro["TDSWSP"] = 'N';
		}
		else
		{ //ho selezionato un'icona... non un giorno in particolare
			$m_data = date('Ymd', strtotime($da_data . " +0 days"));
			$m_data_to = date('Ymd', strtotime($da_data . " +14 days"));
		}
			
		if (!isSet($filtro["TDSWSP"]))
			$filtro["TDSWSP"] = "Y";
		$filtro["data_spedizione"] = $m_data;
		$filtro["a_data_spedizione"] = $m_data_to;
	}
}

if (strlen($filtro["cliente"]) > 0)
	$tipo_elenco = "LIST";

if (trim($col_name)=='fl_da_prog')
	$tipo_elenco = "HOLD";

$titolo_panel = $main_module->get_titolo_panel_by_tipo($tipo_elenco);
$titolo_panel .= " " . implode(' ', array($itin, $cliente, $m_data_per_panel));


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
	$items 	= $main_module->get_elenco_ordini($filtro, $_REQUEST['ordina_per_tipo_ord']);
	$ars_ar = $main_module->crea_array_valori_el_ordini($items, $tipo_elenco, $_REQUEST['ordina_per_tipo_ord']);
	$ars 	= $main_module->crea_alberatura_el_ordini_json($ars_ar, $_REQUEST["node"], $filtro["itinerario"], $titolo_panel, $tipo_elenco, $_REQUEST['ordina_per_tipo_ord'], $m_data);
	echo $ars;
	
	$appLog->save_db();
	
	
	
	exit;
}



/*********************************************************************************************************
 * PANEL GRID
 **********************************************************************************************************/

$m_params = acs_m_params_json_decode();
$itin 		= $m_params->itin;
$da_data 	= $m_params->da_data;
$col_name	= $m_params->col_name;

?>

{"success":true, "items":
 {
  xtype: 'treepanel',
  title: <?php echo j($titolo_panel); ?>,
  closable: true,
  collapsible: true,    
  rootVisible: false,
  cod_iti: "",
  loadMask: true,
  
  defaultSortable: false,
  enableSort: false, // disable sorting  
  
  multiSelect: false,
  singleExpand: false,	
  cls: 'elenco_ordini',
  useArrows: true,  
  
  	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: false,                    

                    model: 'ModelOrdini',
				    
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
                        reader: {
                            root: 'children'
                        },
                    extraParams: <?php echo acs_je($m_params) ?>,
                    
					listeners: {
					
							beforeload: function(store, options) {
				        		console.log('ccccccccccc');
				        		},
				        							
				            load: function () {
				             //PERCHE' NON FUNZIONA????
				             console.log('abcde');
				             // this.up('treepanel').setTitle(this.proxy.reader.jsonData.titolo_panel);
				             }
					
					}                    
                    
                    
                    }
                }),
                
            columns: [	
    		{text: '<?php echo $desc_itin; ?>', 		xtype: 'treecolumn', width: 250, dataIndex: 'task',
          	  renderer: function (value, metaData, record, row, col, store, gridView){						
  				if (record.get('liv') == 'liv_3') metaData.tdCls += ' auto-height';											
  				return value;			    
				}},

    	    {text: '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	     		tooltip: 'Ordini bloccati',
    			    renderer: function(value, p, record){
    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=18>';			    	
    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';
    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=18>';			    				    	
    			    	}},		
    	    {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=25>',	width: 30, tdCls: 'tdAction', 
    	    		dataIndex: 'art_mancanti',
    	     		tooltip: 'Articoli sospesi',    	    			    
    			    renderer: function(value, p, record){
    			    	if (record.get('liv') != 'liv_1'){
	    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
	    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=18>';			    	
	    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=18>';			    	
	    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=18>';
	    			    }				    	
    			    }},			    	
    	    {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
        			dataIndex: 'art_da_prog',	
    	     		tooltip: 'Articoli in esaurimento',        				    	     
    			    renderer: function(value, p, record){
    			    	if (record.get('liv') != 'liv_1'){    			    
    			    		if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
    			    	}	
    			    }},			    	
    	    {text: '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	     		tooltip: 'Ordine recente',    	     
    			    renderer: function(value, p, record){
    			    	if (record.get('ind_modif').trim() == 'A') 	return '<img src=<?php echo img_path("icone/48x48/sub_blue_add.png") ?> width=18>';			    	
    			    	if (record.get('ind_modif').trim() == 'R') 	return '<img src=<?php echo img_path("icone/48x48/sub_grey_add.png") ?> width=18>';
    			    	if (record.get('fl_new')==1) 				return '<img src=<?php echo img_path("icone/48x48/label_blue_new.png") ?> width=18>';			    				    	
    			    	if (record.get('fl_new')==2) 				return '<img src=<?php echo img_path("icone/48x48/mano_ko.png") ?> width=18>';			    	
    			    	if (record.get('fl_new')==3) 				return '<img src=<?php echo img_path("icone/48x48/timer_blue.png") ?> width=18>';    			    	
    			    	if (record.get('fl_new')==4) 				return '<img src=<?php echo img_path("icone/48x48/design.png") ?> width=18>';    			    	
    			    	}},			    		
    		{text: 'Seq', 			width: 33, dataIndex: 'seq_carico'},
    	    {text: 'Localit&agrave;/Riferimento', 	width: 150, dataIndex: 'riferimento'},
    	    {text: 'Tp', 			width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd', 
	    	    	renderer: function (value, metaData, record, row, col, store, gridView){						
						metaData.tdCls += ' ' + record.get('raggr');										
					return value;			    
					}},
    	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', renderer: date_from_AS},
    	    {text: 'St', 			width: 30, dataIndex: 'stato'},	    	    
    	    {text: 'Pr', 			width: 35, dataIndex: 'priorita', tdCls: 'tpPri',
    				renderer: function (value, metaData, record, row, col, store, gridView){
    					if (record.get('tp_pri') == 4)
    						metaData.tdCls += ' tpSfondoRosa';

    					if (record.get('tp_pri') == 6)
    						metaData.tdCls += ' tpSfondoCelesteEl';					
    					
    					return value;
    				    
    	    			}},
    		{text: 'Evasione<br>Richiesta',	width: 60, dataIndex: 'cons_rich', renderer: date_from_AS},	    	    	    	       
    	    {text: 'Pezzi',			width: 45, dataIndex: 'referenze', align: 'right'},    	    
    	    {text: '<img src=<?php echo img_path("icone/48x48/star_full_green.png") ?> width=25>',	width: 30, tdCls: 'tdAction',
    	     		tooltip: 'Avanzamento prebolle',    	    
    			    renderer: function(value, p, record){
						if (parseInt(record.get('fl_ref')) == 1) return '<img src=<?php echo img_path("icone/48x48/star_full_green.png") ?> width=18>';    			    
    			    	if (parseInt(record.get('fl_ref')) == 3) return '<img src=<?php echo img_path("icone/48x48/star_none.png") ?> width=18>';
    			    	return '';
    			    	}},    	    
    	    
    	    {text: 'Preb',			width: 45, dataIndex: 'referenze_cons', align: 'right'},    	    	        			    	
    	    {text: 'Volume',			width: 50, dataIndex: 'volume', align: 'right',
    			    renderer: function(value, p, record){
    			    	if (record.get('liv') == 'liv_0'){ //spedizione
        			    	ret = record.get('volume');
        			    	if (parseFloat(record.get('volume_disp')) > 0)
        			    		ret += '<br><p class="sottoriga-liv_0">[' + record.get('volume_disp') + ']</p>';
    			    		return ret;
    			    	}	
    			    	else
    			    		return value;	
    			    }	    
    	    },
    	    {text: 'Colli',			width: 35, dataIndex: 'colli', align: 'right'},    	    	    
    		{text: 'Peso',				flex: 55, dataIndex: 'peso', align: 'right',
    			    renderer: function(value, p, record){
    			    	if (record.get('liv') == 'liv_0'){ //spedizione
    			    		ret = record.get('peso');
        			    	if (parseFloat(record.get('peso_disp')) > 0)
        			    		ret += '<br><p class="sottoriga-liv_0">[' + record.get('peso_disp') + ']</p>';
    			    		return ret;
    			    		
    			    	}	
    			    	else
    			    		return value;	
    			    }		
    		},
    		{text: 'Importo',			flex: 55, dataIndex: 'importo', align: 'right'},
    		
<?php if ($tipo_elenco == "INFO"){ ?>
    	    {text: 'Evas.<br>Progr.', 	width: 60, dataIndex: 'cons_prog', renderer: date_from_AS},
<?php } ?>    		
    		
    		
    		{text: 'Evas.<br>Conf.',	flex: 50, dataIndex: 'cons_conf', renderer: function (value, metaData, record, row, col, store, gridView){ 
    				if (store.proxy.reader.jsonData.tipo_elenco == 'HOLD'){
    					gridView.headerCt.getHeaderAtIndex(col).setText('Evas.<br>Corrente');
    					return date_from_AS(record.get('cons_prog'), metaData, record);
    				} else {					
    					gridView.headerCt.getHeaderAtIndex(col).setText('Evas.<br>Conf.');					
    					return date_from_AS(record.get('cons_conf'), metaData, record);
    				}			
    			}
    		},		
    		{text: 'Dispon.',			flex: 50, dataIndex: 'data_disp', renderer: function (value, metaData, record, row, col, store, gridView){
    						
    			if (record.get('liv') == 'liv_2' || record.get('liv') == 'liv_3'){
    				if (record.get('fl_data_disp') == '1')
    					metaData.tdCls += ' tpSfondoRosa';
    			}	 		 
    				return date_from_AS(value, metaData, record);
    			}
    		}	
    	]    

		, viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {
		        	v_o = record.get('liv');			        	
		        	v = record.get('liv');
		        	
		        	if (v_o=="liv_1" && record.get('n_carico')==0)
		        		v = v + ' no_carico'; 
		        	
		        	v = v + ' ' + record.get('tp_pri');												        	
		        	
		        	if (v_o=="liv_3"){
		        		v = v + ' ' + record.get('raggr');												        		
		        	} //ordini
		           return v;																
		         }   
		    }												    




    , listeners: {
    
	      celldblclick: {										      		
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
				  	rec = iView.getRecord(iRowEl);
				  	
					if (col_name=='referenze' && rec.get('liv')=='liv_3'){
						iEvent.preventDefault();						
						show_win_righe_ord(rec, rec.get('k_ordine'));
						return false;
					}			  	
				}
	    	}	
    		
    
		  ,itemcontextmenu : function(grid, rec, node, index, event) {
				this.showMenu_liv(grid, rec, node, index, event);
				return false;
		   }
	    	
    
    
    
	      ,itemclick: function(view,rec,item,index,eventObj) {
	          if (rec.get('liv') == 'liv_3')
	          {	
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');	        	        	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdi.store.load();
	        	wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdc.store.load();				
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
				      Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);
				   }
				});
			   }												        	
	         }    		
    		
		
 		, afterrender: function (comp) { 			
 				//attivo il tab qui creato
 				comp.up('tabpanel').setActiveTab(comp.id);
 				}
 	} //listeners
   
   , showMenu_liv: function(grid, rec, node, index, event){
		  event.stopEvent();
	      var record = grid.getStore().getAt(index);	      
	      var voci_menu = [];
	      

	      if (( grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY'
	      		|| grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIST')
	      	   && rec.get('liv') == 'liv_0'){ //livello totali
	      	   
		      voci_menu.push({
	          		text: 'Sincronizza',
	        		iconCls : 'icon-refresh-16',	          		
	        		handler: function() {

	        	    	var id_selected = grid.getSelectionModel().getSelection(),	
				    		list_selected_id = [];
				    	for (var i=0; i<id_selected.length; i++)
				  		   list_selected_id.push(id_selected[i].data);
			
				  		if (list_selected_id.length > 1){
				  			acs_show_msg_error('Selezionare una singola ricezione');
				  			return false;			
				  		}
	        		
	
	       			 Ext.Ajax.request({
	      			   url: 'acs_op_exe.php?fn=sincronizza_generica',
	      			   method: 'POST',
	      			   jsonData: {	record: rec.data,
									extraParams: grid.store.treeStore.proxy.extraParams,	      			   
	      			   				tipo_elenco: grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco
	      			   },	 

	      			   success: function(response, opts) {
	      			   	grid.store.treeStore.load();
	      			   }
	      			});

	        			
	        		}
	    		});	 
	      }	      
	      
	      voci_menu.push({
          		text: 'Riepilogo consegna forniture',
        		iconCls : 'icon-leaf-16',	          		
        		handler: function() {

        	    	var id_selected = grid.getSelectionModel().getSelection(),	        			
			    		list_selected_id = [];
			    	for (var i=0; i<id_selected.length; i++)
			  		   list_selected_id.push(id_selected[i].data);
		             
		              acs_show_win_std('Dettagli report', 'acs_report_info.php?fn=open_form', {
        				filter : <?php echo $raw_post_data; ?>
    					}, 200, 150, {}, 'icon-leaf-16');	
        			
        		}
    	  });	
	      
	      
	      if (( grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'DAY'
	      		|| grid.getStore().treeStore.proxy.reader.jsonData.tipo_elenco == 'LIST')
	      	   && rec.get('liv') == 'liv_1'){ //livello ricezione
		      voci_menu.push({
	          		text: 'Sincronizza',
	        		iconCls : 'icon-refresh-16',	          		
	        		handler: function() {

	        	    	id_selected = grid.getSelectionModel().getSelection();	        			
				    	list_selected_id = [];
				    	for (var i=0; i<id_selected.length; i++)
				  		   list_selected_id.push(id_selected[i].data);
			
				  		if (list_selected_id.length > 1){
				  			acs_show_msg_error('Selezionare una singola ricezione');
				  			return false;			
				  		}

	        			
	       			 Ext.Ajax.request({
	      			   url: 'acs_op_exe.php?fn=sincronizza_spedizione',
	      			   method: 'POST',
	      			   jsonData: {sped_id: list_selected_id[0]['sped_id']}, 

	      			   success: function(response, opts) {
	      			   	grid.store.treeStore.load();
	      			   }
	      			});

	        			
	        		}
	    		});	      
	      }

	      var menu = new Ext.menu.Menu({
	            items: voci_menu
				}).showAt(event.xy);	      
	         
   } //showMenu_liv
   
  }
}	
