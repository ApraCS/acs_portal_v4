<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();


//funcioni generiche
switch($_REQUEST['fn']) {
	case "toggle_fl_solo_con_carico_assegnato":
		$main_module->toggle_fl_solo_con_carico_assegnato();
		$_REQUEST['fn'] = 'get_json_data';		
		break;
}


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
	/* CAMPO DI CUI MOSTRARE IL DETTAGLIO */
	$main_module->set_calendar_field_dett($_REQUEST['set_field']); //se mi chiede di cambiare campo di dettaglio
	$field_dett = $main_module->imposta_field_dett(); //recupero il campo di dettaglio
	
	/* DATE CALENDARIO */
	$n_giorni 	= 14;
	$main_module->set_initial_calendar_date($_REQUEST['move']); //se si muove sul calendario
	$da_data_txt = $main_module->get_initial_calendar_date(); //recupero data iniziale calendario
	$a_data_txt = date('Y-m-d', strtotime($da_data_txt . " +{$n_giorni} days"));
	$da_data 	= new DateTime($da_data_txt);
	$a_data 	= new DateTime($a_data_txt);
		
	$root_id = $_REQUEST["node"]; //radice o sottonodo?
 	
	$its = $main_module->get_dati_plan($da_data, $n_giorni, $root_id);
	$appLog->add_bp('get_dati_plan - END');
	
	$ars_ar = $main_module->crea_array_valori_plan($its, $field_dett, $root_id);
	$appLog->add_bp('crea_array_valori - END');
	
	//Aggiungo le richiesta MTO
	if ($cfg_mod_DeskAcq['plan_con_richieste_MTO'] != 'N')
		$ars_ar['det'] = $main_module->crea_array_valori_plan_richieste_MTO($ars_ar['det']);		
	
	$ars = $main_module->crea_json_dati_plan($ars_ar, $root_id, $da_data_txt, $field_dett);
	$appLog->add_bp('crea_alberatura - END');
	
	//output del risultato
	echo $ars;
	
	$appLog->save_db();
	
	
	exit;
}


?>

{"success":true, "items":
 {
  xtype: 'treepanel',
  id: 'panel-arrivi',
		selType: 'cellmodel',
////		cls: 'tree-calendario',
		cls: 'supply_desk',						
        title: 'Plan',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        da_data: 0,
        
		store: Ext.create('Ext.data.TreeStore', {
	        model: 'RowCalendar',
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
	        },
	        
	        reader: new Ext.data.JsonReader(),
	        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        closable: true,
        
		listeners: {
		
	 			afterrender: function (comp) { 			
	 				//comp.getStore().load();

	 				
						// attacco gli eventi e tooltip
						if (Ext.get("cal_fl_solo_con_carico_assegnato") != null){		
					        Ext.get("cal_fl_solo_con_carico_assegnato").on('click', function(){
					        	comp.getStore().proxy.url= '<?php echo $_SERVER['PHP_SELF']; ?>?fn=toggle_fl_solo_con_carico_assegnato';
								comp.getStore().load();;
					        	comp.getStore().proxy.url= '<?php echo $_SERVER['PHP_SELF']; ?>';
					        });
					        Ext.QuickTips.register({target: "cal_fl_solo_con_carico_assegnato",	text: 'Attiva/Disattiva solo assegnati'});	        
						}
	 				
	 				
	 					url_tmp = comp.getStore().proxy.url;
	 				
						// attacco gli eventi
				        Ext.get("cal_next_1d").on('click', function(){											        
				        	comp.getStore().proxy.url= url_tmp + '&move=next_1d';
							comp.getStore().load();
				        	comp.getStore().proxy.url= url_tmp;			
				        });	        
				        Ext.get("cal_next_1w").on('click', function(){
				        	comp.getStore().proxy.url= url_tmp + '&move=next_1w';
							comp.getStore().load();
				        	comp.getStore().proxy.url= url_tmp;			
				        });
				        Ext.get("cal_prev_1d").on('click', function(){
				        	comp.getStore().proxy.url= url_tmp + '&move=prev_1d';
							comp.getStore().load();
				        	comp.getStore().proxy.url= url_tmp;			
				        });
				        Ext.get("cal_prev_1w").on('click', function(){
				        	comp.getStore().proxy.url= url_tmp + '&move=prev_1w';
							comp.getStore().load();
				        	comp.getStore().proxy.url= url_tmp;			
				        });	 			
				        
				    	//tooltip
				        if (Ext.get("cal_prev_1w") != null)	Ext.QuickTips.register({target: "cal_prev_1w",	text: 'Settimana indietro'});
				        if (Ext.get("cal_prev_1d") != null)	Ext.QuickTips.register({target: "cal_prev_1d",	text: 'Giorno indietro'});        
				        if (Ext.get("cal_next_1d") != null)	Ext.QuickTips.register({target: "cal_next_1d",	text: 'Giorno avanti'});        
				        if (Ext.get("cal_next_1w") != null)	Ext.QuickTips.register({target: "cal_next_1w",	text: 'Settimana avanti'});        
				        
				        	
	 				
	 				}, 		
		
				load: function(sender, node, records) {
						var c = Ext.getCmp('panel-arrivi');
												 								
			        	Ext.getBody().unmask();
			        	
						if (c.store.proxy.reader.jsonData.toggle_fl_solo_con_carico_assegnato == 1)
							Ext.get('cal_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_attivo.png"); ?>;
						else
							Ext.get('cal_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_disattivo.png"); ?>;																					        	
			        	
						c.da_data =	c.store.proxy.reader.jsonData.parameters.da_data;		        	
			        	
						c.headerCt.getHeaderAtIndex(7).setText(c.store.proxy.reader.jsonData.columns.c1);						        	
						c.headerCt.getHeaderAtIndex(8).setText(c.store.proxy.reader.jsonData.columns.c2);									
						c.headerCt.getHeaderAtIndex(9).setText(c.store.proxy.reader.jsonData.columns.c3);						        	
						c.headerCt.getHeaderAtIndex(10).setText(c.store.proxy.reader.jsonData.columns.c4);									
						c.headerCt.getHeaderAtIndex(11).setText(c.store.proxy.reader.jsonData.columns.c5);									
						c.headerCt.getHeaderAtIndex(12).setText(c.store.proxy.reader.jsonData.columns.c6);									
						c.headerCt.getHeaderAtIndex(13).setText(c.store.proxy.reader.jsonData.columns.c7);									
						c.headerCt.getHeaderAtIndex(14).setText(c.store.proxy.reader.jsonData.columns.c8);						        	
						c.headerCt.getHeaderAtIndex(15).setText(c.store.proxy.reader.jsonData.columns.c9);									
						c.headerCt.getHeaderAtIndex(16).setText(c.store.proxy.reader.jsonData.columns.c10);						        	
						c.headerCt.getHeaderAtIndex(17).setText(c.store.proxy.reader.jsonData.columns.c11);									
						c.headerCt.getHeaderAtIndex(18).setText(c.store.proxy.reader.jsonData.columns.c12);									
						c.headerCt.getHeaderAtIndex(19).setText(c.store.proxy.reader.jsonData.columns.c13);									
						c.headerCt.getHeaderAtIndex(20).setText(c.store.proxy.reader.jsonData.columns.c14);									
				},

					
				celldblclick: {								

					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  //fn: function(view,rec,item,index,eventObj) {
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
						
						if (col_name=='task' && rec.get('liv')=='liv_totale'){
						
							url_tmp = iView.initialConfig.grid.store.proxy.url;
							iView.initialConfig.grid.store.proxy.url= url_tmp + '&set_field=' + rec.get('id');
							iView.initialConfig.grid.store.load();
				        	iView.initialConfig.grid.store.proxy.url= url_tmp;										
						}
						

						if (col_name!='task' && rec.get('liv')=='liv_totale'){										
							window.open ('acs_print_riepilogo_programmate.php?da_data=' +rec.get('da_data') + '&col_name=' + col_name,"mywindow"); 										
						}
						
						if (col_name=='ha_contratti' && rec.get('liv')=='liv_3'){										
							acs_show_win_std('Contratti forniture programmate residue - ' + rec.get('task'), 'acs_json_contratti_for.php', rec.data, 800, 400, null, 'icon-blog-16');
							return false; 										
						}

						
						if (col_name=='ha_proposte_MTO' && rec.get('liv')!='liv_3') return false;						
						if (col_name=='ha_proposte_MTO' && rec.get('liv')=='liv_3'){										
							////acs_show_win_std('Proposte MTO - ' + rec.get('task'), 'acs_json_proposte_MTO_for.php', rec.data, 980, 460, null, 'icon-button_black_play-16');
							
							mp = Ext.getCmp('m-panel');
							
					    		//carico la form dal json ricevuto da php
					    		Ext.Ajax.request({
					    		        url        : 'acs_json_proposte_MTO_for.php',
				        				method     : 'POST',
					    		        waitMsg    : 'Data loading',
					    		        jsonData:  rec.data ,
					    		        success : function(result, request){
					    		            var jsonData = Ext.decode(result.responseText);
					    		            mp.add(jsonData.items);
					    		            mp.doLayout();
					    		            mp.show();
					    		            
					    		            //mostro l'ultimo tab
											mp.setActiveTab(mp.items.length -1)
					    		            
					    		        },
					    		        failure    : function(result, request){
					    		            Ext.Msg.alert('Message', 'No data to be loaded');
					    		        }
					    		    });
							
							
							
							return false; 										
						}						
				    	
				    	if (col_name!='task' && (rec.get('liv')=='liv_2' || rec.get('liv')=='liv_3')){							    							    									    		
							mp = Ext.getCmp('m-panel');
							
					    		//carico la form dal json ricevuto da php
					    		Ext.Ajax.request({
					    		        url        : 'acs_arrivi_json_elenco.php',
				        				method     : 'POST',
					    		        waitMsg    : 'Data loading',
					    		        jsonData: { col_name: col_name, 
					    		        			da_data: iView.initialConfig.grid.da_data,
					    		        			itin: rec.get('itin'),
					    		        			cliente: rec.get('k_cliente')},
					    		        success : function(result, request){
					    		            var jsonData = Ext.decode(result.responseText);
					    		            mp.add(jsonData.items);
					    		            mp.doLayout();
					    		            mp.show();
					    		        },
					    		        failure    : function(result, request){
					    		            Ext.Msg.alert('Message', 'No data to be loaded');
					    		        }
					    		    });

						  return false; //previene expand/collapse di default
				    	}
				    	
				    }
				}						
		   },

		viewConfig: {
		        //row class CALENDARIO
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
			        	if (v=="liv_totale"){
			        								        						        	
							if ('tot_' + this.initialConfig.grid.store.proxy.reader.jsonData.parameters.field_dett == record.get('id'))
				        		v = v + ' dett_selezionato';												        		
			        	} //la riga di totale selezionata					        	
		            return v;					            
		         	}   
		    },			


		<?php
				$ss = "";
				// $ss .= "<img src=" . img_path("icone/48x48/button_grey_repeat.png") . " height=30>";		
				$ss .= "<img id=\"cal_prev_1w\"src=" . img_path("icone/48x48/button_grey_rew.png") . " height=30>";
				$ss .= "<img id=\"cal_prev_1d\"src=" . img_path("icone/48x48/button_grey_first.png") . " height=30>";						
				$ss .= "<img id=\"cal_next_1d\" src=" . img_path("icone/48x48/button_grey_last.png") . " height=30>";		
				$ss .= "<img id=\"cal_next_1w\" src=" . img_path("icone/48x48/button_grey_ffw.png") . " height=30>";
				$ss .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				
				//FILTRO PER SOLO ORDINI CON CARICO ASSEGNATO
				$img_name = ($main_module->get_fl_solo_con_carico_assegnato() == 1) ? "filtro_attivo" : "filtro_disattivo";
				$ss .= "<span id=\"cal_fl_solo_con_carico_assegnato\" class=acs_button style=\"\">";
				$ss .= "<img id=\"cal_fl_solo_con_carico_assegnato_img\" src=" . img_path("icone/48x48/{$img_name}.png") . " height=30>";
				//$ss .="<span id=\"cal_fl_solo_con_carico_assegnato_lbl\">Solo con carico assegnato</span>";
				$ss .= "</span>";
				
				
				
				//img per intestazioni colonne flag
				$cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>";
				$cf2 = "<img src=" . img_path("icone/48x48/save.png") . " height=25>";
				$cf3 = "<img src=" . img_path("icone/48x48/warning_black.png") . " height=25>";
				$cf4 = "<img src=" . img_path("icone/48x48/button_blue_pause.png") . " height=25>";																					
				$cf5 = "<img src=" . img_path("icone/48x48/button_black_play.png") . " height=25>";
				$cf6 = "<img src=" . img_path("icone/48x48/blog.png") . " height=25>";								
		?>		


        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 25,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>'
        },{
			text: '<?php echo $cf1; ?>',
			tooltip: 'Fornitori critici',
			width: 30,
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
		  },{
			    text: '<?php echo $cf6; ?>',
            	dataIndex: 'ha_contratti',
				tooltip: 'Contratti',            				    
			    width: 30,
		    	tdCls: 'tdAction',         			
            	menuDisabled: true, sortable: false,            		        
				renderer: function(value, p, record){if (parseInt(record.get('ha_contratti'))==1) return '<img src=<?php echo img_path("icone/48x48/blog.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf5; ?>',
			    dataIndex: 'ha_proposte_MTO',
				tooltip: 'Proposte MTO',			    
			    width: 30,
		    	tdCls: 'tdAction',         			
            	menuDisabled: true, sortable: false,            		        
				renderer: function(value, p, record){
					if (record.get('ha_proposte_MTO')=='0') return '<img src=<?php echo img_path("icone/48x48/button_red_play.png") ?> width=18>';									
					if (record.get('ha_proposte_MTO')=='1') return '<img src=<?php echo img_path("icone/48x48/button_yellow_play.png") ?> width=18>';
					if (record.get('ha_proposte_MTO')=='2') return '<img src=<?php echo img_path("icone/48x48/button_black_play.png") ?> width=18>';															
					if (record.get('ha_proposte_MTO')=='9') return '<img src=<?php echo img_path("icone/48x48/button_grey_play.png") ?> width=18>';
				}         			    
		  },{
			    text: '<?php echo $cf4; ?>',
            	dataIndex: 'fl_da_prog',         			    
				tooltip: 'Ordini da confermare',            	
			    width: 30,
		    	tdCls: 'tdAction',         			
            	menuDisabled: true, sortable: false,            		        
				renderer: function(value, p, record){if (record.get('fl_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf2; ?>',
			    width: 30,
				tooltip: 'EDI',			    
		    	tdCls: 'tdAction',         			
            	menuDisabled: true, sortable: false,            		        
				renderer: function(value, p, record){if (record.get('fl_ord_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/save.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf3; ?>',
			    width: 30,
				tooltip: 'Articoli sospesi/in esaurimento',			    
		    	tdCls: 'tdAction',         			
            	menuDisabled: true, sortable: false,            		        
				renderer: function(value, p, record){if (record.get('fl_art_mancanti')==1) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';}         			    
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_1',
		   align: 'right',
		   id: "col_cal_d_1",
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_2',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_3',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_4',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_5',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_6',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_7',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		   
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_8',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_9',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_10',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_11',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_12',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_13',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		               		   
		  },{
			   text: '&nbsp;<br>&nbsp;',
		   width: 50,
		   dataIndex: 'd_14',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		   
		  }        			          			  
        ]   
  
 }
}