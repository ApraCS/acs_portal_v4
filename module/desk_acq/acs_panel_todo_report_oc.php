<?php
require_once "../../config.inc.php";

$main_module = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'open_form'){?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          
                 {
				     name: 'f_data_in'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data fattura mittente iniziale'
				   , labelAlign: 'left'
				   , labelWidth: 160
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 		 }
			}, {
				     name: 'f_data_fin'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data fattura mittente finale'
				   , labelAlign: 'left'
				   , labelWidth: 160
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 		 }
			   }
	            ],
	            
				buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues())
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	}

if ($_REQUEST['fn'] == 'open_report'){

// ******************************************************************************************
// REPORT
// ******************************************************************************************


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv_tot td{ background-color: #DDDDDD; font-weight: bold;} 
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$sql_dataora= "Select * from {$cfg_mod_DeskAcq['file_richieste_esito']}
               WHERE OCFG01 = 'F' order by OCTIME desc FETCH FIRST 1 ROWS ONLY";

$stmt_dataora = db2_prepare($conn, $sql_dataora);
$result_dataora = db2_execute($stmt_dataora);
$r_dataora = db2_fetch_assoc($stmt_dataora);
$data_ora = print_date($r_dataora['OCDTRI']) . " " . print_ora($r_dataora['OCHMRI']);

echo "<div id='my_content'>";
echo "<div class=header_page>";

echo "<H2>Riepilogo controllo fatturazione costo confermato {$r['TFDCON']}</H2>";

echo"</div>
<div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";

echo "<table class=int1>";

echo "<tr class='liv_data'>";
echo "<th>Anno/Mese/Fattura</th>";

echo "<th>Data</th>";
/*echo "<th><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=20></th>
  	  <th><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=20></th>
  	  <th><img src=" . img_path("icone/48x48/info_gray.png") . " height=20></th>";*/
echo "<th>Imponibile</th>";
echo "<th>Ordine fornitore</th>";
echo "<th>Imponbile ordine for.</th>";
echo "<th>Costo confermato</th>";
echo "<th>Ordine vendita</th>";
echo "<th>ToDo Controllo costo confermato</th>";
echo "<th>ToDo Richiesta modifica importo fornitura</th>";


$sql_where = "";
if(isset($form_values->f_data_in) && strlen($form_values->f_data_in) > 0)
    $sql_where .= " AND OCDTFV >= {$form_values->f_data_in}"; 
if(isset($form_values->f_data_fin) && strlen($form_values->f_data_fin) > 0)
    $sql_where .= " AND OCDTFV <= '{$form_values->f_data_fin}'"; 

$sql= " SELECT *
        FROM {$cfg_mod_DeskAcq['file_richieste_esito']} OC
        WHERE 1=1 /*OCDT = '{$id_ditta_default}' */ AND OCFG01 = 'F' {$sql_where}
";


$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);
while($row = db2_fetch_assoc($stmt)){
    $nr = array();
    $nr = $row;
    
    $ar[] = $nr;
}

$sql_td = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} TD
           WHERE TDOADO = ? AND TDONDO = ? AND TDOTPD = ?";

if(is_array($ar)){
     foreach ($ar as $kar => $r){
                
          
                $ord_forn =  $r['OCANRF']."_".sprintf("%06s", $r['OCNRRF'])."_".$r['OCTPRF'];
                $ord_ven  =  $r['OCANCO']."_".sprintf("%06s", $r['OCNRCO'])."_".$r['OCTPCO'];
                $ord_fatt =  $r['OCANFV']."_".sprintf("%06s", $r['OCNRFV'])."_".$r['OCTPFV'];
                $nrco = sprintf("%06s", $r['OCNRCO']);
                
                
                $stmt_td = db2_prepare($conn, $sql_td);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt_td, array($r['OCANCO'], $nrco, $r['OCTPCO']));
                echo db2_stmt_errormsg();
                $row_td = db2_fetch_assoc($stmt_td);
                
                echo "<tr>
                      <td>".$ord_fatt. "</td>
                      <td>".print_date($r['OCDTFV'])."</td>";
                
               //SONO FLAG DEL FILE RF0
              /*  if($r['p_red'] > 0){
                    echo "<td><img src=" . img_path("icone/48x48/puntina_rossa.png") . " height=15></td>";
                }else{
                    echo "<td>&nbsp;</td>";
                }
                
                if($r['s_black'] > 0){
                    echo "<td><img src=" . img_path("icone/48x48/sub_black_accept.png") . " height=15></td>";
                }else{
                    echo "<td>&nbsp;</td>";
                }
                if($r['p_zero'] > 0){
                    echo "<td><img src=" . img_path("icone/48x48/info_gray.png") . " height=15></td>";
                }else{
                    echo "<td>&nbsp;</td>";
                }
                */
                
                echo "<td class='number'>".n($r['OCFTIM'], 2)."</td>";
                // echo "<td>&nbsp;</td>";
                echo "<td>{$ord_forn}</td>";
                echo "<td class='number'>".n($r['OCTIMP'], 2)."</td>";
                echo "<td class='number'>".n($r['OCFTIM'], 2)."</td>";
                echo "<td>{$ord_ven}</td>";
                echo "<td>";
                if($r['OCANCO'] > 0){
                    $sql_todo = "SELECT ATT_OPEN.*, NT_MEMO.NTMEMO AS MEMO
                    FROM {$cfg_mod_Spedizioni['file_assegna_ord']} ATT_OPEN
                    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
                    ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
                    WHERE ATT_OPEN.ASDOCU = '{$row_td['TDDOCU']}' AND ATT_OPEN.ASCAAS = 'CK_CST_CF'
                    ";
                    
                    
                    
                    $stmt_todo = db2_prepare($conn, $sql_todo);
                    $result = db2_execute($stmt_todo);
                    
                    while($rt = db2_fetch_assoc($stmt_todo)){
                        $todo = print_date(trim($rt['ASDTAS'])).", ";
                        $todo .= trim($rt['ASNOTE']);
                        if(trim($rt['MEMO']) != '')
                            $todo .=  " " .trim($rt['MEMO']);
                            if(trim($rt['ASFLRI']) == 'Y'){
                                $causali_rilascio = $s->find_TA_std('RILAV', trim($rt['ASCAAS']), 'N', 'N', trim($rt['ASCARI']));
                                $d_rilascio =  $causali_rilascio[0]['text'];
                                $todo .= "<br>".print_date(trim($rt['ASDTRI'])).", [".trim($rt['ASCARI'])."] ".$d_rilascio.", ".trim($rt['ASNORI']);
                            }
                            echo $todo."<br><br>";
                    }
                }
                echo "</td>";
                echo "<td>";
                if($r['OCANCO'] > 0){
                    $sql_todo2 = "SELECT ATT_OPEN.*, NT_MEMO.NTMEMO AS MEMO
                    FROM {$cfg_mod_Spedizioni['file_assegna_ord']} ATT_OPEN
                    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
                    ON NT_MEMO.NTTPNO = 'ASMEM' AND INTEGER(NT_MEMO.NTKEY1) = ATT_OPEN.ASIDPR AND NT_MEMO.NTSEQU=0
                    WHERE ATT_OPEN.ASDOCU = '{$row_td['TDDOCU']}' AND ATT_OPEN.ASCAAS = 'RC_MOD_CF'
                    ";
                    
                    
                    $stmt_todo2 = db2_prepare($conn, $sql_todo2);
                    $result = db2_execute($stmt_todo2);
                    
                    while($rt2 = db2_fetch_assoc($stmt_todo2)){
                        $todo2 = print_date(trim($rt2['ASDTAS'])).", ";
                        $todo2 .= trim($rt2['ASNOTE']);
                        if(trim($rt2['MEMO']) != '')
                            $todo2 .=  " " .trim($rt2['MEMO']);
                            if(trim($rt2['ASFLRI']) == 'Y')
                                $todo2 .= " - Rilascio: ".print_date(trim($rt['ASDTRI'])).", ".trim($rt['ASCARI']).", ".trim($rt['ASNORI']);
                                echo $todo2."<br><br>";
                    }
                }
                
                echo "</td>";
                
                
                echo "</tr>";
                
            
        }
        
    }

}





