<?php

require_once "../../config.inc.php";

$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'exe_separa_ddt'){
    
    $m_params = acs_m_params_json_decode();
    $use_session_history = microtime(true);
    $list_righe = $m_params->righe;
    
    foreach ($list_righe as $v){
            
    $sh = new SpedHistory($m_DeskAcq);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DIV_RIG_DDT',
            "k_ordine"	=> $m_params->k_ordine,
            "use_session_history" =>  $use_session_history,
            "vals" => array(
                "RINREC" => $v->nrec,
               
            )
           
            
        )
        );
   }
        
        
        $sh = new SpedHistory($m_DeskAcq);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'DIV_RIG_DDT',
                "k_ordine"		=> $m_params->k_ordine,
                "end_session_history" =>  $use_session_history
            )
            );
        
        
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data'){
 
    $m_params = acs_m_params_json_decode();
    $oe = $s->k_ordine_td_decode_xx($m_params->k_ordine);
    
        
        $sql = "SELECT RD.*, RTPRZ
        FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
        ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
        WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
        AND RD.RDTISR = '' AND RD.RDSRIG = 0 ";
        
       
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $oe);
        
        while ($row = db2_fetch_assoc($stmt)) {
            
            $row['RDART'] = acs_u8e($row['RDART']);
            $row['RDDART'] = acs_u8e($row['RDDART']);
            $row['RDNREC'] = acs_u8e($row['RDNREC']);
            $row['RTPRZ'] = acs_u8e($row['RTPRZ']);
            $row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
           // $row['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
            $ar[] = $row;
        }
        
        
        echo acs_je($ar);
        exit();
}

if ($_REQUEST['fn'] == 'open_tab'){
    
    $m_params = acs_m_params_json_decode();
  
    
    ?>


{"success":true, "items": [

			{
			xtype: 'grid',
	        loadMask: true,	
	        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},
			stateful: true,
       		stateId: 'seleziona-righe-ordini',
            stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],	
  	      store: {
				    xtype: 'store',
				    autoLoad:true,
					loadMask: true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							method: 'POST',								
						    type: 'ajax',
							reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        },
					        actionMethods: {
						          read: 'POST'
						        },
   						     extraParams: {
   		    		    		k_ordine: '<?php echo $m_params->k_ordine_bolla; ?>',
   		    				},               						        
	        			doRequest: personalizza_extraParams_to_jsonData
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART', 'RDUM', 'RDQTA', 
	            			'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO'
	            			, 'RDQTDX', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE', 'RTPRZ'
	        			]
	    			},
	    			
				columns: [
					        
		             {
		                header   : '<br/>&nbsp;Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },			        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>Prezzo',
		                dataIndex: 'RTPRZ', 
		                width    : 50,
		                align: 'right',
						renderer: floatRenderer2	                		                
		             }, {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }	        
		      
		        

		            
		            
		         ]	 ,
	         
	          dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: ['->',{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-blog_accept-32',
                     text: 'Conferma',
			            handler: function() {
			        
			           var loc_win = this.up('window');
			            var grid = this.up('grid');
			            var l_grid = grid.getStore().data.length;
			         	var righe = grid.getSelectionModel().getSelection();
			         	
                          list_righe_selected = [];
                           for (var i=0; i<righe.length; i++) {
				            list_righe_selected.push({
				            	nrec: righe[i].get('RDNREC')
				            	});}
				            	
				            	 if (list_righe_selected.length == 0){
    							  acs_show_msg_error('Selezionare almeno una riga');
    							  button.enable();
    							  return false;
						  			}
						  			
						  			
						  	  if (list_righe_selected.length == l_grid){
    							  acs_show_msg_error('Non selezionare tutte le righe');
    							  button.enable();
    							  return false;
						  			}
			            
			                  Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_separa_ddt',
									        method     : 'POST',
						        			jsonData: {
						        				righe: list_righe_selected,
						        				k_ordine: '<?php echo $m_params->k_ordine_bolla; ?>',
											},							        
									        success : function(result, request){
						            			loc_win.fireEvent('afterConfList', loc_win);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
                  
	        				
			            }
			     }
			     
			     
			     ]
		   }]
	
			
		}//grid
		
	
     ]
        
 }
 
 <?php 
 
 exit;
 
}