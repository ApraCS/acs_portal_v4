<?php

require_once "../../config.inc.php";

$m_DeskAcq = new DeskAcq();

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){

	$m_where = '';	
	
	$ar_seleted_stato = explode("|", $_REQUEST['list_selected_stato']);
	if (count($ar_seleted_stato) > 0 && strlen($_REQUEST['list_selected_stato']) > 0){
		$m_where .= " AND CRCCON IN (" . sql_t_IN($ar_seleted_stato) . ") ";
	}

	//GRUPPO
	if (strlen(trim($_REQUEST['f_gruppo'])) > 0)
		$m_where .= " AND CRSEL1 = '" . trim($_REQUEST['f_gruppo']) . "'";
	//sottoGRUPPO
	if (strlen(trim($_REQUEST['f_sottogruppo'])) > 0)
		$m_where .= " AND CRSEL2 = '" . trim($_REQUEST['f_sottogruppo']) . "'";

	
	//articolo
	if (strlen(trim($_REQUEST['f_cod_art'])) > 0)
		$m_where .= " AND CRPROG IN (SELECT DISTINCT CRPROG FROM {$cfg_mod_DeskAcq['file_regole_riordino']} T_A WHERE T_A.CRART LIKE '%" . trim($_REQUEST['f_cod_art']) . "%')";	
	if (strlen(trim($_REQUEST['f_des_art'])) > 0)
		$m_where .= " AND CRPROG IN (SELECT DISTINCT CRPROG FROM {$cfg_mod_DeskAcq['file_regole_riordino']} T_DA WHERE T_DA.CRDART LIKE '%" . trim($_REQUEST['f_des_art']) . "%')";
			
	
	
	//GRUPPO PIANIFICAZIONE
	if (strlen(trim($_REQUEST['f_gruppo_pianificazione'])) > 0)
		$m_where .= " AND CRPROG IN (SELECT DISTINCT CRPROG FROM {$cfg_mod_DeskAcq['file_regole_riordino']} T_GP WHERE T_GP.CRGRPA = '" . trim($_REQUEST['f_gruppo_pianificazione']) . "')";
	//TIPO APPROVIGIONAMENTO (RIORDINO)
	if (strlen(trim($_REQUEST['f_tipo_approv'])) > 0)
		$m_where .= " AND CRSWTR = '" . trim($_REQUEST['f_tipo_approv']) . "'";	
	


	global $cfg_mod_DeskAcq;
	$ar = array();

	//riga di totale
	$sql = "SELECT CRDTGE, CRORGE, CRTART, CRTATT, CRTESA, CRTSOS, CRTSCA, CRTIDT, CRLTFO, CRLTAS, CRLTRI, CRLTLO, CRLTPR 
			 FROM {$cfg_mod_DeskAcq['file_regole_riordino']} WHERE CRTP ='F'";
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);
	
	if (!$_REQUEST['disableTot'] == 'Y')
	while ($r = db2_fetch_assoc($stmt)) {
				$r['CRDTUI'] = print_date($r['CRDTGE']) . "<br>" . print_ora($r['CRORGE']);
				$r['out_FOR'] = ' ';
				$r['out_FOR_grp'] = 'Ultimo aggiornamento e totali';
				$r['rowCls'] = 'liv_totale';
				$ar[] = $r;
	}	
	

	$sql = "SELECT *
			FROM {$cfg_mod_DeskAcq['file_regole_riordino']}
			WHERE CRTP ='T' {$m_where} ORDER BY CRCCON
			";

			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);

			while ($r = db2_fetch_assoc($stmt)) {
				$r['out_FOR'] = acs_u8e($r['CRDCON']) . " [" . trim($r['CRCCON']) . "]";
				$r['out_FOR_grp'] = 'Fornitore: ' . $r['out_FOR'];
				$ar[] = $r;
			}
			echo acs_je($ar);
			exit();
}








// ******************************************************************************************
// GRID (form)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_grid'){

	$m_params = acs_m_params_json_decode();
	$list_selected_stato = $m_params->selected_id;
	
	$list_selected_stato_js = acs_je(implode("|", $list_selected_stato));

	?>
	
{"success":true, "items":
  
	{
		xtype: 'grid',
		title: 'ATP Setup',
		id: 'regole-riordino',
		loadMask: true,
		closable: true,			
		
		
		viewConfig: {
		        getRowClass: function(record, index, rowParams, store) {
		            return record.get('rowCls');
		        }
		 },		
		
		features: [
		new Ext.create('Ext.grid.feature.Grouping',{
			groupHeaderTpl: '{[values.rows[0].data.out_FOR_grp]}',
			hideGroupedHeader: false
		}),
		{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true,   // defaults to false (remote filtering)
	
			// Filters are most naturally placed in the column definition, but can also be added here.
		filters: [
		{
			type: 'boolean',
			dataIndex: 'visible'
		}
		]
		}],
	
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
					this.show_regole_riordino_dett(rec.get('CRPROG'));						
				}
			},
		},
	
	
	
		store: {
			xtype: 'store',
				
			groupField: 'out_FOR',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
				list_selected_stato: <?php echo $list_selected_stato_js; ?>,
				f_gruppo: <?php echo j($m_params->f_gruppo); ?>,
				f_sottogruppo: <?php echo j($m_params->f_sottogruppo); ?>,
				f_gruppo_pianificazione:  <?php echo j($m_params->f_gruppo_pianificazione); ?>,
				f_tipo_approv:  <?php echo j($m_params->f_tipo_approv); ?>,				
				f_cod_art:  <?php echo j($m_params->f_cod_art); ?>,
				f_des_art:  <?php echo j($m_params->f_des_art); ?>								
				}				
			},
				
			fields: ['out_FOR', 'out_FOR_grp', 'rowCls', 'CRDTGE'
					  	, 'CRGEM1', 'CRGEM2', 'CRGEM3', 'CRGEM4', 'CRGEM5', 'CRGEM6'
 						, 'CRGRF1', 'CRGRF2', 'CRGRF3', 'CRGRF4', 'CRGRF5', 'CRGRF6'					  
 						, 'CRGCF1', 'CRGCF2', 'CRGCF3', 'CRGCF4', 'CRGCF5', 'CRGCF6'
 						, 'CRLTFO', 'CRLTAS', 'CRLTRI', 'CRLTLO', 'CRLTPR', 'CRSWTR', 'CRTPAR', 'CRPRIO'
 						, 'CRTART', 'CRTSOS', 'CRTSCA', 'CRTIDT', 'CRPROG'
 						, 'CRTATT', 'CRTESA', 'CRDTUI'
					  ]
						
						
		}, //store
		multiSelect: false,
		
		<?php 
			$ss = "";
			$ss .= "<img id=\"grafico_acq\"src=" . img_path("icone/48x48/grafici.png") . " height=30 onClick=\"acs_show_win_std(\'Storico codifiche articolo e tipo approvvigionamento\', \'acs_grafici_fornitore.php\', " . strtr(acs_je($m_params), array('"' => "\'")). ", 910, null, null, \'icon-shopping_setup-16\', null, \'Y\')\">";
			$classi_abc = "<img id=\"classi_abc\"src=" . img_path("icone/48x48/puzzle.png") . " height=30 onClick=\"acs_show_win_std(\'Assegna/Visualizza classificazione ABC giacenze/consumi\', \'acs_form_classi_giacenze_consumi.php\', " . strtr(acs_je($m_params), array('"' => "\'")). ", null, 295, null, \'icon-puzzle-16\', null, \'Y\')\">";			
		?>		
	
		columns: [
			
			{header: 'Fornitore', dataIndex: 'out_FOR', flex: 3, filter: {type: 'string'}, filterable: true},
			<?php if($m_params->from_anag_art != 'Y'){?>
			{header: '<?php echo $ss; ?>', flex: 0.60, sortable: false, menuDisabled: true},
			<?php }?>			
			{header: '<?php echo $classi_abc; ?>', flex: 0.45, sortable: false, menuDisabled: true},					
			{header: 'Giorni di invio', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGEM1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGEM2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGEM3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGEM4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGEM5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGEM6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Giorni di ricezione', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGRF1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGRF2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGRF3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGRF4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGRF5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGRF6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Giorni di consegna', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGCF1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGCF2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGCF3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGCF4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGCF5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGCF6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Lead time', flex: 3,
			 columns: [
			 	{header: 'Art',  dataIndex: 'CRLTFO', width: 30, renderer: floatRenderer0, align: 'right'},
			 	{header: 'Rit',  dataIndex: 'CRLTRI', width: 30, renderer: floatRenderer0, align: 'right'},			 	
			 	{header: 'Ass',  dataIndex: 'CRLTAS', width: 30, renderer: floatRenderer0, align: 'right'},			 	
				{header: 'Log',  dataIndex: 'CRLTLO', width: 30, renderer: floatRenderer0, align: 'right'},
				{header: 'Prod', dataIndex: 'CRLTPR', width: 30, renderer: floatRenderer0, align: 'right'}			
			 ]
			}, {
			 header: 'Tipo',
			 columns: [
			 	  {header: 'Appr', dataIndex: 'CRSWTR', width: 35} 		
				, {header: 'Parte', dataIndex: 'CRTPAR', width: 35}
				, {header: 'Prior', dataIndex: 'CRPRIO', width: 35}]
			}, {
			 header: 'Numero referenze',
			 columns: [
				  {header: 'Tot', dataIndex: 'CRTART', width: 40, renderer: floatRenderer0, align: 'right'}
				, {header: 'Attivi', dataIndex: 'CRTATT', width: 40, renderer: floatRenderer0, align: 'right', tdCls: 'attivi'}			
				, {header: 'Esaur', dataIndex: 'CRTESA', width: 40, renderer: floatRenderer0, align: 'right'}				  
				, {header: 'Sosp', dataIndex: 'CRTSOS', width: 40, renderer: floatRenderer0, align: 'right'}			
				, {header: 'Scad', dataIndex: 'CRTSCA', width: 40, renderer: floatRenderer0, align: 'right'}			
				, {header: 'Indisp', dataIndex: 'CRTIDT', width: 40, renderer: floatRenderer0, align: 'right'}				
			 ]
			}
			, {header: 'Ultima<br>Immissione', dataIndex: 'CRDTUI', width: 70, 
					renderer: function (value, metaData, record, row, col, store, gridView){

						//se la data di elaborazione del record e' piu' vecchia di 2 anni o 1
						if ((parseFloat(record.get('CRDTGE')) - 20000) > parseFloat(record.get('CRDTUI'))) 
							metaData.tdCls += 'elaborazione-old-2';
						else if ((parseFloat(record.get('CRDTGE')) - 10000) > parseFloat(record.get('CRDTUI'))) 
							metaData.tdCls += 'elaborazione-old-1';							
							
						if (record.get('rowCls') == 'liv_totale')
							return value;
						return date_from_AS(value);
					}
			  }			   
			
		],		
		
		
		//DETTAGLIO
		show_regole_riordino_dett: function(crprog){
		  
				var win_dett = new Ext.Window({
				  width: 900
				, height: 500
				, minWidth: 600
				, minHeight: 400
				, plain: true
				, title: 'Elenco referenze per regola di riordino'
				, layout: 'fit'
				, border: true
				, closable: true
				, items:  
					new Ext.grid.GridPanel({

							features: [
							new Ext.create('Ext.grid.feature.Grouping',{
								groupHeaderTpl: 'Gruppo pianificazione: {name}',
								hideGroupedHeader: false
							}),
							{
								ftype: 'filters',
								// encode and local configuration options defined previously for easier reuse
								encode: false, // json encode the filter query
								local: true,   // defaults to false (remote filtering)
						
								// Filters are most naturally placed in the column definition, but can also be added here.
							filters: [
							{
								type: 'boolean',
								dataIndex: 'visible'
							}
							]
							}],					
					
							store: new Ext.data.Store({
								autoLoad:true,			
								
								groupField: 'CRDGRP',
									        
			  					proxy: {
										url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_dett&crprog=' + crprog,
										timeout: 2400000,
										type: 'ajax',
										reader: {
								            type: 'json',
								            root: 'root'
								        }
									},
				        			fields: ['CRGRPA', 'CRDGRP', 'CRART', 'CRDART', 'CRDTIV', 'CRDTFV', 'CRSOSP', 'CRESAU', 'CRDTIN']
				    			}),
		    						
		    						
					        columns: [
					               {header   : 'Articolo', dataIndex: 'CRART', width: 120, filter: {type: 'string'}, filterable: true}
					             , {header   : 'Descrizione', dataIndex: 'CRDART', flex: 10, filter: {type: 'string'}, filterable: true}
					             , {header   : 'Data val. iniz', dataIndex: 'CRDTIV', width: 70, renderer: date_from_AS}					             
					             , {header   : 'Data val. fin', dataIndex: 'CRDTFV', width: 70, renderer: date_from_AS}					             
					             , {header   : 'Sosp', dataIndex: 'CRSOSP', width: 35}					             
					             , {header   : 'Esau', dataIndex: 'CRESAU', width: 35}
					             , {header   : 'Ind.Temp', dataIndex: 'CRDTIN', width: 70, renderer: date_from_AS}					             					             
					          ]})   
		
				});
		
				win_dett.show();
		
		  
		} //show_regole_riordino_dett		
	}  

}	
	
	
<?php 	
  exit();
}











// ******************************************************************************************
// DATI PER GRID FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT CRCCON, CRDCON FROM {$cfg_mod_DeskAcq['file_regole_riordino']} WHERE CRDT='{$id_ditta_default}' AND CRTP = 'T'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
	$ar[] = $r;
}
echo acs_je($ar);
exit();
}



// ******************************************************************************************
// DATI PER SELECT GRUPPO PIANIFICAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_gruppo_pianificazione'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT CRGRPA as id, CRDGRP as text FROM {$cfg_mod_DeskAcq['file_regole_riordino']} WHERE CRDT={$id_ditta_default} AND CRGRPA <> '' AND CRTP = 'A'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


// ******************************************************************************************
// DATI PER SELECT TIPO APPROVIGGIONAMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tipo_approv'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT CRSWTR as id, CRSWTR as text FROM {$cfg_mod_DeskAcq['file_regole_riordino']} WHERE CRDT={$id_ditta_default} AND CRSWTR <> '' AND CRTP = 'T'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}



// ******************************************************************************************
// DATI PER SELECT GRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_gruppo_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT CRSEL1 as id, CRDSE1 as text FROM {$cfg_mod_DeskAcq['file_regole_riordino']} WHERE CRDT={$id_ditta_default} AND CRSEL1 <> '' AND CRTP = 'T'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


// ******************************************************************************************
// DATI PER SELECT SOTTOGRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_sottogruppo_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT CRSEL2 as id, CRDSE2 as text FROM {$cfg_mod_DeskAcq['file_regole_riordino']} WHERE CRDT={$id_ditta_default} AND CRSEL2 <> '' AND CRTP = 'T'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


// ******************************************************************************************
// DATI PER GRID DETTAGLIO (PER CRPROG)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_dett'){
    ini_set('max_execution_time', 6000);
    
	//costruzione sql
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	if ($_REQUEST['from_ws'] == 'Y'){
		$sql_where = '';
		if (strlen(trim($_REQUEST['GRPFOR_COD'])) > 0 && trim($_REQUEST['GRPFOR_COD']) != 'Totale')
			$sql_where .= " AND CRSEL1 =" . sql_t($_REQUEST['GRPFOR_COD']);
		if (strlen(trim($_REQUEST['classe_g'])) > 0)
			$sql_where .= " AND WSCLS1 =" . sql_t($_REQUEST['classe_g']);
		if (strlen(trim($_REQUEST['classe_c'])) > 0)
			$sql_where .= " AND WSCLS2 =" . sql_t($_REQUEST['classe_c']);
		switch ($_REQUEST['col_name']){
			case "V_S": $sql_where .= " AND WSTIPO = 'S'"; break;
			case "V_O": $sql_where .= " AND WSTIPO = 'O'"; break;	
			case "V_D": $sql_where .= " AND WSTIPO = 'D'"; break;			
			case "V_P": $sql_where .= " AND WSTIPO = 'P'"; break;			
		}		
		
		//filtro i colli in base agli articoli presenti nella sessione estratta (WS0)
		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_regole_riordino']}
				 INNER JOIN {$cfg_mod_Admin['file_WPI0WS0']}
				    ON WSSEID = " . sql_t(session_id()) . " AND WSTP = 'AR' AND CRART = WSRFG1 
				WHERE CRDT={$id_ditta_default} AND CRTP = 'A' {$sql_where}";
		
			
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($_REQUEST['crprog']));		
	} else {
		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_regole_riordino']} WHERE CRDT={$id_ditta_default} AND CRTP = 'A' AND CRPROG = ?";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($_REQUEST['crprog']));		
	}	



	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['CRDART'] = acs_u8e($r['CRDART']);
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}






// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA (APERTA IN DEFAULT)
// ******************************************************************************************
?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
//            title: 'Selezione dati',
            
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
			            
            
            items: [
            
            
            					 	
					, {					
						xtype: 'grid',
						flex: 1,
						id: 'tab-tipo-stato',
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},
     					
						features: [
						{
							ftype: 'filters',
							// encode and local configuration options defined previously for easier reuse
							encode: false, // json encode the filter query
							local: true,   // defaults to false (remote filtering)
					
							// Filters are most naturally placed in the column definition, but can also be added here.
						filters: [
						{
							type: 'boolean',
							dataIndex: 'visible'
						}
						]
						}],     					
     					
									
						selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},				
						//columnLines: true,			
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fornitore',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['CRCCON', 'CRDCON']
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [{header: 'Fornitore', dataIndex: 'CRCCON', flex: 2, filter: {type: 'string'}, filterable: true},
								  {header: 'Denominazione', dataIndex: 'CRDCON', flex: 8, filter: {type: 'string'}, filterable: true}]
						 
					}            
            
            
            		, {            		
						margin: "0 0 0 2",            		
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			height: 50,
            			
			            layout: {
			                type: 'hbox',
			                align: 'stretch',
			                pack  : 'start'
			     		},            			
            			
     					items: [
     					
					//SELECT PER GRUPPO E SOTTOGRUPPO
					{
								margin: "0 10 0 10",
								flex: 1,					
								name: 'f_gruppo',
								xtype: 'combo',
								fieldLabel: 'Gruppo fornitura',
								labelAlign: 'top',
								width: 290,
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  }, {
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_sottogruppo',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_sottogruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  }     					
     					
     					]
     				}            
            
            
            

            		, {
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			height: 50,
            			
			            layout: {
			                type: 'hbox',
			                align: 'stretch',
			                pack  : 'start'			                
			     		},            			
            			
     					items: [     				
							{
								margin: "0 10 0 10",
								flex: 1,
								name: 'f_gruppo_pianificazione',
								xtype: 'combo',
								fieldLabel: 'Gruppo pianificazione',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo_pianificazione',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  } , {
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_tipo_approv',
								xtype: 'combo',
								fieldLabel: 'Tipo riordino',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tipo_approv',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            }
							    						 
							  }   					
     					
     					
     					]            			
            		}, 
            		
            		
					, {
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			height: 50,
            			
			            layout: {
			                type: 'hbox',
			                align: 'stretch',
			                pack  : 'start'			                
			     		},            			
            			
     					items: [{
								margin: "0 10 0 10",							  
								name: 'f_des_art',
								xtype: 'textfield',
								fieldLabel: 'Descrizione articolo',
								labelAlign: 'top',
								flex: 1
							  }, {
								margin: "0 10 0 10",							  
								name: 'f_cod_art',
								xtype: 'textfield',
								fieldLabel: 'Codice articolo',
								labelAlign: 'top',
								flex: 1					
							  }  ]
     				  }            		
            		
            		
     				
	
						 
				],
			buttons: [
			
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [			
			
			
					
						]	  
				} 
				
				
				
				
				
				
			, {
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [				
				
			
			
			 {
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();
  				list_selected_id = [];
  				for (var i=0; i<id_selected.length; i++) 
	   				list_selected_id.push(id_selected[i].data.CRCCON);
				
				
				m_values = form_p.getValues();
				m_values['selected_id'] = list_selected_id;
				m_values['from_anag_art'] = '<?php echo $m_params->from_anag_art; ?>';
	            
				//carico la form dal json ricevuto da php e la inserisco nel m-panel
				Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_grid',
				        method     : 'POST',
				        jsonData: m_values,
				        waitMsg    : 'Data loading',
				        success : function(result, request){			        	
				        	
							mp = Ext.getCmp('m-panel');					        					        
							mp.remove('regole-riordino');					        
				            var jsonData = Ext.decode(result.responseText);
				            
				            mp.add(jsonData.items);
				            mp.doLayout();
				            mp.items.last().show();
 							this.print_w.close();
						             
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				        				        
				    });
				    
	            
            	                	                
	            }
	        }

	        ]
	       } 
	        
	        
	       ],             
				
        }
]}			            
