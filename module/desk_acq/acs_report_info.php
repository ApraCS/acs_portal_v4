<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
 
$main_module = new DeskAcq();

$da_form = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'open_report'){

// ******************************************************************************************
// REPORT
// ******************************************************************************************

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}

    table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}

    td.titolo{font-weight:bold;}
  
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   tr.ag_liv_data th{background-color: #333333; color: white; font-size: 15px;}  
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   h2.acs_report_title{font-size: 18px; padding: 10px;}
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     

</style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);

    Ext.onReady(function() {	
	});  

  </script>

</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content'>

<h2 class='acs_report_title'>Riepilogo consegna forniture</h2>
<p align="right"> Data elaborazione:  <?php echo date('d/m/Y  H:i');?></p>

<table class=int1>
	

<?php

	$data = array();

	$form_values = (array)json_decode($_REQUEST['form_values']);
    $filter = (array)json_decode($form_values['filter']);
	$filtro = $s->get_elenco_ordini_create_filtro($filter);  
	$stmt 	= $main_module->get_elenco_ordini($filtro, 'N', 'search');
	
	
	
	switch ($backend_ERP){
	    case 'GL':
	        $sql_rd = "SELECT
	                   RD.MECAR0 AS RDART,
	                   MADES0    AS RDDART,
	                   MEUNM0    AS RDUM,
	                   MEQTA0    AS RDQTA
	                   FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
		               LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} ART
    						ON RD.MECAR0 = ART.MACAR0
	               WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
	        
	        break;
	        
	    default: //SV2
	        $sql_rd = "SELECT RDART, RDDART, RTINFI, RDQTA, RDUM
	                   FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
            	       LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
            	       ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
            	       WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
            	       AND RD.RDTISR = '' AND RD.RDSRIG = 0";
	            
	} //switch $backend_ERP
	
	
	

	
	$stmt_rd = db2_prepare($conn, $sql_rd);
	echo db2_stmt_errormsg();

	while ($row = db2_fetch_assoc($stmt)) {
	    
			    $nr = array();
				$nr['TDDCON'] 		= acs_u8e(trim($row['TDDCON']));
				$nr['k_ordine']     = $row['TDDOCU'];
				$nr['ordine']		= trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
				$nr['data_ordine']	= trim($row['TDODRE']);
			    $nr['desc']		    = trim($row['TDDOTD']);
				$nr['stato'] 		= trim($row['TDSTAT']);	
				$nr['TDDTEP'] 		= $row['TDDTEP'];		
				$nr['importo']  	= $row['TDINFI'];
				
				$oe = $s->k_ordine_td_decode_xx($row['TDDOCU']);
				$result = db2_execute($stmt_rd, $oe);
				$count = 0;
				while($row_rd = db2_fetch_assoc($stmt_rd)){
				    if(substr($row_rd['RDART'], 0, 1) != '*')
				        $count ++;
				}
				    
				 $nr['righe']  	= "[# $count]";
				
				$data[] = $nr;
	}
	


	
$cliente = "";
$stacco_data = 0;
foreach ($data as $k0 => $v0){ 

    if($stacco_data != $v0['TDDTEP']){
        
        echo "<tr><th colspan= 7> <b>Consegne del ".print_date($v0['TDDTEP'])." </b></tr>";
    
        ?>
        
        <tr class=liv_totale>
        <td class=titolo>Fornitore</td>
        <td class=titolo>Ordine</td>
         <td class=titolo>St.</td>
        <td class=titolo>Data</td>
        <td class=titolo>Righe</td>
        <td class=titolo>UM</td>
        <td class=titolo>Q.t&agrave;</td>
        </tr>
        
      <?php  
    }

    if($cliente != $v0['TDDCON'])
        $cliente = $v0['TDDCON'];
    else $cliente= "";
    // $v0['desc']; $v0['importo']
    
	?>

   <tr>
   <td><?php echo $cliente; ?></td>
   <td><?php echo $v0['ordine'] ?></td>
   <td><?php echo $v0['stato']; ?></td>
   <td><?php echo print_date($v0['data_ordine']); ?></td>
   <td><?php echo $v0['righe']; ?></td>
   <td></td> 
   
   <td class=number></td>
   </tr>
  

<?php 

if($form_values['f_dettaglio_righe'] == 'Y'){
       
        $oe = $s->k_ordine_td_decode_xx($v0['k_ordine']);
        $result = db2_execute($stmt_rd, $oe);
        
        while($row_rd = db2_fetch_assoc($stmt_rd)){
            if(substr($row_rd['RDART'], 0, 1) != '*'){
                $articolo = "[". trim($row_rd['RDART'])."] ".trim($row_rd['RDDART']);
                $qta = n($row_rd['RDQTA'], 2);
                $um = trim($row_rd['RDUM']);
                $importo = n($row_rd['RTINFI'],2);
                
                echo "<tr>
                <td colspan = 4>&nbsp;</td>
                <td>{$articolo}</td>
                <td>{$um}</td>  
                <td align=right>{$qta}</td>
                </tr>";
           }
        }
    }
    $cliente = $v0['TDDCON'];
    
    $stacco_data = $v0['TDDTEP'];
    
}
?>
 </table>

</div>
</body>
</html>
<?php 
  }
  
  if ($_REQUEST['fn'] == 'open_form'){
  	$m_params = acs_m_params_json_decode();
?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [
				{
                	xtype: 'hidden',
                	name: 'filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                },
            	 {
						flex: 1,						 
						name: 'f_dettaglio',
						xtype: 'checkboxgroup',
						fieldLabel: 'Dettaglio righe',
						labelAlign: 'left',
					   	allowBlank: true,
					   	labelWidth: 140,
					   	items: [{
	                            xtype: 'checkbox'
	                          , name: 'f_dettaglio_righe' 
	                          , boxLabel: ''
	                          , inputValue: 'Y'
	                        }]														
					 }            
            ],
            
			buttons: [					
				{
		            text: 'Visualizza',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		                this.up('form').submit({
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST',
	                        params: {
	                        	form_values: Ext.encode(this.up('form').getValues())
	                        }
	                  });
	                  
	                  this.up('window').close();
		                
		            }
		        } 
	        
	        
	        ]            
            
           
}
	
]}


<?php 
}
?>