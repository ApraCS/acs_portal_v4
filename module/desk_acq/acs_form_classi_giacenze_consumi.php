<?php
require_once "../../config.inc.php";

$main_module = new DeskAcq();

set_time_limit(120);

//**************************************
//  PASSO 1 - ELIMINAZIONE/NUOVO INSERIMENTO
//**************************************
if ($_REQUEST['fn'] == 'passo_1'){

$m_params = acs_m_params_json_decode();

//creazione clausole
$sql_where = crea_where_param_Rriordino($m_params, 'where');
$parametri = crea_where_param_Rriordino($m_params, 'parametri');

$ieri = " CURRENT TIMESTAMP - 1 DAYS";
 
	// QUERY ELIMINAZIONE
	$sql_delete = "DELETE FROM {$cfg_mod_Admin['file_WPI0WS0']} 
				   WHERE WSSEID = '" . session_id() . "' OR WSTSCR < $ieri
				   ";
				   
	$stmt_delete = db2_prepare($conn, $sql_delete);
	$result_delete = db2_execute($stmt_delete);
	

	// QUERY INSERIMENTO
	$sql_insert = "INSERT INTO {$cfg_mod_Admin['file_WPI0WS0']} (WSSEID, WSTP, WSRFG1, WSSGIA, WSSCON, WSTIPO)    
				   SELECT " . sql_t(session_id()) . ", 'AR', CRART, SUM(CRVGIAC), SUM(CRVCONS), CRSWTR                 
				   FROM {$cfg_mod_DeskAcq['file_regole_riordino']}                            
				   WHERE CRTP = 'A' $sql_where                              
				   GROUP BY CRART, CRSWTR
				   ";


	$stmt_insert = db2_prepare($conn, $sql_insert);		
	$result_insert = db2_execute($stmt_insert, $parametri);
	
	
	// QUERY AGGIORNAMENTO VALORI <= 0
	$sql_update_gia = "UPDATE {$cfg_mod_Admin['file_WPI0WS0']} 
						SET WSSGIA = ? 
						WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' AND WSID = ?
						";
				   
	$sql_update_con = "UPDATE {$cfg_mod_Admin['file_WPI0WS0']} 
						SET WSSCON = ? 
						WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' AND WSID = ?
						";			   
			   
	$stmt_update_gia = db2_prepare($conn, $sql_update_gia);
	$stmt_update_con = db2_prepare($conn, $sql_update_con);
	
	$sql = "SELECT WSID, WSSGIA, WSSCON
			FROM {$cfg_mod_Admin['file_WPI0WS0']} 
			WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' 
			";
			
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);

	while ($r = db2_fetch_assoc($stmt)){
		if($r['WSSGIA'] < 0)
			$result = db2_execute($stmt_update_gia, array(0, $r['WSID']));
		if($r['WSSCON'] < 0)
			$result = db2_execute($stmt_update_con, array(0, $r['WSID']));
	}
	

exit;
}


//*********************************
//  PASSO 2 - PROGRESSIVO GIACENZE
//*********************************
if ($_REQUEST['fn'] == 'passo_2'){

// QUERY AGGIORNAMENTO GIACENZE
$sql_update = "UPDATE {$cfg_mod_Admin['file_WPI0WS0']} 
			   SET WSPGIA = ? 
			   WHERE WSSEID = " . sql_t(session_id()) . " AND WSID = ? AND WSTP='AR'
			   ";
			   
$stmt_update = db2_prepare($conn, $sql_update);

$sql = "SELECT WSID, WSSGIA
		FROM {$cfg_mod_Admin['file_WPI0WS0']} 
		WHERE WSSEID = " . sql_t(session_id()) . " AND WSSGIA > 0  AND WSTP='AR'
		ORDER BY WSSGIA DESC
		";

$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt);

$prog_giac = 0;
while ($r = db2_fetch_assoc($stmt)){
	$prog_giac += $r['WSSGIA'];
	$result = db2_execute($stmt_update, array($prog_giac, $r['WSID']));
}

exit;
}//passo2



//*********************************
//  PASSO 3 - PROGRESSIVO CONSUMI
//********************************
if ($_REQUEST['fn'] == 'passo_3'){

// QUERY AGGIORNAMENTO CONSUMI
$sql_update = "UPDATE {$cfg_mod_Admin['file_WPI0WS0']} 
			   SET WSPCON = ? 
			   WHERE WSSEID = " . sql_t(session_id()) . " AND WSID = ? AND WSTP='AR'
			   ";		
			   
$stmt_update = db2_prepare($conn, $sql_update);

$sql = "SELECT WSID, WSSCON
		FROM {$cfg_mod_Admin['file_WPI0WS0']}
		WHERE WSSEID = " . sql_t(session_id()) . " AND WSSCON > 0  AND WSTP='AR'
		ORDER BY WSSCON DESC
		";
		
$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt);

$prog_cons= 0;
while ($r = db2_fetch_assoc($stmt)){
	$prog_cons += $r['WSSCON'];
	$result = db2_execute($stmt_update, array($prog_cons, $r['WSID']));
}

exit;
}//passo3



//*********************************
//  PASSO 4 - ASSEGNAMENTO CLASSI
//*********************************
if ($_REQUEST['fn'] == 'passo_4'){

$m_params = acs_m_params_json_decode();

//Salvo i parametri
$sql_insert = "INSERT INTO {$cfg_mod_Admin['file_WPI0WS0']} (WSSEID, WSTP, WSRFG1, WSMEMO) VALUES(?,?,?,?)";
$stmt_insert = db2_prepare($conn, $sql_insert);
$result_insert = db2_execute($stmt_insert, array(session_id(), 'PA', 'F-INPUT', acs_raw_post_data()));

//Query Giacenze e Consumi totali
$sql = "SELECT SUM(WSSGIA) AS TOT_GIAC, SUM(WSSCON) AS TOT_CONS
		FROM {$cfg_mod_Admin['file_WPI0WS0']}
		WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' AND ( WSSGIA > 0 OR WSSCON > 0 )
		";		
		
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);

/*
$ar_totali = array();
while ($r = db2_fetch_assoc($stmt)){	
	$ar_totali[] = $r;
}
$ar_totali[0]['TOT_GIAC']
*/

$ar_tot_giac = 0;
$ar_tot_cons = 0;
while ($r = db2_fetch_assoc($stmt)){
	$ar_tot_giac = $r['TOT_GIAC'];
	$ar_tot_cons = $r['TOT_CONS'];
}

$percG1 = ($ar_tot_giac) * $m_params->GPA / 100;
$percG2 = ($ar_tot_giac) * $m_params->GPB / 100;
$percG3 = ($ar_tot_giac) * $m_params->GPC / 100;
$percG4 = ($ar_tot_giac) * $m_params->GPD / 100;
$percG5 = ($ar_tot_giac) * $m_params->GPZ / 100; // 100 %

$percC1 = ($ar_tot_cons) * $m_params->CPA / 100;
$percC2 = ($ar_tot_cons) * $m_params->CPB / 100;
$percC3 = ($ar_tot_cons) * $m_params->CPC / 100;
$percC4 = ($ar_tot_cons) * $m_params->CPD / 100;
$percC5 = ($ar_tot_cons) * $m_params->CPZ / 100; //100 %

// QUERY AGGIORNAMENTO CONSUMI
$sql_update = "UPDATE {$cfg_mod_Admin['file_WPI0WS0']}
				SET WSCLS1 = ?,
					WSCLS2 = ?
				WHERE WSSEID = " . sql_t(session_id()) . " AND WSID = ? AND WSTP='AR'
				";
$stmt_update = db2_prepare($conn, $sql_update);

$sql = "SELECT WSID, WSPGIA, WSPCON
		FROM {$cfg_mod_Admin['file_WPI0WS0']}
 		WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR'
		";
		
$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt);

$classe1 = '';
$classe2 = '';
// Assegnamento tipologie classi
while ($r = db2_fetch_assoc($stmt)){

	if($r['WSPGIA'] > 0){
		if ($r['WSPGIA'] <= $percG1)		
			$classe1 = trim($m_params->GTA);
		else if ($r['WSPGIA'] <= $percG2)
			$classe1 = trim($m_params->GTB);
		else if ($r['WSPGIA'] <= $percG3)
			$classe1 = trim($m_params->GTC);
		else if ($r['WSPGIA'] <= $percG4)
			$classe1 = trim($m_params->GTD);
	}else
		$classe1 = trim($m_params->GTZ);
		
	if($r['WSPCON'] > 0){
		if ($r['WSPCON'] <= $percC1)
			$classe2 = trim($m_params->CTA);
		else if ($r['WSPCON'] <= $percC2)
			$classe2 = trim($m_params->CTB);
		else if ($r['WSPCON'] <= $percC3)
			$classe2 = trim($m_params->CTC);
		else if ($r['WSPCON'] <= $percC4)
			$classe2 = trim($m_params->CTD);
	}else
		$classe2 = trim($m_params->CTZ);
		
	$result_update = db2_execute($stmt_update, array($classe1, $classe2, $r['WSID']));
}

exit;
}// passo_4






//**************************************************
// Window esito tabellare Classi Giacenze e Consumi
//**************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){

	$m_params = acs_m_params_json_decode();
	
	$sql_where = '';
	$parametri = array();
	
	if (strlen($_REQUEST['classe_g']) > 0)
		$sql_where .= " AND WSCLS1 = " . sql_t(trim($_REQUEST['classe_g']));
	if (strlen($_REQUEST['classe_c']) > 0)
		$sql_where .= " AND WSCLS2 = " . sql_t(trim($_REQUEST['classe_c']));	
	
	$sql = "SELECT CRSEL1, CRDSE1, WSTIPO, SUM(WSSGIA) AS SOMGIA, SUM(WSSCON) AS SOMCON, COUNT(*) AS CNTREF
			FROM {$cfg_mod_Admin['file_WPI0WS0']} WS
			INNER JOIN {$cfg_mod_DeskAcq['file_regole_riordino']} CR
			ON WS.WSRFG1 = CR.CRART
			WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' {$sql_where}
			GROUP BY CRSEL1, CRDSE1, WSTIPO
			ORDER BY CRDSE1
		";
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);
	
	$array_art_tip = array();
	$ar_tot = array(
			"R" => array('tipo_valore' => 'Conteggio referenze', 	'GRPFOR_COD' => ' Totale', 'GRPFOR' => ' Totale'),
			"G" => array('tipo_valore' => 'Somma giacenza',  		'GRPFOR_COD' => ' Totale', 'GRPFOR' => ' Totale'),
			"C" => array('tipo_valore' => 'Somma consumato', 		'GRPFOR_COD' => ' Totale', 'GRPFOR' => ' Totale'),
	);
	
	
	//CREAZIONE ARRAY AD ALBERO (Gruppo Fornitura - Tipologia - Giacenza & Consumo & Referenze)
	while ($r = db2_fetch_assoc($stmt)) {
	
	$liv0_v = implode("|", array(trim($r['CRSEL1'])));
//	$liv1_v = trim($r['WSTIPO']);
	
	$tmp_ar_id = array();
	$n_children = "children";
	
	// LIVELLO 0
	$s_ar = &$array_art_tip;
	$liv_c     = implode("|", array($liv0_v, "R"));
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");
		$s_ar[$liv_c]["tipo_valore"] = "Conteggio referenze";		
		$s_ar[$liv_c]["id"] = implode("|", $tmp_ar_id);
		$s_ar[$liv_c]["GRPFOR"] = $r['CRDSE1'];
		$s_ar[$liv_c]["GRPFOR_COD"] = $r['CRSEL1'];		
	}
	$ar_tot['R']["V_TOT"] += $r['CNTREF']; 
	$ar_tot['R']["V_{$r['WSTIPO']}"] += $r['CNTREF'];	
	$s_ar[$liv_c]["V_TOT"] += $r['CNTREF'];	
	$s_ar[$liv_c]["V_{$r['WSTIPO']}"] += $r['CNTREF'];
	
	//$s_ar = &$s_ar[$liv_c][$n_children];
	$s_ar = &$array_art_tip;
	
	$liv_c     = implode("|", array($liv0_v, "G"));
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");
		$s_ar[$liv_c]["tipo_valore"] = "Valore giacenza";		
		$s_ar[$liv_c]["id"] = implode("|", $tmp_ar_id);
		$s_ar[$liv_c]["GRPFOR"] = $r['CRDSE1'];
		$s_ar[$liv_c]["GRPFOR_COD"] = $r['CRSEL1'];		
	}	
	$ar_tot['G']["V_TOT"] += $r['SOMGIA']; 
	$ar_tot['G']["V_{$r['WSTIPO']}"] += $r['SOMGIA'];	
	$s_ar[$liv_c]["V_TOT"] += $r['SOMGIA'];	
	$s_ar[$liv_c]["V_{$r['WSTIPO']}"] += $r['SOMGIA'];	

	//$s_ar = &$s_ar[$liv_c][$n_children];
	$s_ar = &$array_art_tip;
	
	$liv_c     = implode("|", array($liv0_v, "C"));
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");
		$s_ar[$liv_c]["id"] = implode("|", $tmp_ar_id);
		$s_ar[$liv_c]["tipo_valore"] = "Valore consumi";		
		$s_ar[$liv_c]["GRPFOR"] = $r['CRDSE1'];
		$s_ar[$liv_c]["GRPFOR_COD"] = $r['CRSEL1'];		
	}	
	$ar_tot['C']["V_TOT"] += $r['SOMCON'];
	$ar_tot['C']["V_{$r['WSTIPO']}"] += $r['SOMCON'];	
	$s_ar[$liv_c]["V_TOT"] += $r['SOMCON'];	
	$s_ar[$liv_c]["V_{$r['WSTIPO']}"] += $r['SOMCON'];	
	

/*	
	// LIVELLO 1
	$liv_c     = $liv1_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_2");
		$s_ar[$liv_c]["TOTGIA"] = $r['SOMGIA'];
		$s_ar[$liv_c]["TOTCON"] = $r['SOMCON'];
		$s_ar[$liv_c]["TOTREF"] = $r['CNTREF'];
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c];
*/	
	
	} //while

	
	$ar_ret = array();


	
	foreach($ar_tot as $kar => $ar){
		$ar_ret[] = $ar;
	}
	
	foreach ($array_art_tip as $kar => $ar){
		
		$ar_ret[] = $ar;
	}
	echo acs_je($ar_ret); exit;			
	
	
/*	
	//CREAZIONE RECORD S-D-O PER GRUPPO FORNITURA
	$ar_tipo_riordino = array();
	foreach ($array_art_tip as $ar){
	$ar_record = array();
	$ar_record['GRPFOR'] = $ar['task'];
	
	foreach ($ar['children'] as $ar_1){
		$ar_record[$ar_1['cod'] . '_GIAC'] = $ar_1['TOTGIA'];
			$ar_record[$ar_1['cod'] . '_CONS'] = $ar_1['TOTCON'];
			$ar_record[$ar_1['cod'] . '_REFE'] = $ar_1['TOTREF'];
	}
	
	$ar_tipo_riordino[] = $ar_record;
	}
	
	echo acs_je($ar_tipo_riordino);
*/	
	
 exit;
}







//**************************************************
// Window esito tabellare Classi Giacenze e Consumi
//**************************************************
if ($_REQUEST['fn'] == 'get_json_view'){
	global $cfg_mod_Admin;
	//recupero i parametri
	$sql = "SELECT * FROM {$cfg_mod_Admin['file_WPI0WS0']} 
			WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='PA' AND WSRFG1 = 'F-INPUT'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);
	$r = db2_fetch_assoc($stmt);
	$f_input = (array)json_decode($r['WSMEMO']);


	//Classi e totali raggruppati
	$sql = "SELECT WSCLS1, WSCLS2, SUM(WSSGIA) AS VGIAC, SUM(WSSCON) AS VCONS, COUNT(*) AS NUMREF
		FROM {$cfg_mod_Admin['file_WPI0WS0']}
 		WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' 
		GROUP BY WSCLS1, WSCLS2
		ORDER BY WSCLS1, WSCLS2
		";		
		
$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt);

$ar_record = array();

$ar_cls1 = array();
$countcls1 = 0;
$ar_cls2 = array();
$countcls2 = 0;
$tot_ref = 0;
while ($r = db2_fetch_assoc($stmt)){
	$ar_record[] = $r;
	
	// conteggio numero colonne
	if(!(in_array($r['WSCLS1'], $ar_cls1))){
		array_push($ar_cls1, $r['WSCLS1']);
		$countcls1++;
	}
	//conteggio numero righe
	if(!(in_array($r['WSCLS2'], $ar_cls2))){
		array_push($ar_cls2, $r['WSCLS2']);
		$countcls2++;
	}
	$tot_gia += $r['VGIAC'];
	$tot_con += $r['VCONS'];
	$tot_ref += $r['NUMREF'];
}

//creazione array ad albero per le classi
$ar_giac_cons = array();
foreach($ar_record as $value){
    $value['PRCGIA'] = 0;
    $value['PRCCON'] = 0;
    $value['PRCREF'] = 0;
    if ($tot_gia != 0) $value['PRCGIA'] = n($value['VGIAC'] * 100 / $tot_gia, 2);
    if ($tot_con != 0) $value['PRCCON'] = n($value['VCONS'] * 100 / $tot_con, 2);
	if ($tot_ref != 0) $value['PRCREF'] = n($value['NUMREF'] * 100 / $tot_ref, 2);
	$ar_giac_cons[$value['WSCLS1']][$value['WSCLS2']] = $value;
	
	//somma per classe
	$ar_tot_v_cls1[$value['WSCLS1']] += $value['VGIAC'];
	$ar_tot_r_cls1[$value['WSCLS1']] += $value['NUMREF'];
	
	$ar_tot_v_cls2[$value['WSCLS2']] += $value['VCONS'];
	$ar_tot_r_cls2[$value['WSCLS2']] += $value['NUMREF'];	
		
}

//ordinamento array classi
sort($ar_cls1);
sort($ar_cls2);

?>
{"success":true, "items": [

	{
	 xtype: 'panel', 
	 layout: {
			type: 'hbox', border: false, pack: 'start', align: 'stretch',				
		},
	 items: [	
	 {
	  xtype: 'panel',
	  flex: 1.9,
	  layout: {
			type: 'vbox', border: false, pack: 'start', align: 'stretch',				
		},
	  items: [

		{
			xtype: 'form',
			flex: 1.9, autoScroll:true,
			title: '',
			layout: {
				type: 'table',
				columns: <?php echo (($countcls1*2)+1) ?>,
			},
			defaults: {
				bodyStyle:'padding:5px'
			},
			items: [ 
			{html:'<a href="#" class="view_dett_class_gc"><B>Giacenza</B><br><HR/>Consumo</a>', height: 65, width: 110, cls: 'x-table-index leg'},
			<?php
			for($i = 0; $i < $countcls1; $i++)
				echo "{html: '<B>" . $ar_cls1[$i] . "</B><br><span class=dett>(" . get_perc_cls1_by_val($ar_cls1[$i]) . ")</span>" . "<span class=dett><br>[" . n($ar_tot_r_cls1[$ar_cls1[$i]], 0) . "] <B>" . n($ar_tot_v_cls1[$ar_cls1[$i]], 0) . "</B></span>', height: 65,  colspan: 2, cls: 'x-table-index'},";
			for($i = 0; $i < $countcls2; $i++){				
				echo "{html: '<B>" . $ar_cls2[$i] . "</B><br><span class=dett>(" . get_perc_cls2_by_val($ar_cls2[$i]) . ")</span>". "<span class=dett><br>[" . n($ar_tot_r_cls2[$ar_cls2[$i]], 0) . "] " . n($ar_tot_v_cls2[$ar_cls2[$i]], 0) . "</span>', cls: 'x-table-index', height: 60, width: 110},";
				for($j = 0; $j < $countcls1; $j++){
					$ref = acquisisci_giac_cons($ar_giac_cons, $ar_cls1[$j], $ar_cls2[$i], 'referenze');
					$gia = acquisisci_giac_cons($ar_giac_cons, $ar_cls1[$j], $ar_cls2[$i], 'giacenza');
					$con = acquisisci_giac_cons($ar_giac_cons, $ar_cls1[$j], $ar_cls2[$i], 'consumo');
					$p_ref = acquisisci_giac_cons($ar_giac_cons, $ar_cls1[$j], $ar_cls2[$i], 'perc_referenze');
					$p_gia = acquisisci_giac_cons($ar_giac_cons, $ar_cls1[$j], $ar_cls2[$i], 'perc_giacenza');
					$p_con = acquisisci_giac_cons($ar_giac_cons, $ar_cls1[$j], $ar_cls2[$i], 'perc_consumo');
					if($ref == ''){
						$indici = '';
						$totali = '';
						$percentuali = '';
					}else{
						$indici = "R:<br>G:<br>C:";
						$totali = "[" . n($ref, 0) . "]" . "<br><B>" . n($gia, 0) . "</B><br>" . n($con, 0);
						$percentuali = $p_ref . " %<br><B>" . $p_gia . " %</B><br>" . $p_con. " %";
					}
					
					//echo "{html: '" . $indici . "', height: 60, width: 20, border: false, componentCls: 'table-border'},";
					echo "{html: '<a href=\"#\" class=\"view_dett_class_gc\" classe_g=\"{$ar_cls1[$j]}\" classe_c=\"{$ar_cls2[$i]}\"> " . $totali . " </a>', height: 60, width: 60, border: false, componentCls: 'table-border " . get_cls1_by_val($ar_cls1[$j]) . get_cls2_by_val($ar_cls2[$i]) . "'},";					
					echo "{html: '$percentuali', height: 60, width: 65, border: false, componentCls: 'table-border-final " . get_cls1_by_val($ar_cls1[$j]) . get_cls2_by_val($ar_cls2[$i]) . "'},";
				}
			}	
			?>
			, {html: 'Legenda: [Nr referenze], <b>Valore giacenze</b>, Valore consumi', colspan: <?php echo (($countcls1*2)+1) ?>, border: false},
			]
			
			, listeners: {
				afterrender: function(comp){	
						Ext.select(".view_dett_class_gc").on("click", function (e, t) {
							e.preventDefault();
																				
							t = Ext.get(t);
							
							if (t.getAttribute('classe_g') === null){
								//sono in <b> quindi gli attributi delle classi le trovo al livello superiore
								classe_g = t.parent().getAttribute('classe_g');
								classe_c = t.parent().getAttribute('classe_c');							
							} else {														
								classe_g = t.getAttribute('classe_g');
								classe_c = t.getAttribute('classe_c');
							}						

							//aggiorno tabella						
							l_grid = Ext.getCmp('classi_giacenze_consumi_grid');
							l_grid.store.proxy.extraParams.classe_g = classe_g;
							l_grid.store.proxy.extraParams.classe_c = classe_c;							
							l_grid.store.load();
							
							//aggiornamento grafici
							chart = Ext.getCmp('classi_giacenze_chart_ciclo_vita');
							chart.store.proxy.extraParams.classe_g = classe_g;
							chart.store.proxy.extraParams.classe_c = classe_c;							
							chart.store.load();							
							
							chart = Ext.getCmp('classi_giacenze_chart_tipo_riordino');
							chart.store.proxy.extraParams.classe_g = classe_g;
							chart.store.proxy.extraParams.classe_c = classe_c;							
							chart.store.load();							
							
							return false;
						});						 
				}			 					 
			}			
			
			
			
        }, //form
        
        
        //pannello grafici
        {
		  xtype: 'panel',
	  	  flex: 1.1,
	  	  layout: {
			type: 'hbox', border: false, pack: 'start', align: 'stretch',				
			},
	  		items: [
	  			// -----------------------------------------
	  			// grafico ciclo di vita
	  			// -----------------------------------------
					{
					 xtype: 'panel',
					 layout: 'fit',
					 width: 400,
			         title: 'Ciclo di vita',					 
					 items: 	  				  		
						{
			            xtype: 'chart',
			            animate: true,
			            id: 'classi_giacenze_chart_ciclo_vita',

						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_ciclo_di_vita&from_ws=Y',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									type: 'json',
									read: 'POST'
								},
					
								extraParams: {},
								
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'							
								},
								
							},
		
							fields: ['name', 'des_name', 'referenze']
										
						}, //store			            
			            
			            
			            shadow: true,
			            legend: {
			            	field: 'des_name',
			                position: 'right'
			            },
			            insetPadding: 10,
			            theme: 'Base:gradients',
			            series: [{
			                type: 'pie',
			                field: 'referenze',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 200,
			                  height: 28,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += rec.get('referenze');
			                    });
			                    this.setTitle(storeItem.get('des_name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'des_name',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('classi_giacenze_chart_ciclo_vita'); // id of the chart
									var index = cmp.store.findExact('des_name', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.name; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
			        }	  			
	  			}
	  			
	  			
	  			// -----------------------------------------
	  			// grafico per tipo riordino
	  			// -----------------------------------------
				  ,	{
					 xtype: 'panel',
					 layout: 'fit',
					 flex: 1,
			         title: 'Tipo riordino (attivi)',					 
					 items:	  				  			
					  {
			            xtype: 'chart',
			            animate: true,
			            width: 200,
			            id: 'classi_giacenze_chart_tipo_riordino',			            

						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart_tipo_riordino&from_ws=Y',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									type: 'json',
									read: 'POST'
								},
					
								extraParams: {},
								
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'							
								},
								
							},
		
							fields: ['name','referenze']
										
						}, //store			            
			            
			            
			            shadow: true,
			            legend: {
			                position: 'right'
			            },
			            insetPadding: 10,
			            theme: 'Base:gradients',
			            series: [{
			                type: 'pie',
			                field: 'referenze',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 200,
			                  height: 28,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += rec.get('referenze');
			                    });
			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'name',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial'
			                }
			            }]
			        }	 	  			
	  			}
	  			
	  		]
	  	}	
        
        
        
       ]
      },  
        
        
        
        
        
        
	{
		xtype: 'grid',
		layout: 'fit',
		id: 'classi_giacenze_consumi_grid',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    
		features: [	
			{
	            id: 'group',
	            ftype: 'grouping',
	            groupHeaderTpl: '{name}',
	            hideGroupedHeader: true
	        }
		],		    
	    
		store: {
			xtype: 'store',
				
			groupField: 'GRPFOR',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {}				
			},
				
			fields: [{name:'GRPFOR', type: 'string'}, 'GRPFOR_COD', 'tipo_valore',
					 {name:'V_TOT', type: 'float'},			
					 {name:'V_O', type: 'float'},
					 {name:'V_D', type: 'float'},
					 {name:'V_S', type: 'float'},
					 {name:'V_P', type: 'float'}]
						
						
		}, //store	    
	    
	    
	    			 		
		columns: [{header: 'Gruppo Fornitura', flex: 1, dataIndex: 'GRPFOR'},
				  {header: 'Valore', flex: 1, dataIndex: 'tipo_valore', align: 'right'},
				  {header: 'Valori', flex: 2, columns:[
					{header: 'TOT', dataIndex: 'V_TOT', width: 60, align: 'right', renderer: floatRenderer0, tdCls: 'grassetto'},				  
					{header: 'O', dataIndex: 'V_O', width: 60, align: 'right', renderer: floatRenderer0},
					{header: 'D', dataIndex: 'V_D', width: 60, align: 'right', renderer: floatRenderer0},
					{header: 'S', dataIndex: 'V_S', width: 60, align: 'right', renderer: floatRenderer0},
					{header: 'P', dataIndex: 'V_P', width: 60, align: 'right', renderer: floatRenderer0}
				]}
    	], 
    	
    	
	  viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {
		           if (record.get('tipo_valore') == 'Conteggio referenze') return 'colora_riga_azzurro';		           	
		         }   
		    },    	
    	
    	
		listeners: {
		
	        'cellclick': function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
				rec = iView.getRecord(iRowEl);								
				col_name = iView.getGridColumns()[iColIdx].dataIndex;
				classi_extra_params = Ext.merge({}, iView.getStore().proxy.extraParams);	        	
				m_extraParams = Ext.merge(Ext.merge(classi_extra_params, rec.data), {col_name: col_name});

							//aggiornamento grafici
							chart = Ext.getCmp('classi_giacenze_chart_ciclo_vita');
							chart.store.proxy.extraParams = m_extraParams;							
							chart.store.load();							
							
							chart = Ext.getCmp('classi_giacenze_chart_tipo_riordino');
							chart.store.proxy.extraParams = m_extraParams;							
							chart.store.load();				
				
					        	
	        },		
		
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					
					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    classi_extra_params = Ext.merge({}, iView.getStore().proxy.extraParams);
					
					var win_dett = new Ext.Window({
					  width: 900
					, height: 500
					, minWidth: 600
					, minHeight: 400
					, plain: true
					, title: 'Elenco referenze per regola di riordino'
					, layout: 'fit'
					, border: true
					, closable: true
					, items:  
						new Ext.grid.GridPanel({
	
								features: [
								new Ext.create('Ext.grid.feature.Grouping',{
									groupHeaderTpl: 'Gruppo pianificazione: {name}',
									hideGroupedHeader: false
								}),
								{
									ftype: 'filters',
									// encode and local configuration options defined previously for easier reuse
									encode: false, // json encode the filter query
									local: true,   // defaults to false (remote filtering)
							
									// Filters are most naturally placed in the column definition, but can also be added here.
								filters: [
								{
									type: 'boolean',
									dataIndex: 'visible'
								}
								]
								}],					
						
								store: new Ext.data.Store({
									autoLoad:true,			
									
									groupField: 'CRDGRP',
										        
				  					proxy: {
											url: 'acs_regole_riordino.php?fn=get_json_data_dett&from_ws=Y',
											type: 'ajax',
											reader: {
									            type: 'json',
									            root: 'root'
									        },
									      extraParams: Ext.merge(Ext.merge(classi_extra_params, rec.data), {col_name: col_name}),  
									        
										},
					        			fields: ['CRGRPA', 'CRDGRP', 'CRART', 'CRDART', 'CRDTIV', 'CRDTFV', 'CRSOSP', 'CRESAU', 'CRDTIN', 'CRQGIAC', 'CRQCONS', 'CRVGIA', 'CRVCONS']

					    			}),
			    						
			    						
						        columns: [
						               {header   : 'Articolo', dataIndex: 'CRART', width: 120, filter: {type: 'string'}, filterable: true}
						             , {header   : 'Descrizione', dataIndex: 'CRDART', flex: 10, filter: {type: 'string'}, filterable: true}
						             , {header   : 'Data val. iniz', dataIndex: 'CRDTIV', width: 70, renderer: date_from_AS}					             
						             , {header   : 'Data val. fin', dataIndex: 'CRDTFV', width: 70, renderer: date_from_AS}					             
						             , {header   : 'Sosp', dataIndex: 'CRSOSP', width: 35}					             
						             , {header   : 'Esau', dataIndex: 'CRESAU', width: 35}
						             , {header   : 'Ind.Temp', dataIndex: 'CRDTIN', width: 70, renderer: date_from_AS}
						             , {header: 'Giacenza', width: 130, columns:[
										, {header   : 'Q.t&agrave;', dataIndex: 'CRQGIAC', width: 50, renderer: floatRenderer0, align: 'right'}						             
										, {header   : 'Valore', dataIndex: 'CRVGIAC', width: 50, renderer: floatRenderer0, align: 'right'}										
						             ]}
						             , {header: 'Consumi', width: 130, columns:[
										, {header   : 'Q.t&agrave;', dataIndex: 'CRQCONS', width: 50, renderer: floatRenderer0, align: 'right'}						             
										, {header   : 'Valore', dataIndex: 'CRVCONS', width: 50, renderer: floatRenderer0, align: 'right'}										
						             ]}						             					             					             
						          ]
						          
								  ,	viewConfig: {
									        //Return CSS class to apply to rows depending upon data values
									        getRowClass: function(record, index) {
									           if (record.get('CRSOSP') == 'S') return 'segnala_riga_rosso';	
									           if (record.get('CRESAU') == 'E') return 'segnala_riga_grigio';									           
									           if (record.get('CRESAU').trim() != '') return 'segnala_riga_giallo';
									         }   
									    }												    
						          
						          
						          })   
			
					});
			
					win_dett.show();					
					
											
				} //celldbclick
			},
		}    	
    							
	}        
        
     ]
   }     
        
        
        
        
	]
}

<?php
exit;
} // get_json_view



function chart_sql_where(){
 	global $_REQUEST;
	$sql_where = '';
	$parametri = array();
	
	if (strlen($_REQUEST['classe_g']) > 0)
		$sql_where .= " AND WSCLS1 = " . sql_t(trim($_REQUEST['classe_g']));
	if (strlen($_REQUEST['classe_c']) > 0)
		$sql_where .= " AND WSCLS2 = " . sql_t(trim($_REQUEST['classe_c']));
	
	switch ($_REQUEST['col_name']){
		case "V_S": $sql_where .= " AND WSTIPO = 'S'"; break;
		case "V_O": $sql_where .= " AND WSTIPO = 'O'"; break;
		case "V_D": $sql_where .= " AND WSTIPO = 'D'"; break;
		case "V_P": $sql_where .= " AND WSTIPO = 'P'"; break;
	}	
	
	if (strlen($_REQUEST['GRPFOR_COD']) > 0 && trim($_REQUEST['GRPFOR_COD']) != 'Totale')
		$sql_where .= " AND CRSEL1 = " . sql_t(trim($_REQUEST['GRPFOR_COD']));	
	
	
	$m_params = json_decode($_REQUEST['atp_setup_filter']);
	if (isset($m_params->f_gruppo)){
		$sql_where .= " AND CRSEL1 = ?";
		$parametri[] = $m_params->f_gruppo;
	}
	if (isset($m_params->f_sottogruppo)){
		$sql_where .= " AND CRSEL2 = ?";
		$parametri[] = $m_params->f_sottogruppo;
	}
	if (isset($m_params->f_gruppo_pianificazione)){
		$sql_where .= " AND CRGRPA = ?";
		$parametri[] = $m_params->f_gruppo_pianificazione;
	}
	if (isset($m_params->f_tipo_approv)){
		$sql_where .= " AND CRSWTR = ?";
		$parametri[] = $m_params->f_tipo_approv;
	}
	if (strlen($m_params->f_des_art) > 0){
		$sql_where .= " AND CRDART = ?";
		$parametri[] = $m_params->f_des_art;
	}
	if (strlen($m_params->f_cod_art) > 0){
		$sql_where .= " AND CRART = ?";
		$parametri[] = $m_params->f_cod_art;
	}
	if ($m_params->selected_id != null)
		$sql_where .= " AND CRCCON IN (". sql_t_IN($m_params->selected_id) . ")";

 return array("sql_where" => $sql_where, "parametri" => $parametri);	
}


//******************************************
//  GRAFICO: DATI: CICLO DI VITA
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_ciclo_di_vita'){
	global $cfg_mod_Admin, $cfg_mod_DeskAcq;

	$sql_adv = chart_sql_where();
	$sql_where = $sql_adv['sql_where'];
		
	if ($_REQUEST['from_ws'] == 'Y')
		$sql = "SELECT CRSOSP, CRESAU, COUNT(*) AS NREF
			FROM {$cfg_mod_Admin['file_WPI0WS0']} WS
			INNER JOIN {$cfg_mod_DeskAcq['file_regole_riordino']} CR
			ON WS.WSRFG1 = CR.CRART
			WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' {$sql_where}
			GROUP BY CRSOSP, CRESAU
			ORDER BY CRSOSP, CRESAU
			";
	else
		$sql = "SELECT CRSOSP, CRESAU, COUNT(*) AS NREF
				FROM {$cfg_mod_DeskAcq['file_regole_riordino']} CR
				WHERE 1=1 {$sql_where}
				GROUP BY CRSOSP, CRESAU
				ORDER BY CRSOSP, CRESAU
			";
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, $sql_adv['parametri']);

	$ret = array();
	$ret_tmp = array();	
	while ($r = db2_fetch_assoc($stmt)) {
		if ($r['CRSOSP'] == 'S')
			$ret_tmp['S'] += $r['NREF'];
		else 
			$ret_tmp[$r['CRESAU']] += $r['NREF'];
	}

	function decod_ciclo_vita($v){
		switch(trim($v)){
			case 'S':return 'Sospesi';
			case 'C': 	return 'Nuovi (incompleti)';
			case 'E': 	return 'In esaurimento';
			case 'F': 	return 'Nuovi (completi)';			
			case 'A': 	return 'In avviamento';			
			case 'R': 	return 'Riservati';			
			case '': 	return 'Attivi';			
			default: return $v;
		}
	}
	
	function decod_k_ciclo_vita($v){
		switch(trim($v)){
			case '': 	return 'Attivi';
			default: return $v;
		}
	}	
	
	foreach($ret_tmp as $k => $ar){
		$ret[] = array("des_name" => decod_ciclo_vita($k), "name" => decod_k_ciclo_vita($k), "referenze" => $ar);
	}
	
 echo acs_je($ret); 
 exit;
}



//******************************************
//  GRAFICO: DATI: TIPO RIORDINO
//******************************************
if ($_REQUEST['fn'] == 'get_json_data_chart_tipo_riordino'){
	global $cfg_mod_Admin, $cfg_mod_DeskAcq;

	$sql_adv = chart_sql_where();
	$sql_where = $sql_adv['sql_where'];
	
	if ($_REQUEST['from_ws'] == 'Y')	
		$sql = "SELECT WSTIPO, COUNT(*) AS NREF
				FROM {$cfg_mod_Admin['file_WPI0WS0']} WS
				INNER JOIN {$cfg_mod_DeskAcq['file_regole_riordino']} CR
				ON WS.WSRFG1 = CR.CRART
				WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' {$sql_where} AND CRSOSP <> 'S'
				GROUP BY WSTIPO
				ORDER BY WSTIPO
		";
	else
		$sql = "SELECT CRSWTR AS WSTIPO, COUNT(*) AS NREF
				FROM  {$cfg_mod_DeskAcq['file_regole_riordino']} CR
				WHERE 1=1 {$sql_where} AND CRSOSP <> 'S'
				GROUP BY CRSWTR
				ORDER BY CRSWTR
				";		

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, $sql_adv['parametri']);

	$ret = array();
	$ret_tmp = array();
	while ($r = db2_fetch_assoc($stmt)) {
			$ret_tmp[$r['WSTIPO']] += $r['NREF'];
	}

		function decod_tipo_riordino($v){
			switch(trim($v)){
				case 'O':	return 'Mto';
				case 'D': 	return 'Discreto';
				case 'S': 	return 'Mts';
				case 'P': 	return 'Mts a Programma';
				default: return $v;
			}
		}	
		
	foreach($ret_tmp as $k => $ar){
		$ret[] = array("name" => decod_tipo_riordino($k), "referenze" => $ar);
	}


		echo acs_je($ret);
		exit;
}



//******************************************
//  WINDOW GRUPPO FORNITURA & TIPOLOGIA (S - O - D)
//******************************************
if ($_REQUEST['fn'] == 'classe_tipo'){

$m_params = acs_m_params_json_decode();

$sql_where = '';
$parametri = array();				

$sql = "SELECT CRSEL1, CRDSE1, WSTIPO, SUM(WSSGIA) AS SOMGIA, SUM(WSSCON) AS SOMCON, COUNT(*) AS CNTREF
		FROM {$cfg_mod_Admin['file_WPI0WS0']} WS
		INNER JOIN {$cfg_mod_DeskAcq['file_regole_riordino']} CR
		ON WS.WSRFG1 = CR.CRART
		WHERE WSSEID = " . sql_t(session_id()) . " AND WSTP='AR' AND  WSCLS1 = '" . $m_params->classe_giac . "' AND WSCLS2 = '" . $m_params->classe_cons . "'
		GROUP BY CRSEL1, CRDSE1, WSTIPO
		ORDER BY CRDSE1
		";
		
$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt);

$array_art_tip = array();

//CREAZIONE ARRAY AD ALBERO (Gruppo Fornitura - Tipologia - Giacenza & Consumo & Referenze)
while ($r = db2_fetch_assoc($stmt)) {

	$liv0_v = trim($r['CRSEL1']);	
	$liv1_v = trim($r['WSTIPO']);

	$tmp_ar_id = array();
	$n_children = "children";

	// LIVELLO 0
	$s_ar = &$array_art_tip;
	$liv_c     = $liv0_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_1");
		$s_ar[$liv_c]["id"] = implode("|", $tmp_ar_id);
		$s_ar[$liv_c]["task"] = $r['CRDSE1'];
	}
	$s_ar = &$s_ar[$liv_c][$n_children];

	// LIVELLO 1
	$liv_c     = $liv1_v;
	$tmp_ar_id[] = $liv_c;
	if (is_null($s_ar[$liv_c])){
		$s_ar[$liv_c] = array("cod"=>$liv_c, "liv"=>"liv_2");
		$s_ar[$liv_c]["TOTGIA"] = $r['SOMGIA'];
		$s_ar[$liv_c]["TOTCON"] = $r['SOMCON'];
		$s_ar[$liv_c]["TOTREF"] = $r['CNTREF'];
		$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
	}
	$s_ar = &$s_ar[$liv_c];
}

//CREAZIONE RECORD S-D-O PER GRUPPO FORNITURA
$ar_tipo_riordino = array();
foreach ($array_art_tip as $ar){
	$ar_record = array();
	$ar_record['GRPFOR'] = $ar['task'];
	
	foreach ($ar['children'] as $ar_1){
		$ar_record[$ar_1['cod'] . '_GIAC'] = $ar_1['TOTGIA'];
		$ar_record[$ar_1['cod'] . '_CONS'] = $ar_1['TOTCON'];
		$ar_record[$ar_1['cod'] . '_REFE'] = $ar_1['TOTREF'];
	}
	
	$ar_tipo_riordino[] = $ar_record;
}

$ar_grid = acs_je($ar_tipo_riordino);

?>
{"success":true, "items": [
{
	xtype: 'panel',
	title: '',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		layout: 'fit',
		title: '',
		autoScroll: true,
		store: new Ext.data.Store({										        
			autoLoad:true,				        
  			data: <?php echo $ar_grid; ?>,
	        fields: [{name:'GRPFOR', type: 'string'},
					 {name:'S_GIAC', type: 'float'},
					 {name:'S_CONS', type: 'float'},
					 {name:'S_REFE', type: 'float'},
					 {name:'D_GIAC', type: 'float'},
					 {name:'D_CONS', type: 'float'},
					 {name:'D_REFE', type: 'float'},
					 {name:'O_GIAC', type: 'float'},
					 {name:'O_CONS', type: 'float'},
					 {name:'O_REFE', type: 'float'}]
	    }),				 		
		columns: [{header: 'Gruppo Fornitura', flex: 2, dataIndex: 'GRPFOR'},
				{header: 'S', columns:[
					{header: 'Giacenza', dataIndex: 'S_GIAC', flex: 1, align: 'right'},
					{header: 'Consumo', dataIndex: 'S_CONS', flex: 1, align: 'right'},
					{header: 'Referenze', dataIndex: 'S_REFE', flex: 1, align: 'right'}
				]},
				{header: 'D', columns:[
					{header: 'Giacenza', dataIndex: 'D_GIAC', flex: 1, align: 'right'},
					{header: 'Consumo', dataIndex: 'D_CONS', flex: 1, align: 'right'},
					{header: 'Referenze', dataIndex: 'D_REFE', flex: 1, align: 'right'}
				]},
				{header: 'O', columns:[
					{header: 'Giacenza', dataIndex: 'O_GIAC', flex: 1, align: 'right'},
					{header: 'Consumo', dataIndex: 'O_CONS', flex: 1, align: 'right'},
					{header: 'Referenze', dataIndex: 'O_REFE', flex: 1, align: 'right'}
				]}
    	]						
	}],
	listeners: {
		afterrender: function(comp){
			Ext.getBody().unmask();		 
		}			 					 
	}
}]
}


<?php
exit;
}// classe_tipo
?>

//***************************************
// Window Classi e % Giacenze e Consumi
//***************************************

{"success":true, "items": [
		{
			xtype: 'form',
			title: '',
			layout: {
				type: 'vbox', border: false,
									  pack: 'start', align: 'stretch',				
				tableAttrs: {
				style: {
					width: '100%'
				}
				},
			},			

			defaults: {
				bodyStyle:'padding:20px'
			},
			items: [
				{
					xtype: 'panel',
					flex: 1,
					layout: {
						type: 'vbox', border: false,
									  pack: 'start', align: 'stretch',						
						tableAttrs: {
						style: {
							width: '100%'
						}
						},
					},
					items: [
					
					
					 {
			            xtype: 'fieldset',
			            border: false,
			            style: 'padding-top: 2px; padding-bottom: 2px;',			            
						layout: {
							type: 'hbox', border: false,
							tableAttrs: {
							style: {
								width: '100%'
							}
							},
						},			            
			            items: [
							{xtype: 'label', width: 120, text: 'Classi ABC Giacenze'},
							{xtype: 'textfield', width: 80, name: 'GTA', maxLength: 1, allowBlank: false},
							{xtype: 'textfield', width: 80, name: 'GTB', maxLength: 1},
							{xtype: 'textfield', width: 80, name: 'GTC', maxLength: 1},
							{xtype: 'textfield', width: 80, name: 'GTD', maxLength: 1, allowBlank: false},
							{xtype: 'textfield', value: 'z', width: 80, name: 'GTZ', maxLength: 1, allowBlank: false},			            
			            ]
			          }, 					
					
					
					
					 {
			            xtype: 'fieldset',
			            border: false,
			            style: 'padding-top: 2px; padding-bottom: 2px;',			            
						layout: {
							type: 'hbox', border: false,
							tableAttrs: {
							style: {
								width: '100%'
							}
							},
						},			            
			            items: [
						{xtype: 'label', width: 120, text: '% Giacenze'},
						{xtype: 'numberfield', value: 0, minValue: 0, maxValue: 100, width: 80, name: 'GPA', allowBlank: false},
						{xtype: 'numberfield', minValue: 0, maxValue: 100, width: 80, name: 'GPB'},
						{xtype: 'numberfield', minValue: 0, maxValue: 100, width: 80, name: 'GPC'},
						{xtype: 'numberfield', value: 100, minValue: 100, maxValue: 100, width: 80, name: 'GPD', allowBlank: false},
						{xtype: 'numberfield', value: 0, minValue: 0, maxValue: 0, width: 80, name: 'GPZ', allowBlank: false},			            
			            ]
			          }, 							
					
					
					
					]
				}			
			

				,
				

				
				{
					xtype: 'panel',
					flex: 1,
					layout: {
						type: 'vbox', border: false,
									  pack: 'start', align: 'stretch',						
						tableAttrs: {
						style: {
							width: '100%'
						}
						},
					},
					items: [
					
					
					 {
			            xtype: 'fieldset',
			            style: 'padding-top: 2px; padding-bottom: 2px;',			            
			            border: false,
						layout: {
							type: 'hbox', border: false,
							tableAttrs: {
							style: {
								width: '100%'
							}
							},
						},			            
			            items: [
							{xtype: 'label', width: 120, text: 'Classi ABC Consumi'},
							{xtype: 'textfield', width: 80, name: 'CTA', maxLength: 1, allowBlank: false},
							{xtype: 'textfield', width: 80, name: 'CTB', maxLength: 1},
							{xtype: 'textfield', width: 80, name: 'CTC', maxLength: 1},
							{xtype: 'textfield', width: 80, name: 'CTD', maxLength: 1, allowBlank: false},
							{xtype: 'textfield', value: 'z', width: 80, name: 'CTZ', maxLength: 1, allowBlank: false},			            			            
			            ]
			          }, 					
					
					
					
					 {
			            xtype: 'fieldset',
			            style: 'padding-top: 2px; padding-bottom: 2px;',			            
			            border: false,
						layout: {
							type: 'hbox', border: false,
							tableAttrs: {
							style: {
								width: '100%'
							}
							},
						},			            
			            items: [
							{xtype: 'label', width: 120, text: '% Consumi'},
							{xtype: 'numberfield', value: 0, minValue: 0, maxValue: 100, width: 80, name: 'CPA', allowBlank: false},
							{xtype: 'numberfield', minValue: 0, maxValue: 100, width: 80,  name: 'CPB'},
							{xtype: 'numberfield', minValue: 0, maxValue: 100, width: 80, name: 'CPC'},
							{xtype: 'numberfield', value: 100, minValue: 100, maxValue: 100, width: 80, name: 'CPD', allowBlank: false},
							{xtype: 'numberfield', value: 0, minValue: 0, maxValue: 0, width: 80, name: 'CPZ', allowBlank: false}			            			            
			            ]
			          }, 							
					
					
					
					]
				}						
				
				

				
				
			],
			listeners: {					
			 	afterrender: function (comp) {
			 		Ext.getBody().unmask();
			 	}
			},
			buttons: [
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "CLASSI_ABC");  ?>
			   {
				text: 'Calcola',
	            iconCls: 'icon-gear-32',				
	            scale: 'large',				
				handler: function(){
					var form = this.up('form').getForm();
                    var form_values = form.getValues();
					if(form.isValid()){
						Ext.MessageBox.show({
							   msg: 'Passo 1/4: Inizializzazione classi ABC ...',
							   progressText: 'Running...',
							   width:500,						   
							   wait:true, 
							   waitConfig: {interval:200},
							   icon:'ext-mb-download',
						});
						
						//carico la form dal json ricevuto da php
						// PASSO 1 ***********************
						Ext.Ajax.request({
								url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=passo_1',
								method     : 'POST',
								timeout: 120000,
								waitMsg    : 'Data loading',
								jsonData: <?php echo acs_je(acs_raw_post_data()) ?>,
								success : function(result, request){
									var text = Ext.MessageBox.msg;
									text.setValue('Passo 2/4: Calcolo classe ABC giacenze ...');
									// PASSO 2 ***********************
									Ext.Ajax.request({
											url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=passo_2',
											method     : 'POST',
											timeout: 120000,
											waitMsg    : 'Data loading',
											jsonData: form_values,
											success : function(result, request){
												var text = Ext.MessageBox.msg;
												text.setValue('Passo 3/4: Calcolo classe ABC consumi ...');
												// PASSO 3 ***********************
												Ext.Ajax.request({
														url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=passo_3',
														method     : 'POST',
														timeout: 120000,
														waitMsg    : 'Data loading',
														jsonData: form_values,
														success : function(result, request){
															var text = Ext.MessageBox.msg;
															text.setValue('Passo 4/4: Assegnazione classe ABC ...');
															// PASSO 4 ***********************
															Ext.Ajax.request({
																	url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=passo_4',
																	method     : 'POST',
																	timeout: 120000,
																	waitMsg    : 'Data loading',
																	jsonData: form_values,
																	success : function(result, request){
																		var text = Ext.MessageBox.msg;
																		text.setValue('Passo 1/4: Eliminazione e Nuovo inserimento...Completato!<br>Passo 2/4: Calcolo Progressivo Giacenze...Completato!<br>Passo 3/4: Calcolo Progressivo Consumi...Completato!<br>Passo 4/4: Aggiornamento Classi...Completato!');
																		//chiudo il msg box di attesa
																		Ext.MessageBox.hide();
																	},
																	failure    : function(result, request){
																		Ext.Msg.alert('Message', 'No data to be loaded');
																	}
															});
														},
														failure    : function(result, request){
															Ext.Msg.alert('Message', 'No data to be loaded');
														}
												});
											},
											failure    : function(result, request){
												Ext.Msg.alert('Message', 'No data to be loaded');
											}
									});
								},
								failure    : function(result, request){
									Ext.Msg.alert('Message', 'No data to be loaded');
								}
						});
					}
				}			
			},{
				text: 'Visualizza',
	            scale: 'large',
	            iconCls: 'icon-folder_search-32',	            				
				handler: function(){
							ar_params = {};
							ar_params['form_input'] = <?php echo acs_je($m_params); ?>;
				
					acs_show_win_std('Analisi incrociata ABC giacenze/consumi', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_view', Ext.encode(ar_params), 1070, 615, null, 'icon-puzzle-16', 'N');
				}
			}]
        }
	]
}







<?php


//**************************
//  FUNZIONI
//*************************

//Trovare le corrispondenze giacenze e consumi (e percentuali) all'interno celle tabella
function acquisisci_giac_cons($ar_giac_cons, $cls1_giac, $cls2_cons, $tipo){
	$esito = '';
	if($ar_giac_cons[$cls1_giac][$cls2_cons] != null){
		$stringa_giac = $ar_giac_cons[$cls1_giac][$cls2_cons]['VGIAC'];
		$stringa_cons = $ar_giac_cons[$cls1_giac][$cls2_cons]['VCONS'];
		$stringa_refe = $ar_giac_cons[$cls1_giac][$cls2_cons]['NUMREF'];
		$perc_giac = $ar_giac_cons[$cls1_giac][$cls2_cons]['PRCGIA'];
		$perc_cons = $ar_giac_cons[$cls1_giac][$cls2_cons]['PRCCON'];
		$perc_refe = $ar_giac_cons[$cls1_giac][$cls2_cons]['PRCREF'];
	}
	
	if ($tipo == 'giacenza')
		$esito = $stringa_giac;
	else if ($tipo == 'consumo')
		$esito = $stringa_cons;
	else if ($tipo == 'referenze')
		$esito = $stringa_refe;
	
	if ($tipo == 'perc_giacenza')
		$esito = $perc_giac;
	else if ($tipo == 'perc_consumo')
		$esito = $perc_cons;
	else if ($tipo == 'perc_referenze')
		$esito = $perc_refe;	
		
	return $esito;	
}

//Costruzione where e parametri SQL da file regole riordino
function crea_where_param_Rriordino($m_params, $clausola){

	$sql_where = '';
	$parametri = array();

	// SQL WHERE Filtri Regole Riordino Fornitore
	if (isset($m_params->f_gruppo)){
		$sql_where .= " AND CRSEL1 = ?";
		$parametri[] = $m_params->f_gruppo;
	}
	if (isset($m_params->f_sottogruppo)){
		$sql_where .= " AND CRSEL2 = ?";
		$parametri[] = $m_params->f_sottogruppo;
	}
	if (isset($m_params->f_gruppo_pianificazione)){
		$sql_where .= " AND CRGRPA = ?";
		$parametri[] = $m_params->f_gruppo_pianificazione;
	}
	if (isset($m_params->f_tipo_approv)){
		$sql_where .= " AND CRSWTR = ?";
		$parametri[] = $m_params->f_tipo_approv;
	}
	if (strlen($m_params->f_des_art) > 0){
		$sql_where .= " AND CRDART = ?";
		$parametri[] = $m_params->f_des_art;
	}
	if (strlen($m_params->f_cod_art) > 0){
		$sql_where .= " AND CRART = ?";
		$parametri[] = $m_params->f_cod_art;
	}
	if ($m_params->selected_id != null)
		$sql_where .= " AND CRCCON IN (". sql_t_IN($m_params->selected_id) . ")";

	if($clausola == 'where')
		return $sql_where;
	else
		return $parametri;
	
}

function get_perc_cls1_by_val($v){ 
 global $f_input;

 if ( $f_input['GTA'] == $v) return $f_input['GPA'] . "%"; 
 if ( $f_input['GTB'] == $v) return $f_input['GPB'] . "%";
 if ( $f_input['GTC'] == $v) return $f_input['GPC'] . "%";
 if ( $f_input['GTD'] == $v) return $f_input['GPD'] . "%";
 if ( $f_input['GTE'] == $v) return $f_input['GPE'] . "%";
 if ( $f_input['GTF'] == $v) return $f_input['GPF'] . "%";
 if ( $f_input['GTZ'] == $v) return $f_input['GPZ']; 
}

function get_perc_cls2_by_val($v){
	global $f_input;

	if ( $f_input['CTA'] == $v) return $f_input['CPA'] . "%";
	if ( $f_input['CTB'] == $v) return $f_input['CPB'] . "%";
	if ( $f_input['CTC'] == $v) return $f_input['CPC'] . "%";
	if ( $f_input['CTD'] == $v) return $f_input['CPD'] . "%";
	if ( $f_input['CTE'] == $v) return $f_input['CPE'] . "%";
	if ( $f_input['CTF'] == $v) return $f_input['CPF'] . "%";
	if ( $f_input['CTZ'] == $v) return $f_input['CPZ'];
}

function get_cls1_by_val($v){
	global $f_input;

	if ( $f_input['GTA'] == $v) return 'GPA';
	if ( $f_input['GTB'] == $v) return 'GPB';
	if ( $f_input['GTC'] == $v) return 'GPC';
	if ( $f_input['GTD'] == $v) return 'GPD';
	if ( $f_input['GTE'] == $v) return 'GPE';
	if ( $f_input['GTF'] == $v) return 'GPF';
	if ( $f_input['GTZ'] == $v) return 'GPZ';
}

function get_cls2_by_val($v){
	global $f_input;

	if ( $f_input['CTA'] == $v) return 'CPA';
	if ( $f_input['CTB'] == $v) return 'CPB';
	if ( $f_input['CTC'] == $v) return 'CPC';
	if ( $f_input['CTD'] == $v) return 'CPD';
	if ( $f_input['CTE'] == $v) return 'CPE';
	if ( $f_input['CTF'] == $v) return 'CPF';
	if ( $f_input['CTZ'] == $v) return 'CPZ';
}

?>