<?php

require_once "../../config.inc.php";


$main_module = new DeskAcq();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_PVEN'));

if ($_REQUEST['fn'] == 'exe_conferma_articoli'){

	$m_params = acs_m_params_json_decode();


	$articoli= $m_params->articoli;
	
	foreach ($articoli as $v){
		
		if(isset($v)){

		$k_ordine = $m_params->k_ordine;
		
		if($v->prezzo == ""){
			$v->prezzo=0;
		}
		
		$sh = new SpedHistory($main_module);
		$sh->crea(
		    'pers',
		    array(
		        "messaggio"	=> $m_params->msg_ri,
		        "k_ordine"	=> $k_ordine,
		        "vals" => array(
		            "RIART" => $v->codice,
		            "RIQTA" => sql_f($v->quant),
		            "RIDART" =>$v->descr,
		            "RIIMPO" => sql_f($v->prezzo)
		            
		        )
		        
		    )
		    );
		
		
		}

	   }

	exit;
}



if ($_REQUEST['fn'] == 'get_json_data_articoli'){

	$m_params = acs_m_params_json_decode();

	
	$codice= $m_params->open_request->codice;
	
	
	if($codice == ''){
		$where_art .= "";
	}else{
		$where_art .= "AND ARART LIKE '{$codice}%'";
	}

	
	$sql="SELECT ARART, ARDART, ARVDI2
			FROM {$cfg_mod_DeskAcq['file_anag_art']} AR 
			WHERE ARDT = '$id_ditta_default' 
				  AND ARFOR1 <> 0  $where_art
			";
//print_r($sql);

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['codice']	    = trim($row['ARART']);
		$nr['articolo']  	= trim($row['ARDART']);
		$ar[] = $nr;

	}

	echo acs_je($ar);
	exit;
}



$m_params = acs_m_params_json_decode();
?>


{"success":true, "items": [

			{
			xtype: 'grid',
	         loadMask: true,	
	         plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
		      
		   features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
			
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_articoli', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['codice', 'articolo', 'quant', 'prezzo']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Codice',
	                dataIndex: 'codice',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	                },
	                {
	                header   : 'Articolo',
	                dataIndex: 'articolo',
	                flex: 1,
					filter: {type: 'string'}, filterable: true,
	                
	                },
	                {
	                header   : 'Quantit&agrave;',
	                dataIndex: 'quant',
	                flex: 1,	                
	                align: 'right',
	                editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }
	                },  
	                {
	                header   : 'Prezzo',
	                dataIndex: 'prezzo',
	                align: 'right',
	                flex: 1,
	                editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }
	                }
	               
	           
	        
	         ] ,
	         
	         
	          dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-print-32',
                     text: 'Conferma articoli',
			            handler: function() {
			        
			           var loc_win = this.up('window');
			            var grid = this.up('grid');
			            all_rows =   grid.getStore().getRange();
                        list_rows = [];
                        
                      
                        Ext.each(all_rows, function(item) {
    							// add the fields that you want to include
    						
    						if(item.get('quant')>0)	{
    						
    							
    							
    						var Obj = {
       							 codice:   item.get('codice'),
       							 quant:    item.get('quant'),
       							 prezzo:   item.get('prezzo')
    								};
    						 }		
    					
   						list_rows.push(Obj); // push this to the array
						}, this);
						
						
					
			            
			                  Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_articoli',
									        method     : 'POST',
						        			jsonData: {
						        				articoli: list_rows,
						        				k_ordine: <?php echo j($m_params->k_ordine) ?>,
						        				msg_ri: <?php echo j($m_params->msg_ri) ?>
											},							        
									        success : function(result, request){
						            			loc_win.fireEvent('afterAddArt', loc_win);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });	
                   
	        				
			            }
			     }
			     
			     
			     ]
		   }]
	   		
			
		}//grid
		 
			
		
 				
	
     ]
        
 }