<?php
require_once("../../config.inc.php");

$main_module = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


function get_sql_where($form_values){
   
    $sql_where = '';
    $sql_where.= sql_where_by_combo_value('TDSTAT', $form_values->f_stato_ordine);
    $sql_where.= sql_where_by_combo_value('TDTPDO', $form_values->f_tipo_ordine);
    $sql_where.= sql_where_by_combo_value('TDCCON', $form_values->f_fornitore);
    $sql_where.= sql_where_by_combo_value('TDREFE', $form_values->f_referente);
    $sql_where.= sql_where_by_combo_value('TDDTGE', $form_values->f_utente_gen);
  
    if (strlen($form_values->f_data_dal) > 0)
        $sql_where .= " AND TDDTEP >= {$form_values->f_data_dal}";
    if (strlen($form_values->f_data_al) > 0)
        $sql_where .= " AND TDDTEP <= {$form_values->f_data_al}";
    
    if (strlen($form_values->f_data_gen) > 0)
        $sql_where .= " AND TDDTGE = {$form_values->f_data_gen}";
   
    return $sql_where;
}

// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
 
    $sql_where = get_sql_where($m_params->form_values);
  
    $pq_where = "";
    $pq_where.= sql_where_by_combo_value('PQUTEN', $form_values->f_utente_his);
    $pq_where.= sql_where_by_combo_value('PQFUNZ', $form_values->f_funzione);
    
    if (strlen($form_values->f_data_his_dal) > 0)
        $pq_where .= " AND PQDTGE >= {$form_values->f_data_his_dal}";
    if (strlen($form_values->f_data_his_al) > 0)
        $pq_where .= " AND PQDTGE <= {$form_values->f_data_his_al}";
    
    $sql = "SELECT TD.*, CF_FORNITORE.CFRGS1 AS D_CF, SMA1, EDI, RA.N_ROW AS N_AL,
            F_RICHIESTO, F_DISPONIBILE, F_ERRORE 
            FROM {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD
            LEFT OUTER JOIN (
              SELECT
              SUM(CASE WHEN PQFUNZ = '/IN' THEN 1 ELSE 0 END) AS SMA1,
              SUM(CASE WHEN PQFUNZ = '@AO' THEN 1 ELSE 0 END) AS EDI, 
              PQDT, PQTIDO, PQINUM, PQAADO, PQNRDO
              FROM {$cfg_mod_DeskAcq['file_cronologia_ordine']} 
              WHERE PQDT = '{$id_ditta_default}' {$pq_where}
              GROUP BY PQDT, PQTIDO, PQINUM, PQAADO, PQNRDO
              ) PQ
              ON PQ.PQDT=TD.TDDT AND PQ.PQTIDO=TD.TDTIDO AND PQ.PQINUM=TD.TDINUM AND PQ.PQAADO=TD.TDAADO AND PQ.PQNRDO=TD.TDNRDO
            LEFT OUTER JOIN (
               SELECT COUNT(*) AS N_ROW, 
               SUM(CASE WHEN RAFLAG = 'R' THEN 1 ELSE 0 END) AS F_RICHIESTO,
               SUM(CASE WHEN RAFLAG = 'D' THEN 1 ELSE 0 END) AS F_DISPONIBILE,
               SUM(CASE WHEN RAFLAG = 'E' THEN 1 ELSE 0 END) AS F_ERRORE,
               RADT, RATIDO, RAINUM, RAAADO, RANRDO
               FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA 
               GROUP BY RADT, RATIDO, RAINUM, RAAADO, RANRDO) RA
            ON TD.TDDT=RA.RADT AND TD.TDTIDO = RA.RATIDO AND TD.TDINUM = RA.RAINUM AND TD.TDAADO = RA.RAAADO AND TD.TDNRDO = RA.RANRDO            
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF_FORNITORE
            ON CF_FORNITORE.CFDT = TD.TDDT AND CF_FORNITORE.CFCD = TD.TDCCON
            WHERE TDDT='{$id_ditta_default}' AND TDTIDO = 'AO' {$sql_where}
            ORDER BY TDDTGE DESC FETCH FIRST 50 ROWS ONLY";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    
    while ($r = db2_fetch_assoc($stmt)) {
        $nr = array();
        $nr['k_ordine']    = implode("_", array($r['TDDT'], $r['TDTIDO'], $r['TDINUM'], $r['TDAADO'], sprintf("%06s", $r['TDNRDO'])));
        $nr['numero']      = $r['TDAADO']."_".sprintf("%06s", $r['TDNRDO']);
        $nr['data']        = $r['TDDTRG'];
        $nr['evasione']    = $r['TDDTEP'];
        $nr['tipo']        = $r['TDTPDO'];
        $nr['stato']       = $r['TDSTAT'];
        $nr['fornitore']   = "[".trim($r['TDCCON'])."] ".trim($r['D_CF']);
        $nr['cod_forn']    = $r['TDCCON'];
        $nr['riferimento'] = $r['TDVSRF'];
        $nr['sma1']        = $r['SMA1'];
        $nr['edi']         = $r['EDI'];
        $nr['allegati']    = $r['N_AL'];
        $nr['a_richiesto']   =  $r['F_RICHIESTO'];
        $nr['a_disponibile'] =  $r['F_DISPONIBILE'];
        $nr['a_errore']      =  $r['F_ERRORE'];
        $ar[] = $nr;
    }

    echo acs_je($ar);
    exit();
}

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------------
$user_to = array();
$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
    $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
    
$ar_users_json = acs_je($user_to);
 ?>
{"success":true, 
	m_win: {
		title: 'Parametri ordini fornitore',
		width: 600, height: 380,
		iconCls: 'icon-label_blue_new-16'
	}, 
	"items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [
             {
				 xtype: 'fieldcontainer',
				 layout: { 	type: 'hbox',
					    pack: 'start',
					    align: 'stretch'},						
			     items: [
						{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data generazione'
						   , name: 'f_data_gen'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						},
						{
			            xtype: 'combo',
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            labelAlign : 'right',
			            fieldLabel: 'Utente generazione',
			            queryMode: 'local',
			            selectOnTab: false,
                        anchor: '-15',
			            name: 'f_utente_gen',
			            allowBlank: true,
						forceSelection: true,			            
						labelWidth : 120
			        }
				]},
                 { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data evasione da'
						  // , labelWidth: 30
						   , name: 'f_data_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , labelWidth : 120
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					},
					{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ 
						 
						    {
            				name: 'f_tipo_ordine',
            				xtype: 'combo',
                        	anchor: '-15',
            				fieldLabel: 'Tipi ordine',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				store: {
            					autoLoad: true,
            					editable: false,
            					autoDestroy: true,
            				    fields: [{name:'id'}, {name:'text'}],
            				    data: [
                                          <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'AO', null, null, 0, "", 'Y'), ""); ?>
            					    ]
            				},listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
            			},{
							name: 'f_stato_ordine',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Stato ordine',
							labelWidth : 120,
							labelAlign : 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?>	
								    ] 
							}						 
						}]},
						{
                    xtype: 'combo',
        			name: 'f_fornitore',
        			fieldLabel: 'Fornitore',
        			minChars: 2,			
                    anchor: '-15',
                    store: {
                    	pageSize: 1000,
                    	proxy: {
    		            type: 'ajax',
    		            url : 'acs_get_select_json.php?select=search_for_anag',
    		            reader: {
    		                type: 'json',
    		                root: 'root',
    		                totalProperty: 'totalCount'
    		            }
    		        },       
    				fields: ['cod', 'descr'],		             	
                },
                        
    			valueField: 'cod',                        
                displayField: 'descr',
                typeAhead: false,
                hideTrigger: true,
                listConfig: {
                    loadingText: 'Searching...',
                    emptyText: 'Nessun fornitore trovato',
                    // Custom rendering template for each item
                    getInnerTpl: function() {
                        return '<div class="search-item">' +
                            '<h3><span>{descr}</span></h3>' +
                            '{cod}' +
                        '</div>';
                    }                
                    
                        },
            
                    },
                     	{
            				name: 'f_referente',
            				xtype: 'combo',
                        	anchor: '-15',
            				fieldLabel: 'Referente',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				store: {
            					autoLoad: true,
            					editable: false,
            					autoDestroy: true,
            				    fields: [{name:'id'}, {name:'text'}],
            				    data: [
                                          <?php echo acs_ar_to_select_json(find_TA_sys('BREF', null, null, null, null, null, 0, "", 'Y'), ""); ?>
            					    ]
            				},listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
            			},
            			
            			 { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [
						{
            				name: 'f_funzione',
            				xtype: 'combo',
                        	anchor: '-15',
            				fieldLabel: 'Funzione history',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				store: {
            					autoLoad: true,
            					editable: false,
            					autoDestroy: true,
            				    fields: [{name:'id'}, {name:'text'}],
            				    data: [
                                          <?php echo acs_ar_to_select_json(find_TA_sys('QUPR', null, null, null, null, null, 0, "", 'Y'), ""); ?>
            					    ]
            				},listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
                    			},
                    			{
        			            xtype: 'combo',
        			            store: Ext.create('Ext.data.ArrayStore', {
        			                fields: [ 'cod', 'descr' ],
        			                data: <?php echo $ar_users_json ?>
        			            }),
        			            displayField: 'descr',
        			            valueField: 'cod',
        			            labelAlign : 'right',
        			            fieldLabel: 'Utente history',
        			            queryMode: 'local',
        			            selectOnTab: false,
                                anchor: '-15',
        			            name: 'f_utente_his',
        			            allowBlank: true,
        						forceSelection: true,			            
        						labelWidth : 120
        			        }
						
						]},{ 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data history da'
						  // , labelWidth: 30
						   , name: 'f_data_his_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_his_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , labelWidth : 120
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					},
            ],
            
			buttons: [{
		            text: 'Visualizza',
			       iconCls: 'icon-folder_search-32',            
			        scale: 'large',		            
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_panel', null, {
        		        form_values: form.getValues()}, null, null);
        		        this.up('window').close();
			        
		            }
		        }
	        ]
	  }
]}
<?php 
	exit;
}

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_panel'){ 
//----------------------------------------------------------------------
?>
 
 {"success":true, "items": [
        {
    xtype: 'panel',    
	title: 'Entries',
	closable: true,
	layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},    
    items: [
    
    {
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    stateful: true,
	    stateId: 'wizard_mts',
	    stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ], 	
	    
        dockedItems: [{
            xtype: 'toolbar'
            , dock: 'top'
            , items: [{xtype: 'label', cls: 'strong', text: 'Avanzamento ordini fornitore'}
           	]
		}],	                    
		
	    selType: 'cellmodel',
		
		store: {
			xtype: 'store',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
				method: 'POST',
				type: 'ajax',
	            timeout: 2400000,
	            autoSave: true,
	            autoSync: true,
	            actionMethods: {
							     read: 'POST'
							    },
				extraParams:  <?php echo acs_je($m_params); ?> ,
								
			    doRequest: personalizza_extraParams_to_jsonData, 								
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				}				
			},
				
			fields: ['numero', 'data', 'stato', 'tipo', 'fornitore', 'riferimento', 'sma1', 'edi', 'cod_forn'
			        ,'allegati', 'a_richiesto', 'a_disponibile', 'a_errore', 'k_ordine', 'evasione',  'ut_his']
						
		}, //store	
		<?php $sma1     = "<img src=" . img_path("icone/48x48/blog_compose.png") . " height=20>"; ?>
		<?php $edi      = "<img src=" . img_path("icone/48x48/save.png") . " height=20>"; ?>    
	    <?php $allegato = "<img src=" . img_path("icone/48x48/attachment.png") . " height=20>"; ?>    
	    			 		
		columns: [
					{header: 'Numero', width: 100, dataIndex: 'numero', filter: {type: 'string'}}
				  , {header: 'Data',  width: 70, dataIndex: 'data', renderer : date_from_AS, filter: {type: 'string'}}
				  , {header: 'Tp', width: 30, dataIndex: 'tipo', filter: {type: 'string'}, tooltip: 'Tipo'}
				  , {header: 'St', width: 30, dataIndex: 'stato', filter: {type: 'string'}, tooltip: 'Stato'}
				  , {header: 'Fornitore', flex: 1, dataIndex: 'fornitore', filter: {type: 'string'}}
				  , {header: 'Riferimento', flex: 1, dataIndex: 'riferimento', filter: {type: 'string'}}
				  , {text: '<?php echo $sma1; ?>', width: 30, dataIndex: 'sma1', tooltip: 'Invio conferma tramite SMA1',
				     renderer: function(value, metaData, record){
				       if(record.get('sma1') > 0) return '<img src=<?php echo img_path("icone/48x48/blog_compose.png") ?> width=15>';
				     }}
				  , {text: '<?php echo $edi; ?>', width: 30, dataIndex: 'edi', tooltip: 'Invio Ordine acquisto EDI',
				     renderer: function(value, metaData, record){
				       if(record.get('edi') > 0) return '<img src=<?php echo img_path("icone/48x48/save.png") ?> width=15>';
				     }}
				  , {text: '<?php echo $allegato; ?>', dataIndex: 'allegati', width: 30, tooltip: 'Allegati',
			          renderer: function(value, metaData, record){
			           if(record.get('a_richiesto') > 0) metaData.tdCls += ' sfondo_giallo';
			           if(record.get('a_disponibile') > 0) metaData.tdCls += ' sfondo_verde';
			           if(record.get('a_errore') > 0) metaData.tdCls += ' sfondo_rosso';
				       if(record.get('allegati') > 0) return '<img src=<?php echo img_path("icone/48x48/attachment.png") ?> width=15>';
				      
				   }}
				    , {header: 'Evasione', width: 80, dataIndex: 'evasione', filter: {type: 'string'}, renderer : date_from_AS,}
				    , {header: 'Utente history', width: 100, dataIndex: 'ut_his', filter: {type: 'string'}}
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    },
		    
		    
         listeners: {
          celldblclick: {								
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			  
			  	col_name = iView.getGridColumns()[iColIdx].dataIndex;					  	
			  	rec = iView.getRecord(iRowEl);
			  	iEvent.stopEvent();
			  	
			  	if(col_name == 'allegati')
			  		acs_show_win_std('Elenco allegati per riga', 'acs_allegati_fornitore.php?fn=open_win', {codice: rec.get('cod_forn'), k_ordine : rec.get('k_ordine')}, 500, 400, null, 'icon-attachment-16');
					  	
			}
					  	
		 },
         
   		   itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
		  		var grid = this;
		  													  
			    var voci_menu = [];
				voci_menu.push({
		         		text: 'Visualizza righe',
		        		iconCls : 'icon-folder_search-16',          		
		        		handler: function() {
		        			var k_ordine = rec.get('k_ordine');
		        			acs_show_win_std('Righe ordine', <?php echo acs_url('module/desk_acq/acs_get_order_rows.php') ?>, {k_ord: k_ordine, dtep: rec.get('data'), show_in_fat : 'Y'}, 1150, 480);        	
		        		}
		    	});
				    	
	    		 var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	 
						  
			}
	         
         }		    
		    
    							
	}    
    
    ] //items
  } //main panel 
 ]}
 
<?php 

exit;
}