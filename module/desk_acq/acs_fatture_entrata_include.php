<?php

function somma_valori_T(&$ar, $r){
	global $s;


	if($r['TFFG01'] != 'Y' || $ar['liv'] == 'liv_3'){
		$ar['fat_merce'] += $r['TFINFI'];
		$ar['fat_imp'] += $r['TFTIMP'];
		$ar['fat_totd'] += $r['TFTOTD'];
	}

	if($ar['liv'] == 'liv_3'){

		$ar['res_merce'] = $ar['fat_merce'] - $ar['ddt_merce'];
		$ar['res_imp'] =  $ar['fat_imp'] - $ar['ddt_imp'];
		$ar['res_totd'] =  $ar['fat_totd'] -$ar['ddt_totd'];
	  

	}else{
		if($r['TFAADO'] == 0){
			$ar['res_merce'] += $r['TFINFI'];
			$ar['res_imp'] += $r['TFTIMP'];
			$ar['res_totd'] += $r['TFTOTD'];
		}
	}

	return $ar;

}


function somma_valori_R(&$ar, $r){
	global $s;

	$ar['ddt_merce'] += $r['RFINFI'];
	$ar['ddt_imp'] += $r['RFTIMP'];
	$ar['ddt_totd'] += $r['RFTOTD'];

	if($ar['liv'] == 'liv_3'){
		$ar['res_merce'] = $ar['fat_merce'] - $ar['ddt_merce'];
		$ar['res_imp'] =  $ar['fat_imp'] - $ar['ddt_imp'];
		$ar['res_totd'] =  $ar['fat_totd'] -$ar['ddt_totd'];
	}

	if($ar['liv'] == 'liv_3' && $ar['fattura0'] == 'Y'){
		$ar['res_merce'] += $r['TFINFI'];
		$ar['res_imp'] += $r['TFTIMP'];
		$ar['res_totd'] += $r['TFTOTD'];
	}

	$ar['p_red'] += $r['RFFG04'];
	return $ar;
}



function parametri_sql_where($form_values){
    
    global $s, $id_ditta_default;
   
    $sql_where.= sql_where_by_combo_value('TFCCON', $form_values->f_forn);
    
    if(isset($form_values->f_stato_fat))
        $sql_where.= sql_where_by_combo_value('TFSTAT', $form_values->f_stato_fat);
    if(isset($form_values->f_stato_ddt))
        $sql_where.= sql_where_by_combo_value('RFSTAT', $form_values->f_stato_ddt);
    $sql_where .= " AND TFCCON <> ''";
    $sql_where .= " AND TFDT='{$id_ditta_default}' ";
    
    //controllo data
    if (strlen($form_values->f_data_in) > 0)
        $sql_where .= " AND TFDTRG >= {$form_values->f_data_in}";
    if (strlen($form_values->f_data_fin) > 0)
        $sql_where .= " AND TFDTRG <= {$form_values->f_data_fin}";
    if (strlen($form_values->f_anno) > 0)
        $sql_where .= " AND TFAARG = {$form_values->f_anno}";
    if (strlen($form_values->f_mese) > 0)
        $sql_where .= " AND TFMMRG = {$form_values->f_mese}";
    if ($form_values->f_filtra_fatture == 'Y')
        $sql_where .= " AND TFFG03 = 'Y' ";
    if ($form_values->f_filtra_fatture == 'N')
        $sql_where .= " AND TFFG03 = '' ";
    if ($form_values->f_solo_attese == 'Y')
        $sql_where .= " AND TFAADO = '0' ";
    if ($form_values->f_solo_var == 'Y')
        $sql_where .= " AND TFVVAR > 0 ";
    if ($form_values->f_solo_scos == 'Y')
        $sql_where .= " AND TFTOTC - TFTOTD > 0  ";
        
    if ($form_values->f_solo_con_anomalie == 'Y') {
        $sql_where .= " AND TFDOCU IN (
        SELECT DISTINCT RFDOCU
        FROM {$cfg_mod_DeskAcq['file_righe_fatture']}
        WHERE RFFG04 = 'Y'
        )
        ";
    }
        
    if ($form_values->report_ddt_variazioni_attese == 'Y') {
        $sql_where .= " AND tfprog='NOTEATT' ";
    } else {
        //Questo e' il default tranne il report ddt attesi. Giusto????
        $sql_where .= " AND TFFU01 ='' ";
    }
    
    if ($form_values->f_non_cont == 'Y')
        $sql_where .= " AND TFFLCO ='Y' AND TFPROC = 0";
    
    
  /*  if ($form_values->f_solo_con_note_credito_attese_aperte == 'Y') {
        //solo fornitori con note di credito attese in stato aperto
        $sql_where .= " AND TFCCON IN (
        SELECT DISTINCT TFCCON
        FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
        WHERE TFFU01='N' AND TFFLAV<>9
        )
        ";
    }
        */                        
            
   return $sql_where;
}

function get_categoria($filtri, $from_report = 'N'){
    
    $main_module=new DeskAcq();
    global $cfg_mod_DeskAcq, $conn, $id_ditta_default;
    
    /*print_r($filtri);
    exit;*/
        
    if(isset($filtri['anno'])){
        $sql_where .= "AND TFAARG = {$filtri['anno']} ";
    }
    
    if(isset($filtri['mese'])){
        $sql_where .= "AND TFMMRG = {$filtri['mese']} ";
    }
    
    if(isset($filtri['fornitore'])){
        $sql_where .= "AND TFCCON = {$filtri['fornitore']} ";
    }
    
    if(isset($filtri['form_values'])){
        $sql_where .= parametri_sql_where($filtri['form_values']);
    }
    
    if(isset($filtri['documento'])){
        $sql_where .= "AND TFDOCU = '{$filtri['documento']}' ";
    }
    
    if($from_report == 'N'){
        $sql_select = " RD.RDTBCO AS RDTBCO, RDART, RDDART, RDTIOR, RDTIDO";
        $sql_group_by = " RDTBCO, RDART, RDDART, RDTIOR, RDTIDO";
    }else{
        $sql_select = " RD.RDTBCO AS RDTBCO";
        $sql_group_by = " RDTBCO";
    }
    
    //query anno mese fornitore categoria
    $sql= "SELECT COUNT(DISTINCT RFNRDE) AS C_ROW, SUM(RT.RTINFI) AS RTINFI, {$sql_select}
    FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF
    LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_fatture']} RF
    ON TF.TFDOCU=RF.RFDOCU
    LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
    ON RD.RDDT = RF.RFDT AND RD.RDTIDO = RF.RFTIDE AND RD.RDINUM = RF.RFINUE AND RD.RDAADO = RF.RFAADE AND RD.RDNRDO = RF.RFNRDE
    LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest_valuta']} RT
    ON RT.RTDT = RD.RDDT AND RT.RTTIDO = RD.RDTIDO AND RT.RTINUM = RD.RDINUM AND RT.RTAADO = RD.RDAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RT.RTVALU='EUR'
    WHERE TFFU01 ='' AND TFFU02 = '' AND RD.RDTISR = '' AND RD.RDSRIG = 0 AND RDTPRI <> 'RA' AND RF.RFSTAT <> 'NF' $sql_where
    GROUP BY {$sql_group_by}";
    
    $stmt = db2_prepare($conn, $sql);
   
    
    return $stmt;
    
}

function crea_ar_tree_fatture_entrata($node, $form_values, $forza_generazione_completa = 'N', $disabled_comment = 'N', $ddt_nf = 'N'){

	$main_module=new DeskAcq();
	global $cfg_mod_DeskAcq, $cfg_mod_Spedizioni, $conn, $id_ditta_default;
	
	
	$ar = array();
	$ret = array();
	
	$sql_where .= parametri_sql_where($form_values);
	
	
	if (strlen($form_values->anno_mese_cliente) > 0) {
	    
		$anno_mese_cliente_exp = explode("|", $form_values->anno_mese_cliente);
		$anno_mese_exp = explode("_", $anno_mese_cliente_exp[0]);
		
		$sql_where .= " AND TFAARG = {$anno_mese_exp[0]}";
		$sql_where .= " AND TFMMRG = {$anno_mese_exp[1]}";
		$sql_where .= " AND TFCCON = '{$anno_mese_cliente_exp[1]}'";
	}elseif($form_values->f_fatt_0 == 'Y'){
	    $sql_where .= " AND TFAADO = '0' ";
	}

	//fatture da pervenire
	if($ddt_nf == 'N'){
	    $sql_where_rf_where = '';
	    if ($form_values->f_ddt_nf == 'NF'){
	        $sql_where_rf .= " AND (RF.RFSTAT <> 'NF' OR RF.RFSTAT IS NULL) ";	    
	        $sql_where_rf2 .= " AND (RF2.RFSTAT <> 'NF' OR RF2.RFSTAT IS NULL) ";
	    }    
	}else{
	    $sql_where_rf_where = " AND RF.RFSTAT = 'NF' ";
	    $sql_where_rf .= " AND RF.RFSTAT = 'NF' ";
	    $sql_where_rf2 .= " AND RF2.RFSTAT = 'NF' ";
	}
	
	
	$sql_1= "SELECT COUNT(DISTINCT TFCCON) AS FORN
			FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
			WHERE TFAARG = ? AND TFMMRG = ? $sql_where";
		
	$stmt_1 = db2_prepare($conn, $sql_1);
	echo db2_stmt_errormsg();
	
	if ($form_values->f_solo_con_note_credito_attese_aperte == 'Y') {
	    //solo fornitori con note di credito attese in stato aperto
	    $sql_where .= " AND TFCCON IN (
	    SELECT DISTINCT TFCCON
	    FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
	    WHERE TFFU01='N' AND TFFLAV<>9
	    )
	    ";
	}
	
	
	if ($cfg_mod_DeskAcq['abilita_digits_on_RD2RF'] == 'Y'){
	    $txt_join_RDNRDO_RFNRDE = 'digits(RD.RDNRDO) = RF2.RFNRDE';
	    $txt_join_RDNRDO_RFNRDE_else = 'digits(RD.RDNRDO) = RF.RFNRDE';
	}
	else {
	    $txt_join_RDNRDO_RFNRDE = 'RD.RDNRDO = RF2.RFNRDE';
	    $txt_join_RDNRDO_RFNRDE_else = 'RD.RDNRDO = RF.RFNRDE';
	}
	
		
		if ($forza_generazione_completa != 'Y' && ($node == '' || $node == 'root')){
		   
						
					$sql= "SELECT TFAARG, TFMMRG, TFFG01, TFAADO, SUM(TFTIMC) AS TFTIMC,
                    SUM (TFTOTC) AS TFTOTC, SUM (TFTOTD) AS TFTOTD,
                    SUM (TFINFI) AS TFINFI, SUM (TFTIMP) AS TFTIMP, 
                    SUM (TFVVAR) AS TFVVAR,
                    SUM(CASE WHEN TFFG04='N' THEN 1 ELSE 0 END) AS TFFG04,
					SUM (RF.RFTOTD) AS RFTOTD, SUM (RF.RFINFI) AS RFINFI, SUM (RF.RFTIMP) AS RFTIMP,
					COUNT(DISTINCT TFCCON) AS FORN, SUM(RF.R_NC) AS R_NC
					FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
						LEFT OUTER JOIN (
                               SELECT RFDOCU, SUM(RD.R_NC) AS R_NC, SUM (RFTOTD) AS RFTOTD, SUM (RFINFI) AS RFINFI, SUM (RFTIMP) AS RFTIMP
                               FROM {$cfg_mod_DeskAcq['file_righe_fatture']} RF2
                                     LEFT OUTER JOIN (
                                        SELECT COUNT(*) AS R_NC, RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                                        FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']} 
            			                WHERE RDPRIO = 'NC'
                                        GROUP BY RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                                ) RD
                                ON RD.RDDT = RF2.RFDT AND RD.RDTIDO = RF2.RFTIDE AND RD.RDINUM= RF2.RFINUE 
                                AND RD.RDAADO = RF2.RFAADE AND {$txt_join_RDNRDO_RFNRDE}
						       WHERE 1=1 $sql_where_rf2
						       GROUP BY RFDOCU) RF
                     	ON TFDOCU=RF.RFDOCU
                    WHERE TFFU02 = '' $sql_where
					GROUP BY TFAARG, TFMMRG, TFFG01, TFAADO
					ORDER BY TFAARG DESC, TFMMRG DESC";
					
				
					
		
		} else {
			
			$value = explode("|", $node);
			$value[0]; //id_anno_mese
			$anno_mese = explode("_", $value[0]);
			$anno = $anno_mese[0];
			$mese = $anno_mese[1];
			
			if ($forza_generazione_completa == 'Y')
				$sql_anno_mese = "";
			else
				$sql_anno_mese = " AND TFAARG ={$anno} AND TFMMRG={$mese}";
			
			
			
			$sql= "SELECT TFDOCU, TFAARG, TFMMRG, TFDCON, TFCCON, TFDTRG, TFAADO, TFNRDO,
					TFTPDO, RFAADE, RFNRDE, RFTPDE, TFSTAT, TFDSST, TFDORF, RFTOTD,
					TFTOTD, RFDTRG, RFSTAT, RFDSST, RFDORF, RFPROG, TFDTRF, RFDTRF,
					TFINFI, TFTIMP, RFINFI, RFTIMP, TFFG01, RFDT, /*RFDTOR,*/ RFTIDE, RFINUE,
					TFDTGE, TFORGE, TFDT, TFTIDO, TFINUM, TFTIMC, RFDTEP, TFVVAR, TFTOTC,
					SUM(CASE WHEN TFFG03='Y' THEN 1 ELSE 0 END) AS TFFG03,
                    SUM(CASE WHEN TFFG04='N' THEN 1 ELSE 0 END) AS TFFG04,
					SUM(CASE WHEN RFFG04='Y' THEN 1 ELSE 0 END) AS RFFG04,
					SUM(CASE WHEN RFFG03='Y' THEN 1 ELSE 0 END) AS RFFG03,
					SUM(CASE WHEN RFFG02='Y' THEN 1 ELSE 0 END) AS RFFG02,
                    SUM(RD.R_NC) AS R_NC
				   FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
				   LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_fatture']} RF
					ON TFDOCU=RF.RFDOCU
                   LEFT OUTER JOIN (
                        SELECT COUNT(*) AS R_NC, RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                        FROM {$cfg_mod_DeskAcq['file_righe_doc_gest']}
				        WHERE RDPRIO = 'NC'
                        GROUP BY RDDT, RDTIDO, RDINUM, RDAADO, RDNRDO
                    ) RD
                    ON RD.RDDT = RF.RFDT AND RD.RDTIDO = RF.RFTIDE AND RD.RDINUM= RF.RFINUE 
                        AND RD.RDAADO = RF.RFAADE AND {$txt_join_RDNRDO_RFNRDE_else}
				   WHERE TFFU02 = '' $sql_anno_mese $sql_where  $sql_where_rf
				   GROUP BY TFDOCU, TFAARG, TFMMRG, TFDCON, TFCCON, TFDTRG, TFAADO, TFNRDO,
					TFTPDO, RFAADE, RFNRDE, RFTPDE, TFSTAT, TFDSST, TFDORF, RFTOTD,
					TFTOTD, RFDTRG, RFSTAT, RFDSST, RFDORF, RFPROG, TFDTRF, RFDTRF,
					TFINFI, TFTIMP, RFINFI, RFTIMP, TFFG01, RFDT, /*RFDTOR,*/ RFTIDE, RFINUE,
					TFDTGE, TFORGE, TFDT, TFTIDO, TFINUM, TFTIMC, RFDTEP, TFVVAR, TFTOTC
				   ORDER BY TFAARG DESC, TFMMRG DESC, TFDCON, TFDTRG, RFPROG";	
			
		}
		
		/*print_r($sql); 
		exit;*/
		
				    $stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					
					$sql_a = "SELECT COUNT(*) C_ROW, MIN(TFFLAV) AS MIN_TFFLAV, TDAACA, TDNRCA, TDTICA 
								FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF
								LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_fatture']} RF
								    ON TF.TFDOCU=RF.RFDOCU
                                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD
					                ON RF.RFDT=TD.TDDT AND RF.RFTIDE=TD.TDTIDO AND RF.RFINUE=TD.TDINUM AND RF.RFAADE=TD.TDAADO AND RF.RFNRDE=TD.TDNRDO
								WHERE TFDT='{$id_ditta_default}' AND TFFU01='N' 
                                AND TFFU02 = '' AND TFCCON = ?
								AND RFDTEP >= ? AND RFDTEP <= ?
                                GROUP BY TDAACA, TDNRCA, TDTICA";
				
					
					$stmt_a = db2_prepare($conn, $sql_a);
					echo db2_stmt_errormsg();
					
					$sql_check = "SELECT TFFLCO, TFPROC
					          FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF
					          WHERE TFDT='{$id_ditta_default}' 
                              AND TFDOCU = ?";
					
					$stmt_check = db2_prepare($conn, $sql_check);
					echo db2_stmt_errormsg();
		
						
					$ar = array();					
	
					while ($r = db2_fetch_assoc($stmt)) {
						$tmp_ar_id = array();
						
			
		
						if ($form_values->totale == 'Y')
						  $liv0 = "Totale generale";
						else
						  $liv0 = implode("_", array($r['TFAARG'], $r['TFMMRG']));
						    
						 // ANNO_MESE
						if ($form_values->report_ddt_variazioni_attese == 'Y')
						    $liv1 = implode("_", array($r['TFCCON'], $r['RFSTAT']));  //FORNITORE_STATO/ddt
						else
						    $liv1 = $r['TFCCON'];  //FORNITORE
						$liv2 = implode("_", array($r['TFAADO'], $r['TFNRDO'], $r['TFTPDO']));  // FATTURA TF0
						$liv3 = implode("_", array($r['RFAADE'], $r['RFNRDE'], $r['RFTPDE']));   //FATTURA ENTRATA RF0
	
						$t_ar = &$ar;
	
						//ANNO_MESE
						$l = $liv0;
						$tmp_ar_id[] = $l;
						if (!isset($t_ar[$l])){
							$t_ar[$l] = array("cod" => $l, "descr"=>$liv0,  "id" =>implode("|", $tmp_ar_id));
				
							$oldLocale = setlocale(LC_TIME, 'it_IT');
							//$t_ar[$l]['mese'] = ucfirst(strftime("%B", strtotime($r['TFDTRG']))); //ANNO E MESE
							$t_ar[$l]['mese'] = $r['TFMMRG'];
							setlocale(LC_TIME, $oldLocale);
							$t_ar[$l]['anno'] = $r['TFAARG'];


							if ($node == '' || $node == 'root'){
								$result = db2_execute($stmt_1, array($r['TFAARG'], $r['TFMMRG'])); //deroghe
								$row1 = db2_fetch_assoc($stmt_1);
								$t_ar[$l]['count_forn'] 	 = trim($row1['FORN']);
							}
							
							
							if (strlen($form_values->anno_mese_cliente) > 0)
								$t_ar[$l]['expanded'] = true;
						
						}
							$t_ar_liv0 = &$t_ar[$l];
							$t_ar[$l]['f_nc'] += $r['R_NC'];
							$t_ar[$l]['imp_cont'] += $r['TFTIMC'];
							$t_ar[$l]['tot_cont'] += $r['TFTOTC'];
							$t_ar[$l]['var'] += $r['TFVVAR'];
							$t_ar[$l]['f_cont']  += $r['TFFG04'];
							if($t_ar[$l]['f_cont'] > 0)
						      $t_ar[$l]['scost'] += $r['TFTOTC'] - $r['TFTOTD'];

							    
							$t_ar[$l]['liv'] = 'liv_1';
							
				
							if ($forza_generazione_completa != 'Y' && ($node == '' || $node == 'root')){
								somma_valori_T($t_ar[$l], $r);
								somma_valori_R($t_ar[$l], $r);
								continue;
							}
							
							$t_ar = &$t_ar[$l]['children'];
	
								
								
							//FORNITORE
							$l = $liv1;
							$tmp_ar_id[] = $l;
							if (!isset($t_ar[$l])){
							    $t_ar[$l] = array("cod" => $l, "descr"=>trim(acs_u8e($r['TFDCON'])), "id" =>implode("|", $tmp_ar_id), "children"=>array());
								$t_ar_liv0['num_sottoliv'] ++;
								
								if (strlen($form_values->anno_mese_cliente) > 0)
									$t_ar[$l]['expanded'] = true;
							
							 $data_iniziale = $r['TFAARG'].sprintf("%02s", $r['TFMMRG'])."01";
							 $data_finale = $r['TFAARG'].sprintf("%02s", $r['TFMMRG'])."31";
							 						
							 $result = db2_execute($stmt_a, array($r['TFCCON'], $data_iniziale, $data_finale));
							 $row_a = db2_fetch_assoc($stmt_a);
							 $t_ar[$l]['nr_nota_cred'] = $row_a['C_ROW'];
							 $t_ar[$l]['nr_nota_cred_id_etich'] = $row_a['MIN_TFFLAV'];
							 $t_ar[$l]['nr_nota_fat'] = trim($row_a['TDAACA'])."_".sprintf("%06s", trim($row_a['TDNRCA']));

							 $t_ar[$l]['f_nc'] += $r['R_NC'];
							}
							$t_ar_liv1 = &$t_ar[$l];
							$t_ar[$l]['liv'] = 'liv_2';
							$t_ar[$l]['f_nc'] += $r['R_NC'];
							$t_ar[$l]['descr_forn'] = trim(acs_u8e($r['TFDCON']));
							$t_ar[$l]['fornitore'] = trim($r['TFCCON']);
							$t_ar[$l]['anno'] = trim($r['TFAARG']);
							$t_ar[$l]['mese'] = trim($r['TFMMRG']);
							$t_ar[$l]['data_reg'] = trim($r['TFDTRG']);
					
							if ($form_values->report_ddt_variazioni_attese == 'Y')
							    $t_ar[$l]['stato'] = trim($r['RFSTAT']);
							
							$t_ar = &$t_ar[$l]['children'];
								
								
							//FATTURA TF0							
							$l = $liv2;
							$tmp_ar_id[] = $l;
							if (!isset($t_ar[$l])){
								$t_ar[$l] = array(	"cod" => $l,
										"liv" => 'liv_3',
										"id" =>implode("|", $tmp_ar_id),
										"tfdocu" => $r['TFDOCU'],
										"descr"=>$liv2, "children"=>array());
	
								somma_valori_T($t_ar[$l], $r);
								somma_valori_T($t_ar_liv0, $r);
								somma_valori_T($t_ar_liv1, $r);
								
								$result = db2_execute($stmt_check, array($r['TFDOCU']));
								$row_check = db2_fetch_assoc($stmt_check);
								if(trim($row_check['TFFLCO']) != '' && $row_check['TFPROC'] == 0)
    								$t_ar[$l]['check_fatt'] = 'Y';
								
								$t_ar[$l]['k_ordine_fatt']= implode("_", array($r['TFDT'], $r['TFTIDO'], $r['TFINUM'], $r['TFAADO'], $r['TFNRDO']));
								$t_ar_liv2 = &$t_ar[$l];
								$t_ar[$l]['f_nc'] = trim($r['R_NC']);
								$t_ar[$l]['nr_fat'] =  trim($r['TFAADO'])."_".trim($r['TFNRDO']);
								//print_r($t_ar_liv1['nr_nota_fat']);
							
								if($t_ar_liv1['nr_nota_fat'] == $t_ar[$l]['nr_fat']){
								    $t_ar[$l]['nr_nota_cred'] = $t_ar_liv1['nr_nota_cred'];
								    $t_ar[$l]['nr_nota_cred_id_etich'] =  $t_ar_liv1['nr_nota_cred_id_etich'];
								}
								
								$f_docu = trim($r['TFTIDO']).trim($r['TFINUM']).trim($r['TFAADO']).trim($r['TFNRDO']);
								$ha_commenti = $main_module->has_commento_ordine_by_k_ordine($f_docu);
								
								
						
								if ($ha_commenti == FALSE)
									$img_com_name = "icone/16x16/comment_light.png";
								else
									$img_com_name = "icone/16x16/comment_edit_yellow.png";
	
									$f_docu = trim($r['TFTPDO'])."_".$f_docu;
							if ($disabled_comment != 'Y')
							    $t_ar[$l]['descr'] .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_docu_for(\'' . $f_docu . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
							
								
								$t_ar[$l]['f_cont'] += $r['TFFG04'];
								if($t_ar[$l]['f_cont'] > 0){
								    $t_ar[$l]['scost'] += $r['TFTOTC'] - $r['TFTOTD'];
								    $t_ar_liv1['scost'] += $r['TFTOTC'] - $r['TFTOTD'];
								    $t_ar_liv0['SCOST'] += $r['TFTOTC'] - $r['TFTOTD'];
								}
						
								$t_ar[$l]['imp_cont'] += $r['TFTIMC'];
								$t_ar[$l]['tot_cont'] += $r['TFTOTC'];
								$t_ar[$l]['var'] += $r['TFVVAR'];
								
								$t_ar_liv1['imp_cont'] += $r['TFTIMC'];
								$t_ar_liv1['tot_cont'] += $r['TFTOTC'];
								$t_ar_liv1['var'] += $r['TFVVAR'];
								
								$t_ar_liv0['IMP_CONT'] += $r['TFTIMC'];
								$t_ar_liv0['TOT_CONT'] += $r['TFTOTC'];
								$t_ar_liv0['VAR'] += $r['TFVVAR'];
								
								
								
							}
	
							somma_valori_R($t_ar[$l], $r);
							somma_valori_R($t_ar_liv0, $r);
							somma_valori_R($t_ar_liv1, $r);
							
								
							$t_ar[$l]['data_reg']=trim($r['TFDTRG']);
							if($r['TFAADO'] == 0){
								$t_ar[$l]['fattura0'] = 'Y';
								$t_ar_liv1['fattura0'] = 'Y';
							}
							
							
							if (strlen($r['R_NC']) > 0){
							 if (strlen($t_ar[$l]['f_nc']) == 0) $t_ar[$l]['f_nc'] = 0;
							 $t_ar[$l]['f_nc'] += (int)$r['R_NC'];
							} 
							$t_ar[$l]['s_black'] += $r['TFFG03'];
							$t_ar[$l]['fattura'] = $liv2;
							$t_ar[$l]['data_evas'] = $r['RFDTEP'];
							$t_ar[$l]['stato']=trim($r['TFSTAT']);
							$t_ar[$l]['desc_stato']=trim($r['TFDSST']);
							$t_ar[$l]['fornitore'] = trim($r['TFCCON']);
							$t_ar[$l]['descr_forn'] = trim(acs_u8e($r['TFDCON']));
							$t_ar[$l]['num_forn']=trim($r['TFDORF']);
							$t_ar[$l]['data_forn']=trim($r['TFDTRF']);
							//$t_ar[$l]['s_black'] += $r['TFFG03'];
							
							
							if ($forza_generazione_completa != 'Y' && strlen($form_values->anno_mese_cliente) == 0) {
								//ci fermiamo a livello fornitore
								$t_ar_liv1['leaf'] = true;
								continue;
							}

							$t_ar = &$t_ar[$l]['children'];

							//bolle o ddt RF0
							
							//se non ho il numero di bolla, non mostro la riga
							if (strlen(trim($r['RFNRDE'])) ==  0)
								continue;									
							
							$l = $liv3;
							$tmp_ar_id[] = $l;
							if (!isset($t_ar[$l])){
								$t_ar[$l] = array("cod" => $l, "descr"=>$l, "record" => $r,
								        "id" =>implode("|", $tmp_ar_id),
										"val" => array(), "children"=>array());
								$t_ar_liv1['num_sottoliv']++;
								//$t_ar_liv1['num_ddt']++;
									
							}
							if($r['TFAADO'] == 0)
								$t_ar[$l]['fattura0'] = 'Y';
							
							
							$result = db2_execute($stmt_check, array($r['TFDOCU']));
							$row_check = db2_fetch_assoc($stmt_check);
							if(trim($row_check['TFFLCO']) != '' && $row_check['TFPROC'] == 0)
							    $t_ar[$l]['check_fatt'] = 'Y';
							$t_ar[$l]['ddt'] = implode("_", array($r['RFAADE'], $r['RFNRDE'], $r['RFTPDE']));
							$t_ar[$l]['p_zero'] += $r['RFFG02'];
							$t_ar[$l]['p_red'] += $r['RFFG04'];
							$t_ar[$l]['s_black'] += $r['RFFG03'];
							$t_ar[$l]['control'] += $r['TFFG03'];
							$t_ar[$l]['totale']=trim($r['RFTOTD']);
							$t_ar[$l]['data_reg']=trim($r['RFDTRG']);
							$t_ar[$l]['stato']=trim($r['RFSTAT']);
							$t_ar[$l]['anno']=trim($r['TFAARG']);
							$t_ar[$l]['mese']=trim($r['TFMMRG']);
							$t_ar[$l]['fornitore']=trim($r['TFCCON']);
							$t_ar[$l]['desc_forn']=trim(acs_u8e($r['TFDCON']));
							$t_ar[$l]['desc_stato']=trim($r['RFDSST']);
							$t_ar[$l]['rec_stato']='N';
							$t_ar[$l]['num_forn']=trim($r['RFDORF']);
							$t_ar[$l]['f_nc'] += $r['R_NC'];
							$t_ar[$l]['data_forn']=trim($r['RFDTRF']);
							$t_ar[$l]['k_ordine_bolla']= implode("_", array($r['RFDT'], $r['RFTIDE'], $r['RFINUE'], $r['RFAADE'], $r['RFNRDE']));
							$t_ar[$l]['liv'] = 'liv_4';

							$f_docu = trim($r['RFTIDE']).trim($r['RFINUE']).trim($r['RFAADE']).trim($r['RFNRDE']);
							$ha_commenti = $main_module->has_commento_ordine_by_k_ordine($f_docu);

							if ($ha_commenti == FALSE)
								$img_com_name = "icone/16x16/comment_light.png";
							else
								$img_com_name = "icone/16x16/comment_edit_yellow.png";
							
							$f_docu = trim($r['RFTPDE'])."_".$f_docu;
							if ($disabled_comment != 'Y')
							    $t_ar[$l]['descr'] .=  '<span style="display: inline; float: right;"><a href="javascript:show_win_annotazioni_docu_for(\'' . $f_docu . '\')";><img class="cell-img" src=' . img_path($img_com_name) . '></a></span>';
	
							$t_ar[$l]['leaf']=true;

							somma_valori_R($t_ar[$l], $r);
							$t_ar_liv1['num_ddt']++;
							
	
					} //while
					
					if ($node != '' && $node != 'root'){
						$ar= $ar[$node]['children'];
					}
			
					return $ar;
	
	
}