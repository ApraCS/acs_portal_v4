<?php

/********************************************************
 * (1) Mancanti
 ********************************************************/
function crea_array_ARTMAN($initial_node){
    global $conn;
    global $cfg_mod_DeskAcq, $cfg_mod_Admin;

    $sql_where_flt	= sql_where_flt_by_REQUEST();
    $sql_where 		= "W2.M2QMAN > 0 AND W3.M3FMAN = 'M' /* AND W2.M2ESAU = 'E' */";
    
    $sql = "SELECT W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO AS DATA, W3.M3DIMP, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
    			SUM(W3.M3QTA) AS S_M3QTA, 
    			W2.M2TAB1 AS GR_PIANIF,
				ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
    			W3.M3DIM1 AS DIM1, W3.M3DIM2 AS DIM2, W3.M3DIM3 AS DIM3,				
				W3.M3VAR1 as VAR1, W3.M3VAN1 as VAN1, W3.M3VAR2 as VAR2, W3.M3VAN2 as VAN2,W3.M3VAR3 as VA3, W3.M3VAN3 as VAN3,
				W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO
            FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
            JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3              
            	ON W2.M2ART = W3.M3ART
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
			    ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE            	
            LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
            	ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'  	
            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
            	ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
            	ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT            	
 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
 	          ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART            	            	
    		WHERE 1=1 {$sql_where_flt} AND {$sql_where}
    		GROUP BY W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT, T_GRPA.TADESC, 
    			W2.M2TAB1, ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
				M3DIM1, M3DIM2, M3DIM3, M3VAR1, M3VAN1, M3VAR2, M3VAN2, M3VAR3, M3VAN3,
				TA_ITIN.TASITI,
				W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO    				
    		ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2TAB1, W2.M2ART    				
            ";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();    
    $result = db2_execute($stmt);
    
    $ar_ret = crea_array_tree_std($stmt, $initial_node, array("M3DIMP" => "Consegna richiesta", "DATA"=> "Consegna standard"), array("M3DIMP"));

return $ar_ret;
}


/********************************************************
 * (2) Senza fabbisogno confermati
********************************************************/
function crea_array_NOFABC($initial_node){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$sql_where_flt	= sql_where_flt_by_REQUEST();	
	$sql_where = "( (M2SWTR = 'O' AND M2QORD > M2QFAB AND M3FMAN IN('N', 'A')) OR (M2SWTR <> 'O' AND M2QORD > 0 AND M2QFAB = 0) )";

	$sql = "SELECT W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO AS DATA, W3.M3DIMP, W2.M2ART, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
			SUM(W3.M3QTA) AS S_M3QTA, 
			
   			W3.M3DIM1 AS DIM1, W3.M3DIM2 AS DIM2, W3.M3DIM3 AS DIM3,				
			W3.M3VAR1 as VAR1, W3.M3VAN1 as VAN1, W3.M3VAR2 as VAR2, W3.M3VAN2 as VAN2,W3.M3VAR3 as VA3, W3.M3VAN3 as VAN3,			
			
			W2.M2TAB1 AS GR_PIANIF,
			ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
			W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO    			
			FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
			JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3
				ON W2.M2ART = W3.M3ART
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
			    ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE				
			LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
				ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
				ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
            	ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
 	          ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART            				
    		WHERE 1=1 {$sql_where_flt} AND {$sql_where}
			GROUP BY W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2ART, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT, T_GRPA.TADESC,
				M3DIM1, M3DIM2, M3DIM3, M3VAR1, M3VAN1, M3VAR2, M3VAN2, M3VAR3, M3VAN3,
    			W2.M2TAB1,
				ANAG_ART.ARSOSP, ANAG_ART.ARESAU, TA_ITIN.TASITI,
				W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO    		
    		ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2TAB1, W2.M2ART    						
			";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();			
			$result = db2_execute($stmt);

			$ar_ret = crea_array_tree_std($stmt, $initial_node, array("M3DIMP" => "Consegna richiesta"), array("M3DIMP"));

			return $ar_ret;
}


/********************************************************
 * (3) Senza fabbisogni ne confermati ne provvisori
********************************************************/
function crea_array_NFANCP($initial_node){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$sql_where_flt	= sql_where_flt_by_REQUEST();	
	$sql_where = "( M3FMAN='N' AND (M2SWTR = 'O' AND M2QORD > M2QFAB AND M2QFAE = 0) OR (M2SWTR <> 'O' AND M2QORD > 0 AND M2QFAB = 0 AND M2QFAE = 0) )";

	$sql = "SELECT W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO AS DATA, W3.M3DIMP, W2.M2ART, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
			SUM(W3.M3QTA) AS S_M3QTA,
   			
   			W3.M3DIM1 AS DIM1, W3.M3DIM2 AS DIM2, W3.M3DIM3 AS DIM3,				
			W3.M3VAR1 as VAR1, W3.M3VAN1 as VAN1, W3.M3VAR2 as VAR2, W3.M3VAN2 as VAN2,W3.M3VAR3 as VA3, W3.M3VAN3 as VAN3,			
			
			W2.M2TAB1 AS GR_PIANIF,
			ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
			W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO
			FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
			JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3
				ON W2.M2ART = W3.M3ART
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
			    ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE				
			LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
				ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
				ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
            	ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
 	          ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART            					
    		WHERE 1=1 {$sql_where_flt} AND {$sql_where}
			GROUP BY W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2ART, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT, T_GRPA.TADESC, 
				M3DIM1, M3DIM2, M3DIM3, M3VAR1, M3VAN1, M3VAR2, M3VAN2, M3VAR3, M3VAN3,
    			W2.M2TAB1,
				ANAG_ART.ARSOSP, ANAG_ART.ARESAU, TA_ITIN.TASITI,
				W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO
    		ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2TAB1, W2.M2ART    				
			";

			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);

			$ar_ret = crea_array_tree_std($stmt, $initial_node, array("M3DIMP" => "Consegna richiesta"), array("M3DIMP"));

			return $ar_ret;
}


/********************************************************
 * (4) Ordini a fornitore scaduti
********************************************************/
function crea_array_ORFOSC($initial_node, $filtri_aggiuntivi = array()){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$sql_where_flt	= sql_where_flt_by_REQUEST();	
	$sql_where = "M3TRIG = 'O' AND M3DTEP < " . oggi_AS_date();
	
	
	//filtri aggiuntivi (es: per report)	
	if (isset($filtri_aggiuntivi['cod_fornitore']) && strlen($filtri_aggiuntivi['cod_fornitore']) > 0)
		$sql_where_flt .= " AND CF.CFCD = " . sql_t($filtri_aggiuntivi['cod_fornitore']);
	
	if (isset($filtri_aggiuntivi['cod_gruppo']) && strlen($filtri_aggiuntivi['cod_gruppo']) > 0)
		$sql_where_flt .= " AND M2SELE = " . sql_t($filtri_aggiuntivi['cod_gruppo']);
		
	if ($filtri_aggiuntivi['solo_manutenuti'] == 'Y')
		$sql_where_flt .= " AND (M3FU01 = 'X' OR M3FU02 = 'X') ";
	
	

	$sql = "SELECT W3.M3FU01, W3.M3FU02, W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO, W2.M2SELE, W2.M2DT, W3.M3CCON AS M2FOR1, CF.CFRGS1, W2.M2DRIO AS DATA, W3.M3DTEP, W2.M2ART, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
			TA_FORN.TAFG01, TA_FORN.TAFG02,
			SUM(W3.M3QTA) AS S_M3QTA, 
   			
   			W3.M3DIM1 AS DIM1, W3.M3DIM2 AS DIM2, W3.M3DIM3 AS DIM3,				
			W3.M3VAR1 as VAR1, W3.M3VAN1 as VAN1, W3.M3VAR2 as VAR2, W3.M3VAN2 as VAN2,W3.M3VAR3 as VA3, W3.M3VAN3 as VAN3,			
			
			W2.M2TAB1 AS GR_PIANIF,
			ANAG_ART.ARSOSP, ANAG_ART.ARESAU    			
			FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
			JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3
				ON W2.M2ART = W3.M3ART
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
			    ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE				
			LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
				ON CF.CFDT = W2.M2DT AND CF.CFCD = W3.M3CCON AND CF.CFTICF = 'F'
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
				ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
            	ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
 	          ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART  

 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_FORN
 	        	ON W2.M2DT = TA_FORN.TADT AND TA_FORN.TATAID = 'FORN' AND digits(W2.M2FOR1) = TA_FORN.TAKEY1
 	          
    		WHERE 1=1 {$sql_where_flt} AND {$sql_where}
			GROUP BY W3.M3FU01, W3.M3FU02, W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO, W2.M2SELE, W2.M2DT, W3.M3CCON, CF.CFRGS1, W2.M2DRIO, W3.M3DTEP, W2.M2ART, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT, T_GRPA.TADESC,
				M3DIM1, M3DIM2, M3DIM3, M3VAR1, M3VAN1, M3VAR2, M3VAN2, M3VAR3, M3VAN3,
    			W2.M2TAB1,
				ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
				TA_FORN.TAFG01, TA_FORN.TAFG02, TA_ITIN.TASITI
    		ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W3.M3DTEP, W2.M2TAB1, W2.M2ART    				
			";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();			
			$result = db2_execute($stmt);
			echo db2_stmt_errormsg($stmt);

			$ar_ret = crea_array_tree_std($stmt, $initial_node, array("M3DTEP" => "Consegna richiesta"), array("M3DTEP"));

			return $ar_ret;
}


/********************************************************
 * (5) Articoli senza listino
********************************************************/
function crea_array_NOLIST($initial_node){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$sql_where_flt	= sql_where_flt_by_REQUEST();	
	$sql_where = "M2PRZ = 0";
	
	global $backend_ERP;
	
	if ($backend_ERP == 'GL'){
	    $f_anag = "W2.M2RGS1 AS CFRGS1, W2.M2DIM1 AS DIM1, W2.M2DIM2 AS DIM2, W2.M2DIM3 AS DIM3, W2.M2SOSP, W2.M2ESAU,";
        $f_anag_group_by = "W2.M2RGS1, W2.M2DIM1, W2.M2DIM2, W2.M2DIM3, W2.M2SOSP, W2.M2ESAU,";
        $join_anag = "";
	} else {
	    //default: SV2
	    $f_anag = "T_BREF.TACINT AS REF_NAME, CF.CFRGS1, ANAG_ART.ARDIM1 AS DIM1, ANAG_ART.ARDIM2 AS DIM2, ANAG_ART.ARDIM3 AS DIM3, ANAG_ART.ARSOSP, ANAG_ART.ARESAU,";
	    $f_anag_group_by = "T_BREF.TACINT, CF.CFRGS1, ANAG_ART.ARDIM1, ANAG_ART.ARDIM2, ANAG_ART.ARDIM3, ANAG_ART.ARSOSP, ANAG_ART.ARESAU,";
	    $join_anag = " LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
 	          ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART ";
	    $join_anag .= "	LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
				ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'";
	    $join_anag .= "	LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
				ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT";
	}

	$sql = "SELECT W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2ART, W2.M2DART, W2.M2SWTR, W2.M2UM AS M3UM, W2.M2QFAB AS S_M3QTA, 
            T_GRPA.TADESC AS GR_PIANIF_DESC,			 
            {$f_anag} 
            W2.M2TAB1 AS GR_PIANIF    			
			FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
			    ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE			

            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
            	ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
 	        {$join_anag}        					
    		WHERE 1=1 {$sql_where_flt} AND {$sql_where}
			GROUP BY W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2ART, W2.M2DART, W2.M2SWTR, W2.M2UM, W2.M2QFAB, 
    			{$f_anag_group_by} 
                W2.M2TAB1, T_GRPA.TADESC, TA_ITIN.TASITI
    		ORDER BY TA_ITIN.TASITI, W2.M2TAB1, W2.M2ART    				
			";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();	
			$result = db2_execute($stmt);
			echo db2_stmt_errormsg();	

			$ar_ret = crea_array_tree_std($stmt, $initial_node, array());

			return $ar_ret;
}


/********************************************************
 * (6) Revoche MTO
********************************************************/
function crea_array_REV_MTO($initial_node){
	global $conn, $cfg_mod_DeskAcq, $cfg_mod_Spedizioni, $cfg_mod_Admin, $id_ditta_default;

	///// TODO
	$sql_where_flt	= sql_where_flt_by_REQUEST();
	$sql_where = "M2PRZ = 0";

	$sql = "SELECT 	MTO.*, MTDT AS M2DT, TD.TDOTPD AS M3TPDO,		
			 MTO.MTDIM1 AS DIM1, MTO.MTDIM2 AS DIM2, MTO.MTDIM3 AS DIM3, 
			 MTVARA1 AS VAR1, MTVARA2 AS VAR2, MTVARA3 AS VAR3,
			 MTVAR1 AS VAN1, MTVAR2 AS VAN2, MTVAR3 AS VAN3,			 
			 ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
			 TA_FORN.TAASPE AS M2SELE, MTFORN AS M2FOR1, TA_FORN.TADESC AS CFRGS1,
			 MTART AS M2ART, MTDART AS M2DART,
			 MTUMCO AS M3UM, MTQTCO AS S_M3QTA,
			 MTAADO AS M3AADO, MTNRDO AS M3NRDO,
			 MTFLE1 AS FL1, MTFLE2 AS FL2, MTFLE3 AS FL3, MTFLE4 AS FL4,
			 MTFLE5 AS FL5, MTFLE6 AS FL6, MTFLE7 AS FL7, MTFLE8 AS FL8
			FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MTO
 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
 	          ON MTO.MTDT = ANAG_ART.ARDT AND MTO.MTART = ANAG_ART.ARART
 	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_FORN
 	        	ON MTO.MTDT = TA_FORN.TADT AND TA_FORN.TATAID = 'FORN' AND digits(MTO.MTFORN) = TA_FORN.TAKEY1
 	         LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
 	          ON TD.TDSWPP = 'Y' AND MTO.MTDT = TD.TDDT AND MTO.MTTIDO = TD.TDOTID AND MTO.MTINUM = TD.TDOINU AND MTAADO = TD.TDOADO AND digits(MTNRDO) = TD.TDONDO
			 WHERE MTDT = '{$id_ditta_default}' AND MTAVAN = 'P' AND
			  (   MTFLE1 IN('M', 'I') OR MTFLE2 IN('M', 'I') OR MTFLE3 IN('M', 'I') OR MTFLE4 IN('M', 'I')
			   OR MTFLE5 IN('M', 'I') OR MTFLE6 IN('M', 'I') OR MTFLE7 IN('M', 'I') OR MTFLE8 IN('M', 'I')
			   OR MTFLE9 IN('M', 'I') OR MTFLE10 IN('M', 'I') OR MTFLE11 IN('M', 'I') OR MTFLE12 IN('M', 'I')
			   OR MTFLE13 IN('M', 'I') OR MTFLE14 IN('M', 'I') OR MTFLE15 IN('M', 'I'))
			  AND MTFLM14 <> 'E' AND MTSTAR = '' 			  
			  /* and mtforn < 610400200 */ 
			 ORDER BY MTDTIM, MTDTCC, MTAADO, MTNRDO 
			";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();	
	$result = db2_execute($stmt);
	$ar_ret = crea_array_tree_std($stmt, $initial_node, array("MTDTCC"=> "Consegna richiesta"), array("MTDTCC"));
	return $ar_ret;
}





/********************************************************
 * (7) Proposte riordino DISCRETO
********************************************************/
function crea_array_PROP_ORD_DIS($initial_node){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$sql_where_flt	= sql_where_flt_by_REQUEST();
	$sql_where 		= " W2.M2TPAR <> 'V' AND W2.M2QMAN > 0 /* AND W2.M2ESAU = 'E' */  AND W2.M2SWTR = 'D' AND  W3.M3DIMP <= W2.M2RIOL AND M3FG01 IN('R', 'C') ";

	$sql = "SELECT W3.M3FG01, W2.M2DEMO, W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO AS DATA, W3.M3DIMP, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
	SUM(W3.M3QTA) AS S_M3QTA,
	W2.M2TAB1 AS GR_PIANIF,
	ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
	W3.M3DIM1 AS DIM1, W3.M3DIM2 AS DIM2, W3.M3DIM3 AS DIM3,
	W3.M3VAR1 as VAR1, W3.M3VAN1 as VAN1, W3.M3VAR2 as VAR2, W3.M3VAN2 as VAN2,W3.M3VAR3 as VA3, W3.M3VAN3 as VAN3,
	W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO
	FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
	JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3
	ON W2.M2ART = W3.M3ART
	LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
	ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE
	LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
	ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'
	LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
	ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
	LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
	ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
	LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
			ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART
			WHERE 1=1 {$sql_where_flt} AND {$sql_where}
			GROUP BY W3.M3FG01, W2.M2DEMO, W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT, T_GRPA.TADESC,
			W2.M2TAB1, ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
			M3DIM1, M3DIM2, M3DIM3, M3VAR1, M3VAN1, M3VAR2, M3VAN2, M3VAR3, M3VAN3,
			TA_ITIN.TASITI,
			W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO
			ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2TAB1, W2.M2ART
			";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			$ar_ret = crea_array_tree_std($stmt, $initial_node, array("M2DEMO"=> "Emissione richiesta", "DATA" => "Consegna richiesta"), array("DATA"), 'N', null, 'S', 'N', 'ART_EXT_TD');

			return $ar_ret;
}





/********************************************************
 * (8) Proposte riordino MTS
********************************************************/
function crea_array_PROP_ORD_MTS($initial_node){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$sql_where_flt	= sql_where_flt_by_REQUEST();
	$sql_where 		= " W2.M2TPAR <> 'V' AND W2.M2QMAN > 0 /* AND W2.M2ESAU = 'E' */  AND W2.M2SWTR = 'S' AND  W3.M3DIMP <= W2.M2RIOL AND M3FG01 IN('R', 'C') ";

	$sql = "SELECT W3.M3FG01, W2.M2DEMO, W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO AS DATA, W3.M3DIMP, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
	SUM(W3.M3QTA) AS S_M3QTA,
	W2.M2TAB1 AS GR_PIANIF,
	ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
	W3.M3DIM1 AS DIM1, W3.M3DIM2 AS DIM2, W3.M3DIM3 AS DIM3,
	W3.M3VAR1 as VAR1, W3.M3VAN1 as VAN1, W3.M3VAR2 as VAR2, W3.M3VAN2 as VAN2,W3.M3VAR3 as VA3, W3.M3VAN3 as VAN3,
	W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO
	FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
	JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3
	ON W2.M2ART = W3.M3ART
	LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
	ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE
	LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
	ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'
	LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
	ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
	LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
	ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
	LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
	ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART
	WHERE 1=1 {$sql_where_flt} AND {$sql_where}
	GROUP BY W3.M3FG01, W2.M2DEMO, W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W3.M3UM, T_BREF.TACINT, T_GRPA.TADESC,
	W2.M2TAB1, ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
	M3DIM1, M3DIM2, M3DIM3, M3VAR1, M3VAN1, M3VAR2, M3VAN2, M3VAR3, M3VAN3,
	TA_ITIN.TASITI,
	W3.M3DT, W3.M3TIDO, W3.M3INUM, W3.M3AADO, W3.M3NRDO, W3.M3TPDO
	ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W2.M2DRIO, W3.M3DIMP, W2.M2TAB1, W2.M2ART
	";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

			$ar_ret = crea_array_tree_std($stmt, $initial_node, array("M2DEMO"=> "Emissione richiesta", "DATA" => "Consegna standard"), array("DATA"), 'N', null, 'S', 'N', 'ART_EXT_TD');						

			return $ar_ret;
}








function sql_where_flt_by_REQUEST(){
	$all_params = (array)acs_m_params_json_decode();
	
	$m_where = '';
	//$ar_seleted_stato = explode("|", $_REQUEST['list_selected_stato']);
	$ar_seleted_stato = (array)$all_params['selected_id'];
	if (count($ar_seleted_stato) > 0)
		$m_where .= " AND M2FOR1 IN (" . sql_t_IN($ar_seleted_stato) . ") ";
	if (strlen(trim($_REQUEST['f_gruppo'])) > 0)
		$m_where .= " AND M2SELE = '" . trim($_REQUEST['f_gruppo']) . "'";
	//TIPO APPROVIGIONAMENTO (RIORDINO)
	if (count($all_params['f_tipo_approv']) > 0)
		$m_where .= " AND M2SWTR IN (" . sql_t_IN($all_params['f_tipo_approv']) . ") ";		
 return $m_where;		
}




function crea_array_tree_std($stmt, $initial_node, $data_liv = array(), $data_liv_bold = array(), $expandAll = 'N', $tipo_elaborazione = null, $converti_in_date = 'S', $mostra_checkbox = 'S', $tipo_raggr_liv3 = null){
	global $main_module;
	$my_array = array();
	$mb = new Base;

	$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_ACQ'));	
	
	//CREAZIONE ARRAY AD ALBERO (Referente - Fornitore - Data - Articolo)
	while ($r = db2_fetch_assoc($stmt)) {
		if ($pre_row_function !== null)
			$pre_row_function($r);
				 
		$liv0_v = trim($r['M2SELE']);		
		$liv1_v = trim($r['M2FOR1']);
		
		$liv2_v_ar = array();
		foreach ($data_liv as $kf=>$f){			
			$liv2_v_ar[] = $r[$kf]; 
		}
		$liv2_v = implode("|", $liv2_v_ar);
		

		if ($tipo_raggr_liv3 === null) {
		
			if ($initial_node == 'LIV0;REV_MTO' || $tipo_elaborazione == 'proposte_MTO'){
				$liv3_v = implode("|", array($r['M2DT'], $r['MTTPSV'], $r['MTTPCO'], $r['MTAACO'], $r['MTNRCO'], $r['MTRISV']));
			} else {				
				//$liv3_v = trim($r['M2ART']);
				$liv3_v = implode("|", array(
							$r['M2ART'], 
							$r['VAR1'], $r['VAR1'], $r['VAR2'], $r['VAR2'], $r['VAR3'], $r['VAR3'],
							$r['DIM1'], $r['DIM2'], $r['DIM3'],
							$r['M3DT'], $r['M3TIDO'], $r['M3INUM'], $r['M3AADO'], sprintf("%06s", $r['M3NRDO'])
				));
			}
		} else {
			switch ($tipo_raggr_liv3){
				case "ART":				//stacco per cod. articolo
					$liv3_v = trim($r['M2ART']);
					//azzero il num ordine per non stamparlo
					$r['M3NRDO'] = 0;					
					break;					
				case "ART_EXT":			//stacco per cod.articolo + var/dim
					$liv3_v = implode("|", array(
						$r['M2ART'], 
						$r['VAR1'], $r['VAR1'], $r['VAR2'], $r['VAR2'], $r['VAR3'], $r['VAR3'],
						$r['DIM1'], $r['DIM2'], $r['DIM3']
					));
					//azzero il num ordine per non stamparlo
					$r['M3NRDO'] = 0;
					break;
				case "ART_EXT_TD":			//stacco per cod.articolo + var/dim + tipo disponibilita'
					$liv3_v = implode("|", array(
					$r['M2ART'],
					$r['VAR1'], $r['VAR1'], $r['VAR2'], $r['VAR2'], $r['VAR3'], $r['VAR3'],
					$r['DIM1'], $r['DIM2'], $r['DIM3'],
					$r['M3FG01']
					));
					//azzero il num ordine per non stamparlo
					$r['M3NRDO'] = 0;
					$r['M3NRDO_ICO'] = $r['M3FG01'];
					break;					
				case "ART_EXT_ORD":
					$liv3_v = implode("|", array(
						$r['M2ART'],
						$r['VAR1'], $r['VAR1'], $r['VAR2'], $r['VAR2'], $r['VAR3'], $r['VAR3'],
						$r['DIM1'], $r['DIM2'], $r['DIM3'],
						$r['M3DT'], $r['M3TIDO'], $r['M3INUM'], $r['M3AADO'], sprintf("%06s", $r['M3NRDO'])
					));
					break;										
			}
		}	
	
		$tmp_ar_id = array($initial_node);
		$n_children = "children";
	
		// LIVELLO 0
		$s_ar = &$my_array;
		$liv_c     = $liv0_v;
		$tmp_ar_id[] = 'liv1;' . $liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_1";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";			
			$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
			//$s_ar[$liv_c]["task"] = $liv_c;
			$s_ar[$liv_c]["task"] = $main_module->decod_std('ITIN', $liv_c);
			if ($expandAll == 'Y') $s_ar[$liv_c]["expanded"] = true;
		}
		add_array_valori_liv($s_ar[$liv_c], $r);		
		$s_ar = &$s_ar[$liv_c][$n_children];
		 
		// LIVELLO 1
		$liv_c     = $liv1_v;
		$tmp_ar_id[] = 'liv2;' .$liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_2";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["forn_TAFG01"]  = $r['TAFG01'];
			$s_ar[$liv_c]["forn_TAFG02"]  = $r['TAFG02'];						
			$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
			$s_ar[$liv_c]["task"] = acs_u8e(trim($r['CFRGS1'])) . " [{$liv_c}]";
			if ($expandAll == 'Y') $s_ar[$liv_c]["expanded"] = true;			
		}
		add_array_valori_liv($s_ar[$liv_c], $r);		
		$s_ar = &$s_ar[$liv_c][$n_children];
		 
		
		
		// LIVELLO 2
		if (count($data_liv) > 0){
			$liv_c     = $liv2_v;
			$tmp_ar_id[] = 'liv3;' .$liv_c;
			if (is_null($s_ar[$liv_c])){
				$s_ar[$liv_c]["liv"]  = "liv_3";
				$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";				
				$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
				
				$liv2_task_ar = array();
				foreach ($data_liv as $kf=>$f){
					
					if ($converti_in_date == 'S')
						$m_value = print_date($r[$kf]);
					else  //STATO ORDINE
						$m_value = " [" . trim($r[$kf]) . "] " . $s->decod_stato_ordine_by_tab_sys($r[$kf]);
					
					if (in_array($kf, $data_liv_bold))
						$liv2_task_ar[] = "<b>" . "{$f}: " . $m_value . "</b>";						
					else	
						$liv2_task_ar[] = "{$f}: " . $m_value;
				}			
				$s_ar[$liv_c]["task"] = implode(" - ", $liv2_task_ar);
				
				//Auto expanded
				if ($expandAll == 'Y' || $initial_node == 'LIV0;REV_MTO')
					 $s_ar[$liv_c]["expanded"] = true;
				
								
				if ($tipo_elaborazione == 'proposte_MTO'){					
					switch(trim($r['MTFLM15'])){
						// case '':
						case '8':
						case '9':  
							$s_ar[$liv_c]['iconCls'] = 'icon-button_grey_play-16'; 
						  break;
						case '0': $s_ar[$liv_c]['iconCls'] = 'icon-button_red_play-16'; break;					 		
						case '1': $s_ar[$liv_c]['iconCls'] = 'icon-button_yellow_play-16'; break;						
						case '2': $s_ar[$liv_c]['iconCls'] = 'icon-button_black_play-16'; break;
						default:  $s_ar[$liv_c]['iconCls'] = $r['MTFLM15']; 						
					}
					if ($s_ar[$liv_c]['iconCls'] == 'icon-button_grey_play-16') $s_ar[$liv_c]["expanded"] = false;										
				}
			}
			add_array_valori_liv($s_ar[$liv_c], $r);			
			$s_ar = &$s_ar[$liv_c][$n_children];
		}
		
		
		 
		// LIVELLO 3
		$liv_c     = $liv3_v;
		$tmp_ar_id[] = 'liv4;' .$liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_4";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";			
			$s_ar[$liv_c]["k_ordine"]  = implode("_", array($r['M3DT'], $r['M3TIDO'], $r['M3INUM'], $r['M3AADO'], sprintf("%06s", $r['M3NRDO'])));			
			foreach ($data_liv as $kf=>$f){
				$s_ar[$liv_c]["k_ordine"] .= "_" . $r[$kf];
			}
			$s_ar[$liv_c]["k_ordine"] .= "_" . trim($liv_c);						
			
			$s_ar[$liv_c]["dt"]   = $r['M2DT'];			
			$s_ar[$liv_c]["id"]   	= implode("|", $tmp_ar_id);
			$s_ar[$liv_c]["task"] 	= trim($r['M2DART']);
			
			if ($tipo_elaborazione == 'proposte_MTO'){
				if (trim($r['MTVAR1']) != '' || trim($r['MTVAR2']) != '' || trim($r['MTVAR3']) != '')
					$s_ar[$liv_c]["task"] .= "<p class=\"sottoriga-liv_2\">" . implode(" ", array($mb->decod_domanda_risposta(trim($r['MTVARA1']), trim($r['MTVAR1'])), $mb->decod_domanda_risposta(trim($r['MTVARA2']), trim($r['MTVAR2'])), $mb->decod_domanda_risposta(trim($r['MTVARA3']), trim($r['MTVAR3'])))) . "</p>";
			}
			
			$s_ar[$liv_c]["task"] = acs_u8e($s_ar[$liv_c]["task"]);
			
			$s_ar[$liv_c]["cod_art"] = trim($r['M2ART']);
			$s_ar[$liv_c]["tipo"] 	= trim($r['M2SWTR']);
			$s_ar[$liv_c]["um"] 	= trim($r['M3UM']);
			$s_ar[$liv_c]["in_esaurimento"] = trim($r['M2ESAU']);
			$s_ar[$liv_c]["tipo_approvvigionamento"] = trim($r['M2SWTR']);
			$s_ar[$liv_c]["dim1"] = $r['DIM1'];
			$s_ar[$liv_c]["dim2"] = $r['DIM2'];
			$s_ar[$liv_c]["dim3"] = $r['DIM3'];
			$s_ar[$liv_c]["var1"] = trim($r['VAR1']);
			$s_ar[$liv_c]["var2"] = trim($r['VAR2']);
			$s_ar[$liv_c]["var3"] = trim($r['VAR3']);			
			$s_ar[$liv_c]["van1"] = trim($r['VAN1']);
			$s_ar[$liv_c]["van2"] = trim($r['VAN2']);
			$s_ar[$liv_c]["van3"] = trim($r['VAN3']);			
			$s_ar[$liv_c]["fl1"]  = $r['FL1'];			
			$s_ar[$liv_c]["fl2"]  = $r['FL2'];
			$s_ar[$liv_c]["fl3"]  = $r['FL3'];
			$s_ar[$liv_c]["fl4"]  = $r['FL4'];									
			$s_ar[$liv_c]["fl5"]  = $r['FL5'];
			$s_ar[$liv_c]["fl6"]  = $r['FL6'];
			$s_ar[$liv_c]["fl7"]  = $r['FL7'];
			$s_ar[$liv_c]["fl8"]  = $r['FL8'];			
			$s_ar[$liv_c]["gr_pianificazione"] = $r['GR_PIANIF'];
			$s_ar[$liv_c]["gr_pianificazione_desc"] = $r['GR_PIANIF_DESC'];
			$s_ar[$liv_c]["M3NRDO"] = $r['M3NRDO'];
			$s_ar[$liv_c]["M3NRDO_ICO"] = $r['M3NRDO_ICO'];
			$s_ar[$liv_c]["M3AADO"] = $r['M3AADO'];
			$s_ar[$liv_c]["M3TPDO"] = $r['M3TPDO'];
			$s_ar[$liv_c]["M3DT"] = $r['M3DT'];
			$s_ar[$liv_c]["M3TIDO"] = $r['M3TIDO'];
			$s_ar[$liv_c]["M3INUM"] = $r['M3INUM'];
			$s_ar[$liv_c]["M3FU01"] = $r['M3FU01'];
			$s_ar[$liv_c]["M3FU02"] = $r['M3FU02'];
			$s_ar[$liv_c]["leaf"] = true;
			
			if ($r['M3FU01'] == 'X') $s_ar[$liv_c]["iconCls"] = "icon-sub_red_delete-16";
			if ($r['M3FU02'] == 'X') $s_ar[$liv_c]["iconCls"] = "iconSpedizione";

			if ($tipo_elaborazione == 'proposte_MTO'){

				//da xx0s2td0
				$s_ar[$liv_c]["TDTPDO"] = $r['TDTPDO'];
				$s_ar[$liv_c]["TDSTAT"] = $r['TDSTAT'];
				$s_ar[$liv_c]["TDDTVA"] = $r['TDDTVA'];
				$s_ar[$liv_c]["TDDTEP"] = $r['TDDTEP'];
				$s_ar[$liv_c]["TDPRIO"] = $r['TDPRIO'];
				
				//da wpi0td0
				$s_ar[$liv_c]["TDDTSP"] = $r['TDDTSP'];
				$s_ar[$liv_c]["TDSWSP"] = $r['TDSWSP'];				
				$s_ar[$liv_c]["TDDCON"] = acs_u8e($r['TDDCON']);
				$s_ar[$liv_c]["tp_pri"]	= $s->get_tp_pri('', $r);
				
				//per checkbox
				if ($mostra_checkbox == 'S')
					$s_ar[$liv_c]["checked"] = false;
				
			}			
			
		}
		add_array_valori_liv($s_ar[$liv_c], $r);		
		$s_ar = &$s_ar[$liv_c];
	
	}
	
	foreach($my_array as $kar => $r)
		$ar_ret[] = array_values_recursive($my_array[$kar]);
	
 return $ar_ret;	
}



function add_array_valori_liv(&$ar, $r){
	$ar["qta"] 	+= $r['S_M3QTA'];

	$ar["conteggio_referenze"]["ordine"]["{$r['M3AADO']}_{$r['M3NRDO']}"]++;
	
	if ($ar["liv"] != 'liv_4'){
		$ar["cod_art"]++;
		$ar['M3AADO'] = 'COUNT';
		$ar['M3NRDO'] = count($ar["conteggio_referenze"]["ordine"]);
	}
	
	if ($ar["liv"] > 'liv_1' ){
		if (trim($r['ARSOSP']) != '') $ar["art_sosp"] = $r['ARSOSP'];
		if (trim($r['ARESAU']) != '') $ar["art_esau"] = $r['ARESAU'];
	}
	
}


?>