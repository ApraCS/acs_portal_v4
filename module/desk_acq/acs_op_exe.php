<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();
 
//funcioni generiche
switch($_REQUEST['fn']) {
	case "upd_commento_ordine": 
		$result = $main_module->exe_upd_commento_ordine($_REQUEST);		
		echo "{success: true}";
		exit();
		break;
	case "sincronizza_spedizione":
		$result = $main_module->exe_sincronizza_spedizione(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;		
	case "sincronizza_generica":
		$result = $main_module->exe_sincronizza_generica(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;
	case "assegna_spedizione":
		$result = $main_module->exe_assegna_spedizione(acs_m_params_json_decode());
		echo "{success: true}";
		exit();
		break;	
	case "exe_send_email":
	    include "../desk_vend/send_email.php";
	    exit();
	    break;
}

?>
