<?php

require_once("../../config.inc.php"); 

$main_module = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));






// ******************************************************************************************
// genera fattura anticipo FORNITORE (DA CHECK FATTURE)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_genera_fattura_anticipo_fornitore'){
    $m_params = acs_m_params_json_decode();
    
    $use_session_history = microtime(true);
    
    $msg_ri = $m_params->mgs_ri;
   
    
    
    foreach($m_params->form_values as $k => $v){
        
        if (substr($k, 0, 7) == 'f_text_'){
            
            $sh = new SpedHistory($main_module);
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> $msg_ri,
                    "use_session_history" 	=> $use_session_history,
                    "vals" => array(
                        "RICITI" => substr($k, 7, 1),
                        "RIDART" => $v
                    )
                    
                )
                );
            
        }
    }
    
    $sh = new SpedHistory($main_module);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> $msg_ri,
            "end_session_history" 	=> $use_session_history,
            "vals" => array(
                "RICVES" => $m_params->fornitore,
                "RIDTEP" => (int)$m_params->form_values->f_data_reg,
                "RIVETT" => $m_params->form_values->f_pagamento,
                "RIIMPO" => sql_f($m_params->form_values->f_importo),
                "RIDART" => $m_params->form_values->f_rif
            )
        )
        );
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}










// ******************************************************************************************
// _anticipo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_genera_fattura'){
	$m_params = acs_m_params_json_decode();
	$fornitore = $m_params->form_values->f_forn;
	
	$sql = "SELECT CFRGS1 FROM {$cfg_mod_Spedizioni['file_anag_cli']} CF
            WHERE CFDT = '{$id_ditta_default}' AND CFCD = '{$fornitore}'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);
	$desc_forn = $row['CFRGS1'];
	
	

	?>
 {"success":true, "items": [
        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            
					buttons: [{
			            text: 'Genera',
			            iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var form_values = form.getValues();
							 var loc_win = this.up('window');
							 
								if(form.isValid()){
									Ext.Ajax.request({

										url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_genera_fattura_anticipo_fornitore',
									        jsonData: {
									        	form_values: form.getValues(),
									        	fornitore : '<?php echo $fornitore; ?>',
									        	<?php if($m_params->nota_credito == 'Y'){?>
									        	mgs_ri : 'GEN_NOT_CRE',
									        	<?php }else{?>
									        	mgs_ri : 'GEN_FAT_ANTG',
									        	<?php }?>
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){	            	  													        
									            var jsonData = Ext.decode(result.responseText);
									          	loc_win.fireEvent('afterSave', loc_win);
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									            console.log('errorrrrr');
									        }
									    });	  
							    }							 
				
			            }
			         }],   		            
		
		            items: [   

							{xtype : 'displayfield',
							fieldLabel : 'Fornitore',
							value : '<?php echo $desc_forn; ?>',
						    labelWidth: 120
							},
							
								{
							     name: 'f_data_reg'
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data registrazione'
							   , labelAlign: 'left'
							   , labelWidth: 120
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
							 }
							},{
							xtype: 'numberfield',
							hideTrigger: true,
							fieldLabel: 'Imponibile',
							name: 'f_importo',
							labelWidth: 120,
							anchor: '-15'
							},
							{
							xtype: 'textfield',
							fieldLabel: 'Riferimento',
							name: 'f_rif',
							labelWidth: 120,
							maxLength: 30,
							anchor: '-15'
							},	
							
									{
								name: 'f_pagamento',
								xtype: 'combo',
								flex: 1,
				                fieldLabel: 'Pagamento',
				                labelWidth: 120, 
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,
							   	anchor: '-15',
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [
								    
								    <?php  echo acs_ar_to_select_json($main_module->find_TA_std('PAGAC'), ''); ?>
								    
									    ]
								}
					
							}, 
							
									 {
					xtype: 'fieldset',
	                title: 'Annotazioni',
	                layout: 'anchor',
	                flex:1,
	                items: [
						<?php for($i=1; $i<= 5;$i++){ ?>
                	
                	{
						name: 'f_text_<?php echo $i ?>',
						xtype: 'textfield',
						fieldLabel: '',
						anchor: '-15',
					    maxLength: 80,					    
					    //value: <?php echo j(trim($commento_txt[$i])); ?>,							
					},	
					
					<?php }?>	
								
									
					 
	             ]}
								 
		           			]
		              }
		
			]
}	
	
	
	
<?php 
exit;
}
