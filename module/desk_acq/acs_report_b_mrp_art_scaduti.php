<?php

require_once "../../config.inc.php";
require_once("acs_panel_background_mrp.inc.php");

$main_module = new DeskAcq();
$mb = new Base;

$ar_email_to = array();

//aggiungo la e-mail del fornitore
$fornitore = new DeskAcqFornitori();
$fornitore->load_rec_data_by_k(array("TAKEY1"=>$_REQUEST['cod_fornitore']));

$ar_email_to[] = array(trim($fornitore->rec_data['TAMAIL']), "FORNITORE " . j(trim($fornitore->rec_data['TADESC'])) . " (" .  trim($fornitore->rec_data['TAMAIL']) . ")");

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");

$ar_email_json = acs_je($ar_email_to);

?>


<html>
<head>

<style>
div#my_content h1{font-size: 22px; padding: 5px;}
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}

table.acs_report{border-collapse:collapse; width: 100%;}
table.acs_report td, table.acs_report th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}

.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{background-color: black; color: white; padding: 5px;}

table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}
 
@media print
{
	.noPrint{display:none;}
}

</style>


  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css") ?> />  
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>


</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'>

<h1>Gestione residuo ordine</h1>

<?php



global $cfg_mod_DeskAcq;
$m_params = acs_m_params_json_decode();

//in $_REQUEST ricarico i valori filtrati in apertura panel_background_mrp
$HTTP_RAW_POST_DATA = $_REQUEST['open_pars'];

$ar = crea_array_ORFOSC('', array('cod_fornitore' => $_REQUEST['cod_fornitore'],
								  'cod_gruppo' => $_REQUEST['cod_gruppo'],
								  'solo_manutenuti' => $_REQUEST['solo_manutenuti']));
$ar = &$ar[0]['children']; //salto liv0 e liv1 (tipo statistica, gruppo)

///echo "<pre>"; print_r($ar); echo "</pre>";

// STAMPO REPORT *******************************
foreach ($ar as $k0 => $l0){

 $l0_tot = array();

 echo "
 
 		<table class=tabh2>
 		  <tr><td colspan=3  class=\"acsreport\">
 			Fornitore " . $l0['task'] . "			
		 </td></tr>
	    </table>"; 
 
 echo "<table class=acs_report>";
 
 ?>
		<tr class="t-l1">
 		 <td width="80">Codice</td>		
 		 <td width="60%">Articolo</td>
 		 <td width="50">L</td> 		 
 		 <td width="50">H</td>
 		 <td width="50">P</td>
 		 <td width="50">UM</td>
 		 <td width="50">Q.t&agrave;</td> 		  		 
 		 <td width="20%">Ordine</td>		 
 		 <td width="20%">Stato</td>		 		 		 
 		</tr>
 		
 <?php 		
   
	 foreach ($l0['children'] as $k1 => $l1){		
		?>
		
		<tr class="d-l1">
		 <td colspan=9><?php echo $l1['task']; ?></td>		 
		</tr>
		
	<?php foreach ($l1['children'] as $k2 => $l2){    ?>	
	
		<TR>
			<td><?php echo $l2['cod_art']; ?></td>		
			<td><?php echo $l2['task']; ?>
				<?php
				$r = $l2;
				if (trim($r['var1']) != '' || trim($r['var2']) != '' || trim($r['var3']) != '')
					echo "<p class=\"sottoriga-liv_2\">" . implode(" ", array($mb->decod_domanda_risposta(trim($r['var1']), trim($r['van1'])), $mb->decod_domanda_risposta(trim($r['var2']), trim($r['van2'])), $mb->decod_domanda_risposta(trim($r['var3']), trim($r['van3'])))) . "</p>";
				?>			
			</td>
			<td class=number><?php echo n($l2['dim1'], 2); ?></td>			
			<td class=number><?php echo n($l2['dim2'], 2); ?></td>
			<td class=number><?php echo n($l2['dim3'], 2); ?></td>
			<td><?php echo $l2['um'] ?></td>
			<td class=number><?php echo n($l2['qta'], 2); ?></td>												
		    <td><?php echo implode("_", array($l2['M3TPDO'], $l2['M3AADO'], $l2['M3NRDO'])) ?></td>		 
		    <td><?php echo out_stato($l2); ?></td>		 		 			
		</TR>
	
	
	<?php } ?>		
		
		
		
		<?php
		
	 } //per ogni riga
	 
	 ?>
	 
	 
	 <?php
	 
	 
	 
 echo "</table>";

}



?>




<?php


function out_stato($l){
 global $main_module, $cfg_mod_DeskAcq, $conn;
 if ($l['M3FU01'] == 'X') return 'Annullato';
 else if ($l['M3FU02'] == 'X'){
	 //recupero la data su cui e' stato riprogrammato (da file RI)
	 $sql = "SELECT RIDTEP FROM {$cfg_mod_DeskAcq['file_richieste']}
	 		 WHERE RIDT=? AND RITIDO=? AND RIINUM=? AND RIAADO=? AND RINRDO=?
	 		       AND RIDTVA=? AND RIART=?
	 		 ORDER BY RITIME DESC
	 		 FETCH FIRST 1 ROWS ONLY      
	 	";
	 $stmt = db2_prepare($conn, $sql);
	 $result = db2_execute($stmt, explode("_", $l['k_ordine']));
	 $row = db2_fetch_assoc($stmt);	 	 
	 
 	 return 'Riprogrammato al ' . print_date($row['RIDTEP']);
 }	 
 else return "Da valutare"; 
}


function cmp_by_TRASP_D($a, $b)
{
	return strcmp($a["TRASP_D"], $b["TRASP_D"]);
}


function somme_conteggi($ar, $r){
  return $ar;	
} 


?>


</div>
</body>
</html>