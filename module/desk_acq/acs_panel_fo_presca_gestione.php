<?php

require_once("../../config.inc.php");
$_module = "DESKACQ";

$main_module = new DeskAcq();

$all_params = array();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());




function to_AS_date_from_format($format, $data){
	switch ($format){
		case "m-d-Y":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[0], $d_ar[1], $d_ar[2]);
			return to_AS_date($d);
		case "Y-m-d":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[2], $d_ar[0]);
			return to_AS_date($d);
			
	}
}


function decodifica_data($calendar_datatime){
	$data_ora = explode("T", $calendar_datatime);
	$data_t = $data_ora[0] . '';
	$data_ext = explode("-", $data_t);

	return implode("", $data_ext);
}

function decodifica_ora($calendar_datatime){
	$data_ora = explode("T", $calendar_datatime);
	$ora_t = $data_ora[1] . '';
	$ora = substr($ora_t, 0, 2) . substr($ora_t, 3, 2) . substr($ora_t, 6, 2);
	return $ora;
}





// ******************************************************************************************
// CREATE EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_crt_events'){
	global $cfg_mod_DeskAcq, $id_ditta_default;
	$ret = array();
	$m_params = acs_m_params_json_decode();
	
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['form_ep'] = str_replace("\\", "", $_REQUEST['form_ep']);	
	
	$form_ep = (array)json_decode($_REQUEST['form_ep']);

	if ($form_ep['ggweek'] > 0){
		$ggweek = $form_ep['ggweek'];
		$data_in = 0; $data_fi = 0;
	} else {
		$ggweek = 0;
		$data_in = decodifica_data($all_params['StartDate']);
		$data_fi = decodifica_data($all_params['EndDate']);
	}
	
	if ((int)$all_params['CalendarId'] == 2)
		$odindi = 'I'; //indisponibile
	else
		$odindi = '';  //Disponibile
		
	
	$sql = "INSERT INTO {$cfg_mod_DeskAcq['file_orari_disponibili']}(
				ODTP, ODDT, ODTRIS, ODCRIS, ODSTAB, ODAREA, ODPORT, ODGGWK, ODDTIN, ODHMIN, ODDTFI, ODHMFI, ODINDI
			) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array( 'O', $id_ditta_default,
												$form_ep['tipo_risorsa'], $form_ep['codice'],
					 							$form_ep['stab'], $form_ep['area'], $_REQUEST['f_porta'],
												$ggweek,
												$data_in, decodifica_ora($all_params['StartDate']),
												$data_fi, decodifica_ora($all_params['EndDate']),
												$odindi));
			echo db2_stmt_errormsg($stmt);			

			$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// UPDATE EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_upd_events'){
	global $cfg_mod_DeskAcq, $id_ditta_default;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "UPDATE {$cfg_mod_DeskAcq['file_orari_disponibili']} SET
				ODHMIN=?, ODHMFI=?
			WHERE ODID=?	
			";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array(
			decodifica_ora($all_params['StartDate']),
			decodifica_ora($all_params['EndDate']),
			$all_params['EventId']));
	echo db2_stmt_errormsg($stmt);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// DELETE EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_del_events'){
	global $cfg_mod_DeskAcq, $id_ditta_default;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "DELETE FROM {$cfg_mod_DeskAcq['file_orari_disponibili']} WHERE ODID=?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($all_params['EventId']));
	echo db2_stmt_errormsg($stmt);

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// EVENTI CALENDARIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_get_events'){
	global $conn, $cfg_mod_DeskAcq, $id_ditta_default;

	$ret = array();
	$ret['events'] = array();

	//data iniziale e di fine
	$from_calendar_format = 'Y-m-d';
	$d_start 	= to_AS_date_from_format($from_calendar_format, $_REQUEST["startDate"]);
	$d_end 		= to_AS_date_from_format($from_calendar_format, $_REQUEST["endDate"]);

				
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['form_ep'] = str_replace("\\", "", $_REQUEST['form_ep']);	
	
 	
	$form_ep = (array)json_decode($_REQUEST['form_ep']);
		

	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_orari_disponibili']} WHERE 1=1 ";
	$sql .= " AND ODTP= ? AND ODDT=? AND ODTRIS=? AND ODCRIS=?
			    AND ODSTAB=? /* AND ODAREA=? */ AND ODPORT=?";
	$sql .= " AND ODDTIN >=? AND ODDTFI <=? AND ODGGWK=?";	
	
	if ($form_ep['ggweek'] > 0){
		$data_in = 0; $data_fi = 0; $ggweek = $form_ep['ggweek']; 
	}
	else { 
		$data_in = $d_start;
		$data_fi = $d_end;
		$ggweek = 0;
	}

	$stmt = db2_prepare($conn, $sql);
	
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array( 'O', $id_ditta_default,
												$form_ep['tipo_risorsa'], $form_ep['codice'],
					 							$form_ep['stab'], 
												//$form_ep['area'], 
												$_REQUEST['f_porta'],
												$data_in, $data_fi, $ggweek,
												));

	while ($r = db2_fetch_assoc($stmt)) {
		
		if ($form_ep['ggweek'] > 0){
			$r['ODDTIN'] = $d_start; $r['ODDTFI'] = $d_start;  
		}
		
		if ($r['ODINDI'] == 'I'){
			$title = 'Indisponibile';	
			$cid = 2; //indisponibile
		}
		else {
			$title = 'Disponibile';
			$cid = 1; //disponibile
		}
		
		$e = array();
		$e['cid'] = $cid; //id calendario
		$e['id'] = (int)$r['ODID']; //id calendario
		$e['title'] = acs_u8e("{$title} (#{$r['ODID']})");
		$e['start'] = print_date($r['ODDTIN'], '%Y-%m-%d') . " " . print_ora($r['ODHMIN']) . ":00";
		$e['end'] 	= print_date($r['ODDTFI'], '%Y-%m-%d') . " " . print_ora($r['ODHMFI']) . ":00";
		$e['notes'] = "Have fun";
		$e['risorsa'] = '';
	
		$ret['events'][] = $e;
	}

 echo acs_je($ret);
 exit;
	} //fn=cal_get_events







// ******************************************************************************************
// OPEN FORM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_open_form'){?>
	
{"success":true, "items": [

	{
		xtype: 'form',
		layout: {
                type: 'vbox',
                align: 'stretch'
            },
		flex: 1, frame: true,
		padding:    '10 10 10 10',
		margins:    '10 10 10 10',
		items: [
				
				{xtype: 'hidden', name: 'tipo_risorsa', value: <?php echo j($all_params['tipo_risorsa'])?>},
				{xtype: 'hidden', name: 'codice', value: <?php echo j($all_params['codice'])?>},				
		
		
		
				{
		            xtype: 'textfield',
		            disabled: true,
		            itemId: this.id + '-title',
		            fieldLabel: 'Risorsa',
		            value: <?php echo j($all_params['descr'] . " [ {$all_params['codice']} ]"); ?>,
		            anchor: '100%',
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        }
		        
		        
	<?php if ($all_params['tipo_risorsa'] != 'STAB'){ ?>		        
		        , {
		            xtype: 'combo',	                     
	                name: 'stabilimento', fieldLabel: 'Stabilimento',
					forceSelection: true, allowBlank: false,
	                anchor: '100%',	                   	                
					store: {
						autoLoad: true,	editable: false, autoDestroy: true, fields: [{name:'id'}, {name:'text'}],
					    data: [<?php echo acs_ar_to_select_json(array_merge(array(array('id'=>'*DEF', 'text'=>'* DEFAULT')),
					    			$main_module->find_TA_std('START')), ""); ?>]},	            
					valueField: 'id', displayField: 'text',		            				
		        }
	<?php } else { ?>
			  , {xtype: 'hidden', name: 'stab', value: <?php echo j($all_params['codice'])?>},		
	<?php }?>		        
		        
/******************		        
	<?php if ($all_params['tipo_risorsa'] != 'AREA'){ ?>		        
		        , {
		            xtype: 'combo',	                     
	                name: 'area', fieldLabel: 'Area ricezione',
					forceSelection: true, allowBlank: false,
	                anchor: '100%',	                 	                
					store: {
						autoLoad: true,	editable: false, autoDestroy: true, fields: [{name:'id'}, {name:'text'}],
					    data: [<?php echo acs_ar_to_select_json(array_merge(array(array('id'=>'*DEF', 'text'=>'* DEFAULT')),
					    			$main_module->find_TA_std('ASPE')), ""); ?>]},	            
					valueField: 'id', displayField: 'text',		            				
		        }
	<?php } else { ?>
			  , {xtype: 'hidden', name: 'area', value: <?php echo j($all_params['codice'])?>},		
	<?php }?>	   
********************/	     
		        
		        
		        
		        , {
				     name: 'data'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				}, {
								name: 'ggweek',
								xtype: 'combo',
								fieldLabel: 'Giorno della settimana',
								forceSelection: false,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		allowBlank: false,								
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: [								    
								     {id: 0, text: '- seleziona singola data -'},
								     {id: 1, text: 'Lunedi'},
								     {id: 2, text: 'Martedi'},								     
								     {id: 3, text: 'Mercoledi'},
								     {id: 4, text: 'Giovedi'},
								     {id: 5, text: 'Venerdi'},
								     {id: 6, text: 'Sabato'},
								     {id: 7, text: 'Domenica'}
								    ]
								},
								
						 } ],
		        
		buttons: [

            {
                text: 'Visualizza',
               	scale: 'large',
                iconCls: 'icon-folder_search-32',
                handler: function(){
                	var form = this.up('form').getForm();
					if(form.isValid()){
											
						if (form.findField("data").getValue() === null && form.findField("ggweek").getValue() == 0){
							acs_show_msg_error('Selezionare una data o un giorno della settimana');
							return false;
						}	
						if (form.findField("data").getValue() !== null && form.findField("ggweek").getValue() > 0){
							acs_show_msg_error('Selezionare una data o un giorno della settimana');
							return false;
						}						
					                	
						acs_show_win_std('Gestione disponibilit&agrave;', 'acs_panel_fo_presca_gestione.php?fn=get_open_calendar', form.getValues(), 1080, 600, null, 'icon-calendar-16');
						return false;
					}                
                }
                //scope: this
            }
		],        
		        
		        
	}

]}	
		
<?php exit; }?>
<?php
// ******************************************************************************************
// OPEN FORM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_open_calendar'){

if ((int)$all_params['data'] == 0)
	$data = oggi_AS_date();
else $data = $all_params['data'];

?>
{"success":true, "items":
 {
  xtype: 'panel',
  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
  items: [
  
  
  // ------------------------ panel e calendario --------------------------------
  
	, {
	  xtype: 'panel', flex: 1,	  
	  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
	  items: [  
  
	  	<?php
	  	$def_stab = $all_params['stab'];	  	 
	  	if ($def_stab == '*DEF')
	  		$el_porte = array(array('id' => '*DEF', 'text' => '*DEF'));
	  	else
	  		$el_porte = $main_module->get_elenco_porte_stabilimento($def_stab);

	  	

	  	
	  	//visualizzo il calendario per ogni porta	  	
	  	foreach ($el_porte as $kp => $porta){  	
	  	?>
			  , {
				   xtype: 'extensible.calendarpanel', flex: 1,
				   
				   title: <?php echo j($porta['text']); ?>,
  
  					enableDD: true,
  					ddGroup: 'sped_grid',
  					ddCreateEventText: 'aaaaa',
  
					//definizione calendari        
        			calendarStore: new Extensible.calendar.data.MemoryCalendarStore({
            			data: {
						    "calendars" : [{
						        "id"    : 1,
						        "title" : "Disponibile",
						        "color" : 26
						    }, {
						        "id"    : 2,
						        "title" : "Indisponibile",
						        "color" : 4
						    }]
						}
        			}),	
        				  	
					eventStore: new Extensible.calendar.data.EventStore({
					       autoLoad: true, autoSync: true,										       

		                    proxy: {
		                    	url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
		                        type: 'ajax',
								method: 'POST',		                        
		                        extraParams: {
		                        	f_porta: <?php echo j($porta['id']); ?>,
		                        	form_ep: <?php echo j(acs_je($all_params)); ?>
		                        },
		                        reader: {
            						type: 'json',		                        
		                            root: 'events'
		                        },		                        
		                        
					            writer: {
					                type: 'json',
					                writeAllFields: true
					            },		
					            
					            api: {
					                read: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
					                create: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_crt_events',
					                update: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_upd_events',
					                destroy: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_del_events',
					            },
					            				                        
		                        
		                    }					          
					    }),

                    activeItem: 0, // day view                       
                    showNavBar: false,
                      
                    dayViewCfg: {
                        showHeader: false,
                        showWeekLinks: true,
                        showWeekNumbers: true,
						startDate: js_date_from_AS(<?php echo $data; ?>),				
						viewStartHour  : 0,
						viewEndHour  : 24,
						scrollStartHour: 6,
						ddGroup: 'DayViewDD',
						showTime: false,
						showTodayText: false,						
						showHourSeparator: true,
						hourHeight: 35							
                    },                      
  
                                        
                    listeners: {
                    
						onEventDrop: function(rec, dt) {
						 console.log('cccccccc');
						}                    
                    

                        , 'eventclick': {
                			fn: function(panel, rec, el){                			
                			
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei                			
                			
		                		acs_show_win_std('Modifica prenotazione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=edit_event', null, 680, 220, null, 'icon-inbox-16', null, null, {
		                			record: rec, 
		                			from_comp: panel,
		                			main_tab_panel: panel.up('#panel-main-fo_presca')
		                			});			                			                            	                           
                                return false; //per non attivare evento std
                            },
                            scope: this
                        }							
						
						, 'eventmove': {
                            fn: function(dd, rec, dt){
                            	console.log('------ eventmove ------');                            	
                            		this.store.reload();                            	
                            }	
                        },
						
						
						'eventresize': {
                            fn: function(dd, rec, dt){
                            	console.log('------ eventresize ------');								
                            		this.store.reload();                            	
                            }
                        },  
                        
                        
						'eventrdrag': {
                            fn: function(dd, rec, dt, a, b){
                            	console.log('------ eventrdrag ------');
                            }
                        },                                                   
                    
                    
                        'eventadd': {
                            fn: function(dd, rec, dt){
                            	console.log('---- eventadd -----');
                            	console.log(dd); console.log(rec); console.log(dt);
                            	dd.store.reload();   
                            },
                            scope: this
                        }           
                        
                        
                       //per prevenire la creazione di nuovi eventi  
					   , 'dayclick': {fn: function(dd, rec, dt, a, b){return false;}}
					   , 'rangeselect': {fn: function(dd, rec, dt, a, b){console.log('--- rangeselect ---'); return true;}}                        
                                 
                    }                    
   
   
   
				    , showMsg: function(msg){
				        //// Ext.fly('app-msg').update(msg).removeCls('x-hidden');
				    }
				    , clearMsg: function(){
				        /// Ext.fly('app-msg').update('').addCls('x-hidden');
				    }
				   
				    , showEditWindow : function(rec, animateTarget){
				    }
                    
  
  
   		        
		        // this is a good idea since we are in a TabPanel and we don't want
		        // the user switching tabs on us while we are editing an event:
		     ,   editModal: true
      
  		} //calendario
  		
  	<?php } //per ogni porta?>	
  		
  	  ]
    } //panel con calendari porte  
  
  
  
  ]
 }
}

<?php exit; }?>
<?php
// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'edit_event'){
	?>
	
{"success":true, "items": [

	{
		xtype: 'form',
		layout: {
                type: 'vbox',
                align: 'stretch'
            },
		flex: 1, frame: true,
		padding:    '10 10 10 10',
		margins:    '10 10 10 10',
		items: [{
		            xtype: 'textfield',
		            disabled: true,
		            itemId: this.id + '-title',
		            name: Extensible.calendar.data.EventMappings.Title.name,
		            fieldLabel:'Prenotazione',
		            anchor: '100%',
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        },{
		            xtype: 'extensible.daterangefield',
		            itemId: this.id + '-dates',
		            name: 'dates',
		            anchor: '95%',
		            singleLine: true,
		            fieldLabel: 'Orario',
		            showAllDay: false,
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        }

		        
		        ,{
		            xtype: 'combo',	  
		            hidden: true,                   
					itemId: 'risorsa',
	                name: Extensible.calendar.data.EventMappings.Risorsa.name,				
					fieldLabel: 'Risorsa',
					forceSelection: true,
	                anchor: '100%',	
	                delimiter: ' , ', 
	                editable: false, 
	                multiSelect: true,                 
	                
					store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [<?php echo acs_ar_to_select_json($main_module->find_TA_std('RISEN'), ""); ?>]
					},
	            
					valueField: 'id',                       
		            displayField: 'text',
		            
		            padding:    '10 0 10 0',
					margins:    '0 0 0 0'				
		        }
		       
		        
		        
		        ],
		        
		buttons: [
			{
                itemId: 'delete-btn',
                iconCls: 'icon-delete',                
                text: 'Elimina prenotazione',
                disabled: false,
                handler: function() {
                	rec = this.up('window').open_vars.record;                	                	
					rec.store.remove(rec);

                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();					
									this.up('window').close();
                	
                },
                //scope: this,
                minWidth: 150,
                hideMode: 'offsets'
            },
            {
                text: 'Salva',
                iconCls: 'icon-save',
                disabled: false,
                handler: function(){
                	rec = this.up('window').open_vars.record; 
			        var fields = rec.fields,
			            values = this.up('form').getForm().getValues(),
			            name,
			            M = Extensible.calendar.data.EventMappings,			            
			            obj = {};
			
			        fields.each(function(f) {
			            name = f.name;
			            if (name in values) {
			                obj[name] = values[name];
			            }
			        });
			        
			        rangefields = this.up('form').query('[xtype="extensible.daterangefield"]');
			        Ext.each(rangefields, function(rf) {
				        var dates = rf.getValue();
				        obj[M.StartDate.name] = dates[0];
				        obj[M.EndDate.name] = dates[1];
				        obj[M.IsAllDay.name] = dates[2];
				    });    
			        
			            rec.beginEdit();
        				rec.set(obj);
        				rec.endEdit();
        					
                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();
												
						
									this.up('window').close();
        					
                
                
                }
                //scope: this
            }
		],        
		        
		        
		listeners: {
				afterrender: function(comp){					

					rec = comp.up('window').open_vars.record;
					rec.set('Risorsa', rec.get('Risorsa').split(','));					
					comp.loadRecord(rec);
					comp.getForm().findField(Extensible.calendar.data.EventMappings.Risorsa.name).setValue(rec.get(Extensible.calendar.data.EventMappings.Risorsa.name).split(','));					
					
					rangefields = comp.query('[xtype="extensible.daterangefield"]');
					Ext.each(rangefields, function(rf) {
						
							//carico i valori per il datarange
							rf.setValue(rec.data);
							
							//disabilito i campi data (puo' modificare solo l'ora)
							Ext.each(rf.query('datefield'), function(dtrf) {
								dtrf.disable();
							});
							
					});
				 
				}
				
                , 'eventdelete': {
                        fn: function(win, rec){
                        	console.log('----- eventdelete ----');
                        	console.log(rec);
                            this.eventStore.remove(rec);
                            this.eventStore.sync();
                            win.hide();
                            this.showMsg('Event '+ rec.data.Title +' was deleted');
                        },
                        scope: this
                    }				
				
							 					 
		}		        
		        
	}

]}	
	
	
<?php exit; } ?>
