<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();


// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_form_parametri_stampa'){
	$m_params = acs_m_params_json_decode();	
	?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
			            
            
            items: [
            	{xtype: 'hidden', name: 'cod_gruppo', value: <?php echo j($m_params->cod_gruppo); ?>}
            	, {xtype: 'hidden', name: 'cod_fornitore', value: <?php echo j($m_params->cod_fornitore); ?>}
				, {xtype: 'hidden', name: 'open_pars', value: <?php echo j(acs_je($m_params->open_pars)); ?>}            	            	
            ]
            
			, buttons: [			
			{
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',	            
	            handler: function() {
	                this.up('form').submit({
                        url: 'acs_report_b_mrp_art_scaduti.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',
                        params : {},
                  });
	                
	            }
	        }]            
            
        }
      ]
}
<?php exit;} ?>        



<?php




function sql_where_flt_by_json_POST(){
	$m_params = acs_m_params_json_decode();

	$m_where = '';
	
	if (isset($m_params->selected_id) && count($m_params->selected_id) > 0)
		$m_where .= " AND WSFORN IN (" . sql_t_IN($m_params->selected_id) . ") ";
	if (strlen(trim($_REQUEST['f_gruppo'])) > 0)
		$m_where .= " AND WSSEL3 = '" . trim($_REQUEST['f_gruppo']) . "'";
	//marchio	 (multiselect)
	if (isset($m_params->f_marchio) && count($m_params->f_marchio) > 0)
		$m_where .= " AND WSCLME IN (" . sql_t_IN($m_params->f_marchio) . ") ";	
	//grupo merceologico (multiselect)
	if (isset($m_params->f_gruppo_merceologico) && count($m_params->f_gruppo_merceologico) > 0)
		$m_where .= " AND WSGRME IN (" . sql_t_IN($m_params->f_gruppo_merceologico) . ") ";	
 return $m_where;		
}

function sql_where_by_sezione($sezione){
	switch ($sezione){
		case "ORDINATO": return " WSTIDO = 'VO' AND WSTPEL = 'R' ";
		case "SPEDITO_CONSEGNATO": return " WSTIDO = 'VO' AND WSTPEL = 'E' ";
		case "FATTURATO": return " WSTIDO = 'VF' AND WSTPEL = 'R' ";
		default: " ABCDE = NNNNNNN";				
	}
}






function add_array_valori_liv(&$ar, $r){
	//venduto globale
		$ar["WSTLIS"] 	+= $r['WSTLIS'];
		$ar["WSTNRI"] 	+= $r['WSTNRI'];
		if ($ar['WSTLIS'] != 0) $ar["WST_M"] = ($ar['WSTLIS'] - $ar['WSTNRI']) / $ar['WSTLIS'] *100;
		if ($r['WSTQTA'] > 0 || $ar['liv'] == 'liv_3') 	$ar["WSTQTA"] 	+= $r['WSTQTA'];
		
	//venduto non promozionale
		$ar["WSSLIS"] 	+= $r['WSSLIS'];
		$ar["WSSNRI"] 	+= $r['WSSNRI'];
		if ($ar['WSSLIS'] != 0) $ar["WSS_M"] = ($ar['WSSLIS'] - $ar['WSSNRI']) / $ar['WSSLIS'] *100;
		if ($ar['WSTNRI'] != 0) $ar["WSS_I"] = ($ar['WSSNRI'] / $ar['WSTNRI']) * 100;				
		if ($r['WSSQTA'] > 0 || $ar['liv'] == 'liv_3') 	$ar["WSSQTA"] 	+= $r['WSSQTA'];
		if ($ar['WSTQTA'] != 0) $ar["WSS_Q"] = ($ar['WSSQTA'] / $ar['WSTQTA']) * 100;				
		
		$ar["WSCLIS"] 	+= $r['WSCLIS'];
		$ar["WSCNRI"] 	+= $r['WSCNRI'];
		if ($ar['WSCLIS'] != 0) $ar["WSC_M"] = ($ar['WSCLIS'] - $ar['WSCNRI']) / $ar['WSCLIS'] *100;
		if ($ar['WSTNRI'] != 0) $ar["WSC_I"] = ($ar['WSCNRI'] / $ar['WSTNRI']) * 100;		
		if ($r['WSCQTA'] > 0 || $ar['liv'] == 'liv_3') 	$ar["WSCQTA"] 	+= $r['WSCQTA'];
		if ($ar['WSTQTA'] != 0) $ar["WSC_Q"] = ($ar['WSCQTA'] / $ar['WSTQTA']) * 100;		
		
		
	//venduto altre promo
		$ar["WSPLIS"] 	+= $r['WSPLIS'];
		$ar["WSPNRI"] 	+= $r['WSPNRI'];
		if ($ar['WSPLIS'] != 0) $ar["WSP_M"] = ($ar['WSPLIS'] - $ar['WSPNRI']) / $ar['WSPLIS'] *100;
		if ($ar['WSTNRI'] != 0) $ar["WSP_I"] = ($ar['WSPNRI'] / $ar['WSTNRI']) * 100;		
		if ($r['WSPQTA'] > 0 || $ar['liv'] == 'liv_3') 	$ar["WSPQTA"] 	+= $r['WSPQTA'];
		if ($ar['WSTQTA'] != 0) $ar["WSP_Q"] = ($ar['WSPQTA'] / $ar['WSTQTA']) * 100;				
		
	//venduto atipico
		$ar["WSALIS"] 	+= $r['WSALIS'];
		$ar["WSANRI"] 	+= $r['WSANRI'];
		if ($ar['WSALIS'] != 0) $ar["WSA_M"] = ($ar['WSALIS'] - $ar['WSANRI']) / $ar['WSALIS'] *100;
		if ($ar['WSTNRI'] != 0) $ar["WSA_I"] = ($ar['WSANRI'] / $ar['WSTNRI']) * 100;
		if ($r['WSAQTA'] > 0 || $ar['liv'] == 'liv_3') 	$ar["WSAQTA"] 	+= $r['WSAQTA'];
		if ($ar['WSTQTA'] != 0) $ar["WSA_Q"] = ($ar['WSAQTA'] / $ar['WSTQTA']) * 100;		
}




// ******************************************************************************************
// DATI PER SELECT TIPO APPROVIGGIONAMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tipo_approv'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT WSCELME as id, WSDCLM as text FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE WSTRIG = 'F'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


// ******************************************************************************************
// DATI PER SELECT marchio
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_marchio'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT WSCLME as id, WSDCLM as text FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE WSTRIG = 'F'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['TEXT'] = trim($r['TEXT']);
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}





// ******************************************************************************************
// DATI PER SELECT GRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_gruppo_fornitore'){

	//costruzione sql
	global $main_module, $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT WSSEL3 as id, WSDSE3 AS text FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE WSTRIG = 'F'";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['TEXT'] = $main_module->decod_std('ITIN', $r['ID']);
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}

// ******************************************************************************************
// DATI PER SELECT GRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_gruppo_merceologico'){

	//costruzione sql
	global $main_module, $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT WSGRME as ID, WSDGRM  AS TEXT FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE WSTRIG = 'F' ORDER BY 2";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['TEXT'] = trim($r['TEXT']);
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


// ******************************************************************************************
// DATI PER GRID FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT WSFORN AS COD_FOR, WSRGS1 AS DES_FOR 
			FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE WSTRIG <> 'E' ";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}





/*************************************************************************
 * GET JSON DATA (RIVENDITA PROMOZIONALE)
 *************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data'){
	global $conn, $cfg_mod_DeskAcq;

	

	if (isset($_REQUEST['node']) && strlen($_REQUEST['node']) > 0 && $_REQUEST['node'] != 'root'){
		$subtree_open = true;
		//sto aprendo un nodo articolo
		$ar_livs = loc_get_ar_livs($_REQUEST['node']);
		
		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE " . sql_where_by_sezione($_REQUEST['sezione']) . " AND WSTRIG = 'A'" . sql_where_flt_by_json_POST();
		$sql .= " AND WSFORN = ? AND WSCLME = ? AND WSGRME = ?";
		$sql .= " ORDER BY WSTQTA DESC";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, $ar_livs);
	} else {
		//livello principale
		$subtree_open = false;
		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE " . sql_where_by_sezione($_REQUEST['sezione']) . " AND WSTRIG = 'F' " . sql_where_flt_by_json_POST();
		$sql .= " ORDER BY WSTNRI DESC";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);		
	}
	
	//CREAZIONE ARRAY AD ALBERO (Referente - Fornitore - Data - Articolo)
	while ($r = db2_fetch_assoc($stmt)) {	

		//print_r($r);
		
		$liv0_v = trim($r['WSFORN']); 	$liv0_d = trim($r['WSRGS1']); //fornitore
		$liv1_v = trim($r['WSCLME']); 	$liv1_d = trim($r['WSDCLM']); //marchio
		$liv2_v = trim($r['WSGRME']); 	$liv2_d = trim($r['WSDGRM']); //gruppo		
		$liv3_v = trim($r['WSART']); 	$liv3_d = trim($r['WSDART']); //articolo (tipo record = 'A')		
	
		$tmp_ar_id = array();
		$n_children = "children";
	
		// LIVELLO 1
		$s_ar = &$my_array;
		$liv_c     = $liv0_v; $liv_d     = $liv0_d;		
		$tmp_ar_id[] = 'liv1;' . $liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_1";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
			$s_ar[$liv_c]["task"] = $liv_d . " [{$liv_c}]";
		}
		add_array_valori_liv($s_ar[$liv_c], $r);
		$s_ar = &$s_ar[$liv_c][$n_children];
			
		// LIVELLO 2
		$liv_c     = $liv1_v; $liv_d     = $liv1_d;
		$tmp_ar_id[] = 'liv2;' .$liv_c;
				if (is_null($s_ar[$liv_c])){
				$s_ar[$liv_c]["liv"]  = "liv_2";
				$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
				$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
				$s_ar[$liv_c]["task"] = $liv_d . " [{$liv_c}]";
		}
		add_array_valori_liv($s_ar[$liv_c], $r);
		$s_ar = &$s_ar[$liv_c][$n_children];
					

		// LIVELLO 3
		$liv_c     = $liv2_v; $liv_d     = $liv2_d;
		$tmp_ar_id[] = 'liv3;' .$liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_3";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
			$s_ar[$liv_c]["task"] = $liv_d . " [{$liv_c}]";
		}
		add_array_valori_liv($s_ar[$liv_c], $r);
		$s_ar = &$s_ar[$liv_c][$n_children];
					
		
		if ($subtree_open){
			// LIVELLO 4
			$liv_c     = $liv3_v; $liv_d     = $liv3_d;
			$tmp_ar_id[] = 'liv4;' .$liv_c;
			if (is_null($s_ar[$liv_c])){
				$s_ar[$liv_c]["leaf"]  = true;
				$s_ar[$liv_c]["liv"]  = "liv_4";
				$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
				$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
				$s_ar[$liv_c]["task"] = $liv_d . " [{$liv_c}]";
			}
			add_array_valori_liv($s_ar[$liv_c], $r);
			$s_ar = &$s_ar[$liv_c][$n_children];
		}		
		
	
	}

	if ($subtree_open){
		foreach($ar_livs as $kl => $l){						
			$my_array = &$my_array[$l][$n_children];
		}
	}
	
	
	
	if ($subtree_open){		
		//ordinamento (qta, desc)
		usort($my_array, "cmp_by_qta_desc"); //LIV1		
	} else {	
		//ordinamento (netto globale, desc)
		usort($my_array, "cmp_by_netto_globale_desc"); //LIV1
		
		foreach($my_array as $kar => $r){
			if (isset($my_array[$kar]['children'])){
				usort($my_array[$kar]['children'], "cmp_by_netto_globale_desc");  //LIV2
			
				foreach($my_array[$kar]['children'] as $kar3 => $r3){
					if (isset($my_array[$kar]['children'][$kar3]['children']))
						usort($my_array[$kar]['children'][$kar3]['children'], "cmp_by_netto_globale_desc"); //LIV3
				}		
			}				
		}
	}	

	
	foreach($my_array as $kar => $r)
		$ar_ret[] = array_values_recursive($my_array[$kar]);
	
	echo acs_je($ar_ret);
	
    exit();
}





/*************************************************************************
 * GET JSON DATA (CAMPAGNA)
*************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data_camp'){
	global $conn, $cfg_mod_DeskAcq;


	if (isset($_REQUEST['node']) && strlen($_REQUEST['node']) > 0 && $_REQUEST['node'] != 'root'){
		$subtree_open = true;
		//sto aprendo un nodo articolo
		$ar_livs = loc_get_ar_livs($_REQUEST['node']);
		$k_liv2s = explode("___", $ar_livs['liv2']);

		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE " . sql_where_by_sezione($_REQUEST['sezione']) . " AND WSTRIG = 'C'" . sql_where_flt_by_json_POST();
		$sql .= " AND WSFORN = ? AND WSCAMP = ?";		
		$sql .= " ORDER BY WSTQTA DESC";
		
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($ar_livs['liv1'], $k_liv2s[1]));
	} else {
		//livello principale
		$subtree_open = false;
		$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']} WHERE " . sql_where_by_sezione($_REQUEST['sezione']) . " AND WSTRIG = 'S' " . sql_where_flt_by_json_POST();
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
	}

	//CREAZIONE ARRAY AD ALBERO (Referente - Fornitore - Data - Articolo)
	while ($r = db2_fetch_assoc($stmt)) {

		$liv0_v = trim($r['WSFORN']); 	$liv0_d = trim($r['WSRGS1']); //fornitore
		
		if ($subtree_open){
			$liv1_v = trim($r['WSCAMP']); 	$liv1_d = trim($r['WSDART']); //campagna
		}
		else {
			$liv1_v = implode("___", array(trim($r['WSART']), trim($r['WSCAMP']))); 	$liv1_d = trim($r['WSDART']); //campagna
		}			

		$liv2_v = trim($r['WSGRME']); 	$liv2_d = trim($r['WSDGRM']); //gruppo
		$liv3_v = trim($r['WSART']); 	$liv3_d = trim($r['WSDART']); //articolo (tipo record = 'A')

		$tmp_ar_id = array();
		$n_children = "children";

		// LIVELLO 0
		$s_ar = &$my_array;
		$liv_c     = $liv0_v; $liv_d     = $liv0_d;
		$tmp_ar_id[] = 'liv1;' . $liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_1";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
			$s_ar[$liv_c]["task"] = $liv_d . " [{$liv_c}]";
		}
		add_array_valori_liv($s_ar[$liv_c], $r);
		$s_ar = &$s_ar[$liv_c][$n_children];
			
		// LIVELLO 1
		$liv_c     = $liv1_v; $liv_d     = $liv1_d;
		$tmp_ar_id[] = 'liv2;' .$liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_2";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
			$s_ar[$liv_c]["task"] = acs_u8e($liv_d . " [{$liv_c}]");
		}
		add_array_valori_liv($s_ar[$liv_c], $r);
		$s_ar = &$s_ar[$liv_c][$n_children];
			

		if ($subtree_open){		
		// LIVELLO 2
		$liv_c     = $liv2_v; $liv_d     = $liv2_d;
		$tmp_ar_id[] = 'liv3;' .$liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_3";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
			$s_ar[$liv_c]["task"] = $liv_d . " [{$liv_c}]";
		}
		add_array_valori_liv($s_ar[$liv_c], $r);
		$s_ar = &$s_ar[$liv_c][$n_children];

			// LIVELLO 3
			$liv_c     = $liv3_v; $liv_d     = $liv3_d;
			$tmp_ar_id[] = 'liv4;' .$liv_c;
			if (is_null($s_ar[$liv_c])){
				$s_ar[$liv_c]["leaf"]  = true;
				$s_ar[$liv_c]["liv"]  = "liv_4";
				$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
				$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
				$s_ar[$liv_c]["task"] = $liv_d . " [{$liv_c}]";
			}
			add_array_valori_liv($s_ar[$liv_c], $r);
			$s_ar = &$s_ar[$liv_c][$n_children];
		}


	}

	
	if ($subtree_open){
		foreach($ar_livs as $kl => $l){
			if ($subtree_open && $kl == 'liv2')
				$my_array = &$my_array[$k_liv2s[1]][$n_children];
			else								
				$my_array = &$my_array[$l][$n_children];
		}
	}
	
	
	
	//ordinamento (netto globale, desc) xxxxxxx
	usort($my_array, "cmp_by_netto_globale_desc");	
	foreach($my_array as $kar => $r){
		if (isset($my_array[$kar]['children']))
			usort($my_array[$kar]['children'], "cmp_by_netto_globale_desc");
	}
	
	
	foreach($my_array as $kar => $r)
		$ar_ret[] = array_values_recursive($my_array[$kar]);


	echo acs_je($ar_ret);

	exit();
}






// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
//            title: 'Selezione dati',
            
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
			            
            
            items: [
            
            
            					 	
					, {					
						xtype: 'grid',
						flex: 1,
						id: 'tab-tipo-stato',
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},
     					
						features: [
						{
							ftype: 'filters',
							// encode and local configuration options defined previously for easier reuse
							encode: false, // json encode the filter query
							local: true,   // defaults to false (remote filtering)
					
							// Filters are most naturally placed in the column definition, but can also be added here.
						filters: [
						{
							type: 'boolean',
							dataIndex: 'visible'
						}
						]
						}],     					
     					
									
						selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},				
						//columnLines: true,			
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fornitore',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
								
							fields: ['COD_FOR', 'DES_FOR']
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [{header: 'Fornitore', dataIndex: 'COD_FOR', flex: 2, filter: {type: 'string'}, filterable: true},
								  {header: 'Denominazione', dataIndex: 'DES_FOR', flex: 8, filter: {type: 'string'}, filterable: true}]
						 
					}            
            
            
            		, {            		
						margin: "0 0 0 2",            		
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			height: 50,
            			
			            layout: {
			                type: 'hbox',
			                align: 'stretch',
			                pack  : 'start'
			     		},            			
            			
     					items: [
     					
					//SELECT PER GRUPPO E SOTTOGRUPPO
						{
								margin: "0 10 0 10",
								flex: 1,					
								name: 'f_gruppo',
								xtype: 'combo',
								fieldLabel: 'Gruppo fornitura',
								labelAlign: 'top',
								width: 290,
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: true,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  }, {
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_marchio',
								xtype: 'combo',
								fieldLabel: 'Marchio',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
							   	multiSelect: true,
					            store: {
					                autoLoad: true,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_marchio',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            }
							    						 
							  }, {
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_gruppo_merceologico',
								xtype: 'combo',
								fieldLabel: 'Gr. merceologico',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
							   	multiSelect: true,
					            store: {
					                autoLoad: true,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo_merceologico',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            }
							    						 
							  }    			
     					
     					]
     				}            
            
     				
	
						 
				],
			buttonAlign:'center',	
			buttons: [
				
				
				
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "AN_RIV_PROMO");  ?>				
			
			
			 ,{
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

	            Ext.getBody().mask('Loading... ', 'loading').show();
	            
				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();
  				list_selected_id = [];
  				for (var i=0; i<id_selected.length; i++) 
	   				list_selected_id.push(id_selected[i].data.COD_FOR);
				
				
				m_values = form_p.getValues();
				m_values['selected_id'] = list_selected_id;
	            
				//carico la form dal json ricevuto da php e la inserisco nel m-panel
				Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_grid',
				        method     : 'POST',
				        jsonData: m_values,

				        waitMsg    : 'Data loading',
				        success : function(result, request){			        	
				        	
				            var jsonData = Ext.decode(result.responseText);				        	

				        	// WINDOW MAXIMIZE
								my_new_w = new Ext.Window({
									  width: 700
									, height: 500
									, plain: true
									, title: 'Analisi rivendite promozionali'
									, layout: 'fit'
									, border: true
									, closable: true
							    	, iconCls: 'icon-shopping_cart_gray-16'
							    	, maximizable: true
								});	
								
									my_new_w.show().maximize();
						            my_new_w.add(jsonData.items);
						            my_new_w.doLayout();									
				        			Ext.getBody().unmask();
				        	
				        
				        
				        	// ALL'INTERNO DEL TAB-PANEL
				        	/*
								mp = Ext.getCmp('m-panel');					        					        
								mp.remove('rivendite_promozionali');					        
					            
				            	mp.add(jsonData.items);
				            	mp.doLayout();
				            	mp.items.last().show();
				            */	
				            
 							this.print_w.close();
						             
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				        				        
				    });
				    
	            
            	                	                
	            }
	        }


	        
	        
	       ],             
				
        }
]}			            

<?php
exit;
}
 







/***********************************************************************************
 * TREE json
 ***********************************************************************************/
if ($_REQUEST['fn'] == 'get_json_grid'){

 $m_params = acs_m_params_json_decode();
 $list_selected_stato = $m_params->selected_id;
 $list_selected_stato_js = acs_je(implode("|", $list_selected_stato));

?>

{"success":true, "items":

  {
    xtype: 'panel',
	id: 'rivendite_promozionali',
	    
//// NON SERVE SE E' APERTO COME WINDOWS MAXIMIZE
	/// title: 'Analisi rivendite promozionali',
	/// closable: true,
	 	
    layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},    
    items: [
    
    // TREE RIVENDITE PROMOZIONALI (ORDINATO) -----------------------------------------------------
 	{
		xtype: 'treepanel',  flex: 1,
		selType: 'cellmodel',
		cls: 'acs-light',		
        title: 'ORDINATO - Rivendite promozionali per fornitore' + <?php echo j(data_ultimo_aggiornamento_file()); ?>,
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        da_data: 0,        
        
		store: Ext.create('Ext.data.TreeStore', {
	        fields: ['task', 'liv', 'liv_cod' 
	        			, 'WSTLIS', 'WSTNRI', 'WST_M', 'WSTQTA' 
	        			, 'WSSLIS', 'WSSNRI', 'WSS_M', 'WSS_I', 'WSS_Q', 'WSSQTA'	        			
	        			, 'WSCLIS', 'WSCNRI', 'WSC_M', 'WSC_I', 'WSC_Q', 'WSCQTA'	        			
	        			, 'WSPLIS', 'WSPNRI', 'WSP_M', 'WSP_I', 'WSP_Q', 'WSPQTA'
	        			, 'WSALIS', 'WSANRI', 'WSA_M', 'WSA_I', 'WSA_Q', 'WSAQTA'
	        		],
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&sezione=ORDINATO',
				
				extraParams: <?php echo acs_raw_post_data() ?>,
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				doRequest: personalizza_extraParams_to_jsonData,
					        
				actionMethods: {
					read: 'POST'
				},

				reader: {
					type: 'json',
				//	root: 'events'
				},					
				  
	        },
        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        closable: true,
		
		listeners: {
              		load: function(sender, node, records) {              		
                      	Ext.getBody().unmask();												
              		}
              		
					, itemcontextmenu : function(grid, rec, node, index, event) {
								   }	      		
              		
			      , celldblclick: {										      		
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
				    		col_name = iView.getGridColumns()[iColIdx].dataIndex;
						  	if (rec.get('liv') == 'liv_4')
						  		acs_show_win_std('Campagne abbinate: ' + rec.get('task'), '<?php echo $_SERVER['PHP_SELF']; ?>?fn=dett_campagne_abbinate', 
						  				{liv_4_cod: rec.get('liv_cod'),
						  				 liv_3_cod: rec.parentNode.get('liv_cod'),
						  				 liv_2_cod: rec.parentNode.parentNode.get('liv_cod'),
						  				 liv_1_cod: rec.parentNode.parentNode.parentNode.get('liv_cod')}, 600, 550, null, 'icon-shopping_cart_gray-16');
						}
			    	}              		
              		
              		
    },    		
		viewConfig: {
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
		        	if (v == 'liv_1') v = 'liv_1_no_b';
		            return v;					            
		         	}   
		    },			


		<?php
				$ss = implode(" / ", array('Fornitore', 'Marchio', 'Gruppo', 'Articolo'));
		?>

        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 10,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>'
        }, {
			 header: 'Venduto globale', flex: 3,
			 columns: [
			 	{header: 'Listino',  dataIndex: 'WSTLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSTNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WST_M', width: 60, renderer: floatRenderer1, align: 'right'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSTQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto non promozionale', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSSLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSSNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSS_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSS_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%QT',  dataIndex: 'WSS_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSSQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto campagne promo', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSCLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSCNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSC_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSC_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},
			 	{header: '%QT',  dataIndex: 'WSC_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},
				{header: 'Q.t&agrave;',  dataIndex: 'WSCQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto altre promo', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSPLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSPNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSP_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSP_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},
			 	{header: '%QT',  dataIndex: 'WSP_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 				 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSPQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto atipico', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSPLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSANRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{hidden: true, header: '%SM',  dataIndex: 'WSP_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSP_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},
			 	{header: '%QT',  dataIndex: 'WSA_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 				 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSAQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}]   
  
 	}   //tree per fornitore - ORDINATO
    
    
    
    ,
    
    
    
    // TREE CAMPAGNE PROMOZIONALI  -----------------------------------------------------
 	{
		xtype: 'treepanel', flex: 1,
		selType: 'cellmodel',
		cls: 'acs-light',		
        title: 'ORDINATO - Campagne promozionali' + <?php echo j(data_ultimo_aggiornamento_file()); ?>, 
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        
		store: Ext.create('Ext.data.TreeStore', {
	        fields: ['task', 'liv', 'liv_cod' 
	        			, 'WSTLIS', 'WSTNRI', 'WST_M', 'WSTQTA' 
	        			, 'WSSLIS', 'WSSNRI', 'WSS_M', 'WSS_I', 'WSS_Q', 'WSSQTA'	        			
	        			, 'WSCLIS', 'WSCNRI', 'WSC_M', 'WSC_I', 'WSC_Q', 'WSCQTA'	        			
	        			, 'WSPLIS', 'WSPNRI', 'WSP_M', 'WSP_I', 'WSP_Q', 'WSPQTA'
	        			, 'WSALIS', 'WSANRI', 'WSA_M', 'WSA_I', 'WSA_Q', 'WSAQTA'	        				        			
	        		],
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_camp&sezione=ORDINATO',
				
				extraParams: <?php echo acs_raw_post_data() ?>,
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				doRequest: personalizza_extraParams_to_jsonData,
					        
				actionMethods: {
					read: 'POST'
				},

				reader: {
					type: 'json',
				//	root: 'events'
				},						
					            
	        },
	        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        closable: true,
		
		listeners: {
              		load: function(sender, node, records) {              		
                      	Ext.getBody().unmask();												
              		}
              		
					, itemcontextmenu : function(grid, rec, node, index, event) {
								   }	      		
              		
			      , celldblclick: {										      		
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						}
			    	}              		
              		
              		
    },    		
		viewConfig: {
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
		        	if (v == 'liv_1') v = 'liv_1_no_b';
		            return v;					            
		         	}   
		    },			


		<?php
				$ss = implode(" / ", array('Fornitore', 'Campagna', 'Gruppo', 'Articolo'));
		?>

        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 10,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>'
        }, {
			 header: 'Promozione globale', flex: 3,
			 columns: [
			 	{header: 'Importo',  dataIndex: 'WSTNRI', width: 80, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%IT',  dataIndex: 'WSP_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 				 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSTQTA', width: 80, renderer: floatRenderer0, align: 'right',
			 	            renderer: function (value, metaData, record, row, col, store, gridView){
		 	            
			 	            	if (record.get('liv') == 'liv_2'){
			 	            	 //segnalazioni su qta
			 	            	 if (parseInt(record.get('WSTQTA')) > parseInt(record.get('WSSQTA')))
			 	            	 	metaData.tdCls += ' sfondo_rosso grassetto';
			 	            	 else if (parseInt(record.get('WSTQTA')) > parseInt(record.get('WSPQTA')))
			 	            	 	metaData.tdCls += ' sfondo_giallo grassetto';		 
			 	            	 }
			 	            	 	
			 	              return floatRenderer0(value); 
			 	            }				
				}
			 ]
		}, {
			 header: 'Budget', flex: 3,
			 columns: [
			 	{header: 'Q.t&agrave; limite',  dataIndex: 'WSSQTA', width: 80, align: 'right', tdCls: 'grassetto',
			 	            renderer: function (value, metaData, record, row, col, store, gridView){			 	            
			 	            	if (record.get('liv') == 'liv_2')
			 	            	 return floatRenderer0(value); 
			 	            }
			 	},			 				 				 	
				{header: 'Prima soglia',  dataIndex: 'WSPQTA', width: 80, align: 'right',
			 	            renderer: function (value, metaData, record, row, col, store, gridView){			 	            
			 	            	if (record.get('liv') == 'liv_2')
			 	            	 return floatRenderer0(value); 
			 	            }				
				}
			 ]
		}]   
  
 	}   //tree campagne promozionali - ORDINATO
 	
 	
 	
 	
    // TREE RIVENDITE PROMOZIONALI (SPEDITO_CONSEGNATO) -----------------------------------------------------
   , {
		xtype: 'treepanel',  flex: 1,
		selType: 'cellmodel',
		cls: 'acs-light',		
        title: 'SPEDITO/CONSEGNATO - Rivendite promozionali per fornitore' + <?php echo j(data_ultimo_aggiornamento_file()); ?>,
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        da_data: 0,        
        
		store: Ext.create('Ext.data.TreeStore', {
	        fields: ['task', 'liv', 'liv_cod' 
	        			, 'WSTLIS', 'WSTNRI', 'WST_M', 'WSTQTA' 
	        			, 'WSSLIS', 'WSSNRI', 'WSS_M', 'WSS_I', 'WSS_Q', 'WSSQTA'	        			
	        			, 'WSCLIS', 'WSCNRI', 'WSC_M', 'WSC_I', 'WSC_Q', 'WSCQTA'	        			
	        			, 'WSPLIS', 'WSPNRI', 'WSP_M', 'WSP_I', 'WSP_Q', 'WSPQTA'
	        			, 'WSALIS', 'WSANRI', 'WSA_M', 'WSA_I', 'WSA_Q', 'WSAQTA'	        			
	        		],
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&sezione=SPEDITO_CONSEGNATO',
				
				extraParams: <?php echo acs_raw_post_data() ?>,
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				doRequest: personalizza_extraParams_to_jsonData,
					        
				actionMethods: {
					read: 'POST'
				},

				reader: {
					type: 'json',
				//	root: 'events'
				},					
				  
	        },
        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        closable: true,
		
		listeners: {
              		load: function(sender, node, records) {              		
                      	Ext.getBody().unmask();												
              		}
              		
					, itemcontextmenu : function(grid, rec, node, index, event) {
								   }	      		
              		
			      , celldblclick: {										      		
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						}
			    	}              		
              		
              		
    },    		
		viewConfig: {
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
		        	if (v == 'liv_1') v = 'liv_1_no_b';
		            return v;					            
		         	}   
		    },			


		<?php
				$ss = implode(" / ", array('Fornitore', 'Marchio', 'Gruppo', 'Articolo'));
		?>

        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 10,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>'
        }, {
			 header: 'Venduto globale', flex: 3,
			 columns: [
			 	{header: 'Listino',  dataIndex: 'WSTLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSTNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WST_M', width: 60, renderer: floatRenderer1, align: 'right'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSTQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto non promozionale', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSSLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSSNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSS_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSS_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%QT',  dataIndex: 'WSS_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 				 				 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSSQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto campagne promo', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSCLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSCNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSC_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSC_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%QT',  dataIndex: 'WSC_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSCQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto altre promo', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSPLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSPNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSP_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSP_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%QT',  dataIndex: 'WSP_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSPQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto atipico', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSPLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSANRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{hidden: true, header: '%SM',  dataIndex: 'WSP_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSP_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},
			 	{header: '%QT',  dataIndex: 'WSA_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 				 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSAQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}]   
  
 	}   //tree per fornitore - SPEDITO_CONSEGNATO
 	
 	
 	
 	
 	
    // TREE RIVENDITE PROMOZIONALI (FATTURATO) -----------------------------------------------------
   , {
		xtype: 'treepanel',  flex: 1,
		selType: 'cellmodel',
		cls: 'acs-light',		
        title: 'FATTURATO - Rivendite promozionali per fornitore' + <?php echo j(data_ultimo_aggiornamento_file()); ?>,
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        da_data: 0,        
        
		store: Ext.create('Ext.data.TreeStore', {
	        fields: ['task', 'liv', 'liv_cod' 
	        			, 'WSTLIS', 'WSTNRI', 'WST_M', 'WSTQTA' 
	        			, 'WSSLIS', 'WSSNRI', 'WSS_M', 'WSS_I', 'WSS_Q', 'WSSQTA'	        			
	        			, 'WSCLIS', 'WSCNRI', 'WSC_M', 'WSC_I', 'WSC_Q', 'WSCQTA'	        			
	        			, 'WSPLIS', 'WSPNRI', 'WSP_M', 'WSP_I', 'WSP_Q', 'WSPQTA'
	        			, 'WSALIS', 'WSANRI', 'WSA_M', 'WSA_I', 'WSA_Q', 'WSAQTA'	        			
	        		],
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&sezione=FATTURATO',
				
				extraParams: <?php echo acs_raw_post_data() ?>,
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				doRequest: personalizza_extraParams_to_jsonData,
					        
				actionMethods: {
					read: 'POST'
				},

				reader: {
					type: 'json',
				//	root: 'events'
				},					
				  
	        },
        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        closable: true,
		
		listeners: {
              		load: function(sender, node, records) {              		
                      	Ext.getBody().unmask();												
              		}
              		
					, itemcontextmenu : function(grid, rec, node, index, event) {
								   }	      		
              		
			      , celldblclick: {										      		
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						}
			    	}              		
              		
              		
    },    		
		viewConfig: {
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
		        	if (v == 'liv_1') v = 'liv_1_no_b';
		            return v;					            
		         	}   
		    },			


		<?php
				$ss = implode(" / ", array('Fornitore', 'Marchio', 'Gruppo', 'Articolo'));
		?>

        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 10,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>'
        }, {
			 header: 'Venduto globale', flex: 3,
			 columns: [
			 	{header: 'Listino',  dataIndex: 'WSTLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSTNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WST_M', width: 60, renderer: floatRenderer1, align: 'right'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSTQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto non promozionale', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSSLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSSNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSS_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSS_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%QT',  dataIndex: 'WSS_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 				 				 				 				 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSSQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto campagne promo', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSCLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSCNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSC_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSC_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%QT',  dataIndex: 'WSC_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSCQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto altre promo', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSPLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSPNRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%SM',  dataIndex: 'WSP_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSP_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
			 	{header: '%QT',  dataIndex: 'WSP_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSPQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}, {
			 header: 'Venduto atipico', flex: 3,
			 columns: [
			 	{hidden: true, header: 'Listino',  dataIndex: 'WSPLIS', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'festivo'},
			 	{header: 'Netto',  dataIndex: 'WSANRI', width: 60, renderer: floatRenderer0, align: 'right', tdCls: 'grassetto'},			 	
			 	{hidden: true, header: '%SM',  dataIndex: 'WSP_M', width: 40, renderer: floatRenderer1, align: 'right'},			 	
			 	{hidden: true, header: '%IT',  dataIndex: 'WSP_I', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},
			 	{header: '%QT',  dataIndex: 'WSA_Q', width: 40, renderer: floatRenderer1, align: 'right', tdCls: 'grassetto'},			 				 	
				{header: 'Q.t&agrave;',  dataIndex: 'WSAQTA', width: 40, renderer: floatRenderer0, align: 'right'}
			 ]
		}]   
  
 	}   //tree per fornitore - FATTURATO
 	
     	     
    
    
    
    ]
  } //main panel 




}
<?php
exit; }


/***********************************************************************************
 * Dettaglio campagna (per articolo)
***********************************************************************************/
if ($_REQUEST['fn'] == 'dett_campagne_abbinate'){
$m_params = acs_m_params_json_decode();
?>

{"success": true, "items":
{
	xtype: 'gridpanel',

	store: Ext.create('Ext.data.Store', {
			
		groupField: 'RDTPNO',
		autoLoad:true,
		proxy: {
			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=det_campagne_abbinate_data',
			type: 'ajax',
			reader: {
				type: 'json',
				root: 'root'
			},
			extraParams: <?php echo acs_je($m_params); ?>
		},
		fields: ['WSCAMP', 'WSTNRI', 'WSTQTA', 'TADESC']
	}),

	columns: [
	{
		header   : 'Campagna',
		dataIndex: 'TADESC',
		flex     : 1,
	}, {
		header   : 'Codice',
		dataIndex: 'WSCAMP',
		width: 80
	}, {
		header   : 'Importo',
		dataIndex: 'WSTNRI',
		width     : 110, align: 'right',
		renderer: floatRenderer2
	}, {
		header   : 'Q.t&agrave;',
		dataIndex: 'WSTQTA',
		width     : 110, align: 'right',
		renderer: floatRenderer0
	}]

	, listeners: {
		afterrender: function (comp) {
		},
	 	
		celldblclick: {
			fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				rec = iView.getRecord(iRowEl);
			}
		}
			
	}
		
	, viewConfig: {
		getRowClass: function(rec, index) {
		}
	}
		
	 
}
}
<?php
exit; }




// ******************************************************************************************
// DATI campagne abbinate per articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'det_campagne_abbinate_data'){

	//costruzione sql
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$sql = "SELECT WSCAMP, TADESC, SUM(WSTNRI) AS WSTNRI, SUM(WSTQTA) AS WSTQTA 
				FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']}
				LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']}
				  ON TAID = 'CAMP' AND TADT = WSDT AND TANR = WSCAMP 
				 WHERE WSTRIG = 'C'
				AND WSFORN = ? AND WSCLME = ? AND WSGRME = ? AND WSART = ?
			GROUP BY WSCAMP, TADESC
			ORDER BY SUM(WSTNRI) DESC
			";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($_REQUEST['liv_1_cod'], $_REQUEST['liv_2_cod'], $_REQUEST['liv_3_cod'], $_REQUEST['liv_4_cod']));

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['TADESC'] = acs_u8e($r['TADESC']);
		$ar[] = $r;
	}

	echo acs_je($ar);
	exit();
}



function loc_get_ar_livs($node){
	$ar_livs = explode("|", $node);
	foreach($ar_livs as $liv){
		if (strlen(trim($liv)) > 0){
			$ar_liv = explode(";", $liv);
			$ret[$ar_liv[0]] = $ar_liv[1];
		}  
	}
 return $ret;	
}
function loc_get_ar_initial_node($node){
	$ar_livs = explode("|", $node);
	foreach($ar_livs as $liv){
		if (strlen(trim($liv)) > 0){
			$ar_liv = explode(";", $liv);
			$ret[] = $ar_liv[0] . ";" . $ar_liv[1];
		}
	}
	return $ret;
}


function cmp_by_netto_globale_desc($a, $b)
{
	//print_r($a);
	if ($a['WSTNRI'] == $b['WSTNRI']) {
		return 0;
	}
	return ($a['WSTNRI'] > $b['WSTNRI']) ? -1 : 1; //desc
}

function cmp_by_qta_desc($a, $b)
{
	//print_r($a);
	if ($a['WSTQTA'] == $b['WSTQTA']) {
		return 0;
	}
	return ($a['WSTQTA'] > $b['WSTQTA']) ? -1 : 1; //desc
}


function data_ultimo_aggiornamento_file(){
	global $conn, $cfg_mod_DeskAcq; 
	$sql = "SELECT MAX(WSDTGE) AS MAX_WSDTGE, MAX(WSORGE) AS MAX_WSORGE FROM {$cfg_mod_DeskAcq['file_analisi_rivendita_promo']}";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($_REQUEST['liv_1_cod'], $_REQUEST['liv_2_cod'], $_REQUEST['liv_3_cod'], $_REQUEST['liv_4_cod']));
	$r = db2_fetch_assoc($stmt);
	return ' [Aggiornamento dati: ' . print_date($r['MAX_WSDTGE']) . " " . print_ora($r['MAX_WSORGE']) . "]";	
}

?>