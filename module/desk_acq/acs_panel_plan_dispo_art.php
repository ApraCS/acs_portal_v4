<?php

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();


/*
 * Solo ordini M3TRIG = 'O'
 */


$f_icon = 'grafici';


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//----------------------------------------------------------------------
    //recupero dati
    $sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2 
            LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_wizard_MTS_3']} W3
             ON W2.M2DT = W3.M3DT AND W2.M2ART = W3.M3ART AND W2.M2PROG = W3.M3PROG AND M3TRIG='O'
	        WHERE M2FOR1 = ? 
            AND M2DT = '{$id_ditta_default}'";
    
    $sql.= sql_where_by_combo_value('M2TAB1', $m_params->open_request->form_values->f_gruppo);    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->open_request->id_fornitore));
    
    //ogni riga e' un articolo. Per ogni ordine imposto a "X" la colonna relativa 
    $ar  = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $liv1 = $row['M2ART'];
        $liv2 = _m3_k_ordine_encode($row);
        
        if (!isset($ar_r[$liv1])){
            $ar[$liv1] = array(
                'M2ART'     => trim($row['M2ART']),
                'M2DART'    => trim($row['M2DART'])
            );            
        }
        
        $ar[$liv1][$liv2] = 'X';        
        $ret[] = $row;
    }
    
    $ret = array();
    foreach($ar as $kar => $v){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je($ret);
    
 exit;
}


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------------
?>
{"success":true, 

	m_win: {
		title: 'Planning analisi disponibilit&agrave; articoli',
		iconCls: <?php echo j("icon-{$f_icon}-16") ?>
	},

	"items": [

        {
            xtype: 'form',
            layout: {
    			type: 'vbox',
    			align: 'stretch'    			
    		},
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            
            default: {width: '100%'},            
            
            items: [
         			{					
						xtype: 'grid',
						itemId: 'grid_fornitore',
						flex: 1,
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},
     					
						features: [
						{
							ftype: 'filters',
							// encode and local configuration options defined previously for easier reuse
							encode: false, // json encode the filter query
							local: true,   // defaults to false (remote filtering)
					
							// Filters are most naturally placed in the column definition, but can also be added here.
						filters: [
						{
							type: 'boolean',
							dataIndex: 'visible'
						}
						]
						}],     					
     					
									
						selModel: {selType: 'checkboxmodel', mode: 'SINGLE'},				
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: 'acs_wizard_MTS.php?fn=get_json_data_fornitore',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
								
						
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['M2FOR1', 'M2RGS1']
										
										
						}, //store
			
						columns: [{header: 'Fornitore', dataIndex: 'M2FOR1', flex: 2, filter: {type: 'string'}, filterable: true},
								  {header: 'Denominazione', dataIndex: 'M2RGS1', flex: 8, filter: {type: 'string'}, filterable: true}]
						 
					}, {
						xtype: 'textfield',
						name: 'f_gruppo',
						fieldLabel: 'Gruppo'
					}
          
          
			 
            ],
            
			buttons: [	
			{
		            text: 'Visualizza',
			        iconCls: 'icon-windows-32',	            
			        scale: 'large',		            
		            handler: function() {
		            
		            var form_p = this.up('form');		            
		            var grid_fornitore = form_p.down('#grid_fornitore');
		            
		            if (grid_fornitore.getSelectionModel().getSelection().length == 0){
		            	acs_show_msg_error('Selezionare un fornitore');
		            	return false;
		            }
		            
		            var id_fornitore = grid_fornitore.getSelectionModel().getSelection()[0].data.M2FOR1;
		            			            
		            	acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_tab', 
		            		null, {
		            			id_fornitore: id_fornitore,
		            			form_values: form_p.getValues()
		            		});
		            	this.up('window').destroy();			                
		            }
		        }
	        
	        
	        ]            

}		
]}
<?php 
	exit;
}



//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){
//----------------------------------------------------------------------
//parametro obbligatorio:
//   id_fornitore

//recupero gli ordini del fornitore, perche' ogni ordine sara' una colonna
    $sql = "SELECT DISTINCT M3DT, M3INUM, M3TPDO, M3AADO, M3NRDO FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
            LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_wizard_MTS_3']} W3
             ON W2.M2DT = W3.M3DT AND W2.M2ART = W3.M3ART AND W2.M2PROG = W3.M3PROG
	        WHERE M2FOR1 = ? AND M3TRIG = 'O'
            AND M2DT = '{$id_ditta_default}'";
    
    $sql.= sql_where_by_combo_value('M2TAB1', $m_params->form_values->f_gruppo);    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->id_fornitore));
    
    $ar_ordini = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $ar_ordini[] = array(
                'k_ordine' => _m3_k_ordine_encode($row),
                'h'        => "{$row['M3AADO']}-{$row['M3NRDO']}"
        );
    }
    
?>
 {
    success:true,
    items: [        
    	{
					xtype: 'grid',
					title: 'Plan dispo',
					<?php echo make_tab_closable(); ?>,
					flex: 1, layout: 'fit',
			        loadMask: true,	      			 
			        
        	        tbar: new Ext.Toolbar({
        		            items:[
        		            '<b>Plan disponibilit&agrave; articolo</b>', '->',
        		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ 
        		            	this.up('panel').getStore().load();
        		             }
        		            }
        		       		<?php echo make_tbar_closable() ?>
        		         ]            
        		     }),
			        
			        store: {
						xtype: 'store',					
						autoLoad:true,
		
						proxy: {
						   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
						   method: 'POST',								
						   type: 'ajax',

					       actionMethods: {read: 'POST'},							        
					       extraParams: {
					       	open_request: <?php echo acs_je($m_params); ?>	
					       },
	        				
	        			   doRequest: personalizza_extraParams_to_jsonData, 
					        
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
						
	       fields: <?php echo_je(_fields($ar_ordini)) ?>					
								
		}, //store
			columns: <?php echo_je(_cols_headers($ar_ordini)) ?>
    } //grid
   ]
   
  }
 
<?php 
	exit;
}



function _cols_headers($ar_ordini){
 $ret = array();
 //$ret[] = dx_mobile();
 $ret[] = array('header' => 'Codice', 'dataIndex' => 'M2ART', 'width' => 70);
 $ret[] = array('header' => 'Descrizione', 'dataIndex' => 'M2DART', 'width' => 300);
 foreach($ar_ordini as $ord){
     $ret[] = array('header' => $ord['h'], 
                    'dataIndex' => $ord['k_ordine'], 
                    'width' => 80);
 }
 return $ret;
}

function _fields($ar_ordini){
    $ret = array();
    //$ret[] = dx_mobile();
    $ret[] = 'M2ART';
    $ret[] = 'M2DART';
    foreach($ar_ordini as $ord){
        $ret[] = $ord['k_ordine'];
    }
    return $ret;
}

function _m3_k_ordine_encode($row){
    
    //M3DT, M3INUM, M3TPDO, M3AADO, M3NRDO    
    return implode('_', array_map('trim', array(
            $row['M3DT'],
            $row['M3INUM'],
            $row['M3TPDO'],
            $row['M3AADO'],
            $row['M3NRDO']
    )));
}