<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// ABBINA DA SISTEMARE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_abbina_file'){
    
    $ret = array();
    $ar_ins = array();
    $oe = $s->k_ordine_td_decode_xx($m_params->k_ordine);
    
    $sql = "SELECT COUNT(*) AS C_ROW, 
            RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC, RAPROG,
            RDANOR, RDNROR, RDTPOR
            FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA
            /*LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
      			ON RA.RADT=RD.RDDT AND RD.RDTIDO = RA.RATIDO AND RD.RDINUM = RA.RAINUM AND RD.RDAADO = RA.RAAADO AND RD.RDNRDO = RA.RANRDO AND RD.RDNREC=RA.RANREC							
            WHERE RANREC = {$m_params->nrec} AND RADT = ? AND RATIDO = ? AND RAINUM = ? AND RAAADO = ? AND RANRDO = ?
            GROUP BY RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC, RAPROG
            ORDER BY RAPROG DESC";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $oe);
    $row = db2_fetch_assoc($stmt);
    
    
    if($row['C_ROW'] > 0){
        $ar_ins['RAPROG'] = $row['RAPROG'] + 1;
    }else{
        $ar_ins['RAPROG'] = 1;
    }
    
    if(strlen($m_params->allegato_id) > 50){
        $ret['msg'] = 'Nome allegato superiore a 50 caratteri';
    }else{
        
        $ret['msg'] = "";
        //insert
        $ar_ins['RADT'] = $oe['TDDT'];
        $ar_ins['RATIDO'] = $oe['TDOTID'];
        $ar_ins['RAINUM'] = $oe['TDOINU'];
        $ar_ins['RAAADO'] = $oe['TDOADO'];
        $ar_ins['RANRDO'] = $oe['TDONDO'];
        $ar_ins['RANREC'] = $m_params->nrec;
        $ar_ins['RAALOR'] = $m_params->name;
        $ar_ins['RAFLAG'] = 'R';
        $ar_ins['RAPAR3'] = implode('_', array($row['RDTPOR'], $row['RDANOR'], $row['RDNROR']));

        $sql = "INSERT INTO {$cfg_mod_DeskPVen['file_allegati_righe']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg($stmt);
    }
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid_rows'){
    
    $ar = array();
    
    $m_params = acs_m_params_json_decode();
    $oe = $s->k_ordine_td_decode_xx($m_params->open_request->k_ordine);
    $fornitore = $m_params->open_request->cod_forn;
   
    
   /* $sql = "SELECT RRN(RA) AS RRN, RAALOR
            FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA
            WHERE RADT = ? AND RATIDO = ? AND RAINUM = ? AND RAAADO = ? AND RANRDO = ? ";*/

    $sql = " SELECT RRN(RA) AS RRN, RA.*
             /*FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD*/
             FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA
	 		 /*LEFT OUTER JOIN (
                       SELECT RAPROG, RAALOR, RADT, RATIDO, RAINUM, RAAADO, RANRDO, RANREC
                       FROM {$cfg_mod_DeskPVen['file_allegati_righe']}
                       ) RA
             ON RD.RDDT=RA.RADT AND RD.RDTIDO = RA.RATIDO AND RD.RDINUM = RA.RAINUM AND RD.RDAADO = RA.RAAADO AND RD.RDNRDO = RA.RANRDO AND RD.RDNREC=RA.RANREC*/                
             WHERE RADT = ? AND RATIDO = ? AND RAINUM = ? AND RAAADO = ? AND RANRDO = ?
            ";

    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $oe);
    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['anno'] = trim($row['RAAADO']);
        $nr['numero'] = trim($row['RANRDO']);
        $nr['anno_numero']  = $row['RAAADO']."_".sprintf("%06s", $row['RANRDO']);
        /*$nr['nrec'] = trim($row['RDNREC']);
        $nr['riga'] = trim($row['RDRIGA']);*/
        $nr['prog'] = trim($row['RAPROG']);
        $nr['display_name'] = trim($row['RAALOR']);
      
        $Path_Allegati_Sma1 = "\\sma1\scambio\"";
        $nr['path']  = $Path_Allegati_Sma1.$fornitore."/".trim($row['RAALOR']);
        
        
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_win'){ ?>

{"success":true, "items": [
{
				              
            xtype: 'grid', //panel 2
            autoScroll: true,
            flex : 1, 
            itemId: 'grid_attachments',
		 	store: {
				xtype: 'store',
				autoLoad:true,
					proxy: {
						   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_rows', 
						   method: 'POST',								
						   type: 'ajax',
                           actionMethods: {
					          read: 'POST'
					        },
					       extraParams: {
								 open_request: <?php echo acs_je($m_params) ?>
	        				},
	        			   doRequest: personalizza_extraParams_to_jsonData, 
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
					
        			fields: ['anno', 'numero', 'anno_numero', 'nrec', 'prog', 'riga', {name:'path'}, {name:'display_name'}]							
									
			}, //store
		 	
			
			  columns: [	
			    {
	                header   : 'Numero',
	                dataIndex: 'anno_numero',
	                width: 100
	            },
	            {
	                header   : 'Allegati',
	                dataIndex: 'display_name',
	                flex: 1
	            }

	      
				],
				
				 listeners: {
	         		 celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);
							  allegatiPopup('<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_image&nome=' + rec.get('display_name') + '&k_ordine=' + <?php echo j($m_params->k_ordine) ?>);
						  }
			   		  }
				  
				 },
	            
			/*dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
					 {
						xtype: 'button',
						text: 'Carica da PC',
						anchor: '96%',
						scale: 'small',
						iconCls: 'icon-sub_blue_add-16',
						handler: function() {
						
							

						
						} //handler		
					}                 
                ]
              }
             ],*/
             
				               
		} //grid allegati	

]}
    
<?php  
}