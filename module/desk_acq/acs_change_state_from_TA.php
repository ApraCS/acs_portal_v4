<?php

require_once "../../config.inc.php";

$m_DeskAcq = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'exe_cambia_stato'){
    
    $m_params = acs_m_params_json_decode();
    
    foreach($m_params->list_selected_id as $v){
        
        $sh = new SpedHistory($m_DeskAcq);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'ASS_ST_DDT',
                "k_ordine"	=> $v->k_ordine,
                "vals" => array(
                    "RICITI" => $m_params->form_values->f_stato,
                )
            )
            );
    }
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'open_form'){
    
    $m_params = acs_m_params_json_decode();
       
    
    ?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            
					buttons: ['->',
			         
			          {
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var loc_win = this.up('window');
			           		Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_stato',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?>,
 			        			    form_values: form.getValues()
 								},							        
 						        success : function(result, request){
 						        	var jsonData = Ext.decode(result.responseText);
 						        	loc_win.fireEvent('afterSave', loc_win);
		 			              },
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
		 				   });	
		 	
			            }
			         }	
						], items: [
						
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
					    items: [
				
						{
							name: 'f_stato',
							xtype: 'combo',
							flex:1,
							fieldLabel: 'Nuovo stato',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',					   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
	                               <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('NCSTA'), ''); ?> 	//, true, 'Y'
								    ] 
							}					 
						}, {
                         xtype : 'button',
	            		 iconCls: 'icon-gear-16',
	            		 margin : '0 0 0 10',
	            		 scale : 'small',
	            		 tooltip : 'Stato',
        		         handler: function(event, toolEl, panel){
        		           		 	acs_show_win_std('Gestione stati', '../desk_acq/acs_gest_stati_nc.php?fn=open_tab', {}, 950, 500, null, 'icon-gear-16');
        		            }
        		        }
					
						]}				 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit; 

	
	
}