<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	           	 {
						xtype: 'checkboxgroup',
						fieldLabel: 'Dettaglio righe',
						labelWidth: 90,
					    items: [{
                            xtype: 'checkbox'
                          , name: 'f_righe' 
                          , boxLabel: ''
                          , inputValue: 'Y'
                        }]							
					}
						
	            ],
	            
				buttons: [	
					
					{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	fornitore : <?php echo j($m_params->fornitore); ?>
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}

if ($_REQUEST['fn'] == 'open_report'){
    
    $form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
    $form_values = json_decode($form_values);
    
    $fornitore = $_REQUEST['fornitore'];
 
    $sql = "SELECT RF.*, TDAACA, TDNRCA, TDTICA, TDANRF, TDAARF, TDMMRF, TDGGRF, TDNRRF,
        TF2.TFTPDO AS TIPO, TF2.TFDTRG AS DATA
        FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_fatture']} RF
            ON TF.TFDOCU=RF.RFDOCU
        LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} TD
            ON RF.RFDT=TD.TDDT AND RF.RFTIDE=TD.TDTIDO AND RF.RFINUE=TD.TDINUM AND RF.RFAADE=TD.TDAADO AND RF.RFNRDE=TD.TDNRDO
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_testate_fatture']} TF2
            ON TF2.TFDT = TD.TDDT AND TF2.TFTIDO = TD.TDTICA AND TF2.TFAADO = TD.TDAACA AND TF2.TFNRDO = digits(TD.TDNRCA)
        WHERE TF.TFDT='{$id_ditta_default}' AND TF.TFFU01='N' AND TF.TFCCON = '{$fornitore}'
        AND TF.TFFU02 = '' AND RF.RFDT IS NOT NULL AND RF.RFFU02 <> 'Y'";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$sql_f = "SELECT CFRGS1 FROM {$cfg_mod_Spedizioni['file_anag_cli']} CF
        WHERE CFDT = '{$id_ditta_default}' AND CFCD = '{$fornitore}'";

$stmt_f = db2_prepare($conn, $sql_f);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_f);
$row = db2_fetch_assoc($stmt_f);
$desc_forn = $row['CFRGS1'];


$sql_rd = "SELECT RDART, RDDART, RTINFI
           FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
           LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
                ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
           WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
           AND RD.RDTISR = '' AND RD.RDSRIG = 0
           /*ORDER BY RD.RDRIGA*/";

$stmt_rd = db2_prepare($conn, $sql_rd);
echo db2_stmt_errormsg();



$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>
<html>
<head>
<meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{background-color: #cccccc; font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<div id='my_content'>
  		<div class=header_page>
			<H2>Riepilogo note credito attese fornitore <?php echo "[{$fornitore}] {$desc_forn}"; ?></H2>
 		</div>
		<div style="text-align: right; margin-bottom:10px; "> Data elaborazione: <?php echo Date('d/m/Y H:i') ?></div>

<table class=int1>
	<tr class='liv_data' >
		<th colspan =2>Fattura interna</th>
	
		<th colspan =2>Fattura fornitore</th>
		
		<th rowspan=2>Note credito attese</th>
		<th rowspan=2>Data registrazione</th>
		<th rowspan=2>Data evasione</th>
		<th rowspan=2>St.</th>
		<th rowspan=2>Imponibile</th>
		<th rowspan=2>Totale documento</th>
	</tr>
	<tr class='liv_data'>
         <th>Numero</th>
         <th>Data</th>
         <th>Numero</th>
         <th>Data</th>
    </tr>
   <?php 
   $tot_doc = 0;
   $tot_imp = 0;
   $n_r = 0;
   
   while ($r = db2_fetch_assoc($stmt)) {?>	
	<tr>
		<td><?php echo implode("_", array($r['TDAACA'], sprintf("%06s", $r['TDNRCA']), $r['TIPO']));?></td>
		<td><?php echo print_date($r['DATA'])?></td>
		<td><?php echo implode("_", array($r['TDANRF'], sprintf("%06s", $r['TDNRRF']))); ?></td>
		<td><?php echo print_date(implode("", array($r['TDAARF'], sprintf("%02s", $r['TDMMRF']), sprintf("%02s", $r['TDGGRF'])))); ?></td>
		<td><?php echo implode("_", array($r['RFAADE'], sprintf("%06s", $r['RFNRDE']), $r['RFTPDE'])); ?></td>
		<td><?php echo print_date($r['RFDTRG']); ?></td>
		<td><?php echo print_date($r['RFDTEP']); ?></td>
		<td><?php echo trim($r['RFSTAT']); ?></td>
		<td align=right><?php echo n($r['RFTIMP'], 2); ?></td>
		<td align=right><?php echo n($r['RFTOTD'], 2); ?></td>
	</tr>
	
	<?php 
	
	if($form_values->f_righe == 'Y'){
    	$k_ordine = implode("_", array($r['RFDT'], $r['RFTIDE'], $r['RFINUE'], $r['RFAADE'], sprintf("%06s", $r['RFNRDE'])));
    	$oe = $s->k_ordine_td_decode_xx($k_ordine);
    	$result = db2_execute($stmt_rd, $oe);
    	
    	while($row_rd = db2_fetch_assoc($stmt_rd)){
    	   $articolo = "[". trim($row_rd['RDART'])."] ".trim($row_rd['RDDART']);
    	   $importo = n($row_rd['RTINFI'],2);
    	   
    	   echo "<tr>
                 <td colspan = 5>&nbsp;</td>      
                 <td colspan = 3>{$articolo}</td>  
                 <td align=right>{$importo}</td>       
                 <td>&nbsp;</td>     
                 </tr>";
    	}
	}
	
	   $tot_doc += $r['RFTOTD'];
	   $tot_imp += $r['RFTIMP'];
	   $n_r ++;
    } 
   ?>
	
	<tr class='liv0' >
	    <td colspan = 4>&nbsp;</td>
		<td> Totale generale [#<?php echo $n_r; ?>]</td>
		<td colspan = 3>&nbsp;</td>
		<td align=right><?php echo n($tot_imp, 2); ?></td>
		<td align=right><?php echo n($tot_doc, 2); ?></td>
	</tr>
	
</table>
</div>
</body>
</html>

<?php 
  exit;
}

