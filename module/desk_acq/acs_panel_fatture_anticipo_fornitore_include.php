<?php

function sum_columns_value(&$ar_r, $r){
	$ar_r['TFTOTD'] += $r['TFTOTD'];
	$ar_r['TFTIMP'] += $r['TFTIMP'];
	$ar_r['detratto'] += $r['S_DETRATTO'];

	if ($r['TFFG01']!= 'C' && $r['TFFG01'] != 'M' ){
		$ar_r['residuo'] += $r['TFTIMP'] + $r['S_DETRATTO'];
	}


}

function detratto($r, $stmt){

	$fat= $r['TFDOCU'];
	$result = db2_execute($stmt, array($fat));
	$row = db2_fetch_assoc($stmt);
	return array($row['S_RFIMPP'], $row['N_RFIMPP']);
}

function crea_ar_tree_fatture_anticipo($node, $form_values){

    global $cfg_mod_Gest, $cfg_mod_Spedizioni, $conn, $cfg_mod_DeskAcq, $id_ditta_default;

$ar = array();
$ret = array();


if ($node == '' || $node == 'root'){
	
	$livello1_expanded=false;
	$livello2_expanded=false;
	
	$sql_where.= sql_where_by_combo_value('CF.CFCD', $form_values->f_fornitore_cod);
    $sql_where.= sql_where_by_combo_value('TFTPDO', $form_values->f_tipo_documento);


	
	//controllo data
	if (strlen($form_values->f_data_da) > 0)
		$sql_where .= " AND TF.TFDTRG >= {$form_values->f_data_da}";
	if (strlen($form_values->f_data_a) > 0)
		$sql_where .= " AND TF.TFDTRG <= {$form_values->f_data_a}";

				
			//controllo fattura(aperta/chiusa)
		$filtro_res=$form_values->f_filtra_fatture;

		if($filtro_res== "C" && $form_values->f_tftido != 'VA'){
			$sql_where.=" AND TFFG01 IN ('C','M')";
				
		}else if ($filtro_res== "A" && $form_values->f_tftido != 'VA'){
				
			$sql_where.=" AND TFFG01 <>'C' AND TFFG01 <> 'M'";
		}


		if (strlen($form_values->f_fornitore_cod)>0){
			$livello1_expanded=true;
			$livello2_expanded=true;
		}
		

			$sql = "SELECT TF.*, CF.CFCD AS TFCCON, CF.CFRGS1 AS TFDCON, S_DETRATTO, N_DETRATTO
			FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_cli']} CF
			     ON CF.CFDT = TF.TFDT AND digits(CF.CFCD) = TF.TFCCON
            LEFT OUTER JOIN (
	                SELECT SUM(RFTIMP) AS S_DETRATTO, COUNT(*) AS N_DETRATTO, RFDOCU
	                FROM {$cfg_mod_DeskAcq['file_righe_fatture']} RF0
	           		GROUP BY RFDOCU
	            ) RF
	           ON TFDOCU = RF.RFDOCU
			WHERE TFDT = '{$id_ditta_default}' AND TFFU02='A'  {$sql_where} ORDER BY TFDNAZ, CF.CFRGS1";
			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
		
			while ($row = db2_fetch_assoc($stmt)) {

				$tmp_ar_id = array();
				$ar_r= &$ar;
				
				

				//stacco dei livelli
				$cli_liv_tot = 'TOTALE';	//totale
				$cod_liv1 = trim($row['TFCCON']); //codice cliente
				$cod_liv2 = trim($row['TFDOCU']); //chiave documento

				/*$ar_detratto=detratto($row, $stmt_detratto);

				$row['detratto']=$ar_detratto[0];*/
                
				$ar_detratto = array($row['S_DETRATTO'], $row['N_DETRATTO']);

				//LIVELLO TOTALE
				$liv = $cod_liv_tot;
				if (!isset($ar_r["{$liv}"])){
					$ar_new = array();
					$ar_new['id'] = 'liv_totale';
					$ar_new['liv_cod'] = 'TOTALE';
					$ar_new['task'] = 'Totale';
					$ar_new['liv'] = 'liv_totale';
					$ar_new['expanded'] = true;
					$ar_r["{$liv}"] = $ar_new;
				}
				$ar_r = &$ar_r["{$liv}"];
				sum_columns_value($ar_r, $row);

				
				
				//creo cliente
				$liv=$cod_liv1;
				$ar_r=&$ar_r['children'];
				$tmp_ar_id[] = $liv;
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['codice']=$liv;
					$ar_new['task']= $row['TFDCON'];
					$ar_new['liv'] = 'liv_2';
					$ar_new['expanded'] =$livello2_expanded;
					$ar_r[$liv]=$ar_new;
				}

				$ar_r=&$ar_r[$liv];
				sum_columns_value($ar_r, $row);


				//chiave documento
				$liv=$cod_liv2;
				$ar_r=&$ar_r['children'];
				$tmp_ar_id[] = $liv;
				$task=array();
				$task[0]=$row['TFAADO'];
			    $task[1]=$row['TFNRDO'];
			    $task[2]=$row['TFDT'];
				if(!isset($ar_r[$liv])){
					$ar_new= array();
					$ar_new['id'] = implode("|", $tmp_ar_id);
					$ar_new['codice']=$row['TFTPDO'];
					$ar_new['task']= "[{$row['TFINUM']}] " .implode("_", $task);
					$ar_new['data'] = $row['TFDTRG'];
					$ar_new['k_ordine_fatt']= implode("_", array($row['TFDT'], $row['TFTIDO'], $row['TFINUM'], $row['TFAADO'], $row['TFNRDO']));
					$ar_new['tfdocu'] = $row['TFDOCU'];
					$ar_new['rif'] = trim($row['TFRGSV']);
					$ar_new['liv'] = 'liv_3';
					$ar_new['liv_cod_qtip'] = acs_u8e($row['TFDTPD']);
					$ar_new['f_cont'] = $row['TFFG04'];
					$ar_new['stato'] = $row['TFFG01'];
					if($ar_detratto[1]==0){
						$ar_new['leaf']=true;
					}
						
					$ar_r[$liv]=$ar_new;
				}
				
				$ar_r=&$ar_r[$liv];
				sum_columns_value($ar_r, $row);
			}

		} 
		
		
		else{

			$value = explode("|", $node);
			$value[0]; //id_cliente
			$value[1]; //id_fattura
		
			
		
			$sql= "SELECT * FROM {$cfg_mod_DeskAcq['file_righe_fatture']} RF WHERE RFDOCU= ".sql_t($value[1]). "ORDER BY RFDTRG";
           
			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
		
			while ($r = db2_fetch_assoc($stmt)){
				
			$task=array();
			$task[0]=$r['RFAADO'];
			$task[1]=$r['RFNRDO'];
			$task[2]=$r['RFDT'];
			
			$ar_new = array();
			$ar_new['id'] = implode("|", array($node, $r['RFINUE'], $r['RFAADE'], $r['RFNRDE']));
			$ar_new['codice']=$r['RFTPDE'];
			$ar_new['task']= "[{$r['RFINUE']}] ".$r['RFAADE']."_".$r['RFNRDE']."_".$r['RFDT'];
			$ar_new['detratto']=$r['RFTIMP'];
			$ar_new['data']=$r['RFDTRG'];
			$ar_new['liv'] = 'liv_4';
			$ar_new['leaf']=true;
			$ar[] = $ar_new;

				}
 			 }
  
 		 return $ar;
}