<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();

$initial_data_txt = $main_module->get_initial_calendar_date(); //recupero data iniziale calendario
$m_week = strftime("%V", strtotime($initial_data_txt)); 
$m_year = strftime("%G", strtotime($initial_data_txt));


//Elenco opzioni "Area spedizione"
$ar_area_spedizione = $main_module->get_options_area_spedizione();
$ar_area_spedizione_txt = '';

$ar_ar2 = array();
foreach ($ar_area_spedizione as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
$ar_area_spedizione_txt = implode(',', $ar_ar2);

//Elenco opzioni "Tipologia ordini"
$ar_tip_ordini = $main_module->get_options_tipologia_ordini();
$ar_tip_ordini_txt = '';

$ar_ar2 = array();
foreach ($ar_tip_ordini as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
$ar_tip_ordini_txt = implode(',', $ar_ar2);

?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            url: 'acs_print_wrapper.php',
			buttonAlign:'center',            
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "PRINT_FORM");  ?>{            
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',	            	            
	            handler: function() {
	                this.up('form').submit({
                        url: 'acs_print_wrapper.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',
                        params : {'tipologia_ordini' : Ext.encode(this.up('form').getForm().findField('sel_tipologia_ordini').getValue())},
                  });
	                
	            }
	        }],             
            
            items: [
                
                {
					xtype: 'fieldset',
	                title: 'Scelta tipo report',
	                defaultType: 'textfield',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	             items: [
					 {
	                    xtype: 'radiogroup',
	                    anchor: '100%',
	                    fieldLabel: '',
	                    allowBlank: false,
	                    items: [
	                        {
	                            xtype: 'radio'
	                          , name: 'tipo'
	                          , inputValue: 'elenco' 
	                          , boxLabel: 'Elenco'
	                        },
	                        {
	                            xtype: 'radio'
	                          , name: 'tipo'                            
	                          , inputValue: 'agenda'                            
	                          , boxLabel: 'Agenda'
	                        }
	                    ]
	                }	             
	             ]
	            }
	            
	            
	            
	            
               , {
					xtype: 'fieldset',
	                //title: 'Scelta periodo',
	                defaultType: 'textfield',
	                layout: 'hbox',
		            items: [
						{
							xtype: 'container',
							flex: 2,
							items: [
							        {
										xtype: 'container',
					            		anchor: '100%',
					            		layout: 'hbox',					            		
					            		items: [
							                {
							                    xtype: 'textfield',
							                    name: 'settimana',
							                    value: '<?php echo $m_week ?>',    
							                    fieldLabel: 'Settimana/Anno',
							                    allowBlank: false,			                    
							                    width: 150, labelWidth: 120
							                }, {xtype: 'label', text: ' / ', anchor:'93%', padding: '2 5 0 5'},                                
							                {
							                    xtype: 'textfield',
							                    name: 'anno',    
							                    value: '<?php echo $m_year ?>',
							                    allowBlank: false,
							                    width: 50
							                }                
					            		
					            		]                	
					                },
				                {
				                    xtype: 'checkboxgroup',
				                    fieldLabel: 'Giorni',
				                    labelWidth: 115,
				                    anchor: '100%',
					                defaults: {
					                    anchor: '100%',
					                    width: 30,
					                    padding: '0 3 0 0'
					                },				                    
				                    items: [
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'L'
				                          , inputValue: '1'
				                          , flex: 1                          
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'M'
				                          , inputValue: 2  
				                          , flex: 1				                                                  
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'M'
				                          , inputValue: 3
				                          , flex: 1				                                                    
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'G'
				                          , inputValue: 4
				                          , flex: 1				                                                    
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'V'
				                          , inputValue: 5
				                          , flex: 1				                                                    
				                        },                
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'S'
				                          , inputValue: 6
				                          , flex: 1				                                                    
				                        },                
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'D'
				                          , inputValue: 7
				                          , flex: 1				                                                    
				                        }
				                    ]
				                }								
		            
							]
						}, {
		                    xtype: 'radiogroup',
		                    layout: 'vbox',
		                    fieldLabel: '',
		                    flex: 1,
		                    items: [
		                        {
		                            xtype: 'radio'
		                          , name: 'sceltadata[]' 
		                          , boxLabel: 'Data Programmata'
		                          , inputValue: 'TDDTEP'
		                          , checked: true
		                        }
		                   ]
		                }
						
						
						
		             ]
	             }	            
	            
	                   
	            
	            
                , {
					xtype: 'fieldset',
	                title: '',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
	             
					{
					    xtype: 'combo',
					    fieldLabel: 'Area di ricezione',
					    name: 'area_spedizione',
					    autoSelect: false,
					    allowBlank: true,
					    editable: false,
					    triggerAction: 'all',
					    typeAhead: true,
					    width:120,
					    listWidth: 120,
					    enableKeyEvents: true,
					    mode: 'local',
					    store: [
					        <?php echo $ar_area_spedizione_txt ?>
					    ]
					}, {
					    xtype: 'combo',
					    fieldLabel: 'Tipologia ordini',
					    name: 'sel_tipologia_ordini',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,
					   	multiSelect: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $main_module), ''); ?>
							    ] 
						}	
					}, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							name: 'num_carico',
							xtype: 'textfield',
							fieldLabel: 'Nr. Carico',
							value: '',
							flex: 1,
							labelAlign: 'right'
						 }, {
							name: 'num_lotto',
							xtype: 'textfield',
							fieldLabel: 'Nr. Lotto',
							value: '',
							flex: 1,
							labelAlign: 'right'														
						 } 
						]
					}
	             
	             ]
	             },	             
	             
	              	            
	            
                
                
                
                {
                    xtype: 'tabpanel',
					anchor: '100%',                    
                    activeTab: 0,
                    layout: 'fit',
			        defaults :{
			            bodyPadding: 10
			        },
                    items: [
						{
				            xtype: 'panel',
				            title: 'Elenco',
							items: [

								{
			                    xtype: 'checkboxgroup',
			                    anchor: '100%',
			                    fieldLabel: 'Dettaglia',
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'dettaglio_per_ordine' 
			                          , boxLabel: 'Ordine'
			                          , inputValue: 'Y'
			                        }
									]
								}]
				        },{
				            xtype: 'panel',
				            title: 'Agenda',
							items: [

							 {
			                    xtype: 'radiogroup',
			                    anchor: '100%',
			                    fieldLabel: 'Totalizza',
			                    allowBlank: false,
			                    defaults: {width: 100, labelWidth: 90},
			                    items: [
			                        {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'
			                          , inputValue: 'volume' 
			                          , boxLabel: 'Volume'
			                          , checked: true
			                          , flex: 1
			                        },
			                        {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'                            
			                          , inputValue: 'ref'                            
			                          , boxLabel: 'Pezzi'
			                          , flex: 1			                          
			                        }
			                        
			                        
			                        , {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'                            
			                          , inputValue: 'pezzi_ref'                            
			                          , boxLabel: 'Pezzi/Referen.'
			                          , flex: 1			                          
			                        }
			                        
			                        
			                        , {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'                            
			                          , inputValue: 'n_ordini'
			                          , boxLabel: 'Nr Ordini'
			                          , flex: 1, fieldWidth: 150			                          
			                        }
			                    ]
			               },{
			                    xtype: 'checkboxgroup',
			                    anchor: '100%',
			                    fieldLabel: 'Dettaglia',
			                    defaults: {width: 100, labelWidth: 90},		
			                    items: [
									{
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_porta' 
			                          , boxLabel: 'Porta'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_itinerario' 
			                          , boxLabel: 'Gruppo'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_cliente' 
			                          , boxLabel: 'Fornitore'
			                          , inputValue: 'Y'
			                        }, {
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_orario_spedizione' 
			                          , boxLabel: 'Orario'
			                          , inputValue: 'Y'
			                          , flex: 1
			                        }
									]
							}]				            
				        },{
				            xtype: 'panel',
				            title: 'Descrizione report',
				            items: [
											{
							                    xtype: 'textfield',
							                    name: 'add_descr_report',    
							                    fieldLabel: '',
							                    allowBlank: true,			                    
							                    width: '100%', anchor: '-10'
							                }				            
				            ]
				        }                                       
                    ]
                 }
                 

                
            ]
        }

	
]}