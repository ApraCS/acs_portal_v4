<?php

require_once("../../config.inc.php");
require_once("acs_panel_background_mrp.inc.php");

$main_module = new DeskAcq();

set_time_limit(120);

// ******************************************************************************************
// EXE - AGGIORNA SEGNALAZIONI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_agg_revoca_mto'){
    
    $sh = new SpedHistory($main_module);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_REVOCHE_MTO',
         )
        );

    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
// EXE - Spegni revoca
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_spegni_revoca'){
	$m_params = acs_m_params_json_decode();
	
	$where_sql = "	MTAVAN = 'P' AND MTFLM14 <> 'E' AND MTSTAR = '' AND
				  (   MTFLE1 = 'M' OR MTFLE2 = 'M' OR MTFLE3 = 'M' OR MTFLE4 = 'M'
				   OR MTFLE5 = 'M' OR MTFLE6 = 'M' OR MTFLE7 = 'M' OR MTFLE8 = 'M'
				   OR MTFLE9 = 'M' OR MTFLE10 = 'M' OR MTFLE11 = 'M' OR MTFLE12 = 'M'
				   OR MTFLE13 = 'M' OR MTFLE14 = 'M' OR MTFLE15 = 'M')				  
				  AND MTDT=? AND MTTPSV=? AND MTTPCO=? AND MTAACO=? AND MTNRCO=? AND MTRISV=? 		
				  ";
	
	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MTO WHERE {$where_sql}";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();	
	
	//per ogni riga selezionata
	foreach($m_params->list_selected as $rs){
		//print_r($rs);		
		$ar_k = explode("|", $rs->liv_cod);
		$result = db2_execute($stmt, $ar_k);
		echo db2_stmt_errormsg($stmt);
		
		while ($r = db2_fetch_assoc($stmt)) {
			$sql_u_ar = array();			
			//dove trovo 'M' imposto 'I' di ignora
			for ($i = 1; $i <= 15; $i++){
				if ($r["MTFLE{$i}"] == 'M')
					$sql_u_ar[] = "MTFLE{$i} = 'I' ";
			}
			
			$sql_u = implode(' , ', $sql_u_ar);
			
			$sql_UPD = "UPDATE {$cfg_mod_DeskAcq['file_proposte_MTO']} SET {$sql_u} WHERE {$where_sql}";
			$stmt_UPD = db2_prepare($conn, $sql_UPD);
			$result_UPD = db2_execute($stmt_UPD, $ar_k);						
			
		} //per ogni riga trovata (ma dovrebbe essere una sola)
		
	}
	


	$ret['success'] = true;
	echo acs_je($ret);		
	exit;
}	



// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_form_parametri_stampa'){
	$m_params = acs_m_params_json_decode();	
	?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
			            
            
            items: [
            	{xtype: 'hidden', name: 'cod_gruppo', value: <?php echo j($m_params->cod_gruppo); ?>}
            	, {xtype: 'hidden', name: 'cod_fornitore', value: <?php echo j($m_params->cod_fornitore); ?>}
				, {xtype: 'hidden', name: 'open_pars', value: <?php echo j(acs_je($m_params->open_pars)); ?>}
				, {
					xtype: 'fieldset',
	                defaultType: 'textfield',
	                frame: false,
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                }, 	
					items: [{
			                    xtype: 'radiogroup',
			                    anchor: '100%',
			                    fieldLabel: '',
			                    allowBlank: false,
			                    frame: true,
			                    items: [{
				                    	xtype: 'label',
				                    	text: 'Solo manutenuti',
				                    	width: 70
				                    }, {
			                            xtype: 'radio'
			                          , name: 'solo_manutenuti'
			                          , inputValue: 'Y' 
			                          , boxLabel: 'Si'
			                        }, {
			                            xtype: 'radio'
			                          , name: 'solo_manutenuti'
			                          , inputValue: 'N' 
			                          , boxLabel: 'No'
			                        }
			                    ]
			                }            	            	
			         ]
			     }           
            ]
            
			, buttons: [{
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',	            
	            handler: function() {
	                this.up('form').submit({
                        url: 'acs_report_b_mrp_art_scaduti.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',
                        params : {},
                  });
	                
	            }
	        }]            
            
        }
      ]
}
<?php exit;} ?>        



<?php

// ******************************************************************************************
// FORZA CHIUSURA ORDINE (ordini a fornitore scaduti)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_forza_chiusura'){

	global $cfg_mod_DeskAcq, $id_ditta_default;
	$m_params = acs_m_params_json_decode();
	
	$oe = explode("_", $m_params->k_ordine);
	///["k_ordine"]  = implode("_", array($r['M3DT'], $r['M3TIDO'], $r['M3INUM'], $r['M3AADO'], sprintf("%06s", $r['M3NRDO'])));	
	$data_progr_attuale = $oe[5];
	$k_articolo 		= $oe[6];
	
	//HISTORY
	$sh = new SpedHistory($main_module);
	$sh->crea(
			'forza_chiusura_ordine',
			array(
					"k_ordine" 		=> $m_params->k_ordine,
					"da_data"		=> $data_progr_attuale,
					"k_articolo"	=> trim($m_params->cod_art)
			)
	);

	//aggiorno il flag sul file
	$sql = "UPDATE {$cfg_mod_DeskAcq['file_WMAF30']}
			SET M3FU01 = 'X'
			WHERE M3DT = ? AND M3TIDO = ? AND M3INUM = ? AND M3AADO = ? AND M3NRDO = ?
			      AND M3DTEP = ? AND M3ART = ? 
			";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $oe);
	
	

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

exit;
}






// ******************************************************************************************
// DATI PER SELECT TIPO APPROVIGGIONAMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tipo_approv'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT M2SWTR as id, M2SWTR as text FROM {$cfg_mod_DeskAcq['file_WMAF20']}";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}




// ******************************************************************************************
// DATI PER SELECT GRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_gruppo_fornitore'){

	//costruzione sql
	global $main_module, $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT M2SELE as id, M2SELE AS text FROM {$cfg_mod_DeskAcq['file_WMAF20']}";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['TEXT'] = $main_module->decod_std('ITIN', $r['ID']);
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


// ******************************************************************************************
// DATI PER GRID FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT M2FOR1 AS COD_FOR, M2RGS1 AS DES_FOR FROM {$cfg_mod_DeskAcq['file_WMAF20']}";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}





/*************************************************************************
 * GET JSON DATA
 *************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data'){
    if ($_REQUEST['node'] == 'root'){
	    $ar_ret = array();
	    $ar_ret['children'] = crea_livello_0();
    echo acs_je($ar_ret);
		}else{
				if($_REQUEST['node'] == 'LIV0;ARTMAN'){														 
						$array_ret = array();					 
						$array_ret = crea_array_ARTMAN($_REQUEST['node']);
						echo acs_je($array_ret);
				}
				if ($_REQUEST['node'] == 'LIV0;NOFABC'){
						$array_ret = array();					 
						$array_ret = crea_array_NOFABC($_REQUEST['node']);
						echo acs_je($array_ret);
				}
				if ($_REQUEST['node'] == 'LIV0;NFANCP'){
						$array_ret = array();					 
						$array_ret = crea_array_NFANCP($_REQUEST['node']);
						echo acs_je($array_ret);
				}
				if ($_REQUEST['node'] == 'LIV0;ORFOSC'){
						$array_ret = array();					 
						$array_ret = crea_array_ORFOSC($_REQUEST['node']);
						echo acs_je($array_ret);
				}
				if ($_REQUEST['node'] == 'LIV0;NOLIST'){
						$array_ret = array();					 
						$array_ret = crea_array_NOLIST($_REQUEST['node']);
						echo acs_je($array_ret);
				}
				if ($_REQUEST['node'] == 'LIV0;REV_MTO'){
					$array_ret = array();
					$array_ret = crea_array_REV_MTO($_REQUEST['node']);
					echo acs_je($array_ret);
				}				
				if ($_REQUEST['node'] == 'LIV0;PROP_ORD_DIS'){
					$array_ret = array();
					$array_ret = crea_array_PROP_ORD_DIS($_REQUEST['node']);
					echo acs_je($array_ret);
				}				
				if ($_REQUEST['node'] == 'LIV0;PROP_ORD_MTS'){
					$array_ret = array();
					$array_ret = crea_array_PROP_ORD_MTS($_REQUEST['node']);
					echo acs_je($array_ret);
				}				
		}
    exit();
}


function crea_livello_0(){
	$ar = array();

	$ar0['id'] = 'LIV0;ARTMAN';
    $ar0['task'] = 'Articoli mancanti';
    array_push($ar, $ar0);
    
    $ar0['id'] = 'LIV0;NOFABC';
    $ar0['task'] = 'Ordini a fornitore senza fabbisogni confermati';
    array_push($ar,$ar0);
    
    $ar0['id'] = 'LIV0;NFANCP';
    $ar0['task'] = 'Ordini a fornitore senza fabbisogni ne confermati ne provvisori';
    array_push($ar,$ar0);
     
    $ar0['id'] = 'LIV0;ORFOSC';
    $ar0['task'] = 'Ordini a fornitore inevasi/scaduti';
    array_push($ar,$ar0);
    
    $ar0['id'] = 'LIV0;NOLIST';
    $ar0['task'] = 'Articoli senza listino';
    array_push($ar,$ar0);
    
    $ar0['id'] = 'LIV0;REV_MTO';
    $ar0['task'] = 'Revoche ordini MTO attese';
    array_push($ar,$ar0);
    
    $ar0['id'] = 'LIV0;PROP_ORD_DIS';
    $ar0['task'] = 'Proposte riordino discreto';
    array_push($ar,$ar0);    
    
    $ar0['id'] = 'LIV0;PROP_ORD_MTS';
    $ar0['task'] = 'Proposte riordino MTS';
    array_push($ar,$ar0);    
        
  return ($ar);
}





// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_form'){
?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
//            title: 'Selezione dati',
            
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
			            
            
            items: [
            
            
            					 	
					, {					
						xtype: 'grid',
						flex: 1,
						id: 'tab-tipo-stato',
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},
     					
						features: [
						{
							ftype: 'filters',
							// encode and local configuration options defined previously for easier reuse
							encode: false, // json encode the filter query
							local: true,   // defaults to false (remote filtering)
					
							// Filters are most naturally placed in the column definition, but can also be added here.
						filters: [
						{
							type: 'boolean',
							dataIndex: 'visible'
						}
						]
						}],     					
     					
									
						selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},				
						//columnLines: true,			
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fornitore',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['COD_FOR', 'DES_FOR']
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [{header: 'Fornitore', dataIndex: 'COD_FOR', flex: 2, filter: {type: 'string'}, filterable: true},
								  {header: 'Denominazione', dataIndex: 'DES_FOR', flex: 8, filter: {type: 'string'}, filterable: true}]
						 
					}            
            
            
            		, {            		
						margin: "0 0 0 2",            		
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			height: 50,
            			
			            layout: {
			                type: 'hbox',
			                align: 'stretch',
			                pack  : 'start'
			     		},            			
            			
     					items: [
     					
					//SELECT PER GRUPPO E SOTTOGRUPPO
						{
								margin: "0 10 0 10",
								flex: 1,					
								name: 'f_gruppo',
								xtype: 'combo',
								fieldLabel: 'Gruppo fornitura',
								labelAlign: 'top',
								width: 290,
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  }	 , {
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_tipo_approv',
								xtype: 'combo',
								fieldLabel: 'Tipo riordino',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,
							   	multiSelect: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tipo_approv',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            }
							    						 
							  }   			
     					
     					]
     				}            
            
     				
	
						 
				],
			buttons: [
			
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [			
			
			
					
						]	  
				} 
				
				
				
				
				
				
			, {
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [				
				
			
			
			 {
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();
  				list_selected_id = [];
  				for (var i=0; i<id_selected.length; i++) 
	   				list_selected_id.push(id_selected[i].data.COD_FOR);
				
				
				m_values = form_p.getValues();
				m_values['selected_id'] = list_selected_id;
	            
				//carico la form dal json ricevuto da php e la inserisco nel m-panel
				Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_grid',
				        method     : 'POST',
				        jsonData: m_values,
//				        jsonData: {
//				        	selected_id: list_selected_id,
//				        	f_gruppo: form_p.getForm().findField("f_gruppo").getValue(),
//				        	f_sottogruppo: form_p.getForm().findField("f_sottogruppo").getValue()
//				        },
				        waitMsg    : 'Data loading',
				        success : function(result, request){			        	
				        	
							mp = Ext.getCmp('m-panel');					        					        
							mp.remove('background_mrp');					        
				            var jsonData = Ext.decode(result.responseText);
				            
				            mp.add(jsonData.items);
				            mp.doLayout();
				            mp.items.last().show();
 							this.print_w.close();
						             
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				        				        
				    });
				    
	            
            	                	                
	            }
	        }

	        ]
	       } 
	        
	        
	       ],             
				
        }
]}			            

<?php
exit;
}
 







/***********************************************************************************
 * TREE json
 ***********************************************************************************/
 $m_params = acs_m_params_json_decode();
 $list_selected_stato = $m_params->selected_id;
 $list_selected_stato_js = acs_je(implode("|", $list_selected_stato));

?>

{"success":true, "items":
 {
		xtype: 'treepanel',
		id: 'background_mrp',
		//selType: 'cellmodel',
		cls: 'tree-calendario',		
        title: 'Background MRP',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        da_data: 0,
        
        
        
        dockedItems2222: [{
            xtype: 'toolbar'
            , dock: 'top'
            , items: [{
                xtype: 'trigger'
                , triggerCls: 'x-form-clear-trigger'
                , onTriggerClick: function () {
                    this.reset();
                    this.focus();
                }
                , listeners: {
                    change: function (field, newVal) {
                        var tree = field.up('treepanel');
                        tree.filter(newVal);
                    }
                    , buffer: 250
                }
            }]
        }],        
        
        
		store: Ext.create('Ext.data.TreeStore', {
	        fields: ['task', 'liv', 'liv_cod', 'k_ordine', 'itinerario', 'dt', 'cod_art', 'um', 'qta', 'in_esaurimento', 'tipo_approvvigionamento', 'dim1', 'dim2', 'dim3', 'var1', 'var2', 'var3', 'fl1', 'fl2', 'fl3', 'fl4', 'fl5', 'fl6', 'fl7', 'fl8', 'gr_pianificazione', 'gr_pianificazione_desc', 'art_sosp', 'art_esau', 'M3NRDO', 'M3NRDO_ICO', 'M3AADO', 'M3TPDO', 'M3DT', 'M3TIDO', 'M3INUM', 'forn_TAFG01', 'forn_TAFG02', 'M3FU01', 'M3FU02', 'var1', 'var2', 'var3', 'van1', 'van2', 'van3'],
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
	            timeout: 120000,
	            
/*	            
				extraParams: {
					list_selected_stato: <?php echo $list_selected_stato_js; ?>,
					f_gruppo: <?php echo j($m_params->f_gruppo); ?>,
					f_gruppo_pianificazione:  <?php echo j($m_params->f_gruppo_pianificazione); ?>,
					f_tipo_approv:  <?php echo j($m_params->f_tipo_approv); ?>											
				}
*/					            
				
				extraParams: <?php echo acs_raw_post_data() ?>,
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				doRequest: personalizza_extraParams_to_jsonData,
					        
				actionMethods: {
					read: 'POST'
				},

				reader: {
					type: 'json',
				},				
				
				
				
	        },
	        
	        reader: new Ext.data.JsonReader(),
	        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        closable: true,
		
		listeners: {
              		load: function(sender, node, records) {              		
                      	Ext.getBody().unmask();												
              		}
              		
					, itemcontextmenu : function(grid, rec, node, index, event) {
						  				  event.stopEvent();
									      var record = grid.getStore().getAt(index);		  
									      var voci_menu = [];

									      liv0 = rec.get('id').split('|')[0];
									      k_liv0 = liv0.split(';')[1];

									      
									   
								// ORDINI A FORNITORE SCADUTI -------------------------------------------	   
								if (k_liv0 == 'ORFOSC'){
									   
									   if (rec.get('liv') == 'liv_2'){
										  rec_fornitore = rec;
										  rec_gruppo = rec.parentNode;

										      voci_menu.push({
									      		text: 'Report residuo ordini',
									    		iconCls : 'icon-print-16',      		
									    		handler: function() {
									    		
								    			  	acs_show_win_std('Parametri report residuo ordini', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_form_parametri_stampa', 
								    			  		{	
								    			  		 cod_gruppo: rec_gruppo.get('liv_cod'),	
								    			  		 cod_fornitore: rec_fornitore.get('liv_cod'),
								    			  		 open_pars: grid.store.treeStore.proxy.extraParams
								    			  		}, 550, 250, null, 'icon-print-16');									    		
									    		
									    		}
											  });										  
										  
										  									   
									   } //liv2
									      
									      
									   if (rec.get('liv') == 'liv_4'){ 
										  fornitore = rec.parentNode.parentNode;

										  //solo se per il fornitore non e' inibito
										  if (fornitore.get('forn_TAFG01') != 'Y'){									      
										      voci_menu.push({
									      		text: 'Forza chiusura',
									    		iconCls : 'icon-sub_red_delete-16',
									    		disabled: (rec.get('M3FU01') == 'X' || rec.get('M3FU02') == 'X'),      		
									    		handler: function() {
									    			
														Ext.MessageBox.confirm('Richiesta conferma', 'Confermi la chiusura a saldo della quantit&agrave; in ordine?', function(btn){
														   if(btn === 'yes'){														   
														        Ext.Ajax.request({
														            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_forza_chiusura',
														            method: 'POST',
												        			jsonData: rec.data,						            
														            success: function ( result, request) {
														                var jsonData = Ext.decode(result.responseText);
									    								rec.set('iconCls', 'icon-sub_red_delete-16');
									    								rec.set('M3FU01', 'X');
														            },
														            failure: function ( result, request) {
														            }
														        });
														   }
														   else{
														      //some code
														   }
														 });									    			

									    		}
											  });
											}	  
											  
											  
											  
											  
											  
											  	id_selected = grid.getSelectionModel().getSelection();											  	
											
												cod_fornitore = '';
											  	list_selected_id = [];
											  	for (var i=0; i<id_selected.length; i++){
													  cod_fornitore = id_selected[i].parentNode.parentNode.get('liv_cod');
													  data_consegna = id_selected[i].parentNode.get('liv_cod');
													  //devo prendere solo livelli ordine o cliente
													  if (id_selected[i].data.liv != 'liv_4') {
														  acs_show_msg_error('Selezionare solo righe a livello ordine');
														  return false;
													  } 
											  	  	 
												 	list_selected_id.push(id_selected[i].data);
											  	}
										      
											  
											  //recupero fornitore attuale											  
											  
										  //solo se per il fornitore non e' inibito
										  if (fornitore.get('forn_TAFG02') != 'Y'){											  
											  voci_menu.push({
									      		text: 'Assegna consegna',
									    		iconCls: 'iconSpedizione',
									    		disabled: (rec.get('M3FU01') == 'X' || rec.get('M3FU02') == 'X'),									    		
									    		handler: function() {
									    		
							                   	my_listeners = {
						        					afterExecute: function(){
						        						console.log('--- afterExecute ---');	
						        						rec.set('iconCls', 'iconSpedizione');
						        						rec.set('M3FU02', 'X');						        									            
										        		}
								    				};									    		
									    		
								    			  	acs_show_win_std('Assegna consegna', 'acs_form_json_assegna_consegna_b_mrp.php', 
								    			  		{		
								    			  		 from: 'BACKGROUND_MRP',						    			  		 
								    			  		 list_selected_id: list_selected_id, 
								    			  		 cod_fornitore: cod_fornitore,
								    			  		 data_consegna: data_consegna, 
								    			  		 grid_id: grid.id
								    			  		}, 1050, 450, my_listeners, 'iconSpedizione');
									    			}
											  });
										   }	  
											  
										} //liv4	
										
									} // ORDINI A FORNITORE SCADUTI ------------------------------
									
									

								// GESTIONE REVOCHE MTO -------------------------------------------	   
								if (k_liv0 == 'REV_MTO'){
								     
							     if(rec.get('liv') == ''){
						     		 voci_menu.push({
							      		text: 'Aggiorna segnalazioni',
							    		iconCls : 'icon-button_black_repeat_dx-16',     		
							    		handler: function() {
								    		  Ext.Ajax.request({
									            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_agg_revoca_mto',
									            method: 'POST',
							        			jsonData: {},						            
									            success: function ( result, request) {
									                var jsonData = Ext.decode(result.responseText);
									            },
									            failure: function ( result, request) {
									            }
									        });
											   						    			

								    		}
										  });
							     }
								
								   if (rec.get('liv') == 'liv_4'){
								   
									  	id_selected = grid.getSelectionModel().getSelection();											  	
									
										cod_fornitore = '';
									  	list_selected_id = [];
									  	for (var i=0; i<id_selected.length; i++){
											  cod_fornitore = id_selected[i].parentNode.parentNode.get('liv_cod');
											  data_consegna = id_selected[i].parentNode.get('liv_cod');
											  //devo prendere solo livelli ordine
											  if (id_selected[i].data.liv != 'liv_4') {
												  acs_show_msg_error('Selezionare solo righe a livello ordine');
												  return false;
											  } 
									  	  	 
										 	list_selected_id.push(id_selected[i].data);
									  	}
								   
								    
									  	fornitore = rec.parentNode.parentNode;
								
									      voci_menu.push({
									      		text: 'Spegni',
									    		iconCls : 'icon-sub_red_delete-16',
									    		disabled: (rec.get('M3FU01') == 'X' || rec.get('M3FU02') == 'X'),      		
									    		handler: function() {
									    			
														Ext.MessageBox.confirm('Richiesta conferma', 'Confermi?', function(btn){
														   if(btn === 'yes'){														   
														        Ext.Ajax.request({
														            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_spegni_revoca',
														            method: 'POST',
												        			jsonData: {list_selected: list_selected_id},						            
														            success: function ( result, request) {
														                var jsonData = Ext.decode(result.responseText);
														                
														                ids = id_selected; 														                
														                for (var i=0; i<id_selected.length; i++){
									    									if (ids[i].get('fl1') == 'M') ids[i].set('fl1', 'I');
									    									if (ids[i].get('fl2') == 'M') ids[i].set('fl2', 'I');
									    									if (ids[i].get('fl3') == 'M') ids[i].set('fl3', 'I');
									    									if (ids[i].get('fl4') == 'M') ids[i].set('fl4', 'I');									    																	    																	    								
									    									if (ids[i].get('fl5') == 'M') ids[i].set('fl5', 'I');
									    									if (ids[i].get('fl6') == 'M') ids[i].set('fl6', 'I');
									    									if (ids[i].get('fl7') == 'M') ids[i].set('fl7', 'I');
									    									if (ids[i].get('fl8') == 'M') ids[i].set('fl8', 'I');
														                }
									    								
														            },
														            failure: function ( result, request) {
														            }
														        });
														   }
														   else{
														      //some code
														   }
														 });									    			

									    		}
											  });
									}		  									
								}	// GESTIONE REVOCHE MTO ----------------------								
									
									
									
																  
				
									      var menu = new Ext.menu.Menu({
									            items: voci_menu
										}).showAt(event.xy);
								   }	      		
              		
			      , celldblclick: {										      		
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
						  	rec = iView.getRecord(iRowEl);
						  	
							if (rec.get('liv')=='liv_4'){
								iEvent.preventDefault();						
								
							  	acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('dt'), rdart: rec.get('cod_art')}, 1200, 600, null, 'icon-shopping_cart_gray-16');								
								
								return false;
							}			  	
						}
			    	}              		
              		
              		
    },    		
		viewConfig: {
		        //row class CALENDARIO
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
		            return v;					            
		         	}   
		    },			


		<?php
				$ss = "Tipo segnalazione / Gruppo / Fornitore";
				// $ss .= "<img src=" . img_path("icone/48x48/button_grey_repeat.png") . " height=30>";		
				//$ss .= "<img id=\"cal_prev_1w\"src=" . img_path("icone/48x48/button_grey_rew.png") . " height=30>";

				//$ss .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				
				
				//img per intestazioni colonne flag
				//$cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>";
;								
		?>		


        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 10,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>',
   			    renderer: function(value, metaData, record){
					liv0 = record.get('id').split('|')[0];
					k_liv0 = liv0.split(';')[1];
										
					//solo per le righe revoche MTO
					if (k_liv0 != 'REV_MTO' || record.get('liv')!= 'liv_4') return value;
		    		
		    		if (record.get('fl2') == 'M') metaData.tdCls += ' sfondo_rosso';
		    		if (record.get('fl2') == 'I') metaData.tdCls += ' sfondo_giallo';		    		
		    		
		    		if (record.get('fl1') == 'M')
		    			value = '(Forn. moidf.) ' + value;		    		
		    		
		    		return value				    	
   			    }           
        },{
		   text: 'Gr. Pian.',
		   width: 110,
		   dataIndex: 'gr_pianificazione_desc',
		   menuDisabled: true, sortable: false
		},{
		   text: 'Articolo',
		   width: 80,
		   dataIndex: 'cod_art',
		   menuDisabled: true, sortable: false,	    
   			    renderer: function(value, p, record){
		    		if (record.get('liv') == 'liv_1' || record.get('liv') == 'liv_2' || record.get('liv') == 'liv_3')
		    			return '[ ' + value + ' ]';
		    		else return value;				    	
   			    }
		},{text: 'Var1', width: 60, dataIndex: 'var1',
   			    renderer: function(value, metaData, record){
		    		if (record.get('liv') != 'liv_4') return value;
					liv0 = record.get('id').split('|')[0];
					k_liv0 = liv0.split(';')[1];										
					//solo per le righe revoche MTO
					if (k_liv0 != 'REV_MTO') return value;
		    		if (record.get('fl3') == 'M') metaData.tdCls += ' sfondo_rosso';
		    		if (record.get('fl3') == 'I') metaData.tdCls += ' sfondo_giallo';
	    		
		    		if (record.get('var1') != '' || record.get('van1') != ''){
						metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('d_var1')) + '"';		    		
		    			return record.get('var1') + '/' + record.get('van1');
		    		}							    		
   			    }		
		},{text: 'Var2', width: 60, dataIndex: 'var2',
   			    renderer: function(value, metaData, record){
		    		if (record.get('liv') != 'liv_4') return value;
					liv0 = record.get('id').split('|')[0];
					k_liv0 = liv0.split(';')[1];										
					//solo per le righe revoche MTO
					if (k_liv0 != 'REV_MTO') return value;
		    		if (record.get('fl4') == 'M') metaData.tdCls += ' sfondo_rosso';
		    		if (record.get('fl4') == 'I') metaData.tdCls += ' sfondo_giallo';		    		
		    		return value;							    		
   			    }		
		},{text: 'Var3', width: 60, dataIndex: 'var3',
   			    renderer: function(value, metaData, record){
		    		if (record.get('liv') != 'liv_4') return value;
					liv0 = record.get('id').split('|')[0];
					k_liv0 = liv0.split(';')[1];										
					//solo per le righe revoche MTO
					if (k_liv0 != 'REV_MTO') return value;
		    		if (record.get('fl5') == 'M') metaData.tdCls += ' sfondo_rosso';
		    		if (record.get('fl5') == 'I') metaData.tdCls += ' sfondo_giallo';		    		
		    		return value;							    		
   			    }		
		}
		 ,{
		   text: 'L',
		   width: 60,
		   dataIndex: 'dim1', align: 'right',
		   menuDisabled: true, sortable: false,
   			    renderer: function(value, metaData, record){
		    		if (record.get('liv') != 'liv_4') return value;
					liv0 = record.get('id').split('|')[0];
					k_liv0 = liv0.split(';')[1];										
					//solo per le righe revoche MTO
					if (k_liv0 != 'REV_MTO') return floatRenderer0(value);
		    		if (record.get('fl6') == 'M') metaData.tdCls += ' sfondo_rosso';
		    		if (record.get('fl6') == 'I') metaData.tdCls += ' sfondo_giallo';		    		
		    		return floatRenderer0(value);							    		
   			    }		   
		},{
		   text: 'H',
		   width: 60,
		   dataIndex: 'dim2', align: 'right',
		   menuDisabled: true, sortable: false,
   			    renderer: function(value, metaData, record){
		    		if (record.get('liv') != 'liv_4') return value;
					liv0 = record.get('id').split('|')[0];
					k_liv0 = liv0.split(';')[1];										
					//solo per le righe revoche MTO
					if (k_liv0 != 'REV_MTO') return floatRenderer0(value);
		    		if (record.get('fl7') == 'M') metaData.tdCls += ' sfondo_rosso';
		    		if (record.get('fl7') == 'I') metaData.tdCls += ' sfondo_giallo';		    		
		    		return floatRenderer0(value);							    		
   			    }		   
		},{
		   text: 'P',
		   width: 60,
		   dataIndex: 'dim3', align: 'right',
		   menuDisabled: true, sortable: false,
   			    renderer: function(value, metaData, record){
		    		if (record.get('liv') != 'liv_4') return value;
					liv0 = record.get('id').split('|')[0];
					k_liv0 = liv0.split(';')[1];										
					//solo per le righe revoche MTO
					if (k_liv0 != 'REV_MTO') return floatRenderer0(value);
		    		if (record.get('fl8') == 'M') metaData.tdCls += ' sfondo_rosso';
		    		if (record.get('fl8') == 'I') metaData.tdCls += ' sfondo_giallo';		    		
		    		return floatRenderer0(value);							    		
   			    }		   
		}, {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=20>',	width: 30, tdCls: 'tdAction',		 
   	    		dataIndex: 'art_sosp',	    
   			    renderer: function(value, p, record){
		    		if (record.get('art_sosp') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';				    	
   			    }
   	    }, {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=20>',	width: 30, tdCls: 'tdAction', 
   	    		dataIndex: 'art_esau',	    
   			    renderer: function(value, p, record){
		    		if (record.get('art_esau') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';				    	
   			    }
   	    },{
		   text: 'Tr',
		   tooltip: 'riordino mtO, mtS, Discreto',
		   width: 35,
		   dataIndex: 'tipo_approvvigionamento',
		   menuDisabled: true, sortable: false
		},{
		   text: 'UM',
		   width: 30,
		   dataIndex: 'um',
		   menuDisabled: true, sortable: false
		},{
		   text: 'Q.t&agrave;',
		   width: 70,
		   dataIndex: 'qta', align: 'right', renderer: floatRenderer2,
		   menuDisabled: true, sortable: false
		},{
		   text: 'Ordine',
		   width: 100,
		   dataIndex: 'M3NRDO', 
				renderer: function(value, metaData, record, row, col, store, gridView){
								if (record.get('M3AADO') == 'COUNT')
									return '[ ' + value + ' ]';				
				
								if (parseInt(record.get('M3NRDO')) > 0)			               
			                		return "" + record.get('M3AADO') + "_" + record.get('M3NRDO') + "_" + record.get('M3TPDO');
			                	else {
									if (record.get('M3NRDO_ICO') == 'C') return '<img src=<?php echo img_path("icone/48x48/blog.png") ?> width=18>'; //ritiro contratto
									else return record.get('M3NRDO_ICO');			                	
			                	}
			                }		   
		}]   
  
 }
}
