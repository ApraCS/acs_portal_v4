<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();

set_time_limit(120);




// ******************************************************************************************
// exe modifica punto riordino su articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'grid_conferma_ordine'){
	$m_params = acs_m_params_json_decode();	
?>

{"success":true, "items":

{
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	  
	  
		dockedItems: [{
			xtype: 'toolbar'
					, dock: 'bottom'
							, items: [       
					 				{
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-shopping_cart_green-32',
					                     text: 'Genera ordini',
								            handler: function() {
			            
			            	list_selected_id = [];
							grid = this.up('grid');
							m_win = this.up('window');
														
							grid.getStore().each(function(rec) {  
							  if (rec.get('OLD_RIORD').trim() == '' && parseFloat(rec.get('QTA_RIORD')) > 0)
							  	list_selected_id.push(rec.data); 
							},this);
							
							
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione degli ordini a fornitore?', function(btn){
							   if(btn === 'yes'){
									Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
							            method: 'POST',
					        			jsonData: {list_selected_id: list_selected_id},						            
							            success: function ( result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                
							                //apro un report passando le righe interessate
												var mapForm = document.createElement("form");
												mapForm.target = "_blank";    
												mapForm.method = "POST";
												mapForm.action = '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report';
												
												// Create an input
												var mapInput = document.createElement("input");
												mapInput.type = "hidden";
												mapInput.name = "jsonData";
												mapInput.value = JSON.stringify(jsonData);
												
												// Add the input to the form
												mapForm.appendChild(mapInput);
												
												// Add the form to dom
												document.body.appendChild(mapForm);
												
												// Just submit
												mapForm.submit();							                
							                
							                	m_win.fireEvent('afterExecute');
							                	m_win.close();
							                
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });
										            
								            
								            
								            
								            
								         	}
								       }  			       
			]
		}],	                    
		
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		            listeners: {
		              'edit': function(editor, e, opts) {
		              	
		              	//se una data, converto il valore in Ymd
		              	if (e.field == 'CONSEGNA_STD'){
		              		e.record.set(e.field, Ext.Date.format(e.value, 'Ymd'));
		              	}
		              
		             	e.grid.store.save();
		             	
		             	//Forzoicalcolo totale riga
		             	e.record.set('PRZ_LIST_TOT');
		             	
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],	    
	    
		features: [
					{ftype: 'summary'}, {
			            id: 'group',
			            ftype: 'groupingsummary',
			            groupHeaderTpl: '{[values.rows[0].data.FORNITORE_D]} [{name}]',
			            hideGroupedHeader: false
			        },					
						
			{
				ftype: 'filters',
				encode: false, //settingsGrid.store.commitChanges() json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store:array',
			autoLoad:true,
			autoSave: true,
			
			data: <?php echo acs_je($m_params->rec_data); ?>, 
			groupField: 'FORNITORE_C',
				
			fields: ['DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'UM', 
						{name: 'DISPONIBILITA', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'}, 
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'QTA_RIORD', 'PRZ_LIST', 
						{name: 'PRZ_LIST_TOT', type: 'float',
							convert: function(val,row) {
    							return parseFloat(row.get('QTA_RIORD')) * parseFloat(row.get('PRZ_LIST'));
    						}
    					}	
					]
						
		}, //store	    
	    			 		
		columns: [				    
				  	{header: 'Codice', width: 100, dataIndex: 'ARTICOLO_C', filter: {type: 'string'}}
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}, 	
				  		 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					           return 'Totale ordine a fornitore'; 
					     }
					}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', tdCls: 'grassetto', 
				  	
					renderer: function (value, metaData, record, row, col, store, gridView){						
			                	if (record.get('OLD_RIORD').trim() != '')
			                		metaData.tdCls += ' grassetto sfondo_grigio';
			                	
			                	return floatRenderer0N(value);
			        },				  	
				  	
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  
				   }
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS, tdCls: 'grassetto',
					editor: {
			                xtype: 'datefield',
			                allowBlank: false			                
			            }				  				   
				  
				  }
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', renderer: floatRenderer2N, tdCls: 'grassetto',
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  				   
				  }				  
				  , {header: 'Importo',	 	width: 72, dataIndex: 'PRZ_LIST_TOT', align: 'right', renderer: floatRenderer2N,
							 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
								            return floatRenderer2(value); 
								        }				  
				  }				  
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    },
		    
		    
         listeners: {
         }		    
		    
    							
	}    
    

}


<?php exit; }
// ******************************************************************************************
// exe modifica punto riordino su articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_punto_riordino'){
	$m_params = acs_m_params_json_decode();

		//modifico il dato su anagrafica articoli e su wmaf20
		// ANAG. ARTICOLI
		$sql = "UPDATE {$cfg_mod_DeskAcq['file_anag_art']} SET ARLOTT=? WHERE ARDT=? AND ARART=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($m_params->f_qta, trim($m_params->f_dt), trim($m_params->f_rdart)));
			echo db2_stmt_errormsg($stmt);;
			
		// WMAF20
			$sql = "UPDATE {$cfg_mod_DeskAcq['file_WMAF20']} SET M2LOTT=? WHERE M2DT=? AND M2ART=?";
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($m_params->f_qta, trim($m_params->f_dt), trim($m_params->f_rdart)));
			echo db2_stmt_errormsg($stmt);;
	
	$ret['success'] = true;
	echo acs_je($ret);
	
	exit;
}
	


// ******************************************************************************************
// FORM PARAMETRI per modifica punto riordino
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_punto_riordino'){
	$m_params = acs_m_params_json_decode();	
	?>
{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [{
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm();
			            	m_win = this.up('window');
			            	form_values = form.getValues();
			            	
			            	 
			            	if(form.isValid()){
					
					        		//carico la form dal json ricevuto da php
					        		Ext.Ajax.request({
					        		        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_punto_riordino',
					        		        jsonData   : form_values,
					        		        method     : 'POST',
					        		        waitMsg    : 'Data loading',
					        		        success : function(result, request){
					        		            var jsonData = Ext.decode(result.responseText);
					        		            mp.add(jsonData.items);
					        		            mp.doLayout();
					        		            mp.show();

												m_win.fireEvent('afterExecute', form_values);					        		            
					        		            
					        		            m_win.close();        		            
					        		        },
					        		        failure    : function(result, request){
					        		            Ext.Msg.alert('Message', 'No data to be loaded');
					        		        }
					        		    });
					
					        	 
							
			            	}
			            }
			         }],   		            
		            
		            items: [
		            

				            	{
								   xtype: 'hiddenfield'
								   , name: 'f_dt'
								   , value: <?php echo j(trim($m_params->dt)); ?>
								}, {
								   xtype: 'hiddenfield'
								   , name: 'f_rdart'
								   , value: <?php echo j(trim($m_params->rdart)); ?>
								}, {
								   xtype: 'displayfield'
								   , name: 'f_no_use'
								   , allowBlank: false
								   , fieldLabel: 'Articolo'
								   , value: <?php echo j(trim($m_params->rddart) . " [" . trim($m_params->rdart) . "]"); ?>
								   , anchor: '-15'
								   , margin: "10 10 0 10"
								   , disabled: false							   
								}, {
								   xtype: 'numberfield'
								   , name: 'f_qta'
								   , allowBlank: false
								   , fieldLabel: 'Punto di riordino'
								   , margin: "10 10 0 10"
								   , value: <?php echo j(trim($m_params->qta)); ?>
								}
												  
						  
						  ]
		              }			  
			  
			  
			  ]
			   
		}


 ]
}

<?php exit; }

/*************************************************************************
 * REPORT (sulle righe interessate alla generazione degli ordini)
*************************************************************************/
if ($_REQUEST['fn'] == 'get_report'){
	
	$m_params = json_decode($_REQUEST['jsonData']);
	$my_array = array();
	foreach($m_params->el_row as $ro){
		$r = (array)$ro;
		
		//raggruppo per fornitore (e data consegna std???)
		
		$liv0_v = trim($r['M2FOR1']);	//fornitore
		$liv1_v = trim($r['M2DTE20']);	//consegna std	
		
		// LIVELLO 0 - fornitore
		$s_ar = &$my_array;
		$liv_c     = $liv0_v;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_0";
			$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
			$s_ar[$liv_c]["task"] = $r['M2RGS1'];

		}
		$s_ar = &$s_ar[$liv_c]["children"];
				
		// LIVELLO 1
		$liv_c     = $liv1_v;
		$tmp_ar_id[] = 'liv2;' .$liv_c;
		if (is_null($s_ar[$liv_c])){
			$s_ar[$liv_c]["liv"]  = "liv_2";
			$s_ar[$liv_c]["liv_cod"]  	= "{$liv_c}";
			$s_ar[$liv_c]["task"] 		= "{$liv_c}";
		}
				
		//accodo la riga
		$r['COSTO_TOT']					= (float)$r['M2PRZ'] * (float)$r['M2QOI20'];
		$s_ar[$liv_c]["children"][]		= $r;
		
	}
	
	//print_r($my_array);
	
	//stampo il report -----------------------------------------------------------------------------------------
	?>
<html>

<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table{border-collapse:collapse; width: 100%;}
div.acs_report table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px; font-size: 14px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}

div.acs_report h1{font-size: 22px; margin-top: 10px; margin-bottom: 10px;}
div.acs_report h2{font-size: 16px;}
 
@media print
{
	.noPrint{display:none;}
}




</style>



  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>



</head>


 <body>
 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'N';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content' class=acs_report> 
 
 <h1>Riepilogo richieste ordine materiali promozionali generate [<?php  echo trim($auth->get_user()); ?>]</h1>
 
  <table class=acs_report>  
 	<?php foreach($my_array as $k0 => $l0){ ?>
 		<tr class="t-l1">
 			<td colspan=7><h2>Fornitore: <?php echo "{$l0['task']} [{$l0['liv_cod']}]"; ?></h2></td>
 		</tr> 			
 		<tr>
 			<th>Consegna richiesta</th>
 			<th>Articolo</th>
 			<th>Codice</th>
 			<th>UM</th>
 			<th>Q.t&agrave; ordinata</th>
 			<th>Costo unitario</th>
 			<th>Costo totale</th>
 		</tr>
 			
 			<?php $costo_tot_for = 0; ?>
 			
			<?php foreach($l0["children"] as $k1 => $l1){ ?>
			 			
				<?php foreach($l1["children"] as $k2 => $l2){ ?>
				
					<?php $costo_tot_for	+= $l2['COSTO_TOT']; ?>				
					<?php $costo_tot 		+= $l2['COSTO_TOT']; ?>
				
					<tr>
						<td><?php echo print_date($k1); ?></td>
						<td><?php echo trim($l2['M2DART']); ?></td>
						<td><?php echo trim($l2['M2ART']); ?></td>
						<td><?php echo trim($l2['M2UM']); ?></td>
						<td class=number><?php echo n($l2['M2QOI20']); ?></td>
						<td class=number><?php echo n($l2['M2PRZ'], 2); ?></td>
						<td class=number><?php echo n($l2['COSTO_TOT'], 2); ?></td>						
					</tr>
				<?php } ?>			 			
			 			
 			<?php } ?>

 			<!--  totale fornitore -->
					<tr>
						<td colspan=6>Totale fornitore</td>
						<td class=number><?php echo n($costo_tot_for, 2); ?></td>						
					</tr> 			
 			
 			
 	<?php } ?>
 	
 			<!--  totale generale -->
 					<tr>
						<td colspan=7>&nbsp;</td>						
					</tr>
 					<tr class="liv_totale">
						<td colspan=6>Totale generale</td>
						<td class=number><?php echo n($costo_tot, 2); ?></td>						
					</tr> 	
 	
 	
   </table>
  </div> 	
 </body>
</html>	
	<?php
	// ----------------------------------------------------------------------------------------- stampo il report
	exit;	
}



/*************************************************************************
 * GENERAZIONE ORDINI MTO
*************************************************************************/
if ($_REQUEST['fn'] == 'exe_generazione_ordini'){
	
	/*
	 Viene passato l'elenco delle chiavi ditta/articolo e relativa qta ordinata
	 1) aggirno le qta' su wmaf20 (usando i campi 20)
	 2) creo una riga su RI per ogni fornitore/data
	*/	
	
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$ar_ord = array();	
	
	$m_params = acs_m_params_json_decode();
	$list_selected_id = $m_params->list_selected_id; //elenco chiavi di fabbisogno	

	$sql = "UPDATE {$cfg_mod_DeskAcq['file_WMAF20']} 
			SET M2QOI20=?, M2FTQ20 = 'I'
			 , M2DTE20=?, M2PRZ=? 
			WHERE M2DT=? AND M2ART=? ";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	
	foreach($list_selected_id as $kr => $r){
		$r = (array)$r;
		$dt 	= trim($r['DT']);
		$art	= trim($r['ARTICOLO_C']);
		$qta	= (float)$r['QTA_RIORD'];
		$cons	= (string)$r['CONSEGNA_STD'];
		$forn	= (string)$r['FORNITORE_C'];
		$prz	= (float)$r['PRZ_LIST'];
		$result = db2_execute($stmt, array($qta, $cons, $prz, $dt, $art));
		echo db2_stmt_errormsg($stmt);

		//mi creo un array per ditta/fornitore/consegna		
		$ar_ord[$dt][$forn][$cons] +=1;		
	}

	
	//Prima di generare gli ordini, recupero l'elenco delle righe interessate (perche' dovro' fornire un report)
	//TODO: gestire la multisessione?
	$sql2 = "SELECT * FROM {$cfg_mod_DeskAcq['file_WMAF20']} WHERE M2FTQ20 = 'I'";
	$stmt2 = db2_prepare($conn, $sql2);
	$result2 = db2_execute($stmt2);
	while ($r = db2_fetch_assoc($stmt2)) {
		$ret_row[] = $r;
	}
	
	
	
	//per ogni riga dell'array creo una riga nell'RI
	foreach($ar_ord as $kdt => $ar_dt){
		foreach($ar_dt as $kfor => $ar_for){
			foreach($ar_for as $kdata => $ar_data){
				//scrivo RI per il fornitore/data inline
				//HISTORY
				$sh = new SpedHistory($main_module);
				$sh->crea(
						'dora_gen_ord_mpr',
						array(
								"ditta"		=> $kdt,
								"fornitore" => $kfor,
								"data_cons"	=> $kdata
						)
				);
			}
		}
	}
	
	
	
	$ret = array();
	$ret['success']	= true;
	$ret['el_row']	= $ret_row;
	echo acs_je($ret);
	
	exit;
}



/*************************************************************************
 * GET JSON DATA
 *************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data_grid'){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$sql_where_flt	= "";
	$sql_where 		= " M2TPAR = 'V' ";
//	$sql_where 		= " 1=1 ";
	
	$sql = "SELECT M2QMAN, M2LOTT, M2FTQ20, M2QOI20, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
			W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM, T_BREF.TACINT AS REF_NAME, T_GRPA.TADESC AS GR_PIANIF_DESC,
			SUM(W3.M3QTA) AS S_M3QTA,
			W2.M2TAB1 AS GR_PIANIF,
			ANAG_ART.ARSOSP, ANAG_ART.ARESAU
			

			FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_WMAF30']} W3
				ON W2.M2DT = W3.M3DT AND W2.M2ART = W3.M3ART
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
				ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE
			LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_cli']} CF
				ON CF.CFDT = W2.M2DT AND CF.CFCD = W2.M2FOR1 AND CF.CFTICF = 'F'
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_BREF
				ON T_BREF.TAID = 'BREF' AND T_BREF.TANR = CF.CFREFE AND T_BREF.TADT = W2.M2DT
			LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} T_GRPA
				ON T_GRPA.TAID = 'GRPA' AND T_GRPA.TANR = W2.M2TAB1 AND T_GRPA.TADT = W2.M2DT
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
				ON W2.M2DT = ANAG_ART.ARDT AND W2.M2ART = ANAG_ART.ARART
			WHERE 1=1 {$sql_where_flt} AND {$sql_where}
			GROUP BY 
 				M2QMAN, M2LOTT, M2FTQ20, M2QOI20, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
				W2.M2SELE, W2.M2DT, W2.M2FOR1, CF.CFRGS1, W2.M2DRIO, W2.M2ART, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM, T_BREF.TACINT, T_GRPA.TADESC,
				W2.M2TAB1, ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
				TA_ITIN.TASITI
				
			ORDER BY TA_ITIN.TASITI, CF.CFRGS1, W2.M2DRIO, W2.M2TAB1, W2.M2ART
			";

	//$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_WMAF20']} W2";
	
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
	
	while ($r = db2_fetch_assoc($stmt)) {
		$r['DT'] 			= acs_u8e($r['M2DT']);
		$r['FORNITORE_C'] 	= acs_u8e($r['M2FOR1']);		
		$r['FORNITORE_D'] 	= acs_u8e($r['CFRGS1']);
		$r['ARTICOLO_C'] 	= acs_u8e($r['M2ART']);		
		$r['ARTICOLO_D'] 	= acs_u8e($r['M2DART']);
		$r['UM'] 			= acs_u8e($r['M2UM']);
		$r['GIACENZA'] 		= acs_u8e($r['M2QGIA']);				
		$r['ORDINATO'] 		= acs_u8e($r['M2QORD']);
		$r['IMPEGNATO'] 	= $r['M2QFAB'];
		$r['SCORTA'] 		= acs_u8e($r['M2SMIC']);
		
		$r['LOTTO_RIORD']	= (int)$r['M2LOTT'];
				
		$r['CONSEGNA_STD']	= acs_u8e($r['M2DRIO']);		
		$r['ULTIMO_IMP']	= acs_u8e($r['M2UIMP']);
		$r['DISPONIBILITA']	= acs_u8e((float)$r['M2QGIA'] + (float)$r['M2QORD'] - (float)$r['M2QFAB']);
		$r['DISPONIBILITA_MANCANTE']	= $r['M2QMAN'];
		
		//valore di scostamento dalla disponibilita'
		if ((int)$r['LOTTO_RIORD'] != 0){
			$r['SCOST_RIORD'] = ( (float)$r['DISPONIBILITA'] - (float)$r['LOTTO_RIORD'] ) / (float)$r['LOTTO_RIORD'] * 100;
		}
		
		
		$r['QTA_RIORD']		= $r['M2QOI20'];
		$r['OLD_RIORD']		= $r['M2FTQ20'];
		$r['PRZ_LIST']		= $r['M2PRZ'];		
		$ar_ret[] = $r;
	}
	
	
	echo acs_je($ar_ret);
	
    exit();
}








/***********************************************************************************
 * GRID
 ***********************************************************************************/
if ($_REQUEST['fn'] == 'get_json_grid'){

 $m_params = acs_m_params_json_decode();
 
 //recuper data/ora ultimo aggiornamento 
	$sql = "SELECT min(M2dtge) as M2DTGE, min(m2orge) AS M2ORGE FROM {$cfg_mod_DeskAcq['file_WMAF20']}		";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$r = db2_fetch_assoc($stmt);
	if ($r != false){
		$situazione_txt = " [Situazione al " . print_date($r['M2DTGE']) . " " . print_ora($r['M2ORGE']) . "]";
	} else 
		$situazione_txt = '';
	

?>

{"success":true, "items":

  {
    xtype: 'panel',
	id: 'panel-riordino_materiali_promo',	    
	title: 'Marketing',
	closable: true,
	 	
    layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},    
    items: [
    
    {
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    
	    
        dockedItems: [{
            xtype: 'toolbar'
            , dock: 'top'
            , items: [{xtype: 'label', cls: 'strong', text: 'Controllo disponibilita/riordino materiali promozionali' + <?php echo j($situazione_txt); ?>}, {xtype: 'tbfill'}
            
				, {
                     xtype: 'button',
                     text: '',
			            handler: function() {
			         	}
			       }
			       
 				, {
                     xtype: 'button',
	            	 //scale: 'medium',
	            	 iconCls: 'icon-shopping_cart_green-16',
                     text: 'Conferma generazione ordini a fornitore',
			            handler: function() {
			            
			            	list_selected_id = [];
							grid = this.up('grid');
							mm_grid = this.up('grid'); //altrimenti se lasciavo grid mi aggiornata la tabella della sottowindow
														
							grid.getStore().each(function(rec)  
							{  
							  if (rec.get('OLD_RIORD').trim() == '' && parseFloat(rec.get('QTA_RIORD')) > 0)
							  	list_selected_id.push(rec.data); 
							},this);
							
							
							//apro form per richiesta conferma e possibilita' modifica prezzo e data consegna
							                   	my_listeners = {
						        					afterExecute: function(){
						        						mm_grid.store.reload();						        									            
										        		}
								    				};							
							
							
							acs_show_win_std('Riepilogo articoli da riordinare per fornitore', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_conferma_ordine', {rec_data: list_selected_id}, 800, 500, my_listeners, 'icon-shopping_cart_gray-16');
							return false;
							
							
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione degli ordini a fornitore?', function(btn){
							   if(btn === 'yes'){
									Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
							            method: 'POST',
					        			jsonData: {list_selected_id: list_selected_id},						            
							            success: function ( result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                
							                //apro un report passando le righe interessate
												var mapForm = document.createElement("form");
												mapForm.target = "_blank";    
												mapForm.method = "POST";
												mapForm.action = '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report';
												
												// Create an input
												var mapInput = document.createElement("input");
												mapInput.type = "hidden";
												mapInput.name = "jsonData";
												mapInput.value = JSON.stringify(jsonData);
												
												// Add the input to the form
												mapForm.appendChild(mapInput);
												
												// Add the form to dom
												document.body.appendChild(mapForm);
												
												// Just submit
												mapForm.submit();							                
							                
							                
							                grid.store.load()
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });
										            
			         	}
			       }  			       
			       
			       
			       
			]
		}],	                    
		
	    
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		              clicksToEdit: 1,
		            listeners: {
		              'beforeedit': function(e, eEv) {
		                var me = this;
		                
		                rec = eEv.record;
		                console.log(rec);		                
						if (rec.data.OLD_RIORD.trim() == '' && rec.data.FORNITORE_D.trim() != '')		                
		                	return true;
		                else
		                	return false;
		              },
		              'edit': function(e) {
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],	    
	    
		features: [
			{
				ftype: 'filters',
				encode: false, // json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
				
				method: 'POST',
				type: 'ajax',
	            timeout: 120000,				
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				}				
			},
				
			fields: ['DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'UM', 
						{name: 'DISPONIBILITA', type: 'int'}, {name: 'DISPONIBILITA_MANCANTE', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'}, 
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'QTA_RIORD', 'PRZ_LIST']
						
		}, //store	    
	    			 		
		columns: [
				    {header: 'Fornitore', flex: 1, dataIndex: 'FORNITORE_D', filter: {type: 'string'}}
				  , {header: 'Codice', width: 100, dataIndex: 'ARTICOLO_C', filter: {type: 'string'}}
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Disponib.',	width: 72, dataIndex: 'DISPONIBILITA', align: 'right',
					    renderer: function (value, metaData, record, row, col, store, gridView){
			                	metaData.tdCls += ' grassetto';

					  				if (parseFloat(value) < 0)
					  					 metaData.tdCls += ' sfondo_rosso';					  					 														  
					  				else if (parseFloat(value) == 0 || parseFloat(record.get('DISPONIBILITA_MANCANTE')) > 0 )
					  					 metaData.tdCls += ' sfondo_giallo';					  					 
					  				if (parseFloat(value) > 0)
					  					 metaData.tdCls += ' sfondo_verde';					  					 
			                			                
					  			return floatRenderer0N(value);									
						} 
				    }				  
				  , {header: 'Giacenza',	width: 72, dataIndex: 'GIACENZA', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Ordinato',	width: 72, dataIndex: 'ORDINATO', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Impegnato',	width: 72, dataIndex: 'IMPEGNATO', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Scorta',	 	width: 72, dataIndex: 'SCORTA'	, align: 'right', renderer: floatRenderer0N}				  				  				  
				  , {header: 'Punto di<br/>riordino',	width: 72, dataIndex: 'SCOST_RIORD', align: 'right',

						renderer: function (value, metaData, record, row, col, store, gridView){
								if (parseFloat(value) != 0){
				                	if (value < -5)
				                		metaData.tdCls += ' grassetto sfondo_rosso';
									else if (value <= 5)				                	
										metaData.tdCls += ' grassetto sfondo_giallo';
								}		
				                	
				                	return floatRenderer0N(record.get('LOTTO_RIORD'));
				        },				  	
				  	 
				  	}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', 
				  	
					renderer: function (value, metaData, record, row, col, store, gridView){
			                	if (record.get('OLD_RIORD').trim() != '')
			                		metaData.tdCls += ' grassetto sfondo_grigio';
			                	
			                	return floatRenderer0N(value);
			        },				  	
				  	
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  
				   }
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS}
				  , {header: 'Ultimo<br/>impegno',	width: 90, dataIndex: 'ULTIMO_IMP', renderer: date_from_AS}
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', renderer: floatRenderer2N}
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    },
		    
		    
         listeners: {
   		  celldblclick: {								
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
			  	rec = iView.getRecord(iRowEl);
			  	
			  	if (col_name == 'SCOST_RIORD'){

							                   	my_listeners = {
						        					afterExecute: function(form_values){
						        						rec.set('LOTTO_RIORD', form_values.f_qta);
						        						
						        						//ricalcolo lo scostamento
						        						
						        						//TODO: verificare perche' sembra che non reimposta correttamente il valore (o almeno lo sfondo)
						        						//if (parseFloat('LOTTO_RIORD') != 0)
														//	rec.set('SCOST_RIORD', ( parseFloat(rec.get('DISPONIBILITA')) - parseFloat(rec.get('LOTTO_RIORD')) ) / parseFloat(rec.get('LOTTO_RIORD')) * 100);
														
														rec.set('SCOST_RIORD', 0);							        						
						        						
						        						//iView.refreshNode(rec);
						        						//iView.getView().refreshRow(rec);						        									            
										        		}
								    				};			  	
			  	
			  		//modifica punto di riordino
					acs_show_win_std('Modifica punto di riordino', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_punto_riordino', {dt: rec.get('DT'), rdart: rec.get('ARTICOLO_C'), rddart: rec.get('ARTICOLO_D'), qta: rec.get('LOTTO_RIORD')}, 600, 200, my_listeners, 'icon-shopping_cart_gray-16');			  		
			  		return false;
			  	} 		  
							  
				  acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('DT'), rdart: rec.get('ARTICOLO_C')}, 1200, 600, null, 'icon-shopping_cart_gray-16');
			  }
   		  }
	         
         }		    
		    
    							
	}    
    
    ] //items
  } //main panel 

}
<?php
exit; }
?>