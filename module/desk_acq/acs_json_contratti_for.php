<?php
require_once "../../config.inc.php";

$main_module = new DeskAcq();

$m_params = acs_m_params_json_decode();



// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	set_time_limit(300);	
	$stmt = $main_module->get_contratti_for($id_ditta_default, $_REQUEST['k_cliente']);

	$ar_tot = array();
	$ar = array();
	while ($r = db2_fetch_assoc($stmt)) {
		$r['contratto'] = implode("_", array($r['RDRIGA'], $r['RDSRIG']));
		$r['importo']   = $r['RDQTA'] * $r['RDQTA3']; 
		$r['RDDES2'] = acs_u8e($r['RDDES2']);		
		$ar[] = $r;
		$ar_tot['RDQTA'] 		+= $r['RDQTA'];
		$ar_tot['importo'] 		+= $r['importo'];
		$ar_tot['count_contratto'][$r['contratto']] 	+= 1;
		$ar_tot['count_RDODER'][$r['RDODER']] 	+= 1;		
		$ar_tot['count_RDART'][$r['RDART']] 	+= 1;		
	}
	
	//riga globale
	array_push($ar, array(	'tipo_riga'	=> 'GLOBALE',
							'RDDES2' 	=> 'Riepilogo globale',
							'RDQTA'		=> $ar_tot['RDQTA'],
							'importo'	=> $ar_tot['importo'],
							'contratto'	=> "(#" . count($ar_tot['count_contratto']) . ")",
							'RDODER'	=> "(#" . count($ar_tot['count_RDODER']) . ")",
							'RDART'		=> "(#" . count($ar_tot['count_RDART']) . ")"
	));
	
	echo acs_je($ar);		
	exit;
}



/******************************************************
 * GRID JSON
 ******************************************************/
?>


{"success": true, "items":
	{
		xtype: 'grid',
		
		
			viewConfig: {
			        getRowClass: function(record, index) {
						if (record.get('tipo_riga') == 'GLOBALE')
							return ' segnala_riga_giallo';			
			         }   
			    },												    
		
		
		
				features: [
					{ftype: 'summary'}, {
			            id: 'group',
			            ftype: 'groupingsummary',
			            groupHeaderTpl: '{name}',
			            hideGroupedHeader: false
			        }
				],	
		
		
				store: new Ext.data.Store({
		
					groupField: 'RDDES2',		
		
					autoLoad:true,				        
  					proxy: {
						url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						timeout: 300000,
						extraParams: <?php echo acs_raw_post_data() ?>,						
						type: 'ajax',
						reader: {
					            type: 'json',
					            root: 'root'
					        }					        
						},
						
	        			fields: ['RDDT', 'RDART', 'RDDES1', 'RDUM', {name: 'RDQTA', type: 'float'}, 'RDODER', 'RDDES2', 'RDUM3', 'RDQTA3', 'importo', 'contratto', 'tipo_riga']
	    			}),
	    			
		        columns: [{
			                header   : 'Contratto',
			                dataIndex: 'contratto', 
			                width:	70
			             }, {
			                header   : 'Consegna<br>dal',
			                dataIndex: 'RDODER', width:	70,
				          	  renderer: function (value, metaData, record, row, col, store, gridView){						
				  				if (record.get('RDDES2') == 'Riepilogo globale'){
				  	  				 return value
				  				} else											
				  					return date_from_AS(value);			    
							}			                
			                
			             }, {
			                header   : 'Articolo',
			                dataIndex: 'RDART', 
			                width     : 90
			             }, {
			                header   : 'Descrizione',
			                dataIndex: 'RDDES1',			                 
			                flex     : 1
			             }, {
			                header   : 'UM', 
			                width     : 40,
			                dataIndex: 'RDUM',			                			                
			             }, {
			                header   : 'Quantit&agrave;',
			                dataIndex: 'RDQTA', 
			                width: 90, align: 'right', renderer: floatRenderer2,
							 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
								            return floatRenderer2(value); 
								        }			                 
			             }, {
			                header   : 'Prezzo<br>unitario',
			                dataIndex: 'RDQTA3', 
			                width: 90, align: 'right', renderer: floatRenderer2 
			             }, {
			                header   : 'Importo',
			                dataIndex: 'importo', 
			                width: 90, align: 'right', renderer: floatRenderer2,
							 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
								            return floatRenderer2(value); 
								        }			                 
			             }]	    	
			             
		, listeners: {		
	 			afterrender: function (comp) {	 				
	 			}
	 			
			      , celldblclick: {										      		
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
						  	rec = iView.getRecord(iRowEl);

							iEvent.preventDefault();														
						  	acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('RDDT'), rdart: rec.get('RDART')}, 1200, 600, null, 'icon-shopping_cart_gray-16');																
							return false;			  
						}
				 }			 			
	 			
	 			
	 	}			             									
		         
	}
}
