<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();

?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'abcdeeeee.php',
            
            items: [
            
					 
					 
		 {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Fornitore',
			minChars: 2,			
            anchor: '-15',
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_for_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            
	        listeners: {
	            change: function(field,newVal) {	            	
	            	var form = this.up('form').getForm();
	            	form.findField('f_destinazione_cod').setValue(''); 	            	
	            	form.findField('f_destinazione_cod').store.proxy.extraParams.cliente_cod = newVal
	            	form.findField('f_destinazione_cod').store.load();	            	
	            }
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '{cod}' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }
					 
					 
					 
					 
					 
		, {
            xtype: 'combo',
			name: 'f_destinazione_cod',
			fieldLabel: 'Destinazione',
			minChars: 2,			
            anchor: '-15',		
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : 'acs_get_select_json.php?select=search_for_des_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            },
                 extraParams: {
        		  	cliente_cod: ''
        		 }		            
		        },       
				fields: ['cod', 'descr'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: false, //mostro la freccetta di selezione
            anchor: '100%',

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessuna destinazione trovata',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        }
        
					 
					 
					 
					 
					 
					 
				  , {
						name: 'f_riferimento',
						xtype: 'textfield',
						fieldLabel: 'Riferimento',
						value: '',
					    anchor: '-15'							
					 }
					 
					 
					 
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data consegna',
						
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}

					
					
					 
					 , { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data emissione',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_ricezione_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_ricezione_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}
					
					
					
					
					
					
					
					, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}, {
							name: 'f_itinerario',
							xtype: 'combo',
							fieldLabel: 'Gruppo',
							labelAlign: 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ITIN', null, 1, null, null, null, $main_module), '') ?> 	
								    ] 
								}						 
							}
						
						]
					}, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							name: 'f_tipo_ordine',
							xtype: 'combo',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV', null, 1, null, null, null, $main_module), '') ?> 	
								    ] 
								}						 
							}, {
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							labelAlign: 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,
						   	multiSelect : true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							}
						
						]
					}, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							name: 'f_priorita',
							xtype: 'combo',
							fieldLabel: 'Priorit&agrave;',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->get_options_priorita(), '') ?> 	
								    ] 
								}						 
							}, {
							name: 'f_solo_bloccati',
							xtype: 'checkboxgroup',
							fieldLabel: 'Solo bloccati',
							labelAlign: 'right',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_solo_bloccati' 
		                          , boxLabel: ''
		                          , inputValue: '1'
		                        }]														
							}
						
						]
					}, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							name: 'f_num_ordine',
							xtype: 'textfield',
							fieldLabel: 'Nr. Ordine',
							value: '',
							flex: 1							
						 }, {
							name: 'f_num_carico',
							xtype: 'textfield',
							fieldLabel: 'Nr. Carico',
							value: '',
							flex: 1,
							labelAlign: 'right'
						 }, {
							name: 'f_num_lotto',
							xtype: 'textfield',
							fieldLabel: 'Nr. Lotto',
							value: '',
							flex: 1,
							labelAlign: 'right'														
						 } 
						]
					}
					
					, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'hbox',
			            style: 'padding-top: 4px; padding-bottom: 4px',			            
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Anomalie evasione',
			                boxLabel: 'Tutti',
			                name: 'anomalie_evasione',
			                checked: true,			                
			                inputValue: 'T',
			                flex: 1
			            }, {
			                boxLabel: 'Con anomalie',
			                checked: false,			                
			                name: 'anomalie_evasione',
			                inputValue: 'Y', 
			                flex: 2			                
			            }]
			        } 
					
					
					
				],
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "MAIN_SEARCH");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
					var form_values = form.getValues();
					form_values.from_type = 'MAIN_SEARCH';
					form_values.f_stato_ordine = form.findField("f_stato_ordine").getValue().join(','),

		                        mp = Ext.getCmp('m-panel');
		                            	
					    		//carico la form dal json ricevuto da php
					    		Ext.Ajax.request({
					    		        url        : 'acs_arrivi_json_elenco.php',
				        				method     : 'POST',
					    		        waitMsg    : 'Data loading',
					    		        jsonData: form_values,
					    		        success : function(result, request){
					    		            var jsonData = Ext.decode(result.responseText);
					    		            mp.add(jsonData.items);
					    		            mp.doLayout();
					    		            mp.show();
					    		        },
					    		        failure    : function(result, request){
					    		            Ext.Msg.alert('Message', 'No data to be loaded');
					    		        }
					    		    });
		                            			
		            this.up('window').close();            	                	                
	            }
	        }]             
				
		, listeners: {		
	 			afterrender: function (comp) {
	// 				comp.up('window').setWidth(600);
	// 				comp.up('window').setHeight(570);
	// 				window_to_center(comp.up('window'));
	 			} 			
			}
	        
				
        }
]}