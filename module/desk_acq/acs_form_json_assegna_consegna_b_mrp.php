<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();

$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

//se non ho ricevuto cod_iti (quindi provengo da INFO) recupero cod_iti dall'ordine selezionato
if (trim(strlen($m_params->cod_iti) == 0)){
	$from_INFO = 'Y';
	//$m_params->cod_iti = trim($m_params->list_selected_id[0]->cod_iti);
	$k_ordine = trim($m_params->list_selected_id[0]->k_ordine);
	$ord = $main_module->get_ordine_by_k_docu($k_ordine);
	$m_params->cod_iti = $ord['TDCITI'];
}

if (trim($m_params->from) == 'BACKGROUND_MRP'){
	$cod_art = trim($m_params->list_selected_id[0]->cod_art);
	$des_art = trim($m_params->list_selected_id[0]->task);
	$title_art = "[{$cod_art}] {$des_art}";
	$dt		 = $m_params->list_selected_id[0]->M3DT;
	
	$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_ACQ'));
	$row_giac_art = db2_fetch_assoc($s->get_art_giac($dt, $cod_art));
	$data_consegna_stardard = $row_giac_art['M2DRIO']; // data consegna standard (articoli)
	$min_data_valida = oggi_AS_date();
} else {
	$title_art = '';
}


//recupero (dall'itinerario attuale) l'area di spedizione attuale
$itin_attuale = $main_module->get_TA_std('ITIN', $m_params->cod_iti);
$itin_attuale_cod = trim($m_params->cod_iti);
$area_attuale_cod = trim($itin_attuale['TAASPE']);

?>

{"success":true, "items": [

  {
   xtype: 'panel',
	layout: {
	    type: 'vbox',
	    align: 'stretch',
	    pack : 'start',
	},
   items: [



	// ----------------- REGOLE RIORDINO -----------------------------

  
	{
		xtype: 'grid',
		title: 'Condizioni riordino standard - Consegne programmate',
		height: 110,
		id: 'regole-riordino',
		loadMask: true,
		closable: false,			
		
		viewConfig: {
		        getRowClass: function(record, index, rowParams, store) {
		            return record.get('rowCls');
		        }
		 },		
		
		features: [
		new Ext.create('Ext.grid.feature.Grouping',{
			groupHeaderTpl: '{[values.rows[0].data.out_FOR_grp]}',
			hideGroupedHeader: false
		}),
		{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true,   // defaults to false (remote filtering)
	
			// Filters are most naturally placed in the column definition, but can also be added here.
		filters: [
		{
			type: 'boolean',
			dataIndex: 'visible'
		}
		]
		}],
	
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						
				}
			},
		},
	
	
	
		store: {
			xtype: 'store',
				
			//groupField: 'out_FOR',
			autoLoad:true,
	
			proxy: {
				url: <?php echo acs_url('module/desk_vend/acs_background_mrp_art.php?fn=get_json_data_regole_riordino_WMAF20&disableTot=Y') ?>,
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
				f_cod_art:  '<?php echo $cod_art; ?>',								
				}				
			},
				
			fields: ['out_FOR', 'out_FOR_grp', 'rowCls', 'CRDTGE'
					  	, 'CRGEM1', 'CRGEM2', 'CRGEM3', 'CRGEM4', 'CRGEM5', 'CRGEM6'
 						, 'CRGRF1', 'CRGRF2', 'CRGRF3', 'CRGRF4', 'CRGRF5', 'CRGRF6'					  
 						, 'CRGCF1', 'CRGCF2', 'CRGCF3', 'CRGCF4', 'CRGCF5', 'CRGCF6'
 						, 'CRLTFO', 'CRLTRI', 'CRLTLO', 'CRLTPR', 'CRSWTR', 'CRTPAR', 'CRPRIO'
 						, 'CRTART', 'CRTSOS', 'CRTSCA', 'CRTIDT', 'CRPROG'
 						, 'CRTATT', 'CRTESA', 'CRDTUI', 'M2DRIO', 'M2DIND'
					  ]
						
						
		}, //store
		multiSelect: false,
		
		<?php 
			$ss = "";
			//$ss .= "<img id=\"grafico_acq\"src=" . img_path("icone/48x48/grafici.png") . " height=30 onClick=\"acs_show_win_std(\'Storico codifiche articolo e tipo approvvigionamento\', \'../desk_acq/acs_grafici_fornitore.php\', " . strtr(acs_je($m_params), array('"' => "\'")). ", null, null, null, \'icon-shopping_setup-16\', null, \'Y\')\">";
		?>		
	
		columns: [
			
			{header: 'Fornitore', dataIndex: 'out_FOR', flex: 2, filter: {type: 'string'}, filterable: true},
			{header: '<?php echo $ss; ?>', width: 50, sortable: false, menuDisabled: true},			
					
			{header: 'Giorni di invio', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGEM1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGEM2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGEM3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGEM4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGEM5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGEM6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Giorni di ricezione', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGRF1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGRF2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGRF3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGRF4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGRF5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGRF6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Giorni di consegna', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGCF1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGCF2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGCF3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGCF4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGCF5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGCF6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Lead time', flex: 3,
			 columns: [
			 	{header: 'Art', dataIndex: 'CRLTFO', width: 30, renderer: floatRenderer0, align: 'right'},
			 	{header: 'Rit', dataIndex: 'CRLTRI', width: 30, renderer: floatRenderer0, align: 'right'},			 	
				{header: 'Log', dataIndex: 'CRLTLO', width: 30, renderer: floatRenderer0, align: 'right'},
				{header: 'Prod', dataIndex: 'CRLTPR', width: 30, renderer: floatRenderer0, align: 'right'}			
			 ]
			}, {header: 'Consegna<br>Standard', dataIndex: 'M2DRIO', width: 70, 
					renderer: function (value, metaData, record, row, col, store, gridView){
						return date_from_AS(value);
					}
			}, {header: 'Indisponib.<br>fino al', dataIndex: 'M2DIND', width: 70, 
					renderer: function (value, metaData, record, row, col, store, gridView){
						return date_from_AS(value);
					}
			  }			   
			
		]		
		
		
	}  

	

	
	
	


    ,    {
            xtype: 'grid',
            flex: 1,
            id: 'grid-el-scelta-spedizioni',
            
            
     <?php if (1 == 1) { ?>
     		dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                align: 'right',
                items: [
                 {
								     name: 'f_to_data'
								   , id: 'f_to_data'
								   , disabled: false                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: 'Assegna data consegna non programmata'
								   , labelAlign: 'right'
								   , labelWidth: 250
								   , format: 'd/m/Y'						   						   
								   , submitFormat: 'Ymd'
								   , allowBlank: false
								   , anchor: '-15'
								   , minValue: js_date_from_AS(<?php echo $min_data_valida; ?>)
				}, {
	                    iconCls: 'icon-save',
	                    itemId: 'save',
	                    text: 'Conferma',
	                    disabled: false,
	                    scope: this,
	                    handler: function(bt){
	                    		to_date = bt.up('window').down('#f_to_data').getSubmitValue();

	                    			if (bt.up('window').down('#f_to_data').isValid() == false)
	                    				return false;

	                    			list_selected_row = <?php echo acs_je($m_params->list_selected_id); ?>;
	                    			
								  	list_selected_id = [];
								  	for (var i=0; i<list_selected_row.length; i++) 
									   list_selected_id.push(list_selected_row[i].k_ordine);	                    			
	                    		
									Ext.Ajax.request({
									        url        : 'acs_op_exe.php?fn=assegna_spedizione',
									        method     : 'POST',
									        waitMsg    : 'Data loading',
						        			jsonData: {
						        				from: 'BACKGROUND_MRP_NUOVA_DATA',
						        				to_date: to_date, 
						        				list_selected_id: list_selected_id
											},							        
									        success : function(result, request){
						            			console.log(bt.up('window'));
									  			bt.up('window').fireEvent('afterExecute');						            			
							                    bt.up('window').close();
							                    m_win.close();
						            												        
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });							
	                    
	                    }
                	}                
                ]
            }],    
     <?php }?>            
            
            
      
            
            listeners: {
			  celldblclick: {								
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

				   rec = iView.getRecord(iRowEl);				  
				   m_win = this.up('window');
				   
				   //se la mi ha passato il listener onSpedSelected
				   // eseguo l'evento ritornando il record selezionato
				   if (typeof(m_win.initialConfig.listeners) !== 'undefined' &&
				   		typeof(m_win.initialConfig.listeners.onSpedSelected) !== 'undefined')
				    {
						m_win.fireEvent('onSpedSelected', rec);
						m_win.close();
				   		return false;						
					}					   

				  

				   m_carichi = rec.get('carico').split(',');
				   
				   carichi = [];
				   for (var i=0; i<m_carichi.length; i++) 
				   		carichi.push([m_carichi[i], m_carichi[i]]);

				   var loc_store = new Ext.data.ArrayStore({
				     fields: ['abbr', 'state'],
				     data : carichi 
				  });				  
				  
				  
				  //Chiedo conferma, o il carico, o un'autorizzazione esplicita
						var win = new Ext.Window({
						  width: 600
						, height: 500
						, minWidth: 300
						, minHeight: 30
						, plain: true
						, title: 'Conferma assegnazione ricezione'
						, layout: 'fit'
						, border: true
						, closable: true
						, items: {
				            xtype: 'form',
				            bodyStyle: 'padding: 10px',
				            layout: 'vbox',
				            frame: true,
				            title: '',
				            url: 'acs_op_exe.php',				            
				            items: [

// ********************** DEROGHE **************************** 					            
					            {
					            xtype: 'fieldset',
					            flex: 1, width: '100%',
					            title: 'Registro deroghe',
					            itemId: 'fs-deroghe',
					            items: [
					            
								 {
										name: 'f_data_programmata',
										xtype: 'hidden'
								 }, 								 {
										name: 'f_data_disponibilita',
										xtype: 'hidden'
								 }, {
										name: 'f_autorizzazione',
										xtype: 'combo',
										fieldLabel: 'Autorizzazione',
										displayField: 'text',
										valueField: 'id',
										emptyText: '- seleziona -',
										forceSelection:true,
									   	allowBlank: false, disableOnHide: true,														
										store: {
											autoLoad: true,
											editable: false,
											autoDestroy: true,	 
										    fields: [{name:'id'}, {name:'text'}],
										    data: [								    
											     <?php echo acs_ar_to_select_json(find_TA_std('AUDE'), ''); ?> 	
											    ] 
										}						 
									}, {
									   xtype: 'datefield'
									   // , width: '50%'
									   , startDay: 1 //lun.
									   , fieldLabel: 'Data'
									   , name: 'f_data'
									   , format: 'd/m/Y'
									   , submitFormat: 'Ymd'
									   , allowBlank: false, disableOnHide: true
									   , listeners: {
									       invalid: function (field, msg) {
									       Ext.Msg.alert('', msg);}
											}
								}, {
									name: 'f_commento',
									xtype: 'textfield',
									maxLength: 100,
									fieldLabel: 'Commento',
								    anchor: '-15',
								    allowBlank: false, disableOnHide: true							
								  }					            	
					            ]
					            }
// ********************** FINE DEROGHE (fieldset) ************************					            
					            
							  , {
							     //segnalazioni
				                 fieldLabel: '',
				                 name: 'LOG',
				                 disabled: true,
				                 xtype     : 'textareafield',
				                 height: 50, width: '100%',
				                 fieldCls: 'segnalazioni errore grassetto'	
					            }					            
					            			            
				            ],
				            
						buttons: [{
				            text: 'Conferma',
				            itemId: 'btnConf',
				            scale: 'large',
				            iconCls: 'icon-sub_blue_accept-32',
					            handler: function() {
					            	var form = this.up('form').getForm();
					            	var m_win = this.up('window');					            	
					            	
					            	m_grid = Ext.getCmp('grid-el-scelta-spedizioni');
					            	
					            	if(form.isValid()) {
					            		this.disable();
										Ext.Ajax.request({
										   url: 'acs_op_exe.php?fn=assegna_spedizione',
										   method: 'POST',
										   jsonData: {list_selected_id: m_grid.store.proxy.reader.jsonData.properties.el_ordini,
										   			  to_sped_id: iView.getSelectionModel().getSelection()[0].data.CSPROG,										   			  
										   			  f_data: Ext.Date.format(form.findField('f_data').getValue(), 'Ymd'),
										   			  f_commento: form.findField('f_commento').getValue(),
										   			  f_autorizzazione: form.findField('f_autorizzazione').getValue(),
										   			  f_data_programmata: form.findField('f_data_programmata').getValue(),
													  f_data_disponibilita: form.findField('f_data_disponibilita').getValue(),
													  from: '<?php echo trim($m_params->from); ?>'
										   			  }, 
										   
										   success: function(response, opts) {
										   					console.log(this.print_w);
									  						this.print_w.fireEvent('afterExecute');										   
										   					   				
							                                this.print_w.close();
							                                m_win.close();										   	
										   }
										});
					            	}
					          }	            	
						}]				            
				            						
						}

										
										
						});
						
						
						//controlli validita' spostamento (data disponib., vol/pes...)
						
						win.down('form').down('#fs-deroghe').hide();
						
						var is_valida = true;					
						var richiedi_deroga = false;
						var txt_log = '';

						if (<?php echo $min_data_valida; ?> > rec.get('DATA')){
							is_valida = false;						
							richiedi_deroga = false;
							txt_log = 'Data programmata non valida (Selezionare una ricezione con data superiore a oggi)';
							
						} else if (<?php echo $data_consegna_stardard; ?> > rec.get('DATA')){
							is_valida = true;						
							richiedi_deroga = false;
							txt_log = '';						
						}
						
						
						if (parseFloat(rec.get('VOL_DA_CARICARE')) > parseFloat(rec.get('VOL_CARICABILE')) && parseFloat(rec.get('VOL_D')) > 0 ){
							is_valida = false;		
							richiedi_deroga = false;											
							txt_log = 'Superato volume disponibile';							
						}						
						if (parseFloat(rec.get('PES_DA_CARICARE')) > parseFloat(rec.get('PES_CARICABILE')) && parseFloat(rec.get('PES_D')) > 0 ){
							is_valida = false;		
							richiedi_deroga = false;											
							txt_log = 'Superato peso disponibile';							
						}
						if (parseFloat(rec.get('PAL_DA_CARICARE')) > parseFloat(rec.get('PAL_CARICABILE')) && parseFloat(rec.get('PAL_D')) > 0 ){
							is_valida = false;	
							richiedi_deroga = false;							
							txt_log = 'Superato numero pallet disponibile';												
						}												

						win.down('form').getForm().findField('LOG').setValue(txt_log);						
									
						if (!is_valida){
							win.down('form').down('#btnConf').disable();		
							win.height = 180;						
						} else {
							txt_log = 'Confermi riprogrammazione in data ' + acs_date_from_AS(iView.getSelectionModel().getSelection()[0].get('DATA')) + '?';
							win.down('form').getForm().findField('LOG').setValue(txt_log);							
							
							if (!richiedi_deroga || 1 ==1 ){ //al momento non mostro mai le deroghe
								//spedizione valida
								win.height = 150;
								//win.down('form').getForm().findField('LOG').hide();
								
								//disabilito cosi' non ho problem con form.isValid()
								win.down('form').getForm().findField('f_autorizzazione').disable();
								win.down('form').getForm().findField('f_data').disable();
								win.down('form').getForm().findField('f_commento').disable();								
								
							} else {
								//richiedo deroga
								win.height = 320;								
								win.down('form').down('#fs-deroghe').show();														
							}
						}
						
						win.show();  				  
				  
				  }
			  }	  
			},
			
			
		viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {
		        
				<?php if ($m_params->scelta_sped_collegata != 1){ ?>		        
		        
				   if (parseFloat(record.get('VOL_DA_CARICARE')) > parseFloat(record.get('VOL_CARICABILE')) && parseFloat(record.get('VOL_D')) > 0)		           
		           		return 'segnala_riga_rosso';		        
		        
		           if ( record.get('DATA') < <?php echo $min_data_valida; ?> )		           		        
		           		return 'segnala_riga_rosso';

		           if ( record.get('DATA') == <?php echo $min_data_valida; ?> )		           		        
		           		return 'segnala_riga_giallo';
		           
		           		
		           //verifico con la data di minima assegnazione disponibile impostata sull'area di spedizione
		           if ( record.get('DATA') < <?php echo $data_consegna_stardard; ?> )		           		        
		           		return 'segnala_riga_viola';		
		           		
		         <?php } else { ?>
		         	
		         	//se sono in scelta spedizione collegata, in rosso le sped con data diversa da quella di partenza
					if ( record.get('DATA') != <?php echo $m_params->data; ?> )		         	
		           		return 'segnala_riga_rosso';
		         <?php } ?>  				           		
		           				           		
		           				           		
		         }   
		    },			
			
			
			store: {
					xtype: 'store',
					autoLoad:true,

					listeners: {
					            load: function () {	
					            var num_ord = parseFloat(this.proxy.reader.jsonData.properties.num_ordini);
					            var sum_vol = parseFloat(this.proxy.reader.jsonData.properties.sum_vol);
					            var sum_pes = parseFloat(this.proxy.reader.jsonData.properties.sum_pes);
					            var sum_pal = parseFloat(this.proxy.reader.jsonData.properties.sum_pal);					            					            
					            				              
					              m_date = this.proxy.reader.jsonData.properties.max_dispo; 		
					              if (m_date.length == 8)
									d_date = new Date(m_date.substr(0,4), m_date.substr(4,2) - 1, m_date.substr(6,2));
								  else
								    d_date = new Date();

						<?php if ($m_params->scelta_sped_collegata != 1){ ?>
								  print_w.setTitle(print_w.title + ' <?php echo $title_art; ?>');
					    <?php } ?>          					              					              
					            }
					 },
					
			
	  					proxy: {
								url: 'acs_get_select_json.php?select=get_el_spedizioni&da_oggi=N',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
			        		    	list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
			        		    	cod_iti: '<?php echo trim($m_params->cod_iti); ?>',
			        		    	cod_art: <?php echo j($cod_art); ?>,
			        		    	cod_fornitore: '<?php echo trim($m_params->cod_fornitore); ?>',
			        		    	solo_scadute: 'N',
			        		    	from: '<?php echo trim($m_params->from); ?>'			        		    	
			        				},
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root',
				        			jsonData: {"bbbbb": 333},						            
						        }
							},
							
		        			fields: [
		            			'CSPROG', 'DATA', 'VETTORE', 'MEZZO', 'CASSA', 'CSDESC'
		            			, 'carico', 'carico_d', 'vmc', 'DATA_PROG', 'ORA_PROG', 'VOL_D', 'VOL_A', 'PES_D', 'PES_A', 'PAL_D', 'PAL_A'
		            			, 'VOL_CARICABILE', 'PES_CARICABILE', 'PAL_CARICABILE'
		            			, 'VOL_DA_CARICARE', 'PES_DA_CARICARE', 'PAL_DA_CARICARE'
		            			, 'ESISTE_CLIENTE', 'OUT_FORNITORE', 'S_RDQTA', 'S_REF_ARTICOLO'
		        			]							
							
			
			}, //store
			
			
			
			        columns: [
			        
			        	
 						{
			                header   : 'CD',
							width: 30,
						    renderer: function(value, p, record){
						    	if (record.get('S_REF_ARTICOLO') > 0) return '<img src=<?php echo img_path("icone/48x48/sub_black_next.png") ?> width=16>';
						    }							
			            }
			            
			            
			            , {
			                header   : 'Data', 
			                dataIndex: 'DATA',
							renderer: function (value, metaData, record, row, col, store, gridView){						
							  				if ( (record.get('DATA') < this.store.proxy.reader.jsonData.properties.max_dispo ) ||
							  				      record.get('DATA') <= this.store.proxy.reader.jsonData.properties.area_min_dispo)
							  					 metaData.tdCls += ' grassetto';											
							  				return date_from_AS(value);			    
											}			                
			             }, {
			                header   : 'Fornitore',
			                dataIndex: 'OUT_FORNITORE',
			                flex: 90
			             }, {
			                header   : 'Q.t&agrave;',
			                dataIndex: 'S_RDQTA',
			                width: 50, align: 'right' 
			             }
			             
			             
			             , {
			                header   : 'Volume',
							dataIndex: 'VOL_A',
							width: 60, align: 'right',
							renderer: function (value, metaData, record, row, col, store, gridView){						
							  				if (parseFloat(record.get('VOL_DA_CARICARE')) > parseFloat(record.get('VOL_CARICABILE')) && parseFloat(record.get('VOL_D')) > 0)
							  					 metaData.tdCls += ' grassetto';											
							  				return value;			    
											}														
			            }
			            
			            
			            /*
			            , {
			                header   : 'Pallet',
							dataIndex: 'PAL_A',
							width: 60
			            }, {
			                header   : 'Max',
							dataIndex: 'PAL_D',
							width: 60							
			            }
			            */
			            
			            
			            , {
			                header   : 'Peso',
							dataIndex: 'PES_A',
							width: 60, align: 'right'							
			            }, {
			                header   : 'Ricezione',
			                dataIndex: 'CSPROG',
			                width: 90, align: 'right' 
			            }
			            
			            
			         ]																					
			
			
			
				  
	            
        }
    
    ]
  }          

]}