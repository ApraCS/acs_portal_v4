<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();


// ******************************************************************************************
// DATI PER GRID (X FORNITORE)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tree_fornitore'){
	$its = $main_module->get_elenco_raggruppato_itinerari($_REQUEST["node"]);
	$ars = $main_module->crea_alberatura_json($its, $field_dett, $_REQUEST["node"]);
	
	echo $ars;	
	
	exit;
}



// ******************************************************************************************
// DATI PER GRID (X DATA)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_tree_data'){
	
	$its = $main_module->get_elenco_raggruppato_itinerari_per_data($_REQUEST["node"]);
	$ars = $main_module->crea_alberatura_json_per_data($its, $field_dett, $_REQUEST["node"], 'Y');	
	echo $ars;	
	exit;
}



?>

{"success":true, "items": [
 {
  		xtype: 'treepanel',
  		id: 'itinerari_tree_data',
  				
			        title: 'Data',
			        collapsible: true,
			        useArrows: true,
			        rootVisible: false,
					store: Ext.create('Ext.data.TreeStore', {
					        model: 'Task',
					        proxy: {
					            type: 'ajax',
					            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tree_data',
					        },
					        folderSort: false
					    }),
			        multiSelect: true,
			        singleExpand: false,
			        iconCls: 'icon-calendar-16',
			        
					viewConfig: {
			            plugins: {
			                ptype: 'treeviewdragdrop',
			                dragGroup: 'firstGridDDGroup',
			                dropGroup: 'secondGridDDGroup'
			            }
			        },
			        
					listeners: {
						
					        itemclick: function(view,rec,item,index,eventObj) {            		            
				            
					            if (rec.get('leaf')){
            							mp = Ext.getCmp('m-panel');
            							mp.remove('elenco_ordini_per_data');
									
							    		//carico la form dal json ricevuto da php
							    		Ext.Ajax.request({
							    		        url        : 'acs_arrivi_json_elenco.php',
						        				method     : 'POST',
							    		        waitMsg    : 'Data loading',
							    		        jsonData: { from_type: 'INDICI', 
							    		        			data: rec.get('data'),
							    		        			cliente: rec.get('k_cliente')},
							    		        success : function(result, request){
							    		            var jsonData = Ext.decode(result.responseText);
							    		            mp.add(jsonData.items);
							    		            mp.doLayout();
							    		            mp.show();
							    		        },
							    		        failure    : function(result, request){
							    		            Ext.Msg.alert('Message', 'No data to be loaded');
							    		        }
							    		    });


            							
									  return false; //previene expand/collapse di default					            					            					        	            		            
							  }					             
						   }					             
					                      
					   },
			
			        //the 'columns' property is now 'headers'
			        columns: [{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            text: '',
			            flex: 2,
			            sortable: true,
			            dataIndex: 'task'
			        }
			        ]  
  
  
  
 }, //treepanel
 
 { //treepanel per fornitore
 		xtype: 'treepanel',
  		id: 'itinerari_tree_fornitore',
  				
			        title: 'Fornitore',
			        collapsible: true,
			        useArrows: true,
			        rootVisible: false,
					store: Ext.create('Ext.data.TreeStore', {
					        model: 'Task',
					        proxy: {
					            type: 'ajax',
					            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_tree_fornitore',
					        },
					        folderSort: false
					    }),
			        multiSelect: true,
			        singleExpand: false,
			        iconCls: 'icon-calendar-16',
			        
					viewConfig: {
			            plugins: {
			                ptype: 'treeviewdragdrop',
			                dragGroup: 'firstGridDDGroup',
			                dropGroup: 'secondGridDDGroup'
			            }
			        },
			        
					listeners: {
						
					        itemclick: function(view,rec,item,index,eventObj) {            		            
				            
					            if (rec.get('leaf')){
            							mp = Ext.getCmp('m-panel');
            							mp.remove('elenco_ordini_per_data');
									
							    		//carico la form dal json ricevuto da php
							    		Ext.Ajax.request({
							    		        url        : 'acs_arrivi_json_elenco.php',
						        				method     : 'POST',
							    		        waitMsg    : 'Data loading',
							    		        jsonData: { from_type: 'INDICI', 
							    		        			data: rec.get('data'),
							    		        			cliente: rec.get('k_cliente')},
							    		        success : function(result, request){
							    		            var jsonData = Ext.decode(result.responseText);
							    		            mp.add(jsonData.items);
							    		            mp.doLayout();
							    		            mp.show();
							    		        },
							    		        failure    : function(result, request){
							    		            Ext.Msg.alert('Message', 'No data to be loaded');
							    		        }
							    		    });


            							
									  return false; //previene expand/collapse di default					            					            					        	            		            
							  }					             
						   }					             
					                      
					   },
			
			        //the 'columns' property is now 'headers'
			        columns: [{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            text: '',
			            flex: 2,
			            sortable: true,
			            dataIndex: 'task'
			        }
			        ]  
  
  	} 
 
 
 ] //items
 
}