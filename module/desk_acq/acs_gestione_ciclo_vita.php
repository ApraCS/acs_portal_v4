<?php

require_once "../../config.inc.php";
require_once("acs_panel_ins_new_anag.php");

$s = new Spedizioni(array('no_verify' => 'Y'));
$m_DeskAcq = new DeskAcq();

if ($_REQUEST['fn'] == 'exe_canc_detail'){
    
    $m_params = acs_m_params_json_decode();
 
    
    $sql = "DELETE FROM {$cfg_mod_DeskAcq['file_tabelle']} TA WHERE RRN(TA) = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->RRN));
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_cambia_ciclo_vita'){

	$m_params = acs_m_params_json_decode();
    $ret = array();
    
    $ar_ins = array();
    $ar_ins['GCCIVI'] = $m_params->form_values->f_ciclo;
    $sql = "UPDATE {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']}
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			WHERE GCPROG = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array_merge(
        $ar_ins,
        array($m_params->prog)
        ));
    echo db2_stmt_errormsg($stmt);
    
    $sh = new SpedHistory($m_DeskAcq);
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_ANAG_CLI',
            "vals" => array(
                "RIPROG" => $m_params->prog,
                "RINOTE" => 'save_anag'
            )
        )
        );
    
    $record = get_gcrow_by_prog($m_params->prog);
    
    $sh = new SpedHistory($m_DeskAcq);
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_CICLO_VITA',
            "vals" => array(
                "RICVES" => sprintf("%09d", $record['GCCDCF']),
                "RICITI"    => $m_params->form_values->f_ciclo,
            )
        )
        );
  
	$ret['success'] = true;
	$ret['ret_RI'] = $ret_RI;
    echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci_stato_from'){

	$m_params = acs_m_params_json_decode();

	$codice= $m_params->form_values->f_codice;

	$descr= $m_params->form_values->f_descr;

	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'CHCVF';
	$ar_ins['TAKEY1'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();


	$sql = "INSERT INTO {$cfg_mod_DeskAcq['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_inserisci_stato_to'){

	$m_params = acs_m_params_json_decode();

	$codice= $m_params->form_values->f_codice;

	$descr= $m_params->form_values->f_descr;

	$stato_from= $m_params->statof;

	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'CHCVT';
	$ar_ins['TAKEY1'] 	= $stato_from;
	$ar_ins['TAKEY2'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();

	$sql = "INSERT INTO {$cfg_mod_DeskAcq['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_master'){

	$m_params = acs_m_params_json_decode();

	$mod_rows= $m_params->mod_rows;

	foreach ($mod_rows as $v){

		if(isset($v)){
				
			$ar_ins = array();

			$ar_ins['TAKEY1'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TAFG01'] 	= trim($v->TAFG01);
			$ar_ins['TAFG02'] 	= trim($v->TAFG02);
			$ar_ins['TAFG03'] 	= trim($v->TAFG03);
			$ar_ins['TAFG04'] 	= trim($v->TAFG04);
				
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$cfg_mod_DeskAcq['file_tabelle']} TA
			SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			WHERE RRN(TA) = '$v->rrn' ";
				
				

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
				
			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);
				
			//exit;

		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_detail'){

	$m_params = acs_m_params_json_decode();
	$mod_rows= $m_params->mod_rows;

	foreach ($mod_rows as $v){

		if(isset($v)){
				
			$ar_ins = array();
				
			$ar_ins['TAKEY2'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$cfg_mod_DeskAcq['file_tabelle']} TA
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE RRN(TA) = '$v->rrn' ";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);

			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);

			//exit;

		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'get_data_grid'){


	$sql = "SELECT RRN (TA) AS RRN, TADESC, TAKEY1
	        FROM {$cfg_mod_DeskAcq['file_tabelle']} TA
        	WHERE TATAID='CHCVF' AND TADT='{$id_ditta_default}'";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['codice'] = acs_u8e(trim($row['TAKEY1']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));


		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}


if ($_REQUEST['fn'] == 'get_data_grid_detail'){

	$m_params = acs_m_params_json_decode();
	$stato_from= trim($m_params->statof);

	$sql = "SELECT RRN(TA) AS RRN, TADESC, TAKEY2
	        FROM {$cfg_mod_DeskAcq['file_tabelle']} TA
	        WHERE TADT='$id_ditta_default' AND TATAID='CHCVT' AND TAKEY1='{$stato_from}'";


	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['codice'] = acs_u8e(trim($row['TAKEY2']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));
		

		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}




if ($_REQUEST['fn'] == 'open_grid'){

	?>

				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'Gestione ciclo vita',
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],

					store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'codice', 'descr', 'rrn'
		        			]
		    			},
		    		
			        columns: [
			       				        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             }
			             
			            
			         ],  
					
						listeners: {
						
						
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
				
			           voci_menu.push({
				         		text: 'Ciclo di vita consentito',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Ciclo di vita consentito', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_detail', {statof: rec.get('codice')}, 900, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}
					
					}, 
					
					 viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
					            console.log(m_grid);
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_stato_from',
											        method     : 'POST',
								        			jsonData: {
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												      m_grid.getStore().load();
												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
					   ]
					}, '->' ,
			     
			     {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			                console.log(list_rows_modified);
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_master',
									        method     : 'POST',
						        			jsonData: {
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php 

exit; 

}

if ($_REQUEST['fn'] == 'open_detail'){
	
	$m_params = acs_m_params_json_decode();
	$stato_from= $m_params->statof;

?>

{"success":true, "items": [

        {
        
        xtype: 'grid',
        multiSelect: true,
        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
			store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid_detail',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										
										statof: '<?php echo $stato_from; ?>'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'codice', 'descr', 'prog', 'rrn'
		            					        			]
		    			},
		    		
			        columns: [
									        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			           
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }
 						
			         ],  
					
		listeners: {
						
						
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
				
			           voci_menu.push({
				         		text: 'Cancella',
				        		iconCls : 'icon-sub_red_delete-16',      		
				        		handler: function() {
				        			Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_detail',
											        method     : 'POST',
								        			jsonData: {
														RRN: rec.get('rrn'),
													},							        
											        success : function(response, opts){											       
												     	grid.getStore().load();												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}
					
					},
					 viewConfig: {
			         getRowClass: function(record, index) {
			         return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_stato_to',
											        method     : 'POST',
								        			jsonData: {
								        				statof: '<?php echo $stato_from ?>',
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												         m_grid.getStore().load();
								            		  
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
			     
			  
					   ]
					},  '->',
					   {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_detail',
									        method     : 'POST',
						        			jsonData: {
						        			    statof: '<?php echo $stato_from ?>',
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }
					
					
					
					
					]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php

exit;

}

if ($_REQUEST['fn'] == 'change_ciclo'){

	$m_params = acs_m_params_json_decode();
	$value = get_ciclo_consentito($m_params->prog);
	?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: ['->'
			         
			         <?php if(count($value) > 0){?>
			         
			         , {
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var loc_win = this.up('window');
			            	
			          			if (!form.isValid()) return false;
							
								Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_ciclo_vita',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    prog: <?php echo j($m_params->prog) ?>,
 			        			    old_stato : '<?php echo $stato; ?>' ,
 			        			    form_values: form.getValues()
 								},							        
 						        success : function(result, request){
 						        	var jsonData = Ext.decode(result.responseText);
 						        	loc_win.fireEvent('afterAssegna', loc_win, form.getValues().f_ciclo);
		 			             },
		 						        failure    : function(result, request){
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });	
		 			
		
			
			            }
			         }
					 <?php }?>
						
						], items: [
						
					
						{
							name: 'f_ciclo',
							xtype: 'combo',
							fieldLabel: 'Nuovo ciclo vita',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
	                                  <?php echo acs_ar_to_select_json($value, '', true, 'N', 'N', 'Y') ?>
								    ] 
							}
							
							
							
							, listeners: {
        					
    						}
							
												 
						}
									 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit; 

	
	
}


function get_ciclo_consentito($prog){
	
    global $cfg_mod_DeskAcq, $id_ditta_default, $conn;
	
	global $auth;
           
        //in base alle regole di cambio stato
	    $row =  get_gcrow_by_prog($prog);
        $ciclo = $row['GCCIVI'];
        $ret = array();
        
        $sql = "SELECT TAKEY2, TADESC FROM {$cfg_mod_DeskAcq['file_tabelle']} TA
                WHERE TADT = '{$id_ditta_default}' AND TATAID = 'CHCVT' AND TAKEY1='{$ciclo}'";
    
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg();
	
	while ($row = db2_fetch_assoc($stmt)) {
		$r = array();
			
		$r['id'] 		= $row['TAKEY2'];
		$r['text']		= acs_u8e(trim($row['TADESC']));
		
		$ret[] = $r;
		
	}
	
	return $ret;
}