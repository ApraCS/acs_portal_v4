<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'exe_avanzamento_segnalazione_arrivi'){
    
    $result = $main_module->exe_avanzamento_segnalazione_arrivi($_REQUEST);
    echo $result;
    exit();
}




$initial_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario
$m_year = strftime("%G", strtotime($initial_data_txt));

$oggi = oggi_AS_date();

//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

//elenco possibili utenti destinatari
$user_to = array();
$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");

$ar_users_json = acs_je($user_to);

//recupero l'elenco degli ordini interessati
$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

//attualmente e' selezione singola
$row_selected = $m_params->list_selected_id[0];

//recupero causale/utente/ordine da usare come chiave per AS0
$m_k_exp = explode("|", $row_selected->id);
 

if (strlen($m_params->causale) > 0)
 $m_causale = $m_params->causale;
else
 $m_causale		= $m_k_exp[0];

if ($m_causale == 'SBLOC')
	$m_utente		= $m_k_exp[1];
else
	$m_utente		= $m_k_exp[2];
		
	
$m_documento	= $row_selected->k_ordine;
$entry_prog		= $row_selected->prog;



	
	//elenco ordini che passo ad un eventuale create_entry
	foreach ($m_params->list_selected_id as $list_selected_ord){
		$ar_documenti[] = $list_selected_ord->k_ordine;
		
	}


 


$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_assegna_ord']}
		 WHERE ASFLRI <> 'Y' /* aperta */
		   AND ASCAAS = ? AND ASUSAT = ? AND ASDOCU = ?";

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, array($m_causale, $m_utente, $m_documento));

$row = db2_fetch_assoc($stmt);


//causali di rilasio
$causali_rilascio = $main_module->find_TA_std('RILAV', $m_causale, 'N', 'Y');


//escludo eventualmente alcune causali di rilascio in base all'utente e ai permessi
foreach ($causali_rilascio as $key => $value){
	
	//In base agli stati dei documenti, eventualmente disabilito alcune risposte
	if (strlen(trim($value['TAMAIL'])) > 0){
		$ar_abilita_su_stati = explode('|', trim($value['TAMAIL']));
		foreach($ar_documenti as $ord_to_test){
			$row_ord_to_test = $s->get_ordine_by_k_docu($ord_to_test);
			if (!in_array($row_ord_to_test['TDSTAT'], $ar_abilita_su_stati))
				unset($causali_rilascio[$key]);
		}
	}

}




?>



{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'exe_avanzamento_segnalazione_arrivi'
                	}, {
	                	xtype: 'hidden',
	                	name: 'list_selected_id',
	                	value: '<?php echo $list_selected_id_encoded; ?>'
                	}, {
                		xtype: 'hidden',
	                	name: 'f_ril',
                		value : <?php echo j($row_selected->f_ril); ?>
                	}, {
						name: 'f_entry_prog_causale',
						itemId: 'f_entry_prog_causale',
						xtype: 'combo',
						fieldLabel: 'Causale di rilascio',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: false,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}, 'TARIF2', 'TAFG01', 'TAFG02'],
						    data: [								    
							     <?php echo acs_ar_to_select_json($causali_rilascio, ""); ?>	
							    ],
						},
						
				        listeners: {
				            change: function(field,newVal, a, b, c) {	            	

				            	var form = this.up('form').getForm();				            	
								var m_rec = field.findRecordByValue(newVal);								
								
								if (m_rec.get('TAFG02') == 'Y'){ //note obbligatorie
									form.findField('f_entry_prog_note').allowBlank = false;
								}
								else {
									form.findField('f_entry_prog_note').allowBlank = true;
								}
				            }
				        },						
						
												 
					}, {
						name: 'f_entry_prog_note',
						itemId: 'f_entry_prog_note',
						xtype: 'textfield',
						fieldLabel: 'Note',
					    maxLength: 100							
					},
				
					],
			buttons: [{
	            text: 'Salva',
	            itemId: 'b_salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')	            	

					if(form.isValid()){					
					
							var m_f = form.findField('f_entry_prog_causale');
							var raw_causale_selected = m_f.findRecordByValue(m_f.getValue());
							
							var causale_successiva = raw_causale_selected.get('TARIF2');
							

							
							if (causale_successiva.trim().length > 0) {
							//se richiesto apro la schermata per la segnalazione successiva
							
									var mw = new Ext.Window({
									  width: 800
									, height: 380
									, minWidth: 300
									, minHeight: 300
									, plain: true
									, title: 'Apertura attivit&agrave; collegata'
									, iconCls: 'iconArtCritici'			
									, layout: 'fit'
									, border: true
									, closable: true
									, id_selected: window.id_selected								
									});				    			
					    			mw.show();							
							
										Ext.Ajax.request({
										        url        : '../desk_acq/acs_form_json_create_entry.php',
										        jsonData: {	tipo_op: causale_successiva.trim(),
										        			entry_prog: <?php echo $entry_prog; ?>,
										        			entry_prog_causale: window.down('#f_entry_prog_causale').getValue(),
										        			entry_prog_note: window.down('#f_entry_prog_note').getValue(),
										        			list_selected_id_old: <?php echo acs_je($ar_documenti)?>,
										        			list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>,
										        			},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										            var jsonData = Ext.decode(result.responseText);
										            mw.add(jsonData.items);
										            mw.doLayout();
										            
										            window.close();
										            				            
										        },
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });
					
									} else { 
										//non e' richiesta la cusale successiva....salvo e chiudo
											
										Ext.getBody().mask('Loading... ', 'loading').show();											
												            	
						                form.submit({
					                            //waitMsg:'Loading...',
					                            success: function(form,action) {					                            
													Ext.getBody().unmask();
													
													//se � un OENTRY devo chiamare APRATOMETRON.EXE
													<?php if ($m_params->auto_set_causale == 'OENTRYyyyyyyy'){ ?>
														///allegatiPopup('../base/call_metron.php?k_ordine=<?php echo $m_params->list_selected_id[0]->k_ordine; ?>');
														<?php $oe   = $s->k_ordine_td_decode_xx($m_params->list_selected_id[0]->k_ordine); ?>
											    	  	var appId = "knldjmfmopnpolahpmmgbagdohdnhkik";
											    	  	chrome.runtime.sendMessage(appId,{cmd: "mkdir <?php echo trim($oe['TDONDO']); ?>"}, function(response) {
												    	    console.log(response);
											    	  	});											    	  	    	  
											    	  	//chrome.runtime.sendMessage(appId,{cmd: "c:\\tecne\\metron4\\APRATOMETRON.EXE <?php echo trim($oe['TDONDO']); ?>"}, function(response) {
												    	//    console.log(response);
											    	  	//});													
														
													<?php } ?>

																		                            
				                    				window.close();        	
					                            },
					                            failure: function(form,action){
					                            	Ext.getBody().unmask();
					                                Ext.MessageBox.alert('Errore', 'Errore imprevisto');		
					                            }
					                        });
				            		}
				    }            	                	                
	            }
	        }],             
			
			
			listeners: {
		        afterrender: function(){
		     	   f = this.getForm().findField('f_entry_prog_causale');
		     	   
		     	   //se imposto direttamente la causale di avanzamento
		     	   <?php if (strlen($m_params->auto_set_causale) > 0) { ?>
		     	    f.setValue(<?php echo j($m_params->auto_set_causale); ?>);
		     	    b_salva = this.down('#b_salva'); //ToDo: come essere sicuri che ci sia solo "Salva"?
		     	    b_salva.fireHandler();
		     	    return;
		     	   <?php } ?>		     	   

		     	   <?php if (count($causali_rilascio) == 1 && strlen(trim($causali_rilascio[0]['TARIF2'])) > 0){ ?>
		     	    f.setValue(<?php echo j(trim($causali_rilascio[0]['id'])); ?>);
		     	    b_salva = this.down('button'); //ToDo: come essere sicuri che ci sia solo "Salva"?
		     	    b_salva.fireHandler();
		     	    //this.up('window').close(); //va in errore     	    
		     	   <?php } ?>
		     	   
		        }
		       
		       
		    }			
			
				
				
        }
]}