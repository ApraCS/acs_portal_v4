<?php

require_once "../../config.inc.php";
require_once "acs_wizard_MTS_include.php";

$main_module = new DeskAcq();

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

$m_where = get_sql_where($form_values);

if(strlen($_REQUEST['order_by']) >0)
    $order_by = $_REQUEST['order_by'];
else 
    $order_by = "M2ART";
    
$sql = "SELECT M2CDOL, M2DVN1, M2DVN2, M2DVN3, M2DVN4, M2DVN5, M2QDOR, M2FLOR, M2QMAN, M2LOTT, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
        W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2RGS1, W2.M2DTEO, W2.M2ART, W2.M2PROG, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM, W2.M2MAGA
        FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
        WHERE 1=1 {$m_where}
        GROUP BY
        M2CDOL, M2DVN1, M2DVN2, M2DVN3, M2DVN4, M2DVN5, M2QDOR, M2FLOR, M2QMAN, M2LOTT, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2QFAB, W2.M2PRZ,
        W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2RGS1, W2.M2DTEO, W2.M2ART, W2.M2PROG, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2SWTR, W2.M2UM, W2.M2MAGA
        ORDER BY $order_by";


$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);
?>
<html>
<head>
<meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<div id='my_content'>
  		<div class=header_page>
			<H2>Wizard MTS</H2>
 		</div>
		<div style="text-align: right; margin-bottom:10px; "> Data elaborazione: <?php echo Date('d/m/Y H:i') ?></div>

<table class=int1>
	<tr class='liv_data' >
		<th>Codice</th>
		<th>Articolo</th>
		<th>Altro codice</th>
		<th>Magazzino</th>
		<th>Prog</th>
		<th>Varianti</th>
		<th>UM</th>
		<th>Giacenza</th>
		<th>&nbsp;</th>
	</tr>
	
	<?php while ($r = db2_fetch_assoc($stmt)) {
	    
	  //VARIANTI
	    if(trim($r['M2DVN1'])!='') $r['VARIANTI'] = acs_u8e($r['M2DVN1']);	    
	    if(trim($r['M2DVN2'])!='') $r['VARIANTI'].= "<br>".acs_u8e($r['M2DVN2']);	    
	    if(trim($r['M2DVN3'])!='') $r['VARIANTI'].= "<br>".acs_u8e($r['M2DVN3']);	   
	    if(trim($r['M2DVN4'])!='') $r['VARIANTI'].= "<br>".acs_u8e($r['M2DVN4']);	    
	    if(trim($r['M2DVN5'])!='') $r['VARIANTI'].= "<br>".acs_u8e($r['M2DVN5']);	    
	
	?>	
	<tr class='liv_data' >
		<td><?php echo trim($r['M2ART'])?></td>
		<td><?php echo trim($r['M2DART'])?></td>
		<td><?php echo trim($r['M2CDOL'])?></td>
		<td><?php echo trim($r['M2MAGA'])?></td>
		<td align=right><?php echo trim($r['M2PROG'])?></td>
		<td><?php echo trim($r['VARIANTI'])?></td>
		<td><?php echo trim($r['M2UM'])?></td>
		<td align=right><?php echo n($r['M2QGIA'])?></td>
		<td align=right>&nbsp;</td>
	</tr>
	
	<?php } ?>
	
</table>
</div>
</body>
</html>


