<?php

require_once("../../config.inc.php");
require_once("acs_panel_background_mrp.inc.php");

$main_module = new DeskAcq();



/*************************************************************************
 * GENERAZIONE ORDINI MTO
*************************************************************************/
if ($_REQUEST['fn'] == 'exe_generazione_ordini_MTO'){
	
	/* 
	  Viene passato l'elenco delle chiavi fabbisogno selezionate
	  1) In base a queste recupero le accoppiate "Fornitore | Data"
	  2) Per ogni "Fornitore | data"
	   2.1) Imposto a "G" il record FLM15 per le relative chiavi di fabbisogno
	   2.2) Scrivo il relativo record nel file RI0 per lanciare la generazione dell'ordine
	 */
	$m_params = acs_m_params_json_decode();
	$list_selected_id = $m_params->list_selected_id; //elenco chiavi di fabbisogno
	
	//Creo tabella temporanea in cui salvare le chiavi di fabbisogno
	$file_tmp = "SESSION.gen_ord_mto_" . session_id();

	//todo: a che livello di sessione � presente?
	$sql = "DROP TABLE {$file_tmp}";	

	$old_display_errors = ini_get('display_errors');
	ini_set('display_errors', '0');
	
	try {	
		if (FALSE === ($stmt = db2_prepare($conn, $sql))){
        	throw new Exception('Tabella non presente??????');
    	}; 		
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		echo db2_stmt_errormsg($stmt);		
	} catch (Exception $e){
		////echo $e->getMessage();
	}
	
	ini_set('display_errors', $old_display_errors);
	
	
	//Creazione tabella temporanea
	$sql = "
		DECLARE GLOBAL TEMPORARY TABLE {$file_tmp} (
			tmp_id 		INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
			tmp_ses 	VARCHAR(32),
			tmp_mtdt  	VARCHAR(2),
			tmp_mttpsv	VARCHAR(2),
			tmp_mttpco	VARCHAR(2),
			tmp_mtaaco	INT,
			tmp_mtnrco	INT,
			tmp_mtrisv	INT
		/* , PRIMARY KEY(tmp_id) SEMBRA CHE NON PRENDE PRIMARY KEY */
		) not logged";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);


	//Nella tabella temporanea aggiungo tutte le chiave fabbisogno passate
	$sql = "INSERT INTO {$file_tmp}(tmp_mtdt, tmp_mttpsv, tmp_mttpco, tmp_mtaaco, tmp_mtnrco, tmp_mtrisv)
			 VALUES(?, ?, ?, ?, ?, ?)";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	
	foreach($list_selected_id as $kf){
		$kf_exp = explode("|", $kf);

		$result = db2_execute($stmt, $kf_exp);
		echo db2_stmt_errormsg($stmt);		
	}
	
		
	$sql = "SELECT MTFORN, MTDTCC,
				MTDT, MTTPSV, MTTPCO, MTAACO, MTNRCO, MTRISV
				FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MTO
				INNER JOIN {$file_tmp}
				ON MTDT = tmp_mtdt and MTTPSV = tmp_mttpsv AND MTTPCO = tmp_mttpco AND MTAACO = tmp_mtaaco AND MTNRCO = tmp_mtnrco AND MTRISV = tmp_mtrisv
			";
	
	$add_query = " WHERE MTDT = '{$id_ditta_default}' AND MTAVAN = 'A' AND MTSTAR ='' AND MTFLM14 <> 'E' AND MTFLM15 NOT IN('P', '')";
	$sql .= $add_query;

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg($stmt);

/*	
	//Verifico se il num di record ritornato corrisponde al num di chiavi passate
	if (db2_num_rows($stmt) != count($list_selected_id)){
		echo "\nNum record: " . db2_num_rows($stmt);
		echo "\nCount: " . count($list_selected_id);
		echo acs_je(array('success' => false, 'error_msg' => 'Num. record ritornati non corrispondenti'));
		return false;
	}
*/				

	$liv0_inline = null;
	//update con "G" in base alla chiave fabbisogno
	$sql = "UPDATE {$cfg_mod_DeskAcq['file_proposte_MTO']}
			SET MTFLM15 = 'G'
			{$add_query}
			AND MTDT = ? AND MTTPSV = ? AND MTTPCO = ? AND MTAACO = ? AND MTNRCO = ? AND MTRISV = ?";
	$stmt_upd = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();			
				
	while ($r = db2_fetch_assoc($stmt)) {
		$liv0_v = implode("_", array($r['MTDT'], $r['MTFORN'], $r['MTDTCC']));
		
		$result = db2_execute($stmt_upd, array($r['MTDT'], $r['MTTPSV'], $r['MTTPCO'], $r['MTAACO'], $r['MTNRCO'], $r['MTRISV']));
		echo db2_stmt_errormsg($stmt_upd);		
		
		if ($liv0_v != $liv0_inline && !is_null($liv0_inline)){
			//scrivo RI per il fornitore/data inline
			//HISTORY
			$forn_data_exp = explode("_", $liv0_inline);
			$sh = new SpedHistory($main_module);
			$sh->crea(
					'dora_gen_ord_mto',
					array(
							"ditta"		=> $forn_data_exp[0],
							"fornitore" => $forn_data_exp[1],
							"data_cons"	=> $forn_data_exp[2]
					)
			);			
		}
		
	  $liv0_inline = $liv0_v;	
	}
	
	//scrivo l'ultimo se presente
	if (!is_null($liv0_inline)){
		//scrivo RI per il fornitore/data inline
		//HISTORY
		$forn_data_exp = explode("_", $liv0_inline);
		$sh = new SpedHistory($main_module);
		$sh->crea(
				'dora_gen_ord_mto',
				array(
						"ditta"		=> $forn_data_exp[0],
						"fornitore" => $forn_data_exp[1],
						"data_cons"	=> $forn_data_exp[2]
				)
		);
	}	
	 
	
	echo acs_je(array('success' => true));
	
	exit;
}


/*************************************************************************
 * GET JSON DATA
 *************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data'){
	global $id_ditta_default, $cfg_mod_Spedizioni;
	$m_params = (array)acs_m_params_json_decode();
	
	if ($_REQUEST['my_opzione'] == 'provvisori'){
		$m_node = '';
		$flt_query = " AND MTFLM15 IN('P', '') ";
		$order_query = " TDSTAT, TDDTEP, MTAADO, MTNRDO, MTART ";
		$mostra_checkbox = 'N';		
	} else {
		$m_node = $_REQUEST['node'];
		$flt_query = " AND MTFLM15 NOT IN('P', '') ";
		$order_query = " MTDTIM, MTDTCC, TDDTSP, TDDTEP, MTAADO, MTNRDO, MTART";
		$mostra_checkbox = 'S';
	}
	
	$sql = "SELECT 
			 MTDTIM, MTTPSV, MTTPCO, MTAACO, MTNRCO, MTRISV,	
			 MTFLM14, MTFLM15, 
			 MTO.MTDT AS M2DT, MTDTCC, MTVAR1, MTVAR2, MTVAR3, MTVARA1, MTVARA2, MTVARA3,
			 GTD.TDTPDO, GTD.TDSTAT, GTD.TDDTEP, GTD.TDPRIO, GTD.TDDTVA,			
			 TD.TDDTSP, TD.TDOPUN, TD.TDFN07, TD.TDFN08, TD.TDSWSP, TD.TDDCON,
			 ANAG_ART.ARDIM1 AS DIM1, ANAG_ART.ARDIM2 AS DIM2, ANAG_ART.ARDIM3 AS DIM3, ANAG_ART.ARSOSP, ANAG_ART.ARESAU,
			 1 AS M2SELE, 1 AS M2FOR1,
			 MTART AS M2ART, MTDART AS M2DART,
			 MTUMCO AS M3UM, MTQTCO AS S_M3QTA,
			 MTAADO AS M3AADO, MTNRDO AS M3NRDO
			 FROM {$cfg_mod_DeskAcq['file_proposte_MTO']} MTO
 	         LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_art']} ANAG_ART
 	          ON MTO.MTDT = ANAG_ART.ARDT AND MTO.MTART = ANAG_ART.ARART
 	         LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_doc_gest']} GTD
 	          ON MTO.MTDT = GTD.TDDT AND MTO.MTTIDO = GTD.TDTIDO AND MTO.MTINUM = GTD.TDINUM AND MTAADO = GTD.TDAADO AND MTNRDO = GTD.TDNRDO 	          			 
 	         LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
 	          ON TD.TDSWPP = 'Y' AND MTO.MTDT = TD.TDDT AND MTO.MTTIDO = TD.TDOTID AND MTO.MTINUM = TD.TDOINU AND MTAADO = TD.TDOADO AND DIGITS(MTNRDO) = TD.TDONDO 	          
			 WHERE MTDT = '{$id_ditta_default}' AND MTAVAN = 'A' AND MTSTAR ='' AND MTFLM14 <> 'E' {$flt_query}
			   AND MTFORN = ?
			 ORDER BY {$order_query} 
			";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params['k_cliente']));
	echo db2_stmt_errormsg($stmt);	
	
	if ($_REQUEST['my_opzione'] == 'provvisori')
		$expandAll = 'N';
	else 
		$expandAll = 'Y';
	
	if ($_REQUEST['my_opzione'] == 'provvisori')	
		$ar_ret = crea_array_tree_std($stmt, $m_node, array("TDSTAT" =>"Stato ordine"), array(), $expandAll, 'proposte_MTO', 'N', 'N');
	else 
		$ar_ret = crea_array_tree_std($stmt, $m_node, array("MTDTIM" => "Emissione richiesta", "MTDTCC"=> "Consegna richiesta"), array("MTDTIM"), $expandAll, 'proposte_MTO');	
	
	$ar_ret = $ar_ret[0]['children'][0];

	echo db2_stmt_errormsg();		
/*	
	//Aggiungo il raggruppamento per quelli "sospesi"
	if ($_REQUEST['node'] == 'root')
		$ar_ret['children'][] = array(
					'id' 		=> '|liv1;ALTRI',
					'liv_cod'	=> 'PROVVISORI',
					'task'		=> 'Richieste ordini provvisori'						
		);
*/		
	
	
	echo acs_je($ar_ret);

    exit();
}



/***********************************************************************************
 * TREE json
 ***********************************************************************************/
 $m_params = acs_m_params_json_decode();
 $m_params_ar = (array)$m_params;
 
 if ($m_params->my_opzione == 'provvisori'){
 	$add_title = ' Hold ';
 	$grid_title = 'Richiesta MTO provvisiorie: ';
 	$ss = "Stato ordine di vendita / Descrizione articolo"; 	
 }
 else {
 	$add_title = '';
 	$grid_title = 'Richiesta emissione ordini MTO: ';
 	$ss = "Data emissione limite / Descrizione articolo";
 }	
 
?>

{"success":true, "items":
 {
		xtype: 'treepanel',
		// selType: 'cellmodel',
		cls: 'tree-calendario',		
        useArrows: true,
        rootVisible: false,
        da_data: 0,
        title: <?php echo j("Richieste MTO " . trim($m_params->k_cliente) . "{$add_title}") ?>,
        closable: true,
        
        
        dockedItems: [{
            xtype: 'toolbar'
            , dock: 'top'
            , items: [{xtype: 'label', cls: 'strong', text: <?php echo j($grid_title . $m_params->task)   ; ?>}, {xtype: 'tbfill'}
            
				, {
                     xtype: 'button',
                     text: '',
			            handler: function() {
			         	}
			       }             

            
 	<?php if ($m_params->my_opzione != 'provvisori'){ ?>
	            , {
                     xtype: 'button',
	            	 //scale: 'medium',
	            	 iconCls: 'icon-shopping_cart_green-16',
                     text: 'Conferma generazione ordini MTO',
			            handler: function() {
			            
			            	list_selected_id = [];
			            	grid = this.up('treepanel');
							id_selected = grid.getChecked();
							
							Ext.each(id_selected, function(n) {
								list_selected_id.push(n.get('liv_cod'));	
							});
							
							if (list_selected_id.length == 0){
								acs_show_msg_error('Spuntare almeno una riga per cui generare gli ordini MTO');
								return false;
							}
							
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Righe selezionate: ' + list_selected_id.length + '.<br>Confermi la generazione degli ordini MTO?', function(btn){
							   if(btn === 'yes'){
									Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini_MTO',
							            method: 'POST',
					        			jsonData: {list_selected_id: list_selected_id},						            
							            success: function ( result, request) {
							                /////var jsonData = Ext.decode(result.responseText);
							                grid.store.load()
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });							
							
										            
			         	}
			       }  	
 	
 	<?php } ?>            
            
 	<?php if ($m_params->my_opzione != 'provvisori'){ ?>            
            
	            , {
                     xtype: 'button',
	            	 //scale: 'medium',
	            	 iconCls: 'icon-button_grey_play-16',
                     text: 'Richieste MTO provvisorie',
			            handler: function() {

							mp = Ext.getCmp('m-panel');
							
					    		//carico la form dal json ricevuto da php
					    		Ext.Ajax.request({
					    		        url        : 'acs_json_proposte_MTO_for.php',
				        				method     : 'POST',
					    		        waitMsg    : 'Data loading',
					    		        jsonData:   <?php $m = $m_params; $m->my_opzione = 'provvisori'; echo acs_je($m_params); ?>,
					    		        success : function(result, request){
					    		            var jsonData = Ext.decode(result.responseText);
					    		            mp.add(jsonData.items);
					    		            mp.doLayout();
					    		            mp.show();
					    		            
					    		            //mostro l'ultimo tab
											mp.setActiveTab(mp.items.length -1)
					    		            
					    		        },
					    		        failure    : function(result, request){
					    		            Ext.Msg.alert('Message', 'No data to be loaded');
					    		        }
					    		    });
			            
			            
			         }
			  }
			  
			<?php } ?>  
			  
			  
			  ]
        }],        
        
        
		store: Ext.create('Ext.data.TreeStore', {
	        fields: ['task', 'liv', 'liv_cod', 'k_ordine', 'itinerario', 'dt', 'cod_art', 'um', 'qta', 
	        		 'in_esaurimento', 'tipo_approvvigionamento', 'dim1', 'dim2', 'dim3', 'gr_pianificazione', 'gr_pianificazione_desc', 'art_sosp', 'art_esau',
	        		 'M3NRDO', 'M3AADO', 'M3TPDO', 'M3DT', 'M3TIDO', 'M3INUM', 'forn_TAFG01', 'forn_TAFG02', 'M3FU01', 'M3FU02',
					 'TDDTSP', 'tp_pri', 'TDSWSP', 'TDDCON',
	        		 'TDTPDO', 'TDSTAT', 'TDDTEP', 'TDDTVA', 'TDPRIO'],
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				
				extraParams: <?php echo acs_raw_post_data() ?>,
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				doRequest: personalizza_extraParams_to_jsonData,
					        
				actionMethods: {
					read: 'POST'
				},

				reader: {
					type: 'json',
				},				
				
	        },
	        
	        reader: new Ext.data.JsonReader(),
	        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
		
		
		fn_select_deselect: function(id_selected, status_to_set){
	  		for (var i=0; i<id_selected.length; i++){
	  			r = id_selected[i];
	  			if (r.get('liv') == 'liv_4') id_selected[i].set('checked', status_to_set);
	  			if (r.get('liv') == 'liv_3'){
	  				//seleziono tutte le sottorighe
	  				r.eachChild(function(n) {
	  					n.set('checked', status_to_set);
	  				});
	  			} 
	  		}				
		},
		
		listeners: {
              		load: function(sender, node, records) {              		
                      	Ext.getBody().unmask();												
              		}
              		
              		
			      , celldblclick: {										      		
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
						  	rec = iView.getRecord(iRowEl);
						  	
							if (rec.get('liv')=='liv_4'){
								iEvent.preventDefault();																	
							  	acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('dt'), rdart: rec.get('cod_art')}, 1200, 600, null, 'icon-shopping_cart_gray-16');								
								
								return false;
							}			  	
						}
			    	}
			    	
			    	
					, itemcontextmenu : function(grid, rec, node, index, event) {
						  				  event.stopEvent();
									      var record = grid.getStore().getAt(index);		  
									      var voci_menu = [];
									      
										      voci_menu.push({
									      		text: 'Seleziona',
									    		iconCls : 'icon-folder_accept-16',      		
									    		handler: function() {
									    		
											  		id_selected = grid.getSelectionModel().getSelection();
									    			grid.panel.fn_select_deselect(id_selected, true);									    		
									    		}
											  });									      
											  
										      voci_menu.push({
									      		text: 'Deseleziona',
									    		iconCls : 'icon-sub_red_delete-16',      		
									    		handler: function() {
									    			id_selected = grid.getSelectionModel().getSelection();
									    			grid.panel.fn_select_deselect(id_selected, false);
									    		}
											  });
											  
						      var menu = new Ext.menu.Menu({
						            items: voci_menu
							}).showAt(event.xy);											  											  
			    	}
			    	              		
              		
              		
    },    		
		viewConfig: {
		        //row class CALENDARIO
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
		            return v;					            
		         	}   
		    },			



        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 10,
            dataIndex: 'task',
            text: '<?php echo $ss; ?>'
        },{
		   text: 'Articolo',
		   width: 80,
		   dataIndex: 'cod_art',	    
   			    renderer: function(value, p, record){
		    		if (record.get('liv') == 'liv_1' || record.get('liv') == 'liv_2' || record.get('liv') == 'liv_3')
		    			return '[ ' + value + ' ]';
		    		else return value;				    	
   			    }
		},{
		   text: 'L',
		   width: 40,
		   dataIndex: 'dim1', align: 'right', renderer: floatRenderer0,
		},{
		   text: 'H',
		   width: 40,
		   dataIndex: 'dim2', align: 'right', renderer: floatRenderer0,
		},{
		   text: 'P',
		   width: 40,
		   dataIndex: 'dim3', align: 'right', renderer: floatRenderer0,
		}, {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=20>',	width: 30, tdCls: 'tdAction',		 
   	    		dataIndex: 'art_sosp', tooltip: 'Articoli sospesi',   	    			    
   			    renderer: function(value, p, record){
		    		if (record.get('art_sosp') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';				    	
   			    }
   	    }, {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=20>',	width: 30, tdCls: 'tdAction', 
   	    		dataIndex: 'art_esau', tooltip: 'Articoli in esaurimento',
   			    renderer: function(value, p, record){
		    		if (record.get('art_esau') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';				    	
   			    }
   	    },{
		   text: 'UM',
		   width: 30,
		   dataIndex: 'um',
		},{
		   text: 'Q.t&agrave;',
		   width: 60,
		   dataIndex: 'qta', align: 'right', renderer: floatRenderer2,
		},{
		   text: 'Cliente', width: 110, dataIndex: 'TDDCON',
		   renderer: function(value, metaData, record, row, col, store, gridView){
		   	metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(value) + '"';
		   	return value;
		   }	
		},{
		   text: 'Ordine',
		   width: 100,
		   dataIndex: 'M3NRDO', 
				renderer: function(value, metaData, record, row, col, store, gridView){
								if (record.get('M3AADO') == 'COUNT')
									return '[ ' + value + ' ]';
								if (parseInt(record.get('M3NRDO')) > 0)			               
			                		return "" + record.get('M3AADO') + "_" + record.get('M3NRDO');
			                	else return "";
			                }		   
		}
	 , {text: 'Tp.', width: 35, dataIndex: 'TDTPDO'}
	 , {text: 'St.', width: 40, dataIndex: 'TDSTAT'}	 
	 , {text: '<img src=<?php echo img_path("icone/16x16/puntina_rossa.png") ?>', width: 25, dataIndex: 'TDDTVA',
	 			tooltip: 'Data confermata',
   			    renderer: function(value, metaData, record){
   			    
   			      //mostro eventuale data confermata
		    		if (value > 0){
					 metaData.tdAttr = 'data-qtip="Data conferma: ' + Ext.String.htmlEncode(date_from_AS(value)) + '"';		    		
		    		 return '<img src=<?php echo img_path("icone/16x16/puntina_rossa.png") ?>';
		    		}
		    		
		    	  //mostro se � hold
		    	    if (record.get('TDSWSP') == 'N'){
						return '<img src=<?php echo img_path("icone/16x16/button_blue_pause.png") ?>';		    	    	
		    	    }		    	  	
		    		 
   			    }	 
	   }	 
	 , {text: 'Ev.Progr.', width: 70, dataIndex: 'TDDTEP', renderer: date_from_AS}
	 
 	<?php if ($m_params_ar['my_opzione'] != 'provvisori'){ ?>	 
	 , {text: 'Sped.', width: 70, dataIndex: 'TDDTSP', renderer: date_from_AS}
	<?php } ?> 
	 
	 , {text: 'Pr.', width: 25, dataIndex: 'TDPRIO',
    				renderer: function (value, metaData, record, row, col, store, gridView){
    					if (record.get('tp_pri') == 4)
    						metaData.tdCls += ' tpSfondoRosa';

    					if (record.get('tp_pri') == 6)
    						metaData.tdCls += ' tpSfondoCelesteEl';					
    					
    					return value;
    				    
    	    			}	 
	 }	 	 	 
	]   
  
 }
}
