<?php

require_once "../../config.inc.php";
require_once "acs_wizard_MTS_include.php";

$m_DeskAcq = new DeskAcq();
$main_module = new DeskAcq();
$m_params = acs_m_params_json_decode();

ini_set('max_execution_time', 6000);
ini_set('memory_limit', '2048M');

/*************************************************************************
 * SALVA QUANTITA'
 *************************************************************************/
if ($_REQUEST['fn'] == 'exe_save_quant'){

	/*
	 Viene passato l'elenco delle chiavi ditta/articolo e relativa qta ordinata
	 1) aggirno le qta' su wmaf20 (usando i campi 20)
	 2) creo una riga su RI per ogni fornitore/data
	 */

	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$ar_ord = array();

	$m_params = acs_m_params_json_decode();
	$list_selected_id = $m_params->list_selected_id; //elenco chiavi di fabbisogno
	
	foreach($list_selected_id as $v){
	
        if($v->QTA_RIORD == 0){
        	$stato='';
        }else{
        	$stato='H';
        }
	}
	$sql = "UPDATE {$cfg_mod_DeskAcq['file_wizard_MTS_2']} M2
	SET M2QDOR=?, M2FLOR = '{$stato}'
	WHERE RRN(M2) = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	
	/*print_r($sql);
	exit;*/

	foreach($list_selected_id as $kr => $r){
		$r = (array)$r;
		$qta	= (float)$r['QTA_RIORD'];
		$dt 	= trim($r['DT']);
		$art	= trim($r['ARTICOLO_C']);
		$prog	= trim($r['PROGRESSIVO']);
		$rrn	= $r['RRN'];
		$result = db2_execute($stmt, array($qta, $rrn));
		echo db2_stmt_errormsg($stmt);

		//mi creo un array per ditta/fornitore/consegna
		$ar_ord[$dt][$forn][$cons] +=1;

		/*print_r($r);
		 exit;*/
	}

	$ret['success'] = true;
	
	echo acs_je($ret);
	exit;

}


/*************************************************************************
 * GENERAZIONE ORDINI MTO
 *************************************************************************/
if ($_REQUEST['fn'] == 'exe_generazione_ordini'){

	/*
	 Viene passato l'elenco delle chiavi ditta/articolo e relativa qta ordinata
	 1) aggirno le qta' su wmaf20 (usando i campi 20)
	 2) creo una riga su RI per ogni fornitore/data
	 */

	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;

	$ar_ord = array();

	$m_params = acs_m_params_json_decode();
	$list_selected_id = $m_params->list_selected_id; //elenco chiavi di fabbisogno

	$sql = "UPDATE {$cfg_mod_DeskAcq['file_wizard_MTS_2']} M2
			SET M2QDOR=?, M2FLOR = 'I', M2DTEO=?, M2PRZ=?
			WHERE RRN(M2) = ?";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	
	foreach($list_selected_id as $kr => $r){
		$r = (array)$r;
		$dt 	= trim($r['DT']);
		$art	= trim($r['ARTICOLO_C']);
		$prog	= trim($r['PROGRESSIVO']);
	    $qta	= (float)$r['QTA_RIORD'];
		$cons	= (string)$r['CONSEGNA_STD'];
		$forn	= (string)$r['FORNITORE_C'];
		$prz	= (float)$r['PRZ_LIST'];
		$rrn	= $r['RRN'];
		$result = db2_execute($stmt, array($qta, $cons, $prz, $rrn));
		echo db2_stmt_errormsg($stmt);

		//mi creo un array per ditta/fornitore/consegna
		$ar_ord[$dt][$forn][$cons] +=1;
		
		/*print_r($r);
		exit;*/
	}


	//Prima di generare gli ordini, recupero l'elenco delle righe interessate (perche' dovro' fornire un report)
	//TODO: gestire la multisessione?
	$sql2 = "SELECT * FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} WHERE M2FLOR = 'I'";
	$stmt2 = db2_prepare($conn, $sql2);
	$result2 = db2_execute($stmt2);
	while ($r = db2_fetch_assoc($stmt2)) {
		$ret_row[] = $r;
	}


	$tipologia = $m_params->tipologia;

				//scrivo RI per il fornitore/data inline
				//HISTORY
				
				$sh = new SpedHistory($m_DeskAcq);   //$m_DeskAcq
				$sh->crea(
						'dora_gen_ord_mts',
						array(
								"ditta" => $dt,
								"fornitore" => $forn,
								"tipologia" => $tipologia,
								"msg" => $m_params->msg_magazzino
								
						)
						);
				
		
	


	$ret = array();
	$ret['success']	= true;
	$ret['el_row']	= $ret_row;
	echo acs_je($ret);

	exit;
}



// ******************************************************************************************
// exe modifica punto riordino su articolo
// ******************************************************************************************
if ($_REQUEST['fn'] == 'grid_conferma_ordine'){
	$m_params = acs_m_params_json_decode();

	
	?>

{"success":true, "items":

{
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	  
	  
		dockedItems: [{
			xtype: 'toolbar'
					, dock: 'bottom'
							, items: [
							
							{ xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-box_open-32',
					                     text: 'Genera movimento magazzino',
								            handler: function() {
			            
			            	list_selected_id = [];
							grid = this.up('grid');
							m_win = this.up('window');
														
							grid.getStore().each(function(rec) {  
							  if (rec.get('OLD_RIORD').trim() == 'H' && parseFloat(rec.get('QTA_RIORD')) > 0)
							  	list_selected_id.push(rec.data); 
							},this);
							
							tipol = Ext.getCmp('id_caus').getValue();
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione del movimento di magazzino?', function(btn){
							   if(btn === 'yes'){
							    Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
							            method: 'POST',
					        			jsonData: {
					        			list_selected_id: list_selected_id, 
					        			tipologia: tipol,
					        			msg_magazzino: 'GEN_MOV_MTS'
					        			
					        			},						            
							            success: function ( result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                
							                	m_win.fireEvent('afterExecute');
							                	m_win.close();
							                
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });
										            
							     
								         	}
								       }  
									  , {
							  	id: 'id_caus',				  
								name: 'f_caus',
								xtype: 'combo',
								fieldLabel: 'Causale',
								labelAlign: 'right',
								labelWidth: 40,
								width: 200,
								displayField: 'text',
							    valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: false,
								//value: '<?php echo '000'; ?>',
							   	allowBlank: true,														
					            	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [		
							        <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('WMTSC'), '') ?>	
								    ] 
							   }	
							    						 
							  }  , '->',       
					 				{ 
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-shopping_cart_green-32',
					                     text: 'Genera ordini',
								            handler: function() {
                    			            
                			            	list_selected_id = [];
                							grid = this.up('grid');
                							m_win = this.up('window');
                							var mostra_errore = false;					
                							grid.getStore().each(function(rec) {  
                							  console.log(rec.data);
                							  if(rec.get('error_qta') == 'Y') mostra_errore = true;
                							  if (rec.get('OLD_RIORD').trim() == 'H' && parseFloat(rec.get('QTA_RIORD')) > 0)
                							  	list_selected_id.push(rec.data); 
                							},this);
							
                							tipol = Ext.getCmp('id_tip').getValue();
                							 if(mostra_errore == true){
        							         	acs_show_msg_error('Controllare la quantit&agrave;');
        							         	return false;
        						             }else{
                							
                							//eseguo procedura
                							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione degli ordini a fornitore?', function(btn){
                							   if(btn === 'yes'){
                							    Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
                							        Ext.Ajax.request({
                							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
                							            method: 'POST',
                					        			jsonData: {
                					        			list_selected_id: list_selected_id, 
                					        			tipologia: tipol
                					      
                					        			},						            
                							            success: function ( result, request) {
                							                var jsonData = Ext.decode(result.responseText);
                							                
                							                	m_win.fireEvent('afterExecute');
                							                	m_win.close();
                							                
                											Ext.getBody().unmask();									    								
                							            },
                							            failure: function ( result, request) {
                											Ext.getBody().unmask();														            
                							            }
                    							        });
                    							   }
                    							   else{
                    							   }
                    							 });
										  		}          
							     
								         	}
								       }  
									  , {
							  	id: 'id_tip',					  
								name: 'f_tipol',
								xtype: 'combo',
								fieldLabel: 'Tipologia',
								labelWidth: 50,
								width: 200,
								margin: '0 5 0 0',
								labelAlign: 'right',
								displayField: 'text',
							    valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: false,
								value: '<?php echo '000'; ?>',
							   	allowBlank: true,														
					            	store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [		
							        <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('WMTST'), '') ?>	
								    ] 
							   }	
							    						 
							  }      			       
			]
		}],	                    
		
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		            listeners: {
		              'edit': function(editor, e, opts) {
		              	
		              	//se una data, converto il valore in Ymd
		              	if (e.field == 'CONSEGNA_STD'){
		              		e.record.set(e.field, Ext.Date.format(e.value, 'Ymd'));
		              	}
		              
		             	//////e.grid.store.save();
		             	
		             	//Forzoicalcolo totale riga
		             	e.record.set('PRZ_LIST_TOT');
		             	
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],	    
	    
		features: [
					{ftype: 'summary'}, {
			            id: 'group',
			            ftype: 'groupingsummary',
			            groupHeaderTpl: '{[values.rows[0].data.FORNITORE_D]} [{name}]',
			            hideGroupedHeader: false
			        },					
						
			{
				ftype: 'filters',
				encode: false, //settingsGrid.store.commitChanges() json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    	signal_error: function(record){
			
				var multi = record.M2LOTT;
				if(parseInt(multi) > 0){
					var remainder = record.QTA_RIORD % multi;
					if (remainder == 0){
                    	var colora_riga = ' sfondo_grigio';
                    	record.error_qta = 'N';
                    } else {
                       var colora_riga = ' sfondo_rosso';
                       record.error_qta = 'Y';
                    }
                }
                
               if (parseInt(record.M2RIOR) > 0 && record.QTA_RIORD < record.M2RIOR){
                   	var colora_riga = ' sfondo_rosso';
                   	record.error_qta = 'Y';
               } 
               else if (parseInt(multi) > 0 && record.error_qta == 'Y'){
               var colora_riga = ' sfondo_rosso';
               }
               else if (record.OLD_RIORD.trim() != '')
                 var colora_riga = ' sfondo_grigio';
            
          
          		return colora_riga;
			},
	    
		store: {
			xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_conferma_grid', 
								   method: 'POST',								
								   type: 'ajax',
    								actionMethods: {
										     read: 'POST'
										    },
				          extraParams: {
								 
								 selected_forn: <?php echo j($m_params->fornitore)?>
							
			        			},
			     
			    doRequest: personalizza_extraParams_to_jsonData, 								
	
							       
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root',
						            idProperty: 'order'						            
						        }
							},
			groupField: 'FORNITORE_C',
				
			fields: ['RRN', 'DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'ALTRO_CODICE', 'UM', 'PROGRESSIVO', 'MAGAZZINO', 'M2RSCA',
						{name: 'DISPONIBILITA', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'},
						{name: 'IMPEGNATO_DA_PROGRAMMARE', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'}, 
						{name: 'M2RIOR', type: 'float'}, {name: 'M2LOTT', type: 'float'}, 
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'ULTIMO_ACQ', 'QTA_RIORD', 'PRZ_LIST', 
						{name: 'PRZ_LIST_TOT', type: 'float',
							convert: function(val,row) {
    							return parseFloat(row.get('QTA_RIORD')) * parseFloat(row.get('PRZ_LIST'));
    						}
    					}, 'error_qta'	
					]
						
		}, //store	    
	    			 		
		columns: [				    
				  	{header: 'Codice', width: 100, dataIndex: 'ARTICOLO_C', filter: {type: 'string'}}
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}, 	
				  		 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					           return 'Totale ordine a fornitore'; 
					     }
					}
				  , {header: 'Altro codice', width: 50, dataIndex: 'ALTRO_CODICE'}		
				  , {header: 'Mag.', width: 70, dataIndex: 'MAGAZZINO', filter: {type: 'string'}}					
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', tdCls: 'grassetto', 
				  	renderer: function (value, metaData, record, row, col, store, gridView){
    					
    					metaData.tdCls += ' grassetto';
    					var colora_riga = this.signal_error(record.data);
                        metaData.tdCls += colora_riga;
                       
    			           return floatRenderer0N(value);
    			        },				  	
    				editor: {
    			                xtype: 'numberfield',
    			                allowBlank: true
    			            }				  
    				   }
				   
				  , {header: 'Riord.Min.',	 width: 70, dataIndex: 'M2RIOR', align: 'right'}
				  , {header: 'Mult.Riord.',	 width: 70, dataIndex: 'M2LOTT',  align: 'right'}
				   
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS, tdCls: 'grassetto',
					editor: {
			                xtype: 'datefield',
			                allowBlank: false			                
			            }				  				   
				  
				  }
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', renderer: floatRenderer2N, tdCls: 'grassetto',
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  				   
				  }				  
				  , {header: 'Importo',	 	width: 72, dataIndex: 'PRZ_LIST_TOT', align: 'right', renderer: floatRenderer2N,
							 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
								            return floatRenderer2(value); 
								        }				  
				  }				  
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    }
		    
	    
		    
    							
	}    
    

}


<?php exit; }

// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_grid'){
	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$m_where = get_sql_where($m_params);
	
	$sql = "SELECT RRN(W2) AS RRN, M2CDOL, M2DVN1, M2DVN2, M2DVN3, M2DVN4, M2DVN5, M2QDOR, M2FLOR, M2QMAN, M2LOTT, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2UEMO, W2.M2FLR1, W2.M2QFAB, W2.M2QGIP, W2.M2RSCA, W2.M2PRZ,
            W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2RGS1, W2.M2DTEO, W2.M2ART, W2.M2PROG, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2UM, M2MAGA
	        FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
            LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
	        ON W2.M2DT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = W2.M2SELE
	        WHERE 1=1 AND M2DT = '{$id_ditta_default}' {$m_where}
	      /*GROUP BY
	        M2CDOL, M2DVN1, M2DVN2, M2DVN3, M2DVN4, M2DVN5, M2QDOR, M2FLOR, M2QMAN, M2LOTT, W2.M2QGIA, W2.M2QORD, W2.M2SMIC, W2.M2SMIA, W2.M2LOTT, W2.M2UIMP, W2.M2FLR1, W2.M2UEMO, W2.M2QFAB, W2.M2QGIP, W2.M2RSCA, W2.M2PRZ,
	        W2.M2SELE, W2.M2DT, W2.M2FOR1, W2.M2RGS1, W2.M2DTEO, W2.M2ART, W2.M2PROG, W2.M2ESAU, W2.M2SWTR, W2.M2DART, W2.M2UM,
	        TA_ITIN.TASITI, M2MAGA*/

	       ORDER BY W2.M2ART, W2.M2MAGA, W2.M2PROG";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg();
	
	while ($r = db2_fetch_assoc($stmt)) {
		$r['DT'] 			= acs_u8e($r['M2DT']);
		$r['FORNITORE_C'] 	= acs_u8e($r['M2FOR1']);
		
		if(trim($r['M2DVN1'])!=''){
			$r['VARIANTI'] 	    = acs_u8e($r['M2DVN1']);
		}
		
		if(trim($r['M2DVN2'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN2']);
		}
		
		if(trim($r['M2DVN3'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN3']);
		}
		
		if(trim($r['M2DVN4'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN4']);
		}
		
		if(trim($r['M2DVN5'])!=''){
			$r['VARIANTI'] 	    .= "<br>".acs_u8e($r['M2DVN5']);
		}
		
		//$ta_sys = find_TA_sys('MUFD', trim($r['M2MAGA']));
		//$r['MAGAZZINO'] 	=  "[".trim($r['M2MAGA'])."] ".$ta_sys[0]['text'];
		$r['MAGAZZINO'] = trim($r['M2MAGA']);
		
		
		$r['FORNITORE_D'] 	= acs_u8e($r['M2RGS1']);
		$r['ARTICOLO_C'] 	= acs_u8e($r['M2ART']);
		$r['ARTICOLO_D'] 	= acs_u8e($r['M2DART']);
		$r['ALTRO_CODICE'] 	= acs_u8e($r['M2CDOL']);
		$r['PROGRESSIVO'] 	= $r['M2PROG'];
		$r['UM'] 			= acs_u8e($r['M2UM']);
		$r['GIACENZA'] 		= acs_u8e($r['M2QGIA']);
		$r['ORDINATO'] 		= acs_u8e($r['M2QORD']);
		$r['IMPEGNATO'] 	= $r['M2QFAB'];
		$r['IMPEGNATO_DA_PROGRAMMARE'] 	= $r['M2QGIP'];
		$r['SCORTA'] 		= acs_u8e($r['M2SMIC']);

		$r['LOTTO_RIORD']	= (int)$r['M2LOTT'];
		
		$r['TIPO_RIORDINO']	= $r['M2SWTR'];
		$r['OBSOLETO']	   = $r['M2FLR1'];

		$r['CONSEGNA_STD']	= acs_u8e($r['M2DTEO']);
		$r['ULTIMO_IMP']	= acs_u8e($r['M2UIMP']);
		$r['ULTIMO_ACQ']	= $r['M2UEMO'];
		$r['DISPONIBILITA']	= acs_u8e((float)$r['M2QGIA'] + (float)$r['M2QORD'] - (float)$r['M2QFAB'] - (float)$r['M2QGIP']);
		$r['DISPONIBILITA_MANCANTE']	= $r['M2QMAN'];

		//valore di scostamento dalla disponibilita'
		if ((int)$r['LOTTO_RIORD'] != 0){
			$r['SCOST_RIORD'] = ( (float)$r['DISPONIBILITA'] - (float)$r['LOTTO_RIORD'] ) / (float)$r['LOTTO_RIORD'] * 100;
		}


		$r['QTA_RIORD']		= $r['M2QDOR'];
		$r['OLD_RIORD']		= $r['M2FLOR'];
		$r['PRZ_LIST']		= $r['M2PRZ'];
		$r['RRN']    = $r['RRN'];
		$ar_ret[] = $r;
	}


	echo acs_je($ar_ret);

	exit();
}

// ******************************************************************************************
// DATI PER DEPOSITO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_deposito'){
    
    global $cfg_mod_DeskAcq;
    
    $sql = "SELECT DISTINCT M2MAGA FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
    WHERE M2DT = '{$id_ditta_default}' AND M2MAGA <> ''";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
        
    $ar = array();
    
    while ($r = db2_fetch_assoc($stmt)) {
        
        $ta_sys = find_TA_sys('MUFD', trim($r['M2MAGA']));
        $ret[] = array("id"=>trim($r['M2MAGA']), "text" =>"[".trim($r['M2MAGA'])."] ".$ta_sys[0]['text']);
        
        
    }
    
    echo acs_je($ret);
    exit();
}


// ******************************************************************************************
// DATI PER GRID FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;

	$sql = "SELECT DISTINCT M2FOR1, M2RGS1 FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']}
	        ORDER BY M2RGS1";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}




//CAMPI CORRISPONDENTI NON TROVATI IN M2
// ******************************************************************************************
// DATI PER SELECT GRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_gruppo_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;
	
	$sql = "SELECT DISTINCT m2cgru as id, TRIM(m2dgru) as text FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2 WHERE m2cgru <> ''";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


//CAMPI CORRISPONDENTI NON TROVARI IN M2
// ******************************************************************************************
// DATI PER SELECT SOTTOGRUPPO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_sottogruppo_fornitore'){

	//costruzione sql
	global $cfg_mod_DeskAcq;	

	$sql = "SELECT DISTINCT m2csgr as id, m2dsgr as text FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2 WHERE m2csgr <> ''";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$ar[] = $r;
	}
	$ret = array("options" => $ar);
	echo acs_je($ret);
	exit();
}


if ($_REQUEST['fn'] == 'get_json_data_conferma_grid'){

	global $conn;
	global $cfg_mod_DeskAcq, $cfg_mod_Admin;
	
	$m_params = acs_m_params_json_decode();
	
	$sql = "SELECT RRN(W2) AS RRN, W2.*
	FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} W2
    WHERE M2FOR1= {$m_params->selected_forn} AND M2FLOR='H' ";

	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while ($r = db2_fetch_assoc($stmt)) {
		$r['DT'] 			= acs_u8e($r['M2DT']);
		$r['FORNITORE_C'] 	= acs_u8e($r['M2FOR1']);
        $r['FORNITORE_D'] 	= acs_u8e($r['M2RGS1']);
		$r['ARTICOLO_C'] 	= acs_u8e($r['M2ART']);
		$r['ARTICOLO_D'] 	= acs_u8e($r['M2DART']);
		$r['ALTRO_CODICE'] 	= acs_u8e($r['M2CDOL']);
		$r['PROGRESSIVO'] 	= $r['M2PROG'];
		$r['UM'] 			= acs_u8e($r['M2UM']);
	    $r['CONSEGNA_STD']	= acs_u8e($r['M2DTEO']);
	    
	    //$ta_sys = find_TA_sys('MUFD', trim($r['M2MAGA']));
	    //$r['MAGAZZINO'] 	=  "[".trim($r['M2MAGA'])."] ".$ta_sys[0]['text'];
	    $r['MAGAZZINO'] = trim($r['M2MAGA']);
		
	    $r['QTA_RIORD']		= $r['M2QDOR'];
		$r['OLD_RIORD']		= $r['M2FLOR'];
		$r['PRZ_LIST']		= $r['M2PRZ'];
		$r['RRN']		= $r['RRN'];
		
		$r['M2RIOR'] 	= $r['M2RIOR'];
		$r['M2LOTT'] 	= $r['M2LOTT'];
		
		$ar_ret[] = $r;
	}


	echo acs_je($ar_ret);

	exit();
}


// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA (APERTA IN DEFAULT)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
//            title: 'Selezione dati',
            
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack  : 'start',
                bodyPadding: 5               
     		},
			            
            
            items: [
  			 	    {					
						xtype: 'grid',
						flex: 1,
						id: 'tab-tipo-stato',
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},
     					
						features: [
						{
							ftype: 'filters',
							// encode and local configuration options defined previously for easier reuse
							encode: false, // json encode the filter query
							local: true,   // defaults to false (remote filtering)
					
							// Filters are most naturally placed in the column definition, but can also be added here.
						filters: [
						{
							type: 'boolean',
							dataIndex: 'visible'
						}
						]
						}],     					
     					
									
						selModel: {selType: 'checkboxmodel', mode: 'SINGLE'},				
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fornitore',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
								
						
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['M2FOR1', 'M2RGS1']
										
										
						}, //store
			
						columns: [{header: 'Fornitore', dataIndex: 'M2FOR1', flex: 2, filter: {type: 'string'}, filterable: true},
								  {header: 'Denominazione', dataIndex: 'M2RGS1', flex: 8, filter: {type: 'string'}, filterable: true}]
						 
					}            
            
            
            		, {            		
						margin: "0 0 0 2",            		
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			height: 160,
            			layout: {
			                type: 'vbox',
			                align: 'stretch',
			                pack  : 'start'
			     		},            			
            			
     					items: [{ 
						   xtype: 'fieldcontainer',
						   layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    //SELECT PER GRUPPO E SOTTOGRUPPO
					        {
								margin: "2 10 0 10",
								flex: 1,					
								name: 'f_gruppo',
								xtype: 'combo',
								fieldLabel: 'Gruppo fornitura',
								labelAlign: 'top',
								multiSelect : true,
								width: 290,
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_gruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  },{
								margin: "0 10 0 10",
								flex: 1,							  
								name: 'f_sottogruppo',
								xtype: 'combo',
								fieldLabel: 'Sottogruppo',
								labelAlign: 'top',
								displayField: 'TEXT',
								valueField: 'ID',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_sottogruppo_fornitore',
							            
					                	extraParams: {},				            
							            
							            reader: {
							                type: 'json',
							                root: 'options'
							            }
							        },       
									fields: ['ID', 'TEXT'],		             	
					            },
							    						 
							  }  
						
						]}, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    {
								margin: "0 10 0 10",
								width : 260,							  
								name: 'f_deposito',
								multiSelect : true,
								xtype: 'combo',
								fieldLabel: 'Deposito',
								labelAlign: 'top',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					       	            store: {
					                autoLoad: false,
									proxy: {
							            type: 'ajax',
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_deposito',
							            reader: {
        					                type: 'json',
        					                root: 'root',
        					                totalProperty: 'totalCount'
    					            }
							        },       
									fields: [{name:'id'}, {name:'text'}],     	             	
					            },
							    						 
							  }  ,
							  {
								margin: "0 10 0 10",							  
								name: 'f_approvvigionatore',
								xtype: 'textfield',
								fieldLabel: 'Approvvigionatore',
								labelAlign: 'top',
								flex: 1					
							  } 
						
						]},  
						
						
						
						
						
						
						{ 
							xtype: 'fieldcontainer',
							layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},							
							items: [
								 {
							    margin: "0 15 0 10",
								width : 260,							  
								name: 'f_tipo_riordino',
								multiSelect : true,
								xtype: 'combo',
								fieldLabel: 'Tipo riordino',
								labelAlign: 'top',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection: true,
							   	allowBlank: true,														
					       	    store: {
    								autoLoad: true,
    								editable: false,
    								autoDestroy: true,	 
    							    fields: [{name:'id'}, {name:'text'}],
    							    data: [								    
    								     <?php echo acs_ar_to_select_json($m_DeskAcq->find_TA_std('TRIO', null, 1, null, null, null, $main_module), '') ?> 	
    								    ] 
								}
							    						 
							  },
							 {
                                xtype: 'radiogroup',
                                layout: 'hbox',
                                fieldLabel: 'Senza impegni/ordini',
                                labelAlign : 'top',
                                flex: 1,
                                defaults: {
                                 width: 100,
                                },
                                items: [
                                {
                                    boxLabel: 'Escludi',
                                    name: 'f_imp_ord',
                                    checked: false,
                                    inputValue: 'E'
                                }, {
                                    boxLabel: 'Includi',
                                    checked: true,
                                    name: 'f_imp_ord',
                                    inputValue: 'T'
                                }, {
                                    boxLabel: 'Solo',
                                    checked: false,
                                    name: 'f_imp_ord',
                                    inputValue: 'S'
                                }
                               ]
                            }
							]
					      }
								    
								    
						
						
     					
					  
					
     					]
     				}            
            
     
					, {
            			xtype: 'panel',
            			border: true,
            			plain: true,
            			height: 50,
            			
			            layout: {
			                type: 'hbox',
			                align: 'stretch',
			                pack  : 'start'			                
			     		},            			
            			
     					items: [{
								margin: "0 10 0 10",							  
								name: 'f_des_art',
								xtype: 'textfield',
								fieldLabel: 'Descrizione articolo',
								labelAlign: 'top',
								flex: 1
							  }, {
								margin: "0 10 0 10",							  
								name: 'f_cod_art',
								xtype: 'textfield',
								fieldLabel: 'Codice articolo',
								labelAlign: 'top',
								flex: 1				
							  } ]
     				  }          		
            		
            		
     				
	
						 
				],
			buttons: [
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "WIZARD_MTS");  ?>
			{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [			
			
			
					
						]	  
				} 
				
				
				
				
				
				
			, {
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: [				
			
			 {
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection()[0].data.M2FOR1;
  				
				m_values = form_p.getValues();
				m_values['selected_id'] = id_selected;
	            
				//carico la form dal json ricevuto da php e la inserisco nel m-panel
				Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_grid',
				        method     : 'POST',
				        jsonData: m_values,
//				        jsonData: {
//				        	selected_id: list_selected_id,
//				        	f_gruppo: form_p.getForm().findField("f_gruppo").getValue(),
//				        	f_sottogruppo: form_p.getForm().findField("f_sottogruppo").getValue()
//				        },
				        waitMsg    : 'Data loading',
				        success : function(result, request){			        	
				        	
							mp = Ext.getCmp('m-panel');					        					        
							mp.remove('wizard-mts');					        
				            var jsonData = Ext.decode(result.responseText);
				            
				            mp.add(jsonData.items);
				            mp.doLayout();
				            mp.items.last().show();
 							this.print_w.close();
						             
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				        				        
				    });
				    
	            
            	                	                
	            }
	        }

	        ]
	       } 
	        
	        
	       ],             
				
        }
]}
<?php			            
exit;
}



/***********************************************************************************
 * GRID
 ***********************************************************************************/
if ($_REQUEST['fn'] == 'get_json_grid'){

	$m_params = acs_m_params_json_decode();

	//recuper data/ora ultimo aggiornamento
	$sql = "SELECT min(M2DTGE) as M2DTGE, min(M2ORGE) AS M2ORGE FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']}";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$r = db2_fetch_assoc($stmt);
	if ($r != false){
		$situazione_txt = " [Situazione al " . print_date($r['M2DTGE']) . " " . print_ora($r['M2ORGE']) . "]";
	} else
		$situazione_txt = '';


	//recuper data/ora ultimo aggiornamento
	$sql_f = "SELECT M2RGS1 FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']}
	        WHERE M2FOR1 = '{$m_params->selected_id}'";
	
	$stmt_f = db2_prepare($conn, $sql_f);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_f);
	
	$row = db2_fetch_assoc($stmt_f);
	$d_for = $row['M2RGS1'];
		
		?>

{"success":true, "items":

  {
    xtype: 'panel',
	id: 'panel-riordino_materiali_promo',	    
	title: <?php echo acs_je('Wizard MTS | ' . $m_params->selected_id)?>,
	closable: true,
	
    layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},    
    items: [
    
    {
		xtype: 'grid',
		layout: 'fit',
		flex: 1, autoScroll:true,
		title: '',
		autoScroll: true,
	    stateful: true,
	    stateId: 'wizard_mts',
	    stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ], 	
	    
        dockedItems: [{
            xtype: 'toolbar'
            , dock: 'top'
            , items: [{xtype: 'label', cls: 'strong', text: 'Controllo disponibilita/riordino ' + <?php echo j($situazione_txt." - ".$d_for); ?>}, {xtype: 'tbfill'}
            
				, {
                     xtype: 'button',
                     text: '',
			            handler: function() {
			         	}
			       }
			       
			       
				, {
	         	xtype: 'splitbutton',
	            text: 'Stampa per controllo giacenze',
	            iconCls: 'icon-print-16',
	            //scale: 'medium',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Ordina per codice', 
				            //scale: 'medium',
				            handler: function() {
				            
								var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_wizard_MTS_report.php?fn=open_report',
								        params: {
								        	form_values: Ext.encode(this.up('grid').store.proxy.extraParams),
								        	order_by : 'M2ART'
								        },
								        });	        				
			            	
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Ordina per articolo', 
				            //scale: 'medium',
				            handler: function() {
				            
				            var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_wizard_MTS_report.php?fn=open_report',
								        params: {
								        	form_values: Ext.encode(this.up('grid').store.proxy.extraParams),
								        	order_by : 'M2DART'
								        },
								        });	
				            
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Ordina per altro codice', 
				            //scale: 'medium',
				            handler: function() {
				            
				            var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_wizard_MTS_report.php?fn=open_report',
								        params: {
								        	form_values: Ext.encode(this.up('grid').store.proxy.extraParams),
								        	order_by : 'M2CDOL'
								        },
								        });	
				            
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Ordina per varianti', 
				            //scale: 'medium',
				            handler: function() {
				            
				            var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_wizard_MTS_report.php?fn=open_report',
								        params: {
								        	form_values: Ext.encode(this.up('grid').store.proxy.extraParams),
								        	order_by : 'M2DVN1, M2DVN2, M2DVN3, M2DVN4, M2DVN5'
								        },
								        });	
				            
				            
				            }
				        },{
							xtype: 'button',
				            text: 'Ordina per giacenza ascendente', 
				           // scale: 'medium',
				            handler: function() {
				            
				            var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_wizard_MTS_report.php?fn=open_report',
								        params: {
								        	form_values: Ext.encode(this.up('grid').store.proxy.extraParams),
								        	order_by : 'M2QGIA ASC'
								        },
								        });	
				            
				            
				            }
				        },
				         {
							xtype: 'button',
				            text: 'Ordina per giacenza discendente', 
				           // scale: 'medium',
				            handler: function() {
				            
				            var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_wizard_MTS_report.php?fn=open_report',
								        params: {
								        	form_values: Ext.encode(this.up('grid').store.proxy.extraParams),
								        	order_by : 'M2QGIA DESC'
								        },
								        });	
				            
				            
				            }
				        }
				      ]
				   }
				}  		       
			       
			       
 				, {
                     xtype: 'button',
	            	 //scale: 'medium',
	            	 iconCls: 'icon-shopping_cart_green-16',
                     text: 'Conferma generazione ordini a fornitore',
			            handler: function() {
			            
			            mm_grid = this.up('grid');
							
							//apro form per richiesta conferma e possibilita' modifica prezzo e data consegna
							                   	my_listeners = {
						        					afterExecute: function(){
						        						mm_grid.store.reload();						        									            
										        		}
								    				};							
							
							
							acs_show_win_std('Riepilogo articoli da riordinare per fornitore', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=grid_conferma_ordine', {fornitore: '<?php echo $m_params->selected_id ?>'}, 800, 500, my_listeners, 'icon-shopping_cart_gray-16');
							return false;
							
							
							//eseguo procedura
							Ext.MessageBox.confirm('Richiesta conferma', 'Articoli selezionati: ' + list_selected_id.length + '<br>Confermi la generazione degli ordini a fornitore?', function(btn){
							   if(btn === 'yes'){
									Ext.getBody().mask('Preparazione dati in corso... ', 'loading').show();							   														   
							        Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_generazione_ordini',
							            method: 'POST',
					        			jsonData: {list_selected_id: list_selected_id},						            
							            success: function ( result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                
							                //apro un report passando le righe interessate
												var mapForm = document.createElement("form");
												mapForm.target = "_blank";    
												mapForm.method = "POST";
												mapForm.action = '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_report';
												
												// Create an input
												var mapInput = document.createElement("input");
												mapInput.type = "hidden";
												mapInput.name = "jsonData";
												mapInput.value = JSON.stringify(jsonData);
												
												// Add the input to the form
												mapForm.appendChild(mapInput);
												
												// Add the form to dom
												document.body.appendChild(mapForm);
												
												// Just submit
												mapForm.submit();							                
							                
							                
							                grid.store.load()
											Ext.getBody().unmask();									    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
							   }
							   else{
							   }
							 });
										            
			         	}
			       }  			       
			       
			       
			       
			]
		}],	                    
		
	    
	    
	    selType: 'cellmodel',
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		              clicksToEdit: 1,
		            listeners: {
		              'beforeedit': function(e, eEv) {
		                var me = this;
		                
		                rec = eEv.record;
		              		                
						if (rec.data.OLD_RIORD.trim() == '' || rec.data.OLD_RIORD == 'H')		                
		                	return true;
		                else
		                	return false;
		              },
		              'edit': function(e) {
		                //this.isEditAllowed = false;
		              }
		            }
		          })
		      ],
	   
		features: [
			{
				ftype: 'filters',
				encode: false, // json encode the filter query
				local: true,   // defaults to false (remote filtering)
							
				// Filters are most naturally placed in the column definition, but can also be added here.
				filters: [
					{
						type: 'boolean',
						dataIndex: 'visible'
					}
				]
			}],		    
	    
	    
		store: {
			xtype: 'store',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
				method: 'POST',
				type: 'ajax',
	            timeout: 2400000,
	            autoSave: true,
	            autoSync: true,
	            actionMethods: {
							     read: 'POST'
							    },
				extraParams:  <?php echo acs_je($m_params); ?> ,
								
			    doRequest: personalizza_extraParams_to_jsonData, 								
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				}				
			},
				
			fields: ['DT', 'FORNITORE_C', 'FORNITORE_D', 'ARTICOLO_C', 'ARTICOLO_D', 'ALTRO_CODICE', 'UM', 'VARIANTI', 'PROGRESSIVO',
						'TIPO_RIORDINO', 'OBSOLETO',
						{name: 'DISPONIBILITA', type: 'int'}, {name: 'DISPONIBILITA_MANCANTE', type: 'int'},
						{name: 'GIACENZA', type: 'float'},
						{name: 'ORDINATO', type: 'float'},
						{name: 'IMPEGNATO', type: 'float'}, {name: 'IMPEGNATO_DA_PROGRAMMARE', type: 'float'},
						{name: 'SCORTA', type: 'float'},
						{name: 'LOTTO_RIORD', type: 'float'},
						{name: 'SCOST_RIORD', type: 'float'},
						{name: 'QTA_RIORD', type: 'float'}, 
						'OLD_RIORD', 'CONSEGNA_STD', 'ULTIMO_IMP', 'ULTIMO_ACQ', 'QTA_RIORD', 'PRZ_LIST', 'MAGAZZINO', 'RRN']
						
		}, //store	    
	    			 		
		columns: [
					{header: 'Codice', width: 100, dataIndex: 'ARTICOLO_C', filter: {type: 'string'}}
				  , {header: 'Articolo', flex: 1, dataIndex: 'ARTICOLO_D', filter: {type: 'string'}}
				  , {header: 'T', width: 30, dataIndex: 'TIPO_RIORDINO', filter: {type: 'string'}, tooltip: 'Tipo riordino'}
				  , {header: 'O', width: 30, dataIndex: 'OBSOLETO', filter: {type: 'string'}, tooltip: 'Obsoleto'}
				  , {header: 'Altro codice', flex: 1, dataIndex: 'ALTRO_CODICE', filter: {type: 'string'}}
				  , {header: 'Mag.', width: 70, dataIndex: 'MAGAZZINO', filter: {type: 'string'}}
				  , {header: 'Varianti', flex: 1, dataIndex: 'VARIANTI', filter: {type: 'string'}}
				  , {header: 'Progressivo', flex: 1, dataIndex: 'PROGRESSIVO', filter: {type: 'string'}}
				  , {header: 'UM',	 width: 50, dataIndex: 'UM'}
				  , {header: 'Disponib.',	width: 72, dataIndex: 'DISPONIBILITA', align: 'right',
					    renderer: function (value, metaData, record, row, col, store, gridView){
			                	metaData.tdCls += ' grassetto';

					  				if (parseFloat(value) < 0)
					  					 metaData.tdCls += ' sfondo_rosso';					  					 														  
					  				else if (parseFloat(value) == 0 || parseFloat(record.get('DISPONIBILITA_MANCANTE')) > 0 )
					  					 metaData.tdCls += ' sfondo_giallo';					  					 
					  				if (parseFloat(value) > 0)
					  					 metaData.tdCls += ' sfondo_verde';					  					 
			                			                
					  			return floatRenderer0N(value);									
						} 
				    }				  
				  , {header: 'Giacenza',	width: 72, dataIndex: 'GIACENZA', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Ordinato',	width: 72, dataIndex: 'ORDINATO', align: 'right',
				  
				  		renderer: function (value, metaData, record, row, col, store, gridView){
								if (record.get('M2RSCA') == 'S')			                	
										metaData.tdCls += ' grassetto sfondo_giallo';										
				                	
				            return floatRenderer0N(value);
				        }
				  
				  }
				  
				  , {header: 'Ultimo<br/>acquisto',	width: 90, dataIndex: 'ULTIMO_ACQ', renderer: date_from_AS}
				  
				  , {header: 'Impegnato',	width: 72, dataIndex: 'IMPEGNATO', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Impegnato<BR>da programmare',	width: 72, dataIndex: 'IMPEGNATO_DA_PROGRAMMARE', align: 'right', renderer: floatRenderer0N}
				  , {header: 'Scorta',	 	width: 72, dataIndex: 'SCORTA'	, align: 'right', renderer: floatRenderer0N}				  				  				  
				 <?php if($cfg_mod_DeskAcq['wizard_hide_col'] != 'Y'){?>
				  , {header: 'Punto di<br/>riordino',	width: 72, dataIndex: 'SCOST_RIORD', align: 'right',

						renderer: function (value, metaData, record, row, col, store, gridView){
								if (parseFloat(value) != 0){
				                	if (value < -5)
				                		metaData.tdCls += ' grassetto sfondo_rosso';
									else if (value <= 5)				                	
										metaData.tdCls += ' grassetto sfondo_giallo';
								}		
				                	
				                	return floatRenderer0N(record.get('LOTTO_RIORD'));
				        },				  	
				  	 
				  	}
				  <?php }?>	
				  , {header: 'Quantit&agrave;<br/>da ordinare',	width: 72, align: 'right', dataIndex: 'QTA_RIORD', 
				  	
					renderer: function (value, metaData, record, row, col, store, gridView){
					
					            if (record.get('OLD_RIORD').trim() == 'H' || record.get('OLD_RIORD').trim() == ''){
					             metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';
					            }
			                	else {
			                		metaData.tdCls += ' grassetto sfondo_grigio';
		                		}
			                	
			                	return floatRenderer0N(value);
			        },				  	
				  	
					editor: {
			                xtype: 'numberfield',
			                allowBlank: true
			            }				  
				   }
				  , {header: 'Consegna<br/>richiesta',	width: 90, dataIndex: 'CONSEGNA_STD', renderer: date_from_AS}
				  , {header: 'Ultimo<br/>impegno',	width: 90, dataIndex: 'ULTIMO_IMP', renderer: date_from_AS}
				  , {header: 'Prezzo<br/>unitario',	 	width: 72, dataIndex: 'PRZ_LIST', align: 'right', renderer: floatRenderer2N}
    	], 
    	
    	
	  viewConfig: {
		        getRowClass: function(record, index) {		           	
		         }   
		    },
		    
		    
         listeners: {
   		  celldblclick: {								
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
			  	rec = iView.getRecord(iRowEl);
			  	
		 /*	if (col_name == 'SCOST_RIORD'){

							                   	my_listeners = {
						        					afterExecute: function(form_values){
						        						rec.set('LOTTO_RIORD', form_values.f_qta);
						        						
						        						//ricalcolo lo scostamento
						        						
						        						//TODO: verificare perche' sembra che non reimposta correttamente il valore (o almeno lo sfondo)
						        						//if (parseFloat('LOTTO_RIORD') != 0)
														//	rec.set('SCOST_RIORD', ( parseFloat(rec.get('DISPONIBILITA')) - parseFloat(rec.get('LOTTO_RIORD')) ) / parseFloat(rec.get('LOTTO_RIORD')) * 100);
														
														rec.set('SCOST_RIORD', 0);							        						
						        						
						        						//iView.refreshNode(rec);
						        						//iView.getView().refreshRow(rec);						        									            
										        		}
								    				};			  	
			  	
			  		//modifica punto di riordino
					acs_show_win_std('Modifica punto di riordino', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_punto_riordino', {dt: rec.get('DT'), rdart: rec.get('ARTICOLO_C'), rddart: rec.get('ARTICOLO_D'), qta: rec.get('LOTTO_RIORD')}, 600, 200, my_listeners, 'icon-shopping_cart_gray-16');			  		
			  		return false;
			  	}  */ 
							  
				  acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('DT'), rdart: rec.get('ARTICOLO_C'), progressivo: rec.get('PROGRESSIVO'), varianti: rec.get('VARIANTI')}, 1200, 600, null, 'icon-shopping_cart_gray-16');
			  }
   		  },
   		  
   		         edit: function(editor, e, a, b){
			  
                 console.log('edit');
                 
                 			list_selected_id = [];
							grid = editor.grid;
							record= e.record;
												
						   /*	grid.getStore().each(function(rec) {  
							  if ((rec.get('OLD_RIORD').trim() == '' || rec.get('OLD_RIORD').trim() == 'H') && parseFloat(rec.get('QTA_RIORD')) > 0)
							  	
							},this);*/
							list_selected_id.push(record.data); 
							
							Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_quant',
							            method: 'POST',
					        			jsonData: {list_selected_id: list_selected_id},						            
							            success: function (result, request) {
							                var jsonData = Ext.decode(result.responseText);
							                    //grid.store.reload();	
							          							    								
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });
                  

            }, itemcontextmenu : function(grid, rec, node, index, event) {
		  		event.stopEvent();
		  		var grid = this;
		  													  
			    var voci_menu = [];
						  
				 voci_menu.push({
	         		text: 'Copia codice articolo',
	        		iconCls : 'icon-copy-16',          		
	        		handler: function () {
	        		   const el = document.createElement('textarea');
            	  		el.value = rec.get('ARTICOLO_C').trim();
            	  		document.body.appendChild(el);
            	  		el.select();
            	  		document.execCommand('copy');
            	  		document.body.removeChild(el);
            	  		
			        }
	    		});		 
	    		
	    		 var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	 
						  
			}
	         
         }		    
		    
    							
	}    
    
    ] //items
  } //main panel 

}
<?php
exit; }
?>