<?php

require_once("../../config.inc.php");
require_once("../desk_acq/acs_panel_ins_new_anag_include.php");

ini_set('max_execution_time', 30000);

$m_DeskAcq = new DeskAcq();
$main_module = new DeskAcq();
$deskGest = new DeskGest(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

function get_gcrow_by_prog($id_prog){
    global $conn, $cfg_mod_DeskAcq, $cfg_mod_Gest, $id_ditta_default;
    
    $sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG
    FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
    LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
    TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
    LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
    TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
    WHERE GCDT='{$id_ditta_default}' AND GCPROG = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_prog));
    $row = db2_fetch_assoc($stmt);
    $row['GCTRAS'] = trim($row['GCTRAS']);
    $row['GCCITI'] = trim($row['GCCITI']);
    $row['GCSEL2'] = trim($row['GCSEL2']);
    //$row = array_map('rtrim', $row);
    return $row;
}

function ar_standard_alternativa(){
	$ret = array();
	$ret[] = array('id' => 'S', 'text' => 'Standard');
	$ret[] = array('id' => 'A', 'text' => 'Alternativa');
	return $ret;
}


function ar_giorno_scarico(){
	$ret = array();
	$ret[] = array('id' => 'X', 'text' => 'Si');
	$ret[] = array('id' => 'M', 'text' => 'Mattino');
	$ret[] = array('id' => 'P', 'text' => 'Pomeriggio');
	return $ret;
}

function insert_parametro_in_db($stmt, $ar_ins, $parametro, $contenuto, $code, $seq, $tipo_contenuto = ''){
	$ar_ins['PARAMETRO'] = acs_u8e($parametro);
	$ar_ins['CONTENUTO'] = acs_u8e($contenuto);
	$ar_ins['CODICE_CONTENUTO'] = acs_u8e($code);
	$ar_ins['TIPO_CONTENUTO'] = $tipo_contenuto;
	$ar_ins['ICSEQU'] 	 = $seq;
	db2_execute($stmt, $ar_ins); echo db2_stmt_errormsg($stmt);
}



function write_parametri_cerved_base($resp_json){
    global $auth, $conn, $cfg_mod_DeskAcq;

	$ci = $resp_json->companyInfo[0];

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'BASE';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_DeskAcq['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'companyName', $ci->companyName, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'street', $ci->address[0]->street, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'postCode', $ci->address[0]->postCode, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'province', $ci->address[0]->province, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'municipality', $ci->address[0]->municipality, $ci->address[0]->municipalityCode, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'activityStatus', $ci->activityStatusDescription, $ci->activityStatus, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'companyType', $ci->companyType, $ci->companyType, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'companyForm ', $ci->companyForm->description, $ci->companyForm->code, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'registrationDate ', strftime('%Y%m%d', strtotime($ci->registrationDate)), '', ++$seq, 'D');
	insert_parametro_in_db($stmt, $ar_ins, 'ateco07  ', $ci->ateco07Description, $ci->ateco07, ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'reaCode  ', implode(' ', array($ci->reaCode->coCProvinceCode, $ci->reaCode->reano)), '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'taxCode  ', $ci->taxCode, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'vatRegistrationNo  ', $ci->vatRegistrationNo, '', ++$seq);

	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}




function write_parametri_cerved_details($resp_json){
    global $auth, $conn, $cfg_mod_DeskAcq;

	$ci = $resp_json->companyInfo;

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'DETAILS';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_DeskAcq['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'telephone', 	$ci->telephone, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'email', 		$ci->email, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'certifiedEmail',$ci->certifiedEmail, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'activityStartDate', strftime('%Y%m%d', strtotime($ci->activityStartDate)), '', ++$seq, 'D');
	insert_parametro_in_db($stmt, $ar_ins, 'paidUpEquityCapital', $ci->paidUpEquityCapital, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'sales', $ci->balanceDetails->sales, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'grossProfit', $ci->balanceDetails->grossProfit, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'profitAndLoss', $ci->balanceDetails->profitAndLoss, '', ++$seq);
	insert_parametro_in_db($stmt, $ar_ins, 'numberOfEmployes', $ci->balanceDetails->numberOfEmployes, '', ++$seq);

	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}





function write_parametri_cerved_scores($resp_json){
    global $auth, $conn, $cfg_mod_DeskAcq;

	$ci = $resp_json;

	$m_user 	= $auth->get_user();
	$m_date 	= oggi_AS_date();
	$m_time 	= oggi_AS_time();

	$ar_ins = array();
	$ar_ins['UTENTE_GENERAZ'] 	= $m_user;
	$ar_ins['DATA_GENERAZ'] 	= $m_date;
	$ar_ins['TIME_GENERAZ'] 	= $m_time;
	$ar_ins['COMPANYID']		= $ci->companyId;
	$ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
	$ar_ins['TIPO_RICHIESTA']	= 'SCORES';

	$ar_ins['PARAMETRO']		= '';
	$ar_ins['CONTENUTO']		= '';
	$ar_ins['CODICE_CONTENUTO']	= '';
	$ar_ins['TIPO_CONTENUTO']	= '';
	$ar_ins['ICSEQU']			= 0;

	//insert riga
	$sql = "INSERT INTO {$cfg_mod_DeskAcq['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();

	insert_parametro_in_db($stmt, $ar_ins, 'negativeEventsGrading', 	implode(', ', array(
			$ci->negativeEventsGrading->grading,
			$ci->negativeEventsGrading->descriptiveGradingSynthesisCode,
			$ci->negativeEventsGrading->subScoreClass
	)), $ci->negativeEventsGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'economicalFinancialGrading', 	implode(', ', array(
			$ci->economicalFinancialGrading->grading,
			$ci->economicalFinancialGrading->descriptiveGradingSynthesisCode,
			$ci->economicalFinancialGrading->subScoreClass
	)), $ci->economicalFinancialGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'paylineGrading', 	implode(', ', array(
			$ci->paylineGrading->grading,
			$ci->paylineGrading->descriptiveGradingSynthesisCode,
			$ci->paylineGrading->subScoreClass
	)), $ci->paylineGrading->available, ++$seq);

	insert_parametro_in_db($stmt, $ar_ins, 'cervedGroupScore', 	implode(', ', array(
			$ci->cervedGroupScore->grading,
			$ci->cervedGroupScore->descriptiveGradingSynthesisCode,
			$ci->cervedGroupScore->icon
	)), $ci->cervedGroupScore->available, ++$seq);


	//ritorno la chiave della tabella
	return $ar_ins['CODICE_RICHIESTA'];
}




function get_request_data($key_request){
    global $conn, $cfg_mod_DeskAcq;
    $sql = "SELECT * FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_cerved']}
    LEFT OUTER JOIN  {$cfg_mod_DeskAcq['file_tabelle']}
	ON PARAMETRO = TAMAIL AND TATAID = 'CRVDS'
	WHERE CODICE_RICHIESTA = ?";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($key_request));

	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		$n = array();
		$n['parametro'] = acs_u8e(trim($row['PARAMETRO']));
		$n['des_parametro'] = acs_u8e(trim($row['TADESC']));
		$n['contenuto'] = acs_u8e(trim($row['CONTENUTO']));
		$n['codice_contenuto'] = acs_u8e(trim($row['CODICE_CONTENUTO']));
		$n['tipo_contenuto'] = acs_u8e(trim($row['TIPO_CONTENUTO']));
		$ret[] = $n;
	}

	return $ret;
}





// ******************************************************************************************
// IMPORT ANAGRAFICA DA GEST
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_import_anag'){

    $m_params = acs_m_params_json_decode();
    
    if (isset($m_params->gccdcf))
        $cod_cli = sprintf("%09d", $m_params->gccdcf);
        else
            $cod_cli = sprintf("%09d", $m_params->form_values->f_cliente_cod);
            
            //NUOVA VERSIONE ATTRAVERSO RI
            $sh = new SpedHistory($m_DeskAcq);
            $return_RI = $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'EXP_ANAG_CLI',
                    "vals" => array(
                        "RICVES" => $cod_cli
                    )
                )
                );
            
        $ret['id_prog'] = (int)$return_RI['RIPROG'];
        echo acs_je($ret);
        exit;
            
}


// ******************************************************************************************
// EXE SAVE FORM CLASSIFICAZIONI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_crm'){
    
    $m_params = acs_m_params_json_decode();
    
    $ar_upd = array();
    $ret = array();
    
    $ar_upd = exe_save_crm_add_field($ar_upd, $m_params);
    
    $ar_upd['GCUSGE'] = trim($auth->get_user());
    $ar_upd['GCDTGE'] = oggi_AS_date();
    $ar_upd['GCORGE'] = oggi_AS_time();
    
    $ar_upd['GCUSUM'] = trim($auth->get_user());
    $ar_upd['GCDTUM'] = oggi_AS_date();
    $ar_upd['GCORUM'] = oggi_AS_time();
    
    
    //update riga
    $sql = "UPDATE {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_crm']}
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE GCPROG=?";
   
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array_merge(
        $ar_upd,
        array($m_params->form_values->GCPROG)
        ));
    echo db2_stmt_errormsg($stmt);
 
            
    //call AGG tramite RI
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory($m_DeskAcq);
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_ANAG_CLI',
            "vals" => array(
                "RIPROG" => $m_params->form_values->GCPROG
            )
        )
        );
           
    $record = get_gcrow_by_prog($m_params->form_values->GCPROG);
    $record = array_map('rtrim', $record);
    $ret['record'] = $record;
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

function exe_save_crm_add_field($ar, $m_params){
    $ar['GCPROG'] 	= utf8_decode($m_params->form_values->GCPROG);
  
    $ar['GCCIVI'] 	= utf8_decode($m_params->form_values->GCCIVI);
    $ar['GCORIG'] 	= utf8_decode($m_params->form_values->GCORIG);
    $ar['GCCRM1'] 	= utf8_decode($m_params->form_values->GCCRM1);
    $ar['GCCRM2'] 	= utf8_decode($m_params->form_values->GCCRM2);
    $ar['GCSEL1'] 	= utf8_decode($m_params->form_values->GCSEL1);
    $ar['GCSEL2'] 	= utf8_decode($m_params->form_values->GCSEL2);
    $ar['GCSEL3'] 	= utf8_decode($m_params->form_values->GCSEL3);
    $ar['GCSEL4'] 	= utf8_decode($m_params->form_values->GCSEL4);
    $ar['GCREFE'] 	= utf8_decode($m_params->form_values->GCREFE);
 
    return $ar;
}


// ******************************************************************************************
// IMPORT ANAGRAFICA DA GEST
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_import_anag_by_prog'){
	$m_params = acs_m_params_json_decode();
	$row = get_gcrow_by_prog($m_params->gcprog);
	$tipo_anag    = trim($row['GCTPAN']);

	if ($row['GCPRAB'] > 0){	//richiesta aggiornamento su destinazione
		$rowAbb = get_gcrow_by_prog($row['GCPRAB']);
		$cod_cli = $rowAbb['GCCDCF'];
		$cod_des = $row['GCCDCF'];
	} else {
		$cod_cli = $row['GCCDCF'];
		$cod_des = '';
	}

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $id_ditta_default);
	$cl_p .= sprintf("%09d", $cod_cli); //cliente
	$cl_p .= sprintf("%-1s", '');
	$cl_p .= sprintf("%010d", ''); //OUTPUT: progressivo ritornato che devo aprire
	$cl_p .= sprintf("%-1s", 'P'); //PUNTO VENDITA
	$cl_p .= sprintf("%-9s", $cod_des);  //destinazione

	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.BR31O7('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "";
		//FINE test per Apra
		
		
		//recupero l'ultimo ID cliente creato
		$sql = "SELECT GCPROG FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
		WHERE GCFG01 <> 'A' AND GCTPAN = 'FOR' AND GCCDCF = '{$cod_cli}'
		AND GCDT = '$id_ditta_default'
		ORDER BY GCPROG DESC FETCH FIRST 1 ROWS ONLY";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$ar = array();
		$r = db2_fetch_assoc($stmt);
		$return_id_prog = $r['GCPROG'];
		
		
		if ($tipo_anag != 'FOR'){ //Destionazione, etc...
		    $sql = "SELECT GCPROG FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
		    WHERE GCFG01 <> 'A' AND GCTPAN = '{$tipo_anag}'
		    AND GCPRAB = {$return_id_prog} AND GCCDCF = '{$cod_des}'
		    AND GCDT = '$id_ditta_default'
		    ORDER BY GCPROG DESC FETCH FIRST 1 ROWS ONLY";
		    
		    $stmt_des = db2_prepare($conn, $sql);
		    echo db2_stmt_errormsg();
		    $result = db2_execute($stmt_des);
		    $r_des = db2_fetch_assoc($stmt_des);
		    $return_id_prog = $r_des['GCPROG'];
		}
		
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('BR31O7', $libreria_predefinita_EXE, $cl_in, null, null);
	}

	
	$ret = array();
	$ret['success'] = true;
	$ret['call_return'] = $call_return['io_param']['LK-AREA'];
	if (isset($return_id_prog))
	    $ret['id_prog'] = $return_id_prog;
    else
        $ret['id_prog'] = (int)substr($ret['call_return'], 259, 10);
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_ricalcola_iban'){
	
	require_once(dirname(__FILE__) . '/../../utility/php-iban-2.5.9/php-iban.php');
	
	$m_params = acs_m_params_json_decode();
	$form_values = $m_params->form_values;
	
	//costruisco iban neutro
	$iban = "IT";
	$iban .= sprintf("%-2s", 'XX'); //CIN IBAN
	$iban .= sprintf("%-1s", 'Y'); //CIN BBAN
	$iban .= sprintf("%05d", $form_values->CCABI);
	$iban .= sprintf("%05d", $form_values->CCCAB);
	
	if (strlen(trim($form_values->CCCOCO)) < 12)
		$iban .= sprintf("%012d", $form_values->CCCOCO);
	else	
		$iban .= trim($form_values->CCCOCO);
	
	$iban = iban_set_nationalchecksum($iban);
	
	$ret = array();
	$ret['success'] = true;
	$ret['iban'] = $iban;
	
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// verifico se localita da cerved e' present in ANCOM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verify_set_localita'){
	$m_params = acs_m_params_json_decode();

	$sql = "SELECT TA_COM.*, TA_REG.TADESC AS des_reg FROM {$cfg_mod_DeskAcq['file_tabelle']} TA_COM			
	        LEFT OUTER JOIN   {$cfg_mod_DeskAcq['file_tabelle']} TA_REG ON
					TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'	
					WHERE TA_COM.TATAID = 'ANCOM' AND TA_COM.TAKEY4 = '{$cfg_mod_Gest['cod_naz_italia']}' AND TA_COM.TAKEY2 = ? AND TA_COM.TADESC = ?";		
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($m_params->provincia, $m_params->comune));
	$row = db2_fetch_assoc($stmt);
	$ret = array();

	if ($row) {
		$ret['success'] = true;
		$ret['data'] = $row;
	}
	else
		$ret['success'] = false;
	
	echo acs_je($ret);
 exit;
}

// ******************************************************************************************
// VIEW GRID RESPONSE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'view_grid_response'){
	$m_params = acs_m_params_json_decode();	
	?>
	{"success":true, "items": [
			{
				xtype: 'grid',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
				store: {
						xtype: 'store',
						autoLoad:true,
						data: <?php echo acs_je(get_request_data($m_params->key_request)); ?>,
			        	fields: ['parametro', 'des_parametro', 'contenuto', 'codice_contenuto', 'tipo_contenuto']
				}, //store
		
				columns: [	
		    		  {text: 'Parametro', width: 90, 	dataIndex: 'des_parametro'}
		    		, {text: 'Contenuto', flex: 1, 		dataIndex: 'contenuto', renderer: function (value, metaData, rec){
		    		
		    				if (rec.get('tipo_contenuto') == 'D')
		    					return date_from_AS(value);
		    		
		    				if (Ext.isEmpty(rec.get('codice_contenuto')) == false){
		    					return '[' + rec.get('codice_contenuto') + '] ' + value;
		    				}
		    				 else return value;		    				
		    			}
		    		  }
				],
				enableSort: false

			, buttons: [
			
			<?php if ($m_params->view_type == 'base') { ?>		
							
						{
							xtype: 'button',
				            text: 'Dettagli', 
				            iconCls: 'icon-folder_open-32', scale: 'large',
				            handler: function() {
				            
								Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
										timeout: 2400000,
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved_by_company',
								        jsonData: {
								        	view_type: 'details',
								        	companyId: <?php echo $m_params->companyId; ?>
								        },	
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        						        
											Ext.getBody().unmask();
								            var jsonData = Ext.decode(result.responseText);
								            
											acs_show_win_std('Cerved response - Details', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
													{
														view_type: 'details',
										  				key_request: jsonData.key_request,
										  				companyId: jsonData.companyId
										  			}, 750, 550, {}, 'icon-globe-16');	
		
								        },
								        failure    : function(result, request){
											Ext.getBody().unmask();							        
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	
				            
				            }
				        }
				        
				        
				        
				        , {
							xtype: 'button',
				            text: 'Scores', 
				            iconCls: 'icon-certificate-32', scale: 'large',
				            handler: function() {
				            
								Ext.getBody().mask('Loading... ', 'loading').show();
								Ext.Ajax.request({
										timeout: 2400000,
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=call_cerved_by_company',
								        jsonData: {
								        	view_type: 'scores',
								        	companyId: <?php echo $m_params->companyId; ?>
								        },	
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        						        
											Ext.getBody().unmask();
								            var jsonData = Ext.decode(result.responseText);
								            
											acs_show_win_std('Cerved response - Scores', 
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=view_grid_response', 
													{
														view_type: 'scores',
										  				key_request: jsonData.key_request,
										  				companyId: jsonData.companyId
										  			}, 750, 550, {}, 'icon-globe-16');	
		
								        },
								        failure    : function(result, request){
											Ext.getBody().unmask();							        
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	
				            
				            }
				        }
				        
				        , {
							xtype: 'button',
				            text: 'Copia intestazione', 
				            iconCls: 'icon-keyboard-32', scale: 'large',
				            handler: function() {
				            
				            	t_win = this.up('window');
				            	from_win = Ext.getCmp(<?php echo j($m_params->from_window_id); ?>);
				            	form = from_win.down('form').getForm();
				            
				            	this.up('grid').store.each(function(rec)  
								{
								  
								  if (rec.get('parametro') == 'taxCode') form.findField('GCCDFI').setValue(rec.get('contenuto'));
								  
								  if (rec.get('parametro') == 'companyName') form.findField('GCDCON').setValue(rec.get('contenuto'));  
								  if (rec.get('parametro') == 'street') form.findField('GCINDI').setValue(rec.get('contenuto'));
								  if (rec.get('parametro') == 'postCode') form.findField('GCCAP').setValue(rec.get('contenuto'));
								  
								  //if (rec.get('parametro') == 'municipality') form.findField('GCTEL').setValue(rec.get('contenuto'));								  								  						           
								  //if (rec.get('parametro') == 'municipality') form.findField('GCLOCA').setValue(rec.get('contenuto'));								  								  						            								  								  						           						            						            						            								  
								  
								  if (rec.get('parametro') == 'municipality') 	tmp_comune = rec.get('contenuto');
								  if (rec.get('parametro') == 'province') 		tmp_provincia = rec.get('contenuto');
								  
								},this);
								
								Ext.Ajax.request({
								   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=verify_set_localita',
								   method: 'POST',
								   jsonData: {
								   		comune: tmp_comune,
								   		provincia: tmp_provincia
								   	}, 
								   
								   success: function(response, opts) {
								   	  Ext.getBody().unmask();
									  var jsonData = Ext.decode(response.responseText);									   	  
     								  if (jsonData.success == true) {
     								  	form.findField('GCLOCA').setValue(tmp_comune);
     								  	form.findField('GCPROV').setValue(tmp_provincia);
     								  	form.findField('GCNAZI').setValue(<?php echo j($cfg_mod_Gest['cod_naz_italia'])?>);
     								  	form.findField('v_regione').setValue(jsonData.data.DES_REG);
     								  	t_win.close();
     								  }	else {
     								  	acs_show_msg_error('Localit&agrave;/Provincia non presente in anagrafica');
     								  }
								   }, 
								   failure: function(response, opts) {
								      Ext.getBody().unmask();
								      alert('error');
								   }
								});									
									
				            }
				           }				        
				        
				        
				        
				 <?php } ?>
				        
				    ]
											    
				    		
	 	}
	]
   }
	
	<?php
	exit;
}





// ******************************************************************************************
// CERVED
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved'){
$m_params = acs_m_params_json_decode();

$service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";

$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $service_url,
		CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_USERAGENT => 'Codular Sample cURL Request'
));
// Send the request & save response to $resp
$resp = curl_exec($curl);

$resp_json = json_decode($resp);
$token = $resp_json->access_token;
$token_type = $resp_json->token_type;

// Close request to clear up some resources
curl_close($curl);

$taxCode = $m_params->form_values->GCPIVA; 

//RICHIEDO INFO AZIENDA
$service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies?&taxCode={$taxCode}&registered=N";

$headr = array();
$headr[] = 'Accept: application/json';
$headr[] = 'Authorization: '. $token_type . ' ' . $token;

$curl = curl_init();

// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $service_url,
		CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
		CURLOPT_USERAGENT => 'php curl',
		//CURLOPT_HEADER => true, //per debug
		CURLINFO_HEADER_OUT => true,
		CURLOPT_HTTPHEADER => $headr
));

// Send the request & save response to $resp
$resp = curl_exec($curl);

$headerSent = curl_getinfo($curl, CURLINFO_HEADER_OUT );

$resp_json = json_decode($resp);

//scrivo i parametri ricevuti su IC0
$key_request = write_parametri_cerved_base($resp_json);

// Close request to clear up some resources
curl_close($curl);

$ret['success'] = true;
$ret['key_request'] = $key_request;
$ret['companyId'] = $resp_json->companyInfo[0]->companyId;
echo acs_je($ret);
exit;
}





// ******************************************************************************************
// CERVED (by company)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'call_cerved_by_company'){
	ini_set('max_execution_time', 300000);	
	$m_params = acs_m_params_json_decode();

	$service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";

	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'Codular Sample cURL Request'
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	$token = $resp_json->access_token;
	$token_type = $resp_json->token_type;

	// Close request to clear up some resources
	curl_close($curl);

	$taxCode = $m_params->form_values->GCPIVA;

	//RICHIEDO INFO AZIENDA
	$service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies/{$m_params->companyId}/{$m_params->view_type}";

	$headr = array();
	$headr[] = 'Accept: application/json';
	$headr[] = 'Authorization: '. $token_type . ' ' . $token;

	$curl = curl_init();

	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $service_url,
			CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
			CURLOPT_USERAGENT => 'php curl',
			//CURLOPT_HEADER => true, //per debug
			CURLINFO_HEADER_OUT => true,
			CURLOPT_HTTPHEADER => $headr
	));

	// Send the request & save response to $resp
	$resp = curl_exec($curl);

	$resp_json = json_decode($resp);
	
	//scrivo i parametri ricevuti su IC0
	if ($m_params->view_type == 'details')
		$key_request = write_parametri_cerved_details($resp_json);
	if ($m_params->view_type == 'scores')
		$key_request = write_parametri_cerved_scores($resp_json);			
	
	// Close request to clear up some resources
	curl_close($curl);
	
	$ret['success'] = true;
	$ret['key_request'] = $key_request;
	$ret['companyId'] = $resp_json->companyInfo->companyId;	
	echo acs_je($ret);
	exit;
}
	






// ******************************************************************************************
// tree grid data
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    
    $sql_where = sql_where_params($m_params->form_ep);
    
    
    $area_manager = $m_params->form_ep->CCARMA;
    $agente = $m_params->form_ep->CCAGE;
	
	if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
	    
	    
	    if(isset($area_manager) && count($area_manager) > 0
	        || isset($agente) && count($agente) > 0 ){
	            
            $where = '';
            if(count($area_manager) > 0)
                $where .= sql_where_by_combo_value('CCARMA', $area_manager);
            if(count($agente) > 0){
                if(count($agente) == 1)
                    $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
                if(count($agente) > 1)
                    $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
            }
	                
            $sql_join = " INNER JOIN (
            SELECT CCPROG
            FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']}
            WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
            ON CC.CCPROG = GC.GCPROG";
	    }
	    
	   /* $tab_schede = $main_module->find_TA_std('SCCLI');
	    $ar_schede = array();
	    foreach($tab_schede as $v)
            $ar_schede[] = $v['id'];
        unset($ar_schede['XSPM']);
        $sche_where = sql_where_by_combo_value('TA_SCHEDE.TAID' , $ar_schede);
	        
        $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_tab_sys']} TA_SCHEDE
	        WHERE TA_SCHEDE.TADT = GC.GCDT AND TA_SCHEDE.TACOR1 = GC.GCCDCF {$sche_where}) AS NR_SCHEDE";
	        
        $sql_campi_select .= " , (SELECT COUNT(*) FROM {$cfg_mod_Gest['file_tab_sys']} XS
	        WHERE XS.TADT = GC.GCDT AND SUBSTRING(XS.TAREST, 35, 9) = GC.GCCDCF AND XS.TAID = 'XSPM') AS NR_XSPM";
	    */
	    
	    $sql = "SELECT GC.*, CF.* 
	            FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
	            LEFT OUTER JOIN {$cfg_mod_Gest['file_anag_cli']} CF
	              ON CF.CFDT = GC.GCDT AND digits(CF.CFCD) = GC.GCCDCF
	            {$sql_join}
	            WHERE /*GCFG01 NOT IN ('I', 'U', 'H')*/ GCFG01 <> 'A'
                AND GCDT = '{$id_ditta_default}' AND GCTPAN = 'FOR'
                {$sql_where}
	            ORDER BY GCDTGE DESC
	            LIMIT 50";
	    
	}else{
	    
	    $prog = $_REQUEST['node'];
	    
	    $rowProg = get_gcrow_by_prog($prog);
	    //prima rieseguo un refresh per sicurezza
	    $sh = new SpedHistory($m_DeskAcq);
	    $return_RI = $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'EXP_ANAG_CLI',
	            "vals" => array(
	                "RICVES" => $rowProg['GCCDCF']
	            )
	        )
	        );
	    
	    
	    $sql = "SELECT GC.*
	            FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
	            WHERE /*GCFG01 NOT IN ('I', 'U', 'H')*/ GCFG01 <> 'A'
	            AND GCDT = '{$id_ditta_default}' AND GCTPAN IN ('DES', 'PVEN') AND GCPRAB = {$prog}
	            ORDER BY GCTPAN, GCTPIN
	            LIMIT 100";
	    
	}

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$ar = array();
	
	$sql_c = "SELECT COUNT(*) AS C_ROW
	          FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
	          WHERE GCDT = '{$id_ditta_default}' AND GCTPAN IN ('DES', 'PVEN') AND GCPRAB = ?
	";
	
	$stmt_c = db2_prepare($conn, $sql_c);
	echo db2_stmt_errormsg();
		
    while ($r = db2_fetch_assoc($stmt)) {
						
			$tmp_ar_id = array();
			$ar_r = &$ar;			
									
			$cod_liv0 = trim($r['GCPROG']); 	//Progressivo	
			
			$liv = $cod_liv0;
			$tmp_ar_id[] = $liv;
			if (!isset($ar_r["{$liv}"])){
			    $ar_new = array();
			    $ar_new = $r;
			    $ar_new['id'] = implode("|", $tmp_ar_id);
			    $ar_new['liv'] = 'liv_2';
			    $ar_new['liv_cod'] = $liv;
			    $ar_new['liv_cod_out'] = $liv;
			    $ar_new['task'] = acs_u8e($r['GCDCON']);
			    $ar_new['sosp'] = trim($r['GCSOSP']);
			    $ar_new['ciclo']  = trim($r['GCCIVI']);
			    $deskGest = new DeskGest();
			    $ha_commenti = $deskGest->has_commento_cliente($r['GCCDCF']);
			    $ar_new['note'] =  $ha_commenti;
			    $ar_new['cliente'] =  $r['GCCDCF'];
			    if(trim($r['GCCDFI']) == '')
			        $ar_new['cod_fi'] =  $r['GCPIVA'];
		        else
		            $ar_new['cod_fi'] =  $r['GCCDFI'];
	            if (in_array(trim($r['GCTPAN']), array('DES', 'PVEN'))){
	                
	                $ar_new['leaf'] = true;
	                $ar_new['iconCls'] = 'icon-delivery-16';
	                $ar_new['liv'] = 'liv_4';
	                
	                $ar_new['t_indi'] = trim($r['GCTPIN']);
	                if($ar_new['t_indi'] == ''){
	                    $row_d = $main_module->get_TA_std('INDI', 'D');
	                    $ar_new['d_indi'] = $row_d['TADESC'];
	                }else{
	                    $row_d = $main_module->get_TA_std('INDI', $ar_new['t_indi']);
	                    $ar_new['d_indi'] = $row_d['TADESC'];
	                }
	                
	            }else{
	                $ar_new['liv'] = 'liv_2';
	                
	                $result = db2_execute($stmt_c, array($r['GCPROG']));
	                $row_c = db2_fetch_assoc($stmt_c);
	                if($row_c['C_ROW'] == 0){
	                    $ar_new['leaf'] = true;
	                    $ar_new['iconCls'] = 'icon-folder_grey-16';
	                }   
	            }
			            
			            
			            $ar_r["{$liv}"] = $ar_new;
			}
			$ar_r = &$ar_r["{$liv}"];
			$ar_liv2 = &$ar_r;
		
				
	}
	
	$ret = array();
	foreach($ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
		//$ret = $ret[0]['children'][0]['children'][0]['children'];
	    $ar= $ar[$_REQUEST['node']]['children'];
	}	
	
	 echo acs_je(array('success' => true, 'children' => $ret));	
	
	exit;
} //get_json_data



// ******************************************************************************************
// EXE SAVE NEW ANAGRAGICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_new_anag'){
	
	$m_params = acs_m_params_json_decode();	
	
	$ar_ins = array();
	$ret = array();
	
	
	$ar_ins['GCUSUM'] = trim($auth->get_user());
	$ar_ins['GCDTUM'] = oggi_AS_date();
	$ar_ins['GCORUM'] = oggi_AS_time();
	
	
	$r_comune  	= $s->get_TA_std('ANCOM', trim($m_params->form_values->GCLOCA));
	$r_regione 	= $s->get_TA_std_by_des('ANREG', trim($m_params->form_values->v_regione));
	$r_prov 	= $s->get_TA_std('ANPRO', trim($m_params->form_values->GCPROV));
	
	$ar_ins['GCUSGE'] = substr(trim($auth->get_user()), 0, 8);
	$ar_ins['GCDTGE'] = oggi_AS_date();
	$ar_ins['GCORGE'] = oggi_AS_time();	
	
	$ar_ins['GCDCON'] 	= utf8_decode(trim($m_params->form_values->GCDCON));
	$ar_ins['GCINDI'] 	= utf8_decode(trim($m_params->form_values->GCINDI));
	$ar_ins['GCCAP'] 	= trim($m_params->form_values->GCCAP);
	
	$ar_ins['GCKMIT'] 	= sql_f(trim($m_params->form_values->GCKMIT));
	
	//$ar_ins['GCLOCA'] 	= trim($r_comune['TADESC']);
	$ar_ins['GCLOCA']	= utf8_decode(trim($m_params->form_values->GCLOCA));
	$ar_ins['GCPROV'] 	= utf8_decode(trim($m_params->form_values->GCPROV)); //trim($r_comune['TAKEY2']);
	$ar_ins['GCNAZI'] 	= utf8_decode(trim($m_params->form_values->GCNAZI));
	$ar_ins['GCCISN'] 	= trim($r_comune['TAKEY4']);   //Nazione
	
	$ar_ins['GCZONA'] 	= trim($r_regione['TANAZI']);   //Zona
	$ar_ins['GCCITI'] 	= trim($r_prov['TANAZI']);   	//Itinerario
	
	$ar_ins['GCPIVA'] 	= utf8_decode(trim($m_params->form_values->GCPIVA));
	$ar_ins['GCCDFI'] 	= utf8_decode(trim($m_params->form_values->GCCDFI));
	
	$ar_ins['GCMAIL'] 	= utf8_decode(trim($m_params->form_values->GCMAIL));
	$ar_ins['GCWWW'] 	= utf8_decode(trim($m_params->form_values->GCWWW));
	$ar_ins['GCMAI1'] 	= utf8_decode(trim($m_params->form_values->GCMAI1));
	$ar_ins['GCMAI2'] 	= utf8_decode(trim($m_params->form_values->GCMAI2));
	$ar_ins['GCMAI3'] 	= utf8_decode(trim($m_params->form_values->GCMAI3));
	$ar_ins['GCMAI4'] 	= utf8_decode(trim($m_params->form_values->GCMAI4));
	
	$ar_ins['GCTEL'] 	= utf8_decode(trim($m_params->form_values->GCTEL));	
	$ar_ins['GCTEL2'] 	= utf8_decode(trim($m_params->form_values->GCTEL2));
	$ar_ins['GCFAX'] 	= utf8_decode(trim($m_params->form_values->GCFAX));
	
	//destinazione standard/alternativa
	$ar_ins['GCTPDS'] 	= utf8_decode(trim($m_params->form_values->GCTPDS));

	if ($m_params->form_values->mode == 'NEW') {
		$ar_ins['GCDT'] 	= $id_ditta_default;
		$ar_ins['GCPROG'] 	= $main_module->next_num('ANAG_GC');
		
		if ($m_params->form_values->tipo_anagrafica == 'DES'){
			$ar_ins['GCTPAN'] 	= 'DES';
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else if ($m_params->form_values->tipo_anagrafica == 'PVEN'){
			$ar_ins['GCTPAN'] 	= 'PVEN';
			$ar_ins['GCPRAB'] 	= $m_params->open_request->parent_prog;
		} else {
			$ar_ins['GCTPAN'] 	= 'FOR';
			$ar_ins['GCDEFA'] 	= $m_params->open_request->modan;
		}
		
	
		//insert riga
		$sql = "INSERT INTO {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
		$ret['GCPROG'] = $ar_ins['GCPROG'];		
	}
	

	if ($m_params->form_values->mode == 'EDIT') {
		//update riga
	    $sql = "UPDATE {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE GCDT = '{$id_ditta_default}' AND GCPROG = ?";
	   
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_ins,
				array($m_params->form_values->GCPROG)
				));
		echo db2_stmt_errormsg($stmt);
		
		$ret['success'] = true;
		$ret['GCPROG'] = $m_params->form_values->GCPROG;
	}

	//call AGG tramite RI
	//NUOVA VERSIONE ATTRAVERSO RI
	$sh = new SpedHistory($m_DeskAcq);
	$return_RI = $sh->crea(
	    'pers',
	    array(
	        "messaggio"	=> 'AGG_ANAG_CLI',
	        "vals" => array(
	            "RIPROG" => $ret['GCPROG'],
	            "RINOTE" => 'save_anag'
	        )
	    )
	    );
	
	if ($m_params->form_values->mode == 'NEW')
	    $prog = $ar_ins['GCPROG'];
    else
        $prog = $m_params->form_values->GCPROG;
    
    $record = get_gcrow_by_prog($prog);
	        
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory($m_DeskAcq);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'EXP_ANAG_CLI',
            "vals" => array(
                "RICVES" => sprintf("%09d", $record['GCCDCF'])
            )
        )
        );
   
    $record = get_gcrow_by_prog($prog);
    $record = array_map('rtrim', $record);
    $ret['record'] = $record;
     
	echo acs_je($ret);
    exit;
}


function exe_save_logis_add_field($ar, $m_params){
	$ar['GCLING'] 	= utf8_decode($m_params->form_values->GCLING);
	$ar['GCSEL3'] 	= utf8_decode($m_params->form_values->GCSEL3);
	$ar['GCCFGC'] 	= utf8_decode($m_params->form_values->GCCFGC);
	$ar['GCNAZO'] 	= utf8_decode($m_params->form_values->GCNAZO);
	$ar['GCFMNC'] 	= utf8_decode($m_params->form_values->GCFMNC);
	$ar['GCICON'] 	= utf8_decode($m_params->form_values->GCICON);
	$ar['GCIFAT'] 	= utf8_decode($m_params->form_values->GCIFAT);
	$ar['GCIPSP'] 	= utf8_decode($m_params->form_values->GCIPSP);
	$ar['GCGGS1'] 	= utf8_decode($m_params->form_values->GCGGS1);
	$ar['GCGGS2'] 	= utf8_decode($m_params->form_values->GCGGS2);
	$ar['GCGGS3'] 	= utf8_decode($m_params->form_values->GCGGS3);
	$ar['GCGGS4'] 	= utf8_decode($m_params->form_values->GCGGS4);
	$ar['GCGGS5'] 	= utf8_decode($m_params->form_values->GCGGS5);
	$ar['GCGGS6'] 	= utf8_decode($m_params->form_values->GCGGS6);
	$ar['GCNOSC'] 	= utf8_decode($m_params->form_values->GCNOSC);	
	return $ar;
}




function exe_save_cc_add_field($ar, $m_params){
    $ar['CCAG1'] 	= utf8_decode($m_params->form_values->CCAG1);
    $ar['CCAG2'] 	= utf8_decode($m_params->form_values->CCAG2);
    $ar['CCAG3'] 	= utf8_decode($m_params->form_values->CCAG3);
    $ar['CCPA1'] 	= sql_f($m_params->form_values->CCPA1);
    $ar['CCPA2'] 	= sql_f($m_params->form_values->CCPA2);
    $ar['CCPA3'] 	= sql_f($m_params->form_values->CCPA3);
    $ar['CCARMA'] 	= sql_f($m_params->form_values->CCARMA);
    
    $ar['CCPAGA'] 	= utf8_decode(trim($m_params->form_values->CCPAGA));
    $ar['CCNGPA'] 	= sql_f($m_params->form_values->CCNGPA);
    $ar['CCMME1'] 	= sql_f($m_params->form_values->CCMME1);
    $ar['CCMME2'] 	= sql_f($m_params->form_values->CCMME2);
    $ar['CCGGE1'] 	= sql_f($m_params->form_values->CCGGE1);
    $ar['CCGGE2'] 	= sql_f($m_params->form_values->CCGGE2);
    $ar['CCTSIV'] 	= utf8_decode(trim($m_params->form_values->CCTSIV));
    
    $ar['CCREFE'] 	= utf8_decode(trim($m_params->form_values->CCREFE));
    $ar['CCLIST'] 	= utf8_decode(trim($m_params->form_values->CCLIST));
    $ar['CCCATR'] 	= utf8_decode(trim($m_params->form_values->CCCATR));
    $ar['CCSC1'] 	= sql_f($m_params->form_values->CCSC1);
    $ar['CCSC2'] 	= sql_f($m_params->form_values->CCSC2);
    $ar['CCSC3'] 	= sql_f($m_params->form_values->CCSC3);
    $ar['CCSC4'] 	= sql_f($m_params->form_values->CCSC4);
    $ar['CCSC5'] 	= sql_f($m_params->form_values->CCSC5);
    $ar['CCSC6'] 	= sql_f($m_params->form_values->CCSC6);
    $ar['CCSC7'] 	= sql_f($m_params->form_values->CCSC7);
    $ar['CCSC8'] 	= sql_f($m_params->form_values->CCSC8);
    
    $ar['CCTCON'] 	= utf8_decode($m_params->form_values->CCTCON);
    $ar['CCGRDO'] 	= utf8_decode($m_params->form_values->CCGRDO);
    $ar['CCDIVI'] 	= utf8_decode($m_params->form_values->CCDIVI);
    if(strlen($m_params->form_values->CCDTVI) > 0)
        $ar['CCDTVI'] 	= $m_params->form_values->CCDTVI;
    if(strlen($m_params->form_values->CCDTVF) > 0)
        $ar['CCDTVF'] 	= $m_params->form_values->CCDTVF;
    $ar['CCFGR1'] 	= utf8_decode($m_params->form_values->CCFGR1);
    $ar['CCFGR2'] 	= utf8_decode($m_params->form_values->CCFGR2);
    $ar['CCFGR3'] 	= utf8_decode($m_params->form_values->CCFGR3);
    $ar['CCDES1'] 	= utf8_decode($m_params->form_values->CCDES1);
    $ar['CCDES2'] 	= utf8_decode($m_params->form_values->CCDES2);
    $ar['CCDES3'] 	= utf8_decode($m_params->form_values->CCDES3);
    $ar['CCDES4'] 	= utf8_decode($m_params->form_values->CCDES4);
    $ar['CCDES5'] 	= utf8_decode($m_params->form_values->CCDES5);
    $ar['CCTMOD'] 	= utf8_decode($m_params->form_values->CCTMOD);
    $ar['CCCPRO'] 	= utf8_decode($m_params->form_values->CCCPRO);
    $ar['CCPRZO'] 	= utf8_decode($m_params->form_values->CCPRZO);
    $ar['CCSCIM'] 	= utf8_decode($m_params->form_values->CCSCIM);
    $ar['CCEINT'] 	= utf8_decode($m_params->form_values->CCEINT);
    $ar['CCAZSA'] 	= utf8_decode($m_params->form_values->CCAZSA);
    $ar['CCAZSD'] 	= utf8_decode($m_params->form_values->CCAZSD);
    $ar['CCAZPR'] 	= utf8_decode($m_params->form_values->CCAZPR);
            
    if (strlen(trim($m_params->form_values->CCVALU)) > 0)
        $ar['CCVALU'] 	= $m_params->form_values->CCVALU;
                
    if (strlen(trim($m_params->form_values->CCABI)) > 0)
        $ar['CCABI'] 	= sprintf("%05s", trim($m_params->form_values->CCABI));
        else
        $ar['CCABI'] = '';
                        
    if (strlen(trim($m_params->form_values->CCCAB)) > 0)
        $ar['CCCAB'] 	= sprintf("%05s", trim($m_params->form_values->CCCAB));
    else
        $ar['CCCAB'] = '';
                                
    if (strlen(trim($m_params->form_values->CCCOCO)) > 0)
        if (strlen(trim($m_params->form_values->CCCOCO)) < 12)
            $ar['CCCOCO'] 	= sprintf("%012d", $m_params->form_values->CCCOCO);
        else
            $ar['CCCOCO'] = trim($m_params->form_values->CCCOCO);
    else
        $ar['CCCOCO'] = '';
                                                
    if (strlen(trim($m_params->form_values->CCIBAN)) > 0)
        $ar['CCIBAN'] 	= $m_params->form_values->CCIBAN;
        $ar['CCBANC'] 	= $m_params->form_values->CCBANC;
  return $ar;
}



function exe_save_fido_add_field($ar, $m_params){
	$ar['CCDTVI'] 	= $m_params->form_values->CCDTVI;
	$ar['CCDTVF'] 	= $m_params->form_values->CCDTVF;
	$ar['CCIMFD'] 	= sql_f($m_params->form_values->CCIMFD);

	return $ar;
}


// ******************************************************************************************
// EXE SAVE CONDIZIONE COMMERCIALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_cc'){
    
    $m_params = acs_m_params_json_decode();
    
    $ar_ins = array();
    $ret = array();
    
    
    
    if ($m_params->open_request->mode == 'NEW' || $m_params->open_request->mode == 'CREATE') {
        
        
        $sql_cc = "SELECT COUNT(*) AS C_ROW
        FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']} CC
        WHERE CCDT = '{$id_ditta_default}' AND CCPROG = '{$m_params->form_values->CCPROG}'
        AND CCSEZI = 'BCCO' AND CCCCON = '{$m_params->form_values->CCCCON}'";
        
        $stmt = db2_prepare($conn, $sql_cc);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if($row['C_ROW'] > 0){
            $ret['success'] = false;
            $ret['msg_error'] = 'Condizione commerciale gi&agrave; esistente';
            echo acs_je($ret);
            return;
        }
        
        
        
        $ar_ins['CCUSGE'] = trim($auth->get_user());
        $ar_ins['CCDTGE'] = oggi_AS_date();
        $ar_ins['CCORGE'] = oggi_AS_time();
        
        $ar_ins['CCDT'] 	= $id_ditta_default;
        $ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
        $ar_ins['CCSEZI'] 	= 'BCCO';
        $ar_ins['CCCCON'] 	= utf8_decode(trim($m_params->form_values->CCCCON));
        
        //Descrizione separata in nome (30) + articolo (30)
        $ar_ins['CCDCON'] 	= sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON))) .
        sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON_ART)));
        
        $ar_ins = exe_save_cc_add_field($ar_ins, $m_params);
        
        //insert riga
        $sql = "INSERT INTO {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg($stmt);
        
        $ret['success'] = true;
    }
    
    
    if ($m_params->open_request->mode == 'EDIT') {
        $ar_upd = exe_save_cc_add_field(array(), $m_params);
        
        $ar_upd['CCUSGE'] = trim($auth->get_user());
        $ar_upd['CCDTGE'] = oggi_AS_date();
        $ar_upd['CCORGE'] = oggi_AS_time();
        
        if (trim($m_params->form_values->CCCON) != 'STD'){
            $ar_upd['CCDCON'] 	= sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON))) .
            sprintf("%-30s", utf8_decode(trim($m_params->form_values->CCDCON_ART)));
        }
        
        //update riga
        $sql = "UPDATE {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']}
        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array_merge(
            $ar_upd,
            array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, $m_params->form_values->CCSEZI, $m_params->form_values->CCCCON)
            ));
        echo db2_stmt_errormsg($stmt);
        
        $ret['success'] = true;
    }
    
    
    //UPDATE GC0
    $ar_upd1 = array();
    if(isset($m_params->form_values->GCCATC) || isset($m_params->form_values->GCCACPC)
        || isset($m_params->form_values->GCIVES)){
            
        $ar_upd1['GCUSGE'] = trim($auth->get_user());
        $ar_upd1['GCDTGE'] = oggi_AS_date();
        $ar_upd1['GCORGE'] = oggi_AS_time();
        if(strlen($m_params->form_values->GCCATC) > 0)
            $ar_upd1['GCCATC'] = $m_params->form_values->GCCATC;
        if(strlen($m_params->form_values->GCCAPC) > 0)
            $ar_upd1['GCCAPC'] = $m_params->form_values->GCCAPC;
        if(strlen($m_params->form_values->GCIVES) > 0)
            $ar_upd1['GCIVES'] = $m_params->form_values->GCIVES;
        //update riga
        $sql = "UPDATE {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']}
                SET " . create_name_field_by_ar_UPDATE($ar_upd1) . "
                WHERE GCDT=? AND GCPROG=?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array_merge(
            $ar_upd1,
            array($id_ditta_default, $m_params->form_values->CCPROG)
            ));
        echo db2_stmt_errormsg($stmt);
                        
    }
    
    //call AGG tramite RI
    //NUOVA VERSIONE ATTRAVERSO RI
    $sh = new SpedHistory($m_DeskAcq);
    $return_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'AGG_ANAG_CLI',
            "vals" => array(
                "RIPROG" => $m_params->form_values->CCPROG,
                "RINOTE" => 'save_cc'
            )
        )
        );
    
    echo acs_je($ret);
    exit;
}






// ******************************************************************************************
// EXE SAVE FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_fido'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();



	if ($m_params->open_request->mode == 'NEW') {

	    $ar_ins['CCUSGE'] = substr(trim($auth->get_user()), 0, 8);
		$ar_ins['CCDTGE'] = oggi_AS_date();
		$ar_ins['CCORGE'] = oggi_AS_time();

		$ar_ins['CCDT'] 	= $id_ditta_default;
		$ar_ins['CCPROG'] 	= utf8_decode(trim($m_params->form_values->CCPROG));
		$ar_ins['CCSEZI'] 	= 'BFID';
		$ar_ins['CCCCON'] 	= utf8_decode(trim($m_params->form_values->CCCCON));
		$ar_ins['CCDCON'] 	= utf8_decode(trim($m_params->form_values->CCDCON));

		$ar_ins = exe_save_fido_add_field($ar_ins, $m_params);

		//insert riga
		$sql = "INSERT INTO {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}


	if ($m_params->open_request->mode == 'EDIT') {
		$ar_upd = exe_save_fido_add_field(array(), $m_params);

		$ar_upd['CCUSGE'] = substr(trim($auth->get_user()), 0, 8);
		$ar_upd['CCDTGE'] = oggi_AS_date();
		$ar_upd['CCORGE'] = oggi_AS_time();

		//update riga
		$sql = "UPDATE {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']}
		SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
				WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON = ?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_upd,
				array($m_params->form_values->CCDT, $m_params->form_values->CCPROG, $m_params->form_values->CCSEZI, $m_params->form_values->CCCCON)
		));
		echo db2_stmt_errormsg($stmt);

		$ret['success'] = true;
	}

	echo acs_je($ret);
	exit;
}






// ******************************************************************************************
// EXE SAVE FORM LOGISTICA/CONFIGURAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_logis'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	$ar_upd = exe_save_logis_add_field(array(), $m_params);
	
	$ar_upd['GCUSGE'] = substr(trim($auth->get_user()), 0, 8);
	$ar_upd['GCDTGE'] = oggi_AS_date();
	$ar_upd['GCORGE'] = oggi_AS_time();
	
	
	//update riga
	$sql = "UPDATE {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']}
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE GCDT='$id_ditta_default' AND GCPROG=?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($m_params->form_values->GCPROG)
	));
	echo db2_stmt_errormsg($stmt);

	$ret['success'] = true;


	echo acs_je($ret);
	exit;
}







// ******************************************************************************************
// ELENCO CONDIZIONI COMMERCIALI PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_cc'){

	$m_params = acs_m_params_json_decode();
	
	$ar_ins = array();
	$ret = array();

	$sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID FROM {$cfg_mod_Gest['file_tabelle']} TA
	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']}  AN ON
				CCDT = ? AND CCPROG = ? AND CCSEZI = 'BCCO' AND CCCCON = TAKEY2
			WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ANCCO' AND TAKEY1 = ?
            AND TAKEY2 = 'STD'";
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id, $m_params->GCDEFA ));
	echo db2_stmt_errormsg($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
	    $n = array();
	    $n['codice'] = $row['TAKEY2'];
	    
	    if(trim($row['CCCCON']) == 'STD')
	        $n['descrizione'] = "Condizioni vendita standard/non condizionate";
        else
            $n['descrizione'] = $row['TADESC'];
        if(strlen($row['CCCCON']) > 0)
            $n['caricata'] = $row['CCCCON'];
        else
            $n['caricata'] = "";
        $n['prog'] = $m_params->rec_id;
        $n['cdcf'] = $m_params->cdcf;
        $n['ditta'] = $id_ditta_default;
        $n['condizioni'] = 'BCCO';
        $n['dettagli'] = "";
        if(trim($row['CCAG1']) != ''){
            $ta_sys_ag = find_TA_sys('CUAG', trim($row['CCAG1']));
            $n['dettagli'] .= "Agente: ".$ta_sys_ag[0]['text']." [".trim($row['CCAG1'])."]";
            //CCPA1
        }
        
        if($row['CCPA1'] > 0){
            if(trim($row['CCAG1']) != '')
                $n['dettagli'] .= ", ". n($row['CCPA1'],0)."%";
                else
                    $n['dettagli'] .= "Agente: , ". n($row['CCPA1'],0)."%";
        }
        
        if(trim($row['CCREFE']) != ''){
            $ta_sys_rf = find_TA_sys('BREF', trim($row['CCREFE']));
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Referente: ".$ta_sys_rf[0]['text']." [".trim($row['CCREFE'])."]";
        }
        if(trim($row['CCPAGA']) != ''){
            $ta_sys_pg = find_TA_sys('CUCP', trim($row['CCPAGA']));
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Pagamento: ".$ta_sys_pg[0]['text']." [".trim($row['CCPAGA'])."]";
        }
        
        //des_banca
        if (strlen(trim($row['CCABI'])) > 0 && strlen(trim($row['CCCAB'])) > 0) {
            $sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($row['CCABI'], $row['CCCAB']));
            $r_cab = db2_fetch_assoc($stmt);
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Banca: ".trim($r_cab['XDSABI']) ."-".trim($r_cab['XDSCAB']);
        }
        
        if(trim($row['CCSC1']) > 0){
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Sconti: ".n($row['CCSC1'],2)."+".n($row['CCSC2'],2)."+".n($row['CCSC3'],2)."+".n($row['CCSC4'],2);
        }
        
        if(trim($row['CCLIST']) != ''){
            if(trim($row['CCSC1']) > 0)
                $n['dettagli'] .= " [Listino ".trim($row['CCLIST'])."]";
                else
                    $n['dettagli'] .= "<br> [Listino ".trim($row['CCLIST'])."]";
        }
        
        if(trim($row['GCIVES']) != ''){
            $ta_sys_as = find_TA_sys('CUAE', trim($row['GCIVES']));
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Assoggettamento: ".$ta_sys_as[0]['text']." [".trim($row['GCIVES'])."]";
        }
        
        if(trim($row['CCVALU']) != ''){
            $ta_sys_val = find_TA_sys('VUVL', trim($row['CCVALU']));
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Valuta: ".$ta_sys_val[0]['text']." [".trim($row['CCVALU'])."]";
        }
        
        if($row['CCDTVI'] > 0 || $row['CCDTVF'] > 0)
            $n['val'] = "Dal: ".print_date($row['CCDTVI']). "<br> Al: ".print_date($row['CCDTVF']);
            if($row['CCDTVF'] > 0 && $row['CCDTVF'] < oggi_AS_date())
                $n['scaduta'] = 'Y';
                
            $n['tipo'] = '1) Predefinite';
            $ret[] = $n;
	}
	
	

	echo acs_je($ret);
	exit;
}







// ******************************************************************************************
// ELENCO CONDIZIONI AMMINISTRATIVE PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_fido'){

	$m_params = acs_m_params_json_decode();

	$ar_ins = array();
	$ret = array();

	$sql = "SELECT TA.*, AN.*, '{$m_params->rec_id}' AS Q_REC_ID FROM {$cfg_mod_DeskAcq['file_tabelle']} TA
	        LEFT OUTER JOIN {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']}  AN ON
			CCDT = ? AND CCPROG = ? AND CCSEZI = 'BFID' AND CCCCON = TAKEY1
			WHERE TADT = '{$id_ditta_default}' AND TATAID = 'ANC05'";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($id_ditta_default, $m_params->rec_id));
	echo db2_stmt_errormsg($stmt);

	while ($row = db2_fetch_assoc($stmt)) {
		$ret[] = $row;
	}

	echo acs_je($ret);
	exit;
}





// ******************************************************************************************
// OPEN TAB
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){ ?>
{
 success:true, 
 items: [
   {
 	xtype: 'panel',
 	layout: {
	    type: 'hbox',
	    align: 'stretch',
	    pack : 'start',
	},
    <?php echo make_tab_closable(); ?>,	
    title: 'Anagrafica fornitori',
    tbar: new Ext.Toolbar({
      items:[
            '<b>Inserimento/Manutenzione anagrafica fornitori punto vendita</b>', '->',
            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').expandAll();}}
            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').collapseAll();}}
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
      ]            
    }),  
	
	
	items: [  
  		 <?php write_main_tree(array(), $m_params->form_open, $m_params->recenti); ?>
  	    , <?php // write_main_form(array()); ?>
  	]
  }
 ]
}
<?php exit; } 
// ******************************************************************************************
// NUOVA ANAGRAFICA
// ******************************************************************************************
$m_params = acs_m_params_json_decode();
if ($_REQUEST['fn'] == 'new_anag_cli' && $m_params->i_e == 'I'){ ?>
{
 success:true,  
 items: [
  	  {
		xtype: 'tabpanel',
		GCPROG: <?php echo j($m_params->rec_id) ?>,
		
		original_record: [],
		
		<?php
		$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
		
		//se rientro in modifica, ricarico il record		
		if ($all_params['mode'] == 'EDIT') {
			$row = get_gcrow_by_prog($all_params['rec_id']);	
		
		} else {
		    //NEW
		    $row = array();
		    
		    if ($m_params->i_e == 'I')
		        $row['GCNAZI'] = $cfg_mod_Gest['cod_naz_italia'];
		}
		?>
			
		
		items: [
		
		   	<?php write_new_form_ITA(
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>, 	

		    <?php write_new_list_COMM(
		   			array(),
		   			$all_params,
		   			$row
		   	); ?>, 
	   	  	
	   	  	<?php write_new_form_CRM(
	   			array(),
	   			$all_params,
	   			$row
	       	); ?>
		   	
		],
		
		listeners : {
		   afterrender: function (comp) {
		   <?php  
		   if($all_params['mode'] == 'EDIT'){
		       $nrow = get_gcrow_by_prog($row['GCPROG']);
		       $title = "[".$nrow['GCCDCF'].", ".trim($nrow['GCCINT'])."] ".trim($nrow['GCDCON']);
		       ?>
		       
	           comp.up('window').setTitle('Modifica anagrafica ' + <?php echo j($title); ?>);	 				
		 	<?php }?>
		 	
		 	},
		 	
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
		
		, acs_actions : {
		   
		   exe_afterSave: function (m_window){
		        var tabPanel = m_window.down('tabpanel');
		        var activeTab = tabPanel.getActiveTab();
                var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                tabPanel.setActiveTab(activeTabIndex + 1);
                
                if(activeTabIndex < tabPanel.items.length - 1){
                	tabPanel.setActiveTab(activeTabIndex + 1);
                } else {
                	m_window.close();
					acs_show_msg_info('Salvataggio completato');
                }
                
           }
		   
		   
		   
		   }
	}
 ]
}  	
<?php exit; } ?>
<?php


// ******************************************************************************************
// OPEN FORM COMM
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_COMM'){ ?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'panel',
		layout: 'fit',
		items: [
				
			   	<?php write_new_form_COMM(
			   			array(),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } 

function write_new_list_COMM($p, $all_params, $row){
    global $s;
    
    ?>
			{
				xtype: 'grid',
				cls: '',
		        flex: 1,
		        loadMask: true,
		        title: 'Condizioni commerciali',
		        features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),
			    store: {
					xtype: 'store',
					groupField: 'tipo',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_list_cc',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
			                    extraParams: {
										rec_id: <?php echo j($all_params['rec_id']) ?>,
										GCDEFA: <?php echo j($row['GCDEFA']) ?>,
										cdcf : <?php echo j($all_params['gccdcf']) ?>
			        				}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['cdcf', 'dettagli', 'tipo', 'codice', 'descrizione', 'caricata', 'prog', 'ditta', 'condizioni', 'val', 'scaduta']							
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		  {text: 'Codice', width: 50, dataIndex: 'codice'}
		    		, {text: 'Descrizione', width: 250, dataIndex: 'descrizione', 
		    		   renderer: function (value, metaData, rec){	
		    		       if(rec.get('caricata') != '')	    		
		    					metaData.tdCls += ' grassetto';
		    			   if(rec.get('scaduta') == 'Y'){
		    			    	metaData.tdCls += ' grassetto';
		    			    	return '<span style = "color: red;">'+ value +'</span>';
		    			   }
		    			
		    			return value;
		    						    				
		    			}}
		    		, {text: 'Validit&agrave;', width: 90, dataIndex: 'val'}
		    		, {text: '&nbsp;', flex: 1, dataIndex: 'dettagli'}
				],
				enableSort: true
				
				
	        	, listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
	            			var my_grid = iView;
	            
							my_listeners_add_cc = {
	        					afterEditRecord: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_grid.store.load();		        						
	        						from_win.close();
					        		}
			    				};					  

			    			
			    			if (Ext.isEmpty(rec.get('caricata')) == false) {
			    				//modifica esistente
		            			acs_show_win_std('Modifica condizione commerciale', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				CCDT: rec.get('ditta'),
						  				CCPROG: rec.get('prog'),
						  				CCSEZI: rec.get('condizioni'),
						  				CCCCON: rec.get('codice'),
						  				cdcf : rec.get('cdcf')
						  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');	
						  								  				    				
			    			}
			    			else {
			    				//crea nuovo
		            			acs_show_win_std('Crea nuova condizione commerciale', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
									{
						  				mode: 'NEW',
						  				modan: 'ToDo',
						  				CCDT: rec.get('ditta'),
						  				CCPROG: rec.get('prog'),
						  				CCSEZI: 'BCCO',
						  				CCCCON: rec.get('codice'),
						  				CCDCON: rec.get('descrizione'),
						  				cdcf : rec.get('cdcf')
						  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');			    				
			    			}	
			    				
						  	
						  }
					  }
				}
				

			/*, buttons: [
					{
							xtype: 'button',
				            text: 'Nuova condizione', 
				            iconCls: 'icon-outbox-32', scale: 'large',
				            handler: function() {
				            
		            			var my_grid = this.up('grid');
		            
								my_listeners_add_cc = {
		        					afterEditRecord: function(from_win){	
		        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
		        						my_grid.store.load();		        						
		        						from_win.close();
						        		}
				    				};					  

		            		 			acs_show_win_std('Nuova condizione commerciale', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_COMM', 
											{
								  				mode: 'CREATE',
								  				modan: 'ToDo',
								  				CCPROG: my_grid.up('tabpanel').GCPROG
								  			}, 750, 600, my_listeners_add_cc, 'icon-globe-16');
				            
				            }
				        }
				          
				    ]*/
											    
				    		
	 	}
<?php
} //write_new_list_COMM





// ******************************************************************************************
// NUOVA DESTINAZIONE - ITALIA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'new_dest' && $m_params->i_e == 'I'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
	    $sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
	            LEFT OUTER JOIN  {$cfg_mod_DeskAcq['file_tabelle']} TA_COM ON
				TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
				LEFT OUTER JOIN   {$cfg_mod_DeskAcq['file_tabelle']} TA_REG ON
				TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
				WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($all_params['rec_id']));
		$row = db2_fetch_assoc($stmt);
	}
	
	?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'DES'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>
		]
		 
	  , listeners : {
		   
			'beforetabchange': function(tabp, panTo, panFrom){
				check_tabpanel_change(tabp, panTo, panFrom);
			}
		
		}
		
	  , acs_actions : {
		   
		   exe_afterSave: function (m_window){
		        var tabPanel = m_window.down('tabpanel');
		        var activeTab = tabPanel.getActiveTab();
                var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                tabPanel.setActiveTab(activeTabIndex + 1);
                
                if(activeTabIndex < tabPanel.items.length - 1){
                	tabPanel.setActiveTab(activeTabIndex + 1);
                } else {
                	m_window.close();
					acs_show_msg_info('Salvataggio completato');
                }
                
           }
		   
		   
		   
		   }
	}
 ]
}  	
<?php exit; } ?>

<?php
if ($_REQUEST['fn'] == 'new_pven' && $m_params->i_e == 'I'){

	$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());
	
	//se rientro in modifica, ricarico il record
	if ($all_params['mode'] == 'EDIT') {
	    $sql = "SELECT GC.*, TA_REG.TADESC AS DES_REG FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
	    LEFT OUTER JOIN  {$cfg_mod_DeskAcq['file_tabelle']} TA_COM ON
		TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
		LEFT OUTER JOIN   {$cfg_mod_DeskAcq['file_tabelle']} TA_REG ON
		TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
		WHERE GCDT = '$id_ditta_default' AND GCPROG = ?";
			
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($all_params['rec_id']));
		$row = db2_fetch_assoc($stmt);
	}
	
	
?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'tabpanel',
		items: [
				
			   	<?php write_new_form_ITA(
			   			array(
			   				'tipo_anagrafica' => 'PVEN'		
			   			),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode()),
			   			$row
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } ?>




<?php
// ******************************************************************************************
// OPEN FORM FIDO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_FIDO'){ ?>
{
 success:true, 
 items: [
 
 	{
		xtype: 'panel',
		layout: 'fit',
		items: [
				
			   	<?php write_new_form_FIDO(
			   			array(),
			   			array_merge((array)$_REQUEST, (array)acs_m_params_json_decode())
			   	); ?>
		]
	}
 ]
}  	
<?php exit; } ?>



<?php

/********************************************************************************** 
 WIDGET
 **********************************************************************************/
function write_main_tree($p, $form_ep, $recenti){
    global $s, $main_module;
?>	
			{
				xtype: 'treepanel',
				cls: '',
		        flex: 1,
		        useArrows: true,
		        rootVisible: false,
		        loadMask: true,
		        
		        store: Ext.create('Ext.data.TreeStore', {
	                    autoLoad: true,                    
					    fields: ['iconCls', 'trCls', 'task', 'liv', 'liv_data', 'liv_cod', 'liv_cod_out', 'liv_cod_qtip',
					    	'GCDT', 'GCPROG', 'sosp', 'note', 'cliente', 'GCUSUM', 'GCUSGE', 'GCDTGE', 'GCDTUM', 
					    	'GCDCON', 'GCMAIL', 'GCWWW', 'GCMAI1', 'GCMAI2', 'GCMAI3', 'GCMAI4', 'GCTEL', 'GCTEL2', 'GCFAX',
					    	'GCINDI', 'GCCDCF', 'GCCDFI', 'GCPIVA', 'GCLOCA', 'GCTPAN', 'GCPROV', 'ciclo',
					    	'GCUSUM', 'GCUSGE', 'GCDTGE', 'GCDTUM', 'cod_fi', 'GCCRM1', 't_indi', 'd_indi'
					    ],
					                                          
	                    proxy: {
	                        type: 'ajax',
	                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							actionMethods: {read: 'POST'},
							timeout: 240000,
	                        reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'children'						            
						    }, 
							  extraParams: {
	                          form_ep : <?php echo acs_je($form_ep); ?>,
	                          recenti : <?php echo j($recenti); ?>
							}
	                	    , doRequest: personalizza_extraParams_to_jsonData        				
	                    }
	                }),
	
	           multiSelect: false,
		        singleExpand: false,
		        
		        <?php  $sosp = "<img src=" . img_path("icone/48x48/divieto.png") . " height=20>"; ?>
		        <?php  $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>"; ?>
		
				columns: [	
				 {text: '<?php echo $sosp; ?>', width: 40, dataIndex: 'sosp', 
					renderer: function(value, metaData, record){
		    			  
		    			   if(record.get('liv') == 'liv_2'){
    		    		 	   if(record.get('ciclo') == '2') 
    		    		 	       metaData.tdCls += ' sfondo_giallo';
    	    		 	       if(record.get('ciclo') == '1' || record.get('ciclo') == '3') 
    		    		 	       metaData.tdCls += ' sfondo_arancione';
		    		 	   }    
		    			  
		    			  if(record.get('sosp') == 'S'){ 
                              metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode('Sospeso') + '\"';
		    			      return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';
		    		 	   }
					 }}
				   ,{
				    text: '<?php echo $note; ?>',
				    width: 32, 
				    tooltip: 'Blocco note',
				    tdCls: 'tdAction',  
				    dataIndex : 'note',       		       		        
					renderer: function(value, metaData, record){
    					if(record.get('liv') == 'liv_2'){
    						if (record.get('note') == true) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
    			    		if (record.get('note') == false) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
    			    	}
			    		
			    	}
			    	}
			    	, {text: 'TI', tooltip : 'Tipo indirizzo', width: 40, dataIndex: 'GCCRM1',
			    	  renderer: function(value, metaData, record){
					      if (record.get('GCCRM1').trim() == 'P' || record.get('GCCRM1').trim() == 'E') 
                    	     return '<img src=<?php echo img_path("icone/48x48/shopping_bag.png") ?> width=18>';
        	          	  if (record.get('GCCRM1').trim() == 'D') 
                    	     return '<img src=<?php echo img_path("icone/48x48/barcode.png") ?> width=18>';
                        
                	}}
		    		, {text: 'Ragione sociale/Denominazione', flex: 1, xtype: 'treecolumn', dataIndex: 'task', menuDisabled: true, tdCls: ' grassetto'}
		    		, {text: 'Localit&agrave;', flex: 1, dataIndex: 'GCLOCA', tdCls: ' grassetto'}					
		    		, {text: 'Indirizzo', flex: 1, dataIndex: 'GCINDI'}
		    		, {text: 'Pr.', width: 40, dataIndex: 'GCPROV', tdCls: ' grassetto'}
					, {text: 'Codice', width: 100, dataIndex: 'GCCDCF', tdCls: ' grassetto'}		    			   	
					, {text: 'Cod.Fiscale', width: 150, dataIndex: 'cod_fi',
    					 renderer: function(value, metaData, record){
    					      if (record.get('cod_fi').trim() != record.get('GCPIVA').trim()) 
                        	          	metaData.tdCls += ' grassetto';
            	          	  return value;	
                    	}}
					, {text: 'Partita IVA', width: 130, dataIndex: 'GCPIVA',
					     renderer: function(value, metaData, record){
					      if (record.get('GCPIVA').trim() != record.get('cod_fi').trim()) 
                    	          	metaData.tdCls += ' grassetto';
        	          	  if (record.get('GCPIVA').trim() == record.get('cod_fi').trim()) 
                    	          	metaData.tdCls += ' grassetto';
                          return value;	
                	}}
					, {header: 'Immissione',
                	 columns: [
                	 {header: 'Data', dataIndex: 'GCDTGE', renderer: date_from_AS, width: 70, sortable : true,
                	 renderer: function(value, metaData, record){
                	         if(record.get('GCDTUM') > 0){
                    	         if (record.get('CFDTGE') != record.get('GCDTUM')) 
                    	          	metaData.tdCls += ' grassetto';
                    	          	
                    	          q_tip = 'Modifica: ' + date_from_AS(record.get('GCDTUM')) + ', Utente ' +record.get('GCUSUM');
                			      metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';  	
                	         }
                              			    	
                		     return date_from_AS(value);	
                	}
                	 
                	 }
                	 ,{header: 'Utente', dataIndex: 'GCUSGE', width: 70, sortable : true,
                	  renderer: function(value, metaData, record){
                	  
                	  	
                	        if(!Ext.isEmpty(record.get('GCUSUM')) && record.get('GCUSUM').trim() != ''){
                	  			if (record.get('GCUSGE') != record.get('GCUSUM')) 
                	          		metaData.tdCls += ' grassetto';
                	          		
                	          q_tip = 'Modifica: ' + date_from_AS(record.get('GCDTUM')) + ', Utente ' +record.get('GCUSUM');
                			  metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';   		
                	        }  
                	  
                              			    	
                	    return value;	
                	} 
                	 }
                	 ]}
					
				],
			    enableSort: true,
	
		        listeners: {
			        	beforeload: function(store, options) {
			        		Ext.getBody().mask('Loading... ', 'loading').show();
			        		},		
				
			            load: function () {
			              Ext.getBody().unmask();
			            },
			          
					    celldblclick: {				  							
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  
						  	rec = iView.getRecord(iRowEl);
						  	var col_name = iView.getGridColumns()[iColIdx].dataIndex;
						  	iEvent.stopEvent();
						  
	            			var my_tree = iView;
							my_listeners_upd_dest = {
	        					afterEditCC: function(from_win){	
	        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
	        						my_tree.getTreeStore().load();		        						
	        						from_win.close();
					        		}
			    				};		
			    				
			    		if(col_name != 'note'){	
			    			
		    				if (rec.get('GCTPAN').trim() == 'DES') {
								acs_show_win_std('Modifica destinazione', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				i_e: 'I', //ToDo
						  				rec_id: rec.get('GCPROG')
						  			}, 750, 550, my_listeners_upd_dest, 'icon-globe-16');							
							 	return;
							}			  

						    //nel caso di anagrafica cliente, prima richiedo aggiornamento della stessa
							if (rec.get('GCTPAN').trim() == 'FOR' && rec.get('GCCDCF').trim().length > 0){
						    	Ext.getBody().mask('Loading... ', 'loading').show();
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_import_anag',
									        timeout: 2400000,
									        jsonData: {
									        	gcprog: rec.get('GCPROG'),
									        	gccdcf: rec.get('GCCDCF').trim() 
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									        	Ext.getBody().unmask();
									            var jsonData = Ext.decode(result.responseText);
																	  						  	
												acs_show_win_std('Modifica anagrafica',  
													'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
													{
										  				mode: 'EDIT',
										  				modan: 'ToDo',
										  				i_e: 'I', //ToDo
										  				rec_id: jsonData.id_prog,
										  				gccdcf: rec.get('GCCDCF').trim()
										  			}, 750, 600, {}, 'icon-globe-16');						  
										  	
										  	return false;									            
									            									            
									        }, //success
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });  						    	
						    	
						    	return;
						    }	

															
													  						  	
								acs_show_win_std('Modifica anagrafica', 
									'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
									{
						  				mode: 'EDIT',
						  				modan: 'ToDo',
						  				i_e: 'I', //ToDo
						  				rec_id: rec.get('id')
						  			}, 750, 550, my_listeners_upd_dest, 'icon-globe-16');						  
						  		  
						  	return false;
						
						}
							if (rec.get('liv') == 'liv_2' && col_name == 'note')
								show_win_bl_cliente(rec.get('cliente'), rec.get('task'));
						  				
						  }
					    },
			            
						itemclick: function(view, record, item, index, e) {
								//form = this.up('panel').down('form');
								//console.log(record);
								//form.loadRecord(record);
						},			            
			          
			          
			          itemcontextmenu : function(grid, rec, node, index, event) {
			          
			                event.stopEvent();
				  													  
					 		var voci_menu = [];
				     		var row = rec.data;
				     		var m_grid = this;
			          
			              if(rec.get('GCTPAN').trim() == 'FOR'){
			                   <?php if(1 == 1 || $cfg_mod_Gest['gestione_avanzata_ciclo_vita'] == 'Y'){?>
                    			   voci_menu.push({
                    	      		text: 'Assegna ciclo di vita',
                    	    		iconCls : 'icon-blog_compose-16',      		
                    	    		handler: function() {
                    	    		    
                    	    		    my_listeners = {
        		        					afterAssegna: function(from_win, ciclo){	
        		        						rec.set('ciclo', ciclo);		        						
        		        						from_win.close();
        						        		}
        				    				};	
                    	    		    
                    	    			acs_show_win_std('Assegna ciclo di vita', '../desk_acq/acs_gestione_ciclo_vita.php?fn=change_ciclo', {prog : rec.get('GCPROG')}, 400, 150,  my_listeners, 'icon-blog_compose-16');	
                    	    		}
                    			  });
            			  	<?php }?>
            			  	
            			  	 var menu = new Ext.menu.Menu({
        				    items: voci_menu
        					}).showAt(event.xy);
			              
			              }
			          			          				                          
			          }
			            	        
			        } //listeners
				

		, buttons: [
			'->'	
		, {
	         	xtype: 'splitbutton',
	            text: 'Crea nuova anagrafica',    //privato azienda
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [
		        	
		        	<?php foreach ($main_module->find_TA_std('MODAN', null, 'N', 'Y') as $ec) { ?> 	
						{
							xtype: 'button',
				            text: <?php echo j(trim($ec['text'])) ?>,
				            //iconCls: 'icon-save-32', 
				            scale: 'large',
				            handler: function() {
				            
				            			var my_tree = this.up('treepanel');
				            
										my_listeners_inserimento = {
				        					afterInsertRecord: function(from_win, record){	
				        						
				        						    var values = record;
				        						    values['out_codice'] = record.GCCDCF;
    					                            values['task'] = record.GCDCON;
    					                            values['liv'] = 'liv_2';
    					                            values['iconCls'] = 'icon-folder_grey-16';
    					                            values['id'] = record.GCPROG;
    					                          	var rootNode = my_tree.getRootNode();
													rootNode.insertChild(0, values); 
												    var new_rec = my_tree.store.getNodeById(values['id']);
												    my_tree.getView().select(new_rec);
			  					   	     			my_tree.getView().focusRow(new_rec);
								        		},
								        		afterEditCC: function(from_win){	
    	        									//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
    	        									my_tree.store.load();		        						
    	        									from_win.close();
					        					}
						    				};					            
										acs_show_win_std('Immissione nuova anagrafica fornitore', 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_anag_cli', 
											{
								  				mode: 'NEW',
								  				modan: <?php echo j(trim($ec['id'])) ?>,
								  				i_e: <?php echo j(trim($ec['TAFG01'])) ?>
								  			}, 750, 550, my_listeners_inserimento, 'icon-globe-16');
				            
				            }
				        },
				       <?php } ?>  
				            	        	
		        	]
		        }
	        }, {
	         	xtype: 'splitbutton',
	            text: 'Aggiungi<br/>destinazione',
	            iconCls: 'icon-button_black_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
		        	items: [{
							xtype: 'button',
				            text: 'Italia', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            				  
						    				          	
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo fornitore dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'FOR'){
				            				acs_show_msg_error('Selezionare un singolo fornitore dall\'elenco');
				            				return false;				            			
				            			}
				            			
				            			var parent_prog = selected_record[0].get('id');		
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();
				            					
				            					
				            			my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						//my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};	
				            						            							            			
										acs_show_win_std('Nuova destinazione [' + cod_cli + '] ' + d_cli, 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'I',
								  				type : 'D',
								  				parent_prog: parent_prog
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }, {
							xtype: 'button',
				            text: 'Estero', 
				            scale: 'large',
				            handler: function() {
				            
				            			my_tree = this.up('treepanel');
				            
										
	          				            selected_record = this.up('treepanel').view.getSelectionModel().getSelection();
	          				            if (selected_record.length != 1){
				            				acs_show_msg_error('Selezionare un singolo fornitore dall\'elenco');
				            				return false;
				            			}
				            			if (selected_record[0].get('GCTPAN').trim() != 'FOR'){
				            				acs_show_msg_error('Selezionare un singolo fornitore dall\'elenco');
				            				return false;				            			
				            			}						    				
				            
		            					var parent_prog = selected_record[0].get('id');		
				            			var cod_cli = selected_record[0].get('GCCDCF');
				            			var d_cli = selected_record[0].get('GCDCON').trim();		            							            			
													
				            			my_listeners_add_dest = {
				        					afterInsertRecord: function(from_win){	
				        						//dopo che ha chiuso l'inserimento dell'attivita' reload del livello cliente				        						
				        						
				        						selected_record[0].store.treeStore.load({node: selected_record[0]});
				        						//my_tree.store.load();		        						
				        						from_win.close();
								        		}
						    				};	            							            			
										acs_show_win_std('Nuova destinazione [' + cod_cli + '] ' + d_cli, 
											'<?php echo $_SERVER['PHP_SELF']; ?>?fn=new_dest', 
											{ 
								  				mode: 'NEW',
								  				i_e: 'E',
								  				type : 'D',
								  				parent_prog: parent_prog
								  			}, 750, 550, my_listeners_add_dest, 'icon-globe-16');
				            
				            }
				        }
				      ]
				   }
				},          
         ]
			        
			        
			        
			, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');

			           if (record.get('trCls')=='grassetto')
					        return ret + ' grassetto';
			           
			           if (record.get('trCls')=='G')
					        return ret + ' colora_riga_grigio';
			           if (record.get('trCls')=='Y')
					        return ret + ' colora_riga_giallo';					        
			           		        	
			           return ret;																
			         }   
			    }												    
				    		
	 	}
<?php
} //write_main_tree



function write_main_form($p){
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            flex: 0.3,
            
            defaults:{ anchor: '-10' , xtype: 'textfield', labelAlign: 'top'},
            
            items: [
            	{name: 'GCDCON',	fieldLabel: 'Denominazione'},
            	{name: 'GCWW', 		fieldLabel: 'Sito web'},
            	{name: 'GCMAIL', 	fieldLabel: 'Email azienda'},
            	{name: 'GCMAI1', 	fieldLabel: 'Email - Conf. ordine'},
            	{name: 'GCMAI2', 	fieldLabel: 'Email - Fatt. vendita'},
            	{name: 'GCMAI3', 	fieldLabel: 'Email - Piani sped.'},
            	{name: 'GCMAI4', 	fieldLabel: 'Email - Resi'},
            	{name: 'GCTEL', 	fieldLabel: 'Telefono'},
            	{name: 'GCTEL2', 	fieldLabel: 'Telefono 2'},
            	{name: 'GCFAX', 	fieldLabel: 'Fax'},
            ],
			buttons: [],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_main_form




function write_new_form_CRM($p = array(), $request = array(), $r = array()){
    global $conn, $cfg_mod_DeskAcq;
    global $s, $main_module;
    
    if ($request['mode'] == 'EDIT'){
        $r = get_gcrow_by_prog($request['rec_id']);      
      
    } else {
        //NUOVA
        $r = array(
        //'GCPROG' => $request['GCPROG']
        );
    }
    ?>
    {
        xtype: 'form',
        itemId: 'form_CRM',
        bodyStyle: 'padding: 10px',
        bodyPadding: '0 0 0 3',
        frame: true,
        title: 'Classificazioni',
        layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},	
        autoScroll: true,
        <?php
			 if ($request['mode'] == 'EDIT')
			 	echo "disabled: false, ";
			 else 
			 	echo "disabled: true, "; 
			?>   
        items: [
        {xtype: 'hiddenfield', name: 'GCPROG', value: <?php echo j($r['GCPROG']); ?>},
        {
      			xtype: 'fieldset',
      			allowBlank: true,
      			layout: 'hbox',
      			items:
      			[
        		 { 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
    						 { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						margin : '0 20 0 0',
    						layout: { 	type: 'vbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
    					     <?php if($cfg_mod_Gest['gestione_avanzata_ciclo_vita'] != 'Y'){?>
    						  <?php write_combo_std('GCCIVI', 'Ciclo di vita', $r['GCCIVI'], acs_ar_to_select_json(find_TA_sys('CIVI', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>					
    						<?php }?>
    						, <?php write_combo_std('GCCRM1', 'Class. CRM1', $r['GCCRM1'], acs_ar_to_select_json(find_TA_sys('BCR1', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>	
    						, <?php write_combo_std('GCCRM2', 'Class. CRM2', $r['GCCRM2'], acs_ar_to_select_json(find_TA_sys('BCR2', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>	
    						, <?php write_combo_std('GCSEL1', 'Classific.1', $r['GCSEL1'], acs_ar_to_select_json(find_TA_sys('CUS1', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>	
    						, <?php write_combo_std('GCSEL2', 'Classific.2', $r['GCSEL2'], acs_ar_to_select_json(find_TA_sys('CUS2', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>	
    					    
    						]}, { 
    						xtype: 'fieldcontainer',
    						flex:1,
    						layout: { 	type: 'vbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
    						     {xtype : 'component', flex : 1}
        						, <?php write_combo_std('GCORIG', 'Origine', $r['GCORIG'], acs_ar_to_select_json(find_TA_sys('BORI', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>	
              			   	    , <?php write_combo_std('GCREFE', 'Referente', $r['GCREFE'], acs_ar_to_select_json(find_TA_sys('BREF', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>		
    					        , <?php write_combo_std('GCSEL3', 'Classific.3', $r['GCSEL3'], acs_ar_to_select_json(find_TA_sys('CUS3', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>	
    				            , <?php write_combo_std('GCSEL4', 'Classific.4', $r['GCSEL4'], acs_ar_to_select_json(find_TA_sys('CUS4', null, null, null, null, null, 0, '', 'Y', 'Y'), '', 'R')) ?>			
    					    ]}
						
						   
						]}
        
        		]
        }
        ],
		buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', 
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	var tabPanel = loc_win.down('tabpanel');
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_crm',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	cliente : <?php echo j($r['GCCDCF']); ?>,
						        	open_request: <?php echo acs_je($request) ?>,
						        	GCPRAB : <?php echo j($r['GCPRAB']); ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            var record = jsonData.record;
						            
			                        tabpanel_load_record_in_form(tabPanel, record);
			                         tabPanel.acs_actions.exe_afterSave(loc_win);	
						            
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						           
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],    
        
        }
<?php     


}





function write_new_form_ITA($p = array(), $request = array(), $r = array()){
	global $s, $main_module;
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '0 0 0 3',
            frame: true,
            title: 'Intestazione',
            autoScroll: true,
            
            flex: 0.7,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'mode',
	                	value: <?php echo j($request['mode']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'tipo_anagrafica',
	                	value: <?php echo j($p['tipo_anagrafica']) ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'tipologia',
	                	value: <?php echo j($cfg_mod_Gest['cod_naz_italia']); ?>
                	}, {
	                	xtype: 'hidden',
	                	name: 'GCPROG',
	                	value: <?php echo j(trim($r['GCPROG'])); ?>	
                	}
                	
                	
<?php if (in_array($p['tipo_anagrafica'], array('DES'))) { ?>
					, <?php write_combo_std('GCTPDS', 'Tipo destinazione', $r['GCTPDS'], acs_ar_to_select_json(ar_standard_alternativa(), '', 'R'), array() ) ?>
<?php } ?>                	
                	
                	
                	
                	
                	
                	, {
						name: 'GCDCON',
						fieldLabel: 'Denominazione',
					    maxLength: 100,
					    value: <?php echo j(trim(acs_u8e($r['GCDCON']))); ?>								
					},
					{
									name: 'GCINDI',
									fieldLabel: 'Indirizzo',
									maxLength: 100,
									value: <?php echo j(trim(acs_u8e($r['GCINDI']))); ?>							
							},
					 {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							  
								{
									xtype: 'fieldcontainer', flex: 3,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'GCLOCA', flex: 1,
											fieldLabel: 'Localit&agrave;', labelWidth: 130,
										    value: <?php echo j(trim(acs_u8e($r['GCLOCA']))); ?>,
										    readOnly: true
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners_search_loc = {
									        					afterSelected: function(from_win, record_selected){
										        						m_form.findField('GCLOCA').setValue(record_selected.get('descr'));
										        						m_form.findField('GCPROV').setValue(record_selected.get('COD_PRO'));
										        						m_form.findField('v_regione').setValue(record_selected.get('DES_REG'));
										        						m_form.findField('GCNAZI').setValue(record_selected.get('COD_NAZ'));
										        						from_win.close();
														        		}										    
														    },
															
															
															acs_show_win_std('Seleziona localit&agrave;', 
																'../desk_gest/search_loc.php?fn=open_search', 
																{}, 450, 150, my_listeners_search_loc, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								}							  
							  	
								
								
								
						        
						        , {
									name: 'GCCAP', width: 110, labelWidth: 70,
									fieldLabel: 'Cap', labelAlign: 'right',
								    maxLength: 5,
								    value: <?php echo j(trim(acs_u8e($r['GCCAP']))); ?>
								}								    	
							
						]
					 }, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
							  
								{
									xtype: 'fieldcontainer', flex: 3,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'v_regione',  flex: 1,
											fieldLabel: 'Regione',
										    labelWidth: 130,
										    value: <?php echo j(trim(acs_u8e($r['DES_REG']))); ?>,
										    readOnly: true
										}
								  ]
								}							  
							  	
						        
						        , {
									name: 'GCPROV', width: 110, labelWidth: 70,
									fieldLabel: 'Prov.', labelAlign: 'right',
								    maxLength: 5, readOnly: true,
								    value: <?php echo j(trim(acs_u8e($r['GCPROV']))); ?>,
							    	readOnly: true	
								}	
							
						]
					 },
					 
					 		{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [					
							 {
								name: 'GCNAZI',
								fieldLabel: 'Nazione', labelWidth: 130,
							    maxLength: 2,
							    value: <?php echo j(trim(acs_u8e($r['GCNAZI']))); ?>,
							    readOnly: true							
							},{
								name: 'GCTEL',
								fieldLabel: 'Telefono 1',labelAlign: 'right',
								maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCTEL']))); ?>							
							}
						]
					},{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [

							<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>						
							 {
								name: 'GCCDFI',
								fieldLabel: 'Codice fiscale',
							    maxLength: 100,
							    allowBlank: true,
							    value: <?php echo j(trim(acs_u8e($r['GCCDFI']))); ?>							
							} ,
							<?php } ?>
							 {
								name: 'GCTEL2',
								fieldLabel: 'Telefono 2', labelAlign: 'right',
							    maxLength: 20,
							    value: <?php echo j(trim(acs_u8e($r['GCTEL2']))); ?>
							}
						]
					},{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [

							<?php if (!in_array($p['tipo_anagrafica'], array('DES', 'PVEN'))) { ?>						
								{
								name: 'GCPIVA',
								fieldLabel: 'P. IVA',
							    maxLength: 100,
							    value: <?php echo j(trim(acs_u8e($r['GCPIVA']))); ?>							
							} ,
							<?php } ?>
							 {
								name: 'GCFAX',
								fieldLabel: 'Fax', labelAlign: 'right',
							    maxLength: 20,
								value: <?php echo j(trim(acs_u8e($r['GCFAX']))); ?>
							}
						]
					}, {
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						//anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield', flex: 1},
						items: [
    						{
    						name: 'GCWWW',
    						fieldLabel: 'Sito web',
    					    maxLength: 60,
    					    value: <?php echo j(trim(acs_u8e($r['GCWWW']))); ?>							
    						},  {
    						name: 'GCCSDI',
    						fieldLabel: 'Codice SDI', 
    					    maxLength: 7,
    					    labelAlign: 'right',
    					    //width : 200,
    						value: <?php echo j(trim(acs_u8e($r['GCCSDI']))); ?>
    						}
						
						]
					}
					, {
						xtype: 'fieldset', title: 'E-mail',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								{
									vtype: 'email',
									name: 'GCMAIL',
									fieldLabel: 'Principale',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAIL']))); ?>							
								}
								
								
<?php if (!in_array($p['tipo_anagrafica'], array('PVEN'))) { ?>								
								, {
									vtype: 'email',
									name: 'GCMAI1',
									fieldLabel: 'Ordine',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI1']))); ?>							
								}
					<?php if (!in_array($p['tipo_anagrafica'], array('DES'))) { ?>								
								/*, {
									vtype: 'email',
									name: 'GCMAI2',
									fieldLabel: 'Fattura',
								    maxLength: 60,
								    value: <?php echo j(trim(acs_u8e($r['GCMAI2']))); ?>							
								}*/
					<?php } ?>
					
					<?php if($p['tipo_anagrafica'] != 'DES'){?>	
								, {
									vtype: 'email',
									name: 'GCPEC',
									fieldLabel: 'Indirizzo PEC',
								    maxLength: 120,
								    value: <?php echo j(trim(acs_u8e($r['GCPEC']))); ?>							
								}
								<?php }?>
								
							
<?php } ?>			

			
								
						]
					  }
					  
				
					
					
					
					],
					
					
					
					
		dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            fixed: true,
            items: [					
			{
		        xtype:'tbspacer',
		        flex:1
		    } 
	        , {
	            text: 'Gmap',
	            iconCls: 'icon-gmaps_logo-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	
	            	//costruisco indirizzo
	            	ind = 'VIA INSALA, 236 - 61122 - PESARO';
	            	
					//$ar[$liv1_v]["val"][$liv2_v]["gmap_ind"]	= implode(",", array_map('trim', array($row['TDDNAD'], $row['TDDCAP'], $row['TDPROD'], $row['TDDLOC'] , substr($row['TDIDES'], 0, 30))));
					
					f = [];
					//f.push(form.findField('f_nazione').getValue());
					f.push(form.findField('GCCAP').getValue());
					f.push(form.findField('GCPROV').getValue());
					f.push(form.findField('GCLOCA').getValue());
					f.push(form.findField('GCINDI').getValue());
					
					//km_field_id = form.findField('GCKMIT').id;	            	
	            	
					gmapPopup("../desk_vend/gmap_route.php?ind=" + f.join());	            	
	            	
	            }
	         }
/*	         
	         , {
	            text: 'Percorso',
	            iconCls: 'icon-pneumatico-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');
	            	
	            	//costruisco indirizzo
					f = [];
					//f.push(form.findField('f_nazione').getValue());
					f.push(form.findField('GCCAP').getValue());
					f.push(form.findField('GCPROV').getValue());
					f.push(form.findField('GCLOCA').getValue());
					f.push(form.findField('GCINDI').getValue());
					gmapPopup("../desk_vend/gmap_percorso_single.php?ind=" + f.join());	            	
	            	
	            }
	         }
*/	         
	         
			, {
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            		
	            	var panel_form = this.up('form'),
	            		form = panel_form.getForm(),           	
	            		form_values = form.getValues();

					panel_form.acs_actions.exe_form_submit(panel_form);        	                	                
	            }
	        }	         
	         
	        ]// buttons
	       }
	      ]             
			
			
		, acs_actions: {
	    	exe_form_submit: function(panel_form){
				var form = panel_form.getForm(),
				loc_win = panel_form.up('window');
		
         	   		  if(form.isValid()){
							  Ext.Ajax.request({
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_new_anag',
						        jsonData: {
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>,
						        	cod_cli : <?php echo j($r['cod_cli'])?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){	            	  													        
						            var jsonData = Ext.decode(result.responseText);
						            var record = jsonData.record;
						            var tabPanel = loc_win.down('tabpanel');
						            						            
						            loc_win.fireEvent('afterInsertRecord', loc_win, record);
						            
						            tabpanel_load_record_in_form(tabPanel, record);
						            
						            tabPanel.GCPROG = jsonData.GCPROG;
						            form.findField('mode').setValue('EDIT');
						            form.findField('mode').resetOriginalValue(); //clear isDirty
						            
						            var title = 'Modifica anagrafica [' + record.GCCDCF + ', ' + record.GCCINT + '] ' +record.GCDCON;
						            loc_win.setTitle(title);
						            
						            if (Ext.isEmpty(tabPanel) == true) {
						            	return;
						            }
						            						            						            
									tabPanel.items.each(function(panelItem, index, totalCount){
										panelItem.setDisabled(false);
										
										//per aggiornare le condizioni commerciali: MIGLIORARE: TODO
										if (panelItem.xtype == 'grid'){
											panelItem.store.proxy.extraParams.GCDEFA = 'ITA';
											panelItem.store.proxy.extraParams.rec_id = jsonData.GCPROG;
											panelItem.store.proxy.extraParams.cdcf = record.GCCDCF;
											panelItem.store.load();
										}
									
										if (panelItem.itemId == 'form_CRM'){
										  panelItem.getForm().findField('GCPROG').setValue(jsonData.GCPROG);											
									      panelItem.getForm().setValues(jsonData.record);
									    }
										
									});
									
									tabPanel.acs_actions.exe_afterSave(loc_win);
									
						            	
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    } //is valid		
		
			} //exe_form_submit
			
	    } //acs_actions				
			
				
				
        }
<?php
} //write_new_form_ITA


function write_new_form_COMM($p = array(), $request = array()){
    global $conn, $cfg_mod_Gest, $cfg_mod_DeskAcq;
    
    if ($request['mode'] == 'EDIT'){
        //recupero la riga da modificare
        $sql = "SELECT RRN(CC) AS RRN, CC.*, GC.*, TA.TADESC AS DES1, TA.TADES2 AS DES2
        FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']} CC
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
        ON CCDT = GCDT AND CCPROG = GCPROG
        LEFT OUTER JOIN {$cfg_mod_Gest['file_tab_sys']} TA
        ON CCDT = TADT AND TAID = 'CUBA' AND TANR = CCBANC
        WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($request['CCDT'], $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));
        
        $ret = array();
        $r = db2_fetch_assoc($stmt);
        $r['des_cuba'] = trim($r['DES1']).trim($r['DES2']);
        if(trim($r['CCCCON']) == 'STD')
            $r['CCDCON'] = "Condizioni vendita standard/non condizionate";
            
            //des_banca
            if (strlen(trim($r['CCABI'])) > 0 && strlen(trim($r['CCCAB'])) > 0) {
                $sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, array($r['CCABI'], $r['CCCAB']));
                $r_cab = db2_fetch_assoc($stmt);
                $des_banca = implode(" - ", array(trim($r_cab['XDSABI']), trim($r_cab['XDSCAB'])));
            }
            
            
    } else {
        
        
        //DALLA REGIONE PROVO A RECUPERARE IL REFERENTE (TATISP IN TATAID='ANREG')
        
        $sql = "SELECT TA_REG.TADESC AS DES_REG, TA_REG.TATISP AS REF_BY_REG, GCDEFA
        FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
        LEFT OUTER JOIN  {$cfg_mod_Gest['file_tabelle']} TA_COM ON
        TA_COM.TAKEY4 = GCNAZI AND TA_COM.TAKEY2 = GCPROV AND TA_COM.TADESC = GCLOCA AND TA_COM.TATAID = 'ANCOM'
        LEFT OUTER JOIN   {$cfg_mod_Gest['file_tabelle']} TA_REG ON
        TA_COM.TAKEY3 = TA_REG.TAKEY1 AND TA_REG.TATAID = 'ANREG'
        WHERE GCPROG = ?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($request['CCPROG']));
        $r_regione = db2_fetch_assoc($stmt);
        
        //NUOVA CONDIZIONE COMMERCIALE
        $r = array(	'CCDT' => $request['CCDT'],
        'CCPROG' => $request['CCPROG'],
        'CCSEZI' => $request['CCSEZI'],
        'CCCCON' => $request['CCCCON'],
        'CCDCON' => $request['CCDCON'],
        'CCREFE' => $r_regione['REF_BY_REG'],
        'GCDEFA' => $r_regione['GCDEFA'],
        );
    }
    
    
    global $s, $main_module, $deskGest;
    ?>
       
        {
            xtype: 'form',
            frame: true,
            disabled: false, //true,
            autoScroll: true,
            flex: 1,
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield' <?php echo $readOnly ?>},
            items: [
            
            	{xtype: 'tabpanel',
        		items: [
        		
        		{  xtype : 'form',
        		   title : 'Dettagli',
        		   frame: true,
        		   items : [
        		   
        		   {xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($r['CCDT']); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j($r['CCSEZI']); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($r['CCPROG']); ?>},
            		
            		<?php if($request['mode'] != 'CREATE'){?>
            			{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j($r['CCCCON']); ?>},
            		<?php }?>
            		
            		<?php if (trim($r['CCCCON']) == 'STD'){ ?>
            		    {xtype: 'hiddenfield', name: 'CCDCON', value: <?php echo j($r['CCDCON']); ?>},
            		<?php } ?>
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							   	value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,							    
							    <?php if($request['mode'] != 'CREATE'){ ?>
							    	disabled: true
							    <?php }?>
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    
							    //La STD non e' gestibile,
							    //le altre SI ma devo separare la descrizione in 30 (nome) + 30 (articolo)
							    
							    <?php if (trim($r['CCCCON']) == 'STD'){ ?>
							    	value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    	maxLength: 60,
							    	disabled: true,
							    <?php } else { ?>
							    	value: <?php echo j(trim(acs_u8e(substr($r['CCDCON'], 0, 30)))); ?>,
							    	maxLength: 30,
							    	disabled: false,							    	
							    <?php }?>						
							}
							
							
							<?php if (trim($r['CCCCON']) != 'STD'){ ?>
								, {
									name: 'CCDCON_ART',
									fieldLabel: 'Art.', flex: 0.8, labelAlign: 'right', labelWidth: 40,
									value: <?php echo j(trim(acs_u8e(substr($r['CCDCON'], 30, 30)))); ?>,
							    	maxLength: 30,
							    	disabled: false,									
								}
							<?php } ?>
							]
						}            
            
            
					, {
						xtype: 'fieldset', title: 'Coordinate bancarie/Pagamento',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
						          {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'CCBANC',
											fieldLabel: 'Banca codificata', width: 150,
											maxLength: 3,
											readOnly : true, 
										    value: <?php echo j(trim(acs_u8e($r['CCBANC']))); ?>							
										},{
											name: 'des_cuba',
											fieldLabel: '', 
											flex: 1,
											readOnly : true,
										    maxLength: 160, 
										    value: <?php echo j(trim(acs_u8e($r['des_cuba']))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners = {
									        					afterSelected: function(from_win, row){
									        					 	m_form.findField('des_cuba').setValue(row.descrizione);
										        					m_form.findField('CCBANC').setValue(row.codice);
										        					m_form.findField('CCABI').setValue(row.abi);
										        					m_form.findField('CCCAB').setValue(row.cab);
										        					m_form.findField('des_banca').setValue(row.descrizione);
										        			        from_win.close();
										        				}										    
														    },
															acs_show_win_std('Ricerca banca codificata [CUBA]', '../desk_gest/acs_ricerca_tab_sys.php?fn=open_tab', {taid : 'CUBA'}, 1000, 400, my_listeners, 'icon-leaf-16');    
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								},	
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [													
										{
											name: 'CCABI',
											fieldLabel: 'Archivio sportelli',
										    maxLength: 5,
										    width: 180,
										    value: <?php echo j(trim(acs_u8e($r['CCABI']))); ?>							
										}, {
											name: 'CCCAB',
											fieldLabel: '', labelWidth: 0, width: 60,
										    maxLength: 5,
										    value: <?php echo j(trim(acs_u8e($r['CCCAB']))); ?>,
										    listeners: {
                                				'blur': function(field) {
                                					var form = this.up('form').getForm();
                                					var form_values = form.getValues();
                                					if(form_values.CCABI.trim() != ''){
                                					
                                    					Ext.Ajax.request({
                        								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_abicab',
                        								        method     : 'POST',
                        					        			jsonData: {
                        					        				form_values : form_values
                        										},							        
                        								        success : function(result, request){
                        								            var jsonData = Ext.decode(result.responseText);
                        								        	if(jsonData.success == false){
                                						           	    acs_show_msg_error(jsonData.msg_error);
                                	 					 			    return;
                                						            }else{
                                						               if(form_values.CCBANC.trim() == ''){
                                						               	  form.findField('CCBANC').setValue(jsonData.CCBANC);
										        					   	  form.findField('des_cuba').setValue(jsonData.des_cuba);
										        					   	  form.findField('des_banca').setValue(jsonData.des_cuba);
                                						            	}
                        								            }
                        					            		},
                        								        failure    : function(result, request){
                        								            Ext.Msg.alert('Message', 'No data to be loaded');
                        								        }
                        								    });
                    								  }
                                				}
                                			}							
										},{
											name: 'des_banca',
											fieldLabel: '', flex: 1,
										    maxLength: 160, 
										    readOnly : true,
										    value: <?php echo j(trim(acs_u8e($des_banca))); ?>							
										}, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
														    my_listeners= {
									        					afterSelected: function(from_win, form_values){
									        													        					
										        						m_form.findField('CCABI').setValue(form_values.f_abi);
										        						m_form.findField('CCCAB').setValue(form_values.f_cab);
										        						m_form.findField('des_banca').setValue(form_values.des_banca);
										        															        						
										        						  Ext.Ajax.request({
                                    								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_check_cuba',
                                    								        method     : 'POST',
                                    					        			jsonData: {
                                    					        				//rrn : <?php echo j($r['RRN'])?>,
                                    					        				form_values : form_values,
                                    					        				from_search : 'Y'
                                    										},							        
                                    								        success : function(result, request){
                                    								            var jsonData = Ext.decode(result.responseText);
                                    								        	m_form.findField('CCBANC').setValue(jsonData.CCBANC);
										        								m_form.findField('des_cuba').setValue(jsonData.des_cuba);
                                    								            from_win.close();
                                    					            		},
                                    								        failure    : function(result, request){
                                    								            Ext.Msg.alert('Message', 'No data to be loaded');
                                    								        }
                                    								    });
										        					
														        		}										    
														    },
															
															
															acs_show_win_std('Ricerca banca', 
																'../desk_gest/search_bank.php?fn=open_search', 
																{}, 450, 300, my_listeners, 'icon-sub_blue_add-16');
															});										            
											             }
													}										    
										    
										    
										}
								  ]
								}		
								
								, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [								
										 {
											name: 'CCCOCO',
											fieldLabel: 'Conto corrente',
										    maxLength: 20, flex: 0.8,
										    value: <?php echo j(trim(acs_u8e($r['CCCOCO']))); ?>							
										},
										{
											name: 'CCIBAN',
											fieldLabel: 'IBAN',
										    labelWidth: 40, maxLength: 34, flex: 1,
										    labelAlign : 'right',
										    value: <?php echo j(trim(acs_u8e($r['CCIBAN']))); ?>							
										    }, {										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
										    value: <?php echo j("<img src=" . img_path("icone/48x48/button_black_repeat_dx.png") . " width=16>"); ?>,
										    
										    
											listeners: {
											            render: function( component ) {
											            
											            	m_form = this.up('form').getForm();
											            
															component.getEl().on('dblclick', function( event, el ) {
															
															Ext.getBody().mask('Loading... ', 'loading').show();
															
															Ext.Ajax.request({
																	timeout: 2400000,
															        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ricalcola_iban',
															        jsonData: {
															        	form_values: m_form.getValues()
															        },	
															        method     : 'POST',
															        waitMsg    : 'Data loading',
															        success : function(result, request){
															        						        
																		Ext.getBody().unmask();
															            var jsonData = Ext.decode(result.responseText);
																		
																		m_form.findField('CCIBAN').setValue(jsonData.iban);
									
															        },
															        failure    : function(result, request){
																		Ext.getBody().unmask();							        
															            Ext.Msg.alert('Message', 'No data to be loaded');
															        }
															    });																
															
															
															
														
															});
											             }
													}										    
										    
										    
										}
										
										
									]
								},
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox'},
									anchor: '-10',
									items: [
									<?php 
									if (trim($r['CCCCON']) == 'STD') 
									   $allowBlank = 'false';
									else
									   $allowBlank = 'true';
									?>
									
									
										{
								    xtype : 'combo',
                                    name: 'CCPAGA',
                                    value: <?php echo j($r['CCPAGA']); ?>,
                                    flex : 1.5,
                                    fieldLabel: 'Pagamento',
                                    displayField: 'text',
                                    valueField: 'id',
                                    anchor: '-10',
                                    editable : true,
                                    forceSelection:true,
                                    allowBlank: <?php echo $allowBlank ?>,													
                                  	store: {
        								autoLoad: true,
        								editable: false,
        								autoDestroy: true,	 
        							    fields: [{name:'id'}, {name:'text'}],
        							    data: [								    
        								     <?php //echo acs_ar_to_select_json($s->get_options_pagamento(), '') ?>
        								     <?php echo acs_ar_to_select_json(find_TA_sys('CUCP'), '') ?> 	
        								    ] 
        							},
                                    tpl: [
                                    	 '<ul class="x-list-plain">',
                                            '<tpl for=".">',
                                            '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                                    		 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                                                   '</tpl>'
                                                     
                                                    ),
                                        
                                    	queryMode: 'local',
                                    	minChars: 1,	
                                        listeners: { 
                                        	   beforequery: function (record) {
                                        		record.query = new RegExp(record.query, 'i');
                                        		record.forceAll = true;
                                        	},
                                        	 change: function (combo) {
                                                   combo.expand();
                                             },
                                        	 select: function(combo, newValue, oldValue, eOpts) {
                                        	    if(combo.value == 'ALL'){
                                        	       combo.store.proxy.extraParams.gruppo = '';
                                        	       combo.store.load();
                                        	       //combo.focus();
                                        	      
                                        	    }
    											
    										 }
                                       		   
                                    	} 					 
                                    }
									   , <?php write_combo_std('CCTSIV', 'Rata IVA', $r['CCTSIV'], acs_ar_to_select_json($main_module->find_TA_std('ANC03', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelAlign' => 'right') ) ?>
									]
								}, 
								
								<?php if (trim($r['CCCCON']) == 'STD'){ ?>
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [										
										{
											name: 'CCNGPA',
											fieldLabel: 'GG inizio scad.',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCNGPA']))); ?>							
										}, {
											name: 'CCMME1',
											fieldLabel: '1&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCMME1']))); ?>							
										}, <?php write_combo_std('CCGGE1', '', $r['CCGGE1'], acs_ar_to_select_json($deskGest->find_TA_std('ANC04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>
										
										, {
											name: 'CCMME2',
											fieldLabel: '2&deg; Mese escl.', labelAlign: 'right',
										    maxLength: 5, flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCMME2']))); ?>							
										}, <?php write_combo_std('CCGGE2', '', $r['CCGGE2'], acs_ar_to_select_json($deskGest->find_TA_std('ANC04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>
									]
								}
								<?php }?>
						]
					}
					
					
					,
					
					
					{
						xtype: 'fieldset', title: 'Agenti',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
									
										<?php //write_combo_std('CCAG1', 'Agente (1)', $r['CCAG1'], acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), '', 'R'), array('transorm' => 'NO', 'flex_width' => "flex: 2") ) ?>
										
										{
								            xtype: 'combo',
											name: 'CCAG1',
											flex: 2,
											fieldLabel: 'Agente (1)',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, {name:'color'}, {name:'sosp'}, 'perc_provv', 'area_manager'],
												    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array(
												            'value'  => $r['CCAG1'],
												            'i_sosp' => 'N', 'cli_cc' => 'Y')), '', 'R'); ?>] 
											},
								            
								            value: '<?php echo trim($r['CCAG1']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            anchor: '100%',
								            queryMode: 'local',
     										minChars: 1,
     										tpl: [
                            				 '<ul class="x-list-plain">',
                                                '<tpl for=".">',
                                                '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                            				 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                            
                                                    '</tpl>'
                                             
                                            ),
									        listeners: {
									            select: function(combo, row, index) {
												   this.up('form').getForm().findField('CCPA1').setValue(row[0].data.perc_provv);
												   
													<?php if (trim($r['CCCCON']) == 'STD'){ ?>
														this.up('form').getForm().findField('CCARMA').setValue(row[0].data.area_manager);													
													<?php } ?>												   
												   								            
									            }, 
									              beforequery: function (record) {
                        	         				record.query = new RegExp(record.query, 'i');
                        	         				record.forceAll = true;
                        				 		}
									        }            
										
								        }										
										
										
										
									, {
											xtype: 'numberfield', hideTrigger:true,
											name: 'CCPA1',
											fieldLabel: '% provv.', labelAlign: 'right',
										    flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCPA1']))); ?>							
										}
									]
								}, 								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php //write_combo_std('CCAG2', 'Agente (2)', $r['CCAG2'], acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), '', 'R'), array('transorm' => 'NO', 'flex_width' => "flex: 2") ) ?>
										
										
										{
								            xtype: 'combo',
											name: 'CCAG2',
											flex: 2,
											fieldLabel: 'Agente (2)',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, 'perc_provv', {name:'color'}, {name:'sosp'}],
												    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('value'  => $r['CCAG2'], 'cli_cc' => 'Y')), '', 'R'); ?>] 
											},
								            
								            value: '<?php echo trim($r['CCAG2']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            anchor: '100%',
								            queryMode: 'local',
     										minChars: 1,
     										tpl: [
                            				 '<ul class="x-list-plain">',
                                                '<tpl for=".">',
                                                '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                            				 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                            
                                                    '</tpl>'
                                             
                                            ),
									        listeners: {
									            select: function(combo, row, index) {
												   this.up('form').getForm().findField('CCPA2').setValue(row[0].data.perc_provv);								            
									            },
									            
									            beforequery: function (record) {
                        	         				record.query = new RegExp(record.query, 'i');
                        	         				record.forceAll = true;
                        				 		}
									        }            
										
								        },
										
										
										{
											xtype: 'numberfield', hideTrigger:true,
											name: 'CCPA2',
											fieldLabel: '% provv.', labelAlign: 'right',
										    flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCPA2']))); ?>							
										}
									]
								}, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php //write_combo_std('CCAG3', 'Agente (3)', $r['CCAG3'], acs_ar_to_select_json($main_module->find_sys_TA('CUAG'), '', 'R'), array('transorm' => 'NO', 'flex_width' => "flex: 2") ) ?>
										
										
										{
								            xtype: 'combo',
											name: 'CCAG3',
											flex: 2,
											fieldLabel: 'Agente (3)',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, 'perc_provv', {name:'color'}, {name:'sosp'}],
												    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('value'  => $r['CCAG3'], 'cli_cc' => 'Y')), '', 'R'); ?>] 
											},
								            
								            value: '<?php echo trim($r['CCAG3']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            anchor: '100%',
								            queryMode: 'local',
     										minChars: 1,
     										tpl: [
                            				 '<ul class="x-list-plain">',
                                                '<tpl for=".">',
                                                '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                                '</tpl>',
                                                '</ul>'
                            				 ],
                                               // template for the content inside text field
                                            displayTpl: Ext.create('Ext.XTemplate',
                                                   '<tpl for=".">',
                                                        '{text}',
                            
                                                    '</tpl>'
                                             
                                            ),
									        listeners: {
									            select: function(combo, row, index) {
												   this.up('form').getForm().findField('CCPA3').setValue(row[0].data.perc_provv);								            
									            }, beforequery: function (record) {
                        	         				record.query = new RegExp(record.query, 'i');
                        	         				record.forceAll = true;
                        				 		}
									        }            
										
								        },
										{
											xtype: 'numberfield', hideTrigger:true,
											name: 'CCPA3',
											fieldLabel: '% provv.', labelAlign: 'right',
										    flex: 1,
										    value: <?php echo j(trim(acs_u8e($r['CCPA3']))); ?>							
										}
									]
								}
								
								<?php if (trim($r['CCCCON']) == 'STD'){ ?>
								, {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [								
										{
								            xtype: 'combo',
											name: 'CCARMA',
											width : '67%',
											margin : '0 5 0 0',
											fieldLabel: 'Area manager',
											store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}, 'perc_provv'],
												    data: [<?php echo acs_ar_to_select_json($main_module->find_sys_TA('CUAG', array('value'  => $r['CCARMA'], 'tipo' => 'area_manager')), '', 'R'); ?>] 
											},
								            
								            value: '<?php echo trim($r['CCARMA']); ?>',            
											valueField: 'id',                       
								            displayField: 'text',
								            allowBlank: false      										
								        }
								    ]
								  }
								<?php } ?>
								
								
						]
					}
					, {
						xtype: 'fieldset', title: 'Vendita',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 120, xtype: 'textfield'},
						items: [
								
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									items: [
										<?php write_combo_std('CCREFE', 'Referente', $r['CCREFE'], acs_ar_to_select_json(find_TA_sys('BREF', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 2.8") ) ?>,
									
									{
											name: 'CCSC1',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: 'Sconti comm.',
											labelAlign : 'right', 
										    maxLength: 5, width : 145,
										    value: <?php echo j(trim(acs_u8e($r['CCSC1']))); ?>							
										}, {
											name: 'CCSC2',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC2']))); ?>							
										}, {
											name: 'CCSC3',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC3']))); ?>							
										}, {
											name: 'CCSC4',
											xtype: 'numberfield', hideTrigger:true,
										    labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC4']))); ?>							
										}
									
									]
								},		
								{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
									
									<?php write_combo_std('CCLIST', 'Listino', $r['CCLIST'], acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 2.8") ) ?>,
									
									{
											name: 'CCSC5',
											xtype: 'numberfield', hideTrigger:true,
											fieldLabel: 'Sconti docum.',
											labelAlign : 'right', 
										    maxLength: 5, width : 145,
										    value: <?php echo j(trim(acs_u8e($r['CCSC5']))); ?>							
										}, {
											name: 'CCSC6',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC6']))); ?>							
										}, {
											name: 'CCSC7',
											xtype: 'numberfield', hideTrigger:true,
											labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC7']))); ?>							
										}, {
											name: 'CCSC8',
											xtype: 'numberfield', hideTrigger:true,
										    labelWidth : 20,
											fieldLabel: '+', labelAlign: 'right',
										    maxLength: 5, width : 65,
										    labelSeparator : '',
										    value: <?php echo j(trim(acs_u8e($r['CCSC8']))); ?>							
										}
									  
									
									]
								}	,		
								
								<?php if (trim($r['CCCCON']) == 'STD'){ ?>
									 {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										<?php write_combo_std('GCIVES', 'Assoggettamento', $r['GCIVES'], acs_ar_to_select_json($deskGest->find_TA_std('CUAE', null, 'N', 'N', null, null, null, 'N', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
										<?php write_combo_std('CCVALU', 'Valuta', $r['CCVALU'], acs_ar_to_select_json(find_TA_sys('VUVL', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'flex_width' => "width: 340", "allowBlank" => 'false') ) ?>,
								    ]},
									<?php write_combo_std('CCCATR', 'Causale vendita', $r['CCCATR'], acs_ar_to_select_json(find_TA_sys('VUCT', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 100) ) ?>	
									<?php }?>				
								
									
								    
								    
								   , {
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
									
									<?php write_combo_std('GCCATC', 'Cat. sconto', $r['GCCATC'], acs_ar_to_select_json(find_TA_sys('VUCS', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
									<?php write_combo_std('GCCAPC', 'Cat. provvigioni', $r['GCCAPC'], acs_ar_to_select_json(find_TA_sys('VUCP', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'flex_width' => "width: 340") ) ?>,
									
									]}
									
									
							
								
								
						]
					}	
        		   
        		   
        		   
        		   
        		   ]
        		
        		
        		}, 
        		
        		 <?php if(trim($request['CCCCON']) != 'STD'){ ?>
          
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            height : 470,
            title: 'Condizionamenti',
            autoScroll: true,
            //flex: 1,
            defaults:{ anchor: '-10' , labelWidth: 100},
            items: [
            		
        		{ 
					xtype: 'fieldcontainer',
					layout: 'hbox',
					flex: 1,
					items: [
						<?php write_combo_std('CCTCON', 'Tipo condizione', $r['CCTCON'], acs_ar_to_select_json(find_TA_sys('TCOM', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
						<?php write_combo_std('CCGRDO', 'Gruppo documenti', $r['CCGRDO'], acs_ar_to_select_json(find_TA_sys('RDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
				
				]},
            		
             { 
				xtype: 'fieldcontainer',
				layout: 'hbox',
				flex: 1,
				items: [{
				   xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Validit&agrave; iniziale'
				   , value: '<?php echo print_date($r['CCDTVI'], "%d/%m/%Y"); ?>'
				   , flex : 1
				   , name: 'CCDTVI'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
						}
				},{
				   xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'finale'
				   , value: '<?php echo print_date($r['CCDTVF'], "%d/%m/%Y"); ?>'
				   , flex : 1
				   , labelWidth : 120
				   , name: 'CCDTVF'
				   , labelAlign : 'right'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
						}
				}
				
						  
						 ]
					},
					
					{ 
					xtype: 'fieldcontainer',
					layout: 'hbox',
					flex: 1,
					items: [
						<?php write_combo_std('CCDIVI', 'Divisione docu.', $r['CCDIVI'], acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
				
				]},
					
					{ 
				xtype: 'fieldcontainer',
				layout: 'hbox',
				flex: 1,
				items: [
				<?php write_combo_std('CCFGR1', 'Tipologia ordini 1', $r['CCFGR1'], acs_ar_to_select_json($main_module->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 3", 'labelWidth' => 100) ) ?>,
				<?php write_combo_std('CCFGR2', '2', $r['CCFGR2'], acs_ar_to_select_json($main_module->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 2", 'labelWidth' => 20, "labelAlign" => "right") ) ?>,
				<?php write_combo_std('CCFGR3', '3', $r['CCFGR3'], acs_ar_to_select_json($main_module->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 2", 'labelWidth' => 20, "labelAlign" => "right") ) ?>
				
				]},
					
					{ 
				xtype: 'fieldcontainer',
				layout: 'hbox',
				flex: 1,
				items: [
				<?php write_combo_std('CCDES1', 'Destinazioni', $r['CCDES1'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $request['cdcf'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1.7", 'labelWidth' => 100) ) ?>,
				<?php write_combo_std('CCDES2', '2', $r['CCDES2'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $request['cdcf'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 20, "labelAlign" => "right") ) ?>,
				<?php write_combo_std('CCDES3', '3', $r['CCDES3'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $request['cdcf'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 20, "labelAlign" => "right") ) ?>,
				<?php write_combo_std('CCDES4', '4', $r['CCDES4'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $request['cdcf'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 20, "labelAlign" => "right") ) ?>,
				<?php write_combo_std('CCDES5', '5', $r['CCDES5'], acs_ar_to_select_json(find_TA_sys('VUDE', null, $request['cdcf'], null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 20, "labelAlign" => "right") ) ?>,
				]},
					
					{
						xtype: 'fieldset', title: 'Altre opzioni',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ labelWidth: 130, xtype: 'textfield'},
						items: [
						{ 
        					xtype: 'fieldcontainer',
        					layout: 'hbox',
        					flex: 1,
        					items: [
        						<?php write_combo_std('CCCPRO', 'Composizione promozionale', $r['CCCPRO'], acs_ar_to_select_json($main_module->find_TA_std('CCT01', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1.2", 'labelWidth' => 160) ) ?>,
        						<?php write_combo_std('CCPRZO', 'Prezzo obbligatorio', $r['CCPRZO'], acs_ar_to_select_json($main_module->find_TA_std('CCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
        				
        				]},
						{ 
    					xtype: 'fieldcontainer',
    					layout: 'hbox',
    					flex: 1,
    					items: [
    						<?php write_combo_std('CCSCIM', 'Sconto a importo', $r['CCSCIM'], acs_ar_to_select_json($main_module->find_TA_std('CCT02', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1.2", 'labelWidth' => 160) ) ?>,
    						<?php write_combo_std('CCEINT', 'Elabor. interattiva', $r['CCEINT'], acs_ar_to_select_json($main_module->find_TA_std('CCT03', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 120, "labelAlign" => "right") ) ?>,
    				
    					]},
    					{ 
    					xtype: 'fieldcontainer',
    					layout: 'hbox',
    					flex: 1,
    					items: [
    						<?php write_combo_std('CCAZSA', 'Azzera % sconti articolo', $r['CCAZSA'], acs_ar_to_select_json($main_module->find_TA_std('CCT04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1.2", 'labelWidth' => 160) ) ?>,
    						<?php write_combo_std('CCAZSD', 'Azzera % sconti docu.', $r['CCAZSD'], acs_ar_to_select_json($main_module->find_TA_std('CCT04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 1.1", 'labelWidth' => 140, "labelAlign" => "right") ) ?>,
    						<?php write_combo_std('CCAZPR', 'Azzera provvigioni', $r['CCAZPR'], acs_ar_to_select_json($main_module->find_TA_std('CCT04', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('flex_width' => "flex: 0.9", 'labelWidth' => 110, "labelAlign" => "right") ) ?>,
    				
    					]}
						
								
								
						]
					}		
            
            ]
            
            }
            <?php }?>
        		
        		]},
            
            
            		
										
					
			],  //fine items form
			buttons: [
			
			  <?php if(trim($request['CCCCON']) == 'STD'){ ?>
				{
				 text : 'Listino a punti',
	             scale: 'medium',			                 
	             iconCls: 'icon-button_blue_play-24',
	             handler : function() {
	                 acs_show_win_std('Listino punti', '../desk_gest/acs_anag_cli_listino_punti.php?fn=open_panel', {cdcf : <?php echo j($request['cdcf']); ?>}, 1300, 500, null, 'icon-button_blue_play-16');	          	                	   
	         		
				 } //handler function()
				 
				 },
				 { xtype: 'tbfill'},
				<?php }?>
			
			<?php if($request['from_todo'] != 'Y'){?>
			   {
	            text: 'Salva',
	            iconCls: 'icon-save-24', scale: 'medium',
	            width : 120,
	            handler: function() {
            		var panel_form = this.up('form'),
	            		form = panel_form.getForm(),           	
	            		form_values = form.getValues();
	                    loc_win = this.up('window');
	            	if(form.isValid())
						 panel_form.acs_actions.exe_form_submit(panel_form); 
		
                         	
	            	
	            } //handler
	        } //salva
	        <?php }?>
	        ]
			
		 , acs_actions: {
	    	exe_form_submit: function(panel_form){
	    	
    	    	var form = panel_form.getForm(),
    		        loc_win = panel_form.up('window');
    	    	
    	    	Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_cc',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>,
						        	GCDEFA : <?php echo j(trim($r['GCDEFA'])); ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        	Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            if(jsonData.success == false){
						            	acs_show_msg_error(jsonData.msg_error);
						            }else{
						            	loc_win.fireEvent('afterEditRecord', loc_win);
						            }
						
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });		
	    	
	    	}}			
				
        }
        
         
        
       
<?php
} //write_new_form_COMM





function write_new_form_FIDO($p = array(), $request = array()){
	global $conn, $cfg_mod_Gest;

	if ($request['mode'] == 'EDIT'){
		//recupero la riga da modificare
	    $sql = "SELECT * FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']} WHERE CCDT=? AND CCPROG=? AND CCSEZI=? AND CCCCON=?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array($request['CCDT'], $request['CCPROG'], $request['CCSEZI'], $request['CCCCON']));

		$ret = array();
		$r = db2_fetch_assoc($stmt);

	} else {
		//NUOVA CONDIZIONE COMMERCIALE
		$r = array(	'CCDT' => $request['CCDT'],
				'CCPROG' => $request['CCPROG'],
				'CCSEZI' => $request['CCSEZI'],
				'CCCCON' => $request['CCCCON'],
				'CCDCON' => $request['CCDCON'],
		);
	}

	global $s, $main_module;
	?>
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            disabled: false, //true,
            autoScroll: true,
            
            flex: 1,
            
            defaults:{ anchor: '-10' , labelWidth: 130, xtype: 'textfield'},
            
            items: [
            		{xtype: 'hiddenfield', name: 'CCDT',   value: <?php echo j($r['CCDT']); ?>},
            		{xtype: 'hiddenfield', name: 'CCSEZI', value: <?php echo j($r['CCSEZI']); ?>},
            		{xtype: 'hiddenfield', name: 'CCPROG', value: <?php echo j($r['CCPROG']); ?>},
            		{xtype: 'hiddenfield', name: 'CCCCON', value: <?php echo j($r['CCCCON']); ?>},
            		{xtype: 'hiddenfield', name: 'CCDCON', value: <?php echo j($r['CCDCON']); ?>},
            
					{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [								
							{
								name: 'CCCCON',
								fieldLabel: 'Codice',
							    maxLength: 5,
							    labelWidth: 50, width: 130,
							    value: <?php echo j(trim(acs_u8e($r['CCCCON']))); ?>,
							    disabled: true
							}, {
								name: 'CCDCON',
								fieldLabel: 'Descrizione', flex: 1, labelAlign: 'right',
							    maxLength: 5,
							    value: <?php echo j(trim(acs_u8e($r['CCDCON']))); ?>,
							    disabled: true							
							}
							]
						}            
            
						, <?php write_numberfield_std('CCIMFD', 'Importo fido', trim($r['CCIMFD'])) ?>		
						, <?php write_datefield_std('CCDTVI', 'Data validit&agrave; iniziale', trim($r['CCDTVI'])) ?>
						, <?php write_datefield_std('CCDTVF', 'Data validit&agrave; finale', trim($r['CCDTVF'])) ?>
					
									
            
					
			],
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-save-32', scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')
	            	var loc_win = this.up('window');	            	
	            	
					if(form.isValid()){
						Ext.getBody().mask('Loading... ', 'loading').show();
						Ext.Ajax.request({
								timeout: 240000,
						        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_fido',
						        jsonData: { 
						        	form_values: form.getValues(),
						        	open_request: <?php echo acs_je($request) ?>
						        },	
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        						        
									Ext.getBody().unmask();
						            var jsonData = Ext.decode(result.responseText);
						            
						            loc_win.fireEvent('afterEditRecord', loc_win);
						            
						        },
						        failure    : function(result, request){
									Ext.getBody().unmask();							        
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });						
				    }	            	
	            	
	            } //handler
	        } //salva
	        ],             
			
			
			listeners: {
		        afterrender: function(){
		        },
		    }			
			
				
				
        }
<?php
} //write_new_form_FIDO


if($_REQUEST['fn'] == 'exe_check_cuba'){
    
    $ret = array();
    
    $abi = sprintf("%05s", $m_params->form_values->f_abi);
    $cab = sprintf("%05s", $m_params->form_values->f_cab);
    
    $sql = "SELECT COUNT(*) AS C_ROW, TANR, TADESC, TADES2
    FROM {$cfg_mod_Gest['file_tab_sys']} TA
    WHERE TADT = '{$id_ditta_default}' AND TAID = 'CUBA'
    AND SUBSTRING(TAREST, 61, 5) = '{$abi}'
    AND SUBSTRING(TAREST, 66, 5) = '{$cab}'
    GROUP BY TANR, TADESC, TADES2";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    if($row['C_ROW'] > 0){
        $ret['CCBANC']   = trim($row['TANR']);
        $ret['des_cuba'] = trim($row['TADESC']).trim($row['TADES2']);
        
    }else{
        $ret['CCBANC']   = "";
        $ret['des_cuba'] = "Abi/Cab non acquisito";
    }
    
    
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
}


if($_REQUEST['fn'] == 'exe_check_abicab'){
    
    $ret = array();
    
    $abi = $m_params->form_values->CCABI;
    $cab = $m_params->form_values->CCCAB;
    $banca = $m_params->form_values->CCBANC;
    if(trim($banca) != '')
        $sql_where = " AND TANR = '{$banca}'";
        else{
            $sql_where = " AND SUBSTRING(TAREST, 61, 5) = '{$abi}' AND SUBSTRING(TAREST, 66, 5) = '{$cab}'";
        }
        
        $sql = "SELECT SUBSTRING(TAREST, 61, 5) AS ABI, SUBSTRING(TAREST, 66, 5) CAB, TANR,
        TADESC, TADES2
        FROM {$cfg_mod_Gest['file_tab_sys']} TA
        WHERE TADT = '{$id_ditta_default}' AND TAID = 'CUBA'
        {$sql_where}
        ";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg($stmt);
        $row = db2_fetch_assoc($stmt);
        
        if($row != false && count($row) > 0){
            if(trim($banca) == ''){
                $ret['CCBANC']   = trim($row['TANR']);
                $ret['des_cuba'] = trim($row['TADESC']).trim($row['TADES2']);
                $ret['success']  = true;
            }else{
                if($row['ABI'] != trim($abi) || $row['CAB'] != trim($cab)){
                    $ret['success'] = false;
                    $ret['msg_error'] = "ABI/CAB non corrispondono a quelli del codice banca";
                }
                
            }
        }else{
            $ret['CCBANC']   = "";
            $ret['des_cuba'] = "Abi/Cab non acquisito";
            $ret['success']  = true;
        }
        
        
        
        echo acs_je($ret);
        exit;
        
}

