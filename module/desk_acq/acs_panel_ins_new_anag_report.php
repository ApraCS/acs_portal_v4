<?php

require_once "../../config.inc.php";
require_once("acs_panel_ins_new_anag.php");

ini_set('max_execution_time', 6000);
ini_set('memory_limit', '2048M');

$main_module = new DeskAcq();
$s = new Spedizioni(array('abilita_su_modulo' => 'DESK_ACQ'));

$users = new Users;
$ar_users = $users->find_all();


foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>

<html>
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

  
    tr.liv_totale td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;  font-weight: bold;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   tr.liv0 td{font-weight: bold;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold; background-color: #cccccc;}
   tr.liv_vuoto td{border-left: 0px ; border-right: 0px;}
   
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	 
 		  table.int1 {
        border-collapse: unset;
	}
      
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);
$sql_where = sql_where_params($form_values);

$area_manager = $form_values->CCARMA;
$agente = $form_values->CCAGE;

if(count($area_manager) > 0 || count($agente) > 0 ){
    
    $where = '';
    if(count($area_manager) > 0)
        $where .= sql_where_by_combo_value('CCARMA', $area_manager);
    if(count($agente) > 0){
        if(count($agente) == 1)
            $where .= " AND (CCAG1 = '{$agente[0]}' OR CCAG2 = '{$agente[0]}' OR CCAG3 = '{$agente[0]}')";
        if(count($agente) > 1)
            $where .= " AND (CCAG1 IN (" . sql_t_IN($agente) . ") OR CCAG2 IN (" . sql_t_IN($agente) . ") OR CCAG3 IN (" . sql_t_IN($agente) . "))";
    }
        
        $sql_join = " INNER JOIN (
        SELECT CCPROG
        FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio_cc']}
        WHERE CCDT = '{$id_ditta_default}' AND CCSEZI = 'BCCO' AND CCCCON = 'STD' {$where}) CC
        ON CC.CCPROG = GC.GCPROG";
}

//elenco anagrafiche clienti
$sql = "SELECT GC.*, CF.* {$sql_campi_select}
        FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
        {$sql_join}
        LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_anag_cli']} CF
        ON GC.GCDT = CF.CFDT AND GC.GCCDCF = digits(CF.CFCD)
        WHERE GCDT = '{$id_ditta_default}' AND GCTPAN = 'FOR' {$sql_where}
        ORDER BY CFDTGE DESC
        LIMIT 100";
        
        

    
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

$sql_dp = "SELECT GC.*
          FROM {$cfg_mod_DeskAcq['ins_new_anag']['file_appoggio']} GC
          WHERE GCDT = '{$id_ditta_default}' AND GCTPAN IN ('DES', 'PVEN') AND GCPRAB = ?
          ORDER BY UPPER(GCDCON)
          LIMIT 100";
        
$stmt_dp = db2_prepare($conn, $sql_dp);
echo db2_stmt_errormsg();

while($row = db2_fetch_assoc($stmt)){
    $nr = array();
    $nr = $row;

    $ar[] = $nr;
}

   

echo "<div id='my_content'>";
echo "<div class=header_page>";
echo "<H2>Anagrafica fornitori</H2>";

echo "</div>";
echo "<div style=\"text-align: right; margin-bottom:10px; \"> Data elaborazione: " .  Date('d/m/Y H:i') . "  </div>";

echo "<table class=int1>";
echo "<tr class='liv_data'>";

echo "
    <th rowspan=2><img src=" . img_path("icone/48x48/divieto.png") . " height=20></th>
    <th rowspan=2>TI</th>
	<th rowspan=2>Codice</th>
	<th rowspan=2>Ragione sociale/Destinazione</th>
    <th rowspan=2>Localit&agrave;</th>
    <th rowspan=2>Indirizzo</th>
    <th rowspan=2>Cap</th>
    <th rowspan=2>Pr.</th>
    <th rowspan=2>Naz.</th>
    <th rowspan=2>Cod. Fiscale</th>
    <th rowspan=2>Partita IVA</th>
    <th colspan = 2 class='center'> Immissione </th>
    <tr class='liv_data'>
          <th>Data</th>
          <th>Utente</th>
      </tr>
    ";

echo "</tr>";  

if(is_array($ar)){
foreach ($ar as $kar => $r){
    
    echo "<tr>";
    
    if(trim($r['GCSOSP']) == 'S'){
        echo "<td><img src=" . img_path("icone/48x48/divieto.png") . " height=15></td>";
    }else{
        echo "<td>&nbsp;</td>";
    }
    
    if(trim($r['GCCRM1']) == 'P' || trim($r['GCCRM1']) == 'E'){
        echo "<td><img src=" . img_path("icone/48x48/shopping_bag.png") . " height=15></td>";
    }elseif(trim($r['GCCRM1']) == 'D'){
        echo "<td><img src=" . img_path("icone/48x48/barcode.png") . " height=15></td>";
    }else{
        echo "<td>".$r['GCCRM1']."</td>";
    }
    
    
    
   echo "<td valign='top'>".$r['GCCDCF']."</td>
        <td valign='top'>".$r['GCDCON']."</td>
        <td valign='top'>".$r['GCLOCA']."</td>
		<td valign='top'>".$r['GCINDI']."</td>
        <td valign='top'>".$r['GCCAP']."</td>
	    <td valign='top'>".$r['GCPROV']."</td>";
   if(trim($r['GCNAZI']) != ''){
       $naz = get_TA_sys('BNAZ', trim($r['GCNAZI']));
       $d_nazione = trim($naz['text']);
       echo "<td valign='top'>[".trim($r['GCNAZI'])."] ".$d_nazione."</td>";
   }else{
       echo "<td>&nbsp;</td>";
   }   
   if(trim($r['GCCDFI']) != trim($r['GCPIVA']))     
       echo"<td valign='top'><b>".$r['GCCDFI']."<b></td>";
   else
       echo"<td valign='top'>".$r['GCCDFI']."</td>";
   echo"<td valign='top'><b>".$r['GCPIVA']."</b></td>
        <td valign='top'>".print_date($r['CFDTGE'])."</td>
        <td valign='top'>".$r['CFUSGE']."</td>";
    echo "</tr>";
    
    $result = db2_execute($stmt_dp, array($r['GCPROG']));
    while($row = db2_fetch_assoc($stmt_dp)){
        echo "<tr>";
        if(trim($row['GCSOSP']) == 'S'){
            echo "<td><img src=" . img_path("icone/48x48/divieto.png") . " height=15></td>";
        }else{
            echo "<td>&nbsp;</td>";
        }
        
        if(trim($row['GCCRM1']) == 'P' || trim($row['GCCRM1']) == 'E'){
            echo "<td><img src=" . img_path("icone/48x48/shopping_bag.png") . " height=15></td>";
        }elseif(trim($row['GCCRM1']) == 'D'){
            echo "<td><img src=" . img_path("icone/48x48/barcode.png") . " height=15></td>";
        }else{
            echo "<td>".$row['GCCRM1']."</td>";
        }
        
        echo "<td valign='top'>[".trim($row['GCTPAN'])."]".$row['GCCDCF']."</td>
        <td valign='top'>".$row['GCDCON']."</td>
        <td valign='top'>".$row['GCLOCA']."</td>
		<td valign='top'>".$row['GCINDI']."</td>
        <td valign='top'>".$row['GCCAP']."</td>
	    <td valign='top'>".$row['GCPROV']."</td>";
        if(trim($r['GCNAZI']) != ''){
            $naz = get_TA_sys('BNAZ', trim($row['GCNAZI']));
            $d_nazione = trim($naz['text']);
            echo "<td valign='top'>[".trim($row['GCNAZI'])."] ".$d_nazione."</td>";
        }else{
            echo "<td>&nbsp;</td>";
        }  
        if(trim($row['GCCDFI']) != trim($row['GCPIVA']))
            echo"<td valign='top'><b>".$row['GCCDFI']."<b></td>";
            else
                echo"<td valign='top'>".$row['GCCDFI']."</td>";
        echo "<td valign='top'><b>".$row['GCPIVA']."</b></td>
              <td valign='top'>".print_date($row['CFDTGE'])."</td>
              <td valign='top'>".$row['CFUSGE']."</td>";
        echo "</tr>";
    }
    

            
}

}


?>
</div>
</body>
</html>	

