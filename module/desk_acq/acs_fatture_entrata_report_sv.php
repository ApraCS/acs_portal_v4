<?php
require_once "../../config.inc.php";
require_once "acs_fatture_entrata_include.php";


$m_DeskAcq = new DeskAcq();

$main_module=new DeskAcq();

$da_form = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
	          
                 {
				     name: 'f_data_in'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data iniziale'
				   , labelAlign: 'left'
				   , labelWidth: 150
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
				}, {
				     name: 'f_data_fin'
				   , flex: 1                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data finale'
				   , labelAlign: 'left'
				   , labelWidth: 150
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 }
			}, {
                    xtype: 'checkbox'
                  , flex: 1.5
                  , name: 'f_solo_scos' 
                  , boxLabel: 'Solo fatture con scostamento'
                  , checked: false
                  , inputValue: 'Y'
                },{
                    xtype: 'checkbox'
                  , flex: 1.5
                  , name: 'f_solo_var' 
                  , boxLabel: 'Solo fatture con variazione'
                  , checked: false
                  , inputValue: 'Y'
                }, {
                    xtype: 'checkbox'
                  , flex: 1.5
                  , name: 'f_filtra_fatture' 
                  , boxLabel: 'Solo fatture controllate'
                  , checked: false
                  , inputValue: 'Y'
                }
	            ],
	            
				buttons: [	
				{
			            text: 'Report',
				        iconCls: 'icon-print-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues()),
		                        	filter : '<?php echo json_encode($m_params->filter); ?>'
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        }
		        
		        
		        ]          
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	
	if ($_REQUEST['fn'] == 'open_report'){

// ******************************************************************************************
// REPORT
// ******************************************************************************************
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   tr.liv_tot td{ background-color: #DDDDDD; font-weight: bold;} 
   
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);


$form_values_f =	strtr($_REQUEST['filter'], array('\"' => '"'));
$form_values_f = json_decode($form_values_f);

$form_values->f_forn = $form_values_f->f_forn;

 $ddt_nf = 'N';
        
$ar=crea_ar_tree_fatture_entrata('', $form_values, 'Y', 'Y', $ddt_nf); //forza generazione completa
        
$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
        WHERE TFNRDO LIKE '{$m_params->open_request->chiave}%' ORDER BY TFDTGE DESC, TFORGE DESC FETCH FIRST 1 ROWS ONLY";
       
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);
$r = db2_fetch_assoc($stmt);
$data_ora = print_date($r['TFDTGE']) . " " . print_ora($r['TFORGE']);
        
        echo "<div id='my_content'>";
        echo "<div class=header_page>";
        echo "<H2>Riepilogo Scostamenti/Variazioni (Dal ".print_date($form_values->f_data_in)." al ".print_date($form_values->f_data_fin).")</H2>";
        
        echo"</div>
        <div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";
        
        echo "<table class=int1>";
        
        echo "<tr class='liv_data'>";
        echo "<th>Anno/Mese/Fornitore/Fattura</th>";
        echo "<th>Data</th>";
        echo "<th>Imponibile fattura</th>";
        echo "<th>Totale fattura</th>";
        echo "<th>Imponibile contabile</th>";
        echo "<th>Totale contabile</th>";
        echo "<th>Importo scostamento</th>";
        echo "<th>Importo variazione</th>";
        
        echo "</tr>";
        
        foreach ($ar as $kar => $r){
         
            /*echo "<tr class ='liv3'> <td>".$r['descr']." [#".$r['num_sottoliv']."]</td>";
            echo "<td>".print_date($r['data_reg'])."</td>";
            echo "<td class = number>".n($r['fat_imp'],2)."</td>";
            echo "<td class = number>".n($r['fat_totd'],2)."</td>";
            echo "<td class = number>".n($r['IMP_CONT'],2)."</td>";
            echo "<td class = number>".n($r['TOT_CONT'],2)."</td>";
            echo "<td class = number>".n($r['SCOST'],2)."</td>";
            echo "<td class = number>".n($r['VAR'],2)."</td>";
            echo "</tr>";*/
            
            foreach ($r['children'] as $kar1 => $r1){
               
                                                
                echo "<tr class='liv2'><td style='font-size:12px;'><b>".$r1['descr']."</b></td>";
                echo "<td>".print_date($r1['data_reg'])."</td>";
                echo "<td class = number>".n($r1['fat_imp'],2)."</td>";
                echo "<td class = number>".n($r1['fat_totd'],2)."</td>";
                echo "<td class = number>".n($r1['imp_cont'],2)."</td>";
                echo "<td class = number>".n($r1['tot_cont'],2)."</td>";
                echo "<td class = number>".n($r1['scost'],2)."</td>";
                echo "<td class = number>".n($r1['var'],2)."</td>";
                echo "</tr>";
                                                 
            
            foreach ($r1['children'] as $kar2 => $r2){
             
                // $commento= $main_module->get_commento_ordine_by_k_ordine($r2['k_ordine_fatt']);  //'1 _AD_AD1_2017_000013'
                 
                 echo "<tr><td>".$r2['descr']."</td>";
                 echo "<td>".print_date($r2['data_reg'])."</td>";
                 echo "<td class = number>".n($r2['fat_imp'],2)."</td>";
                 echo "<td class = number>".n($r2['fat_totd'],2)."</td>";
                 echo "<td class = number>".n($r2['imp_cont'],2)."</td>";
                 echo "<td class = number>".n($r2['tot_cont'],2)."</td>";
                 echo "<td class = number>".n($r2['scost'],2)."</td>";
                 echo "<td class = number>".n($r2['var'],2)."</td>";
                 
            
                 echo "</tr>";
                                                
                 }
                 
                 $tot_fat_imp += $r1['fat_imp'];
                 $tot_fat_totd += $r1['fat_totd'];
                 $tot_imp_cont += $r1['imp_cont'];
                 $tot_tot_cont += $r1['tot_cont'];
                 $tot_scost += $r1['scost'];
                 $tot_var += $r1['var'];
                                                 
                                            }
                                            
                                            
                     }
                     
                     echo "<tr class = liv_tot>"; 
                     echo "<td> Totale generale</td>";
                     echo "<td>&nbsp;</td>";
                     echo "<td class = number>".n($tot_fat_imp,2)."</td>";
                     echo "<td class = number>".n($tot_fat_totd,2)."</td>";
                     echo "<td class = number>".n($tot_imp_cont,2)."</td>";
                     echo "<td class = number>".n($tot_tot_cont,2)."</td>";
                     echo "<td class = number>".n($tot_scost,2)."</td>";
                     echo "<td class = number>".n($tot_var,2)."</td>";
                     echo "</tr>";




	}






