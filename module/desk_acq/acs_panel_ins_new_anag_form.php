<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq(array('abilita_su_modulo' => 'DESK_VEND'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'open_form_filtri'){ ?>

   {"success":true, "items": [

        {
            xtype: 'form',
            flex: 1,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
        	frame: true,
            items: [
            <?php if($m_params->from_opm != 'Y'){?>
            	{xtype: 'fieldcontainer',
				flex:1,
				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
				items: [
				
			   	{
                    xtype: 'combo',
        			name: 'f_cliente_cod',
        			flex:1,
        			fieldLabel: 'Denominazione',
        			minChars: 0,			
                    margin: "5 20 5 10",
                    itemId : 'search_cli',
                    store: {
                    	pageSize: 1000,
                    	
        				proxy: {
        		            type: 'ajax',
        		            url : <?php
        		            		echo acs_je('../desk_acq/acs_get_select_json.php?select=search_for_anag');
        		            		?>,
        		            reader: {
        		                type: 'json',
        		                root: 'root',
        		                totalProperty: 'totalCount'
        		            },
        		              actionMethods: {
        							          read: 'POST'
        							        },
        		         
                		 doRequest: personalizza_extraParams_to_jsonData
        		        }, 
        		              
        				fields: ['cod', 'descr', 'out_loc'],		             	
                    },
                                
        			valueField: 'cod',                        
                    displayField: 'descr',
                    typeAhead: false,
                    hideTrigger: true,
                    anchor: '100%',
                    
        	       listeners: {
                                    'change': function(field,newVal, b){
                                    this.up('form').agg_pro(this.up('form'));
                                         combo_cli = this.up('form').down('#search_cli');   
                                         delete combo_cli.lastQuery;
                            			
                                    }
                                    
                                  },        
        
                    listConfig: {
                        loadingText: 'Searching...',
                        emptyText: 'Nessun cliente trovato',
                        
        
                        // Custom rendering template for each item
                        getInnerTpl: function() {
                            return '<div class="search-item">' +
                                '<h3><span>{descr}</span></h3>' +
                                '[{cod}] {out_loc}' + 
                            '</div>';
                        }                
                        
                    },
                    
                    pageSize: 1000
        
                }, 
        
                { 
        			xtype: 'textfield',
        			fieldLabel: 'Codice', 
        			name: 'f_cod',
        			margin : '5 0 5 10',
        			flex : 1,
        			labelWidth : 50,
        			labelAlign : 'right'
        			 }
			 
				]},
				
        		{
					xtype: 'fieldset',
	                title: 'Riferimenti diretti',
	                layout: 'anchor',
	         		items: [  
	         		
	         		{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				{
            			name: 'f_cod_fi',						
            			xtype: 'textfield',
            			fieldLabel: 'Codice fiscale',
            		  	flex : 1					
                		},{
            			name: 'f_piva',						
            			xtype: 'textfield',
            			fieldLabel: 'Partita iva',
            		  	labelAlign : 'right',
            		  	labelWidth : 70,
            		  	flex : 1					
                		}
        				
    				]},{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				{
            			name: 'f_tel',						
            			xtype: 'textfield',
            			fieldLabel: 'Telefono',	
            			flex : 1							
            		},{
            			name: 'f_email',						
            			xtype: 'textfield',
            			fieldLabel: 'Email',
            			labelAlign : 'right',
            		  	labelWidth : 70,
            		  	flex : 1									
            		}
        				
    				]},
    				{xtype: 'fieldcontainer',
    				flex:1,
    				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
    				items: [
    				 {
					name: 'f_sos',
					xtype: 'radiogroup',
					fieldLabel: 'Sospesi',
					flex:1,
					allowBlank: true,
				   	
				   	items: [{
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Solo'
                          , inputValue: 'Y'
                          , width: 45
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Inclusi'
                          , inputValue: 'T'
                          , width: 60   
						  , checked: true                       
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Esclusi'	
                          , inputValue: 'N'
                          , width: 60
                         
                          
                        }]
					}, {xtype : 'component', flex : 1}
        				
        			]}
    				 
	         	 
            	 
	             ]},
	             
	             <?php }?>
	             
	             <?php if($m_params->from_opm == 'Y'){?>
	             
	             {
					name: 'f_sos',
					xtype: 'radiogroup',
					margin : '0 0 0 10',
					fieldLabel: 'Sospesi',
					width : 400,
					allowBlank: true,
				   	
				   	items: [{
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Solo'
                          , inputValue: 'Y'
                          , width: 45
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Inclusi'
                          , inputValue: 'T'
                          , width: 60   
						  , checked: true                       
                        }, {
                            xtype: 'radio'
                          , name: 'f_sospeso' 
                          , boxLabel: 'Esclusi'	
                          , inputValue: 'N'
                          , width: 60
                         
                          
                        }]
					},
	             
	             <?php }?>
	             
	             {
					xtype: 'fieldset',
	                title: 'Raggruppamenti',
	                layout: 'anchor',
	         		items: [  
	         		
	         	 	{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				 <?php write_combo_std('GCNAZI', 'Nazione', '', acs_ar_to_select_json(find_TA_sys('BNAZ', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>
        			       , {xtype: 'textfield', 
        			         name: 'GCLOCA', 
        			         flex: 0.9,
        			         margin : '0 0 0 22',
        			         fieldLabel: 'Localit&agrave;',
        			         labelAlign : 'right',
							 labelWidth : 48,
    						 },{										  
								xtype: 'displayfield',
								editable: false,
								fieldLabel: '',
								padding: '0 0 0 5',
							    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
								listeners: {
							            render: function( component ) {
							            
							            	m_form = this.up('form').getForm();
							            	component.getEl().on('dblclick', function( event, el ) {
											my_listeners_search_loc = {
					        					afterSelected: function(from_win, record_selected){
						        						m_form.findField('GCLOCA').setValue(record_selected.get('descr'));
						        						m_form.findField('GCPROV').setValue(record_selected.get('COD_PRO'));
						        						m_form.findField('GCNAZI').setValue(record_selected.get('COD_NAZ'));
						        						from_win.close();
									        		}										    
										    },
											acs_show_win_std('Seleziona localit&agrave;', 
												'../desk_gest/search_loc.php?fn=open_search', 
												{}, 450, 150, my_listeners_search_loc, 'icon-sub_blue_add-16');
											});										            
							             }
									}										    
							}
        				
    				]},
	         		{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				  <?php write_combo_std('GCTPIN', 'Tipo indirizzo', $r['GCTPIN'], acs_ar_to_select_json($main_module->find_TA_std('INDI', null, 'N', 'N', null, null, null, 'N', 'Y'), ''), array('multiSelect' => 'true', 'transorm' => 'NO')) ?>
        				  , {  xtype : 'textfield',
							name: 'GCPROV', labelWidth: 70,
							flex : 1,
							fieldLabel: 'Provincia', labelAlign: 'right',
						    maxLength: 5
						}
        				
    				]},
    				{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				<?php write_combo_std('GCCRM1', 'Class. CRM1', $r['GCCRM1'], acs_ar_to_select_json(find_TA_sys('BCR1', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>
        				 , <?php write_combo_std('GCCRM2', 'Class. CRM2', $r['GCCRM2'], acs_ar_to_select_json(find_TA_sys('BCR2', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'labelWidth' => 70, 'multiSelect' => 'true')) ?>					
					]},
    				{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				  	
						<?php write_combo_std('GCSEL1', 'Classific.1', $r['GCSEL1'], acs_ar_to_select_json(find_TA_sys('CUS1', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>	
						, <?php write_combo_std('GCSEL2', 'Classific.2', $r['GCSEL2'], acs_ar_to_select_json(find_TA_sys('CUS2', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true', 'labelWidth' => 70, 'labelAlign' => 'right')) ?>	
							
					]},
    					{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				      
    						 <?php write_combo_std('GCSEL3', 'Classific.3', $r['GCSEL3'], acs_ar_to_select_json(find_TA_sys('CUS3', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('multiSelect' => 'true')) ?>	
    						, <?php write_combo_std('GCSEL4', 'Classific.4', $r['GCSEL4'], acs_ar_to_select_json(find_TA_sys('CUS4', null, null, null, null, null, 0, '', 'Y', 'Y'), '', false), array('multiSelect' => 'true', 'labelWidth' => 70, 'labelAlign' => 'right', 'transorm' => 'NO')) ?>
    						
					]},
    				{xtype: 'fieldcontainer',
        				flex:1,
        				layout: { 	type: 'hbox', pack: 'start',	align: 'stretch'},						
        				items: [
        				
        				 
    				    {
        						name: 'f_todo',
        						xtype: 'combo',
        						flex : 1,
        						fieldLabel: 'To Do',
        						displayField: 'text',
        						valueField: 'id',
        						emptyText: '- seleziona -',
        						forceSelection: true,
        					   	allowBlank: true,		
        					   	multiSelect : true,											
        						store: {
        							autoLoad: true,
        							editable: false,
        							autoDestroy: true,	 
        						    fields: [{name:'id'}, {name:'text'}],
        						    data: [								    
        							     <?php
        							     echo acs_ar_to_select_json($main_module->find_TA_std('ATTAV', null, 'Y', 'N', null, null, 'CLI'), "");
        							       ?>	
        							    ] 
        						}						 
        					}
    				    
    				    ,<?php write_combo_std('GCCIVI', 'Ciclo di vita', $r['GCCIVI'], acs_ar_to_select_json(find_TA_sys('CIVI', null, null, null, null, null, 0, '', 'Y', 'Y'), ''),  array('multiSelect' => 'true', 'labelAlign' => 'right', "labelWidth" => 70)) ?>
    				    			
        			]}
            	 
	             ]}
						
						            
            ],
            
            buttons: [
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "ANAG_CLI");  ?>
            <?php if($m_params->from_opm == 'Y'){?>
             {
	            text: 'Visualizza clienti inclusi',
	            iconCls: 'icon-gift-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            		
	            	if(form.isValid()){
	            		            	
	            		acs_show_win_std('Genera ordine marketing', 'acs_genera_ordine_marketing.php?fn=open_form', {
	            		form_values: form.getValues()}, 
	            		400, 250, null, 'icon-gift-16');		            
			        }
		            	
			           // this.up('window').destroy();
			                 	                	                
	            }
	        }
	        <?php }else{?>
            {
	            text: 'Crea nuovo',
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            		//per ora: apro elenco vuoto e poi dovra' riselezionare 'Crea nuova anagrafica'           	
	            	     acs_show_panel_std('<? echo ROOT_PATH ?>module/desk_acq/acs_panel_ins_new_anag.php?fn=open_tab', null, {
        		            	form_open: {f_cliente_cod: -1}, 
        		                recenti : 50
    		            	}, null, null);
    		            
		            	
			            this.up('window').destroy();
			                 	                	                
	            }
	        }, {
	            text: 'Recenti [50]',
	            iconCls: 'icon-news-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){	            	
	            	     acs_show_panel_std('<? echo ROOT_PATH ?>module/desk_acq/acs_panel_ins_new_anag.php?fn=open_tab', null, {
        		            	form_open: form.getValues(), 
        		                recenti : 50
    		            	}, null, null);
    		            
		            	
			            this.up('window').hide();
			        }            	                	                
	            }
	        },	{
	            text: 'Grafici',
	            iconCls: 'icon-grafici-32',
	            scale: 'large',
	            handler: function() {
	            
	                var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){
	            		            	
	            		acs_show_win_std('Anagrafica articoli', '../desk_acq/acs_panel_ins_new_anag_grafici.php?fn=open', {
	            		form_open: form.getValues()}, 
	            		800, 400, null, 'icon-grafici-16', 'Y');		            
			        }
	                      	                	                
	            }
	            },{
	            text: 'Report',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function() {
                    form = this.up('form').getForm();
                    this.up('form').submit({
                    url: '../desk_acq/acs_panel_ins_new_anag_report.php',
                    target: '_blank', 
                    standardSubmit: true,
                    method: 'POST',                        
                    params: {
                        form_values: Ext.encode(form.getValues())
				    }
          			});            	                	     
    	         }
	            } ,{
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();
	            	var win_id = this.up('window').getId();
	            	
	            	if(form.isValid()){	            	
            			acs_show_panel_std('../desk_acq/acs_panel_ins_new_anag.php?fn=open_tab', null, {
    		            	form_open: form.getValues()
    		           }, null, null);
    		            
		            	this.up('window').close();
			        }            	                	                
	            }
	        }
	        <?php }?>
            ]
            } 
    
    ]}
    
 <?php 
 
 exit;
}