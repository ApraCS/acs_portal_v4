<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();

if ($_REQUEST['fn'] == 'open_form'){
    $m_params = acs_m_params_json_decode();
    
    ?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            items: [
        		   {
				     name: 'f_data_sca'             		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data'
				   , labelAlign: 'left'
				   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'
				   , allowBlank: true
				   , anchor: '-15'
				   , flex: 1
				   , value: '<?php echo print_date($m_params->rows->data_sca, "%d/%m/%Y") ?>' 
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
			 			}
			      },{
					name: 'f_tipo_sca',
					xtype: 'combo',
					fieldLabel: 'Tipo',
					value: '<?php echo trim($m_params->rows->tipo); ?>',
					displayField: 'text',
					valueField: 'id',
					emptyText: '- seleziona -',
					forceSelection:true,
				   	allowBlank: true,														
					store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}, {name:'tacint'}],
						    data: [
							    <?php echo acs_ar_to_select_json(find_TA_sys('C2TS', null, null, null, null, null, 0, "", 'Y'), '') ?>
							    ]
						},  listeners: { 
			 		  change: function (field,newVal, oldValue) {
			 		     var form  = this.up('form'); 
			 		     var d_tipo = this.up('form').down('#d_tipo'); 
			 		     var desc = field.store.data.map[newVal].data.tacint;  
			 		     d_tipo.setValue(desc);
    	         		
    				 }
 			},						 
				},{
					name: 'f_desc_tipo',
					xtype: 'textfield',	
					itemId : 'd_tipo',
				    hidden: true
				   },
	           		{
					name: 'f_impo_sca',
					xtype: 'numberfield',	
				    hideTrigger : true,
				    fieldLabel : 'Importo',
				    flex: 1,
				    value : '<?php echo $m_params->rows->impo_sca ?>'
				   },
                 
	            ],
	            
				buttons: [	
				{
			            text: 'Conferma',
				        iconCls: 'icon-blog_accept-32',		            
				        scale: 'large',		            
			            handler: function() {
			            	var loc_win = this.up('window');
			                var form_values = this.up('form').getValues();
			                loc_win.fireEvent('afterConf', loc_win, form_values);
			                
			            }
			        } 
		        
		        ]            
	           
	}
		
  ]}
	
<?php 
exit;
}