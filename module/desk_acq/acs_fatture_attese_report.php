<?php
require_once "../../config.inc.php";
require_once "acs_fatture_entrata_include.php";


$m_DeskAcq = new DeskAcq();

$main_module=new DeskAcq();


$da_form = acs_m_params_json_decode();

// ******************************************************************************************
// REPORT
// ******************************************************************************************
$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);
?>

<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   div.header_page{font-size: 18px; padding: 5px; margin-top: 10px; margin-bottom: 15px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}
   .center{text-align: center;}
   .grassetto{font-weight: bold; text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #DDDDDD;} 
   .giallo {background-color: #F4E287;}
   .verde {background-color: #A0DB8E;}
   tr.liv_data th{font-weight: bold;}
   tr.e_row td{border: 0px;  padding-bottom: 20px}
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
      
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	  
 		  table.int1 {
        border-collapse: unset;
	}
 	 
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>
 </head>
 
 <body>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<?php 

$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
$form_values = json_decode($form_values);

if(isset($_REQUEST['value_f_solo_attese']) && $_REQUEST['value_f_solo_attese'] == 'Y'){
    $form_values->f_solo_attese = 'Y';
    $title = 'Riepilogo fatture attese per mese';
}

if(isset($_REQUEST['value_ddt_attese']) && $_REQUEST['value_ddt_attese'] == 'Y'){
    $form_values->f_solo_con_note_credito_attese_aperte = 'Y';
    $title = 'Riepilogo note variazioni attese';
}

if(isset($_REQUEST['report_ddt_variazioni_attese']) && $_REQUEST['report_ddt_variazioni_attese'] == 'Y'){
    $var_attese = $_REQUEST['report_ddt_variazioni_attese'];
    $form_values->report_ddt_variazioni_attese = 'Y';
}

if(isset($_REQUEST['totale']) && $_REQUEST['totale'] == 'Y'){
        $form_values->totale = 'Y';
}

$ar_tot = crea_ar_tree_fatture_entrata('', $form_values, 'Y', 'Y');

$form_values->totale = 'N';
$ar=crea_ar_tree_fatture_entrata('', $form_values, 'Y', 'Y'); //forza generazione completa

$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
WHERE TFNRDO LIKE '{$m_params->open_request->chiave}%' ORDER BY TFDTGE DESC, TFORGE DESC FETCH FIRST 1 ROWS ONLY";

/*print_r($m_params);
exit;
*/
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);
$r = db2_fetch_assoc($stmt);
$data_ora = print_date($r['TFDTGE']) . " " . print_ora($r['TFORGE']);
$count_row = 1;

$filtri_cat = array('anno' => '?',
                    'mese' => '?',
                    'fornitore' => '?',
                    'form_values' => $form_values
);

$filtri_tot = array('anno' => '?',
                    'mese' => '?',
                    'form_values' => $form_values
    
);


echo "<div id='my_content'>";
echo "
  		<div class=header_page>
			<H2>".$title."</H2>
 		</div>
		<div style=\"text-align: right; margin-bottom:10px; \"> [Aggiornato al {$data_ora}] </div>";


if(strlen($form_values->f_mese) == 0 && $form_values->value_ddt_attese =='Y'){

    
    echo "<table class=int1>";
    echo "<tr class='liv_data'>
		<th rowspan=2>Fornitore</th>";
    
    if($var_attese == 'Y')
        echo "<th rowspan=2> Stato </th>";
        
    if($var_attese == 'Y')
        echo "<th colspan =4 class='center'> Note variazione</th>";
    else
        echo "<th colspan =4 class='center'> Ddt entrata</th>";
                
     echo "</tr><tr class='liv_data'>";
                
     if($var_attese == 'Y')
         echo "<th>Nr doc</th>";
     else
         echo "<th>Nr ddt</th>";
   
   echo "<th>Merce</th>
		<th>Imponibile</th>
		<th>Totale</th>
      
</tr>";
   
   foreach ($ar_tot as $kar => $r){
       
       echo "</tr> <tr class ='liv_totale'> <td  style='font-size:12px;'>".$r['cod']." [#".$r['num_sottoliv']."]</td>";
       
       if($var_attese == 'Y')
           echo"<td>&nbsp;</td>";
           
           echo "<td>&nbsp;</td>
	  <td class='number'>". n($r['ddt_merce'],2)."</td>
	  <td class='number'>".n($r['ddt_imp'],2)."</td>
	  <td class='number'>".n($r['ddt_totd'],2)."</td>
			    
  		</tr>";
           
           foreach ($r['children'] as $kar1 => $r1){
               
               echo "<tr class='liv1'><td>".$r1['descr']."</td>";
               
               if($var_attese == 'Y')
                   echo "<td>".$r1['stato']."</td>";
                   
                   echo "<td>".$r1['num_ddt']."</td>";
                   echo " <td class='grassetto'>". n($r1['ddt_merce'],2)."</td>
               <td class='number'>".n($r1['ddt_imp'],2)."</td>
   		       <td class='number'>".n($r1['ddt_totd'],2)."</td>
    
  		</tr>";
                   
                   $stmt_c = get_categoria($filtri_cat, 'Y');
                   $result = db2_execute($stmt_c, array($r1['anno'], $r1['mese'], $r1['forn']));
                   while($row_c = db2_fetch_assoc($stmt_c)){
                       
                       $row_ta = find_TA_sys('MUTA', trim($row_c['RDTBCO']));
                       if(trim($row_c['RDTBCO']) == ''){
                           $categoria = 'Non definito';
                       }else{
                           $categoria = "[".$row_c['RDTBCO']."] ".$row_ta[0]['text'];
                       }
                       
                       
                       echo "<tr><td>".$categoria."</td>";
                       if($var_attese == 'Y')
                           echo"<td>&nbsp;</td>";
                           echo "<td>[".$row_c['C_ROW']."]</td>";
                           echo "<td class='number'>".n($row_c['RTINFI'],2)."</td>";
                           echo "<td>&nbsp;</td>";
                           echo "<td>&nbsp;</td>";
                           echo "</tr>";
                           
                   }
                   
           }
       
       
   }
    
    echo "</table>";
    echo "<br>";
    echo "<br>";
    
}






echo "<table class=int1>";


echo "<tr class='liv_data'>
		<th rowspan=2>Anno/Mese/Fornitore</th>";

    if($var_attese == 'Y')
       echo "<th rowspan=2> Stato </th>"; 

    if($var_attese == 'Y')
       echo "<th colspan =4 class='center'> Note variazione</th>";
    else
       echo "<th colspan =4 class='center'> Ddt entrata</th>";
           
  echo "</tr><tr class='liv_data'>";
  
    if($var_attese == 'Y')
      echo "<th>Nr doc</th>";
    else
      echo "<th>Nr ddt</th>";
  
  echo "<th>Merce</th>
		<th>Imponibile</th>
		<th>Totale</th>

</tr>";


foreach ($ar as $kar => $r){
	


	
	
	if($count_row > 1){
		echo "<tr  class = 'e_row' style=\"page-break-after: always;\"><td colspan = 5>&nbsp;</td></tr> ";
		
		echo "<tr class='liv_data'>
		<th rowspan=2>Anno/Mese/Fornitore</th>";
		
		if($var_attese == 'Y')
            echo "<th rowspan=2>Stato</th>";
        
        if($var_attese == 'Y')
            echo "<th colspan =4 class='center'> Note variazione</th>";
        else
            echo "<th colspan =4 class='center'> Ddt entrata</th>";
                    
        echo "</tr><tr class='liv_data'>";
                    
        if($var_attese == 'Y')
            echo "<th>Nr doc</th>";
        else
            echo "<th>Nr ddt</th>";
                          
   echo "<th>Merce</th>
		<th>Imponibile</th>
		<th>Totale</th>
	    </tr>";
	
	}
	  echo "</tr> <tr class ='liv_totale'> <td  style='font-size:12px;'>".$r['anno']."_".$r['mese']." [#".$r['num_sottoliv']."]</td>";
     
	  if($var_attese == 'Y')
        echo"<td>&nbsp;</td>";     

	  echo "<td>&nbsp;</td>
	  <td class='number'>". n($r['ddt_merce'],2)."</td>
	  <td class='number'>".n($r['ddt_imp'],2)."</td>
	  <td class='number'>".n($r['ddt_totd'],2)."</td>
   		 
  		</tr>";

	
	//livello anno_mese
	//echo "<tr class='liv1'><td colspan = 5 ><b>Totale generale</b></td></tr>";
	
	$stmt_t = get_categoria($filtri_tot, 'Y');

	$result = db2_execute($stmt_t, array($r['anno'], $r['mese']));
	echo db2_stmt_errormsg();
	while($row_t = db2_fetch_assoc($stmt_t)){
	    
	    $row_ta = find_TA_sys('MUTA', trim($row_t['RDTBCO']));
	    if(trim($row_t['RDTBCO']) == ''){
	        $categoria = 'Non definito';
	    }else{
	        $categoria = "[".$row_t['RDTBCO']."] ".$row_ta[0]['text'];
	    }
	
	    echo "<tr><td>".$categoria."</td>";
        
        if($var_attese == 'Y')
            echo"<td>&nbsp;</td>";   
            
        echo"<td>".$row_t['C_ROW']."</td>
            <td class='number'>".n($row_t['RTINFI'],2)."</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            </tr>";
	}
	
	$count_row++;
	
	//livello fornitore
	foreach ($r['children'] as $kar1 => $r1){
	 
		echo "<tr class='liv1'><td>".$r1['descr']."</td>";
		
		if($var_attese == 'Y')
		echo "<td>".$r1['stato']."</td>";
		
		echo "<td>".$r1['num_ddt']."</td>";
		echo " <td class='grassetto'>". n($r1['ddt_merce'],2)."</td>
               <td class='number'>".n($r1['ddt_imp'],2)."</td>
   		       <td class='number'>".n($r1['ddt_totd'],2)."</td>
   		  
  		</tr>";
		
		$stmt_c = get_categoria($filtri_cat, 'Y');
		$result = db2_execute($stmt_c, array($r1['anno'], $r1['mese'], $r1['forn']));
		while($row_c = db2_fetch_assoc($stmt_c)){
		    
		    $row_ta = find_TA_sys('MUTA', trim($row_c['RDTBCO']));
		    if(trim($row_c['RDTBCO']) == ''){
		        $categoria = 'Non definito';
		    }else{
		        $categoria = "[".$row_c['RDTBCO']."] ".$row_ta[0]['text'];
		    }
		    
		    
		    echo "<tr><td>".$categoria."</td>";
		    if($var_attese == 'Y')
		        echo"<td>&nbsp;</td>";   
		    echo "<td>[".$row_c['C_ROW']."]</td>";
		    echo "<td class='number'>".n($row_c['RTINFI'],2)."</td>";
		    echo "<td>&nbsp;</td>";
		    echo "<td>&nbsp;</td>";
		    echo "</tr>";
		    
		}
		
		
	
		
		
	
	}
	
	
}











