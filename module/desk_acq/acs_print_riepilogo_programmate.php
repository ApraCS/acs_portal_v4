<?php


require_once "../../config.inc.php";

$main_module = new DeskAcq();

//recupero settimana spedizione (da testate)
//$week = strftime("%V", strtotime($m_data));
//$year = strftime("%G", strtotime($m_data));

$week = $_REQUEST['settimana'];
$year = $_REQUEST['anno'];

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);

?>


<html>
 <head>

  <style>
   body.acs_content h2{font-size: 14px; padding: 15px 2px 15px 2px;}
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table.int0{border-collapse:collapse; width: 100%;}
   table.int0 td, table.int0 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
   tr.ag_liv2 td{background-color: #DDDDDD;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}
   tr.ag_liv_porta td{background-color: black; color: white; font-weight: bold;}      
   tr.ag_liv_data th{font-weight: bold; font-size: 14px;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  


 </head>
 <body class='acs_content'>
 	

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content'>

<?php






//$campo_data = $_REQUEST['sceltadata'][0];
$campo_data = 'TDDTEP';

//filtro in base a tipologia ordini (multiSelect)
$tipologia_ordini = json_decode($_REQUEST['tipologia_ordini']);

 
$k_field = "{$campo_data} as DATA, TASITI, TDDT,  TDNBOC, TDCITI, SP.CSCVET, SP.CSHMPG, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR ";
$k_field_ord = "{$campo_data}, TAASPE, TASITI, TDCITI, SP.CSCVET, TDNBOC, SP.CSHMPG";
$k_field_grp = "{$campo_data}, TDDT, TAASPE, TASITI, TDCITI, SP.CSCVET, TDNBOC, SP.CSHMPG, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR ";

if ($dett_carico=="Y"){
	$k_field .= " ";
	$s_field .= " ";	
}


if ($_REQUEST['tipo'] == 'agenda'){
	$add_field = " , TDDCON, TDCCON, TDOINU, TDOADO, TDONDO, SP.CSHMPG, SP.CSTISP, TDCLOR, SP.CSPORT, SP.CSHMPG ";
	$add_field .= " , TDTOCO, TDTART, TDVOLU, TDBANC, TDPLOR, TDPNET, TDTIMP ";	
	$k_field 		.= $add_field;
	$k_field_ord 	.= $add_field;
	$k_field_grp 	.= $add_field;	
	$s_field 		.= $add_field;	
}
if ($_REQUEST['tipo'] == 'elenco'){
	$add_field = " , TDDCON, TDOADO, TDONDO";
	$k_field 		.= $add_field;
	$k_field_ord 	.= $add_field;
	$k_field_grp 	.= $add_field;
	$s_field 		.= $add_field;
}

 //$s_field = $k_field . ", CSCVET ";
 $s_field = $k_field;
 
 if ($_REQUEST['tipo'] == 'agenda' && $_REQUEST['ag_dettaglio_per_porta'] == 'Y')
 	$k_field_ord = " CSPORT, CSHMPG, " . $k_field_ord;

//costruzione sql
if ($_REQUEST['tipo'] == "colli") {
	$sql_FROM = "FROM {$cfg_mod_DeskAcq['file_testate']} 
				  LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest']}
					ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=digits(RDONDO)
				  INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} ON
								TDDT = CSDT AND {$campo_data} = CSDTRG AND CSCALE = '{$cfg_mod_DeskAcq['tipo_calendario']}'
					LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN 
									  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI													 
					";
	$sql_WHERE = " WHERE " . $main_module->get_where_std() . " AND TDSWSP='Y' 
					AND CSNRSE = {$week} AND CSAARG = {$year}  
					AND RDTPNO='COLLI' AND RDRIGA=0 ";
	if (isset($_REQUEST['gg'])){
		$filtra_giorni = implode(",", $_REQUEST['gg']);
		$sql_WHERE .= " AND CSGIOR IN ($filtra_giorni) ";
	}

	
	//TODO: DRY	
	if (isset($_REQUEST['area_spedizione']) AND $_REQUEST['area_spedizione'] != '')
		$sql_WHERE .= " AND TAASPE = '{$_REQUEST['area_spedizione']}' ";

	if (count($tipologia_ordini) > 0)
		$sql_WHERE .= " AND TDCLOR IN (" . sql_t_IN($tipologia_ordini) . ") ";	

	if (isset($_REQUEST["num_carico"])  && strlen($_REQUEST["num_carico"]) > 0 )
		$sql_WHERE .= " AND " . $main_module->add_where_num_carico($_REQUEST["num_carico"]);
	if (isset($_REQUEST["num_lotto"])  && strlen($_REQUEST["num_lotto"]) > 0 )
		$sql_WHERE .= " AND " . $main_module->add_where_num_lotto($_REQUEST["num_lotto"]);
	
	if ($_REQUEST['carico_assegnato'] == "Y")
		$sql_WHERE .= " AND TDNRCA > 0 ";
	if ($_REQUEST['carico_assegnato'] == "N")
		$sql_WHERE .= " AND TDNRCA = 0 ";
	if ($_REQUEST['lotto_assegnato'] == "Y")
		$sql_WHERE .= " AND TDNRLO > 0 ";
	if ($_REQUEST['anomalie_evasione'] == "Y")
		$sql_WHERE .= " AND TDOPUN = 'N' ";
	if ($_REQUEST['lotto_assegnato'] == "N")
		$sql_WHERE .= " AND TDNRLO = 0 ";	
	

	
	$sql = "SELECT {$campo_data} AS DATA, RDRIFE, RDART, RDDES1, TAASPE, TDCITI, SUM(RDQTA) AS COLLI, SUM(RDQTA2) AS COLLI_PROD, SUM(RDQTA3) AS COLLI_SPED "
			. $sql_FROM . $sql_WHERE
			. " GROUP BY {$campo_data}, RDRIFE, RDART, RDDES1, TAASPE, TDCITI"
			. " ORDER BY {$campo_data}, RDDES1 ";	

}
else {
        
    switch ($backend_ERP){
        
        case 'GL':
            $s_field .= ", COUNT(DISTINCT(MECAR0)) AS C_D_RDART";
            $join = "LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
		              ON TD.TDDT= '1 ' AND TD.TDOTID=METPD0 /*AND TD.TDOINU=MERID0 */ AND TD.TDOADO=MEAND0 AND TD.TDONDO=digits(MENUD0)
		              AND (/* SOLO NON EVASE RDSRIG = 0 AND*/ MECAR0 NOT LIKE '*%' AND MEQTA0>0 AND MEQTA0>MEQTS0 /*AND RDSTEV<>'S'*/)  ";
        break;
         default: //SV2
            $s_field .= ", COUNT(DISTINCT(RDART)) AS C_D_RDART";
            $join = "LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_righe_doc_gest']} RD
                     ON TD.TDDT=RDDT AND TD.TDOTID=RDTIDO AND TD.TDOINU=RDINUM AND TD.TDOADO=RDAADO AND TD.TDONDO=digits(RDNRDO)
                     AND (/* SOLO NON EVASE*/ RDSRIG = 0 AND RDART NOT LIKE '*%' AND RDQTA>0 AND RDQTA>RDQTE AND RDSTEV<>'S')  ";
    }
    
	$sql = "SELECT $s_field
				, TDTOCO AS S_COLLI, TDTART AS S_REF, TDVOLU AS S_VOLUME, TDBANC AS S_PALLET
				, TDPLOR AS S_PESOL, TDPNET AS S_PESON, TDTIMP AS S_IMPORTO
								   	
			/* 
				, SUM(TDTOCO) AS S_COLLI, SUM(TDTART) AS S_REF, SUM(TDVOLU) AS S_VOLUME, SUM(TDBANC) AS S_PALLET
				, SUM(TDPLOR) AS S_PESOL, SUM(TDPNET) AS S_PESON, SUM(TDTIMP) AS S_IMPORTO
			*/	    
				, COUNT(DISTINCT(CONCAT(TDIDES, CONCAT(TDDLOC, CONCAT(TDDCAP, TDNAZD))))) AS C_CLIENTI_DEST 
		FROM {$cfg_mod_DeskAcq['file_testate']} TD 		
		{$join}
		INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} CAL 
		   ON TDDT = CAL.CSDT AND {$campo_data} = CAL.CSDTRG AND CAL.CSCALE = '{$cfg_mod_DeskAcq['tipo_calendario']}'
		INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP 
		   ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
		LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN 
						  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI		   		   
		WHERE " . $main_module->get_where_std() . " AND TDSWSP='Y' AND CAL.CSNRSE = {$week} AND CAL.CSAARG = {$year} ";
		
		if (isset($_REQUEST['gg'])){
			$filtra_giorni = implode(",", $_REQUEST['gg']);
			$sql .= " AND CAL.CSGIOR IN ($filtra_giorni) ";
		}
		
		if ($_REQUEST['ag_solo_ordini'] == 'Y')
			$sql .= " and TDCLOR IN ('O', 'M', 'P') ";


		
	//TODO: DRY		
	if (isset($_REQUEST['area_spedizione']) && strlen($_REQUEST['area_spedizione'])>0)
		$sql .= " AND TAASPE = '{$_REQUEST['area_spedizione']}' ";
				 
	if (count($tipologia_ordini) > 0)
		$sql .= " AND TDCLOR IN (" . sql_t_IN($tipologia_ordini) . ") ";
	
	if (isset($_REQUEST["num_carico"])  && strlen($_REQUEST["num_carico"]) > 0 )
		$sql .= " AND TDNRCA ='{$_REQUEST["num_carico"]}'";
	if (isset($_REQUEST["num_lotto"])  && strlen($_REQUEST["num_lotto"]) > 0 )
		$sql .= " AND TDNRLO ='{$_REQUEST["num_lotto"]}'";
	
	if ($_REQUEST['carico_assegnato'] == "Y")
		$sql .= " AND TDNRCA > 0 ";
	if ($_REQUEST['carico_assegnato'] == "N")
		$sql .= " AND TDNRCA = 0 ";
	if ($_REQUEST['lotto_assegnato'] == "Y")
		$sql .= " AND TDNRLO > 0 ";
	if ($_REQUEST['anomalie_evasione'] == "Y")
		$sql .= " AND TDOPUN = 'N' ";
	if ($_REQUEST['lotto_assegnato'] == "N")
		$sql .= " AND TDNRLO = 0 ";	
	
	
	$sql .= "GROUP BY {$k_field_grp}, TDTOCO, TDTART, TDVOLU, TDBANC, TDPLOR, TDPNET, TDTIMP
			 ORDER BY $k_field_ord";

}	


$stmt = db2_prepare($conn, $sql);		
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

$ar = array();
$ar_tot = array();










/***********************************************************************************************
 *  AGENDA 
 ***********************************************************************************************/
if ($_REQUEST['tipo'] == "agenda") {
 echo "<h2>Agenda consegne programmate - Settimana $week / $year" . add_descr_report($_REQUEST['add_descr_report']) . "</h2>";
 
 while ($r = db2_fetch_assoc($stmt)) {
			
		if (!isset($ar[$r['DATA']])) 				
 			$ar[$r['DATA']] = array("cod" => $r['DATA'], "descr"=>$r['DATA'], 
																  "val" => array(), "children"=>array());

	 	$d_ar = &$ar[$r['DATA']]['children'];
		 
		 $tmp_ar = &$ar[$r['DATA']];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI']  	+= $r['S_COLLI'] ;		 
		 $tmp_ar["val"]['REF']  	+= $r['S_REF'] ;		 
		 $tmp_ar["val"]['C_D_RDART']+= $r['C_D_RDART'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 		 
		 if (in_array($r['TDCLOR'], array('O', 'M', 'P'))){
		 	$tmp_ar["val"]['N_ORDINI'] += 1 ;		 
		 }
	 	
	 	
		
		$l_itinerario = $r['TDCITI'];
		$d_itinerario = $main_module->decod_std('ITIN', $l_itinerario);
		$cstisp = "";		

		if ($_REQUEST['ag_dettaglio_per_tip_spedizione'] == "Y"){
			$l_itinerario .= $r['CSTISP'];
			//$d_itinerario .= " " . $r['CSTISP'];
			$cstisp = "<br>" . $r['CSTISP']; 			
		}			
		
		
		//-----------------------------------------------------------------
		// STACCO PER PORTA
		//-----------------------------------------------------------------
		$tmp_ar = &$ar[$r['DATA']]['children'];		
		if ($_REQUEST['ag_dettaglio_per_porta'] == 'Y')
			$liv_porta = $r['CSPORT'];
		else
			$liv_porta = 'PORTA';
		
		if (!isset($tmp_ar[$liv_porta]))
			$d_ar[$liv_porta] = array("cod" => $liv_porta,
					"descr" 	=> $liv_porta,
					"val" => array(), "children"=>array());
		$tmp_ar = &$d_ar[$liv_porta]['children'];					
		
		
				
		
		//-----------------------------------------------------------------
		// STACCO PER ITINERARIO (GRUPPO)
		//-----------------------------------------------------------------
		if ($_REQUEST['ag_dettaglio_per_itinerario'] != "Y"){
			$l_itinerario = 'ITIN';
			$d_itinerario = 'ITIN';
		}				
		if (!isset($tmp_ar[$l_itinerario]))	
				$tmp_ar[$l_itinerario] = array("cod" => $l_itinerario, 
																 "descr" 	=> $d_itinerario,
																 "cstisp" 	=> $cstisp,
																  "val" => array(), "children"=>array()); 														  
																  																  
		 $tmp_ar = &$tmp_ar[$l_itinerario];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;		 
		 $tmp_ar["val"]['REF'] 		+= $r['S_REF'] ;
		 $tmp_ar["val"]['C_D_RDART']+= $r['C_D_RDART'] ;		 		 		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;

		 if (in_array($r['TDCLOR'], array('O', 'M', 'P')))		 
		 	$tmp_ar["val"]['N_ORDINI'] += 1 ;
		 					
	 
		 
	 	// ------------- liv2 (vettore / mezzo) -------------------------
		$liv2_cod = 0; //non stacco piu' per vettore/mezzo	 	

		
		//-----------------------------------------------------------------
		// liv3: fornitore
		//-----------------------------------------------------------------

		$liv3_cod = $r['TDCCON'];
		$liv3_des = $r['TDDCON'];
		if ($_REQUEST['ag_dettaglio_per_orario_spedizione'] == "Y"){
			$liv3_cod .= $r['CSHMPG'];
			if ($r['CSHMPG'] > 0)   $liv3_des .= " (" . print_ora($r['CSHMPG']) . ")";
		}		
		
		if (!isset($tmp_ar['children'][$liv2_cod]['children'][$liv3_cod]))	
				$tmp_ar['children'][$liv2_cod]['children'][$liv3_cod] = array("cod" => $liv3_cod, "descr"=>$liv3_des, 
																  "val" => array(), );

		$tmp_ar = &$tmp_ar['children'][$liv2_cod]['children'][$liv3_cod];
																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;			 
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;		 
		 $tmp_ar["val"]['REF'] 		+= $r['S_REF'];
		 $tmp_ar["val"]['C_D_RDART']+= $r['C_D_RDART'] ;		 		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 
		 ////if (in_array($r['TDCLOR'], array('O', 'M', 'P')))		 
		 	$tmp_ar["val"]['N_ORDINI'] += 1 ;		 
		 
	
 } //while



 $campo_totalizzato = strtoupper($_REQUEST['ag_f_tot']);
 switch (strtoupper($_REQUEST['ag_f_tot'])){
	case "VOLUME":
		$n_dec = 3; break;
	default:
		$n_dec = 0;	
 }
 
 

 $cl_liv_cont = 0;
 if ($_REQUEST['ag_dettaglio_per_cliente'] == "Y") $liv3_row_cl = ++$cl_liv_cont;
 if ($_REQUEST['ag_dettaglio_per_vettore'] == "Y") $liv2_row_cl = ++$cl_liv_cont; 
 if ($_REQUEST['ag_dettaglio_per_itinerario'] == "Y") $liv1_row_cl = ++$cl_liv_cont; 
 
 echo "<table class=int0><tr>";
 foreach ($ar as $kgg => $gg){
	echo "<td valign=top>";  
	 echo "<table class=int0>";
	  echo "<tr class=ag_liv_data><th colspan=2>" . ucfirst(print_date($kgg, "%A %d/%m")) . "</th></tr>";
	  
	  
	  foreach ($gg['children'] as $klp => $lp){
	  	if ($_REQUEST['ag_dettaglio_per_porta'] == 'Y'){
	  		echo "<tr class=ag_liv_porta><td>Porta " . $lp['descr'] . "</td><td class=number>&nbsp;</td></tr>";	  		
	  	}
	  	
	  foreach ($lp['children'] as $kl1 => $l1){
 		if ($_REQUEST['ag_dettaglio_per_itinerario'] == "Y")	  	
	  	  echo "<tr class=ag_liv{$liv1_row_cl}><td>" . $l1['descr'] . "</td><td class=number>" . n_ag($l1['val'], $campo_totalizzato, $n_dec) . $l1['cstisp'] . "</td></tr>";	  	
		
	  		foreach ($l1['children'] as $kl2 => $l2){
	  		  if ($_REQUEST['ag_dettaglio_per_vettore'] == "Y" || $_REQUEST['ag_dettaglio_per_mezzo'] == "Y")
	  		  	
	  		  	if ($_REQUEST['ag_dett_carichi'] == 'Y'){
	  		  		//stampo l'elenco dei carichi
	  		  		$ar_carichi = array();
	  		  		foreach ($l2['carichi'] as $k_carico => $carico)
	  		  			$ar_carichi[] = $k_carico;
	  		  		
	  		  		$l2['descr'] .= "<br><div style='float: right; font-size: 9px; font-weight: bold;'>[" . implode(", ", $ar_carichi) . "]</div>";	  		  		
	  		  	}
	  		  	
	  			//echo "<tr class=ag_liv{$liv2_row_cl}><td>" . $l2['descr'] . "</td><td class=number>" . n($l2['val'][$campo_totalizzato], $n_dec) . "</td></tr>";

	  		  		//se visualizzo la porta devo lasciare i fornitori ordinati per ora
	  		  		if ($_REQUEST['ag_dettaglio_per_porta'] != 'Y')	  		  	
	  		  			usort($l2['children'], "cmp");
	  		  
			  		foreach ($l2['children'] as $kl3 => $l3){
			  		  if ($_REQUEST['ag_dettaglio_per_cliente'] == "Y")
			  			echo "<tr class=ag_liv{$liv3_row_cl}><td>" . $l3['descr'] . "</td><td class=number>" . n_ag($l3['val'], $campo_totalizzato, $n_dec) . "</td></tr>";	  			
					}				
					  			
			}		
			  	
	  } //liv1
	  } //liv porta
	  
	 echo "</table>";
  echo "</td>";
 }
 echo "</tr></table>";
} //agenda







/***********************************************************************************************
 *  ELENCO 
 ***********************************************************************************************/
 
if ($_REQUEST['tipo'] == "elenco") {

 echo "<h2>Riepilogo consegne programmate - Settimana $week / $year" . add_descr_report($_REQUEST['add_descr_report']) ."</h2>";

 $f_liv1 = "ITIN_FORN_SPED"; 	$d_liv1 = "D_ITIN_FORN_SPED";
 $f_liv2 = "ORDINE";			$d_liv2 = "ORDINE";
 $f_liv3 = "";					$d_liv3 = "";
 
 $ar_tot["TOTALI"] = array();
 
 while ($r = db2_fetch_assoc($stmt)) {
			
		$r['ITIN_FORN_SPED'] 	= implode("|",  array($r['TDCITI'], $r['TDCCON'], $r['TDNBOC'])); 	
		$r['D_ITIN_FORN_SPED'] 	= implode("|", 	array($main_module->decod_std('ITIN', $r['TDCITI']), $r['TDDCON'], $r['TDNBOC']));
		$r['ORDINE'] 			= implode("_",  array($r['TDOADO'], $r['TDONDO']));
		
	 
		 $tmp_ar = &$ar_tot["TOTALI"];																  
		 $tmp_ar['VOLUME'] 	+= $r['S_VOLUME'] ;					
		 $tmp_ar['COLLI'] 	+= $r['S_COLLI'] ;
		 $tmp_ar['REF'] 	+= $r['S_REF'] ;		 
		 $tmp_ar['PALLET'] 	+= $r['S_PALLET'] ;
		 $tmp_ar['PESOL'] 	+= $r['S_PESOL'] ;
		 $tmp_ar['IMPORTO'] += $r['S_IMPORTO'] ;		 
		 $tmp_ar['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;	 
			
		if (!isset($ar[$r['DATA']])) 				
 			$ar[$r['DATA']] = array("cod" => $r['DATA'], "descr"=>$r['DATA'], 
																  "val" => array(), "children"=>array());

	 	$d_ar = &$ar[$r['DATA']]['children'];
		 
		 $tmp_ar = &$ar[$r['DATA']];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;
		 $tmp_ar["val"]['REF'] 		+= $r['S_REF'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] 	+= $r['S_PESOL'] ;
		 $tmp_ar["val"]['IMPORTO'] 	+= $r['S_IMPORTO'] ;
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;		 		 
	 	
		//liv1
		if (!isset($d_ar[$r[$f_liv1]]))	
				$d_ar[$r[$f_liv1]] = array("cod" => $r[$f_liv1], "descr"=>$r[$d_liv1], 
																  "val" => array(),
																  "itinerario" => $main_module->decod_std('ITIN', $r['TDCITI']), 
																  "fornitore" => $r['TDDCON'],
																  "orario" => $r['CSHMPG'],
																  "CSTISP" => $r['CSTISP'], "CSTITR" => $r['CSTITR'], 
																  "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$r[$f_liv1]];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;
		 $tmp_ar["val"]['REF'] 		+= $r['S_REF'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] 	+= $r['S_PESOL'] ;
		 $tmp_ar["val"]['IMPORTO'] 	+= $r['S_IMPORTO'] ;
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;		 		 
					
	 
	 	//liv2
		if (!isset($d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]]))	
				$d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]] = array("cod" => $r[$f_liv2], "descr"=>$r[$d_liv2],
																	"note_carico" => note_carico($r), 
																  	"val"  => array(), "children"=>array());
		 $tmp_ar = &$d_ar[$r[$f_liv1]]['children'][$r[$f_liv2]];																  
		 $tmp_ar["val"]['VOLUME'] 	+= $r['S_VOLUME'] ;					
		 $tmp_ar["val"]['COLLI'] 	+= $r['S_COLLI'] ;
		 $tmp_ar["val"]['REF'] 		+= $r['S_REF'] ;		 
		 $tmp_ar["val"]['PALLET'] 	+= $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] 	+= $r['S_PESOL'] ;
		 $tmp_ar["val"]['IMPORTO'] 	+= $r['S_IMPORTO'] ;		 
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;		 		 
		 
		 
	
 } //while






	echo "<table class=int0>";
	
	echo "
			<tr>
			 <th>Itinerario</th>
			 <th>Fornitore</th>
			 <th>Orario</th>
			 <th>TS</th>			 
			 <th>TT</th>			 
			 ";			 
			 
			if ($_REQUEST['dettaglio_per_ordine'] == "Y") echo "<th>Ordine</th>";
			
			echo "
		
					 <th>Pezzi</th>
					 <th>Volume</th>			 
					 <th>Peso</th>			 			
					 <th>Importo</th>			 
					</tr>  
				";
	
 


 $cl_liv_cont = 0;
 if ($_REQUEST['dettaglio_per_ordine'] == "Y") $liv2_row_cl = ++$cl_liv_cont;
 $liv1_row_cl = ++$cl_liv_cont; 


 if ($_REQUEST['dettaglio_per_ordine'] == "Y") $r_colspan = 6; else $r_colspan=5;

 foreach ($ar as $kgg => $gg){

	  echo "<tr class=ag_liv_data><th colspan={$r_colspan}>" . ucfirst(print_date($kgg, "%A %d/%m")) . "</th>
	  	  			<th class=number>" . $gg['val']['REF'] . "</th>
	  	  			<th class=number>" . n($gg['val']['VOLUME'],3) . "</th>	  	  			
	  	  			<th class=number>" . n($gg['val']['PESOL'],1) . "</th>	  
	  	  			<th class=number>" . n($gg['val']['IMPORTO'], 0) . "</th>	  	  			
	  		</tr>";
	  
	  foreach ($gg['children'] as $kl1 => $l1){
	  	
		//ITINERARIO / VETTORE	  	
	  	  echo "<tr class=ag_liv{$liv1_row_cl}>
	  	  			<td>" . $l1['itinerario'] . "</td>
	  	  			<td>" . $l1['fornitore'] . "</td>
					<td colspan=1>" . print_ora($l1['orario']) . "</td>" . " 
					<td colspan=1>" . $l1['CSTISP'] . "</td>" . "
					<td colspan=1>" . $l1['CSTITR'] . "</td>" . "										
					";
					
		  if ($_REQUEST['dettaglio_per_ordine'] == "Y") echo "<td colspan=1>&nbsp;</td>";
		  					
		  echo "		
	  	  			<td class=number>" . $l1['val']['REF'] . "</td>
	  	  			<td class=number>" . n($l1['val']['VOLUME'],3) . "</td>	  	  			
	  	  			<td class=number>" . n($l1['val']['PESOL'],1) . "</td>	  	  				  	  				  	  				  	  			
	  	  			<td class=number>" . n($l1['val']['IMPORTO'], 0) . "</td>	  	  			
	  	  		</tr>";	  	

			// ORDINE		
	  		foreach ($l1['children'] as $kl2 => $l2){
	  		  if ($_REQUEST['dettaglio_per_ordine'] == "Y")
	  			echo "<tr class=ag_liv{$liv2_row_cl}>
	  					<td colspan=5 align=right></td>
	  					<td>" . $l2['descr'] . "</td>
		  	  			<td class=number>" . $l2['val']['REF'] . "</td>
		  	  			<td class=number>" . $l2['val']['VOLUME'] . "</td>		  	  			
		  	  			<td class=number>" . $l2['val']['PESOL'] . "</td>
		  	  			<td class=number>" . n($l2['val']['IMPORTO'], 0) . "</td>		  	  			
	  				</tr>";
					  			
			}		
			  	
	  }

 }

		
	//STAMPO TOTALE
					echo "
					<tr class=liv_totale>
					 <td colspan={$r_colspan}>TOTALE GENERALE</td>
					 <td class=number>" . $ar_tot['TOTALI']['COLLI'] . "</td>
					 <td class=number>" . n($ar_tot['TOTALI']['VOLUME'], 3) . "</td>					 
					 <td class=number>" . n($ar_tot['TOTALI']['PESOL'], 1) . "</td>			 			
					 <td class=number>" . n($ar_tot['TOTALI']['IMPORTO'], 0) . "</td>					 
					</tr>				
					";		
		
		
	echo "</table>";
 } //elenco
 
 
 
 
 function cmp($a, $b)
 {
 	return strcmp($a["descr"], $b["descr"]);
 }
 
 
 function note_carico($row){
	global $main_module;
	$carico = $main_module->get_carico_td($row);
	if (strlen(trim($carico['PSDESC']) . trim($carico['PSNOTE'])) > 0)
		return trim($carico['PSDESC']) . "  " . trim($carico['PSNOTE']);
	else
		return "";
 }
 
 function n_ag($l, $campo_totalizzato, $n_dec){
	if ($campo_totalizzato == 'PEZZI_REF')
		return n($l['REF'], 0) . "&nbsp;[" . n($l['C_D_RDART'], 0) . "]";
	else return n($l[$campo_totalizzato], $n_dec);
 }
 
 function add_descr_report($desc){
 	if (strlen($desc) > 0)
 		return ' - ' . $desc;
 	else return ''; 	
 }
 
 		
?>
  </div>
 </body>
</html>		