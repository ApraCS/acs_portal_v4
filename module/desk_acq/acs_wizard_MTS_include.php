<?php

function get_sql_where($form_values){
    global $cfg_mod_DeskAcq;
    
    $sql_where = '';
    $sql_where.= sql_where_by_combo_value('M2FOR1', $form_values->selected_id);
    $sql_where.= sql_where_by_combo_value('M2CGRU', $form_values->f_gruppo);
    $sql_where.= sql_where_by_combo_value('M2CSGR', $form_values->f_sottogruppo);
    $sql_where.= sql_where_by_combo_value('M2MAGA', $form_values->f_deposito);
    $sql_where.= sql_where_by_combo_value('M2SWTR', $form_values->f_tipo_riordino);
    


    if (strlen(trim($form_values->f_cod_art)) > 0)
        $sql_where .= " AND M2ART LIKE '%" . trim($form_values->f_cod_art) . "%'";
    if (strlen(trim($form_values->f_des_art)) > 0)
        $sql_where .= " AND M2DART LIKE '%" . trim($form_values->f_des_art) . "%'";    
    
/*    
    if (strlen(trim($form_values->f_cod_art)) > 0)
        $sql_where .= " AND M2PROG IN (SELECT DISTINCT M2PROG FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} T_A WHERE T_A.M2ART LIKE '%" . trim($form_values->f_cod_art) . "%')";
    if (strlen(trim($form_values->f_des_art)) > 0)
        $sql_where .= " AND M2PROG IN (SELECT DISTINCT M2PROG FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} T_DA WHERE T_DA.M2DART LIKE '%" . trim($form_values->f_des_art) . "%')";
*/        
    
    //if (strlen(trim($form_values->f_approvvigionatore)) > 0)
    //    $sql_where .= " AND M2PROG IN (SELECT DISTINCT M2PROG FROM {$cfg_mod_DeskAcq['file_wizard_MTS_2']} T_A WHERE T_A.M2APP0 LIKE '%" . trim($form_values->f_approvvigionatore) . "%')";
    $sql_where.= sql_where_by_combo_value('M2APP0', $form_values->f_approvvigionatore);
    
    //$r['ORDINATO'] 		= acs_u8e($r['M2QORD']);
   // $r['IMPEGNATO'] 	= $r['M2QFAB'];
    //$r['IMPEGNATO_DA_PROGRAMMARE'] 	= $r['M2QGIP'];
    if($form_values->f_imp_ord == 'S'){
        //tutti e tre zero
        $sql_where .= " AND M2QORD = 0 AND M2QFAB = 0 AND M2QGIP = 0"; 
    }elseif($form_values->f_imp_ord == 'E'){
       //uno dei tre deve diversi da zero
        $sql_where .= " AND (M2QORD > 0 OR M2QFAB > 0 OR M2QGIP > 0)"; 
    }else{
        //includi non faccio niente
        
    }
    

    return $sql_where;  
}
