<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();

//*************************************************************************
if ($_REQUEST['fn'] == 'get_json_rapporto_sincro_data'){
//*************************************************************************
	
	$m_params = acs_m_params_json_decode();
	$m_where_ar = array();

	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_righe_fatture']}  
			WHERE RFNRDO LIKE '{$m_params->open_request->chiave}%' ORDER BY RFDTGE DESC, RFORGE DESC";
	

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, $m_where_ar);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['data'] 	= $r['RFDTGE'];
		$r['ora'] 	= $r['RFORGE'];
		$r['segnalazioni'] 	= trim($r['RFNOTE']);
		$r['data_ora'] = print_date($r['data']) . " " . print_ora($r['ora']);
		$ar[] = $r;
	}
	
	echo acs_je($ar);
	exit();
}

	if ($_REQUEST['fn'] == 'open_tab'){
	
		$m_params = acs_m_params_json_decode();
	
		?>
		
	{"success":true, "items":
	 {
	  xtype: 'panel',
	  title: 'SynchroLog',
	  closable: true,
	  autoScroll: true,    
	  items: [		
		{
			xtype: 'grid',
			title: 'Rapporto sincronizzazione fatture anticipo',
			loadMask: true,
			
			features: [
			new Ext.create('Ext.grid.feature.Grouping',{
				groupHeaderTpl: 'Data/ora elaborazione: {name}',
				sortInfo:{field: 'data_ora', direction: 'DESC'},			
				hideGroupedHeader: false
			}),
			{
				ftype: 'filters',
				// encode and local configuration options defined previously for easier reuse
				encode: false, // json encode the filter query
				local: true,   // defaults to false (remote filtering)
		
				// Filters are most naturally placed in the column definition, but can also be added here.
			filters: [
			{
				type: 'boolean',
				dataIndex: 'visible'
			}
			]
			}],
		
		
		
			store: <?php echo extjs_grid_store(null, 'get_json_rapporto_sincro_data', 'data ora segnalazioni data_ora', 
					acs_je(array(
						"open_request" => acs_m_params_json_decode()
						)),					
					array(
							'groupField' 	=> 'data_ora',
							'groupOnSort'	=> false,
							'remoteGroup'	=> true,							
						),
					"sortInfo:{field: 'data_ora', direction: 'DESC'}"
					) ?>,
		
		
			columns: [
				{header: 'Segnalazioni',  dataIndex: 'segnalazioni', flex: 1, filterable: true}
			]
			 
		}
	]
	 } 
	
	}	
		
		
	<?php 	
	  exit();
	}
