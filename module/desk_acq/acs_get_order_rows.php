<?php

require_once "../../config.inc.php";

$main_module = new DeskAcq();
$m_params = acs_m_params_json_decode();
$s = new Spedizioni(array('no_verify' => 'Y'));

ini_set('max_execution_time', 6000);
ini_set('memory_limit', '2048M');

function cmp_by_inDataSelezionata($a, $b)
{
	return strcmp($a["inDataSelezionata"] . sprintf("%08s", $a["RDRIGA"]), $b["inDataSelezionata"] . sprintf("%08s", $b["RDRIGA"]));
}


	$raw_post_data = acs_raw_post_data();
	if (strlen($raw_post_data) > 0)
	{	
		$m_params = acs_m_params_json_decode();
		$n_ord = $m_params->k_ord;
		$dtep  = $m_params->dtep;
	}

	if (isset($_REQUEST['nord']))
		$n_ord = $_REQUEST['nord'];
	if (isset($_REQUEST['nord']))
		$dtep = $_REQUEST['dtep'];	

	$oe = $main_module->k_ordine_td_decode($n_ord);
	

	
	
	// ******************************************************************************************
	// EXE - aggiungi riga articolo DDT (acq)
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_aggiungi_articolo'){
	
		$m_params = acs_m_params_json_decode();
		$cod = $m_params->codice;
	
		$ret = array();
			
		//print_r(strtoupper($m_params->codice));
		if($cod != '*' && strtoupper($cod) != 'NOTE'){
	
			$sql="SELECT COUNT(*) AS C_ART
			FROM {$cfg_mod_DeskAcq['file_anag_art']} AR
			WHERE ARDT = '$id_ditta_default'
			AND ARFOR1 <> 0 AND ARART ='{$m_params->codice}'";
	
	
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			$row = db2_fetch_assoc($stmt);
	
			if($row['C_ART'] == 0){
				$ret['success'] = false;
				$ret['message_cod'] = 'art_not_found';
				echo acs_je($ret);
				exit;
			}
	
		}
		
		
		if(strlen($m_params->is_ddt)>0){
		    $messaggio = 'INS_RIGA_DDT';
		}else{
		    $messaggio = 'INS_RIGA_FAC';
		}
		
		$sh = new SpedHistory($main_module);
		 $sh->crea(
		 'pers',
		 array(
		     "messaggio"	=> $messaggio,
    		 "k_ordine"	=> $m_params->k_ordine,
    		 "vals" => array(
        		 "RIART" => $m_params->codice,
        		 "RIQTA" => sql_f($m_params->quant),
        		 "RIIMPO" => sql_f($m_params->prezzo)
		 
		 )
		 
		 )
		 );
	
	
	
		$ret['success'] = true;
		echo acs_je($ret);
		exit;
	}

	
	// ******************************************************************************************
	// EXE - cancella riga
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_cancella_riga'){
	
		$m_params = acs_m_params_json_decode();
		$k_ordine = $m_params->k_ordine;
		
		if(strlen($m_params->is_ddt)>0){
		    $messaggio = 'DEL_RIGA_DOC';
		}else{
		    $messaggio = 'DEL_RIGA_FAC';
		}
	
	
		
		foreach($m_params->rows as $v){
		
	    $sh = new SpedHistory($main_module);
		$sh->crea(
				'pers',
				array(
				        "messaggio"	=> $messaggio,
						"k_ordine"      => $k_ordine,
						"vals" => array(
						    "RINREC" 	=> $v->nrec
						),
					)
				);
		
		}
	
		$ret = array();
		$ret['success'] = true;
		echo acs_je($ret);
		exit;
	}
	
if ($_REQUEST['fn'] == 'exe_save_desc'){
	    
	    $m_params = acs_m_params_json_decode();
	    	    
	    $sh = new SpedHistory($main_module);
	    $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> 'MOD_DES_RIGA',
	            "k_ordine"      => $m_params->k_ordine,
	            "vals" => array(
	                "RINREC" => $m_params->rec->RDNREC,
	                "RIART"  => $m_params->rec->RDART,
	                "RIDART" => $m_params->new_desc
	            ),
	        )
	        );
	    
	    $ret['success'] = true;
	    echo acs_je($ret);
	    exit;
	}
	
// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){

	
			$stmt = $main_module->get_rows_ordine_by_num_dtep($n_ord, $dtep);
			$data = array();
			
			while ($row = db2_fetch_assoc($stmt)) {
				$row['data_selezionata'] = sprintf("%08s", $dtep);
				$row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
				$row['RDART'] = acs_u8e($row['RDART']);
				$row['RDDART'] = acs_u8e(trim($row['RDDART']));
				$row['RDLIST'] = acs_u8e($row['RDLIST']);
				$row['RDNREC'] = acs_u8e($row['RDNREC']);
				$row['RRN'] = acs_u8e($row['RRN']);
				$row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
				$row['rife1'] =  $row['RDTIDO'].$row['RDINUM'].$row['RDAADO'].sprintf("%06s", $row['RDNRDO']).sprintf("%06s", $row['RDNREC']);
				$row['note']  = $main_module->has_commento_riga($row['rife1'] , 'PNC');
				$row['riga_ordine']  = $row['RDANOR'].'_'.sprintf("%06s", $row['RDNROR']).'_'.sprintf("%06s", $row['RDRGOR']);
				$data[] = $row;
			}
			
			//metto prima le righe della data selezionata
				////usort($data, "cmp_by_inDataSelezionata");
			
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $main_module->get_ordine_by_k_docu($n_ord);

			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		
exit();				
}


?>

{"success": true, "items":
	{
		xtype: 'gridpanel',
	    multiSelect: true,	
	   stateful: true,
        stateId: 'vis_righe-fatture-ddt',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
        features: [	
				{
					ftype: 'filters',
					encode: false, 
					local: true,   
			   		 filters: [
			       {
			 		type: 'boolean',
					dataIndex: 'visible'
			     }
			      ]
			}],
			
	         plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		               listeners:{
			        	afteredit: function(cellEditor, context, eOpts){
				        	var value = context.value;
				        	var grid = context.grid;
				        	var record = context.record;
				        	
				        		        	
			        	   	Ext.Ajax.request({
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_desc',
					            method: 'POST',
			        		    jsonData: {
					      			   rec : record.data,
					      			   new_desc: value,
									   k_ordine: <?php echo j($m_params->k_ord); ?>, 
									 				      			  
					      			   }, 					            
					            success: function ( result, request) {
					                var jsonData = Ext.decode(result.responseText);
					                record.set('RDDART', value);
					   						                															
					            },
					            failure: function (result, request) {
					            }
					        });
			        	}
					 }
		          })
		      ],
		      
  	    store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,				        
  					proxy: {
							url: '../desk_acq/acs_get_order_rows.php?fn=get_json_data&nord=<?php echo $n_ord; ?>&dtep=<?php echo $dtep; ?>',
							timeout: 2400000,
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDART', 'RDDART', 'RDUM', 'RDQTA', 'RDQTE', 
	            			 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata', 'inDataSelezionata', 
	            			  'RDSTEV', 'RDSTAT', 'residuo', 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 
	            			  'RDTPLO', 'RDNRLO', 'RDNREC', 'RDUM','RDQTDX', 'ARSOSP', 'ARESAU', 'ARFOR1', 'RDANOR', 
	            			  'RDNROR', 'RDDT', 'RDSC1', 'RDSC2', 'RDSC3', 'RDSC4', 'RTPRZ', 'RTINFI', 'RDMAGG',
	            			  'RDLIST', 'RDUMTE', 'RDQTAT', 'RDUMCO', 'RDQTCO', 'RDTBCO', 'RDIVRI', 'RRN', 'rife1', 'note',
	            			  'RDNROR', 'RDANOR', 'RDRGOR', 'riga_ordine'
	        			]
	    			}),
	    			
		        columns: [
		        
		        <?php 
           
                $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>";
               
                
                if($m_params->show_in_ddt == 'Y' || $m_params->show_in_fat == 'Y'){?>		
		        
		        	{
		                header   : 'Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer4(value);			    
										}		                		                
		             },{
		                header   : 'Um',
		                dataIndex: 'RDUM', 
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
								                		                
		             }, {
		                header   : 'Prezzo',
		                dataIndex: 'RTPRZ', 
		                width    : 70,
		                align: 'right',
		                renderer: floatRenderer5,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Importo',
		                dataIndex: 'RTINFI', 
		                width    : 60,
		                align: 'right',
		                renderer: floatRenderer2,
		                filter: {type: 'string'}, filterable: true
		             }, {
		                header   : 'Articolo',
		                dataIndex: 'RDART', 
		                width     : 80,
		                filter: {type: 'string'}, filterable: true
		                
		             },
		             
		              <?php if($m_params->show_in_ddt == 'Y'){ ?>
		               {
				    text: '<?php echo $note; ?>',
				    width: 30, 
				    tooltip: 'Note di riga',
				    tdCls: 'tdAction',  
				    dataIndex : 'note',       			
	    	        menuisabled: true, sortable: false,        		        
					renderer: function(value, p, record){
    						if (record.get('note') == 1) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
    			    		if (record.get('note') == 0) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
    			    
			    		
			    	}
			    	},
			    <?php } ?>		          	{
		                header   : 'Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 1,
		                filter: {type: 'string'}, filterable: true              			                
		             },{
		                header   : 'Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right',
		                filter: {type: 'string'}, filterable: true
		             }
		        
		          ,{
		                header   : 'Sconto1',
		                dataIndex: 'RDSC1', 
		                width    : 60,
		                align: 'right',
		                renderer: floatRenderer2,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Sconto2',
		                dataIndex: 'RDSC2', 
		                width    : 60,
		                align: 'right',
		                renderer: floatRenderer2,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Sconto3',
		                dataIndex: 'RDSC3', 
		                width    : 60,
		                align: 'right',
		                renderer: floatRenderer2,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Sconto4',
		                dataIndex: 'RDSC4', 
		                width    : 60,
		                align: 'right',
		                renderer: floatRenderer2,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Maggior.',
		                dataIndex: 'RDMAGG', 
		                width    : 65,
		                align: 'right',
		                renderer: floatRenderer2,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'List.',
		                tooltip: 'Listino',		                
		                dataIndex: 'RDLIST', 
		                width     : 40,
		                filter: {type: 'string'}, filterable: true      			                
		             }, {
		                header   : 'St.',
		                dataIndex: 'RDSTAT', 
		                tooltip: 'Stato di riga',	
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
		             }, {
		                header   : 'Tp.',
		                dataIndex: 'RDTPRI', 
		                tooltip: 'Tipo di riga',	
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Cat.',
		                tooltip: 'Categoria contabile',
		                dataIndex: 'RDTBCO', 
		                width    : 40,
		                filter: {type: 'string'}, filterable: true
		             }, {
		                header   : 'Ass.',
		                tooltip: 'Aliquota IVA',
		                dataIndex: 'RDIVRI', 
		                width    : 40,
		                filter: {type: 'string'}, filterable: true
		             },{
		                header   : 'Riga ordine',
		                tooltip: 'Riga ordine',
		                dataIndex: 'riga_ordine', 
		                flex    : 1,
		                filter: {type: 'string'}, filterable: true
		             },

		             
		        <?php }else{?>
		     
		         <?php if($m_params->nc != 'Y'){?>
					{
		                header   : 'Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
						  					return date_from_AS(value);			    
										}		                
		                 
		             },	
	             <?php }?>   
		             {
		                header   : 'Riga',
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right',
		                filter: {type: 'string'}, filterable: true
		             },			        
		             {
		                header   : 'Articolo',
		                dataIndex: 'RDART', 
		                width     : 80,
		                filter: {type: 'string'}, filterable: true
		             },
		             
		               <?php if($m_params->nc != 'Y'){?>
		             {
		                header   : 'numero',
		                dataIndex: 'RDNREC',
		                hidden: true,
		                filter: {type: 'string'}, filterable: true
		                }
	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=20>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=20>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
			    	   }},	    			             
		             
		             <?php }?>
		             {
		                header   : 'Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150,
		                filter: {type: 'string'}, filterable: true,
		                <?php if($m_params->nc == 'Y'){?>
		                editor: {
			                xtype: 'textfield',
			                allowBlank: true,
			                maxLength: 50
			            }             			    
			            <?php }?>            
		             }, {
		                header   : 'Um',
		                dataIndex: 'RDUM', 
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
		             }, {
		                header   : 'Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             },
		                
		               <?php if($m_params->nc != 'Y'){?> 
		             
		              {
		                header   : 'Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : 'Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }, {
		                header   : 'Prebolla',
		                dataIndex: 'RDQTDX', 
		                width    : 80,
		                align: 'right',
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : 'S',
		                dataIndex: 'RDSTEV', 
		                width    : 20,
		                align: 'right',
		                filter: {type: 'string'}, filterable: true
		             },
		             <?php }?>
		             
		            
		              {
		                header   : 'T.R.',
		                dataIndex: 'RDTPRI', 
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
		             },
		             
		           
		             <?php if($m_params->nc != 'Y'){?>  
		             {
		                header   : 'Pr.',
		                dataIndex: 'RDPRIO', 
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
		             },
		             <?php }?>
		              {
		                header   : 'St.',
		                dataIndex: 'RDSTAT', 
		                width    : 30,
		                filter: {type: 'string'}, filterable: true
		             },
		             
		             <?php if($m_params->nc != 'Y'){?>  
		              {
		                header   : 'Ordine',
		                dataIndex: 'RDNROR', 
		                width    : 80,
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNROR') == 0) return ''; //non mostro																											
						  					return rec.get('RDANOR') + "_" + rec.get('RDNROR');			    
										}		                
		             }, {
		                header   : 'Carico',
		                dataIndex: 'RDNRCA', 
		                width    : 80,
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRCA') == 0) return ''; //non mostro																											
						  					return rec.get('RDTICA') + "_" + rec.get('RDNRCA');			    
										}		                
		             }, {
		                header   : 'Lotto',
		                dataIndex: 'RDNRLO', 
		                width    : 80,
		                filter: {type: 'string'}, filterable: true,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRLO') == 0) return ''; //non mostro																											
						  					return rec.get('RDTPLO') + "_" + rec.get('RDNRLO');			    
										}		                
		             }
		             
		       <?php }else{?>
		            {
		                header   : 'Prezzo',
		                dataIndex: 'RTPRZ', 
		                width    : 70,
		                align: 'right',
		                renderer: floatRenderer3,
		                filter: {type: 'string'}, filterable: true
		             }
		             
		            <?php }}?>
		         ]	
		         <?php if(($m_params->show_in_ddt == 'Y' || $m_params->show_in_fat == 'Y' || $m_params->nc == 'Y') && $m_params->only_view != 'Y' ){?>					
			, dockedItems: [{
	                dock: 'bottom',
	                xtype: 'toolbar',
	                scale: 'large',
	              
	                items: [
	                
	                {
                     xtype: 'button',
               		 text: 'Chiudi',
		            iconCls: 'icon-sub_red_delete-32',
		            scale: 'large',	                     
		            handler: function() {
		            	var window = this.up('window');
			        	window.close();    
			       }

			     },  {
                     xtype: 'button',
               		 text: 'Chiudi e aggiorna',
		            iconCls: 'icon-button_black_repeat_dx-32',
		            scale: 'large',	                     
		            handler: function() {
		           		 var loc_win = this.up('window');
		           		 console.log(loc_win);
			             loc_win.fireEvent('beforeCl', loc_win);	
			            
			
			            }

			     }
			     
				//se e' possibile aggiungere articoli
				<?php if($m_params->show_in_ddt == 'Y' || $m_params->show_in_fat == 'Y' || $m_params->nc == 'Y'){ ?>					
				, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [	
						{
			                name   : 'f_codice',
			                id : 'Codice',
			                xtype : 'textfield',
			                flex: 1,
			                maxLength: 30,
			                fieldLabel: 'Codice',
			                labelAlign: 'right',
			                margin: '0 10 0 100'
			                
			                }, 
			                {
			                xtype: 'button',
			                <?php if($m_params->show_in_ddt == 'Y'){ ?>
			                text: 'Aggiungi articolo in DDT',
			                <?php }elseif($m_params->show_in_fat == 'Y'){?>	
			            	text: 'Aggiungi articolo in fattura',
			            	<?php }elseif($m_params->nc == 'Y'){?>
			            	text: 'Aggiungi articolo in nota credito attesa',
			            	<?php }?>
			                handler: function() {
			                
			                var grid = this.up('grid');
			                cod = Ext.getCmp('Codice').getValue();
			               
									Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiungi_articolo',
								        method     : 'POST',
					        			jsonData: {
					        			    codice : cod,
					        			    quant : 1,
											prezzo : 0,
											articolo : '',
					        				k_ordine: <?php echo j($m_params->k_ord) ?>,
					        				is_ddt: '<?php echo $m_params->show_in_ddt; ?>'
										},							        
								        success : function(result, request){
					            			
					            		   var jsonData = Ext.decode(result.responseText);
					            		   if(jsonData.success == true){
					            		     grid.getStore().load();
					            		     Ext.getCmp('Codice').setValue(''); 
					            		     return;
					            		     }
					            		     
					            		    if(jsonData.success == false && jsonData.message_cod =='art_not_found'){
					            		    
						            		    <?php if($m_params->show_in_ddt == 'Y'){?>
						            		          var messaggio = 'INS_RIGA_DDT';
						            		    <?php }else{?>
						            		          var messaggio = 'INS_RIGA_FAC';
						            		    <?php }?>
					            		    								            		   
						            		    my_listeners = {
							        					afterSelectArt: function(from_win, new_art){
							        					  Ext.Ajax.request({
                    									        url        : 'acs_elenco_articoli.php?fn=exe_conferma_articoli',
                    									        method     : 'POST',
                    						        			jsonData: {
                    						        				articoli: new_art,
                    						        				k_ordine: <?php echo j($m_params->k_ord) ?>,
                    						        				msg_ri: messaggio
                    											},							        
                    									        success : function(result, request){
                    						            			grid.getStore().load();	 
				 													from_win.close();
                    									        },
                    									        failure    : function(result, request){
                    									            Ext.Msg.alert('Message', 'No data to be loaded');
                    									        }
                    									    });
							        					    
				 											
											        	}
												}
	        									acs_show_win_std('Visualizza articoli', '../base/acs_elenco_articoli_adv.php', {
	        										k_ordine: <?php echo j($m_params->k_ord); ?>, 
	        										//codice : cod,
	        										//msg_ri: messaggio,
	        										esclusi_sosp: 'Y',
	        										abilita_inserimento_in_ordine: true,
	        									}, 900, 550, my_listeners, 'icon-shopping_setup-16');
					            		    }
					            		   	
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
					            
					            
					            }
			                
			                }
						
						]
					 }
<?php } ?>
							     
			     
			     
			     
			     
			     ] //items
		   }]    					
		<?php }?>
		, listeners: {		
	 			afterrender: function (comp) {
	 				 <?php if($m_params->show_in_ddt != 'Y' && $m_params->show_in_fat != 'Y' && $m_params->nc != 'Y'){?>
					comp.up('window').setTitle('<?php echo "Righe ordine {$oe['TDOADO']}_{$oe['TDONDO']} {$oe['TDPROG']} - consegna dal " . print_date($dtep); ?>');
						<?php }?>		
	 			},
	 			
				  celldblclick: {	
				  				
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  		
	        		    if(col_name == 'note'){
	        		
			        		  var my_listeners = {
			        			
			        					afterSave: function(from_win, jsonData){
			        						//aggiorno il record con il row ritornato		
 										    //rec.set(jsonData.row);
 										    grid.getStore().load();
 											from_win.close();
 											
							        	}
									}
									
		            		acs_show_win_std('Blocco note di riga/Descrizione non conformit&agrave;', '../desk_acq/acs_fatture_entrata.php?fn=blocco_note', {
							k_ordine: <?php echo j($m_params->k_ord) ?>,
							rife1 : rec.get('rife1'),					            		
		            		nrec : rec.get('RDNREC')
		            		}, 350, 350, my_listeners, 'icon-blog_compose-16');  
		            		
		            		}else{
		            		
		            		  <?php if($m_params->nc != 'Y'){?>
		            		    acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {dt: rec.get('RDDT'), rdart: rec.get('RDART')}, 1100, 600, null, 'icon-shopping_cart_gray-16');
		            		  <?php }?>
		            		}
					       
					    
					  }
					  
					
				  }
				  

				  	, itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
								var grid = this;
			             		var rows = grid.getSelectionModel().getSelection();
				  				
				  				var list_righe_selected = [];
                                   for (var i=0; i<rows.length; i++) {
				                    list_righe_selected.push({
				            	   	nrec: rows[i].get('RDNREC')
				            	});}
				  				
								var voci_menu = [];
								
      <?php if($m_params->show_in_ddt == 'Y' && $m_params->only_view == 'Y'){ ?>
    			voci_menu.push({
         		text: 'Riga non conforme (inserisci/togli)',
        		iconCls : 'icon-button_red_play-16',          		
        		     handler: function() {
        		
		        		  Ext.Ajax.request({
							url: '../desk_acq/acs_fatture_entrata.php?fn=exe_agg_prio',
							jsonData: {
					        	 k_ordine: '<?php echo $m_params->k_ord ?>', 
					        	 rrn: rec.get('RRN'),
					        	 prio : rec.get('RDPRIO')
					         },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	            	  													        
					             grid.getStore().load();	
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					            console.log('errorrrrr');
					        }
					    });	 
						  
	            	}
    		});      
      <?php } ?>								
								
      <?php if(($m_params->show_in_ddt == 'Y' || $m_params->show_in_fat == 'Y' || $m_params->nc == 'Y') && $m_params->only_view != 'Y'){?>								
				
            		<?php if($m_params->show_in_fat == 'Y'){?> 				
            				if(rec.get('RDNROR') == 0 && rec.get('RDANOR') == 0 ){
            				//fattura 0
            		<?php } else { ?>
            			if(1==1){
            		<?php }?>	
            		
            		if (rec.get('RDART').substr(0,1) != '*'){			
			           voci_menu.push({
				         		text: 'Modifica prezzo',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		  
				        		  if (list_righe_selected.length > 1){
            				  			acs_show_msg_error('Selezionare un solo articolo');
            				  			return false;			
            				  		}
				        		  
						        		  var my_listeners = {						        			
						        					afterModArt: function(from_win, jsonData){
						        						//aggiorno il record con il row ritornato
						        						rec.set(jsonData.row);
			 											from_win.close();
										        	}
												}
					            			acs_show_win_std('Modifica prezzo articolo', '../desk_acq/acs_fatture_entrata.php?fn=form_mod_art', {k_ordine: '<?php echo $m_params->k_ord ?>', articolo: rec.data, is_ddt: '<?php echo $m_params->show_in_ddt ?>', is_fat: '<?php echo $m_params->show_in_fat; ?>', nc : '<?php echo $m_params->nc?>'}, 400, 520, my_listeners, 'icon-pencil-16');          		
					            	}
				    		});
				    		
				    		voci_menu.push({
				         		text: 'Modifica quantit&agrave;',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		     
				        		     if (list_righe_selected.length > 1){
            				  			acs_show_msg_error('Selezionare un solo articolo');
            				  			return false;			
            				  		}
				        		  
						        		  var my_listeners = {
						        			
						        					afterModArt: function(from_win, jsonData){
						        						//aggiorno il record con il row ritornato		
			 										    rec.set(jsonData.row);
			 											from_win.close();
			 											
										        	}
												}
												
					            			acs_show_win_std('Modifica quantit&agrave; articolo', '../desk_acq/acs_fatture_entrata.php?fn=form_mod_art', {k_ordine: '<?php echo $m_params->k_ord ?>', articolo: rec.data, quant: 'Y', is_ddt: '<?php echo $m_params->show_in_ddt ?>', is_fat: '<?php echo $m_params->show_in_fat; ?>', nc : '<?php echo $m_params->nc?>'}, 400, 350, my_listeners, 'icon-pencil-16');          		
					            	}
				    		});
				    		
				    		}
				    		
				    		<?php if($m_params->nc != 'Y'){?>
				    		voci_menu.push({
				         		text: 'Modifica sconti',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		
						        		  var my_listeners = {
						        			
						        					afterModArt: function(from_win, jsonData){
						        						//aggiorno il record con il row ritornato		
			 										    //rec.set(jsonData.row);
			 										    grid.getStore().load();
			 											from_win.close();
			 											
										        	}
												}
												
					            			acs_show_win_std('Modifica sconti in '+ list_righe_selected.length +' articolo/i', '../desk_acq/acs_fatture_entrata.php?fn=form_mod_art', {k_ordine: '<?php echo $m_params->k_ord ?>', rows: list_righe_selected, sconti: 'Y', is_ddt: '<?php echo $m_params->show_in_ddt ?>', is_fat: '<?php echo $m_params->show_in_fat; ?>' }, 400, 250, my_listeners, 'icon-pencil-16');          		
					            	}
				    		});
				    		
				    		voci_menu.push({
				         		text: 'Modifica categoria',
				        		iconCls : 'icon-pencil-16',          		
				        		     handler: function() {
				        		
						        		  var my_listeners = {
						        			
						        					afterChange: function(from_win, jsonData){
						        						//aggiorno il record con il row ritornato		
			 										    //rec.set(jsonData.row);
			 										    grid.getStore().load();
			 											from_win.close();
			 											
										        	}
												}
												
					            		acs_show_win_std('Modifica categoria', '../desk_acq/acs_fatture_entrata.php?fn=elenco_categorie', {
										k_ordine: <?php echo j($m_params->k_ord) ?>,					            		
					            		from_righe : 'Y', 
					            		rows : list_righe_selected, 
					            		}, 350, 300, my_listeners, 'icon-pencil-16');  
					            	}
				    		});
				    		
				    		
				    		
				    			voci_menu.push({
				         		text: 'Riga non conforme (inserisci/togli)',
				        		iconCls : 'icon-button_red_play-16',          		
				        		     handler: function() {
				        		
						        		  Ext.Ajax.request({
											url: '../desk_acq/acs_fatture_entrata.php?fn=exe_agg_prio',
											jsonData: {
									        	 k_ordine: '<?php echo $m_params->k_ord ?>', 
									        	 rrn: rec.get('RRN'),
									        	 prio : rec.get('RDPRIO')
									         },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){	            	  													        
									             grid.getStore().load();	
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									            console.log('errorrrrr');
									        }
									    });	 
										  
					            	}
				    		});
				    		<?php }?>
				    		
				    		voci_menu.push({
				         		text: 'Cancella riga',
				        		iconCls : 'icon-sub_red_delete-16',          		
				        		     handler: function() {
											
											   Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'operazione?', function(btn, text){																							    
											   if (btn == 'yes'){					        		
							        		
													Ext.Ajax.request({
													   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cancella_riga',
													   method: 'POST',
													   jsonData: {
													   k_ordine: '<?php echo $m_params->k_ord ?>', 
													   rows: list_righe_selected,
													   is_ddt: '<?php echo $m_params->show_in_ddt; ?>',
													   }, 
													   
													   success: function(response, opts) {
													   	  Ext.getBody().unmask();
														  var jsonData = Ext.decode(response.responseText);
														  grid.getStore().load();									   	  
													   }, 
													   failure: function(response, opts) {
													      Ext.getBody().unmask();
													      alert('error on save_manual');
													   }
													});	
			
												  }
												});											
											
					            			          		
					            	} //handler
				    		});	
				    		
				    		voci_menu.push({
				         		text: 'Visualizza listino',
				        		iconCls : 'icon-currency_black_dollar-16',          		
				        		     handler: function() {
											
            				  	       acs_show_win_std('Listino di ACQUISTO [' +rec.get('RDART') + '] ' +rec.get('RDDART') , '../desk_utility/acs_anag_art_listini.php?fn=open_tab', {c_art: rec.get('RDART'), tipo_list: 'A', forn: rec.get('ARFOR1'), solo_visualizzazione : 'Y'}, 1300, 530, null, 'icon-currency_black_dollar-16');									
											
					            			          		
					            	} //handler
				    		});	
				    		
				}
				  
				    					  
				  	<?php }?>				  
				  
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							


					
					} //itemcontextmenu

			
	 			 			
			} //listeners
			
		, viewConfig: {
		        getRowClass: function(rec, index) {
		        	 <?php if($m_params->show_in_ddt != 'Y' && $m_params->show_in_fat != 'Y' && $m_params->nc != 'Y'){?>
		        
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (rec.get('RDPRIO') == 'NC') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';		        	
		       <?php } else {?>	
		            if (rec.get('RDPRIO') == 'NC') return 'rigaRevocata';
		            else return 'rigaNonSelezionata'; //COMMENTO
		       	    
		       <?php }?>
		       
		         }   
		    }												    
			
		         
	}
}


