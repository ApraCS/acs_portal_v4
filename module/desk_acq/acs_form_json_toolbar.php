<?php

require_once "../../config.inc.php";

$s = new DeskAcq();

?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			cls: 'acs_toolbar',
			tbar: [{
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Base',
			            items: [
			     
			     

// ***************************************************************************************
// Vettori
// ***************************************************************************************
			            {
			                text: 'Vettori<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-user_group-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqVettori(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_vettori") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_vettori") ?>
								<?php echo $cl->out_Writer_Model("deskacq_vettori") ?>
								<?php echo $cl->out_Writer_Store("deskacq_vettori") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_vettori") ?>
								<?php echo $cl->out_Writer_main("Lista vettori", "deskacq_vettori") ?>								
								<?php echo $cl->out_Writer_window("Tabella vettori") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Auto  / Mezzo
// ***************************************************************************************
			            {
			                text: 'Mezzi',
			                scale: 'large',			                 
			                iconCls: 'icon-delivery-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqAuto(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_auto") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_auto") ?>
								<?php echo $cl->out_Writer_Model("deskacq_auto") ?>
								<?php echo $cl->out_Writer_Store("deskacq_auto") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_auto") ?>
								<?php echo $cl->out_Writer_main("Lista mezzi", "deskacq_auto") ?>								
								<?php echo $cl->out_Writer_window("Tabella mezzi") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Aree di spedizione
// ***************************************************************************************
			            {
			                text: 'Aree<br>ricezione',
			                scale: 'large',			                 
			                iconCls: 'icon-globe-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqAreeSpedizione(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_areespedizione") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_areespedizione") ?>
								<?php echo $cl->out_Writer_Model("deskacq_areespedizione") ?>
								<?php echo $cl->out_Writer_Store("deskacq_areespedizione") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_areespedizione") ?>
								<?php echo $cl->out_Writer_main("Lista aree ricezione", "deskacq_areespedizione") ?>								
								<?php echo $cl->out_Writer_window("Tabella aree ricezione") ?>							

								} //handler function()
			            },			             
// ***************************************************************************************
// Itinerari
// ***************************************************************************************
			            {
			                text: 'Gruppo<br>fornitura',
			                scale: 'large',			                 
			                iconCls: 'icon-compass-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqItinerari(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_itinerari") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_itinerari") ?>
								<?php echo $cl->out_Writer_Model("deskacq_itinerari") ?>
								<?php echo $cl->out_Writer_Store("deskacq_itinerari") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_itinerari") ?>
								<?php echo $cl->out_Writer_main("Gruppo fornitura", "deskacq_itinerari") ?>								
								<?php echo $cl->out_Writer_window("Gruppo fornitura") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Trasportatori
// ***************************************************************************************
			            {
			                text: 'Trasportatori',
			                scale: 'large',			                 
			                iconCls: 'icon-address_book-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqTrasportatori(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_trasportatori") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_trasportatori") ?>
								<?php echo $cl->out_Writer_Model("deskacq_trasportatori") ?>
								<?php echo $cl->out_Writer_Store("deskacq_trasportatori") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_trasportatori") ?>
								<?php echo $cl->out_Writer_main("Lista trasportatori", "deskacq_trasportatori") ?>								
								<?php echo $cl->out_Writer_window("Tabella trasportatori") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Fornitori
// ***************************************************************************************
			            {
			                text: 'Fornitori',
			                scale: 'large',			                 
			                iconCls: 'icon-address_book-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqFornitori(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_fornitori") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_fornitori") ?>
								<?php echo $cl->out_Writer_Model("deskacq_fornitori") ?>
								<?php echo $cl->out_Writer_Store("deskacq_fornitori") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_fornitori") ?>
								<?php echo $cl->out_Writer_main("Lista fornitori", "deskacq_fornitori") ?>								
								<?php echo $cl->out_Writer_window("Tabella fornitori") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Risorse
// ***************************************************************************************
			            {
			                text: 'Risorse',
			                scale: 'large',			                 
			                iconCls: 'icon-user_group-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqRisorse(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_risorse") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_risorse") ?>
								<?php echo $cl->out_Writer_Model("deskacq_risorse") ?>
								<?php echo $cl->out_Writer_Store("deskacq_risorse") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_risorse") ?>
								<?php echo $cl->out_Writer_main("Lista risorse", "deskacq_risorse") ?>								
								<?php echo $cl->out_Writer_window("Tabella risorse") ?>							

								} //handler function()
			            }					            
			    	]
			   }, {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Spedizione',
			            items: [
			      	        
// ***************************************************************************************
// Itinerari/vettore
// ***************************************************************************************
			            {
			                text: 'Itinerari<br>vettori',
			                scale: 'large',			                 
			                iconCls: 'icon-delivery_globe-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqItVe(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_itve") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_itve") ?>
								<?php echo $cl->out_Writer_Model("deskacq_itve") ?>
								<?php echo $cl->out_Writer_Store("deskacq_itve") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_itve", 0.4) ?>
								<?php echo $cl->out_Writer_main("Lista Itinerari/Vettori", "deskacq_itve") ?>								
								<?php echo $cl->out_Writer_window("Tabella Itinerari/Vettori") ?>							

								} //handler function()
			            },			             
// ***************************************************************************************
// Tipologie spedizione
// ***************************************************************************************
			            {
			                text: 'Tipologie<br>spedizione',
			                scale: 'large',			                 
			                iconCls: 'icon-battery_half-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqTipologieSpedizione(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_tipologiespedizione") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_tipologiespedizione") ?>
								<?php echo $cl->out_Writer_Model("deskacq_tipologiespedizione") ?>
								<?php echo $cl->out_Writer_Store("deskacq_tipologiespedizione") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_tipologiespedizione") ?>
								<?php echo $cl->out_Writer_main("Lista tipologie spedizione", "deskacq_tipologiespedizione") ?>								
								<?php echo $cl->out_Writer_window("Tabella tipologie spedizione") ?>							

								} //handler function()
			            }, 
// ***************************************************************************************
// Tipologie trasporto
// ***************************************************************************************
			            {
			                text: 'Tipologie<br>trasporto',
			                scale: 'large',			                 
			                iconCls: 'icon-shopping_cart-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqTipologieTrasporto(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_tipologietrasporto") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_tipologietrasporto") ?>
								<?php echo $cl->out_Writer_Model("deskacq_tipologietrasporto") ?>
								<?php echo $cl->out_Writer_Store("deskacq_tipologietrasporto") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_tipologietrasporto") ?>
								<?php echo $cl->out_Writer_main("Lista tipologie trasporto", "deskacq_tipologietrasporto") ?>								
								<?php echo $cl->out_Writer_window("Tabella tipologie trasporto") ?>							

								} //handler function()
			            },
// ***************************************************************************************
// Tipologie ordine
// ***************************************************************************************
			            {
			                text: 'Tipologie<br>ordine',
			                scale: 'large',			                 
			                iconCls: 'icon-blog_compose-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqTipologieOrdine(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_tipologieordine") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_tipologieordine") ?>
								<?php echo $cl->out_Writer_Model("deskacq_tipologieordine") ?>
								<?php echo $cl->out_Writer_Store("deskacq_tipologieordine") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_tipologieordine") ?>
								<?php echo $cl->out_Writer_main("Lista tipologie ordine", "deskacq_tipologieordine") ?>								
								<?php echo $cl->out_Writer_window("Tabella tipologie ordine") ?>							

								} //handler function()
			            },
// ***************************************************************************************
// Autorizzazioni
// ***************************************************************************************
			            {
			                text: 'Autorizzazioni<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-unlock-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqAutorizzazioni(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_autorizzazioni") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_autorizzazioni") ?>
								<?php echo $cl->out_Writer_Model("deskacq_autorizzazioni") ?>
								<?php echo $cl->out_Writer_Store("deskacq_autorizzazioni") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_autorizzazioni") ?>
								<?php echo $cl->out_Writer_main("Lista autorizzazioni", "deskacq_autorizzazioni") ?>								
								<?php echo $cl->out_Writer_window("Tabella autorizzazioni") ?>							

								} //handler function()
			            } 
			    	]
			   }, {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Percorso',
			            items: [			            
			            
// ***************************************************************************************
// Start Point
// ***************************************************************************************
			            {
			                text: 'Stabilimenti<br>&nbsp;',
			                scale: 'large',			                 
			                iconCls: 'icon-home-32',
			                iconAlign: 'top',			                
			                 handler : function() {
								<?php $cl = new DeskAcqStabilimenti(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "deskacq_stabilimenti") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "deskacq_stabilimenti") ?>
								<?php echo $cl->out_Writer_Model("deskacq_stabilimenti") ?>
								<?php echo $cl->out_Writer_Store("deskacq_stabilimenti") ?>
								<?php echo $cl->out_Writer_sotto_main("deskacq_stabilimenti") ?>
								<?php echo $cl->out_Writer_main("Stabilimento", "deskacq_stabilimenti") ?>								
								<?php echo $cl->out_Writer_window("Tabella Stabilimenti") ?>							

								} //handler function()
			            }
			            

			            



			            
			            ]                        
			         }
			]
			
		, listeners: {
		
	 			afterrender: function (comp) {
	 				comp.up('window').setWidth(850);
	 				comp.up('window').setHeight(300);
	 			} 			
			}
			
		}
	]		
}            