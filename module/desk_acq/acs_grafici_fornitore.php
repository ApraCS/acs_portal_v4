<?php
require_once "../../config.inc.php";
		
$m_params = acs_m_params_json_decode();	

$sql_where = '';
$parametri = array();
		
if (isset($m_params->f_gruppo)){
	$sql_where .= " AND CRSEL1 = ?";
	$parametri[] = $m_params->f_gruppo;
}
if (isset($m_params->f_sottogruppo)){
	$sql_where .= " AND CRSEL2 = ?";
	$parametri[] = $m_params->f_sottogruppo;
}
if (isset($m_params->f_gruppo_pianificazione)){
	$sql_where .= " AND CRGRPA = ?";
	$parametri[] = $m_params->f_gruppo_pianificazione;
}
if (isset($m_params->f_tipo_approv)){
	$sql_where .= " AND CRSWTR = ?";
	$parametri[] = $m_params->f_tipo_approv;
}
if (strlen($m_params->f_des_art) > 0){
	$sql_where .= " AND CRDART = ?";
	$parametri[] = $m_params->f_des_art;
}
if (strlen($m_params->f_cod_art) > 0){
	$sql_where .= " AND CRART = ?";
	$parametri[] = $m_params->f_cod_art;
}
if ($m_params->selected_id != null)
	$sql_where .= " AND CRCCON IN (". sql_t_IN($m_params->selected_id) . ")";
		
		
$anno_partenza = date('Y')-10;
$s_field = " substr(trim(CRDTIV), 1, 4)";

$sql = "SELECT CRSWTR, COUNT(*) AS NUM_ART, $s_field AS ANNO
		FROM {$cfg_mod_DeskAcq['file_regole_riordino']}
		WHERE CRSWTR <> '' AND CRTP = 'A' AND $s_field > '$anno_partenza'
		$sql_where
		GROUP BY CRSWTR, $s_field
		ORDER BY $s_field
		";		
		
$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt, $parametri);

$ar_records = array();
$ar_tipo = array();
$ar_fields = array();
while ($r = db2_fetch_assoc($stmt)){	
	if (isset($ar_records[($r['ANNO'])]) === 'undefined'){
		$ar_records[($r['ANNO'])] = array();    		 		
	}
	$ar_records[($r['ANNO'])]['ANNO'] = $r['ANNO'];
	$ar_records[($r['ANNO'])]['TOTALE'] += $r['NUM_ART'];
	$ar_records[($r['ANNO'])][($r['CRSWTR'])] = $r['NUM_ART'];
	
	if (!(in_array($r['CRSWTR'], $ar_tipo))){
		array_push($ar_tipo, $r['CRSWTR']);
		array_push($ar_fields, $r['CRSWTR']);
	} 
}

array_push($ar_fields, "ANNO");
array_push($ar_fields, "TOTALE");



$ar_ret = array();
foreach($ar_records as $ar1){
	for($i=0; $i<count($ar_tipo); $i++){
		if (!($ar1[($ar_tipo[$i])] > 0))
			$ar1[($ar_tipo[$i])] = 0;
	}
	$ar_ret[] = $ar1;
}


$tipo_appr = acs_je($ar_ret);
$fields = acs_je($ar_fields);

?>


{"success":true, "items": [
		{
			xtype: 'container',
			title: '',
	 		layout: {
				type: 'hbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [

				{			
				xtype: 'panel',
				title: '',
				flex: 25,
				layout: 'fit',
				items: [{
					xtype: 'chart',
					id: 'chartCmp2',
					animate: true,
					shadow: true,
					flex: 1, 
					height: 300,
					legend: {
						position: 'bottom'
					},
					
					listeners: {					
			 			afterrender: function (comp) {
			 				Ext.getBody().unmask();
			 			}
			 		},						
					
										
					store: {
						fields: <?php echo ($fields) ?>,
						data: <?php echo($tipo_appr)?>
					},					
					axes: [{
						type: 'Numeric',
						position: 'left',
						fields: ['TOTALE'],
						title: 'Nr articoli codificati',
						grid: true
					}, {
						type: 'Category',
						position: 'bottom',
						fields: ['ANNO'],
						title: false
					}],
					series: [{
						type: 'column',
						axis: 'left',
						gutter: 80,
						xField: ['ANNO'],
						yField: ['TOTALE'],
						stacked: true,
							tips: {
								trackMouse: true,
								width: 65,
								height: 28,
								renderer: function(storeItem, item) {
									this.setTitle(String(item.value[1]));
								}
							}						
					}, 
					<?php 
					for($i=0; $i<count($ar_tipo); $i++){
						echo "
						{
							type: 'line',
							axis: 'left',
							smooth: true,								
							xField: ['ANNO'],
							yField: ['$ar_tipo[$i]'],
							tips: {
								trackMouse: true,
								width: 65,
								height: 28,
								renderer: function(storeItem, item) {
									this.setTitle(String(item.value[1]));
								}
							}
						}, 
						"; 
					}?>
					]
				}]
			}
			
		, {
			xtype: 'container',
			width: 400,
			title: '',
	 		layout: {
				type: 'vbox', border: false, pack: 'start', align: 'stretch',				
			},
			items: [
	  			// -----------------------------------------
	  			// grafico ciclo di vita
	  			// -----------------------------------------
					{
					 xtype: 'panel',
					 layout: 'fit',
					 flex: 1,
			         title: 'Ciclo di vita',					 
					 items: 	  				  		
						{
			            xtype: 'chart',
			            animate: true,
			            id: 'classi_giacenze_chart_ciclo_vita',

						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: 'acs_form_classi_giacenze_consumi.php?fn=get_json_data_chart_ciclo_di_vita',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									type: 'json',
									read: 'POST'
								},
					
								extraParams: {atp_setup_filter: Ext.encode(<?php echo acs_je($m_params); ?>)},
								
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'							
								},
								
							},
		
							fields: ['name', 'des_name', 'referenze']
										
						}, //store			            
			            
			            
			            shadow: true,
			            legend: {
			            	field: 'des_name',
			                position: 'right'
			            },
			            insetPadding: 10,
			            theme: 'Base:gradients',
			            series: [{
			                type: 'pie',
			                field: 'referenze',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 200,
			                  height: 28,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += rec.get('referenze');
			                    });
			                    this.setTitle(storeItem.get('des_name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'des_name',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('classi_giacenze_chart_ciclo_vita'); // id of the chart
									var index = cmp.store.findExact('des_name', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.name; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
			        }	  			
	  			}			
			
			
	  			// -----------------------------------------
	  			// grafico per tipo riordino
	  			// -----------------------------------------
				  ,	{
					 xtype: 'panel',
					 layout: 'fit',
					 flex: 1,
			         title: 'Tipo riordino (attivi)',					 
					 items:	  				  			
					  {
			            xtype: 'chart',
			            animate: true,
			            width: 200,
			            id: 'classi_giacenze_chart_tipo_riordino',			            

						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: 'acs_form_classi_giacenze_consumi.php?fn=get_json_data_chart_tipo_riordino&from_ws=N',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									type: 'json',
									read: 'POST'
								},
					
								extraParams: {atp_setup_filter: Ext.encode(<?php echo acs_je($m_params); ?>)},
								
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'							
								},
								
							},
		
							fields: ['name','referenze']
										
						}, //store			            
			            
			            
			            shadow: true,
			            legend: {
			                position: 'right'
			            },
			            insetPadding: 10,
			            theme: 'Base:gradients',
			            series: [{
			                type: 'pie',
			                field: 'referenze',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 200,
			                  height: 28,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += rec.get('referenze');
			                    });
			                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('referenze') / total * 100) + '% [' + floatRenderer0(storeItem.get('referenze')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'name',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial'
			                }
			            }]
			        }	 	  			
	  			}			
			
			
			]
		}				
			
			]
        }
	]
}