<?php

require_once "../../config.inc.php";
require_once("../desk_acq/acs_fatture_entrata_include.php");

$m_DeskAcq = new DeskAcq();
$main_module=new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();
// ******************************************************************************************
// SAVE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_upd'){
   
    $list_rows = $m_params->list_selected_id;
        
    foreach($list_rows as $v){
        
        $ar_upd = array();
        //$ar_upd['TFUSUM'] = $auth->get_user();
        $ar_upd['TFDTUM'] = oggi_AS_date();
        $ar_upd['TFFU03'] = '2';
        
        $sql = "UPDATE {$cfg_mod_DeskAcq['file_testate_fatture']} TF
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE RRN(TF) = '{$v->rrn}'";
        
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg($stmt);
        
        //Rileggo il TFDOCU e scrivo messaggio (per memorizzare gli xml visionati)
        $sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF                
                WHERE RRN(TF) = '{$v->rrn}'";        
        $stmtTF = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmtTF);
        echo db2_stmt_errormsg($stmtTF);
        $rowTF = db2_fetch_assoc($stmtTF);
        
        //SCRITTURA MESSAGGIO        
        $sh = new SpedHistory($m_DeskAcq);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'XML_VISIONATO',
                "vals" => array(
                    'RITIDO' => $rowTF['TFTIDO'],
                    'RIINUM' => $rowTF['TFINUM'],
                    'RIAADO' => $rowTF['TFAADO'],
                    'RINRDO' => $rowTF['TFNRDO']
                )));
        
    } //foreach
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
    
    
}


// ******************************************************************************************
//VERIFICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verify'){

    $sql  = "SELECT COUNT(*) AS C_ROW
             FROM {$cfg_mod_DeskAcq['file_testate_fatture']}
             WHERE TFDT = '{$id_ditta_default}' AND TFFU03 = '1'";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    
    echo json_encode(array('success' => true, 'c_row' => $row['C_ROW']));
  exit;
}


// ******************************************************************************************
//VERIFICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
    $sql  = "SELECT RRN(TF) AS RRN, TF.*, RF.C_DDT AS C_DDT, RF.RFDOCU AS DDT
             FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF
             LEFT OUTER JOIN (
                SELECT COUNT (*) AS C_DDT, RFDOCU
                FROM {$cfg_mod_DeskAcq['file_righe_fatture']}
                WHERE RFDT = '{$id_ditta_default}'
                GROUP BY RFDOCU
             ) RF
             ON TF.TFDOCU=RF.RFDOCU
             WHERE TFDT = '{$id_ditta_default}' AND TFFU03 = '1'";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        
        $nr['nr_int'] = $row['TFNRDO'];
        $nr['data_ge'] = $row['TFDTGE'];
        $nr['ora_ge'] = $row['TFORGE'];
        $nr['fornitore'] = '['.trim($row['TFCCON']).'] '.trim(acs_u8e($row['TFDCON']));
        $nr['num_forn']  =trim($row['TFDORF']);
        $nr['data_forn'] =trim($row['TFDTRF']);
        $nr['tipo'] = trim($row['TFTPDO']);
        $nr['imp_tot'] = trim($row['TFTOTD']);
        $nr['rrn'] = $row['RRN'];
        $nr['ddt'] = $row['C_DDT'];
        $nr['rfdocu'] = $row['DDT'];
        $ar[] = $nr;
        
    }
    
    
    echo acs_je($ar);
    exit();
 
}

// ******************************************************************************************
//VERIFICA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_ddt'){
    
    $sql  = "SELECT *
             FROM {$cfg_mod_DeskAcq['file_righe_fatture']} RF
             WHERE RFDT = '{$id_ditta_default}' AND RFDOCU = '{$m_params->rfdocu}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        
        $nr['nr_int'] = $row['RFNRDE'];
        $nr['num_forn']  =trim($row['RFDORF']);
        $nr['data_forn'] =trim($row['RFDTRF']);
        $nr['imp_tot'] = trim($row['RFTOTD']);
        $ar[] = $nr;
        
    }
    
    
    echo acs_je($ar);
    exit();
    
}

if ($_REQUEST['fn'] == 'open_tab'){?>
    
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            flex : 1,
	            autoScroll : true,
	            title: '',
	            items: [
	          		 
					 { 	xtype: 'grid',
				  		loadMask: true,	
				  		multiSelect: true,
				  		height : 200,
				  		autoScroll : true,
				  		selModel: {selType: 'checkboxmodel', checkOnly: true},
        		        store: {
							xtype: 'store',
							autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['rfdocu', 'ddt', 'data_ge', 'ora_ge', 'nr_int', 'tipo', 'imp_tot', 'fornitore', 'rrn', 'num_forn', 'data_forn']						
									
					},
		
				columns: [
					 {header: 'Data', dataIndex: 'data_ge', width: 60, sortable : true, renderer: date_from_AS},
					 {header: 'Ora', dataIndex: 'ora_ge', width: 40, sortable : true, renderer: time_from_AS},
					 {header : 'Fornitore',dataIndex: 'fornitore', flex: 1},
				     {header: 'Documento fornitore',
                	 columns: [
                	 {header: 'Numero', dataIndex: 'num_forn', width: 80, sortable : true},
                	 {header: 'Data', dataIndex: 'data_forn', width: 70, sortable : true, renderer: date_from_AS}
                      ]},
					{ header   : 'Tipo',
	                  dataIndex: 'tipo', 
		              width: 40,
		             },	{ header   : 'Imp. tot.',
	                  dataIndex: 'imp_tot', 
	                  align : 'right',
		              width: 70,
		              renderer: floatRenderer2
		             },{ header   : 'Nr. interno',
	                  dataIndex: 'nr_int', 
		               width: 70,
		             },
		    	     {header   : 'DDT',
	                  dataIndex: 'ddt', 
		              width: 35,
		              renderer: function(value, metaData, record){

								if (record.get('ddt') > 0)
									metaData.tdCls += ' sfondo_verde';	
								else		
									metaData.tdCls += ' sfondo_rosso';	
																	
								
								return value;	
							}
		             },
		    		
		    		
				], 
				  listeners: {
				  
		  		 	celldblclick: {								
					  	fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;					  	
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();
					  	
					  		if(1 == 1 || col_name == 'ddt' && rec.get('ddt') > 0)
					  			acs_show_win_std('Documenti fornitori da XML-Fatturazione elettronica', '../desk_acq/acs_fatture_entrata_check_new_entry.php?fn=open_ddt', {rfdocu : rec.get('rfdocu')}, 380, 250, {}, 'icon-search-16');
					
					    }}
	            
	        		},   
				
				buttons: [	
				{
			            text: 'Visionato',
				        iconCls: 'icon-button_blue_play-24',		            
				        scale: 'medium',		            
			            handler: function() {
			            	var win = this.up('window');
			            	var form = this.up('form').getForm();
			            	var grid = this.up('window').down('grid');
			            	id_selected = grid.getSelectionModel().getSelection();
    			         	list_selected_id = [];
    			     		for (var i=0; i<id_selected.length; i++){
    			     			list_selected_id.push(id_selected[i].data);
    						}		
    			            Ext.Ajax.request({
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upd',
						        jsonData: {
						        	list_selected_id : list_selected_id
						        	},
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        success : function(result, request){
						        	win.close();	    										        	
						        }
						   });
	            						                   	                	                
	            		}
			        }
		        
		        
		        ]
				    
				    		
                    }, //grid
				
	            ],
	       
	            
	           
	}
		
	]}
    
    <?php 
    
    exit;
}
if ($_REQUEST['fn'] == 'open_ddt'){?>
    
{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            flex : 1,
	            autoScroll : true,
	            title: '',
	            items: [
	          		 
					 { 	xtype: 'grid',
				  		loadMask: true,	
				  		multiSelect: true,
				  		height : 175,
				  		autoScroll : true,
        		        store: {
							xtype: 'store',
							autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_ddt', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							       extraParams: {
							         rfdocu : <?php echo j($m_params->rfdocu); ?>
							       },
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
							        
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['nr_int', 'imp_tot', 'num_forn', 'data_forn']						
									
					},
		
				columns: [
					
				     {header: 'Documento fornitore',
                	 columns: [
                	 {header: 'Numero', dataIndex: 'num_forn', width: 80, sortable : true},
                	 {header: 'Data', dataIndex: 'data_forn', width: 70, sortable : true, renderer: date_from_AS}
                      ]},
				     {header   : 'Importo totale',
	                  dataIndex: 'imp_tot', 
	                  align : 'right',
		              width: 100,
		              renderer: floatRenderer2
		             },{ header   : 'Nr. interno',
	                  dataIndex: 'nr_int', 
		               flex : 1,
		             }
		    	], 
				  listeners: {}
				    
				    		
                    }, //grid
				
	            ],
	       
	            
	           
	}
		
	]}
    
    <?php 
    
    exit;
}

  
  