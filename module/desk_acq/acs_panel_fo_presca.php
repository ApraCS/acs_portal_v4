<?php

require_once("../../config.inc.php");
$_module = "DESKACQ";

$main_module = new DeskAcq();

$all_params = array();
$all_params = array_merge((array)$_REQUEST, (array)acs_m_params_json_decode());


/*

//TODO: recuperare (o chidere come parametro) l'area di ricezione
//$def_area = 'ST';
$def_area = $cfg_mod_DeskAcq['def_area'];

//dall'area recupero lo stabilimento
$aspe = new DeskAcqAreeSpedizione();
$aspe->load_rec_data_by_k(array('TAKEY1' => $def_area));
$stab = new DeskAcqStabilimenti();

if (isset($aspe->rec_data['TARIF2']) && !is_null($aspe->rec_data['TARIF2']) && strlen(trim($aspe->rec_data['TARIF2'])) > 0){
	$stab->load_rec_data_by_k(array('TAKEY1' => $aspe->rec_data['TARIF2']));		
}
*/



function get_orari($tipo_risorsa, $codice, $porta, $data, $ar_filtri = array()){
	global $conn, $cfg_mod_DeskAcq, $id_ditta_default;
	$ret = array();
	
	//dalla data recuper il giorno della settimana
	$ggweek = date('w', strtotime($data));	
	
	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_orari_disponibili']} WHERE 1=1 ";
	$sql .= " AND ODTP= ? AND ODDT=? AND ODTRIS=? AND ODCRIS=?
			    AND (ODDTIN = {$data} OR (ODDTIN = 0 AND ODGGWK = {$ggweek})) ";
	$sql .= " /* AND  ODAREA IN ('*DEF', ?) */ AND ODPORT = ?
			  ORDER BY ODGGWK, ODHMIN
			";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array('O', $id_ditta_default,
										$tipo_risorsa, $codice,
										//$ar_filtri['area'], 
										$porta));
	
	//devo prendere solo quelli della data specifica, o generica per giorno della settimana
	$per_giorno_specifico = false;
	while ($r = db2_fetch_assoc($stmt)) {		
		if ((int)$r['ODGGWK'] == 0) $per_giorno_specifico = true;
		if ($per_giorno_specifico == true && (int)$r['ODGGWK'] > 0) break;
		$ret[] = $r;				
	}
		
  return $ret;	
}



function create_ev_orari_indisponibilita($ar_orari, $data){

	$ret = array();
	
			foreach($ar_orari as $r){
				if ($r['ODINDI'] == 'I'){ //indisponibilita
					$e = array();
					$e['cid'] 		= 2; //id calendario INDISPON
					$e['id'] 		= (int)$r['ODID']; //id calendario
					$e['title'] 	= 'Non disponibile';
//					$e['start'] 	= print_date($data, '%Y-%m-%d') . " " . print_ora($r['ODHMIN']) . ":00";
//					$e['end'] 		= print_date($data, '%Y-%m-%d') . " " . print_ora($r['ODHMFI']) . ":00";
					$e['start'] 	= print_date($data, '%Y-%m-%d') . " " . print_ora(0, 6, 'Y') . ":00";
					$e['end'] 		= print_date($data, '%Y-%m-%d') . " " . print_ora(235500) . ":00";
					
					$e['notes'] 	= "Have fun";
					$e['risorsa'] 	= '';
					$e['porta'] 	= '';
					
					$ret[] = $e;					
					
					//per ora, se ho una indisponibilita' considero tutto il giorno indisponibile
					return $ret;
					
				}
			}	
			
			
			$ora_iniziale = 0;
			$cont_indisp  = 0;
			foreach($ar_orari as $r){
				if ($r['ODINDI'] != 'I'){ //in base agli orari disponibili, genero quelli indisponibili
					$e = array();
					$e['cid'] 		= 2; //id calendario INDISPON
					$e['id'] 		= ++$cont_indisp;
					$e['title'] 	= 'Non disponibile';
					$e['start'] 	= print_date($data, '%Y-%m-%d') . " " . print_ora($ora_iniziale, 6, 'Y') . ":00";
					$e['end'] 		= print_date($data, '%Y-%m-%d') . " " . print_ora($r['ODHMIN']) . ":00";
					$e['notes'] 	= "Have fun";
					$e['risorsa'] 	= '';
					$e['porta'] 	= '';
						
					$ora_iniziale = $r['ODHMFI'];

					if ($r['ODHMIN'] > 0)
						$ret[] = $e;
				}				
			}			

			
			//indisponibilita fino alle 24
			if ($ora_iniziale < 240000 || ($ora_iniziale == 0 && $cont_indisp == 0) ){
				$next_day = date('Ymd', strtotime(print_date($data, '%Y-%m-%d') . " +1 days"));				
				$e = array();
				$e['cid'] 		= 2; //id calendario INDISPON
				$e['id'] 		= ++$cont_indisp;
				$e['title'] 	= 'Non disponibile';
				$e['start'] 	= print_date($data, '%Y-%m-%d') . " " . print_ora($ora_iniziale, 6, 'Y') . ":00";
				$e['end'] 		= print_date($data, '%Y-%m-%d') . " " . print_ora(235500, 6, 'Y') . ":00";
				$e['notes'] 	= "Have fun";
				$e['risorsa'] 	= '';
				$e['porta'] 	= '';
				$ret[] = $e;
			}
				
	
	return $ret;	
}






function verifica_validita_evento($evento){
	$ret = array();
	$ret['success'] = false;	
	
	$from_calendar_format = 'Y-m-d';
	$d_start 	= to_AS_date_from_format($from_calendar_format, $evento->StartDate);
	$d_end 		= to_AS_date_from_format($from_calendar_format, $evento->EndDate);

	$dt_start 	= strtotime($evento->StartDate);
	$dt_end 	= strtotime($evento->EndDate);

	//verifico sovrapposizione su PORTA
	$ar_ev = get_ar_eventi('AREA?????', $evento->Porta, $d_start, $d_end);
	foreach ($ar_ev as $ev){
		if ($ev['id'] != $evento->EventId){
			$ev_start 	= strtotime($ev['start']);
			$ev_end 	= strtotime($ev['end']);
			if ($dt_start < $ev_end && $dt_end > $ev_start){				
				$ret['success'] = false;
				$ret['error']	  = 'SOVRAPPOSIZIONE';
				$ret['message'] = "Sovrapposizione su porta {$evento->Porta}";
				return $ret;	
			}
		}		
	}
	
	
	//verifico orario disponibile PORTA
	global $cfg_mod_DeskAcq;
	$area = $cfg_mod_DeskAcq['def_area'];

	//dall'area recupero lo stabilimento
	$aspe = new DeskAcqAreeSpedizione();
	$aspe->load_rec_data_by_k(array("TAKEY1"=>$area));
	
	$stab = new DeskAcqStabilimenti();
	$stab->load_rec_data_by_k(array('TAKEY1' => $aspe->rec_data['TARIF2']));

	$orari = get_orari('STAB', $stab->rec_data['TAKEY1'], $evento->Porta, $d_start, array());
	
	foreach($orari as $o){
		$o['start'] = print_date($d_start, '%Y-%m-%d') . " " . print_ora($o['ODHMIN']) . ":00";
		$o['end'] 	= print_date($d_end, '%Y-%m-%d') . " " . print_ora($o['ODHMFI']) . ":00";
		
		if ($dt_start >= strtotime($o['start']) && $dt_end <= strtotime($o['end'])){
			$ret['success'] = true;
			$ret['ODID'][] = array(tipo_risorsa => 'AREA', codice => $area, odid=>$o['ODID']);		
		}
	}
	
	if ($ret['success'] == false){
		$ret['error']	  = 'NO_DISPO';
		$ret['message'] = "Orario non disponibile (Risorsa: PORTA - {$evento->Porta})";
		return $ret;		
	}
	
	
	//verifico ogni risorsa richiesta
	if (strlen(trim($evento->Risorsa)) > 0){		
	$ar_risorse = explode(',', $evento->Risorsa);
		foreach($ar_risorse as $codice_risorsa){
			
			//verifico non sovrapposizione RISORSA
			$ar_ev = get_ar_eventi_risorse($codice_risorsa, $d_start, $d_end);
			foreach ($ar_ev as $ev){
				if ($ev['id'] != $evento->EventId){
					$ev_start 	= strtotime($ev['start']);
					$ev_end 	= strtotime($ev['end']);
					if ($dt_start < $ev_end && $dt_end > $ev_start){
						$ret['success'] = false;
						$ret['error']	  = 'SOVRAPPOSIZIONE';
						$ret['message'] = "Sovrapposizione su risorsa {$codice_risorsa}";
						return $ret;
					}
				}
			}			
			
			
			//verifico se orario disponibile RISORSA
			$ret['success'] = false;
			$orari = get_orari('RISEN', $codice_risorsa, $evento->Porta, $d_start, array('area' => $area));
			
			foreach($orari as $o){
				$o['start'] = print_date($d_start, '%Y-%m-%d') . " " . print_ora($o['ODHMIN']) . ":00";
				$o['end'] 	= print_date($d_end, '%Y-%m-%d') . " " . print_ora($o['ODHMFI']) . ":00";
			
				if ($dt_start >= strtotime($o['start']) && $dt_end <= strtotime($o['end'])){
					$ret['success'] = true;
					$ret['ODID'][] = array(tipo_risorsa => 'RISEN', codice => $codice_risorsa, odid=>$o['ODID']);
				}
			}
			
			if ($ret['success'] == false){
				$ret['error']	  = 'NO_DISPO';
				$ret['message'] = "Orario non disponibile (Risorsa: RISEN - {$codice_risorsa})";
				return $ret;
			}
		}
	}
	
	
	
	return $ret;
}



function get_ar_eventi($stab, $porta, $d_start, $d_end){
	global $conn, $cfg_mod_DeskAcq;
	$ret = array();
	$sql = "SELECT CSPROG, CSDTSP, CSHMPG, CSHMFG, CSCCON, TDDCON
			FROM {$cfg_mod_DeskAcq['file_testate']} TD
			INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
				ON SP.CSPROG = TD.TDNBOC AND CSCALE = '*SPR'
			WHERE 1=1
			AND CSDTSP >= {$d_start} AND CSDTSP <= {$d_end}
			 /* AND TDSTAB = '{$stab}' TODO: come trovo lo stabilimento??? */ 
			AND CSPORT = '{$porta}' AND CSHMPG > 0
			GROUP BY CSPROG, CSDTSP, CSHMPG, CSHMFG, CSCCON, TDDCON
			";

		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);

		while ($r = db2_fetch_assoc($stmt)) {
			$e = array();
			$e['cid'] = 1; //id calendario
			$e['id'] = (int)$r['CSPROG']; //id calendario
			$e['title'] = acs_u8e($r['TDDCON']);
			$e['start'] = print_date($r['CSDTSP'], '%Y-%m-%d') . " " . print_ora($r['CSHMPG']) . ":00";
			$e['end'] 	= print_date($r['CSDTSP'], '%Y-%m-%d') . " " . print_ora($r['CSHMFG']) . ":00";
			$e['notes'] = "Have fun";
 			$e['risorsa'] = trim($r['CSCCON']);
 			$e['stabilimento'] = trim($stab);
 			$e['porta'] = $_REQUEST['f_porta'];

 			$ret[] = $e;
		}

	return $ret;
}

function get_ar_eventi_risorse($risorsa, $d_start, $d_end){
	global $conn, $cfg_mod_DeskAcq;
	$ret = array();
	$sql = "SELECT CSPROG, CSDTSP, CSHMPG, CSHMFG, CSCCON, TDDCON
			FROM {$cfg_mod_DeskAcq['file_testate']} TD
			INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
			ON SP.CSPROG = TD.TDNBOC AND CSCALE = '*SPR'
			WHERE 1=1
			AND CSDTSP >= {$d_start} AND CSDTSP <= {$d_end} AND CSCCON LIKE '%{$risorsa}%' AND CSHMPG > 0
			GROUP BY CSPROG, CSDTSP, CSHMPG, CSHMFG, CSCCON, TDDCON
			";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);

	while ($r = db2_fetch_assoc($stmt)) {
	$e = array();
		$e['cid'] = 1; //id calendario
		$e['id'] = (int)$r['CSPROG']; //id calendario
		$e['title'] = acs_u8e($r['TDDCON']);
		$e['start'] = print_date($r['CSDTSP'], '%Y-%m-%d') . " " . print_ora($r['CSHMPG']) . ":00";
		$e['end'] 	= print_date($r['CSDTSP'], '%Y-%m-%d') . " " . print_ora($r['CSHMFG']) . ":00";
		$e['notes'] = "Have fun";
		$e['risorsa'] = trim($r['CSCCON']);
		$e['porta'] = $_REQUEST['f_porta'];

		$ret[] = $e;
	}

	return $ret;
}





function to_AS_date_from_format($format, $data){
	switch ($format){
		case "m-d-Y":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[0], $d_ar[1], $d_ar[2]);
			return to_AS_date($d);
		case "Y-m-d":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[2], $d_ar[0]);
			return to_AS_date($d);
			
	}
}


function decodifica_ora($calendar_datatime){
	$data_ora = explode("T", $calendar_datatime);
	$ora_t = $data_ora[1] . '';
	$ora = substr($ora_t, 0, 2) . substr($ora_t, 3, 2) . substr($ora_t, 6, 2);	
	return $ora;
}



//costruzione array con prenotazioni attuali porte
function carica_prenotazioni($area, $data){
	global $conn, $cfg_mod_DeskAcq, $main_module;
	$s_field = explode(" ", "TDDT TDNBOC TDCCON TDDCON CSPORT CSHMPG CSHMFG");
	$sql = "SELECT " . implode(", ", $s_field) . " 
				FROM {$cfg_mod_DeskAcq['file_testate']} TD
				INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
					ON SP.CSPROG = TD.TDNBOC AND CSCALE = '*SPR' AND SP.CSDT = TD.TDDT
				LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
					ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
				WHERE " . $main_module->get_where_std() . "
					AND CSPORT <> '' AND CSHMPG > 0
 			  		AND TA_ITIN.TAASPE = " . sql_t($area) . "
	 			  	AND TDDTEP = {$data}
	 			  	GROUP BY " . implode(", ", $s_field) . "
	 			  	ORDER BY CSPORT, CSHMPG, CSHMFG
			";	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while ($row = db2_fetch_assoc($stmt)) {
		$ret['CSPORT'][] = $row;
	}
	
	return $ret;
}


function trova_orario_disponibile($prenotazioni = array(), $ora_inizio, $durata_in_minuti = 30){
	if (!isset($prenotazioni)) $prenotazioni = array();
	
	$ora_inizio_t = date('His', strtotime(print_ora($ora_inizio)));
	$ora_finale_t = date('His', strtotime(print_ora($ora_inizio)) + $durata_in_minuti*60);
	
	//scorro tutte le prenotazioni per vedere se trovo un posto
	while ($ora_inizio_t < date('His', strtotime('23:30:00'))){
		$ammesso = true;
		foreach($prenotazioni as $p){
			if ($ora_inizio_t < $p['CSHMFG'] && $ora_finale_t > $p['CSHMPG']){
				//impossibile... provo ad avanzare di mezz'ora
				$ora_inizio_t = date('His', strtotime(print_ora($ora_inizio_t)) + 30*60);
				$ora_finale_t = date('His', strtotime(print_ora($ora_inizio_t)) + $durata_in_minuti*60);				
				$ammesso = false;
			}
		}
		if ($ammesso == true){
			return array('ora_inizio_t' => $ora_inizio_t, 'ora_finale_t' => $ora_finale_t);
		}	
	}
	
	return false; //non ho trovato un orario disponibile
}
?>
<?php
// ******************************************************************************************
// ASSEGNA COME STANDARD SU FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_ordini_per_fornitore_stabilimento'){
	global $cfg_mod_DeskAcq, $id_ditta_default;
	$ret = array();
	$sql = "SELECT *
			FROM {$cfg_mod_DeskAcq['file_testate']} TD
			INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
				ON SP.CSPROG = TD.TDNBOC AND CSCALE = '*SPR' AND SP.CSDT = TD.TDDT
			WHERE " . $main_module->get_where_std() . " AND TDCCON=?
		   	  AND TDFN11=0 /* non evasi */	
	";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($all_params['f_fornitore']));	
	while ($r = db2_fetch_assoc($stmt)){
		$r['ORDINE_OUT'] = implode("_",  array($r['TDOADO'], $r['TDONDO']));
		$ret[] = $r; 
	}	
	
	echo acs_je($ret);
exit; }
?>
<?php
// ******************************************************************************************
// FORM APERTURA per CREAZIONE NUOVA RICEZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'form_nuova_ricezione'){
	?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [
					
			         <?php if ($js_parameters->p_solo_inter != 'Y'){?>										
					  {
			            text: 'Conferma',
			            iconCls: 'icon-sub_blue_accept-32',
			            scale: 'large',
			            handler: function() {		
			            	//TODO
			            	acs_show_msg_error('Funzione in fase di implementazione');	            	
			            }
			         }
			         <?php } ?>
		            
		            ],   		            
		            
		            items: [{
						   xtype: 'hiddenfield'
						   , name: 'f_data'
						   , value: <?php echo $all_params['f_data']; ?>
						   
						}, {
						   xtype: 'hiddenfield'
						   , name: 'f_stabilimento'
						   , value: <?php echo j($all_params['f_stabilimento']); ?>
						   
						}
						
						, {
							name: 'f_fornitore',
							xtype: 'combo',
							fieldLabel: 'Fornitore',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,		
						    anchor: '-15',
						    //margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_TA_std('FORN'), ""); ?>		
								    ] 
								}
							, listeners: {
							    select: function(combo, record, index) {
							      this.up('form').down('grid').store.proxy.extraParams = this.up('form').getValues();
							      this.up('form').down('grid').store.reload();
							    }
							  }														 
							}
						, {
							name: 'f_auto',
							xtype: 'combo',
							fieldLabel: 'Auto',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,		
						    anchor: '-15',
						    //margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_TA_std('AUTO'), ""); ?>		
								    ] 
								}						 
							}															
							
							
					   , {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
		 			        layout: 'hbox',
		 			        items: [						  
						  
						  
								  , {
									name: 'f_pallet',
									xtype: 'numberfield',
									fieldLabel: 'Pallet',
								    anchor: '-15'							
								  }, {
									name: 'f_volume',
									xtype: 'numberfield',
									fieldLabel: 'Volume',
									labelAlign: 'right',
								    anchor: '-15'							
								  }
							]
						}						
							
							
					, {
						xtype: 'grid',
						loadMask: true,
						flex: 1,
						
						layout: {
                			type: 'vbox',
                			align: 'stretch'
     					},
									
						selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},				
						//columnLines: true,			
						store: {
							xtype: 'store',
							autoLoad: false,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_ordini_per_fornitore_stabilimento',
								method: 'POST',
								type: 'ajax',
								extraParams: {},								
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['TDDTEP', 'TDONDO', 'ORDINE_OUT']
										
						}, //store
						multiSelect: true,
						columns: [
							{header: 'Data', dataIndex: 'TDDTEP', flex: 1, renderer: date_from_AS}
						  ,	{header: 'Ordine', dataIndex: 'ORDINE_OUT', width: 90}
						]
						 
					}							
							
																	  
						  
						  ]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		

<?php exit; }?>
<?php 
// ******************************************************************************************
// EDIT EVENT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'edit_event'){
	?>
	
{"success":true, "items": [

	{
		xtype: 'form',
		layout: {
                type: 'vbox',
                align: 'stretch'
            },
		flex: 1, frame: true,
		padding:    '10 10 10 10',
		margins:    '10 10 10 10',
		items: [{
		            xtype: 'textfield',
		            disabled: true,
		            itemId: this.id + '-title',
		            name: Extensible.calendar.data.EventMappings.Title.name,
		            fieldLabel:'Prenotazione',
		            anchor: '100%',
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        },{
		            xtype: 'extensible.daterangefield',
		            itemId: this.id + '-dates',
		            name: 'dates',
		            anchor: '95%',
		            singleLine: true,
		            fieldLabel: 'Orario',
		            showAllDay: false,
		            padding:    '5 0 5 0',
					margins:    '0 0 0 0'		            
		        },{
		            xtype: 'combo',	                     
					itemId: 'risorsa',
	                name: Extensible.calendar.data.EventMappings.Risorsa.name,				
					fieldLabel: 'Risorsa',
					forceSelection: true,
	                anchor: '100%',	
	                delimiter: ' , ', 
	                editable: false, 
	                multiSelect: true,                 
	                
					store: {
						autoLoad: true,
						editable: false,
						autoDestroy: true,	 
					    fields: [{name:'id'}, {name:'text'}],
					    data: [<?php echo acs_ar_to_select_json($main_module->find_TA_std('RISEN'), ""); ?>]
					},
	            
					valueField: 'id',                       
		            displayField: 'text',
		            
		            padding:    '10 0 10 0',
					margins:    '0 0 0 0'				
		        }],
		        
		buttons: [
			{
                itemId: 'delete-btn',
                iconCls: 'icon-delete',                
                text: 'Elimina prenotazione',
                disabled: false,
                handler: function() {
                	rec = this.up('window').open_vars.record;                	                	
					rec.store.remove(rec);

                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = this.up('window').open_vars.main_tab_panel.down('grid');												
									grid_sped.getStore().reload();				                    	
						
									this.up('window').close();
                	
                },
                //scope: this,
                minWidth: 150,
                hideMode: 'offsets'
            },
            {
                text: 'Salva',
                iconCls: 'icon-save',
                disabled: false,
                handler: function(){
                	rec = this.up('window').open_vars.record; 
			        var fields = rec.fields,
			            values = this.up('form').getForm().getValues(),
			            name,
			            M = Extensible.calendar.data.EventMappings,			            
			            obj = {};
			
			        fields.each(function(f) {
			            name = f.name;
			            if (name in values) {
			                obj[name] = values[name];
			            }
			        });
			        
			        rangefields = this.up('form').query('[xtype="extensible.daterangefield"]');
			        Ext.each(rangefields, function(rf) {
				        var dates = rf.getValue();
				        obj[M.StartDate.name] = dates[0];
				        obj[M.EndDate.name] = dates[1];
				        obj[M.IsAllDay.name] = dates[2];
				    });    
			        
			            rec.beginEdit();
        				rec.set(obj);
        				rec.endEdit();
        					
                            		//aggiorno calendario attuale
                            		this.up('window').open_vars.from_comp.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = this.up('window').open_vars.main_tab_panel.down('grid');												
									grid_sped.getStore().reload();				                    	
						
									this.up('window').close();
        					
                
                
                }
                //scope: this
            }
		],        
		        
		        
		listeners: {
				afterrender: function(comp){					

					rec = comp.up('window').open_vars.record;
					rec.set('Risorsa', rec.get('Risorsa').split(','));					
					comp.loadRecord(rec);
					comp.getForm().findField(Extensible.calendar.data.EventMappings.Risorsa.name).setValue(rec.get(Extensible.calendar.data.EventMappings.Risorsa.name).split(','));					
					
					rangefields = comp.query('[xtype="extensible.daterangefield"]');
					Ext.each(rangefields, function(rf) {
						
							//carico i valori per il datarange
							rf.setValue(rec.data);
							
							//disabilito i campi data (puo' modificare solo l'ora)
							Ext.each(rf.query('datefield'), function(dtrf) {
								dtrf.disable();
							});
							
					});
				 
				}
				
                , 'eventdelete': {
                        fn: function(win, rec){
                        	console.log('----- eventdelete ----');
                        	console.log(rec);
                            this.eventStore.remove(rec);
                            this.eventStore.sync();
                            win.hide();
                            this.showMsg('Event '+ rec.data.Title +' was deleted');
                        },
                        scope: this
                    }				
				
							 					 
		}		        
		        
	}

]}	
	
	
<?php	
	exit;
}








function assegna_porte_orari_default($area, $data){
	global $conn, $main_module, $cfg_mod_DeskAcq, $id_ditta_default;
	$prenotazioni_caricate = false;

	//Verifico se ci sono gi� prenotazioni... Se e' cos� non eseguo nuovamente
	$sql = "SELECT COUNT(*) AS C_ROW
	FROM {$cfg_mod_DeskAcq['file_testate']} TD
	INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
			ON SP.CSPROG = TD.TDNBOC AND CSCALE = '*SPR' AND SP.CSDT = TD.TDDT
			LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
			ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
			WHERE " . $main_module->get_where_std() . "
			AND CSPORT <> '' AND CSHMPG <> 0
 			  AND TA_ITIN.TAASPE = " . sql_t($area) . "
	 			  AND TDDTEP = {$data}
	 			  ";
	
	 			  $stmt = db2_prepare($conn, $sql);
	 			  echo db2_stmt_errormsg();
	 			  $result = db2_execute($stmt);
	 			  $row = db2_fetch_assoc($stmt);
	 			  if ($row['C_ROW'] > 0){
	 			  		$ret = array();
	 			  		$ret['success'] = true;
	 			  		$ret['msg'] = 'Assegnazione gi&agrave; effettuata';
	 			  		return $ret;
	}
	
	
	
	 			  $s_field = explode(" ", "TDDT TDNBOC TDCCON TDDCON CSPORT CSHMPG TA_FORN.TARIF1 TA_FORN.TARIF2 TA_FORN.TAPMIN");
	 			  $sql = "SELECT " . implode(", ", $s_field) . " FROM {$cfg_mod_DeskAcq['file_testate']} TD
	 			  INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
	 			  ON SP.CSPROG = TD.TDNBOC AND CSCALE = '*SPR' AND SP.CSDT = TD.TDDT
	 			  INNER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_FORN
	 			  ON TD.TDDT = TA_FORN.TADT AND TD.TDCCON = TA_FORN.TAKEY1 AND TA_FORN.TATAID = 'FORN'
	 			  LEFT OUTER JOIN {$cfg_mod_DeskAcq['file_tabelle']} TA_ITIN
	 			  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
	 			  WHERE " . $main_module->get_where_std() . "
			  AND CSPORT = '' AND CSHMPG = 0
	 			  		AND TA_ITIN.TAASPE = " . sql_t($area) . "
	 			  		AND TDDTEP = {$data}
	 			  		GROUP BY " . implode(", ", $s_field) . "
			ORDER BY TA_FORN.TAPMIN, TA_FORN.TARIF1, TA_FORN.TARIF2
				";

				$stmt = db2_prepare($conn, $sql);
				$result = db2_execute($stmt);
	
				$sql_upd = "UPDATE {$cfg_mod_DeskAcq['file_calendario']}
				SET CSPORT = ?, CSHMPG = ?, CSHMFG = ?
				WHERE CSCALE = '*SPR' AND CSDT = ? AND CSPROG = ?";
				$stmt_upd = db2_prepare($conn, $sql_upd);
					
				while ($row = db2_fetch_assoc($stmt)) {
	
					if (trim($row['TARIF1']) != '' && (int)$row['TARIF2'] > 0){
	
					if ((int)$row['TAPMIN'] == 0){
					//Assegno nell'orario prefissato
					
					$durata_in_minuti = 30;								
					$ora_start	= date('His', strtotime(print_ora($row['TARIF2'], 6, 'Y')));
					$ora_end	= date('His', strtotime(print_ora($ora_start, 6, 'Y')) + $durata_in_minuti*60);									
									
					$result = db2_execute($stmt_upd, array(trim($row['TARIF1']), (int)$ora_start, (int)$ora_end, $row['TDDT'], $row['TDNBOC']));
					if (!$result) echo db2_stmt_errormsg($stmt_upd);
					} else {
					//Cerco il primo orario disponibile
					if ($prenotazioni_caricate == false){
						$prenotazioni = carica_prenotazioni($area, $data);
						$prenotazioni_caricate = true;
					}
						
						
					//cerco il primo orario disponibile nella porta
					$mia_prenotazione = trova_orario_disponibile($prenotazioni[$row['TARIF1']], (int)$row['TARIF2']);
						
							//salvo la prenotazione
							if ($mia_prenotazione != false){
							$result = db2_execute($stmt_upd, array(trim($row['TARIF1']), $mia_prenotazione['ora_inizio_t'], $mia_prenotazione['ora_finale_t'], $row['TDDT'], $row['TDNBOC']));
									if (!$result)	echo db2_stmt_errormsg($stmt_upd);
	
									//aggiungo la prenotazione tra quelle esistenti
							$prenotazioni[$row['TARIF1']][] = array('CSHMPG' => $mia_prenotazione['ora_inizio_t'], 'CSHMFG' => $mia_prenotazione['ora_finale_t']);
							}
	
							}
			
							}
							}
	
							$ret = array();
							$ret['success'] = true;
							return $ret;
	
	
}





// ******************************************************************************************
// ASSEGNA LE PORTE E GLI ORARI STANDARD (IN BASE ALLA TABELLA FORNITORE)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_assegna_porte_orari_default'){
	global $cfg_mod_DeskAcq, $id_ditta_default;
	$m_params = acs_m_params_json_decode();
	$prenotazioni_caricate = false;	

	$ret = assegna_porte_orari_default($def_area, $m_params->f_data);
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// ASSEGNA COME STANDARD SU FORNITORE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_assegna_come_standard_su_fornitore'){
	global $cfg_mod_DeskAcq, $id_ditta_default;
	$ret = array();
	$m_params = acs_m_params_json_decode();
	
	//Verifico se il fornitore esiste gia'
	$forn = new DeskAcqFornitori();
	$forn->load_rec_data_by_k(array('TAKEY1' => $m_params->TDCCON));

	if (!isset($forn->rec_data) || empty($forn->rec_data)){
		//creo il fornitore
		$sql = "INSERT INTO " . $forn->get_table() . "(TADT, TATAID, TAKEY1, TADESC, TARIF1, TARIF2)
				VALUES(?, ?, ?, ?, ?, ?)";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($id_ditta_default, 'FORN', $m_params->TDCCON, $m_params->TDDCON, trim($m_params->CSPORT), $m_params->CSHMPG));
		$ret['success'] = $result;
	} else {
		//aggiorno porta e ora scarico predefinite
		$sql = "UPDATE " . $forn->get_table() ."
				SET TARIF1 = ?, TARIF2 = ?
				WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";

			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt, array(trim($m_params->CSPORT), $m_params->CSHMPG, $id_ditta_default, 'FORN', $m_params->TDCCON, ));
			$ret['success'] = $result;				
	}

	
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// AGGIORNA PORTA/ORA SU SPEDIZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_sped_set_ora'){	
	global $cfg_mod_DeskAcq;
	$ret = array();
	$m_params = acs_m_params_json_decode();
	
	$sql = "UPDATE {$cfg_mod_DeskAcq['file_calendario']} SET CSPORT = ?, CSHMPG = ?, CSHMFG = ? WHERE CSPROG = ? AND CSCALE = '*SPR'";
	$stmt = db2_prepare($conn, $sql);
	
	$durata_in_minuti = 30;
	$ora_start	= date('His', strtotime(print_ora($m_params->ora, 6, 'Y')));
	$ora_end	= date('His', strtotime(print_ora($ora_start, 6, 'Y')) + $durata_in_minuti*60);	
	
	$result = db2_execute($stmt, array($m_params->porta, $m_params->ora, (int)$ora_end, $m_params->sped_id));
		
	$ret['success'] = true;
	echo acs_je($ret);
 exit;
}


// ******************************************************************************************
// AGGIORNA EVENTO (resize, ....)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_update_event'){
	global $cfg_mod_DeskAcq;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "UPDATE {$cfg_mod_DeskAcq['file_calendario']} SET CSHMPG = ?, CSHMFG = ? WHERE CSPROG = ? AND CSCALE = '*SPR'";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($m_params->start, $m_params->end, $m_params->rec->id));

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// AGGIORNA EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_upd_events'){
	global $cfg_mod_DeskAcq;
	$ret = array();
	$m_params = acs_m_params_json_decode();
	
	//verifico ammissibilita' spostamento
	$ammesso = verifica_validita_evento($m_params);
	
	if ($ammesso['error'] == false){
		$sql = "UPDATE {$cfg_mod_DeskAcq['file_calendario']} 
				SET CSCCON = ?, CSHMPG = ?, CSHMFG = ? 
				WHERE CSPROG = ? AND CSCALE = '*SPR'";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array( $m_params->Risorsa,
										decodifica_ora($m_params->StartDate),
										decodifica_ora($m_params->EndDate),
										$m_params->EventId));
		$ret = $ammesso;
	} else 
		$ret = $ammesso;


	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// CREA EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_crt_events'){
	global $cfg_mod_DeskAcq;
	$ret = array();
	$m_params = acs_m_params_json_decode();	

	//su creazione prenotazione forzo 30 minuti
	$durata_in_minuti = 30;
	$ora_start	= date('His', strtotime(print_ora(decodifica_ora($all_params['StartDate']), 6, 'Y')));
	$ora_end	= date('His', strtotime(print_ora($ora_start, 6, 'Y')) + $durata_in_minuti*60);	

	//verifico ammissibilita' spostamento
	$ammesso = verifica_validita_evento($m_params);
	
	if ($ammesso['error'] == false){	
		$sql = "UPDATE {$cfg_mod_DeskAcq['file_calendario']} SET CSPORT = ?, CSHMPG = ?, CSHMFG = ? WHERE CSPROG = ? AND CSCALE = '*SPR'";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt, array($all_params['Porta'], decodifica_ora($all_params['StartDate']), $ora_end, $all_params['CreatedBy']));
		$ret = $ammesso;
	} else 
		$ret = $ammesso;
	
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// DELETE EVENTO (api writer)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_wr_del_events'){
	global $cfg_mod_DeskAcq;
	$ret = array();
	$m_params = acs_m_params_json_decode();

	$sql = "UPDATE {$cfg_mod_DeskAcq['file_calendario']} 
			SET CSPORT = ?, CSCCON = ?, CSHMPG = 0, CSHMFG = 0 
			WHERE CSPROG = ? AND CSCALE = '*SPR'";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array('', '', $m_params->EventId));

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// EVENTI CALENDARIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'cal_get_events'){
	global $conn, $cfg_mod_DeskAcq;	
	
	
 $ret = array();
 $ret['events'] = array(); 
 
 //devo per forza aver specificato unaporta
 if (strlen($_REQUEST['f_porta']) == 0){
 	echo acs_je($ret); exit;
 } 
 
 		//data iniziale e di fine
 			$from_calendar_format = 'Y-m-d'; 		
 			$d_start 	= to_AS_date_from_format($from_calendar_format, $_REQUEST["startDate"]);
 			$d_end 		= to_AS_date_from_format($from_calendar_format, $_REQUEST["endDate"]); 			 			
 	
 			
 			$ret['events'] = get_ar_eventi($_REQUEST['stab'], $_REQUEST['f_porta'], $d_start, $d_end);
 			
 			//aggiungo gli orari di indisponibilita'
 			$ar_orari = get_orari('STAB', $_REQUEST['stab'], $_REQUEST['f_porta'], $d_start);
 			$ar_ev_indisp = create_ev_orari_indisponibilita($ar_orari, $d_start);

 			foreach($ar_ev_indisp as $ri)
 				$ret['events'][] =  $ri;
 
 echo acs_je($ret);	
 exit;	
} //fn=cal_get_events





// ******************************************************************************************
// ELENCO SPEDIZIONI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_spedizioni'){
	global $conn;
	global $cfg_mod_DeskAcq;
	
	$ret = array();
	
	//devo per forza aver specificato una data
	if (strlen($_REQUEST['f_data']) == 0){
		echo acs_je($ret); exit;
	}
		
	
	$s_field = explode(" ", "TDNBOC TDCCON TDDCON CSPORT CSHMPG");
	$sql = "SELECT " . implode(", ", $s_field) . " FROM {$cfg_mod_DeskAcq['file_testate']} TD
				INNER JOIN {$cfg_mod_DeskAcq['file_calendario']} SP
				   ON SP.CSPROG = TD.TDNBOC AND CSCALE = '*SPR'
			WHERE " . $main_module->get_where_std() . "
			  AND TDDTEP = {$_REQUEST['f_data']} 
			GROUP BY " . implode(", ", $s_field); 

	$stmt = db2_prepare($conn, $sql);	
	$result = db2_execute($stmt);
	
	while ($row = db2_fetch_assoc($stmt)) {
		$row['id'] = $row['TDNBOC'];		
		$ret[] = $row;
	}
	

	echo acs_je($ret);
	exit;
} //fn=cal_get_events


// ******************************************************************************************
// PANNELLO PRINCIPALE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_panel_main'){

//dalla form mi viene passato data e stabilimento
$stab = new DeskAcqStabilimenti();
$stab->load_rec_data_by_k(array('TAKEY1' => $all_params['f_stabilimento']));


////////TODO: per stabilimento??
////////assegna_porte_orari_default($def_area, oggi_AS_date());

?>
{"success":true, "items":
 {
  xtype: 'panel',
  id: 'panel-main-fo_presca',
  title: 'Download',
  closable: true,
  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
  items: [
  
  // ------------------------ menu laterale -----------------------------
  		, {
			xtype: 'panel',
			layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
			flex: 0.3,			
			items: [			
					
					//form filtri
					, {
						xtype: 'form',
						items: [{
							    xtype: 'hiddenfield',
								name: 'f_stabilimento',
								value: <?php echo j($all_params['f_stabilimento'])?>
							}

			
							 , {
								     name: 'f_data'
								   , disabled: false                		
								   , xtype: 'datefield'
								   , startDay: 1 //lun.
								   , fieldLabel: '<b>Data consegna</b>'
								   , labelAlign: 'right'
								   , margins:    '10 10 10 10'
								   , padding:    '15 10 10 10'								   
								   , format: 'd/m/Y'						   						   
								   , submitFormat: 'Ymd'
								   , allowBlank: false
								   , anchor: '-15'
								   , value: '<?php echo print_date($all_params['f_data'], "%d/%m/%Y"); ?>'
								   , listeners: {
								       		invalid: function (field, msg) {
								       			Ext.Msg.alert('', msg);
								       		},
								       		
								            change: function(){      
								                  		
								            	var form = this.up('form').getForm();
										
												if(form.isValid()){
												
													f_d = form.getValues()['f_data'];
													f_d_Y = f_d.substr(0, 4);
													f_d_M = f_d.substr(4, 2) - 1;
													f_d_D = f_d.substr(6, 2);																		
													var d1 = new Date(f_d_Y, f_d_M, f_d_D);
												
													//sul giorno selezionato eseguo la procedura per associare
													// le porte e orari di default
													
													panel_main_fo_presca = this.up('#panel-main-fo_presca');
													
											        Ext.Ajax.request({
											            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_porte_orari_default',
											            method: 'POST',
									        			jsonData: form.getValues(),						            
											            success: function ( result, request) {
											                var jsonData = Ext.decode(result.responseText);
											                
				
											                //aggiorno la vista
											                
																//aggiorno i calendari sulle varie porte
													        	cal_porte = panel_main_fo_presca.query('[xtype="extensible.calendarpanel"]');	                    	
											                	Ext.each(cal_porte, function(cal) {cal.setStartDate(d1);});
																
																//aggiorno elenco spedizioni del giorno selezionato
																grid_sped = panel_main_fo_presca.down('#fo_presca_grid_spedizioni');
																grid_sped.getStore().proxy.extraParams = form.getValues();
																grid_sped.getStore().reload();							                
											                
											                											                															
											            },
											            failure: function ( result, request) {
											            }
											        });									
													
				
											    }    								                  		
												                  		
								                  		            	
								            } //change															       		
								       		
									 },
									 	
								}, {
								 xtype: 'button',
								 text: 'Crea nuova ricezione',
			            		 iconCls: 'iconSpedizione',			            		 
			            		 //scale: 'large'						
								 anchor: '100%', margin: "3 10 5 10",
								 handler: function() {
								   acs_show_win_std('Crea nuova ricezione', 
								   		'<?php echo $_SERVER['PHP_SELF']; ?>?fn=form_nuova_ricezione', 
								   		this.up('form').getValues(),
								   		680, 420, null, 'iconSpedizione'
		                			);
								 }
								} 
											
			
							
						
						]
						
 	
					}
					
  
  					//----- elenco ricezioni ------------
  					, {					
						xtype: 'grid',
						flex: 1,
						id: 'fo_presca_grid_spedizioni',
						loadMask: true,
						
						layout: {
                			type: 'fit',
                			align: 'stretch'
     					},

	        			viewConfig: {
	        
				            plugins: {
				                ptype: 'gridviewdragdrop',
				                dragGroup: 'DayViewDD',
				                dropGroup: 'DayViewDD'
				            }
				        },
     					
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_spedizioni',
								method: 'POST',
								type: 'ajax',
								
								extraParams: {
									f_data: <?php echo $all_params['f_data']; ?>,
									f_stabilimento: <?php echo j($all_params['f_stabilimento']); ?>
								},

								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
					            						
							},
								
							fields: ['id', 'TDNBOC', 'TDCCON', 'TDDCON', 'CSPORT', {name: 'CSHMPG', type: 'int'}]
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [{header: '#', dataIndex: 'TDNBOC', width: 50, hidden: true},
								  {header: 'Fornitore', dataIndex: 'TDDCON', flex: 8},
								  {header: 'Porta', dataIndex: 'CSPORT', width: 75},
								  {header: 'Ora', dataIndex: 'CSHMPG', width: 50, renderer: time_from_AS, align: 'right'}]
								  
					 , listeners: {

						itemcontextmenu : function(grid, rec, node, index, event) {
						  				  event.stopEvent();
									      var record = grid.getStore().getAt(index);		  
									      var voci_menu = [];
									      
									      voci_menu.push({
								      		text: 'Assegna come standard',
								    		iconCls : 'icon-save-16',      		
								    		handler: function() {
											        Ext.Ajax.request({
											            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna_come_standard_su_fornitore',
											            method: 'POST',
									        			jsonData: rec.data,						            
											            success: function ( result, request) {
											                var jsonData = Ext.decode(result.responseText);
															console.log('ok');											                															
											            },
											            failure: function ( result, request) {
											            }
											        });
								    		}
										  });
				
									      var menu = new Ext.menu.Menu({
									            items: voci_menu
										}).showAt(event.xy);
								   }					 
					 	
					 	
					 }			  
						 
					} //menu laterale
  			]
  		}	
  
  
  
  // ------------------------ panel e calendario --------------------------------
  
	, {
	  xtype: 'panel', flex: 1,	  
	  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
	  title: <?php echo j("Stabilimento " . $stab->rec_data['TADESC']); ?>,
	  cls: 'acs-light',	  	  
	  items: [  
  
  
	  	<?php
	  	//visualizzo il calendario per ogni porta 
	  	$el_porte = $main_module->get_elenco_porte_stabilimento($stab->rec_data['TAKEY1']);
	  	
	  	//in base allo stabilimento verifico se se e' stato impostato (yaml_params) l'orario di apertura/chiusura
	  	$stab_yp = (array)json_decode(trim($stab->get_YAML_PARAMETERS($stab->rec_data)));

	  	$ora_apertura = 0; $ora_chiusura = 24;
		if (isset($stab_yp['ora_apertura'])) $ora_apertura = $stab_yp['ora_apertura'];
		if (isset($stab_yp['ora_chiusura'])) $ora_chiusura = $stab_yp['ora_chiusura'];
	  	
	  	foreach ($el_porte as $kp => $porta){  	
	  	?>

			  , {
				   xtype: 'extensible.calendarpanel', flex: 1,
		        	border: false,
		        	frame: true,				   
                	collapsible: false,
                	collapsed: false,                	
                					   
					title: <?php echo j("Porta: " . $porta['text']); ?>,
  
  					enableDD: true,
  					ddGroup: 'sped_grid',
  					ddCreateEventText: 'aaaaa',
  
					//definizione calendari        
        			calendarStore: new Extensible.calendar.data.MemoryCalendarStore({
            			data: {
						    "calendars" : [{
						        "id"    : 1,
						        "title" : "Std",
						        "color" : 26
						    },{
						        "id"    : 2,
						        "title" : "non usato",
						        "color" : 2
						    },{
						        "id"    : 3,
						        "title" : "Non modificabili",
						        "color" : 22
						    },{
						        "id"    : 4,
						        "title" : "Altro",
						        "color" : 26
						    }]
						}
        			}),	
        				  	
        			//recupero eventi
					eventStore: new Extensible.calendar.data.EventStore({

					       autoLoad: true,
        				   autoSync: true,										       

		                    proxy: {
		                    	url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
		                        type: 'ajax',
								method: 'POST',		                        
		                        extraParams: {
		                        	f_porta: <?php echo j($porta['id']); ?>,
		                        	stab: <?php echo j($stab->rec_data['TAKEY1']); ?> 
		                        },
		                        reader: {
            						type: 'json',		                        
		                            root: 'events',
		                            successProperty: 'success',
		                            messageProperty: 'message'
		                        },
		                        
		                        
					            writer: {
					                type: 'json',
					                writeAllFields: true
					            },	
					            
					            listeners: {
						            exception: function(proxy, response, operation){						            
										acs_show_msg_error(operation.getError());
						            }
						        },
					            	
					            
					            api: {
					                read: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
					                create: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_crt_events',
					                update: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_upd_events',
					                destroy: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_del_events',
					            },
					            				                        
		                        
		                    },
	                    
		                    
		                    					          
					    }),

                    activeItem: 0, // day view   
                    
                    showNavBar: false,
                      
                    dayViewCfg: {
                        showHeader: false,
                        showWeekLinks: true,
                        showWeekNumbers: true,
						startDate: js_date_from_AS(<?php echo $all_params['f_data']; ?>),				
						viewStartHour  : <?php echo $ora_apertura; ?>,
						viewEndHour  : <?php echo $ora_chiusura; ?>,
						scrollStartHour: 6,
						ddGroup: 'DayViewDD',
						showTime: false,
						showTodayText: false,						
						showHourSeparator: true,
						hourHeight: 45							
                    },                      
  
                                        
                    listeners: {
                    
						onEventDrop: function(rec, dt) {
						 console.log('cccccccc');
						}                    
                    

                        , 'eventclick': {
                			fn: function(panel, rec, el){                			
                			
		                		if (parseInt(rec.get(Extensible.calendar.data.EventMappings.CalendarId.name)) == 3)
                                	return false; //non posso spostare gli eventi non miei                			
                			
		                		acs_show_win_std('Modifica prenotazione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=edit_event', null, 680, 220, null, 'icon-inbox-16', null, null, {
		                			record: rec, 
		                			from_comp: panel,
		                			main_tab_panel: panel.up('#panel-main-fo_presca')
		                			});			                			                            	                           
                                return false; //per non attivare evento std
                            },
                            scope: this
                        }							
						
						, 'eventmove': {
                            fn: function(dd, rec, dt){
                            	console.log('------ eventmove ------');
                            	
                            		this.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = dd.up('#panel-main-fo_presca').down('#fo_presca_grid_spedizioni');												
									grid_sped.getStore().reload();				                    	
                            	
                            	
                            }	
                        },
						
						
						'eventresize': {
                            fn: function(dd, rec, dt){
                            	console.log('------ eventresize ------');
								
                            		this.store.reload();
												
									//aggiorno elenco spedizioni del giorno selezionato
									grid_sped = dd.up('#panel-main-fo_presca').down('#fo_presca_grid_spedizioni');												
									grid_sped.getStore().reload();				                    	
                           	
                            	
                            }
                        },  
                        
                        
						'eventrdrag': {
                            fn: function(dd, rec, dt, a, b){
                            	console.log('------ eventrdrag ------');
								
                            	
                            }
                        },                                                   
                    
                    
                        'eventadd': {
                            fn: function(dd, rec, dt){
                            	console.log('---- eventadd -----');
                            	
                           		//Aggiorno tutti i calendari
                            	//aggiorno tutte
                            	pan = Ext.getCmp('panel-main-fo_presca');
						        cal_porte = pan.query('[xtype="extensible.calendarpanel"]');	                    	
				                Ext.each(cal_porte, function(cal) {cal.store.reload();});                           		
												
								//aggiorno elenco spedizioni del giorno selezionato
								grid_sped = pan.down('#fo_presca_grid_spedizioni');												
								grid_sped.getStore().reload();				                    	
                            	

                            },
                            scope: this
                        }           
                        
                        
                       //per prevenire la creazione di nuovi eventi  
					   , 'dayclick': {fn: function(dd, rec, dt, a, b){return false;}}
					   , 'rangeselect': {fn: function(dd, rec, dt, a, b){return false;}}                        
                                 
                    }                    
   
   
   
				    , showMsg: function(msg){
				        //// Ext.fly('app-msg').update(msg).removeCls('x-hidden');
				    }
				    , clearMsg: function(){
				        /// Ext.fly('app-msg').update('').addCls('x-hidden');
				    }
				   
				    , showEditWindow : function(rec, animateTarget){
				    }
                    
  
  
   		        
		        // this is a good idea since we are in a TabPanel and we don't want
		        // the user switching tabs on us while we are editing an event:
		     ,   editModal: true
      
  		} //calendario
  		
  	<?php } //per ogni porta?>	
  		
  	  ]
    } //panel con calendari porte
  
  ]
  
 }
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// FORM APERTURA RICHIESTA PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_open_form'){
?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [
					
					
					
					<?php
						//se non ho il menu, entro solo con una sotto visualizzazione
						// TODOOOOOOOOOOOOOOOOO
						if ($js_parameters->p_solo_inter != 'Y'){					 
							$f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "OPEN_UPLOAD");
						}	
					  ?>
					
					
			         <?php if ($js_parameters->p_solo_inter != 'Y'){?>										
					  {
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	m_win = this.up('window');
			            	form_values = form.getValues();
			            	
			            	//in visualizzazione non tengo conto della data finale (usata solo nel report)
			            	form_values['f_data_a'] = null;
			            	 
			            	if(form.isValid()){
			            	
					        	mp = Ext.getCmp('m-panel');
					        	av_prenotazione_carichi = Ext.getCmp('panel-main-fo_presca');					        	
					
					        	if (av_prenotazione_carichi){
					        		av_prenotazione_carichi.show();		    	
					        	} else {
					
					        		//carico la form dal json ricevuto da php
					        		Ext.Ajax.request({
					        		        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_panel_main',
					        		        jsonData   : form_values,
					        		        method     : 'POST',
					        		        waitMsg    : 'Data loading',
					        		        success : function(result, request){
					        		            var jsonData = Ext.decode(result.responseText);
					        		            mp.add(jsonData.items);
					        		            mp.doLayout();
					        		            mp.show();
					        		            av_prenotazione_carichi = Ext.getCmp('panel-main-fo_presca').show();
//					        		            av_prenotazione_carichi.store.reload();
					        		            
					        		            m_win.close();        		            
					        		        },
					        		        failure    : function(result, request){
					        		            Ext.Msg.alert('Message', 'No data to be loaded');
					        		        }
					        		    });
					
					        	} 
			            	
			            	
								
							
			            	}
			            }
			         }
			         <?php } ?>
		            
		            ],   		            
		            
		            items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , margin: "10 10 0 10"
						}
						
			//se non ho il menu, entro solo con una sotto visualizzazione  						
			<?php if ($js_parameters->p_solo_inter != 'Y'){ ?>																		
						, {
							name: 'f_stabilimento',
							xtype: 'combo',
							fieldLabel: 'Stabilimento',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($main_module->find_TA_std('START'), ""); ?>		
								    ] 
								}						 
							}								
							
			<?php } ?>																	  
						  
						  ]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		

<?php exit; }?>
