<?php

require_once "../../config.inc.php";
require_once "acs_panel_protocollazione_include.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$main_module = new DeskAcq();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
    $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}


//$ar_email_json = acs_je($ar_email_to);
$ar_email_json = $ar_email_to;


$oe = $s->k_ordine_td_decode_xx($_REQUEST['k_ordine']);
$row= $s->get_ordine_gest_by_k_docu($_REQUEST['k_ordine']);
$commento= $s->get_commento_ordine_by_k_ordine($_REQUEST['k_ordine']);

$doc_type = get_doc_type_by_tipo($row['TDOTPD']);
$modello = '';



$ar = array();

$sql_rd = "SELECT RDART, RDDART, RDRIGA, RDQTA, RDUM, RDPRIO, RTINFI, RTPRZ
, AL.ALDAR1 AS TRAD_LNG_1, AL.ALDAR2 AS TRAD_LNG_2
, AL.ALDAR3 AS TRAD_LNG_3, AL.ALDAR4 AS TRAD_LNG_4
FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_art_trad']} AL
ON AL.ALDT=RD.RDDT AND AL.ALART=RD.RDART AND AL.ALLING='{$cfg_mod_DeskPVen['traduzione']}'
WHERE RD.RDART NOT IN('ACCONTO', 'CAPARRA') AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
AND RD.RDTISR = '' AND RD.RDSRIG = 0
ORDER BY RD.RDRIGA";

$stmt_rd = db2_prepare($conn, $sql_rd);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_rd, $oe);


$sql_c = "SELECT RT.RTPRZ AS CAPARRA
FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
INNER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
WHERE RD.RDART = 'CAPARRA' AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
AND RD.RDTISR = '' AND RD.RDSRIG = 0";

$stmt_c = db2_prepare($conn, $sql_c);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_c, $oe);

$row_c = db2_fetch_assoc($stmt_c);

$sql_a = "SELECT RT.RTPRZ AS ACCONTO
FROM {$cfg_mod_DeskPVen['file_righe_doc_gest']} RD
INNER JOIN {$cfg_mod_DeskPVen['file_righe_doc_gest_valuta']} RT
ON RT.RTDT=RD.RDDT AND RD.RDTIDO = RT.RTTIDO AND RD.RDINUM = RT.RTINUM AND RD.RDAADO = RT.RTAADO AND RD.RDNRDO = RT.RTNRDO AND RD.RDNREC=RT.RTNREC AND RTVALU='EUR'
WHERE RD.RDART = 'ACCONTO' AND RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
AND RD.RDTISR = '' AND RD.RDSRIG = 0";

$stmt_a = db2_prepare($conn, $sql_a);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_a, $oe);


$row_a = db2_fetch_assoc($stmt_a);


$sql_imp = "SELECT TOIMPO1, TOVTRA, TOCTRA
FROM {$cfg_mod_DeskPVen['file_testate_gest_valuta']}

WHERE TODT = ? AND TOTIDO = ? AND TOINUM = ? AND TOAADO = ? AND TONRDO = ? AND TOVALU='EUR'";


$stmt_imp = db2_prepare($conn, $sql_imp);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_imp, $oe);

$row_imp = db2_fetch_assoc($stmt_imp);

$des_pag_ar_cap = $s->get_TA_std('TPCAP', trim($row['TDCARP']));
$des_pag_cap = trim($des_pag_ar_cap['TADESC']);
$flag_g = trim($des_pag_ar_cap['TAFG01']);

if($flag_g == 'G'){
    $tot_finale = $row_imp['TOIMPO1'] + $row_a['ACCONTO'];
}else{
    $tot_finale = $row_imp['TOIMPO1'] + $row_a['ACCONTO'] + $row_c['CAPARRA'];
}

$ret_imp = array(
    "imp_totale" 	=> $tot_finale,
    "saldo"	        => $row_imp['TOIMPO1'],
    "imp_trasp"	    => $row_imp['TOVTRA'],
    "imp_trasp_2"	=> $row_imp['TOCTRA']
    
);

/*if(trim(risposta_PVNOR($row['TDDT'], $_REQUEST['k_ordine'], '06'))=='EM')
 $ret_imp["imp_trasp"] = $row_imp['TOCTRA'];*/
                                                                                                                                                                                               


$sql_cli = "SELECT CF.*, SUBSTR(TA_ON.TAREST, 61, 60) AS EMAIL_ON, SUBSTR(TA_RN.TAREST, 41, 60) AS EMAIL_RN
            FROM {$cfg_mod_Spedizioni['file_anag_cli']} CF {$join_riservatezza}
            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} TA_RN
              ON TA_RN.TADT = CF.CFDT AND TA_RN.TACOR1 = CF.CFCD AND TA_RN.TAID = 'BRCF' AND TA_RN.TANR = '@RN'           
            LEFT OUTER JOIN {$cfg_mod_Admin['file_tab_sys']} TA_ON
			  ON TA_ON.TADT = CF.CFDT AND TA_ON.TACOR1 = CF.CFCD AND TA_ON.TAID = 'BWWW' AND TA_ON.TANR = '*ON' 
            WHERE CFCD= '{$row['TDCCON']}'
            AND CFDT = '{$row['TDDT']}'
            ORDER BY CFRGS1, CFRGS2";

$stmt_cli = db2_prepare($conn, $sql_cli);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_cli);

$row_cli = db2_fetch_assoc($stmt_cli);

if(trim($row_cli['EMAIL_RN']) != '')
    $email = trim($row_cli['EMAIL_RN']);
else
    $email = trim($row_cli['EMAIL_ON']) ;

$ret_cli = array(
    "descr" 	=> acs_u8e(trim($row_cli['CFRGS1']) . " " . trim($row_cli['CFRGS2'])),
    "cod_cli" 	=> acs_u8e(trim($row_cli['CFCD'])),
    "out_loc"	=> acs_u8e(trim($row_cli['CFLOC1']) . " " . trim($row_cli['CFPROV'])),
    "out_ind"	=> acs_u8e(trim($row_cli['CFIND1']) . " " . trim($row_cli['CFIND2'])),
    "cap"		=> acs_u8e(trim($row_cli['CFCAP'])),
    "tel"		=> acs_u8e(trim($row_cli['CFTEL'])),
    "tel2"		=> acs_u8e(trim($row_cli['CFTEX'])),
    "p_iva"		=> acs_u8e(trim($row_cli['CFPIVA'])),
    "cod_fisc"  => acs_u8e(trim($row_cli['CFCDFI'])),
    "email"     => acs_u8e($email),
);

if($ret_cli['email'] != ''){
    $ar_email_for = array(
        array('default' => 'Y', $ret_cli['email'], "Fornitore {$ret_cli['descr']} ({$ret_cli['email']})")
    );

    $ar_email_json = acs_je(array_merge($ar_email_to, $ar_email_for));
}else{
    $ar_email_json = acs_je($ar_email_to);
    
}

$des_pag_ar = get_TA_sys_by_code('CUCP', trim($row['TDPAGA']));
$des_pag = trim($des_pag_ar['TADESC']);



if (trim($des_pag_ar['TADES2']) != '')
    $des_pag .= "<br/>" . trim($des_pag_ar['TADES2']);
    
    $ret = array(
        
        "data_contratto"		=> acs_u8e(trim($row['TDDTRG'])),
        "data_consegna"		    => acs_u8e(trim($row['TDDTEP'])),
        "data_consegna_con"		=> acs_u8e(trim($row['TDODER'])),
        "numero"		        => (int)trim($row['TDNRDO']),
        "pagamento"             => acs_u8e($des_pag),
        "imponibile"		    => acs_u8e(trim($row['TDTIMP'])),
        
        "email"                 => acs_u8e(trim($row['TDMAIL'])),
        "tot_imp"               => acs_u8e(trim($row['TDTOTD'])),
        "prior"                 => acs_u8e(trim($row['TDDPRI'])),
        "refe"                  => acs_u8e(trim($row['TDREFE'])),
        "rif"                   => acs_u8e(trim($row['TDVSRF'])),
        "tipo_ordine"           => acs_u8e(trim($row['TDOTPD'])),
        "tipologia_ordine"      => acs_u8e(trim($row['TDCLOR'])),
        "tipol_amb"             => trim($row['TDCVN1']),
        "stab"                  => trim($row['TDSTAB']),
        "tipo_pag_cap"          => acs_u8e($des_pag_cap)
        //"tipol_amb"             => decode_PUVN_code('MOD', trim($row['TDCVN1']))
        
    );
    
    
    
    function decod_risposta($domanda, $risposta){
        global $s;
        $ar=$s->get_TA_std('PRVAN', $domanda, $risposta);
        return $ar['TADESC'];
    }
    
    
    /**************** CREAZIONE AR RIGHE RIGHE   *********************************************************************************/
    $c_righe = 0;
    while($row_rd = db2_fetch_assoc($stmt_rd)){
        $c_righe++;
        
        $ar_new = array();
        $ar_new['cod_articolo']  = trim($row_rd['RDART']);
        
        $is_order_prec = 'N';
        //verifica se ordine precedente a modifica su gestione descrizioni
        if ($doc_type == 'P' && ($oe['TDOADO'] < 2017 || ($oe['TDOADO'] == 2017 && $oe['TDONDO']<='000058')))
            $is_order_prec = 'Y';
            if ($doc_type == 'O' && ($oe['TDOADO'] < 2017 || ($oe['TDOADO'] == 2017 && $oe['TDONDO']<='000331')))
                $is_order_prec = 'Y';
                
                if($is_order_prec == 'Y' && trim($row_rd['TRAD_LNG_1']) != ''){
                    $ar_new['articolo'] = trim($row_rd['TRAD_LNG_1']);
                    
                    //dalla trad del primo articolo recupero il modello (se presente)
                    if ($c_righe==1)
                        $modello = $row_rd['TRAD_LNG_1'];
                        
                        //accodiamo altre parti della descrizione
                        if(trim($row_rd['TRAD_LNG_2']) != '')
                            $ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_2']);
                            if(trim($row_rd['TRAD_LNG_3']) != '')
                                $ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_3']);
                                if(trim($row_rd['TRAD_LNG_4']) != '')
                                    $ar_new['articolo'] .= "<br/>" . trim($row_rd['TRAD_LNG_4']);
                                    
                } else{
                    $ar_new['articolo'] = trim($row_rd['RDDART']);
                }
                
                $ar_new['quant']  = $row_rd['RDQTA'];
                $ar_new['un_mis'] = trim($row_rd['RDUM']);
                $ar_new['prezzo'] = $row_rd['RTPRZ'];
                $ar_new['importo'] = $row_rd['RTINFI'];
                $ar_new['prio'] = $row_rd['RDPRIO'];
                
                $ar[] = $ar_new;
    }



?>
   
 <head>

  <style>
   <?php @include '../../personal/report_css_logo_cliente.php'; ?>
   #email_obj {visibility: hidden;}
   div.{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
 
   .cella_gray{background-color: #D1CCBF;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 12px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}  
   
  table.intest td{border: 0px;} 
 
    
   tr.ag_liv_data th{background-color: #333333; color: white;}
   .grassetto th{font-weight: bold;}
    .box{width: 250px;
   		 height: 150;
   		 padding:10px;
   		 border: 1px solid;
   		 
   	  } 
   
   @media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
 @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>
  


 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'N';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<html>
<div id='email_obj'>
<?php $var = "Richiesta di variazione numero {$ret['numero']} del ".print_date($ret['data_contratto']); 
echo $var;
?>

</div>
		

<?php




echo "<div id='my_content'>";
  $stampa='N';
 include "acs_report_include.php"; 


echo "<br>";
echo "<br>";
echo "<table class='grassetto'>";
echo "<tr>";
echo "<tr><td colspan=6 class='cella_gray' style='text-align:center;'> <i> Riepilogo condizioni non conformi</i> </td></tr>";
echo "<th>Articolo</th>";
echo "<th>Descrizione</th>";
echo "<th>UM</th>";
echo "<th>Quantit&agrave;</th>";
echo "<th>Prezzo</th>";
echo "<th>Importo</th>";
echo"</tr>";

$tot_imp = 0;
foreach ($ar as $k => $v){
	  
		if (trim($v['cod_articolo']) == '*'){
		    if(trim($v['articolo']) != ''){
		      echo "<tr>";
		      echo "<td>&nbsp;</td>";
			  echo "<td valign=top colspan=5>".trim($v['articolo'])."</td>";
			  echo "</tr>";
		    }
		} else {
		    echo "<tr>";
		    echo "<td valign=top>".$v['cod_articolo']."</td>";
		    echo "<td valign=top>".$v['articolo']."</td>";
			echo "<td valign=top style='width:30px;'>".$v['un_mis']."</td>";
			echo "<td valign=top style='text-align:right; width:70px;'>".n($v['quant'],2)."</td>";
			echo "<td valign=top style='text-align:right;'>".n($v['prezzo'],2)."</td>";
			echo "<td valign=top style='text-align:right;'>".n($v['importo'],2)."</td>";
			echo "</tr>";
		}	
			
		if($_REQUEST['nota_credito'] == 'Y'){
		    if($v['prio'] != 'NC')
		      $tot_imp += $v['importo'];
		}else 
		  $tot_imp += $v['importo'];
} 
if ($_REQUEST['tipo_stampa'] == 'SCHEDA'){
    
    echo "<tr>";
    echo "<td valign=top colspan=5><b>Importo totale accredito atteso</b></td>";
    echo "<td valign=top style='text-align:right; width:70px;'>".n($tot_imp, 2)."</td>";
    echo "</tr>";
}
echo "</table>";

echo "<br/><br/>";


//*********************************************************************************
if ($_REQUEST['tipo_stampa'] != 'SCHEDA'){
	
	if(trim($ret['tipologia_ordine']) =='A'){
		echo "<p> Pagamento: ".$ret['pagamento']."</p>";
	}else{
	
	echo "<table class='totale'>";
	echo "<tr><td style = 'width: 60%; border: 0px;'>Pagamento: ".$ret['pagamento']."</td><th class='cella_gray'><b>Importo totale IVA inclusa</b></th><td class='cella_gray' style='text-align:right;'><b>".n($ret_imp['imp_totale'], 2)."</b></td></tr>";
	
	
	
	if ($doc_type != 'P'){
		if ((float)$row_c['CAPARRA'] <> 0)
			echo "<tr><td style = 'border: 0px;'>&nbsp;</td><th>Caparra (".$ret['tipo_pag_cap'].")</th><td style='text-align:right;'>".n($row_c['CAPARRA'],2)."</td></tr>";
		if ((float)$row_a['ACCONTO'] <> 0)
			echo "<tr><td style = 'border: 0px;'>&nbsp;</td><th>Acconto</th><td style='text-align:right;'>".n($row_a['ACCONTO'],2)."</td></tr>";
		if ((float)$row_c['CAPARRA'] <> 0 || (float)$row_a['ACCONTO'] <> 0)
			echo "<tr><td style = 'border: 0px;'>&nbsp;</td><th><b>Saldo </b></th><td style='text-align:right;'>".n($ret_imp['saldo'],2)."</td></tr>";
		
	}	
	
	echo "</table>";
	
	echo "<br>";
	echo "<br>";
	
	echo "<table class='totale'>";
	
	if($ret_imp['imp_trasp'] > 0){
	echo "<tr><td style = 'width: 60%; border: 0px;'>&nbsp;</td><th><b>Importo trasporto </b></th><td style='text-align:right;'>".n($ret_imp['imp_trasp'],2)."</td></tr>";
	}
	
	
	echo "<tr>";
	echo "<td colspan= 3 style = 'border: 0px;'>&nbsp;</td>";
	echo "</tr>";
	echo "<tr><td style = 'width: 60%; border: 0px;'>&nbsp;</td><th th class='cella_gray'><b>";
	echo $doc_type=='P' ? 'Totale proposta': 'Totale vs. dare<br/></b>Il saldo va effettuato entro 4gg prima della consegna';
	echo "</b></th>";
	
	if(trim(risposta_PVNOR($row['TDDT'], $_REQUEST['k_ordine'], '06'))=='EM')	
		echo "<td class='cella_gray' style='text-align:right;'><b>".n($ret_imp['saldo'],2)."</b></td></tr>";	
	else 
		echo "<td class='cella_gray' style='text-align:right;'><b>".n($ret['tot_imp'],2)."</b></td></tr>";
	
	
	if($ret_imp['imp_trasp_2'] > 0){
		echo "<tr>";
		echo "<td colspan= 3 style = 'border: 0px;'>&nbsp;</td>";
		echo "</tr>";
		echo "<tr><td style = 'width: 60%; border: 0px;'>&nbsp;</td><th><b>Importo trasporto da pagare a montatore</b></th><td style='text-align:right;'>".n($ret_imp['imp_trasp_2'],2)."</td></tr>";
	}
		
		
	
	echo "</table>";
	
	}
	
	echo "<br>";
	echo "<br>";
	echo "<p>&nbsp;Termini di consegna ";
	echo $doc_type=='P' ? 'prospettati': 'concordati';
	echo ":</p>";
	if(trim($ret['tipo_ordine']) != 'MO'){
		echo "<p>&nbsp;".$ret['prior']." ".print_date($ret['data_consegna'])."</p>";
	}else{
		echo "<p>&nbsp;consegna della merce entro 30/40 gg lavorativi dal momento in cui viene effettuato il saldo del 30% dell'importo totale.</p>";
	}
	echo "<br>";
	echo "<br>";
	

} //not SCHEDA

	echo "<table>";
	echo "<tr>";
	echo "<th><b> Annotazioni</b></th>";
	echo "<tr><td>";
	foreach ($commento as $c => $t){
		
		echo $t['text']."<br>";
		
	}
	echo "</td></tr>";
	echo "</table>";
	echo "<br>";
	
	//*********************************************************************************
	if ($_REQUEST['tipo_stampa'] != 'SCHEDA'){
	?>
	
	<?php if ($doc_type != 'P' && trim($ret['tipologia_ordine']) != 'A'){ ?>
	<table width="100%" class="intest">
	 <tr>
	   <td width="33%" align=center>
	    PARTE VENDITRICE
	    <br/>&nbsp;<br/>
	    _________________________________
	   </td>
	
	   <td width="33%">&nbsp;</td>
	
	   <td width="33%" align=center>
	    L'ACQUIRENTE
	    <br/>&nbsp;<br/>
	    _________________________________
	   </td>   
	 </tr>
	</table>
	
	<br>
	<br>
	
	<?php if (trim($ret['tipo_ordine']) !='MA' || trim($ret['tipo_ordine']) !='ME' ){ ?>
	
	<div class=page-break></div>
		<p style='text-align:center'> <b>Contratto numero <span style= 'font-weight: bold; font-size: 14px;'><?php echo $ret['numero'] ?></span> del <?php echo print_date($ret['data_contratto']); ?></b></p>
		<p style="text-align: center">
		    <div style='
		    	height: 1000px;
		    	width: 700px; 
		    	margin-right: auto; margin-left: auto;
   				background-image: url(<?php echo '"' . "http://" . $_SERVER['HTTP_HOST'] . ROOT_PATH .  'personal/condizioni_generali.png' . '"'; ?>);   		
		   		background-repeat: no-repeat;
		   		background-size: 700px 1000px;'>
 			</div>
		</p>
	
	<?php 
	}
	
	$stampa='Y';
	
	if (trim($ret['tipo_ordine']) =='MO' && trim($ret['tipol_amb']) != '30'){ ?>
	
	<div class=page-break></div>	
		
		<?php include "acs_report_include.php";?>
	<br>
	<br>	
	<div style='width:100%; height:40%; border:1px solid black;'></div>	
	<br>
	<p><b>NOTE: </b>  <span style='color:gray;'>___________________________________________________________________________________________________________  </span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________ </span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________</span></p>
	<br>
	<p><span style='color:gray;'>_________________________________________________________________________________________________________________</span></p>
	
	<?php }

} //if tipo_stampa != 'SCHEDA
//***************************************************


?>	

</div>

<?php } ?>
 </body>
</html>		