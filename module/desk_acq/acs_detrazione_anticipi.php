<?php

require_once "../../config.inc.php";

$m_DeskAcq = new DeskAcq();

$main_module=new DeskAcq();
$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'exe_detrai_anticipo'){

    $m_params = acs_m_params_json_decode();
    
    $use_session_history = microtime(true);
    
    foreach($m_params->list_righe_selected as $v){
        
              
        $sh = new SpedHistory($m_DeskAcq);
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'DET_FAT_ANTG',
                "k_ordine" => $v->fattura,
                "use_session_history" 	=> $use_session_history,
                "vals" => array(
                    "RIIMPO" 	=> sql_f($v->importo),
                   
                ),
               
            )
            );
        
        
    }
    
    $sh = new SpedHistory($m_DeskAcq);
    $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'DET_FAT_ANTG',
            "k_ordine" => $m_params->k_ord,
            "end_session_history" 	=> $use_session_history,
           
        )
        );
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);

    exit;
}



// ******************************************************************************************
// get_json_data_fattura
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_fattura'){
    
    $m_params = acs_m_params_json_decode();
  
    $fornitore=$m_params->open_request->fornitore;
    
    $sql= "SELECT TF.*, S_DETRATTO
    FROM {$cfg_mod_DeskAcq['file_testate_fatture']} TF
    LEFT OUTER JOIN (
	                SELECT SUM(RFTIMP) AS S_DETRATTO, RFDOCU
	                FROM {$cfg_mod_DeskAcq['file_righe_fatture']} RF0
	           		GROUP BY RFDOCU
	            ) RF
	           ON TFDOCU = RF.RFDOCU
    WHERE TFDT='{$id_ditta_default}' AND TFCCON='{$fornitore}' AND TFFU02='A' 
    AND TFFG01 <>'C' AND TFFG01 <> 'M'
    ";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);

    
    while($row = db2_fetch_assoc($stmt)){
        $nr = array();
        $nr['fattura']	= implode('_', array($row['TFAADO'], $row['TFNRDO'], $row['TFTPDO']));
        $nr['data']	    = trim($row['TFDTRG']);
        $nr['importo']	= $row['TFINFI'] + $row['S_DETRATTO'];
        $nr['k_ordine_fatt']	= implode("_", array($row['TFDT'], $row['TFTIDO'], $row['TFINUM'], $row['TFAADO'], $row['TFNRDO']));
        
        
        $ar[] = $nr;
        
    }
    
    echo acs_je($ar);
    exit;
    
}

if ($_REQUEST['fn'] == 'open_tab'){
    $m_params = acs_m_params_json_decode();
    
    ?>
 
 {"success":true, "items": [
 
         {
             xtype: 'panel',
             title: '',
             autoScroll: true, 
             layout: 'fit',
 			  items: [

 			      {
 		            xtype: 'form',
 		            //id: 'm_form',
 		            bodyStyle: 'padding: 10px',
 		            bodyPadding: '5 5 0',
 		            frame: false,
 		            title: '',
 		            buttons: [  {
 			            text: 'Conferma',
 			            iconCls: 'icon-button_blue_play-32',
 			            scale: 'large',
 			               handler: function() {
 			               
 			               var rows = this.up('form').down('grid').getSelectionModel().getSelection();
 			               list_righe_selected = [];
                           for (var i=0; i<rows.length; i++) {
				               list_righe_selected.push({
				            	   fattura: rows[i].get('k_ordine_fatt'),
				            	   importo: rows[i].get('importo')
				            });
				            }
 			               
 			               var loc_win = this.up('window');
 			              				
 							
 							 Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_detrai_anticipo',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    list_righe_selected: list_righe_selected,
 			        			    k_ord: '<?php echo $m_params->k_ord; ?>'
 								},							        
 						        success : function(result, request){
 			            			loc_win.fireEvent('afterDetrai', loc_win);	
 			            		 
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
 						
 			
 			            }
 			         }],   		            
 		            
 		            items: [   {
            		xtype: 'grid',
            		height: 100,
            		name: 'f_ordini',
            	    loadMask: true,	
            		multiSelect : true,
            		plugins: [
		            Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		               listeners: {
		              'edit': function(editor, e, opts) {
		              
		              	if (e.value > e.originalValue){
		              		acs_show_msg_error('Il valore modificato non deve essere superiore all\'importo iniziale');
		              	    e.record.set(e.field, e.originalValue);
		              	    return false;
		              	}
		              
		              }
		               
		            	}
		          	})
		     		 ],
            		store: {
 						xtype: 'store',
 						autoLoad:true,
 			
 	  							proxy: {
 								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fattura', 
 								   method: 'POST',								
 								   type: 'ajax',
 
 							       actionMethods: {
 							          read: 'POST'
 							        },
 							        
 							        
 							           extraParams: {
 										 open_request: <?php echo acs_raw_post_data(); ?>
 									
 			        				},
 			        				
 			        				doRequest: personalizza_extraParams_to_jsonData, 
 						
 								   reader: {
 						            type: 'json',
 									method: 'POST',						            
 						            root: 'root'						            
 						        }
 							},
 							
 		        			fields: ['fattura', 'data', 'importo', 'k_ordine_fatt']							
 									
 			}, //store
 				
 
 			      columns: [	
 			      {
 	                header   : 'Fattura',
 	                dataIndex: 'fattura',
 	                flex: 1
 	              
 	            },
 				{
 	                header   : 'Data',
 	                dataIndex: 'data',
 	                flex: 1,
 	                renderer: date_from_AS,
 	            },
 	           {
 	                header   : 'Importo',
 	                dataIndex: 'importo',
 	                flex: 1,
 	                renderer: floatRenderer2,
                    editor: {
		                xtype: 'numberfield',
		                allowBlank: true
		            }
 	            }, 
 	            
 	            
 	            
 	        
 	         ] ,		
 	         
 	         		listeners: {	
 					
 					},
 	   		
 			
 		} //fine grid			 
 		         	   
 					
 					 
 										 
 		           ]
 		              }	
 		              
 		            
 			  
 			  ] //ITEM panel
 			   
 		}
 	]
 }	
 	
 		
 <?php
 	exit;
 }