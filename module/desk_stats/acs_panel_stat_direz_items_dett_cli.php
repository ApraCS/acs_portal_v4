<?php


function crt_item_grid_dett_cli_menu($p = array()){
	$ret = "
        {
            xtype: 'panel',
			height: 170,
            bodyStyle: 'padding: 10px',
            bodyPadding: '0 5 0',
            frame: true,
            title: 'Filtri / Opzioni',
			cls: 'acs-light',	 			
			collapsible: true,

	        layout: {
			    type: 'hbox',
			    align: 'stretch',
			    pack : 'start',
			},			

            items: [
		
					" . implode(",", array(
							crt_item_grid_dett_cli_menu_anno_mese(),
							crt_item_grid_dett_cli_menu_filtri()
							)) . " 
			]

        }";
	return $ret;
}







function crt_item_grid_dett_cli_menu_filtri($p = array()){
	
	$m_params = acs_m_params_json_decode();
	
	$ret = "
        {
            xtype: 'panel',
			height: 100, flex: 1,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Filtri applicati',
            url: 'acs_op_exe.php',

            items: [
				{
					xtype: 'container',
					html: \"" . get_out_m_filter($m_params->main_filtri) . "\"}		
			]

        }";
	return $ret;
}








function crt_item_grid_dett_cli_menu_anno_mese($p = array()){
	
	$m_params = acs_m_params_json_decode();
	
	$mese_from 	= 1;
	$mese_to	= 12;
	$group_by   = 'gr_st_stccon';
	
	if (isset($m_params->mese_from))
		$mese_from = (int)$m_params->mese_from;
	if (isset($m_params->mese_to))
		$mese_to = (int)$m_params->mese_to;
	
	if (isset($m_params->group_by))
		$group_by = $m_params->group_by;	
	
	$ret = " 
        {
            xtype: 'form',
			width: 300, 
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Selezione opzioni',
            url: 'acs_op_exe.php',
            
            items: [			
			
					{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						fieldLabel: 'Anno / mese',
									
						items: [			
								{name: 'anno', 	xtype: 'textfield', hideLabel: true, width: 40},
								{xtype: 'label', width: 10, text: ' '},			
								{name: 'anno_p',xtype: 'textfield', hideLabel: true, width: 40},
								{xtype: 'label', width: 10, text: ' '},			
								{			
										xtype: 'slider',
										flex: 1,
										name: 'sl_mesi',
										fieldLabel: 'Mesi',
								        hideLabel: true,
										labelAlign: 'right',
										//width: '100%',
										labelWidth: 50,
								        minValue: 1,
								        maxValue: 12,
								        values: [" . $mese_from . ", " . $mese_to . "]
								 }			
			
						]
					}			
			
					        		
					, {
							name: 'f_group_by',
							flex: 1,
							xtype: 'combo',
							fieldLabel: 'Raggruppa per',
							displayField: 'text',
							valueField: 'id',
							anchor: '0',
							forceSelection:true,
						   	allowBlank: false,
					       	value: " . j($group_by) . ",														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
					        		 {id: 'gr_st_stdivi', text: 'Divisione'},
								     {id: 'gr_st_stmerc', text: 'Mercato'},
					        		 {id: 'gr_st_starma', text: 'Area manager'},
					        		 {id: 'gr_st_stcag1', text: 'Agente'},
					       			 {id: 'gr_st_stccon', text: 'Cliente'},
					        		 {id: 'gr_st_stzona', text: 'Zona'},
									 {id: 'gr_st_stnazi', text: 'Nazione'}					        		
								    ] 
								}						 
							}					        		
					        		
					        		
			],
			buttons: [{
	            text: 'Applica',
	            handler: function() {
	            	var form = this.up('form').getForm();
						
					m_grid = this.up('panel').up('panel').up('panel').down('grid');			
					m_grid.store.proxy.extraParams.anno = form.findField('anno').getValue();			
					m_grid.store.proxy.extraParams.anno_p = form.findField('anno_p').getValue();
			
					//mesi
					m_grid.store.proxy.extraParams.mese_from = form.findField('sl_mesi').getValues()[0];
					m_grid.store.proxy.extraParams.mese_to 	 = form.findField('sl_mesi').getValues()[1];
					       			
					m_grid.store.proxy.extraParams.group_by  = form.findField('f_group_by').getValue();					       			
			
					m_grid.store.load();
				    }            	                	                
	            }
	        ]  

				
        }";
	return $ret;
}





function crt_item_grid_dett_cli($p = array()){
 $ret = "
	{
		xtype: 'gridpanel',
		cls: 'stat-dett_cli',
		multiSelect: true,
		//selType: 'cellmodel',
		flex: 1,
		
		store: new Ext.data.Store({
	
			autoLoad:true,
			proxy: {
				url: '{$p['store_proxy_url']}',
				type: 'ajax',
				reader: {
					type: 'json',
					root: 'root'
				}
				
				, actionMethods: {
					read: 'POST'
				}				
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				, doRequest: personalizza_extraParams_to_jsonData				
			},
			fields: ['COD', 'DES', 'IMP', 'POS', 'INC', 'INC_PR', 'IMP_P', 'INC_P', 'SC_V', 'SC_PC'],
			
			listeners: {
				load: function(store, records, successful, operation, eOpts) {
					var me = this;				
                    
/*                    
					me.each(function(record) { 
						record.set('SC_V', parseFloat(record.get('IMP')) - parseFloat(record.get('IMP_P')));
						
						if (record.get('IMP_P') > 0)
							record.set('SC_PC', record.get('SC_V')/record.get('IMP_P'));
						
						record.commit(); //altrimenti rimane il flag 'modificato'
					},this);
*/					
					                    
					
				}			
			},			
			
		}),
		columns: [
			  {header   : 'Codice', dataIndex: 'COD', 			 width    : 100}			  
			, {header   : 'Denominazione', 	dataIndex: 'DES', 	 flex     : 1}
			, {header   : 'Importo', 		dataIndex: 'IMP', 	 width    : 100, align: 'right', renderer: floatRenderer2}
			, {header   : 'Pos.', 			dataIndex: 'POS', 	 width    : 37,  align: 'right'}
			, {header   : '% Incidenza', 	dataIndex: 'INC', 	 width    : 100, align: 'right'}
			, {header   : '% Inc. Prog.', 	dataIndex: 'INC_PR', width    : 100, align: 'right'}
			, {header   : 'Importo (ap)', 	dataIndex: 'IMP_P',	 width    : 100, align: 'right', renderer: floatRenderer2}
			, {header   : 'Incidenza (ap)', dataIndex: 'INC_P',	 width    : 100, align: 'right', renderer: floatRenderer2}
			, {header   : 'Scost. (v)',dataIndex: 'SC_V',	 width    : 100, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){
				m_v = parseFloat(record.get('IMP')) - parseFloat(record.get('IMP_P'));
				return this.renderer_cell_confronto(m_v, metaData, record, row, col, store, gridView,floatRenderer2(m_v))}
			} 
			, {header   : 'Scost. (%)',dataIndex: 'SC_PC',	 width    : 100, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){
					if (record.get('IMP_P') > 0)
						m_v =  (parseFloat(record.get('IMP')) - parseFloat(record.get('IMP_P')))/record.get('IMP_P');											
				return this.renderer_cell_confronto(m_v, metaData, record, row, col, store, gridView, perc1(m_v))}
			}
		],
		
		
		renderer_cell_confronto: function (value, metaData, record, row, col, store, gridView, value_formatted){
  					if (value > 0)
  	  				 metaData.tdCls += ' cell_confronto cell_confronto_pos';
					if (value < 0)
  	  				 metaData.tdCls += ' cell_confronto cell_confronto_neg';  	  				 											
  				return value_formatted;			    
		},
		
		
		viewConfig: {

		         trackOver: false		

		
		       , getRowClass: function(record, index) {
		           v = 'stat_direz_row_dett_cli';
		           if (record.get('TIPODOC') == 'TOT')
		            v = v + ' TOT';
		           return v;																
		         }
		          


				, listeners: {		        	
					  cellcontextmenu : function(view, cell, cellIndex, record, row, rowIndex, event) {
					    event.preventDefault();
					    event.stopEvent();
					  	
					    var voci_menu = [];
						  
						var menu = new Ext.menu.Menu({items: voci_menu}).showAt(event.xy);						  						  
					  	return false;
					  } //cellcontextmenu
				}		         
				
		    } //viewConfig


		    
		, listeners: {
			afterrender: function (comp) {
				//nel proxy copio i parametri impostati sul panel (in apertura)
				comp.store.proxy.extraParams = comp.up('panel').config_p;
				
				//carico la form in base ai parametri
				comp.up('panel').down('form').getForm().loadRecord({data: comp.up('panel').config_p});
			}		
		}    
 
		
		
	}";
 return $ret;
}



function get_json_data_grid_dett_cli(){
	global $conn, $cfg_mod_DeskStats;
	
	$ret = array();
	$ar  = array();

	$m_params = acs_m_params_json_decode();
	
	
	$mese_from 	= 1;
	$mese_to	= 12;
	$group_by	= 'gr_st_stccon';
	
	if (isset($m_params->mese_from))
		$mese_from = (int)$m_params->mese_from;
	if (isset($m_params->mese_to))
		$mese_to = (int)$m_params->mese_to;
	if (isset($m_params->group_by))
		$group_by = $m_params->group_by;	
		
	
	
	$where_m_filter = get_where_m_filter($m_params->main_filtri);


	//da group_by recupero campo codice e descrizione
	//struttura nome della colonna (es: fl_st_stdivi)
	$ar_nome_field = explode("_", $group_by);
	$n_field = $ar_nome_field[2];

	$campo_cod 			= $n_field;
	$campo_des 			= get_desc_field($n_field);
	
	//ToDo
	$m_field_importo	= 'sttimp';
	
	//recupero l'elenco dei clienti/agenti, ...
	$sql = "SELECT staarg as ANNO, {$campo_cod} as COD, {$campo_des} as DES, sum({$m_field_importo}) as tot_importo
			 FROM {$cfg_mod_DeskStats['file_testate_ven']}
			 WHERE STAREA = 'FATTURATO' AND {$where_m_filter}
			 AND STAARG IN({$m_params->anno}, {$m_params->anno_p})
			 AND (STMMRG >= {$mese_from} AND STMMRG <= {$mese_to})
			 GROUP BY staarg, {$campo_cod}, {$campo_des}
			 ORDER BY ANNO DESC, TOT_IMPORTO DESC 
			";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);	
	  
			$pos = 0;
			while ($row = db2_fetch_assoc($stmt)){
				
				$k_rec = to_cod($row['COD']);

				if (is_null($ar[$k_rec])){
					$ar[$k_rec] = array(
							'POS' => ++$pos,
							'COD' => trim($row['COD']),
							'DES' => trim(acs_u8e($row['DES']))
					);
				}				
				
				$s_ar = &$ar[$k_rec];
				
				if ($row['ANNO'] == $m_params->anno){
					$s_ar['IMP'] += $row['TOT_IMPORTO'];
				}
				if ($row['ANNO'] == $m_params->anno_p){
					$s_ar['IMP_P'] += $row['TOT_IMPORTO'];
				}				
				
				
			}
			
			$c = 0;
			foreach ($ar as $ar_r){
				if (++$c > 100) break;
				$ret[] = $ar_r;
			}
			
		
	 return acs_je($ret);	
}

?>