<?php



function get_desc_field($cod_field){
	switch ($cod_field){
		case "stdivi":
			return 'stdivi'; break;
		case "stmerc":
			return 'stdmer'; break;
		case "starma":
			return 'stdarm'; break;
			break;
		case "stcag1":
			return 'stdag1'; break;
		case "stzona":
			return 'stdzon'; break;
		case "stnazi":
			return 'stdnaz'; break;
		case "stccon":
			return 'stdcon'; break;
	}
}

function crt_main_left_menu(){
	$ret = "";
	
	$ret .= "
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Filtri',
			collapsible: true,
            url: 'acs_op_exe.php',
			
			layout: {
			    type: 'hbox',
		    	align: 'stretch',
		    	pack : 'start',
			}, 
	
            items: [
				" . implode(", ", array(
						 
						m_std_create_combo_ajax(array(
							'name'			=> 'fl_st_stdivi',
							'fieldLabel'	=> 'Divisione',
							'proxy_url'		=>  "_get_data.php?fn=m_std_create_store_ajax",
							'extraParams'	=> array(
									'field'	=> 'stdivi'
							)
						))
						
					, m_std_create_combo_ajax(array(
								'name'			=> 'fl_st_stmerc',
								'fieldLabel'	=> 'Mercato',
								'proxy_url'		=>  "_get_data.php?fn=m_std_create_store_ajax",
								'extraParams'	=> array(
										'field'	=> 'stmerc'
								)
						))

					, m_std_create_combo_ajax(array(
								'name'			=> 'fl_st_starma',
								'fieldLabel'	=> 'Area manager',
								'proxy_url'		=>  "_get_data.php?fn=m_std_create_store_ajax",
								'extraParams'	=> array(
										'field'	=> 'starma'
								)
						))
						
					, m_std_create_combo_ajax(array(
								'name'			=> 'fl_st_stcag1',
								'fieldLabel'	=> 'Agente',
								'proxy_url'		=>  "_get_data.php?fn=m_std_create_store_ajax",
								'extraParams'	=> array(
										'field'	=> 'stcag1'
								)
						))
						
					, m_std_create_combo_ajax(array(
								'name'			=> 'fl_st_stzona',
								'fieldLabel'	=> 'Zona',
								'proxy_url'		=>  "_get_data.php?fn=m_std_create_store_ajax",
								'extraParams'	=> array(
										'field'	=> 'stzona'
								)
						))
						
					, m_std_create_combo_ajax(array(
							'name'			=> 'fl_st_stnazi',
							'fieldLabel'	=> 'Nazione',
							'proxy_url'		=>  "_get_data.php?fn=m_std_create_store_ajax",
							'extraParams'	=> array(
									'field'	=> 'stnazi'
							)
					))
						
						
						
					, "{
							xtype: 'combo',
							name: 'fl_st_stccon',
							fieldLabel: 'Cliente',
							minChars: 2,
							labelAlign: 'top',
						
				            store: {
				            	pageSize: 1000,
				            	
								proxy: {
						            type: 'ajax',
						            url : '_get_data.php?fn=m_std_create_store_ajax',
									extraParams: {field: 'stccon'},				            
				            
						            reader: {
						                type: 'json',
						                root: 'options',
						                totalProperty: 'totalCount'
						            },
						
									actionMethods: {
										read: 'POST'					
									},		    
				
									doRequest: personalizza_extraParams_to_jsonData						
						
						        },       
								fields: ['id', 'text', 'stdloc'],		             	
				            },					
							
							valueField: 'id',
							displayField: 'text',
							typeAhead: false,
							hideTrigger: true,
							anchor: '100%',
						
							listeners: {
								change: function(field,newVal) {
									var form = this.up('form').getForm();
								}
							},
						
							listConfig: {
								loadingText: 'Searching...',
								emptyText: 'Nessun cliente trovato',
						
						
								// Custom rendering template for each item
								getInnerTpl: function() {
									return '<div class=\"search-item\">' +
									'<h3><span>{text}</span></h3>' +
									'[{id}] {stdloc}' +
									'</div>';
								}
						
							},
						
							pageSize: 1000
						
						}"						
						
						
						
				)) . "
			],
						
						
		 dockedItems: [	
		   {
		     xtype: 'toolbar',
		     dock: 'right',
		     title: '',
		     layout: {
		      pack: 'center',
		      type: 'hbox'
		     },
		     items: [
				{
	            text: 'Applica',
	            iconCls: 'icon-windows-32',
	            scale: 'large',						
	            handler: function() {
	            	var form = this.up('form').getForm();
						mp = Ext.getCmp('panel-stat-direz');
						mp.m_filters = form.getValues();
						mp.refresh_all();
				    }
	            }												
			]
		 }
		]			
						
						
						
						
						
						
	
	
        }";
	return $ret;		
} //crt_main_left_menu



function m_std_create_store_ajax($p){
	$ret = "";
	$ret .= "
		            {
		                autoLoad: false,
						proxy: {
				            type: 'ajax',
				            url : " . j($p['proxy_url']) . ",
				            
		                	extraParams: " . acs_je($p['extraParams']) . ",				            
				            
				            reader: {
				                type: 'json',
		                		method: 'POST',
				                root: 'options'
				            }, 
		                			
							actionMethods: {
								read: 'POST'					
							},		    

							doRequest: personalizza_extraParams_to_jsonData			                			
		                			
				        },       
						fields: ['id', 'text']		             	
		            }			
	";
 return $ret;
}


function m_std_create_combo_ajax($p){
  if (strlen($p['labelAlign']) == 0) $p['labelAlign'] = 'top';
  $ret = "";
  $ret .="	  
				 {
		            xtype: 'combo',
					name: " . j($p['name']) . ",
					fieldLabel: " . j($p['fieldLabel']) . ",
					labelAlign: " . j($p['labelAlign']) . ",
		            store: " . m_std_create_store_ajax($p) . ",
		            value: " . j($p['value']) . ",
					valueField: 'id',
		            displayField: 'text',
		            forceSelection:true,
		            anchor: '100%',

			        listeners: {
			        }            

		        }
 ";
  return $ret;
}



function get_where_m_filter($m_params){
	$ret = " 1=1 ";
	
	foreach ($m_params as $k_field=>$v_field){
		
		if (count($v_field) == 1 && strlen(trim($v_field)) == 0) continue;
		
		//struttura nome della colonna (es: fl_st_stdivi)
		$ar_nome_field = explode("_", $k_field);
		$n_field = $ar_nome_field[2];
		
		if (count($v_field) == 1)
			$ret .= " AND {$n_field} = " . sql_t($v_field);
		else
			$ret .= " AND {$n_field} IN (" . sql_t_IN($v_field) . ")";
	} 
	
	return $ret;
}


function get_out_m_filter($m_params){
	$ar_ret = array();

	foreach ($m_params as $k_field=>$v_field){

		if (count($v_field) == 1 && strlen(trim($v_field)) == 0) continue;

		//struttura nome della colonna (es: fl_st_stdivi)
		$ar_nome_field = explode("_", $k_field);
		$n_field = $ar_nome_field[2];

		if (count($v_field) == 1)
			$ar_ret[] = "{$n_field} = " . sql_t($v_field);
		else
			$ar_ret[] = "{$n_field} IN (" . sql_t_IN($v_field) . ")";
	}

	return implode("<br>", $ar_ret);
}



?>