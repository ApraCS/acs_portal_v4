<?php 
	require_once("../../config.inc.php");
	require_once("./_utility.php");	
	$_module_descr = "Cruscotto Vendite";
			
	$main_module = new DeskStats();
	
	$users = new Users;
	$ar_users = $users->find_all();
	
/*	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
	}
*/	
	
	$ar_email_json = acs_je($ar_email_to);	
		
?>



<html>
<head>
<title>ACS Portal_Stats</title>
<link rel="stylesheet" type="text/css" href="../../extjs/resources/css/ext-all.css" />

<!-- <link rel="stylesheet" type="text/css" href="resources/css/calendar.css" /> -->
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/extensible-all.css" />
<link rel="stylesheet" type="text/css" href="../../js/extensible-1.5.2/resources/css/calendar.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">
 /* REGOLE RIORDINO */
 .x-grid-cell.lu{background-color: #dadada;} 
 .x-grid-cell.ma{background-color: #e5e5e5;} 
 .x-grid-cell.me{background-color: #eeeeee;} 
 .x-grid-cell.gi{background-color: #FFFFFF;} 
 .x-grid-cell.ve{background-color: #F4EDAF;} 
 .x-grid-cell.sa{background-color: #FFFFFF;}
 
 .x-grid-cell.attivi{background-color: #A0DB8E;}
 .x-grid-cell.elaborazione-old-1{background-color: #e5e5e5;} 
 .x-grid-cell.elaborazione-old-2{background-color: #F9BFC1;} 

.x-panel td.x-grid-cell.festivo{opacity: 0.6;}

.x-panel td.x-grid-cell.con_carico_1{background-color: #909090; color: white; font-weight: bold;} /* SOLO ALCUNI HANNO IL CARICO */
.x-panel td.x-grid-cell.con_carico_2{background-color: #3c3c3c; color: white; font-weight: bold;}  /* HANNO TUTTI IL CARICO */
.x-panel td.x-grid-cell.con_carico_3xxx{background-color: #F9827F; font-weight: bold;}  /* NON CARICO. CON DATE CONF. */
.x-panel td.x-grid-cell.con_carico_4xxx{background-color: #F9BFC1; font-weight: bold;}  /* NON CARICO. CON DATE TASS. */
.x-panel td.x-grid-cell.con_carico_5xxx{background-color: #CEEA82; font-weight: bold;}  /* ALCUNI EVASI */
.x-panel td.x-grid-cell.con_carico_6{background-color: #8CD600; font-weight: bold;}  /* TUTTI EVASI */


.x-panel .x-column-header-text{font-weight: bold;}


tr.liv_totale td.x-grid-cell{background-color: #F4EDAF; border: 1px solid white;}
tr.liv_totale td.x-grid-cell.festivo{opacity: 0.6;}
tr.liv_0 td.x-grid-cell{border: 1px solid white; font-weight: bold;}
tr.liv_1 td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: bold;}
tr.liv_1_no_b td.x-grid-cell{background-color: #AFBCDB; border: 1px solid white; font-weight: normal;}
tr.liv_1.no_carico td.x-grid-cell{background-color: #C4D8E2}

.x-panel.elenco_ordini tr.liv_1 td.x-grid-cell{font-weight: normal;}
.x-panel.elenco_ordini tr.liv_2 td.x-grid-cell{font-weight: bold;}

tr.liv_totale.dett_selezionato td.x-grid-cell{background-color: #F4ED7C; font-weight: bold;}

/*TIPO ORDINE (raggr)*/
.x-grid-cell.tipoOrd.O {background-color: #93C6E0;} /* mostre */
.x-grid-cell.tipoOrd.S {background-color: #D3BFB7;} /* materiale promozionale */
.x-grid-cell.tipoOrd.R {background-color: #E2D67C;} /* composizioni promozionali */
.x-grid-cell.tipoOrd.L {background-color: white;}   /* assistenze */


/*RIGHE ORDINE*/
.x-grid-row.rigaRevocata .x-grid-cell{background-color: #F9BFC1}
.x-grid-row.rigaChiusa .x-grid-cell{background-color: #CEEA82}
.x-grid-row.rigaParziale .x-grid-cell{background-color: #F4EDAF}
.x-grid-row.rigaNonSelezionata .x-grid-cell{background-color: #cccccc}


/*TIPO PRIORITA*/
 .x-grid-cell.tpSfondoRosa{background-color: #F9BFC1;}
 .x-grid-cell.tpSfondoCelesteEl{background-color: #99D6DD;}


/*ICONE*/
.iconSpedizione {background-image: url(<?php echo img_path("icone/16x16/spedizione.png") ?>) !important;}
 
/* celle classe ABC */
.x-panel.x-table-index .x-panel-body {
		text-align: center;
    	background-color: #D3D3D3;
		font-size: 18;
		/*font-weight: bold;*/
}
 .x-panel.x-table-index.leg .x-panel-body {		
		font-size: 14;
}
.x-panel.x-table-index .x-panel-body span {font-size: 10px;}
 
/* bordi laterali tabella non visualizzabili */
.table-border {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	text-align: right;
}
.table-border-final {
	border-bottom: 1px solid #99BBE8;
	border-top: 1px solid #99BBE8;
	border-right: 1px solid #99BBE8;
	text-align: right;
}

a.view_dett_class_gc{text-decoration: none;}
 

.x-toolbar .strong{font-weight: bold;}

.x-grid-cell-inner p.sottoriga-liv_2{text-indent: 70px}



/* STATISTICHE DIREZIONALI */
tr.stat_direz_row td.x-grid-cell{background-color: #ffff99; border: 1px solid white; padding-top: 4px; padding-bottom: 4px;}
tr.stat_direz_row.TOT td.x-grid-cell{background-color: #AFBCDB; font-weight: bold;}
tr.stat_direz_row.TOT_PROG td.x-grid-cell{background-color: #E5C4D6; font-weight: normal;}
tr.stat_direz_row.HEADER td.x-grid-cell{background-color: white; font-weight: normal; font-size: 10px; padding: 0px; border: 0px;}
tr.stat_direz_row.HEADER td.x-grid-cell.c_TOT{border: 1px dotted #99bbe8; font-weight: bold;}
tr.stat_direz_row td.x-grid-cell.c_TOT{font-weight: bold;}

tr.stat_direz_row.CONFRONTO_PERC td.x-grid-cell{background-color: #C4D6D6; font-weight: normal;}

tr.stat_direz_row td.x-grid-cell.c_TOT{border-left: 1px dotted #99bbe8; border-right: 1px dotted #99bbe8;}

 /* intestazione group */
tr.stat_direz_row td.x-grid-cell{border: 0px; border-bottom: 1px solid white;}
.x-panel.x-grid.stat-main .x-grid-group-hd .x-grid-cell-inner{padding-top: 13px; padding-bottom: 0px; font-size: 1.0em; font-style: bold; border-width: 0px;}
.x-panel.x-grid.stat-main .x-grid-group-hd .x-grid-cell-inner .x-grid-group-title{font-size: 0.8em; font-style: bold;}

.x-grid-cell.cell_confronto.cell_confronto_pos {color: green;}
.x-grid-cell.cell_confronto.cell_confronto_neg {color: red;}


.x-panel.x-grid.stat-dett_cli .x-grid-row .x-grid-cell .x-grid-cell-inner{padding-top: 8px; padding-bottom: 8px;}

.x-grid-cell.cell_confronto_perc table.double_cell{width: 100%; padding: 0px; margin: 0px;}
.x-grid-cell.cell_confronto_perc table.double_cell td{width: 50%; border: 0px; text-align: right; padding: 0px;} 


</style>

<script type="text/javascript" src="../../extjs/ext-all.js"></script>

<script type="text/javascript" src="../../js/extensible-1.5.2/lib/extensible-all-debug.js"></script>
<script type="text/javascript" src="../../js/extensible-1.5.2/src/locale/extensible-lang-it.js"></script>


<script type="text/javascript" src="../../extjs/locale/ext-lang-it.js"></script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src=<?php echo acs_url("js/gmaps.js") ?>></script>
<script src=<?php echo acs_url("js/acs_js.js") ?>></script>



<script type="text/javascript">

	function allegatiPopup(apri)
	{
	  gmap_poup_stile = "top=10, left=10, width=600, height=600, status=no, menubar=no, toolbar=no scrollbars=no";		
	  window.open(apri, "", gmap_poup_stile);
	}	


	var refresh_stato_aggiornamento = {
        run: doRefreshStatoAggiornamento,
        interval: 30 * 1000 //millisecondi
      }

	function doRefreshStatoAggiornamento() {
        Ext.Ajax.request({
            url : 'get_stato_aggiornamento.php' ,
            method: 'GET',
            success: function ( result, request) {
                var jsonData = Ext.decode(result.responseText);
                var btStatoAggiornamento = document.getElementById('bt-stato-aggiornamento-esito');
                btStatoAggiornamento.innerHTML = jsonData.html;
            },
            failure: function ( result, request) {
                console.log('failed');
            }
        });
	}


	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {
            //'Ext.calendar': 'src',
	        "Extensible": "../../js/extensible-1.5.2/src", 
			"Extensible.example": "../../js/extensible-1.5.2/examples"	                   
        }	    
	});Ext.Loader.setPath('Ext.ux', '../../ux');


    Ext.require(['*'
                 , 'Ext.ux.grid.FiltersFeature',                 
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
                 , 'Extensible.calendar.data.MemoryEventStore'
                 , 'Extensible.calendar.CalendarPanel'  
                 , 'Extensible.example.calendar.data.Events'
				 , 'Extensible.calendar.data.CalendarMappings',
    			 , 'Extensible.calendar.data.EventMappings'                 
                 ]);

//    ,
//    'Ext.calendar.data.MemoryCalendarStore',
//    'Ext.calendar.data.MemoryEventStore',
//    'Ext.calendar.data.Events',
//    'Ext.calendar.data.Calendars'	  




    

    Ext.define('ModelOrdini', {
        extend: 'Ext.data.Model',
        fields: [
        			{name: 'task'}, {name: 'n_carico'}, {name: 'k_carico'}, {name: 'sped_id'}, {name: 'cod_iti'},
        			{name: 'k_ordine'}, {name: 'prog'}, {name: 'n_O'}, {name: 'n_M'}, {name: 'n_P'}, {name: 'n_CAV'},
        			{name: 'liv'},
					{name: 'cliente'}, {name: 'k_cli_des'}, {name: 'gmap_ind'}, {name: 'scarico_intermedio'},
					{name: 'nr'},
					{name: 'volume'},{name: 'pallet'},{name: 'peso'},
					{name: 'volume_disp'},{name: 'pallet_disp'},{name: 'peso_disp'},
					{name: 'colli'},{name: 'colli_disp'},{name: 'colli_sped'},{name: 'data_disp'}, {name: 'data_cons_cli'}, {name: 'data_conf_ord'}, {name: 'fl_data_disp'},
					{name: 'riferimento'},
					{name: 'tipo'},{name: 'raggr'},
					{name: 'cons_rich'},					
					{name: 'priorita'},{name: 'tp_pri'},
					{name: 'seq_carico'},					
					{name: 'stato'},
					{name: 'cons_conf'},{name: 'cons_prog'},{name: 'ind_modif'},
					{name: 'data_reg'},{name: 'data_scadenza'},					
					{name: 'importo'}, {name: 'referenze'}, {name: 'referenze_cons'},
					{name: 'fl_evaso'}, {name: 'fl_ref'}, {name: 'fl_bloc'}, {name: 'fl_art_manc'}, {name: 'fl_da_prog'}, {name: 'art_da_prog'}, {name: 'fl_new'}
        ]
    });


    Ext.define('RowCalendar', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task',     type: 'string'},
            {name: 'liv',      type: 'string'},            
            {name: 'flag1',    type: 'string'},            
            {name: 'user',     type: 'string'},
            {name: 'duration', type: 'string'},
            {name: 'done',     type: 'boolean'},
            {name: 'da_data',  type: 'float'}, {name: 'itin'}, {name: 'sped_id'}, {name: 'k_cliente'},            
            {name: 'd_1'}, {name: 'd_1_f'}, {name: 'd_1_t'}, {name: 'd_1_d'},
            {name: 'd_2'}, {name: 'd_2_f'}, {name: 'd_2_t'}, {name: 'd_2_d'},
            {name: 'd_3'}, {name: 'd_3_f'}, {name: 'd_3_t'}, {name: 'd_3_d'},
            {name: 'd_4'}, {name: 'd_4_f'}, {name: 'd_4_t'}, {name: 'd_4_d'},
            {name: 'd_5'}, {name: 'd_5_f'}, {name: 'd_5_t'}, {name: 'd_5_d'},
            {name: 'd_6'}, {name: 'd_6_f'}, {name: 'd_6_t'}, {name: 'd_6_d'},
            {name: 'd_7'}, {name: 'd_7_f'}, {name: 'd_7_t'}, {name: 'd_7_d'},
            {name: 'd_8'}, {name: 'd_8_f'}, {name: 'd_8_t'}, {name: 'd_8_d'},
            {name: 'd_9'}, {name: 'd_9_f'}, {name: 'd_9_t'}, {name: 'd_9_d'},
            {name: 'd_10'}, {name: 'd_10_f'}, {name: 'd_10_t'}, {name: 'd_10_d'},
            {name: 'd_11'}, {name: 'd_11_f'}, {name: 'd_11_t'}, {name: 'd_11_d'},
            {name: 'd_12'}, {name: 'd_12_f'}, {name: 'd_12_t'}, {name: 'd_12_d'},
            {name: 'd_13'}, {name: 'd_13_f'}, {name: 'd_13_t'}, {name: 'd_13_d'},
            {name: 'd_14'}, {name: 'd_14_f'}, {name: 'd_14_t'}, {name: 'd_14_d'},
			{name: 'fl_bloc'}, {name: 'fl_ord_bloc'}, {name: 'fl_art_mancanti'}, {name: 'fl_da_prog'}, {name: 'ha_contratti'}, {name: 'ha_proposte_MTO'}                                   
        ]
    });    

    //Model per albero itinerari
    Ext.define('Task', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'task', type: 'string'}, {name: 'user', type: 'string'}, 'liv', 'sped_id', 'k_cliente', 'data'
        ]
    });    

    
	//TODO: da spostare in PLAN
	function getCellClass (val, meta, rec, rowIndex, colIndex, store) {
		idx = colIndex - 6; //colonna5 corrisponde a d_1
		 
		if (rec.get('d_' + idx + '_f') == 'F' || rec.get('d_' + idx + '_f') == 'S' || rec.get('d_' + idx + '_f') == 'D'){
			meta.tdCls += ' festivo';
		}
		if (rec.get('d_' + idx + '_t') == '1'){
			meta.tdCls += ' con_carico_1';
		}		
		if (rec.get('d_' + idx + '_t') == '2'){
			meta.tdCls += ' con_carico_2';
		}
		if (rec.get('d_' + idx + '_t') == '3'){
			meta.tdCls += ' con_carico_3';
		}		
		if (rec.get('d_' + idx + '_t') == '4'){
			meta.tdCls += ' con_carico_4';
		}		
		if (rec.get('d_' + idx + '_t') == '5'){
			meta.tdCls += ' con_carico_5';
		}
		if (rec.get('d_' + idx + '_t') == '6'){
			meta.tdCls += ' con_carico_6';
		}				
		
		if ((rec.get('liv') == 'liv_2' || rec.get('liv') == 'liv_3') && rec.get('d_' + idx).length > 0 && rec.get('d_' + idx + '_d').length > 0)		
			return val += "<span class=c_disp>/" + rec.get('d_' + idx + '_d') + "</span>";
		else
			return val;	
	}

    

    

    //PLAN ARRIVI
    function show_el_arrivi(){
    	mp = Ext.getCmp('m-panel');
    	arrivi = Ext.getCmp('panel-arrivi');

    	if (arrivi){
        	arrivi.store.reload();
        	arrivi.show();		    	
    	} else {

    		//carico la form dal json ricevuto da php
    		Ext.Ajax.request({
    		        url        : 'acs_arrivi_json_plan.php',
    		        method     : 'GET',
    		        waitMsg    : 'Data loading',
    		        success : function(result, request){
    		            var jsonData = Ext.decode(result.responseText);
    		            mp.add(jsonData.items);
    		            mp.doLayout();
    		            mp.show();
    		        },
    		        failure    : function(result, request){
    		            Ext.Msg.alert('Message', 'No data to be loaded');
    		        }
    		    });

    	}
    }



    //PANEL fo_presca
    function show_fo_presca(){
       	   acs_show_win_std('Prenotazione scarichi', 'acs_panel_fo_presca.php?fn=get_open_form', null, 530, 420);	
    }
    
    
    


    function show_win_righe_ord(ord, k_ord){
 	   acs_show_win_std('Righe ordine', <?php echo acs_url('module/desk_acq/acs_get_order_rows.php') ?>, {k_ord: k_ord, dtep: ord.get('cons_prog')}, 1150, 480);        
    }


    
    


    Ext.onReady(function() {

        Ext.QuickTips.init();






    Extensible.calendar.data.EventMappings = {
        // These are the same fields as defined in the standard EventRecord object but the
        // names and mappings have all been customized. Note that the name of each field
        // definition object (e.g., 'EventId') should NOT be changed for the default fields
        // as it is the key used to access the field data programmatically.
        EventId:     {name: 'EventId', mapping:'id', type:'int'}, // int by default
        CalendarId:  {name: 'CalendarId', mapping: 'cid', type: 'string'}, // int by default
        Title:       {name: 'Title', mapping: 'title'},
        StartDate:   {name: 'StartDate', mapping: 'start', type: 'date', dateFormat: 'c'},
        EndDate:     {name: 'EndDate', mapping: 'end', type: 'date', dateFormat: 'c'},
        RRule:       {name: 'RecurRule', mapping: 'rrule'},
        Location:    {name: 'Location', mapping: 'loc'},
        Notes:       {name: 'Notes', mapping: 'notes'},
        Url:         {name: 'Url', mapping: 'url'},
        IsAllDay:    {name: 'IsAllDay', mapping: 'ad', type: 'boolean'},
        Reminder:    {name: 'Reminder', mapping: 'rem'},        


        //Acs
        Risorsa: 	  {name:'Risorsa', mapping: 'risorsa', type: 'string'},
        Stabilimento: {name:'Stabilimento', mapping: 'stabilimento', type: 'string'},
        Trasp:		  {name:'Trasp', mapping: 'trasp', type: 'string'},
        Porta:    	  {name:'Porta', mapping: 'porta'},        
        
        
        // We can also add some new fields that do not exist in the standard EventRecord:
        CreatedBy:   {name: 'CreatedBy', mapping: 'created_by'},
        IsPrivate:   {name: 'Private', mapping:'private', type:'boolean'}
        
    };
    // Don't forget to reconfigure!
    Extensible.calendar.data.EventModel.reconfigure();






 /* PER DRAG & DROP **********/
    /*
     * Currently the calendar drop zone expects certain drag data to be present, so if dragging
     * from the grid, default the data as required for it to work
     */
    var nodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag1';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
        }
    };
    
    /*
     * Need to hook into the drop to provide a custom mapping between the existing grid
     * record being dropped and the new calendar record being added
     */
    var nodeDropInterceptor = function(n, dd, e, data){
        if(n && data){
            if(typeof(data.type) === 'undefined'){
                var rec = Ext.create('Extensible.calendar.data.EventModel'),
                    M = Extensible.calendar.data.EventMappings;
                
                // These are the fields passed from the grid's record
                //var gridData = data.selections[0].data;
                var gridData = data.records[0].data
                rec.data[M.Title.name] = 'salvataggio...';
                rec.data[M.CalendarId.name] = gridData.cid;
                
                // You need to provide whatever default date range logic might apply here:
                rec.data[M.StartDate.name] = n.date;
                rec.data[M.EndDate.name] = Extensible.Date.add(n.date, { hours: 1 });;
                rec.data[M.IsAllDay.name] = false;

                rec.data[M.CreatedBy.name] = gridData.TDNBOC;  //lo appoggio qui altrimenti non mi esegui il create               
                rec.data[M.Stabilimento.name] = gridData.TDSTAB;
                rec.data[M.Porta.name] = this.view.store.proxy.extraParams.f_porta;
                
                // save the evrnt and clean up the view
                ///////this.view.onEventDrop(rec, n.date);
                this.view.store.add(rec.data);                                                
                this.onCalendarDragComplete();
				this.view.fireEvent('eventadd');
                return false; //per non eseguire onDrop Normale
            }
        }
    };
    
    /*
     * Day/Week view require special logic when dragging over to create a 
     * drag shim sized to the event being dragged
     */
    var dayViewNodeOverInterceptor = function(n, dd, e, data){
        if (data.selections) {
            data.type = 'griddrag2';
            data.fromExternal = true;
            data.start = n.date;
            data.proxy = {
                updateMsg: Ext.emptyFn
            }
    
            var dayCol = e.getTarget('.ext-cal-day-col', 5, true);
            if (dayCol) {
                var box = {
                    height: Extensible.calendar.view.Day.prototype.hourHeight,
                    width: dayCol.getWidth(),
                    y: n.timeBox.y,
                    x: n.el.getLeft()
                }
                this.shim(n.date, box);
            }
        }
    };
    
    // Apply the interceptor functions to each class:
    var dropZoneProto = Extensible.calendar.dd.DropZone.prototype,
        dayDropZoneProto = Extensible.calendar.dd.DayDropZone.prototype 
    
    dropZoneProto.onNodeOver = Ext.Function.createInterceptor(dropZoneProto.onNodeOver, nodeOverInterceptor);
    dropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dropZoneProto.onNodeDrop, nodeDropInterceptor);
    
    dayDropZoneProto.onNodeOver = Ext.Function.createInterceptor(dayDropZoneProto.onNodeOver, dayViewNodeOverInterceptor);
    dayDropZoneProto.onNodeDrop = Ext.Function.createInterceptor(dayDropZoneProto.onNodeDrop, nodeDropInterceptor);



    
    
    /*
     * This is a simple override required for dropping from outside the calendar. By default it
     * assumes that any drag originated within itelf, so a drag would be a move of an existing event.
     * This is no longer the case and it must check to see what the record state is.
     */
    Extensible.calendar.view.AbstractCalendar.override({
        onEventDrop : function(rec, dt){
            if (rec.phantom) {
                this.onEventAdd(null, rec);
            }
            else {
                this.moveEvent(rec, dt);
            }
        }
    });    	


	//disabilito il menu sugli eventi
	Extensible.calendar.menu.Event.override({
		showForEvent: function(rec, el, xy){
				return false;
		}
	});
    

	//formato data negli header
	Extensible.calendar.template.BoxLayout.prototype.singleDayDateFormat = 'd / m';	

    
    //PER RIMUOVERE L'ORARIO NEGLI EVENTI
    Extensible.calendar.view.DayBody.override({
    getTemplateEventData : function(evt){
        var M = Extensible.calendar.data.EventMappings,
            extraClasses = [this.getEventSelectorCls(evt[M.EventId.name])],
            data = {},
            colorCls = 'x-cal-default',
            title = evt[M.Title.name],
            fmt = Extensible.Date.use24HourTime ? 'G:i ' : 'g:ia ',
            recurring = evt[M.RRule.name] != '';
        
        this.getTemplateEventBox(evt);
        
        if(this.calendarStore && evt[M.CalendarId.name]){
            var rec = this.calendarStore.findRecord(Extensible.calendar.data.CalendarMappings.CalendarId.name, 
                    evt[M.CalendarId.name]);
                
            if(rec){
                colorCls = 'x-cal-' + rec.data[Extensible.calendar.data.CalendarMappings.ColorId.name];
            }
        }
        colorCls += (evt._renderAsAllDay ? '-ad' : '') + (Ext.isIE || Ext.isOpera ? '-x' : '');
        extraClasses.push(colorCls);
        
        if(this.getEventClass){
            var rec = this.getEventRecord(evt[M.EventId.name]),
                cls = this.getEventClass(rec, !!evt._renderAsAllDay, data, this.store);
            extraClasses.push(cls);
        }
        
        data._extraCls = extraClasses.join(' ');
        data._isRecurring = evt.Recurrence && evt.Recurrence != '';
        data._isReminder = evt[M.Reminder.name] && evt[M.Reminder.name] != '';
        data.Title =  (!title || title.length == 0 ? this.defaultEventTitleText : title);
        return Ext.applyIf(data, evt);
    }    
    });
    
  











            

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            // create instance immediately
            Ext.create('Ext.Component', {
                region: 'north',
                id: 'page-header',
                height: 110, // give north and south regions a height
                contentEl: 'header'
            }),

            
            Ext.create('Ext.tab.Panel', {
            	id: 'm-panel',
        		cls: 'supply_desk_main_panel',            	            	
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 1,     // first tab initially active
                items: [
			                                   
                             					                                                
                ]
            })]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------
      	
    	//Ecommerce: gestione articoli
        if (Ext.get("bt-stat-direz") != null){
        	Ext.QuickTips.register({
    			target: "bt-stat-direz",
    			title: 'Statistiche sul venduto',
    			text: 'Ordinato, spedito, fatturato'
    		});
                    
	        Ext.get("bt-stat-direz").on('click', function(){
	        	acs_show_panel_std('acs_panel_stat_direz.php?fn=open_tab', 'panel-stat-direz');
	        });
        }            
    	

    	//Documentale
        if (Ext.get("bt-docum") != null){
        	Ext.QuickTips.register({
    			target: "bt-docum",
    			title: 'Archivio documenti',
    			text: 'Consultazione copia documenti'
    		});
                    
	        Ext.get("bt-docum").on('click', function(){
	        	//acs_show_panel_std('acs_panel_docum.php?fn=open_tab', 'panel-docum');
	        	acs_show_win_std('Consultazione documenti', 'acs_panel_docum.php?fn=open_tab', {}, null, null, {}, 'icon-blog_compose-16', 'Y');
	        });
        }            
        
      	
      	


    	     
		//---------------------------------------------------------------
		//carico gli indici
		//---------------------------------------------------------------
     		mp_indici = Ext.getCmp('west-panel');
            	

  		//schedulo la richiesta dell'ora ultimo aggiornamento
   		/////Ext.TaskManager.start(refresh_stato_aggiornamento);	
     		

        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
