<?php

require_once("../../config.inc.php");
require_once("./_utility.php");

if ($_REQUEST['fn'] == 'm_std_create_store_ajax') {echo get_m_std_create_store_ajax(); exit;}



function get_m_std_create_store_ajax(){
	$m_params = (array)acs_m_params_json_decode();

	switch ($m_params['field']){
		case "stccon":
			return m_get_options($m_params['field'], get_desc_field($m_params['field']), array('live_search'=>'Y', 'add_columns'=>array('STDLOC'))); break;
		default:
			return m_get_options($m_params['field'], get_desc_field($m_params['field'])); break;
	}
}



function m_get_options($f_id, $f_text, $p=array()){
	global $conn, $cfg_mod_DeskStats;
	
	
	//aggiungo colonne
	if (!isset($p['add_columns']))
			 $p['add_columns'] = array();
	
	if (count($p['add_columns']) > 0){
		$sql_add_column = ", " . implode(",", $p['add_columns']);
	} else
		$sql_add_column = '';
	
	//ricerca live (per clienti, ...)
	if ($p['live_search'] == 'Y'){
		$where_live_search = " AND UPPER(REPLACE(REPLACE({$f_text}, '.', ''), ' ', '')) 
								" . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE');
	} else
		$where_live_search = " AND 1=1 ";
	
	$ret = array('options' => array());

	$sql = "SELECT {$f_id} AS id, {$f_text} AS text {$sql_add_column}
			FROM {$cfg_mod_DeskStats['file_testate_ven']}
			WHERE 1=1 {$where_live_search}
			GROUP BY {$f_id}, {$f_text} {$sql_add_column}
			ORDER BY {$f_text}
			";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	 
	$pos = 0;
	while ($row = db2_fetch_assoc($stmt)){
		 $r= array('id' => trim($row['ID']), 'text' => trim($row['TEXT']));
		 foreach($p['add_columns'] as $k_col){
		 	$r[strtolower($k_col)] = trim($row[strtoupper($k_col)]);
		 }
		 $ret['options'][] = $r;
	}
	$ret['success'] = true;
	return acs_je($ret);
}


?>