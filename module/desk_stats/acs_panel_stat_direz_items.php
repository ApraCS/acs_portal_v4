<?php

function crt_item_grid_per_anno($p = array()){
 $ret = "
	{
		xtype: 'gridpanel',
		cls: 'stat-main',
		multiSelect: true,
		selType: 'cellmodel',
		title: 'Fatturato',
		flex: 1,
		hideHeaders: true,
		
     	features: new Ext.create('Ext.grid.feature.Grouping',{
   			groupHeaderTpl: 'Fatturato {name)}',
   			hideGroupedHeader: false
   		}),               			
		
		
		store: new Ext.data.Store({
	
			autoLoad:true,
			proxy: {
				url: '{$p['store_proxy_url']}',
				type: 'ajax',
				reader: {
					type: 'json',
					root: 'root'
				}
				
				, actionMethods: {
					read: 'POST'
				}

				, extraParams: {}
				
				/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
				, doRequest: personalizza_extraParams_to_jsonData				
				
			},
			fields: ['ANNO', 'TIPORIGA', 'TIPODOC', 'TOT', 'M01', 'M02', 'M03', 'M04', 'M05', 'M06', 'M07', 'M08', 'M09', 'M10', 'M11', 'M12'],
			groupField: 'ANNO',
			groupDir: 'DESC',
			
			listeners: {
			
				beforeload: function(){this.proxy.extraParams = Ext.getCmp('panel-stat-direz').m_filters;},
			
				load: function(store, records, successful, operation, eOpts) {
					var me = this;				
                    
                    //preparo i totali generali scorrendo tutti i record
                    // e calcolo il totale di riga
                    my_tots = {};
                    
					me.each(function(record)  
					{ 
						
					   if (record.get('TIPORIGA') != 'HEADER'){
					
						   tot_riga = 0;
						
						   if (my_tots[record.get('ANNO')] === undefined )
						   	 my_tots[record.get('ANNO')] = {TOT: 0, M01:0, M02:0, M03:0, M04:0, M05:0, M06:0, M07:0, M08:0, M09:0, M10:0, M11:0, M12:0};
						   	 
							for (i = 1; i <= 12; i++) {																			
								if (record.get('M' + pad(i, 2)) != ''){
								
									tot_riga += parseFloat(record.get('M' + pad(i, 2)));							
								
									my_tots[record.get('ANNO')]['TOT'] += parseFloat(record.get('M' + pad(i, 2)));
									my_tots[record.get('ANNO')]['M' + pad(i, 2)] += parseFloat(record.get('M' + pad(i, 2)));																
								}						    
							}
						
						   record.set('TOT', tot_riga);
						   record.commit(); //altrimenti rimane il flag 'modificato'
					    }
					   						
					},this);                    
					
					
					
					//per ogni anno vado a costruirmi la riga di totale
					Ext.Object.each(my_tots, function(k_rec) {
						m_obj = my_tots[k_rec];
						
						n_rec = {ANNO: k_rec, TIPORIGA: 'TOT', TIPODOC: 'TOTALE', TOT: parseFloat(m_obj.TOT)};
						n_rec_prog = {ANNO: k_rec, TIPORIGA: 'TOT_PROG', TIPODOC: 'TOTALE PROG', TOT: 0};
						tot_prog = 0;
						for (i = 1; i <= 12; i++){
						 tot_prog = tot_prog + m_obj['M' + pad(i, 2)];
						 n_rec['M' + pad(i, 2)] = m_obj['M' + pad(i, 2)];
						 n_rec_prog['M' + pad(i, 2)] = tot_prog; 
						}
                    	me.add(n_rec);
                    	me.add(n_rec_prog);
                    }, this);
                    
                    
                    
					
                    //**********************************************************************************
					//costruzione riga di scostamento rispetto all'anno precedente (mese e progressivo)
                    //**********************************************************************************					
					Ext.Object.each(my_tots, function(k_rec) {
					
						//anno in linea						
						m_obj = my_tots[k_rec];
						
						//anno precedente
						m_obj_prec = my_tots[k_rec - 1];
						
						prog_anno = 0;
						prog_anno_prec = 0;
						
						
						if (m_obj_prec !== undefined){
							n_rec = {ANNO: k_rec, TIPORIGA: 'CONFRONTO_PERC', TIPODOC: 'Scostamento A.P.'};
							
							n_rec['TOT'] = [0, 0];
							if (parseFloat(m_obj_prec['TOT']) != 0)
								n_rec['TOT'][1] = (parseFloat(m_obj['TOT']) - parseFloat(m_obj_prec['TOT'])) / parseFloat(m_obj_prec['TOT']) * 100 ;
							else if (parseFloat(m_obj['TOT']) > 0)
								n_rec['TOT'][1] = 100;
							else
							    n_rec['TOT'][1] = -100;
							
							    
							    
							for (i = 1; i <= 12; i++){							
								n_rec['M' + pad(i, 2)] = [0, 0];
								
								 prog_anno += parseFloat(m_obj['M' + pad(i, 2)]);
								 prog_anno_prec += parseFloat(m_obj_prec['M' + pad(i, 2)]);
								
								
								 if (parseFloat(m_obj_prec['M' + pad(i, 2)]) != 0) 							
								 	n_rec['M' + pad(i, 2)][0] = (parseFloat(m_obj['M' + pad(i, 2)]) - parseFloat(m_obj_prec['M' + pad(i, 2)])) / parseFloat(m_obj_prec['M' + pad(i, 2)]) * 100 ;
								 else if (parseFloat(m_obj['M' + pad(i, 2)][0]) > 0)
								 	n_rec['M' + pad(i, 2)][0] = 100;
								 else
								 	n_rec['M' + pad(i, 2)][0] = -100;
								 	
								 //progressivo
								 if (prog_anno_prec != 0) 							
								 	n_rec['M' + pad(i, 2)][1] = (prog_anno - prog_anno_prec) / prog_anno_prec * 100 ;
								 else if (prog_anno > 0)
								 	n_rec['M' + pad(i, 2)][1] = 100;
								 else
								 	n_rec['M' + pad(i, 2)][1] = -100;								 	
								 	
								 	
							}
							 													
	                    	me.add(n_rec);						
						}
                    }, this);
                    
                
                    
				}			
			},			
			
		}),
		columns: [
			  {header   : 'Tipo', 	dataIndex: 'TIPODOC', 	flex     : 1.5}			  
			, {header   : 'Tot', 	dataIndex: 'TOT', 	flex     : 1, align: 'right', tdCls: 'c_TOT', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Gen', 	dataIndex: 'M01', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Feb', 	dataIndex: 'M02', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Mar', 	dataIndex: 'M03', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Apr', 	dataIndex: 'M04', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Mag', 	dataIndex: 'M05', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Giu', 	dataIndex: 'M06', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Lug', 	dataIndex: 'M07', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Ago', 	dataIndex: 'M08', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Set', 	dataIndex: 'M09', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Ott', 	dataIndex: 'M10', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Nov', 	dataIndex: 'M11', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
			, {header   : 'Dic', 	dataIndex: 'M12', 	flex     : 1, align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){return this.renderer_cell_confronto(value, metaData, record, row, col, store, gridView)}}
		],
		
		
		renderer_cell_confronto: function (value, metaData, record, row, col, store, gridView){
		
				if (record.get('TIPORIGA') == 'HEADER'){
					return value;
				}
		
  				if (record.get('TIPORIGA') == 'CONFRONTO'){
  					if (value > 0)
  	  				 metaData.tdCls += ' cell_confronto cell_confronto_pos';
					if (value < 0)
  	  				 metaData.tdCls += ' cell_confronto cell_confronto_neg';  	  				 
  				}				
				

				if (record.get('TIPORIGA') == 'CONFRONTO_PERC'){   				
  					metaData.tdCls += ' cell_confronto_perc';  
  					return '<table class=double_cell><tr><td class=\"x-grid-cell cell_confronto cell_confronto_pos\">' + floatRenderer0(value[0]) + '%</td><td align=right width=\"50%\" class=\"x-grid-cell cell_confronto cell_confronto_neg\">' + floatRenderer0(value[1]) + '%</td></tr></table>';
  				}
			    
 			return floatRenderer0(value);
		},
		
		
		viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {
		           v = 'stat_direz_row';
		           if (record.get('TIPODOC') == 'TOTALE')
		            v = v + ' TOT';

		           if (record.get('TIPORIGA') == 'TOT_PROG')
		            v = v + ' TOT_PROG';		            
		            
		           if (record.get('TIPORIGA') == 'HEADER')
		            v = v + ' HEADER';

		           if (record.get('TIPORIGA') == 'CONFRONTO')
		            v = v + ' CONFRONTO';		            
		            
		           if (record.get('TIPORIGA') == 'CONFRONTO_PERC')
		            v = v + ' CONFRONTO_PERC';
		            
		           return v;																
		         } 


		        , trackOver: false
				, listeners: {		        	
					  cellcontextmenu : function(view, cell, cellIndex, record, row, rowIndex, event) {
					    event.preventDefault();
					    event.stopEvent();
					  	
					    var voci_menu = [];
					    
					    //dettaglio per cliente/agente/....
					    voci_menu.push({	      	
				      		text: 'Dettaglio',
				    		iconCls : 'iconCarico',      		
				    		handler: function() {
				    		
				    			col_name = view.getGridColumns()[cellIndex].dataIndex;
				    			
				    			switch(col_name) {
								    case 'M01': mese_to = 1; break;
								    case 'M02': mese_to = 2; break;
								    case 'M03': mese_to = 3; break;
									case 'M04': mese_to = 4; break;
								    case 'M05': mese_to = 5; break;
								    case 'M06': mese_to = 6; break;
								    case 'M07': mese_to = 7; break;
								    case 'M08': mese_to = 8; break;
								    case 'M09': mese_to = 9; break;
									case 'M10': mese_to = 10; break;
								    case 'M11': mese_to = 11; break;
								    case 'M12': mese_to = 12; break;								    								    
								    default:
								        mese_to = null;
								} 
				    			 
				    			params = {	
				    						anno: parseInt(record.get('ANNO')),
				    						anno_p: parseInt(record.get('ANNO')) -1,
				    						mese_from: 1,
				    						mese_to: mese_to,
				    						main_filtri: Ext.getCmp('panel-stat-direz').m_filters
				    					  };  
				    							    			
				    			this.open_dettaglio(params, view, cell, cellIndex, record, row, rowIndex, event)
				    		}, scope: this
						  });
					    
						  
						var menu = new Ext.menu.Menu({items: voci_menu}).showAt(event.xy);						  						  
					  	return false;
					  } //cellcontextmenu
				}		         
		         
				
		         , open_dettaglio: function(params, view, cell, cellIndex, record, row, rowIndex, event){
		         	acs_show_panel_std('acs_panel_stat_direz.php?fn=open_tab_dett_cli', Ext.id(), params);
		         }				
				
		    } //viewConfig

		    
 
		
		
	}";
 return $ret;
}

function get_json_data_grid_per_anno(){
	global $conn, $cfg_mod_DeskStats;
	$m_params = (array)acs_m_params_json_decode();	
	
	$ret = array();
	$ar  = array();

	$where_m_filter = get_where_m_filter($m_params);

	//recupero l'elenco delle spedizioni/carichi per itinerario/giorno
	$sql = "SELECT STAARG AS ANNO, STMMRG, CONCAT('M', digits(STMMRG)) AS MESE, sum(STTIMP) as IMPORTO, sum(STTDOC) as NR_DOCUMENTI, STDOCU as TIPODOC
			 FROM {$cfg_mod_DeskStats['file_testate_ven']}
			 WHERE STAREA = 'FATTURATO' AND {$where_m_filter}
			 GROUP BY STAARG, STMMRG, STDOCU
			";
	
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);

			$el_anno = array();
	  
			while ($row = db2_fetch_assoc($stmt)){
				
				//costruisco elenco anni presenti (per scrittura intestazione)
				$el_anno[$row['ANNO']] +=1;
				
				$row['MESE'] = sprintf("M%02d", (int)$row['STMMRG']);
				
				$k_rec = implode("_", array($row['ANNO'], $row['TIPODOC']));
				
				if (is_null($ar[$k_rec])){
					$ar[$k_rec] = array('ANNO' => (int)$row['ANNO'], 
										'TIPODOC' => trim($row['TIPODOC']),
										'M01' => 0, 'M02' => 0, 'M03' => 0, 'M04' => 0, 
										'M05' => 0, 'M06' => 0, 'M07' => 0, 'M08' => 0,
										'M09' => 0, 'M10' => 0, 'M11' => 0, 'M12' => 0
					);
				}

				// LIVELLO 0 (tipo operazione)
				$s_ar = &$ar[$k_rec];
				$s_ar[$row['MESE']] += $row['IMPORTO'];				
				
			}

			//righe per intestazione anno
			foreach($el_anno as $k_anno => $anno){
				$ret[] = array('ANNO' => (int)$k_anno,
						'TIPORIGA' => 'HEADER', 'TIPODOC' => '', 'TOT' => 'Tot',
						'M01' => 'Gen', 'M02' => 'Feb', 'M03' => 'Mar', 'M04' => 'Apr',
						'M05' => 'Mag', 'M06' => 'Giu', 'M07' => 'Lug', 'M08' => 'Ago',
						'M09' => 'Set', 'M10' => 'Ott', 'M11' => 'Nov', 'M12' => 'Dic'
				);
			}			
			
			foreach ($ar as $ar_r)
				$ret[] = $ar_r;
			

		
	 return acs_je($ret);	
}

?>