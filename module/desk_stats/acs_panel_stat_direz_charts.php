<?php

function crt_item_charts_per_anno($p = array()){
 $ret = "
	{
		xtype: 'panel',
 		title: 'Grafici',
 		collapsible: true,
 		flex: 0.6,

        layout: {
		    type: 'hbox',
		    align: 'stretch',
		    pack : 'start',
		}, 		
 		
 		items: [
 		
 						{
								xtype: 'chart',
								rowspan: 2,
								flex: 1,
								
								store: {
									xtype: 'store',
									autoLoad: true,	
									proxy: {
										url: '" . $_SERVER['PHP_SELF'] . "?fn=get_json_data_fatturato',
										method: 'POST',
										type: 'ajax',
							
										//Add these two properties
										actionMethods: {
											type: 'json',
											read: 'POST'
										}
							
										, extraParams: {}
												
										/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
										, doRequest: personalizza_extraParams_to_jsonData				
										
										, reader: {
											type: 'json',
											method: 'POST',
											root: 'root'							
										}
										
									},
				
									fields: ['2015', '2014', '2013', 'MESE', 'MESE_out'],
												
									listeners: {									
										beforeload: function(){this.proxy.extraParams = Ext.getCmp('panel-stat-direz').m_filters;},
									}			
												
								}, //store	

								style:{
									background: '#fff',
                 					height:'100%'
             					},
								animate: true,
								
								legend: {
									position: 'left'
								},
								
								
								
								axes: [{
									type: 'Numeric',
									grid: true,
									position: 'left',
									fields: [],
									title: 'Fatturato (mln)',
									minimum: 0,
									adjustMinimumByMajorUnit: 0
								}, {
									type: 'Category',
									position: 'bottom',
									fields: ['MESE_out'],
									grid: true
								}],
								series: [{
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2015],
										smooth: true,
                						fill: true,
                						fillOpacity: 0.9																								
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2014]
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2013]
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2012]
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2011]
									}
								]
				} 		
 		
				, {

								xtype: 'chart',
								rowspan: 2,
								flex: 1,
								
								store: {
									xtype: 'store',
									autoLoad: true,	
									proxy: {
										url: '" . $_SERVER['PHP_SELF'] . "?fn=get_json_data_fatturato_prog',
										method: 'POST',
										type: 'ajax',
							
										//Add these two properties
										actionMethods: {
											type: 'json',
											read: 'POST'
										}
							
										, extraParams: {}
												
										/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
										, doRequest: personalizza_extraParams_to_jsonData																
										
										, reader: {
											type: 'json',
											method: 'POST',
											root: 'root'							
										}
										
									},
				
									fields: ['2015', '2014', '2013', 'MESE', 'MESE_out'],
												
									listeners: {									
										beforeload: function(){this.proxy.extraParams = Ext.getCmp('panel-stat-direz').m_filters;},
									}			
												
												
								}, //store	

								style:{
									background: '#fff',
                 					height:'100%'
             					},
								animate: true,
								
								legend: {
									position: 'left'
								},
								
								
								
								axes: [{
									type: 'Numeric',
									grid: true,
									position: 'left',
									fields: [],
									title: 'Progressivo (mln)',
									minimum: 0,
									adjustMinimumByMajorUnit: 0
								}, {
									type: 'Category',
									position: 'bottom',
									fields: ['MESE_out'],
									grid: true
								}],
								series: [{
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2015],
										smooth: true,
                						fill: true,
                						fillOpacity: 0.9																								
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2014]
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2013]
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2012]
									}, {
										type: 'line',
										highlight: false,
										axis: 'left',
										xField: ['MESE'],
										yField: [2011]
									}
								]
				} 												
 		
 		
 		
 		
 		] //items
	}";
 return $ret;
}




//fatturato (confronto mesi)
function get_json_data_fatturato($progressivo = false){
	global $conn, $cfg_mod_DeskStats;
	$m_params = (array)acs_m_params_json_decode();	
	
	$ret = array();
	$ar  = array();
	
	$where_m_filter = get_where_m_filter($m_params);	
	
	//recupero l'elenco delle spedizioni/carichi per itinerario/giorno
	$sql = "SELECT STAARG AS ANNO, STMMRG, CONCAT('M', digits(STMMRG)) AS MESE, sum(STTIMP/1000000) as IMPORTO, sum(STTDOC) as NR_DOCUMENTI
			FROM {$cfg_mod_DeskStats['file_testate_ven']}
			WHERE STAREA = 'FATTURATO' AND {$where_m_filter}
			GROUP BY STAARG, STMMRG
	";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	 
	$el_anni = array();
	while ($row = db2_fetch_assoc($stmt)){
		
		//elenco anni esportati
		$el_anni[$row['ANNO']] +=1;
	
		$row['MESE'] = sprintf("M%02d", (int)$row['STMMRG']);
	
		$k_rec = implode("_", array($row['MESE']));
	
		if (is_null($ar[$k_rec])){
			$ar[$k_rec] = array();
		}
	
		// LIVELLO 0 (tipo operazione)
		$s_ar = &$ar[$k_rec];
		$s_ar[$row['ANNO']] += (float)$row['IMPORTO'];
	
	}
		

	
	for ($i=1; $i<=12; $i++){
		$mese = sprintf("M%02d", $i);
		$mese_prec = sprintf("M%02d", $i-1);
		$r = array('MESE' => $mese);
		$r['MESE_out'] = get_mese_out($mese);
		
		foreach($el_anni as $k_anno=>$anno){
			$r[$k_anno] = (float)$ar[$mese][$k_anno];
			
			if ($progressivo == true){
				$r[$k_anno] += (float)$ar[$mese_prec][$k_anno];
				$ar[$mese][$k_anno] = $r[$k_anno];
			}
				
			
		}
		$ret[] = $r;		
	}
	
	return acs_je($ret);	
	
	
}



function get_mese_out($mese){
	switch ($mese){
		case "M01": return "Gen";
		case "M02": return "Feb";
		case "M03": return "Mar";
		case "M04": return "Apr";
		case "M05": return "Mag";
		case "M06": return "Giu";
		case "M07": return "Lug";
		case "M08": return "Ago";
		case "M09": return "Set";
		case "M10": return "Ott";
		case "M11": return "Nov";
		case "M12": return "Dic";
		default: return "??";
	}
}



?>