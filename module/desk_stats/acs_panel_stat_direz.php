<?php

require_once("../../config.inc.php");
require_once("./_utility.php");
require_once("./" . basename($_SERVER['PHP_SELF'], '.php') . "_items.php");
require_once("./" . basename($_SERVER['PHP_SELF'], '.php') . "_items_dett_cli.php");
require_once("./" . basename($_SERVER['PHP_SELF'], '.php') . "_charts.php");

$main_module = new DeskStats();


if ($_REQUEST['fn'] == 'get_json_data_grid_per_anno') {echo get_json_data_grid_per_anno(); exit;}
if ($_REQUEST['fn'] == 'get_json_data_grid_dett_cli') {echo get_json_data_grid_dett_cli(); exit;}
if ($_REQUEST['fn'] == 'get_json_data_fatturato') {echo get_json_data_fatturato(); exit;}
if ($_REQUEST['fn'] == 'get_json_data_fatturato_prog') {echo get_json_data_fatturato(true); exit;}

// ******************************************************************************************
// MAIN PANEL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
?>
{
 success:true, items: [
	{
		id: 'panel-stat-direz',
		title: 'Statistiche direzionali',
		xtype: 'panel',
        loadMask: true,
        closable: false,		
        layout: {
		    type: 'vbox',
		    align: 'stretch',
		    pack : 'start',
		},
        
        m_filters: {},
        
        items: [
        
        	<?php echo crt_main_left_menu() ?>,
        
			<?php echo crt_item_charts_per_anno(); ?>,
        
         	<?php echo crt_item_grid_per_anno(array(
         		'store_proxy_url' => $_SERVER['PHP_SELF'] . "?fn=get_json_data_grid_per_anno")
         	); ?>
        ],
		
		refresh_all: function(){
		
			//aggiorno grid e charts		
			Ext.each(this.query('grid'), function(cmp_to_reload) {cmp_to_reload.store.load();});
			Ext.each(this.query('chart'), function(cmp_to_reload) {
				cmp_to_reload.store.load();
			});
		}
	
	}
]
}
<?php exit; } ?>
<?php
// ******************************************************************************************
// DETTAGLIO PER CLIENTE/AGENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab_dett_cli'){
	
?>
{
 success:true, items: [
	{
		title: 'Dettagli',
		xtype: 'panel',
        loadMask: true,
        closable: true,		
        layout: {
		    type: 'vbox',
		    align: 'stretch',
		    pack : 'start',
		},
        
        config_p: <?php echo acs_raw_post_data(); ?>,
        
        items: [
			<?php echo crt_item_grid_dett_cli_menu(); ?>        
        	,
         	<?php echo crt_item_grid_dett_cli(array(
         		'store_proxy_url' => $_SERVER['PHP_SELF'] . "?fn=get_json_data_grid_dett_cli")
         	); ?>
        ]
		
	
	}
]
}
<?php exit; } ?>