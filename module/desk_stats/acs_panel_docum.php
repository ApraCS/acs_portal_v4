<?php

require_once("../../config.inc.php");

$main_module = new DeskStats();



function to_AS_date_from_format($format, $data){
	switch ($format){
		case "d/m/y":
			$d_ar = explode("/", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[0], 2000 + $d_ar[2]);
			return to_AS_date($d);		
		case "m-d-Y":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[0], $d_ar[1], $d_ar[2]);
			return to_AS_date($d);
		case "Y-m-d":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[2], $d_ar[0]);
			return to_AS_date($d);
				
	}
}



function docum_get_options_std($f_cod, $f_des, $mostra_codice = 'Y'){
	global $conn, $main_module;	
	$cfg_mod = $main_module->get_cfg_mod();
	$ar = array();
	$sql = "SELECT DISTINCT {$f_cod}, {$f_cod} FROM {$cfg_mod['file_docum']} ORDER BY {$f_des}";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);
	while ($row = db2_fetch_assoc($stmt)){
		if ($mostra_codice == 'Y')
			$text = "[" . trim($row[$f_cod]) . "] " . trim($row[$f_des]);
		else $text = trim($row[$f_des]);
		$ar[] = array("id"=>$row[$f_cod], "text" => $text);
	}
	return $ar;
}




####################################################
if ($_REQUEST['fn'] == 'search_cli_anag'){
####################################################
		$ar = array();
	
		/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
		$sql = "SELECT CFCD, CFRGS1, CFRGS2, CFIND1, CFIND2, CFLOC1, CFPROV, CFNAZ
			FROM {$cfg_mod_Admin['file_anag_cli']}
			WHERE UPPER(
				REPLACE(REPLACE(CONCAT(CFRGS1, CFRGS2), '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			AND CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = 'C' AND CFFLG3 = ''
 				";
	
  			$stmt = db2_prepare($conn, $sql);
	  		echo db2_stmt_errormsg();
	  		$result = db2_execute($stmt);
	
	  		$ret = array();
	  		while ($row = db2_fetch_assoc($stmt)) {
	  		    if (trim($row['CFNAZ']) == 'ITA' || trim($row['CFNAZ']) == 'IT'  || trim($row['CFNAZ']) == ''){
	  			$out_loc = acs_u8e(trim($row['CFLOC1']) . " (" . trim($row['CFPROV']) . ")");
	  			} else {
	  				$des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
	  				$out_loc = acs_u8e(trim($row['CFLOC1']) . ", " . trim($des_naz['text']));
	  			}
	
	  						$ret[] = array( "cod" 		=> $row['CFCD'],
	  						"descr" 	=> acs_u8e(trim($row['CFRGS1']) . " " . trim($row['CFRGS2'])),
	  								"out_loc"	=> $out_loc,
	  								"out_ind"	=> acs_u8e(trim($row['CFIND1']) . " " . trim($row['CFIND2']))
	  								);
	 		}
	  echo acs_je($ret);
	  exit;
	}
	







####################################################
if ($_REQUEST['fn'] == 'open_file'){
####################################################
	$cfg_mod = $main_module->get_cfg_mod();
	//interrogo il server con le immagini
	$abs_file_path = $cfg_mod['dir_fatture'] . "/" . $_REQUEST['file_path'] . ".pdf";
	$cont_file = file_get_contents($abs_file_path);
	
	$path_info = pathinfo($abs_file_path);
	$tipo_estensione = $path_info['extension'];
	
	switch(strtolower($tipo_estensione)){
		case 'pdf':
			header('Content-type: application/pdf');
			echo $cont_file;
			break;
		case 'tiff':
			header('Content-type: image/tiff');
			echo $cont_file;
			break;
		case 'png':
			header('Content-type: image/png');
			echo $cont_file;
			break;
		case 'jpg':
			header('Content-type: image/jpeg');
			echo $cont_file;
			break;				
		default:
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
			header("Content-disposition: attachment; filename=\"" . preg_replace('/^.+[\\\\\\/]/', '', $path_info['basename']) . "\"");
			echo $cont_file;
			break;
	}
	
	
 exit;
}



####################################################
if ($_REQUEST['fn'] == 'get_json_data_cli'){
####################################################
	$ar = array();
	$cfg_mod = $main_module->get_cfg_mod();
	
	/* ESCLUDO DAL LIKE GLI SPAZI E PUNTI */
	$sql = "SELECT DOCCON, DORGSO
			FROM {$cfg_mod['file_docum']}
			WHERE UPPER(
			REPLACE(REPLACE(DORGSO, '.', ''), ' ', '')
			) " . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
			GROUP BY DOCCON, DORGSO					
 				";
	
  			$stmt = db2_prepare($conn, $sql);
	  			echo db2_stmt_errormsg();
	  			$result = db2_execute($stmt);
	
	  			$ret = array();

			while ($row = db2_fetch_assoc($stmt)) {
	
				$ret[] = array( "cod" 		=> trim($row['DOCCON']),
								"descr" 	=> acs_u8e(trim($row['DORGSO']))
  				 );
			}

	echo acs_je($ret);		
	
 exit;
}


####################################################
if ($_REQUEST['fn'] == 'get_json_data'){
####################################################	
	$ret = array();
	$ar =  array();
	$n_children = 'children';
	$m_params = acs_m_params_json_decode();
	$form_ep = $m_params->form_ep;
	$cfg_mod = $main_module->get_cfg_mod();
	$sql_where = '';	
	$group_by = 'ARTPAR';
	$ar_where = array();

	$dt = $m_params->form_ep->f_ditta;
	
	
	//filtro per ditta (obbligatori)
	if (strlen($m_params->form_ep->f_ditta) > 0){
		$sql_where .= " AND DODT = " . sql_t($m_params->form_ep->f_ditta);   		
	} else {
		echo acs_je(array("success" => true));
		exit;
	}
	
	if (strlen($m_params->form_ep->f_cliente_cod) > 0){
		//$sql_where .= " AND DOCCON = " . sql_t(sprintf("%09s", $m_params->form_ep->f_cliente_cod));
		$sql_where .= " AND DOCCON = " . sql_t(transform_codice_cliente($m_params->form_ep->f_cliente_cod, $main_module->get_cfg_mod()));
	}

	if (strlen($m_params->form_ep->f_cartella) > 0){
		$sql_where .= " AND DOTIDO = " . sql_t($m_params->form_ep->f_cartella);
	}
	
	
	//filtro per data_ins (da...a)
	if (strlen($m_params->form_ep->f_data_dal) > 0){
		$sql_where .= " AND DODTRG >= " . to_AS_date_from_format("d/m/y", $m_params->form_ep->f_data_dal);
	}	
	if (strlen($m_params->form_ep->f_data_al) > 0){
		$sql_where .= " AND DODTRG <= " . to_AS_date_from_format("d/m/y", $m_params->form_ep->f_data_al);
	}
	
	//documento
	if (strlen($m_params->form_ep->f_num_docu) > 0){
		$sql_where .= " AND DONRDO LIKE '%\_%" . $m_params->form_ep->f_num_docu . "%\_%' ESCAPE '\'";
	}
	if (strlen($m_params->form_ep->f_num_docu_orig) > 0){
		$sql_where .= " AND DONROR LIKE '%" . $m_params->form_ep->f_num_docu_orig . "%'";
	}	
	
	
	
	$sql = "SELECT * FROM {$cfg_mod['file_docum']} where 1=1 {$sql_where}";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array($ar_where));
	while ($row = db2_fetch_assoc($stmt)) {
		
		$tmp_ar_id = array();
		
		//definizione livello di alberatura
		$liv0_v = trim($row['DOCCON']);   //CLIENTE
		$liv1_v = implode("___", array(trim($row['DOTIDO']), $row['DONRDO']));   //documento (pdf)
		$liv2_v = implode("___", array(trim($row['DOTIOR']), $row['DONROR']));   //documento (pdf)
		
		// LIVELLO 0 (cliente)
		$s_ar = &$ar;
		$liv 	= $liv0_v;
		$tmp_ar_id[] = $liv;
		if (is_null($s_ar[$liv])){
			$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_0", "task"=> $liv);
			$s_ar[$liv]["task"]   = "[{$liv}] " . trim($row['DORGSO']);
				$rif_cli = array();
				if (strlen(trim($row['DOPIVA'])) > 0) $rif_cli[] = "P.I. " . trim($row['DOPIVA']);
				if (strlen(trim($row['DOCFIS'])) > 0) $rif_cli[] = "C.F. " . trim($row['DOCFIS']);
			$s_ar[$liv]["riferimento"] = implode(", ", $rif_cli);
			$s_ar[$liv]["expanded"]   = true;
			$s_ar[$liv]["riferimento"]   = 0;
		}
		
		$s_ar_rif_liv0 = &$s_ar[$liv]["riferimento"]; //lo aumento solo quando sono al liv pdf
		$s_ar = &$s_ar[$liv][$n_children];
	
		
			// LIVELLO 1 (documento pdf)
			$liv 	= $liv1_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_1", "task"=> $liv);
				$s_ar[$liv]["task"]   = "[{$row['DOTIDO']}] {$row['DONRDO']}";
				$s_ar[$liv]["tipologia"]   	 = trim($row['DOCAUD'])  ;
				$s_ar[$liv]["descrizione"]   = "[" . trim($row['DONATD']) . "] " . trim($row['DODTDO']);
				$s_ar[$liv]["riferimento"]   = trim($row['DOVSRF']);
				$s_ar[$liv]["data"]   		 = trim($row['DODTRG']);
				$s_ar[$liv]["file_path"]   	 = trim($row['DODT']) . "/" . rtrim($row['DODPDF']);
				
				$s_ar_rif_liv0++;				
				
			}
			$s_ar = &$s_ar[$liv][$n_children];
		


			// LIVELLO 2 (documento origine)
			$liv 	= $liv2_v;
			$tmp_ar_id[] = $liv;
			if (is_null($s_ar[$liv])){
				$s_ar[$liv] = array("id" => implode("|", $tmp_ar_id), "liv_cod"=>$liv, "liv"=>"liv_2", "task"=> $liv);
				$s_ar[$liv]["task"]   = "[{$row['DOTIOR']}] {$row['DONROR']}";
				
				$nr_ord_ar = explode("_", $row['DONROR']);				
				$k_ordine = implode("_", array(trim($row['DODT']), " ", " ", " ", $nr_ord_ar[1], " ", trim($row['DODT'])));
				
				$s_ar[$liv]["k_ordine"] = $k_ordine;
				$s_ar[$liv]["tipo_ordine"] = trim($row['DOTIOR']);	 
			//	k_ordine 1 _VO_VO1_2015_132798         _          _1
			//			 1_ _ _ _124983_ _1
				$s_ar[$liv]["tipologia"]   = trim($row['DOCAUO']);	
				$s_ar[$liv]["data"]   		 = trim($row['DODTOR']);				
				$s_ar[$liv]["descrizione"]	 = trim($row['DODDOR']);				
				$s_ar[$liv]["leaf"] = true;
			}
		
	}

	
	//scrittura risultato
	$ret = array();
	
	$s_ar = &$ar;
	
	foreach($s_ar as $kar => $r){
		$ret[] = array_values_recursive($ar[$kar]);
	}
	
	$ar_ret = array();
	$ar_ret['children'] = $ret;
	
	echo acs_je($ar_ret);
	
	
	
exit;
}?>	






<?php 	
$m_params = acs_m_params_json_decode();
?>
{
 "success":true, "items":
 
 
 { 
  			xtype: 'panel',
	        title: '',
	        id: 'panel-docum',
	        closable: false,
	        
	        layout: {
			    type: 'vbox',
			    align: 'stretch',
			    pack : 'start',
			}, 	        

/*			
	        tbar: new Ext.Toolbar({
	            items:['<b>Parametri di ricerca</b>', '->'
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('treepanel').getStore().load();}}
	       		, {iconCls: 'tbar-x-tool x-tool-close', handler: function(event, toolEl, panel){ this.up('panel').close();}}
	         ]            
	        }),
*/	        
	        
	        
	        
	items: [        	        
 
 
 
 	//filtri *****************************************************
 
 		{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Parametri di ricerca',
	        collapsible: true,            
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
		 	  layout: {
			    type: 'vbox',
			    align: 'stretch',
			    pack : 'start',
			  },
            
            
            items: [
            
            
			{
			  xtype: 'fieldcontainer',
			  fieldLabel: '',
			  defaultType: 'textfield',
			  flex: 1,
		 	  layout: {
			    type: 'hbox',
			    align: 'stretch',
			    pack : 'start',
			  },
			  items: [				 	        
            		
						{
								name: 'f_ditta',
								width: 320,
								labelWidth: 70,
								xtype: 'combo',
								fieldLabel: 'Azienda',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		allowBlank: false,
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data: 
										<?php
										 if (isset($cfg_mod_DeskStats["ar_ditte"]))
										 	echo acs_je($cfg_mod_DeskStats["ar_ditte"]);
										 else
										 	echo "[" . acs_ar_to_select_json($main_module->find_sys_TA('XUDT'), '') . "]";
										 ?>
								    
								}
								
						 }
						 
						 
						 
						, {
								name: 'f_cartella',
								width: 300,
								labelWidth: 100, labelAlign: 'right',
								xtype: 'combo',
								fieldLabel: 'Cartella',
								forceSelection: true,								
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
						   		allowBlank: true,
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
								    data:
										<?php
										 if (isset($cfg_mod_DeskStats["ar_cartelle"]))
										 	echo acs_je($cfg_mod_DeskStats["ar_cartelle"]);
										 else
										 	echo "[" . acs_ar_to_select_json(docum_get_options_std('DOTIDO', 'DOTIDO', 'N'), '') . "]";
										 ?>								    
								    
								}
								
						 } 
						 
						 
						 
						 
					 , {
			            xtype: 'combo',
						name: 'f_cliente_cod',
						fieldLabel: 'Intestatario',
						labelAlign: 'right',
						flex: 1,
						minChars: 2,			
			            
			            store: {
			            	pageSize: 1000,
			            	
							proxy: {
					            type: 'ajax',
					            //url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_cli',
					            //url : 'acs_get_select_json.php?select=search_cli_anag',
					            //url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=search_cli_anag',
					            url : <?php
				            		if ($main_module->get_cfg_mod()['anagrafiche_cliente_da_modulo'] == 'Y')
				            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=get_json_data_cli');
				            		else
				            			echo acs_je($_SERVER['PHP_SELF'] . '?fn=search_cli_anag');
				            		?>,
					            reader: {
					                type: 'json',
					                root: 'root',
					                totalProperty: 'totalCount'
					            }
					        },       
							fields: ['cod', 'descr', 'out_loc'],		             	
			            },
			                        
						valueField: 'cod',                        
			            displayField: 'descr',
			            typeAhead: false,
			            hideTrigger: true,
			            allowBlank: true,
			
			            listConfig: {
			                loadingText: 'Searching...',
			                emptyText: 'Nessun cliente trovato',
			                
			
			                // Custom rendering template for each item
			                getInnerTpl: function() {
			                    return '<div class="search-item">' +
			                        '<h3><span>{descr}</span></h3>' +
			                        '[{cod}] {out_loc}' + 
			                    '</div>';
			                }                
			                
			            },
			            
			            pageSize: 1000
			
			        }						 

						 
	  ]
	 }
	 
	 
 		, {
			  xtype: 'fieldcontainer',
			  fieldLabel: 'Data da/a',
			  defaultType: 'textfield',
			  labelWidth: 70,
			  flex: 1,
		 	  layout: {
			    type: 'hbox',
			    align: 'stretch',
			    pack : 'start',
			  },
			  items: [
 								{
								     name: 'f_data_dal', width: 123								                     		
								   , xtype: 'datefield'
								   , startDay: 1 //lun. , format: 'd/m/Y', submitFormat: 'Ymd'
								   , listeners: { invalid: function (field, msg) { Ext.Msg.alert('', msg);} }
								}, {
								     name: 'f_data_al', width: 123
								   , xtype: 'datefield'
								   , startDay: 1 //lun. , format: 'd/m/Y', submitFormat: 'Ymd'
								   , listeners: { invalid: function (field, msg) { Ext.Msg.alert('', msg);} }
								}, {						
									name: 'f_num_docu', labelAlign: 'right',
									width: 300,
									labelWidth: 100,									
									xtype: 'textfield',
									fieldLabel: 'Nr documento'
								 }, {						
									name: 'f_num_docu_orig', labelAlign: 'right',
									xtype: 'textfield',
									fieldLabel: 'Nr doc. origine',
									flex: 1
								 }
			  ]
		   }	 
	 				        
    
				
					        
	],
	
	
 dockedItems: [	
   {
     xtype: 'toolbar',
     dock: 'right',
     title: '',
     layout: {
      pack: 'center',
      type: 'hbox'
     },
     items: [
			
			{
	            text: 'Visualizza',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();	
	            	if(form.isValid()){
	            	
	            		//verifico i filtri minimi
	            		if (!form.findField('f_num_docu').getValue() &&
	            			!form.findField('f_num_docu_orig').getValue() &&
	            			!form.findField('f_cliente_cod').getValue()) {
	            			acs_show_msg_error('Selezionare un cliente o un documento');
	            			return false;	            			
	            		 }

	            	
	            		grid = this.up('form').up('panel').down('treepanel');
	            		grid.store.proxy.extraParams['form_ep'] = form.getValues(false, false, false, false);
	            		
		            	grid.store.load();
			        }            	                	                
	            }
	        }			
	        
	  ]
	}		
			
	]
			
				
   },
 
 
 
 	//grid *******************************************************
 		{ 
  			xtype: 'treepanel',
	        title: '',
	        closable: false,
	        flex: 1,

	        //cls: 'elenco_ordini arrivi',
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: false,                    
				    fields: ['task', 'liv', 'descrizione', 'um', 'data', 'config', 'tipologia', 'riferimento', 'file_path', 'dt', 'k_ordine', 'tipo_ordine'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                      extraParams: {form_ep: {}}
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),
	        multiSelect: true,
	        singleExpand: false,
	
			columns: [	
	    		{xtype: 'treecolumn', text: 'Cliente / Elenco documenti', 	width: 300, dataIndex: 'task', tdCls: 'auto-height rif',
	          	  renderer: function (value, metaData, record, row, col, store, gridView){						
	  	  				 metaData.tdCls += ' auto-height';
		  				return value;			    
					}	    		
	    		},
	    		{text: 'Data', 			width: 80, dataIndex: 'data', renderer: date_from_AS},	
	    		{text: 'Tipo', 	width: 130, dataIndex: 'tipologia'},
	    	    {text: 'Descrizione', 	flex: 1, dataIndex: 'descrizione'},
	    	    {text: 'Riferimento', 	flex: 1, dataIndex: 'riferimento',
	          	  renderer: function (value, metaData, record, row, col, store, gridView){						
	  	  				if (record.get('liv') == 'liv_0') return '(Tot. documenti: ' + value + ')';
		  				return value;			    
					}	    	    
	    	    }
	    	    
	    	    
			  , {
					text: '<img src=<?php echo img_path("icone/48x48/design.png") ?> height=25>',			  
		            xtype:'actioncolumn', 
		            width:35,
		            items: [
														{
                                                          icon   : <?php echo img_path("icone/24x24/design.png") ?>,                                                         
                                                          tooltip: 'Visualizza allegati ordine',
                                                          handler: function(grid, rowIndex, colIndex, node, e, record, rowNode) {                                                          	
															
															
															//creo window per visualizzazione allegati
															
															print_w = new Ext.Window({
																  width: 500
																, height: 600
																, plain: true
																, title: 'Visualizzazione allegati'
																, layout: 'fit'
																, border: true
																, closable: true
														    	, maximizable: false
														    	, items: [
												                    new Ext.grid.GridPanel({
												                     title: 'Allegati',
												               		 store: new Ext.data.Store({
												               				autoLoad: true,				        
												               	  			proxy: {
												               							url: '../desk_vend/acs_get_order_images.php',
												               							type: 'ajax',
												               							reader: {
												               						      type: 'json',
												               						      root: 'root'
												               						     },
												               						     extraParams: {
												               		    		    		k_ordine: record.get('k_ordine')
												               		    				}               						        
																        
												               						},
												               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
												               		     	groupField: 'tipo_scheda',               		     	
												               			}),
												    		    	features: new Ext.create('Ext.grid.feature.Grouping',{
																		groupHeaderTpl: '{name}',
																		hideGroupedHeader: true
																	}),               			
												                  						    						
												                     columns: [{
															                header   : 'Tipo',
															                dataIndex: 'tipo_scheda', 
															                flex     : 5
															             }, {
																                header   : 'Data',
																                dataIndex: 'DataCreazione', 
																                width    : 70
																         }, {
															                header   : 'Nome',
															                dataIndex: 'des_oggetto', 
															                flex     : 5
															             }],
												
															         listeners: {
																      activate: {
																	      fn: function(e){
																		   	if (this.store.proxy.extraParams.k_ordine != '')
																			   	this.store.load();
																	      }
																      },
																	       
															   		  celldblclick: {								
																		  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
																			  rec = iView.getRecord(iRowEl);							  
																			  allegatiPopup('../desk_vend/acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
																		  }
															   		  }
																         
															         }
												
												                     })									
																    	
														    	]
															});	
															
															print_w.show();
															

															
															                                                          
                                                          },                                                          
                                                          getClass: function(v, meta, rec) {          
                                                              if(rec.get('liv')!='liv_2' || rec.get('tipo_ordine')!='VO') {                                                                      
                                                                  return 'x-hide-display';
                                                              }
                                                          }
                                                        }		            
		            ]
		        }
	    	    
	    	    
	    	    	    	    
			  , {
					text: '<img src=<?php echo img_path("icone/48x48/blog_compose.png") ?> height=25>',			  
		            xtype:'actioncolumn', 
		            width:35,
		            items: [
														{
                                                          icon   : <?php echo img_path("icone/24x24/blog_compose.png") ?>,                                                         
                                                          tooltip: 'Pdf',
                                                          handler: function(grid, rowIndex, colIndex, node, e, record, rowNode) {                                                          	
															allegatiPopup('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_file&file_path=' + record.get('file_path'));                                                          
                                                          },                                                          
                                                          getClass: function(v, meta, rec) {          
                                                              if(rec.get('liv')!='liv_1') {                                                                      
                                                                  return 'x-hide-display';
                                                              }
                                                          }
                                                        }		            
		            ]
		        }
	    	        	
			],
			enableSort: false, // disable sorting

	        listeners: {
		        	beforeload: function(store, options) {
		        		Ext.getBody().mask('Loading... ', 'loading').show();
		        		},		
			
		            load: function () {
		              Ext.getBody().unmask();
		            },	        
		        },
			

			viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }			
			    		
 	} 	
   ]
 } 	
} 