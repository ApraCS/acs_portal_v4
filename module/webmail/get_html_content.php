<?php
require_once "../../config.inc.php";
require_once "_process_email.php";
require_once "_email_functions.php";

$m_params = acs_m_params_json_decode();

/*ricevo come parametro:
 open_request:
   email_config:
     server:
     username:
     password:
     folder:
     folder_to: (nel caso di "spostamento" dei messaggi
*/ 

//recupero elenco messaggi
$imap_config = array(
    'account_mbox' => "{{$m_params->open_request->email_config->server}}",
    'mail_user'    => $m_params->open_request->email_config->username,
    'mail_psw'     => $m_params->open_request->email_config->password,
    'mbox_from'    => $m_params->open_request->email_config->folder
);

//lista messaggi
$mbox = imap_open($imap_config['account_mbox'] . $imap_config['mbox_from'], $imap_config['mail_user'], $imap_config['mail_psw']);
$msgid =  imap_msgno ($mbox, $m_params->message_uid );


$structure = imap_fetchstructure($mbox, $msgid);
if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
    $part = $structure->parts[1];
    $message = imap_fetchbody($mbox, $msgid, 1);
    
    if($part->encoding == 3) {
        $message = imap_base64($message);
    } else if($part->encoding == 1) {
        $message = imap_8bit($message);
    } else {
        $message = imap_qprint($message);
    }
}



/*
$imap_structure_arra = imap_fetchstructure($mbox, $msgid);
$imap_structure = $imap_structure_arra->parts[1]->encoding ?? '';

$imapx__fetchbody = imap_fetchbody($mbox, $msgid, 2);
// This line of for emails that don't have HTML part (it happens some times...)
if ( $imapx__fetchbody == '' ) { $imapx__fetchbody = imap_fetchbody($mbox, $msgid, 1);  }

switch ( $imap_structure ) {
    case 0 : $imapx__fetchbody = mb_convert_encoding($imapx__fetchbody, "UTF-8", "auto"); break;
    case 1 : $imapx__fetchbody = quoted_printable_decode(imap_8bit($imapx__fetchbody)); break;
    case 2 : $imapx__fetchbody = imap_binary($imapx__fetchbody); break;
    case 3 : $imapx__fetchbody = imap_base64($imapx__fetchbody); break;
    case 4 : $imapx__fetchbody = quoted_printable_decode($imapx__fetchbody); break;
    case 5 : $imapx__fetchbody = $imapx__fetchbody; break; // UNSHURE ABOUT THIS ONE...
    default : $imapx__fetchbody = $imapx__fetchbody; break;
}

$message = $imapx__fetchbody;
*/


/* attachments */
$has_attachments  = email_has_attachments($structure);
$list_attachments = email_list_attachments($structure);
$inline_attachments = email_list_inline_attachments($structure, $mbox, $msgid);


$message = imap_fetchbody($mbox, $msgid, 1);
$n_allegati = 0;
/* accodo elenco attanchemts */
if ($has_attachments) {
    $html_allegati = 'Allegati:<ul>';
    //$html_allegati .= print_r($list_attachments, true);
    $extjs_allegati = array();
    foreach($list_attachments as $att){
        if ($att['is_attachment']) {
            $n_allegati++;
            $url_parameters = array();
            $url_parameters['email_config'] = $imap_config;
            $url_parameters['message_uid'] = $m_params->message_uid;
            $url_parameters['part_number'] = $att['part_number'];
            $extjs_allegati[] = extjs_button("{$att['part_number']}) {$att['name']}", "small", null, array(
                'handler' => extjs_code("
                    function() {
                         window.open('" . ROOT_PATH . "module/webmail/view_attachment.php?" . http_build_query($url_parameters) . "', '_self');
                   }
                ")
            ));
            $html_allegati .= "<li><a target=\"_blank\" href=' " . ROOT_PATH . "module/webmail/view_attachment.php?" . http_build_query($url_parameters) . "'>{$att['part_number']}) {$att['name']}</a>";
        }
    }
    $html_allegati .= "</ul>";
    $html_allegati .= '<br><hr><br>';
    
    //$html_allegati .= print_r($list_attachments, true);
    //$html_allegati .= print_r($structure, true);
    
}


$s2 = _get_body_structure($mbox, $msgid);

$content = _email_add_inline_images($s2['content'], $inline_attachments);
$h = _email_info_header($mbox, $msgid);

//from
$mail_from = trim($h['info']->from[0]->mailbox);
$host_from = trim($h['info']->from[0]->host);
$fromaddress = "{$mail_from}@{$host_from}";

$mittente = $h['info']->fromaddress;
$oggetto = imap_utf8($h['info']->subject);

$intestazione = "<p><span style = 'margin-left : 35px; color: grey;'> Da </span> {$mittente} ({$fromaddress})</p>
                 <p><span style = 'color: grey;'>Oggetto </span> {$oggetto} </p>";

$ar_destinatari = array();
$ar_destinatari_cc = array();

if(isset($h['info']->to)){
    foreach($h['info']->to as $v){
        $ar_destinatari[] = "{$v->personal} ({$v->mailbox}@{$v->host})";
    }
    $destinarari = implode(", ", $ar_destinatari);
    $intestazione .= "<p><span style = 'margin-left : 40px; color: grey;'> A </span>  {$destinarari}</p>";
}

if(isset($h['info']->cc)){
    foreach($h['info']->cc as $v){
        $ar_destinatari_cc[] = "{$v->personal} ({$v->mailbox}@{$v->host})";
    }
    $destinarari_cc = implode(", ", $ar_destinatari_cc);
    $intestazione .= "<p><span style = 'margin-left : 35px; color: grey;'> CC </span>  {$destinarari_cc}</p>";
}



$message = $content; //$message;

echo echo_je(array('success' => true, 
        'attachments' => $list_attachments, 
        'att' => $attachments, 'h' => $hi, 'pe' => $part->encoding, 
        'msgid' => $msgid, 
        'intestazione' => $intestazione,
        'n_allegati' => $n_allegati,
        'allegati' => $extjs_allegati,
        'html_content' => $message));