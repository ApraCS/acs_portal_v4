<?php
require_once "../../config.inc.php";
require_once "_process_email.php";
require_once "_email_functions.php";

$m_params = acs_m_params_json_decode();


//recupero elenco messaggi
$imap_config = array(
    'account_mbox' => "{$_GET['email_config']['account_mbox']}",
    'mail_user'    => $_GET['email_config']['mail_user'],
    'mail_psw'     => $_GET['email_config']['mail_psw'],
    'mbox_from'    => $_GET['email_config']['mbox_from']
);

//lista messaggi
$mbox = imap_open($imap_config['account_mbox'] . $imap_config['mbox_from'], $imap_config['mail_user'], $imap_config['mail_psw']);
$msgid =  imap_msgno ($mbox, $_GET['message_uid'] );


$structure = imap_fetchstructure($mbox, $msgid);
$att_part = $structure->parts[$_GET['part_number']];

$file_name = email_get_part_name($att_part);

$file_content = imap_fetchbody($mbox, $msgid, $_GET['part_number'] +1);



if($att_part->encoding == 3) { // 3 = BASE64
    $file_content = base64_decode($file_content);
}
elseif($att_part->encoding == 4) { // 4 = QUOTED-PRINTABLE
    $file_content = quoted_printable_decode($file_content);
}


header("Content-Description: File Transfer");
header("Content-Type: application/octet-stream");
header("Content-disposition: attachment; filename=\"" . preg_replace('/^.+[\\\\\\/]/', '', $file_name) . "\"");
echo $file_content;
