<?php
require_once "../../config.inc.php";
$m_params = acs_m_params_json_decode();

if (isset($m_params->email_config->profilo_cod)) {
    $profilo_desc = 'Profilo di ricezione: ' . j($m_params->email_config->profilo_cod);
} else {
    $profilo_desc = 'Profilo di ricezione: ' . j($m_params->email_config->profilo_cod);
}
    
?>
{
	success: true, 
	m_win: {
		title: 'Protocollazione nuovi ordini/accodamento messaggi e allegati ad ordini esistenti',
		//title_suf: <?php echo j($profilo_desc) ?>,
		iconCls: 'icon-blog_add-16',
		maximize: 'Y',
		
	},
	items: [
	
	{
	 xtype: 'panel',
	 layout: 'hbox',
	  tbar: new Ext.Toolbar({
	          items:[  
	            {iconCls: 'icon-address_black-16'}, 
	            {xtype: 'displayfield', flex : 1, text  : 'Indirizzo di posta attivo', itemId: 'ttbar'} , '->',
	            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('grid').getStore().load();}}
		      ]            
	        }),
	 items: [
	     {
            xtype: 'grid',
            height: '100%',
            autoScroll: true,
            flex : 1, 
            features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}],
            store: {
				xtype: 'store',
				autoLoad:true,
	
						proxy: {
						   url: '../webmail/list_messages.php', 
						   method: 'POST',								
						   type: 'ajax',

					       actionMethods: {
					          read: 'POST'
					        },
					        extraParams: {
					        	open_request: <?php echo acs_je($m_params)?>
							},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						    reader: {
				             type: 'json',
							 method: 'POST',						            
				             root: 'root'						            
				           }
					},
					
        			fields: ['message_uid', 'mittente', 'oggetto', 'data', 'structure', 'body_content', 
        			         'fromaddress', 'toaddress', 'has_attachments', 'recent', 'unseen', 'answered']
			}, //store
			
			 <?php $att = "<img src=" . img_path("icone/48x48/attachment.png") . " height=16>"; ?>
			
			columns: [
				{header: '<br><?php echo $att; ?>', dataIndex: 'has_attachments', width: 20, tdCls: 'tdAction',
				renderer: function(value, p, record){
		    			  if(record.get('has_attachments') == true) return '<img src=<?php echo img_path("icone/48x48/attachment.png") ?> width=15>';
		    		  }},			
				{header: 'Mittente', dataIndex: 'mittente', width: 200, tdCls: 'tdAction', filter: {type: 'string'}, filterable: true}, 
	            {header: '', dataIndex: 'answered', width: 20, tdCls: 'tdAction',
				renderer: function(value, p, record){
		    			  if(record.get('answered') == 'A') return '<img src=<?php echo img_path("icone/48x48/button_blue_repeat.png") ?> width=15>';
		    		  }},	
	            {header: 'Oggetto', dataIndex: 'oggetto', flex: 1, tdCls: 'tdAction', filter: {type: 'string'}, filterable: true},
	            {header: 'Data', dataIndex: 'data', width: 120, tdCls: 'tdAction'},
	        ]
	        , viewConfig: {
	           listeners : {
                   'itemkeydown' : function(view, record, item, index, key) {
                       var grid = this.up('grid');
                       var nextRecord = grid.getStore().getAt(index+1);
                       var prevRecord = grid.getStore().getAt(index-1);
                      
                       if(key.keyCode == 40){
                        grid.fireEvent('itemclick', view, nextRecord, item, index, key);	
                       }
                       
                       if(key.keyCode == 38){
                        grid.fireEvent('itemclick', view, prevRecord, item, index, key);	
                       }
             
                   }
               },
		        getRowClass: function(record, index) {
		           if (record.get('recent')=='N' || record.get('unseen')=='U')
		           		return ' grassetto';		        
		                      		
		           return '';																
		         }   
		    },
	        listeners: {
	        
	         afterrender: function(comp){
        	  
				//autocheck degli ordini che erano stati selezionati
    	  		comp.store.on('load', function(store, records, options) {
    	  		    var toaddress = records[0].get('toaddress');
					var title_tbar = 'Indirizzo di posta attivo: <b>' + toaddress + '</b> - ' + <?php echo j($profilo_desc) ?>;
					var tbar = comp.up('panel').down('#ttbar');
					console.log(tbar);
					tbar.setValue(title_tbar);
									
				}, comp);
	 		 },
	 		 
	          itemclick: {								
				  fn: function(iView,rec,item,index,eventObj){
				    var body_content_panel = iView.up('window').down('#body_content'); 
				    var intestazione = iView.up('window').down('#intestazione');
				    var b_allegati = iView.up('window').down('#allegati'); 
				    var menu_allegati = iView.up('window').down('#menu_allegati'); 
				    iView.getEl().scrollTo('Top', 0);
				    
				   /* recupero il contenuto html */
    					Ext.Ajax.request({
    				        url: '../webmail/get_html_content.php',
    				        jsonData: {
    				        	open_request: {
    				        		email_config: <?php echo acs_je($m_params->email_config) ?>,
    				        	},
    				        	message_uid: rec.get('message_uid')   						        	
    				        },
    				        method     : 'POST',
    				        waitMsg    : 'Data loading',
    				        success : function(result, request){	
    				            var jsonData = Ext.decode(result.responseText);
    				            
    				            rec.set('recent', '');
    				            rec.set('unseen', '');
    				            rec.commit();
    				          
    				           // body_content_panel.setTitle('Oggetto: '+ rec.get('oggetto'));
    				            intestazione.update(jsonData.intestazione);
    				           
    				            body_content_panel.update(jsonData.html_content);
    				            
    				            if(jsonData.n_allegati > 0){
    				                b_allegati.show(true);
        				            b_allegati.setText(jsonData.n_allegati + ' Allegati')
        				            menu_allegati.removeAll(true);
        				            menu_allegati.add(jsonData.allegati);
    				            }else{
    				            	b_allegati.hide(true);
    				            }
    				        },
    				        failure    : function(result, request){
    				            Ext.getBody().unmask();
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    });				    
				    
				  	//body_content_panel.update(rec.get('body_content'));
				  }
			  }	 
            
	        }//listeners
	      
	        
	   } //grid
	   ,
	
	{
	xtype: 'container',
	flex: 1,
	//width: '100%',
	height: '100%',
	layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
	items: [
	 {itemId: 'intestazione', html: ''},
	 {
        flex: 1,
	 	itemId: 'body_content',
	 	//title: 'Anteprima',
	 	html: '',
	 	//height: '150%',
	 	scrollable: true, autoScroll: true,
	 	bodyStyle: {
	 		fontSize: '14px',
	 		//whiteSpace: 'pre',
	 		padding: '10px 10px 10px 10px'
	 	},
	 		buttons: [
	 		{
	 		xtype:'splitbutton',
	 		text: 'Allegati',
	 		scale: 'large',
	 		iconCls : 'icon-attachment-32',
	 		itemId : 'allegati',
	 		hidden : true,
 		    handler: function(){
                 this.maybeShowMenu();
           			},
	 		 menu: {
            	xtype: 'menu',
            	itemId : 'menu_allegati',
            	items: [	
				
				]}
	 		}, '->',
	 		{
	 			text: 'Gestisci ordine',
	 			scale: 'large',
	 			handler: function(b) {
	 			  var loc_win = this.up('window');
	 			  var loc_grid = b.up('window').down('grid');
	 			  
	 			  var id_selected = loc_grid.getSelectionModel().getSelection();
	 			  console.log(id_selected);
	 			  
	 			  if(id_selected.length == 0){
	 			      acs_show_msg_error('Selezionare una riga');
					  return false;
	 			  }
	 			  
	 			  var id_messages = [];		
				  for (var i=0; i<id_selected.length; i++) {
				    //num_evasi += parseFloat(id_selected[i].data.fl_evaso);
				    id_messages.push({message_uid : id_selected[i].get('message_uid'),
				    				  fromaddress : id_selected[i].get('fromaddress')});
				     }
	 			  
	 			  var my_listeners = 
                     { 
                       afterConferma: function(from_win, values) {
                         loc_win.fireEvent('afterProtocolla', loc_win, values);
                         from_win.close();
                       },
                       afterAbbina: function(from_win, from_win_2){
                       		console.log('afterAbbina!!!!!');                       		
                       		from_win.destroy();
                       		from_win_2.destroy();
                       		loc_grid.store.load();
                       		var body_content_panel = loc_win.down('#body_content');
                       		body_content_panel.setTitle('Anteprima');
                       		body_content_panel.update(''); 
                       }
                     }
	 			
	 			
	 			
	 			  acs_show_win_std(null, 
	 			  	'../webmail/acs_form_webmail.php?fn=open_form', {
	 			  		id_messages: id_messages,
	 			  		email_config: <?php echo acs_je($m_params->email_config) ?>
	 			  }, null, null, my_listeners);
	 			}
	 		}
	 	]
	 
	   }
	
	],
	
	},
	
	
		  
		
	  
	
	 
	 ]
	}
  ]
}

<?php 



