<?php
require_once "../../config.inc.php";
require_once "_process_email.php";

$m_params = acs_m_params_json_decode();

/*ricevo come parametro:
 open_request:
   email_config:
     server:
     username:
     password:
     folder:
     folder_to: (nel caso di "spostamento" dei messaggi
*/ 

//recupero elenco messaggi
$imap_config = array(
    'account_mbox' => "{{$m_params->open_request->email_config->server}}",
    'mail_user'    => $m_params->open_request->email_config->username,
    'mail_psw'     => $m_params->open_request->email_config->password,
    'mbox_from'    => $m_params->open_request->email_config->folder
);

//lista messaggi
$mbox = imap_open($imap_config['account_mbox'] . $imap_config['mbox_from'], $imap_config['mail_user'], $imap_config['mail_psw']);
$msgid =  imap_msgno ($mbox, $m_params->message_id );


//genero .eml content    
$headers_eml = imap_fetchheader($mbox, $msgid, FT_PREFETCHTEXT);
$body_eml = imap_body($mbox, $msgid);
echo acs_je(array('success' => true, 'email' => $headers_eml . "\n" . $body_eml));

