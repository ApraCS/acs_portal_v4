<?php
require_once "../../config.inc.php";
require_once "_process_email.php";

$m_params = acs_m_params_json_decode();

/*ricevo come parametro:
 open_request:
   email_config:
     server:
     username:
     password:
     folder:
     folder_to: (nel caso di "spostamento" dei messaggi
*/ 

//recupero elenco messaggi
$imap_config = array(
    'account_mbox' => "{{$m_params->open_request->email_config->server}}",
    'mail_user'    => $m_params->open_request->email_config->username,
    'mail_psw'     => $m_params->open_request->email_config->password,
    'mbox_from'    => $m_params->open_request->email_config->folder,
    'mbox_to'      => $m_params->open_request->email_config->folder_to,
);

//lista messaggi
$mbox = imap_open($imap_config['account_mbox'] . $imap_config['mbox_from'], $imap_config['mail_user'], $imap_config['mail_psw']);
$msgid =  imap_msgno ($mbox, $m_params->message_id );

//$folders = imap_listmailbox($mbox, $imap_config['account_mbox'], "*");

//sposto email in eventuale cartella "importati"
if (strlen($imap_config['mbox_to']) > 0){
    $ret = imap_mail_move($mbox, "$msgid", $imap_config['mbox_to']);
    imap_expunge($mbox);
    echo acs_je(array(
            "success" => $ret,
            //"folders" => $folders,
            "log" => "Sposto il messaggio nella cartella " . $imap_config['mbox_to']));
}	
