<?php

function email_has_attachments($structure){
    
    $has_attachments = false;
    $attachments = array();
    if(isset($structure->parts) && count($structure->parts)) {
        
        for($i = 0; $i < count($structure->parts); $i++) {
            
            
            $attachments[$i] = array(
                'is_attachment' => false,
                'filename' => '',
                'name' => '',
                'attachment' => ''
            );
            
            
            if($structure->parts[$i]->ifdparameters && $structure->parts[$i]->ifid != 1) {
                foreach($structure->parts[$i]->dparameters as $object) {
                    if(strtolower($object->attribute) == 'filename') {
                        $has_attachments = true;
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['filename'] = $object->value;
                    }
                }
            }
            
            if($structure->parts[$i]->ifparameters && $structure->parts[$i]->ifid != 1) {
                foreach($structure->parts[$i]->parameters as $object) {
                    if(strtolower($object->attribute) == 'name') {
                        $has_attachments = true;
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['name'] = $object->value;
                    }
                }
            }
            
            /*
             if($attachments[$i]['is_attachment']) {
             $attachments[$i]['attachment'] = imap_fetchbody($mbox, $msgid, $i+1);
             if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
             $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
             }
             elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
             $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
             }
             }
             */
        }
    }
    return $has_attachments;
}


function email_list_attachments($structure){
    $has_attachments = false;
    $attachments = array();
    if(isset($structure->parts) && count($structure->parts)) {
        
        for($i = 0; $i < count($structure->parts); $i++) {            
            
            $attachments[$i] = array(
                'is_attachment' => false,
                'filename' => '',
                'name' => '',
                'attachment' => ''
            );
            
            
            if($structure->parts[$i]->ifdparameters && $structure->parts[$i]->ifid != 1) {
                foreach($structure->parts[$i]->dparameters as $object) {
                    if(strtolower($object->attribute) == 'filename') {
                        $has_attachments = true;
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['filename'] = $object->value;
                        $attachments[$i]['part_number'] = $i;
                    }
                }
            }
            
            if($structure->parts[$i]->ifparameters && $structure->parts[$i]->ifid != 1) {
                foreach($structure->parts[$i]->parameters as $object) {
                    if(strtolower($object->attribute) == 'name') {
                        $has_attachments = true;
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['name'] = $object->value;
                        $attachments[$i]['part_number'] = $i;
                    }
                }
            }
        }
    }
    return $attachments;
}


function email_list_inline_attachments($structure, $mbox, $msgid){
    $has_attachments = false;
    $attachments = array();
    if(isset($structure->parts) && count($structure->parts)) {        
        foreach($structure->parts as $kp => $part){
            $attachments = _get_inline_attachment_rec($attachments, $mbox, $msgid, $ret, $part, '', $kp + 1);
        }
    }
    return $attachments;
}

function _get_inline_attachment_rec($attachments, $mbox, $mid, $ret, $part, $pref, $c){    
    if ($part->disposition == 'inline' || $part->ifid == 1){
        $attachments[] = imap_fetchbody($mbox, $mid, "{$pref}{$c}");
    }
    
    if ($part->parts){        
        foreach($part->parts as $kp => $part2){
            $attachments = _get_inline_attachment_rec($attachments, $mbox, $mid, $ret, $part2, "{$pref}{$c}.", $kp +1);
        }
    }
    
    
    return $attachments;
}





function email_get_part_name($part){
    if($part->ifdparameters) {
        foreach($part->dparameters as $object) {
            if(strtolower($object->attribute) == 'filename') {
                return $object->value;
            }
        }
    }
    
    if($parts->ifparameters) {
        foreach($part->parameters as $object) {
            if(strtolower($object->attribute) == 'name') {
                return $object->value;                
            }
        }
    }
}



function _get_body_structure($mbox, $mid) {
    $struct = imap_fetchstructure($mbox, $mid);
    
    $ret = array(
        'attachment' => array()
    );
    
    
    $parts = $struct->parts;
    $i = 0;
    if (!$parts) { /* Simple message, only 1 piece */
        $attachment = array(); /* No attachments */
        $content = imap_body($mbox, $mid);
        $ret['content'] = "<div style=\"white-space: pre\">" . imap_fetchbody($mbox, $mid, 1) . '</div>';
    } else { /* Complicated message, multiple parts */
        
        foreach($parts as $kp => $part){
            $ret = _get_body_structure_rec($mbox, $mid, $ret, $part, '', $kp + 1, 'HTML');
        }
 
    } /* complicated message */
    
    if (!isset($ret['content'])){
        foreach($parts as $kp => $part){
            $ret = _get_body_structure_rec($mbox, $mid, $ret, $part, '', $kp + 1, 'PLAIN');
        }
        $ret['content'] = "<div style=\"white-space: pre\">" . $ret['content'] . '</div>';
    }
    
    //$content = print_r($struct, true);
    
    //$ret['attachment'] = $attachment;
    //$ret['body'] = quoted_printable_decode($ret['body']);
    
    //$ret['content'] = "<pre>" . print_r($struct, true);
    return $ret;
}

function _get_body_structure_rec($mbox, $mid, $ret, $part, $pref, $c, $tag){
    if ($part->subtype == $tag){
       $ret['content'] .= imap_fetchbody($mbox, $mid, "{$pref}{$c}");
       
       if ($part->encoding == 3){
           $ret['content'] = base64_decode($ret['content']);
       }
       if ($part->encoding == 4){
           $ret['content'] = quoted_printable_decode($ret['content']);           
       }
    }
    
    if ($part->parts){
        foreach($part->parts as $kp => $part){
            $ret = _get_body_structure_rec($mbox, $mid, $ret, $part, "{$pref}{$c}.", $kp +1, $tag);
        }
    }
    
    
    return $ret;
}


function _email_add_inline_images($content, $inlineAttachments){
    //return '<pre>' . print_r($inlineAttachments, true);
    \preg_match_all("/\bcid:[^'\"\s]{1,256}/mi", $content, $matches);
    //$ret['content'] .= '<pre>' . print_r($matches, true);
    //$ret['content'] .= '<pre>' . print_r($struct, true);
    
    
    if (isset($matches[0]) && \is_array($matches[0]) && \count($matches[0])) {
        /** @var list<string> */
        $matches = $matches[0];
        $attachments = $inlineAttachments;
        foreach ($matches as $match) {
            
            $cid = \str_replace('cid:', '', $match);
            
            //foreach ($inlineAttachments as $attachment) {
                
                //if ($attachment->contentId == $cid && 'inline' == $attachment->disposition) {
                    $contents = array_shift($inlineAttachments); //$attachment->getContents();
                    
                    //$base64encoded = \base64_encode($contents);
                    $base64encoded = $contents;
                    $replacement = 'data:'.$contentType.';base64, '.$base64encoded;
                    
                    $content = \str_replace($match, $replacement, $content);                    
                //}
            //}
        }
    }
    return $content;
}
