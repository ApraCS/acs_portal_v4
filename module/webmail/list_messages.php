<?php
require_once "../../config.inc.php";
require_once "_process_email.php";
require_once "_email_functions.php";

$m_params = acs_m_params_json_decode();

/*ricevo come parametro:
 open_request:
   email_config:
     server:
     username:
     password:
     folder:
     folder_to: (nel caso di "spostamento" dei messaggi
*/ 

//recupero elenco messaggi
$imap_config = array(
    'account_mbox' => "{{$m_params->open_request->email_config->server}}",
    'mail_user'    => $m_params->open_request->email_config->username,
    'mail_psw'     => $m_params->open_request->email_config->password,
    'mbox_from'    => $m_params->open_request->email_config->folder
);

//apertura cartella
//$mbox = imap_open ($imap_config['account_mbox'], $imap_config['mail_user'], $imap_config['mail_psw']);

//lista messaggi
$mbox = imap_open($imap_config['account_mbox'] . $imap_config['mbox_from'], $imap_config['mail_user'], $imap_config['mail_psw']);
$headers = imap_headers($mbox);

$ret = array();
if ($headers == false) {
    echo "\nNessun messaggio presente.\n";
} else {
    $i = 0;
    foreach ($headers as $val) {
        $i++;
        $h = _email_info_header($mbox, $i);
        $structure = imap_fetchstructure($mbox, $i);
        
        
        
        //from
        $mail_from = trim($h['info']->from[0]->mailbox);
        $host_from = trim($h['info']->from[0]->host); 
        //to
        $mail_to = trim($h['info']->to[0]->mailbox);
        $host_to = trim($h['info']->to[0]->host);
        
        $ret[] = array(
            'message_id'  => $h['info']->message_id,
            'message_uid' => imap_uid($mbox, $i),
            'mittente'    => $h['info']->fromaddress,
            'fromaddress' => "{$mail_from}@{$host_from}",
            'toaddress' => "{$mail_to}@{$host_to}",
            'oggetto'     => imap_utf8($h['info']->subject),
            'data'        => date('d/m/Y - h:i', strtotime($h['info']->date)),
            'recent'      => $h['info']->Recent,
            'unseen'      => $h['info']->Unseen,
            'answered'    => $h['info']->Answered,
            'has_attachments' => email_has_attachments($structure),
            //'body_content'=> utf8_decode($h['body_content'])
        );
    }
}

echo acs_je($ret);
