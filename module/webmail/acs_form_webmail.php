<?php 

require_once "../../config.inc.php";
$m_params = acs_m_params_json_decode();

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------------

    $email = $m_params->id_messages[0]->fromaddress;
    $sql = "SELECT TADESC, TACOR1, CFCD, CFRGS1, CFRGS2, CFIND1, CFIND2, CFLOC1, CFPROV, CFNAZ,
            SUBSTRING(TAREST, 101, 3) AS DEST1 
            FROM {$cfg_mod_Admin['file_tab_sys']} TA
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
                ON CF.CFDT = TA.TADT AND CF.CFCD = TA.TACOR1 AND CF.CFTICF = 'C' AND CF.CFFLG3 = ''
            
            WHERE TADT = '{$id_ditta_default}' AND TAID = 'BRCF'
            AND TALINV='' AND TRIM(SUBSTRING(TAREST, 40, 60)) = '{$email}'
            LIMIT 1";
    
 
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    $ret = array();
    $ret['cod'] = $row['CFCD'];
    $ret['descr'] = acs_u8e(trim($row['CFRGS1']) . "" . trim($row['CFRGS2']));
    if (trim($row['CFNAZ']) == 'ITA' || trim($row['CFNAZ']) == ''){
        $out_loc = acs_u8e(trim($row['CFLOC1']) . " (" . trim($row['CFPROV']) . ")");
    } else {
        $des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
        $out_loc = acs_u8e(trim($row['CFLOC1']) . ", " . trim($des_naz['text']));
    }
    $ret['out_loc'] = $out_loc;
    $ret['out_ind'] = acs_u8e(trim($row['CFIND1']) . " " . trim($row['CFIND2']));
   
    $cliente = sprintf("%09s", $row['CFCD']);

    $desc_dest1 = get_TA_sys('VUDE', trim($row['DEST1']), $cliente, null, null, null, 0, " AND SUBSTRING(TAREST, 163, 1) = 'D'");
        
    $ret['f_destinazione_cod'] = trim($row['DEST1']);
    $ret['f_destinazione'] = $desc_dest1['text'];
    $ret['out_ind_dest']   = $desc_dest1['out_ind_dest'];
    $ret['out_loc_dest']   = $desc_dest1['out_loc_dest'];
    
    $desc_pven = get_TA_sys('VUDE', trim($row['DEST1']), $cliente, null, null, null, 0, " AND SUBSTRING(TAREST, 163, 1) = 'P'");
    $ret['f_pven'] = $desc_pven['text'];
    $ret['out_ind_pven']   = $desc_pven['out_ind_dest'];
    $ret['out_loc_pven']   = $desc_pven['out_loc_dest'];
    
    ?>
{"success":true, 
	m_win: {
		title: 'Assegna cliente/sede ordine',
		width: 450, height: 500,
		iconCls: 'icon-button_blue_play-16'
	}, 
	"items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            items: [
             {
            xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,		
			 labelWidth : 120,
			allowBlank : false,	
            store: {
            	pageSize: 1000,
            	
				proxy: {
		            type: 'ajax',
		            
		            url : <?php echo acs_je('../desk_vend/acs_get_select_json.php?select=search_cli_anag'); ?>,
		            		
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
					fields: ['cod', 'descr', 'out_ind', 'out_loc'],	             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
	        listeners: {
    	        afterrender: function(comp){
    	                var form = this.up('form').getForm();
    	                comp.store.loadData([<?php echo acs_je($ret); ?>]);
                        comp.setValue(<?php echo j($ret['cod']); ?>);
                       
    	        },
	            change: function(field,newVal) {	            	
	            	var form = this.up('form').getForm();
	                  form.findField('f_destinazione_cod').setValue('');	
					  form.findField('f_destinazione').setValue('');
                      form.findField('out_ind_dest').setValue('');
                      form.findField('out_loc_dest').setValue('');  
                      form.findField('out_ind_dest_h').setValue('');
                      form.findField('out_loc_dest_h').setValue('');           	
	            },
	            	select: function(combo, row, index) {
	            	var form = this.up('form').getForm();
	     			form.findField('out_ind').setValue(row[0].data.out_ind);									     			
	     			form.findField('out_loc').setValue(row[0].data.out_loc);

 					//imposto quella a standard
					form.findField('f_destinazione_cod').store.load({
	 					callback: function(records, operation, success) {
 							
 							if (records.length == 1){
 								this.setValue(records[0].get('cod'));
 							}
    					}, scope: form.findField('f_destinazione_cod') 	
 					});
 								
 		
				} 
	        },            

            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item">' +
                        '<h3><span>{descr}</span></h3>' +
                        '[{cod}] {out_loc}' + 
                    '</div>';
                }                
                
            },
            
            pageSize: 1000

        }
					 
				,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind', 
 			 labelWidth : 120,
 			value : <?php echo j($ret['out_ind']); ?>,	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc', 
 			 labelWidth : 120,
 			value : <?php echo j($ret['out_loc']); ?>,
 			anchor: '100%',		
 		}	 
					 
					 
		, {
			xtype: 'fieldcontainer',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_destinazione_cod',
                    hidden : true,
                    value : <?php echo j($ret['f_destinazione_cod']); ?>,
				},
                {   xtype: 'textfield',
				    name: 'f_destinazione',
				    fieldLabel: 'Destinazione merce',
                 	flex : 1,
                    readOnly : true,
                    labelWidth : 120,
                    value : <?php echo j($ret['f_destinazione']); ?>,
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_destinazione_cod').setValue(record_selected.cod);	
												form.findField('f_destinazione').setValue(record_selected.denom);
                                                form.findField('out_ind_dest').setValue(record_selected.IND_D);
                                                form.findField('out_loc_dest').setValue(record_selected.LOC_D);
                                                form.findField('out_ind_dest_h').setValue(record_selected.IND_D);
                                                form.findField('out_loc_dest_h').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
                                    
                                    if(cliente == ''){
                    	 			      acs_show_msg_error('Inserire un cliente');
                    					  return false;
                    	 			  }
							  
									acs_show_win_std('Seleziona destinazione', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, type : 'D'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
 		
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_dest', 
 			anchor: '100%',
 			 labelWidth : 120,
 			 value : <?php echo j($ret['out_ind_dest']); ?>,
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_dest', 
 			anchor: '100%',
 			 labelWidth : 120,
 			value : <?php echo j($ret['out_loc_dest']); ?>,
 		} 
 		
 		,  {
 			xtype: 'textfield', hidden: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_dest_h', 
 			anchor: '100%',
 			value : <?php echo j($ret['out_ind_dest']); ?>,
 		}, {
 			xtype: 'textfield', hidden: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_dest_h', 
 			anchor: '100%',
 			value : <?php echo j($ret['out_loc_dest']); ?>,
 		} ,
        
        {
			xtype: 'fieldcontainer',
			flex : 1,
			//anchor: '-10',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_pven_cod',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'f_pven',
				    fieldLabel: 'Punto vendita',
                    flex : 1,
                    allowBlank : true,
                    labelWidth : 120,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_pven_cod').setValue(record_selected.cod);	
												form.findField('f_pven').setValue(record_selected.denom);
                                                form.findField('out_ind_pven').setValue(record_selected.IND_D);
                                                form.findField('out_loc_pven').setValue(record_selected.LOC_D);
                                                form.findField('out_ind_pven_h').setValue(record_selected.IND_D);
                                                form.findField('out_loc_pven_h').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
                                    
                                    if(cliente == ''){
                    	 			      acs_show_msg_error('Inserire un cliente');
                    					  return false;
                    	 			  }
							  
									acs_show_win_std('Seleziona punto vendita', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, type : 'P'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
			 labelWidth : 120,
 			name: 'out_ind_pven', 	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_pven', 	
 			 labelWidth : 120,	
 			anchor: '100%'
				
			}	,
			{
 			xtype: 'textfield', hidden: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_pven_h', 
 			anchor: '100%',
 			value : <?php echo j($ret['out_ind_pven']); ?>,
 		}, {
 			xtype: 'textfield', hidden: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_pven_h', 
 			anchor: '100%',
 			value : <?php echo j($ret['out_loc_pven']); ?>,
 		} ,
        
        {
				xtype: 'fieldset',
                title: 'Rubrica cliente/mittenti [BRCF]',
                layout: 'anchor',
                flex:1,
                items: [
        	{
            xtype: 'textfield',
            fieldLabel : 'Denominazione mittente',
            name: 'f_denominazione',
            labelWidth : 150,
            anchor: '-10',
            labelAlign : 'top',
            value : <?php echo j($row['TADESC']); ?>,
            flex : 1,
            },
       		{
				xtype: 'button',
				text: 'Aggiungi a rubrica',
				scale: 'small',
				margin : '0 0 0 120',
				width : 150,
				handler : function(){
				var form = this.up('form').getForm();
				var cliente = form.findField('f_cliente_cod').value;
				if(Ext.isEmpty(cliente)){
					acs_show_msg_error('Inserire un cliente');
		        	return;
				}
					 var my_listeners = {
    		  			afterOkSave: function(from_win, src){
    						from_win.close();  
			        		}
	    				};
				
				 acs_show_win_std('Aggiungi BRCF', '../desk_gest/acs_anag_cli_schede_aggiuntive.php?fn=open_mod', {
						 cliente: cliente, form_values : form.getValues(), email : <?php echo j($email); ?>, tipo_scheda : 'BRCF'  
					}, 500, 470, my_listeners, 'icon-pencil-16');
    		   } 
			}
			
        ]}
		  	
            
            
            
            	
            ],
            
			buttons: [
			
			{
		            text: 'Allega a ordine esistente',
		            iconCls: 'icon-button_blue_play-32',
			       	scale: 'large',		            
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	var loc_win = this.up('window');
		            	var cliente = form.findField('f_cliente_cod').value;
		            	var destinazione = form.findField('f_destinazione_cod').value;
		            	
		            	 if(cliente == ''){
        	 			      acs_show_msg_error('Inserire un cliente');
        					  return false;
        	 			  }
		            	
		            	var my_listeners = {
	    		  			afterAssegna: function(from_win, k_ordine){
        						
        						Ext.Ajax.request({
        						        url: '../desk_vend/acs_allega_a_ordine_esistente.php?fn=exe_allega',
        						        jsonData: {
        						        	k_ordine : k_ordine,
        						        	id_messages: <?php echo acs_je($m_params->id_messages) ?>,
        						        	email_config: <?php echo acs_je($m_params->email_config) ?>,
        						        	move_to_importati: true
        						        },
        						        method     : 'POST',
        						        waitMsg    : 'Data loading',
        						        success : function(result, request){	        						                     	  													        
        						            var jsonData = Ext.decode(result.responseText);
        						            loc_win.fireEvent('afterAbbina', loc_win, from_win, 
                    		            		{
                    		            		}
                    		            	);
        						        },
        						        failure    : function(result, request){
        						            Ext.getBody().unmask();
        						            Ext.Msg.alert('Message', 'No data to be loaded');
        						        }
        						    });  
				        		}
		    				};
    				    	
    				    	 acs_show_win_std('Elenco ordini aperti', 
    				         	'../base/acs_seleziona_ordini_gest.php?fn=open', {
    				         		cod_cli: cliente,
    				         		cod_dest: destinazione,
    				         		doc_gest_search: 'WEBMAIL_abbina',
    				         		show_parameters: false,
    				         		auto_load: true,
    				         		from_webmail : 'Y'
    				         	}, 800, 550,  my_listeners, 'icon-clessidra-16');
    				    				
		            	       		
			              
		            }
		        }, '->',
			      {
		            text: 'Protocolla nuovo ordine',
		            iconCls: 'icon-button_blue_play-32',
			       	scale: 'large',		            
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	var loc_win = this.up('window');
		            	var r_cli = form.findField('f_cliente_cod').displayTplData[0];
		            	var cliente = form.findField('f_cliente_cod').value;
		            	
		            	 if(cliente == ''){
        	 			      acs_show_msg_error('Inserire un cliente');
        					  return false;
        	 			  }
		            	
		            
		            	loc_win.fireEvent('afterConferma', loc_win, 
		            		{
		            		  form_values: form.getValues(),
		            		  r_cli : r_cli,
		            		  open_request: <?php echo acs_je($m_params) ?>
		            		}
		            	);
			              
		            }
		        }
	        ]
	  }
]}
<?php 
	exit;
}
