<?php

	class DeskTranslations {
	
		private $mod_cod = "DESK_TRANS";
		private $mod_dir = "desk_trans";
	
	
		function __construct($parameters = array()) {
			global $auth;
			$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
	
			if (is_null($mod_permitted) || !$mod_permitted){
	
				//se mi ha passato un secondo modulo testo i permessi
				if (isset($parameters['abilita_su_modulo'])){
					$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
					if (is_null($mod_permitted) || !$mod_permitted){
						die("\nNon hai i permessi!!");
					}
				} else
					die("\nNon hai i permessi!!!");
			}
		}
		
		public function get_cod_mod(){
			return $this->mod_cod;
		}

		
		public function fascetta_function(){
		    global $auth, $main_module;
		    $js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
		    
		    echo "";
		    echo "<div class='fascetta-toolbar'>";
		    echo "<img id='bt-arrivi' src=" . img_path("icone/48x48/arrivi.png") . " height=30>";
		    echo "<div id='stato-aggiornamento'>[<img id='bt-stato-aggiornamento' class='st-aggiornamento' src=" . img_path("icone/48x48/rss_blue.png") . " height=20><span id='bt-stato-aggiornamento-esito'></span>]</div>";
		    echo "</div>";
		            
		}
		
		
		function find_TA_std($tab, $k1 = null, $is_get = 'N', $esporta_esteso = 'N', $k2 = null, $aspe = null, $gruppo = null, $order_by_seq = 'N', $mostra_codice = 'N', $add_where = ''){
		    global $conn;
		    global $cfg_mod_DeskArt, $id_ditta_default;
		    $ar = array();
		    
		    $sql = "SELECT *
		    FROM {$cfg_mod_DeskArt['file_tabelle']}
		    WHERE TADT='$id_ditta_default' AND TATAID = '$tab'
            {$add_where}";
		    
		    if (isset($k1))
		        $sql .= " AND TAKEY1 = '$k1' ";
		        
	        if (isset($k2))
	            $sql .= " AND TAKEY2 = '$k2' ";
		            
            if (isset($aspe))
                $sql .= " AND TAASPE = '$aspe' ";
		                
            if (isset($gruppo))
                $sql .= " AND TARIF1 = '$gruppo' ";
		                    
		                    
            if ($order_by_seq == 'Y')
                $sql .= "  ORDER BY TASITI, UPPER(TADESC)";
            else if ($order_by_seq == 'P')
                $sql .= "  ORDER BY TAPESO, UPPER(TADESC)";
            else
                $sql .= "  ORDER BY UPPER(TADESC)";
		                                
            $stmt = db2_prepare($conn, $sql);
            $result = db2_execute($stmt);
		                                
            $ret = array();
            while ($row = db2_fetch_assoc($stmt)) {
                if (is_null($k1) || $is_get == 'Y') $m_id = trim($row['TAKEY1']);
                else $m_id = trim($row['TAKEY2']);
		                                    
            $r = array();
            
            $r['id'] 		= $m_id;
            
            if ($mostra_codice == 'Y')
                $r['text'] = "[" . $m_id. "] " . $row['TADESC'];
                else
                    $r['text'] = acs_u8e(trim($row['TADESC']));
                    
                    
                    if ($esporta_esteso == 'Y'){
                        $r['TAKEY1'] = $row['TAKEY1'];
                        $r['TAKEY2'] = $row['TAKEY2'];
                        $r['TAKEY3'] = $row['TAKEY3'];
                        $r['TAKEY4'] = $row['TAKEY4'];
                        $r['TAKEY5'] = $row['TAKEY5'];
                        $r['TARIF2'] = $row['TARIF2'];
                        $r['TAFG01'] = $row['TAFG01'];
                        $r['TAFG02'] = $row['TAFG02'];
                    }
                    
                    //		 $ret[] = array("id" => $m_id, "text" => $row['TADESC'] );
                    $ret[] = $r;
        }
        
        return $ret;
		}
		
		

		
	}
	