<?php

require_once "../../config.inc.php";

$main_module = new DeskTranslations();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

    
// ******************************************************************************************
// REPORT
// ******************************************************************************************
    
    $ar_email_to = array();
    
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u){
        $ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
    }
    
    $ar_email_json = acs_je($ar_email_to);
    
    ?>

<html>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}

    table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}

    td.titolo{font-weight:bold;}
  
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   tr.su {border-top: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray;padding-top: 10px}
   tr.giu {border-bottom: 1px solid gray; border-left: 1px solid gray; border-right: 1px solid gray; padding-bottom: 10px}
   .number{text-align: right;}

   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.liv3 td{background-color: #333333; font-weight: bold; color: white; font-size: 0.9em;}
   tr.liv2 td{background-color: #cccccc;}   
   tr.liv1 td{background-color: #ffffff;}   
   tr.liv_data th{background-color: #333333; color: white;}
   tr.ag_liv_data th{background-color: #333333; color: white; font-size: 15px;}  
   table.int0{margin-bottom: 10px;}
   table.int0 td{border: 0px; font-size: 18px; font-weight: bold; }
   h2.acs_report_title{font-size: 18px; padding: 10px;}
   
	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     

</style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);

    Ext.onReady(function() {	
	});  

  </script>

</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content'>


<?php


	$todo   = $_REQUEST['todo'];
	$lingua = $_REQUEST['lingua'];
	$sql_where = "";
	$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
	
	$desc = get_TA_sys('VULN', $lingua);
	$d_lingua = strtolower($desc['text']);
	
	//filtro in base all'utente (entry)
	if ($js_parameters->entry_flt == 1){
	    $sql_where .= " AND ASUSAT = " . sql_t_trim($auth->get_user()) . " ";
	}
   
	if(isset($todo) && strlen($todo) > 0)
	    $sql_where .= sql_where_by_combo_value('TA_ATTAV.TAKEY1', $todo);
	else
        $sql_where .= " AND TA_ATTAV.TAKEY1 LIKE  'TRAD%'";
	        
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.*, ARART, ARDART
            FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT_OPEN
            INNER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
            ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1 AND TA_ATTAV.TATAID = 'ATTAV'
            INNER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR
            ON ATT_OPEN.ASDT = AR.ARDT AND ATT_OPEN.ASDOCU = AR.ARART
            WHERE ASDT = '{$id_ditta_default}'
            {$sql_where}  AND ASFLRI <> 'Y'
    ";
     
 
    if(isset($lingua) && strlen($lingua) > 0){
        $sql_i = "SELECT ALDAR1, ALDAR2, ALDAR3, ALDAR4
                  FROM {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
                  WHERE ALDT = '{$id_ditta_default}' AND ALLING = '{$lingua}'
                  AND ALART = ?";
        
        $stmt_i = db2_prepare($conn, $sql_i);
        echo db2_stmt_errormsg();
    }
	      
	        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();

    $ar = array();
	while ($row = db2_fetch_assoc($stmt)) {
	    $nr = array();
	    
	    $trad = $row['TANAZI']; //CODICE LINGUA DEFINITO IN ATTAV
	    
	    $sql_t = "SELECT RRN(AL) AS RRN, ALDAR1, ALDAR2, ALDAR3, ALDAR4
	    FROM {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
	    WHERE ALDT = '{$id_ditta_default}' AND ALLING = '{$trad}'
	    AND ALART = ?
	    ";
	    
	    $stmt_t = db2_prepare($conn, $sql_t);
	    echo db2_stmt_errormsg();
	    
	    $nr['articolo'] =  $row['ASDOCU'];
	    $nr['flag'] =  $row['ASFLRI'];
	    $nr['prog'] =  $row['ASIDPR'];
	    $nr['todo'] =  $row['TADESC'];
	    $nr['d_art'] = acs_u8e(trim($row['ARDART']));
	    $nr['lingua'] =  $trad;
	    
	    if(isset($lingua) && strlen($lingua) > 0){
	        $result = db2_execute($stmt_i, array($row['ASDOCU']));
	        $row_i = db2_fetch_assoc($stmt_i);
	        $nr['trad_lng'] =  $row_i['ALDAR1'];
	    }
	    $result = db2_execute($stmt_t, array($row['ASDOCU']));
	    $row_t = db2_fetch_assoc($stmt_t);
	    $nr['trad'] =  trim($row_t['ALDAR1']);
	  
	    $ar[] = $nr;
	    
	  
	}
	

  echo "<h2 class='acs_report_title'>{$ar[0]['todo']}</h2> ";
  echo "<p align='right'> Data elaborazione: ". date('d/m/Y  H:i')."</p>";
  echo "<table class=int1>";
         
  echo "<tr class=liv_totale>
        <td class=titolo>Articolo</td>
        <td class=titolo>Descrizione</td>
        <td class=titolo>Traduzione</td>";
    
    if(strlen($lingua)> 0)
    echo "<td class=titolo>Traduzione in {$d_lingua}</td>";
   
  echo "</tr>";
    

  foreach ($ar  as $k0 => $v0){ 
    
        echo "<tr>
        <td>{$v0['articolo']}</td>
        <td>{$v0['d_art']}</td>
        <td>{$v0['trad']}</td>";
        if(strlen($lingua)> 0)
            echo "<td>{$v0['trad_lng']}</td><tr>";
        
    
    
}?>

 </table>

</div>
</body>
</html>
<?php 
  
