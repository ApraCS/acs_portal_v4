<?php

require_once("../../config.inc.php");

$main_module = new DeskTranslations();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_avanzamento_segnalazione_arrivi_json'){
    
    $result = $desk_art->exe_avanzamento_segnalazione_arrivi_json(acs_m_params_json_decode());
    echo $result;
    exit();
}


// ******************************************************************************************
// RECUPERO FLAG RILASCIO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_richiesta_record_rilasciato'){
    
    
    $prog = $m_params->prog;
    
    $sql = "SELECT * FROM {$cfg_mod_DeskArt['file_assegna_ord']} WHERE ASIDPR = ?";
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt, array($prog));
    
    $r = db2_fetch_assoc($stmt);
    $ret = array();
    $ret['success'] = true;
    $ret['ASFLRI'] = $r['ASFLRI'];
    echo acs_je($ret);
    exit;
} //get_json_data


if ($_REQUEST['fn'] == 'exe_save_traduzione'){
    
    $m_params = acs_m_params_json_decode();
    
    if($m_params->multi == 'Y'){
           
        foreach($m_params->list_selected_id as $v){
                
                $sql_s = "SELECT RRN(AL) AS RRN
                          FROM {$cfg_mod_DeskUtility['file_anag_art_trad']} AL
                          WHERE ALDT = '{$id_ditta_default}'
                          AND ALART = '{$v->articolo}'
                          AND ALLING = '{$v->lingua}'";
                $stmt_s = db2_prepare($conn, $sql_s);
                $result = db2_execute($stmt_s);
                $row_s = db2_fetch_assoc($stmt_s);
                
                if($row_s['RRN'] != false && trim($row_s['RRN']) != ''){
                    $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
                            WHERE RRN(AL) = '{$row_s['RRN']}'";
                    
                    $stmt = db2_prepare($conn, $sql);
                    echo db2_stmt_errormsg();
                    $result = db2_execute($stmt);
                    
                }
                
                $ar_ins = array();
                $ar_ins["ALDAR1"] = acs_toDb($m_params->form_values->f_text_1);
                $ar_ins["ALDT"]   = $id_ditta_default;
                $ar_ins["ALART"]  = $v->articolo;
                $ar_ins["ALLING"] = $v->lingua;
                
                
                $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_art_trad']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_ins);
                echo db2_stmt_errormsg($stmt);
                
                
                
            }
        
            
            
            
        
        
    }else{
        if(isset($m_params->rrn) && trim($m_params->rrn) != ''){
            
            $ar_ins = array();
            $ar_ins["ALDAR1"] = acs_toDb($m_params->n_trad);
            /*  $ar_ins["ALDAR2"] = acs_toDb($m_params->form_values->f_text_2);
             $ar_ins["ALDAR3"] = acs_toDb($m_params->form_values->f_text_3);
             $ar_ins["ALDAR4"] = acs_toDb($m_params->form_values->f_text_4);*/
            
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
            WHERE RRN(AL) = '{$m_params->rrn}'
            ";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
        }else{
            
            $ar_ins = array();
            
            $ar_ins["ALDAR1"] = acs_toDb($m_params->n_trad);
            /* $ar_ins["ALDAR2"] = acs_toDb($m_params->form_values->f_text_2);
             $ar_ins["ALDAR3"] = acs_toDb($m_params->form_values->f_text_3);
             $ar_ins["ALDAR4"] = acs_toDb($m_params->form_values->f_text_4);*/
            $ar_ins["ALDT"]   = $id_ditta_default;
            $ar_ins["ALART"]  = $m_params->articolo;
            $ar_ins["ALLING"] = $m_params->lingua;
            
            
            $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_art_trad']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg($stmt);
            
            
        }
        
    }
    
      
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


// ******************************************************************************************
// MAIN TREE DATI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    ini_set('max_execution_time', 300);
    $m_params = acs_m_params_json_decode();
    $sql_where = "";
    $js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());
    //global $cfg_mod_DeskArt;

    
    if(isset($m_params->form_values->f_note) && strlen($m_params->form_values->f_note) > 0){
      $sql_campi_select = " , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_note_anag']} ART_COMM
                             WHERE ART_COMM.RLDT = AR.ARDT AND ART_COMM.RLTPNO = 'AR' 
                             AND ART_COMM.RLRIFE1 = AR.ARART AND ART_COMM.RLRIFE2 = '{$m_params->form_values->f_note}'
                           ) AS NR_ART_COMM";
    }
    
    if(isset($m_params->form_values->f_ciclo) && strlen($m_params->form_values->f_ciclo) > 0)
        $sql_where .= " AND AR.ARESAU <> '{$m_params->form_values->f_ciclo}'";
    
    
    if ($js_parameters->entry_flt == 1){
        $sql_where .= " AND ASUSAT = " . sql_t_trim($auth->get_user()) . " ";
    }
    
    if(isset($m_params->form_values->f_todo) && strlen($m_params->form_values->f_todo) > 0)
        $sql_where .= sql_where_by_combo_value('TA_ATTAV.TAKEY1', $m_params->form_values->f_todo);
    else
        $sql_where .= " AND TA_ATTAV.TAKEY1 LIKE  'TRAD%'";
       
    $sql = "SELECT ATT_OPEN.*, TA_ATTAV.*, ARART, ARDART {$sql_campi_select}
    FROM {$cfg_mod_DeskArt['file_assegna_ord']} ATT_OPEN
    INNER JOIN {$cfg_mod_DeskArt['file_tabelle']} TA_ATTAV
    ON ATT_OPEN.ASDT = TA_ATTAV.TADT AND ATT_OPEN.ASCAAS = TA_ATTAV.TAKEY1 AND TA_ATTAV.TATAID = 'ATTAV'
    INNER JOIN {$cfg_mod_DeskUtility['file_anag_art']} AR
    ON ATT_OPEN.ASDT = AR.ARDT AND ATT_OPEN.ASDOCU = AR.ARART
  	WHERE ASDT = '{$id_ditta_default}'
  	{$sql_where}  AND ASFLRI <> 'Y' 
   ";
 

  	if(isset($m_params->form_values->f_lingua) && strlen($m_params->form_values->f_lingua) > 0){ 
        $sql_i = "SELECT ALDAR1, ALDAR2, ALDAR3, ALDAR4
        FROM {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
        WHERE ALDT = '{$id_ditta_default}' AND ALLING = '{$m_params->form_values->f_lingua}'
        AND ALART = ?
        ";
        
        $stmt_i = db2_prepare($conn, $sql_i);
        echo db2_stmt_errormsg();
  	}
 
  	
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg();
    $count = 0;
    $ar = array();
    while ($row = db2_fetch_assoc($stmt)) {
        
        $trad = $row['TANAZI']; //CODICE LINGUA DEFINITO IN ATTAV        
        
        $sql_t = "SELECT RRN(AL) AS RRN, ALDAR1, ALDAR2, ALDAR3, ALDAR4
        FROM {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
        WHERE ALDT = '{$id_ditta_default}' AND ALLING = '{$trad}'
        AND ALART = ? 
        ";
        
        $stmt_t = db2_prepare($conn, $sql_t);
        echo db2_stmt_errormsg();
        
        
        //stacco dei livelli
        $cod_liv0 = trim($row['ASCAAS']);
        $cod_liv1 = trim($row['ASDOCU']);
        
        
        $tmp_ar_id = array();
        $ar_r= &$ar;
        
        //CAUSALE
        $liv =$cod_liv0;
        $tmp_ar_id[] = $liv;
        if (!isset($ar_r["{$liv}"])){
            $ar_new = $row;
            $ar_new['children'] = array();
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] = "<b>".$row['TADESC']."</b>";
            $ar_new['causale'] = trim($row['ASCAAS']);
            $ar_new['liv'] = 'liv_0';
            
            $ar_r["{$liv}"] = $ar_new;
        }
        $ar_r = &$ar_r["{$liv}"];
        
        //ARTICOLO
        $liv=$cod_liv1;
        $ar_r = &$ar_r['children'];
        $tmp_ar_id[] = $liv;
        if(!isset($ar_r[$liv])){
            $ar_new = $row;
            
            $ar_new['id'] = implode("|", $tmp_ar_id);
            $ar_new['task'] =  $row['ASDOCU'];
            $ar_new['articolo'] =  trim($row['ASDOCU']);
            $ar_new['flag'] =  $row['ASFLRI'];
            $ar_new['prog'] =  $row['ASIDPR'];
            $ar_new['d_art'] = acs_u8e(trim($row['ARDART']));
            $ar_new['lingua'] =  $trad;
            $ar_new['note'] =  $row['NR_ART_COMM'] > 0 ? true : false;
            $ar_new['nr'] = $count++;
            if(isset($m_params->form_values->f_lingua) && strlen($m_params->form_values->f_lingua) > 0){
                $result = db2_execute($stmt_i, array($row['ASDOCU']));
                $row_i = db2_fetch_assoc($stmt_i);
                $ar_new['trad_lng'] =  $row_i['ALDAR1'];
            }
            $result = db2_execute($stmt_t, array($row['ASDOCU']));
            $row_t = db2_fetch_assoc($stmt_t);
            $ar_new['trad'] =  trim($row_t['ALDAR1']);
            $ar_new['rrn_trad'] =  $row_t['RRN'];
            
      
            $ar_new['liv'] = 'liv_1';
            $ar_new['leaf'] = true;
            $ar_r["{$liv}"] = $ar_new;
        }
        
    }
    
   
    
    foreach($ar as $kar => $r){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
    echo acs_je(array('success' => true, 'children' => $ret));
    exit;
}


if ($_REQUEST['fn'] == 'open'){
    $m_params = acs_m_params_json_decode();
    
?>

{"success":true, "items": [

        {
        xtype: 'treepanel' ,
        multiSelect: true,
        cls: 's_giallo',
        title: 'To Do list' ,
	    tbar: new Ext.Toolbar({
	            items:['<b> Gestione attivit&agrave; di manutenzione base dati articoli</b>', '->',
    	            /* {iconCls: 'icon-gear-16',
    	             text: 'CAUSALI', 
		           		handler: function(event, toolEl, panel){
			           		acs_show_panel_std('acs_gestione_attav.php?fn=open_grid', 'panel_gestione_attav', "");
			          
		           		 }
		           	 }*/
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	 
	        
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,     
	        plugins: [
                  Ext.create('Ext.grid.plugin.CellEditing', {
                    clicksToEdit: 1,
                    listeners:{
			        afteredit: function(cellEditor, context, eOpts){
			               
				        	var value = context.value;
				        	var grid = context.grid;
				        	var record = context.record;	
				        	//console.log(grid);
				        	//console.log(grid.columns[3].dataIndex);
			        	   	Ext.Ajax.request({
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_traduzione',
					            method: 'POST',
			        		    jsonData: {
					      			   n_trad : value,
									   rrn: record.get('rrn_trad'),
									   articolo : record.get('articolo'),
									   lingua : record.get('lingua') 					      			  
					      			   }, 					            
					            success: function ( result, request) {
					                var jsonData = Ext.decode(result.responseText);
					                record.set('trad', value);
					   						                															
					            },
					            failure: function (result, request) {
					            }
					        });
			        	}
					 }
                  })
              ],       
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['id', 'nr', 'task', 'liv', 'd_art', 'um', 'flag', 'prog', 'articolo', 'imm', 'causale',
                             'ut_ins', 'ut_ass', 'scadenza', 'memo', 'trad_lng', 'trad', 'rrn_trad', 'lingua', 'note'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						reader: {
                            root: 'children'
                        } ,
                        extraParams: {
	                      form_values: <?php echo acs_je($m_params->form_open); ?>,
                      }
                    , doRequest: personalizza_extraParams_to_jsonData           				
                    }

                }),
                <?php  $note = "<img src=" . img_path("icone/48x48/comment_edit.png") . " height=20>"; ?>
    	    			
            columns: [{xtype: 'treecolumn', 
        	    		text: 'Causale/Articolo', 	
        	    		width : 280,
        	    		dataIndex: 'task'
        	    		},
        	    		{text: 'Descrizione', flex:1, dataIndex: 'd_art'},	
        	    		{
        				    text: '<?php echo $note; ?>',
        				    width: 32, 
        				    tooltip: 'Blocco note',
        				    tdCls: 'tdAction',  
        				    dataIndex : 'note',       		       		        
        					renderer: function(value, metaData, record){
            					if(record.get('leaf')){
            						if (record.get('note') == true) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=18>';
            			    		if (record.get('note') == false) return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=18>';
            			    	    if (record.get('note') == 'N') return '';    			    	   
            			    	}    		
        			    	}
        			    },
        	    		{text: 'Traduzione', 
        	    		flex:1, 
        	    		dataIndex: 'trad',
						renderer: function (value, metaData, record, row, col, store, gridView){
						    if (record.get('liv') == 'liv_1')
							  metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';
						    return value;
						  },	        	    		        	    		
        	    		editor: {
			                xtype: 'textfield',
			                allowBlank: true
			            }}	
			         <?php if(isset($m_params->form_open->f_lingua) && strlen($m_params->form_open->f_lingua) > 0){ 
			             $cod  = $m_params->form_open->f_lingua;
			             $desc = get_TA_sys('VULN', $cod);
			             ?>
			        , {text: 'Traduzione in <?php echo strtolower($desc['text']); ?>', flex:1, dataIndex: 'trad_lng'},
			         <?php }?>
        	    		
    	    ],
    	     listeners: {
	         
	         itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     row = rec.data;
					     
					     id_selected = grid.getSelectionModel().getSelection();
					     list_selected_id = [];
					     for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push({id: id_selected[i].get('id'), 
									prog: id_selected[i].get('prog'), 
									lingua: id_selected[i].get('lingua'),
									articolo: id_selected[i].get('articolo'),
									trad: id_selected[i].get('trad')
									});
									
									
							 if (rec.get('liv') == 'liv_0'){
							 
							voci_menu.push({
    			      			text: 'Report ToDo',
    			    			iconCls: 'icon-print-16',
    			    			handler: function() {
    			    				window.open('acs_report_todo.php?todo=' + rec.get('causale') + '&lingua=' + <?php echo j($m_params->form_open->f_lingua)?>);
    			    		       }
							 });
									
					     	}
    					     if (rec.get('liv') == 'liv_1'){
    					      
    					    voci_menu.push({
    			      		text: 'Avanzamento/Rilascio attivit&agrave;',
    			    		iconCls: 'icon-arrivi-16',
    			    		handler: function() {
    
      	
    				    		//verifico che abbia selezionato solo righe non gia' elaborate 
    							  for (var i=0; i<id_selected.length; i++){ 
    								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
    									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
    									  return false;
    								  }
    							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('flag', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							        list_selected_id: list_selected_id, 
							        grid_id: grid.id},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });
					  
					<?php
$causali_rilascio = $desk_art->find_TA_std('RILAV', null, 'N', 'Y'); //recupero tutte le RILAV

foreach($causali_rilascio as $ca) {
     
?>	

			
	if (rec.get('flag')!='Y' 
	&& rec.get('id').split("|")[0] == <?php echo j(trim($ca['id'])); ?>){ 		  
	
	voci_menu.push({
	      		text: <?php echo j($ca['text']); ?>,
	    		iconCls: 'iconAccept',
	    		handler: function() {

				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('flag') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('flag', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {
							           			list_selected_id: list_selected_id, grid_id: grid.id,
												auto_set_causale: <?php echo j(trim($ca['TAKEY2'])); ?>							        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
	    
						    
		
			    }
			  });
			  
			  }
  <?php } ?>
					  
					     }
					     
				   if(id_selected.length > 1){
				     voci_menu.push({
		         		text: 'Inserisci traduzione',
		        		iconCls : 'icon-globe-16',          		
		        		handler: function () {
		        		
		        		 var my_listeners = {
		    		  			afterInsert: function(from_win, trad){
		    		  			  for (var i=0; i<id_selected.length; i++)
		    		  			    id_selected[i].set('trad', trad)  
		    		  			  
		    		  			   from_win.close();
		    		  			   }
			    			};
		        		
		        		    acs_show_win_std('Traduzione articoli', 
	    				   '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_al', 
				    	   {list_selected_id: list_selected_id, multi : 'Y'}, 400, 150, my_listeners, 'icon-globe-16');
					    
			            }
		    		});
		    		
		    		
					}
					
					if (rec.get('liv') == 'liv_1'){
						voci_menu.push({
			         		text: 'Modifica traduzione',
			        		iconCls : 'icon-blog_compose-16',          		
			        		handler: function () {
			        		     	        		
			        			my_listeners = {
		        					afterConfirm: function(list, from_win){	
		        					id_selected = grid.getSelectionModel().getSelection();	
		        					console.log(list);
		        					for (var i=0; i<id_selected.length; i++){
		        					 for(var a=0; a<list.length; a++) {
		        					   if(id_selected[i].data.articolo == list[a].cod)
		        					      id_selected[i].set('trad', list[a].n_trad); 
		        					       
								         }
		        					} 
		        					  
		        						from_win.close();
		        						
						        		}
				    				};
				    				
				    				list_selected_art = [];	        
			        		     	        
			        		      for (var i=0; i<id_selected.length; i++) {
							        list_selected_art.push(id_selected[i].get('articolo'));
									}
			        		
					    		  acs_show_win_std('Modifica traduzione', 'acs_modifica_traduzione.php?fn=open_mod', {list_selected_id:list_selected_art, lingua : rec.get('lingua')}, 1000, 300, my_listeners, 'icon-blog_compose-16');
				                }
			    		});	
			    		
			    		}
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
			    	
			    	,celldblclick: {
	           
		              fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		var col_name = iView.getGridColumns()[iColIdx].dataIndex,				  	
					  		rec = iView.getRecord(iRowEl),
					  		grid = this;
					  		
					  		<?php if(isset($m_params->form_open->f_note) && strlen($m_params->form_open->f_note) > 0) {?>
					  		if(col_name == 'note'){
					  		   acs_show_win_std( 'Annotazioni articolo ' + rec.get('articolo') + ' blocco note ' + <?php echo j($m_params->form_open->f_note); ?>, 
		    					'../desk_utility/acs_anag_art_note.php?fn=open_tab', 
		    					{ c_art: rec.get('articolo'), 
		    					  bl: <?php echo j($m_params->form_open->f_note); ?>, 
		    					}, 800, 400,  {
	        						afterSave: function(from_win, src){
	    			  			  	  rec.set('note', src.note);
	    			  			  	  from_win.destroy();
 							     	}
	        				   }, 'iconCommGray');
																		   		
					   		}	
					   		<?php }?>
					  		
					  		
				   }}
	        
				  
				 },
				 
				 viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if(record.get('liv') == 'liv_1'){
		              if(record.get('nr') % 2 == 0)
        		     return ' segnala_riga_pari';
        		    }
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('flag') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }	
    	    
    	    
    	}	
	 	
	]  	
  }


<?php 
exit;
}

if($_REQUEST['fn'] == 'open_form'){
    
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
        $ar_users_json = acs_je($user_to);
    
  ?>
    
    {"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
        	frame: true,
            title: '',
            flex : 1,
            items: [{
						name: 'f_todo',
						xtype: 'combo',
						fieldLabel: 'To Do',
						labelWidth : 120,
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: true,	
					   	flex : 1, 	
					   	anchor: '-15',											
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php
							  
							     echo acs_ar_to_select_json($main_module->find_TA_std('ATTAV', null, 'Y', 'N', null, null, null, 'N', 'N', "AND TAKEY1 LIKE  'TRAD%'"), "");
							       ?>	
							    ] 
						}						 
            	}, {name: 'f_lingua',
            		xtype: 'combo',
            		fieldLabel: 'Lingua da esporre',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',						
            		emptyText: '- seleziona -',
                    anchor: '-15',
                    labelWidth : 120,
                    flex : 1,
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [ 
            		        <?php echo acs_ar_to_select_json($cfg_mod_DeskUtility['ar_lingue'], ''); ?>
            		    ]
            		}
            			
            	 },
            	 {name: 'f_note',
            		xtype: 'combo',
            		fieldLabel: 'Blocco note',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',						
            		emptyText: '- seleziona -',
                    anchor: '-15',
                    labelWidth : 120,
                    flex : 1,
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [ 
            		        <?php echo acs_ar_to_select_json(find_TA_sys('NUSN', null, 'AR', '*ST', null, null, 0, '', 'Y'), ''); ?>
            		    ]
            		}
            			
            	 },
            	  {name: 'f_ciclo',
            		xtype: 'combo',
            		fieldLabel: 'Ciclo di vita da escludere',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',						
            		emptyText: '- seleziona -',
                    anchor: '-15',
                    labelWidth : 120,
                    flex : 1,
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [ 
            		    <?php echo acs_ar_to_select_json($desk_art->find_TA_std('AR012', null, 'N', 'N', null, null, null, 'N', 'Y'), '');  ?> 	
            		    ]
            		}
            			
            	 }
					   
            ],
            
			buttons: [	
			<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "TRANSLATION");  ?>				
				{
	            text: 'Apri',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',	                     
	             handler: function() {
	                var form = this.up('form').getForm();
	                var form_values = form.getValues();
	                var fornitore = form_values.f_fornitore;
	                        
		               if (form.isValid()){	    
		               
		               acs_show_panel_std('<?php echo $_SERVER['PHP_SELF']; ?>?fn=open', null, {
        		            	form_open: form.getValues()
    		            	}, null, null);
		                           	                
			              this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        
	        }
	        ]            
            
           
}
	
]}
    
    
  <?php   
  exit;  
}

if($_REQUEST['fn'] == 'open_al'){?>

{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: false,
	            autoScroll : true,
	            title: '',
	            buttons: [{
			            text: 'Salva',
			            //iconCls: 'icon-button_blue_play-32',
			            scale: 'large',
			            handler: function() {
			                var m_form = this.up('form');
			            	var form = this.up('form').getForm();
			            	var form_values = form.getValues();
							var loc_win = this.up('window');
							
						  	Ext.Ajax.request({
					            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_traduzione',
					            method: 'POST',
			        		    jsonData: {
			        		           multi : 'Y',
					      			   form_values : form_values,
									   list_selected_id : <?php echo acs_je($m_params->list_selected_id); ?> 					      			  
					      			   }, 					            
					            success: function ( result, request) {
					                var jsonData = Ext.decode(result.responseText);
					                loc_win.fireEvent('afterInsert', loc_win, form_values.f_text_1);
					   						                															
					            },
					            failure: function (result, request) {
					            }
					        });
							 
							
					
			            }
			         }],   		
	            items: [
	            
	                   {
    						name: 'f_text_1',
    						xtype: 'textfield',
    						fieldLabel: '',
    						anchor: '-5',
    					    maxLength: 80,					    							
    					}
	            ]}
]}
    
<?php 
}