<?php

require_once("../../config.inc.php");

$main_module = new DeskTranslations();
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_conferma_mod'){
    
    $list_rows = $m_params->list_selected_id;
    $ar_upd = array();
    
    foreach($list_rows as $v){
        
        if($v->rrn == '' || is_null($v->rrn)){
            
            $ar_upd['ALDT'] 	= $id_ditta_default;
            $ar_upd['ALART']    = $v->cod;
            $ar_upd['ALLING'] 	= $m_params->lingua;
            $ar_upd['ALDAR1'] 	=  acs_toDb($v->n_trad);
            
            $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_art_trad']}(" . create_name_field_by_ar($ar_upd) . ") VALUES (" . create_parameters_point_by_ar($ar_upd) . ")";
                            
        }else{
          
            $ar_upd["ALDAR1"] = acs_toDb($v->n_trad);
            
            $sql = "UPDATE {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(AL) = '{$v->rrn}'";
            
        }
       
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_upd);
        echo db2_stmt_errormsg($stmt);
        
        
        
    }
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
    $list_rows = $m_params->list_selected_id;
    $where_art = sql_where_by_combo_value('ARART', $list_rows);
    
    if($m_params->p_lng != ''){
        if($m_params->p_lng == 'STD'){
            $sql_select = " , AR.ARDART AS N_TRAD";
        }else{
            $sql_select = " , AL2.ALDAR1 AS N_TRAD";
            $sql_join = "LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_art_trad']} AL2
                        ON AL2.ALDT = AL.ALDT AND AL2.ALART = AL.ALART AND AL2.ALLING = '{$m_params->p_lng}'";
        }
     }    
    
    $sql = "SELECT RRN(AL) AS RRN, AR.ARART, AL.ALLING, AL.ALDAR1 {$sql_select} 
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_art_trad']} AL
             ON AL.ALDT = AR.ARDT AND AL.ALART = AR.ARART AND AL.ALLING = '{$m_params->lingua}'
            {$sql_join}
            WHERE ARDT = '{$id_ditta_default}'
            {$where_art}";
                 
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    echo db2_stmt_errormsg($stmt);
    while ($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        $nr['cod']     = trim($row['ARART']);
        $nr['trad']    = utf8_decode(trim($row['ALDAR1']));
        if($m_params->p_lng != '' && trim($row['ALDAR1']) == '')
            $nr['trad']  = utf8_decode(trim($row['N_TRAD']));
        else
            $nr['trad']  = utf8_decode(trim($row['ALDAR1']));
        $nr['len']     = strlen($nr['trad']);
        if($m_params->p_lng != '' && trim($row['ALDAR1']) == '')
            $nr['n_trad']  = utf8_decode(trim($row['N_TRAD']));
        else 
            $nr['n_trad']  = utf8_decode(trim($row['ALDAR1']));
        $nr['n_len']   = strlen($nr['n_trad']);
        $nr['lingua']  = trim($row['ALLING']);
        $nr['rrn']  = $row['RRN'];
        $ar[] = $nr;
         
        
    }
    
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_mod'){
    
    ?>
    {"success":true, "items": [
    
 {
				xtype: 'grid',
				title: '',
		        loadMask: true,	
		        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1
		          })
		      	],
		        store: {
				xtype: 'store',
				autoLoad:true,
				proxy: {
					   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
					   method: 'POST',								
					   type: 'ajax',

				       actionMethods: {
				          read: 'POST'
				        },
				        extraParams: {
							 list_selected_id: <?php echo acs_je($m_params->list_selected_id) ?>,
							 lingua : <?php echo j($m_params->lingua); ?>,
							 p_lng : ''
        				},
        				
        				doRequest: personalizza_extraParams_to_jsonData, 
			
					   reader: {
			            type: 'json',
						method: 'POST',						            
			            root: 'root'						            
			        }
				},
			
				fields: [ 'cod', 'trad' , 'len', 'n_trad' , 'n_len', 'lingua', 'rrn']							
							
	}, //store
		

	      columns: [
	
            {
            header   : 'Codice',
            dataIndex: 'cod',
            width : 120
            },
            {
             header   : 'Traduzione',
             dataIndex: 'trad',
             flex: 1,
             tdCls: 'white-space-pre font-monospace',
            },
         	{
             header   : 'Lung.',
             dataIndex: 'len',
             width : 40
            },   {
             header   : 'Nuova traduzione',
             dataIndex: 'n_trad',
             flex: 1,
             tdCls: 'white-space-pre font-monospace',
             renderer: function(value, p, record){
             	   if (record.get('n_len') > 80)
             	   		p.tdCls += ' sfondo_rosso';
             	   return value;
    		 },
             editor: {
	                xtype: 'textfield',
	                allowBlank: true,
	                fieldCls: 'x-form-field font-monospace'	
	            }
	            
            }, 
              {
            header   : 'Lung.',
            dataIndex: 'n_len',
            width : 40
            }
            
     ], listeners: {
     
     	edit: function(editor, e, a, b){
                 
          	grid = editor.grid;
        	record = e.record;	
        	console.log(record.data);
        	new_value =  record.data.n_trad;
        	record.set('n_len', record.data.n_trad.length);
	        if (record.get('n_len') > 80){ 
     	   				acs_show_msg_error('Presenti traduzioni oltre gli 80 caratteri!');
						return value;
     	   			}

         }
     
		 }, 
		 dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                    {
        			name: 'f_sos',
        			xtype: 'textfield',
        			fieldLabel: 'Sostituisci',
        			itemId : 'w_sos',
        			labelWidth : 60,
        			width : 170
    			   },  {
        			name: 'f_con',
        			xtype: 'textfield',
        			fieldLabel: 'Con',
        			itemId : 'w_con',
        			labelWidth : 30,
        			width : 150
        									
        		 	},{
                     xtype: 'button',
                    text: 'Applica',
                    iconCls: 'icon-button_black_play-32',
		            scale: 'large',	      
		            handler: function() {
		             
		               var grid = this.up('grid');
		               var rows = grid.getStore().data.items;
		               var sos = this.up('window').down('#w_sos').getValue();
		               var con = this.up('window').down('#w_con').getValue();
		              
		               id_selected = grid.getSelectionModel().getSelection();	
		                  						
					   list_desc = [];             
		                 for (var i=0; i<rows.length; i++){
		                     if(rows[i].data.n_trad.indexOf(sos) > -1){
		                        var old_trad = rows[i].data.n_trad;
		                        var new_trad = old_trad.replace(sos, con);
		                        var new_len = new_trad.length;
		                        rows[i].set('n_trad', new_trad);
		                        rows[i].set('n_len', new_len);
		                     }
		               
                             }
                         
                             
                             
                        }
		            },
		            
		            {name: 'f_lingua',
            		xtype: 'combo',
            		fieldLabel: 'Lingua da proporre',
            		forceSelection: true,								
            		displayField: 'text',
            		valueField: 'id',						
            		emptyText: '- seleziona -',
                    anchor: '-15',
                    width : 250,
            		store: {
            			editable: false,
            			autoDestroy: true,
            		    fields: [{name:'id'}, {name:'text'}],
            		    data: [ 
            		     {id : 'STD', text : 'Standard'},
            		     <?php 
            		      $lingua = trim($m_params->lingua);
            		      $ar_lng = $cfg_mod_DeskUtility['ar_lingue'];
            		      foreach($ar_lng as $k => $v){
            		          if($lingua == $v['id'])
            		              unset($ar_lng[$k]);
            		      }      
            		      echo acs_ar_to_select_json($ar_lng, ''); 
            		     
            		     ?>
            		    ]
            		},
            		listeners : {
            				'change': function(field) {
      				    		var grid = this.up('window').down('grid');
      				    		grid.store.proxy.extraParams.p_lng = field.value;
      				    		grid.getStore().load();

            		}
             			
            		
            		
            		}	
            	 }
		            
		            , '->',
		            {
                     xtype: 'button',
                    text: 'Reset',
		            iconCls: 'icon-button_blue_repeat-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                   		var grid = this.up('grid');
                   		this.up('window').down('#w_sos').setValue('');
                   		this.up('window').down('#w_con').setValue('');
                   		grid.getStore().load();
                	
			
			            }

			     },  {
                     xtype: 'button',
                    text: 'Conferma',
                    itemId : 'b_confirm',
		            iconCls: 'icon-save-32',
		            scale: 'large',	                     

		           handler: function() {
		              
                      var grid = this.up('grid');
                      var loc_win = this.up('window');
                            
                      var id_selected = grid.getStore().getRange();
                      
					  var list_selected_id = [];
					     for (var i=0; i<id_selected.length; i++)
							list_selected_id.push(id_selected[i].data);
                      
		              for (var i=0; i<id_selected.length; i++) {
		              	if(id_selected[i].get('n_len') > 80){
		              	 acs_show_msg_error('Presenti traduzioni oltre gli 80 caratteri!');
	 					 return;
	 					} 
		              }
		              
		    	       loc_win.fireEvent('afterConfirm', list_selected_id, loc_win);
	            
					   Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_mod',
						        method     : 'POST',
			        			jsonData: {
			        				list_selected_id : list_selected_id,
			        				lingua : <?php echo j(trim($m_params->lingua)); ?>
								},							        
						        success : function(result, request){
						           var jsonData = Ext.decode(result.responseText);
						           loc_win.fireEvent('afterConfirm', list_selected_id, loc_win);								        
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });	
			
			            }

			     }
			     ]
		   }]
		 
		  ,viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			           
			           return '';																
			         }   
			    }
			       


}
    
    ]}

<?php 
exit;
}