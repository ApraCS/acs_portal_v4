<?php
	
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors','On');
	
	require_once("../../config.inc.php");
	require_once("mod.DeskTranslations.php");
	$_module_descr = "Translations";	
	
	$main_module = new DeskTranslations(array('abilita_su_modulo' => 'DESK_VEND'));
			
?>



<html>
<head>
<title>ACS Translation</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">
.icon-folder_search-64 {background-image: url("../../images/icone/64x64/folder_search.png") !important;}


/* NASCONDO ICONE */
#main-site-search, #main-site-tools, #main-site-help {	visibility: hidden; }


</style>



<script type="text/javascript" src="../../../extjs/ext-all.js"></script>
<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>

<script src=<?php echo acs_url("js/acs_js.js") ?>></script>



<script type="text/javascript">

	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {       
        }	    
	}); Ext.Loader.setPath('Ext.ux', '../../ux');


    Ext.require(['*'
                 , 'Ext.ux.grid.FiltersFeature'                 
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
    			 , 'Ext.ux.grid.Printer'
      			 , 'Ext.ux.RowExpander' 
                 ]);



    Ext.onReady(function() {

        Ext.QuickTips.init();

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            layout: 'border',
            items: [ 
                    
	            Ext.create('Ext.Component', {
	                region: 'north',
	                id: 'page-header',
	                height: 110, // give north and south regions a height
	                contentEl: 'header'
	            }),
            
	            Ext.create('Ext.tab.Panel', {
	            	id: 'm-panel',
	        		cls: 'supply_desk_main_panel',            	            	
	                region: 'center', // a center region is ALWAYS required for border layout
	                deferredRender: false,
	                activeTab: 1,     // first tab initially active
	                items: [

								                      
	                             					                                                
	                ]
	            })

            ]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------
      	
        if (Ext.get("bt-arrivi") != null){
           	Ext.QuickTips.register({
       			target: "bt-arrivi",
       			title: 'To Do List',
       			text: 'Gestione attivit&agrave; di manutenzione base dati articoli'
       		});            
   	        Ext.get("bt-arrivi").on('click', function(){    
   	   	      	acs_show_win_std('ToDo Traduzioni', 'acs_panel_todolist.php?fn=open_form', null, 400, 250, null, 'icon-home-16');                 
   	        	//acs_show_panel_std('acs_panel_todolist.php?fn=open');
   	     		});	    
           }



       	acs_show_win_std('ToDo Traduzioni', 'acs_panel_todolist.php?fn=open_form', null, 400, 250, null, 'icon-home-16');        
 


	//RITORNO ALLA HOME PAGE
         if (Ext.get("main-site-home") != null){

         	Ext.QuickTips.register({
    			target: "main-site-home",
    			title: 'Home Page',
    			text: 'Chiude il desktop e torna alla pagina iniziale '
    		});         
         }

        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
