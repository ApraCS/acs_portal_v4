<?php
require_once "../../config.inc.php";

if ($_REQUEST['fn'] == 'get_data'){
    
    
    $sql = "SELECT distinct CFCD, CFRGS1, CFRGS2, CFLOC1, CFPROV, CFNAZ, CFFLG3
    FROM {$cfg_mod_Spedizioni['file_anag_cli']} {$join_riservatezza}
    WHERE (UPPER(
    REPLACE(REPLACE(CONCAT(CFRGS1, CFRGS2), '.', ''), ' ', '')
    )" . sql_t_compare(strtoupper(strtr($_REQUEST['query'], array(" " => "", "." => ""))), 'LIKE') . "
 				
    OR CFCD " . sql_t_compare($_REQUEST['query'], 'LIKE') .")  
    AND CFDT = " . sql_t($id_ditta_default) . " AND CFTICF = '{$_REQUEST['cod']}'
 				order by CFRGS1, CFRGS2
 				";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while ($row = db2_fetch_assoc($stmt)) {
        
        if (trim($row['CFNAZ']) == 'ITA' || trim($row['CFNAZ']) == 'IT' || trim($row['CFNAZ']) == ''){
            $out_loc = acs_u8e(trim($row['CFLOC1']) . " [" . trim($row['CFPROV']) . "]");
        } else {
            $des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
            $out_loc = "[".trim($row['CFNAZ']) . "] " . trim($des_naz[0]['text']);
        }
        
        $sosp = trim($row['CFFLG3']);
        if($sosp == 'S')
           $color = "#F9BFC1";
        else $color = "";

        $ret[] = array("id"=>trim($row['CFCD']), "text" =>trim($row['CFRGS1']), "loca" => $out_loc, "color" => $color);
   }
    
    echo acs_je($ret);
    exit;
    
    
}