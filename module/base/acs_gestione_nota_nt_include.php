<?php

function _get_nota_nt_config($chiave, $nt_config){
    global $conn, $id_ditta_default;
    
    $nt_config = acs_ar_to_object($nt_config); //forzo a object, potrebbe essere array
    
    if (!isset($nt_config->NTDT))
        $ditta = $id_ditta_default;
    else
        $ditta = $nt_config->NTDT;
    
    $sql = "SELECT RRN(NT) AS RRN, NT.* FROM {$nt_config->file} NT
                WHERE NTDT = ? AND NTTPNO = ? AND NTKEY1 = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($ditta, $nt_config->NTTPNO, $chiave));
    echo db2_stmt_errormsg();
    $row = db2_fetch_assoc($stmt);
    return $row;
}

function _ha_nota_nt_row($row_nt){
    if ($row_nt) return 1;
    return 0;
}

function _ha_nota_nt_config($chiave, $nt_config){
    $row_nt = _get_nota_nt_config($chiave, $nt_config);
    return _ha_nota_nt_row($row_nt);
}