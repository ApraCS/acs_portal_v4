<?php

require_once "../../config.inc.php";
$m_params = acs_m_params_json_decode();


//********************************************************************
// window per modifica domanda/risposta
//********************************************************************
if ($_REQUEST['fn'] == 'open_win'){
//********************************************************************
    $m_params_dup = $m_params;
    $m_params_dup->parentValue = $m_params->dom_c;
    
    $row_dom = get_TA_sys('PUVR', $m_params->dom_c, null, null, null, null, 1);
    $dom_d = $row_dom['text'];
    $type_variabile = $row_dom['tarest'];
    $output_df = "<b>[".trim($m_params->dom_c)."] ".$dom_d. "</b>";
    
    $proc = new ApiProc();
    
    $initialData_dom = _get_elenco_dom($m_params->file_TA);
   
    if (isset($m_params->dom_c) && strlen($m_params->dom_c) > 0)
        $initialData_ris = _get_elenco_ris($m_params->file_TA, $m_params->dom_c, $m_params->tipo_c, $m_params->tipo_opz_risposta);
    else 
        $initialData_ris = array();
    
 //   $initialData_dom = _get_elenco_dom($m_params->file_TA);
    
    $ar_componenti = array(); 

    if (isset($m_params->tipo_opz_risposta->output_domanda) && $m_params->tipo_opz_risposta->output_domanda == true){
        
        $ar_componenti[] = extjs_combo(array(
            'fieldLabel' => 'Variabile',
            'name' => 'dom_c',
            'url' => "{$_SERVER['PHP_SELF']}?fn=get_json_data_dom",
            'extraParams' => $m_params,
            //'initialData' => array(array('id' => $m_params->dom_c, 'text' => "[{$m_params->dom_c}] {$m_params->dom_d}")),
            'initialData'  => $initialData_dom,
            'initialValue'=> $m_params->dom_c,
            'queryMode'     => 'local',
            'onChangeCallFormFunction' => 'refreshForm',
            //'onChangeReset' => 'ris_c',
            'store_fields'  => array('id', 'text', 'sosp', 'tarest')
            ));
 
     }else{
          $ar_componenti[] =  array(
             'xtype' => 'displayfield',
             'fieldLabel' => 'Variabile',
             'value' => $output_df
         );
          
          $ar_componenti[] =  array(
              'xtype' => 'hiddenfield',
              'name' => 'dom_c',
              'value' => $m_params->dom_c
          );
       
     }
    
   
    
    
    if (isset($m_params->risposta_non_obbligatoria))
        $risposta_non_obbligatoria = $m_params->risposta_non_obbligatoria;
    else
        $risposta_non_obbligatoria = false;
    
    //se richiesto aggiungo il combo per la selezione avanzata delle risposte
        if (isset($m_params->tipo_opz_risposta) && isset($m_params->tipo_opz_risposta->opzioni)){
        
        $initialData_f_tipo = $m_params->tipo_opz_risposta->opzioni;
        
        $ar_componenti[] =  extjs_combo(array(
                                    'fieldLabel' => 'Tipo valore',
                                    'name' => 'f_tipo',
                                    'initialData'  => $initialData_f_tipo,
                                    'initialValue' => trim($m_params->tipo_c),
                                    'onChangeReset' => 'ris_c',
                                    'queryMode'     => 'local',
                                    'store_fields'  => array('id', 'text', 'sosp', 'tarest', 'color'),
                                    'allowBlank'    => $risposta_non_obbligatoria
                                ));
    }
    
    
    if($type_variabile == 'N'){
        $h_val = true;
        $h_num = false;
    }else{
        $h_val = false;
        $h_num = true;
    }
        
    $ar_componenti[] =  extjs_combo(array(
                                'fieldLabel' => 'Valore',
                                'name' => 'ris_c',
                                'url' => "{$_SERVER['PHP_SELF']}?fn=get_json_data_ris",
                                'extraParams' => $m_params_dup,
                                //'initialData' => array(array('id' => $m_params->ris_c, 'text' => "[{$m_params->ris_c}] {$m_params->ris_d}")),
                                'initialData'  => $initialData_ris,
                                'initialValue'=> $m_params->ris_c,
                                'queryMode'     => 'local',
                                'allowBlank'    => true,
                                'hidden' => $h_val,
                                'store_fields'  => array('id', 'text', 'sosp', 'tarest', 'color'),
                                //'tpl' => extjs_code($tpl)
                                ));
    
        $ar_componenti[] = array(
            'fieldLabel' => 'Valore numerico',
            'xtype' => 'numberfield', 'name' => 'f_van_n',
            'hidden' => $h_num,
            'value' => trim($m_params->ris_c)
        );
        
        
    
    
    
    
    $c = new Extjs_Form( layout_ar('vbox'), null, array(
        'items' => $ar_componenti
    ));
    
    
    $c->set(array(
        'refreshForm' => extjs_code("
            function(){
               console.log('---- refreshForm ----- ');
               var me = this,
                   form    = me.getForm(),
                   f_var   = form.findField('dom_c'),
                   f_van   = form.findField('ris_c'),
                   f_tp    = form.findField('tipo_c'),
                   f_van_n = form.findField('f_van_n'),
                   var_type = f_var.valueModels[0].get('tarest');
            
               if (var_type == 'N'){
                    f_van.hide();
                    f_van_n.show();
               } else {
                    f_van.show();
                    f_van_n.hide();
            
               }

                f_van.setValue('');
                f_van.getStore().getProxy().extraParams.parentValue = f_var.getValue();
                f_van.getStore().getProxy().extraParams.winFormValues = form.getValues()
                f_van.getStore().load();

            }
        ")
    ));
    
    
   // $c->add_button(array('xtype' => 'tbfill'));
    $c->add_buttons(array(
        array(
            'xtype' => 'button',
            'text' => 'Elimina',
            'scale' => 'large',
            'iconCls' => 'icon-sub_red_delete-32',
            'handler' => extjs_code("
            function(){
               var me = this,
                    loc_form = me.up('form').getForm(),
                    loc_win  = me.up('window'); 
                    dom_c  =  loc_form.findField('dom_c');
                    if(dom_c.xtype == 'combo')
                        var_type = dom_c.valueModels[0].get('tarest');
                
                var outValues = [];
                outValues['f_tp'] = '';
                outValues['f_van'] = '';
                outValues['f_van_n'] = '';
                outValues['f_var'] = '';
                outValues['var_type'] = '';
                outValues.dom_d = '';
                outValues.ris_d = '';
				loc_win.fireEvent('afterSave', loc_win, outValues);
                
            }
        ")
        ),
       array('xtype' => 'tbfill'),
       array( 
        'xtype' => 'button',
        'text' => 'Conferma',
        'iconCls' => 'icon-sub_blue_accept-32',
        'handler' => extjs_code("
            function(){
                var me = this,
                    loc_form = me.up('form').getForm(),
                    loc_win  = me.up('window'); 
                    dom_c  =  loc_form.findField('dom_c');
                    if(dom_c.xtype == 'combo')
                        var_type = dom_c.valueModels[0].get('tarest');
                       
                var outValues = loc_form.getValues();
                if(dom_c.xtype == 'combo')
                    outValues['var_type'] = var_type;
                outValues.dom_d = loc_form.findField('dom_c').rawValue;
                outValues.ris_d = loc_form.findField('ris_c').rawValue;
            
               if (loc_form.isValid()){
					loc_win.fireEvent('afterSave', loc_win, outValues);
                }
            }
        ")
       )     
    ), 'bottom');
    
    
    
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => $c->code()
    ));
    
    
    exit;
}

//********************************************************************
// decodifica domanda/risposta
//********************************************************************
if ($_REQUEST['fn'] == 'decod'){
//********************************************************************

  $ar_output = decod_dom_ris($m_params->file_TA, $m_params->dom_c, $m_params->ris_c, $m_params->tipo_c, $m_params->tipo_opz_risposta);

  echo acs_je(array('success' => true, 'dom_c' => $m_params->dom_c, 'dom_d' => $ar_output['dom_d'], 'ris_c' => $m_params->ris_c, 'ris_d' => $ar_output['ris_d'], 'output' => $ar_output['output']));
exit;
}


//********************************************************************
// recupero elenco domande
//********************************************************************
if ($_REQUEST['fn'] == 'get_json_data_dom'){
//********************************************************************
    $ret = array();
    $ret = _get_elenco_dom($m_params->file_TA);
    
    echo acs_je($ret);
    exit;
}


//********************************************************************
// recupero elenco risposte
//********************************************************************
if ($_REQUEST['fn'] == 'get_json_data_ris'){
//********************************************************************
    $ret = array();    
    $ret = _get_elenco_ris($m_params->file_TA, $m_params->winFormValues->dom_c, $m_params->winFormValues->f_tipo, $m_params->tipo_opz_risposta);

/*    
    if (!isset($m_params->winFormValues->f_tipo) || $m_params->winFormValues->f_tipo == ''){
        //decodifica da risposta a domanda
        
    } else {
        //carico risposte in base a configuratore
        foreach ($m_params->tipo_opz_risposta->opzioni as $t_config) {
            if ($t_config->id == $m_params->winFormValues->f_tipo){
                $ret = find_TA_sys($t_config->taid, null, null, null, null, null, 0, '', 'Y', 'Y', 'Y');
            }
        }
    }
*/
    
    echo acs_je($ret);
    exit;
}



function _get_elenco_dom($file_TA){
    $ret = array();
    switch ($file_TA) {
        case 'sys':      //XX0S2TA0
            $ret = find_TA_sys('PUVR', null, null, null, null, null, 1, '', 'Y', 'Y', 'Y');
            break;
            
        case 'vend':    //WPI0TA0
            break;
            
        case 'acq':     //WPI1TA0
            break;
            
        case 'util':    //WPI2TA0
            break;
    }
    
  return $ret;
}

function _get_elenco_ris($file_TA, $dom_c, $tipo_c, $tipo_opz_risposta, $opz = array()){
    
    $ret = array();
    switch ($file_TA) {
        case 'sys':      //XX0S2TA0
            //$ret = find_TA_sys('PUVN', null, null, $dom, null, null, 0, '', 'Y', 'Y', 'Y');
                                   
            if (is_array($tipo_opz_risposta))
                $ob_tipo_opz_risposta= json_decode(json_encode($tipo_opz_risposta, JSON_FORCE_OBJECT), false);
            else
                $ob_tipo_opz_risposta = $tipo_opz_risposta;
            
            if (strlen($tipo_c) == 0)    
                $ret = find_TA_sys('PUVN', null, null, $dom_c, null, null, 0, '', 'Y', 'Y', 'Y');
            else
            foreach ($ob_tipo_opz_risposta->opzioni as $t_config) {
                if (trim($t_config->id) == trim($tipo_c)){
                    if ($t_config->taid == '*RIS*')
                        $ret = find_TA_sys('PUVN', null, null, $dom_c, null, null, 0, '', 'Y', 'Y', 'Y');
                    else
                        $ret = find_TA_sys($t_config->taid, null, null, null, null, null, 0, '', 'Y', 'Y', 'Y');
                }
            }
            
            break;
            
        case 'vend':    //WPI0TA0
            break;
            
        case 'acq':     //WPI1TA0
            break;
            
        case 'util':    //WPI2TA0
            break;
    }
    
    return $ret;
}