<?php
require_once "../../config.inc.php";
require_once "acs_gestione_nota_nt_include.php";

$main_module = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();



if($_REQUEST['fn'] == 'exe_save_nota'){
    
    $ar_ins = array();
    $ar_ins['NTMEMO'] 	= $m_params->form_values->f_nota;
    
    if(trim($m_params->rrn) != ''){
        if(trim($m_params->form_values->f_nota) == ''){
            $sql = "DELETE FROM {$m_params->nt_config->file} NT
                    WHERE RRN(NT) = '{$m_params->rrn}'";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            
        }else{
        
        $sql = "UPDATE {$m_params->nt_config->file} NT
                SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                WHERE RRN(NT) = '{$m_params->rrn}'";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            echo db2_stmt_errormsg();
        }
        
    }else{
        
        if (!isset($m_params->nt_config->NTDT))
            $ditta = $id_ditta_default;
        else
            $ditta = $nt_config->NTDT;
        
        $ar_ins['NTDT'] 	= $ditta;
        $ar_ins['NTKEY1'] 	= $m_params->chiave;
        $ar_ins['NTSEQU'] 	= 0;
        $ar_ins['NTTPNO'] 	= $m_params->nt_config->NTTPNO;
         
        $sql = "INSERT INTO {$m_params->nt_config->file}
               (" . create_name_field_by_ar($ar_ins) . ")
               VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, $ar_ins);
        echo db2_stmt_errormsg();
  
    }
    
    //rileggo la nota
    $row_nt = _get_nota_nt_config($m_params->chiave, $m_params->nt_config);
    $ha_nota = _ha_nota_nt_row($row_nt);
    
    $ret = array();
    $ret['success'] = true;
    $ret['note'] = $row_nt['NTMEMO'];
    $ret['icon'] = $ha_nota;
    echo acs_je($ret);
    exit;
}


if($_REQUEST['fn'] == 'open_panel'){
  
    $row_nt = _get_nota_nt_config($m_params->chiave, $m_params->ar_nt_config->nt_config);
    
    $blocco_note = $m_params->ar_nt_config->nt_config->NTTPNO;
    $t_nota = $m_params->ar_nt_config->nt_config->title;
    $chiave = $m_params->chiave;
    $desc = $m_params->desc;
    $title = "{$t_nota} [{$chiave}] {$desc}";
    if(isset($m_params->ar_nt_config->iconCls) && strlen($m_params->ar_nt_config->iconCls) > 0)
        $icona = $m_params->ar_nt_config->iconCls;
    else 
        $icona = "comment_edit";
    
    ?>
    
 {"success":true,
   m_win: {     iconCls: <?php echo j("icon-{$icona}-16"); ?>,
				title: <?php echo j($title); ?>
			}, 
 
  "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll : true,
            frame: true,
            flex:1,
            title: '',
            items: [ 
              	   {
						name: 'f_nota',
						xtype: 'textarea',
						fieldLabel: '',
					    anchor: '-15',		
					    height : 300,		    
					    value: <?php echo j(trim($row_nt['NTMEMO'])); ?>,							
					}
				                	
				], buttons: [
		
				{
	            text: 'Salva',
	            iconCls: 'icon-save-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_win = this.up('window');

					if(form.isValid()){	  
					
						Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_nota',
					        jsonData: {
					        	form_values: form.getValues(),
					        	rrn: <?php echo j($row_nt['RRN']); ?>,
					        	chiave: <?php echo acs_je($m_params->chiave) ?>,
					        	nt_config: <?php echo acs_je($m_params->ar_nt_config->nt_config) ?> 
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	
					        try {
                               var jsonData = Ext.decode(result.responseText);
							   loc_win.fireEvent('afterSave', loc_win, jsonData);
                            }
                            catch(err) {
                                Ext.Msg.alert('Try&catch error', err.message);
                            }
					       
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });	  
					
	

				    }            	                	                
	            }
	        }
   
	        ]              
				
        }
]}   
    
    
    
    
<?php  
exit;
}
