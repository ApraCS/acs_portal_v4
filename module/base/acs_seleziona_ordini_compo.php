<?php
require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

function _ea_adv_main_w($m_params){
    $c = new Extjs_compo('panel', 'border');
    $c->set(array('itemId' => 'el_ordini'));
    
    $c->add_items(array(
        _ea_adv_main_viewport_sidebar(),
        extjs_compo('panel', layout_ar('vbox')
            , null, array('region' => 'center', 'flex' => 1, 'items' => array(
                _ea_adv_main_viewport_grid($m_params) )))));
    
    $c->set(array(
        'refresh_data' => extjs_code("
            function(form_values){
                var  m_grid = this.down('#ord_grid');
                m_grid.store.proxy.extraParams.form_values = form_values;
                m_grid.store.load();
                m_grid.getView().refresh();
            }
        ")
    ));
    
    return $c->code();
}

function _ea_adv_main_viewport_sidebar(){
    $c = new Extjs_compo('panel', 'vbox', 'Parametri');
    $c->set(array(
        'region'      => 'west',
        'width'       => 295,
        'collapsible' => true));
    $c->add_items(array(
        _ea_adv_filtro_panel() ));
    return $c->code();
}

function _ea_adv_main_viewport_grid(){
    global $m_params, $cfg_mod_Spedizioni;
    $c = new Extjs_Grid(null, 'Elenco ordini');
    $c->set(array('itemId' => 'ord_grid', 'flex' => 1));
    $c->set_features(array('filters' => array()));
    //selModel, multiselect
    if (isset($m_params->doc_wpi_search)){
        $doc_wpi_search = $m_params->doc_wpi_search;
        if (isset($cfg_mod_Spedizioni['doc_wpi_search']) &&
            isset($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search])){
                if ($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search]['multiselect'] == true){
                    $c->set(array('multiSelect' => true, 'selModel' => array('selType' => 'checkboxmodel', 'checkOnly' => true)));
    }}}
    
    $c->set(array(
        'columns' => array(
              grid_column_h('Anno', 'anno',  'w80', null, null, array('filter_as' => 'string'))
            , grid_column_h('Numero',  'numero', 'w80', null, null, array('filter_as' => 'string'))
            , grid_column_h_img('power_black', 'fl_bloc', 'Ordini bloccati', array(
                'renderer' => extjs_code("
                    function(value, p, record){
	    	           if(record.get('fl_bloc') == 1) return '<img src=" .  img_path("icone/48x48/power_gray.png") . " width=15>';
	    		       if(record.get('fl_bloc') == 2) return '<img src=" .  img_path("icone/48x48/power_blue.png") . " width=15>';
                       if(record.get('fl_bloc') == 3) return '<img src=" .  img_path("icone/48x48/power_black.png") . " width=15>';
                       if(record.get('fl_bloc') == 4) return '<img src=" .  img_path("icone/48x48/power_black_blue.png") . " width=15>';
        

                    }
              ")))
            , grid_column_h_img('info_blue', 'fl_art_manc', 'Ordini MTO', array(
                'renderer' => extjs_code("
                    function(value, p, record){
    	               if(record.get('fl_art_manc') == 1) return '<img src=" .  img_path("icone/48x48/info_gray.png") . " width=15>';
	    		       if(record.get('fl_art_manc') == 2) return '<img src=" .  img_path("icone/48x48/shopping_cart_green.png") . " width=15>';
                       if(record.get('fl_art_manc') == 3) return '<img src=" .  img_path("icone/48x48/shopping_cart.png") . " width=15>';
                       if(record.get('fl_art_manc') == 4) return '<img src=" .  img_path("icone/48x48/shopping_cart_gray.png") . " width=15>';
                       if(record.get('fl_art_manc') == 5) return '<img src=" .  img_path("icone/48x48/info_blue.png") . " width=15>';
                       if(record.get('fl_art_manc') == 20) return '<img src=" .  img_path("icone/48x48/shopping_cart_red.png") . " width=15>';
    		    }
              ")))
            , grid_column_h('Tipo',  'tipologia', 'w40', null,  array(
                'renderer' => extjs_code("
                    function(value, p, record){
	    	           p.tdCls += ' ' + record.get('raggr');										
			          return value;	
                    
                    }
              ")), array('filter_as' => 'string'))
            , grid_column_h_date('Data', 'data_reg')
            , grid_column_h_date('Evasione', 'data_eva')
            , grid_column_h('Stato', 'stato', 'w50', null, null, array('filter_as' => 'string'))
            , grid_column_h('Riferimento', 'riferimento',  'f1', null, null, array('filter_as' => 'string'))
         
        ),
        'store'       => array(
            'autoLoad'    => false,
            'fields'      => array('age_uno', 'age_due', 'age_tre', 'agente', 'art_da_prog', 'fl_bloc', 'fl_art_manc', 'raggr', 'ordine', 'anno', 'numero', 'riferimento', 'tipologia', 'stato', 'data_eva', 'data_reg', 'k_ordine', 'riga', 'RDNREC', 'RDDART', 'RDART', 'RDQTA', 'anno_l1', 'numero_l1'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_json_data_ordini'),array('open_parameters' => $m_params))),
       
        'listeners'   => array(
            'afterlayout' => extjs_code("function(comp){comp.getSelectionModel().selectAll(true);}")
        )
    ));
    
    
    
    if (isset($m_params->doc_wpi_search)){
        $doc_wpi_search = $m_params->doc_wpi_search;
        if (isset($cfg_mod_Spedizioni['doc_wpi_search']) &&
          isset($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search])){
              if ($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search]['multiselect'] == true){ 
                  $select_handler = 
            		  "var rec_selected = this.up('grid').getSelectionModel().getSelection();	       			
	       				var list_selected_id = [];
          	                for (var i=0; i<rec_selected.length; i++)
        		 				list_selected_id.push(rec_selected[i].data);		  
          				if (!rec_selected){
	       					acs_show_msg_error('Selezionare un ordine');
							return false;}	            				
	       				if (!Ext.isEmpty(m_win.events.afteroksave)) {
                      	    m_win.fireEvent('afterOkSave', m_win, list_selected_id);
                  		}";
                   
              }else{ 
                       
                  $select_handler = 
                       "var rec_selected = this.up('grid').getSelectionModel().getSelection()[0];	       				
	       				  if (!rec_selected){
	       					acs_show_msg_error('Selezionare un ordine');
							return false;
						}	            				
	       			    if (!Ext.isEmpty(m_win.events.afteroksave)) {
                          m_win.fireEvent('afterOkSave', m_win, rec_selected.get('k_ordine'));
                    	}";
                              
             }}}
             
     $c->add_buttons( array(
         array('xtype' => 'tbfill'),
         array(
             'xtype'   => 'button',
             'text'    => 'Seleziona',
             'scale' => 'large',
             'handler' => extjs_code("
               function(){
                    var m_win  = this.up('window');
                    {$select_handler}
                }
        ")
         )
     ), 'bottom');

    
    return $c->code();
}

//*********************************************
// Panel Filtro
//*********************************************
function _ea_adv_filtro_panel(){
    global $s;
    
    $c = new Extjs_Form('vbox');
    $c->set(array('height' => 490, 'width' => '100%'));
    $c->add_items(array(
        extjs_compo('fieldcontainer', 'hbox', null, array('items' => array(
            array('xtype' => 'displayfield', 'name'=>'ordine', 'fieldLabel' => 'Anno/Nr. Ordine')
            , array('xtype' => 'numberfield',  'name' => 'f_anno_or', 'fieldLabel' => '', 'width' => 70, 'hideTrigger' => true)
            , array('xtype' => 'numberfield',  'name' => 'f_nr_or', 'fieldLabel' => '', 'width' => 80, 'hideTrigger' => true)
            
        )))
        , extjs_combo_cliente(array('name' => 'f_cli'))
        , extjs_datefield('f_data_reg_i', 'Data reg. iniziale')
        , extjs_datefield('f_data_reg_f', 'Data reg. finale')
        , extjs_datefield('f_data_eva_i', 'Data evas. iniziale')
        , extjs_datefield('f_data_eva_f', 'Data evas. finale')
        , extjs_combo(array(
            'fieldLabel' => 'Divisione',
            'name' => 'f_divisone',
            'initialData'  => find_TA_sys('DDOC', null, null, null, null, null, 0, '', 'Y'),
            'initialValue' => '',
            'store_fields'  => array('id', 'text'),
            'allowBlank'    => true,
            'flex' => 1,
            
        ))
        , extjs_combo(array(
            'fieldLabel' => 'Tipologia',
            'name' => 'f_tipologia',
            'initialData'  => $s->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'),
            'initialValue' => '',
            'store_fields'  => array('id', 'text'),
            'allowBlank'    => true,
            'flex' => 1,
            
        ))
        , extjs_combo(array(
            'fieldLabel' => 'Tipo',
            'name' => 'f_tipo_ordine',
            'initialData'  => find_TA_sys('BDOC', null, null, 'VO', null, null, 0, "", 'Y'),
            'initialValue' => '',
            'store_fields'  => array('id', 'text'),
            'allowBlank'    => true,
            'flex' => 1,
            
        ))
        , extjs_combo(array(
            'fieldLabel' => 'Stato',
            'name' => 'f_stato_ordine',
            'initialData'  => $s->get_options_stati_ordine(),
            'initialValue' => '',
            'store_fields'  => array('id', 'text'),
            'allowBlank'    => true,
            'flex' => 1,
            
        ))
        , extjs_combo(array(
            'fieldLabel' => 'Agente',
            'name' => 'f_agente',
            'initialData'  => find_TA_sys('CUAG', null, null, null, null, null, 0, '', 'Y'),
            'initialValue' => '',
            'store_fields'  => array('id', 'text'),
            'allowBlank'    => true,
            'flex' => 1,
            
        ))
        
        , extjs_compo('fieldcontainer', 'hbox', null, array('items' => array(
              array('xtype' => 'displayfield', 'fieldLabel' => '&nbsp;', 'labelSeparator'=> '')
            , array('xtype' => 'checkbox', 'name'=> 'f_ag_uno', 'fieldLabel' => '1�', 'labelWidth' => 20, 'inputValue' => 'Y', 'margin'=> '0 5 0 0')
            , array('xtype' => 'checkbox', 'name' => 'f_ag_due', 'fieldLabel' => '2�', 'labelWidth' => 20, 'inputValue' => 'Y', 'margin'=> '0 5 0 0')
            , array('xtype' => 'checkbox', 'name' => 'f_ag_tre', 'fieldLabel' => '3�', 'labelWidth' => 20, 'inputValue' => 'Y')
            
        )))
      
    ));
    
    
    $c->add_buttons( array(
        extjs_buttons_f8($s->get_cod_mod(), "ELENCO_ORDINI"),
        array('xtype' => 'tbfill'),
        array(
            'xtype'   => 'button',
            'text'    => 'Applica filtri',
            'scale' => 'small',
            'handler' => extjs_code("
              function(){
                var me = this,
                    m_panel = this.up('#el_ordini'),
                    form_values = this.up('form').getValues();
                    m_panel.refresh_data(form_values);
            }
        ")
        )
    ), 'bottom');
    
  ;
    return $c->code();
}

