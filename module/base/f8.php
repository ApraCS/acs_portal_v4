<?php

require_once "../../config.inc.php";

$mod_base = new Base();
$f8 = new F8();

$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == "f8_m_exe"){
    
    $ret = array();
    
    if(isset($m_params->form_values)){
        $m_params->data->memo = acs_je($m_params->form_values);
        $f8->save((array)$m_params->data);
    }else{
        $f8->save($_REQUEST);
    }
    
   
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
} 

if ($_REQUEST['fn'] == "f8_m_delete"){
	$f8->delete(acs_m_params_json_decode());
	exit;
}


if ($_REQUEST['fn'] == "get_data"){
    $ret = array();
    $m_params->ar_parameters->name = '*DEF';
    $ret['success'] = true;
    $ret['data'] = $f8->get_memorizzazioni_data($m_params->ar_parameters);
   
    echo acs_je($ret);
    exit;
}



if ($_REQUEST['fn'] == "get_json_r"){	
?>

{"success":true, "items": [

        {
            xtype: 'grid',
            frame: false,
  			autoScroll: true,
  			layout: 'fit',            
            title: '',
	          
			store: Ext.create('Ext.data.JsonStore', {
			        fields: ['DFNAME', 'DFMEMO', 'DFID'],
			        data:<?php echo acs_je($f8->get_memorizzazioni_data($m_params)) ?>
			    }),			
				
        columns: [{
                text     : 'Elenco parametri preimpostati',
                flex     : 1,
                sortable : true,
                dataIndex: 'DFNAME'
            }],
            
            
        listeners: {

		  celldblclick: {								
			  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			  	form_origine = Ext.getCmp('<?php echo $m_params->form_id ?>');
			  	form = form_origine.getForm();
				rec = iView.getRecord(iRowEl);
				
			  	memorizzazione = Ext.decode(rec.get('DFMEMO').replace(/\\"/g, '"'));
			  	
			  	 p_ar = new Array();
			  	 Ext.Object.each(memorizzazione, function(key, value) {
			  	 	p_ar[key] = value;
			  	 });

			  	//per tutti gli input della form
			  	form_origine.getForm().getFields().items.forEach(function(n) {
			  	  if (n.xtype == 'datefield'){
			  	    if (!Ext.isEmpty(p_ar[n.name]) && p_ar[n.name].length > 0)
			  	  		n.setValue(acs_date_from_AS(p_ar[n.name], 'd/m/Y'));
			  	  	else
			  	  		n.setValue(null);	
			  	  } else if(n.xtype == 'timefield') {
			  	    if (!Ext.isEmpty(p_ar[n.name]) && p_ar[n.name].length > 0)
			  	  		n.setValue(time_from_AS(p_ar[n.name], 'H:i'));
			  	  	else
			  	  		n.setValue(null);
			  	  }else if (n.xtype == 'hidden') {
			  		//non ricompilo i campi hidden
			  	  } else if (n.xtype == 'displayfield') {
			  		//non ricompilo i campi displayfield
			  	  } else {
			  		n.setValue(p_ar[n.name]);
			  	  }	
			  	});
			  	
			  	//riapplico eventuali memorizzazioni su grid
			    var grids = form_origine.query('[grid_save_in_F8]');
	        	Ext.each(grids, function(gr) {
	        		var gr_p_name = gr.grid_save_in_F8; 
                    gr.set_f8_data(p_ar[gr_p_name]);
	        	});
							  	
			  	
			  	
			  	
			    //se c'e' modifico anche la descrizione del report (che passero' in stampa)
			    if (form_origine.getForm().findField('add_descr_report') !== null)
			      	if (p_ar['add_descr_report'] === null || p_ar['add_descr_report'].trim().length == 0)
			    		form_origine.getForm().findField('add_descr_report').setValue(rec.get('DFNAME'));
			  	
		  	   this.up('window').close();			  	 			  	
		  	  }
 
		  }
		} //listeners
			    
   } //grid
]}

<?php exit; }?>






<?php 
if ($_REQUEST['fn'] == "get_json_m"){
 $memo_encoded = strtr(acs_je($m_params->memo), array("'" => "\'", '"' => '\"'));	
?>

{"success":true, "items": [

        {
            xtype: 'form',
            frame: true,
            autoScroll: true,
            title: '',
            url: '<?php echo $mod_base->path_module(); ?>/f8.php',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			                        
            
            items: [
				{
                	xtype: 'hidden',
                	name: 'fn',
                	value: 'f8_m_exe'
                }, {
                	xtype: 'hidden',
                	name: 'mod',
                	value: <?php echo j($m_params->mod) ?>
                }, {
                	xtype: 'hidden',
                	name: 'func',
                	value: <?php echo j($m_params->func) ?>
                }, {
                	xtype: 'hidden',
                	name: 'memo',
                	value: '<?php echo $memo_encoded; ?>'
                }, {
					fieldLabel: 'Intestazione parametri',
                	xtype: 'textfield',
                	name: 'name',
                	disabled: false,
                	maxLength: 50, allowBlank: false
	            }, { 
			            xtype: 'grid',
			            frame: false,
			  			autoScroll: true,
			  			layout: 'fit',            
			            title: '',
				          
						store: Ext.create('Ext.data.JsonStore', {
						        fields: ['DFID', 'DFNAME', 'DFMEMO'],
						        data:<?php echo acs_je($f8->get_memorizzazioni_data($m_params)) ?>
						    }),			
							
			        columns: [{
			                text     : 'Id<br>&nbsp;',
			                width	 : 50,
			                sortable : true,
			                dataIndex: 'DFID'
			            }, {
			                text     : 'Elenco parametri preimpostati<br>&nbsp;',
			                flex     : 1,
			                sortable : true,
			                dataIndex: 'DFNAME'
			            }, {
			                xtype: 'actioncolumn',
			                width: 30,
			                text: '<?php echo "<img src=" . img_path("icone/48x48/sub_red_delete.png") . " height=20>"; ?>',
			                items: [{
			                    icon   : <?php echo img_path("icone/16x16/sub_red_delete.png") ?>, 
			                    tooltip: 'Rimuovi memorizzazione',
			                    handler: function(grid, rowIndex, colIndex) {
			                        var rec = grid.store.getAt(rowIndex);
									Ext.Ajax.request({
								        url: '<?php echo $mod_base->path_module(); ?>/f8.php?fn=f8_m_delete',
								        jsonData: {DFID: rec.get('DFID')},
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        	grid.store.remove(rec);	            	  													        
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });			                    
			                    }
			                }]
			            }    
                ],
			            
			            
			        listeners: {
			
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							rec = iView.getRecord(iRowEl);			  	
			
						  	//propongo come nome quello selezionato
						  	form = this.up('form').getForm();
						  	f_name = form.findField('name');						  	
						  	f_name.setValue(rec.get('DFNAME'));						  	
					  	  }
			 
					  }
					} //listeners
						    
			   } //grid
            ],
            
            
			buttons: [{
	            text: 'Salva',
	            iconCls: 'icon-button_blue_play-32',
	            scale: 'large',	            
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	win = this.up('window');
					
					if(form.isValid()){
					
							Ext.MessageBox.show({
							           msg: 'Elaborazione procedura in corso',
							           progressText: 'Running...',
							           width:300,
							           wait:true, 
							           waitConfig: {interval:200},
							           icon:'ext-mb-download', //custom class in msg-box.html
							           //animateTarget: 'mb7'
							       });					
					
										
							form.submit(
										{
				                            success: function(form,action) {
				                            
				                            	//chiudo il msg box di attesa
												Ext.MessageBox.hide();		                            
												
												win.close();
														                            	
				                            },
				                            failure: function(form,action){
				                            	//chiudo il msg box di attesa
												Ext.MessageBox.hide();
														                            
				                                alert('errore nel richiamo della funzione');
				                            }
				                        }					
							)
					};
							
            	                	                
	            }
	        }],             
				
        }
]}

<?php }?>


