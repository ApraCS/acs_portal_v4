<?php 

class Base {
	
	private $mod_cod = "BASE";
	private $mod_dir = "base";	
	
	public function path_module(){
		return ROOT_PATH . "module/" . $this->mod_dir;
	}
	
	
	public function decod_domanda_risposta($domanda, $risposta, $ditta = null){
	 	global $conn, $cfg_mod_Admin, $id_ditta_default;
	 	
	 	if (is_null($ditta)) $ditta = $id_ditta_default;
	 	
	 	if (trim($domanda) == '' && trim($risposta) == '') return '';
	 	
	 	$ar = array();
	 	$sql = "SELECT TADESC FROM {$cfg_mod_Admin['file_tab_sys']} WHERE TADT = ? AND TAID = 'PUVN' AND TACOR2 = ? AND TANR = ?";
	 
	 			$stmt = db2_prepare($conn, $sql);
	 			$result = db2_execute($stmt, array($id_ditta_default, $domanda, $risposta));
	 			$row = db2_fetch_assoc($stmt);
	 			if ($row == false)
	 				return "[{$domanda}|{$risposta}]";
	 			else
	 				return $row['TADESC']; 
	}
	
	
	public function exe_call_pgm($p){
	    global $tkObj, $useToolkit, $conn;
		global $libreria_predefinita, $libreria_predefinita_EXE;
	
		set_time_limit(1000);		
		
		$m_pgm = explode("|", $p['f_pgm']);
	
		//costruisco il parametro
		$cl_p = str_repeat(" ", 246);
		$cl_p .= $p['user_parameters'] . '';
		
		if ($useToolkit == 'N'){
		    $m_lib = trim($m_pgm[1]);
		    $m_prg = trim($m_pgm[0]);
		    //per test in Apra
		    $qry1	 =	"CALL {$m_lib}.{$m_prg}('{$cl_p}')";
		    $stmt1   = 	db2_prepare($conn, $qry1);
		    $result1 = 	db2_execute($stmt1);
		    $call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		    //FINE test per Apra
		} else {	
    		//ambiente S36
    		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
    		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
    		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
    	
    		$cl_in 	= array();
    		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p);
    		$call_return = $tkObj->PgmCall(trim($m_pgm[0]), trim($m_pgm[1]), $cl_in, null, null);
		}
	
		$ret = array();
		$ret['success'] = true;
		$ret['esito']	= $call_return;
		$ret['esito_LK_AREA'] = trim($call_return['io_param']['LK-AREA']);
		return acs_je($ret);
	
	}
	
	
	
	
	
}

?>