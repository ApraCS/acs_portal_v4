<?php

require_once("../../config.inc.php");

$s = new Spedizioni(array('no_verify' => 'Y'));
set_time_limit(120);

$m_params = acs_m_params_json_decode();	

 
// ******************************************************************************************
// FORM di PROTOCOLLAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){ ?>

{"success":true, "items": [
	  {
		xtype: 'form',
		cfg_protocollazione : <?php echo acs_je($cfg_protocollazione); ?>,
        bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        flex : 1,
        items: [
			{name: 'tido', xtype: 'hidden', value: 'VO'}
 	     ,  {xtype: 'combo',
			name: 'f_cliente_cod',
			fieldLabel: 'Cliente',
			minChars: 2,		
			labelWidth : 120,	
			allowBlank: false,            
            store: {
            	pageSize: 1000,            	
				proxy: {
		            type: 'ajax',
		            url : '../desk_vend/acs_get_select_json.php?select=search_cli_anag',
		            reader: {
		                type: 'json',
		                root: 'root',
		                totalProperty: 'totalCount'
		            }
		        },       
				fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
            },
                        
			valueField: 'cod',                        
            displayField: 'descr',
            typeAhead: false,
            hideTrigger: true,
            anchor: '100%',
            listeners: {
	            change: function(field,newVal) {
	            	var form = this.up('form').getForm(); 	            	
	            	  form.findField('f_destinazione_cod').setValue('');	
					  form.findField('f_destinazione').setValue('');
                      form.findField('out_ind_dest').setValue('');
                      form.findField('out_loc_dest').setValue('');
                      form.findField('f_pven_cod').setValue('');	
					  form.findField('f_pven').setValue('');
                      form.findField('out_ind_pven').setValue('');
                      form.findField('out_loc_pven').setValue('');
	            }, 
				select: function(combo, row, index) {
	            	var form = this.up('form').getForm();
	     			form.findField('out_ind').setValue(row[0].data.out_ind);									     			
	     			form.findField('out_loc').setValue(row[0].data.out_loc);

 					//imposto quella a standard
					form.findField('f_destinazione_cod').store.load({
	 					callback: function(records, operation, success) {
 							
 							if (records.length == 1){
 								this.setValue(records[0].get('cod'));
 							}
    					}, scope: form.findField('f_destinazione_cod') 	
 					});
 								
 		
				} 		
 		
 		
	        },
            
            
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'Nessun cliente trovato',
                
                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class=\"search-item\">' +
                        '<h3><span>{descr}</span>[{cod}]</h3>' +
 						' {out_ind}<br/>' +
                        ' {out_loc}' +
                    '</div>';
                }                
                
            },
            
            pageSize: 1000
        }
 		
 		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind', 	
 			labelWidth : 120,	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc', 
 			labelWidth : 120,
 			anchor: '100%',		
 		}

		, {
			xtype: 'fieldcontainer',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_destinazione_cod',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'f_destinazione',
				    fieldLabel: 'Destinazione',
                 	labelWidth : 120,
                 	flex : 1,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_destinazione_cod').setValue(record_selected.cod);	
												form.findField('f_destinazione').setValue(record_selected.denom);
                                                form.findField('out_ind_dest').setValue(record_selected.IND_D);
                                                form.findField('out_loc_dest').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
							  
									acs_show_win_std('Seleziona destinazione', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, type : 'D'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
 		
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_dest', 	
 			labelWidth : 120,	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_dest', 		
 			labelWidth : 120,
 			anchor: '100%'
 		} 
 		, {
		     name: 'vsrf'                		
		   , xtype: 'textfield'
		   , fieldLabel: 'Riferimento'
		   , maxLength: 30
		   , labelWidth : 120
		   , anchor: '100%'
		}, 
		{
		     name: 'dtrg'                		
		   , xtype: 'datefield'
		   , startDay: 1 //lun.
		   , labelWidth : 120
		   , fieldLabel: 'Data ricezione'
		   , format: 'd/m/Y'
		   , anchor: '100%'
		   , submitFormat: 'Ymd'
		   , allowBlank: false
		   , value: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'				   		
		   , listeners: {
		       invalid: function (field, msg) {
		       Ext.Msg.alert('', msg);}
			 }
		}			
 		
		, {
				name: 'tpdo',
				xtype: 'combo',
				labelWidth : 120,
            	anchor: '100%',
				fieldLabel: 'Tipo ordine',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			    allowBlank: false,
			    queryMode: 'local',
			    listeners: {

					select: function(combo, row, index) {
                        var form = this.up('form').getForm();
                        var tacor2 = row[0].get('TACOR2').trim();
                        
                        form.findField('tido').setValue(tacor2);
                        
                        var cfg_base_dati = this.up('form').cfg_protocollazione.base_dati;
                        
                        if(!Ext.isEmpty(cfg_base_dati[tacor2])){ 
                               for (var camp in cfg_base_dati[tacor2]) { 
                               
                               form.findField(camp).setValue(cfg_base_dati[tacor2][camp].def);
                               form.findField(camp).allowBlank = cfg_base_dati[tacor2][camp].allowBlank;
                               if(cfg_base_dati[tacor2][camp].hidden == true){
                                  form.findField(camp).hide();
                                  form.findField(camp).allowBlank = true;
                               }else
                                  form.findField(camp).show();
                               
                                if(!Ext.isEmpty(cfg_base_dati[tacor2][camp].list))
                                    form.findField(camp).getStore().proxy.extraParams.list = cfg_base_dati[tacor2][camp].list;
                               
                             }
                          }                    
                        

                        form.findField('stdo').getStore().proxy.extraParams.tacor2 = tacor2;
                        form.findField('stdo').getStore().load({
                               scope: this,
                               callback: function(records, operation, success) {
                                     form.findField('stdo').setValue(cfg_base_dati[tacor2].stdo.def);  
                               }
                            });

                        form.findField('prio').getStore().proxy.extraParams.tacor2 = tacor2;
                        form.findField('prio').getStore().load({
                               scope: this,
                               callback: function(records, operation, success) {
                                     form.findField('prio').setValue(cfg_base_dati[tacor2].prio.def);  
                               }
                            });
                      
					},
                	 beforequery: function (record) {
                        record.query = new RegExp(record.query, 'i');
                        record.forceAll = true;
                    }
                }, //listeners
                
				store: {
					autoLoad: true,
					editable: false,
					autoDestroy: true,
				    fields: [{name:'id'}, {name:'text'}, 'TACOR2'],
				    data: <?php
				    
    				    $filtra_TDTPDO = null;
    				    $filtra_TDTPDO_TACOR2 = null;
    				    if (isset($m_params->protocollazione_config)){
    				        if (isset($cfg_protocollazione[$m_params->protocollazione_config]))
    				            $filtra_TDTPDO = $cfg_protocollazione['base_dati']['TDTPDO'];
    				            $filtra_TDTPDO_TACOR2 =  $cfg_protocollazione['base_dati']['TDTPDO_TACOR2'];
    				    }
    				    
    				    echo  acs_je(find_TA_sys('BDOC', $filtra_TDTPDO, null, $filtra_TDTPDO_TACOR2, null, null, 0, '', 'Y', 'N', true, true)); 
				    ?>					    
				}				     		
 	
			}	
    
		, {
				name: 'stdo',
				xtype: 'combo',
            	anchor: '100%',				
				fieldLabel: 'Stato',
				displayField: 'text',
				valueField: 'id',
				labelWidth : 120,
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,														
				store: {
                    xtype: 'store',
					autoLoad:false,
				    proxy: {
		               url : '../base/acs_get_stati_doc.php?fn=get_json_data',
		               method: 'POST',								
					   type: 'ajax',
                       actionMethods: {read: 'POST'},
					   extraParams: {
										list: '',
                                        tacor2 : '' 
			        				},
			           doRequest: personalizza_extraParams_to_jsonData,	
					   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
				      fields: ['id', 'text']
                 }, 
 						 
			}
            , {
				     name: 'dtva'                		
				   , xtype: 'datefield'
				   , startDay: 1 //lun.
				   , fieldLabel: 'Data validit&agrave;'
                   , format: 'd/m/Y'
                  // , hidden : true
                   , labelWidth : 120
				   , submitFormat: 'Ymd'
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
									     		
					     		
				}	
	
		, {
				name: 'prio',
				xtype: 'combo',
            	anchor: '100%',		
            	labelWidth : 120,		
				fieldLabel: 'Priorit&agrave',
				displayField: 'text',
				valueField: 'id',
				emptyText: '- seleziona -',
				forceSelection: true,
			   	allowBlank: false,					     																
				store: {
                    xtype: 'store',
					autoLoad:false,
				    proxy: {
		               url : '../base/acs_get_prio_doc.php?fn=get_json_data',
		               method: 'POST',								
					   type: 'ajax',
                       actionMethods: {read: 'POST'},
					   extraParams: {
									   tacor2 : '' 
			        				},
			           doRequest: personalizza_extraParams_to_jsonData,	
					   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
				      fields: ['id', 'text']
                 },					 
			}	

				, {
				     name: 'dtep'                		
				   , xtype: 'datefield'
				   , labelWidth : 120
				   , startDay: 1 //lun.
				   , fieldLabel: 'Consegna richiesta'
                   , format: 'd/m/Y'
				   , submitFormat: 'Ymd'		   		
				   , listeners: {
				       invalid: function (field, msg) {
				       Ext.Msg.alert('', msg);}
					 }
					     		
				   , validator: function(value){
				   			var m = this; 
				   			var form = this.up('form').getForm();
				   			
				   			if (Ext.isEmpty(form.findField('prio').valueModels))
				   			  return true;
				   			  
					     	if (Ext.Array.contains(['>', '<', 'W', '='], form.findField('prio').valueModels[0].get('tarest')))
					        	return 'Indicare una data consegna richiesta';
					     	return true;
					    }					     		
					     		
				},
				 		{
			xtype: 'fieldcontainer',
			flex : 1,
			anchor: '-10',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'f_pven_cod',
                    hidden : true,
				},
                {   xtype: 'textfield',
				    name: 'f_pven',
				    fieldLabel: 'Punto vendita',
                 	labelWidth : 120,
                 	flex : 1,
                    readOnly : true
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('f_pven_cod').setValue(record_selected.cod);	
												form.findField('f_pven').setValue(record_selected.denom);
                                                form.findField('out_ind_pven').setValue(record_selected.IND_D);
                                                form.findField('out_loc_pven').setValue(record_selected.LOC_D);
                                                from_win.close();
												}										    
									};
									
                                    var cliente = form.findField('f_cliente_cod').value;
							  
									acs_show_win_std('Seleziona punto vendita', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : cliente, type : 'P'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
								});										            
							 }
						}
				}
				
				]
		}
		,  {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Indirizzo', 		
 			name: 'out_ind_pven', 	
 			labelWidth : 120,	
 			anchor: '100%',
 		}, {
 			xtype: 'textfield', disabled: true,
			fieldLabel: 'Localit&agrave;', 		
 			name: 'out_loc_pven', 		
 			labelWidth : 120,
 			anchor: '100%'
				
			}	
				
				
	]
	
	, dockedItems: [
	
                {xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->',
                    {
                    iconCls: 'icon-button_black_repeat-16',
 					// scale: 'large',
                    itemId: 'listino',
                    text: 'Conferma generazione ordine cliente',
                    disabled: false,
                    scope: this,
 					call_protocolla: function(form, m_button, loc_win){
 					     Ext.Ajax.request({
							        url        : '../desk_vend/acs_op_exe.php?fn=exe_protocollazione',
 		                            timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText); 	
										call_return = jsonData.call_return;	
 										var k_ordine = call_return.substr(345, 50);
             				            var anno   = k_ordine.substr(10, 4);
             				            var numero = k_ordine.substr(15, 6);
             				            var tipo = form.findField('tpdo').getValue();
             				            var ordine = anno + '_' + numero + '_' + tipo
             				            
             				            loc_win.fireEvent('afterOkSave', loc_win, k_ordine); 		
	 		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	 		
 					},
 		
 		
                    handler: function(b){
                     
 						var form = b.up('form').getForm();
 						
 						 						
 						var m_button = b;
 						m_button.disable();
 						
 						var loc_win = b.up('window');
 						
 						if(form.isValid()){
 		
 									Ext.Ajax.request({
							        url        : '../desk_vend/acs_panel_protocollazione.php?fn=check_cliente_riferimento',
 		                            timeout: 2400000,
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText);
 		                                if (jsonData.num_find > 0){
 											Ext.Msg.confirm('Richiesta conferma', 'Esistono ordini gi&agrave; presenti con cliente/riferimento: <br> '+ jsonData.message +'<br> Vuoi procedere?',
                                 				 function(btn, text){																							    
                            								if (btn == 'yes'){																	         	
                            										b.call_protocolla(form, m_button,loc_win);							         	
                            																	      				         	
                            								}else{
                             	    								m_button.enable();
                            								}
                            					   });
 		
 		
 										} else 
 											b.call_protocolla(form, m_button, loc_win);
	 		
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });	 
 		
 		
 							 				
 						} else {
 							m_button.enable();
 						}
 		
                    }
                }]
            } 
	
	]
	
	
	}
]
}

<?php }