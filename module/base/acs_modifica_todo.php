<?php

require_once("../../config.inc.php");

$main_module = new DeskAcq();

$m_params = acs_m_params_json_decode();

if(isset($m_params->cfg_mod))
    $cfg_mod = (array)$m_params->cfg_mod;
else
    $cfg_mod = $main_module->get_cfg_mod();

// ******************************************************************************************
// RIPRISTINA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_ripristina'){
        
    $ar_upd_1 = array();
    $ar_upd_1['ASFLRI'] = '';
                    
    foreach($m_params->list_selected_id as $v){
        
        if (count($ar_upd_1) > 0){
            $sql = "UPDATE {$cfg_mod['file_assegna_ord']} AO
            SET  " . create_name_field_by_ar_UPDATE($ar_upd_1) . "
            WHERE ASIDPR = {$v}";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd_1);
            echo db2_stmt_errormsg($stmt);
        }
        
        //salvo eventuali note memo
        if (strlen(trim($m_params->form_values->f_memo)) > 0){
            
            $na = new SpedAssegnazioneOrdini();
            $memo = $na->get_memo($v, $m_params->blocco);
            
            if(strlen($memo) > 0){
                
                $ar_upd = array();
                $ar_upd['NTMEMO'] = trim($m_params->form_values->f_memo);
                
                $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
                SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                WHERE NTDT = '{$id_ditta_default}' AND NTKEY1 = '{$v}'
                AND NTSEQU = 0 AND NTTPNO = '{$m_params->blocco}'";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_upd);
                echo db2_stmt_errormsg($stmt);
            }else {
                
                $sqlMemo = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(NTDT, NTMEMO, NTKEY1, NTSEQU, NTTPNO)
                VALUES(?, ?, ?, ?, ?)";
                
                
                $stmtMemo = db2_prepare($conn, $sqlMemo);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmtMemo, array($id_ditta_default, trim($m_params->form_values->f_memo), $v, 0, $m_params->blocco));
                
            }
            
        }
        
    }
    
    
    
    echo acs_je(array('success' => true));
    exit;
                    
    }

// ******************************************************************************************
// AGGIORNAMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_upd'){
    
    $ar_upd_1 = array();
    if(strlen(trim($m_params->form_values->f_utente_assegnato)) > 0)
        $ar_upd_1['ASUSAT'] = $m_params->form_values->f_utente_assegnato;
    if (strlen(trim($m_params->form_values->f_scadenza)) > 0)
        $ar_upd_1['ASDTSC'] = (int)$m_params->form_values->f_scadenza;
    if (strlen(trim($m_params->form_values->f_note)) > 0)
        $ar_upd_1['ASNOTE'] = $m_params->form_values->f_note;
                
    foreach($m_params->list_selected_id as $v){
        
        if (count($ar_upd_1) > 0){
            $sql = "UPDATE {$cfg_mod['file_assegna_ord']} AO
            SET  " . create_name_field_by_ar_UPDATE($ar_upd_1) . "
            WHERE ASIDPR = {$v}";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd_1);
            echo db2_stmt_errormsg($stmt);
        }
        
        //salvo eventuali note memo
        if (strlen(trim($m_params->form_values->f_memo)) > 0){
            
            $na = new SpedAssegnazioneOrdini();
            $memo = $na->get_memo($v, $m_params->blocco);
         
            if(strlen($memo) > 0){
                
                $ar_upd = array();
                $ar_upd['NTMEMO'] = trim($m_params->form_values->f_memo);
                
                $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
                        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                        WHERE NTDT = '{$id_ditta_default}' AND NTKEY1 = '{$v}'
                        AND NTSEQU = 0 AND NTTPNO = '{$m_params->blocco}'";
                
                $stmt = db2_prepare($conn, $sql);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmt, $ar_upd);
                echo db2_stmt_errormsg($stmt);
            }else {
                                
                $sqlMemo = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(NTDT, NTMEMO, NTKEY1, NTSEQU, NTTPNO)
                VALUES(?, ?, ?, ?, ?)";
                
                                
                $stmtMemo = db2_prepare($conn, $sqlMemo);
                echo db2_stmt_errormsg();
                $result = db2_execute($stmtMemo, array($id_ditta_default, trim($m_params->form_values->f_memo), $v, 0, $m_params->blocco));
                
            }
            
        }
        
    }
                
                
                
    echo acs_je(array('success' => true));
    exit;
                
}

// ******************************************************************************************
// FORM MODIFICA TODO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_mod'){
    
    //elenco possibili utenti destinatari
    $user_to = array();
    $users = new Users;
    //default
    $ar_users = $users->find_all();
    
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
        $ar_users_json = acs_je($user_to);
        
        if(count($m_params->list_selected_id) == 1){
            $sql = "SELECT AO.*, NT_MEMO.NTMEMO AS MEMO
                    FROM {$cfg_mod['file_assegna_ord']} AO
                    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_note']} NT_MEMO
                    ON NT_MEMO.NTTPNO = '{$m_params->blocco}' AND INTEGER(NT_MEMO.NTKEY1) = AO.ASIDPR AND NT_MEMO.NTSEQU=0
                    WHERE ASIDPR = '{$m_params->list_selected_id[0]->prog}'";
           
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            $row = db2_fetch_assoc($stmt);
            
        }
     
        $ar_evasi = array();
        $ar_todo = array();
        foreach ($m_params->list_selected_id as $v){
            if($v->flag == 'Y')
                $ar_evasi[] = $v->prog;
            else 
                $ar_todo[] = $v->prog;
        }
        
       
        
        
        ?>
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            flex : 1,
	            layout: 'anchor',
	            items: [        
						{
			            xtype: 'combo',
			            flex : 1,
			            store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
			            displayField: 'descr',
			            valueField: 'cod',
			            fieldLabel: 'Utente assegnato',
			            labelWidth : 110,
			            queryMode: 'local',
			            selectOnTab: false,
			            name: 'f_utente_assegnato',
			            allowBlank: true,
						forceSelection: true,
						anchor: '-5',		
						queryMode: 'local',
     					minChars: 1,	            
			            value: <?php echo j(trim($row['ASUSAT'])); ?>,
			            listeners: { 
			 				beforequery: function (record) {
    	         				record.query = new RegExp(record.query, 'i');
    	         				record.forceAll = true;
    				 		}
 						}
			        	}, {
					   xtype: 'datefield'
					   , startDay: 1 //lun.
					   , fieldLabel: 'Scadenza'
					   , labelWidth : 110
					   , name: 'f_scadenza'
					   , format: 'd/m/Y'
					   , minValue: '<?php echo print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'	
					   , value : '<?php echo print_date($row['ASDTSC'], "%d/%m/%Y"); ?>'							   						   
					   , submitFormat: 'Ymd'
					   , allowBlank: true
					   , anchor: '-5'
					   , listeners: {
					       invalid: function (field, msg) {
					       Ext.Msg.alert('', msg);}
						}
					   
					}, 
					
					  {
						name: 'f_note',
						labelWidth : 110,
						xtype: 'textfield',
						fieldLabel: 'Riferimento',
					    maxLength: 100, 
					    anchor: '-5',
					    value : <?php echo j(trim($row['ASNOTE'])); ?>							
					  },{
						name: 'f_memo',
						xtype: 'textarea',
						height : 100,
						labelWidth : 110,
						fieldLabel: 'Memo',
					    maxLength: 100, 
					    anchor: '-5',
					    value : <?php echo j($row['MEMO']); ?>							
					} 		
						 		
						        
						        
					],
				buttons: [
				  <?php if(count($ar_evasi) > 0){?>
		
				    {
					            text: 'Ripristina',
						        iconCls: 'icon-button_black_repeat_dx-24',		            
						        scale: 'medium',		            
					            handler: function() {
						            var form = this.up('form').getForm();
						            var loc_win = this.up('window');
						             Ext.Msg.confirm('Richiesta conferma', 'Confermi RIAPERTURA ToDo?' , function(btn, text){
						            if (btn == 'yes'){
						            Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_ripristina',
							        jsonData: {
							           form_values: form.getValues(),
							           blocco : <?php echo j($m_params->blocco); ?>,
							           list_selected_id : <?php echo acs_je($ar_evasi); ?>,
							           cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>,				        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							          var jsonData = Ext.decode(result.responseText);
						              loc_win.fireEvent('afterOkSave', loc_win);	           
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
						            }
        	   		 			 });
						           
					                
					            }
					        },
					        <?php }?>
					         '->',
				
				{
					            text: 'Conferma',
						        iconCls: 'icon-button_blue_play-24',		            
						        scale: 'medium',		            
					            handler: function() {
						            var form = this.up('form').getForm();
						            var loc_win = this.up('window');
						            Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_upd',
							        jsonData: {
							           form_values: form.getValues(),
							           list_selected_id : <?php echo acs_je($ar_todo); ?>,
							           cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>,
							           blocco : <?php echo j($m_params->blocco); ?>						        
							        },
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							          var jsonData = Ext.decode(result.responseText);
						              loc_win.fireEvent('afterOkSave', loc_win);	           
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
						            
						           
					                
					            }
					        }]             
					
	        }
	]}	
	<?php	
		exit;
	} 
	