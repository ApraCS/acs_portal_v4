<?php

$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
   
  
    $ar_ins['TADT'] 	= $id_ditta_default;
    $ar_ins['TATAID'] 	= $m_table_config['TATAID'];
    
    $sql = "DELETE FROM {$m_table_config['tab_name']} TA WHERE RRN(TA) = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->form_values->rrn));
    
    $ret = array();
    $ret['success'] = true;
    echo json_encode($ret);
    exit;
}

// ******************************************************************************************
// INSERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ar_ins = (array)$m_params->form_values;
    unset($ar_ins['rrn']);
    unset($ar_ins['rec_index']);
    
    if (isset($ar_ins['TADESC']))
        $ar_ins['TADESC'] = acs_toDb($ar_ins['TADESC']);
    
    $ar_ins['TAUSGE'] 	= $auth->get_user();
    $ar_ins['TADTGE']   = oggi_AS_date();
    $ar_ins['TAORGE']   = oggi_AS_time();
    $ar_ins['TADT'] 	= $id_ditta_default;
    $ar_ins['TATAID'] 	= $m_table_config['TATAID'];
    
    $sql = "INSERT INTO {$m_table_config['tab_name']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    
    $ret = array();
    $ret['success'] = true;
    echo json_encode($ret);
    exit;
}



// ******************************************************************************************
// UPDATE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica'){
	
    $ar_upd = (array)$m_params->form_values;
    unset($ar_upd['rrn']);
    unset($ar_upd['rec_index']);
    
	if (isset($ar_upd['TADESC']))
	    $ar_upd['TADESC'] = acs_toDb($ar_upd['TADESC']);

	$sql = "UPDATE {$m_table_config['tab_name']} TA
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE RRN(TA) = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
	           array($m_params->form_values->rrn)
			));

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// GET GRID DATA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_data'){
    
    if(isset($m_params->takey1) && strlen($m_params->takey1) > 0)
        $sql_where = " AND TAKEY1 = '{$m_params->takey1}'";
    
    $sql = "SELECT RRN (TA) AS RRN, TA.*
            FROM {$m_table_config['tab_name']} TA
            WHERE TADT='{$id_ditta_default}'
            AND TATAID = ? {$sql_where}";
       
    $stmt = db2_prepare($conn, $sql); 	echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_table_config['TATAID']));
    $data = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $row['TADESC'] = acs_u8e($row['TADESC']);
        $row['rrn'] = acs_u8e(trim($row['RRN']));
        $data[] = array_map('trim', $row);
    }
    
    echo json_encode($data);
    exit;
}	



// ******************************************************************************************
// GRID VIEW
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
  

	?>
{"success":true, "items": [

   {
			xtype: 'panel',
        	layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            autoScroll: true,
            title: '',
            flex:0.7,
            items: [
				
		{
    		xtype: 'grid',
        	multiSelect: true,
        	store: {
        	flex:0.7,
    	    features: [{
    				ftype: 'filters',
    				encode: false, 
    				local: true,   
    		   		 filters: [
    		       {
    		 		type: 'boolean',
    				dataIndex: 'visible'
    		     }
    		      ]
    			}],
    		xtype: 'store',
    		autoLoad:true,
    		proxy: {
    			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data',
    			type: 'ajax',
    			actionMethods: {
    				read: 'POST'
    			},
    				extraParams: {
                        takey1 : <?php echo j($m_params->takey1); ?>,
               			m_table_config : <?php echo acs_je($m_table_config); ?>		
				},
        				
        				doRequest: personalizza_extraParams_to_jsonData, 
			
					    reader: {
			            type: 'json',
						method: 'POST',						            
			            root: 'root'						            
			        }
				},
        			fields: <?php echo m_gest_tataid_get_fields($m_table_config) ?>
    	}, //store
    		
	        columns: [
	        
	        	<?php m_gest_tataid_get_columns($m_table_config) ?>	
	            
	         ],  
			listeners: {
			  	 selectionchange: function(selModel, selected) { 
				   if(selected.length > 0){
					   var form_dx = this.up('form').up('panel').down('#dx_form');
					   form_dx.getForm().reset();
					   rec_index = selected[0].index;
					   form_dx.getForm().findField('rec_index').setValue(rec_index);
					   acs_form_setValues(form_dx, selected[0].data);
					 //  form_dx.getForm().setValues(selected[0].data);
					}
					
				 }
			
	         }

			 ,viewConfig: {
	         getRowClass: function(record, index) {
	         //return ret;																
	         }   
	    },
			    
	        
        } //grid
        
	]},
	
	{
			xtype: 'form',
			itemId: 'dx_form',
			autoScroll : true,
			title: 'Dettagli',
			bodyStyle: 'padding: 10px',
			bodyPadding: '5 5 0',
			flex:0.3,
			frame: true,
			items: [
			   {xtype: 'textfield', name: 'rrn', hidden : true},
			   {xtype : 'textfield',name : 'rec_index',hidden : true}, 
		       <?php m_gest_tataid_get_form_insert($m_table_config) ?>
			
			
			],
			
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                 {
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		     
		           
			
			            }

			     }, '->',
                 {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
			
			}	
				
				
		]} 

]
}
<?php 
exit;
} 


//function utility
function m_gest_tataid_get_fields($m_table_config){
	$r = array('TADT', 'TATAID', 'rrn');
	foreach ($m_table_config['fields'] as $kf=>$f)
		$r[] = $kf;
	return acs_je($r);
}


function m_gest_tataid_get_columns($m_table_config){
	foreach ($m_table_config['fields'] as $kf=>$f) {
		?>
			{
				header: <?php echo j($f['label'])?>,
			 	dataIndex: <?php echo j($kf)?>, 
			 	<?php echo isset($f['fw']) ? $f['fw'] : 'flex: 1' ?>,
			 	filter: {type: 'string'}, filterable: true,
			 	
			 },		
		<?php
	}
}


function m_gest_tataid_get_form_insert($m_table_config){
	foreach ($m_table_config['fields_key'] as $kf) {
	    
	    switch($m_table_config['fields'][$kf]['type']){
	            case 'user':
	                get_user_combo($m_table_config, $kf);
	                break;
	            case 'from_TA':
	                get_from_TA($m_table_config, $kf);
	                break;
	            case 'numeric':
	                get_numberfield($m_table_config, $kf);
	                break;
	            default:
	                get_textfield($m_table_config, $kf);
	                break;
	        }
	}

	
}

function get_textfield($m_table_config, $kf){
    
    $maxLen = 100;
    if(isset($m_table_config['fields'][$kf]['maxLength']))
        $maxLen = $m_table_config['fields'][$kf]['maxLength'];
   
    $value = '';
    if(isset($m_table_config['fields'][$kf]['value']))
        $value = $m_table_config['fields'][$kf]['value'];
    
    ?>
				{ 
					xtype: 'textfield',
					name: <?php echo j($kf) ?>, 
					fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>, 
					anchor: '-5',
					value : <?php echo j($value); ?>,
					maxLength: <?php echo $maxLen; ?>,
					/*listeners:{
                       change:function(field){
                            field.setValue(field.getValue().toUpperCase());
                       }
                   }*/
				},
		
		<?php
    
}

function get_numberfield($m_table_config, $kf){
    
    if(isset($m_table_config['fields'][$kf]['maxLength']))
        $maxLen = $m_table_config['fields'][$kf]['maxLength'];
        else
            $maxLen = 10;
            
            ?>
				{ 
					xtype: 'numberfield',
					name: <?php echo j($kf) ?>, 
					fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>, 
					width: '150', 
					labelWidth: 40, 
					hideTrigger : true,
					maxLength: 1,
					margin: "0 10 0 0",
			
				},
		
		<?php
    
}



function get_user_combo($m_table_config, $kf){
    
    $users = new Users;
    $ar_users = $users->find_all();
        
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']),  j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
    $ar_users_json = acs_je($user_to);
            ?>
				 {
						xtype: 'combo',
						name: <?php echo j($kf) ?>,
						fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>,
						forceSelection: true,								
						displayField: 'descr',
			            valueField: 'cod',							
						emptyText: '- seleziona -',
				   		width: '150', 
						labelWidth: 40, 
						margin: "0 10 0 0",
						store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
						typeAhead: true,
    					queryMode: 'local',                             
    					anyMatch: true, 
        				listeners   : {
							beforequery: function(record){  
							    record.query = new RegExp(record.query, 'ig');
    							record.forceAll = true;
							}
						}			
						 },
		
		<?php
    
}


function get_from_TA($m_table_config, $kf){
    $module = $m_table_config['module'];
    
    
        ?>
				 {
						xtype: 'combo',
						name: <?php echo j($kf) ?>,
						fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>,
						anchor: '-5',
						//labelWidth: 40, 
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',							
						emptyText: '- seleziona -',
				   		store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [							
						      <?php echo acs_ar_to_select_json($module->find_TA_std($m_table_config['fields'][$kf]['TAID'], null, 'N', 'N', null, null, null, 'N', 'Y'), ''); ?>	
						    ]
						}
								
						 } ,
		
		<?php
    
}
