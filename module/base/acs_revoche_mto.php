<?php

require_once("../../config.inc.php");

$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

$k_ordine = $m_params->k_ordine;
$oe = $s->k_ordine_td_decode_xx($k_ordine);

// ******************************************************************************************
// GET JSON DATA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
   
   
    $sql = "SELECT MT.*, CFRGS1
            FROM {$cfg_mod_Spedizioni['file_proposte_MTO']} MT
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli']} CF
            ON MT.MTDT = CF.CFDT AND MT.MTFORN = CF.CFCD AND CFTICF = 'F'
            WHERE MTDT = '{$id_ditta_default}' AND MTSTAR IN ('R', 'C') 
            AND MTDT = ? AND MTTIDO = ? AND MTINUM = ? AND MTAADO = ? AND MTNRDO = ?
            ORDER BY MTDTGE DESC, MTHMGE DESC";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $oe);
    $ret  = array();
    
    while ($row = db2_fetch_assoc($stmt)) {
        $nr = array();
        $nr = $row;
        if(trim($row['MTSTAR']) == 'R')
            $nr['stato'] = 'Visionata';
        else
            $nr['stato'] = 'Da visionare';
        $nr['d_forn'] = trim($row['CFRGS1']);
        
        $nr['generazione'] = print_date($row['MTDTGE']);
        
        if(trim($row['MTAVAN']) == 'P')
            $nr['documento'] = "OF: ".implode('_', array($row['MTAADQ'], $row['MTNRDQ']))." Riga {$row['MTRIGQ']}";
        if(trim($row['MTAVAN']) == 'E')
            $nr['documento'] = "DDT: ".implode('_', array($row['MTAADE'], $row['MTNRDE']))." Riga {$row['MTRIGE']}";
        
        $ret[] = $nr;
    }
    echo acs_je($ret);
    exit();
} 


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_win'){
//----------------------------------------------------------------------
   
    ?>
        {"success":true, 
        	m_win: {
        		title: 'Revoche MTO ' + <?php echo j($oe['TDOADO']."_".$oe['TDONDO'])?>
        		, iconCls: 'icon-shopping_cart-16'
        		, width: 950
        		, height: 500
        	},
        	"items": [
        		{
                   xtype: 'grid',

                   store: {
                   		xtype: 'store',
    					autoLoad:true,
    					proxy: {
	           				type: 'ajax', timeout: 240000, actionMethods: {read: 'POST'},
                            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
                            extraParams: <?php echo acs_je($m_params); ?>,
                            doRequest: personalizza_extraParams_to_jsonData,
                            reader: {type: 'json', method: 'POST', root: 'root'}
                        },

						fields: [
	            		'generazione', 'stato', 'documento', 'd_forn', 'MTSTAR', 'MTDTRE', 'MTFORN', 'MTDART', 'MTART', 'MTUMTE', {name: 'MTQTAT', type: 'float'}
	        			]
	        			
	        			, reader: new Ext.data.JsonReader()
                   }, //store
                   
				    columns: [
				        {
                        	header    : 'Generazione',
                        	dataIndex : 'generazione',
                            width     : 90
                         },
                        {
                        	header    : 'Data revoca',
                        	dataIndex : 'MTDTRE',
                            width     : 90,
                            renderer  : date_from_AS
                         }, {
                            header    : 'Revoche',
                            dataIndex : 'stato',
                            width     : 70
                         }, {
                            header    : 'Fornitore',
                            dataIndex : 'd_forn',
                            flex      : 1
                         }, {
                            header    : 'Descrizione',
                            dataIndex : 'MTDART',
                            flex      : 1
                         }, {
                            header    : 'Codice',
                            dataIndex : 'MTART',
                            width      : 100
                         },{
                            header    : 'Um',
                            dataIndex : 'MTUMTE',
                            width     : 30
                         }, {
                            header    : 'Q.t&agrave;',
                            dataIndex : 'MTQTAT',
                            width     : 50,
                            align	  : 'right', 
                            renderer  : floatRenderer2
                         }, {
                            header    : 'Documento',
                            dataIndex : 'documento',
                            width      : 130
                         }
                     ]
				 
            	} //grid
        	]
        }
<?php } exit;