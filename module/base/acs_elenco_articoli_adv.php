<?php

/* In apertura posso ricevere in m_params:
    esclusi_sosp: 'Y'
    ord_ass: 'Y'
    k_ordine: 6 _VO_VO1_2017_000111 _ _6  
*/

require_once "../../config.inc.php";
require_once "acs_elenco_articoli_adv_include.php";
require_once "acs_elenco_articoli_adv_compo.php";

$m_params = acs_m_params_json_decode();




//****************************************************
if ($_REQUEST['fn'] == 'get_json_data_art'){
//****************************************************
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_json_data_art($m_params)
    ));
    exit;
}


//****************************************************
if ($_REQUEST['fn'] == 'get_json_data_tree_classe_fornitore'){
//****************************************************
    $proc = new ApiProc();
    $proc->out_json_response(array(
          'success'=>true
        , 'children' => _get_json_data_tree_classe_fornitore($m_params)
    )); 
	exit;
}


//****************************************************
// WIN PRINCIPALE
//****************************************************
$proc = new ApiProc();
$proc->out_json_response(array(
    'success'=>true
  , 'items' => _ea_adv_main_w($m_params)
));    
exit;
