<?php

require_once "../../config.inc.php";
require_once "acs_seleziona_ordini_include.php";
require_once "acs_seleziona_ordini_compo.php";

$m_params = acs_m_params_json_decode();

//****************************************************
if ($_REQUEST['fn'] == 'get_json_data_ordini'){
//****************************************************
    $proc = new ApiProc();
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => _get_json_data_ordini($m_params)
    ));
    exit;
}

//****************************************************
// WIN PRINCIPALE
//****************************************************
$proc = new ApiProc();
$proc->out_json_response(array(
    'success'=>true
  , 'items' => _ea_adv_main_w($m_params)
));    
exit;
