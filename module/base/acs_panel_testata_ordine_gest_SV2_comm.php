{  
	xtype : 'form',
    title : 'Condizioni commerciali',
    frame: true,
    items : [
    
    <?php if($from_protocollazione != 'Y'){?>
        {
        	xtype: 'fieldset', title: 'Riferimento',
        	layout: {type: 'vbox', pack: 'start', align: 'stretch'},
        	anchor: '-10',
        	defaults:{xtype: 'textfield'},
        	items: [{
                xtype: 'fieldcontainer',
                layout: {type: 'hbox', pack: 'start', align: 'stretch'},
                anchor: '-10',
                defaults: {xtype: 'textfield'},
                items: [{
            		name: 'TDVSRF',
            		fieldLabel: 'Riferimento', flex: 1,
            		maxLength: 30,
            	    value: <?php echo j(trim(acs_u8e($ord['TDVSRF']))); ?>							
                }, {xtype: 'tbfill'}]
            }]
        }
      <?php }?>
        //---------------------------
        // Coordinate bancarie
        //---------------------------
		, {
			xtype: 'fieldset', title: 'Coordinate bancarie/Pagamento',
			layout: {type: 'vbox', pack: 'start', align: 'stretch'},
			anchor: '-10',
			defaults:{ labelWidth: 120, xtype: 'textfield'},
			items: [{
                xtype: 'fieldcontainer',
                layout: {type: 'hbox', pack: 'start', align: 'stretch'},
                anchor: '-10',
                defaults: {xtype: 'textfield'},
                items: [{
            		name: 'TDBANC',
            		fieldLabel: 'Vostra banca', width: 150,
            		maxLength: 3,
            		readOnly : true, 
            	    value: <?php echo j(trim(acs_u8e($ord['TDBANC']))); ?>							
                }, {
                	name: 'TDABI',
                	fieldLabel: '', 
                	flex: 1,
                	readOnly : true,
                    maxLength: 160, 
                    hidden: true,
                    value: <?php echo j(trim(acs_u8e($ord['TDABI']))); ?>							
                }, {
                	name: 'TDCAB',
                	fieldLabel: '', 
                	flex: 1,
                	readOnly : true,
                    maxLength: 160, 
                    hidden: true,
                    value: <?php echo j(trim(acs_u8e($ord['TDCAB']))); ?>							
                }, {
                	name: 'des_cuba',
                	fieldLabel: '', 
                	flex: 1,
                	readOnly : true,
                    maxLength: 160, 
                    value: <?php
                        $r_CUBA = get_TA_sys('CUBA', $ord['TDBANC']);
                        echo j(trim(acs_u8e($r_CUBA['text'].$r_CUBA['TADES2']))); 
                    ?>							
                }, {										  
					xtype: 'displayfield',
					editable: false,
					fieldLabel: '',
					padding: '0 0 0 5',
				    value: <?php echo j("<img src=" . img_path("icone/48x48/sub_red_delete.png") . " width=16>"); ?>,
				    listeners: {
			            render: function( component ) {
			                m_form = this.up('form').getForm();
			                component.getEl().on('dblclick', function( event, el ) {
							
        					m_form.findField('TDBANC').setValue('');
        					m_form.findField('TDABI').setValue('');
        					m_form.findField('TDCAB').setValue('');
        					m_form.findField('des_cuba').setValue('');
		        			       
		        				
							});										            
			             }
					}										    
				},{
                	xtype: 'displayfield',
                	editable: false,
                	fieldLabel: '',
                	padding: '0 0 0 10',
                    value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
        			listeners: {
    		            render: function( component ) {
    		            	m_form = this.up('form').getForm();
    						component.getEl().on('dblclick', function( event, el ) {
    					    my_listeners = {
            					afterSelected: function(from_win, row){
            					 	m_form.findField('des_cuba').setValue(row.descrizione);
    	        					m_form.findField('TDBANC').setValue(row.codice);
    	        					m_form.findField('TDCAB').setValue(row.cab);
    	        					m_form.findField('TDABI').setValue(row.abi);
    	        			        from_win.close();
    	        				}
    					    },
    						acs_show_win_std('Ricerca banca codificata [CUBA]', '../desk_gest/acs_ricerca_tab_sys.php?fn=open_tab', {taid : 'CUBA'}, 1000, 400, my_listeners, 'icon-leaf-16');    
    						});
    		             }
    				}
        		}]
            },	
			{
				xtype: 'fieldcontainer',
				layout: {type: 'hbox'},
				anchor: '-10',
				items: [
				<?php 
				if (trim($ord['TDCCON']) == 'STD') 
				   $allowBlank = 'false';
				else
				   $allowBlank = 'true';
				?>
    			{
    			    xtype : 'combo',
                    name: 'TDPAGA',
                    value: <?php echo j(trim($ord['TDPAGA'])); ?>,
                    flex : 1,
                    fieldLabel: 'Pagamento',
                    displayField: 'text',
                    valueField: 'id',
                    anchor: '-10',
                    editable : true,
                    forceSelection:true,
                    allowBlank: <?php echo $allowBlank ?>,													
                  	store : {
        				autoLoad: true,
        				pageSize: 1000,            	
        				proxy: {
        					type: 'ajax',
        					url : '../desk_gest/acs_panel_ins_new_anag.php?fn=get_json_data_pagamento',
        					reader: {
        						type: 'json',
        						method: 'POST',		
        						root: 'root',
        					},
        					actionMethods: {
							  	read: 'POST'
							},
        					extraParams: {
								gruppo: <?php echo j($request['g_paga']); ?>
						    },
        					doRequest: personalizza_extraParams_to_jsonData
        				},       
        					fields: ['id', 'text', 'sosp', 'color']   
        					<?php if(trim($ord['TDPAGA']) != ''){?>
            					,listeners: {
            					   load: function(store, a, b, c) {
                                        if(store.proxy.extraParams.gruppo != '')
                                        	store.insert(0, {id: <?php echo j($ord['TDPAGA'])?>, text: <?php echo j("[".trim($ord['TDPAGA']). "] ".trim($ord['des_paga'])) ?>});
                                    }
                                }    
                            <?php }?> 	
        			},
                    tpl: [
                    	 '<ul class="x-list-plain">',
                            '<tpl for=".">',
                            '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                                '</tpl>',
                                '</ul>'
                    		 ],
                               // template for the content inside text field
                            displayTpl: Ext.create('Ext.XTemplate',
                                   '<tpl for=".">',
                                        '{text}',
                                   '</tpl>'
                                     
                                    ),
                        
                    	queryMode: 'local',
                    	minChars: 1,	
                        listeners: { 
                        	   beforequery: function (record) {
                        		record.query = new RegExp(record.query, 'i');
                        		record.forceAll = true;
                        	},
                        
                        	 
                        	 select: function(combo, newValue, oldValue, eOpts) {
                        	    if(combo.value == 'ALL'){
                        	       combo.store.proxy.extraParams.gruppo = '';
                        	       combo.setValue('');
                        	       combo.store.load();
                        	     
                        	       //combo.focus();
                        	      
                        	    }
    							
    						 },
    						 afterrender: function(comp){
    						   <?php if(trim($ord['TDPAGA']) != ''){?>
                                 data = [{id: <?php echo j($ord['TDPAGA'])?>, text: <?php echo j("[".trim($ord['TDPAGA']). "] ".trim($ord['des_paga'])) ?>}
                                 ];
                                 comp.store.loadData(data);
                                 comp.setValue(<?php echo j($ord['TDPAGA'])?>);
                               <?php }?>              
                              } 
                    	} 					 
                    },
			    	<?php write_combo_std('TDTSIV', 'Rata IVA', $ord['TDTSIV'], acs_ar_to_select_json($main_module->find_TA_std('ANC03', null, 'N', 'N', null, null, null, 'N', 'Y'), '', true, 'N', 'Y'), array('labelAlign' => 'right') ) ?>
					
				]
			}]
		}
		
        //---------------------------
        // Agenti
        //---------------------------		
        			
		, {
			xtype: 'fieldset', title: 'Agenti',
			layout: {type: 'vbox', pack: 'start', align: 'stretch'},
			anchor: '-10',
			defaults:{ labelWidth: 120, xtype: 'textfield'},
			items: [{
				xtype: 'fieldcontainer',
				layout: {type: 'hbox', pack: 'start', align: 'stretch'},
				anchor: '-10',
				defaults:{xtype: 'textfield'},
				items: [{
		            xtype: 'combo',
					name: 'TDAG1',
					flex: 2,
					fieldLabel: 'Agente (1)',
					store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}, {name:'color'}, {name:'sosp'}, 'perc_provv', 'area_manager'],
						    data: [<?php echo acs_ar_to_select_json($deskGest->find_sys_TA('CUAG', array(
						            'value'  => $ord['TDAG1'],
						            'i_sosp' => 'N', 'cli_cc' => 'Y')), '', 'R'); ?>] 
					},
		            
		            value: '<?php echo trim($ord['TDAG1']); ?>',            
					valueField: 'id',                       
		            displayField: 'text',
		            anchor: '100%',
		            queryMode: 'local',
					minChars: 1,
					tpl: [
    				 '<ul class="x-list-plain">',
                        '<tpl for=".">',
                        '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                        '</tpl>',
                        '</ul>'
    				 ],
                       // template for the content inside text field
                    displayTpl: Ext.create('Ext.XTemplate',
                           '<tpl for=".">',
                                '{text}',
    
                            '</tpl>'
                     
                    ),
			        listeners: {
			            select: function(combo, row, index) {
						   this.up('form').getForm().findField('TDPA1').setValue(row[0].data.perc_provv);
			            }, 
			              beforequery: function (record) {
	         				record.query = new RegExp(record.query, 'i');
	         				record.forceAll = true;
				 		}
			        }
		        }, {
						xtype: 'numberfield', hideTrigger:true,
						name: 'TDPA1',
						fieldLabel: '% provv.', labelAlign: 'right',
					    flex: 1,
					    value: <?php echo j(trim(acs_u8e($ord['TDPA1']))); ?>							
					}
				]}, {
    				xtype: 'fieldcontainer',
    				layout: {type: 'hbox', pack: 'start', align: 'stretch'},
    				anchor: '-10',
    				defaults:{xtype: 'textfield'},
    				items: [{
			            xtype: 'combo',
						name: 'TDAG2',
						flex: 2,
						fieldLabel: 'Agente (2)',
						store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}, 'perc_provv', {name:'color'}, {name:'sosp'}],
							    data: [<?php echo acs_ar_to_select_json($deskGest->find_sys_TA('CUAG', array('value'  => $ord['TDAG2'], 'cli_cc' => 'Y')), '', 'R'); ?>] 
						},
			            
			            value: '<?php echo trim($ord['TDAG2']); ?>',            
						valueField: 'id',                       
			            displayField: 'text',
			            anchor: '100%',
			            queryMode: 'local',
						minChars: 1,
						tpl: [
        				 '<ul class="x-list-plain">',
                            '<tpl for=".">',
                            '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                            '</tpl>',
                            '</ul>'
        				 ],
                           // template for the content inside text field
                        displayTpl: Ext.create('Ext.XTemplate',
                               '<tpl for=".">',
                                    '{text}',
        
                                '</tpl>'
                         
                        ),
				        listeners: {
				            select: function(combo, row, index) {
							   this.up('form').getForm().findField('TDPA2').setValue(row[0].data.perc_provv);								            
				            },
				            
				            beforequery: function (record) {
    	         				record.query = new RegExp(record.query, 'i');
    	         				record.forceAll = true;
    				 		}
				        }            
					
			        },
					{
						xtype: 'numberfield', hideTrigger:true,
						name: 'TDPA2',
						fieldLabel: '% provv.', labelAlign: 'right',
					    flex: 1,
					    value: <?php echo j(trim(acs_u8e($ord['TDPA2']))); ?>							
					}
				]
			},
			 
			{
				xtype: 'fieldcontainer',
				layout: {type: 'hbox', pack: 'start', align: 'stretch'},
				anchor: '-10',
				defaults:{xtype: 'textfield'},
				items: [
					{
			            xtype: 'combo',
						name: 'agente3',
						flex: 2,
						fieldLabel: 'Agente (3)',
						store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}, 'perc_provv', {name:'color'}, {name:'sosp'}],
							    data: [<?php echo acs_ar_to_select_json($deskGest->find_sys_TA('CUAG', array('value'  => $ord['agente3'], 'cli_cc' => 'Y')), '', 'R'); ?>] 
						},
			            
			            value: '<?php echo trim($ord['agente3']); ?>',
						valueField: 'id',                       
			            displayField: 'text',
			            anchor: '100%',
			            queryMode: 'local',
						minChars: 1,
						tpl: [
        				 '<ul class="x-list-plain">',
                            '<tpl for=".">',
                            '<li class="x-boundlist-item listItmes" style="background-color:{color}">{text}</li>',
                            '</tpl>',
                            '</ul>'
        				 ],
                           // template for the content inside text field
                        displayTpl: Ext.create('Ext.XTemplate',
                               '<tpl for=".">',
                                    '{text}',
        
                                '</tpl>'
                         
                        ),
				        listeners: {
				            select: function(combo, row, index) {
							   this.up('form').getForm().findField('provv3').setValue(row[0].data.perc_provv);								            
				            }, beforequery: function (record) {
    	         				record.query = new RegExp(record.query, 'i');
    	         				record.forceAll = true;
    				 		}
				        }            
					
			        },
					{
						xtype: 'numberfield', hideTrigger:true,
						name: 'provv3',
						fieldLabel: '% provv.', labelAlign: 'right',
					    flex: 1,
					    value: <?php echo j(trim(acs_u8e($ord['provv3']))); ?>							
					}
				]
			}
			
		]
	}
		
    //---------------------------
    // Vendita
    //---------------------------					
	, {
		xtype: 'fieldset', title: 'Vendita',
		layout: {type: 'vbox', pack: 'start', align: 'stretch'},
		anchor: '-10',
		defaults:{ labelWidth: 120, xtype: 'textfield'},
		items: [{
			xtype: 'fieldcontainer',
			layout: {type: 'hbox', pack: 'start', align: 'stretch'},
			anchor: '-10',
			items: [
				<?php write_combo_std('TDREFE', 'Referente', $ord['TDREFE'], acs_ar_to_select_json(find_TA_sys('BREF', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 2.8") ) ?>,
			
				{
					name: 'TDSC1',
					xtype: 'numberfield', hideTrigger:true,
					fieldLabel: 'Sconti righe',
					labelAlign : 'right', 
				    maxLength: 5, width : 145,
				    value: <?php echo j(trim(acs_u8e($ord['TDSC1']))); ?>							
				}, {
					name: 'TDSC2',
					xtype: 'numberfield', hideTrigger:true,
					labelWidth : 20,
					fieldLabel: '+', labelAlign: 'right',
				    maxLength: 5, width : 65,
				    labelSeparator : '',
				    value: <?php echo j(trim(acs_u8e($ord['TDSC2']))); ?>							
				}, {
					name: 'TDSC3',
					xtype: 'numberfield', hideTrigger:true,
					labelWidth : 20,
					fieldLabel: '+', labelAlign: 'right',
				    maxLength: 5, width : 65,
				    labelSeparator : '',
				    value: <?php echo j(trim(acs_u8e($ord['TDSC3']))); ?>							
				}, {
					name: 'TDSC4',
					xtype: 'numberfield', hideTrigger:true,
				    labelWidth : 20,
					fieldLabel: '+', labelAlign: 'right',
				    maxLength: 5, width : 65,
				    labelSeparator : '',
				    value: <?php echo j(trim(acs_u8e($ord['TDSC4']))); ?>							
				}
			
			]
		},		
    	{
    		xtype: 'fieldcontainer',
    		layout: {type: 'hbox', pack: 'start', align: 'stretch'},
    		anchor: '-10',
    		defaults:{xtype: 'textfield'},
    		items: [
    		
    		<?php write_combo_std('TDLIST', 'Listino', $ord['TDLIST'], acs_ar_to_select_json(find_TA_sys('BITL', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 2.8") ) ?>,
    		
    		{
    				name: 'TDSC5',
    				xtype: 'numberfield', hideTrigger:true,
    				fieldLabel: 'Sconti docum.',
    				labelAlign : 'right', 
    			    maxLength: 5, width : 145,
    			    value: <?php echo j(trim(acs_u8e($ord['TDSC5']))); ?>							
    			}, {
    				name: 'TDSC6',
    				xtype: 'numberfield', hideTrigger:true,
    				labelWidth : 20,
    				fieldLabel: '+', labelAlign: 'right',
    			    maxLength: 5, width : 65,
    			    labelSeparator : '',
    			    value: <?php echo j(trim(acs_u8e($ord['TDSC6']))); ?>							
    			}, {
    				name: 'TDSC7',
    				xtype: 'numberfield', hideTrigger:true,
    				labelWidth : 20,
    				fieldLabel: '+', labelAlign: 'right',
    			    maxLength: 5, width : 65,
    			    labelSeparator : '',
    			    value: <?php echo j(trim(acs_u8e($ord['TDSC7']))); ?>							
    			}, {
    				name: 'TDSC8',
    				xtype: 'numberfield', hideTrigger:true,
    			    labelWidth : 20,
    				fieldLabel: '+', labelAlign: 'right',
    			    maxLength: 5, width : 65,
    			    labelSeparator : '',
    			    value: <?php echo j(trim(acs_u8e($ord['TDSC8']))); ?>							
    			}
    		  
    		
    		]
    	}, {
			xtype: 'fieldcontainer',
			layout: {type: 'hbox', pack: 'start', align: 'stretch'},
			anchor: '-10',
			defaults:{xtype: 'textfield'},
			items: [
				<?php write_combo_std('TDIVDO', 'Assoggettamento', $ord['TDIVDO'] /*$r['GCIVES']*/, acs_ar_to_select_json($main_module->find_TA_std('CUAE', null, 'N', 'N', null, null, null, 'N', 'Y', 'Y'), ''), array('flex_width' => "flex: 1") ) ?>,
				<?php write_combo_std('TDVALU', 'Valuta', $ord['TDVALU'], acs_ar_to_select_json(find_TA_sys('VUVL', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'right', 'flex_width' => "width: 340") ) ?>,
	    ]},  {
			xtype: 'fieldcontainer',
			layout: {type: 'hbox', pack: 'start', align: 'stretch'},
			anchor: '-10',
			defaults:{xtype: 'textfield'},
			items: [
				<?php write_combo_std('TDCAUT', 'Causale vendita', $ord['TDCAUT'], acs_ar_to_select_json(find_TA_sys('VUCT', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('flex_width' => "flex: 1", 'labelWidth' => 100) ) ?>,
				, {xtype : 'component', width: 340}
		    ]},{
			xtype: 'fieldcontainer',
			flex : 1,
			layout: {type: 'hbox', pack: 'start', align: 'stretch'},
			anchor: '-10',
			defaults:{xtype: 'textfield'},
			items: [
				{
					xtype: 'fieldcontainer',
					flex : 1.45,
					layout: {type: 'hbox', pack: 'start', align: 'stretch'},
					anchor: '-10',
					defaults:{xtype: 'textfield'},
					items: [
						<?php write_combo_std('TDLING', 'Lingua', $ord['TDLING'], acs_ar_to_select_json(find_TA_sys('VULN', null, null, null, null, null, 0, '', 'Y', 'Y'), ''), array('labelAlign' => 'left', 'flex_width' => "flex: 1", "allowBlank" => 'true') ) ?>,
						]
					},
					{
			xtype: 'fieldcontainer',
			flex : 1,
			anchor: '-10',
			layout: { 	type: 'hbox',
				    pack: 'start',
				    align: 'stretch'},						
			items: [
			    {   xtype: 'textfield',
				    name: 'TDCINT',
                    hidden : true,
				},
				
				<?php $decod = get_TA_sys('VUDE', $ord['TDCINT'], $ord['TDCCON']);
				      $desc = "[".trim($ord['TDCINT'])."] ".trim($decod['text']);  
				?>
				
				
                {   xtype: 'textfield',
				    name: 'f_pven', 
				    flex : 1,
				    fieldLabel: 'Punto vendita',
                    labelAlign: 'right',
                    readOnly : true,
                    value : <?php echo j($desc); ?>
				},
				{										  
				  xtype: 'displayfield',
				  fieldLabel: '',
				  padding: '0 0 0 5',
				  value: <?php echo j("<img src=" . img_path("icone/48x48/search.png") . " width=16>"); ?>,
				  listeners: {
							render: function( component ) {
							   var form = this.up('form').getForm();
                               
								component.getEl().on('dblclick', function( event, el ) {
								
									var my_listeners_search_loc = {
										afterSelected: function(from_win, record_selected){
											    form.findField('TDCINT').setValue(record_selected.cod);	
												form.findField('f_pven').setValue(record_selected.denom);
                                               
                                                from_win.close();
												}										    
									};
									
								
									 acs_show_win_std('Seleziona destinazione', 
			 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
									{cliente : <?php echo j($ord['TDCCON'])?>, type : 'P'
									 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
									 
								});										            
							 }
						}
				}
				
				]
		}
			    ]}
			]
		}],			
	    <?php if($from_protocollazione != 'Y'){?>	   
   	 buttons: [{
   	            text: 'Salva',
                iconCls: 'icon-save-32', scale: 'large',
                handler: function() {
                	var form = this.up('form').getForm();
                	var loc_win = this.up('window');
                	var tabPanel = loc_win.down('tabpanel');
                	
        			if(form.isValid()){
        				Ext.getBody().mask('Loading... ', 'loading').show();
        				Ext.Ajax.request({
        						timeout: 240000,
        				        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_form_comm',
        			        jsonData: { 
        			        	form_values: form.getValues(),
        			        	open_request: <?php echo acs_je($m_params) ?>
        			        },	
        			        method     : 'POST',
        			        waitMsg    : 'Data loading',
        			        success : function(result, request){
        			        						        
        						Ext.getBody().unmask();
        			            var jsonData = Ext.decode(result.responseText);
        			            
        			            if (jsonData.success){
        			            	acs_show_msg_info('Salvataggio completato');
        			            	loc_win.destroy()
        			            } else {
        			            	if (jsonData.message)
        			            		acs_show_msg_error(jsonData.message);
        			            	else
        			            		acs_show_msg_error('Errore in fase di salvataggio');
        			            }															            
        			            
        			        },
        			        failure    : function(result, request){
        						Ext.getBody().unmask();							        
        			            Ext.Msg.alert('Message', 'No data to be loaded');
        			        }
        			    });						
        	    	}	            	
            	} //handler
        	} //salva
    	]
    	<?php }?>
}