<?php

set_time_limit("999999");
ini_set("max_execution_time","999999");
ini_set("max_input_time", "999999");

require_once("../../config.inc.php");

error_reporting(E_ERROR);
ini_set('display_errors','On');

$m_params = acs_m_params_json_decode();


// emogrifier function
$xmldoc = new DOMDocument();
$xmldoc->strictErrorChecking = false;
$xmldoc->formatOutput = true;
$xmldoc->loadHTML(utf8_decode($m_params->html_text));

$xmldoc->normalizeDocument();

$html_tag = $xmldoc->getElementsByTagName('html')->item(0);
$head_tag = $html_tag->appendChild($xmldoc->createElement('head'));
$node = $head_tag->appendChild($xmldoc->createElement('meta'));
$node->setAttribute('http-equiv', 'content-type');
$node->setAttribute('content', 'application/vnd.ms-excel; charset=UTF-8');

$html = $xmldoc->saveHTML();

$body = acs_emogrify($html, utf8_decode($m_params->style_text));
		
echo trim($body);
exit;
?>