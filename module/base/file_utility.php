<?php

require_once "../../config.inc.php";

// ******************************************************************************************
// DELETE FILE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete_file'){
	    $ret = array();

	    $m_params = acs_m_params_json_decode();
	    
	    
	    foreach($m_params->list_selected_id as $path){
	    	
	        unlink($path->attachments[0]);
	    
	    }

		$ret['success'] = true;
		echo acs_je($ret);
		exit;
}