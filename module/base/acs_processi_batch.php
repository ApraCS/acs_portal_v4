<?php

require_once("../../config.inc.php");


$main_module = new DeskUtility(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

//$funzione, $utente
function check_verifica_permesso(){
    
    global $auth, $cfg_mod_DeskArt, $id_ditta_default, $conn;
    $m_utente = trim($auth->get_user());
    
    $sql = "SELECT TA_B.TAREST AS TAREST, TA_U.TADES2 AS TADES2
            FROM {$cfg_mod_DeskArt['file_tabelle_sys']} TA_B
            LEFT OUTER JOIN {$cfg_mod_DeskArt['file_tabelle_sys']} TA_U
            ON /*TA_B.TADT = TA_U.TADT AND*/ TA_U.TAID = 'USER' AND TA_U.TANR = '*GR' 
            AND TA_U.TACOR1 = 'PIPPO' AND TA_U.TACOR2 = '*ST'
            WHERE TA_B.TADT = '{$id_ditta_default}' AND TA_B.TAID = 'BRIS' AND TA_B.TANR = 'BRIS'
            AND TA_B.TACOR1 = 'BR21A0'
            ";
   
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    $s_utenti = substr($row['TAREST'], 8, 128);
    $list_ut = array();
    for($i=0; $i<=112; $i+=8){
        $utente = substr($s_utenti, $i, 8);
        if(trim($utente) != '')
            $list_ut[] = $utente;
    }
    
    $g_utente = substr($row['TAREST'], 158, 15);
    $list_gr = array();
    for($i=0; $i<=12; $i+=3){
        $gruppo = substr($g_utente, $i, 3);
        if(trim($gruppo) != '')
          $list_gr[] = $gruppo;
    }
    
    
    $ar_gruppo = explode(",", $row['TADES2']);
    
    $ar_value = array_intersect($list_gr, $ar_gruppo);
    
 
    if(in_array($auth->get_user(), $list_ut) || count($ar_value) > 0)
        return true;
    else 
        return false;
    
}

//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'get_json_data'){
//---------------------------------------------------------------------
    $cfg_mod = (array)$m_params->open_request->file_tabelle;
    $sql_where = '';
    $menu_type = $m_params->open_request->menu_type;
    
    if(isset($menu_type) && strlen($menu_type)>0){
        if($menu_type == 'ORD_VEN'){
            $sql_where .= " AND TA_G.TAKEY2 = 'DOVE' /*AND TA_G.TAKEY3 = 'OV'*/";
            $j_where .= " AND TA_B.TAKEY2 = 'DOVE' /*AND TA_B.TAKEY3 = 'OV'*/";
        }
        if($menu_type == 'PRO_MON'){
            $sql_where .= " AND TA_G.TAKEY2 = 'DOVE' /*AND TA_G.TAKEY3 = 'PM'*/";
            $j_where .= " AND TA_B.TAKEY2 = 'DOVE' /*AND TA_B.TAKEY3 = 'PM'*/";
        }
    }
    
    $sql = "SELECT TA_G.TAKEY1 AS GRUPPO, TA_B.TAKEY1 AS BATCH, TA_G.TADESC AS D_GR, 
            TA_B.TADESC AS D_BATCH, TA_B.TATELE AS MSG_RI, TA_B.TAINDI AS RICHIAMA_FUNZIONE,
            TA_G.TAMAIL AS G_NOTE, TA_B.TAMAIL AS B_NOTE, TA_B.TAKEY3 AS TAKEY3
            FROM {$cfg_mod['file_tabelle']} TA_G
            LEFT OUTER JOIN {$cfg_mod['file_tabelle']} TA_B
            ON TA_G.TADT = TA_B.TADT AND TA_B.TATAID = 'BATCH' AND TA_G.TAKEY1 = TA_B.TARIF1 {$j_where}
            WHERE TA_G.TADT = '{$id_ditta_default}' AND TA_G.TATAID = 'BATCG' {$sql_where}
            ORDER BY TA_G.TAKEY1, TA_B.TAKEY1    
";
    
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while ($row = db2_fetch_assoc($stmt)){
        
        $liv1 = trim($row['GRUPPO']);  
        $liv2 = trim($row['BATCH']); 
        $tmp_ar_id = array();
        
        //liv1: GRUPPO
        $d_ar = &$ar;
        $c_liv = $liv1;
        $tmp_ar_id[] = $c_liv;
        if (!isset($d_ar[$c_liv])){
            $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());
            $d_ar[$c_liv]['liv']   = 'liv_1';
            $d_ar[$c_liv]['liv_type']   = 'GRUPPO';
            $d_ar[$c_liv]['liv_c'] = $liv1;
            $d_ar[$c_liv]['task']  = "[{$liv1}] ".acs_u8e($row['D_GR']);
            $d_ar[$c_liv]['note']  = acs_u8e($row['G_NOTE']);
            $d_ar[$c_liv]['expanded'] = true;
        }
        
        
        //liv2: BATCH
        $d_ar = &$d_ar[$c_liv]['children'];
        $c_liv = $liv2;
        $tmp_ar_id[] = $c_liv;
        if (!isset($d_ar[$c_liv])){
            $d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id),  "children"=>array());
            $d_ar[$c_liv]['liv']   = 'liv_3';
            $d_ar[$c_liv]['liv_c'] = $liv2;
            $d_ar[$c_liv]['liv_type']   = 'BATCH';
            $d_ar[$c_liv]['menu']  = $row['TAKEY3'];
            $d_ar[$c_liv]['d_funz']   = acs_u8e($row['D_BATCH']);
            $d_ar[$c_liv]['task']  = "[{$liv2}] ".acs_u8e($row['D_BATCH']);
            $d_ar[$c_liv]['msg_ri']  = trim($row['MSG_RI']);
            $d_ar[$c_liv]['note']   = acs_u8e($row['B_NOTE']);
            $d_ar[$c_liv]['richiama_funzione']  = trim($row['RICHIAMA_FUNZIONE']);
            $d_ar[$c_liv]['leaf']  = true;
            $d_ar[$c_liv]['disabled'] = check_verifica_permesso();
    
        }
        
    } //while
    
    
    
    
    $ret = array();
    foreach($ar as $kar => $v) $ret[] = array_values_recursive($ar[$kar]);
    
    echo acs_je($ret);
    exit;
}



//----------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){
//----------------------------------------------------------------
?>

{"success":true, "items": [

        {
        xtype: 'treepanel' ,
        multiSelect: 'true',
           tbar: new Ext.Toolbar({
	            items:[ '->', 
	                    {
        	             iconCls: 'icon-gear-16',
        	             text: 'Gruppi',
        	             tooltip: 'BATCG',
        	             handler: function(event, toolEl, panel){
        	             		acs_show_win_std(null, 
								'acs_gest_BATCG.php?fn=open_tab', {menu_type: <?php echo j($m_params->menu_type); ?>}, 950, 500, null, 'icon-gear-16');
    			          
    		           		 
    		           		 }
    		           	 },
    		           	  {
        	             iconCls: 'icon-gear-16',
        	             text : 'Funzioni',
        	             tooltip: 'BATCH',
        	             handler: function(event, toolEl, panel){
        	             		acs_show_win_std(null, 
								'acs_gest_BATCH.php?fn=open_tab', {menu_type: <?php echo j($m_params->menu_type); ?>}, 950, 500, null, 'icon-gear-16');
    			          
    		           		 
    		           		 }
    		           	 },
	         ]            
	        }),
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,         
		    store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                    fields: ['id', 'task', 'liv', 'liv_type', 'liv_c', 'msg_ri', 'd_funz', 'note', 'menu', 'richiama_funzione'],
				    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
						extraParams: {
						  open_request: <?php echo acs_je($m_params); ?>
	                    }
                    , doRequest: personalizza_extraParams_to_jsonData     
					, reader: {root: 'children'}        				
                    }

                }),
                
    	    			
            columns: [ {xtype: 'treecolumn', 
        	    		text: 'Gruppo/Funzione', 	
        	    		flex : 1,
        	    		dataIndex: 'task',
        	    		},
        	    		{text: 'Segnalazioni', 	
        	    		flex : 1,
        	    		dataIndex: 'note',
        	    		}
        	    		    		
    	    ],
    	    
			listeners: {	
 				itemcontextmenu : function(grid, rec, node, index, event) {
					grid.panel.acs_actions.show_menu_dx(grid, rec, node, index, event);
    			}, 
    			
    			 celldblclick: {								
					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						var rec = iView.getRecord(iRowEl), 
							col_name = iView.getGridColumns()[iColIdx].dataIndex;
						 
						 if (Ext.isEmpty(rec.get('richiama_funzione'))){
						 	//apro form richiesta parametri in base a config parametri
						 	acs_show_win_std('Esegui ' + rec.get('d_funz'), '../base/acs_processi_batch.php?fn=open_form', 
	    	        	 		{batch : rec.get('liv_c'), msg_ri : rec.get('msg_ri'), file_tabelle : <?php echo j($m_params->file_tabelle); ?>, menu_type: rec.get('menu')}, 500, 450, null, 'icon-button_blue_play-16');
						 } else {
						 	//apro win in base a richiama_funzione
						 	acs_show_win_std(null, '../' + rec.get('richiama_funzione'), {d_funz : rec.get('d_funz')});
						 	
						 }
						}
						
    				}
    		
			}
			
	  //funzioni / utility  	
  	  , acs_actions: {   	
      	 show_menu_dx: function(grid, rec, node, index, event) {
      	 	event.stopEvent();
      	 	 var voci_menu = [];
      	        
      	           voci_menu.push({
		      		text: 'Parametri',
		    		iconCls: 'icon-leaf-16',
		    		handler: function() {  
		    		    	acs_show_win_std(null, 
						   'acs_gest_BATCP.php?fn=open_tab', {takey1 : rec.get('liv_c'), menu_type: rec.get('menu')}, 1100, 500, null, 'icon-gear-16');
		    		   }
                    });
                    
        			
			var menu = new Ext.menu.Menu({
        	items: voci_menu
			}).showAt(event.xy);
			
			return false;
      	} //show_menu_dx
    },	viewConfig: {
				 toggleOnDblClick: false,
		         getRowClass: function(record, index) {			        	
		           v = record.get('liv');		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');		           
 		           return v;																
		         }   
		    }	
    	    
    	    
    	}	
	 	
	]  	
  }


<?php 
exit;
}


//----------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_form'){
//----------------------------------------------------------------
?>

   {"success":true, "items": [
		{
			xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            flex : 1,
		    autoScroll : true,
            title: '',
			items: [
			
			{xtype: 'displayfield', hidden : true , name : 'error'},
			
			 <?php 
			 $cfg_mod = (array)$m_params->file_tabelle;
			 $ret = get_component($cfg_mod, $m_params->batch);
			 $proc = new ApiProc();
			 $proc->out_json_response($ret);
			 
			 ?>
			
			]
			
			,buttons: [ 
				{
                    text: 'Conferma esecuzione',
                    scale: 'large',
                    iconCls: 'icon-button_blue_play-32',
                    handler: function () {
                   
                        var m_win = this.up('window');
                        var form = this.up('form').getForm();
    	            	//var form_values = form.getFieldValues();
    	            	var form_values = form.getValues();
    	            	
    	            	/*for (var f in form_values) {
    	            	     if (f == 'error') {
                                delete form_values[f];
                            }
    	            	    if (form.findField(f).xtype == 'datefield'){
    	            	      var vn = form.findField(f).getSubmitValue();
    	            	      form_values[f] = vn;
    	            	    }
    	            	}*/
    	            	
    	            	
    	            	if (!form.isValid()) return false;
    	            	
    	            	
    	            	<?php 
    	            	$lancia = '';
    	            	    
    	            	   
    	            	    $sql = "SELECT COUNT(*) AS C_ROWS
    	            	    FROM {$cfg_mod['file_richieste']}
    	            	    WHERE RIESIT NOT IN ('C', 'W') AND RIRGES = '{$m_params->msg_ri}'";
    	            	    
    	            	    
    	            	    $stmt = db2_prepare($conn, $sql);
    	            	    echo db2_stmt_errormsg();
    	            	    $result = db2_execute($stmt);
    	            	    echo db2_stmt_errormsg($stmt);
    	            	    $r = db2_fetch_assoc($stmt);
    	            	    
    	            	    if($r['C_ROWS'] == 0)
    	            	        $lancia = 'Y';

                           ?>
                           
                           	var my_listeners = {
		        				afterLancia: function(from_win, ret_RI){
		        				   console.log(ret_RI);
		        				    if(ret_RI.RIESIT == 'W'){
    		        				    var testo = 'Esito <b>W, </b> ' + ret_RI.RINOTE + '. Riesegui:'
    		        				    form.findField('error').setValue(testo);
    		        				    form.findField('error').show();
    		        				    from_win.close();
		        				    }else{
		        				        from_win.close();
		        				    	m_win.close();
		        				    }
		        					
						        	}								  
								  }	
    	            	
    	            	acs_show_win_std('Sincronizzazione', '../base/acs_submit_job.php?fn=open_form', {
    	            	file_tabelle : <?php echo j($m_params->file_tabelle); ?>, 
    	            	lancia : <?php echo j($lancia); ?>,
           				RIRGES: <?php echo j($m_params->msg_ri); ?>,
           				form_values,
           				batch : <?php echo j($m_params->batch); ?>,
           				menu_type: <?php echo j($m_params->menu_type); ?>
           				} , 650, 250, my_listeners, 'icon-listino'); 
   
                    }
            	}]
                        	
            }
   
   ]}
    
<?php }

function get_component($cfg_mod, $batch){
    
    global $cfg_mod_DeskArt, $id_ditta_default, $conn;
    
    $c = new Extjs_compo('fieldcontainer', layout_ar('vbox'));
    
    $sql = "SELECT *
            FROM {$cfg_mod['file_tabelle']} TA
            WHERE TADT = '{$id_ditta_default}' AND TATAID = 'BATCP'
            AND TAKEY1 = '{$batch}'
            ORDER BY TASITI
            ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $campo_accodato = '';
    while($row = db2_fetch_assoc($stmt)){
        
        switch(trim($row['TAFG01'])){
           case 'A':
               $c->add_items(array(get_articolo($row, $batch, $campo_accodato)));
                break;
           case 'X':
               $c->add_items(array(get_from_TA($row)));
                break;
           case 'W':
               $c->add_items(array(get_from_TA($row)));
               break;
           case 'N':
               $c->add_items(array(get_numberfield($row)));
               break;
           case 'T':
               $c->add_items(array(get_textfield($row)));
               break;
           case 'D':
               $c->add_items(array(get_datefield($row)));
               break;
           case 'F':
               $c->add_items(array(get_fornitore($row)));
               break;
         
        }
   
        if(trim($row['TATISP']) != ''){
            $campo_accodato = 'Y';
        }else{
            $campo_accodato = '';
        }
        
    }
    
    return $c->code();
    
}

function get_from_TA($row){
    
    global $desk_art, $main_module;
    
  if(trim($row['TAFG02']) == 'Y')   $allowBlank  = false;
    else     $allowBlank = true;
    
    $tacor1 = null;
    if(isset($row['TATELE']) && strlen(trim($row['TATELE'])) > 0)
        $tacor1 = trim($row['TATELE']);
    $tacor2 = null;
    if(isset($row['TAASPE']) && strlen(trim($row['TAASPE'])) > 0)
        $tacor2 = trim($row['TAASPE']);
    
    if(trim($row['TAFG01']) == 'X') 
        $ar_value = find_TA_sys($row['TACOGE'], null, $tacor1, $tacor2, null, null, 0, '', 'Y', 'N', 'R');
    else if(trim($row['TAFG01']) == 'W'){
        if(trim($row['TAFG03']) == 'Y'){
            $module = $desk_art;
        }else{
            $module = $main_module;
        }
        $ar_value = $module->find_TA_std($row['TACOGE'], null, 'N', 'N', null, null, null, 'N', 'Y');
    }
       
     $numberows = $row['TARIF1'];
       
     $combo = extjs_combo(array(
            'fieldLabel'   => trim($row['TADESC']),
            'labelWidth'   => 160,
            'name'         => trim($row['TAKEY2']),
            'initialData'  => $ar_value,
            'initialValue' => trim($row['TAINDI']),
            'queryMode'    => 'local',
            'allowBlank'   => $allowBlank,
            'selectRows' => $numberows
        ));
     
     
     
     return $combo;
}

function get_numberfield($row){
    
    if(trim($row['TAFG02']) == 'Y')   $allowBlank  = false;
    else     $allowBlank = true;
    
    $numberfield =  array(
        'xtype' => 'numberfield',
        'name'=>   trim($row['TAKEY2']),
        'labelWidth' => 160,
        'fieldLabel' => trim($row['TADESC']),
        'allowBlank' => $allowBlank,
        'hideTrigger' => true,
        'forcePrecision' => true
    );
    
    return $numberfield;
}

function get_textfield($row){
    
    if(trim($row['TAFG02']) == 'Y')   $allowBlank  = false;
    else     $allowBlank = true;
    
    
    if(isset($row['TAPROV']) && strlen(trim($row['TAPROV'])) > 0){
        $readOnly  = true;
        $value = trim($row['TAPROV']);
    }else{
        $readOnly = false;
        $value = '';
    }
    
    
    
    $textfield =  array(
        'xtype' => 'textfield',
        'name'=>   trim($row['TAKEY2']),
        'labelWidth' => 160,
       // 'maxLength'=>   trim($row['TAKEY3']),
        'fieldLabel' => trim($row['TADESC']),
        'allowBlank' => $allowBlank,
        'readOnly' => $readOnly,
        'value' => $value,
        
    );
    
    return $textfield;
}

function get_articolo($row, $batch, $campo_accodato){
    
    global $desk_art;
   
    if(trim($row['TAFG02']) == 'Y')   $allowBlank  = false;
    else     $allowBlank = true;
    
    $textfield = array();
    if($campo_accodato == ''){
        $textfield[] =  array(
            'xtype' => 'textfield',
            'name'=>   trim($row['TAKEY2']),
            'labelWidth' => 80,
            'maxLength'=>   15,
            'width' => 200,
            'fieldLabel' => trim($row['TADESC']),
            'allowBlank' => $allowBlank,
            'listeners'   => array(
                'blur' => extjs_code("
                    function(field){
                        var form = this.up('form').getForm();
                        if(field.getValue() != ''){
                        Ext.Ajax.request({
 						        url     : '" . $_SERVER['PHP_SELF'] . "?fn=exe_check',
 						        method  : 'POST',
 			        			jsonData: {codice : field.getValue()},
 								success: function(result, request) {
						        	 var jsonData = Ext.decode(result.responseText);
                					 if(jsonData.msg_error != ''){
					      	    		acs_show_msg_error(jsonData.msg_error);
                                        field.setValue('');
                                     }
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
                            }
                        }"
                 
                ))
            
        );
    
    
    if(trim($row['TATISP']) != ''){
        $row_s = $desk_art->get_TA_std('BATCP', $batch, trim($row['TATISP']));
        
        if(trim($row_s['TAFG02']) == 'Y')   $allowBlank  = false;
        else     $allowBlank = true;
     
        $textfield[] =  array(
            'xtype' => 'textfield',
            'name'=>   trim($row_s['TAKEY2']),
            'labelWidth' => 80,
            'width' => 200,
            'anchor' => '-15',
            'labelAlign' => 'right',
            'maxLength'=>   15,
            'fieldLabel' => trim($row_s['TADESC']),
            'allowBlank' => $allowBlank,
             'listeners'   => array(
                'blur' => extjs_code("
                    function(field){
                        var form = this.up('form').getForm();
                        if(field.getValue() != ''){
                            form.findField('".trim($row['TAKEY2'])."').allowBlank = false;
                            
                            Ext.Ajax.request({
 						        url     : '" . $_SERVER['PHP_SELF'] . "?fn=exe_check',
 						        method  : 'POST',
 			        			jsonData: {codice : field.getValue()},
 								success: function(result, request) {
						        	 var jsonData = Ext.decode(result.responseText);
                					 if(jsonData.msg_error != ''){
					      	    		 acs_show_msg_error(jsonData.msg_error);
                                         field.setValue('');
                                     }
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
                    
 						    });
                            }else{
                                form.findField('".trim($row['TAKEY2'])."').allowBlank = true;
                            }
                        }"
                    
                    ))
            
        );
      
    }
    }
 
    
    $articolo = array('xtype' => 'fieldcontainer',
        'layout' => layout_ar('hbox'),
        'items' => $textfield);
    
  
    return $articolo;
}

function get_datefield($row){
    
    $datefield = extjs_datefield(trim($row['TAKEY2']), $row['TADESC'], null, array('labelWidth' => 160));
    return $datefield;
}


function get_fornitore($row){
    
    $fornitore = extjs_combo_fornitore(array('name' => trim($row['TAKEY2']), 'labelWidth' => 160, 'fieldLabel' => $row['TADESC']));
    return $fornitore;
}

//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_check'){
//---------------------------------------------------------------------
    $sql = "SELECT COUNT(*) AS C_ROWS
            FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
            WHERE ARART = '{$m_params->codice}'";
  
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    $error_cod = "";
    if($row['C_ROWS'] == 0){
        $error_cod = "Codice articolo non esistente";
    }
    
    $ret['success'] = true;
    $ret['msg_error'] = $error_cod;
    echo acs_je($ret);
    exit;
    
    
}

