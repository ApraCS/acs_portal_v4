<?php


/* ------------------------------------------------------------- */
function _get_json_data_art($m_params){
/* ------------------------------------------------------------- */
  global $conn, $id_ditta_default, $cfg_mod_DeskPVen, $cfg_mod_DeskArt, $cfg_mod_Spedizioni;
  
  //da config.inc.php
  if (isset($cfg_mod_DeskPVen["add_where_to_elenco_articoli"]))
      $where_art .= " " . $cfg_mod_DeskPVen["add_where_to_elenco_articoli"] . " ";
  
  if (isset($m_params->classe_fornitore)){
    $cf_ar = explode('|', $m_params->classe_fornitore);
    $where_art .= " AND ARCLME = " . sql_t($cf_ar[0]);
    $where_art .= " AND ARFOR1 = " . sql_t($cf_ar[1]);
  }
  
  if (isset($m_params->ricerca_solo_fornitore) && $m_params->ricerca_solo_fornitore == 'Y')
      $where_art .= " AND ARFOR1 <> 0 ";
      
  
  if (isset($m_params->form_values))
      $where_art .= add_sql_where($m_params->form_values);
  

  //se ricevo un k_ordine, filtro il listino in base a quello dell'ordine stesso
  if (isset($m_params->open_parameters) && isset($m_params->open_parameters->k_ordine)){
      $s = new Spedizioni(array('no_verify' => 'Y'));
      $oe = $s->k_ordine_td_decode_xx($m_params->open_parameters->k_ordine);
      $ord = $s->get_ordine_gest_by_k_docu($m_params->open_parameters->k_ordine);
      $cod_listino = $ord['TDLIST'];
      $cod_valuta  = $ord['TDVALU'];
      $tipo_listino  = 'V'; //ToDo: recperare dalla tipologia ordine

      $join_listino = "LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_listini']} LI
                        ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART
                        AND LI.LILIST = '{$cod_listino}'
                        AND LI.LIVALU = '{$cod_valuta}'
                        AND LI.LITPLI = '{$tipo_listino}'";
  } else {
      //di base JOIN con il listino di vendita. Ma non ha senso perche' esplode i risultati!!!
      //era cosi' nel PVen!
      //ToDo:
      $join_listino = "LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_listini']} LI
                        ON LI.LIDT = AR.ARDT AND LI.LIART = AR.ARART AND LI.LITPLI = 'V'";
  }
      
  
  //solo articoli promo? (solo_promo = 'Y')
  if (isset($m_params->open_parameters) && $m_params->open_parameters->solo_promo == 'Y'){
      $where_art .= " AND ARART LIKE 'MP%' AND ARSOSP='' AND ARESAU NOT IN ('C', 'R')";
      
      if (isset($cfg_mod_Spedizioni["add_where_to_elenco_articoli_promo"]))
          $where_art .= " " . $cfg_mod_Spedizioni["add_where_to_elenco_articoli_promo"] . " ";
      
  }
  
      
  $sql="SELECT TA_ALLEGATI.NR_ALLEGATI, ARART, ARDART, ARMODE, ARARFO, ARUMTE, MAX(LI.LIPRZ) AS LIPRZ
        FROM {$cfg_mod_DeskPVen['file_anag_art']} AR
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_cli']} CF
           ON CF.CFDT = AR.ARDT AND CF.CFCD = AR.ARFOR1
        {$join_listino}

        /* Allegati */
        LEFT OUTER JOIN (
           SELECT COUNT(*) AS NR_ALLEGATI, TADT, TATAID, TAKEY1
           FROM {$cfg_mod_DeskArt['file_tabelle']}
           GROUP BY TAKEY1, TADT, TATAID) TA_ALLEGATI
     	ON TA_ALLEGATI.TADT = AR.ARDT AND TA_ALLEGATI.TATAID = 'ARTAL' AND TA_ALLEGATI.TAKEY1 = AR.ARART

        WHERE ARDT = '{$id_ditta_default}' $where_art
        GROUP BY ARART, ARDART, ARMODE, ARARFO, ARUMTE, TA_ALLEGATI.NR_ALLEGATI
        LIMIT 1000";
  
  
  $stmt = db2_prepare($conn, $sql);
  echo db2_stmt_errormsg();
  $result = db2_execute($stmt);
  
  $ret = array();
  
  while ($row = db2_fetch_assoc($stmt)) {
    $row['c_art'] = "[".trim($row['ARART'])."]"; 
    $row['d_art'] = trim($row['ARDART']);
    $row['config_par']	= '';
    $row['CONFIG']		= trim($row['ARMODE']);
    $ret[] = $row;      
  }

  return grid_to_json_data($ret);
}



/* ------------------------------------------------------------- */
function _get_json_data_tree_classe_fornitore($m_params){
/* ------------------------------------------------------------- */
  global $conn, $id_ditta_default, $cfg_mod_DeskPVen, $cfg_mod_Spedizioni;
  $ret = array();
  
  //da config.inc.php
  if (isset($cfg_mod_DeskPVen["add_where_to_elenco_articoli"]))
    $where_art .= " " . $cfg_mod_DeskPVen["add_where_to_elenco_articoli"] . " ";
  
    if (isset($m_params->ricerca_solo_fornitore) && $m_params->ricerca_solo_fornitore == 'Y')
      $where_art .= " AND ARFOR1 <> 0 ";
    
  if (isset($m_params->form_values))
    $where_art .= add_sql_where($m_params->form_values);
    

  //solo articoli promo? (solo_promo = 'Y')
  if (isset($m_params->open_parameters) && $m_params->open_parameters->solo_promo == 'Y'){
      $where_art .= " AND ARART LIKE 'MP%' AND ARSOSP='' AND ARESAU NOT IN ('C', 'R')";
      
      if (isset($cfg_mod_Spedizioni["add_where_to_elenco_articoli_promo"]))
          $where_art .= " " . $cfg_mod_Spedizioni["add_where_to_elenco_articoli_promo"] . " ";
  }
        
  $sql="SELECT ARCLME, ARFOR1, CFRGS1, TA_CLASSE.TADESC AS D_CLASSE
        FROM {$cfg_mod_DeskPVen['file_anag_art']} AR
        LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_anag_cli']} CF
          ON CF.CFDT = AR.ARDT AND CF.CFCD = AR.ARFOR1
          LEFT OUTER JOIN {$cfg_mod_DeskPVen['file_tab_sys']} TA_CLASSE
		  ON TA_CLASSE.TADT = AR.ARDT AND TA_CLASSE.TAID = 'MUCM' AND TA_CLASSE.TANR = AR.ARCLME  
        WHERE ARDT = '{$id_ditta_default}' 
        {$where_art}
        GROUP BY ARCLME, TA_CLASSE.TADESC, ARFOR1, CFRGS1";

  $ar_vals = create_tree_data_from_sql($sql, 
    array(
        'ARCLME' => array('f_descr' => 'D_CLASSE', 'descr_add_code' =>'Y'),           
        'ARFOR1' => array('f_descr' => 'CFRGS1', 'descr_add_code' =>'Y')         
    )
  );
    
  return tree_to_json_data($ar_vals);       
}


function add_sql_where($form_values){
    $where_art = "";
    
    if ($form_values->f_cod_art)
        $where_art .= sql_where_by_combo_value('UPPER(ARART)', strtoupper($form_values->f_cod_art), 'LIKE');
  
    if ($form_values->f_descr)
        $where_art .= sql_where_by_combo_value('UPPER(ARDART)', strtoupper($form_values->f_descr), 'LIKE');
   
    if ($form_values->f_forn)
        $where_art .= sql_where_by_combo_value('ARFOR1', $form_values->f_forn);
    
    if ($form_values->f_riferimento)
        $where_art .= sql_where_by_combo_value('UPPER(ARARFO)', strtoupper($form_values->f_riferimento), 'LIKE');
        
    return $where_art;
    
}