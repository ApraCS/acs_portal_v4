<?php

require_once "../../config.inc.php";
$mod_base = new Base();
$m_params = acs_m_params_json_decode();

$s = new Spedizioni(array('no_verify' => 'Y'));
$oe   = $s->k_ordine_td_decode_xx($_REQUEST['k_ordine']);

?>
<?php header('Content-type: application/x-java-jnlp-file'); ?>
<?php header('Content-disposition: attachment; filename="test.jnlp"'); ?>

<jnlp spec="1.0" codebase="http://<?php echo($_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI'])) ?>">
  <information>
    <title>Acs CALL Demo</title>
    <vendor>Apra Computer System - Pesaro</vendor>
  </information>
  <security>
	  <all-permissions/>
  </security>  
  <resources>
    <property name="jnlp.publish-url" value="$$context/publish"/>
    <j2se version="1.3+" href="http://java.sun.com/products/autodl/j2se"/>
    <jar href="myApplet.jar"/>
  </resources>
  <application-desc main-class="MyApplet">
    <argument>c:\\tecne\\metron4\\APRATOMETRON.EXE <?php echo trim($oe['TDONDO']); ?></argument>
  </application-desc>
</jnlp>