<?php

function cerved_api_call_search($piva){
    global $cerved_user_data;
    
    if (!isset($cerved_user_data)){
        return array('success' => false, 'message' => 'Configurazione Cerved non trovata');
    }
    
    if ($cerved_user_data['api_version'] == '1.2.0'){
        return cerved_api_call_search_1_2_0($piva);
    } else {
        return cerved_api_call_search_default($piva);
    }
}



//******************************************************************
function cerved_api_call_search_default($piva){
//******************************************************************
    global $cerved_user_data;
    $service_url = "https://cas.cerved.com/oauth2/oauth/token?grant_type=password&client_id=cerved-client&username={$cerved_user_data['username']}&password={$cerved_user_data['password']}";
    
    $curl = cerved_api_call_prepare_curl($service_url);
    
    // Send the request & save response to $resp
    $resp = curl_exec($curl);
    
    $resp_json = json_decode($resp);
    $token = $resp_json->access_token;
    $token_type = $resp_json->token_type;
    
    // Close request to clear up some resources
    curl_close($curl);
    
    $taxCode = $piva;
    
    //RICHIEDO INFO AZIENDA
    $service_url = "https://bi.cerved.com:8444/smartservice/rest/cervedapi/companies?&taxCode={$piva}&registered=N";
    
    $headr = array();
    $headr[] = 'Accept: application/json';
    $headr[] = 'Authorization: '. $token_type . ' ' . $token;
    
    $curl = cerved_api_call_prepare_curl($service_url, $headr);
    
    // Send the request & save response to $resp
    $resp = curl_exec($curl);
    
    $headerSent = curl_getinfo($curl, CURLINFO_HEADER_OUT );
    
    $resp_json = json_decode($resp);
    
    if($resp_json->foundPositions == false){
        $ret['success'] = false;
        $ret['error_msg'] = 'Partita IVA non presente nella banca dati CERVED';        
        return $ret;
    }
    
    //scrivo i parametri ricevuti su IC0
    $key_request = cerved_api_call_write_parametri_cerved_base($resp_json);
    
    // Close request to clear up some resources
    curl_close($curl);
    
    $ret['success'] = true;
    $ret['key_request'] = $key_request;
    $ret['companyId']   = $resp_json->companyInfo[0]->companyId;
    return $ret;
}


//******************************************************************
function cerved_api_call_search_1_2_0($piva){
//******************************************************************
    global $cerved_user_data;
    $taxCode = $piva;
    
    $id_richiesta = microtime(true);
    
    //RICHIEDO INFO AZIENDA (base)
    $service_url = "https://api.cerved.com/cervedApi/v1/entitySearch/live?testoricerca={$piva}";
    
    $headr = array();
    $headr[] = 'accept: application/json';
    $headr[] = 'apikey: '. $cerved_user_data['consumer_key'];
    
    $curl = cerved_api_call_prepare_curl($service_url, $headr);
    
    // Send the request & save response to $resp
    $resp = curl_exec($curl);    
    $headerSent = curl_getinfo($curl, CURLINFO_HEADER_OUT );    
    $resp_json = json_decode($resp);
    
    if($resp_json->companiesTotalNumber == 0){
        $ret['success'] = false;
        $ret['error_msg'] = 'Partita IVA non presente nella banca dati CERVED';
        return $ret;
    }
        
    $cervedCompanyId =  $resp_json->companies[0]->dati_anagrafici->id_soggetto;
    
    // Close request to clear up some resources
    curl_close($curl);
    
    //richiedo pec ***************
    $service_url = "https://api.cerved.com/cervedApi/v1/entityProfile/pec?id_soggetto={$cervedCompanyId}";
    $curl = cerved_api_call_prepare_curl($service_url, $headr);
    
    // Send the request & save response to $resp
    $resp = curl_exec($curl);
    $headerSent = curl_getinfo($curl, CURLINFO_HEADER_OUT );
    $resp_json_pec = json_decode($resp);
    curl_close($curl);
    
    if (!isset($resp_json->companies[0]->acs_dati_aggiuntivi)){
        $resp_json->companies[0]->acs_dati_aggiuntivi = new ArrayObject(array());
    }
    $resp_json->companies[0]->acs_dati_aggiuntivi->pec = $resp_json_pec->indirizzi_email_certificate[0]->email;   
    
    if($resp_json->indirizzi_email_certificate){
        cerved_api_call_write_parametri_cerved_base_1_2_0($id_richiesta, $resp_json);
    }
    
    //scrivo i parametri ricevuti su IC0
    $key_request = cerved_api_call_write_parametri_cerved_base_1_2_0($id_richiesta, $resp_json);

    
    $ret['success'] = true;
    $ret['key_request'] = $key_request;
    $ret['companyId']   = $cervedCompanyId;
    return $ret;
}





//*************************************************************
//*************************************************************
//*************************************************************

function cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, $parametro, $contenuto, $code, $seq, $tipo_contenuto = ''){
    $ar_ins['PARAMETRO'] = acs_u8e($parametro);
    $ar_ins['CONTENUTO'] = acs_u8e($contenuto);
    $ar_ins['CODICE_CONTENUTO'] = acs_u8e($code);
    $ar_ins['TIPO_CONTENUTO'] = $tipo_contenuto;
    $ar_ins['ICSEQU'] 	 = $seq;
    db2_execute($stmt, $ar_ins); echo db2_stmt_errormsg($stmt);
}

function cerved_api_call_write_parametri_cerved_base($resp_json){
    global $auth, $conn, $cfg_mod_Gest;
    
    $ci = $resp_json->companyInfo[0];
    
    $m_user 	= $auth->get_user();
    $m_date 	= oggi_AS_date();
    $m_time 	= oggi_AS_time();
    
    $ar_ins = array();
    $ar_ins['UTENTE_GENERAZ'] 	= $m_user;
    $ar_ins['DATA_GENERAZ'] 	= $m_date;
    $ar_ins['TIME_GENERAZ'] 	= $m_time;
    $ar_ins['COMPANYID']		= $ci->companyId;
    $ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
    $ar_ins['TIPO_RICHIESTA']	= 'BASE';
    
    $ar_ins['PARAMETRO']		= '';
    $ar_ins['CONTENUTO']		= '';
    $ar_ins['CODICE_CONTENUTO']	= '';
    $ar_ins['TIPO_CONTENUTO']	= '';
    $ar_ins['ICSEQU']			= 0;
    
    //insert riga
    $sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'companyName', $ci->companyName, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'street', $ci->address[0]->street, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'postCode', $ci->address[0]->postCode, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'province', $ci->address[0]->province, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'municipality', $ci->address[0]->municipality, $ci->address[0]->municipalityCode, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'activityStatus', $ci->activityStatusDescription, $ci->activityStatus, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'companyType', $ci->companyType, $ci->companyType, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'companyForm ', $ci->companyForm->description, $ci->companyForm->code, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'registrationDate ', strftime('%Y%m%d', strtotime($ci->registrationDate)), '', ++$seq, 'D');
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'ateco07  ', $ci->ateco07Description, $ci->ateco07, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'reaCode  ', implode(' ', array($ci->reaCode->coCProvinceCode, $ci->reaCode->reano)), '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'taxCode  ', $ci->taxCode, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'vatRegistrationNo  ', $ci->vatRegistrationNo, '', ++$seq);
    
    //ritorno la chiave della tabella
    return $ar_ins['CODICE_RICHIESTA'];
}



function cerved_api_call_write_parametri_cerved_base_1_2_0($id_richiesta, $resp_json){
    global $auth, $conn, $cfg_mod_Gest;
    
    $ci = $resp_json->companies[0];
    
    $m_user 	= $auth->get_user();
    $m_date 	= oggi_AS_date();
    $m_time 	= oggi_AS_time();
    
    $ar_ins = array();
    $ar_ins['UTENTE_GENERAZ'] 	= $m_user;
    $ar_ins['DATA_GENERAZ'] 	= $m_date;
    $ar_ins['TIME_GENERAZ'] 	= $m_time;
    $ar_ins['COMPANYID']		= $ci->dati_anagrafici->id_soggetto;
    $ar_ins['CODICE_RICHIESTA']	= $id_richiesta;
    $ar_ins['TIPO_RICHIESTA']	= 'BASE';
    
    $ar_ins['PARAMETRO']		= '';
    $ar_ins['CONTENUTO']		= '';
    $ar_ins['CODICE_CONTENUTO']	= '';
    $ar_ins['TIPO_CONTENUTO']	= '';
    $ar_ins['ICSEQU']			= 0;
    
    //insert riga
    $sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'companyName', $ci->dati_anagrafici->denominazione, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'street', $ci->dati_anagrafici->indirizzo->descrizione, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'postCode', $ci->dati_anagrafici->indirizzo->cap, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'province', $ci->dati_anagrafici->indirizzo->provincia, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'municipality', $ci->dati_anagrafici->indirizzo->descrizione_comune, $ci->dati_anagrafici->indirizzo->codice_comune, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'activityStatus', $ci->dati_attivita->codice_stato_attivita, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'companyType', $ci->dati_attivita->ateco, $ci->dati_attivita->codice_ateco, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'companyForm ', $ci->dati_attivita->company_form->description, $ci->dati_attivita->company_form->code, ++$seq);
    //cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'registrationDate ', strftime('%Y%m%d', strtotime($ci->registrationDate)), '', ++$seq, 'D');
    //cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'ateco07  ', $ci->ateco07Description, $ci->ateco07, ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'reaCode  ', $ci->dati_attivita->codice_rea, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'taxCode  ', $ci->dati_anagrafici->partita_iva, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'vatRegistrationNo  ', $ci->dati_anagrafici->partita_iva, '', ++$seq);
    
    //dati aggiuntivi
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'certifiedEmail  ', $ci->acs_dati_aggiuntivi->pec, '', ++$seq);
    
    //ritorno la chiave della tabella
    return $ar_ins['CODICE_RICHIESTA'];
}







function cerved_api_call_write_parametri_cerved_details($resp_json){
    global $auth, $conn, $cfg_mod_Gest;
    
    $ci = $resp_json->companyInfo;
    
    $m_user 	= $auth->get_user();
    $m_date 	= oggi_AS_date();
    $m_time 	= oggi_AS_time();
    
    $ar_ins = array();
    $ar_ins['UTENTE_GENERAZ'] 	= $m_user;
    $ar_ins['DATA_GENERAZ'] 	= $m_date;
    $ar_ins['TIME_GENERAZ'] 	= $m_time;
    $ar_ins['COMPANYID']		= $ci->companyId;
    $ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
    $ar_ins['TIPO_RICHIESTA']	= 'DETAILS';
    
    $ar_ins['PARAMETRO']		= '';
    $ar_ins['CONTENUTO']		= '';
    $ar_ins['CODICE_CONTENUTO']	= '';
    $ar_ins['TIPO_CONTENUTO']	= '';
    $ar_ins['ICSEQU']			= 0;
    
    //insert riga
    $sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'telephone', 	$ci->telephone, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'email', 		$ci->email, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'certifiedEmail',$ci->certifiedEmail, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'activityStartDate', strftime('%Y%m%d', strtotime($ci->activityStartDate)), '', ++$seq, 'D');
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'paidUpEquityCapital', $ci->paidUpEquityCapital, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'sales', $ci->balanceDetails->sales, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'grossProfit', $ci->balanceDetails->grossProfit, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'profitAndLoss', $ci->balanceDetails->profitAndLoss, '', ++$seq);
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'numberOfEmployes', $ci->balanceDetails->numberOfEmployes, '', ++$seq);
    
    //ritorno la chiave della tabella
    return $ar_ins['CODICE_RICHIESTA'];
}





function cerved_api_call_write_parametri_cerved_scores($resp_json){
    global $auth, $conn, $cfg_mod_Gest;
    
    $ci = $resp_json;
    
    $m_user 	= $auth->get_user();
    $m_date 	= oggi_AS_date();
    $m_time 	= oggi_AS_time();
    
    $ar_ins = array();
    $ar_ins['UTENTE_GENERAZ'] 	= $m_user;
    $ar_ins['DATA_GENERAZ'] 	= $m_date;
    $ar_ins['TIME_GENERAZ'] 	= $m_time;
    $ar_ins['COMPANYID']		= $ci->companyId;
    $ar_ins['CODICE_RICHIESTA']	= $resp_json->requestId;
    $ar_ins['TIPO_RICHIESTA']	= 'SCORES';
    
    $ar_ins['PARAMETRO']		= '';
    $ar_ins['CONTENUTO']		= '';
    $ar_ins['CODICE_CONTENUTO']	= '';
    $ar_ins['TIPO_CONTENUTO']	= '';
    $ar_ins['ICSEQU']			= 0;
    
    //insert riga
    $sql = "INSERT INTO {$cfg_mod_Gest['ins_new_anag']['file_cerved']}(" . create_name_field_by_ar($ar_ins) . ")
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'negativeEventsGrading', 	implode(', ', array(
        $ci->negativeEventsGrading->grading,
        $ci->negativeEventsGrading->descriptiveGradingSynthesisCode,
        $ci->negativeEventsGrading->subScoreClass
    )), $ci->negativeEventsGrading->available, ++$seq);
    
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'economicalFinancialGrading', 	implode(', ', array(
        $ci->economicalFinancialGrading->grading,
        $ci->economicalFinancialGrading->descriptiveGradingSynthesisCode,
        $ci->economicalFinancialGrading->subScoreClass
    )), $ci->economicalFinancialGrading->available, ++$seq);
    
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'paylineGrading', 	implode(', ', array(
        $ci->paylineGrading->grading,
        $ci->paylineGrading->descriptiveGradingSynthesisCode,
        $ci->paylineGrading->subScoreClass
    )), $ci->paylineGrading->available, ++$seq);
    
    cerved_api_call_insert_parametro_in_db($stmt, $ar_ins, 'cervedGroupScore', 	implode(', ', array(
        $ci->cervedGroupScore->grading,
        $ci->cervedGroupScore->descriptiveGradingSynthesisCode,
        $ci->cervedGroupScore->icon
    )), $ci->cervedGroupScore->available, ++$seq);
    
    
    //ritorno la chiave della tabella
    return $ar_ins['CODICE_RICHIESTA'];
}



//*********************************************************
function cerved_api_call_prepare_curl($url, $headr = array()){
//*********************************************************
    $curl = curl_init();
    
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_SSL_VERIFYHOST => false, //disabilito la verifica del certificato altrimenti ho errore SSL
        CURLOPT_SSL_VERIFYPEER => false, //disabilito la verifica del certificato altrimenti ho errore SSL
        CURLOPT_USERAGENT => 'php curl',
        //CURLOPT_HEADER => true, //per debug
        CURLINFO_HEADER_OUT => true,
        CURLOPT_HTTPHEADER => $headr
    ));
    return $curl;
}
