<?php

require_once "../../config.inc.php";
$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_save_single'){
  $ar_params = array();

  if (strlen($m_params->form_values->row_id) > 0){
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} SET NTMEMO=? WHERE NTID=?";
    $ar_params = array($m_params->form_values->f_text, $m_params->form_values->row_id);
  } else {
      if (isset($m_params->open_values->blocco)) $ar_params['NTTPNO'] = $m_params->open_values->blocco;
      if (isset($m_params->open_values->key1))   $ar_params['NTKEY1'] = $m_params->open_values->key1;
      if (isset($m_params->open_values->key2))   $ar_params['NTKEY2'] = $m_params->open_values->key2;
      if (isset($m_params->open_values->seq))    $ar_params['NTSEQU'] = $m_params->open_values->seq;
      $ar_params['NTDT']   = $id_ditta_default;
      $ar_params['NTMEMO'] = $m_params->form_values->f_text;
      $sql = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}(" . create_name_field_by_ar($ar_params) . ") VALUES (" . create_parameters_point_by_ar($ar_params) . ")";
  }
  
  $stmt = db2_prepare($conn, $sql);
  echo db2_stmt_errormsg();
  $result = db2_execute($stmt, $ar_params);
  echo acs_je(array('success' => $result, 'text' => $m_params->form_values->f_text));
  exit;
}

if ($_REQUEST['fn'] == 'edit_single'){
    
    //recupero (se presente) la prima voce in base al blocco/chiave passato    
    $sql = "SELECT * from {$cfg_mod_Spedizioni['file_note']} NT WHERE NTDT={$id_ditta_default}";
    if (isset($m_params->blocco))  $sql .= " AND NTTPNO = " . sql_t($m_params->blocco);
    if (isset($m_params->key1))    $sql .= " AND NTKEY1 = " . sql_t($m_params->key1);
    if (isset($m_params->key2))    $sql .= " AND NTKEY2 = " . sql_t($m_params->key2);
    if (isset($m_params->seq))     $sql .= " AND NTSEQU = " . sql_t($m_params->seq);
    $sql .= " LIMIT 1";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    if ($row){ 
      $row_id   = $row['NTID'];
      $row_text = $row['NTMEMO'];
    }
    else {
      $row_id = null;
      $row_text = '';
    }
    
    ?>{"success":true, "items": [
        	{
        		xtype: 'form'
        	  , layout: {type: 'vbox', pack: 'start', align: 'stretch'}
        	  , bodyPadding: 10
        	  , items: [
        	  		{name: 'row_id', xtype: 'hiddenfield',   value: <?php echo j($row_id); ?>}  
				  , {name: 'f_text', xtype: 'textareafield', flex: 1, fieldLabel: 'Contenuto', labelAlign: 'top', value: <?php echo j($row_text); ?>}        	  
        	    ]
        	  , buttons: [
        	  	{
        	  	    text: 'Salva'
        	  	  , handler: function(){
        	  	    var form = this.up('form').getForm();
        	  	    var win  = this.up('window');
	             	Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_single',
					        method     : 'POST',
		        			jsonData: {
								form_values: form.getValues(),
								open_values: <?php echo acs_je($m_params) ?>
							},							        
					        success : function(result, request){
					            jsonData = Ext.decode(result.responseText);
								
								if (!Ext.isEmpty(win.events.aftersave))
									win.fireEvent('afterSave', win, jsonData)
							    else
							    	win.close();
		            		},
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });        	  	  
        	  	  } 
        	  	}
        	  ]
        	}
         ]
      }    
    <?php
    exit;
}