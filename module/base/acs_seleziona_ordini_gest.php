<?php 

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

//********************************************************
if ($_REQUEST['fn'] == 'get_json_data_ordini'){
//********************************************************
	
	$data_eva_i=$m_params->form_values->f_data_eva_i;
	$data_eva_f=$m_params->form_values->f_data_eva_f;
	
	//controllo data
	if (strlen($data_eva_i) > 0)
		$sql_where .= " AND TDDTEP >= '{$data_eva_i}'";
	if (strlen($data_eva_f) > 0)
		$sql_where .= " AND TDDTEP <= '{$data_eva_f}'";
	
	$data_reg_i=$m_params->form_values->f_data_reg_i;
	$data_reg_f=$m_params->form_values->f_data_reg_f;
	
	if (strlen($data_reg_i) > 0)
		$sql_where .= " AND TDDTRG >= '{$data_reg_i}'";
	if (strlen($data_reg_f) > 0)
		$sql_where .= " AND TDDTRG <= '{$data_reg_f}'";
	
	$cliente = $m_params->form_values->f_cli;
		
	if (strlen($cliente) > 0)	
		$sql_where.= " AND TDCCON = '{$cliente}' ";
	
	$destinazione = $m_params->open_request->cod_dest;
	if(isset($destinazione) && strlen($destinazione) > 0)
	    $sql_where.= " AND TDDEST = '{$destinazione}' ";
	
	$nr_ord =  $m_params->form_values->f_nr_or;
		
	if (strlen($nr_ord) > 0)
	    $sql_where.= " AND TDNRDO = '{$nr_ord}' ";
	
    $anno_ord =  $m_params->form_values->f_anno_or;   
    if (strlen($anno_ord) > 0)
        $sql_where.= " AND TDAADO = {$anno_ord} ";
    
    $sql_where.= sql_where_by_combo_value('TDTPDO', $m_params->form_values->f_tipo_ordine);
    $sql_where.= sql_where_by_combo_value('TDSTAT', $m_params->form_values->f_stato_ordine);
    //$sql_where.= sql_where_by_combo_value('TDSTAT', $m_params->form_values->f_tipologia);
    $sql_where.= sql_where_by_combo_value('TDSTAT', $m_params->form_values->f_divisione);

    if(strlen($m_params->form_values->f_riga) > 0){
        $riga = $m_params->form_values->f_riga;
        $anno = $m_params->form_values->f_anno;
        $anno_or = $m_params->form_values->f_anno_or;
        $nrec = $m_params->form_values->f_nrec;
        $join = "LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
                 ON TD0.TDDT = RD.RDDT AND TD0.TDTIDO = RD.RDTIDO AND TD0.TDINUM = RD.RDINUM AND
                 TD0.TDAADO = RD.RDAADO AND TD0.TDNRDO = RD.RDNRDO";
      
        $select = ", RD.*";
        $sql_where .= " AND RDRIGA = '{$riga}'";
        if($nrec != '')
            $sql_where .= " AND RDNREC = '{$nrec}'";
            
    }
    
    
    //EVENTUALI FILTRI IN BASE A doc_gest_search (eventualmente configurato in config.inc.php)
    if (isset($m_params->open_request->doc_gest_search)){
        $doc_gest_search = $m_params->open_request->doc_gest_search;
        if (isset($cfg_mod_Spedizioni['doc_gest_search']) && 
            isset($cfg_mod_Spedizioni['doc_gest_search'][$doc_gest_search])){
                $doc_gest_search_config = $cfg_mod_Spedizioni['doc_gest_search'][$doc_gest_search];
                $sql_where.= sql_where_by_combo_value('TDTIDO', $doc_gest_search_config['TDTIDO']);
                if($m_params->open_request->show_det != 'Y')
                    $sql_where.= sql_where_by_combo_value('TDSTAT', $doc_gest_search_config['TDSTAT']);
                $sql_where.= sql_where_by_combo_value('TDTPDO', $doc_gest_search_config['TDTPDO']);
        }
    }
   	
  
	$sql = "SELECT TD0.* {$select}
            FROM {$cfg_mod_Spedizioni['file_testate_doc_gest_cliente']} TD0
            {$join}
            WHERE TDDT = '{$id_ditta_default}' {$sql_where} 
            ORDER BY TDAADO DESC, CASE WHEN TDSTAT = 'CC' THEN 9 ELSE 0 END
            FETCH FIRST 100 ROWS ONLY";
            
     
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	
	$sql_td = "SELECT * FROM {$cfg_mod_Spedizioni['file_testate']} 
               WHERE TDDT = '{$id_ditta_default}' AND TDOTID = 'VO' AND 
               TDOADO = ? AND TDONDO = ?";
	

	$stmt_td = db2_prepare($conn, $sql_td);
	echo db2_stmt_errormsg();
	
	$ar = array();
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
	
		$nr['anno']	        = trim($row['TDAADO']);
		$nr['numero']	    = sprintf("%06s", trim($row['TDNRDO']));
		$nr['k_ordine']     = implode("_", array(
		      $row['TDDT'], 
		      $row['TDTIDO'], 
		      $row['TDINUM'], 
		      $row['TDAADO'], 
		      sprintf("%-15s", sprintf("%06s", trim($row['TDNRDO']))), 
		      sprintf("%-10s", ""), 
		      $row['TDDT'] ));
	    
		$nr['riferimento']	= trim($row['TDVSRF']);
	    $nr['ordine']	    = implode("_", array(trim($row['TDAADO']), sprintf("%06s", trim($row['TDNRDO'])), trim($row['TDTPDO']) ));
		$nr['tipologia']	= trim($row['TDTPDO']);
		
		$result = db2_execute($stmt_td, array($row['TDAADO'], sprintf("%06s", trim($row['TDNRDO']))));
		$row_td = db2_fetch_assoc($stmt_td);
		
		$nr['raggr']        = trim($row_td['TDCLOR']);
		$nr['data_eva']	    = trim($row['TDDTEP']);
		$nr['data_val']	    = trim($row['TDDTVA']);
		$nr['data_reg'] 	= trim($row['TDDTRG']);
		$nr['stato']	    = trim($row['TDSTAT']);
		$nr['riga']	        = $row['RDRIGA'];
		$nr['RDDART'] 	    = trim($row['RDDART']);
		$nr['RDART']	    = trim($row['RDART']);
		$nr['RDQTA']	    = $row['RDQTA'];
		$nr['RDNREC']	    = $row['RDNREC'];
		$nr['fl_art_manc']  = $s->get_fl_art_manc($row_td);
		$nr['fl_bloc']      = $s->get_ord_fl_bloc($row_td);
		$nr['art_da_prog'] = $s->get_art_da_prog($row_td);
		$nr['anno_l1']	    = $m_params->form_values->f_anno;
		$nr['numero_l1']	= $m_params->form_values->f_numero;
		$sconti = "";
		if($row['TDSC1'] > 0)
		    $sconti .= n($row['TDSC1'],2) ."%; ";
	    if($row['TDSC2'] > 0)
	        $sconti .= n($row['TDSC2'],2) ."%; ";
        if($row['TDSC3'] > 0)
            $sconti .= n($row['TDSC3'],2) ."%; ";
        if($row['TDSC4'] > 0)
            $sconti .= n($row['TDSC4'],2) ."%; ";
        $nr['sconti'] = $sconti;
        $nr['cod_dest'] = trim($row['TDDEST']);
        $oe = $s->get_ordine_by_k_ordine($nr['k_ordine']);
        $nr['cod_pven'] = trim($row['TDCINT']);
        
        $row_dest = get_TA_sys('VUDE', $nr['cod_dest'], $row['TDCCON'], null, null, null, 0, " AND SUBSTRING(TAREST, 163, 1) IN ('D', '')");
        $nr['desc_dest']= $row_dest['text'];
        
        
        $row_pven = get_TA_sys('VUDE', $nr['cod_pven'], $row['TDCCON'], null, null, null, 0, " AND SUBSTRING(TAREST, 163, 1) = 'P'");
        $nr['desc_pven'] = $row_pven['text'];
        $nr['cliente'] = sprintf("%09s", trim($row['TDCCON']));
        $nr['TDPROD']	    = trim($row['TDPROD']);
        $nr['TDNAZI']	    = trim($row['TDNAZI']);
       
		$ar[] = $nr;
	
	}
	echo acs_je($ar);
	exit;
}


//********************************************************
if ($_REQUEST['fn'] == 'open'){
//********************************************************
	?>
{"success":true, "items": [

     { xtype: 'form'
     , bodyStyle: 'padding: 10px'
     , bodyPadding: '5 5 0'
     , autoScroll:true
     , frame: true
     , layout: 
         {  type: 'vbox',
           align: 'stretch'
    	 }
    	 
     , listeners: {
     	afterrender: function(compo){
     		var b_applica_filtri = compo.down('#b_applica_filtri');
     		<?php if ($m_params->auto_load == true) { ?>
     			b_applica_filtri.fireHandler();
     		<?php } ?>	
     	}
     }	 
    	 
    <?php if($m_params->show_det != 'Y' && $m_params->from_webmail != 'Y'){?>	 
     , buttons: [               
            {
	        	text: 'Seleziona',
	            iconCls: 'icon-sub_blue_accept-32',
	            scale: 'large',	
	       		handler: function() {
	       			var m_win  = this.up('window'),
	       				rec_selected = this.up('form').down('grid').getSelectionModel().getSelection()[0];	       				
	       				
	       				if (!rec_selected){
	       					acs_show_msg_error('Selezionare un ordine');
							return false;
						}	            				
	       				
	       			if (!Ext.isEmpty(m_win.events.afteroksave)) {
                      m_win.fireEvent('afterOkSave', m_win, rec_selected.get('k_ordine'));
                  	}
	       		}
	       	}
	   ]	
    <?php }?>
     , items: [
     
     {
      xtype: 'container',
      layout: 
         {  type: 'vbox',
           align: 'stretch'
    	 },
      hidden: <?php if ($m_params->show_parameters == false) echo j(true); else echo j(false); ?>,
      flex: 1,
      items: [
     
         , { xtype: 'fieldcontainer'
    	 , layout: 
    	     { type: 'hbox'
    		 ,	pack: 'start'
    	     , align: 'stretch'
    	     }
    	 , items: [
    	 <!-- filtro Nr. Ordine -->
    	 
	       { xtype: 'displayfield',
	         fieldLabel: 'Anno / Nr. Ordine',
	         labelWidth : 141,
	         margin: "0 10 0 9"	
	       },
	               
	       { 
	       
	       xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_anno_or'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },{ 
	       
	         xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_nr_or'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },
	       { 
	       
	       xtype: 'textfield'
           , name: 'f_riga'
           , fieldLabel: 'Riga'
           , width : 80
           , labelWidth : 30
           , anchor: '-15'
           , margin: "0 5 0 10"	
	       },
	       
	       
	<?php if (isset($m_params->cod_cli)){ ?>
		   , {
		   		xtype: 'hiddenfield'
		   		, name: 'f_cli'
		   		, value: <?php echo j($m_params->cod_cli) ?>
			}
		   , {
		   		xtype: 'displayfield'
		   		, fieldLabel: 'Cliente'
		   		, value: <?php echo j($m_params->cod_cli) ?>
		   		, labelWidth : 50
		   		, anchor: '-15'
		   		, flex: 1		   		
		   		, margin: "0 10 0 43"
		   	
		   }
	<?php } else { ?>       
    	   <!-- filtro cliente -->
    	   , { flex: 1
    	   , xtype: 'combo'
    	   , name: 'f_cli'
    	   , fieldLabel: 'Cliente'
    	   , labelWidth : 50
    	   , anchor: '-15'
    	   , margin: "0 10 0 43"	
    	   , minChars: 2			
           , store: 
               { pageSize: 1000            	
    		   , proxy: 
    		       { type: 'ajax'
    		       , url : '../desk_vend/acs_get_select_json.php?select=search_cli_anag'
    		       , reader: 
    		           { type: 'json'
    		           , root: 'root'
    		           , totalProperty: 'totalCount'
    			       }
    			   }
    	       , fields: ['cod', 'descr', 'out_loc', 'ditta_orig'],		             	
    	       }
    	   , valueField: 'cod'
    	   , displayField: 'descr'
    	   , typeAhead: false
    	   , hideTrigger: true
    	   , anchor: '100%'
    	   , listConfig: 
    	       { loadingText: 'Searching...'
    	       , emptyText: 'Nessun cliente trovato'
    	       // Custom rendering template for each item
    	       , getInnerTpl: function() 
    	           { return '<div class="search-item">' +
    	                    '<h3><span>{descr}</span></h3>' +
    	                    '[{cod}] {out_loc} {ditta_orig}' + 
    	                    '</div>';
    	           }                
                    
               }
           , pageSize: 1000
    	   }
    	   <?php } ?>   
    	   <!-- end cliente -->
    	   
    	   
		   ] <!-- end items field container -->
		 } <!-- end field container -->
		 
       , { xtype: 'fieldcontainer'
    	 , layout: 
    	     { type: 'hbox'
    		 ,	pack: 'start'
    	     , align: 'stretch'
    	     }
    	 , items: [

	       { xtype: 'displayfield',
	         fieldLabel: 'Anno / Nr. Ordine prod. ',
	         labelWidth : 141,
	         margin: "0 10 0 10"	
	       },
	       
	        { xtype: 'hiddenfield'
           , name: 'f_nrec'
	       },
	       
	       { 
	       
	       xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_anno'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },{ 
	       
	         xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_numero'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       }    	   
		   ] <!-- end items field container -->
		 } <!-- end field container -->         
       , { xtype: 'fieldcontainer'
	     , layout: 
	       { type: 'hbox'
	       , pack: 'start'
	       , align: 'stretch'
		   }
		 , items: [
    	       { name: 'f_data_reg_i'
    		   , margin: "0 10 0 10"   
    		   , flex: 1        
    		   , labelWidth :150   		
    		   , xtype: 'datefield'
    		   , startDay: 1 //lun.
    		   , fieldLabel: 'Data registrazione iniziale'
    		   , labelAlign: 'left'
    		   , format: 'd/m/Y'
    		   , submitFormat: 'Ymd'
    		   , allowBlank: true
    		   , anchor: '-15'
    		   }
    		 , { name: 'f_data_reg_f'
    		   , margin: "0 10 0 10"						     
    		   , flex: 1						                     		
    		   , xtype: 'datefield'
    		   , labelWidth :120
    		   , startDay: 1 //lun.
    		   , fieldLabel: 'finale'
    		   , labelAlign: 'right'						   
    		   , format: 'd/m/Y'
    		   , submitFormat: 'Ymd'
    		   , allowBlank: true
    		   , anchor: '-15'
    		   }
		   ]
		 }
		 
	   ,     		 { xtype: 'fieldcontainer'
					 , layout: { type: 'hbox'
							   , pack: 'start'
							   , align: 'stretch'
								}
					 , items: [
						       { name: 'f_data_eva_i'
							   , margin: "0 10 0 10"   
							   , flex: 1        
							   , labelWidth :150    		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data evasione iniziale'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   }
							    
							,  { name: 'f_data_eva_f'
							   , margin: "0 10 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , labelWidth :120 
							   , startDay: 1 //lun.
							   , fieldLabel: 'finale'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   }
						
					   ] 
					 },	 
					 
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ 
						
						{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ 
						 
						    {
            				name: 'f_tipo_ordine',
            				xtype: 'combo',
                        	anchor: '-15',
					        margin: "0 43 5 10",	
					    	labelWidth : 150,  
            				fieldLabel: 'Tipi ordine',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            			    flex: 1,  
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				store: {
            					autoLoad: true,
            					editable: false,
            					autoDestroy: true,
            				    fields: [{name:'id'}, {name:'text'}],
            				    data: [
                                          <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'VO', null, null, 0, "", 'Y'), ""); ?>
            					    ]
            				},listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
            			},{
							name: 'f_stato_ordine',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Stato ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "0 43 0 10",
						    labelWidth : 150,
						    flex : 1,
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?>	
								    ] 
							}						 
						}]},
						{
							xtype: 'button',	
							text: '<b>Applica filtri</b>',
							itemId: 'b_applica_filtri',
							margin: "0 10 10 100",
							width : 240,
							anchor: '-15',
							handler: function() {
		            	
								var l_form = this.up('form').getForm(),
	            					l_grid = this.up('window').down('grid'),
	            					cliente = l_form.findField('f_cli').getValue(),
	            					nr_ord = l_form.findField('f_nr_or').getValue();
	            				
	            				/*if(!cliente && !nr_ord){	            				
	            					acs_show_msg_error('Inserire cliente o nr ordine');
									return false;	            				
	            				}*/

	            				l_grid.store.proxy.extraParams.form_values = l_form.getValues();
	            				l_grid.store.load();
	            				l_grid.getView().refresh();	   		                     
							 }
							  
							}
						
						]
					 }
			]
		}		 
					 
					 
	 , {
			xtype: 'grid',
			name: 'f_ordini',
			margin: "0 10 10 10",
			flex: 1,	
			
			tbar: new Ext.Toolbar({
		           items:[
		            '->',
		            {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       	   ]            
		        }), 
			
			features: [
	
				{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     	}
		      ]
					}],	
			loadMask: true,	
			
			store: {
						xtype: 'store',
						autoLoad: false,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_ordini', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>,
										 form_values: ''
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['cliente', 'd_cliente', 'cod_dest', 'desc_dest', 'cod_pven', 'desc_pven', 
		        			         'art_da_prog', 'fl_bloc', 'fl_art_manc', 'raggr', 'ordine', 'anno', 'numero', 
		        			         'riferimento', 'tipologia', 'stato', 'data_eva', 'data_reg', 'k_ordine', 'riga', 
		        			         'RDNREC', 'RDDART', 'RDART', 'RDQTA', 'anno_l1', 'numero_l1', 'sconti', 'data_val',
		        			         'TDPROD', 'TDNAZI']							
									
			}, //store
				

			columns: [	
			      {
	                header   : 'Anno',
	                titleAlign: 'center', 
	                dataIndex: 'anno',
	                width: 40,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
				{
	                header   : 'Numero',
	                dataIndex: 'numero',
	                width: 60,
	                filter: {type: 'string'}, 
	                filterable: true
	            }, 
	            {text: '&nbsp;<br><img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',	  
	    			    renderer: function(value, metaData, record){
	    			    
	    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=15>';
	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=15>';			    	
	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=15>';
	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=15>';    			    				    	
	    			    	}},
	    			    			
    	         {text: '&nbsp;<br><img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini MTO',  
    	    		dataIndex: 'fl_art_manc',
					renderer: function(value, p, record){
    			    	if (record.get('fl_art_manc')==20) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_red.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=15>';			    	
    			    	}},
    			    	
	    	     {text: '&nbsp;<br><img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>',width: 30, tdCls: 'tdAction', tooltip: 'Articoli mancanti MTS',
	        			dataIndex: 'art_da_prog',
						renderer: function(value, p, record){
	    			    	if (record.get('art_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=15>';
			    			if (record.get('art_da_prog')==2) return '<img src=<?php echo img_path("icone/48x48/warning_red.png") ?> width=15>';			    	
			    			if (record.get('art_da_prog')==3) return '<img src=<?php echo img_path("icone/48x48/warning_blue.png") ?> width=15>';	    			    	
				}},
	            {
	                header   : 'Tipo',
	                dataIndex: 'tipologia',
	                width: 40,
	                filter: {type: 'string'}, 
	                filterable: true,
	                <?php if($m_params->doc_gest_search != 'CONTRACT_abbina'){?>
	                 tdCls: 'tipoOrd',
	                <?php }?>
	                renderer: function (value, metaData, record, row, col, store, gridView){						
				      //metaData.tdCls += ' ' + record.get('raggr');										
			          return value;			    
					}
	            },
	            {
	                header   : 'Data',
	                dataIndex: 'data_reg',
	                renderer: date_from_AS,
	                width: 60,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Evasione',
	                dataIndex: 'data_eva',
	                renderer: date_from_AS,
	                width: 70
	            },
	            {
	                header   : 'St.',
	                dataIndex: 'stato',
	                width: 30,
	                filter: {type: 'string'}, 
	                filterable: true,
	                renderer: function (value, metaData, record, row, col, store, gridView){						
				      if(record.get('stato') == 'CC') metaData.tdCls += ' sfondo_rosso';										
			          return value;			    
					}
	            },
				{
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                flex: 1,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            
	            <?php if($m_params->show_det == 'Y'){?>
	            
	            
	            {
	                header   : 'Sconti',
	                dataIndex: 'sconti',
	                width: 80,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	             {
	                header   : 'Validit&agrave;',
	                dataIndex: 'data_val',
	                renderer: date_from_AS,
	                width: 70
	            },
	            {
	                header   : 'Cod.',
	                dataIndex: 'cod_dest',
	                width: 40
	            },
	            {
	                header   : 'Localit&agrave; destinazione',
	                dataIndex: 'desc_dest',
	                flex:1
	            },
	             {
	                header   : 'Cod.',
	                dataIndex: 'cod_pven',
	                width: 40
	            },
	            {
	                header   : 'Punto vendita',
	                dataIndex: 'desc_pven',
	                flex:1
	            },
	            <?php }?>
	            
	            <?php if($m_params->from_webmail == 'Y'){?>
	            {header: 'Des.', dataIndex: 'cod_dest', width: 40},
	            {header: 'Localit&agrave;', dataIndex: 'desc_dest', flex: 1},
    			{header: 'Pr.', dataIndex: 'TDPROD', width: 30},
    			{header: 'Naz.', dataIndex: 'TDNAZI', width: 35}, 
	            
	            <?php }?>
	            
	            <?php echo dx_mobile() ?>
	        
	         ]
	         
	    	, listeners: {
	    	
	    	  afterrender: function (comp) {
					//window_to_center(comp.up('window'));
					<?php if(isset($m_params->cod_cli) && strlen($m_params->cod_cli) > 0){
					    $d_cliente = trim($s->decod_cliente($m_params->cod_cli));
					    $text_win = "Elenco ordini aperti [{$m_params->cod_cli}] {$d_cliente}";
					}?>
					comp.up('window').setTitle(<?php echo j($text_win); ?>);	 				
	 			},
	    	
	    	
	    		itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var row = rec.data;
					     var m_grid = this;
                         var loc_win = this.up('window');

				<?php					     
				
				
				if (isset($m_params->doc_gest_search)){
                    $doc_gest_search = $m_params->doc_gest_search;
                    if (isset($cfg_mod_Spedizioni['doc_gest_search']) &&
                        isset($cfg_mod_Spedizioni['doc_gest_search'][$doc_gest_search])){
                            if ($cfg_mod_Spedizioni['doc_gest_search'][$doc_gest_search]['abilita_assegna_stato_CC'] == 'Y'){
                                ?>

					 	 voci_menu.push({
			         		text: 'Assegna stato CC',
			        		iconCls : 'icon-gear-16',          		
			        		handler: function () {
         			          Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta?' , function(btn, text){
                    	   		if (btn == 'yes'){
                 			          Ext.Ajax.request({
                				        url        : '../base/acs_panel_messaggio_ordine_gest_SV2.php?fn=exe_scrivi_messaggio',
                				        timeout: 2400000,
                				        method     : 'POST',
                	        			jsonData: {
                	        				form_values : {},
                	        				open_request: {
                	        					tddocu: rec.get('k_ordine'),
                	        					messaggio: 'ASS_STDOC_B',
                	        					messaggio_ar: {
                	        						RISTNW: 'CC' //nuovo stato
                	        					}
                	        				}
                						},							        
                				        success : function(result, request){
                					        var jsonData = Ext.decode(result.responseText);
                					        if (jsonData.success == true){
                					        	acs_show_msg_info('Mesaggio scritto correttamente');
                					        	grid.store.load();
                					        }
                				        },
                				        failure    : function(result, request){
                				            Ext.Msg.alert('Message', 'No data to be loaded');
                				        }
                				    }); 		     
		           
				    			}
        	   		  			});			        		
			        		} //handler
			        	});                                
							<?php }
                    }
                } ?>
                
                <?php if($m_params->show_det == 'Y'){ ?>
                    if(rec.get('stato') != 'CC'){
                    	voci_menu.push(
    				          { text: 'Modifica testata'
    				          , iconCls : 'icon-leaf-16'
    				          , handler: function()  {
    				          		acs_show_win_std('Modifica testata ' + rec.get('ordine'), 
    				          		'../base/acs_panel_testata_ordine_gest_SV2.php?fn=open_form', 
    				          		{tddocu: rec.get('k_ordine')});   	     	          		
    	        		          }
    	    		          }
    	    		        );
    	    		}
                 <?php }?>       
                 
                 <?php if($m_params->from_webmail == 'Y'){?>
                 
                 	voci_menu.push(
			          { text: 'Conferma abbinamento'
			          , iconCls : 'icon-leaf-16'
			          , handler: function()  {
			          	    loc_win.fireEvent('afterAssegna', loc_win, rec.get('k_ordine')); 	     	          		
        		          }
    		          }
    		        );
                 
                 <?php }?>   

			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				  }).showAt(event.xy);
					     
				} //itemcontextmenu
				
				<?php if($m_params->show_det == 'Y'){ ?>
				 , celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           		
		          	    var grid = this, 
					  		col_name = iView.getGridColumns()[iColIdx].dataIndex,
					  		rec = iView.getRecord(iRowEl);
					  		
					  		if(col_name == 'cod_dest'){
					  		
    					  		if(col_name == 'cod_dest') type = 'D';
    					  		if(col_name == 'cod_pven') type = 'P';
					  		
    					  		var my_listeners_search_loc = {
    										afterSelected: function(from_win, record_selected){
    										
    										Ext.Ajax.request({
                						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_mod_destinazione',
                						        timeout: 2400000,
                						        method     : 'POST',
                			        			jsonData: {
                			        				k_ordine : rec.get('k_ordine'),
                			        				codice : record_selected.cod,
                			        				type : type
                								},							        
                						        success : function(result, request){
                							        var jsonData = Ext.decode(result.responseText);
                							        grid.getStore().load();
                                                	from_win.close();
                							    },
                						        failure    : function(result, request){
                						            Ext.Msg.alert('Message', 'No data to be loaded');
                						        }
                						    });
    										
    										    
    										}										    
    									};
    							  
    							acs_show_win_std(null, 
    	 				        '../base/acs_seleziona_destinazione.php?fn=open_panel', 
    							{cliente : rec.get('cliente'), type : type
    							 }, 800, 300, my_listeners_search_loc, 'icon-sub_blue_add-16');
					  		
					  		}
		           
		           }
		          }
				
				<?php }?>
	    	}     
			
		} <!-- end grid -->
						 
				]	
				
        }
     ]        
 }



<?php 
	exit;
	}

	if ($_REQUEST['fn'] == 'exe_mod_destinazione'){
	    $ret = array();
	    $ar_upd = array();
	    $oe = $s->k_ordine_td_decode_xx($m_params->k_ordine);
	    
	    $ar_upd = array();
	    $ar_upd['TDDTGE'] = oggi_AS_date();
	    $ar_upd['TDUSGE'] = $auth->get_user();
	   // if($m_params->type == 'D')
	       $ar_upd['TDDEST'] = trim($m_params->cod_dest);
       
	    //eseguo update
	    
	    $sql = "UPDATE {$cfg_mod_Spedizioni['file_testate_doc_gest']}
	    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
        WHERE TDDT = ? AND TDTIDO = ? AND TDINUM = ? AND TDAADO = ? AND TDNRDO = ?";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, array_merge($ar_upd, $oe));
	    echo db2_stmt_errormsg($stmt);
	    
	    //RITORNO
	    $ord = $s->get_ordine_gest_by_k_docu($m_params->k_ordine);
	    $ret['success'] = true;
	    $ret['ord']     = $ord;
	    echo acs_je($ret);
	    exit;
	    
	}
