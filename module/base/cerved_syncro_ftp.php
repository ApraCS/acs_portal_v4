<?php

require __DIR__ . '/../../config.inc.php';       

if (!isset($cerved_syncro_payline_ftp))
    die('ERROR: Configurazione cerved_syncro_payline_ftp mancante');
    
if (!isset($cfg_mod_Gest['file_cerved_payline']))
    die('ERROR: Configurazione file cerved payline mancante');

$file_cerved_payline = $cfg_mod_Gest['file_cerved_payline'];
    
//Connessione ftp
$conn_ftp = ftp_connect($cerved_syncro_payline_ftp['host']);   
$login_result = ftp_login($conn_ftp, $cerved_syncro_payline_ftp['user'], $cerved_syncro_payline_ftp['pwd']) or die("Ftp login error");
    
$files = ftp_nlist($conn_ftp , $cerved_syncro_payline_ftp['dir']);

foreach($files as $remote_file){
  $path = pathinfo($remote_file);

  if ($path['extension'] == 'csv') {
    echo "\nElaboro il file {$path['basename']}";
    $temp_file = tempnam('/tmp', 'cerved-');
    if (ftp_get($conn_ftp, $temp_file, $remote_file, FTP_ASCII)) {
      echo("\nSuccessfully written $remote_file to $temp_file");
    }
    
    //gestione file
    cerved_elabora_file($temp_file, $path);
    
    //sposto il file nella cartella remote ftp
    ftp_rename($conn_ftp, $remote_file, $cerved_syncro_payline_ftp['dir_done'] . '/' . $path['basename']);
    
    unlink($temp_file); //rimuovo il file temporaneo    
  }  
}//foreach files

//print_r($files);
//echo "\nFine\n";



//*****************************************************************************
function cerved_elabora_file($local_file, $path){
//*****************************************************************************
    global $conn, $id_ditta_default, $file_cerved_payline;
    if (($handle = fopen($local_file, "r")) !== FALSE) {
        
        $codice_richiesta = date('YmdHis');
        $crow = 0;
        while(!feof($handle)) { //scorro le righe
            $line = fgets($handle);
            $crow++;
            
            if ($crow==1) {  //intestazioni colonna
                $rowH  = explode("|", trim($line));
            } else {
                
                $row  = explode("|", trim($line));
                
                //Non gestisco se non ho il codice cliente passato da cerved
                if(strlen(trim($row[0])) == 0)
                    continue;
                    
                $cod_cliente     = trim($row[0]); //codice cliente (AS) dalla prima colonna del file cerved
                    
                for($i=0; $i<count($rowH); $i++){
                    
                    if(strlen($rowH[$i]) == 0)
                        continue;
                    
                    $name_attributo      = trim($rowH[$i]);
                    $val_attributo       = substr(trim($row[$i]), 0, 256);
                        
                    $row_cp = controllo_campo($cod_cliente,  $name_attributo);
                    
                    if($row_cp == false || trim($row_cp['CPVALP']) != $val_attributo){                        
                        
                        if($row_cp){ //se devo storicizzare il vecchio record
                            
                            $ar_upd = array();
                            $ar_upd['CPRIGA'] 	= 'H';
                            
                            $sql = "UPDATE {$file_cerved_payline} cp
                                    SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
                                    WHERE RRN(cp) = '{$row_cp['RRN']}' ";
                            
                            $stmt = db2_prepare($conn, $sql);
                            echo db2_stmt_errormsg();
                            $result = db2_execute($stmt, $ar_upd);                            
                        }
                        
                        //Inserimento
                        $ar_ins = array();
                        $ar_ins['CPUSCR'] = 'SD_AUTO';
                        $ar_ins['CPDTCR'] = oggi_AS_date();
                        $ar_ins['CPHHCR'] = oggi_AS_time();
                        $ar_ins['CPBAND'] = 'SUD0'; //$row[1];
                        $ar_ins['CPAZIE'] = $id_ditta_default;
                        $ar_ins['CPAREA'] = 'AZIE';
                        $ar_ins['CPCOPR'] = $cod_cliente;
                        $ar_ins['CPATTR'] = $name_attributo;
                        $ar_ins['CPVALP'] = $val_attributo;
                        $ar_ins['CPDVAI'] = oggi_AS_date();
                        $ar_ins['CPOVAI'] = oggi_AS_time();
                        $ar_ins['CPDVAF'] = oggi_AS_date();
                        $ar_ins['CPOVAF'] = oggi_AS_time();
                        $ar_ins['CPCONP'] = 'CERVED';
                        $ar_ins['CPSEQU'] = '';
                        $ar_ins['CPRIGA'] = 'A';
                        $ar_ins['CPDSTO'] = 0;//oggi_AS_date();
                        $ar_ins['CPHSTO'] = 0;//oggi_AS_time();
                        $ar_ins['CPTALI'] = 'E';
                        $ar_ins['CPNOTE'] = $path['basename'];
                        
                        $sql = "INSERT INTO {$file_cerved_payline}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
                        
                        $stmt = db2_prepare($conn, $sql);
                        echo db2_stmt_errormsg();
                        $result = db2_execute($stmt, $ar_ins);
                        echo db2_stmt_errormsg($stmt);

                    } else {
                        //echo "\n Valore non modificato";
                    }
                } //per ogni campo
            } //per ogni riga cliente
        } //per ogni riga
        
        fclose($handle);        
    }
} //elabora file


function controllo_campo($cod_cli, $attributo){
    
    global $conn, $id_ditta_default, $file_cerved_payline;
    
    $sql = "SELECT RRN(CP) AS RRN, CPVALP
            FROM {$file_cerved_payline} CP
            WHERE CPAZIE = '{$id_ditta_default}' 
             AND CPCOPR = '{$cod_cli}' 
             AND CPATTR = '{$attributo}' 
             AND CPRIGA = 'A'
            ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row_cp = db2_fetch_assoc($stmt);
    
    if(!$row_cp)
        return false;
     else {
        $row_cp['CPVALP'] = trim($row_cp['CPVALP']);
        return $row_cp;        
     }
}
