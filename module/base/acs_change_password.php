<?
require_once "../../config.inc.php";

$main_module = new DeskPVen();
$s = new Spedizioni(array('no_verify' => 'Y'));

if ($_REQUEST['fn'] == 'exe_verify_password'){

	$m_params = acs_m_params_json_decode();
	$ret = array();
	$user = $auth->get_user();
	$psw = $m_params->form_values->f_pass;
	$new_psw = $m_params->form_values->f_new_pass;

	$sql = "SELECT COUNT(*) AS U_ROW FROM {$cfg_mod_Admin['file_utenti']} UT
	WHERE UTCUTE = '{$user}' AND UTPSW = '{$psw}'" ;
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$r = db2_fetch_assoc($stmt);
	
	if ($r['U_ROW'] == 0){
		$ret['success'] = false;
		$ret['message'] = 'Password inserita sbagliata';
	}else{
		
		$ar_ins = array();
		
		$ar_ins['UTPSW'] 	= $new_psw;
			
		$ar_ins['UTUSGE'] 	= $auth->get_user();
		$ar_ins['UTDTGE']   = oggi_AS_date();
		$ar_ins['UTORGE'] 	= oggi_AS_time();
		
		$sql = "UPDATE {$cfg_mod_Admin['file_utenti']} UT
		SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
		WHERE UTCUTE = '{$user}' AND UTPSW = '{$psw}'";
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		
		
		$ret['success'] = true;
	}
	
	echo acs_je($ret);
	exit;
}

if($_REQUEST['fn'] == 'change_password'){


	?>
	
	{"success" : true, "items":
		
			{
		        xtype: 'form',
		        layout: 'form',
		        collapsible: false,
		        frame: false,
		        bodyPadding: '10 10 5 10',
		       // width: 300,
		        items: [{
						xtype: 'fieldset',
		                title: 'Inserisci dati',
		                defaultType: 'textfield',
		                layout: 'anchor',
		               /* defaults: {
		                    anchor: '100%'
		                },*/
		             	items: [{
							   xtype: 'displayfield'
							   , fieldLabel: 'Utente'
							   , name: 'f_name'
							   , allowBlank: true
							   , anchor: '-5'
							   , margin: "0 5 5 5"
							   , value : '<?php echo $auth->out_name(); ?>'
				
							}, {
							   xtype: 'displayfield'
							   , fieldLabel: 'Email'
							   , name: 'f_email'
							   , allowBlank: true
							   , anchor: '-5'
							   , margin: "5 5 10 5"
							   , value : '<?php echo $auth->get_email(); ?>'
				
							},{
							   xtype: 'textfield'
							   , inputType: 'password'
							   , fieldLabel: 'Password'
							   , name: 'f_pass'
							   , allowBlank: true
							   , anchor: '-5'
							   , margin: "5 5 10 5"
				
							}]
		            },{
						xtype: 'fieldset',
		                title: 'Imposta password',
		                defaultType: 'textfield',
		                layout: 'anchor',
		               /* defaults: {
		                    anchor: '100%'
		                },*/
		             	items: [{
							   xtype: 'textfield'
							   , fieldLabel: 'Nuova password'
							   , inputType: 'password'
							   , name: 'f_new_pass'
							   , id: 'f_new_pass'
							   , allowBlank: true
							   , anchor: '-5'
							   , margin: "5 5 10 5"
							   , labelWidth: 120
				
							},{
							   xtype: 'textfield'
							   , fieldLabel: 'Conferma password'
							   , labelWidth: 120
							   , inputType: 'password'
							   , name: 'f_confirm_pass'
							   , vtype : 'password'
        					   , initialPassField : 'f_new_pass'
							   , allowBlank: true
							   , anchor: '-5'
							   , margin: "5 5 10 5"
				
							}]
		            }],
	
		        buttons: [{
			            text: 'Conferma',
			            iconCls: 'icon-print-32',
			            scale: 'large',	                     
			            handler: function() {
			                form = this.up('form').getForm();
			                loc_win = this.up('window');
			               if (form.isValid()){	
			               
									Ext.Ajax.request({
		 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_verify_password',
		 						        method     : 'POST',
		 			        			jsonData: {
		 			        			  
		 			        			   form_values: form.getValues()
		 								},							        
		 						        success : function(result, request){
		 						        	jsonData = Ext.decode(result.responseText);
		 						        	if(jsonData.success == false){
			 						        	acs_show_msg_error('Password iniziale sbagliata');
		 						        	}else{
		 						        		loc_win.close();
		 						        	}
		 			            	
		 			            		},
		 						        failure    : function(result, request){
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });	
							
			                }	                
			            }
			        }
		        ]
		    }
		
			
		}
	
	
	
	<?php 
		exit;
		}
