<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data'){
    
    if(isset($m_params->list) && 
        (is_string($m_params->list) && strlen($m_params->list) > 0) ||
        (is_array($m_params->list) && count($m_params->list) > 0)
      )
        $filtra_codici = $m_params->list;
    else 
        $filtra_codici = null;
    
    if(isset($m_params->tacor2))
        $tacor2 = $m_params->tacor2;
    else
        $tacor2 = null;
  
    
    $values = find_TA_sys('BSTA', null, null, $tacor2, null, $filtra_codici, 0, '', 'Y', 'N', 'R');
    
    
    echo acs_je($values);
    exit;

}