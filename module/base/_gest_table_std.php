<?php

require_once("../base/_rilav_g_history_include.php");

$s = new Spedizioni(array('no_verify' => 'Y')); 
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// DELETE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_delete'){
  
    $sql = "DELETE FROM {$m_table_config['tab_name']} TA WHERE RRN(TA) = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->form_values->rrn));
    
    $ret = array();
    $ret['success'] = true;
    echo json_encode($ret);
    exit;
}

// ******************************************************************************************
// INSERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_inserisci'){
    
    $ar_ins = (array)$m_params->form_values;

    unset($ar_ins['rrn']);
    unset($ar_ins['rec_index']);
   
    
    $ar_ins = m_gest_tataid_prepare_values($m_table_config, $ar_ins);
  
    #solo php7: $f_ditta = $m_table_config['f_ditta'] ?? 'TADT';
    $f_ditta = isset($m_table_config['f_ditta']) ? $m_table_config['f_ditta'] : 'TADT';
    $ar_ins[$f_ditta] 	= $id_ditta_default;
    
    if (isset($m_table_config['field_IDPR']))
        $ar_ins[$m_table_config['field_IDPR']] 	= m_gest_tataid_get_next_num($m_table_config);
   
        
    if(isset($m_table_config['immissione'])){
        $immissione = $m_table_config['immissione'];
        $ar_ins[$immissione['data_gen']] = oggi_AS_date();
        $ar_ins[$immissione['user_gen']] = $auth->get_user();
        if(isset($immissione['data_mod']))
            $ar_ins[$immissione['data_mod']] = oggi_AS_date();
        if(isset($immissione['user_mod']))
            $ar_ins[$immissione['user_mod']] = $auth->get_user();
            
    }
    
    //valori preimpostati
    if(isset($m_table_config['fields_preset'])){
        foreach ($m_table_config['fields_preset'] as $kf=>$v){
            $ar_ins[$kf] = $v;
        }
    }
    
   
    $sql = "INSERT INTO {$m_table_config['tab_name']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);    
    
    $ret = array();
    $ret['success'] = true;
    echo json_encode($ret);
    exit;
}



// ******************************************************************************************
// UPDATE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica'){
	
    $ar_upd = (array)$m_params->form_values;
    
    unset($ar_upd['rrn']);
    unset($ar_upd['rec_index']);
    
    $ar_upd = m_gest_tataid_prepare_values($m_table_config, $ar_upd);
    
    if(isset($m_table_config['immissione'])){
        $immissione = $m_table_config['immissione'];
      
        if(isset($immissione['data_mod']))
            $ar_ins[$immissione['data_mod']] = oggi_AS_date();
        if(isset($immissione['user_mod']))
            $ar_ins[$immissione['user_mod']] = $auth->get_user();
                
    }
    
  
	$sql = "UPDATE {$m_table_config['tab_name']} TA
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE RRN(TA) = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
	           array($m_params->form_values->rrn)
			));
	echo db2_stmt_errormsg($stmt);
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// GET GRID DATA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_data'){
 
    $sql_where = "";
    $sql_ar = array('select' => array(), 'joins' => array(), 'group_by' => array());
    
    $sql_ar['select'][] = 'RRN(TA) AS RRN';
    $sql_ar['select'][] = 'TA.*';
    
    if(isset($m_table_config['f_ditta']))
      $ditta = $m_table_config['f_ditta'];
    else
      $ditta = "TADT";
    
    /*if(isset($m_table_config['fields_pre']['TATAID']))
        $sql_where .= " AND TATAID = '{$m_table_config['fields_pre']['TATAID']}'";
   */
     
    //filtri avanzati
    if(isset($m_table_config['fields_preset'])){
        foreach ($m_table_config['fields_preset'] as $kf=>$v){
            $sql_where .= sql_where_by_combo_value("TA.{$kf}", $v, null, 'I', true);
        }
    }
   
    //filtri avanzati    
    if(isset($m_table_config['flt_adv'])){        
        foreach ($m_table_config['flt_adv'] as $kf=>$v){
            $sql_where .= sql_where_by_combo_value($kf, $v);            
        }
    }
    
    //join per eventuali NT e AH
    foreach ($m_table_config['fields'] as $kf => $f){
        if ($f['type'] == 'AH'){            
            $_rilav_H_for_query = _rilav_H_for_query(
                $m_table_config,
                $m_table_config['tab_name'],
                $cfg_mod['file_tabelle'],
                'TA',
                $f['h_config']['causale'],
                array($f['h_config']['raggruppamento'])
                );
            $sql_ar['select'][] = $_rilav_H_for_query['select'];
            $sql_ar['joins'][] = $_rilav_H_for_query['joins'];
        }
        
        //TABELLA ABBINATA
        if ($f['type'] == 'TA'){
            $sql_ar['select'][] = $f['select'];
            $sql_ar['joins'][] = $f['join'];
        }
    }
        
    if(isset($m_params->open_request->sezione))
        $sql_where.= sql_where_by_combo_value('TARIF1', $m_params->open_request->sezione);
    
    $sql = "SELECT " . implode(",", $sql_ar['select']) . "
            FROM {$m_table_config['tab_name']} TA
                " . implode(" ", $sql_ar['joins']) . "
            WHERE {$ditta} ='{$id_ditta_default}' {$sql_where}
            {$order}";
        

    $stmt = db2_prepare($conn, $sql); 	
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $data = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $row['TADESC'] = acs_u8e($row['TADESC']);
        $row['rrn'] = acs_u8e(trim($row['RRN']));
        $row['TAPESO'] = (int)$row['TAPESO'];   //ToDo: parametrizzare
        if (isset($_row_post_fn)){
            $row = $_row_post_fn($row, $m_table_config);
        }
        if(trim($row['TATAID']) == 'MACMA' || trim($row['TATAID']) == 'CATIN')
            $row["TACOGE_DE"] = "[".trim($row['TACOGE'])."] ".$s->decod_cliente($row['TACOGE'], 'F');
        
        if(trim($row['TATAID']) == 'DTIMP'){
            if(trim($row['TALOCA']) != '')
             $row["TIPOL_DA"] = "{$row['TALOCA']} - {$row['TACAP']}";
        }
        
        foreach($m_table_config['fields'] as $k => $v){
            if($v['desc'] == 'Y'){
                if(trim($row["{$k}"]) != ''){
                    
                    if(isset($v['module'])){
                        $module = $v['module'];
                        $row_ta = $module->get_TA_std($v['TAID'], trim($row["{$k}"]));
                    }else 
                        $row_ta = $s->get_TA_std($v['TAID'], trim($row["{$k}"]));
                   
                    if($v['no_cod'] != 'Y')
                        $row["{$k}_d"] = "[".trim($row["{$k}"])."] ".trim($row_ta['TADESC']);
                    else 
                        $row["{$k}_d"] = trim($row_ta['TADESC']);
                }
            }
            
           
        }
        
        $data[] = array_map('rtrim', $row);
    }
    
    echo acs_je($data);
    exit;
}	



// ******************************************************************************************
// GRID VIEW
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
    $m_win = array();
    
    if (isset($m_table_config['iconCls']))
        $m_win['iconCls'] = $m_table_config['iconCls'];
    else 
        $m_win['iconCls'] = 'icon-gear-16';
    
    if(isset($m_table_config['fields_preset']['TATAID']))
        $title = "Tab.Id: {$m_table_config['fields_preset']['TATAID']} - ";
    $title .= "{$m_table_config['descrizione']}";
   
   if (isset($m_params->cod))
        $title .= " [{$m_params->cod}] $m_params->desc";
   
    $m_win['title'] = $title;
    
    if (isset($m_table_config['maximize']))
        $m_win['maximize'] = $m_table_config['maximize'];
    
?>
{"success": true, 
 
 m_win: <?php echo_je($m_win) ?>,
 
 items: [
   {
			xtype: 'panel',
			
			<?php if (isset($m_table_config['t_panel'])){ ?>
			title : <?php echo j($m_table_config['t_panel']); ?>,
			<?php echo make_tab_closable(); ?>,
		    <?php }?>
			autoScroll : true,
        	layout: {
				type: 'hbox',
				align: 'stretch',
				pack : 'start',
			},
				items: [
				{
            xtype: 'form', layout: 'fit',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            flex:0.7,
            items: [
				
		   {
    		xtype: 'grid',
        	multiSelect: true,
        	autoScroll: true,
        	features: [{
				ftype: 'filters',
				encode: false, 
				local: true,   
		   		 filters: [
		       {
		 		type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
			}],
        	store: {
        	xtype: 'store',
    		autoLoad:true,
    		proxy: {
    			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data',
    			type: 'ajax',
    			actionMethods: {
    				read: 'POST'
    			},
    				extraParams: {
                        open_request : <?php echo acs_je($m_params); ?>		
				},
        				
    			doRequest: personalizza_extraParams_to_jsonData, 
		
				reader: {
		            type: 'json',
					method: 'POST',						            
		            root: 'root'						            
		        }
			},
        	fields: <?php echo m_gest_tataid_get_fields($m_table_config) ?>
    	}, //store
    		
	        columns: [
	        	
	        	<?php m_gest_tataid_get_columns($m_table_config) ?>	
	            
	         ],  
			listeners: {
			  	 selectionchange: function(selModel, selected) { 
				   if(selected.length > 0){
					   var form_dx = this.up('form').up('panel').down('#dx_form');
					   form_dx.getForm().reset();
					   rec_index = selected[0].index;
					   form_dx.getForm().findField('rec_index').setValue(rec_index);
					   acs_form_setValues(form_dx, selected[0].data);
					 //  form_dx.getForm().setValues(selected[0].data);
					}					
				 },
				 
				 itemcontextmenu : function(grid, rec, node, index, event) {
				 
				  	event.stopEvent();
				 	var voci_menu = [];
		     		var row = rec.data;
		     		var m_grid = this;
		     		var id_selected = grid.getSelectionModel().getSelection();
		     		var list_selected_id = [];
				    for (var i=0; i<id_selected.length; i++) 
						list_selected_id.push(id_selected[i].data.TAKEY1);
		     		
		     	        <?php if (isset($m_table_config['tasto_dx'])){ 
			               
		     	           foreach($m_table_config['tasto_dx'] as $b){
		     	               echo "voci_menu.push(";
    					        echo_je($b);  
    					       echo ");";
            			   }
			      } ?>
		     		
		     		
		     		var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);
				 
				 },
				 
				 celldblclick: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				 	  
				 	  var rec = iView.getRecord(iRowEl);
					  var grid = this;
					  var col = iView.getGridColumns()[iColIdx];
					  var col_name = iView.getGridColumns()[iColIdx].dataIndex;	
					  
					    
					    
					    	
			  			if (!Ext.isEmpty(col.ta_config)){
			  			    var dbclick = true;
			  				if (typeof(col.ta_config.only_opz) !== 'undefined' && col.ta_config.only_opz == 'Y'){
			  				  
			  				  if(rec.get('TATAID') == 'ARPRO'){
			  				    var tataid = rec.get('TAASPE').trim();
			  				    if(tataid.substr(0, 2) != 'P#')
			  				        dbclick = false;
			  				  }
			  				  
			  				  if(rec.get('TATAID') == 'DTIMP'){
			  				    var tataid = rec.get('TASITI').trim();
			  				  	if(tataid.substr(0, 2) != 'A#')
			  				   		dbclick = false;
			  				  }
			  				}
			  				
			  			    if(dbclick == true)
				  			   acs_show_panel_std(col.ta_config.file_acs, 'panel_gestione_tab_abb', {TAKEY1 : rec.get('TAKEY1'), TAKEY2 : rec.get('TAKEY1'), TAASPE : tataid, params : col.ta_config.params});
    
					  	}
					  	
					  
					  		//blocco note
					  		if (!Ext.isEmpty(col.nt_config)){
        					  	var my_listeners = {
        	    		  				afterSave: function(from_win, values){
        	    		  			    rec.set(col_name, values.icon);
        	    		  			    from_win.close();
        		     					}	
        	     					};
        	     					
        	     					
        					  	acs_show_win_std(null, '../base/acs_gestione_nota_nt.php?fn=open_panel', {
        					  		ar_nt_config: col.nt_config,
        					  		desc : rec.get(<?php echo j($m_table_config['f_desc']) ?>),
        					  		chiave : rec.get(<?php echo j($m_table_config['field_NOTE']) ?>)
        					  		}, null, null, my_listeners);
    					  		return;
    					  	}		
					  
				 			//Gestione History RILAV
 						    if (!Ext.isEmpty(col.exeGestRilavH)){
 						    	var my_listeners = {
 						    		afterInsert: function(from_win, jsonData){
 						    			rec.set(jsonData.item);
						        	 	rec.commit();
 						    			from_win.destroy();
 						    		}
 						    	}
     							acs_show_win_std(null, '../base/acs_panel_rilav_H.php?fn=open_tab', {
    			        		   		file_H: <?php echo j(_rilav_H_file())?>,
    			        		   		file: <?php echo j(_rilav_H_db_file_Orig($m_table_config['tab_name'])) ?>,
    			        		   		file_TA: <?php echo j($cfg_mod['file_tabelle'])?>,
    			        		   		rec_id: rec.get(<?php echo j($m_table_config['field_IDPR']) ?>),
                	            		causale: col.exeGestRilavH_causale,
                	            		group: col.exeGestRilavH
                	               }, null, null, my_listeners);
                	        }
               	}
			
	         }

			 ,viewConfig: {
	         getRowClass: function(record, index) {
	         //return ret;																
	         }   
	    },
			    
	        
        } //grid
        
	]},
	
	<?php if(isset($m_table_config['form_title']))
	        $f_title = $m_table_config['form_title'];
	      else 
	        $f_title = "Dettagli";
	?>
	{
			xtype: 'form',
			itemId: 'dx_form',
			autoScroll : true,
			title: <?php echo j($f_title); ?>,
			bodyStyle: 'padding: 10px',
			bodyPadding: '5 5 0',
			flex:0.3,
			frame: true,
			items: [
			   {xtype: 'textfield', name: 'rrn', hidden : true},
			   {xtype : 'textfield',name : 'rec_index',hidden : true}, 
		       <?php m_gest_tataid_get_form_insert($m_table_config) ?>
			
			
			],
			
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [
                
                {  xtype: 'button',
                    tooltip: 'Opzioni disponibili',
                    scale: 'small',	                  
        			iconCls : 'icon-button_grey_play-16',
        			style:'border: 1px solid gray;',    
                    handler: function(a , event) {
                      
                         var form = this.up('form');
		                 var form_values = form.getValues();
                    	 var grid = this.up('form').up('panel').down('grid');
                    	 var index = grid.getStore().findExact('RRN', form_values.RRN); 			       		
 			       		 var record = grid.getStore().getAt(index);
	                	 grid.fireEvent('itemcontextmenu', grid, record, null, index, event);
        		
        		       }
        
        	     },
                
               <?php if (isset($m_table_config['buttons'])){ 
			     /*{
                	xtype:'splitbutton',
                	text: '',
                	tooltip: 'Configura filtri',
                	scale: 'small',
                	iconCls: 'icon-filter-16',
                	style:'border: 1px solid gray;',  
                	handler: function(){
                	 this.maybeShowMenu();
                	},
                	menu: {
                		xtype: 'menu',
                		items: [
                		   
					   ]}
                		
                	},*/
                	
                	    foreach($m_table_config['buttons'] as $b){
    					       echo_je($b);  
    					       echo ",";
            			   }
			      } ?>
			     
						
						
			    '->'   
			   ,{
                     xtype: 'button',
                    text: 'Elimina',
		            scale: 'small',	                     
					iconCls: 'icon-sub_red_delete-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
	       			  
	       			 <?php if($m_table_config['conferma_elimina'] == 'Y'){?>
	       			 
	       			 		Ext.Msg.confirm('Richiesta conferma', 'Confermi richiesta cancellazione?', function(btn, text){
            	   				if (btn == 'yes'){
	       			 
	       			 <?php }?>
 			        
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_delete',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					        if (jsonData.success == true){
					        	var gridrecord = grid.getSelectionModel().getSelection();
					        	grid.store.remove(gridrecord);	
					            form.getForm().reset();					        
					        }
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 		 
				    
				     <?php if($m_table_config['conferma_elimina'] == 'Y'){?>
	       			 		}
                	   				
    	   				});
	       			 <?php }?>    
		           
			
			            }

			     }, 
			      
                 {
                     xtype: 'button',
                    text: 'Genera',
		            scale: 'small',	                     
					iconCls: 'icon-button_blue_play-16',
		          	handler: function() {
		          	
		          	  var form = this.up('form');
	       			  var form_values = form.getValues();
	       			  var grid = this.up('panel').up('panel').down('grid'); 
 			        if (form.getForm().isValid()){	
 			          Ext.Ajax.request({
				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci',
				        timeout: 2400000,
				        method     : 'POST',
	        			jsonData: {
	        				form_values : form_values,
	        				open_request : <?php echo acs_je($m_params); ?>
						},							        
				        success : function(result, request){
					        var jsonData = Ext.decode(result.responseText);
					         grid.store.load();
					         form.getForm().reset();
					        				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				    }); 
		           }
			
			            }

			     },
			      {
                     xtype: 'button',
                    text: 'Aggiorna',
		            scale: 'small',	                     
					iconCls: 'icon-save-16',
		           	handler: function() {
		                    var form = this.up('form');
 			       			var form_values = form.getValues();
 			       			var grid = this.up('panel').up('panel').down('grid'); 
 			       			if (form.getForm().isValid()){
 			       			Ext.Ajax.request({
						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica',
						        timeout: 2400000,
						        method     : 'POST',
			        			jsonData: {
			        				form_values : form_values
								},							        
						        success : function(result, request){
							        var jsonData = Ext.decode(result.responseText);
							        grid.store.load();
							        form.getForm().reset();
							    },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						    });
						  }
			            }

			     }
			     ]
		   }]
			
			}	
				
				
		]} 

]
}
<?php 
exit;
} 


//function utility

//converto/applico valori default su alcuni tipi di dati
function m_gest_tataid_prepare_values($m_table_config, $ar_upd){
    foreach ($m_table_config['fields'] as $kf=>$f){
        if ($f['type'] == 'date' && isset($ar_upd[$kf]) && trim($ar_upd[$kf]) == "")
            $ar_upd[$kf] = 0;
        
        if ($f['type'] == 'numeric' && isset($ar_upd[$kf]) && trim($ar_upd[$kf]) == "") //numeric
            $ar_upd[$kf] = 0;
        
        if (!isset($f['type']) && isset($ar_upd[$kf])) //text
            $ar_upd[$kf] = acs_toDb($ar_upd[$kf]);
        
    }
    return $ar_upd;
}

function m_gest_tataid_get_fields($m_table_config){
	$r = array('TADT', 'TATAID', 'rrn');
	foreach ($m_table_config['fields'] as $kf=>$f){
		$r[] = $kf;
		
		if ($f['type'] == 'AH'){
		    $ar = _rilav_H_fields(array($f['h_config']['raggruppamento']));
		    foreach ($ar as $kk)
		        $r[] = $kk;
		}
		
	}
	return acs_je($r);
}


function m_gest_tataid_get_columns($m_table_config){
    
    if(isset($m_table_config['fields_grid'])){
        foreach ($m_table_config['fields_grid'] as $kf) {
            $ar_fields[$kf] = $m_table_config['fields'][$kf];
        }
    }else{
        foreach ($m_table_config['fields'] as $kf => $vf)
            $ar_fields[$kf] = $vf;
            
    }
    
    
    
    $ar = array();
    foreach ($ar_fields as $kf=>$f) {
        
        
        if(isset($f['short']))
            $header = $f['short'];
        else
            $header = $f['label'];
        
        
        switch ($f['type']) {
            case 'AH':
                ?>
				{
					header: <?php echo j($f['label'])?>,
					tooltip: <?php echo acs_je($f['tooltip'])?>,
			 		dataIndex: <?php echo j("H_{$f['h_config']['raggruppamento']}_COD")?>, 
			 		exeGestRilavH: <?php echo j($f['h_config']['raggruppamento']) ?>,
			 		exeGestRilavH_causale: <?php echo j($f['h_config']['causale']) ?>,
			 		width: 35, align: 'right',
			 		filter: {type: 'string'}, filterable: true,
			 		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          	<?php echo _rilav_H_icon_style($f['h_config']['raggruppamento']) ?>
					}
			 	},		
			<?php
            break;
            
            case 'checkbox':
                ?>
				{
					header: '<img src=<?php echo img_path("icone/48x48/{$f['iconCls']}.png") ?> width=24>',
					tooltip: <?php echo acs_je($f['label'])?>,
			 		dataIndex: <?php echo j($kf)?>, 
			 		width: 35,
			 		filter: {type: 'string'}, filterable: true,
			 		renderer: function (value, metaData, rec, row, col, store, gridView){
    		          	if (value == 'Y') return '<img src=<?php echo img_path("icone/48x48/{$f['iconCls']}.png") ?> width=16>';;
    		          	return null;
    		        }
			 	},		
			<?php
            break;
            
            case 'NT': ?>
                {
                    header: '<img src=<?php echo img_path("icone/48x48/{$f['iconCls']}.png") ?> width=16>',
                    tooltip: <?php echo j($f['tooltip']) ?>,
                    dataIndex: <?php echo j($kf)?>,
                    nt_config: <?php echo acs_je($f)?>,
			 		width: 35, align: 'right',
			 		filter: {type: 'string'}, filterable: true,
			 		renderer: function(value, metaData, record, row, col, store, gridView){
	    			  if(!Ext.isEmpty(value) && value > 0) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
	    			  else return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
	    		    }
			 	}, <?php 
            break;
            
          case 'TA': ?>
                {
                    header: '<img src=<?php echo img_path("icone/48x48/{$f['iconCls']}.png") ?> width=16>',
                    tooltip: <?php echo j($f['tooltip']) ?>,
                    dataIndex: <?php echo j($kf)?>,
                    ta_config: <?php echo acs_je($f['ta_config'])?>,
                    width: 35,
			 		filter: {type: 'string'}, filterable: true,
			 		renderer: function(value, metaData, record, row, col, store, gridView){
			 		   
			 		    
			 		    if((!Ext.isEmpty(record.get('TAASPE')) && record.get('TAASPE').substr(0,2) == 'P#')
			 		       || !Ext.isEmpty(record.get('TASITI')) && record.get('TASITI').substr(0,2) == 'A#'){
			 		       if(value == 0)
                           		metaData.tdCls += ' sfondo_rosso';
                           return '<img src=<?php echo img_path("icone/48x48/{$f['iconCls']}.png") ?> width=15>';
                        }else{
	    			  	  if(!Ext.isEmpty(value) && value > 0) return '<img src=<?php echo img_path("icone/48x48/{$f['iconCls']}.png") ?> width=15>';
	    			  }
	    		    }
			 	}, <?php 
            break;
            
            
            case 'immissione' : ?>
            
            {header: 'Immissione',
        	 columns: [
        	 {header: 'Data', 
        	 dataIndex: <?php echo j($f['config']['data_gen'])?>, 
        	 filter: {type: 'string'}, filterable: true,
        	 <?php echo isset($f['fw']) ? $f['fw'] : 'flex: 1' ?>,
        	 renderer: function(value, metaData, record){
        	      if(!Ext.isEmpty(record.get(<?php echo j($f['config']['data_mod'])?>))){
    	          	if (record.get(<?php echo j($f['config']['data_gen'])?>) != record.get(<?php echo j($f['config']['data_mod'])?>)) 
    	         	 metaData.tdCls += ' grassetto';
    	 			
    	 			q_tip = 'Modifica: ' + date_from_AS(record.get(<?php echo j($f['config']['data_mod'])?>)) + ', Utente ' +record.get(<?php echo j($f['config']['user_mod'])?>);
    			    metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';  	
    			  }		    	
    		     return date_from_AS(value);	
			}
        	 
        	 } 
        	 ,{header: 'Utente',
        	  dataIndex: <?php echo j($f['config']['user_gen'])?>, 
        	  <?php echo isset($f['fw']) ? $f['fw'] : 'flex: 1' ?>,
        	  filter: {type: 'string'}, filterable: true,
	         renderer: function(value, metaData, record){
	           if(!Ext.isEmpty(record.get(<?php echo j($f['config']['data_mod'])?>))){
	           if (record.get(<?php echo j($f['config']['user_gen'])?>) != record.get(<?php echo j($f['config']['user_mod'])?>)) 
    	          metaData.tdCls += ' grassetto';
	  
               q_tip = 'Modifica: ' + date_from_AS(record.get(<?php echo j($f['config']['data_mod'])?>)) + ', Utente ' +record.get(<?php echo j($f['config']['user_mod'])?>);
    		   metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(q_tip) + '"';    
    		   } 			    	
		       return value;	
            	} 
            	 }
        	
        	 ]},
            
            
            <?php 
            break;
            
            default:
            ?>
				{
					header: <?php echo j($header)?>,
			 		dataIndex: <?php echo j($kf)?>, 
			 		<?php echo isset($f['c_fw']) ? $f['c_fw'] : 'flex: 1' ?>,
			 		filter: {type: 'string'}, filterable: true,
			 		<?php echo out_column_renderer($kf, $f) ?>
			 	},		
			<?php
            break;
        }
        
	   
	    
	}
}

function out_column_renderer($kf, $f){
    if ($f['type'] == 'date')
        return 'renderer: date_from_AS,'; 
    
    if ($f['type'] == 'valid'){
        $ini = $f['config']['ini'];
        $fin = $f['config']['fin'];
        $date = oggi_AS_date();
        
        $ret .= "
        renderer: function(value, metaData, record){
            var ini = record.get('{$ini}');
            var fin = record.get('{$fin}');

            if (ini == 0 || fin == 0) 
                metaData.tdCls += ' sfondo_giallo';
            if (ini > 0 && fin > 0){

               q_tip =  date_from_AS(ini) + ' - ' + date_from_AS(fin);
    		   metaData.tdAttr = 'data-qtip=\"' + Ext.String.htmlEncode(q_tip) + '\"'; 

                if({$date} > ini && {$date} < fin)
                    metaData.tdCls += ' sfondo_verde';
                else
                    metaData.tdCls += ' sfondo_rosso';
                
            }
        }

     ";
      return $ret;
    }
    
}


function m_gest_tataid_get_form_insert($m_table_config){
    
    if(isset($m_table_config['fields_form'])){        
        $ar_fields = $m_table_config['fields_form'];
    } else {
        foreach ($m_table_config['fields'] as $kf => $vf) 
                $ar_fields[] = $kf;         
    }
    
    foreach ($ar_fields as $k => $v) {
        switch($m_table_config['fields'][$v]['type']){
	            case 'user':
	                get_user_combo($m_table_config, $v);
	                break;
	            case 'from_TA':
	                get_from_TA($m_table_config, $v);
	                break;
	            case 'from_TA_sys':
	                get_from_TA($m_table_config, $v, 'Y');
	                break;
	            case 'numeric':
	                get_numberfield($m_table_config, $v);
	                break;
	            case 'date':
	                get_datefield($m_table_config, $v);
	                break;	 
	            case 'IDPR':   //Id progressivo auto-incrementato -> displayfield
	                get_IDPR_field($m_table_config, $v);
	                break;	   
	            case 'textarea':
	                get_textareafield($m_table_config, $v);
	                break;
	            case 'checkbox':
	                get_checkboxfield($m_table_config, $v);
	                break;
	            case 'radio_tn':
	                get_radio_tn($m_table_config, $v);
	                break;
	            case 'fornitore':
	                get_fornitore($m_table_config, $v);
	                break;
	            default:
	                get_textfield($m_table_config, $v);
	                break;
	        }
	 
	}

	
}

function get_textfield($m_table_config, $kf){
    $f_def = $m_table_config['fields'][$kf];
    $maxLen = 100;
    if(isset($f_def['maxLength']))
        $maxLen = $f_def['maxLength'];
   
    $value = '';
    if(isset($f_def['value']))
        $value = $f_def['value'];
    
    if(isset($f_def['f_label']))
        $label = $f_def['f_label'];
    else 
        $label = $f_def['label'];
    
    ?>
				{ 
					xtype: 'textfield',
					name: <?php echo j($kf) ?>, 
					fieldLabel: <?php echo j($label)?>, 
					//anchor: '-5',
					value : <?php echo j($value); ?>,
					maxLength: <?php echo $maxLen; ?>,
					<?php echo isset($f_def['fw']) ? $f_def['fw'] : "flex: 1, anchor: '-5'" ?>,
					/*listeners:{
                       change:function(field){
                            field.setValue(field.getValue().toUpperCase());
                       }
                   }*/
				},
		
		<?php
    
}



function get_textareafield($m_table_config, $kf){
    $f_def = $m_table_config['fields'][$kf];
    $maxLen = 100;
    if(isset($f_def['maxLength']))
        $maxLen = $f_def['maxLength'];
        
        $value = '';
        if(isset($f_def['value']))
            $value = $f_def['value'];
            
        if(isset($f_def['f_label']))
            $label = $f_def['f_label'];
        else
            $label = $f_def['label'];
        
        if(isset($f_def['height']))
            $height = $f_def['height'];
        else
            $height = 40;
            
            ?>
    	{ 
    		xtype: 'textareafield',
    		name: <?php echo j($kf) ?>, 
    		fieldLabel: <?php echo j($label)?>, 
    		anchor: '-5',
    		value : <?php echo j($value); ?>,
    		height: <?php echo $height; ?>, 
    		maxLength: <?php echo $maxLen; ?>,
    		<?php echo isset($f_def['fw']) ? $f_def['fw'] : 'flex: 1' ?>
    	},
		
	<?php
    
}


function get_IDPR_field($m_table_config, $kf){
    $f_def = $m_table_config['fields'][$kf];
    $f = new Extjs_compo('displayfield', null, null, array('name' => $kf, 'fieldLabel' => $f_def['label']));
    echo_je($f->code());
    echo ",";
}

function get_numberfield($m_table_config, $kf){
    $f_def = $m_table_config['fields'][$kf];
    if(isset($m_table_config['fields'][$kf]['maxLength']))
        $maxLen = $m_table_config['fields'][$kf]['maxLength'];
    else
        $maxLen = 10;
    
    if(isset($f_def['f_label']))
        $label = $f_def['f_label'];
    else
        $label = $f_def['label'];
            
            ?>
				{ 
					xtype: 'numberfield',
					name: <?php echo j($kf) ?>, 
					fieldLabel: <?php echo j($label)?>, 
					width: '150', 
					//labelWidth: 40, 
					hideTrigger : true,
					maxLength: <?php echo $maxLen ?>,
					<?php echo isset($f_def['fw']) ? $f_def['fw'] : 'flex: 1' ?>
			
				},
		
		<?php
    
}


function get_datefield($m_table_config, $kf){
    $f_def = $m_table_config['fields'][$kf];
    echo_je(extjs_datefield($kf, $f_def['label']));
    echo ",";
}

function get_radio_tn($m_table_config, $kf){
    $f_def = $m_table_config['fields'][$kf];
  
    echo "{
            xtype: 'radiogroup',
            //width: 150,
            fieldLabel: '{$f_def['label']}',
            items: [{
            xtype: 'radio'
            , name: '{$kf}'
            , inputValue: 'T'
            , boxLabel: 'Testo'
            , checked: true
        },{
        xtype: 'radio'
        , name: '{$kf}'
        , inputValue: 'N'
        , boxLabel: 'Numero'

        }
        ]
        }, ";
}

function get_checkboxfield($m_table_config, $kf){
    $f_def = $m_table_config['fields'][$kf];
    echo_je(extjs_checkboxfield($kf, $f_def['label']));
    echo ",";
}

function get_fornitore($m_table_config, $kf){ 
    $f_def = $m_table_config['fields'][$kf];
    $f_def = $m_table_config['fields'][$kf];
    $maxLen = 100;
    if(isset($f_def['maxLength']))
        $maxLen = $f_def['maxLength'];
        
    $value = '';
    if(isset($f_def['value']))
        $value = $f_def['value'];
            
    if(isset($f_def['f_label']))
        $label = $f_def['f_label'];
    else
        $label = $f_def['label'];
                   
    ?>
    
      {
            xtype: 'fieldcontainer',
            flex:1,
            layout: { 	type: 'hbox',
            pack: 'start',
            align: 'stretch'},
            items: [
               {
                   xtype: 'displayfield',
					name: 'TACOGE_DE',
					fieldLabel: <?php echo j($label)?>, 
					anchor: '-5',
					//value : <?php echo j($value); ?>,
					maxLength: <?php echo $maxLen; ?>,
					<?php echo isset($f_def['fw']) ? $f_def['fw'] : 'flex: 1' ?>,
					
				}, { xtype: 'hiddenfield', name: <?php echo j($kf) ?>},
				
			{ xtype: 'button'
                 , margin: '0 0 0 5'
                 , scale: 'small'
                 , iconCls: 'icon-search-16'
                 , iconAlign: 'top'
                 , width: 25
                 , handler : function() {
                   var m_form = this.up('form').getForm();
                   var my_listeners = 
                     { afterSel: function(from_win, row) {
                         m_form.findField(<?php echo j($kf) ?>).setValue(row.CFCD);
                         m_form.findField(<?php echo j($kf."_DE") ?>).setValue('['+row.CFCD+']'+' '+row.CFRGS1);
                         from_win.close();
                       }
                     }
                   acs_show_win_std('Anagrafica clienti/fornitori'
                                   , '../desk_utility/acs_anag_ricerca_cli_for.php?fn=open_tab'
                                   , {cf: 'F'}, 600, 500, my_listeners
                                   , 'icon-search-16');
                 }
                        
            },
            { xtype: 'button'
             , margin: '0 0 0 5'
             , scale: 'small'
             , iconCls: 'icon-sub_red_delete-16'
             , iconAlign: 'top'
             , width: 25
             , handler : function() {
                   var m_form = this.up('form').getForm();
                   m_form.findField(<?php echo j($kf) ?>).setValue(0);
                   m_form.findField(<?php echo j($kf."_DE") ?>).setValue('');
                   
                 }
                 
            }	
            
            
            ]},
    
	<?php 
  
    
    /*$f_def = $m_table_config['fields'][$kf];
    
    if(isset($f_def['f_label']))
        $label = $f_def['f_label'];
    else
        $label = $f_def['label'];

    echo_je(extjs_combo_fornitore(array('name' => $kf, 'fieldLabel' => $label)));*/
    //echo ",";
}



function get_user_combo($m_table_config, $kf){
    
    $users = new Users;
    $ar_users = $users->find_all();
        
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']),  j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
    $ar_users_json = acs_je($user_to);
            ?>
				 {
						xtype: 'combo',
						name: <?php echo j($kf) ?>,
						fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>,
						forceSelection: true,								
						displayField: 'descr',
			            valueField: 'cod',							
						emptyText: '- seleziona -',
				   		anchor: '-5',
						store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
						typeAhead: true,
    					queryMode: 'local',                             
    					anyMatch: true, 
        				listeners   : {
							beforequery: function(record){  
							    record.query = new RegExp(record.query, 'ig');
    							record.forceAll = true;
							}
						}			
						 },
		
		<?php
    
}


function get_from_TA($m_table_config, $kf, $sys = 'N'){
    
   
    $f_def  = $m_table_config['fields'][$kf];
    
    if(isset($f_def['module']))
        $module = $f_def['module'];
    else  $module = $m_table_config['module'];
    
    if(isset($f_def['f_label']))
        $label = $f_def['f_label'];
    else
        $label = $f_def['label'];
    
     ?>
				 {
						xtype: 'combo',
						name: <?php echo j($kf) ?>,
						fieldLabel: <?php echo j($label)?>,
						anchor: '-5',
						//labelWidth: 40, 
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',							
						emptyText: '- seleziona -',
						queryMode: 'local',
     		            minChars: 1,
				   		store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [
						    <?php if($sys == 'Y') 	
						              echo acs_ar_to_select_json(find_TA_sys($f_def['TAID'], null, null, null, null, null, 0, '', 'Y', 'N', 'R'), ''); 
						          else  {
						              $aggiungi_vuoto = 'N';
						              if ($f_def['allowBlank'] == true)
						                  $aggiungi_vuoto = 'Y';
						              echo acs_ar_to_select_json(
						                  $module->find_TA_std($f_def['TAID'], null, 'N', 'N', $f_def['k2'], null, $f_def['gruppo'], 'N', 'Y', $f_def['k3']), 
						                      '', 'R', 'N', $aggiungi_vuoto); 
						          }
						    ?>	
						    ]
						}
						, listeners: { 
        			 		beforequery: function (record) {
            	         		record.query = new RegExp(record.query, 'i');
            	         		record.forceAll = true;
    				 }}
								
						 } ,
		
		<?php
    
}

function m_gest_tataid_get_next_num($m_table_config){
    global $conn, $id_ditta_default;
    
    if(isset($m_table_config['f_ditta']))
        $ditta = $m_table_config['f_ditta'];
        else
            $ditta = "TADT";
            
            $sql = "SELECT MAX({$m_table_config['field_IDPR']}) AS M_MAX
            FROM {$m_table_config['tab_name']} TA
            WHERE {$ditta} = '$id_ditta_default'";
            
            $stmt = db2_prepare($conn, $sql); 	echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
            $row = db2_fetch_assoc($stmt);
            db2_free_stmt ( $stmt );
            $c = $row['M_MAX'];
            $c++;
            return $c;
}
