<?php

require_once "../../config.inc.php";
$m_params = acs_m_params_json_decode();

$component_file_name = substr(acs_decamelize($m_params->component_params->component_name), 5);
require_once "utility/extjs_php_functions/components/{$component_file_name}.php";

$op = $_REQUEST['fn'];


$component = new $m_params->component_params->component_name('seq_config', 
    $m_params->component_params,
    $m_params->formValues
    );
$component->{$op}();
 