<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'get_json_data'){
    
    $ar = array();
    $ret = array();
    $cliente = $m_params->cliente;
   
    if ($backend_ERP == 'GL'){
        
        $sql_where = "";
        $today = oggi_AS_date();
        
        
        $sql = "SELECT *
        FROM {$cfg_mod_Spedizioni['file_anag_cli_des']}
        WHERE UPPER(BFDES0) LIKE '%" . strtoupper($p['query']) . "%'
 				AND BFTCF0=1 AND BFCLF0 = " . sql_t($cliente) . "
 				{$sql_where}";
 				
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
 				
    	while ($row = db2_fetch_assoc($stmt)) {
    	    $c_naz = trim($row['BFNAZ0']);
    	    if (trim($c_naz) == '' || trim($c_naz) == 'IT' || trim($c_naz) == 'ITA')
    	        $out_loc = trim(acs_u8e($row['BFLOC0']));
	        else {
	            $des_naz = find_TA_sys('BNAZ', trim($row['BFNAZ0']));
	            $des_naz = trim($row['BFNAZ0']);
	            $out_loc = trim(acs_u8e($row['BFLOC0'])) . " (" . $des_naz . ")";
	        }
    	        
    	        $ret[] = array( "cod" 		=> $row['BFTIN0'],
    	            "cod_cli"	=> $row['BFCLF0'],
    	            "dt"		=> $id_ditta_default,
    	            
    	            "riser" => acs_u8e(trim($row['BFLI10'])),
    	            
    	            "descr" 	=> acs_u8e(
    	                trim($row['BFRAG0'])
    	                ),
    	            "denom"  => acs_u8e(trim($row['BFRAG0'])),
    	            "out_loc"=> $out_loc,
    	            
    	            //Decodifico i vari campi indirizzo
    	            'IND_D' => acs_u8e(trim($row['BFIND0'])),
    	            'LOC_D' => acs_u8e(trim($row['BFLOC0'])),
    	            'CAP_D' => acs_u8e(trim($row['BFCAP0'])),
    	            'PRO_D' => acs_u8e(trim($row['BFPRO0'])),
    	            'NAZ_D' => acs_u8e(trim($row['BFNAZ0'])),
    	            'TEL_D' => acs_u8e(trim($row['BFTEL0'])),
    	            
    	        );
    	}
 				
    } else {
        
        $sql_where = '';
        $or_where = array();
        
        if($m_params->type != ''){
            if($m_params->type == 'D')
                $or_where[] = " SUBSTRING(TA.TAREST, 163, 1) IN ('', '{$m_params->type}')";
            else
                $or_where[] = " SUBSTRING(TA.TAREST, 163, 1) = '{$m_params->type}'";
        }
        
        if ($m_params->includi_AG == 'Y'){
            $or_where[] = " TA.TANR = '=AG' ";
        }
        
        if (count($or_where) > 0)
            $sql_where = ' AND (' . implode(' OR', $or_where) . ')';
        
        $sql = "SELECT TA.*, SUBSTRING(V2.TADES2, 27, 3) AS D_ASS
                FROM {$cfg_mod_Spedizioni['file_anag_cli_des']} TA
                LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_anag_cli_des']} V2
                ON TA.TADT = V2.TADT AND TA.TANR = V2.TANR AND TA.TACOR1 = V2.TACOR1
                AND V2.TAID = 'VUD2'
                WHERE TA.TADT = '{$id_ditta_default}' AND TA.TAID ='VUDE' 
                AND TA.TACOR1 = " . sql_t(sprintf("%09s", $cliente)) . "
 				AND TA.TATP <>'S' {$sql_where}
 				";

        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt);
        echo db2_stmt_errormsg();
        
        while ($row = db2_fetch_assoc($stmt)) {
            
            
            $c_naz = substr($row['TAREST'], 193,   2);
            if (trim($c_naz) == '' || trim($c_naz) == 'IT' || trim($c_naz) == 'ITA')
                $out_loc = trim(acs_u8e(substr($row['TAREST'],  90,  60))) . " (" . substr($row['TAREST'], 160,   2) . ")";
            else {
                $des_naz = find_TA_sys('BNAZ', trim($row['CFNAZ']));
                $out_loc = trim(acs_u8e(substr($row['TAREST'],  90,  60))) . " (" . $des_naz . ")";
            }
                
            $m_cod = trim($row['TANR']);
            
            $ret[] = array( "cod" 		=> acs_u8e($m_cod),
                "cod_cli"	=> $row['TACOR1'],
                "dt"		=> $row['TADT'],
                
                
                "descr" 	=> acs_u8e(
                    trim(substr($row['TAREST'], 90, 30)) . " " .
                    trim(substr($row['TAREST'], 150, 10)) . " " .
                    trim(substr($row['TAREST'], 160, 2)) . " "
                    ),
                "denom"  => acs_u8e(trim($row['TADESC'] . $row['TADES2'] )),
                "out_loc"=> $out_loc,
                
                //Decodifico i vari campi indirizzo
                'IND_D'  => acs_u8e(substr($row['TAREST'],  30,  60)),
                'LOC_D'  => acs_u8e(substr($row['TAREST'],  90,  60)),
                'CAP_D'  => substr($row['TAREST'], 150, 10),
                'PRO_D'  => substr($row['TAREST'], 160, 2),
                't_indi' => substr($row['TAREST'], 162, 1),
                'des_s'  => substr($row['TAREST'], 172, 1),
                'd_ass'  => trim($row['D_ASS']),
                'NAZ_D'  => substr($row['TAREST'], 193, 2),
                'TEL_D'  => substr($row['TAREST'], 173, 20)
                
            );
        }
    }
   
    
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'open_panel'){ 

    if($m_params->type == 'D') $title = "Seleziona destinazione";
    if($m_params->type == 'P') $title = "Seleziona punto vendita";
    ?>

{"success":true, 
    m_win: {
		title: <?php echo j($title); ?>		
	}, 
    "items": [

        {
            xtype: 'grid',
			
			store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
			        		    	cliente: <?php echo j($m_params->cliente); ?>,
			        		    	type : <?php echo j($m_params->type); ?>,
			        		    	includi_AG: <?php echo j($m_params->includi_AG); ?>
			        				},
							    doRequest: personalizza_extraParams_to_jsonData,	
								
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['d_ass', 'des_s', 't_indi', 'cod', 'cod_cli', 'dt', 'descr', 'denom', 'IND_D', 'LOC_D', 'CAP_D', 'PRO_D', 'TEL_D']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'TI',
	                tooltip : 'Tipo indirizzo',
	                dataIndex: 't_indi',
	                width: 40,
	                renderer: function(value, p, record){
	                      if(record.get('des_s') == 'S') p.tdCls += ' sfondo_verde';
		    			  if(record.get('t_indi') == 'P') return '<img src=<?php echo img_path("icone/48x48/clienti.png") ?> width=15>';
		    			  if(record.get('t_indi') == 'D' || record.get('t_indi').trim() == '') return '<img src=<?php echo img_path("icone/48x48/delivery.png") ?> width=15>';
		    			  if(record.get('t_indi') == 'A') return '<img src=<?php echo img_path("icone/48x48/briefcase.png") ?> width=15>';
						  			    		 
		    		  } 
	            }, {
	                header   : 'Codice',
	                dataIndex: 'cod',
	                width: 50 
	            }, {
	                header   : 'Denominazione',
	                dataIndex: 'denom',
	                flex: 10
	            }, {
	                header   : 'Indirizzo',
	                dataIndex: 'IND_D',
	                flex: 10
	            }, {
	                header   : 'CAP',
	                dataIndex: 'CAP_D',
	                width: 80
	            }, {
	                header   : 'Localit&agrave;',
	                dataIndex: 'LOC_D',
	                flex: 5
	            }, {
	                header   : 'Prov',
	                dataIndex: 'PRO_D',
	                width: 50
	            }
	            
	         ],
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	var loc_win = this.up('window');
						  	loc_win.fireEvent('afterSelected', loc_win, rec.data);
					  	
						  }
					  }	
					  
			}, viewConfig: {
			        //Return CSS class to apply to rows depending upon data values
			        getRowClass: function(record, index) {
			           return '';																
			         }   
					}
			
				  
	            
        }    

]}

<?php 


}