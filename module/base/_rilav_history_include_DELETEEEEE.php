<?php



function _rilav_H_fields($ar_raggruppamenti){
    $tmp = array();
    foreach($ar_raggruppamenti as $ragg_code){
        $tmp[] = "H_{$ragg_code}_COD";
        $tmp[] = "H_{$ragg_code}_ICONA";
        $tmp[] = "H_{$ragg_code}_STYLE_CLASS";        
    }
    return $tmp;
}

function _rilav_H_write_model_fields($ar_raggruppamenti){
    $ar = _rilav_H_fields($ar_raggruppamenti);
    return implode(',', array_map('sql_t', $ar));
}

#return:
# select
# joins
function _rilav_H_for_query($file_AH, $file_TA, $as_table_alias, $ar_raggruppamenti){
    $tmp = array(
        'select' => array(),
        'joins'  => array()
    );
    
    foreach($ar_raggruppamenti as $ragg_code){
        $tmp['select'][] = "H_{$ragg_code}_FILE.AHCARI AS H_{$ragg_code}_COD";
        $tmp['select'][] = "H_{$ragg_code}_TA.TAINDI AS H_{$ragg_code}_ICONA";
        $tmp['select'][] = "H_{$ragg_code}_TA.TAPROV AS H_{$ragg_code}_STYLE_CLASS";
        
        
        $tmp['joins'][] = "LEFT OUTER JOIN {$file_AH} H_{$ragg_code}_FILE
                             ON H_{$ragg_code}_FILE.AHDT = {$as_table_alias}.ASDT
                            AND H_{$ragg_code}_FILE.AHIDPR = {$as_table_alias}.ASIDPR
                            AND H_{$ragg_code}_FILE.AHRAG1 = '{$ragg_code}'
                            AND H_{$ragg_code}_FILE.AHSTAT = '' ";        
        $tmp['joins'][] = "LEFT OUTER JOIN {$file_TA} H_{$ragg_code}_TA
                             ON H_{$ragg_code}_TA.TADT = {$as_table_alias}.ASDT
                            AND H_{$ragg_code}_TA.TATAID = 'RILAV'
                            AND H_{$ragg_code}_TA.TAKEY1 = H_{$ragg_code}_FILE.AHCAAS
                            AND H_{$ragg_code}_TA.TAKEY2 = H_{$ragg_code}_FILE.AHCARI ";
        
    }
    
    return array(
        'select' => implode(',', $tmp['select']),
        'joins'  => implode("\n", $tmp['joins'])
    );
    
}



function _rilav_H_icon_style($ragg, $from = null){
    global $_flag_config;
    $ret = "";
    
    switch ($from) {
        case 'TA':
            $f_style_class = 'TAPROV';
            $f_icona       = 'TAINDI';
        break;
        
        default:
            $f_style_class = "H_{$ragg}_STYLE_CLASS";
            $f_icona       = "H_{$ragg}_ICONA";
        break;
    }
    
    //Sfondo cella
    $ret .= "
        var style_class = rec.get('{$f_style_class}');
        if (!Ext.isEmpty(style_class)){
          style_class = style_class.trim();
          if (style_class == 'G') metaData.tdCls += ' sfondo_giallo';
          if (style_class == 'R') metaData.tdCls += ' sfondo_rosso';
          if (style_class == 'V') metaData.tdCls += ' sfondo_verde';
        }
    ";
    
    $ret .= "
        var n_icona = rec.get('{$f_icona}');
        var n_path = " . img_path("icone/48x48/") . ";
        if (!Ext.isEmpty(n_icona) && !Ext.isEmpty(n_icona.trim())){
            return '<img src=\"' + n_path.trim() + n_icona.trim() + '.png\"  width=18>';
        } else {
            return '';
        }
    ";
    
    return $ret;
}


//esempio di ritorno(
/*array(
 '' => array('label' => 'Non terminata', 'icon' => null),
 '1' => array('label' => 'Terminata', 'icon' => 'bandiera_scacchi'),
 '2' => array('label' => 'Terminata in errore', 'icon' => 'bandiera_red'))
 )*/
function _rilav_H_field_config($file_TA, $causale, $group){
    $stmt = _rilav_H_stmt_causali_avanzamento($file_TA, $causale, $group);
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $ret[trim($row['TAKEY2'])] = array(
            'label' => trim(acs_u8e($row['TADESC'])),
            'icon'  => trim($row['TAINDI']),
            'style_class' => trim($row['TAPROV'])
        );
    }
    return $ret;
}

function _rilav_H_stmt_causali_avanzamento($file_TA, $causale, $group){
    global $conn, $id_ditta_default;
    $sql = "SELECT * FROM {$file_TA} WHERE tadt = '{$id_ditta_default}'
               AND tataid = 'RILAV'
               AND TAKEY1 = ? AND TARIF1 = ?
               ORDER BY TAPESO";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($causale, $group));
    echo db2_stmt_errormsg();
    return $stmt;
}


function _rilav_H_add_history($file, $rec_id, $group, $causale, $rilascio, $note, $fileOrig = null){
    global $conn, $id_ditta_default, $auth;
    
    //verifico se c'e' una attiva che va storicizzata
    $sql = "SELECT COUNT(*) AS C_ROW FROM {$file} WHERE AHDT = ? AND AHIDPR = ? AND AHRAG1 = ?";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($id_ditta_default, $rec_id, $group));
    echo db2_stmt_errormsg($stmt);
    $row = db2_fetch_assoc($stmt);
    db2_free_stmt ( $stmt );
    
    if ($row['C_ROW'] > 0){         
        //storicizzo la precedente (ci deve sempre esser una sola per gruppo con AHSTAT = '')
        $sql = "UPDATE {$file} SET AHSTAT='H' WHERE AHDT = ? AND AHIDPR = ? AND AHRAG1 = ?";
        $stmt = db2_prepare($conn, $sql);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt, array($id_ditta_default, $rec_id, $group));
        echo db2_stmt_errormsg($stmt);
    }
    
    //Inserimento
    $ar_ins = array();
    $ar_ins['AHDT']     = $id_ditta_default;
    $ar_ins['AHIDPR']   = $rec_id;
    $ar_ins['AHRAG1']   = $group;
    $ar_ins['AHCAAS']   = $causale;
    $ar_ins['AHCARI']   = $rilascio;
    $ar_ins['AHUSAS']   = SV2_db_user($auth->get_user());
    $ar_ins['AHDTAS']   = oggi_AS_date();
    $ar_ins['AHHMAS']   = oggi_AS_time();
    $ar_ins['AHNOTE']   = utf8_decode($note);
    
    $sql = "INSERT INTO {$file}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    echo db2_stmt_errormsg($stmt);
    return true;
}




function _rilav_H_change_to_next_rilav($file_tabelle, $file_H, $rec_id, $causale, $group, $current_value){
    //esempio di ritorno(
    /*array(
     '' => array('label' => 'Non terminata', 'icon' => null),
     '1' => array('label' => 'Terminata', 'icon' => 'bandiera_scacchi'),
     '2' => array('label' => 'Terminata in errore', 'icon' => 'bandiera_red'))
     )*/
    
    $field_config = _rilav_H_field_config($file_tabelle, $causale, $group);
    reset($field_config);
    
    //verifico in che punto sono dell'array
    $trovato = false;
    $new_flag_value = false;
    while (!$trovato) {
        if (trim(key($field_config)) == trim($current_value)){
            $trovato = true;
            $new_value = next($field_config);
            if (!$new_value){ //ho raggiunto la fine
                reset($field_config);
            }
            $new_flag_value = key($field_config);
        } else {
            
            $v = next($field_config);
            if (!$v){
                $trovato = true;
                reset($field_config);
                $new_flag_value = key($field_config);
            }
        }
    }
    
    $next_ar_value = current($field_config);
    
    if (!is_null($new_flag_value)){
        //scrivo il record
        _rilav_H_add_history($file_H, $rec_id, $group, $causale, $new_flag_value, '');
        
        //ritorno i valori impostati
        $ar_upd = array();
        $ar_upd["H_{$group}_COD"] = $new_flag_value;
        $ar_upd["H_{$group}_ICONA"] = $next_ar_value['icon'];
        $ar_upd["H_{$group}_STYLE_CLASS"] = $next_ar_value['style_class'];
        
        return(array('success' => true, 'item' => $ar_upd));
    } else {
        return(array('success' => false, 'message' => 'Non ci sono altri stati configurati'));
    }
} //
