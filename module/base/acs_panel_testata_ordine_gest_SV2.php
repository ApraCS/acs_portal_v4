<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$main_module = new Spedizioni(array('no_verify' => 'Y'));
$deskGest = new DeskGest(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// ESECUZIONI EVENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_form_comm'){
    $ret = array();
    
    if (!isset($m_params->open_request->tddocu)){
        $ret['success'] = false;
        $ret['message'] = 'Chiave documento non presente';
        echo acs_je($ret);
        exit;
    }
    
    $k_ordine = $m_params->open_request->tddocu;
    
    $oe = $s->k_ordine_td_decode_xx($k_ordine);
    $ord = $s->get_ordine_gest_by_k_docu($k_ordine);
    
    if (!$ord){
        $ret['success'] = false;
        $ret['message'] = 'Ordine non presente';
        echo acs_je($ret);
        exit;
    }
    
    //update campi su testata ordine gest
    $ar_upd = array();
    
    
    set_ar_value_if_is_set($ar_upd, 'TDBANC', acs_toDb($m_params->form_values->TDBANC));    
    set_ar_value_if_is_set($ar_upd, 'TDABI', $m_params->form_values->TDABI);
    set_ar_value_if_is_set($ar_upd, 'TDCAB', $m_params->form_values->TDCAB);
    
    set_ar_value_if_is_set($ar_upd, 'TDPAGA', $m_params->form_values->TDPAGA);
    set_ar_value_if_is_set($ar_upd, 'TDTSIV', $m_params->form_values->TDTSIV);
    set_ar_value_if_is_set($ar_upd, 'TDGGIV', $m_params->form_values->TDGGIV);
    set_ar_value_if_is_set($ar_upd, 'TD1RIV', $m_params->form_values->TD1RIV);
    set_ar_value_if_is_set($ar_upd, 'TDABI', $m_params->form_values->TDABI);
    set_ar_value_if_is_set($ar_upd, 'TDCAB', $m_params->form_values->TDCAB);
    set_ar_value_if_is_set($ar_upd, 'TDAG1', $m_params->form_values->TDAG1);
    set_ar_value_if_is_set($ar_upd, 'TDVSRF', $m_params->form_values->TDVSRF);
    
    
    if (isset($m_params->form_values->TDPA1))
        set_ar_value_if_is_set($ar_upd, 'TDPA1', sql_f($m_params->form_values->TDPA1));
    set_ar_value_if_is_set($ar_upd, 'TDAG2', $m_params->form_values->TDAG2);
    if (isset($m_params->form_values->TDPA2))
        set_ar_value_if_is_set($ar_upd, 'TDPA2', sql_f($m_params->form_values->TDPA2));
   /* set_ar_value_if_is_set($ar_upd, 'TDAG3', $m_params->form_values->TDAG3);
    if (isset($m_params->form_values->TDPA3))
        set_ar_value_if_is_set($ar_upd, 'TDPA3', sql_f($m_params->form_values->TDPA3));
 */
    set_ar_value_if_is_set($ar_upd, 'TDREFE', $m_params->form_values->TDREFE);
    set_ar_value_if_is_set($ar_upd, 'TDLIST', $m_params->form_values->TDLIST);
    set_ar_value_if_is_set($ar_upd, 'TDIVDO', $m_params->form_values->TDIVDO);
    set_ar_value_if_is_set($ar_upd, 'TDLING', $m_params->form_values->TDLING);
    set_ar_value_if_is_set($ar_upd, 'TDSC1', $m_params->form_values->TDSC1);
    set_ar_value_if_is_set($ar_upd, 'TDSC2', $m_params->form_values->TDSC2);
    set_ar_value_if_is_set($ar_upd, 'TDSC3', $m_params->form_values->TDSC3);
    set_ar_value_if_is_set($ar_upd, 'TDSC4', $m_params->form_values->TDSC4);
    set_ar_value_if_is_set($ar_upd, 'TDSC5', $m_params->form_values->TDSC5);
    set_ar_value_if_is_set($ar_upd, 'TDSC6', $m_params->form_values->TDSC6);
    set_ar_value_if_is_set($ar_upd, 'TDSC7', $m_params->form_values->TDSC7);
    set_ar_value_if_is_set($ar_upd, 'TDSC8', $m_params->form_values->TDSC8);
    set_ar_value_if_is_set($ar_upd, 'TDVALU', $m_params->form_values->TDVALU);
    set_ar_value_if_is_set($ar_upd, 'TDCINT', $m_params->form_values->TDCINT);
    set_ar_value_if_is_set($ar_upd, 'TDCAUT', $m_params->form_values->TDCAUT);
    
    //eseguo update
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_testate_doc_gest']}
        SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
        WHERE TDDT = ? AND TDTIDO = ? AND TDINUM = ? AND TDAADO = ? AND TDNRDO = ?";

    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array_merge($ar_upd, $oe));
    echo db2_stmt_errormsg($stmt);
    
    //messaggio su RI
    $sh = new SpedHistory();
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'UPD_GEST_TD',
            "k_ordine"	=> $k_ordine,
            "vals" => array(
            )));

    
    //RITORNO
    $ord = $s->get_ordine_gest_by_k_docu($k_ordine);
	$ret['success'] = true;
	$ret['ord']     = $ord;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// FORM APERTURA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){

    $tddocu = $m_params->tddocu;
    $ord = $s->get_ordine_gest_by_k_docu($tddocu);
    $oe  = $s->k_ordine_td_decode_xx($tddocu);
    //AGENTE 3
    
    $sql = "SELECT TOFIL1, HEX(SUBSTRING(TOFIL1, 4, 3)) AS PROVVIGIONE
            FROM {$cfg_mod_DeskPVen['file_testate_gest_valuta']}
            WHERE TODT = ? AND TOTIDO = ? AND TOINUM = ? AND TOAADO = ? AND TONRDO = ? AND TOVALU='EUR'";
     
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $oe);
    $row = db2_fetch_assoc($stmt);
    $ord['agente3'] = substr($row['TOFIL1'], 0, 3);
    $ord['provv3']  = text_to_number($row['PROVVIGIONE'], 2, 3);
   
?>
{"success":true, 
   m_win: {
   	width: 900, height: 640, iconCls: 'icon-edit-16'
   },
   "items": [
    {
    	xtype: 'tabpanel',
    	bodyStyle: 'padding: 10px',
    	bodyPadding: '5 5 0',
    	frame: true,
    	title: '',
    	defaults:{ anchor: '-10' , labelWidth: 130 },    
    	
    	items: [
			//form "Intestazione"
			<?php //include 'acs_panel_testata_ordine_gest_SV2_info.php'; ?>,
			
			//form "Dati contabili"
			<?php include 'acs_panel_testata_ordine_gest_SV2_comm.php'; ?>
    	]
    } //form
]}
<?php exit; } ?>