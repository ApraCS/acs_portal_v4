<?php

require_once "../../config.inc.php";
$mod_base = new Base();
$m_params = acs_m_params_json_decode();


if ($_REQUEST['fn'] == 'save_changes'){
  ditta_default($m_params->form_values->f_ditta);
  exit;
}

if ($_REQUEST['fn'] == 'open_form'){

?>{"success":true, "items": [
     {
        xtype: 'form',
        title: '',
      	bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',        
        layout: {type: 'vbox', align: 'stretch'},    		                                    
    	items: [
          {name: 'f_ditta', xtype: 'textfield', fieldLabel: 'Ditta', value: <?php echo j(ditta_default()); ?>}
    	]
      , buttons: [
       {text: 'Applica', handler: function() {
            var form_values = this.up('form').getValues();
			Ext.Ajax.request({
			   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_changes',
			   method: 'POST',
			   jsonData: {form_values: form_values}, 
			   
			   success: function(response, opts) {
			   	window.location.reload();
			   }, 
			   failure: function(response, opts) {
			      Ext.getBody().unmask();
			      alert('error');
			   }
			});	           
         }
       }
      ]	
    }
  ]
}<?php 
	exit;
}