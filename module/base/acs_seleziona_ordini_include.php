<?php 

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

/* ------------------------------------------------------------- */
function _get_json_data_ordini($m_params){
    /* ------------------------------------------------------------- */
    global $conn, $id_ditta_default, $cfg_mod_Spedizioni, $s;
    	
	$data_eva_i=$m_params->form_values->f_data_eva_i;
	$data_eva_f=$m_params->form_values->f_data_eva_f;
	
	//controllo data
	if (strlen($data_eva_i) > 0)
		$sql_where .= " AND TDDTEP >= '{$data_eva_i}'";
	if (strlen($data_eva_f) > 0)
		$sql_where .= " AND TDDTEP <= '{$data_eva_f}'";
	
	$data_reg_i=$m_params->form_values->f_data_reg_i;
	$data_reg_f=$m_params->form_values->f_data_reg_f;
	
	if (strlen($data_reg_i) > 0)
		$sql_where .= " AND TDODRE >= '{$data_reg_i}'";
	if (strlen($data_reg_f) > 0)
		$sql_where .= " AND TDODRE <= '{$data_reg_f}'";
	
	$cliente = $m_params->form_values->f_cli;
		
	if (strlen($cliente) > 0)	
	    $sql_where.= " AND TDCCON = '".sprintf("%09s", $cliente)."' ";
	
	$nr_ord =  $m_params->form_values->f_nr_or;
		
	if (strlen($nr_ord) > 0)
	    $sql_where.= " AND TDONDO = '".sprintf("%06s", $nr_ord)."'";
	
    $anno_ord =  $m_params->form_values->f_anno_or;   
    if (strlen($anno_ord) > 0)
        $sql_where.= " AND TDOADO = {$anno_ord} ";
    
    $sql_where.= sql_where_by_combo_value('TDOTPD', $m_params->form_values->f_tipo_ordine);
    $sql_where.= sql_where_by_combo_value('TDSTAT', $m_params->form_values->f_stato_ordine);
    $sql_where.= sql_where_by_combo_value('TDCDIV', $m_params->form_values->f_divisione);
    $sql_where.= sql_where_by_combo_value('TDCLOR', $m_params->form_values->f_tipologia);
    
    
    $doc_wpi_search = $m_params->open_request->doc_wpi_search;
    if ($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search]['non_evasi'] == 'Y')
        $sql_where.= " AND TDFN11 <> 1 ";
      
    

    $agente = $m_params->form_values->f_agente;
    $cage1 = $m_params->form_values->f_ag_uno;
    $cage2 = $m_params->form_values->f_ag_due;
    $cage3 = $m_params->form_values->f_ag_tre;
    $or_where = [];
    if(strlen($agente) > 0){
        if(trim($cage1) == 'Y')
            $or_where[] = " TDCAG1 =  '{$agente}'";
        if(trim($cage2) == 'Y')
            $or_where[] =  " TDCAG2 = '{$agente}'";
        if(trim($cage3) == 'Y')
            $or_where[] =  " TDCAG3 =  '{$agente}'";
                    
    }
    
    //se ho almeno una condizione (check):
    if (count($or_where) > 0)
        $sql_where .= " AND (" . implode(' OR ', $or_where) . ")";
		
	$sql = "SELECT *
            FROM {$cfg_mod_Spedizioni['file_testate']} TD0
            WHERE TDDT = '{$id_ditta_default}' {$sql_where} 
            {$a_where} 
            ORDER BY TDOADO DESC FETCH FIRST 500 ROWS ONLY";
        
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	
	$ar = array();
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
	
		$nr['anno']	        = trim($row['TDOADO']);
		$nr['numero']	    = sprintf("%06s", trim($row['TDONDO']));
		$nr['k_ordine']     = implode("_", array(
		      $row['TDDT'], 
		      $row['TDOTID'], 
		      $row['TDOINU'], 
		      $row['TDOADO'], 
		      sprintf("%-15s", sprintf("%06s", trim($row['TDONDO']))), 
		      sprintf("%-10s", ""), 
		      $row['TDDT'] ));
	    
		$nr['riferimento']	= trim($row['TDVSRF']);
	    $nr['ordine']	    = implode("_", array(trim($row['TDOADO']), sprintf("%06s", trim($row['TDONDO'])), trim($row['TDOTPD']) ));
		$nr['tipologia']	= trim($row['TDOTPD']);
		$nr['raggr']        = trim($row['TDCLOR']);
		$nr['data_eva']	    = trim($row['TDDTEP']);
		$nr['data_reg'] 	= trim($row['TDODRE']);
		$nr['stato']	    = trim($row['TDSTAT']);
		$nr['fl_art_manc']  = $s->get_fl_art_manc($row);
		$nr['fl_bloc']      = $s->get_ord_fl_bloc($row);
		$nr['art_da_prog'] = $s->get_art_da_prog($row);
		$nr['agente']      = $agente;
		$nr['age_uno']      = $m_params->form_values->f_ag_uno;
		$nr['age_due']      = $m_params->form_values->f_ag_due;
		$nr['age_tre']      = $m_params->form_values->f_ag_tre;
		
		$ar[] = $nr;
		
		$s->get_ordine_by_k_docu($nr['k_ordine']);
	}
	return grid_to_json_data($ar);
}


//********************************************************
if ($_REQUEST['fn'] == 'open'){
//********************************************************
	?>
{"success":true, "items": [

     { xtype: 'form'
     , bodyStyle: 'padding: 10px'
     , bodyPadding: '5 5 0'
     , autoScroll:true
     , frame: true
     , layout: 
         {  type: 'vbox',
           align: 'stretch'
    	 }
    	 
     , listeners: {
     	afterrender: function(compo){
     		var b_applica_filtri = compo.down('#b_applica_filtri');
     		<?php if ($m_params->auto_load == true) { ?>
     			b_applica_filtri.fireHandler();
     		<?php } ?>	
     	}
     }	 
    	 
    	 
     , buttons: [               
            {
	        	text: 'Seleziona',
	            iconCls: 'icon-sub_blue_accept-32',
	            scale: 'large',	
	       		handler: function() {
	       			var m_win  = this.up('window');
	       			
	       			
	       			
	       			<?php if (isset($m_params->doc_wpi_search)){
	       			    $doc_wpi_search = $m_params->doc_wpi_search;
                              if (isset($cfg_mod_Spedizioni['doc_wpi_search']) &&
                                  isset($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search])){
                                      if ($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search]['multiselect'] == true){ ?>
            		    var rec_selected = this.up('form').down('grid').getSelectionModel().getSelection();	       				
	       				var list_selected_id = [];
          	                for (var i=0; i<rec_selected.length; i++)
        		 				list_selected_id.push(rec_selected[i].data);		  
          					
	       				if (!rec_selected){
	       					acs_show_msg_error('Selezionare un ordine');
							return false;
						}	            				
	       				
	       				if (!Ext.isEmpty(m_win.events.afteroksave)) {
                      	m_win.fireEvent('afterOkSave', m_win, list_selected_id);
                  		}
                    <?php }else{ ?>
                        var rec_selected = this.up('form').down('grid').getSelectionModel().getSelection()[0];	       				
	       				if (!rec_selected){
	       					acs_show_msg_error('Selezionare un ordine');
							return false;
						}	            				
	       				
	       			    if (!Ext.isEmpty(m_win.events.afteroksave)) {
                          m_win.fireEvent('afterOkSave', m_win, rec_selected.get('k_ordine'));
                    	}
                              
                     <?php } 
                              }}?>  
	       			
	       			
	       			
	       			
	       			
	       		}
	       	}
	   ]	
    
     , items: [
     
     {
      xtype: 'container',
      layout: 
         {  type: 'vbox',
           align: 'stretch'
    	 },
      hidden: <?php if ($m_params->show_parameters == false) echo j(true); else echo j(false); ?>,
      flex: 1,
      items: [
     
         , { xtype: 'fieldcontainer'
    	 , layout: 
    	     { type: 'hbox'
    		 ,	pack: 'start'
    	     , align: 'stretch'
    	     }
    	 , items: [
    	 <!-- filtro Nr. Ordine -->
    	 
	       { xtype: 'displayfield',
	         fieldLabel: 'Anno / Nr. Ordine',
	         labelWidth : 141,
	         margin: "0 10 0 9"	
	       },
	               
	       { 
	       
	       xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_anno_or'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },{ 
	       
	         xtype: 'numberfield'
	       , hideTrigger : true
           , name: 'f_nr_or'
           , anchor: '-15'
           , margin: "0 0 0 0"	
           , width : 80
           , keyNavEnabled : false
           , mouseWheelEnabled : false
	       },
	       
	       
	       
	<?php if (isset($m_params->cod_cli)){ ?>
		   , {
		   		xtype: 'hiddenfield'
		   		, name: 'f_cli'
		   		, value: <?php echo j($m_params->cod_cli) ?>
			}
		   , {
		   		xtype: 'displayfield'
		   		, fieldLabel: 'Cliente'
		   		, value: <?php echo j($m_params->cod_cli) ?>
		   		, labelWidth : 50
		   		, anchor: '-15'
		   		, flex: 1		   		
		   		, margin: "0 10 0 140"
		   	
		   }
	<?php } else { ?>       
    	   <!-- filtro cliente -->
    	   , { flex: 1
    	   , xtype: 'combo'
    	   , name: 'f_cli'
    	   , fieldLabel: 'Cliente'
    	   , labelWidth : 50
    	   , anchor: '-15'
    	   , margin: "0 10 0 140"	
    	   , minChars: 2			
           , store: 
               { pageSize: 1000            	
    		   , proxy: 
    		       { type: 'ajax'
    		       , url : '../desk_vend/acs_get_select_json.php?select=search_cli_anag'
    		       , reader: 
    		           { type: 'json'
    		           , root: 'root'
    		           , totalProperty: 'totalCount'
    			       }
    			   }
    	       , fields: ['cod', 'descr', 'out_loc', 'ditta_orig'],		             	
    	       }
    	   , valueField: 'cod'
    	   , displayField: 'descr'
    	   , typeAhead: false
    	   , hideTrigger: true
    	   , anchor: '100%'
    	   , listConfig: 
    	       { loadingText: 'Searching...'
    	       , emptyText: 'Nessun cliente trovato'
    	       // Custom rendering template for each item
    	       , getInnerTpl: function() 
    	           { return '<div class="search-item">' +
    	                    '<h3><span>{descr}</span></h3>' +
    	                    '[{cod}] {out_loc} {ditta_orig}' + 
    	                    '</div>';
    	           }                
                    
               }
           , pageSize: 1000
    	   }
    	   <?php } ?>   
    	   <!-- end cliente -->
    	   
    	   
		   ] <!-- end items field container -->
		 } <!-- end field container -->
		       
       , { xtype: 'fieldcontainer'
	     , layout: 
	       { type: 'hbox'
	       , pack: 'start'
	       , align: 'stretch'
		   }
		 , items: [
    	       { name: 'f_data_reg_i'
    		   , margin: "0 10 0 10"   
    		   , flex: 1        
    		   , labelWidth :150   		
    		   , xtype: 'datefield'
    		   , startDay: 1 //lun.
    		   , fieldLabel: 'Data registrazione iniziale'
    		   , labelAlign: 'left'
    		   , format: 'd/m/Y'
    		   , submitFormat: 'Ymd'
    		   , allowBlank: true
    		   , anchor: '-15'
    		   }
    		 , { name: 'f_data_reg_f'
    		   , margin: "0 10 0 10"						     
    		   , flex: 1						                     		
    		   , xtype: 'datefield'
    		   , labelWidth :120
    		   , startDay: 1 //lun.
    		   , fieldLabel: 'finale'
    		   , labelAlign: 'right'						   
    		   , format: 'd/m/Y'
    		   , submitFormat: 'Ymd'
    		   , allowBlank: true
    		   , anchor: '-15'
    		   }
		   ]
		 }
		 
	   ,     		 { xtype: 'fieldcontainer'
					 , layout: { type: 'hbox'
							   , pack: 'start'
							   , align: 'stretch'
								}
					 , items: [
						       { name: 'f_data_eva_i'
							   , margin: "0 10 0 10"   
							   , flex: 1        
							   , labelWidth :150    		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data evasione iniziale'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   }
							    
							,  { name: 'f_data_eva_f'
							   , margin: "0 10 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , labelWidth :120 
							   , startDay: 1 //lun.
							   , fieldLabel: 'finale'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   }
						
					   ] 
					 },	 
					 
					<?php  if (isset($m_params->doc_wpi_search)){
					           $doc_wpi_search = $m_params->doc_wpi_search;
                              
                                    if (isset($cfg_mod_Spedizioni['doc_wpi_search']) &&
                                        isset($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search])){ 
                                      ?>
					  
					{ xtype: 'fieldcontainer'
					 , layout: { type: 'hbox'
							   , pack: 'start'
							   , align: 'stretch'
								},					
    				items: [
    				 <?php if($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search]['abilita_filtro_divisione'] == 'Y'){ ?>
					        {
            				name: 'f_divisone',
            				xtype: 'combo',
                        	anchor: '-15',
					        margin: "0 10 0 10",		
					    	labelWidth : 150,  
            				fieldLabel: 'Divisione',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            			    flex: 1,  
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				store: {
            					autoLoad: true,
            					editable: false,
            					autoDestroy: true,
            				    fields: [{name:'id'}, {name:'text'}],
            				    data: [
                                          <?php echo acs_ar_to_select_json(find_TA_sys('DDOC', null, null, null, null, null, 0, "", 'Y'), ""); ?>
            					    ]
            				},listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
            			}
					 <?php }?>
					 
					  <?php if($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search]['abilita_filtro_tipologia'] == 'Y'){?>
					    ,{
            				name: 'f_tipologia',
            				xtype: 'combo',
                        	anchor: '-15',
					        margin: "0 10 0 10",	
					    	labelWidth : 120,  
					    	labelAlign : 'right',
            				fieldLabel: 'Tipologia ordini',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            			    flex: 1,  
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				store: {
            					autoLoad: true,
            					editable: false,
            					autoDestroy: true,
            				    fields: [{name:'id'}, {name:'text'}],
            				    data: [
                                          <?php echo acs_ar_to_select_json($s->find_TA_std('TIPOV', null, 'N', 'N', null, null, null, 'N', 'Y'), ""); ?>
            					    ]
            				},listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
            			}
					      
					  <?php }else{?>
					  , {xtype : 'component', flex : 1, margin: "0 10 0 10"}
					  <?php }?>

    				
    				]},
					 <?php }}?>
					 
					 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ 
						
						{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [ 
						 
						    {
            				name: 'f_tipo_ordine',
            				xtype: 'combo',
                        	anchor: '-15',
					        margin: "0 10 5 10",	
					    	labelWidth : 150,  
            				fieldLabel: 'Tipi ordine',
            				displayField: 'text',
            				valueField: 'id',
            				emptyText: '- seleziona -',
            				forceSelection: true,
            			    flex: 1,  
            				multiSelect : true,
            			    queryMode: 'local',
                            minChars: 1,      	     		
            				store: {
            					autoLoad: true,
            					editable: false,
            					autoDestroy: true,
            				    fields: [{name:'id'}, {name:'text'}],
            				    data: [
                                          <?php echo acs_ar_to_select_json(find_TA_sys('BDOC', null, null, 'VO', null, null, 0, "", 'Y'), ""); ?>
            					    ]
            				},listeners: { beforequery: function (record) {
            			         record.query = new RegExp(record.query, 'i');
            			         record.forceAll = true;
            		             }}
            			},{
							name: 'f_stato_ordine',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Stato ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "0 10 0 10",
						    labelWidth : 150,
						    flex : 1,
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?>	
								    ] 
							}						 
						}]},
						
							<?php if (isset($m_params->doc_wpi_search)){
			                     $doc_wpi_search = $m_params->doc_wpi_search;
                                    if (isset($cfg_mod_Spedizioni['doc_wpi_search']) &&
                                        isset($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search])){
                                        if ($cfg_mod_Spedizioni['doc_wpi_search'][$doc_wpi_search]['abilita_filtro_agente'] == 'Y'){
                                ?>
						{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						
						{ 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    {
                        		name: 'f_agente',
                        		xtype: 'combo',
                        		labelWidth : 120,
                        		labelAlign : 'right',
                        		margin: "0 10 0 10",
                        		flex: 1,
                        		fieldLabel: 'Agente',
                        		forceSelection: true,								
                        		displayField: 'text',
                        		valueField: 'id',								
                        		emptyText: '- seleziona -',
                        		//allowBlank: false,								
                        		anchor: '-15',
                        		queryMode: 'local',
                        		minChars: 1,	
                        		store: {
                        			editable: false,
                        			autoDestroy: true,
                        			fields: [{name:'id'}, {name:'text'}],
                        			data: [								    
                        			 <?php echo acs_ar_to_select_json(find_TA_sys('CUAG', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
                        			]
                        		},listeners: { 
                        				beforequery: function (record) {
                        				record.query = new RegExp(record.query, 'i');
                        				record.forceAll = true;
                        		 }
                        		}
                        		
                             },    {
        						xtype: 'checkboxgroup',
        						fieldLabel: '',
        						width : 100,
        					    items: [{
                                    xtype: 'checkbox'
                                  , name: 'f_ag_uno' 
                                  , boxLabel: '1�'
                                  , checked : true
                                  , inputValue: 'Y'
                                }, {
                                    xtype: 'checkbox'
                                  , name: 'f_ag_due' 
                                  , boxLabel: '2�'
                                  , inputValue: 'Y'
                                }, {
                                    xtype: 'checkbox'
                                  , name: 'f_ag_tre' 
                                  , boxLabel: '3�'
                                  , inputValue: 'Y'
                                }]							
        					}
						]}
						
					      , {xtype : 'component', flex : 1}
						]}
						
						<?php }else{?>
						    {xtype : 'component', flex : 1}
						<?php } }}?>
						
						]
					 },
					 { 
						xtype: 'fieldcontainer',
						flex : 1,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{xtype : 'component', flex : 1},
						{
							xtype: 'button',	
							text: '<b>Applica filtri</b>',
							itemId: 'b_applica_filtri',
							margin: "0 10 10 0",
							width : 240,
							height :10,
							anchor: '-15',
							handler: function() {
		            	
								var l_form = this.up('form').getForm(),
	            					l_grid = this.up('window').down('grid'),
	            					cliente = l_form.findField('f_cli').getValue(),
	            					nr_ord = l_form.findField('f_nr_or').getValue();
	            				
	            				/*if(!cliente && !nr_ord){	            				
	            					acs_show_msg_error('Inserire cliente o nr ordine');
									return false;	            				
	            				}*/

	            				l_grid.store.proxy.extraParams.form_values = l_form.getValues();
	            				l_grid.store.load();
	            				l_grid.getView().refresh();	   		                     
							 }
							  
							}
						
						]
						}
					 
			]
		}		 
			
						 
				]	
				
        }
     ]        
 }



<?php 
	exit;
	}
?>
