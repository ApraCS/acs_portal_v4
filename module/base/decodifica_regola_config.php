<?php

require_once "../../config.inc.php";
$m_params = acs_m_params_json_decode();


//********************************************************************
// window per modifica domanda/risposta
//********************************************************************
if ($_REQUEST['fn'] == 'open_win'){
//********************************************************************
    $m_params_dup = $m_params;
    $m_params_dup->parentValue = $m_params->f_var;
    $proc = new ApiProc();
    
    $initialData_dom = find_TA_sys('PUVR', null, null, null, null, null, 1, '', 'Y', 'Y', 'Y');

    $initialData_f_os = array(
        
        array('id' => 'O', 'text' => '[O]  Obbligatorio'),
        array('id' => 'S', 'text' => '[S] Scelta tra lista opzioni')
    );
    
    
    $initialData_f_tp = array(
        array('id' => '=', 'text' => '[=] Uguale a'),
        array('id' => 'T', 'text' => '[T] Tipologia Variante'),
        array('id' => '>', 'text' => '[>] Maggiore di'),
        array('id' => '<', 'text' => '[<] Minore di'),
        array('id' => 'M', 'text' => '[M] Maggiore o uguale di'),
        array('id' => 'N', 'text' => '[N] Minore o uguale di')
    );
    
    switch ($m_params->f_tp) {
        case "=": //decodifico la risposta (PUVN)
            $initialData_ris = find_TA_sys('PUVN', null, null, $m_params->f_var, null, null, 0, '', 'Y', 'Y', 'Y');
            break;
        case "T": //decodifo la tipologia (PUTI)
            $initialData_ris = find_TA_sys('PUTI', null, null, null, null, null, 0, '', 'Y', 'Y', 'Y');
            break;            
    }

    

    
    $var_tab = find_TA_sys('PUVR', $m_params->f_var, null, null, null, null, 1);
    $var_num = $var_tab['tarest'];
    if($var_num == 'N')
        $hide_num = false;
    else 
        $hide_num = true;
    
    $c = new Extjs_Form( layout_ar('vbox'), null, array(
        'items' => array(
            
            extjs_combo(array(
                'fieldLabel' => 'Obbl./Scelta',
                'name' => 'f_os',
                'initialData'  => $initialData_f_os,
                'initialValue'=> trim($m_params->f_os),
                'queryMode'     => 'local'
            )),
            
            extjs_combo(array(
                'fieldLabel' => 'Variabile',
                'name' => 'f_var',
                'initialData'  => $initialData_dom,
                'initialValue'=> trim($m_params->f_var),
                'onChangeCallFormFunction' => 'refreshForm',
                'queryMode'     => 'local',
                'store_fields'  => array('id', 'text', 'sosp', 'tarest')
            )),             
            
            extjs_combo(array(
                'fieldLabel' => 'Test',
                'name' => 'f_tp',
                'initialData'  => $initialData_f_tp,
                'initialValue'=> trim($m_params->f_tp),
                'queryMode'     => 'local',
                'onChangeCallFormFunction' => 'refreshForm'
            )),
            
            extjs_combo(array(
                'fieldLabel' => 'Variante',
                'name' => 'f_van',
                'url' => "{$_SERVER['PHP_SELF']}?fn=get_json_data_ris",
                'extraParams' => $m_params_dup,
                'initialData'  => $initialData_ris,
                'initialValue'=> trim($m_params->f_van),
                'queryMode'     => 'local',
                'allowBlank'    => true
            )),
            
            array(
                'fieldLabel' => 'Valore numerico',
                'hidden' => $hide_num,
                'xtype' => 'numberfield', 'name' => 'f_van_n',
                'value' => trim($m_params->f_van)
            )
        )
    ));
    
    $c->set(array(
        'refreshForm' => extjs_code("
            function(){
               console.log('---- refreshForm ----- ');
               var me = this,
                   form    = me.getForm(),
                   f_var   = form.findField('f_var'),
                   f_van   = form.findField('f_van'),
                   f_tp    = form.findField('f_tp'),
                   f_van_n = form.findField('f_van_n'),
                   var_type = f_var.valueModels[0].get('tarest');
                   
               if (var_type == 'N'){
                    f_van.hide();
                    f_van_n.show();
               } else {
                    f_van.show();
                    f_van_n.hide();
                    
                    //in base a quanto indicato in f_tp mostro le risposte (PUVN) o le tipologie (PUTI)
                    f_van.setValue('');
                    f_van.getStore().getProxy().extraParams.parentValue = f_var.getValue();
                    f_van.getStore().getProxy().extraParams.parentValueType = f_tp.getValue();
                    f_van.getStore().load();
               }
            }
        ")
    ));
    
    
    $c->add_buttons(array(
        array(
            'xtype' => 'button',
            'text' => 'Elimina',
            'scale' => 'small',
            'iconCls' => 'icon-sub_red_delete-16',
            'handler' => extjs_code("
            function(){
                var me = this,
                    loc_form = me.up('form').getForm(),
                    loc_win  = me.up('window'),
                    f_var   =  loc_form.findField('f_var')
                    var_type = f_var.valueModels[0].get('tarest');
                
                var outValues = [];
                outValues['f_os'] = '';
                outValues['f_tp'] = '';
                outValues['f_van'] = '';
                outValues['f_van_n'] = '';
                outValues['f_var'] = '';
                outValues['var_type'] = '';                
                outValues.dom_d = '';
                outValues.ris_d = '';                                
				loc_win.fireEvent('afterSave', loc_win, outValues);
                
            }
        ")
        ), 
       array('xtype' => 'tbfill'),
       array(
        'xtype' => 'button',
        'text' => 'Conferma',
        'scale' => 'small',
        'iconCls' => 'icon-sub_blue_accept-16',
        'handler' => extjs_code("
            function(){
                var me = this,
                    loc_form = me.up('form').getForm(),
                    loc_win  = me.up('window'),
                    f_var   =  loc_form.findField('f_var')
                    var_type = f_var.valueModels[0].get('tarest');

                var outValues = loc_form.getValues();
                outValues['var_type'] = var_type;

                outValues.dom_d = loc_form.findField('f_var').rawValue;
                outValues.ris_d = loc_form.findField('f_van').rawValue;
                            
                if (loc_form.isValid()){
					loc_win.fireEvent('afterSave', loc_win, outValues);
                }
            }
        ")
       ) 
    ), 'bottom');
    
    
    
    $proc->out_json_response(array(
        'success'=>true
        , 'items' => $c->code()
    ));
    
    
    exit;
}

//********************************************************************
// decodifica domanda/risposta
//********************************************************************
if ($_REQUEST['fn'] == 'decod'){
//********************************************************************
    $ar_p = array();
    $ar_p['risposta_a_capo'] = $m_params->combo_params->risposta_a_capo;
    
    $output = decod_regole_config($m_params->f_os, $m_params->f_tp, $m_params->f_var, $m_params->f_van, $ar_p);
      
    echo acs_je(array('success' => true, 'output' => $output, 'dec_dom' => $dec_dom));    
exit;
}





//********************************************************************
// recupero elenco risposte
//********************************************************************
if ($_REQUEST['fn'] == 'get_json_data_ris'){
    //********************************************************************
    $ret = array();
    
    switch ($m_params->parentValueType) {
        case 'T':   //elenco tipologie
            $ret = find_TA_sys('PUTI', null, null, null, null, null, 0, '', 'Y', 'N', 'Y');;
        break;
        case '=':   //elenco tipologie
            $ret = find_TA_sys('PUVN', null, null, $m_params->parentValue, null, null, 0, '', 'Y', 'N', 'Y');
        break;
    }
    
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'session_copy'){      
    if (is_null( $_SESSION['regola_config']))
        $_SESSION['regola_config'] = array();
    
    $_SESSION['regola_config'][] = $m_params->regola_config;
    exit;
}

if ($_REQUEST['fn'] == 'session_incolla'){
    
    if (is_null($_SESSION['regola_config']))
        $_SESSION['regola_config'] = array();
    
    $ret['regola_config'] = $_SESSION['regola_config'];
    
    echo acs_je($ret);
    exit;
    
}
