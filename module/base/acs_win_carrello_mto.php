<?php

require_once("../../config.inc.php");
require_once("../desk_prod/acs_panel_prog_prod_include.php");

$main_module = new DeskProd();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// GET JSON DATA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    $n_ord = $m_params->ordine;
    $stmt  = $s->get_rows_ordine_by_num($n_ord, array('MTO', 'MTS', 'B_MRP'), 'Y');
    $data  = array();
    
    while ($row = db2_fetch_assoc($stmt)) {
        //if (strlen(trim($row['RDDES1'])) > 0)
        //	$row['RDDES1'] = trim($row['RDDES1']) . "<br>" . trim($row['RDDES2']);
        
        $row['RDDES1'] = acs_u8e($row['RDDES1']);
        $row['RDDART'] = acs_u8e($row['RDDES1']);
        $row['RDFG02'] = acs_u8e($row['RDFG02']);
        $row['RDFG01'] = acs_u8e($row['RDFG01']);
        $row['maga'] = trim($row['M2MAGA']);
        $row['disponibilita'] = acs_u8e((float)$row['M2QGIA'] + (float)$row['M2QORD'] - (float)$row['M2QFAB']);

        if (trim($row['RDDES2']) != ''){
            $ord_ass_exp = explode("_", $row['RDDES2']);
            if (count($ord_ass_exp) >= 5){
                $row['k_ord_art'] = implode("_", array(
                    $id_ditta_default,
                    trim($ord_ass_exp[4]),
                    trim($ord_ass_exp[3]),
                    trim($ord_ass_exp[0]),
                    trim($ord_ass_exp[1])
                ));
                
                $ord_doc = $s->get_ordine_gest_by_k_docu($row['k_ord_art']);
                if ($ord_doc)
                    $row['maga'] = $ord_doc['TDDEPO'];
            }
        }
        
        $row['stato_MTO'] = $s->decod_std('MTOST', trim($row['RDFG03']));
        //$row['k_ord_art'] = "{$id_ditta_default}_AO_AO1_".trim($row['RDDES2']);

        //Da TDDOCU ricavo ditta origine
        $row['k_ordine']=$row['TDDOCU'];
        $ordine_exp = $s->k_ordine_td_decode_xx($row['TDDOCU']);
        $row['ditta_origine'] = $ordine_exp['TDDT'];
        
        //php7
        $row = array_map('utf8_encode', $row);
        $accoda_riga = true;
        
        if (isset($_REQUEST['ssolo_stato']))
            if (trim($_REQUEST['solo_stato']) != trim($row['RDRIFE']))
                $accoda_riga = false;
                
        if (isset($_REQUEST['solo_con_documento']) && $_REQUEST['solo_con_documento'] == 'Y')
            if (trim($row['RDDES2']) == '')
                $accoda_riga = false;
        
        if ($accoda_riga)
            $data[] = $row;
    }
    
    $ord_exp = explode('_', $n_ord);
    $anno = $ord_exp[2];
    $ord = $ord_exp[3];
    $ar_ord =  $s->get_ordine_by_k_ordine($n_ord);
    $m_ord = array(
        'TDONDO' => $ar_ord['TDONDO'],
        'TDOADO' => $ar_ord['TDOADO'],
        'TDDTDS' => print_date($ar_ord['TDDTDS'])
    );
    
    echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
    
    $appLog->save_db();
    exit();
} 


//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_win'){
//----------------------------------------------------------------------
    $n_ord = $m_params->ordine;

    ?>
        {"success":true, 
        	m_win: {
        		title: 'Segnalazione articoli critici'
        		, iconCls: 'icon-shopping_cart-16'
        		, width: 950
        		, height: 500
        	},
        	"items": [
        		{
                   xtype: 'grid',

                   store: {
                   		xtype: 'store',
    					autoLoad:true,
    					groupField: 'RDTPNO',

				        proxy: {
	           				type: 'ajax', timeout: 240000, actionMethods: {read: 'POST'},
                            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
                            extraParams: <?php echo acs_je($m_params); ?>,
                            doRequest: personalizza_extraParams_to_jsonData,
                            reader: {type: 'json', method: 'POST', root: 'root'}
                        },

						fields: [
	            			'REVOCHE', 'ditta_origine', 'TDDTEP', 'RDFMAN', 'RDDTDS', 'RDDFOR', 'RDDTEP', 'RDTPNO', 'RDOADO', 'RDONDO', 'RDRIFE', 'RDART', 'RDDART', 'RDDES1', 'RDDES2', 'RDUM', 'RDDT', {name: 'RDQTA', type: 'float'}
	        			]
	        			
	        			, reader: new Ext.data.JsonReader()
                   }, //store
                   
				   columns: [
                        {
                        	header    : 'Disp. Sped.',
                        	dataIndex : 'RDDTDS',
                            width     : 70,
                            renderer  : date_from_AS
                         }, {
                            header    : 'Stato',
                            dataIndex : 'RDRIFE',
                            width     : 95
                         }, {
                            header    : 'Fornitore',
                            dataIndex : 'RDDFOR',
                            flex      : 50
                         }, {
                            header    : 'Descrizione',
                            dataIndex : 'RDDES1',
                            flex      : 150
                         }, {
                            header    : 'Codice',
                            dataIndex : 'RDART',
                            width      : 150
                         },{
                            header    : 'Um',
                            dataIndex : 'RDUM',
                            width     : 30
                         }, {
                            header    : 'Q.t&agrave;',
                            dataIndex : 'RDQTA',
                            width     : 50,
                            align	  : 'right', 
                            renderer  : floatRenderer2
                         }, {
                            header    : 'Consegna',
                            dataIndex : 'RDDTEP',
                            flex      : 30,
                            renderer  : date_from_AS
                         }, {
                            header    : 'Documento',
                            dataIndex : 'RDDES2',
                            flex      : 60
                         }
					],
					
					listeners: {
					 	afterrender: function(comp){
			                 //autocheck degli ordini che erano stati selezionati
		            	  		comp.store.on('load', function(store, records, options) {
			            	  		b_rev = comp.up('window').down('#b_revoche');
									if(records[0].data.REVOCHE > 0){
										revoche = records[0].data.REVOCHE;
										b_rev.setText( '<b>' + b_rev.text + ' ('+ revoche + ') </b>');
										 
									}
			            	  							
		    					}, comp);
		    			 },
    					  celldblclick: {								
    						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
      						  	rec = iView.getRecord(iRowEl);
      						  	
      						  	acs_show_win_std(null,
      						  		'../desk_vend/acs_background_mrp_art.php', {
      						  		dt: rec.get('ditta_origine'), 
      						  		rdart: rec.get('RDART')
  						  		}, null, null, null, null);
  						  		
    						  } //function
    					  } //celldbclick
					} //listeners
				  , dockedItems: [{
                    dock: 'bottom',
                    xtype: 'toolbar',
                    scale: 'large',
                    items: [
                     {
                        xtype: 'button',
                        text: 'Revoche MTO',
                        itemId : 'b_revoche',
                        style:'border: 1px solid gray;', 
    		            scale: 'large',	               
    		            width : 150,     
    		            margin : '0 0 0 400',      
    		          	handler: function() {
    		          	 
    	       			      acs_show_win_std(null,
      						  		'../base/acs_revoche_mto.php?fn=open_win', {
      						  		k_ordine: <?php echo j($m_params->ordine); ?>
  						  	}, null, null, null, null);
    	       			  
    		              
    		              
    		              }

			        }
                    ]
                    
                    }]
            	} //grid
        	]
        }
<?php } exit;