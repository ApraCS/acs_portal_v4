<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$main_module = new Spedizioni(array('no_verify' => 'Y'));
$deskGest = new DeskGest(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// ESECUZIONI EVENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_scrivi_messaggio'){
    $ret = array();
    
    if (!isset($m_params->open_request->tddocu)){
        $ret['success'] = false;
        $ret['message'] = 'Chiave documento non presente';
        echo acs_je($ret);
        exit;
    }
    
    if (!strlen($m_params->open_request->messaggio) > 0 ){
        $ret['success'] = false;
        $ret['message'] = 'Messaggio non presente';
        echo acs_je($ret);
        exit;
    }
    
    $k_ordine = $m_params->open_request->tddocu;
    
    $oe = $s->k_ordine_td_decode_xx($k_ordine);
    $ord = $s->get_ordine_gest_by_k_docu($k_ordine);
    
    if (!$ord){
        $ret['success'] = false;
        $ret['message'] = 'Ordine non presente';
        echo acs_je($ret);
        exit;
    }
    
    //messaggio su RI
    $sh = new SpedHistory();
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> $m_params->open_request->messaggio,
            "k_ordine"	=> $k_ordine,
            "vals" => (array)$m_params->open_request->messaggio_ar));
    
    //RITORNO
    $ord = $s->get_ordine_gest_by_k_docu($k_ordine);
	$ret['success'] = true;
	$ret['ord']     = $ord;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// FORM APERTURA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){

    $tddocu = $m_params->tddocu;
    $ord = $s->get_ordine_gest_by_k_docu($tddocu);

?>
{"success":true, "items": [
  {
    xtype : 'form',
    frame: false,
    items : [
    	{xtype: 'displayfield', fieldLabel: 'Ordine',    value: <?php echo j($m_params->tddocu); ?>},
    	{xtype: 'displayfield', fieldLabel: 'Messaggio', value: <?php echo j($m_params->messaggio); ?>}
    	
    ]
    
    , buttons: [{
            text: 'Esegui',
            iconCls: 'icon-save-32', scale: 'large',
            handler: function() {
            	var form = this.up('form').getForm();
            	var loc_win = this.up('window');

    			if(form.isValid()){
    				Ext.getBody().mask('Loading... ', 'loading').show();
    				Ext.Ajax.request({
    						timeout: 240000,
    				        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_scrivi_messaggio',
    			        jsonData: {
    			        	form_values: form.getValues(),
    			        	open_request: <?php echo acs_je($m_params) ?>
    			        },	
    			        method     : 'POST',
    			        waitMsg    : 'Data loading',
    			        success : function(result, request){

    						Ext.getBody().unmask();
    			            var jsonData = Ext.decode(result.responseText);
    			            
    			            if (jsonData.success == true){
    			            	acs_show_msg_info('Mesaggio scritto correttamente');
    			            	loc_win.destroy();
    			            } else {
    			            	acs_show_msg_error('Errore in fase di scrittura messaggio');
    			            }
    			            
    			            
    			        },
    			        failure    : function(result, request){
    						Ext.getBody().unmask();
    			            Ext.Msg.alert('Message', 'No data to be loaded');
    			        }
    			    });
    	    	}
        	} //handler
    	} //salva
	]
  } //form  
]}
<?php exit; } ?>