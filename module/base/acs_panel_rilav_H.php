<?php

require_once("../../config.inc.php");
require_once("../base/_rilav_g_history_include.php");


$m_params = acs_m_params_json_decode();

$main_module = new DeskProd();


/*if (isset($m_params->open_parameters->file))
    require_once("../base/_rilav_g_history_include.php");
else
    require_once("../base/_rilav_history_include.php");*/


//---------------------------------------------------------------------
if ($_REQUEST['fn'] == 'exe_add_rilav_H'){
//---------------------------------------------------------------------
    _rilav_H_add_history($m_params->open_parameters->file_H, 
        $m_params->open_parameters->rec_id, 
        $m_params->open_parameters->group, 
        $m_params->open_parameters->causale, 
        $m_params->causale_rilascio->TAKEY2, 
        $m_params->form_values->NOTA,
        $m_params->open_parameters->file);
    
    //return
    $group = $m_params->open_parameters->group;
    $ar_upd = array();
    $ar_upd["H_{$group}_COD"] = $m_params->causale_rilascio->TAKEY2;
    $ar_upd["H_{$group}_ICONA"] = $m_params->causale_rilascio->TAINDI;
    $ar_upd["H_{$group}_STYLE_CLASS"] = $m_params->causale_rilascio->TAPROV;
    
    echo acs_je(array('success' => true, 'item' => $ar_upd));
    exit;
}


// ******************************************************************************************
// GET JSON DATA H (history per todo)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_H'){
   
    $where_file = '';
    
    //se ricevo $m_params->file sono nel file globale (WPIGAH0) quindi filtro per file
    $where_file .= sql_where_by_combo_value('AHFILE', $m_params->file);
    
    $sql = "SELECT * from {$m_params->file_H} 
            WHERE AHIDPR = ? AND AHRAG1 = ? 
            {$where_file}
            ORDER BY AHDTAS DESC, AHHMAS DESC ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->rec_id, $m_params->group));
    $ret  = array();
    
    while ($row = db2_fetch_assoc($stmt)) {
        $row = array_map('rtrim', $row);
        $row['AHNOTE'] = acs_u8e($row['AHNOTE']);
        $row_cv = $main_module->find_TA_std('RILAV', $m_params->causale, 'N', 'N', $row['AHCARI'], null, $m_params->group);
     
        $row['descrizione'] = $row_cv[0]['text'];
        $ret[] = $row;
    }
    echo acs_je($ret);
    exit();
} 


// ******************************************************************************************
// GET JSON DATA CAUSALI (causali ammesse per avanzamento)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_causali'){
    
    $stmt = _rilav_H_stmt_causali_avanzamento($m_params->file_TA, $m_params->causale, $m_params->group);
    $ret = array();
    while ($row = db2_fetch_assoc($stmt)) {
        $row = array_map('rtrim', $row);
        $ret[] = $row;
    }    
    echo acs_je($ret);
    exit();
} 

//----------------------------------------------------------------------
if ($_REQUEST['fn'] == 'open_tab'){
//----------------------------------------------------------------------
    ?>
        {"success":true, 
        	m_win: {
        		title: 'History avanzamento stato riga distinta'
        		, width: 950
        		, height: 500
        		, iconCls : 'icon-button_blue_play-16'
        	},
        	"items": [
        	  {
        		xtype: 'container', layout: <?php echo acs_je(layout_ar('hbox')) ?>,
        		items: [
        			<?php echo _rilav_history_grid($m_params) ?>,
        			<?php echo_je(_rilav_history_form_insert($m_params)) ?>
        		]
              }		
        	]
        }
<?php } exit;




function _rilav_history_form_insert($m_params){
    $c = new Extjs_Form( layout_ar('vbox'), 'Nuovo avanzamento');    
    $c->set(array('flex' => 1, 'frame' => true));
    $c->set(array('padding' => 10));   
    $c->add_items(array(_rilav_history_grid_causali($m_params)));
    $c->add_items(array(
        array('xtype' => 'textfield', 'padding' => '10 0', 'name'=>'NOTA', 'fieldLabel' => 'Nota', 'maxLength' => 100)));
    

    $c->add_items(array(
        array('xtype' => 'button', 'text' => 'Inserisci avanzamento', 'width' => '50%',
            'iconCls' => 'icon-button_blue_play-32', 'scale' => 'large',
            'handler' => extjs_code("
                   function (value, metaData, rec, row, col, store, gridView){
                            var form = this.up('form'),
                                causali_grid = form.down('grid'),
                                form_values = this.up('form').getValues(),
                                causale_rilascio = causali_grid.getSelectionModel().getSelection(),
                                m_win = this.up('window');
                            
                            if (Ext.isEmpty(causale_rilascio)){
                                acs_show_msg_error('Selezionare una causale');
						        return false;
                            }

	    	          		Ext.Ajax.request({
 						        url     : '" . $_SERVER['PHP_SELF'] . "?fn=exe_add_rilav_H',
 						        method  : 'POST',
 			        			jsonData: {
 			        			   open_parameters: " . acs_je($m_params) . ",
                                   form_values: form_values,
                                   causale_rilascio: causale_rilascio[0].data
 								},
 								
 								success: function(response, opts) {
						        	 var jsonData = Ext.decode(response.responseText);
						        	 
						        	 if (!jsonData.success){
						        	 	acs_show_msg_error(jsonData.message);
						        	 	return false;
						        	 }
						        	 
						        	 m_win.fireEvent('afterInsert', m_win, jsonData);
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
 								
 						    });
	    		   }
              "))));
    
    return $c->code();
}

//elenco causali ammesse su ATTAV/raggruppamento
function _rilav_history_grid_causali($m_params){
    $c = new Extjs_Grid(null, 'Seleziona causale');
    $c->set(array('height' => 200));
    $c->set(array(
        'columns' => array(
              grid_column_h('Codice',    'TAKEY2',  'w50', 'Codice')
            , grid_column_h('Causale',   'TADESC',  'f1',   'Descrizione')
            , grid_column_h_img('button_blue_play', 'H_', 'Icona', array(
                'renderer' => extjs_code("
                   function (value, metaData, rec, row, col, store, gridView){
	    	        " . _rilav_H_icon_style('ICONA', 'TA') . "
	    		   }
              ")) )
        ),
        'store'       => array(
            'autoLoad'    => true,
            'fields'      => array('TAKEY2', 'TADESC', 'TAINDI', 'TAPROV'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_json_data_causali'), $m_params))
    ));
    return $c->code();
}



function _rilav_history_grid($m_params){
   $ret = "{
        xtype: 'grid', flex: 2,
        store: {
            xtype: 'store',
            autoLoad:true,
            proxy: {
                type: 'ajax', timeout: 240000, actionMethods: {read: 'POST'},
                url: '{$_SERVER['PHP_SELF']}?fn=get_json_data_H',
                extraParams: " . acs_je($m_params) . ",
                doRequest: personalizza_extraParams_to_jsonData,
                reader: {type: 'json', method: 'POST', root: 'root'}
            },
			fields: ['AHUSAS', 'AHDTAS', 'AHHMAS', 'AHCARI', 'AHNOTE', 'descrizione'],
	        reader: new Ext.data.JsonReader()
        }, //store
                   
	    columns: [
	        {
            	header    : 'Data',
            	dataIndex : 'AHDTAS',
                width     : 70, 
                renderer  : date_from_AS
             },
            {
            	header    : 'Ora',
            	dataIndex : 'AHHMAS',
                width     : 70,
                renderer  : time_from_AS
             }, {
                header    : 'Utente',
                dataIndex : 'AHUSAS',
                width     : 70
             }, {
                header    : 'Causale',
                dataIndex : 'AHCARI',
                width     : 50
             }, {
                header    : 'Descrizione',
                dataIndex : 'descrizione',
                flex      : 1
             },{
                header    : 'Nota',
                dataIndex : 'AHNOTE',
                flex      : 1
             }
         ]				 
    }";
   return $ret;
}