<?php

$s = new Spedizioni(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));

// ******************************************************************************************
// INSERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_canc'){
    
	$m_params = acs_m_params_json_decode();
	$ret = array();
	
	foreach($m_params->list_selected_id as $v){

	if(isset($m_table_config['CL_AF']) && strlen($m_table_config['CL_AF']) > 0){
	 
	    $sql = "SELECT COUNT(*) AS C_ROW
	    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
	    LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art_formati']} AF
	    ON AR.ARDT = AF.AFDT AND AR.ARART = AF.AFCMAT
	    WHERE ARDT = '{$id_ditta_default}' AND {$m_table_config['CL_AF']} = '{$v->cod}' ";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt);
	    $row = db2_fetch_assoc($stmt);
	    
	}
	
	
	if($row['C_ROW'] > 0){
	    
	    $ret['error_msg'] = 'Voce utilizzata, non cancellabile!';
	    $ret['success'] = false;
	    
	}else{
	    
	    
	    $sql = "DELETE FROM {$m_table_config['tab_name']} TA WHERE RRN(TA) = ?";
	    
	    $stmt = db2_prepare($conn, $sql);
	    echo db2_stmt_errormsg();
	    $result = db2_execute($stmt, array($v->rrn));
	    
	    $ret['success'] = true;
	    
	   
	}
	

	
	}

	
	
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// INSERIMENTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_insert'){
	$m_params = acs_m_params_json_decode();
	$ar_ins = (array)$m_params->form_values;
	
	if (isset($ar_ins['TADESC']))
	    $ar_ins['TADESC'] = acs_toDb($ar_ins['TADESC']);

	#solo php7: $f_ditta = $m_table_config['f_ditta'] ?? 'TADT';
	$f_ditta = isset($m_table_config['f_ditta']) ? $m_table_config['f_ditta'] : 'TADT';	
	$ar_ins[$f_ditta] 	= $id_ditta_default;
	
	if (isset($m_table_config['TATAID']))
	  $ar_ins['TATAID'] 	= $m_table_config['TATAID'];
	
    if (isset($m_params->cod) && strlen($m_params->cod) > 0)
      $ar_ins['CDDICL'] 	= $m_table_config['cod_indice'];
   
    //Eventale field auto incrementato globale
    if (isset($m_table_config['field_IDPR'])){
      
        //ToDo: eventuale parametro per indicare da quale contatore prendere il next_num
        
        //Se non indicato il parametro per il contatore, recuper il max attuale e incremento di 1
        //ToDo: lock
        $ar_ins[$m_table_config['field_IDPR']] 	= m_gest_tataid_get_next_num($m_table_config);        
    }
	
	$sql = "INSERT INTO {$m_table_config['tab_name']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;	
}



// ******************************************************************************************
// UDDATE CELL ROIW VALUE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save_cell_data'){
	$m_params = acs_m_params_json_decode();
	$ar_upd = array($m_params->field => $m_params->value);
	
	
	if (isset($ar_upd['TADESC']))
	    $ar_upd['TADESC'] = acs_toDb($ar_upd['TADESC']);

	$sql = "UPDATE {$m_table_config['tab_name']} TA
			SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
			WHERE RRN(TA) = ?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_upd,
			array($m_params->rrn)
			));
	
	
	if(trim($m_params->chiave)!= ''){
	    $ha_nota = $desk_art->has_nota_indici($m_params->chiave);
	    if($ha_nota > 0){
	     	$sql = "SELECT * FROM {$m_table_config['tab_name']} CD
	               WHERE RRN(CD) = '{$m_params->rrn}'";
	        
	        $stmt = db2_prepare($conn, $sql);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt);
	        $row = db2_fetch_assoc($stmt);
	        
	        $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
	        $new_chiave = implode ("|", $ar_comp);
	        	        
	        $ar_ins = array();
	        $ar_ins['NTKEY1'] = $new_chiave;
	        $sql_up = "UPDATE {$cfg_mod_Spedizioni['file_note']} NT
	                  SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
	                  WHERE NTKEY1 = '{$m_params->chiave}'";
	        $stmt_up = db2_prepare($conn, $sql_up);
	        echo db2_stmt_errormsg();
	        $result = db2_execute($stmt_up, $ar_ins);
	        echo db2_stmt_errormsg();
	        
	    }
	}
	

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// GET GRID DATA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_data'){
    
    if (isset($m_params->m_table_config))
        $m_table_config = $m_params->m_table_config;
    else 
        $m_table_config = (object)$m_table_config;
 
    $sql_where = "";
     
    if(isset($m_table_config->f_ditta))
        $ditta = $m_table_config->f_ditta;
    else
        $ditta = "TADT";
    
    if(isset($m_table_config->TATAID))
        $sql_where .= " AND TATAID = '{$m_table_config->TATAID}'";
            
    
        
    if(isset($m_table_config->flt_adv)){
  
        foreach ($m_table_config->flt_adv as $kf=>$f){
            $campo = $kf;
            $vals = $f; 
            
            foreach($f as $k=>$v){
                if($v != '')
                    $sql_where .= sql_where_by_combo_value($campo, $vals);
          
            }
                
               
        }
    }
   
       
    if(isset($m_table_config->fields->CDSEQI))
        $order = " ORDER BY CDSEQI";
    
    if(isset($m_table_config->cod_indice))
        $sql_where .=  " AND CDDICL = '{$m_table_config->cod_indice}'";
    
    if(isset($m_table_config->form_values)){
        
        $sql_where.= sql_where_by_combo_value('CDSEQI', $m_table_config->form_values->f_seq_ind);
        
        $sql_where.= sql_where_by_combo_value('CDCMAS', $m_table_config->form_values->f_gruppo);
        
        if(strlen($m_table_config->form_values->f_d_voce) > 0)
            $sql_where.= "AND CDDESC LIKE '%{$m_table_config->form_values->f_d_voce}%'";
        if(strlen($m_table_config->form_values->f_radice) > 0)
            $sql_where.= "AND CDRSTR = '{$m_table_config->form_values->f_radice}'";
        
    }
    
    if(isset($m_table_config->f_lista) && strlen($m_table_config->f_lista) > 0)
        $sql_where.= "AND CLCDLI = '{$m_table_config->f_lista}'";
    
        

	$sql = "SELECT RRN (TA) AS RRN, TA.*
			FROM {$m_table_config->tab_name} TA
			WHERE {$ditta} = '$id_ditta_default'
			$sql_where $order";
				
	$stmt = db2_prepare($conn, $sql); 	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);	
	$data = array();
	while ($row = db2_fetch_assoc($stmt)) {
	    $row['TADESC'] = acs_u8e($row['TADESC']);
	    $ar_comp = array(trim($row['CDDT']), trim($row['CDDICL']), trim($row['CDCMAS']), trim($row['CDSEQU']), trim($row['CDSLAV']));
	    $row['chiave'] = implode ("|", $ar_comp);
	    if(isset($m_table_config->note) && $m_table_config->note == 'Y'){
	        $row['note'] =  $desk_art->has_nota_indici($row['chiave']);
	    }
	    
	    if (isset($_row_post_fn)){
	        $row = $_row_post_fn($row, $m_table_config);
	    }
	        
		$data[] = array_map('trim', $row);
	}
	echo acs_je($data);
	exit;
}	



// ******************************************************************************************
// GRID VIEW
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
    
    $title = "";    
    if(isset($m_table_config['TATAID']))
        $title = "Tab.Id: {$m_table_config['TATAID']} - ";        
?>
{"success":true, 
	
  <?php if (isset($m_table_config['m_win'])){ ?>
  	m_win: <?php echo acs_je($m_table_config['m_win']) ?>,
  <?php } ?>


  "items": [
  {
	xtype: 'grid',
	multiSelect: true,
	title: <?php echo j($title. $m_table_config['descrizione'])?>,
	 tbar: new Ext.Toolbar({
	          items:['->',
	            {iconCls: 'tbar-x-tool x-tool-refresh', 
	           	   handler: function(event, toolEl, panel){ 
	           	   	this.up('panel').getStore().load();
	           	   		}
	           	   }
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	    
	
        features: [
	
		{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
			 	type: 'boolean',
				dataIndex: 'visible'
		     }
		      ]
		}],	

	  plugins: [
          Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
          })
      ],		
		
	
	store: {

		xtype: 'store',
		autoLoad:true,

		proxy: {
			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data',
			type: 'ajax',
			actionMethods: {
				read: 'POST'
			},
			extraParams: {
			
	                       m_table_config : <?php echo acs_je($m_table_config); ?>		
	        				},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
        			fields: <?php echo m_gest_tataid_get_fields($m_table_config) ?>
    	}, //store
    		
	        columns: [
	        
	        <?php if($m_table_config['show_dx_mobile'] == 'Y'){ ?>
	        	<?php echo dx_mobile(); ?>,
	        <?php } ?>
	        
	        	<?php m_gest_tataid_get_columns($m_table_config) ?>	
	            
	         ],  
			listeners: {
			
			 <?php if($m_table_config['fireEvent'] == 'Y'){?>
			  afterrender: function(comp) {
			        win = comp.up('window');
			        win.tools.close.hide();
			        var myCloseTool = Ext.create('Ext.panel.Tool', {
			            type: 'close',
			            handler: function() {
			               win.fireEvent('afterInsert', win);
			              
			            }
			        })
			        win.header.insert(3, myCloseTool); // number depends on other items in header
			    
			    },
	          <?php }?>	
	          	celldblclick: {	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){		           
		           		var col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	var rec = iView.getRecord(iRowEl);
					  	var loc_win = this.up('window');
					  	var grid = this;
					  		
					  	var col = iView.getGridColumns()[iColIdx];
					  	if (!Ext.isEmpty(col.nt_config)){
    					  	var my_listeners = {
    	    		  				afterSave: function(from_win, values){
    	    		  			    rec.set(col_name, values.icon);
    	    		  			    from_win.close();
    		     					}	
    	     					};
    					  	acs_show_win_std(null, '../base/acs_gestione_nota_nt.php?fn=open_panel', {
    					  		nt_config: col.nt_config,
    					  		chiave : rec.get('chiave')
    					  		}, null, null, my_listeners);
					  		return;
					  	}	
					  	
					  	if(col_name == 'note'){
					  	
					  		<?php 
					  		   if(isset($m_table_config['note_src']))
					  		       $note_src = $m_table_config['note_src'];
					  		   else 
					  		       $note_src = 'acs_anag_artd_note_indici.php?fn=open_panel';
					  		?>    
					  	
					  		var my_listeners = {
	    		  				afterSave: function(from_win, values){
	    		  			    rec.set('note', values.icon);
	    		  			    grid.getStore().load();
		     					from_win.close();
		     					}	
	     					};
					  		acs_show_win_std('Note', <?php echo j($note_src) ?>, {chiave : rec.get('chiave')}, 400, 250, my_listeners, 'icon-comment_edit-16');
					  	}else{
					  		loc_win.fireEvent('afterClick', rec, loc_win);
					  	}
					  	           
	            	}	          
	           	},
	           	
	           	
	           	
	        		edit: function(editor, e, a, b){
							var grid = editor.grid;
							var record= e.record;	
							
							var rrn = record.get('RRN');
							var chiave = record.get('chiave');
							var field = editor.context.field;
							var old_value =  e.originalValue;
							var new_value = e.value;
							
							if (old_value != new_value){
								Ext.Ajax.request({
							            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_cell_data',
							            timeout: 2400000,
							            method: 'POST',
					        			jsonData: {
					        				rrn: rrn,
					        				chiave : chiave,
					        				field: field,
					        				value: new_value
					        				},						            
							            success: function (result, request) {
							             var jsonData = Ext.decode(result.responseText);
							             if (jsonData.success == false){
							             	console.log('error');
							             }
							             if (jsonData.success == true){
							             	record.commit();
							             	//grid.getStore().load();
							             }							        	 							        		
							            },
							            failure: function ( result, request) {
											Ext.getBody().unmask();														            
							            }
							        });							
							} 
            		}, //after edit	           	
	           	
	           		           
			  	itemcontextmenu : function(grid, rec, node, index, event) {	
					event.stopEvent();
					var voci_menu = [];
					
					 id_selected = grid.getSelectionModel().getSelection();
					     list_selected_id = [];
					     for (var i=0; i<id_selected.length; i++) 
							list_selected_id.push({rrn: id_selected[i].get('RRN'), cod :id_selected[i].get('TAKEY1') });
					
				    		voci_menu.push({
				         		text: 'Cancella',
				        		iconCls : 'icon-sub_red_delete-16',          		
				        		handler: function() {
				        		
				        		  Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'operazione?', function(btn, text){																							    
								   if (btn == 'yes'){					        		
				        		
									Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc',
											        method     : 'POST',
								        			jsonData: {
														list_selected_id : list_selected_id,
													},							        
											        success : function(response, opts){
											            jsonData = Ext.decode(response.responseText);
											             if (jsonData.success == false){
                    						            	acs_show_msg_error(jsonData.error_msg);
                    						            	return;
                    						            }else{
                    						              grid.getStore().load();	
                    						            
                    						            }
											        											       
												     												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });

									  }
									});

				        			
				        		}
				    		});
				    		
				    		<?php if($m_table_config['tasto_dx']['elenco_stringe'] == 'Y'){?>
				    		
				    			voci_menu.push({
				         		text: 'Elenco stringhe',
				        		iconCls : 'icon-search-16',          		
				        		handler: function() {
				        		
                                    var my_listeners = {						        			
		        					afterDuplica: function(from_win, rows){
		        					    
		        					    Ext.Ajax.request({
									        url: 'acs_anag_artd_elenco_stringhe.php?fn=exe_ins_dup',
									        jsonData: {
									        	rows : rows,
									        	or_row : rec.data, 
									        	indice : '<?php echo $m_table_config['cod_indice']; ?>'	,
									        	from_cl : '<?php echo $m_table_config['tasto_dx']['from_cl']; ?>'						        	
									         },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									        	jsonData = Ext.decode(result.responseText);
									        	
             						        	if(jsonData.error_msg != ''){
            	 						        	acs_show_msg_error(jsonData.error_msg);
            	 						        	return;
            	 						        }
             						        		
									            grid.getStore().load();    	  													        
									            from_win.close();	
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									            console.log('errorrrrr');
									        }
									    });	
    						            
    						            
						        	}
								}
								
				
        				
				        			acs_show_win_std('Parametri elenco stringhe', '../desk_utility/acs_anag_artd_elenco_stringhe.php?fn=open_codice', {
            	            		row : rec.data, from_cl : '<?php echo $m_table_config['tasto_dx']['from_cl']; ?>', distinta : '<?php echo $m_table_config['cod_indice']; ?>'	}, 
            	            		450, 220, my_listeners, 'icon-search-16');
            	            		
				        		}
				    		});
				    		
				    		<?php }?>
				    		
				    			<?php if($m_table_config['tasto_dx']['duplica'] == 'Y'){?>
				    		
				    			voci_menu.push({
				         		text: 'Duplica',
				        		iconCls : 'icon-button_black_play-16',          		
				        		handler: function() {
				        		
				        		var my_listeners = {						        			
		        					afterDup: function(from_win, jsonData){
		        					
		        					if(jsonData.error_msg != ''){
            	 						acs_show_msg_error(jsonData.error_msg);
            	 						return;
            	 				    }
		        					
		        					    grid.getStore().load();
		        					    from_win.close();
		        			
						        	}
								}
								
			        			acs_show_win_std('Duplica', '../desk_utility/acs_anag_artd_elenco_stringhe.php?fn=open_duplica', {
        	            		row : rec.data, from_cl : '<?php echo $m_table_config['tasto_dx']['from_cl']; ?>', 
        	            		indice : '<?php echo $m_table_config['cod_indice']; ?>'},
        	            		200, 180, my_listeners, 'icon-button_black_play-16');	
    						
            	            		
				        		}
				    		});
				    		
				    		voci_menu.push({
				         		text: 'Duplica multipla',
				        		iconCls : 'icon-button_black_play-16',          		
				        		handler: function() {
				        		
				        		list_rows = [];
				        		for (var i=0; i<id_selected.length; i++) 
							       list_rows.push(id_selected[i].data);
				        		
				        		var my_listeners = {						        			
		        					afterDup: function(from_win, jsonData){
		        					
		        					    from_win.close();
		        			
						        	}
								}
								
			        			acs_show_win_std('Duplica multipla', '../desk_utility/acs_anag_artd_duplica_multipla.php?fn=open_duplica', {
        	            		list_rows : list_rows, 
        	            		indice : '<?php echo $m_table_config['cod_indice']; ?>'},
        	            		900, 300, my_listeners, 'icon-button_black_play-16');
    						
            	            		
				        		}
				    		});
				    		
				    		
				    		
				    		<?php }?>
				
				
				    <?php if($m_table_config['tasto_dx']['lista'] == 'Y'){?>
				    	if(rec.get('CDCDLI').trim() != ''){	
			    			voci_menu.push({
			         		text: 'Configura lista',
			        		iconCls : 'icon-windows-16',          		
			        		handler: function() {
			        			//row : rec.data
			        			 acs_show_win_std('Lista opzioni', 'acs_anag_artd_config_distinta_lista.php?fn=open_tab', {lista : rec.get('CDCDLI')}, 950, 500, null, 'icon-windows-16');
			        		}
			    			});
				    	}	
				    <?php }?>
				    
				    
				    <?php if($m_table_config['tasto_dx']['config_filtri'] == 'Y'){?>
				    	
			    			voci_menu.push({
			         		text: 'Filtro colli/componenti',
			        		iconCls : 'icon-windows-16',          		
			        		handler: function() {
			        			acs_show_win_std(null,
			        				'acs_panel_indici_config_distinta_filtri.php?fn=open_tab', {
			        					lista : rec.get('chiave') //es: 1|LNET|N020||B020
			        				});
			        		}
			    			});
				    
				    <?php }?>
				    
				
				
					//eventuali voci di menu
					<?php if(isset($m_table_config['rec_menu'])){
					   foreach($m_table_config['rec_menu'] as $b){
					       echo "voci_menu.push(" . code_je($b) . ");";
        			   }    
					}?>
				
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	

	            } //itemcontextmenu
	         

	         }

			 ,viewConfig: {
	         getRowClass: function(record, index) {
	         //return ret;																
	         }   
	    },
			    
				
         
		dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
				{
		            xtype: 'form',
		            frame: true,
					layout: 'hbox',
					items: [		

						<?php m_gest_tataid_get_form_insert($m_table_config) ?>		
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								var search_form = this.up('form');
							    var m_grid = this.up('form').up('panel');
					            var form = this.up('form').getForm();
					            if(form.isValid()){	
					          	 loc_win = this.up('window');
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_insert',
											        method     : 'POST',
								        			jsonData: {
														form_values: search_form.getValues(),
														cod : '<?php echo $m_table_config['cod_indice']?>'
													},							        
											        success : function(response, opts){
											        	m_grid.getStore().load();
												
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		}
					            }
					     }
					     
					     
					   ]
					}    
					
					<?php if(isset($m_table_config['report'])){?>
					    ,{xtype: 'tbfill'}
					    ,{ xtype: 'button',
    					    text: 'Report',
    			            iconCls: 'icon-print-24',
    			            scale: 'medium',
    			            handler: function() {
    		                     window.open('acs_anag_artd_config_distinta_report.php?cod= <?php echo $m_table_config['cod_indice']?> &des= <?php echo $m_table_config['des_indice']?>'); 										           	                	     
                	         	}
        			       }
					<?php }?>
					
					//eventuali bottoni in fondo alla form
					<?php if(isset($m_table_config['buttons'])){
					   foreach($m_table_config['buttons'] as $b){
					       echo ",";
					       echo_je($b);  
        			   }    
					}?>
					
					
                ]
        }]         
         
	        																			  			
	            
        } //grid 

]
}
<?php } 


//function utility
function m_gest_tataid_get_fields($m_table_config){
	$r = array('TADT', 'TATAID', 'RRN', 'chiave');
	foreach ($m_table_config['fields'] as $kf=>$f)
		$r[] = $kf;
	return acs_je($r);
}


function m_gest_tataid_get_columns($m_table_config){
	foreach ($m_table_config['fields'] as $kf=>$f) {
		?>
			{
				header: <?php echo j($f['label'])?>,
			 	dataIndex: <?php echo j($kf)?>, 
			 	<?php echo isset($f['fw']) ? $f['fw'] : 'flex: 1' ?>,
			 	tooltip: <?php echo j(isset($f['tooltip']) ? $f['tooltip'] : $f['label']) ?>,
			 	filter: {type: 'string'}, filterable: true,
			 	
			 	<?php if($kf == 'CDTODO' && !isset($f['comboEditorData'])){ ?>
                  editor: {
	                xtype: 'combobox',
	                allowBlank: true,
	                valueField: 'id',
	                displayField: 'text',
	                	store: {
						fields: [{name:'id'}, {name:'text'}],
					    data: [								    
					    <?php 
					    
					    $desk_art = new DeskArt();
					    echo acs_ar_to_select_json($desk_art->find_TA_std('ATTAV', null, 'Y', 'N', null, null, 'ART'), ""); ?>	
						    ] 
					}
           	    } 
           	    <?php }elseif(isset($f['comboEditorData'])){?>
                  editor: {
	                xtype: 'combobox',
	                allowBlank: true,
	                valueField: 'id',
	                displayField: 'text',
	                	store: {
						fields: [{name:'id'}, {name:'text'}],
					    data: [<?php echo acs_ar_to_select_json($f['comboEditorData'], ""); ?>] 
					}
           	    }           	               	    
           	    <?php }elseif($kf == 'note'){?>
			 	   renderer: function(value, metaData, record, row, col, store, gridView){
	    			  if(record.get('note') > 0) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
	    			  else return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
	    		 }
	    		 <?php }elseif($f['type'] == 'NT'){?>
	    		   width: 40,
	    		   nt_config: <?php echo acs_je($f['nt_config'])?>,
			 	   renderer: function(value, metaData, record, row, col, store, gridView){
	    			  if(!Ext.isEmpty(value) && value > 0) return '<img src=<?php echo img_path("icone/48x48/comment_edit_yellow.png") ?> width=15>';
	    			  else return '<img src=<?php echo img_path("icone/48x48/comment_light.png") ?> width=15>';
	    		 }
			 	<?php }else{?>
    			 	editor: {
    				    xtype: 'textfield',
    				    allowBlank: true
    				}
			 	<?php } ?>
			 },		
		<?php
	}
}


function m_gest_tataid_get_next_num($m_table_config){
    global $conn, $id_ditta_default;
    
    if(isset($m_table_config['f_ditta']))
        $ditta = $m_table_config['f_ditta'];
    else
        $ditta = "TADT";
    
    $sql = "SELECT MAX({$m_table_config['field_IDPR']}) AS M_MAX
			FROM {$m_table_config['tab_name']} TA
			WHERE {$ditta} = '$id_ditta_default'";
		
	$stmt = db2_prepare($conn, $sql); 	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	$row = db2_fetch_assoc($stmt);	
	db2_free_stmt ( $stmt );
	$c = $row['M_MAX'];
	$c++;
	return $c;
}

function m_gest_tataid_get_form_insert($m_table_config){
	foreach ($m_table_config['fields_key'] as $kf) {
	    
	    switch($m_table_config['fields'][$kf]['type']){
	            case 'user':
	                get_user_combo($m_table_config, $kf);
	                break;
	            case 'from_TA':
	                get_from_TA($m_table_config, $kf);
	                break;
	            case 'numeric':
	                get_numberfield($m_table_config, $kf);
	                break;
	            default:
	                get_textfield($m_table_config, $kf);
	                break;
	        }
	}
	
	$f_desc = isset($m_table_config['f_desc']) ? $m_table_config['f_desc']: 'TADESC';
	?>
		{ xtype: 'textfield', name: '<?php echo $f_desc; ?>', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},	
	<?php
	
}

function get_textfield($m_table_config, $kf){
    
    if(isset($m_table_config['fields'][$kf]['maxLength']))
        $maxLen = $m_table_config['fields'][$kf]['maxLength'];
    else
        $maxLen = 10;
    
    ?>
				{ 
					xtype: 'textfield',
					name: <?php echo j($kf) ?>, 
					fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>, 
					width: '150', 
					labelWidth: 60, 
					maxLength: <?php echo $maxLen; ?>,
					margin: "0 10 0 0",
					listeners:{
                       change:function(field){
                            field.setValue(field.getValue().toUpperCase());
                       }
                   }
				},
		
		<?php
    
}

function get_numberfield($m_table_config, $kf){
    
    if(isset($m_table_config['fields'][$kf]['maxLength']))
        $maxLen = $m_table_config['fields'][$kf]['maxLength'];
        else
            $maxLen = 10;
            
            ?>
				{ 
					xtype: 'numberfield',
					name: <?php echo j($kf) ?>, 
					fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>, 
					width: '150', 
					labelWidth: 40, 
					hideTrigger : true,
					maxLength: 1,
					margin: "0 10 0 0",
			
				},
		
		<?php
    
}



function get_user_combo($m_table_config, $kf){
    
    $users = new Users;
    $ar_users = $users->find_all();
        
    foreach ($ar_users as $ku=>$u)
        $user_to[] = array(trim($u['UTCUTE']),  j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");
        
    $ar_users_json = acs_je($user_to);
            ?>
				 {
						xtype: 'combo',
						name: <?php echo j($kf) ?>,
						fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>,
						forceSelection: true,								
						displayField: 'descr',
			            valueField: 'cod',							
						emptyText: '- seleziona -',
				   		width: '150', 
						labelWidth: 40, 
						margin: "0 10 0 0",
						store: Ext.create('Ext.data.ArrayStore', {
			                fields: [ 'cod', 'descr' ],
			                data: <?php echo $ar_users_json ?>
			            }),
						typeAhead: true,
    					queryMode: 'local',                             
    					anyMatch: true, 
        				listeners   : {
							beforequery: function(record){  
							    record.query = new RegExp(record.query, 'ig');
    							record.forceAll = true;
							}
						}			
						 },
		
		<?php
    
}


function get_from_TA($m_table_config, $kf){
    global $s, $desk_art; ?>
				 {
						xtype: 'combo',
						name: <?php echo j($kf) ?>,
						fieldLabel: <?php echo j($m_table_config['fields'][$kf]['label'])?>,
						width: '150', 
						labelWidth: 40, 
						margin: "0 10 0 0",
						forceSelection: true,								
						displayField: 'text',
						valueField: 'id',							
						emptyText: '- seleziona -',
				   		store: {
							editable: false,
							autoDestroy: true,
						    fields: [{name:'id'}, {name:'text'}],
						    data: [			
						    <?php if($m_table_config['from_art'] == 'Y')
						              echo acs_ar_to_select_json($desk_art->find_TA_std($m_table_config['fields'][$kf]['TAID']), '');
						         else 
						              echo acs_ar_to_select_json($s->find_TA_std($m_table_config['fields'][$kf]['TAID']), '');
						         
						      ?> 
    
						    ]
						}
								
						 } ,
		
		<?php
    
}
