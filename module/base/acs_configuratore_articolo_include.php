<?php

function insert_TC_row($idsc, $m_params){
    
    global $cfg_mod_DeskArt, $id_ditta_default, $conn, $auth;
        
    $ar_ins = array();
    $ar_ins['TCUSES'] 	= substr($auth->get_user(), 0, 8);
    $ar_ins['TCDTES']   = oggi_AS_date();
    $ar_ins['TCHMES'] 	= oggi_AS_time();
    $ar_ins['TCDT'] 	= $id_ditta_default;
    $ar_ins['TCSESS'] 	= $idsc;
    
    if(isset($m_params->k_ordine) && strlen($m_params->k_ordine) > 0){
        $ar = explode("_", $m_params->k_ordine);
        $ar_ins['TCTIDO'] 	= $ar[1];
        $ar_ins['TCINUM'] 	= $ar[2];
        $ar_ins['TCAADO'] 	= $ar[3];
        $ar_ins['TCNRDO'] 	= $ar[4];
        $ar_ins['TCNREC'] 	= $m_params->nrec;
        
    }else{
        $ar_ins['TCCDAR'] 	= $m_params->c_art;
        
        $row_art = get_AR_row($m_params->c_art);
        $dim1 = $row_art['ARDIM1'];
        $dim2 = $row_art['ARDIM2'];
        $dim3 = $row_art['ARDIM3'];
        
        $ar_ins['TCDIM1'] 	= sql_f($dim1);
        $ar_ins['TCDIM2'] 	= sql_f($dim2);
        $ar_ins['TCDIM3'] 	= sql_f($dim3);
    }
     
    $sql_ins = "INSERT INTO {$cfg_mod_DeskArt['elab_configuratore']['file_config_testata']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
    
    $stmt_ins = db2_prepare($conn, $sql_ins);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_ins, $ar_ins);
    echo db2_stmt_errormsg($stmt_ins);
}

function update_RC_row($values){
    
    global $cfg_mod_DeskArt, $id_ditta_default, $conn, $auth;
    
    $ar_ins = array();
    $ar_ins['RCUSES'] 	= substr($auth->get_user(), 0, 8);
    $ar_ins['RCDTES']   = oggi_AS_date();
    $ar_ins['RCHMES'] 	= oggi_AS_time();
    
    foreach($values as $k =>$v){
        
        $rrn = $k;
        $stmt_rc =  get_RC_stmt($rrn);
        $row_rc = db2_fetch_assoc($stmt_rc);
        
        $row_ta = get_TA_sys('PUVR', trim($row_rc['RCVARI']), null, null, null, null, 1);
        $numerica = $row_ta['tarest'];
        
        if($numerica == 'N')
            $ar_ins['RCVANN'] 	= $v;
        else
            $ar_ins['RCVANA'] 	= $v;
        
        
                
        $sql_ins = "UPDATE {$cfg_mod_DeskArt['elab_configuratore']['file_config_righe']} RC
                    SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
                    WHERE RRN(RC) = {$rrn}";
   
                
        $stmt_ins = db2_prepare($conn, $sql_ins);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_ins, $ar_ins);
        echo db2_stmt_errormsg($stmt_ins);
                
    }
}

function write_RI_elab_config($idsc, $m_params){
    
    global $id_ditta_default;
    
    $deskArt = new DeskArt(array('no_verify' => 'Y'));
    
    if(isset($m_params->k_ordine) && strlen($m_params->k_ordine) > 0){
        $k_ordine = $m_params->k_ordine;
        $nrec = $m_params->nrec;
    }else{
        $k_ordine = '';
        $nrec = 0;
    }
    
    
    //eseguo prima elaborazione (RI0)
    $sh = new SpedHistoryConfigArt($deskArt);
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ELAB_CONFIG',
            "k_ordine"	=> $k_ordine,
            "vals" => array(
                "RIDT"  	=> $id_ditta_default,
                "RISESS"  	=> $idsc,
                "RICDAR"  	=> $m_params->c_art,
                "RINREC"   => $nrec
            ),
        )
        );
    
    if($ret_RI['RIESIT'] == 'W'){
        $errore = $ret_RI['RINOTE'];
        return;
    }
    
    return $ret_RI;
    
}


function get_AR_row($articolo){
    
    global $cfg_mod_DeskUtility, $id_ditta_default, $conn;
    
    $sql = "SELECT *
    FROM {$cfg_mod_DeskUtility['file_anag_art']} AR
    WHERE ARDT = '{$id_ditta_default}' AND ARART = '{$articolo}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row_art = db2_fetch_assoc($stmt);
    
    return $row_art;
}


function get_TC_row($idsc){
    
    global $cfg_mod_DeskArt, $id_ditta_default, $conn;
    
    $sql_tc = "SELECT TCPAGI, TCFINE
    FROM {$cfg_mod_DeskArt['elab_configuratore']['file_config_testata']} TC
    WHERE TCDT = '{$id_ditta_default}' AND TCSESS = '{$idsc}'";
    
    $stmt_tc = db2_prepare($conn, $sql_tc);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_tc);
    $row_tc = db2_fetch_assoc($stmt_tc);
    
    return $row_tc;
}

function get_RC_stmt($rrn = 0, $idsc = 0, $pagina = 0){
    
    global $cfg_mod_DeskArt, $id_ditta_default, $conn;
    
    if($rrn > 0)
        $sql_where = " AND RRN(RC) = '{$rrn}'";
    else
        $sql_where = " AND RCSESS = '{$idsc}' AND RCPAGI = {$pagina}";
    
    $sql_rc = "SELECT RRN(RC) AS RRN, RCVARI, RCVANN, RCVANA, RCMODE, RCSEQ3, RCSEQ4, RCTIPO
               FROM {$cfg_mod_DeskArt['elab_configuratore']['file_config_righe']} RC
               WHERE RCDT = '{$id_ditta_default}' {$sql_where}";
    
    $stmt_rc = db2_prepare($conn, $sql_rc);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_rc);
   
    if (!$result)
        echo db2_stmt_errormsg($stmt_rc);
        
        return $stmt_rc;
}


function get_valori_ammessi($variabile, $configuratore, $sequenza){
    
    global $cfg_mod_DeskUtility, $id_ditta_default, $conn;
    
    $ret = array();
    
    $sql = "SELECT MOVALO, MOVARI, MDVAM1, MDVAM5
            FROM {$cfg_mod_DeskUtility['file_valori_ammessi']} MO
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_config']} MD
            ON MO.MODT=MD.MDDT AND MO.MOMODE=MD.MDMODE AND MO.MOSEQU = CONCAT (MD.MDSEQ3, MD.MDSEQ4) AND MO.MOVARI=MD.MDVARI AND MDSEQC='999'
            LEFT OUTER JOIN {$cfg_mod_DeskUtility['file_tab_sys']} TA
            ON TA.TADT = MO.MODT AND TA.TAID = 'PUVN' AND TA.TANR = MO.MOVALO AND TA.TACOR2 = MO.MOVARI AND TA.TALINV = ''
            WHERE MODT = '{$id_ditta_default}' AND MOMODE = '{$configuratore}' AND MOSEQU='{$sequenza}' AND MOVARI = '{$variabile}'
            AND (MDVAM1 = '' OR MOVALO >= MDVAM1)
            AND (MDVAM5 = '' OR MOVALO <= MDVAM5)
            ORDER BY MOVALO";
  
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    
    while($row = db2_fetch_assoc($stmt)){
        
        $val_risp = get_TA_sys('PUVN', rtrim($row['MOVALO']), null, rtrim($row['MOVARI']), null, null, 1, '', 'N', 'Y');
        
        if($val_risp['sosp'] == true){
            $nr['sosp'] = 'S';
            $text = str_replace('(*S*)', '', $val_risp['text']);
        }else{
            $text = $val_risp['text'];
        }
        
        $rr = array( "id" 	=> rtrim($row['MOVALO']),
            "text"	=> "[".trim($row['MOVALO'])."] ".$text,
            
        );
        
        
        $ret[] = $rr;
        
    }
    
    return $ret;
    
}

function check_allowBlank($variabile, $configuratore, $seq3, $seq4){
    
    global $cfg_mod_DeskUtility, $id_ditta_default, $conn;
    
    $sql = "SELECT MDVAM1, MDVAM5
    FROM {$cfg_mod_DeskUtility['file_config']} MD
    WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$configuratore}' AND MDSEQC='999'
    AND MDVARI = '{$variabile}' AND MDSEQ3 = '{$seq3}' AND MDSEQ4 = '{$seq4}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    if(trim($row['MDVAM1']) == '' && trim($row['MDVAM5']) == '')
        return 'true';
        else
            return 'false';
            
}

function get_new_page($idsc, $pagina){
    
    global $cfg_mod_DeskArt, $id_ditta_default, $conn;
    
    $c = new Extjs_Form(layout_ar('vbox'), "Pagina {$pagina}");
    
    $sql_rc = "SELECT RRN(RC) AS RRN, RCVARI, RCVANN, RCVANA, RCMODE, RCSEQ3, RCSEQ4, RCTIPO
    FROM {$cfg_mod_DeskArt['elab_configuratore']['file_config_righe']} RC
    WHERE RCDT = '{$id_ditta_default}' AND RCSESS = '{$idsc}' AND RCPAGI = {$pagina}";
    
    $stmt_rc = db2_prepare($conn, $sql_rc);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_rc);
    while($row_rc = db2_fetch_assoc($stmt_rc)){
        
        if($row_rc['RCTIPO'] != 'S'){
            $row_ta = get_TA_sys('PUVR', trim($row_rc['RCVARI']), null, null, null, null, 1);
            $numerica = $row_ta['tarest'];
            $sequenza = $row_rc['RCSEQ3'].$row_rc['RCSEQ4'];
            $variabile = trim($row_rc['RCVARI']);
            $configuratore = trim($row_rc['RCMODE']);
            $allowBlank = check_allowBlank(trim($row_rc['RCVARI']), trim($row_rc['RCMODE']), trim($row_rc['RCSEQ3']), trim($row_rc['RCSEQ4']));
            
            if($numerica == 'N'){
                $ar_valid = get_valid_number($configuratore, $sequenza, $variabile);
                $c->add_items(array(
                    array(
                        'xtype' => 'numberfield',
                        'name'=>   $row_rc['RRN'],
                        'labelWidth' => 180,
                        'fieldLabel' => trim($row_ta['text']),
                        'value' => trim($row_rc['RCVANN']),
                        'allowBlank' => $allowBlank,
                        'hideTrigger' => true,
                        'minValue' => $ar_valid['MDVAM1'],
                        'maxValue' => $ar_valid['MDVAM5'],
                    )));
                
            }else{
                
                $ar_varianti = get_valori_ammessi($variabile, $configuratore, $sequenza);
                if(count($ar_varianti) == 0)
                    $ar_varianti = find_TA_sys('PUVN', null, null, $variabile, null, null, 0, '', 'Y', 'N', 'R');
                    
                $c->add_items(array(
                    
                    extjs_combo(array(
                        'fieldLabel' =>  trim($row_ta['text']),
                        'labelWidth' => 180,
                        'name' => $row_rc['RRN'],
                        //'value' => trim($row_rc['RCVANA']),
                        'fieldLabel' => trim($row_ta['text']),
                        'initialData'  => $ar_varianti,
                        'initialValue' => rtrim($row_rc['RCVANA']),
                        'queryMode'     => 'local',
                        'allowBlank'    => $allowBlank
                    ))
                    
                    ));
                
                                   
            }
        }
        
    }
    
    
    return $c->code();
    
}

function get_valid_number($configuratore, $sequenza, $variabile){
    
    global $id_ditta_default, $cfg_mod_DeskUtility, $conn;
    
    $sql_md = "SELECT MDVAM1, MDVAM5
    FROM {$cfg_mod_DeskUtility['file_config']} MD
    WHERE MDDT = '{$id_ditta_default}' AND MDMODE = '{$configuratore}' AND CONCAT (MD.MDSEQ3, MD.MDSEQ4)='{$sequenza}' AND MDVARI = '{$variabile}'
    AND MDSEQC = 999";
    
    $stmt_md = db2_prepare($conn, $sql_md);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_md);
    $row_md = db2_fetch_assoc($stmt_md);
    
    return $row_md;
}



function get_dom_ris_by_idsc($idsc){
    global $conn, $id_ditta_default, $cfg_mod_DeskArt;
    $ret = array();
    $ar_values = array();
    
    $sql= "SELECT RCVARI, RCVANN, RCVANA, RCTIPO
               FROM {$cfg_mod_DeskArt['elab_configuratore']['file_config_righe']} RC
               WHERE RCDT = '{$id_ditta_default}' AND RCSESS = '{$idsc}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while($row = db2_fetch_assoc($stmt)){
        
        //creare la combo o la numberfield e sistemare l'array
        $row_ta = get_TA_sys('PUVR', trim($row['RCVARI']), null, null, null, null, 1);
        $numerica = $row_ta['tarest'];
        //if(trim($row['RCTIPO']) != 'S'){
        $variabile = trim($row['RCVARI']);
        if($numerica == 'N')
            $variante = $row['RCVANN'];
        else
            $variante = trim($row['RCVANA']);
                
        $ar_values[$variabile] = $variante;
    } //while
    
    return $ar_values;
}