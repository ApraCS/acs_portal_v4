<?php

require_once "../../config.inc.php";
$mod_base = new Base();
$m_params = acs_m_params_json_decode();

$s = new Spedizioni(array('no_verify' => 'Y'));




// ******************************************************************************************
// SEND EMAIL
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_send_email'){
	require_once('utility/PHPMailer-master/PHPMailerAutoload.php');
	
	    $mail = prepare_new_mail();
	    $from = trim($auth->get_email());
	
		//Gestione indirizzi multipli, seprarati da ";"
		foreach (explode(';', $m_params->form_values->to) as $addr){
			if (strlen(trim($addr)) > 0)
				$mail->addAddress(trim($addr));
		}
		foreach (explode(';', $m_params->form_values->to_cc) as $addr){
			if (strlen(trim($addr)) > 0)
				$mail->AddCC(trim($addr));
		}
		foreach (explode(';', $m_params->form_values->to_ccn) as $addr){
			if (strlen(trim($addr)) > 0)
				$mail->AddBCC(trim($addr));
		}

	
		$mail->Subject = $m_params->form_values->f_oggetto;
		$mail->ConfirmReadingTo = $from;
		
		$body = $m_params->form_values->f_text;
	
		$mail->CharSet = 'UTF-8';
		$mail->Body = $body;
		$mail->AltBody = 'Questa applicazione mail non supporta il linguaggio html';
		
		if (is_array($m_params->attachments)){
			foreach($m_params->attachments as $a)
				$mail->AddAttachment($a->path, $a->display_name);
		}

	
		$ret = array();
		if(!$mail->send()){
			$ret['success'] = false;
			$ret['error_message'] = 'Errore: ' . $mail->ErrorInfo;
		}else
			$ret['success'] = true;
	
	   echo acs_je($ret);	
	exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid_rows'){
    
    $nr = array();
    $ar = array();
    
    $m_params = acs_m_params_json_decode();
    $oe = $s->k_ordine_td_decode_xx($m_params->open_request->k_ordine);
    $ord = $s->get_ordine_by_k_docu($m_params->open_request->k_ordine);
    
    $sql = "SELECT RRN(RA) AS RRN, RAALOR
    FROM {$cfg_mod_DeskPVen['file_allegati_righe']} RA
    WHERE RANREC = '{$m_params->open_request->row->num_ord}' AND RADT = ? AND RATIDO = ? AND RAINUM = ? AND RAAADO = ? AND RANRDO = ? ";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $oe);
    
    while($row = db2_fetch_assoc($stmt)){
        
        
        $nr['display_name'] = trim($row['RAALOR']);        
        //$nr['path']  = "/SV2/ORDINI/".$ord['TDCCON']."/".trim($row['RAALOR']);
        $nr['path']  = $cfg_mod_DeskPVen['allegati_root_C'].$ord['TDCCON']."/".trim($row['RAALOR']);
        
        
        $ar[] = $nr;
    }
    
    echo acs_je($ar);
    exit;
}

if ($_REQUEST['fn'] == 'open_email'){
    
    if($m_params->from_righe != 'Y'){
   	
    	$attachments= array();
    	
    	foreach($m_params->list_selected_id as $v){
    	
    		$attachments[]= $v->attachments;
    	}

    }	

	

?>{"success":true, "items": [

        {
            xtype: 'form',
            frame: true,
            title: '',
          	bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			                        
            
		items: [					{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						//fieldcontainer con le righe
						
						
						  { 
						xtype: 'fieldcontainer',
						flex : 3, 
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						{
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: []
            }),
            <?php if($m_params->from_righe == 'Y'){?>
            	value: '<?php echo $m_params->row->email; ?>',
            <?php }else{?>
            	value: '',
            <?php }?>
            displayField: 'descr',
            valueField: 'email',
            fieldLabel: 'A',
            itemId: 'email_to',
            queryMode: 'local',
            selectOnTab: false,
            name: 'to'
        }, {
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: []
            }),
            displayField: 'descr',
            value: '',
            valueField: 'email',
            fieldLabel: 'Cc',
            queryMode: 'local',
            selectOnTab: false,
            name: 'to_cc',
            itemId: 'email_to_cc',
        },{
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: []
            }),
            displayField: 'descr',
            valueField: 'email',
            fieldLabel: 'Ccn',
            queryMode: 'local',
            selectOnTab: false,
            name: 'to_ccn',
            itemId: 'email_to_ccn',
            value: '<?php echo $auth->get_email(); ?>'
        }, {
            xtype: 'textfield',
            name: 'f_oggetto',
            fieldLabel: 'Oggetto',
            anchor: '96%',
		    allowBlank: false,
		    value: <?php echo j($m_params->subject)?>	    
        }
						
						
						]
					 } 
							
							
	//GRID ALLEGATI
		, {
				              
            xtype: 'grid', //panel 2
            autoScroll: true,
            flex : 1, 
            height: 130,
            margin: '0 0 5 10',
		 	itemId: 'grid_attachments',
		 	<?php if($m_params->from_righe == 'Y'){?>
		 	store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid_rows', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: [{name:'path'}, {name:'display_name'}]							
									
			}, //store
		 	
		 	<?php }else{?>
			store: {
				autoLoad: true,
				editable: false,
				autoDestroy: true,	 
			    fields: [{name:'path'}, {name:'display_name'}],
			    
			
			    data:<?php echo acs_je(to_grid_allegati(($attachments))) ?>
		
			},
			<?php }?>
			
			  columns: [	
			
	            {
	                header   : 'Allegati',
	                dataIndex: 'display_name',
	                flex: 1
	            }

	      
				],
	            
			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                <?php if($m_params->k_ordine  != ''){  ?>
					{
						xtype: 'button',
						text: 'Aggiungi da cartella cliente',
						anchor: '96%',
						scale: 'small',
						iconCls: 'icon-sub_blue_add-16',
						handler: function() {
						
						var m_grid = this.up('grid');
						
						var my_listeners = {
        					onRecSelected: function(rec, from_win){	
        						//dopo la selezione di un allegato				        						
        							m_grid.getStore().add({
        								path: rec.get('IDOggetto'), 
        								display_name: rec.get('des_oggetto')});
        						from_win.close();		
				        		}
		    				};							
						
							acs_show_win_std('Aggiungi', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_allegati', 
								{k_ordine: '<?php echo $m_params->k_ordine; ?>'} , 600, 200, my_listeners, 'icon-search-16');						
						} //handler		
					},
					<?php }?>
					 {
						xtype: 'button',
						text: 'Carica da PC',
						anchor: '96%',
						scale: 'small',
						iconCls: 'icon-sub_blue_add-16',
						handler: function() {
						
							var m_grid = this.up('grid');
							
							acs_show_win_std('Upload', 'acs_upload.php?fn=open_form', {save_in_tmp: 'Y'} , 600, 200, {
	         		            		afterUpload: function(from_win, data){
  										m_grid.getStore().add({
	        								path: data.tmp_path, 
	        								//display_name: data.upload_file.name
    	    								display_name: data.new_name});
	        		            		from_win.close();
	             		            	}
	         		            	});

						
						} //handler		
					}                 
                ]
              }
             ],
             
             
             viewConfig : {
               listeners : {
                   'itemkeydown' : function(view, record, item, index, key) {
                       if (key.getKey() == 46) {//the delete button
                         record.store.remove(record);
                       }  
                   }
               }
             }	       
	            
				               
		} //grid allegati	
						
						]
					 }
        
      
        
        , {
            xtype: 'htmleditor',
            //xtype: 'textareafield',
            name: 'f_text',
            fieldLabel: 'Testo del messaggio',
            layout: 'fit',
            anchor: '96%',
            height: '100%',
		    allowBlank: false,
		    value: '',
		    flex: 1, //riempie tutto lo spazio
			hideLabel: true,
            style: 'margin:0' // Remove default margin		              
        }] ,
            
            
		dockedItems: [{
		    xtype: 'toolbar',
		    dock: 'top',
	        items: [{
	            text: 'Invia',
	            scale: 'large',
	            iconCls: 'icon-email_send-32',
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	var m_win = this.up('window');
		            	
		            	var ar_attachments = new Array();
						m_win.down('#grid_attachments').getStore().each(function(record) {
						    ar_attachments.push(record.data);
						});		            	
		            	
						if(form.isValid()){
						Ext.getBody().mask('Invio in corso... ', 'invio').show();
							Ext.Ajax.request({
							        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_send_email',
							        jsonData: {
							        	form_values: form.getValues(),
							        	attachments: ar_attachments
							        },	
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							         Ext.getBody().unmask();
							        	var jsonData = Ext.decode(result.responseText);
							        	if (jsonData.success == true){
											m_win.close();		        
											acs_show_msg_info('Invio email completato');
										} else {
											Ext.Msg.alert('Errore in fase di invio', jsonData.error_message);
										}	
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });						
					    }	            	
		            	
		            		            	
	
			            //this.up('window').close();            	                	                
		             }            
	        },'->', {
	            text: 'Rubrica',
	            scale: 'large',
	            iconCls: 'icon-address_book-32',
		            handler: function() {
		            
		            	var form = this.up('form').getForm();
		            	var m_win = this.up('window');
		            
		            var my_listeners = {
        					onEmaSelected: function(ar_rec, tipologia, from_win){	
        					
        					 	if (tipologia == 'TO')
        					 	 m_text = m_win.down('#email_to');
        					 	 if (tipologia == 'TO_CC')
        					 	 m_text = m_win.down('#email_to_cc');
        					 	 if (tipologia == 'TO_CCN')
        					 	 m_text = m_win.down('#email_to_ccn');
        				
        					 	for (var chiave in ar_rec) {
        					 	
        					 	var value = m_text.getValue().trim();
        					 	
        					 	if(value == '')
        					 		m_text.setValue(ar_rec[chiave].email);
        					 	else
        					 		m_text.setValue(value + ' ; ' + ar_rec[chiave].email);
        					 
        					 	}
        						
        						from_win.close();		
				        		}
		    				};
		            
		            	acs_show_win_std('Rubrica', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_address', {k_ordine : '<?php echo $m_params->k_ordine; ?>'}, 600, 500, my_listeners, 'icon-address_book-16');	
			                     	                	                
		             }            
	        }]  		    

		}]            
            
            
         
				
        }
]}


<?php
exit;
} 

if ($_REQUEST['fn'] == 'open_allegati'){
	
	
	?>
	
	
	{"success":true, "items": [
	
	{
	                     
			 xtype: 'grid',
			 features: [
		        new Ext.create('Ext.grid.feature.Grouping',{
			   groupHeaderTpl: '{name}',
        	   hideGroupedHeader: true
		      })],
					/*tbar: new Ext.Toolbar({
     		            items:[
     		            	'->', '<b>Upload</b>', 
     		            {iconCls: 'icon-search-16', 
         		            handler: function(event, toolEl, panel){
             		            k_ordine=this.up('grid').store.proxy.extraParams.k_ordine;
             		            var m_grid = this.up('grid');
         		            	acs_show_win_std('Upload', 'acs_upload.php?fn=open_form', {k_ordine: k_ordine} , 600, 200, {
         		            		afterUpload: function(from_win){
         		            			m_grid.store.load();
         		            			from_win.close()
             		            	}
         		            	}, 'icon-search-16');

             		            }}
     		       		
     		         ]        
     		        }),*/   
               		 store: {
						xtype: 'store',
						autoLoad: true,				        
               	  		proxy: {
               							url: 'acs_get_order_images.php?k_ordine=<?php echo $m_params->k_ordine; ?>',
               							type: 'ajax',
               							reader: {
               						      type: 'json',
               						      root: 'root'
               						     },
               						     extraParams: {
               		    		    		
               		    				}               						        
				        
               						},
               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
               		     	groupField: 'tipo_scheda',               		     	
               			},
    		     			
                  						    						
                     columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],
			             
			                 listeners: {
				           
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  var loc_win = this.up('window');						  
							  var rec = iView.getRecord(iRowEl);							  
							 //fireEvent
							  loc_win.fireEvent('onRecSelected', rec, loc_win);
						  }
			   		  }

				
				         
			         } 

			        
	
	}
	

	
	]}
	
	<?php 
exit;	

}

if ($_REQUEST['fn'] == 'get_json_data_grid'){
	
	if($m_params->tipologia == 'vettori'){
		
		$vettori = new Vettori;
		$ar_vett = $vettori->find_all();
		
		foreach ($ar_vett as $ku=>$u){
			$nr = array();
			$nr['descr'] = trim($u['TADESC']);
			$nr['email'] = trim($u['TAMAIL']);
			$ar[] = $nr;
		}
		
	}elseif($m_params->tipologia == 'aziende'){
		
		$aziende = new Trasportatori;
		$ar_aziende = $aziende->find_all();
		
		foreach ($ar_aziende as $ku=>$u){
			$nr = array();
			$nr['descr'] = trim($u['TADESC']);
			$nr['email'] = trim($u['TAMAIL']);
			$ar[] = $nr;
		}
		
	}elseif($m_params->tipologia == 'cliente'){
		
		$r = $s->get_ordine_by_k_ordine($m_params->k_ordine);
		
			$nr = array();
			$nr['descr'] = trim($r['TDDCON']);
			$nr['email'] = trim($r['TDMAIL']);
			$ar[] = $nr;
		
		
	}else{
		
		$users = new Users;
		$ar_users = $users->find_all();
		
		foreach ($ar_users as $ku=>$u){
			$nr = array();
			$nr['descr'] = trim($u['UTDESC']); 
			$nr['email'] = trim($u['UTMAIL']);
			$ar[] = $nr;
		}
	
	}
	
	
	echo acs_je($ar);
	 exit;
	
}

if ($_REQUEST['fn'] == 'open_address'){
	
	$m_params = acs_m_params_json_decode();
	

	
	?>
	
	
	{"success":true, "items": [
	
	{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						     {
							 xtype: 'fieldcontainer',
							 flex : 1,	
							 layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
							{
						     	xtype: 'button',
					            text: 'Utenti',
					            scale: 'medium',
						            handler: function() {
						            	grid = this.up('window').down('grid');
						            	grid.store.proxy.extraParams.tipologia = 'user';
						            	grid.store.load();        	                	                
						             }            
					        },{
						     	xtype: 'button',
					            text: 'Vettori',
					            scale: 'medium',
						            handler: function() {
						            	grid = this.up('window').down('grid');
						            	grid.store.proxy.extraParams.tipologia = 'vettori';
						            	grid.store.load();
							                     	                	                
						             }            
					        }, {
						     	xtype: 'button',
					            text: 'Aziende',
					            scale: 'medium',
					            //iconCls: 'icon-address_book-32',
						            handler: function() {
						            	grid = this.up('window').down('grid');
						            	grid.store.proxy.extraParams.tipologia = 'aziende';
						            	grid.store.load();
							          	                	                
						             }            
					        },
					        {
						     	xtype: 'button',
					            text: 'Cliente',
					            scale: 'medium',
					            //iconCls: 'icon-address_book-32',
						            handler: function() {
						            	grid = this.up('window').down('grid');
						            	grid.store.proxy.extraParams.tipologia = 'cliente';
						            	grid.store.load();
							          	                	                
						             }            
					        }
						
						]}, 
							
						{
            xtype: 'grid',
            flex:3,
            multiSelect: true,
            features: [
							{
							ftype: 'filters',
							encode: false, 
							local: true,   
						    filters: [
						       {
						 	type: 'boolean',
							dataIndex: 'visible'
						     }
						      ]
						}],
            store: {
					xtype: 'store',
					autoLoad:true,
		
  							proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
					   		method: 'POST',								
					   		type: 'ajax',

					       actionMethods: {
					          read: 'POST'
					        },
				        
				        
				           extraParams: {
								tipologia: '',
								k_ordine : '<?php echo $m_params->k_ordine; ?>'
	        				},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
					
        			fields: ['descr', 'email']							
									
			}, //store
				
	        columns: [	
				{
	                header   : 'Descrizione',
	                dataIndex: 'descr',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	            }, {
	                header   : 'Email',
	                dataIndex: 'email',
	                flex: 1,
	                filter: {type: 'string'}, filterable: true
	            }
	         ]
	         
	         , listeners: {
	         
	           celldblclick: {
	           
		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
            		 var loc_win = this.up('window');
            		 var rec = iView.getRecord(iRowEl);					  
					 //fireEvent
					  loc_win.fireEvent('onEmaSelected', rec.data, loc_win);
	           
	            	}
	          
	           	  }
	           }
	           ,dockedItems: [{
			    xtype: 'toolbar',
			    dock: 'bottom',
			    fixed: true,
		        items: [ '->',
					{
					xtype: 'button',
		            text: 'A',
		            scale: 'small',
			        handler: function(){
			            
			            var grid = this.up('grid');
                        var loc_win = this.up('window');
                        var type = 'TO';
                        
                        email_selected = grid.getSelectionModel().getSelection();
                        list_email_selected = [];
                            
                              for (var i=0; i<email_selected.length; i++) {
						            list_email_selected.push({
						            	email: email_selected[i].get('email')
						            	});
				            	}
							loc_win.fireEvent('onEmaSelected', list_email_selected, type, loc_win);
								  
		             }            
	        		},{
	        		xtype: 'button',
		            text: 'Cc',
		            scale: 'small',
			        handler: function(){
			        
			            var grid = this.up('grid');
                        var loc_win = this.up('window');
                         var type = 'TO_CC';
                        
                        email_selected = grid.getSelectionModel().getSelection();
                        list_email_selected = [];
                            
                              for (var i=0; i<email_selected.length; i++) {
						            list_email_selected.push({
						            	email: email_selected[i].get('email')
						            	});
				            	}
							loc_win.fireEvent('onEmaSelected', list_email_selected, type, loc_win);
		             }            
	        		},{
	        		xtype: 'button',
		            text: 'Ccn',
		            scale: 'small',
			        handler: function(){
			           var grid = this.up('grid');
                       var loc_win = this.up('window');  
                        var type = 'TO_CCN';  
                       email_selected = grid.getSelectionModel().getSelection();
                       list_email_selected = [];
                            
                              for (var i=0; i<email_selected.length; i++) {
						            list_email_selected.push({
						            	email: email_selected[i].get('email')
						            	});
				            	}
							loc_win.fireEvent('onEmaSelected', list_email_selected, type, loc_win);
		             }            
	        		}
			]  		    

		}]   																			  			
	            
        }  //grid  
					
						]}
	
	
	]}
	
	<?php 
	exit;
}


function to_grid_allegati($ar_att = array()){
	$ret = array();
	
	foreach($ar_att as $ar_path){
	
		foreach($ar_path as $p){
					
			$nome_file_ar = explode('/', $p);
			$nome_file = end($nome_file_ar);
			
			$ret[] = array('path' => $p, 'display_name' => $nome_file);
		}
	
	}
	return $ret;	
}

