{
    xtype : 'form',
    title : 'Intestazione',
    frame: true,
    items : [  
        {
        	xtype: 'fieldcontainer',
        	layout: {type: 'hbox', pack: 'start', align: 'stretch'},
        	anchor: '-10',
        	defaults:{xtype: 'textfield'},
        	items: []
        },
        {
			xtype: 'fieldset', title: 'Intestazione Ordine',
			layout: {type: 'vbox', pack: 'start', align: 'stretch'},
			anchor: '-10',
			defaults:{ labelWidth: 120, xtype: 'textfield'},
			items: [
    			{
                    xtype: 'fieldcontainer',
                    layout: {type: 'vbox', pack: 'start', align: 'stretch'},
                    anchor: '-10',
                    items: [{
                        xtype: 'combo',
            			name: 'f_cliente_cod',
            			fieldLabel: 'Cliente',
            			minChars: 2,
            			allowBlank: false,
						value: '<?php echo trim($ord['TDCCON']); ?>',
                        displayField: 'descr',
                        typeAhead: false,
                        hideTrigger: true,
                        anchor: '100%'
                	},  {
             			xtype: 'textfield', disabled: true,
            			fieldLabel: 'Indirizzo',
             			name: 'out_ind'
             		}, {
             			xtype: 'textfield', disabled: true,
            			fieldLabel: 'Localit&agrave;',
             			name: 'out_loc'
             		}, {
                        xtype: 'combo',
            			name: 'f_destinazione_cod',
            			fieldLabel: 'Destinazione',
            			minChars: 2,
            			allowBlank: true,
                        value: '<?php echo trim($ord['TDDEST']); ?>',
            			valueField: 'cod',
                        displayField: 'denom',
                        typeAhead: false,
                        hideTrigger: false, //mostro la freccetta di selezione
                        anchor: '100%'
                    }, {
             			xtype: 'textfield', disabled: true,
            			fieldLabel: 'Indirizzo',
             			name: 'out_ind_dest'
             		}, {
             			xtype: 'textfield', disabled: true,
            			fieldLabel: 'Localit&agrave;',
             			name: 'out_loc_dest'
         			}, {
        				name: 'divisione',
        				xtype: 'combo',
                    	anchor: '100%',
        				fieldLabel: 'Divisione',
        				displayField: 'text',
        				valueField: 'id',
        				forceSelection: true,
        			   	allowBlank: false,
        			   	hideTrigger: false,
        			   	hidden: true
        			   	//, value: '<?php echo trim($ord['']); ?>'
					}, {
        				name: 'tpdo',
        				xtype: 'combo',
                    	anchor: '100%',
        				fieldLabel: 'Tipo ordine',
        				displayField: 'text',
        				valueField: 'id',
        				forceSelection: true,
        			    allowBlank: false,
                        queryMode: 'local',
                        hideTrigger: false,
			     		value: '<?php echo trim($ord['TDTPDO']); ?>'
        			}, {
    				     name: 'vsrf'
    				   , xtype: 'textfield'
    				   , fieldLabel: 'Riferimento'
    				   , maxLength: 30
    				   , hideTrigger: true
    				   , value: '<?php echo trim($ord['TDVSRF']); ?>'
        			}, {
            			name: 'mode',
        				xtype: 'combo',
                    	anchor: '100%',
        				fieldLabel: 'Modello',
        				displayField: 'text',
        				valueField: 'id',
        				forceSelection: true,
        			   	allowBlank: false,
                        queryMode: 'local',
        				minChars: 1,
        				hideTrigger: false,
        				hidden: true
        				//, value: '<?php echo trim($ord['']); ?>'
        			}, {
						name: 'tip_prod',
        				xtype: 'combo',
                    	anchor: '100%',
        				fieldLabel: 'Tipologia produzione',
        				displayField: 'text',
        				valueField: 'id',
        				forceSelection: true,
        			   	allowBlank: false,
        			   	hideTrigger: false,
        			   	value: '<?php echo trim($ord['TDTICA']); ?>'
        			}, {
						name: 'prio',
        				xtype: 'combo',
                    	anchor: '100%',
        				fieldLabel: 'Priorit&agrave',
        				displayField: 'text',
        				valueField: 'id',
        				//emptyText: '- seleziona -',
        				forceSelection: true,
        			   	allowBlank: true,
        			   	hideTrigger: false,
        			   	value: '<?php echo trim($ord['TDPRIO']); ?>'
        			}, {
    				     name: 'dtep'
    				   , xtype: 'datefield'
    				   , startDay: 1 //lun.
    				   , fieldLabel: 'Consegna richiesta'
    				   , format: 'd/m/Y'
    				   , submitFormat: 'Ymd'
    				   , allowBlank: false
    				   , value: '<?php echo trim($ord['TDSWAF']); ?>'
        			}, {
    				     name: 'dtrg'
    				   , xtype: 'datefield'
    				   , startDay: 1 //lun.
    				   , fieldLabel: 'Data registrazione'
    				   , format: 'd/m/Y'
    				   , submitFormat: 'Ymd'
    				   , allowBlank: false
    				   , value: '<?php echo trim($ord['TDDTRG']); ?>'
        			}, {
						name: 'stdo',
        				xtype: 'combo',
                    	anchor: '100%',
        				fieldLabel: 'Stato',
        				displayField: 'text',
        				valueField: 'id',
        				hideTrigger: false,
        				value: '<?php echo trim($ord['TDSTAT']); ?>',
        				forceSelection: true,
        			   	allowBlank: false
        			}
                  ]
                }
			]
		}
    ],
    
  	buttons: [{
            text: 'Salva',
            iconCls: 'icon-save-32', scale: 'large',
            handler: function() {
            	var form = this.up('form').getForm();
            	var loc_win = this.up('window');
            	var tabPanel = loc_win.down('tabpanel');

    			if(form.isValid()){
    				Ext.getBody().mask('Loading... ', 'loading').show();
    				Ext.Ajax.request({
    						timeout: 240000,
    				        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_logis',
    			        jsonData: {
    			        	form_values: form.getValues(),
    			        	open_request: <?php echo acs_je($request) ?>
    			        },	
    			        method     : 'POST',
    			        waitMsg    : 'Data loading',
    			        success : function(result, request){

    						Ext.getBody().unmask();
    			            var jsonData = Ext.decode(result.responseText);
    			            var record = jsonData.record;
    			            tabpanel_load_record_in_form(tabPanel, record);

    			            if (Ext.isEmpty(tabPanel) == true) {
    			            	return;
    			            }

    						tabPanel.acs_actions.exe_afterSave(loc_win);

    			        },
    			        failure    : function(result, request){
    						Ext.getBody().unmask();
    			            Ext.Msg.alert('Message', 'No data to be loaded');
    			        }
    			    });
    	    	}
        	} //handler
    	} //salva
	]
}