<?php

function _ea_adv_main_w($m_params){ 
    $c = new Extjs_compo('panel', 'border');
    $c->set(array('itemId' => 'art_adv'));

    $c->add_items(array(
        _ea_adv_main_viewport_sidebar(),
        extjs_compo('panel', layout_ar('vbox') 
                , null, array('region' => 'center', 'flex' => 1, 'items' => array(
                _ea_adv_main_viewport_grid($m_params),
                _ea_adv_main_viewport_form_add($m_params) )))));
    
    $c->set(array(
        'refresh_data' => extjs_code("
            function(form_values){
                var m_tree = this.down('#classe_fornitore_tree'),
                    m_grid = this.down('#art_grid');
                m_tree.store.proxy.extraParams.form_values = form_values;
                m_tree.store.load();
                m_grid.store.proxy.extraParams.form_values = form_values;
                m_grid.store.load();
                m_grid.store.removeAll();
            }
        "),
        'afterAddArt_notify' => extjs_code("
            //ToDo
            function(win){
                alert('Inserimento riuscito');
                win.down('#form_add').reset();
            }
        ")
    ));
    return $c->code();
}



function _ea_adv_main_viewport_sidebar(){    
  $c = new Extjs_compo('panel', 'vbox', 'Ricerca e selezione');
  $c->set(array(
      'region'      => 'west',
      'width'       => 295,
      'collapsible' => true));
  $c->add_items(array(
      _ea_adv_filtro_panel(),
      _ea_adv_classe_fornitore_tree()  ));
  return $c->code();
}


function _ea_adv_main_viewport_grid(){
    global $m_params;
    $c = new Extjs_Grid(null, 'Elenco articoli');
    $c->set(array('itemId' => 'art_grid', 'flex' => 1));
    $c->set_features(array('filters' => array()));
    $c->set(array(
        'columns' => array(
            grid_column_h('Codice',    'ARART',  'w110', 'Codice articolo', null, array('filter_as' => 'string'))
          , grid_column_h('Articolo',  'ARDART', 'f1',   'Articolo', null, array('filter_as' => 'string'))
          , grid_column_h_f2('Prezzo', 'LIPRZ')
          , grid_column_h('Riferimento', 'ARARFO',  'w110', 'Riferimento', null, array('filter_as' => 'string'))
          , grid_column_h_img('camera', 'NR_ALLEGATI', 'Allegati', array(
             'renderer' => extjs_code("
                function(value, p, record){
	    	      if(record.get('NR_ALLEGATI') > 0) return '<img src=" .  img_path("icone/48x48/camera.png") . " width=15>';
	    		}
              ")) )
         ),
        'store'       => array(
            'autoLoad'    => false,
            'fields'      => array('ARARFO', 'ARART', 'ARDART', 'LIPRZ', 'c_art', 'd_art', 'NR_ALLEGATI', 'CONFIG', 'config_par', 'idsc', 'ARUMTE'),
            'proxy'       => extjs_grid_proxy(extjs_url('get_json_data_art'),array('open_parameters' => $m_params))),
        'listeners'   => array(
            'itemclick' => extjs_code(grid_itemclick("
              //refresh form add
                var me = this,
                    m_panel  = this.up('#art_adv'),
                    form_add = m_panel.down('#form_add');
                form_add.loadRecord(rec);   
                b_config  = m_panel.down('#b_config');
                if(rec.get('CONFIG').trim() == '') b_config.disable();
                else b_config.enable();

           ")),
           'celldblclick' => extjs_code(grid_celldblclick("
                if (col_name == 'NR_ALLEGATI'){
                    acs_show_win_std('Articolo ' +rec.get('ARART') , '../desk_utility/acs_anag_art_allegati.php?fn=open_tab', {c_art: rec.get('ARART')}, 600, 400, null, 'icon-camera-16');
                    return;
                }

                " . _ea_add_celldblclick_by_params($m_params) . "

           "))
        )
    ));
    return $c->code();
}


function _ea_add_celldblclick_by_params($m_params){
    if ($m_params->open_on_dblclick == 'background_mrp_from_wizard'){
        return "
           acs_show_win_std('Loading...', '../desk_vend/acs_background_mrp_art.php', {from_wz : 'Y', rdart: rec.get('codice')}, 1200, 600, null, 'icon-shopping_cart_gray-16');
        ";
    }
}



function _ea_adv_main_viewport_form_add($m_params){
    global $m_params;
    $c = new Extjs_Form( layout_ar('vbox'), 'Aggiungi a ordine');
    $c->set(array('height' => 170, 'itemId' => 'form_add'));
    
    if (!$m_params->abilita_inserimento_in_ordine)
        $c->set(array('hidden' => true));
    
    if(isset($m_params->prezzo_non_obb))
        $prezzo_non_obb = $m_params->prezzo_non_obb;
    else
        $prezzo_non_obb = false;
        
    $c->add_items(array(

        array('xtype' => 'textfield', 'name'=>'ARART', 'hidden' => true),
        array('xtype' => 'textfield', 'name'=>'ARDART', 'hidden' => true),
        array('xtype' => 'textfield', 'name'=>'CONFIG', 'hidden' => true),
        array('xtype' => 'textfield', 'name'=>'config_par', 'hidden' => true),
        array('xtype' => 'textfield', 'name'=>'idsc', 'hidden' => true),
        //extjs_compo('container', layout_ar('hbox'), null, array('items' => array(
            extjs_compo('fieldcontainer', 'vbox', null, array('flex'=> 1.5, 'items' => array(
                array('xtype' => 'displayfield', 'name'=>'c_art', 'fieldLabel' => 'Codice'),
                array('xtype' => 'displayfield', 'name'=>'d_art', 'fieldLabel' => 'Descrizione',
                    'fieldStyle' => 'font-weight: bold;',
                ),
            ))),
        extjs_compo('fieldcontainer', 'hbox', null, array('flex'=> 1, 'items' => array(
            array('xtype' => 'displayfield',  'name' => 'ARUMTE', 'fieldLabel' => 'Quantit&agrave;', 'labelSeparator' => '', 'labelWidth' => 50),
            array('xtype' => 'textfield',  'name' => 'f_quant', 'fieldLabel' => '&nbsp;', 'value' => 1, 'labelWidth' => 30,
                'height' => 25, 'allowBlank' => false,
                'fieldStyle' => 'text-transform: uppercase;',
                'flex'=> 0.3
            ),
            array('xtype' => 'textfield',  'name' => 'f_nota_1', 'fieldLabel' => 'Note', 'labelWidth' => 50, 'labelAlign' => 'right', 'maxLength' => 50, 'flex'=> 1)
        ))),
        
        extjs_compo('fieldcontainer', 'hbox', null, array('flex'=> 1, 'items' => array(
            array('xtype' => 'numberfield', 'name' => 'LIPRZ', 'fieldLabel' => 'Prezzo Unitario',
                'height' => 25, 'allowBlank' => $prezzo_non_obb, 'hideTrigger' => true, 'flex'=> 0.5
            ),
            array('xtype' => 'textfield',  'name' => 'f_nota_2', 'fieldLabel' => '&nbsp;', 'labelWidth' => 50, 'labelAlign' => 'right', 'maxLength' => 50, 'flex'=> 1, 'labelSeparator' => '')
        ))),
                
                 
        
        //)))
            
    ));
    $c->add_buttons(array(
        array(
            'xtype'   => 'button',
            //'text'    => 'Seleziona',
            'iconCls' => 'icon-game_pad-32',
            'scale' => 'large',
            'itemId' => 'b_config',
            'handler' => extjs_code("
                function(){
                   var me = this,
                    m_panel  = this.up('#art_adv');
                    var loc_win = this.up('window');
                    form_add = m_panel.down('#form_add');
                    form_values = form_add.getForm().getValues();
                   
                        var my_listeners = {
            					afterConferma: function(from_win, idsc, config_par){
            					    form_add.getForm().findField('idsc').setValue(idsc);
                                    form_add.getForm().findField('config_par').setValue(config_par);
                                    from_win.destroy();
            					     						        									            
    				        		}
    		    				};

                            //avvio configurazione
				  	    	acs_show_win_std('Configuratore articolo', 
        		  				'../base/acs_configuratore_articolo.php?fn=open_tab', 
        		  				{	
        		  					c_art :  form_values.ARART, 
        		  					gestione_dim: 'N',
        		  					ar_config_row: form_values.ar_config_row
        		  				}, 600, 400, my_listeners, 'icon-copy-16');		  	      
				    }
            
            ")
                ),
        
        array(
        'xtype' => 'button', 
        'text' => 'Aggiungi a<br>riga ordine',
        //'iconCls' => 'icon-folder_search-32',
        'flex' => 1,
        'handler' => extjs_code("
            function(){
                var me = this,
                    m_panel  = this.up('#art_adv');
                    var loc_win = this.up('window');
                    form_add = m_panel.down('#form_add');
                    form_values = form_add.getForm().getValues();

                    console.log(form_values);

                    list_selected_art = [];
                    list_note = [];
                    list_note.push({
				            	nota_1 : form_values.f_nota_1,
                                nota_2 : form_values.f_nota_2
                          });
                   	list_selected_art.push({
				            	codice: form_values.ARART,
				            	descr: form_values.ARDART,
				            	quant: form_values.f_quant,
				            	prezzo: form_values.LIPRZ,
                                config_par : form_values.config_par,
                                idsc :  form_values.idsc,
                                note: list_note
                          });
                    
                if(form_values.CONFIG != '' && form_values.idsc == ''){
                    acs_show_msg_error('Configurare articolo');
				    return;
                }
         

                if (form_add.getForm().isValid()){

                        if (!Ext.isEmpty(loc_win.events.afterselectart))
								loc_win.fireEvent('afterSelectArt', loc_win, list_selected_art);
					
                }  
            }
        ")
    )), 'right');
    
    return $c->code();
}


//*********************************************
// Panel Filtro
//*********************************************
function _ea_adv_filtro_panel(){
  $c = new Extjs_Form('vbox');
  $c->set(array('height' => 150, 'width' => '100%'));  
  $c->add_items(array(
      array('xtype' => 'textfield', 'name' => 'f_descr',   'fieldLabel' => 'Descrizione')
    , array('xtype' => 'textfield', 'name' => 'f_cod_art', 'fieldLabel' => 'Codice')
    , extjs_combo_fornitore(array('name' => 'f_forn'))
    , array('xtype' => 'textfield', 'name' => 'f_riferimento', 'fieldLabel' => 'Riferimento')
  ));
  $c->add_button(array(
      'xtype'   => 'button', 
      'text'    => 'Applica',
      'scale' => 'small',
      'flex' => 1, 'handler' => extjs_code("
            function(){
                var me = this,
                    m_panel = this.up('#art_adv'),
                    form_values = this.up('form').getValues();
                m_panel.refresh_data(form_values);
            }
        ")
  ));
  return $c->code();  
}





//*********************************************
// Tre - Classe/Fornitore
//*********************************************

function _ea_adv_classe_fornitore_tree(){
    $c = new Extjs_compo('treepanel', null, 'Classe / Fornitore');
    $c->set(array('itemId' => 'classe_fornitore_tree'));
    $c->set(array('flex' => 1.5, 'width' => '100%'));
    $c->set(array(
        'rootVisible' => false, //obbligatorio altrimenti non esegue autoLoad dello store
        'store'       => _ea_adv_classe_fornitore_tree_store(),
        'columns'     => _ea_adv_classe_fornitore_tree_columns(),
        'listeners'   => array(
          'itemclick' => extjs_code(tree_itemclick("
              //refresh grid in base a nodo selezionato
              var me = this,
                  m_panel = this.up('#art_adv'),
                  m_grid  = m_panel.down('grid');              
              m_grid.store.proxy.extraParams.classe_fornitore = rec.get('id');
              m_grid.store.load();
           "))
        )
    ));
    return $c->code();
}


function _ea_adv_classe_fornitore_tree_columns(){
  $ret = array();
  $ret[] = array('xtype' => 'treecolumn', 'flex'=> 1, 'dataIndex' => 'task', 'header' => 'Classe/Fornitore',
      'renderer' => extjs_code(" function(value, p, record){
                 if(record.get('liv_cod') == 'ARFOR1'){
                    ar_id = record.get('id').split('|');
	    	        if(ar_id[1] == '0') return 'Fornitore non assegnato [000000000]';
                  
                  }
                    return value;
	    		}
       ")
  );
  return $ret;
}


function _ea_adv_classe_fornitore_tree_store(){
    global $m_params;    
    $c = new Extjs_compo();
    $c->set(array(
        'autoLoad'    => true,
        'fields'      => array('task', 'liv', 'liv_cod'),
        'proxy'       => extjs_tree_proxy(
            extjs_url('get_json_data_tree_classe_fornitore'), 
            array('open_parameters' => $m_params)) ));
    return $c->code();    
}




