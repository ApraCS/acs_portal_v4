<?php
require_once "../../config.inc.php";

$main_module = new DeskUtility(array('no_verify' => 'Y'));
$desk_art = new DeskArt(array('no_verify' => 'Y'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_lancia'){
    $ritime     = microtime(true);
    $ar_vals = array();
    $write_pi = false;
    $file = $m_params->open_request->file_tabelle->file_tabelle;
    $menu_type = $m_params->open_request->menu_type;
    $batch = $m_params->open_request->batch;
    $form_values = $m_params->open_request->form_values;
    $stringa =  sprintf("%-1250s", "");
    
    $sql = "SELECT *
    FROM {$file} TA
    WHERE TADT = '{$id_ditta_default}' AND TATAID = 'BATCP'
    AND TAKEY1 = '{$batch}'
    ORDER BY TASITI
    ";
    
  
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    while($row = db2_fetch_assoc($stmt)){
        
        $field = trim($row['TAKEY2']);
        $campo_ri = trim($row['TACAP']);
        $f_pi = trim($row['TARIF2']);
        $formato = trim($row['TARIF1']);
        
        if($campo_ri != ''){
            if($campo_ri == 'RIFOR1' || $campo_ri == 'RIFOR2')
                $ar_vals["{$campo_ri}"] = sql_f($form_values->$field);
            else
                $ar_vals["{$campo_ri}"] = $form_values->$field;
            
           if($formato != '')
                $stringa .= $form_values->$field;
            
        }
        
        if($f_pi != ''){
             $posizione = $f_pi;
           
            if($row['TAFG01'] == 'N'){
                $ar_numb = explode(',', $formato);
                $chars = number_to_text($form_values->$field, $ar_numb[0], $ar_numb[1]);
            }else if($row['TAFG01'] == 'D'){
                $m_len = "%0{$formato}s";
                $chars = sprintf($m_len, $form_values->$field);
            }else{
                $m_len = "%-{$formato}s";
                $chars = substr(sprintf($m_len, $form_values->$field), 0, $formato);
            }
            
            $new_value = substr_replace($stringa, $chars, $posizione - 1, $formato);
            $stringa = $new_value;
            $write_pi = true;
            
            
        }
        
    }

  
     if($write_pi == true){
         $ar_ins = array();
         $ar_ins['PITIME'] = $ritime;
         $ar_ins['PIDT']   = $id_ditta_default;
         
         $ar_ins['PIDTGE'] = oggi_AS_date();
         $ar_ins['PIORGE'] = oggi_AS_time();
         $ar_ins['PIUSGE'] = $auth->get_user();
         $ar_ins['PIDEF1'] = substr($stringa, 0, 250);
         $ar_ins['PIDEF2'] = substr($stringa, 250, 250);
         $ar_ins['PIDEF3'] = substr($stringa, 500, 250);
         $ar_ins['PIDEF4'] = substr($stringa, 750, 250);
         $ar_ins['PIDEF5'] = substr($stringa, 1000, 250);
         
         
         $sql = "INSERT INTO {$cfg_mod_DeskArt['file_parametri']} (" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
         
         $stmt = db2_prepare($conn, $sql);
         echo db2_stmt_errormsg();
         $result = db2_execute($stmt, $ar_ins);
         echo db2_stmt_errormsg($stmt);
     
     }
     
     
  
     if(trim($menu_type) != ''){
         if($stringa != '')
             $ar_vals['RIART'] = trim($stringa);
         $sh = new SpedHistory();
     } else 
         $sh = new SpedHistory($desk_art);
     

	$ret_RI = $sh->crea(
			'pers',
			array(
			        "RITIME"    => $ritime,
			        "messaggio"	=> $m_params->open_request->RIRGES,
					"vals" => $ar_vals
			)
			);

    
	$ret['success'] = true;
	$ret['ret_RI'] = $ret_RI;
	echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'get_json_data_grid'){

	$msg_ri = $m_params->open_request->RIRGES;
	$cfg_mod = (array)$m_params->open_request->file_tabelle;
	
	$sql = "SELECT *
	        FROM {$cfg_mod['file_richieste']}
	        WHERE  RIRGES = '{$msg_ri}'
			ORDER BY RITIME DESC FETCH FIRST 5 ROWS ONLY";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['data_i'] = print_date(trim($row['RIDTRI']));
		$nr['ora_i'] = print_ora(trim($row['RIHMRI']));
		$nr['utente'] = trim($row['RIUSRI']);
		$nr['stato'] = trim($row['RIESIT']);
		$nr['note'] = trim($row['RINOTE']);
		$nr['data_f'] = print_date(trim($row['RIDTES']));
		$nr['ora_f'] = print_ora(trim($row['RIHMES']));
		//$nr['default'] = trim($row['RITAB1']);
		$ar[] = $nr;
	}

	echo acs_je($ar);
	exit;
}



if ($_REQUEST['fn'] == 'open_form'){?>

{"success":true, 
"items": [

        {
				xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            
	            buttons: [  {
 			            text: 'Lancia',
 			            itemId: 'b_lancia',
 			            iconCls: 'icon-button_blue_play-32',
 			            hidden : true,
 			            scale: 'large',
 			               handler: function() {
 			               var loc_win = this.up('window');
 			               
 			               var m_grid = this.up('window').down('grid');
 			               Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_lancia',
 						        timeout: 2400000,
 						        method     : 'POST',
 			        			jsonData: {
 			        			    open_request: <?php echo acs_je($m_params) ?>
 								},							        
 						        success : function(result, request){
 						             jsonData = Ext.decode(result.responseText);
 						             console.log(jsonData);
 						    		 if (jsonData.ret_RI.RIESIT == 'W'){
 						    		    loc_win.fireEvent('afterLancia', loc_win, jsonData.ret_RI);
									    return;
									 }else{
									 	m_grid.getStore().load();	
									 }
 						        
 						        
 						            
 						    		//loc_win.fireEvent('afterLancia', loc_win);		            			
 			            		},
 						        failure    : function(result, request){
 						            Ext.Msg.alert('Message', 'No data to be loaded');
 						        }
 						    });	
 						    


 						
 			
 			            }
 			         }], 
	               items: [ 	
	                 {
						xtype: 'grid',
						title: 'Stato elaborazione ' + <?php echo j($m_params->RIRGES); ?>,
						flex:0.7,
				        loadMask: true,	
				        store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_je($m_params) ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['data_i', 'ora_i', 'utente', 'stato', 'note', 'data_f', 'ora_f', 'default']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Data inizio',
	                dataIndex: 'data_i',
	                flex: 1
	                },
	                {
	                header   : 'Ora inizio',
	                dataIndex: 'ora_i',
	                flex: 1
	                },
	                {
	                header   : 'Utente',
	                dataIndex: 'utente',
	                flex: 1
	                }/*,{
	                header   : 'Default',
	                dataIndex: 'default',
	                flex: 1
	                }*/,
	                {
	                header   : 'Stato',
	                dataIndex: 'stato',
	                flex: 1
	                },
	                {
	                header   : 'Note',
	                dataIndex: 'note',
	                flex: 1
	                }, {
	                header   : 'Data fine',
	                dataIndex: 'data_f',
	                flex: 1
	                },
	                {
	                header   : 'Ora fine',
	                dataIndex: 'ora_f',
	                flex: 1
	                },                
	                
	                
	         ], listeners: {
				   afterrender: function(comp){
				       var loc_win = comp.up('window');
				       
				       var run = 0
				       var task = {
				        run: function(){
				           
				           run++;
				           
				           var func_att = 0;
				           comp.store.each(function(item){
        			          if(item.get('stato') != 'C' && item.get('stato') != 'W'){
        		                  func_att = func_att + 1;
        		                }
                            });
				         
				           if(run > 1){
				                
            				 	if(func_att == 0){
            				 	  if (!Ext.isEmpty(loc_win.events.afterlancia))
            				 	  	loc_win.fireEvent('afterLancia', loc_win);
            				 	 }else{
            				      comp.store.load();
            				    }
        				    }else{
				              comp.store.load();
				            }
				            
        				    
				        	  
						},
				        interval: 5000 //3*60*1000  3 minuti (in millisecondi)
				    }
				
				    Ext.TaskManager.start(task);
				 
				    
				   }
				 }
				 
				  ,viewConfig: {
					        //Return CSS class to apply to rows depending upon data values
					        getRowClass: function(record, index) {
					           if (record.get('stato') == 'C')
					           		return 'colora_riga_grigio';
					           return '';																
					         }   
					    }
					       
		
		
		}
				
					 ],
					 
					  listeners: {
                     	afterrender: function(compo){
                     		var b_lancia = compo.down('#b_lancia');
                     		<?php if ($m_params->lancia == 'Y') { ?>
                     			b_lancia.fireHandler();
                     		<?php }else{ ?>
                     		     var loc_win = compo.up('window');
                     		     loc_win.fireEvent('afterSincro', loc_win);
                     		<?php } ?>	
                     	}
     }
					 
		  
					
					
	}
	
]}


<?php 
}