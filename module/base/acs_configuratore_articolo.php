<?php

require_once "../../config.inc.php";
require_once "acs_configuratore_articolo_include.php";

$s = new Spedizioni(array('no_verify' => 'Y'));
$deskArt = new DeskArt(array('no_verify' => 'Y'));
$m_params = acs_m_params_json_decode();

//**********************************************************
if ($_REQUEST['fn'] == 'get_dom_risp'){
//**********************************************************
    $ar_values = get_dom_ris_by_idsc($m_params->idsc);        
    $ret['success'] = true;
    $ret['ar_config_row'] = $ar_values;
    echo acs_je($ret);
    exit();
    
}

//**********************************************************
if ($_REQUEST['fn'] == 'exe_elabora'){
//**********************************************************

    $ar_upd = array();
    $ar_upd['TCDIM1'] = sql_f($m_params->form_values->dim1);
    $ar_upd['TCDIM2'] = sql_f($m_params->form_values->dim2);
    $ar_upd['TCDIM3'] = sql_f($m_params->form_values->dim3);
    
    //aggiorno il tc con le dimensioni
    $sql = "UPDATE {$cfg_mod_DeskArt['elab_configuratore']['file_config_testata']} TC
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE TCDT = '{$id_ditta_default}' AND TCSESS = '{$m_params->idsc}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
    
    if(isset($m_params->k_ordine) && strlen($m_params->k_ordine) > 0){
        $k_ordine = $m_params->k_ordine;
        $nrec = $m_params->nrec;
    }else{
        $k_ordine = '';
        $nrec = 0;
    }
    
   
   //elaboro il primo messaggio
    $sh = new SpedHistoryConfigArt($deskArt);
    $ret_RI = $sh->crea(
           'pers',
            array(
                "messaggio"	=> 'ELAB_CONFIG',
                "k_ordine"	=> $k_ordine,
                "vals" => array(
                    "RIDT"  	=> $id_ditta_default,
                    "RISESS"  	=> $m_params->idsc,
                    "RICDAR"  	=> $m_params->c_art,
                    "RINREC"   => $nrec
                ),
            )
            );
    
  
        
    if($ret_RI['RIESIT'] == 'W'){
        $errore = $ret_RI['RINOTE'];
        return;
    }
    
    $row_tc = get_TC_row($m_params->idsc);
    
    if($row_tc['TCFINE'] == 'Y'){
        $ret['success']  = true;
        $ret['fine_elab'] = 'Y';
        echo acs_je($ret);
        exit;
    }
    
    $ret['form'] = get_new_page($m_params->idsc, $row_tc['TCPAGI']);
    
    $proc = new ApiProc();
    $proc->out_json_response($ret);
    exit;
       
}

//**********************************************************
if ($_REQUEST['fn'] == 'exe_config'){
//**********************************************************
    $ret = array();
    $values = $m_params->form_values;
    $idsc = $m_params->idsc;
    //$pagina = $m_params->pagina;
   
    update_RC_row($values);
    $ret_RI = write_RI_elab_config($idsc, $m_params);
    if($ret_RI['RIESIT'] == 'W'){
        $errore = $ret_RI['RINOTE'];
        return;
    }
    
    $row_tc = get_TC_row($m_params->idsc);
    
    if($row_tc['TCFINE'] == 'Y'){
        $ret['success']  = true;
        $ret['fine_elab'] = 'Y';
        echo acs_je($ret);
        exit;
    }
    
    $ret['form'] = get_new_page($m_params->idsc, $row_tc['TCPAGI']);

    $proc = new ApiProc();
    $proc->out_json_response($ret);
    exit;
}


//************************************************************************
if ($_REQUEST['fn'] == 'open_tab'){
//************************************************************************
    //creazione IDSC
    $idsc = uniqid();
    
    //recupero dettagli articolo (descrizione, dimensioni ecc..)
   /* 3) Mostro TabPanel con form INFO precompilata
    *  - se richiesta gestione dimensioni: resto in attesa dell'utente
    */
   
    $row_art = get_AR_row($m_params->c_art);
    $dim1 = $row_art['ARDIM1'];
    $dim2 = $row_art['ARDIM2'];
    $dim3 = $row_art['ARDIM3'];
    $mode = trim($row_art['ARMODE']);
    $dimensioni = n($dim1, 2)."x".n($dim2, 2)."x".n($dim3, 2);
    
    insert_TC_row($idsc, $m_params);
    
    //se ho richiesto le dimensioni non vado avanti
    if($m_params->gestione_dim != 'Y'){
       $ret_RI = write_RI_elab_config($idsc, $m_params);
       if($ret_RI['RIESIT'] == 'W'){
             $errore = $ret_RI['RINOTE'];
             return;
         }
    }
     
     $row_tc = get_TC_row($idsc);
     $pagina = $row_tc['TCPAGI'];
     
     $stmt_rc = get_RC_stmt(0, $idsc, $pagina);
    
     $tab_pumo = get_TA_sys('PUMO', $mode);
     $config = "[".$mode."] ".trim($tab_pumo['text']);
     
    
    ?>
    {"success":true, "items": [
       {
 		xtype: 'form',
 		idsc:  <?php echo j($idsc); ?>,
 		flex : 1,
		layout : 'fit',
 		items: [
                {
                xtype: 'tabpanel',
                tabPosition2: 'left',
                layout : 'fit',
                items: [                
               	 {
  		            xtype: 'form',
 		            flex : 1,
 		            title : 'Info',
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            frame: true,
  		            items: [
  		            	 {xtype : 'displayfield', fieldLabel : 'Sessione', value : <?php echo j($idsc); ?>}
  		               , {xtype : 'displayfield', fieldLabel : 'Articolo', value : <?php echo j($m_params->c_art); ?>}
  		               , {xtype : 'displayfield', fieldLabel : 'Descrizione', value : <?php echo j($row_art['ARDART']); ?>}
  		               , {xtype : 'displayfield', fieldLabel : 'Configuratore', value : <?php echo j($config); ?>}
  		               <?php if($m_params->gestione_dim == 'N'){?>
  		               , {xtype : 'displayfield', fieldLabel : 'LxHxP', value : <?php echo j($dimensioni); ?>}
  		               <?php }else{?>
  		               , {xtype: 'fieldcontainer',
					  	  layout: { type: 'hbox',
					                pack: 'start',
							        align: 'stretch'},						
    					  items: [
        						 {xtype : 'numberfield', 
        						 name : 'dim1',
          		                 fieldLabel : 'L x H x S',
          		                 width : 200,
          		                 hideTrigger : true,
          		                 value : <?php echo j($dim1); ?>
          		                 },
          		                 {xtype : 'numberfield',
          		                 name : 'dim2', 
          		                 width : 100,
          		                 hideTrigger : true,
          		                 value : <?php echo j($dim2); ?>
          		                 },
          		                 {xtype : 'numberfield', 
          		                 name : 'dim3',
          		                 width : 100,
          		                 hideTrigger : true,
          		                 value : <?php echo j($dim3); ?>
          		                 }
    						]}
  		               <?php }?>
  		                
  		             ]
  		           
 		         },
 		      <?php if($m_params->gestione_dim == 'N'){?>
 		          {
  		            xtype: 'form',
 		            flex : 1,
 		            title : 'Pagina ' + <?php echo j($pagina); ?>,
 		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
 		            frame: true,
  		            items: [
  		            <?php while($row_rc = db2_fetch_assoc($stmt_rc)){
  		              
  		                $row_ta = get_TA_sys('PUVR', trim($row_rc['RCVARI']), null, null, null, null, 1);
	            	    $numerica = $row_ta['tarest'];
	    	            $sequenza = $row_rc['RCSEQ3'].$row_rc['RCSEQ4'];
	    	            $variabile = trim($row_rc['RCVARI']);
	    	            $configuratore = trim($row_rc['RCMODE']);
	    	            $allowBlank = check_allowBlank(trim($row_rc['RCVARI']), trim($row_rc['RCMODE']), trim($row_rc['RCSEQ3']), trim($row_rc['RCSEQ4']));
	    	            
	            	      
	            	    if($row_rc['RCTIPO'] != 'S'){
	            	       if($numerica == 'N'){
	            	           
	            	          $ar_valid = get_valid_number($configuratore, $sequenza, $variabile);
	            	          if(trim($ar_valid['MDVAM1']) != '')
	            	               $min = "minValue : {$ar_valid['MDVAM1']},";
	            	          else 
	            	              $min = '';
            	              if(trim($ar_valid['MDVAM5']) != '')
	            	                  $max = "maxValue : {$ar_valid['MDVAM5']},";
	            	           else 
	            	              $max = '';
	            	           
	            	           ?>
  		            	    
      		            	  { xtype: 'numberfield',
        						name: <?php echo j($row_rc['RRN']); ?>,
        						fieldLabel: <?php echo j($row_ta['text'])?>,
        					    hideTrigger : true,
        						anchor: '-15',
        						allowBlank : <?php echo $allowBlank; ?>,
        						labelWidth : 180,	
        						<?php echo $min; ?>
        						<?php echo $max; ?>
        						value : <?php echo j($row_rc['RCVANN']); ?>,
        					  },
  		            	    
  		            	    <?php }else{
  		            	    
  		            	        $ar_varianti = get_valori_ammessi($variabile, $configuratore, $sequenza);
  		            	        
  		            	        if(count($ar_varianti) == 0)
  		            	            $ar_varianti = find_TA_sys('PUVN', null, null, $variabile, null, null, 0, '', 'Y');
  		            	            
  		            	        ?>
  		            	    
  		            	    {
                        		name: <?php echo j($row_rc['RRN']); ?>,
                        		xtype: 'combo',
                        		flex: 1,
                        		labelWidth : 180,
                        		fieldLabel: <?php echo j($row_ta['text'])?>,
                        		forceSelection: true,								
                        		displayField: 'text',
                        		valueField: 'id',								
                        		emptyText: '- seleziona -',	
                        		allowBlank : <?php echo $allowBlank; ?>,					
                        	    anchor: '-15',
                        	    value:  <?php echo j(trim($row_rc['RCVANA'])); ?>,
                        	  	store: {
                    				editable: false,
                    				autoDestroy: true,
                    			    fields: [{name:'id'}, {name:'text'}],
                    			    data: [								    
                    			     <?php echo acs_ar_to_select_json($ar_varianti, ''); ?>		
                    			    ]
                    			},listeners: { 
                    			 		beforequery: function (record) {
                    	         		record.query = new RegExp(record.query, 'i');
                    	         		record.forceAll = true;
                    			 }
                     			}
                        			
                           		},
  		            	
  		            	<?php }
  		            	    } 
                          }?> 
  		                    
  		              ],
  		       
 		         }
 		         <?php }?>
 		        ],
 		        listeners: {
     		        'afterrender': function (comp) {
     		           var win = comp.up('window');
                       var b_avanti = win.down('#b_avanti');
                        <?php if($m_params->gestione_dim == 'N'){?>
                             b_avanti.hide();
                        <?php }else{?>
                             b_avanti.show();
                        <?php }?>
     		        
     		        },
                    'tabchange': function (tabPanel, tab) {
                        var win = tabPanel.up('window');
                        var activeTab = tabPanel.getActiveTab();
                        var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                        var b_avanti = win.down('#b_avanti');
                        <?php if($m_params->gestione_dim == 'N'){?>
                            if(activeTabIndex > 0) b_avanti.show();
                            else b_avanti.hide();
                        <?php }else{?>
                             b_avanti.show();
                        <?php }?>
                        
                    }
                }
 		       } //tabpanel
		  
                  
        ], 
 		         
 		dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [ '->',
                   {	
                     xtype: 'button',
                     text: 'Avanti',
 		             scale: 'small',
 		             itemId : 'b_avanti',               
					 iconCls: 'icon-button_blue_play-16',
					 handler: function() {
 		          	     var main_form = this.up('form');
 		          	     if(main_form.getForm().isValid()){
 		          	     
 		          	     	var tabPanel = main_form.down('tabpanel');
    		        		var activeTab = tabPanel.getActiveTab();
                    		var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
 		          	     	if(activeTabIndex > 0){
 		          	     		main_form.acs_actions.exe_avanti(main_form);
 		          	     	}else{
 		          	     	    main_form.acs_actions.exe_elabora(main_form);
 		          	     	}
 		          	     	
 		          	     	
 		          	     } 		          	
 		          	 }
                   }                                  
                 ]
                                
           }] //dockitems
        
          , acs_actions : {
		       exe_elabora: function (main_form){
    		        var tabPanel = main_form.down('tabpanel');
    		        var activeTab = tabPanel.getActiveTab();
                    var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                    var form_values = activeTab.getValues();
                  	Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_elabora',
					        jsonData: {
					        	idsc: main_form.idsc,
					        	c_art : <?php echo j($m_params->c_art); ?>,
					        	k_ordine : <?php echo j($m_params->k_ordine); ?>,
					        	nrec : <?php echo j($m_params->nrec); ?>,
					        	form_values : form_values
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){								        								        
					            var jsonData = Ext.decode(result.responseText);
					            if (jsonData.fine_elab == 'Y'){
					            	main_form.acs_actions.exe_fine_elaborazione(main_form); 	
					            	return;
					            }	
					         
				            	//mostro nuova form
				        		var new_form = jsonData.form;
				        		tabPanel.add(new_form).show();
					            
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });                                        
               },
    		   exe_avanti: function (main_form){
    		        var tabPanel = main_form.down('tabpanel');
    		        var activeTab = tabPanel.getActiveTab();
                    var activeTabIndex = tabPanel.items.findIndex('id', activeTab.id);
                    //tabPanel.setActiveTab(activeTabIndex + 1);
                    var form_values = activeTab.getValues();
                  	Ext.Ajax.request({
					        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_config',
					        jsonData: {
					        	idsc: main_form.idsc,
					        	c_art : <?php echo j($m_params->c_art); ?>,
					        	k_ordine : <?php echo j($m_params->k_ordine); ?>,
					        	nrec : <?php echo j($m_params->nrec); ?>,
					        	form_values : form_values,
					        	pagina : <?php echo j($pagina); ?> 
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){								        								        
					            var jsonData = Ext.decode(result.responseText);
					            
					            if (jsonData.success == false){
					            	acs_show_msg_error(jsonData.msg_error);
							       	return;
					            }
					            
					            if (jsonData.fine_elab == 'Y'){
					            	main_form.acs_actions.exe_fine_elaborazione(main_form); 	
					            	return;
					            }	
					            
					            
					           /* if (jsonData.error_page != ''){
					            	//ToDo: fireEvent fine config????
					            	acs_show_msg_error(jsonData.error_page);
					            	return;
					            } */
				            	
				            	//mostro nuova form
				        		var new_form = jsonData.form;
				        		tabPanel.add(new_form).show();
					            
					        },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });                                        
               },
               
               exe_fine_elaborazione(main_form){
               		//ToDo: fireEvent ritornando main_form e idsc
               		var tabPanel = main_form.down('tabpanel');
    		        var activeTab = tabPanel.getActiveTab();
                    var form_values = activeTab.getValues();
                    if(!Ext.isEmpty(form_values)){
                    	var config_par = 'P'
                    }else config_par = '';
                    
               		loc_win = main_form.up('window');
               		loc_win.fireEvent('afterConferma', loc_win, main_form.idsc, config_par);
               }
		   
		  } //acs_actions 
           
            		          		          		         
 		} //form
    ]}
    <?php 
    
}

if ($_REQUEST['fn'] == 'open_form'){
    
    if (isset($m_params->ar_config_row)){
        $ar_config_row = $m_params->ar_config_row;
    } else {
        //ar_config_row lo recupero da idsc
        $ar_config_row = get_dom_ris_by_idsc($m_params->idsc);
    }
?>


{"success":true, "items": [

        {
            xtype: 'form',
            autoScroll : true,
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            
            items: [
            
            <?php 
                $i = 0;
                
                foreach($ar_config_row as $k => $v){
                    
                $i++;
                $dom = $k;
                $val_dom = get_TA_sys('PUVR', $dom, null, null, null, null, 1);
                $value_D = "{$i}) [{$dom}] ".$val_dom['text'];
                $swn = $val_dom['tarest'];
            	
            	if($swn == 'N'){
            	    $value_R =   $v;
            	}else{ 
            		
            	    $val_risp = get_TA_sys('PUVN', $v, null, $k);
            	    $value_R = "[{$v}] ".$val_risp['text'];
                }
            	
            	if($i % 2 == 0)
            	    $class = "row_p";
            	else
            	    $class = "row_d";
            
            ?>
            
            {
						xtype: 'fieldcontainer',
						cls: '<?php echo $class; ?>',	
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						    {						
								name: 'f_D',
								xtype: 'displayfield',
								value: <?php echo j($value_D) ?>,
								labelWidth : 250,
								flex: 1,
								hideTrigger:true				
							}
							,{						
								name: 'f_R',
								xtype: 'displayfield',
								value: <?php echo j($value_R) ?>,
								labelWidth : 250,
								flex: 1,
								hideTrigger:true				
							}
				   
						
						]
				},
				
				<?php  } ?>	  	
					                    
            ]          
            
           
}
	
]}

<?php 
exit;
}

//************************************************************************
if ($_REQUEST['fn'] == 'open_pumo'){
//************************************************************************
 ?>
    
     {"success":true, "items": [
       {
 		xtype: 'form',
 		bodyStyle: 'padding: 10px',
        bodyPadding: '5 5 0',
        frame: true,
        autoScroll : true,
        title: '',
        layout: {pack: 'start', align: 'stretch'},
 		items: [
 		{
    		name: 'f_configuratore',
    		xtype: 'combo',
    		flex : 1,
    		value : <?php echo j($m_params->mode); ?>,
    		fieldLabel: 'Configurazione intestazione',
    		labelWidth : 180,
    		forceSelection: true,								
    		displayField: 'text',
    		valueField: 'id',								
    		emptyText: '- seleziona -',
       		//allowBlank: false,								
    	    anchor: '-15',
    		store: {
    			editable: false,
    			autoDestroy: true,
    		    fields: [{name:'id'}, {name:'text'}],
    		    data: [								    
    		      <?php echo acs_ar_to_select_json(find_TA_sys('PUMO', null, null, null, null, null, 0, '', 'Y'), ''); ?>	
    		    ]
    		},
    		queryMode: 'local',
    		minChars: 1, 	
    		listeners: { 
    		 	beforequery: function (record) {
             	record.query = new RegExp(record.query, 'i');
             	record.forceAll = true;
             }
          }
    		
     }
 		
 		], buttons: [	
				{
	            text: 'Conferma',
		        iconCls: 'icon-print-24',		            
		        scale: 'medium',		            
	            handler: function() {
	              var form = this.up('form');
	       	      var form_values = form.getValues();
	       	      var loc_win = this.up('window');
	            
	              Ext.Ajax.request({
    				        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna_mode',
    				        timeout: 2400000,
    				        method     : 'POST',
    	        			jsonData: {
    	        				form_values : form_values,
    	        				rrn : '<?php echo $m_params->rrn; ?>'
    						},							        
    				        success : function(result, request){
    					        var jsonData = Ext.decode(result.responseText);
    					        loc_win.fireEvent('afterAggiorna', loc_win);
					        	
    				        },
    				        failure    : function(result, request){
    				            Ext.Msg.alert('Message', 'No data to be loaded');
    				        }
    				    }); 
	                
	                
	            }
	        }
		        
		        
		        ]    
 		
 		}
 		
 		
 		
 		]}
    
    
    
    <?php 
}

//************************************************************************
if ($_REQUEST['fn'] == 'exe_aggiorna_mode'){
//************************************************************************

    $m_params = acs_m_params_json_decode();
    $form_values = $m_params->form_values;
    
    $ar_upd = array();
    
    $ar_upd['RDUSUM'] 	= $auth->get_user();
    $ar_upd['RDDTUM'] 	= oggi_AS_date();
    $ar_upd['RDMODE'] 	= $form_values->f_configuratore;
    
    
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE RRN(RD) = '{$m_params->rrn}' ";
            
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_upd);
    echo db2_stmt_errormsg($stmt);
        
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}
