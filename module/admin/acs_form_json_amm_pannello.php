<?php

require_once "../../config.inc.php";
require_once "Admin.php";

/*if (!$auth->is_sec_admin()){
    ?>
		{"success":true, "items": [
        	{html: 'ATTENZIONE!!! Non hai permessi di Amministratore della Sicurezza', frame: true}
        	]
       	}    
    <?php
  exit;  
}*/


$s = new Admin();
?>

{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
			cls: 'acs_toolbar',            
			tbar: [{
			            xtype: 'buttongroup',
			            columns: 20,
			            title: 'Permessi',
			            items: [
			     
			     
// ***************************************************************************************
// BOTTONE UTENTI
// ***************************************************************************************			            
			       
			     <?php  if ($auth->is_sec_admin()){ ?>
			            {
			                text: 'Utenti',
			                scale: 'large',			                 
			                iconCls: 'icon-user-32',
			                iconAlign: 'top',			                
			                 handler : function() {

								<?php echo Users::out_Writer_Form(Users::out_Writer_Form_initComponent(), "users") ?>
								<?php echo Users::out_Writer_Grid(Users::out_Writer_Grid_initComponent_columns(), "users") ?>
								<?php echo Users::out_Writer_Model("users") ?>
								<?php echo Users::out_Writer_Store("users") ?>
								<?php echo Users::out_Writer_sotto_main("users") ?>
								<?php echo Users::out_Writer_main("Lista utenti", "users") ?>								
								<?php echo Users::out_Writer_window("Tabella utenti") ?>							

								} //handler function()
			            }			            
			            
,			            


// ***************************************************************************************
// BOTTONE PROFILI
// ***************************************************************************************
			            {
			                text: 'Profili',
			                scale: 'large',			                 
			                iconCls: 'icon-group-32',
			                iconAlign: 'top',			                
			                 handler : function() {


								<?php echo Profiles::out_Writer_Form(Profiles::out_Writer_Form_initComponent(), "profiles") ?>
								<?php echo Profiles::out_Writer_Grid(Profiles::out_Writer_Grid_initComponent_columns(), "profiles") ?>
								<?php echo Profiles::out_Writer_Model("profiles") ?>
								<?php echo Profiles::out_Writer_Store("profiles") ?>



// LEGAME MODULI

	Ext.define('Writer.ProfileModule', {
	    extend: 'Ext.data.Model',
	    idProperty: 'id',
	    fields: [{
	        	name: 'id',
	        	type: 'int',
	        	useNull: true
	    		}, 
	     
	    	{name: 'UPFLSI', type: 'string', convert: function(v, rec) { return (v === "Y" || v === "S" || v === true) ? 'Y' : 'N'; }},
	    	{name: 'UPFLSS', type: 'string', convert: function(v, rec) { return (v === "Y" || v === "S" || v === true) ? 'Y' : 'N'; }},
	    	{name: 'UPFLMP', type: 'string', convert: function(v, rec) { return (v === "Y" || v === "S" || v === true) ? 'Y' : 'N'; }},
	    	{name: 'UPFLNI', type: 'string', convert: function(v, rec) { return (v === "Y" || v === "S" || v === true) ? 'Y' : 'N'; }},
			{name: 'UPFG01', type: 'string', convert: function(v, rec) { return (v === "Y" || v === "S" || v === true) ? 'Y' : 'N'; }},	    		    	
	    'k_UPPRAC', 'k_UPCMOD', 'UPPRAC', 'UPCMOD', 'UPCMOD_D', 'UPPARA'		
	    ]		    		    	
	});



    var storeProfileModule = Ext.create('Ext.data.Store', {
        model: 'Writer.ProfileModule',
        autoLoad: false,
        autoSync: true,
        proxy: {
            type: 'ajax',
            api: {
                read: '<? echo ROOT_PATH ?>/module/admin/app.php?obj=profiles&fn=profile_module_view_jd',
                create: '<? echo ROOT_PATH ?>/module/admin/app.php?obj=profiles&fn=profile_module_create_jd',
                update: '<? echo ROOT_PATH ?>/module/admin/app.php?obj=profiles&fn=profile_module_update_jd',
                destroy: '<? echo ROOT_PATH ?>/module/admin/app.php?obj=profiles&fn=profile_module_destroy_jd'
            },
            extraParams: {
            	UPPRAC: ''
        	},
            reader: {
                type: 'json',
                successProperty: 'success',
                root: 'data',
                messageProperty: 'message'
            },
            writer: {
                type: 'json',
                writeAllFields: true,
                root: 'data'
            },
            listeners: {
                exception: function(proxy, response, operation){
                    Ext.MessageBox.show({
                        title: 'REMOTE EXCEPTION',
                        msg: operation.getError(),
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }
            }
        }
    });





Ext.define('Writer.ProfileModule.Form', {
    extend: 'Ext.form.Panel',
    alias: 'widget.writerProfileModuleform',

    requires: ['Ext.form.field.Text'],

    initComponent: function(){
        this.addEvents('create');
        Ext.apply(this, {
            activeRecord: null,
            iconCls: 'icon-user',
            frame: true,
            title: 'Permessi modulo',
            defaultType: 'textfield',
            bodyPadding: 5,
            fieldDefaults: {
                anchor: '100%',
                labelAlign: 'right',
                labelWidth: 130,
            },
            items: [
            {name: 'k_UPCMOD', xtype: 'hidden'}, {name: 'k_UPPRAC', xtype: 'hidden'},
            {
							name: 'UPPRAC',
							xtype: 'combo',
							fieldLabel: 'Profilo',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: false,		
						   /*	store: {
                                autoLoad: true,
    			            	pageSize: 1000,            	
    							proxy: {
    					            type: 'ajax',
                                    
    					            url : '<? echo ROOT_PATH ?>/module/base/acs_get_tab_TA.php?fn=get_data',
    					            reader: {
    					                type: 'json',
    	                                method: 'POST',		
    					                root: 'root',
    					                totalProperty: 'totalCount'
    					            },
                                    actionMethods: {
        							          read: 'POST'
        							        },
                                    extraParams: {
                   		    		    		tab: 'PRAC'
                   		    		       },               						        
    				        		doRequest: personalizza_extraParams_to_jsonData
    					        },       
    								fields: ['id', 'text']          	
			              }*/												
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('PRAC'), ""); ?>	
								    ] 
							}					 
			}, {
							name: 'UPCMOD',
							xtype: 'combo',
							fieldLabel: 'Modulo',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
						   	allowBlank: false,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('MODUL'), ""); ?>	
								    ] 
							}						 
			}, {
                fieldLabel: 'Disabilita menu',
                name: 'UPFLSI',
                xtype: 'checkboxfield',
                inputValue: 'Y',
				//uncheckedValue: 'N'                
            }, {
                fieldLabel: 'Disabilita Info',
                name: 'UPFG01',
                xtype: 'checkboxfield',
                inputValue: 'Y',
				//uncheckedValue: 'N'                
            }, {
                fieldLabel: 'Accesso statistiche',
                name: 'UPFLSS',
                xtype: 'checkboxfield',
                inputValue: 'Y',
				//uncheckedValue: 'N'                
            }, {
                fieldLabel: 'Modifica parametri',
                name: 'UPFLMP',
                xtype: 'checkboxfield',
                inputValue: 'Y',
				//uncheckedValue: 'N'                
            }, {
                fieldLabel: 'Nascondi importi',
                name: 'UPFLNI',
                xtype: 'checkboxfield',
                inputValue: 'Y',
				//uncheckedValue: 'N'                
            }, {
                fieldLabel: 'Parametri',
                name: 'UPPARA',
                xtype: 'textareafield'                
            }],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                items: ['->', {
                    iconCls: 'icon-save',
                    itemId: 'save',
                    text: 'Salva',
                    disabled: true,
                    scope: this,
                    handler: this.onSave
                }, {
                    iconCls: 'icon-user-add',
                    text: 'Crea',
                    scope: this,
                    handler: this.onCreate
                }, {
                    iconCls: 'icon-reset',
                    text: 'Reset',
                    scope: this,
                    handler: this.onReset
                }]
            }]
        });
        this.callParent();
    },

    setActiveRecord: function(record){
        this.activeRecord = record;
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },

    onSave: function(){
        var active = this.activeRecord,
            form = this.getForm();

console.log(0)            
        if (!active) {
            return;
        }
console.log(1)        
        if (form.isValid()) {
console.log(2)        
        	active.data = form.getValues();
            form.updateRecord(active);
            this.onReset();
            this.up('window').close();
        }
    },

    onCreate: function(){
        var form = this.getForm();

        if (form.isValid()) {
            this.fireEvent('create', this, form.getValues());
            this.up('window').close();                        
        }

    },

    onReset: function(){
        this.setActiveRecord(null);
        this.getForm().reset();
    }
});






Ext.define('Writer.ProfileModule.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.profilemodulegrid',

    requires: [
        'Ext.grid.plugin.CellEditing',
        'Ext.form.field.Text',
        'Ext.toolbar.TextItem'
    ],


    listeners: {
    	
	  celldblclick: {								
		  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
			rec = iView.getRecord(iRowEl);		  	
		  	
			print_w2 = new Ext.Window({
					  width: 600
					, height: 600
					, minWidth: 400
					, minHeight: 400
					, plain: true
					, title: 'Configura permessi modulo'
					, layout: 'fit'
					, border: true
					, closable: true
					, items: {
			            itemId: 'formProfileModule',
			            flex: 1,            
			            xtype: 'writerProfileModuleform',
			            margins: '0 0 0 0',
			            listeners: {
			                create: function(form, data){
			                    storeProfili.insert(0, data);
			                }
			            }
			        }

				});	
			print_w2.show();
			print_w2.child('#formProfileModule').setActiveRecord(rec || null);
		  			  	
		  }
	  }
	},



    initComponent: function(){

        this.editing = Ext.create('Ext.grid.plugin.CellEditing');

        Ext.apply(this, {
            iconCls: 'icon-grid',
            frame: true,
            plugins: [this.editing],
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    iconCls: 'icon-add',
                    text: 'Aggiungi',
                    scope: this,
                    handler: this.onAddClick
                }, {
                    iconCls: 'icon-delete',
                    text: 'Elimina',
                    disabled: true,
                    itemId: 'delete',
                    scope: this,
                    handler: this.onDeleteClick
                }]
            }],
            columns: [{
                text: 'Modulo',
                sortable: true,
                flex: 1,
                dataIndex: 'UPCMOD'
            }, {
                text: 'Descrizione',
                sortable: true,
                flex: 2,
                dataIndex: 'UPCMOD_D'
            }, {
                text: 'Dis.menu',
                sortable: true,
                width: 60,
                dataIndex: 'UPFLSI'
            }, {
                text: 'Dis.Info',
                sortable: true,
                width: 60,
                dataIndex: 'UPFG01'
            }, {
                text: 'Acc.stat',
                sortable: true,
                width: 60,
                dataIndex: 'UPFLSS'
            }, {
                text: 'Mod.param.',
                sortable: true,
                width: 60,
                dataIndex: 'UPFLMP'
            }, {
                text: 'Nasc.imp.',
                sortable: true,
                width: 60,
                dataIndex: 'UPFLNI'
            }]
        });
        this.callParent();
        this.getSelectionModel().on('selectionchange', this.onSelectChange, this);        
    },


    onSelectChange: function(selModel, selections){
        this.down('#delete').setDisabled(selections.length === 0);
    },

    onDeleteClick: function(){
        var selection = this.getView().getSelectionModel().getSelection()[0];
        if (selection) {
            this.store.remove(selection);
        }
    },
    
    onAddClick: function(){
			print_w2 = new Ext.Window({
					  width: 600
					, height: 600
					, minWidth: 400
					, minHeight: 400
					, plain: true
					, title: 'Crea nuovo permesso'
					, layout: 'fit'
					, border: true
					, closable: true
					, items: {
			            itemId: 'formProfileModule',
			            flex: 1,            
			            xtype: 'writerProfileModuleform',
			            margins: '0 0 0 0',
			            listeners: {
			                create: function(form, data){
			                    storeProfileModule.insert(0, data);
			                }
			            }
			        }

				});

				
			var new_rec = new Writer.ProfileModule({
			        	UPPRAC: this.getStore().getProxy().extraParams.UPPRAC
			        })				
					
			print_w2.show();
			console.log(new_rec);
			console.log(print_w2);
			print_w2.child('#formProfileModule').setActiveRecord(new_rec);

    }    

});


								<?php echo Profiles::out_Writer_sotto_main("profiles") ?>
								<?php echo Profiles::out_Writer_main("Lista profili", "profiles", Profiles::out_main_add_grid_selectionchange()) ?>
								<?php echo Profiles::out_Writer_window("Tabella profili") ?>







								} //handler function()
			            }
,


<?php }else{?>
{html: 'ATTENZIONE!!! <br> Non hai i permessi <br> di Amministratore <br>della Sicurezza', frame: true},
<?php }?>
// ***************************************************************************************
// MODULI
// ***************************************************************************************
			            {
			                text: 'Moduli',
			                scale: 'large',			                 
			                iconCls: 'icon-module-32',
			                iconAlign: 'top',			                
			                 handler : function() {

								<?php echo Modules::out_Writer_Form(Modules::out_Writer_Form_initComponent(), "modules") ?>
								<?php echo Modules::out_Writer_Grid(Modules::out_Writer_Grid_initComponent_columns(), "modules") ?>
								<?php echo Modules::out_Writer_Model("modules") ?>
								<?php echo Modules::out_Writer_Store("modules") ?>
								<?php echo Modules::out_Writer_sotto_main("modules") ?>
								<?php echo Modules::out_Writer_main("Lista moduli", "modules") ?>								
								<?php echo Modules::out_Writer_window("Tabella moduli") ?>							

								} //handler function()
			            }
			            
			            
			            
			            
			            , {
			                text: 'Permessi utente',
			                scale: 'large',			                 
			                iconCls: 'icon-user-32',
			                iconAlign: 'top',			                
			                 handler : function() {
									acs_show_win_std('Permessi per modulo/utente', 
										'<?php echo ROOT_PATH?>/module/admin/acs_permessi_utente.php?fn=open_tab', {}, 950, 500, null, 'icon-shopping_cart_gray-16');
								} //handler function()
			            }			            
			            
			            
			            			            
			            
			            ]
			            
			   }, {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Supporto',
			            items: [
			            
						{
			                text: 'Connessioni',
			                scale: 'large',			                 
			                iconCls: 'icon-game_pad-32',
			                iconAlign: 'top',			                
			                 handler : function() {
									acs_show_win_std('Connessioni utenti', 'module/admin/acs_connessioni_utenti.php?fn=json_main', {}, 980, 550, null, 'icon-game_pad-16');
								} //handler function()
			            },			            
			            
						{
			                text: 'Guida',
			                scale: 'large',			                 
			                iconCls: 'icon-info_blue-32',
			                iconAlign: 'top',			                
			                 handler : function() {
			                 	window.open(' <?php echo $s->get_doc_link("Login", "pdf"); ?> ');
			                 }
			            }     			            
			            
			            
			            ]
			                                    
			   }, {
			            xtype: 'buttongroup',
			            columns: 50,
			            title: 'Gestione tabelle',
			            items: [
			            
						{
			                text: 'Vendita',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                handler : function() {
									acs_show_win_std('', '<?php echo ROOT_PATH?>/module/desk_vend/acs_gestione_tabelle.php?fn=open_grid', {open_parameters : {tp_tab : 'ven'}}, 1150, 550, null, '');
								} //handler function()
			            },{
			                text: 'Acquisti',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                handler : function() {
									acs_show_win_std('', '<?php echo ROOT_PATH?>/module/desk_vend/acs_gestione_tabelle.php?fn=open_grid', {open_parameters : {tp_tab : 'acq'}}, 1150, 550, null, '');
								} //handler function()
			            },{
			                text: 'Dati di base',
			                scale: 'large',			                 
			                iconCls: 'icon-database_active-32',
			                iconAlign: 'top',			                
			                handler : function() {
									acs_show_win_std('', '<?php echo ROOT_PATH?>/module/desk_vend/acs_gestione_tabelle.php?fn=open_grid', {open_parameters : {tp_tab : 'art'}}, 1150, 550, null, '');
								} //handler function()
			            }			            
			          			            
			            
			            
			            ]
			                                    
			   }
			   
<?php  if ($abilita_gestione_variabili_di_sessione == 'Y'){ ?>			   
			   , {
    	            xtype: 'buttongroup',
    	            columns: 50,
    	            title: 'Sessione',
    	            items: [
    					{
    		                text: 'Variabili',
    		                scale: 'large',			                 
    		                iconCls: 'icon-database_active-32',
    		                iconAlign: 'top',			                
    		                handler : function() {
    						  acs_show_win_std('', '<?php echo ROOT_PATH?>/module/base/session_vars.php?fn=open_form', {}, 1150, 550, null, '');
    						} //handler function()
    		            }			            
    	            ]
			   }
<?php } ?>			   
			   
			   
			]
		}
	]		
}            