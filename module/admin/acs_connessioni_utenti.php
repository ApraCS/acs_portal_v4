<?php

require_once("../../config.inc.php");

/* TODO: verificare permessi Admin */


function to_AS_date_from_format($format, $data){
	switch ($format){
		case "m-d-Y":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[0], $d_ar[1], $d_ar[2]);
			return to_AS_date($d);
		case "Y-m-d":
			$d_ar = explode("-", $data);
			$d = mktime(0, 0, 0, $d_ar[1], $d_ar[2], $d_ar[0]);
			return to_AS_date($d);

	}
}



/***********************************************************************************
 * Dettaglio campagna (per articolo)
***********************************************************************************/
if ($_REQUEST['fn'] == 'get_json_data'){
	global $conn, $cfg_mod_Admin;
	$sql = "SELECT * FROM {$cfg_mod_Admin['file_utenti']}  WHERE UTFLUT <> 'T' ORDER BY UTCUTE";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {
		if ($r['UTDTFSP'] == oggi_AS_date() && $r['UTDTFS'] == 0)
			$r['STATO'] = 'CONNESSO';
		else 
			$r['STATO'] = 'DISCONNESSO';
		$r['UTCUTE'] = acs_u8e($r['UTCUTE']);
		$ar[] = $r;
	}	
	
	function cmp_by_STATO($a, $b)
	{
		//ordino per stato (CONNESSO | DISCONNESSO)
		if ($a["STATO"] != $b["STATO"])
			return strcmp($a["STATO"], $b["STATO"]);
		
		//per data/ora ultima operazione (decrescente)
		if (sprintf("%014s", $a["UTDTFSP"] . $a["UTORFSP"]) != sprintf("%014s", $b["UTDTFSP"] . $b["UTORFSP"]))
			return strcmp(sprintf("%014s", $b["UTDTFSP"] . $b["UTORFSP"]), sprintf("%014s", $a["UTDTFSP"] . $a["UTORFSP"]));		
		
		//per codice utente		
		return strcmp(strtoupper($a["UTCUTE"]), strtoupper($b["UTCUTE"]));		
	}	
	
	//ordino per stato (CONNESSO | DISCONNESSO)
	usort($ar, "cmp_by_STATO");
	echo acs_je($ar);
	exit;
}


/***********************************************************************************
 * Dettaglio campagna (per articolo)
***********************************************************************************/
if ($_REQUEST['fn'] == 'json_main'){
	?>

{"success": true, "items":
{
	xtype: 'grid',

	dockedItems: [{
	    xtype: 'toolbar',
	    dock: 'bottom',
	    items: [{
	        xtype: 'button',
	        text: 'Storico connessioni',
	        labelAlign: 'left',
	        iconCls: 'icon-game_pad-32',
	        scale: 'large',
	        handler : function(){
	        	acs_show_win_std('Storico connessioni', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_storico_connessioni', {}, 500, 550, null, 'icon-calendar-16', 'Y');
	        } 
	    }]
	}],	
	
	
	store: Ext.create('Ext.data.Store', {
			
		groupField: 'RDTPNO',
		autoLoad:true,
		proxy: {
			url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
			type: 'ajax',
			reader: {
				type: 'json',
				root: 'root'
			},
		},
		fields: ['UTCUTE', 'UTDESC', 'UTDTIS', 'UTORIS', 'UTDTFS', 'UTORFS', 'UTDTFSP', 'UTORFSP', 'UTNCON', 'STATO', 'UTCMOD']
	}),

	columns: [
	{
		header   : 'User',
		dataIndex: 'UTCUTE',
		width     : 100,
	}, {
		header   : 'Denominazione',
		dataIndex: 'UTDESC',
		flex	 : 10
	}, {
		header   : '<img src=<?php echo img_path("icone/48x48/game_pad.png"); ?>  height=25>',
		dataIndex: 'STATO',
		width	 : 40,
		renderer: function(value, p, record){
    			    	if (value == 'CONNESSO') return '<img src=<?php echo img_path("icone/48x48/game_pad.png") ?> width=18>';
    			    	else return '<img src=<?php echo img_path("icone/48x48/coffee.png") ?> width=18>';			    				    	
    			    	}
	}, {header: 'Ultimo login',
			 columns: [
			 	{header: 'Data', dataIndex: 'UTDTIS', width: 70, renderer: date_from_AS},
				{header: 'Ora', dataIndex: 'UTORIS', width: 45, renderer: time_from_AS},
			 ]
	}, {header: 'Ultima operazione',
			 columns: [
			 	{header: 'Data', dataIndex: 'UTDTFSP', width: 70, renderer: date_from_AS},
				{header: 'Ora', dataIndex: 'UTORFSP', width: 45, renderer: time_from_AS},
				{header: 'Modulo', dataIndex: 'UTCMOD', width: 90}				
			 ]
	}, {header: 'Ultimo logout',
			 columns: [
				{header: 'Data', dataIndex: 'UTDTFS', width: 70, renderer: date_from_AS},							 	
			 	{header: 'Ora', dataIndex: 'UTORFS', width: 45, renderer: time_from_AS}				
			 ]
	}, {
		header   : 'Numero<br>connessioni',
		dataIndex: 'UTNCON',
		width	 : 75, align: 'right'
	}]

	, listeners: {
		afterrender: function (comp) {
		},
	 	
		celldblclick: {
			fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
				rec = iView.getRecord(iRowEl);
				iView.getStore().load();
			}
		}
			
	}
		
	, viewConfig: {
		getRowClass: function(rec, index) {
			if (rec.get('STATO') == 'CONNESSO')
				return ' segnala_riga_verde';
		}
	}
		
	 
}
}
<?php exit; } ?>
<?
// ******************************************************************************************
// get_events
// ******************************************************************************************
 if ($_REQUEST['fn'] == 'cal_get_events'){
	
	//data iniziale e di fine
	$from_calendar_format = 'Y-m-d';
	$d_start 	= to_AS_date_from_format($from_calendar_format, $_REQUEST["startDate"]);
	$d_end 		= to_AS_date_from_format($from_calendar_format, $_REQUEST["endDate"]);

	$d_start_db = date('Y-m-d', strtotime($d_start));
	$d_end_db = date('Y-m-d', strtotime($d_end));	
	
	$sql_FROM 	= " FROM {$cfg_mod_Admin['file_app_log_history']} LH
			         INNER JOIN {$cfg_mod_Admin['file_utenti']} UT
			            ON LH.LHUSER = UT.UTCUTE 
					";	
	$sql_WHERE 	= " WHERE LHDATE >= '{$d_start_db}' AND LHDATE <= '{$d_end_db}' AND UT.UTFLUT <> 'T' ";	
	$sql = "SELECT * " . $sql_FROM . $sql_WHERE;

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);	
	
	$fine_ora = array();
	$durata_in_minuti = 30;
	while ($r = db2_fetch_assoc($stmt)) {

		if (!isset($fine_ora["{$r['LHDATE']}"]))
			$fine_ora["{$r['LHDATE']}"] = date('His', strtotime(print_ora('000000', 6, 'Y')));
		
		$ora_start	= date('His', strtotime(print_ora($fine_ora["{$r['LHDATE']}"], 6, 'Y')));
		$ora_end	= date('His', strtotime(print_ora($ora_start, 6, 'Y')) + $durata_in_minuti*60);
		
		//aggiorno fine ora
		$fine_ora["{$r['LHDATE']}"] = date('His', strtotime(print_ora($ora_end, 6, 'Y')));		
	
		$e = array();
		$e['id'] = (int)$r['LHID']; //id calendario
		$e['title'] = "<b>" . trim($r['LHUSER']) . " (#{$r['LHCONT']})</b>";
		$e['start'] = $r['LHDATE'] . " " . print_ora($ora_start, 6, 'Y');
		$e['end'] 	= $r['LHDATE'] . " " . print_ora($ora_end, 6, 'Y');
		$e['notes'] = "Have fun";
		$e['risorsa'] = '';
		$e['stabilimento'] = '';
		$e['cid'] = $_SESSION['admin']['stor_conn_user_cal_id'][trim($r['LHUSER'])];
	
		$ret['events'][] = $e;
	}	
 echo acs_je($ret);	
exit; } ?>
<?php



/***********************************************************************************
 * Calendario storico connessioni
***********************************************************************************/

//costruisco l'elenco dei calendari (itinerari)
$sql = "SELECT MAX(LHID), LHUSER 
		FROM {$cfg_mod_Admin['file_app_log_history']} LH
         INNER JOIN {$cfg_mod_Admin['file_utenti']} UT
            ON LH.LHUSER = UT.UTCUTE
		WHERE LHUSER is not null
		  AND UT.UTFLUT <> 'T' 
		GROUP BY LHUSER
        ORDER BY UCASE(LH.LHUSER)";
$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);

$_SESSION['admin']['stor_conn_user_cal_id'] = array();
while ($r = db2_fetch_assoc($stmt)) {
	$c++; $ar_it[] = array("id" => $c, "title" => trim($r['LHUSER']), "color" => $c, "hidden"=>true);

	//memorizzo in session l'id del calendario	
	$_SESSION['admin']['stor_conn_user_cal_id'][trim($r['LHUSER'])] = $c;
}

$calendar_store = acs_je(array("data" => array("calendars" => $ar_it)));






if ($_REQUEST['fn'] == 'get_json_storico_connessioni'){ ?>
{"success":true, "items": [
 {
  xtype: 'panel',
  closable: false,
  layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
  calendarStore: new Extensible.calendar.data.MemoryCalendarStore(<?php echo $calendar_store; ?>),    
  
  listeners: {                    
    afterrender: function (comp) {
    				var use_calsendar_store = null;
    						//uso sempre gli stessi calendari
                    		elenco_calendari = comp.query('[xtype="extensible.calendarlist"]');
					        	Ext.each(elenco_calendari, function(rf) {
									call = comp.down('[xtype="extensible.calendarpanel"]');
									rf.setStore(call.calendarStore);					        							        		
					        		rf.refresh();
					        	});
                    		calendarios = comp.query('[xtype="extensible.calendarpanel"]');
					        	Ext.each(calendarios, function(rf) {					        	
					        		rf.setStore(comp.calendarStore);					        		
					        		});					        	
					        	    
                    		comp.show();
                     		
                 	},
  }
  
  
  , items: [
  		
  
 {
	xtype: 'panel',
	layout: {type: 'hbox', border: false, pack: 'start', align: 'stretch'},
	flex: 1,	
    collapsible: false,
    collapsed: false,			
	items: [	  
    
    
		    //sidebar con calendario data e elenco calendario (itinerari)
			{
				xtype: 'panel',
				layout: {type: 'vbox', border: false, pack: 'start', align: 'stretch'},
				width: 165,
												
				items: [
					{
                        xtype: 'extensible.calendarlist',
                        autoScroll: true,
                        title: 'Utenti',
                		collapsible: false,
                		collapsed: false,	                        
                        store: new Extensible.calendar.data.MemoryCalendarStore(),
                        border: false,
                        flex: 1,
                        
						dockedItems: [{
						    xtype: 'toolbar',
						    dock: 'bottom',
						    flex: 1,
						    items: [{
						        xtype: 'button',
						        text: 'Mostra tutti',
						        handler : function(){
		                    		cl = this.up('window').query('[xtype="extensible.calendarlist"]');
							        	Ext.each(cl, function(rf) {
											rf.store.each(function(cal){
												cal.set('IsHidden', false);
											});
											rf.refresh();
							        	});						        	
						        	
						        } 
						    }, {
						        xtype: 'button',
						        text: 'Nascondi tutti',
						        handler : function(){
		                    		cl = this.up('window').query('[xtype="extensible.calendarlist"]');
							        	Ext.each(cl, function(rf) {
											rf.store.each(function(cal){
												cal.set('IsHidden', true);
											});
											rf.refresh();
							        	});						        	
						        	
						        } 
						    }]
						}],	
                        
                        
                    }
				]
			}, 	    
    
    
    	// **************************************************************************
    	// calendario
    	// **************************************************************************
  
			{
		        xtype: 'extensible.calendarpanel',		        
		        flex: 1,
		        border: false,
		        frame: true,
				title: '',
				
                collapsible: false,
                collapsed: false,	
                
				enableEditDetails: false,
				readOnly: true,  
				
				showMonthView: true,
				showDayView: false,
				showWeekView: true,
				showMultiWeekView: false,				               

				//definizione calendari
        		calendarStore: new Extensible.calendar.data.MemoryCalendarStore(<?php echo $calendar_store; ?>),
        						
        			//recupero eventi
					eventStore: new Extensible.calendar.data.EventStore({

					       autoLoad: true,
        				   autoSync: true,

		                    proxy: {
		                    	url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
		                        type: 'ajax',
								method: 'POST',
								
								extraParams: <?php echo acs_raw_post_data() ?>,
								
								/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */					
					            doRequest: personalizza_extraParams_to_jsonData,	   								
								
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'events'
								},
						            		                        
		                        
					            writer: {
					                type: 'json',
					                writeAllFields: true
					            },		
					            
					            api: {
					                read: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_get_events',
					                create: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_crt_events',
					                update: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_upd_events',
					                destroy: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=cal_wr_del_events',
					            },
					            				                        
		                        
		                    }					          
					    }),


                    activeItem: 2, // month view
                    lastActiveItem: 2, //per memorizzare il tipo di visualizzazione e gestire eventi se passo da week a month
                    
                    showNavBar: true,
                    
                    weekViewCfg: {
                        showHeader: true,
                        showWeekLinks: true,
                        showWeekNumbers: true,                        
						// startDate: js_date_from_AS(<?php echo $data; ?>),
						viewStartHour  : 0,
						scrollStartHour: 0,
						showTime: false,
						showTodayText: false,						
						showHourSeparator: true,
						hourHeight: 50,
						startDay: 1, //lunedi
						abcdeeeee: 100							
                    },

                    
					monthViewCfg: {
                        startDay: 1, //lunedi
                        showTime: false,
                        showTodayText: false,
                        cccccc: 300
                    },                    
                    
                    dayViewCfg: {
                        showHeader: false,
                        showWeekLinks: true,
                        showWeekNumbers: true,
						// startDate: js_date_from_AS(<?php echo $data; ?>),
						viewStartHour  : 0,
						scrollStartHour: 0,
						ddGroup: 'DayViewDD',
						showTime: false,
						showTodayText: false,						
						showHourSeparator: true,
						hourHeight: 60,
						dddddddd: 400							
                    }                      
			    
                , listeners: {
                
                		'itemcontextmenu' : function(grid, rec, node, index, event) {
                			console.log('dx click');
                		},
                
                        'eventclick': {
                			fn: function(panel, rec, el){	
								return false;					    		
							}	   						
                		},

                		
                		'viewchange': {
                			fn: function(panel, calView, ext){
                			
                				if (calView.xtype == 'extensible.weekview' && panel.lastActiveItem != 1){
                					panel.lastActiveItem = 1;                				
                					//show su tutti i calendari
		                    		cl = panel.up('window').query('[xtype="extensible.calendarlist"]');
							        	Ext.each(cl, function(rf) {
											rf.store.each(function(cal){
												cal.set('IsHidden', false);
											});
											rf.refresh();
							        	});              
                				}
                				
                				if (calView.xtype == 'extensible.monthview' && panel.lastActiveItem != 2){
                					panel.lastActiveItem = 2;
                					//hide su tutti i calendari
		                    		cl = panel.up('window').query('[xtype="extensible.calendarlist"]');
							        	Ext.each(cl, function(rf) {
											rf.store.each(function(cal){
												cal.set('IsHidden', true);
											});
											rf.refresh();
							        	});                					                				
                				}                									    	
							}                		
                		}
                		                	
                }			    
		        , editModal: true
		    } //calendar panel 
		    
		]
	}	    
  ]
  
 }
]
}
<?php exit; } ?>