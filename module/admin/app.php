<?php

require_once "../../config.inc.php";

switch ($_REQUEST['obj']){
	case "users":
		$obj = new Users();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;
	case "usersattivita":
		$obj = new UsersAttivita();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
		break;	
	case "profiles":		
		$obj = new Profiles();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;
	case "modules":		
		$obj = new Modules();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "vettori":		
		$obj = new Vettori();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "auto":		
		$obj = new Auto();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "casse":		
		$obj = new Casse();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;
	case "indicirottura":
		$obj = new SpedIndiciRottura();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
		break;		
	case "itinerari":		
		$obj = new Itinerari();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "trasportatori":		
		$obj = new Trasportatori();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "trasportatoriitinerari":
		$obj = new TrasportatoriItinerari();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
		break;	
	case "staticarico":		
		$obj = new StatiCarico();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "tipologiespedizione":		
		$obj = new TipologieSpedizione();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "tipologieordine":
		$obj = new TipologieOrdine();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
		break;	
	case "tipologietrasporto":		
		$obj = new TipologieTrasporto();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "tpstartpoint":		
		$obj = new TpStartPoint();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;
	case "itve":		
		$obj = new ItVe();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;			
	case "listinotrasportatori":		
		$obj = new ListinoTrasportatori();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "areespedizione":		
		$obj = new AreeSpedizione();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;
	case "spedmanutenzionecosti":		
		$obj = new SpedManutenzioneCosti();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;			
	case "autorizzazioni":
		$obj = new Autorizzazioni();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;	
	case "capacitaproduttiva":
		$obj = new SpedCapacitaProduttiva();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;
	case "forzaturaatp":
		$obj = new SpedForzaturaAtp();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
	break;
	case "parametrioperatorioe":
		$obj = new SpedParametriOperatoriOE();
		$fn = $_REQUEST['fn'];
		$obj->$fn($_REQUEST);
		break;
		
	
	//DESK_ACQ ----------------------------------------
		case "deskacq_areespedizione":
			$obj = new DeskAcqAreeSpedizione();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;	

		case "deskacq_vettori":
			$obj = new DeskAcqVettori();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;		

		case "deskacq_auto":
			$obj = new DeskAcqAuto();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;

		case "deskacq_casse":
			$obj = new DeskAcqCasse();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;

		case "deskacq_itinerari":
			$obj = new DeskAcqItinerari();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;		
				
		case "deskacq_trasportatori":
			$obj = new DeskAcqTrasportatori();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;

		case "deskacq_itve":
			$obj = new DeskAcqItVe();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;			

		case "deskacq_staticarico":
			$obj = new DeskAcqStatiCarico();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;
							
		case "deskacq_tipologiespedizione":
			$obj = new DeskAcqTipologieSpedizione();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;

		case "deskacq_tipologietrasporto":
			$obj = new DeskAcqTipologieTrasporto();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;

		case "deskacq_tipologieordine":
			$obj = new DeskAcqTipologieOrdine();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;

		case "deskacq_capacitaproduttiva":
			$obj = new DeskAcqSpedCapacitaProduttiva();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;

		case "deskacq_autorizzazioni":
			$obj = new DeskAcqAutorizzazioni();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;
						
		case "deskacq_tpstartpoint":
			$obj = new DeskAcqTpStartPoint();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;
			
		case "deskacq_fornitori":
			$obj = new DeskAcqFornitori();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;
		case "deskacq_risorse":
			$obj = new DeskAcqRisorse();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;					
		case "deskacq_stabilimenti":
			$obj = new DeskAcqStabilimenti();
			$fn = $_REQUEST['fn'];
			$obj->$fn($_REQUEST);
			break;			
		
}


?>
