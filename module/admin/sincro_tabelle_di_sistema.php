<?php

require_once("../../config.inc.php");
require_once 'sincro_tabelle_di_sistema_data.php';

$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();


if($m_params->open_parameters->tp_tab == 'ven'){
   $ar_data_tab = $tab_WPI0TA0;
   $ta0 = $cfg_mod_Spedizioni['file_tabelle'];
}else if($m_params->open_parameters->tp_tab == 'acq'){
    $ar_data_tab = $tab_WPI1TA0;
   $ta0 = $cfg_mod_DeskAcq['file_tabelle'];
}else{
   $ar_data_tab = $tab_WPI2TA0;
   $ta0 = $cfg_mod_DeskArt['file_tabelle'];
}



if ($_REQUEST['fn'] == 'exe_confirm_tab'){
    
    foreach($m_params->rows as $r){
        
        
        if($r->ope == 'I'){   //INSERIMENTO NUOVE RIGHE IN CFGTA
                        
            $ar_ins = array();
            $ar_ins['TADT'] 	= $id_ditta_default;
            $ar_ins['TATAID'] 	= 'CFGTA';
            $ar_ins['TAKEY1'] 	= $r->tataid;
            $ar_ins['TADESC'] 	= $r->descrizione;
            
            $ar_ins['TAUSGE'] 	= $auth->get_user();
            $ar_ins['TADTGE']   = oggi_AS_date();
            $ar_ins['TAORGE'] 	= oggi_AS_time();
            
            $sql = "INSERT INTO {$ta0}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_ins);
            
            
        }elseif($r->ope == 'U'){ //UPDATE, AGGIORNAMENTO DESCRIZIONE
            
                       
            $ar_upd = array();
            $ar_upd['TADESC'] 	= utf8_decode($r->value_desc);
            
            $ar_upd['TAUSGE'] 	= $auth->get_user();
            $ar_upd['TADTGE']   = oggi_AS_date();
            $ar_upd['TAORGE'] 	= oggi_AS_time();
            
            $sql = "UPDATE {$ta0} TA
            SET " . create_name_field_by_ar_UPDATE($ar_upd) . "
            WHERE TADT = '{$id_ditta_default}' AND TATAID = 'CFGTA' AND TAKEY1 = '{$r->tataid}'";
            
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, $ar_upd);
            
        }else{   //DELETE LE RIGHE NEL TATAID, NON SERVONO 
            
            $sql = "DELETE FROM {$ta0} TA
                    WHERE TADT = '{$id_ditta_default}' AND TATAID = '{$r->tataid}'";
                      
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt);
        } 
        
                
    }
    
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    
    exit;
    
    
}


if ($_REQUEST['fn'] == 'get_data_tab'){
    
    
    //conteggio delle diverse tataid
    $sql_c = "SELECT TATAID, COUNT(*) AS C_ROW
              FROM {$ta0} TA
              WHERE TADT='$id_ditta_default' AND TATAID = ?
              GROUP BY TATAID";
    
    $stmt_c = db2_prepare($conn, $sql_c);
    echo db2_stmt_errormsg();
    
  
    //tutte le tataid presenti nel TA0, qui non ho la descrizione
    $sql_t = "SELECT DISTINCT TATAID
    FROM {$ta0} TA
    WHERE TADT='$id_ditta_default'";
    
    
    $stmt_t = db2_prepare($conn, $sql_t);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_t);
    
    $tataid_from_ta0 = array();
    while ($row = db2_fetch_assoc($stmt_t)) {
        $tataid_from_ta0[trim($row['TATAID'])] = acs_u8e(trim($row['TATAID']));
    }
   
    //LA TABELLA DELLE TABELLE
    $sql_cfg = "SELECT TAKEY1, TADESC
    FROM {$ta0} TA
    WHERE TADT='$id_ditta_default' AND TATAID = 'CFGTA'";
    
    
    $stmt_cfg = db2_prepare($conn, $sql_cfg);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_cfg);
 
    $values_from_cfgta = array();
    while ($row = db2_fetch_assoc($stmt_cfg)) {
        $values_from_cfgta[trim($row['TAKEY1'])] = acs_u8e(trim($row['TADESC']));
    }
      
  
    $data_to_grid = array();
    //scorro i valori del mio Array PHP
    foreach ($ar_data_tab  as $k => $v){

        $result = db2_execute($stmt_c, array($k));
        $row_c = db2_fetch_assoc($stmt_c);
     
        if(!array_key_exists($k, $values_from_cfgta)){
           $tataid = $k;
           $descrizione = $v;
           $stato = "NW";
           $ope = "I";
           $value_desc = '';
           $errore = "Solo nell'Array PHP";
        }else{ 
            
            if(!in_array($v, $values_from_cfgta)){
                $tataid = $k;
                $descrizione = "{$v} <br> {$values_from_cfgta[$k]}";
                $value_desc = $v;
                $stato = 'DD';
                $ope = "U";
                $errore = 'Descrizione diversa';
            }else{
               $tataid = $k;
               $descrizione = $v;
               $value_desc = '';
               $stato = "OK";
               $ope = "NA";
               $errore = "Presente nella CFGTA";
            }
        }
        
                 
        $r = array(
            'tataid'       => $tataid,
            'descrizione'  => $descrizione,
            'value_desc'   => $value_desc,
            'C_ROW'        => $row_c['C_ROW'],
            'stato'        => $stato,
            'ope'          => $ope,
            'errore'       => $errore
            
        );
                
        
        if($m_params->view == true){
           if($r['stato'] != 'OK')
            $data_to_grid[] = $r;
        }else{
          $data_to_grid[] = $r;
        }
    }
          
    //scorro i valori che ho presenti nelle tataid
    foreach ($tataid_from_ta0 as $k => $v) {
               
       $result = db2_execute($stmt_c, array($v));
       $row_c = db2_fetch_assoc($stmt_c);
       
       if(!array_key_exists($k , $values_from_cfgta) && !array_key_exists($k, $ar_data_tab)){
    
       $r = array(
           'tataid' => $v,
           'descrizione' => '',
           'value_desc' => '',
           'C_ROW' => $row_c['C_ROW'],
           'ope' => 'D',
           'stato' =>  'TA', 
           'errore' => "Solo nel TA0"
       );
       
       if($m_params->view == true){
           if($r['stato'] != 'OK')
               $data_to_grid[] = $r;
       }else{
           $data_to_grid[] = $r;
       }
       
       }
       
     }
 
     
    echo acs_je($data_to_grid);    
    exit();
 
}
    
if ($_REQUEST['fn'] == 'open_tab'){

        
        ?>

{"success":true, "items": [

        {
        
        xtype: 'grid',
        multiSelect: true,
        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
    	tbar: new Ext.Toolbar({
          items:['->',
           {   xtype: 'checkbox'
              , margin : 2
              , name: 'f_view'
              , boxLabel: 'Da gestire'
              , checked: true
              , inputValue: 'Y'
              , listeners: {change: function(comp, check){
                 var value_check = check;
                 console.log(value_check);
                 var grid = this.up('grid');
                 grid.store.proxy.extraParams.view = check;
		         grid.store.load();
		         
                 }
              }
            }
       		
         ]            
        }),	 
        store: {
						
					xtype: 'store',
					autoLoad:true,
				   					        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tab',
						type: 'ajax',
						 actionMethods: {
					          read: 'POST'
					        },
				           extraParams: {
				           open_parameters : <?php echo acs_je($m_params->open_parameters); ?>,
				           view : 'true'
	        				},
	        				
	        				doRequest: personalizza_extraParams_to_jsonData, 
				
						   reader: {
				            type: 'json',
							method: 'POST',						            
				            root: 'root'						            
				        }
					},
        			fields: [ 'tataid', 'descrizione', 'C_ROW', 'stato', 'ope', 'errore', 'value_desc']
    			},
    		
	        columns: [
							        
	       		 {header   : 'TATAID', dataIndex: 'tataid', flex: 1},
	       	     {header   : 'descrizione', dataIndex: 'descrizione', flex: 1},
	       		 {header   : '#', dataIndex: 'C_ROW', width: 50, aligh: 'right'},
	       		// {header   : 'St', dataIndex: 'stato', width: 50},
	       		 {header   : 'Ope', dataIndex: 'ope', width: 50},		
	       		 {header   : 'Errore', dataIndex: 'errore', flex : 1},	
	       		         
	            
	         ],  
			listeners: {
	         
	         	   beforeselect: function(grid, record, index, eOpts) {
            			if (record.get('ope').trim() == 'NA') {//replace this with your logic.
               				 return false;
            			}
       				 },
       			   celldblclick: {	           
    		           fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){		           
    		           		var col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
    					  	var rec = iView.getRecord(iRowEl);
    					  	var loc_win = this.up('window');
    					  	loc_win.fireEvent('DoppioClick', rec, loc_win);
    					        
    	            	}	          
    	           	},

	         }

			 ,viewConfig: {
	              getRowClass: function(record, index) {
		           if (record.get('stato') == 'NW')
		           		return ' segnala_riga_verde';
		           if (record.get('stato') == 'DD')
		           		return ' segnala_riga_giallo';				           		
		      	   if (record.get('stato') == 'TA')
		           		return ' segnala_riga_rosso';
		           if (record.get('stato') == 'OK')
		           		return ' segnala_riga_disabled';	
		          					           		
		           return '';																
		         }   
	    },
			    
			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                '->' ,
			     
			     
			     {
                     xtype: 'button',
               		 text: 'Conferma',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			         rows_selected = grid.getSelectionModel().getSelection();
                     list_rows_selected = [];
                            
                        for (var i=0; i<rows_selected.length; i++) {
			            list_rows_selected.push(rows_selected[i].data);}
			            
			            
			            
			              Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'operazione?', function(btn, text){																							    
								   if (btn == 'yes'){					        		
				      			             
                		             	Ext.Ajax.request({
                					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_confirm_tab',
                					        method     : 'POST',
                		        			jsonData: {
                		        				rows : list_rows_selected,
                		        				open_parameters : <?php echo acs_je($m_params->open_parameters); ?>
                		        			
                							},							        
                					        success : function(result, request){
                		            			grid.getStore().load();	
                		            		   
                		            		},
                					        failure    : function(result, request){
                					            Ext.Msg.alert('Message', 'No data to be loaded');
                					        }
                					    });

									  }
									});
			          
			            }

			     }]
		   }]	
         
	        																			  			
	            
        }  

]
}

<?php

exit;

}


