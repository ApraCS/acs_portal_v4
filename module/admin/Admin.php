<?php

class Admin {
	
	private $mod_cod = "ADMIN";
	private $mod_dir = "admin";	
	

	function get_doc_link($file_name, $extension){
		$ret = ROOT_PATH . "module/" . $this->mod_dir . "/" . "docs" . "/" . "ACS_Portal_IBMi_" . ucfirst(strtolower($this->mod_cod)) . "_" . 
					$file_name . "." . $extension; 
		return $ret;
	}
	
	
	 function find_TA_std($tab, $k1 = null){
 		global $conn;
		global $cfg_mod_Spedizioni;
		$ar = array();  		
		$sql = "SELECT * 
				FROM {$cfg_mod_Spedizioni['file_tabelle']}
				WHERE TATAID = '$tab' ";
		if (isset($k1))		
				$sql .= " AND TAKEY1 = '$k1' ";
				
		$stmt = db2_prepare($conn, $sql);		
		$result = db2_execute($stmt);	
		
		$ret = array();
		while ($row = db2_fetch_assoc($stmt)) {
		 $ret[] = array("id" => $row['TAKEY1'],
		 		        "text" => $row['TADESC'] );	
		}		
	return $ret;	
 }
	
}

?>