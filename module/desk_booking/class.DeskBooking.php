<?php

	class DeskBooking {
	
		private $mod_cod = "DESK_BOOK";
		private $mod_dir = "desk_booking";
	
	
		function __construct($parameters = array()) {
			global $auth;
			$mod_permitted = $auth->verify_module_permission($this->get_cod_mod());
	
			if (is_null($mod_permitted) || !$mod_permitted){
	
				//se mi ha passato un secondo modulo testo i permessi
				if (isset($parameters['abilita_su_modulo'])){
					$mod_permitted = $auth->verify_module_permission($parameters['abilita_su_modulo']);
					if (is_null($mod_permitted) || !$mod_permitted){
						die("\nNon hai i permessi!!");
					}
				} else
					die("\nNon hai i permessi!!!");
			}
		}
		
		public function get_cod_mod(){
			return $this->mod_cod;
		}

		
		public function fascetta_function(){
			global $auth, $main_module;
			return '';			
		}		
		
		public function verify_num_sped($sped_id, $cliente_cod = null){
			global $conn, $cfg_mod_Spedizioni, $cfg_mod_Booking, $id_ditta_default, $auth;
			$ret = array();
			$s = new Spedizioni(array('no_verify' => 'Y'));
			$sped = $s->get_spedizione($sped_id);
			
			$sql="SELECT COUNT(*) as C_ROW
			      FROM {$cfg_mod_Booking['file_richieste']} BS
                  WHERE BSNBOC='{$sped_id}' AND  BSSTAT IN ('01', '02')";

			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt);
			
			$r = db2_fetch_assoc($stmt);
			
			if ($sped == false){
				$ret['success'] = false;
				$ret['message'] = 'Spedizione inesistente';				
			} else if ($r['C_ROW'] == 0) { //ToDo verificare che non ci siano gia' richieste in corso
					$ret['success'] = true;
					$ret['sped'] = $sped;
			} else {
				$ret['success'] = false;
				$ret['message'] = 'Stato spedizone non valido';
			}
			
			if ($ret['success'] == true && !is_null($cliente_cod)){
				//verifico che ci sia almeno un ordine per il codice cliente inserito
				$sql = "SELECT COUNT(*) as C_ROW FROM {$cfg_mod_Spedizioni['file_testate']} 
						WHERE " .  $s->get_where_std() . "
						AND TDNBOF = ? AND TDCCON = ?
						";
				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt, array($sped_id, $cliente_cod));
				$r = db2_fetch_assoc($stmt);
				
				if ($r['C_ROW'] == 0){
					$ret['success'] = false;
					$ret['message'] = 'Codice cliente non corrispondente';
				}
			}
			
			//scriviamo l'history se la verifica � andata a buon fine
			if($ret['success'] == true){
				
				$value = $s->get_totali_by_TDNBOF($sped_id, $id_ditta_default);
				
				/*print_r($value['TDVOLU']);
				exit;*/
				$ar_ins = array();
				$ar_ins['BSTIME'] 	= microtime(true);
				$ar_ins['BSUSGE']   = trim($auth->get_user());
				$ar_ins['BSDTGE']   = oggi_AS_date();
				$ar_ins['BSORGE']   = oggi_AS_time();
				$ar_ins['BSDT'] 	= $id_ditta_default;
				$ar_ins['BSNBOC'] 	= $sped_id;
				$ar_ins['BSCCON'] 	= $cliente_cod;
				$ar_ins['BSCITI'] 	= $sped['CSCITI'];
				$ar_ins['BSSTAT'] 	= '00';
				$ar_ins['BSVOLU'] 	= $value['TDVOLU'];
				$ar_ins['BSPLOR'] 	= $value['S_PESO'];
				$ar_ins['BSPNET'] 	= $value['S_PESO_N'];
				$ar_ins['BSTOCO'] 	= $value['S_COLLI'];
				$ar_ins['BSELCA']	= $s->get_el_carichi_by_TDNBOF($sped_id, 'AN');		
				
				$sql = "INSERT INTO {$cfg_mod_Booking['file_richieste']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt, $ar_ins);
				echo db2_stmt_errormsg($stmt);
				
				$ret['BSTIME'] = $ar_ins['BSTIME'];
			}
				
			return $ret;
		}
		
	}
	