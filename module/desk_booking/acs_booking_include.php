<?php


function dettagli_anagrafici($tipo, $denominazione, $valore, $ar_value=array(), $trad=array()){
	?>
	
	
				{
					xtype: 'fieldset',
					itemId: 'fs_<?php echo $tipo?>',
	                title: <?php echo j($denominazione); ?>,
	                layout: 'anchor',
	                //itemId: 'sosta_yes'
	                flex:1,
	                disabled: <?php echo $valore ?>,
	              	items: [
							{
						xtype: 'fieldcontainer',
						layout: {type: 'vbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{ xtype: 'textfield', flex: 1},
						items: [					
							{
						name: '<?php echo $tipo?>_f_denom',
						fieldLabel: '<?php echo $trad['denom']?><span style="color:red;">*</span>',
						labelWidth: 100,
						maxLength: 100,
						allowBlank: false,
						value: <?php echo j($ar_value['denom']); ?>
					   	},{
						name: '<?php echo $tipo?>_f_indi',
						labelWidth: 100,
						margin: '0 0 0 0',
						fieldLabel: '<?php echo $trad['indi']?><span style="color:red;">*</span>',
						maxLength: 100,
						allowBlank: false,
						value: <?php echo j($ar_value['indirizzo']); ?>					
						}
						]
					},
						{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
																						
								{
									name: '<?php echo $tipo?>_f_loca', 
									flex: 1,
								    labelWidth: 100,
									fieldLabel: '<?php echo $trad['loca']?><span style="color:red;">*</span>',
									allowBlank: false,
									value: <?php echo j($ar_value['loca']); ?>	
									
								}, {
									name: '<?php echo $tipo?>_f_cap', 
									width: 150,
									fieldLabel: '<?php echo $trad['cap']?>',
									labelWidth: 60,
									maxLength: 5,
									labelAlign: 'right',
									value: <?php echo j($ar_value['cap']); ?>	
								    }
				
								  ]
								}	,
								
								
							{
									xtype: 'fieldcontainer',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									anchor: '-10',
									defaults:{xtype: 'textfield'},
									items: [
										{
									name: '<?php echo $tipo?>_f_nazi',
									flex:1,
									fieldLabel: '<?php echo $trad['nazi']?><span style="color:red;">*</span>',
									labelWidth: 100,
									value: <?php echo j($ar_value['nazione']); ?>,
									allowBlank: false
										},{
									name: '<?php echo $tipo?>_f_prov', 
									width: 100,
									fieldLabel: '<?php echo $trad['prov']?>',
									labelWidth: 40,
									maxLength: 5,
									labelAlign: 'right',
									value: <?php echo j($ar_value['prov']); ?>
									   }
				
								  ]
								}	,	
								
					 	{
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', pack: 'start', align: 'stretch'},
						anchor: '-10',
						defaults:{xtype: 'textfield'},
						items: [ 
						
							{	name: '<?php echo $tipo?>_f_email',
							    fieldLabel: '<?php echo $trad['email']?><span style="color:red;">*</span>',
							   	flex: 3,
							    labelWidth: 100,
							    value: <?php echo j($ar_value['email']); ?>,
								allowBlank: false							
								},
							{
								name: '<?php echo $tipo?>_f_tel',
								fieldLabel: '<?php echo $trad['tel']?><span style="color:red;">*</span>',
							    maxLength: 20,
							    flex: 2,
							    labelAlign: 'right',
							    labelWidth: 35,
							    value: <?php echo j($ar_value['telefono']); ?>,
								allowBlank: false
							},{
								name: '<?php echo $tipo?>_f_iva',
								fieldLabel: '<?php echo $trad['iva']?>',
								maxLength: 100,
							    flex: 2,
							    labelAlign: 'right',
							    labelWidth: 80,
							    value: <?php echo j($ar_value['p_iva']); ?>,
							    allowBlank: <?php if ($tipo == 'VETT') echo 'false'; else echo 'true'; ?>,							
								listeners : {
								'blur': function(field) {
								    var form = this.up('form').getForm();
      				                var form_values = this.up('form').getForm().getValues();
      				                     				    
              					    Ext.Ajax.request({
        						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_values_from_piva',
        						        timeout: 2400000,
        						        method     : 'POST',
        			        			jsonData: {
        			        				form_values : form_values,
        			        				tipo : <?php echo j($tipo); ?>
        								},				
        								success : function(result, request){
        							        var jsonData = Ext.decode(result.responseText);
        							        var row = jsonData.row;
        							        
        							        if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_denom))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_denom').setValue(row.IGRGSO);
        							        if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_indi))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_indi').setValue(row.IGINDI);
        					        		if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_loca))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_loca').setValue(row.IGLOCA);
        							        if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_cap))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_cap').setValue(row.IGCAP);
							        	    if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_nazi))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_nazi').setValue(row.IGDNAZ);
        							        if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_prov))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_prov').setValue(row.IGPROV);
        							        if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_email))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_email').setValue(row.IGMAI1);
        							        if (Ext.isEmpty(form_values.<?php echo $tipo; ?>_f_tel))
        							        	form.findField(<?php echo j($tipo); ?> + '_f_tel').setValue(row.IGTEL1);
        						
        					      	    	
        							    },
        						        failure    : function(result, request){
        						            Ext.Msg.alert('Message', 'No data to be loaded');
        						        }
        						    });
                	 		

            		}
								
								}
							}
						]
					}
							
	             ]
	             }	

<?php }


if ($_REQUEST['fn'] == 'get_values_from_piva'){
    $m_params = acs_m_params_json_decode();
    $p_iva = "{$m_params->tipo}_f_iva";
    $p_iva = $m_params->form_values->$p_iva;
    
    //TRASPORTATORE O VETTORE
    $sql = "SELECT *
            FROM {$cfg_mod_Booking['file_indirizzi']} IG
            WHERE IGDT= '{$id_ditta_default}' AND IGTAID= 'BKSPE' AND IGKEY2='{$m_params->tipo}'
            AND IGPIVA = '{$p_iva}'
            ORDER BY IGDTGE DESC";
        
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt);
    
    $ret = array();
    $ret['success'] = true;
    $ret['row'] = $row;
    echo acs_je($ret);
    exit;
    
    
}