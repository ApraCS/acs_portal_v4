<?php
	
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors','On');
	
	require_once("../../config.inc.php");
	require_once("class.DeskBooking.php");
	$_module_descr = "Booking";	
	
	$main_module = new DeskBooking(array('abilita_su_modulo' => 'DESK_VEND'));
			
?>



<html>
<head>
<title>ACS Portal_Gest</title>
<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />


<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />
<link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />

    <!-- GC -->


<style type="text/css">
.icon-folder_search-64 {background-image: url("../../images/icone/64x64/folder_search.png") !important;}


.x-btn-default-large-icon-text-left .x-btn-icon {width: 64px;}
.x-ie6 .x-btn-default-large-icon-text-left .x-btn-icon, .x-quirks .x-btn-default-large-icon-text-left .x-btn-icon {height: 64px}

/* NASCONDO ICONE */
#main-site-home, #main-site-search, #main-site-tools, #main-site-help {	visibility: hidden; }

</style>



<script type="text/javascript" src="../../../extjs/ext-all.js"></script>
<script type="text/javascript" src="../../../extjs/locale/ext-lang-it.js"></script>

<script src=<?php echo acs_url("js/acs_js.js") ?>></script>



<script type="text/javascript">

	Ext.Loader.setConfig({
	    enabled: true,
	    paths: {       
        }	    
	}); Ext.Loader.setPath('Ext.ux', '../../ux');


    Ext.require(['*'
                 , 'Ext.ux.grid.FiltersFeature'                 
                 //'Ext.calendar.util.Date',
                 //'Ext.calendar.CalendarPanel',
                 //'Ext.calendar.data.MemoryCalendarStore',
                 //'Ext.calendar.data.MemoryEventStore',
                 //'Ext.calendar.data.Events',
                 //'Ext.calendar.data.Calendars',
    			 , 'Ext.ux.grid.Printer'
      			 , 'Ext.ux.RowExpander' 
                 ]);



    Ext.onReady(function() {

        Ext.QuickTips.init();

        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

        var viewport = Ext.create('Ext.Viewport', {
            layout: 'border',
            items: [ 
                    
	            Ext.create('Ext.Component', {
	                region: 'north',
	                id: 'page-header',
	                height: 110, // give north and south regions a height
	                contentEl: 'header'
	            }),
            
	            Ext.create('Ext.tab.Panel', {
	            	id: 'm-panel',
	        		cls: 'supply_desk_main_panel',            	            	
	                region: 'center', // a center region is ALWAYS required for border layout
	                deferredRender: false,
	                activeTab: 1,     // first tab initially active
	                items: [

						{
						    xtype: 'form',
						    bodyStyle: 'padding: 30px',
						    bodyPadding: '30 30 30 30',
						    margin: "10 10 10 10",
						    border: true,
						    frame: false,
						    title: 'Booking',
						    flex: 1,
						    layout: {
						    	type: 'vbox',
						    	align: 'center',
						    	pack: 'center'
						    },

						    items: [
								{
									name: 'f_user',
									xtype: 'hidden',
									value: <?php echo j($_REQUEST['user'])?>,
								},


								{
									xtype: 'fieldcontainer',
									layout: {
									    type: 'hbox',
									    align: 'stretch'
									},
									items: [

										{
											name: 'f_chiave',
											xtype: 'textfield',
											hasfocus:true,
											fieldLabel: 'Numero spedizione', labelAlign: 'top',
											bodyPadding: '30 30 30 30',
											bodyStyle: 'padding: 30px',
										    maxLength: 80,
										    fieldStyle: 'font-size: 60px; text-transform: uppercase;',
										    height: 100, width: 600,
										    enableKeyEvents: true,
										    listeners: {                   
									             'afterrender': function(field) {
									                    field.focus();
									                }
								            }						
										}
										
										
										, {
											xtype: 'button',
											id: 'firma_button',
											iconCls: 'icon-folder_search-64',
											scale: 'large', width: 150,
											autoWidth : true,
											autoHeight : true,
											bodyPadding: '30 30 30 30',
											bodyStyle: 'padding: 30px',
											margin: "23 10 2 10",
											text: 'Start',
											handler : function() {

												var form = this.up('form').getForm();

												//verifico correttezza/esistenza spedizione
												Ext.Ajax.request({
												   url        : 'acs_booking.php?fn=verify_num_sped',
												   method: 'POST',
												   jsonData: {form_values: form.getValues()}, 
												   
												   success: function(response, opts) {
													  var jsonData = Ext.decode(response.responseText);									   	  
										              if (jsonData.success == true){
										            	  //acs_show_panel_std('acs_booking.php?fn=open_parameters', 'panel_stato_ordini', {form_values: form.getValues()});  //recupero i valori del form per passarli nel tree
										            	  acs_show_win_std( 'Inserimento dati di booking', 
												            	  			'acs_booking.php?fn=open_parameters', 
												            	  			{form_values: form.getValues()}, 
												            	  			null, null, {}, null, 'Y');											                 
										              } else {
										            	  alert(jsonData.message);
										            	  return false;
										              }
												   }, 
												   failure: function(response, opts) {
												      Ext.getBody().unmask();
												      alert('Spedizione non valida');
												   }
												});												 
												 
												 
												} //handler function()
											
										}
											
									]
								}



								
								
							]


						}
				                                   
	                             					                                                
	                ]
	            })

            ]
        });


        
      	//--------------------------------------------------------------------
      	// BOTTONI FASCETTA 
      	//--------------------------------------------------------------------
      	
      	
        //--------------------------------------------------------------------------------        
  
  		//schedulo la richiesta dell'ora ultimo aggiornamento
   		/////Ext.TaskManager.start(refresh_stato_aggiornamento);	
     		

        
    });  //onReady
                
    </script>
</head>



<body>
    
    <div id="header" class="x-hide-display">
        <?php include("../../templates/header.php"); ?>
    </div>
    
    
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;"></div>
    
    <!-- LEGENDA -->
    <div id="south" class="x-hide-display">
    </div>
    
    
</body>
</html>
