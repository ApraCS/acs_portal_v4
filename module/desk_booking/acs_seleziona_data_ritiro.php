<?php

require_once "../../config.inc.php";
require_once("class.DeskBooking.php");
require_once("acs_booking_include.php");



$m_trad = array(
	"ita" => array(
			"data" => "Data",
			"data_non_selezionabile" => "Data non selezionabile",
			"data_con_conferma_richiesta" => "Data selezionabile con conferma ufficio logistico"
	),
	"eng" => array(
			"data" => "Date",
			"data_non_selezionabile" => "Not avaiable day",
			"data_con_conferma_richiesta" => "date with acceptance request"
	),
);



$main_module = new DeskBooking(array('abilita_su_modulo' => 'DESK_VEND'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if (isset($m_params->lng) && strlen($m_params->lng) > 0)
    $lng = $m_params->lng;
else 
    $lng = 'ita';


if ($_REQUEST['fn'] == 'get_json_data_grid'){

	$sped_id = $m_params->sped_id;
	$sped = $s->get_spedizione($sped_id);
	
	//colli per sped
	$colli_sped = $s->get_totale_by_TDNBOF($sped_id, 'S_COLLI');
	
	$ar_PS = $s->get_ar_tot_PS(oggi_AS_date(), 17);
	
	foreach($ar_PS as $d => $ar){
		//recupero i colli in spedizione
		$ar_PS[$d]['data']					= $d;
		
		if ($m_params->lng == 'eng'){
			$oldLocale = setlocale(LC_TIME, null);
			$bl_day = ucfirst(strftime("%A", strtotime($d)));
			setlocale(LC_TIME, $oldLocale);				
		} else {
			$oldLocale = setlocale(LC_TIME, 'it_IT');
			$bl_day = ucfirst(strftime("%A", strtotime($d)));
			setlocale(LC_TIME, $oldLocale);
		}
		$ar_PS[$d]['d_data'] = acs_u8e($bl_day) . " " . print_date($d);
		
		$ar_PS[$d]['programmati'] 			= (int)$s->get_totale_by_data("TDDTSP", $d, 'S_COLLI'); 
		$ar_PS[$d]['max_programmabili'] 	= (int)$ar['PS'];
		
		if ((int)$ar_PS[$d]['max_programmabili'] == 0)
			$ar_PS[$d]['stato_programmazione'] = 0; //grigio, giorno non lavorativo
		else if ( ((int)$ar_PS[$d]['max_programmabili'] * 1.10 ) >= $ar_PS[$d]['programmati'] + $colli_sped)
			$ar_PS[$d]['stato_programmazione'] = 1; //verde
		else
		    $ar_PS[$d]['stato_programmazione'] = 0; //grigio, se supera la capacita + del 10% non lo faccio selezionare
			//$ar_PS[$d]['stato_programmazione'] = 2; //giallo, supero il limite programmabili piu' del 5%
		
		if ($ar_PS[$d]['stato_programmazione'] == 0){
			$ar_PS[$d]['td_class'] = 'sfondo_grigio';
			$ar_PS[$d]['qtip_text'] = $m_trad[$lng]['data_non_selezionabile'];
		}

		if ($ar_PS[$d]['stato_programmazione'] == 1)
			$ar_PS[$d]['td_class'] = 'sfondo_verde';
		
		if ($ar_PS[$d]['stato_programmazione'] == 2){
			$ar_PS[$d]['td_class'] = 'sfondo_giallo';
			$ar_PS[$d]['qtip_text'] = $m_trad[$lng]['data_con_conferma_richiesta'];
		}	
		
		//recupero il giorno di calendario perche' il sabato non deve essere selezionabile
		$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_calendario']}
				WHERE CSCALE = '*CS' AND CSDT = '{$id_ditta_default}'
				AND CSDTRG = {$d}
				";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		$dc = db2_fetch_assoc($stmt);
			
		if ($dc['CSGIOR'] == 6){
			$ar_PS[$d]['stato_programmazione'] = 0; //sabato, grigio
			$ar_PS[$d]['td_class'] = 'sfondo_grigio';
			$ar_PS[$d]['qtip_text'] = $m_trad[$lng]['data_non_selezionabile'];
		}
		
		if (trim($dc['CSTPGG']) == 'F'){
		    $ar_PS[$d]['stato_programmazione'] = 0; //sabato, grigio
		    $ar_PS[$d]['td_class'] = 'sfondo_grigio';
		    $ar_PS[$d]['qtip_text'] = $m_trad[$lng]['data_non_selezionabile'];
		}
		
		//data < di quella programmabile?
		if ($d <= $sped['CSDTSP']){
			$ar_PS[$d]['stato_programmazione'] = 0; //sabato, grigio
			$ar_PS[$d]['td_class'] = 'sfondo_grigio';
			$ar_PS[$d]['qtip_text'] = $m_trad[$lng]['data_non_selezionabile'];
		}
		
	}
	
	$ar_ret = array();
	$c = 0;
	foreach($ar_PS as $d => $ar){
	    
	    //recupero il giorno di calendario perche' devo saltare 1 o 2 giorni lavorativi
	    $sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_calendario']}
	               WHERE CSCALE = '*CS' AND CSDT = '{$id_ditta_default}' AND CSDTRG = {$d}";
	    $stmt = db2_prepare($conn, $sql);
	    $result = db2_execute($stmt);
	    $dc = db2_fetch_assoc($stmt);	    

	    if ($dc['CSTPGG'] == 'L')
		  $c++;
	    
		//salto i primi 2 giorni, oppure considero anche domani entro le 15 di oggi
		if ($c > 2 || ($c > 1 && oggi_AS_time() < 150000))
			$ar_ret[] = $ar;
	}
	
	//dal secondo giorno successivo a oggi, mostro i 15 giorni (lavorativi) con lo stato della disponibilita' colli cosi' come file flight
	
	$ar = array();
	
	$ret = array();
	$ret['success'] = true;
	$ret['root'] = $ar_ret;
	echo acs_je($ret);
	exit;
}






// ******************************************************************************************
// Form inserimento dati
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form'){
?>
{"success":true, "items": [
	{
		xtype: 'grid',
		flex: 1,
		loadMask: true,	
		store: <?php echo extjs_grid_store(null, 'get_json_data_grid', 
					"data stato programmati max_programmabili stato_programmazione td_class qtip_text d_data", 
					acs_je($m_params), array()
				)?>,

	    columns: [	
		      {header   : <?php echo j($m_trad[$lng]['data']) ?>, dataIndex: 'd_data', flex: 1, align: 'center',
    			    renderer: function(value, metaData, record){    			    	
						metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_text')) + '"';						
    			    	metaData.tdCls += ' ' + record.get('td_class');							
							
						return '<h1 style="font-size: 14px"><br/>' + value + '</h1>'		
					}			    					       
		      }
		   /*             
			, {header   : 'Max Programmabili', dataIndex: 'max_programmabili', width: 100}
			, {header   : 'Programmati', dataIndex: 'programmati', width: 100}			      
			, {header   : 'Stato', dataIndex: 'stato_programmazione', flex: 1}
		   */			
	     ]         
		
		, listeners: {
				celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	iEvent.stopEvent();					  
					  	var m_win = iView.up('window');
					  	var col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	var rec = iView.getRecord(iRowEl);
					  	if (rec.get('stato_programmazione') == 1 || rec.get('stato_programmazione') == 2)
					  		iView.up('window').fireEvent('onSelected', m_win, rec.get('data'), rec.get('stato_programmazione'));
					  	else
					  		acs_show_msg_error(<?php echo j($m_trad[$lng]['data_non_selezionabile']) ?>);
					  	}
				}	  	
		}
				
        }
        
    ]}	
<?php
 exit;
}