<?php

require_once "../../config.inc.php";
require_once("class.DeskBooking.php");
require_once("acs_booking_include.php");
require_once("../desk_vend/acs_booking_include.php");



$main_module = new DeskBooking(array('abilita_su_modulo' => 'DESK_VEND'));

$s = new Spedizioni(array('no_verify' => 'Y'));

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);


?>


<html>
 <head>

  <style>
   <?php @include '../../personal/report_css_logo_cliente.php'; ?>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   .number{text-align: right;}
   .cella_gray{background-color: #D1CCBF;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 12px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}  
   
  table.intest td{border: 0px;} 
  
  table.data td{border: 0px; width:25%} 
  table.dati td{width:50%} 
 
    
   tr.ag_liv_data th{background-color: #333333; color: white;}
   .grassetto th{font-weight: bold;}
    .box{width: 250px;
   		 height: 150;
   		 padding:10px;
   		 border: 1px solid;
   		 
   	  } 
   
    div#my_content h1{font-size: 22px; padding: 10px;}
   div#my_content h2{font-size: 18px; padding: 5px; margin-top: 20px;}   
   
   @media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
 @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}     
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>
  
  <script type="text/javascript">    
		Ext.onReady(function() {
		//apertura automatica finestra invio email
		<?php if ($_REQUEST['r_auto_email'] == 'Y'){ ?>
			Ext.get('p_send_email').dom.click();
		<?php } ?>	
		});
  </script>  

 </head>
 <body>
 	
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'N';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>  	

<div id='my_content'> 


<table class='intest int1'>


<tr> <td style='width: 50%'>

<?php 
$controllo = $_REQUEST['controllo'];?>


</td>
<td style= 'border: 1px solid; width: 20%; text-align:center;' > <B>
 NUMERO DI SPEDIZIONE
 <BR>
<?php echo $_REQUEST['nr_sped']; ?></B>
</td>
</tr>
</table>

<BR>
<BR>

<p><b>LUOGO DI RITIRO:</b></p>
<p><b>ORARI DI MAGAZZINO: </b></p>
<br>
<p>Riepilogo merce </p>
<table class=int1>
<tr>
<th>&nbsp;</th>
<th>Nr ordine</th>
<th class=number>Colli</th>
<th class=number>Peso</th>
<th class=number>Volume</th>
<th class=number>Importo</th>
</tr>



<?php 

$nr_sped= $_REQUEST['nr_sped'];
$bstime = $_REQUEST['bstime'];

if(isset($bstime) && trim($bstime) != ''){
	
	$br = bk_get_row($bstime);
	//print_r($br);
	$data_ritiro = $br['BSDTSR'];
	
	if($br['BSSTAT'] == '02'){
		$conferma     = true;
		$non_conferma = false;
	}else{
		$conferma     = false;
		$non_conferma = true;
	}
}else{
	
	//DATA
	$sql_data="SELECT BSDTSR, BSSTAT
		FROM {$cfg_mod_Booking['file_richieste']} BS
		WHERE BSDT= '{$id_ditta_default}' AND BSNBOC='{$nr_sped}'
		ORDER BY BSDTSR DESC FETCH FIRST 1 ROWS ONLY";
	
	/*print_r($sql_data);
	 exit;*/
	
	$stmt_data= db2_prepare($conn, $sql_data);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_data);
	
	$row_data = db2_fetch_assoc($stmt_data);
	$data_ritiro = trim($row_data['BSDTSR']);
	
	if($row_data['BSSTAT'] == '02'){
		$conferma     = true;
		$non_conferma = false;
	}else{
		$conferma     = false;
		$non_conferma = true;

	}
}

$sql="SELECT TDONDO, TDOADO, TDOTPD, TDVOLU, TDTOCO, TDPLOR, TDINFI
		FROM {$cfg_mod_Booking['file_testate']} TD 
		WHERE TDNBOF='{$nr_sped}'";

$stmt = db2_prepare($conn, $sql);
echo db2_stmt_errormsg();
$result = db2_execute($stmt);


$sql_tot="SELECT SUM(TDVOLU) AS S_VOLUME, SUM(TDTOCO) AS S_COLLI,  SUM(TDPLOR) AS S_PESO,  SUM(TDINFI) AS S_IMPORTO
		FROM {$cfg_mod_Booking['file_testate']} TD
		WHERE  ". $s->get_where_std()." AND TDNBOF='{$nr_sped}'";

$stmt_tot = db2_prepare($conn, $sql_tot);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_tot);
$row_tot = db2_fetch_assoc($stmt_tot);

while($row = db2_fetch_assoc($stmt)){
	$nr = array();

	$nr['NR_ORD']= implode("_", array($row['TDOADO'], $row['TDONDO'], $row['TDOTPD']));
	$nr['COLLI']= $row['TDTOCO'];
	$nr['PESO']= $row['TDPLOR'];
	$nr['VOLUME']= $row['TDVOLU'];
	$nr['IMPORTO']= $row['TDINFI'];

	$ar[] = $nr;

}


//TRASPORTATORE
$sql_tras="SELECT *
FROM {$cfg_mod_Booking['file_indirizzi']} IG
WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='TRAS'";

$stmt_tras= db2_prepare($conn, $sql_tras);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_tras);

$row_tras = db2_fetch_assoc($stmt_tras);

$ret_tras = array(
	    "denom"  => acs_u8e(trim($row_tras['IGRGSO'])),
	    "indirizzo"  => acs_u8e(trim($row_tras['IGINDI'])),
		"cap"  => acs_u8e(trim($row_tras['IGCAP'])),
		"loca"  => acs_u8e(trim($row_tras['IGLOCA'])),
		"prov"  => acs_u8e(trim($row_tras['IGPROV'])),
		"nazione"  => acs_u8e(trim($row_tras['IGDNAZ'])),
		"p_iva"  => acs_u8e(trim($row_tras['IGPIVA'])),
		"telefono"  => acs_u8e(trim($row_tras['IGTEL1'])),
		"email"  => acs_u8e(trim($row_tras['IGMAI1'])),
		"orario"  => acs_u8e(trim($row_tras['IGFG01']))
		
		
);



//VETTORE
$sql_vett="SELECT *
FROM {$cfg_mod_Booking['file_indirizzi']} IG
WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='VETT'";

$stmt_vett= db2_prepare($conn, $sql_vett);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_vett);

$row_vett = db2_fetch_assoc($stmt_vett);

$ret_vett = array(
		"denom"  => acs_u8e(trim($row_vett['IGRGSO'])),
		"indirizzo"  => acs_u8e(trim($row_vett['IGINDI'])),
		"cap"  => acs_u8e(trim($row_vett['IGCAP'])),
		"loca"  => acs_u8e(trim($row_vett['IGLOCA'])),
		"prov"  => acs_u8e(trim($row_vett['IGPROV'])),
		"nazione"  => acs_u8e(trim($row_vett['IGDNAZ'])),
		"p_iva"  => acs_u8e(trim($row_vett['IGPIVA'])),
		"telefono"  => acs_u8e(trim($row_vett['IGTEL1'])),
		"email"  => acs_u8e(trim($row_vett['IGMAI1'])),

);

//SOSTA TECNICA
$sql_stec="SELECT *
FROM {$cfg_mod_Booking['file_indirizzi']} IG
WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='STEC'";

$stmt_stec= db2_prepare($conn, $sql_stec);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_stec);

$row_stec = db2_fetch_assoc($stmt_stec);

$ret_stec = array(
		"denom"  => acs_u8e(trim($row_stec['IGRGSO'])),
		"indirizzo"  => acs_u8e(trim($row_stec['IGINDI'])),
		"cap"  => acs_u8e(trim($row_stec['IGCAP'])),
		"loca"  => acs_u8e(trim($row_stec['IGLOCA'])),
		"prov"  => acs_u8e(trim($row_stec['IGPROV'])),
		"nazione"  => acs_u8e(trim($row_stec['IGDNAZ'])),
		"p_iva"  => acs_u8e(trim($row_stec['IGPIVA'])),
		"telefono"  => acs_u8e(trim($row_stec['IGTEL1'])),
		"email"  => acs_u8e(trim($row_stec['IGMAI1'])),

);

//ANTICIPO OPERAZIONE DOGANALI
$sql_doga="SELECT *
FROM {$cfg_mod_Booking['file_indirizzi']} IG
WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='DOGA'";

$stmt_doga= db2_prepare($conn, $sql_doga);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_doga);

$row_doga = db2_fetch_assoc($stmt_doga);

$ret_doga = array(
		"denom"  => acs_u8e(trim($row_doga['IGRGSO'])),
		"telefono"  => acs_u8e(trim($row_doga['IGTEL1'])),
		"email"  => acs_u8e(trim($row_doga['IGMAI1'])),

);


$spedizione = $s->get_spedizione($nr_sped);
//$targa= $spedizione['CSTARG'];
$targa  = $row_doga['IGLOCA'];

$booking  = $row_doga['IGDNAZ'];




$conteggio=0;

if(is_array($ar)){
	foreach ($ar as $k0 => $v0){
	
		?>
		<tr>
		<?php if($conteggio==0){ ?>
			<td>Ordini</td>
		<?php }else{ ?>
			<td>&nbsp;</td>
		<?php }?>
		<td class=number><?php echo $v0['NR_ORD']; ?></td>
	  	<td class=number><?php echo $v0['COLLI']; ?></td>
	    <td class=number><?php echo n($v0['PESO'],2); ?></td>
	    <td class=number><?php echo n($v0['VOLUME'],2); ?></td>
	    <td class=number><?php echo n($v0['IMPORTO'],2); ?></td>
	    </tr>
	  

	<?php 
	$conteggio++;
 }

}

?>

<tr style='font-weight: bold;'>
	<td>Totali:</td><td>&nbsp;</td>
	<td class=number> <?php echo $row_tot['S_COLLI']?></td>
	<td class=number><?php echo n($row_tot['S_PESO'],2) ?></td>
	<td class=number><?php echo n($row_tot['S_VOLUME'],2) ?></td>
	<td class=number><?php echo n($row_tot['S_IMPORTO'],2) ?></td>	
 </tr>

</table>
<br>
<?php if($ret_tras['orario']=='M')
			$orario='Mattina';
		else 
			$orario='Pomeriggio';
	
	?>
<table class="data int1"><tr><td>DATA RITIRO: <b><?php echo print_date($data_ritiro); ?></b>, orario: <?php echo $orario; ?></td></tr></table>
<table class="data int1"><tr><td>BOOKING: <b><?php echo $booking; ?></b></td></tr></table>
<br>
<table class="dati int1">
<tr>
<th><b>TRASPORTATORE CHE RICHIEDE IL BOOKING:</b></th>
<th><b>VETTORE INCARICATO:</b></th>
</tr>
<tr>
<td> <?php echo $ret_tras['denom']?>, <?php echo $ret_tras['indirizzo']?></td>
<td> <?php echo $ret_vett['denom']?>, <?php echo $ret_vett['indirizzo']?></td>
</tr>
<tr>
<td> <?php echo $ret_tras['cap']?>, <?php echo $ret_tras['loca']?> (<?php echo $ret_tras['prov']?>) <?php echo $ret_tras['nazione']?></td>
<td> <?php echo $ret_vett['cap']?>, <?php echo $ret_vett['loca']?> (<?php echo $ret_vett['prov']?>) <?php echo $ret_vett['nazione']?></td>
</tr>
<tr>
<td> PI: <?php echo $ret_tras['p_iva']?></td>
<td> PI: <?php echo $ret_vett['p_iva']?></td>
</tr>
<tr>
<td> Tel: <?php echo $ret_tras['telefono']?></td>
<td> Tel: <?php echo $ret_vett['telefono']?></td>
</tr>
<tr>
<td> email: <?php echo $ret_tras['email']?></td>
<td> email: <?php echo $ret_vett['email']?></td>
</tr>
</table>
<br>
<p style='text-align:center;'>TARGA DEL MEZZO: <?php echo $targa; ?> </p>
<br>


<p><b>SOSTA TECNICA:</b></p>
<?php if($ret_stec['denom']!=''){?>
<p> <?php out_checkbox(true); ?> SI  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	Indirizzo sosta tecnica: <?php echo $ret_stec['denom']?>, <?php echo $ret_stec['indirizzo']?>, <?php echo $ret_stec['cap']?>, <?php echo $ret_stec['loca']?> (<?php echo $ret_stec['prov']?>) <?php echo $ret_stec['nazione']; ?>, PI: <?php echo $ret_stec['p_iva']?>, Tel: <?php echo $ret_stec['telefono']?>, email: <?php echo $ret_stec['email']?></p>
<p> <?php out_checkbox(false); ?> NO</p>
<?php }else{?>
<p> <?php out_checkbox(false); ?> SI </p>
<p> <?php out_checkbox(true); ?> NO</p>
<?php }?>
<br>
<br>
<p> Dettagli per anticipo documenti per le OPERAZIONI DOGANALE</p>
<p> Ragione sociale: <?php echo $ret_doga['denom']?>, Telefono: <?php echo $ret_doga['telefono']?>, Email: <?php echo $ret_doga['email']?> </p>

<br>

<?php if($controllo == 'Y'){?>

<p>La prenotazione si intende confermata se e solo se Stosa invier&agrave; il presente modulo con l'area sottostante compilata</p>
<hr>
<br>
<p><?php out_checkbox($conferma); ?> <b>CONFERMATO</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php out_checkbox($non_conferma); ?> <b>NON COFERMATO</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; note:___________________ </p>
<br>
<p><b>ATTENZIONE:</b> comunicare eventuali cambiamenti entro e non oltre le ore 15:00 del giorno precedente il carico. In caso di mancato arrivo del carico sar&agrave; necessario effettuare il nuovo booking. Stosa confermer&agrave; in base alla disponibilit&agrave;.</p>	

</div></body></html>
<?php 
}