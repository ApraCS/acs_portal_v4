<?php

require_once "../../config.inc.php";
require_once("class.DeskBooking.php");
require_once("acs_booking_include.php");
require_once("../desk_vend/acs_booking_include.php");

$main_module = new DeskBooking(array('abilita_su_modulo' => 'DESK_VEND'));
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if($m_params->nr_sped !=''){
	$nr_sped=$m_params->nr_sped;
}else{
	$nr_sped= $m_params->form_values->f_chiave;
}


if($m_params->form_values->lng =='eng'){
	$trad = array(
			"cli"  			=> 'Customer',
			"nr_book"       => 'Nr Booking',
			"targa"         => 'Truck-Plate',
			"data_ritiro"   => 'Loading date',
			"mattina"       => 'Morning',
			"pome"          => 'Afternoon',
			"vatt_trasp"    => 'Carrier as transporter',
			"sosta_tec"     => 'Technical stop',
			"denom"         => 'Name',
			"indi"          => 'Address',
			"loca"          => 'Place',
			"cap"           => 'Zip Code',
			"nazi"          => 'Nation',
			"prov"          => 'Prov.',
			"email"         => 'Email',
			"tel"           => 'Tel',
			"iva"           => 'VAT Number',
			"nr_ord"        => 'Order number',
			"data"          => 'Date',
			"rif"           => 'Reference',
			"colli"         => 'Items',
			"peso"          => 'Gross W.KG',
			"volume"        => 'Vol.MC',
			"importo"       => 'Amount',
			"vettore"       => 'Carrier',
			"trasp"         => 'Transporter',
			"doga"          => 'Details for customs operations',
			"invio_richiesta" => 'Send request'
	
	);
	

}else{
	
	$trad = array(
			"cli"  			=> 'Cliente',
			"nr_book"       => 'Nr Booking',
			"targa"         => 'Targa',
			"data_ritiro"   => 'Data del ritiro',
			"mattina"       => 'Mattina',
			"pome"          => 'Pomeriggio',
			"vatt_trasp"    => 'Vettore uguale al trasportatore',
			"sosta_tec"     => 'Sosta tecnica',
			"denom"         => 'Ragione sociale',
			"indi"          => 'Indirizzo',
			"loca"          => 'Localit&agrave;',
			"cap"           => 'Cap',
			"nazi"          => 'Nazione',
			"prov"          => 'Prov.',
			"email"         => 'Email',
			"tel"           => 'Tel',
			"iva"           => 'P.IVA',
			"nr_ord"        => 'Numero ordine',
			"data"          => 'Data',
			"rif"           => 'Riferimento',
			"colli"         => 'Colli',
			"peso"          => 'Peso L.KG',
			"volume"        => 'Volume',
			"importo"       => 'Importo',
			"vettore"       => 'Vettore',
			"trasp"         => 'Trasportatore',
			"doga"          => 'Dati per anticipo operazione doganali',
			"invio_richiesta" => 'Invio richiesta'
	);

}

$sped = $s->get_spedizione($nr_sped);
// ******************************************************************************************
// Salva dati
// ******************************************************************************************

if ($_REQUEST['fn'] == 'get_json_data_grid'){
	
	$sql="SELECT TDONDO, TDVOLU, TDTOCO, TDPLOR, TDOADO, TDOTPD, TDDTEP, TDVSRF, TDINFI, TDODRE
	FROM {$cfg_mod_Booking['file_testate']} TD
	WHERE TDNBOF='{$nr_sped}'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
	
		$nr['NR_ORD']= implode("_", array($row['TDOADO'], $row['TDONDO'], $row['TDOTPD']));
		$nr['DATA']= $row['TDODRE'];
		$nr['RIFER']= $row['TDVSRF'];
		$nr['COLLI']= $row['TDTOCO'];
		$nr['PESO']= $row['TDPLOR'];
		$nr['VOLUME']= $row['TDVOLU']  * ( (100 + (int)$cfg_mod_Spedizioni['perc_magg_volume_std']) / 100);
		$nr['IMPORTO']= $row['TDINFI'];
	
		$ar[] = $nr;
	
        }
        echo acs_je($ar);
        exit;

}  

if ($_REQUEST['fn'] == 'exe_change_state'){
	
	$m_params = acs_m_params_json_decode();
	
	        $ar_ins['CSFG03'] = $m_params->stato;
	
			$sql = "UPDATE {$cfg_mod_Booking['file_calendario']} SP
			SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			WHERE CSDT='{$id_ditta_default}' AND CSPROG='{$nr_sped}' AND CSCALE = '*SPR'";
	
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
	
			echo db2_stmt_errormsg($stmt);
	
			$ret = array();
			$ret['success'] = true;
			$ret['cod_stato'] = $m_params->stato;
			$ret['desc_stato'] = $conf_stato_booking[$m_params->stato];
			echo acs_je($ret);
			exit;
}

if ($_REQUEST['fn'] == 'exe_save_booking_data'){

	$m_params = acs_m_params_json_decode();
	$form_values= $m_params->form_values;

	$sql = "DELETE FROM {$cfg_mod_Booking['file_indirizzi']}
			WHERE IGDT='{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID='BKSPE' ";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);


	//TRASPORTATORE

	$ar_ins = array();
	$ar_ins['IGDT'] 	= $id_ditta_default;
	$ar_ins['IGUSGE']   = trim($auth->get_user());
	$ar_ins['IGDTGE']   = oggi_AS_date();
	$ar_ins['IGORGE']   = oggi_AS_time();
	$ar_ins['IGUSUM']   = trim($auth->get_user());
	$ar_ins['IGDTUM']   = oggi_AS_date();
	$ar_ins['IGORUM']   = oggi_AS_time();
	$ar_ins['IGKEY1'] 	= $nr_sped;
	$ar_ins['IGKEY2'] 	= 'TRAS';
	$ar_ins['IGTAID'] 	= 'BKSPE';
	$ar_ins['IGRGSO'] 	=  $form_values->TRAS_f_denom;
	$ar_ins['IGINDI'] 	=  $form_values->TRAS_f_indi;
	$ar_ins['IGCAP'] 	=  $form_values->TRAS_f_cap;
	$ar_ins['IGLOCA']	=  $form_values->TRAS_f_loca;
	$ar_ins['IGPROV']	=  $form_values->TRAS_f_prov;
	$ar_ins['IGDNAZ']	=  $form_values->TRAS_f_nazi;
	$ar_ins['IGTEL1']	=  $form_values->TRAS_f_tel;
	$ar_ins['IGMAI1']	=  $form_values->TRAS_f_email;
	$ar_ins['IGPIVA']	=  $form_values->TRAS_f_iva;
	$ar_ins['IGFG01']	=  $form_values->f_time;



	$sql = "INSERT INTO {$cfg_mod_Booking['file_indirizzi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	echo db2_stmt_errormsg($stmt);

	
	//vettore (se non uguale a trasportatore)
	if($form_values->f_vett_tras!='Y'){
		$ar_ins = array();
		$ar_ins['IGDT'] 	=  $id_ditta_default;
		$ar_ins['IGUSGE']   = trim($auth->get_user());
		$ar_ins['IGDTGE']   = oggi_AS_date();
		$ar_ins['IGORGE']   = oggi_AS_time();
		$ar_ins['IGUSUM']   = trim($auth->get_user());
		$ar_ins['IGDTUM']   = oggi_AS_date();
		$ar_ins['IGORUM']   = oggi_AS_time();
		$ar_ins['IGKEY1'] 	=  $nr_sped;
		$ar_ins['IGKEY2'] 	= 'VETT';
		$ar_ins['IGTAID'] 	= 'BKSPE';
		$ar_ins['IGRGSO'] 	=  $form_values->VETT_f_denom;
		$ar_ins['IGINDI'] 	=  $form_values->VETT_f_indi;
		$ar_ins['IGCAP'] 	=  $form_values->VETT_f_cap;
		$ar_ins['IGLOCA']	=  $form_values->VETT_f_loca;
		$ar_ins['IGPROV']	=  $form_values->VETT_f_prov;
		$ar_ins['IGDNAZ']	=  $form_values->VETT_f_nazi;
		$ar_ins['IGTEL1']	=  $form_values->VETT_f_tel;
		$ar_ins['IGMAI1']	=  $form_values->VETT_f_email;
		$ar_ins['IGPIVA']	=  $form_values->VETT_f_iva;
	
		$sql = "INSERT INTO {$cfg_mod_Booking['file_indirizzi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);
	}	



	if($form_values->f_sosta=='Y'){
		//SOSTA TECNICA

		$ar_ins = array();
		$ar_ins['IGDT'] 	= $id_ditta_default;
		$ar_ins['IGUSGE']   = trim($auth->get_user());
		$ar_ins['IGDTGE']   = oggi_AS_date();
		$ar_ins['IGORGE']   = oggi_AS_time();
		$ar_ins['IGUSUM']   = trim($auth->get_user());
		$ar_ins['IGDTUM']   = oggi_AS_date();
		$ar_ins['IGORUM']   = oggi_AS_time();
		$ar_ins['IGKEY1'] 	= $nr_sped;
		$ar_ins['IGKEY2'] 	= 'STEC';
		$ar_ins['IGTAID'] 	= 'BKSPE';
		$ar_ins['IGRGSO'] 	= $form_values->STEC_f_denom;
		$ar_ins['IGINDI'] 	= $form_values->STEC_f_indi;
		$ar_ins['IGCAP'] 	=  $form_values->STEC_f_cap;
		$ar_ins['IGLOCA']	=  $form_values->STEC_f_loca;
		$ar_ins['IGPROV']	=  $form_values->STEC_f_prov;
		$ar_ins['IGDNAZ']	=  $form_values->STEC_f_nazi;
		$ar_ins['IGTEL1']	=  $form_values->STEC_f_tel;
		$ar_ins['IGMAI1']	=  $form_values->STEC_f_email;
		$ar_ins['IGPIVA']	=  $form_values->STEC_f_iva;

		$sql = "INSERT INTO {$cfg_mod_Booking['file_indirizzi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
	}

	//ANTICIPO OPERAZIONI DOGANALI

	$ar_ins = array();
	$ar_ins['IGDT'] 	= $id_ditta_default;
	$ar_ins['IGUSGE']   = trim($auth->get_user());
	$ar_ins['IGDTGE']   = oggi_AS_date();
	$ar_ins['IGORGE']   = oggi_AS_time();
	$ar_ins['IGUSUM']   = trim($auth->get_user());
	$ar_ins['IGDTUM']   = oggi_AS_date();
	$ar_ins['IGORUM']   = oggi_AS_time();
	$ar_ins['IGKEY1'] 	= $nr_sped;
	$ar_ins['IGKEY2'] 	= 'DOGA';
	$ar_ins['IGTAID'] 	= 'BKSPE';
	$ar_ins['IGRGSO'] 	=  $form_values->f_denom_doga;
	$ar_ins['IGTEL1']	=  $form_values->f_tel_doga;
	$ar_ins['IGMAI1']	=  $form_values->f_email_doga;
	$ar_ins['IGMAI2']	=  $form_values->f_email2_doga;
	$ar_ins['IGDNAZ']	=  $form_values->f_booking;
	$ar_ins['IGLOCA']	=  $form_values->f_targa;
	//$ar_ins['IGCDFI']	=  $form_values->f_data;
	if($form_values->f_vett_tras != '')
		$ar_ins['IGFG02']	=  $form_values->f_vett_tras;
	if($form_values->f_sosta != '')
		$ar_ins['IGFG03']	=  $form_values->f_sosta;
	


	$sql = "INSERT INTO {$cfg_mod_Booking['file_indirizzi']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	echo db2_stmt_errormsg($stmt);

	$bstime = $m_params->bstime;
	
	if (strlen($bstime) == 0 && $m_params->mod_sped == 'Y'){
		//Da modifica spedizione: INSERT nuova richiesta
		$bstime = microtime(true);
		$sped = $s->get_spedizione($nr_sped);
		$value = $s->get_totali_by_TDNBOF($nr_sped, $id_ditta_default);
		
		//recupero il primo codice cliente della spedizione
		$cliente_cod = $s->get_first_TDCCON_by_TDNBOF($nr_sped);
		
		$ar_ins = array();
		$ar_ins['BSTIME'] 	= $bstime;
		$ar_ins['BSUSGE']   = trim($auth->get_user());
		$ar_ins['BSDTGE']   = oggi_AS_date();
		$ar_ins['BSORGE']   = oggi_AS_time();
		$ar_ins['BSDT'] 	= $id_ditta_default;
		$ar_ins['BSNBOC'] 	= $nr_sped;
		$ar_ins['BSCCON'] 	= $cliente_cod;
		$ar_ins['BSCITI'] 	= $sped['CSCITI'];
		$ar_ins['BSSTAT'] 	= '01';					//in stato inviata
		$ar_ins['BSVOLU'] 	= $value['TDVOLU'];
		$ar_ins['BSPLOR'] 	= $value['S_PESO'];
		$ar_ins['BSPNET'] 	= $value['S_PESO_N'];
		$ar_ins['BSTOCO'] 	= $value['S_COLLI'];
		$ar_ins['BSDTSR'] 	= $form_values->f_data;		
		$ar_ins['BSELCA']	= $s->get_el_carichi_by_TDNBOF($nr_sped, 'AN');
			
		$sql = "INSERT INTO {$cfg_mod_Booking['file_richieste']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);
		
	} else {
		//UPDATE, MODIFICO BS0 CON STATO 01
		$ar_ins = array();
		$ar_ins['BSSTAT'] 	= '01';
		$ar_ins['BSDTSR'] 	= $form_values->f_data;
		
		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$bstime}' ";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
		echo db2_stmt_errormsg($stmt);
		
	}

	
	//data verde, accettazione logistica automatica
	if ((int)$form_values->f_stato_programmazione == 1){
		$ar_ins = array();
		$ar_ins['BSFLAL'] 	= 'Y';
		$ar_ins['BSUSAL']   = trim($auth->get_user());
		$ar_ins['BSDTAL']   = oggi_AS_date();
		$ar_ins['BSORAL']   = oggi_AS_time();
		
		
		$sql = "UPDATE {$cfg_mod_Booking['file_richieste']}
				SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
				WHERE BSTIME= '{$bstime}' ";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);
	
		auto_accettazione_amministrativa();
		set_conferma($bstime); 

	}

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}




// ******************************************************************************************
// Verifica num sped inserito
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verify_num_sped'){
	$ret = array();
	$ret = $main_module->verify_num_sped($m_params->form_values->f_chiave, $m_params->form_values->f_cliente);

	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// Form inserimento dati
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_parameters'){

	$m_params = acs_m_params_json_decode();

	$form_values= $m_params->form_values;
	$cliente = $m_params->form_values->f_cliente;
	
	//CLIENTE
	$sql_cli="SELECT TDDCON
	FROM {$cfg_mod_Booking['file_testate']} TD
	WHERE TDDT= '{$id_ditta_default}' AND TDNBOF='{$nr_sped}' AND TDCCON= '{$cliente}'";
	
	$stmt_cli= db2_prepare($conn, $sql_cli);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_cli);
	
	$row_cli = db2_fetch_assoc($stmt_cli);
    $rag_sociale = trim($row_cli['TDDCON']);
    

	//TRASPORTATORE
	$sql_tras="SELECT *
				FROM {$cfg_mod_Booking['file_indirizzi']} IG
				WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='TRAS'";

	

	$stmt_tras= db2_prepare($conn, $sql_tras);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_tras);

	$row_tras = db2_fetch_assoc($stmt_tras);

	$ret_tras = array(
			"denom"  => acs_u8e(trim($row_tras['IGRGSO'])),
			"indirizzo"  => acs_u8e(trim($row_tras['IGINDI'])),
			"cap"  => acs_u8e(trim($row_tras['IGCAP'])),
			"loca"  => acs_u8e(trim($row_tras['IGLOCA'])),
			"prov"  => acs_u8e(trim($row_tras['IGPROV'])),
			"nazione"  => acs_u8e(trim($row_tras['IGDNAZ'])),
			"p_iva"  => acs_u8e(trim($row_tras['IGPIVA'])),
			"telefono"  => acs_u8e(trim($row_tras['IGTEL1'])),
			"email"  => acs_u8e(trim($row_tras['IGMAI1'])),
			"orario"  => acs_u8e(trim($row_tras['IGFG01']))


	);

	$spedizione = $s->get_spedizione($nr_sped);

	//VETTORE
	$sql_vett="SELECT *
				FROM {$cfg_mod_Booking['file_indirizzi']} IG
				WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='VETT'";

	$stmt_vett= db2_prepare($conn, $sql_vett);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_vett);

	$row_vett = db2_fetch_assoc($stmt_vett);

	$ret_vett = array(
			"denom"  => acs_u8e(trim($row_vett['IGRGSO'])),
			"indirizzo"  => acs_u8e(trim($row_vett['IGINDI'])),
			"cap"  => acs_u8e(trim($row_vett['IGCAP'])),
			"loca"  => acs_u8e(trim($row_vett['IGLOCA'])),
			"prov"  => acs_u8e(trim($row_vett['IGPROV'])),
			"nazione"  => acs_u8e(trim($row_vett['IGDNAZ'])),
			"p_iva"  => acs_u8e(trim($row_vett['IGPIVA'])),
			"telefono"  => acs_u8e(trim($row_vett['IGTEL1'])),
			"email"  => acs_u8e(trim($row_vett['IGMAI1'])),

	);

	//SOSTA TECNICA
	$sql_stec="SELECT *
	FROM {$cfg_mod_Booking['file_indirizzi']} IG
	WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='STEC'";

	$stmt_stec= db2_prepare($conn, $sql_stec);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_stec);

	$row_stec = db2_fetch_assoc($stmt_stec);

	$ret_stec = array(
			"denom"  => acs_u8e(trim($row_stec['IGRGSO'])),
			"indirizzo"  => acs_u8e(trim($row_stec['IGINDI'])),
			"cap"  => acs_u8e(trim($row_stec['IGCAP'])),
			"loca"  => acs_u8e(trim($row_stec['IGLOCA'])),
			"prov"  => acs_u8e(trim($row_stec['IGPROV'])),
			"nazione"  => acs_u8e(trim($row_stec['IGDNAZ'])),
			"p_iva"  => acs_u8e(trim($row_stec['IGPIVA'])),
			"telefono"  => acs_u8e(trim($row_stec['IGTEL1'])),
			"email"  => acs_u8e(trim($row_stec['IGMAI1'])),

	);

	
	//ANTICIPO OPERAZIONE DOGANALI
	$sql_doga="SELECT *
				FROM {$cfg_mod_Booking['file_indirizzi']} IG
				WHERE IGDT= '{$id_ditta_default}' AND IGKEY1='{$nr_sped}' AND IGTAID= 'BKSPE' AND IGKEY2='DOGA'";


	$stmt_doga= db2_prepare($conn, $sql_doga);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_doga);

	$row_doga = db2_fetch_assoc($stmt_doga);

	$ret_doga = array(
			"denom"  => acs_u8e(trim($row_doga['IGRGSO'])),
			"telefono"  => acs_u8e(trim($row_doga['IGTEL1'])),
			"email"  => acs_u8e(trim($row_doga['IGMAI1'])),
			"email2"  => acs_u8e(trim($row_doga['IGMAI2'])),
			"nr_book"  => acs_u8e(trim($row_doga['IGDNAZ'])),
			"targa"  => acs_u8e(trim($row_doga['IGLOCA'])),
			//"data_ritiro"  => acs_u8e(trim($row_doga['IGCDFI'])),
			"vett_trasp"  => acs_u8e(trim($row_doga['IGFG02'])),
			"sosta"  => acs_u8e(trim($row_doga['IGFG03']))
			

	);

	//nr booking
	
	$sped_id = sprintf("%08s", $nr_sped);
	$tataid = 'SPINT';
	
    $sql_b = "SELECT * FROM {$cfg_mod_Booking['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
	$stmt_b = db2_prepare($conn, $sql_b);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt_b, array($id_ditta_default, $tataid, $sped_id));
	$row_b = db2_fetch_assoc($stmt_b);
	
	if ($m_params->visualizza == 'Y') {
		$disabilita_nuovo_inserimento = "true";
		$br = bk_get_row($m_params->bstime);
		$data_ritiro = $br['BSDTSR'];		
	} else {
		
		$disabilita_nuovo_inserimento = "false";
		
		$sql_c= "SELECT COUNT(*) as C_ROW, max(BSTIME) AS M_BSTIME FROM {$cfg_mod_Booking['file_richieste']} BS
				 WHERE BSNBOC='{$nr_sped}' AND  BSSTAT IN ('01', '02')";			
		
		$stmt_c = db2_prepare($conn, $sql_c);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt_c);
			
		$r = db2_fetch_assoc($stmt_c);
			
		if ($r['C_ROW'] > 0){
			$disabilita_nuovo_inserimento = "true";
			
			$br = bk_get_row($r['M_BSTIME']);
			$data_ritiro = $br['BSDTSR'];
		}
	}
		
	
	//se sono in modifica testata e non posso inviare una nuova richiesta... avviso
	//---> non mostro piu' messaggio di errore ma vede in visualizzazione l'ultima richiesta in corso
	if (1==2 && $disabilita_nuovo_inserimento == "true" && $m_params->mod_sped=='Y'){
	?>
		{"success":true, "items": [
			{
	            xtype: 'form',
	            
	            listeners: {
		 		    afterrender: function (comp) {
						comp.up('window').setTitle('Booking information - Scheda informazioni di prenotazione ritiro merce');	 				
		 			}            
	            },	            
	            
	            bodyStyle: 'padding: 50px',
	            bodyPadding: '150 150 150 150',
	            frame: true,
	            title: '',
	            layout: {
				    type: 'vbox',
				    align: 'stretch',
				    pack : 'start',
				},
				items: [
		    		{
		    			html: 'Esiste gi&agrave; una richiesta ricevuta o confermata per questa spedizione',
		    			flex: 1		
		    		}, {
		    			xtype: 'button',
		    			text:  'Chiudi',
		    			scale: 'large',
		    			handler: function() {
		    				this.up('window').close();
						}
		    		}
		    	]
		    }
    	 ]
    	}
	<?php	
		exit;
	}

	
	?>
	
{"success":true, "items": [

    {
            xtype: 'panel',
            title: '',
            autoScroll: true,

            listeners: {
	 		    afterrender: function (comp) {
					comp.up('window').setTitle('Booking information - Scheda informazioni di prenotazione ritiro merce');	 				
	 			}            
            },	            
 
            layout: 'fit',
			  items: [ 

        {   xtype: 'form',
            frame: true,
            autoScroll: true,
            buttons: [
            
				{
		            text: '<?php echo $trad['invio_richiesta']; ?>',
		            itemId: 'b_invia',
		            disabled: <?php echo $disabilita_nuovo_inserimento; ?>,
	            	iconCls: 'icon-button_red_play-24',
	            	scale: 'medium',	            
		            handler: function() {
		            	var m_form = this.up('form').getForm();
		            	var m_win  = this.up('window');
		            	

		            	var b_invia = this.up('form').down('#b_invia');	
		            	var b_report = this.up('form').down('#b_report');

						if(m_form.isValid()){
						
		            	b_invia.disable();						
						
						Ext.MessageBox.show({
                            msg: 'Loading',
                            progressText: 'Loading...',
                            width:200,
                            wait:true,
                            waitConfig: {interval:200},
                            icon:'ext-mb-download', //custom class in msg-box.html               
                        });  
						
							Ext.Ajax.request({
							        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save_booking_data',
							        jsonData: {
							        	form_values: m_form.getValues(),
							        	nr_sped: <?php echo $nr_sped;?>,
							        	mod_sped: <?php echo j($m_params->mod_sped); ?>,
							        	bstime: <?php echo j($m_params->form_values->bstime); ?>
							        },	
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){	            	  													        
							            var jsonData = Ext.decode(result.responseText);
							            if (jsonData.success == true){
											Ext.MessageBox.hide();							            
											acs_show_msg_info('Operazione conclusa con successo');
											b_report.enable();											
							            
							            } else {
							            	acs_show_msg_error('Errore in fase di salvataggio dati');
							            	b_invia.enable();
							            }							            	
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							            b_invia.enable();							            
							        }
							    });						
					    }
		                
		            } //handler
		        }, {
		            text: 'Report',
		            itemId: 'b_report',
	            	iconCls: 'icon-print-24',
            		disabled: true,
	            	scale: 'medium',	            
		            handler: function() {
		            	var m_form = this.up('form').getForm();
		            	var m_win  = this.up('window');  
						window.open ('<?php echo ROOT_PATH ?>/module/desk_booking/acs_booking_report.php?nr_sped=' + <?php echo $nr_sped; ?>,"mywindow");	
		            }  
		            
		            } 
		            
		            <?php if($m_params->mod_sped=='Y'){?>	
		            , {
		            text: 'Report booking',
	            	iconCls: 'icon-print-24',
	            	scale: 'medium',	            
		            handler: function() {
		            	var m_form = this.up('form').getForm();
		            	var m_win  = this.up('window');  
		            	window.open ('../desk_booking/acs_booking_report.php?nr_sped=' + <?php echo $nr_sped; ?> + '&controllo=Y'  ,"mywindow");	
									                
		            	}  
		            
		            }  
		           <?php }?> 
		              
            ],
            
            layout: {
               type: 'vbox',
                align: 'stretch'
     		},
			                        
            
            items: [{ 
						xtype: 'fieldcontainer',
						
						height: 90,
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},												
						items: [
						
									 {
					xtype: 'fieldset',
	                layout: 'anchor',
	                flex:2,
	                items: [
								  { 
						xtype: 'fieldcontainer',
						flex: 2,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {						
									name: 'f_rag_soc',
									xtype: 'displayfield',
									fieldLabel: '<?php echo $trad['cli']; ?>',
									labelAlign: 'left',
									value: <?php echo j(trim($rag_sociale)); ?>,
									labelWidth: 70,
									//flex:2,
									margin: "5 10 5 10"	
								 }, { 
								xtype: 'fieldcontainer',
								layout: { 	type: 'hbox',
										    pack: 'start',
										    align: 'stretch'},						
								items: [
						    	 {
								xtype: 'textfield',
								name: 'f_booking',
								fieldLabel: '<?php echo $trad['nr_book']; ?>',
								margin: "0 10 10 10",	
							    labelWidth: 80,
							    allowBlank: true,
							    value: '<?php echo $ret_doga['nr_book'] ?>',
							    flex: 1							
								}, {
								xtype: 'textfield',
								name: 'f_targa',
								labelAlign: 'right',
								fieldLabel: '<?php echo $trad['targa']; ?><span style="color:red;">*</span>',
								margin: '0 10 10 0',
							    labelWidth: 100,
							    value: <?php echo j($ret_doga['targa']); ?>,
							    flex: 1,
								allowBlank: false,
								maxLength: 20
								}
						
						]}
						
						  ]
	             }
						
						,
						
						]}, 
						
													 {
					xtype: 'fieldset',
	                layout: 'anchor',
	                flex:1,
	                items: [
						{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{
						        xtype: 'fieldcontainer',
						        fieldLabel: '<?php echo $trad['data_ritiro']; ?><span style="color:red;">*</span>',
						        labelAlign: 'right',
								defaultType: 'textfield',
			 			        layout: 'hbox',
			 			        items: [						
						
									  {
									  	    name: 'f_stato_programmazione',
									  	    xtype: 'hiddenfield'
									  }, {
										    name: 'f_data'
										   , margin: "5 10 0 10"						     
										   , flex: 1				                     		
										   , xtype: 'datefield'
										   , startDay: 1 //lun.
										   , labelAlign: 'right'						   
										   , format: 'd/m/Y'
										   , submitFormat: 'Ymd'
										   , allowBlank: false
										   , readOnly: true
										  // , anchor: '-15'
										   , value: '<?php echo print_date($data_ritiro, "%d/%m/%Y"); ?>'
										   , listeners: {
										       invalid: function (field, msg) {
										       Ext.Msg.alert('', msg);}
											 }
										}, {
											name: 'f_sped_coll_out_set',										  
											xtype: 'displayfield',
											editable: false,
											fieldLabel: '',
											padding: '0 0 0 10',
											frame: true,
										    value: <?php echo j("<img src=" . img_path("icone/48x48/calendar.png") . " width=28>"); ?>,
											listeners: {
											            render: function( component ) {
											                component.getEl().on('click', function( event, el ) {
											                	acs_show_win_std(<?php echo j($trad['data_ritiro']); ?>, 
											                			'<?php echo ROOT_PATH ?>module/desk_booking/acs_seleziona_data_ritiro.php?fn=open_form', 
											                			{
											                				sped_id: <?php echo $nr_sped;?>,
											                				lng: <?php echo j($m_params->form_values->lng) ?>
											                			}, 700, 550, {
											                				//my_listeners
											                				onSelected: function(from_win, data, stato_programmazione){
											                					from_win.close();
											                					var m_form = component.up('form').getForm();
											                					m_form.findField('f_data').setValue(js_date_from_AS(data));
											                					m_form.findField('f_stato_programmazione').setValue(stato_programmazione);
											                				}
											                			}, 'icon-leaf-16');
											                });
											            }
											}
										}
								]
						}
							
							
							
							
							
							, {
								name: 'f_time',
								xtype: 'radiogroup',
								//flex: 1,
								allowBlank: true,
								margin: "0 10 0 30",
								value: '<?php echo $ret_tras['orario']; ?>',
								items: [{
		                            xtype: 'radio'
		                          , name: 'f_time' 
		                          , boxLabel: '<?php echo $trad['mattina']; ?>'
		                          , inputValue: 'M'
		                          <?php if ($ret_tras['orario']== 'M' || $ret_tras['orario']== ''){ ?>
		                          , checked: true
		                          <?php }?>
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_time' 
		                          , boxLabel: '<?php echo $trad['pome']; ?>'
		                          , inputValue: 'P'
		                           <?php if ($ret_tras['orario']== 'P'){ ?>
		                          , checked: true
		                          <?php }?>
		                          
		                        }]
						}
						
						]}
						
						]}
						
						,
									 {
					xtype: 'fieldset',
	                layout: 'anchor',
	                flex:1,
	                items: [
						
						{ 
						xtype: 'fieldcontainer',
						flex: 1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						  {
														 
								name: 'f_vett_tras',
								xtype: 'checkboxgroup',
								fieldLabel: '<?php echo $trad['vatt_trasp']; ?>',
								margin: "5 10 0 30",
								allowBlank: true,
							   	labelWidth: 200,
							   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_vett_tras' 
									<?php if($ret_doga['vett_trasp'] == 'Y'){?>
		                          		, checked: true
		                            <?php }else{?>
		                          		, checked: false
		                          <?php }?>

		                          , inputValue: 'Y'
		                       	  , listeners: {
								    change: function(checkbox, newValue, oldValue, eOpts) {
								    	var m_form = this.up('form');
								    	var fs_vettore = m_form.down('#fs_VETT');
								    	
			                    	    if (newValue == false)
			                    	    	fs_vettore.enable();
			                    	    else
			                    	    	fs_vettore.disable();	
			                    	  
								    }
								}
		                        }]														
						 },   {
								width: 200,						 
								name: 'f_sosta',
								xtype: 'checkboxgroup',
								fieldLabel: '<?php echo $trad['sosta_tec']; ?>',
								allowBlank: true,
							   	labelWidth: 200,
							    margin: "0 10 0 30",
							   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_sosta' 
		                          , boxLabel: ''
		                          <?php  if( $ret_doga['sosta'] == 'Y'){?>
		                          , checked: true
		                          <?php }else{?>
		                          , checked: false
		                          <?php }?>
		                          , inputValue: 'Y'
		                       	  , listeners: {
								    change: function(checkbox, newValue, oldValue, eOpts) {
								    	var m_form = this.up('form');
								    	var fs_sosta_tecnica = m_form.down('#fs_STEC');
								    	
			                    	    if (newValue == true)
			                    	    	fs_sosta_tecnica.enable();
			                    	    else
			                    	    	fs_sosta_tecnica.disable();	
			                    	  
								    }
								}
		                        }]														
						 }
						
						]}
						
						     ]}
						]
					 },	
					 	 
					 
							{ 
						xtype: 'fieldcontainer',
					    layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						defaults: {padding: '2px 10px'},						
						items: [		
								<?php dettagli_anagrafici('TRAS', $trad['trasp'], 'false', $ret_tras, $trad) ?>,
								<?php
								
								if($ret_doga['vett_trasp'] == 'Y'){
									dettagli_anagrafici('VETT', $trad['vettore'], 'true', $ret_vett, $trad);
								}else{
									dettagli_anagrafici('VETT', $trad['vettore'], 'false', $ret_vett, $trad);
								}
								?>

						
						]
					 },
					 
					 					{ 
						xtype: 'fieldcontainer',
						
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
							{
							xtype: 'grid',
							flex: 1,
							height: 250, 
							name: 'f_ordini',
						    loadMask: true,	
							features: [{
								ftype: 'filters',
								encode: false, 
								local: true,   
						    filters: [
						       {
						 	type: 'boolean',
							dataIndex: 'visible'
						     }
						      ],
						      ftype: 'summary'
						}],	
							store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							           extraParams: {
			                                  nr_sped: <?php echo $nr_sped; ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['NR_ORD', 'DATA', 'RIFER', {name: 'VOLUME', type: 'float'}, {name: 'COLLI', type: 'float'}, {name: 'IMPORTO', type: 'float'}, {name: 'PESO', type: 'float'}]							
									
			}, //store
				

			      columns: [	
			      {
	                header   : '<?php echo $trad['nr_ord']; ?>',
	                dataIndex: 'NR_ORD',
	                flex: 1,  
	                summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return 'Totale'; 
                                } 
	              
	            },{
	                header   : '<?php echo $trad['data']; ?>',
	                dataIndex: 'DATA',
	                width: 60,
	                renderer: date_from_AS  
	             
	            },{
	                header   : '<?php echo $trad['rif']; ?>',
	                dataIndex: 'RIFER',
	                flex: 1,  
	             
	            },
				{
	                header   : '<?php echo $trad['colli']; ?>',
	                dataIndex: 'COLLI',
	                width: 80,
	                align: 'right',
	                summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return value; 
                                } 
	            },
	                
	            {
	                header   : '<?php echo $trad['peso']; ?>',
	                dataIndex: 'PESO',
	                width: 80,
	                align: 'right',
	                renderer: floatRenderer2,
	                 summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            },
	            {
	                header   : '<?php echo $trad['volume']; ?>',
	                dataIndex: 'VOLUME',
	                width: 80,
	                align: 'right',
	                renderer: floatRenderer2, summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            }, {
	                header   : '<?php echo $trad['importo']; ?>',
	                dataIndex: 'IMPORTO',
	                width: 80,
	                align: 'right',
	                renderer: floatRenderer2, summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            }
	            
	        
	         ] 	
	         
						},		{ 
						xtype: 'fieldcontainer',
						flex:1,
						layout: { 	type: 'vbox',
								    pack: 'start',
								    align: 'stretch'},
						defaults: {padding: '2px 10px'},								    
						items: [
								 <?php 
								 
								 if($ret_stec['denom'] != ''){
								 	dettagli_anagrafici('STEC', $trad['sosta_tec'], 'false', $ret_stec, $trad); 
								 }else{
								 	dettagli_anagrafici('STEC', $trad['sosta_tec'], 'true', $ret_stec, $trad);
								 }
								 ?>
								 
								 
							, {
					xtype: 'fieldset',
					margin: '2 0 0 0',
	                title: '<?php echo $trad['doga']; ?>',
	                layout: 'anchor',
	                flex:1,
	                items: [
						{
									xtype: 'fieldcontainer',
									flex:1,
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [
						
						  {
								name: 'f_denom_doga',
								fieldLabel: '<?php echo $trad['denom']; ?><span style="color:red;">*</span>',
								labelWidth: 100,
								margin: '0 10 0 0',
							    maxLength: 100,
							    flex: 2,
							    value: '<?php echo $ret_doga['denom']; ?>',
							    allowBlank: false
							   	}, {
								name: 'f_tel_doga',
								fieldLabel: '<?php echo $trad['tel']; ?><span style="color:red;">*</span>',
							    maxLength: 20,
							    flex: 1,
							    labelWidth: 60,							    
							    value: '<?php echo $ret_doga['telefono']; ?>',
							    allowBlank: false
							}
								  ]
								}	,
								
								{
									xtype: 'fieldcontainer',
									flex:1,
									margin: '0 0 10 0',
									layout: {type: 'hbox', pack: 'start', align: 'stretch'},
									defaults:{xtype: 'textfield'},
									items: [
						     {	name: 'f_email_doga',
								
								fieldLabel: '<?php echo $trad['email']; ?><span style="color:red;">*</span>',
							   	flex: 2,
							    labelWidth: 100,
							    value: '<?php echo $ret_doga['email']; ?>',
							    allowBlank: false							
								},{	name: 'f_email2_doga',
								labelAlign: 'right',
								fieldLabel: 'Email 2',
							   	flex: 2,
							    labelWidth: 100,
							    value: '<?php echo $ret_doga['email2']; ?>',
							    allowBlank: true
								}
								  ]
								}	
					 
	             ]
	             }
						
						]
					 },
					 
					 
					
						
						]
					 }   //fine hbox
									 
			 
					

            ]         
				
        }
        
        ]}
			
]}
	
<?php
 exit;
}