<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

// funzione di utilita'
function get_ar_tipo_stato($row){
	global $auth, $s;
	$js_parameters = $auth->get_module_parameters($s->get_cod_mod());	
	global $cfg_mod_Spedizioni, $conn;
	$ret = array();
	
	if ($js_parameters->alert_CF == 1)
		$where_alert_CF = " TDDTCF > 0";
	else 
		$where_alert_CF = " 1=1 ";

	$sql = "SELECT DISTINCT RDTPNO, RDRIFE
				FROM {$cfg_mod_Spedizioni['file_righe']}
				INNER JOIN {$cfg_mod_Spedizioni['file_testate']}
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
				WHERE RDFMAN IN('M', 'C') AND {$where_alert_CF}
					
				AND TDDT=" . sql_t($row['TDDT']) . " AND TDOTID=" . sql_t($row['TDOTID'])  . "
				AND TDOINU=" . sql_t($row['TDOINU']) . " AND TDOADO=" . sql_t($row['TDOADO'])  . " AND TDONDO=" . sql_t($row['TDONDO'])  . "
						AND RDDTDS = " . $row['RDDTDS'] . " /* DATA MAGGIORE */
				
					GROUP BY RDRIFE, RDTPNO
					ORDER BY RDRIFE
					";

					$stmt = db2_prepare($conn, $sql);
					$result = db2_execute($stmt);

	while ($r = db2_fetch_assoc($stmt)) {
	$ret[] = acs_u8e(implode(' ', array(trim($r['RDTPNO']), trim($r['RDRIFE']))));
	}
	return $ret;
	}



// ******************************************************************************************
// Recupero singolo record per aggiornamento
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_record'){

	$ret = array();
 	$m_params = acs_m_params_json_decode();
 	$ret['ha_commenti']	= $s->has_commento_ordine_by_k_ordine($m_params->k_ordine); 	
	$ar_note = $s->get_commento_ordine_by_k_ordine($m_params->k_ordine);	
	//$ret['commenti'] = implode('<br>', $ar_note);
	$ret['commenti'] = implode('<br>', array_map('utf8_encode', $ar_note['text']));
	
	echo acs_je($ret);
 exit;
}	


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){

	$ar_seleted_stato = explode("|", $_REQUEST['list_selected_stato']);
	if (count($ar_seleted_stato) > 0 && strlen($_REQUEST['list_selected_stato']) > 0){
		$m_where = " AND RDRIFE IN (" . sql_t_IN($ar_seleted_stato) . ") ";
	}
	else $m_where = '';

	//bug parametri linux
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['form_ep'] = strtr($_REQUEST['form_ep'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	
	$form_ep = (array)json_decode($_REQUEST['form_ep']);
	
	if (strlen(trim($form_ep['f_data_da'])) > 0)
		$m_where .= " AND TDDTEP >= {$form_ep['f_data_da']}";
	if (strlen(trim($form_ep['f_data_a'])) > 0)
		$m_where .= " AND TDDTEP <= {$form_ep['f_data_a']}";	
	
	if (strlen(trim($form_ep['f_referente_cliente'])) > 0)
		$m_where .= " AND TDCOCL = '{$form_ep['f_referente_cliente']}'";	
	
	if (strlen(trim($form_ep['f_area'])) > 0)
		$m_where .= " AND TA_ITIN.TAASPE = '{$form_ep['f_area']}'";	
	
	if(strlen(trim($form_ep['f_tipo']))> 0)
	    $m_where .= " AND RDTPNO = '{$form_ep['f_tipo']}'"; //MTO
	    // MTS, TDFMTS = 'Y'
	
	if(trim($form_ep['f_blocco']) == 'A'){
	    $m_where .= " AND TDBLEV = 'Y'";

	}elseif(trim($form_ep['f_blocco']) == 'C'){
	    $m_where .= " AND TDBLOC = 'Y'";
	    
	}elseif(trim($form_ep['f_blocco']) == 'N'){
	    $m_where .= " AND TDBLEV <> 'Y' AND TDBLOC <> 'Y'";
	}
	//costruzione sql
	
	global $cfg_mod_Spedizioni;
	
	
	global $auth, $s;
	$js_parameters = $auth->get_module_parameters($s->get_cod_mod());	
	if ($js_parameters->alert_CF == 1)
		$m_where .= " AND TDDTCF > 0";
	
	
	
	$sql = "SELECT TDDTEP, TDDTDS, TDCCON, TDDCON
			, TDDT, TDOTID, TDOINU, TDOADO, TDONDO, TDDOCU
			, TDVSRF, TDOTPD, TDSTAT, TDDOCL, TDCOCL, CSCITI,
			MAX(RDDTDS) AS RDDTDS, SUM(RDQTA) AS RDQTA
	        FROM {$cfg_mod_Spedizioni['file_righe']}
			INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TD.TDDT=SP.CSDT AND TD.TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
							  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI			  
				ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO
					WHERE RDFMAN IN('M', 'C') {$m_where} 
					GROUP BY TDDTEP, TDDTDS, TDCCON, TDDCON, TDDT, TDOTID, TDOINU, TDOADO, TDONDO, TDDOCU, TDVSRF, TDOTPD, TDSTAT, TDDOCL, TDCOCL, CSCITI
					ORDER BY TDDTEP, TDONDO
					";

	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);
	
	$ar = array();
	$ar_tot = array();	
	
	$autDeroghe = new SpedAutorizzazioniDeroghe();
	$as			= new SpedAssegnazioneOrdini();
	
	while ($r = db2_fetch_assoc($stmt)) {		
		$k_ordine = $r['TDDOCU'];
		$r['k_ordine'] = $k_ordine;		
		$r['k_ordine_out'] = implode('_' , array(trim($r['TDOADO']), trim($r['TDONDO']), trim($r['TDOTPD'])));
		
		//costruisco un elenco di tipo-stato (per ultima data disponibilita)
		$r['ar_tipo_stato'] = implode('<br>', get_ar_tipo_stato($r));
		
		$r['ha_deroghe']	= $autDeroghe->ha_deroghe_by_k_ordine($k_ordine);
		$r['ha_commenti']	= $s->has_commento_ordine_by_k_ordine($k_ordine);
		
		//elenco deroghe per tooltip
		if ($r['ha_deroghe'] == 1){
			$ar_deroghe = array();
			$array_deroghe = $autDeroghe->array_deroghe_by_k_ordine($k_ordine);
			foreach ($array_deroghe as $r_deroga){
				$ar_deroghe[] = "[" . trim($r_deroga['ADUSRI']) . "] " . print_date($r_deroga['ADDTDE']) . " - " . trim($s->decod_std('AUDE', $r_deroga['ADAUTOR'])) . ": " .  acs_u8e(trim($r_deroga['ADCOMM']));
			}
			$r['tooltip_deroghe'] = implode("<br>", $ar_deroghe);
		} else $r['tooltip_deroghe'] = '';
		
		
		$r['ha_POSTM']		= $as->stato_entry_per_ordine_causale($k_ordine, 'POSTM');
		
		//Note per ordine
		$ar_note = $s->get_commento_ordine_by_k_ordine($k_ordine, null, true);
		$r['NOTE'] = implode('<br>', $ar_note);
		$r['TDDCON'] = acs_u8e($r['TDDCON']);
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}






// ******************************************************************************************
// DATI PER GRID STATO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_stato'){


	//costruzione sql

	global $cfg_mod_Spedizioni;

	$sql = "SELECT DISTINCT RDRIFE FROM {$cfg_mod_Spedizioni['file_righe']}
					WHERE RDFMAN IN('M', 'C') 
					";

					$stmt = db2_prepare($conn, $sql);
					$result = db2_execute($stmt);

					$ar = array();
					$ar_tot = array();

					while ($r = db2_fetch_assoc($stmt)) {
						$ar[] = $r;
					}
		echo acs_je($ar);
	exit();
}









// ******************************************************************************************
// GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_grid'){
	
	$m_params = acs_m_params_json_decode();
	$list_selected_stato = $m_params->selected_id;
	$list_selected_stato_js = acs_je(implode("|", $list_selected_stato));
	
?>
	
{"success":true, "items":
 {
  xtype: 'panel',
  title: 'Alert',
  id: 'panel-alert',
  <?php echo make_tab_closable(); ?>,
  
        tbar: new Ext.Toolbar({
            items:['<b>Segnalazione articoli critici/mancanti per data evasione programmata/ordine cliente</b>', '->',
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').down('grid').getStore().load();}}
       		<?php echo make_tbar_closable() ?>
         ]            
        }),  
  
  autoScroll: true,    
  items: [		
	{
		xtype: 'grid',
		id: 'tab-alert',
		loadMask: true,
		
		features: [
		new Ext.create('Ext.grid.feature.Grouping',{
			groupHeaderTpl: 'Raggruppamento: {name}',
			hideGroupedHeader: false
		}),
		{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true,   // defaults to false (remote filtering)
	
			// Filters are most naturally placed in the column definition, but can also be added here.
		filters: [
		{
			type: 'boolean',
			dataIndex: 'visible'
		}
		]
		}],
	
		store: {
			xtype: 'store',
				
			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: 'acs_form_json_articoli_mancanti_critici.php?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {list_selected_stato: <?php echo $list_selected_stato_js; ?>, form_ep: <?php echo j(acs_je($m_params->form_ep)); ?>}				
			},
				
			fields: [
				'RDFORN', 'RDDFOR', 'RDTPNO', 'TDDTEP', 'TDDTDS', 'TDCCON', 'TDDCON', 'TDOADO', 'TDONDO', 'TDDOCU', 'TDVSRF', 'TDOTPD', 'TDSTAT', 'TDDOCL', 'TDCOCL', 'CSCITI',
				'RDART', 'RDDES1', 'RDUM', 'RDDTDS', 'RDODER', 'RDRIFE', 'RDDES2', 'RDQTA', 'k_ordine', 'k_ordine_out', 'NOTE', 'ar_tipo_stato', 'ha_deroghe', 'ha_commenti', 'tooltip_deroghe', 'ha_POSTM'
					]
						
						
		}, //store
		multiSelect: false,
	
		columns: [
		{header: 'Data', renderer: date_from_AS, dataIndex: 'TDDTEP', width: 80, filter : {width: 150}},		
		{header: 'Disponibilit&agrave;',  dataIndex: 'RDDTDS', width: 80, renderer: date_from_AS},
		{header: 'Stato fornitura',  dataIndex: 'ar_tipo_stato', width: 130, filter: {type: 'string'}, filterable: true},				
		{header: 'Cliente',  dataIndex: 'TDDCON', flex: 1, filter: {type: 'string'}, filterable: true},
		{header: 'Ordine',  dataIndex: 'k_ordine_out', width: 100, filter: {type: 'string'}, filterable: true},
		{header: 'St',  dataIndex: 'TDSTAT', width: 40, filter: {type: 'string'}, filterable: true},
		{header: 'Riferimento',  dataIndex: 'TDVSRF', flex: 1, filter: {type: 'string'}, filterable: true},
		{header: '<img src=<?php echo img_path("icone/48x48/comment_edit.png") ?> width=25>',  dataIndex: 'ha_commenti', width: 30,
				renderer: function(value, p, record, a, b, c, grid){
					
			    	if (record.get('ha_commenti'))
			    	  img_path = <?php echo img_path("icone/16x16/comment_edit_yellow.png") ?>;
			    	else
			    	  img_path = <?php echo img_path("icone/16x16/comment_light.png") ?>;			    	

					return '<img class="cell-img" src=' + img_path + '>';			    	  
					//return '<a href="javascript:show_win_annotazioni_ordine(\'' + record.get('TDDOCU') + '\', \'' + record.id + '\', \'' + 'ALERT' + '\')";><img class="cell-img" src=' + img_path + '></a>';								    	
			    	}		 
		},						
		
		{header: 'Note',  dataIndex: 'NOTE', flex: 0.5, filter: {type: 'string'}, filterable: true},
		{header: 'Referente Cliente',  dataIndex: 'TDDOCL', flex: 0.8, filter: {type: 'string'}, filterable: true},
		{header: '<img src=<?php echo img_path("icone/48x48/arrivi.png") ?> width=25>',  dataIndex: 'ha_POSTM', width: 30,
				renderer: function(value, p, record){
			    	if (record.get('ha_POSTM') == 1) return '<a href="javascript:show_win_entry_ordine(\'' + record.get('TDDOCU') + '\')";><img src=<?php echo img_path("icone/48x48/arrivi_gray.png") ?> width=18></a>';			    	
					if (record.get('ha_POSTM') == 2) return '<a href="javascript:show_win_entry_ordine(\'' + record.get('TDDOCU') + '\')";><img src=<?php echo img_path("icone/48x48/arrivi_yellow.png") ?> width=18></a>';			    	
			    	}		 
		},
		{header: '<img src=<?php echo img_path("icone/48x48/unlock.png") ?> width=25>',  dataIndex: 'ha_deroghe', width: 30,
				renderer: function(value, metaData, record){
					if (record.get('ha_deroghe')==1){
	    			 metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('tooltip_deroghe')) + '"';					
					}
				
			    	if (record.get('ha_deroghe')==1) return '<img src=<?php echo img_path("icone/48x48/unlock.png") ?> width=18>';			    	
			    	}		 
		}		
		],
	
		listeners: {
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
				    if (col_name == 'k_ordine_out'){						
						mp = Ext.getCmp('m-panel');
						mp.add(
						  show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_json.php?tipo_cal=iti_data&cod_iti=' + rec.get('CSCITI') + '&data=' + rec.get('TDDTEP'), Ext.id())
			            ).show();
						return false; //previene expand/collapse di default						
				    }
				    

				    if (col_name == 'ha_commenti'){
				    
			          	my_listeners = {
								afterOkSave: function(from_win){	
									//dopo che ha chiuso la maschera di assegnazione indice rottura
									console.log('afterSave.............');
									
									//aggiorno record
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_record',
									        jsonData: {k_ordine: rec.get('TDDOCU')},
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									            var jsonData = Ext.decode(result.responseText);
									            rec.set('ha_commenti', jsonData.ha_commenti);
									            rec.set('NOTE', jsonData.commenti);			            
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });				    			
									
									
									from_win.close();						
									//iView.getStore().load();			            
					        		}
			    				};	
				    
				    	acs_show_win_std('Annotazione ordine', 'acs_form_json_annotazioni_ordine.php?k_ordine=' + rec.get('TDDOCU') + '&liv1=&liv2=', {}, 800, 300, my_listeners, null);
				    						
						return false; //previene expand/collapse di default						
				    }
				    
				    
					rec = iView.getRecord(iRowEl);	
					show_win_art_critici(rec.get('k_ordine'), 'MTO');	
				}
			},

		  itemcontextmenu : function(grid, rec, node, index, event) {			  	

				  event.stopEvent();
			      var record = grid.getStore().getAt(index);		  
			      var voci_menu = [];
				      
				  id_selected = grid.getSelectionModel().getSelection();
				  list_selected_id = [];
				  for (var i=0; i<id_selected.length; i++) 
					list_selected_id.push({k_ordine: id_selected[i].data.k_ordine});				      
	      
						  voci_menu.push({
				      		text: 'Richiesta posticipo consegna',
				    		iconCls: 'iconArtCritici',
				    		handler: function() {

				    			//apro form per richiesta parametri
								var mw = new Ext.Window({
								  width: 800
								, height: 380
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Richiesta posticipo consegna'
								, iconCls: 'iconArtCritici'			
								, layout: 'fit'
								, border: true
								, closable: true								
								});				    			
				    			mw.show();
				    			
								//carico la form dal json ricevuto da php
								Ext.Ajax.request({
								        url        : 'acs_form_json_create_entry.php',
								        jsonData: {tipo_op: 'POSTM', list_selected_id: list_selected_id},
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								            var jsonData = Ext.decode(result.responseText);
								            mw.add(jsonData.items);
								            mw.doLayout();				            
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });				    			
								    	    							    			
				    		}
						  });
				      
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);		  
				
				
				}, //itemcontextmenu			
			
			
	        itemclick: function(view,rec,item,index,eventObj) {
	        	var w = Ext.getCmp('OrdPropertyGrid');
	        	//var wd = Ext.getCmp('OrdPropertyGridDet');
	        	var wdi = Ext.getCmp('OrdPropertyGridImg');  
	        	var wdc = Ext.getCmp('OrdPropertyCronologia');      	

	        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdi.store.load();	        	
	  			wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
				wdc.store.load();
	        	
				Ext.Ajax.request({
				   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
				   success: function(response, opts) {
				      var src = Ext.decode(response.responseText);
				      w.setSource(src.riferimenti);
			          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);				      
				      //wd.setSource(src.dettagli);
				   }
				});
				
				//files (uploaded) - ToDo: dry 
				var wdu = Ext.getCmp('OrdPropertyGridFiles');
				if (!Ext.isEmpty(wdu)){
					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
			        	if (wdu.isVisible())		        	
							wdu.store.load();
					}					
				}				
				
	         }			
			
		}
		 
	}
]
 } 

}	
	
	
<?php 	
  exit();
}




// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA
// ******************************************************************************************
?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Parametri',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			            
            
            items: [					 	
					{
						xtype: 'grid',
						id: 'tab-tipo-stato',
						loadMask: true,
						flex: 1,
						
						layout: {
                			type: 'vbox',
                			align: 'stretch'
     					},
									
						selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},				
						//columnLines: true,			
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: 'acs_form_json_articoli_mancanti_critici.php?fn=get_json_data_stato',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
							},
								
							fields: ['RDRIFE']
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [{header: 'Stato fornitura', dataIndex: 'RDRIFE', flex: 1}]
						 
					}	
		
	
	
            
            		, {            		
            			xtype: 'panel',            		
						margin: "10 0 5 2",            		
            			border: false,
            			plain: true,
            			frame: true,
            			height: 150,
            			
			            layout: {
			                type: 'vbox',
			                align: 'stretch',
			                pack  : 'start'
			     		},            			
            			
            			
     					items: [
     					
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_da'
							   , margin: "0 20 0 10"   
							   , flex: 1                		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Evas.programm.: Da'
							   , labelAlign: 'top'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}, {
							     name: 'f_data_a'
							   , margin: "0 20 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'A'
							   , labelAlign: 'top'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
								 }
							}
						
						]
					 }      					
     					
     					
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							name: 'f_area',
							margin: "0 20 0 10",
							flex: 1,
							xtype: 'combo',
							fieldLabel: 'Area spedizione', labelAlign: 'top',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}, {
							name: 'f_referente_cliente',
							margin: "0 20 0 10",
							flex: 1,							
							xtype: 'combo',
							fieldLabel: 'Referente cliente', labelAlign: 'top',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_referente_cliente(), '') ?> 	
								    ] 
								}						 
							}
						]
					 } , { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						
						{  name: 'f_tipo',
							margin: "0 20 0 10",
							flex: 1,							
							xtype: 'combo',
							fieldLabel: 'Tipo fornitura',
						    labelAlign: 'top',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id : 'MTO', text : 'MTO'},
								     {id : 'B_MRP', text : 'MTS'},	
								    ] 
								}						 
							},{  name: 'f_blocco',
							margin: "0 20 0 10",
							flex: 1,							
							xtype: 'combo',
							fieldLabel: 'Blocco ordini',
						    labelAlign: 'top',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								   {id : 'A', text : 'Con blocco evasione'},
								   {id : 'C', text : 'Con blocco commerciale'},
								   {id : 'N', text : 'Senza blocco'},	
								    ] 
								}						 
							}
						
						
						]    }					
     					
     					
     					
     					
     					
     					
						  			
     					
     					]
     				}            
            	
	
	
	
	
						 
						 
				],
			buttons: [{
	            text: 'Lista per ordine',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();
  				list_selected_id = [];
  				for (var i=0; i<id_selected.length; i++) 
	   				list_selected_id.push(id_selected[i].data.RDRIFE);
				
	            
				//carico la form dal json ricevuto da php e la inserisco nel m-panel
				Ext.Ajax.request({
				        url        : 'acs_form_json_articoli_mancanti_critici.php?fn=get_json_grid',
				        method     : 'POST',
				        jsonData: {selected_id: list_selected_id, form_ep: form_p.getValues()},
				        waitMsg    : 'Data loading',
				        success : function(result, request){			        	
				        	
							mp = Ext.getCmp('m-panel');					        					        
							mp.remove('panel-alert');					        
				            var jsonData = Ext.decode(result.responseText);
				            
				            mp.add(jsonData.items);
				            mp.doLayout();
				            mp.items.last().show();
 							this.print_w.close();
						             
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				        				        
				    });
				    
	            
            	                	                
	            }
	        }, {
	            text: 'Report fornitore',
	            iconCls: 'icon-print-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();
  				list_selected_id = [];
  				for (var i=0; i<id_selected.length; i++) 
	   				list_selected_id.push(id_selected[i].data.RDRIFE);
	            
	            
	                this.up('form').submit({
                        url: 'acs_report_art_mancanti.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',                        
                        params: {
							list_selected_stato: Ext.encode(list_selected_id)
					    },
                  });            	                	     
            	                	                
	            }
	        } ,{
	            text: 'Report cliente',
	            iconCls: 'icon-print-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tab-tipo-stato');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();
  				list_selected_id = [];
  				for (var i=0; i<id_selected.length; i++) 
	   				list_selected_id.push(id_selected[i].data.RDRIFE);
	            
	            
	                this.up('form').submit({
                        url: 'acs_report_art_mancanti.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',                        
                        params: {
							list_selected_stato: Ext.encode(list_selected_id),
							cliente: 'Y'
					    },
                  });            	                	     
            	                	                
	            }
	        } 
	        ],             
				
        }
]}			            