<?php

require_once("../../config.inc.php");

$main_module = new Spedizioni();
$s = new Spedizioni();

set_time_limit(240);



function sql_where_by_open_form($form_values){
	$ret = "";
	
	if (isset($form_values->f_data_da)  && strlen($form_values->f_data_da) > 0 )
		$ret .= " AND TDDTSP >= {$form_values->f_data_da} ";
	
	if (isset($form_values->f_area_spedizione) && count($form_values->f_area_spedizione) > 0)
		$ret .= " AND TA_ITIN.TAASPE IN (" . sql_t_IN($form_values->f_area_spedizione) . ") ";
	
	if (isset($form_values->f_divisione) && count($form_values->f_divisione) > 0)
		$ret .= " AND TD.TDCDIV IN (" . sql_t_IN($form_values->f_divisione) . ") ";	

	return $ret;
}


function cmp_by_data_presunta($a, $b)
{
	//al momento non ordino per area
	$a_aspe = '';
	$b_aspe = '';
//	if (trim($a['taaspe']) == 'ITA') $a_aspe = '1'; else $a_aspe = '2'; 
//	if (trim($b['taaspe']) == 'ITA') $b_aspe = '1'; else $b_aspe = '2';

	//se non hanno data spunta... li mostro in alto (vado per ora spunta decrescente)
	if ($a["max_spunta_sped"] == 0) $a["max_spunta_sped"] = 99999999999999;
	if ($b["max_spunta_sped"] == 0) $b["max_spunta_sped"] = 99999999999999;	
//	return strcmp(
//			implode("|", array($a_aspe, sprintf("%014s",$a["max_spunta_sped"]), sprintf("%08s",$a["ora_spedizione"]), $a['carico_lotto'])),
//			implode("|", array($b_aspe, sprintf("%014s",$b["max_spunta_sped"]), sprintf("%08s",$b["ora_spedizione"]),  $b['carico_lotto']))
//	);
 
	
	
	if (sprintf("%014s", $a["max_spunta_sped"]) != sprintf("%014s", $b["max_spunta_sped"]))
		return strcmp(sprintf("%014s",$b["max_spunta_sped"]), sprintf("%014s",$a["max_spunta_sped"])); 	//ora sputa decrescente

	if (sprintf("%08s", $a["ora_spedizione"]) != sprintf("%08s", $b["ora_spedizione"]))
		return strcmp(sprintf("%08s",$a["ora_spedizione"]), sprintf("%08s",$b["ora_spedizione"]));    	//ora spedizione (crescente)

	return strcmp($a['carico_lotto'], $b['carico_lotto']); 										 		//carico (crescente)	
		
	
}

function add_parziali(&$ar, $r){
        
    
	$ar['colli_T'] += $r['COLLI_T'];
	$ar['SCAR']    += $r['SCAR'];	
	$ar['colli_S'] += $r['COLLI_S'];
	$ar['colli_D'] += $r['COLLI_D'];	
	$ar['volume']  += $r['VOLUME_S'];	
	$ar['importo']  += $r['IMPORTO_S'];	
	/*$ar['costo_tr']   += $r['CSCSTR'];
    $ar['costo_td']   += $r['CSCSTD'];*/
    $ar['da_spedire'] = $ar['colli_T'] - $ar['colli_S'];  
	$ar['non_disponibili'] = $ar['colli_T'] - $ar['colli_D'];	
	$ar['non_scaricati'] = $ar['colli_T'] - $ar['SCAR'];	

	if ($r['liv'] != 'liv_0'){
		$ar['min_spunta_disp']  = max($r['SPUNTA_DISP_M'], $ar['min_spunta_disp']);
		$ar['min_spunta_scar']  = max($r['SPUNTA_SCAR'], $ar['min_spunta_scar']);
		$ar['max_spunta_sped']  = max($r['SPUNTA_SPED_MAX'], $ar['max_spunta_sped']);

		if ($r['SPUNTA_SPED_MIN'] != 0)
			if ($ar['min_spunta_sped'] == 0)
				$ar['min_spunta_sped'] = $r['SPUNTA_SPED_MIN'];
			else
				$ar['min_spunta_sped']  = min($r['SPUNTA_SPED_MIN'], $ar['min_spunta_sped']);
			
		
		$ar['data_presunta']  	= max($r['DATA_PRESUNTA'], $ar['data_presunta']);
	}		
	
	$ar['n_ord_evasi']	+= $r['ORD_EVASI'];	
	$ar['n_ord']		+= $r['T_ROW'];	
	

	if ($ar['n_ord_evasi'] == $ar['n_ord']){ //tutti evasi
  		$ar['iconCls'] 	= 'icon-lock-16';		
	} else if ($ar['n_ord_evasi'] > 0){
		$ar['iconCls'] 	= 'icon-lock_grey-16';		
	} else if ($ar['CSFG04'] == 'F'){
	    $ar['iconCls'] 	= 'iconPartenze';
	} else {
		$ar['iconCls'] 	= null;		
	}
	
	//progress
	if ($ar['colli_T'] > 0)
		$ar['colli_S_progress'] = $ar['colli_S'] / $ar['colli_T'] * 100;
	else $ar['colli_S_progress'] = 100;	
	
	if ($ar['colli_T'] > 0)
		$ar['colli_D_progress'] = $ar['colli_D'] / $ar['colli_T'] * 100;
	else $ar['colli_D_progress'] = 100;	
	
	if ($ar['colli_T'] > 0)
	    $ar['colli_SCAR_progress'] = $ar['SCAR'] / $ar['colli_T'] * 100;
    else $ar['colli_SCAR_progress'] = 100;
	
}







// ******************************************************************************************
// FORM PARAMETRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_form_params'){
	?>
{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "OPEN_GRID");  ?>{
			            text: 'Visualizza',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            
			            	var form = this.up('form').getForm();
			            	m_win = this.up('window');
			            	form_values = form.getValues();
			            	
			            	//in visualizzazione non tengo conto della data finale (usata solo nel report)
			            	form_values['f_data_a'] = null;
			            	 
			            	if(form.isValid()){
			            	
					        	mp = Ext.getCmp('m-panel');
					        	if(form_values.tipo_data == 'D'){
					        		panel_id = 'panel-avanzamento_produzione';	
					        		title = 'Grid';	
					        		subtitle = 'Avanzamento attivit&agrave; di spunta produzione/disponibilit&agrave; colli e carico/spedizione colli per giorno di spedizione';		        	
					            }else{
					            	panel_id = 'panel-avanzamento_produzione_scar';	
					            	title = 'Grid delivery';
					            	subtitle = 'Avanzamento attivit&agrave; di spunta carico/spedizione colli e Scarico a destinazione per giorno di spedizione';
					            }
				
					        		//carico la form dal json ricevuto da php
					        		Ext.Ajax.request({
					        		        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_panel_grid',
					        		        jsonData   : {
					        		         form_values : form_values,
					        		         panel_id : panel_id,
					        		         title : title,
					        		         subtitle : subtitle
					        		        },
					        		        method     : 'POST',
					        		        waitMsg    : 'Data loading',
					        		        success : function(result, request){
					        		            var jsonData = Ext.decode(result.responseText);
					        		            mp.add(jsonData.items);
					        		            mp.doLayout();
					        		            mp.show();
					        		            av_prenotazione_carichi = Ext.getCmp(panel_id).show();					        		            
					        		            m_win.close();        		            
					        		        },
					        		        failure    : function(result, request){
					        		            Ext.Msg.alert('Message', 'No data to be loaded');
					        		        }
					        		    });
					
					        	 
							
			            	}
			            }
			         }],   		            
		            
		            items: [
		            
						  {
							name: 'f_area_spedizione',
							xtype: 'combo',
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('ASPE'), ""); ?>	
								    ] 
							}						 
						 }, {
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",
						    multiSelect: true,						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}	
							
						 , {
					        xtype: 'fieldcontainer',
							defaultType: 'textfield',
				 	        layout: 'hbox',
				 	        items: [		            		            		            
				            	{
								   xtype: 'datefield', hidden: false
								   , fieldLabel: 'Da data'
								   , startDay: 1 //lun.
								   , name: 'f_data_da'
								   , format: 'd/m/Y'
								   , submitFormat: 'Ymd'
								   , allowBlank: true
								   , anchor: '-15'
								   , margin: "10 10 0 10"
								   , value: '<?php echo  print_date(date("Ymd", strtotime( '-2 days' ) ), "%d/%m/%Y"); ?>'
								}
							]
						}, 
						
						{
			                    xtype: 'radiogroup',
			                    margin: "10 10 0 5",
			                    width : 350,
			                    fieldLabel: 'Evidenza avanzamento',
			                    labelWidth : 130,
			                    allowBlank: false,
			                    items: [{
			                            xtype: 'radio'
			                          , name: 'tipo_data'
			                          , inputValue: 'D' 
			                          , boxLabel: 'Disponibilit&agrave;'
			                          , checked: true
			                          },{
			                            xtype: 'radio'
			                          , name: 'tipo_data'
			                          , inputValue: 'S' 
			                          , boxLabel: 'Scarico'
			                          		                         
			                        }
			                    ]
			                },							
							
						  ]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
<?php exit; }











// ******************************************************************************************
// DATI PER GRID AVANZAMENTO LOTTO (COLLI)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_avanzamento_lotto'){

	//tree con data/spedizione/lotto
	$sql_fs = array('RDART', 'RDDES1', 'RDRIFE');
	$sql_ob = array('RDART', 'RDDES1', 'RDRIFE');
	$sql_ff = array(
			'SUM(RDQTA) AS COLLI_T',
			'SUM(RDQTA2) AS COLLI_D', 'SUM(RDQTA - RDQTA2) AS COLLI_ND',
			'SUM(RDQTA3) AS COLLI_S', 'SUM(RDQTA - RDQTA3) AS COLLI_NS',
	        'SUM(SCAR) AS SCAR'
	);

	
	if (strlen($_REQUEST['k_ordine']) > 0){
		$sql_where = " AND TDDOCU = ? ";
		$sql_params = array($_REQUEST['k_ordine']);
	} else {
		
		//da grid: deve diventare parametrico?
		$sql_where .= " AND TDFN01 = 1 "; /* con carico assegnato */
		$sql_where .= " AND TDSWSP = 'Y' ";
		
		if ($_REQUEST['liv'] == 'liv_2'){
			//per spedizione
			$sql_where = " AND TDNBOC = ? ";
			$sql_params = array($_REQUEST['sped_id']);
		} else {
			//per spedizione/lotto
			$sql_where = " AND TDNBOC = ? AND TDNRLO = ? ";
			$sql_params = array($_REQUEST['sped_id'], $_REQUEST['lotto']);
		}
			
	}
	
	$sql = "SELECT " . implode(",", array_merge($sql_fs, $sql_ff)) . "
			FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
				  INNER JOIN {$cfg_mod_Spedizioni['file_righe']} RD
					ON TDDT=RDDT AND TDOTID=RDOTID AND TDOINU=RDOINU AND TDOADO=RDOADO AND TDONDO=RDONDO	
				  INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP 
					ON TDDT = SP.CSDT AND TDNBOC = SP.CSPROG AND SP.CSCALE = '*SPR'
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
				LEFT OUTER JOIN(
	               SELECT COUNT(*) AS SCAR, PSTIDO, PSINUM, PSAADO, PSNRDO, PLOPE2
	               FROM {$cfg_mod_Spedizioni['file_colli']} PL
    				 INNER JOIN {$cfg_mod_Spedizioni['file_colli_dc']} PS2
    				  ON PLDT = PS2.PSDT
    				  AND PLTIDO = PS2.PSTIDO
    				  AND PLINUM = PS2.PSINUM
    				  AND PLAADO = PS2.PSAADO
    				  AND PLNRDO = PS2.PSNRDO
    				  AND PLNREC = PS2.PSNREC
                   WHERE PS2.PSDT = '{$id_ditta_default}'
	               GROUP BY PS2.PSTIDO, PS2.PSINUM, PS2.PSAADO, PS2.PSNRDO, PL.PLOPE2) PS
	            ON TD.TDOTID=PS.PSTIDO AND TD.TDOINU=PS.PSINUM AND TD.TDOADO=PS.PSAADO AND TD.TDONDO=PS.PSNRDO AND RD.RDART = PS.PLOPE2 
				
                WHERE " . $s->get_where_std() . " 				
				AND RD.RDTPNO='COLLI' AND RD.RDRIGA=0
				" . $sql_where . "					
 				GROUP BY " . implode(",", $sql_fs) . "
 				ORDER BY " . implode(",", $sql_ob) . "
 				";

	
 				$stmt = db2_prepare($conn, $sql);
 				echo db2_stmt_errormsg();
 				$result = db2_execute($stmt, $sql_params);
 				echo db2_stmt_errormsg($stmt);
 				 
 				//creo l'albero
			$ret = array();
			while ($row = db2_fetch_assoc($stmt)){
			    $row['non_scaricati'] = $row['COLLI_T'] - $row['SCAR'];
				$ret[] = $row;	
				
			}
			
	
	echo acs_je($ret);
	exit;
}








// ******************************************************************************************
// GRID PER AVANZAMENTO COLLI LOTTO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_avanzamento_lotto_grid'){	
	$m_params = acs_m_params_json_decode();	
	
?>

{"success":true, "items":
	{
		xtype: 'grid',
		id: 'tab-alert',
		loadMask: true,
	
		features: [
			{ftype: 'summary'}, {
	            id: 'group',
	            ftype: 'groupingsummary',
	            groupHeaderTpl: '{name}',
	            hideGroupedHeader: true
	        }
		],	
	
	
		store: {
			xtype: 'store',
			autoLoad:true,
	
			groupField: 'RDRIFE',	
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_avanzamento_lotto',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
				
				extraParams: <?php echo acs_je($m_params); ?>				
			},
				
			fields: ['RDRIFE', 'RDART', 'RDDES1', {name: 'non_scaricati', type: 'float'}, {name: 'COLLI_T', type: 'float'}, {name: 'COLLI_D', type: 'float'}, {name: 'COLLI_S', type: 'float'}, {name: 'COLLI_ND', type: 'float'}, {name: 'COLLI_NS', type: 'float'}]
						
						
		}, //store
		multiSelect: false,
	
		columns: [
			{header: 'Centro',  dataIndex: 'RDRIFE', width: 80, hidden: true},		
			{header: 'Reparto', dataIndex: 'RDDES1', flex: 1},			
			{header: 'Colli totale', dataIndex: 'COLLI_T', width: 90, align: 'right', renderer: floatRenderer0,
				 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					            return floatRenderer0(value); 
					        }},
			{header: 'Non disponibili', dataIndex: 'COLLI_ND', width: 90, align: 'right', renderer: floatRenderer0,
				 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					            return floatRenderer0(value); 
					        }},						
			{header: 'Da spedire', dataIndex: 'COLLI_NS', width: 90, align: 'right', renderer: floatRenderer0,
				 summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
					            return floatRenderer0(value); 
					        }},		
	        {header: 'Non scaricati', dataIndex: 'non_scaricati', width: 90, align: 'right', renderer: floatRenderer0,
 				summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
	            return floatRenderer0(value); 
	        }}
		],
		
		
		listeners: {
					
				celldblclick: {								

					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	
						if (1==1){
							iEvent.preventDefault();							
							ar_params = {};
							ar_params['sped_lotto'] = <?php echo acs_je($m_params); ?>;
							ar_params['filtro'] = rec.data;
							ar_params['filtro']['col_name'] = col_name;

							acs_show_win_std('Elenco corrente colli', 'acs_json_elenco_colli.php?fn=get_json_grid', Ext.encode(ar_params), 900, 500, null, 'icon-box_open-16')
							return false;
						}
					}
				}				
		}
		
				 
	}

}	


<?php
	exit;
}





// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	
	$m_params = acs_m_params_json_decode();	
		
	//tree con data/spedizione/lotto
	$f_data = "TDDTSP"; //lavoro con la data di spedizione
	$sql_fs = array($f_data, 'CSSTSP', 'CSTITR', 'TDNBOF', 'TDTPLO', 'TDAALO', 'TDNRLO', 'CSCITI', 'CSHMPG', 'CSCVET', 'CSCAUT', 'CSCCON', 'CSTARG', 'CSPORT', 'CSDTIC', 'CSHMIC', 'TA_ITIN.TAASPE', 'TDFN11', 'CSDESC', 'CSFG02', 'CSFG04', 'CSCSTD',
					'CSCSTR');
	$sql_ob = array($f_data,
//					"CASE WHEN MAX(TDDTSSI*1000000 + TDHMSSI) > ({$f_data}*1000000 + CSHMPG) THEN MAX(TDDTSSI*1000000 + TDHMSSI) ELSE CASE WHEN (CSHMPG) >0 THEN ({$f_data}*1000000 + CSHMPG) ELSE 0 END END DESC",  //sped
					"CASE WHEN MAX(TDDTSSI*1000000 + TDHMSSI) >0 THEN MAX(TDDTSSI*1000000 + TDHMSSI) ELSE 0 END DESC",  //sped
					"CSHMPG",  //ora sped
					"MAX(TDNRCA)"
/*
					"CASE WHEN CSTITR = 'RD' THEN 1 WHEN CSTITR = 'CP' THEN 2 ELSE 0 END",
					'CASE WHEN CSHMPG = 0 THEN 9 ELSE 0 END', 'CSHMPG', //orario sped (0 in fondo) 
					'CASE WHEN MAX(TDDTSSI*1000000 + TDHMSSI) = 0 THEN 9 ELSE 0 END', 'MAX(TDDTSSI*1000000 + TDHMSSI)',  //sped 			
					'CASE WHEN MIN(TDDTSPI*1000000 + TDHMSPI) = 0 THEN 9 ELSE 0 END', 'MIN(TDDTSPI*1000000 + TDHMSPI)'
*/					
	); //disp	
	$sql_ff = array("CASE WHEN CSTITR = 'RD' THEN 1 WHEN CSTITR = 'CP' THEN 2 ELSE 0 END AS TIPOLOGIA",
					'SUM(TDTOCO) AS COLLI_T', 
					'SUM(TDCOSP) AS COLLI_S',
					'SUM(TDVOLU) AS VOLUME_S',				
	 				'SUM(TDCOPR) AS COLLI_D',
					'SUM(TDTIMP) AS IMPORTO_S',
					'SUM(TDFN11) AS ORD_EVASI', 'count(*) as T_ROW',
					/*'SUM(CSCSTD) AS COSTO_TD',
					'SUM(CSCSTR) AS COSTO_TR',*/
					'MAX(TDDTSPI*1000000 + TDHMSPI) AS SPUNTA_DISP_M',
					'MIN(TDDTSSI*1000000 + TDHMSSI) AS SPUNTA_SPED_MIN',
					'MAX(TDDTSSF*1000000 + TDHMSSF) AS SPUNTA_SPED_MAX',
					"CASE WHEN MAX(TDDTSSI*1000000 + TDHMSSI) > ({$f_data}*1000000 + CSHMPG) THEN MAX(TDDTSSI*1000000 + TDHMSSI) ELSE CASE WHEN (CSHMPG) >0 THEN ({$f_data}*1000000 + CSHMPG) ELSE 0 END END AS DATA_PRESUNTA"			
	);

	$sql_where = "";
	
	$js_parameters = $auth->get_module_parameters($s->get_cod_mod());
	if (isset($js_parameters->area_sped)){
		$sql_where .= " AND TA_ITIN.TAASPE IN (" . sql_t_IN($js_parameters->area_sped) . ")";
	}
	
	
	$sql_where .= sql_where_by_open_form($m_params->form_ep);
	
	if($m_params->form_ep->tipo_data == 'S'){
	    $select_ps = ", SCAR, SPUNTA_SCAR";
	    $group_ps = ", SCAR, SPUNTA_SCAR";
	    $join_ps = "LEFT OUTER JOIN(
                        SELECT COUNT(*) AS SCAR, MAX(PSDTSC*1000000 + PSTMSC) AS SPUNTA_SCAR, PSTIDO, PSINUM, PSAADO, PSNRDO 
				        FROM {$cfg_mod_Spedizioni['file_colli_dc']}
                        WHERE PSDT = '{$id_ditta_default}'
                        GROUP BY PSTIDO, PSINUM, PSAADO, PSNRDO) PS
                    ON TD.TDOTID=PS.PSTIDO AND TD.TDOINU=PS.PSINUM AND TD.TDOADO=PS.PSAADO AND TD.TDONDO=PS.PSNRDO";
	    
	}
	
 	$sql = "SELECT " . implode(",", array_merge($sql_fs, $sql_ff)) . "
 	            {$select_ps}
 				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $s->add_riservatezza() . "
 	
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				  ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOF = SP.CSPROG
				  
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI				  
				  ". $join_ps ."
			 	WHERE " . $s->get_where_std() . " AND TDSWSP = 'Y'
 				AND TDFN01 = 1 /* con carico assegnato */
			 	/* AND SP.CSSTSP <> 'DP' Le programmare ora le raggruppo in una data singola */
				" . $sql_where . "				
 				GROUP BY " . implode(",", $sql_fs) . $group_ps ."
 				ORDER BY " . implode(",", $sql_ob) . "
			";
 	
 	        	            
 	$stmt = db2_prepare($conn, $sql); 	
 	echo db2_stmt_errormsg(); 	
  	$result = db2_execute($stmt, array($data, $tddt, $tdccon, $tdcdes, $aspe));
  	
  	$sql_bk = "SELECT BSUSGE, BSTIME
  	           FROM {$cfg_mod_Booking['file_richieste']} BS
  	           WHERE BSDT='{$id_ditta_default}' AND BSNBOC = ?
               AND BSSTAT NOT IN('00', '03', '04') /* escludo stati NON validi */
  	           ORDER BY BSTIME DESC";
  	$stmt_bk = db2_prepare($conn, $sql_bk);
  	echo db2_stmt_errormsg();
  	
  	//creo l'albero
  	$ar = array();
  	
  	//come prime righe metto le 'DA PROGRAMMARE', per ogni area di spedizone
  	$ar_ASPE = $main_module->find_TA_std('ASPE');
  	foreach ($ar_ASPE as $aspe)  	
  		$ar["DP_{$aspe['id']}"] = array("cod" => "DP_{$aspe['id']}", "task" => "Da program. [" . trim($aspe['id']) . "]", 
  					"id" => implode("|", array("DP_{$aspe['id']}")),  "children"=>array());

  	
  	while ($row = db2_fetch_assoc($stmt)){

  		if (trim($row['CSSTSP']) == 'DP' && $row['TDFN11'] == 0){ //da programmare e non ancora evaso
  		 $row[$f_data] = "DP_" . trim($row['TAASPE']);
  		 $row['tipoSped'] = 'DP';
  		}
  		 
  		
 		$liv0 = implode("_", array($row[$f_data]));   //data  			
  		$liv1 = implode("_", array($row['TDNBOF']));  //spedizione
  		$liv2 = implode("_", array($row['TDTPLO'], $row['TDAALO'], $row['TDNRLO']));  //lotto  		
  		$tmp_ar_id = array();
  		$data = strtotime($row[$f_data]);  		

  		//*******************************************  		
  		//liv0 (data)
  		//*******************************************  		
  		$d_ar = &$ar;
  		$c_liv = $liv0;
  		$tmp_ar_id[] = $c_liv;  		
  		if (!isset($d_ar[$c_liv])){
  			$d_ar[$c_liv] = array("cod" => $c_liv, "id" => implode("|", $tmp_ar_id),  "children"=>array());
  			
  			$oldLocale = setlocale(LC_TIME, 'it_IT');  			
 			$d_ar[$c_liv]['task'] 		= ucfirst(strftime("%a", $data)) . " " . strftime("%d/%m", $data);
  			$d_ar[$c_liv]['tipoSped'] = $row['tipoSped'];
  			$d_ar[$c_liv]['liv']  = 'liv_1';
  			$d_ar[$c_liv]['costo_tr']   = 0;
  			$d_ar[$c_liv]['costo_td']   = 0;
  			$d_ar[$c_liv]['exapnded']	= false;  	
			setlocale(LC_TIME, $oldLocale);
  		}
  		$t_liv_data = &$d_ar[$c_liv];
  		add_parziali($d_ar[$c_liv], $row);

  		//*******************************************
  		//liv1 (spedizione)
  		//*******************************************  		
  		$d_ar = &$d_ar[$c_liv]['children'];
  		$c_liv = $liv1;
  		$tmp_ar_id[] = $c_liv;  		
  		if (!isset($d_ar[$c_liv])){
  			$d_ar[$c_liv] = array("cod" => $c_liv, "id" => implode("|", $tmp_ar_id),  "children"=>array());
  			
  			$data = strtotime($row['TDNBOF']);  			
  			$d_ar[$c_liv]['task'] 		= "#{$row['TDNBOF']} [" . trim($row['TAASPE']) . "]";
  				
  				//se ho i inserito i dati aggiuntivi (booking, etc...)
	  			$sqlInt = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
	  			$stmtInt = db2_prepare($conn, $sqlInt);
	  			$result = db2_execute($stmtInt, array($id_ditta_default, 'SPINT', sprintf("%08s", $row['TDNBOF'])));
	  			$rowInt = db2_fetch_assoc($stmtInt);
	  			
	  			$spedizione = sprintf("%08s", $row['TDNBOF']);
	  			
	  			$result = db2_execute($stmt_bk, array($spedizione));
	  			$row_bk = db2_fetch_assoc($stmt_bk);
	  			$utente_generazione = acs_u8e(trim($row_bk['BSUSGE']));
	  			
	  			if ($rowInt || strlen(trim($row['CSDESC']))>0) {
	  			    $d_ar[$c_liv]['task'] .= ' <img class="cell-img" src=' . img_path("icone/16x16/blog_compose.png") . '>';
	  			    
	  			  $d_ar[$c_liv]['qtip_spedizione'] = '';
	  			    
	  			    if(strlen(trim($row['CSDESC']))>0)
	  			        $d_ar[$c_liv]['qtip_spedizione'] .= "Descrizione: {$row['CSDESC']}<br/>";

  			        if ($rowInt) {
  			            $d_ar[$c_liv]['qtip_spedizione'] .= "E-mail: {$rowInt['TAMAIL']}<br/>Dogana: {$rowInt['TARIF1']}<br/>Nr. Booking: {$rowInt['TARIF2']}";
  			        }
	  			}	  			    
	  			
	  			if($utente_generazione == 'BOOKING') 
	  			    $d_ar[$c_liv]['task'] .= ' <a href="javascript:show_scheda_booking(\'' . $row['TDNBOF'] . '\', \'' . $row_bk['BSTIME'] . '\')";> <img class="cell-img" src=' . img_path("icone/16x16/globe.png") . '></a>';

  			
  			$d_ar[$c_liv]['taaspe'] 	= trim($row['TAASPE']);
  			
  			$d_ar[$c_liv]['CSFG04'] 	= trim($row['CSFG04']);
 			$d_ar[$c_liv]['iconCls'] 	= 'iconSpedizione';  
 			$d_ar[$c_liv]['costo_tr']   = $row['CSCSTR'];
 			$d_ar[$c_liv]['costo_td']   = $row['CSCSTD'];
 			$t_liv_data['costo_tr']    += $row['CSCSTR'];
 			$t_liv_data['costo_td']    += $row['CSCSTD'];
  			$d_ar[$c_liv]['sped_id'] 	= $row['TDNBOF'];  			
  			$d_ar[$c_liv]['itinerario'] = $s->decod_std('ITIN', $row['CSCITI']);
  			$d_ar[$c_liv]['tipologia']  = $row['TIPOLOGIA'];  			
  			$d_ar[$c_liv]['ora_spedizione'] = print_ora($row['CSHMPG']);
  			$d_ar[$c_liv]['vmc'] = $s->decod_vmc_by_sped_row($row);
  			$d_ar[$c_liv]['c_trasportatore'] = $row['CSCVET'];
  			$d_ar[$c_liv]['trasportatore'] = acs_u8e(trim($s->decod_trasportatore_by_sped_row($row)));

  			if (strlen(trim($row['CSDESC'])) > 0)
  				$d_ar[$c_liv]['trasportatore'] .= " [" . acs_u8e(trim($row['CSDESC'])) . "]";
  			
  			$d_ar[$c_liv]['targa'] = trim($row['CSTARG']);
  			$d_ar[$c_liv]['porta'] = trim($row['CSPORT']);  			
  			$d_ar[$c_liv]['carico_lotto'] = $s->get_el_carichi_by_TDNBOF($row['TDNBOF'], 'N', 'N', 'N', null, 'Y', 'N', 'Y');
  			$d_ar[$c_liv]['carico_lotto_est'] = $s->get_el_carichi_by_TDNBOF($row['TDNBOF'], 'Y');
  			$d_ar[$c_liv]['liv']  = 'liv_2';  			  			
  			$d_ar[$c_liv]['data_inizio']  		= $row['CSDTIC'];  			
  			$d_ar[$c_liv]['tipologia']  		= $row['TIPOLOGIA'];  			
  			$d_ar[$c_liv]['ora_inizio']  		= sprintf("%06s", $row['CSHMIC']);
  			$d_ar[$c_liv]['exapnded']	= false;
  			$d_ar[$c_liv]['data_riferimento'] = $row[$f_data];  			
  			$d_ar[$c_liv]['secondaria'] = $row['CSFG02'];
  		}   		
  		add_parziali($d_ar[$c_liv], $row);  		 		
  		
  		
  		//*******************************************
  		//liv2 (lotto)
  		//*******************************************
  		$d_ar = &$d_ar[$c_liv]['children'];
  		$c_liv = $liv2;
  		$tmp_ar_id[] = $c_liv;
  		if (!isset($d_ar[$c_liv])){
  			$d_ar[$c_liv] = array("cod" => $c_liv, "id" => implode("|", $tmp_ar_id),  "children"=>array());
  			$d_ar[$c_liv]['task'] = 'Lotto';
  			$d_ar[$c_liv]['carico_lotto'] = $row['TDNRLO'];
  			$d_ar[$c_liv]['liv']  = 'liv_3';
  			$d_ar[$c_liv]['sped_id'] 	= $row['TDNBOF'];
  			//$d_ar[$c_liv]['max_spunta_sped']  = $row['SPUNTA_SPED_M'];  			  			
  			//$d_ar[$c_liv]['min_spunta_sped']  = $row['SPUNTA_SPED_M'];
  			$d_ar[$c_liv]['leaf']	= true;
  			$d_ar[$c_liv]['data_riferimento'] = $row[$f_data];  			
  		}
  		add_parziali($d_ar[$c_liv], $row);  		
  		
  	}  	

 	
  	//ordinamento personalizzato
  	foreach($ar as $kar => $r){
			usort($ar[$kar]['children'], "cmp_by_data_presunta");
	}
	
	
  	
  	//esporto i dati
  	foreach($ar as $kar => $r){
  		$ret[] = array_values_recursive($ar[$kar]);  	
  	}  	

	echo acs_je($ret);	
	exit;
}


$m_params = acs_m_params_json_decode();
?>

{"success":true, "items":
 {
  		xtype: 'treepanel',
  		id: '<?php echo $m_params->panel_id; ?>',
  		
        stateful: true,
  		
		selType: 'cellmodel',
		cls: 'tree-calendario',		
        title: '<?php echo $m_params->title; ?>',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        da_data: 0,
		<?php echo make_tab_closable(); ?>,        
        
        tbar: new Ext.Toolbar({
            items:['<b><?php echo $m_params->subtitle; ?></b>', '->'
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>            
            ]
        }),  		
        
        
        
		store: Ext.create('Ext.data.TreeStore', {
	        proxy: {
	            type: 'ajax',
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
				actionMethods: {read: 'POST'},	            
	            extraParams: {
	              form_ep: {
	            	f_data_da: <?php echo acs_je($m_params->form_values->f_data_da)?>,
	            	f_area_spedizione: <?php echo acs_je($m_params->form_values->f_area_spedizione)?>,
	            	f_divisione: <?php echo acs_je($m_params->form_values->f_divisione)?>,
	            	tipo_data: <?php echo acs_je($m_params->form_values->tipo_data)?>
	              }
	            },
                timeout: 240000,
                doRequest: personalizza_extraParams_to_jsonData
	        },
	        
			fields: ['cod','task', 'liv', 'data', 'itinerario', 'colli_T', 'colli_S', 'colli_S_progress', 'colli_D', 'colli_D_progress', 'colli_SCAR_progress',
					'da_spedire', 'non_disponibili', 'non_scaricati', 'data_inizio', 'ora_inizio', 'qtip_spedizione', 'taaspe',
					'carico_lotto', 'carico_lotto_est', 'ora_spedizione', 'volume', 'importo', 'costo_tr', 'costo_td', 'c_trasportatore', 'trasportatore', 'vmc', 'targa', 'porta', 'sped_id',
					'min_spunta_disp', 'min_spunta_scar', 'max_spunta_sped', 'min_spunta_sped', 'data_presunta', 'tipologia', 'data_riferimento', 'secondaria'],	        
	        
	        reader: new Ext.data.JsonReader(),

	        //ordinamento	        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}, 
				load: function () {
				    Ext.getBody().unmask();				
				}	        	
	        }		        
	        
	    }),
        
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        
		listeners: {
		
	 			afterrender: function (comp) { 			
	 				
	 				}, 		
		
				load: function(sender, node, records) {
				},

					
				celldblclick: {								

					fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	
						if (( rec.get('liv')=='liv_2' || rec.get('liv')=='liv_3') && 
								(col_name=='colli_T' || col_name=='da_spedire' || col_name=='non_disponibili' || col_name=='non_scaricati')){
							iEvent.preventDefault();							
							ar_params = {liv: rec.get('liv'), sped_id: rec.get('sped_id'), lotto: rec.get('carico_lotto')};							
							acs_show_win_std('Riepilogo avanzamento spunta colli', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_avanzamento_lotto_grid', ar_params, 500, 500, null, 'icon-box_open-16')
							return false;
						}
						//sara
						if (( rec.get('liv')=='liv_2' || rec.get('liv')=='liv_3') && 
								(col_name=='colli_S_progress' || col_name=='colli_D_progress')){
							iEvent.preventDefault();							
							ar_params = {};
							ar_params['sped_lotto'] = {liv: rec.get('liv'), sped_id: rec.get('sped_id'), lotto: rec.get('carico_lotto')};							
							ar_params['filtro'] = rec.data;
							ar_params['filtro']['col_name'] = col_name;
							
							if(col_name == 'colli_S_progress')
								var title = 'Elenco corrente colli DA SPEDIRE';
							if(col_name == 'colli_D_progress')
								var title = 'Elenco corrente colli NON DISPONIBILI';
								 
							acs_show_win_std( title, 'acs_json_elenco_colli.php?fn=get_json_grid', Ext.encode(ar_params), 900, 500, null, 'icon-box_open-16')
							
							return false;
						}
						
						
						if (rec.get('liv') == 'liv_2' && col_name == 'task'){
							
							iEvent.preventDefault();						
							mp = Ext.getCmp('m-panel');
							
							//Spedizioni FLIGHT (CSFG04 = 'F') - Apro grid richiesta day
							if (rec.get('iconCls') == 'iconPartenze'){
								//Mostriamo elenco di riepilogo per TDDTEP/ITIN/TDNBOC)
			    			  	acs_show_win_std('Riepilogo date di produzione per programma di spedizione', 'acs_dettaglio_produzione_per_sped.php', 
			    			  		{		
			    			  		 sped_id: rec.get('sped_id'),
			    			  		}, 600, 550, {}, 'iconSpedizione');
				    			return false;
							}
							
							
							mp.add(
						  		show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_json.php?tipo_cal=per_sped&sped_id=' + rec.get('sped_id'), Ext.id(), [rec.get('sped_id')])
			            	).show();						  
						   return false;						
						}
						
						
					  	
				    }
				}
				
				
				, itemcontextmenu : function(grid, rec, node, index, event) {
					event.stopEvent();				

                	var voci_menu = [];
                	                	
				    voci_menu.push({
    	          	  text: 'Refresh',
    	        	  iconCls : 'icon-button_black_repeat_dx-16',	          		
    	        	  handler: function() {
    	        		//TODO: refresh di un singolo giorno
    	      			grid.getStore().treeStore.load();					        			
            		  }
		    		});				    		
                	                	
                	
                	//a livello di data				
                	if (rec.get('liv') == 'liv_1'){
    				    voci_menu.push({
        	          	  text: 'Invia dati spedizione del giorno a trasportatore',
        	        	  iconCls : 'icon-mobile-16',	          		
        	        	  handler: function() {
        	        	      var ar_trasp = {}
        	        		  rec.eachChild(function(rec_sped){
        	        		    if (rec_sped.get('c_trasportatore').trim() != ''){
            	        		    if (Ext.isEmpty(ar_trasp[rec_sped.get('c_trasportatore')]))
            	        		      ar_trasp[rec_sped.get('c_trasportatore')] = {den: rec_sped.get('trasportatore'), count: 0, ar_sped_id: []};
            	        		    
    						        ar_trasp[rec_sped.get('c_trasportatore')]['count'] += 1;
    						        ar_trasp[rec_sped.get('c_trasportatore')]['ar_sped_id'].push(rec_sped.get('sped_id'));
    						    }    
                              });
								
							var trasp_buttons = [];	
							Ext.Object.each(ar_trasp, function(key, trasp) {
                              trasp_buttons.push({
                              	xtype: 'button',
                              	text: '<h2>' + trasp.den.trim() + ' [' + key.trim() + ']</h2>Nr. spedizioni: ' + trasp.count,
                              	width: '100%',
                              	height: 50,
                              	handler: function(){
                              	
                              		//ToDo: DRY: e' come la funzione sotto
                            		Ext.Msg.confirm('Richiesta conferma', 'Confermi invio dati di monitoraggio scarico colli?', function(btn, text){
                        			     var timestamp = new Date().getTime();
                        			     if (btn == 'yes'){
                        
                        	     		      Ext.MessageBox.show({
                              					 msg: 'Estrazione dati dal gestionale',
                              					 progressText: 'Running...',
                              					 width:300,
                              					 wait:true, 
                              					 waitConfig: {interval:200},
                             				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
                        				       });
                        				       	
                        	    			   Ext.Ajax.request({
                        		      			   url: 'acs_delivery_control_to_trasp.php?fn=exe_gen_to_trasp',
                        		      			   method: 'POST',
                        		      			   jsonData: {
                            		      			   ar_sped_id: trasp.ar_sped_id,
                            		      			   tms: timestamp
                        		      			   }, 
                        		      			   				
                        		      			   success: function(response, opts) {
                            		      			     Ext.MessageBox.show({
                            	          					 msg: 'Generazione e invio file in corso',
                            	          					 progressText: 'Running...',
                            	          					 width:300,
                            	          					 wait:true, 
                            	          					 waitConfig: {interval:200},
                            	         				  	 icon:'ext-mb-download', //custom class in msg-box.html    	          
                            	      					 });
                            		      			  
                                		      			 Ext.Ajax.request({
                                		      			   url: 'acs_delivery_control_to_trasp.php?fn=exe_send_to_trasp',
                                		      			   method: 'POST',
                                		      			   jsonData: {
                                    		      			   ar_sped_id: trasp.ar_sped_id,
                                    		      			   tms: timestamp
                                		      			   }, 
                                		      			   				
                                		      			   success: function(response, opts) {
                                		      			     	Ext.MessageBox.hide();										      			
                                		      			   }
                                		      			 });		      				      			
                        		      			   }
                        		      		});
                        			     
                              			} //btn == 'yes'
                           
                            		}); //confirm                              		
                              	
                              	}
                              });
                            });	
                            
                            
								
                            	var new_win = new Ext.Window({
                            		  width: 300
                            		, height: 500
                            		, plain: true
                            		, title: 'Seleziona trasportatore'
                            		, layout: 'vbox'
                            		, border: true
                            		, items: trasp_buttons
                            		, autoScroll: true                     	
                            	});
                            	
                            	new_win.show();								
								
								return false;
                              
                		  } //handler
    		    		});	
    		    		
    		    	    voci_menu.push({
        	          	  text: 'Report orari carico',
        	        	  iconCls : 'icon-print-16',	          		
        	        	  handler: function() {
        	        	       acs_show_win_std('Report orari carico', 
            	               'acs_report_orari_carico.php?fn=open_form', {data: rec.get('cod')}, 400, 200, null, 'icon-leaf-16');           
        	        	      
    		    					    		                	
                	} });
                	}               					
                						
					
					// a livello di spedizione
                	if (rec.get('liv') == 'liv_2'){
                		                	
				  		    voci_menu.push({
				          		text: 'Dettagli spedizione',
				        		iconCls : 'iconSpedizione',          		
				        		handler: function() {
									mp = Ext.getCmp('m-panel');
									
									//Mostriamo elenco di riepilogo per TDDTEP/ITIN/TDNBOC)
				    			  	acs_show_win_std('Riepilogo date di produzione per programma di spedizione', 'acs_dettaglio_produzione_per_sped.php', 
				    			  		{		
				    			  		 sped_id: rec.get('sped_id'),
				    			  		}, 600, 550, {}, 'iconSpedizione');									
									
									/*
									mp.add(
								  		show_el_ordini(grid, rec, null, null, null, 'acs_get_elenco_ordini_json.php?tipo_cal=per_sped&sped_id=' + rec.get('sped_id'), Ext.id(), [rec.get('sped_id')])
					            	).show();
					            	*/
					            						        							        	
				        		}
				    		});
				    		
				    		
				  		    voci_menu.push({
				          		text: 'Modifica spedizione',
				        		iconCls : 'iconSpedizione',          		
				        		handler: function() {
				        		
				        			
					                   	my_listeners = {
				        					acsaftersave: function(){
				        							grid.getStore().treeStore.load();
								        		}
						    				};												
									
						    			  	acs_show_win_std('Modifica spedizione', 'acs_form_json_spedizione.php', 
						    			  		{		
						    			  		 sped_id: rec.get('sped_id'),						    			  		 
						    			  		 from_sped_id: 'Y'
						    			  		}, 600, 570, my_listeners, 'iconSpedizione');
																	    			  													
				        		
				        		
				        		}
				    		});
				    		
				    			
				  		    voci_menu.push({
				          		text: 'Modifica data/ora carico',
				        		iconCls : 'icon-leaf-16',          		
				        		handler: function() {
				        			my_listeners = {
				        				afterSave: function(from_win){
				        						acs_show_msg_info('Salvataggio eseguito');
				        						from_win.close();
								        	}
						    		};												
									
				    			  	acs_show_win_std(null, 'acs_form_json_spedizione_mod_data_carico.php?fn=open_form', 
		    			  		    {sped_id: rec.get('sped_id'), area : rec.get('taaspe')}, 500, 250, my_listeners);
							
				        		}
				    		});
				    		
	                   	my_listeners = {
        					beforeClose: function(){	
        						//dopo che ha chiuso la prenotazione orari/porte riaggiorno il grid
        						grid.getStore().treeStore.load();        									            
				        		}
		    				};				    		
				    		
				    		//Prenotazione orario/porta spedizione
				  		    voci_menu.push({
				          		text: 'Programmazione varchi',
				        		iconCls : 'icon-inbox-16',          		
				        		handler: function() {
				        		
							    	mp = Ext.getCmp('m-panel');
						    		Ext.Ajax.request({
						    		        url        : 'acs_panel_prenotazione_spedizioni_cal.php',
						    		        method     : 'POST',
						    		        jsonData: {
						    		        		sped_id_selected: rec.get('sped_id'),
						    		        		data_spedizione: rec.get('data_riferimento'),
						    		        		my_listeners: my_listeners

						    		        },
						    		        waitMsg    : 'Data loading',
						    		        success : function(result, request){
						    		            var jsonData = Ext.decode(result.responseText);
						    		            mp.add(jsonData.items);
						    		            mp.doLayout();
						    		            mp.show();
						    		        },
						    		        failure    : function(result, request){
						    		            Ext.Msg.alert('Message', 'No data to be loaded');
						    		        }
						    		    });
				        		
				        		}
				    		});				    		
				    		
				    		
				    		
				    		
				    						    		
				  		    voci_menu.push({
				          		text: 'Report scarichi',
				        		iconCls : 'iconPrint',          		
				        		handler: function() {
									
						    			  	acs_show_win_std('Parametri report scarichi', 'acs_get_elenco_stampa_form.php', 
						    			  		{		
						    			  		 rec_id: '|SPED|' + rec.get('sped_id'),						    			  		 
						    			  		 rec_liv: 'liv_0',
						    			  		 tipo_elenco: 'GATE'
						    			  		}, 600, 400, null, 'iconPrint');
																	    			  													
				        		
				        		
				        		}
				    		});		
				    		
				    		
				    		
					 		  voci_menu.push({
					        	text: 'Report Personalizzati',
					       		iconCls : 'iconPrint',          		
					       		handler: function() {
					       			acs_show_win_std('Report personalizzati', '<?php echo ROOT_PATH; ?>personal/dove_report_personalizzati_el_ordini.php?fn=get_json_form', {sped_id: rec.get('sped_id'), rec_liv: 'liv_0', rec: rec.data, tipo_elenco: 'GATE'}, null, null, null, 'iconPrint');          		
					       		}
					   		});
					   		
					   		
					   		
					   		
 		  voci_menu.push({
       		text: 'Verbale di carico',
      		iconCls : 'icon-pencil-16',          		
      		handler: function() {
      		
			 Ext.Msg.confirm('Richiesta conferma', 'Confermi generazione verbale di carico?', function(btn, text){
			    
			  if (btn == 'yes'){
	
				carichi = rec.get('carico_lotto_est');
				carichi_exp = carichi.split(",");
				
				
		    	  wait_win = new Ext.Window({
					  width: 400
					, height: 500
					, plain: true
					, title: 'Elaborazione verbale di carico'
					, layout: 'vbox'
					, border: true
					, closable: false
					, modal: true
					, items: [
					   {				
						xtype:'image', height: 400,
						src: <?php echo img_path("elaborazione.gif") ?>
					   }, {
						   flex: 1, width: '100%', align: 'center',
						   html: '<center>Avvio procedura<br/><br/><b>Attendere prego</b></center>',												   
					   }
					]
				});	
				wait_win.show();					
				

				
				//aggiunge 0 non significativi .. sembra non prendere quella in acs_js
				function tmp_pad(number, length) {
				    var str = '' + number;    
				    
				    while (str.length < length) {
				        str = '0' + str;
				    }
				   
				    return str;
				
				}
				
				

// Non piu' per carico ma per spedizione				
//				Ext.each(carichi_exp, function(c, index) {
				
//		      			carico	= c.trim();
//						carico_exp = carico.split("_");
												
						
//						if (tmp_pad(carico_exp[2], 6) != '000000') {
			
							        Ext.Ajax.request({
							        	url        : '../desk_firma/index2.php?fn=open_doc',
							        	jsonData   : {form_values: {
								        					//f_chiave: tmp_pad(carico_exp[0], 4) + tmp_pad(carico_exp[2], 6),
								        					f_chiave: tmp_pad(rec.get('sped_id'), 9),
									        				f_user: <?php echo j($auth->get_user()); ?>,
											        		check_disabled: 'Y'								        					
							        					} 
								        },
							            method: 'POST',
							            success: function ( result, request) {
							                jsonData = Ext.decode(result.responseText);
							                if (jsonData.success == true) {
							                    //acs_show_msg_info('Attendere elaborazione Verbale di carico');
							                    
							                    
							                    // INVIO A DELIVERY CONTROL
							                    if (rec.get('taaspe').trim() != '' && rec.get('taaspe') == <?php echo j($cfg_mod_Spedizioni["invia_dati_spedizione_su_stampa_verbale_per_area"]) ?>){
							                    	console.log('invio a delivery control');
							                    	var timestamp = new Date().getTime();
							                    	
					  		    			     		      Ext.MessageBox.show({
									          					 msg: 'Estrazione dati dal gestionale',
									          					 progressText: 'Running...',
									          					 width:300,
									          					 wait:true, 
									          					 waitConfig: {interval:200},
									         				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
														       });
											       	
											    			   Ext.Ajax.request({
												      			   url: 'acs_delivery_control.php?fn=call',
												      			   method: 'POST',
												      			   jsonData: {
												      			   sped_id: rec.get('sped_id'),
												      			   tms: timestamp
												      			   }, 
												      			   				
												      			   success: function(response, opts) {
												      			   Ext.MessageBox.show({
											          					 msg: 'Generazione e invio file in corso',
											          					 progressText: 'Running...',
											          					 width:300,
											          					 wait:true, 
											          					 waitConfig: {interval:200},
											         				  	 icon:'ext-mb-download', //custom class in msg-box.html
											          
											      					 });	
												      			  
														      			 Ext.Ajax.request({
														      			   url: 'acs_delivery_control.php?fn=send',
														      			   method: 'POST',
														      			   jsonData: {
														      			   sped_id: rec.get('sped_id'),
														      			   tms: timestamp
														      			   }, 
														      			   				
														      			   success: function(response, opts) {
														      			   		//grid.getStore().treeStore.load();
														      			     	Ext.MessageBox.hide();										      			
														      			   }
														      			   
														      			   
														      			});
												      		
												      			
												      			   }
												      			   
												      			   
												      			});							                    	
									                    	
							                    	
							                    }
							                    
							                    
							                    wait_win.down('panel').update('<center>Richiesta inviata a gestionale<br/><br/><b>Attendere prego</b></center>');					                    
							                    
							                } else {
								                wait_win.close();
												acs_show_msg_error('Errore: ' + jsonData.message);
							                }	
							                    
							            },
							            failure: function ( result, request) {
								            wait_win.close();
							            	acs_show_msg_error('Errore: ' + jsonData.message);
							            }
							        });
//					} //if != 000000							        
                        
//                }); //each carico

	                
                
                
                //chiudo dopo 60 secondi
					setTimeout(function(){
					   wait_win.close();
					}, 60*1000);
					
				
                
			} else {
			      //nothing
			 }
			});	                
                
                 
          		
      		} //handler verbale di carico
  		});
  		

<?php if ($cfg_mod_Spedizioni['invia_dati_spedizione_a_mbm'] == 'Y') { ?> 		  
				  		    voci_menu.push({
				          		text: 'Invia spedizione',
				        		iconCls : 'icon-mobile-16',  
				        		   		
				        		handler: function() {
				        		var timestamp = new Date().getTime();
				        		
				        		if (rec.get('secondaria') == 'S'){
									acs_show_msg_error('Spedizione secondaria. Opzione non disponibile');				        		
				        			return false;
				        		}
				        		
				        		
				        		Ext.Msg.confirm('Richiesta conferma', 'Confermi invio dati di monitoraggio scarico colli?', function(btn, text){
									
				    			     if (btn == 'yes'){

  		    			     		      Ext.MessageBox.show({
				          					 msg: 'Estrazione dati dal gestionale',
				          					 progressText: 'Running...',
				          					 width:300,
				          					 wait:true, 
				          					 waitConfig: {interval:200},
				         				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
									       });
									       	
									    			   Ext.Ajax.request({
										      			   url: 'acs_delivery_control.php?fn=call',
										      			   method: 'POST',
										      			   jsonData: {
										      			   sped_id: rec.get('sped_id'),
										      			   tms: timestamp
										      			   }, 
										      			   				
										      			   success: function(response, opts) {
										      			   Ext.MessageBox.show({
									          					 msg: 'Generazione e invio file in corso',
									          					 progressText: 'Running...',
									          					 width:300,
									          					 wait:true, 
									          					 waitConfig: {interval:200},
									         				  	 icon:'ext-mb-download', //custom class in msg-box.html
									          
									      					 });	
										      			  
										      			 Ext.Ajax.request({
										      			   url: 'acs_delivery_control.php?fn=send',
										      			   method: 'POST',
										      			   jsonData: {
										      			   sped_id: rec.get('sped_id'),
										      			   tms: timestamp
										      			   }, 
										      			   				
										      			   success: function(response, opts) {
										      			   		//grid.getStore().treeStore.load();
										      			     	Ext.MessageBox.hide();										      			
										      			   }
										      			   
										      			   
										      			});
										      		
										      			
										      			   }
										      			   
										      			   
										      			});
										      							    			     
									    			     
				    			     
					      			} //btn == 'yes'
				       
				        		});
				        		
				        		}
				        		
				    		});
				    		
<?php } ?>			


<?php if ($cfg_mod_Spedizioni['invia_dati_spedizione_a_sav'] == 'Y') { ?> 		  
				  		    voci_menu.push({
				          		text: 'Invia spedizione',
				        		iconCls : 'icon-mobile-16',  
				        		   		
				        		handler: function() {
				        		var timestamp = new Date().getTime();
				        		
				        		Ext.Msg.confirm('Richiesta conferma', 'Confermi invio dati di monitoraggio scarico colli?', function(btn, text){
									
				    			     if (btn == 'yes'){

  		    			     		      Ext.MessageBox.show({
				          					 msg: 'Estrazione dati dal gestionale',
				          					 progressText: 'Running...',
				          					 width:300,
				          					 wait:true, 
				          					 waitConfig: {interval:200},
				         				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
									       });
									       	
									    			   Ext.Ajax.request({
										      			   url: 'acs_delivery_control.php?fn=call_sav',
										      			   method: 'POST',
										      			   jsonData: {
										      			   sped_id: rec.get('sped_id'),
										      			   tms: timestamp
										      			   }, 
										      			   				
										      			   success: function(response, opts) {
										      			   Ext.MessageBox.show({
									          					 msg: 'Generazione e invio file in corso',
									          					 progressText: 'Running...',
									          					 width:300,
									          					 wait:true, 
									          					 waitConfig: {interval:200},
									         				  	 icon:'ext-mb-download', //custom class in msg-box.html
									          
									      					 });	
										      			  
										      			 Ext.Ajax.request({
										      			   url: 'acs_delivery_control.php?fn=send_sav',
										      			   method: 'POST',
										      			   jsonData: {
										      			   sped_id: rec.get('sped_id'),
										      			   tms: timestamp
										      			   }, 
										      			   				
										      			   success: function(response, opts) {
										      			   		//grid.getStore().treeStore.load();
										      			     	Ext.MessageBox.hide();										      			
										      			   }
										      			   
										      			   
										      			});
										      		
										      			
										      			   }
										      			   
										      			   
										      			});
										      							    			     
									    			     
				    			     
					      			} //btn == 'yes'
				       
				        		});
				        		
				        		}
				        		
				    		});
				    		
<?php } ?>







<?php if ($cfg_mod_Spedizioni['abilita_invia_dati_dc_spedizione_trasportatore'] == 'Y') { ?> 		  
    voci_menu.push({
  		text: 'Invia dati spedizione a trasportatore',
		iconCls : 'icon-mobile-16',  
		   		
		handler: function() {
    		var timestamp = new Date().getTime();
    		
    		Ext.Msg.confirm('Richiesta conferma', 'Confermi invio dati di monitoraggio scarico colli?', function(btn, text){
				
			     if (btn == 'yes'){

	     		      Ext.MessageBox.show({
      					 msg: 'Estrazione dati dal gestionale',
      					 progressText: 'Running...',
      					 width:300,
      					 wait:true, 
      					 waitConfig: {interval:200},
     				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
				       });
				       	
	    			   Ext.Ajax.request({
		      			   url: 'acs_delivery_control_to_trasp.php?fn=exe_gen_to_trasp',
		      			   method: 'POST',
		      			   jsonData: {
    		      			   ar_sped_id: [rec.get('sped_id')],
    		      			   tms: timestamp
		      			   }, 
		      			   				
		      			   success: function(response, opts) {
    		      			     Ext.MessageBox.show({
    	          					 msg: 'Generazione e invio file in corso',
    	          					 progressText: 'Running...',
    	          					 width:300,
    	          					 wait:true, 
    	          					 waitConfig: {interval:200},
    	         				  	 icon:'ext-mb-download', //custom class in msg-box.html    	          
    	      					 });
    		      			  
        		      			 Ext.Ajax.request({
        		      			   url: 'acs_delivery_control_to_trasp.php?fn=exe_send_to_trasp',
        		      			   method: 'POST',
        		      			   jsonData: {
            		      			   sped_id: rec.get('sped_id'),
            		      			   tms: timestamp
        		      			   }, 
        		      			   				
        		      			   success: function(response, opts) {
        		      			     	Ext.MessageBox.hide();										      			
        		      			   }
        		      			 });		      				      			
		      			   }
		      		});
			     
      			} //btn == 'yes'
   
    		}); //confirm
    		
		} //handler
		
	});				    		
<?php } ?>		




				
 		  
  		
  		
  		
				
				  		    voci_menu.push({
				          		text: 'Acquisisci Peso/Immagini',
				        		iconCls : 'icon-camera-16',          		
				        		handler: function() {
									
				    			  	acs_show_win_std('Acquisici peso/immagine spedizione', 'acs_acquisisci_peso_immagine.php', 
				    			  		{		
				    			  		 sped_id: rec.get('sped_id'),						    			  		 
				    			  		 from_sped_id: 'Y'
				    			  		}, 700, 550, {}, 'icon-camera-16');
				        		
				        		}
				    		});				
				
					   		
					   		
					   		
					   		
					   		
					   		
					   						    		
				    		
				    		
						      voci_menu.push({
					          		text: 'Sincronizza',
					        		iconCls : 'iconRefresh',	          		
					        		handler: function() {
					        			
					       			 Ext.Ajax.request({
					      			   url: 'acs_op_exe.php?fn=sincronizza_spedizione',
					      			   method: 'POST',
					      			   jsonData: {sped_id: rec.get('sped_id')}, 				
					      			   success: function(response, opts) {
					      			   	grid.getStore().treeStore.load();
					      			   }
					      			});
				        		}
				    		});				    		
                		
                	} //liv_1 - spedizione	
                	
                
                	
					
					var menu = new Ext.menu.Menu({
		            	items: voci_menu
					}).showAt(event.xy);
					return false;                		

                }		
										
		   },

		viewConfig: {
		        //row class CALENDARIO
		        getRowClass: function(record, index) {
		        	v = record.get('liv');					        	
		            return v;					            
		         	}   
		    },			

        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            width: 120,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            header: 'Spedizione',
            renderer: function (value, metaData, record, row, col, store, gridView){            
            	if (record.get('qtip_spedizione').length > 0)
	    			 	metaData.tdAttr = ' data-qtip="' + Ext.String.htmlEncode(record.get('qtip_spedizione')) + '"';
            
            	return value;
            }
        }, { 
            dataIndex: 'carico_lotto',
            header: 'Carico', width: 50
        }, { 
            dataIndex: 'ora_spedizione',
            header: 'Ora', width: 50
        }, { 
            dataIndex: 'porta',
            header: 'Porta', width: 40
        }, { 
            dataIndex: 'inizio',width: 75, align: 'right',
            header: 'Inizio',  renderer: function(value, p, record){
 
 				if (record.get('liv') != 'liv_2' && record.get('liv') != 'liv_3')
 					return '';

                        if (record.get('data_inizio') == 0 && record.get('ora_inizio')==0)                        	
                        	return datetime_from_AS_concat(record.get('min_spunta_sped'), 'd/m', record.get('data_riferimento'));
                        			
						return datetime_from_AS(record.get('data_inizio'), record.get('ora_inizio'), 'd/m') + '(s)';
            }
        }, { 
            dataIndex: 'itinerario',
            header: 'Itinerario', renderer: function (value, metaData, record, row, col, store, gridView){
            			metaData.tdCls += ' grassetto';
						return value;
            }
        }, { 
            dataIndex: 'colli_T',
            header: 'Colli', align: 'right', width: 50
        }, { 
        	xtype: 'progressbarcolumn',
            dataIndex: 'colli_S_progress',
            header: 'Spediti', width: 60
        }, { 
            dataIndex: 'da_spedire',
            header: 'Da sped.', align: 'right', width: 50
        }, { 
            dataIndex: 'max_spunta_sped', width: 75,
            header: 'Spunta <br>sped.', align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){           
						if (record.get('liv') == 'liv_1') metaData.tdCls += ' font-normal';            
						return datetime_from_AS_concat(value, 'd/m', record.get('data_riferimento'));
            }
        }, 
        
        <?php if($m_params->form_values->tipo_data == 'S'){?>
         { 
        	xtype: 'progressbarcolumn',
            dataIndex: 'colli_SCAR_progress',
            header: 'Scarico', width: 60
        }, { 
            dataIndex: 'non_scaricati',
            header: 'Non scar.', align: 'right', width: 50
        }, { 
            dataIndex: 'min_spunta_scar', width: 75,
            header: 'Spunta scar.', align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){
						if (record.get('liv') == 'liv_1') metaData.tdCls += ' font-normal';            
						return datetime_from_AS_concat(value, 'd/m', record.get('data_riferimento'));
            }
        }
        
        <?php }else{?>
        
        { 
        	xtype: 'progressbarcolumn',
            dataIndex: 'colli_D_progress',
            header: 'Dispon.', width: 60
        }, { 
            dataIndex: 'non_disponibili',
            header: 'Non disp.', align: 'right', width: 50
        }, { 
            dataIndex: 'min_spunta_disp', width: 75,
            header: 'Spunta dispon.', align: 'right', renderer: function(value, metaData, record, row, col, store, gridView){
						if (record.get('liv') == 'liv_1') metaData.tdCls += ' font-normal';            
						return datetime_from_AS_concat(value, 'd/m', record.get('data_riferimento'));
            }
        }
        
        <?php }?>
        
        , { 
            dataIndex: 'volume',
            header: 'Volume', align: 'right', renderer: floatRenderer1, width: 50
        }, { 
            dataIndex: 'trasportatore',
            header: 'Trasportatore', flex: 1
        }, { 
            dataIndex: 'vmc',
            header: 'Vettore'
        }, { 
            dataIndex: 'costo_tr',
            header: 'Costo Trasporto', align: 'right', width: 100,
            renderer: function(value, metaData, record, row, col, store, gridView){
               console.log(record);
				if (record.get('costo_td') > 0)          
				return floatRenderer2(value) + '<BR>' + floatRenderer2(record.get('costo_td'));
				
				return floatRenderer2(value);
            }
        }, { 
            dataIndex: 'targa',
            header: 'Targa', width: 70
        }, { 
            dataIndex: 'importo',
            header: 'Importo', align: 'right', renderer: floatRenderer0, width: 70
        }  
         , <?php echo dx_mobile() ?>   			          			  
        ]   
  
 }
}