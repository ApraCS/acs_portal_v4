<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$mod_js_parameters = $s->get_mod_parameters();

//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($s->get_cod_mod());

$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

//se non ho ricevuto cod_iti (quindi provengo da INFO) recupero cod_iti dall'ordine selezionato
if (trim(strlen($m_params->cod_iti) == 0)){
	$from_INFO = 'Y';
	//$m_params->cod_iti = trim($m_params->list_selected_id[0]->cod_iti);
	$k_ordine = trim($m_params->list_selected_id[0]->k_ordine);
	$ord = $s->get_ordine_by_k_docu($k_ordine);
	$m_params->cod_iti = $ord['TDCITI'];
}

//recupero (dall'itinerario attuale) l'area di spedizione attuale
$itin_attuale = $s->get_TA_std('ITIN', $m_params->cod_iti);
$itin_attuale_cod = trim($m_params->cod_iti);
$area_attuale_cod = trim($itin_attuale['TAASPE']);

if(isset($m_params->solo_data) && strlen($m_params->solo_data) > 0)
    $solo_data = $m_params->solo_data;
else
    $solo_data = null;

//parametro per indicare (in ASS_SPED) se devo anche scrivere la data e il flag di conferma data
if (isset($m_params->confermaData))
	$richiesta_confermaData = $m_params->confermaData;
else
	$richiesta_confermaData = '';

	
//verifico che non ci siano ordini HOLD in stati non programmabili
if ($m_params->solo_raffronto != 'Y' && isset($cfg_mod_Spedizioni['stati_programmabili_HOLD'])){

    
    
    $el_ord = $s->get_el_spedizioni(array('list_selected_id' => acs_je($m_params->list_selected_id)), 'Y');
    
   	if (count($el_ord) > 0)
		$sql_where_tddocu .= " AND TDDOCU IN (" . sql_t_IN($el_ord) . ")";
	
		$sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_testate']}
				WHERE 1=1 {$sql_where_tddocu}
				AND TDSWSP = 'N' AND TDSTAT NOT IN (" . sql_t_IN($cfg_mod_Spedizioni['stati_programmabili_HOLD']) . ")";
		$stmt = db2_prepare($conn, $sql);
		$result = db2_execute($stmt);
		$row = db2_fetch_assoc($stmt);
		
		if ($row['C_ROW'] > 0){
			?>
				{"success":true, "items": [
				        {
				            html: 'Errore: la selezione comprende ordini HOLD con stati non programmabili. Gli ordini devono essere nei seguenti stati: ' + <?php echo acs_je($cfg_mod_Spedizioni['stati_programmabili_HOLD']) ?>
				        }
				]}			
		<?php	
			exit;
		}
}	



//inibire l'assegnazione di una nuova spedizione se l'ordine � in un certo stato
$ar_stati = $s->get_stati_non_ammessi('STONA', $area_attuale_cod);
if (count($ar_stati) > 0){
    $el_ord = $s->get_el_spedizioni(array('list_selected_id' => acs_je($m_params->list_selected_id)), 'Y');
    if (count($el_ord) > 0)
        $sql_where_tddocu .= " AND TDDOCU IN (" . sql_t_IN($el_ord) . ")";
        
        $sql = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_testate']}
        WHERE 1=1 {$sql_where_tddocu}
        AND TDSWSP = 'Y' AND TDSTAT IN (" . sql_t_IN($ar_stati) . ")";
              
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        $row = db2_fetch_assoc($stmt);
      
        if ($row['C_ROW'] > 0){
        
            ?>
				{"success":true, "items": [
				       {
				            html: '<h1>OPERAZIONE NON AMMESSA:</h1> <br> La selezione comprende ordini in stato "NON Programmabile" <br> (gli stati ordine non programmabili sono: ' + <?php echo acs_je($ar_stati) ?> + ')' 
				        }
				]}		
		<?php	
			exit;
		}
}

	
?>

{"success":true, "items": [

        {
            xtype: 'grid',
            id: 'grid-el-scelta-spedizioni',
            
     <?php if ($from_INFO != 'Y' || $cfg_mod_Spedizioni['forza_scelta_itinerario_su_ASS_SPED_da_INFO']=='Y') { ?>       

            dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                items: [										
				 {
		            xtype: 'combo',
					name: 'new_iti',
					id: 'combo_itinerario',
					fieldLabel: 'Itinerario',
					labelAlign: 'right',
		            store: {
		                autoLoad: true,
						proxy: {
				            type: 'ajax',
				            url : 'acs_get_select_json.php?select=std_TA_combo',
				            
		                	extraParams: {
		    		    		taid: 'ITIN',
		    		    		aspe: <?php echo acs_je($area_attuale_cod); ?>
		    				},				            
				            
				            reader: {
				                type: 'json',
				                root: 'options'
				            }
				        },       
						fields: ['id', 'text'],		             	
		            },
		            
		            value: '<?php echo trim($itin_attuale_cod); ?>',            
					valueField: 'id',                       
		            displayField: 'text',
		            anchor: '100%',
		            
			        listeners: {
			            change: function(field,newVal) {	      
			            				            
						   m_grid = this.up('window').down('grid');
						   m_grid.store.proxy.extraParams.cod_iti = newVal;
						   m_grid.store.load();			            
			                  		            	
			            }
			        }            
				
		        }					
					
					 
					

					
				, {
		            xtype: 'combo',
					name: 'area_spedizione',
					id: 'combo_area_spedizione',
					fieldLabel: 'Area spedizione',
					labelAlign: 'right',					
		            store: {
		                autoLoad: true,		            
						proxy: {
				            type: 'ajax',
				            url : 'acs_get_select_json.php?select=std_TA_combo',
				            
		                	extraParams: {
		    		    		taid: 'ASPE'
		    				},				            
				            
				            reader: {
				                type: 'json',
				                root: 'options'
				            }
				        },       
						fields: ['id', 'text'],		             	
		            },
		                        
		            value: '<?php echo trim($area_attuale_cod); ?>',            
					valueField: 'id',                       
		            displayField: 'text',
		            anchor: '100%',
		            
			        listeners: {
			            change: function(field,newVal) {
			            	combo_itin = this.up('window').down('#combo_itinerario').setValue('');
			            	combo_itin = this.up('window').down('#combo_itinerario');
			            	combo_itin.store.proxy.extraParams.aspe = newVal;
			            	combo_itin.store.load(); 	            				            
			            }
			        }            
				
		        }
						
									
		        , { xtype: 'fieldset', flex: 1,
					defaultType: 'textfield',
 			        layout: 'hbox',
 			        border: false,
 			        items: [
 			        
					<?php $scaduti_da_data = date('Ymd', strtotime($da_data . " -5 days")) ?>					
					<?php echo write_datefield_std('f_scadute_dal', 'Scad. da data', $scaduti_da_data, 0, array(
								'labelAlign' => 'right',
								'flex_width' => 'flex: 1',
								'itemId'	 => 'scadute_da_data',
								'on_change'  => "
													var f_scadute_dal = this.up('fieldset').down('#scadute_da_data');
													var f_solo_scadute = this.up('fieldset').down('#solo_bloccati');
													var m_grid = this.up('window').down('grid');
													if (f_solo_scadute.getValue() == true){
														m_grid.store.proxy.extraParams.scadute_da_data = Ext.Date.format(f_scadute_dal.getValue(), 'Ymd');
									   					m_grid.store.load();											
													}
												"
					)) ?> 			        							
					
					, {
							name: 'f_solo_scadute',
							itemId: 'solo_scadute',
							xtype: 'checkboxgroup',
							fieldLabel: 'Scadute',
							labelAlign: 'right',
						   	allowBlank: true,
						   	width: 100,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_solo_bloccati' 
		                          , itemId: 'solo_bloccati'
		                          , boxLabel: ''
		                          , inputValue: 'Y'		                          
		                        }],
		                    listeners: {
		                    	change: function(checkbox, checked){
		                    		var f_scadute_dal = checkbox.up('fieldset').down('#scadute_da_data');
									   m_grid = this.up('window').down('grid');
									   m_grid.store.proxy.extraParams.solo_scadute = checked;
									   m_grid.store.proxy.extraParams.scadute_da_data = Ext.Date.format(f_scadute_dal.getValue(), 'Ymd');
									   m_grid.store.load();			            						            			 	            					            					                    		
		                    	}
		                    }    														
					}

					]
				}	
					
					
					
					]
            }],            
            
     <?php } ?>       
            
            listeners: {
			  celldblclick: {								
				  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

				   rec = iView.getRecord(iRowEl);				  
				   m_win = this.up('window');
				   
				   //se la mi ha passato il listener onSpedSelected
				   // eseguo l'evento ritornando il record selezionato
				   if (typeof(m_win.initialConfig.listeners) !== 'undefined' &&
				   		typeof(m_win.initialConfig.listeners.onSpedSelected) !== 'undefined')
				    {
				    
						if ( rec.get('STATUS') == 'ERR' ){
							acs_show_msg_error(rec.get('STATUS_MESSAGE'));
							return false;						
						}				    
				    
						m_win.fireEvent('onSpedSelected', rec);
						m_win.close();
				   		return false;						
					}					   

				  

				   m_carichi = rec.get('carico').split(',');
				   
				   carichi = [];
				   
				   //se passato, aggiungo il carico passato (se provengo da un tasto dx su una riga carico)
				   if (iView.store.proxy.reader.jsonData.properties.k_carico_provenienza)
				   		carichi.push([iView.store.proxy.reader.jsonData.properties.k_carico_provenienza, iView.store.proxy.reader.jsonData.properties.k_carico_provenienza]);
				   
				   //aggiungo i carichi in base alla spedizione selezionata
				   for (var i=0; i<m_carichi.length; i++) 
				   		carichi.push([m_carichi[i], m_carichi[i]]);

				   var loc_store = new Ext.data.ArrayStore({
				     fields: ['abbr', 'state'],
				     data : carichi 
				  });				  

				   
				   if (iView.store.proxy.reader.jsonData.properties.AR_NUOVO_STATO)				   
				   	data_nuovo_stato_ordine = iView.store.proxy.reader.jsonData.properties.AR_NUOVO_STATO
				   else
				    data_nuovo_stato_ordine = []
				   
				   var loc_nuovo_stato_ordine = new Ext.data.ArrayStore({
				     fields: ['id', 'text'],
				     data: data_nuovo_stato_ordine
				   });
				   
				   //se ho ordini di diverso stato (con CAR0S) e solo carico 0 allora non si puo'
					if (iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_DISTINCT > 1 &&					
											        iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_CAR0S > 0 &&
											        carichi.length < 2){
						acs_show_msg_error('Operazione ammessa solo su ordini con stesso stato');
						return false;
					}				   
				   
				  
				  
				  //se nello stesso giorno esiste un'altra spedizione con all'interno il cliente... chiedo conferma
				  var warning_message_confirm = '';
				  iView.store.each(function(rec_sped){
				  	if (rec_sped.get('CSPROG') != rec.get('CSPROG') && 
				  		rec_sped.get('DATA') == rec.get('DATA') && 
				  		rec_sped.get('ESISTE_CLIENTE') > 0) {
							warning_message_confirm = 'Scarico programmato su altra spedizione nella stessa data.<br/>Proseguire ugualmente?';												   		
				  	}
				  });

				  
				  
				  //Chiedo conferma, o il carico, o un'autorizzazione esplicita
						var win = new Ext.Window({
						  width: 600
						, height: 500
						, minWidth: 300
						, minHeight: 30
						, plain: true
						, title: 'Conferma assegnazione spedizione'
						, layout: 'fit'
						, border: true
						, closable: true
						, items: {
				            xtype: 'form',
				            bodyStyle: 'padding: 10px',
				            layout: 'vbox',
				            frame: true,
				            title: '',
				            url: 'acs_op_exe.php',				            
				            items: [
				            
				            
					, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},
						items: [		    				            
				            
				            
				            
				            {
								name: 'f_carico',
								height: 30,
								xtype: 'combo',
								fieldLabel: 'Carico',
								displayField: 'state',
								valueField: 'abbr',
								emptyText: '- seleziona -',
								forceSelection:true,								
							   	allowBlank: false,														
								store: loc_store,
								triggerAction: 'all',
								selectOnFocus:true,
								listeners: {
								    afterrender: function(combo) {
								        var recordSelected = combo.getStore().getAt(0);                     
								        combo.setValue(recordSelected.get('abbr'));
								    },
								    
									change: function(combo, row, index) {
						            	if (iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_DISTINCT > 0 &&
						            	    iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_CAR0S > 0 && 
						            		this.valueModels){
											
											var form = this.up('form').getForm();
																																							            
											if (this.valueModels[0].get('abbr').split('_')[2] == 0){
											
											   //segnalo errori se c'erano piu' stati (almeno un con CAR0S)
											   if (iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_DISTINCT > 1 &&
											        iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_CAR0S > 0){
											        acs_show_msg_error('Operazione ammesso solo su ordini con stesso stato');
											        if (this.up('window'))
											         this.up('window').close();
											        return true;
											   }     
																							
	     										form.findField('f_nuovo_stato_ordine').show();
	     										form.findField('f_nuovo_stato_ordine').enable();
	     									}											
											else {
												form.findField('f_nuovo_stato_ordine').hide();
												form.findField('f_nuovo_stato_ordine').disable();
											}
																													            	
						            	}
						            	  
						            	  
						            	  
<?php if (!isset($mod_js_parameters->ASS_CAR_bl) || $mod_js_parameters->ASS_CAR_bl != 'Y'){ ?>
						//se avevo indicato "Spedizione/carico non selezionabile per ordini/clienti bloccati"
						// quando mi seleziona il carico 0 devo abilitare il bottone di conferma
						form = this.up('form').getForm();
						
						if (form.findField('LOG').getValue() == 'Spedizione/carico non selezionabile per ordini/clienti bloccati'){
								if ( form.findField('f_carico').getValue().trim().split('_')[2] != '0' ){
									win.down('form').down('#btnConf').disable();
									
					           		//ToDo: rischio di saltare qualche controllo sulla deroga?
									form.findField('LOG').show();							
									//disabilito cosi' non ho problem con form.isValid()
									form.findField('f_autorizzazione').enable();
									form.findField('f_data').enable();
									form.findField('f_commento').enable();								
											         	
					           	} else {
					           		win.down('form').down('#btnConf').enable();
					           		
					           		//ToDo: rischio di saltare qualche controllo sulla deroga?
									form.findField('LOG').hide();							
									//disabilito cosi' non ho problem con form.isValid()
									form.findField('f_autorizzazione').disable();
									form.findField('f_data').disable();
									form.findField('f_commento').disable();								
					           		
					           	}
						}
<?php } ?>						            	  
						            	  
						            	  
						            	  
						            	 		
									},  		
									select: function(combo, row, index) {
						            	if (iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_DISTINCT > 0 &&
						            	    iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_CAR0S > 0 && 
						            		this.valueModels){
						            											
											var form = this.up('form').getForm();																					
																																							            
											if (this.valueModels[0].get('abbr').split('_')[2] == 0){
											
											   //segnalo errori se c'erano piu' stati (almeno un con CAR0S)
											   if (iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_DISTINCT > 1 &&
											        iView.store.proxy.reader.jsonData.properties.NUOVO_STATO_CAR0S > 0){
											        acs_show_msg_error('Operazione ammesso solo su ordini con stesso stato');
											        if (this.up('window'))
											        	this.up('window').close();
											        return true;
											   }     
											        
																							
	     										form.findField('f_nuovo_stato_ordine').show();
	     										form.findField('f_nuovo_stato_ordine').enable();
	     									}											
											else {
												form.findField('f_nuovo_stato_ordine').hide();
												form.findField('f_nuovo_stato_ordine').disable();
											}
						            														
											}		
											
											
											
<?php if (!isset($mod_js_parameters->ASS_CAR_bl) || $mod_js_parameters->ASS_CAR_bl != 'Y'){ ?>
						//se avevo indicato "Spedizione/carico non selezionabile per ordini/clienti bloccati"
						// quando mi seleziona il carico 0 devo abilitare il bottone di conferma
						form = this.up('form').getForm();
						
						if (form.findField('LOG').getValue() == 'Spedizione/carico non selezionabile per ordini/clienti bloccati'){
								if ( form.findField('f_carico').getValue().trim().split('_')[2] != '0' ){
									win.down('form').down('#btnConf').disable();
									
					           		//ToDo: rischio di saltare qualche controllo sulla deroga?
									form.findField('LOG').show();							
									//disabilito cosi' non ho problem con form.isValid()
									form.findField('f_autorizzazione').enable();
									form.findField('f_data').enable();
									form.findField('f_commento').enable();								
											         	
					           	} else {
					           		win.down('form').down('#btnConf').enable();
					           		
					           		//ToDo: rischio di saltare qualche controllo sulla deroga?
									form.findField('LOG').hide();							
									//disabilito cosi' non ho problem con form.isValid()
									form.findField('f_autorizzazione').disable();
									form.findField('f_data').disable();
									form.findField('f_commento').disable();								
					           		
					           	}
						}
<?php } ?>


															            	
									} 									    
								    
								}														 
							},

								
// ********************** NUOVO STATO (DA WPI0TA0 CON TATAID = 'CAR0S' **************************
							{
								name: 'f_nuovo_stato_ordine', hidden: true,
								height: 30,
								xtype: 'combo',
								fieldLabel: 'Modifica stato', labelAlign: 'right',
								displayField: 'text',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,								
							   	allowBlank: false,
							   	disabled: true,														
								store: loc_nuovo_stato_ordine,
								triggerAction: 'all',
								selectOnFocus:true,
							},
								
					]
				},				
								
								
// ********************** DEROGHE **************************** 					            
					            {
					            xtype: 'fieldset',
					            flex: 1, width: '100%',
					            title: 'Registro deroghe',
					            itemId: 'fs-deroghe',
					            items: [
					            
								 {
										name: 'f_data_programmata',
										xtype: 'hidden'
								 }, 								 {
										name: 'f_data_disponibilita',
										xtype: 'hidden'
								 }, {
										name: 'f_autorizzazione',
										xtype: 'combo',
										fieldLabel: 'Autorizzazione',
										displayField: 'text',
										valueField: 'id',
										emptyText: '- seleziona -',
										forceSelection:true,
									   	allowBlank: false, disableOnHide: true,														
										store: {
											autoLoad: true,
											editable: false,
											autoDestroy: true,	 
										    fields: [{name:'id'}, {name:'text'}],
										    data: [								    
											     <?php echo acs_ar_to_select_json(find_TA_std('AUDE'), ''); ?> 	
											    ] 
										}						 
									}, {
									   xtype: 'datefield'
									   // , width: '50%'
									   , startDay: 1 //lun.
									   , fieldLabel: 'Data'
									   , name: 'f_data'
									   , format: 'd/m/Y'
									   , submitFormat: 'Ymd'
									   , allowBlank: false, disableOnHide: true
									   , listeners: {
									       invalid: function (field, msg) {
									       Ext.Msg.alert('', msg);}
											}
								}, {
									name: 'f_commento',
									xtype: 'textfield',
									maxLength: 100,
									fieldLabel: 'Commento',
								    anchor: '-15',
								    allowBlank: false, disableOnHide: true							
								  }					            	
					            ]
					            }
// ********************** FINE DEROGHE (fieldset) ************************					            
					            
							  , {
							     //segnalazioni
				                 fieldLabel: '',
				                 name: 'LOG',
				                 disabled: true,
				                 xtype     : 'textareafield',
				                 height: 50, width: '100%',
				                 fieldCls: 'segnalazioni errore grassetto'	
					            }
					            			            
				            ],
				            
						buttons: [{
				            text: 'Conferma',
				            itemId: 'btnConf',
				            scale: 'large',
				            iconCls: 'icon-sub_blue_accept-32',
					            handler: function() {
					            	var form = this.up('form').getForm();
					            	var m_win = this.up('window');		
					            	var print_w = m_win;			            	
					            	
					            	m_grid = Ext.getCmp('grid-el-scelta-spedizioni');
					            	
					            	if(form.isValid()) {
					            		this.disable();
										Ext.Ajax.request({
										   url: 'acs_op_exe.php?fn=assegna_spedizione',
										   timeout: 2400000,
										   method: 'POST',
										   jsonData: {list_selected_id: m_grid.store.proxy.reader.jsonData.properties.el_ordini,
										   			  to_sped_id: iView.getSelectionModel().getSelection()[0].data.CSPROG,
										   			  to_carico:  form.findField('f_carico').getRawValue(),
										   			  f_data: Ext.Date.format(form.findField('f_data').getValue(), 'Ymd'),
										   			  f_commento: form.findField('f_commento').getValue(),
										   			  f_autorizzazione: form.findField('f_autorizzazione').getValue(),
										   			  f_data_programmata: form.findField('f_data_programmata').getValue(),
													  f_data_disponibilita: form.findField('f_data_disponibilita').getValue(),
													  f_nuovo_stato_ordine: form.findField('f_nuovo_stato_ordine').getValue(),
													  richiesta_confermaData: <?php echo j($richiesta_confermaData); ?>
										   			  }, 
										   
										   success: function(response, opts) {
										   
										   var jsonData = Ext.decode(response.responseText);
										   if(jsonData.success == false){
										   
										    acs_show_msg_error(jsonData.message);
     						        	    return; 
										   
										   }
										   
										   
														   //se la mi ha passato il listener onSpedConfirmed
														   // eseguo l'evento ritornando il record selezionato
														   if (typeof(print_w.initialConfig.listeners) !== 'undefined' &&
														   		typeof(print_w.initialConfig.listeners.onSpedConfirmed) !== 'undefined')
														    {
																print_w.fireEvent('onSpedConfirmed', rec);
							                                	print_w.close();																
																m_win.close();
														   		return false;						
															}										   
										   					   				
										   					
   				
													<?php if ($from_INFO=='Y' && $cfg_mod_Spedizioni['no_refresh_dopo_ASS_SPED']=='Y') { ?>   				
										   					//no refresh, ma barro le righe da cui sono partito
										   					///////////////////////////////////////////////////////////////////////////////
										   					m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
										   					<?php foreach($m_params->list_selected_id as $ls) { ?>
										   						var record = m_grid.getStore().treeStore.getRootNode().findChild('id',<?php echo j($ls->id)?>,true);
										   						record.set('rec_stato', 'Y');
																this.print_w.close();							                                
							                                	m_win.close();										   						
										   					<?php } ?>	
										   					return true;
										   			<?php } ?>
   				
										   					   											                                
							                                //altrimenti chiusra standard con chisura win e reload tree di partenza
							                                this.print_w.close();							                                
							                                m_win.close();
							                                console.log('ricarico m_grid');
									  						m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
									  						m_grid.getStore().treeStore.load();
									  						console.log('fine ricarico m_grid');
										   	
										   }
										});
					            	} else {
					            		console.log('non validooooo');
											var invalidFields = [];
											    Ext.suspendLayouts();
											    form.getFields().filterBy(function(field) {
											        if (field.validate()) return;
											        invalidFields.push(field);
											    });
											    Ext.resumeLayouts(true);
											    console.log(invalidFields);
					            		
					            		
					            	}
					          }	            	
						}]				            
				            						
						}

										
										
						}); //fine creazione win
						
						
						//controlli validita' spostamento (data disponib., vol/pes...)
						
						win.down('form').down('#fs-deroghe').hide();
						
						var is_valida = true;					
						var richiedi_deroga = false;
						var txt_log = '';
						
				<?php if (!isset($mod_js_parameters->check_CP) || $mod_js_parameters->check_CP != 0){ ?>						
			           //verifiche su colli PRODUZIONE
			           //es: 3|1750|817.0000|N (stadio | max_esubero | produzione attaule | solo assistenze Y/N)
			           dati_colli_exp = rec.get('DATI_COLLI_PER_DATA').split('|');
			           if (
			           			parseInt(this.store.proxy.reader.jsonData.properties.sum_colli_PRODUZIONE) > 0 && //ho colli di produzione
			           			( parseInt(dati_colli_exp[0]) == 2 || parseInt(dati_colli_exp[0]) == 1) &&	//SLUSH o ICE
			           			dati_colli_exp[3] == 'N' //non solo produzione
			           		){
			           		//verifico che non supero il limite max esubero produzione 
			           		if (parseInt(dati_colli_exp[2]) + parseInt(this.store.proxy.reader.jsonData.properties.sum_colli_PRODUZIONE) > parseInt(dati_colli_exp[1])){
								is_valida = false;		
								richiedi_deroga = false;											
								txt_log = 'Spostamento richiesto in data \'SLUSH\' o \'ICE\' con esubero limite capacita\' produttiva.';
			           		}			           			
						}
				<?php } ?>		
						
						
				        if (parseFloat(this.store.proxy.reader.jsonData.properties.ordini_con_obbligo_causale_riprogrammazione) > 0){
				         if ( rec.get('DATA') != this.store.proxy.reader.jsonData.properties.TDDTEP ){		         	
							is_valida = false;		
							richiedi_deroga = false;											
							txt_log = 'Immettere causale di riprogrammazione (ordine confermato, con forniture MTO programmate o in data \'ICE\')';
							}
				        }							

						if (iStore.store.proxy.reader.jsonData.properties.max_dispo > rec.get('DATA')){
							is_valida = true;						
							richiedi_deroga = true;
							txt_log = 'Data programmata non valida (ATP ' + date_from_AS(iStore.store.proxy.reader.jsonData.properties.max_dispo) + ')';
							
							win.down('form').getForm().findField('f_data_programmata').setValue(rec.get('DATA'));
							win.down('form').getForm().findField('f_data_disponibilita').setValue(iStore.store.proxy.reader.jsonData.properties.max_dispo);							
						} else if (iStore.store.proxy.reader.jsonData.properties.area_min_dispo >= rec.get('DATA')){
							is_valida = true;						
							richiedi_deroga = true;
							txt_log = 'Data programmata non valida (Programmazione ammessa solo dopo il ' + date_from_AS(iStore.store.proxy.reader.jsonData.properties.area_min_dispo) + ')';
							
							win.down('form').getForm().findField('f_data_programmata').setValue(rec.get('DATA'));
							win.down('form').getForm().findField('f_data_disponibilita').setValue(iStore.store.proxy.reader.jsonData.properties.area_min_dispo);							
						}

						
<?php if (!isset($mod_js_parameters->ASS_CAR_bl) || $mod_js_parameters->ASS_CAR_bl != 'Y'){ ?>						
						//se ho ordini/clienti bloccati non posso selezionare spedizioni con carico		
				           if (parseFloat(iStore.store.proxy.reader.jsonData.properties.T_cli_bloc) + 
				           		parseFloat(iStore.store.proxy.reader.jsonData.properties.T_ord_bloc) > 0){
					           	if ( rec.get('carico_d').length > 0 && rec.get('carico_d') != '0' ){		         	
									is_valida = false;		
									richiedi_deroga = false;											
									txt_log = 'Spedizione/carico non selezionabile per ordini/clienti bloccati';					           		
					           	}
				           }
<?php } ?>				           
						
						
						if (parseFloat(rec.get('VOL_DA_CARICARE')) > parseFloat(rec.get('VOL_CARICABILE')) && parseFloat(rec.get('VOL_D')) > 0 ){
							is_valida = false;		
							richiedi_deroga = false;											
							txt_log = 'Superato volume disponibile';							
						}						
						if (parseFloat(rec.get('PES_DA_CARICARE')) > parseFloat(rec.get('PES_CARICABILE')) && parseFloat(rec.get('PES_D')) > 0 ){
							is_valida = false;		
							richiedi_deroga = false;											
							txt_log = 'Superato peso disponibile';							
						}
						if (parseFloat(rec.get('PAL_DA_CARICARE')) > parseFloat(rec.get('PAL_CARICABILE')) && parseFloat(rec.get('PAL_D')) > 0 ){
							is_valida = false;	
							richiedi_deroga = false;							
							txt_log = 'Superato numero pallet disponibile';												
						}												

						
						if ( rec.get('STATUS') == 'ERR' ){
								is_valida = false;		
								richiedi_deroga = false;											
								txt_log = rec.get('STATUS_MESSAGE');						
						}
						
						if ( rec.get('STATUS') == 'FORCE_OK' ){
								is_valida = true;		
								richiedi_deroga = false;											
								txt_log = rec.get('STATUS_MESSAGE');						
						}						
						
						
						win.down('form').getForm().findField('LOG').setValue(txt_log);
						
						if (richiedi_deroga){
							<?php
							//se il profilo non ha i permessi per indicare la deroga....
								 if (strlen($js_parameters->deroga) > 0 && $js_parameters->deroga == 0){
								 	echo "is_valida = false;";
								 	echo "richiedi_deroga = false;";
								 }														
							?>								
						}						
									
						if (!is_valida){
							win.down('form').down('#btnConf').disable();
							///win.down('form').getForm().findField('LOG').show();		
							win.height = 180;						
						} else {
						
							if (!richiedi_deroga){
								//spedizione valida
								win.height = 150;
								win.down('form').getForm().findField('LOG').hide();
								
								//disabilito cosi' non ho problem con form.isValid()
								win.down('form').getForm().findField('f_autorizzazione').disable();
								win.down('form').getForm().findField('f_data').disable();
								win.down('form').getForm().findField('f_commento').disable();								
								
							} else {
								//richiedo deroga
								win.height = 320;								
								win.down('form').down('#fs-deroghe').show();														
							}
						}

						if (warning_message_confirm != ''){
					   		Ext.Msg.confirm('ATTENZIONE - Richiesta conferma', warning_message_confirm, function(btn, text){																							    
								if (btn == 'yes') {
									win.show();								
									return false;
								}
							});
						} else {													
							win.show();
				  		}
				  }
			  }	  
			},
			
			
		viewConfig: {
		        //Return CSS class to apply to rows depending upon data values
		        getRowClass: function(record, index) {
		        
				<?php if (strlen($m_params->scelta_sped_collegata_per_sped_id) == 0 && $m_params->solo_raffronto != 'Y'){ ?>		        
		        
				   if ( record.get('STATUS') == 'FORCE_OK' )		           		        
		           		return '';		        
		        
				   if (parseFloat(record.get('VOL_DA_CARICARE')) > parseFloat(record.get('VOL_CARICABILE')) && parseFloat(record.get('VOL_D')) > 0)		           
		           		return 'segnala_riga_rosso';		        
		        
		           if ( record.get('DATA') < this.store.proxy.reader.jsonData.properties.max_dispo )		           		        
		           		return 'segnala_riga_rosso';
		           		
		           //se ho ordini con vincolo riprogrammazione (senza causale riprog.) allora devo mantenere la data		
		           if (parseFloat(this.store.proxy.reader.jsonData.properties.ordini_con_obbligo_causale_riprogrammazione) > 0){
		           	if ( record.get('DATA') != this.store.proxy.reader.jsonData.properties.TDDTEP )		         	
		           		return 'segnala_riga_rosso';
		           }	
		           
<?php if (!isset($mod_js_parameters->ASS_CAR_bl) || $mod_js_parameters->ASS_CAR_bl != 'Y'){ ?>		           
		           //se ho ordini/clienti bloccati non posso selezionare spedizioni con carico		
		           if (parseFloat(this.store.proxy.reader.jsonData.properties.T_cli_bloc) + 
		           		parseFloat(this.store.proxy.reader.jsonData.properties.T_ord_bloc) > 0){
			           	if ( record.get('carico_d').length > 0 && record.get('carico_d') != '0' ){		         	
			           		return 'segnala_riga_rosso';
			           	}
		           }
<?php } ?>		           

		           
				<?php if (!isset($mod_js_parameters->check_CP) || $mod_js_parameters->check_CP != 0){ ?>		           
		           //verifiche su colli PRODUZIONE
		           //es: 3|1750|817.0000|N (stadio | max_esubero | produzione attuale | solo assistenze Y/N)
		           dati_colli_exp = record.get('DATI_COLLI_PER_DATA').split('|');
		           if (
		           		parseInt(this.store.proxy.reader.jsonData.properties.sum_colli_PRODUZIONE) > 0 && //ho colli di produzione
		           		( parseInt(dati_colli_exp[0]) == 2 || parseInt(dati_colli_exp[0]) == 1) &&	//SLUSH o ICE 
		           		dati_colli_exp[3] == 'N'			//no solo assistenze
		           	){
		           	//console.log('SLUSH');
		           	//console.log('Esubero: ' + parseInt(dati_colli_exp[1])); 
		           	//console.log('In spostamento: ' + parseInt(this.store.proxy.reader.jsonData.properties.sum_colli_PRODUZIONE));
		           	//console.log('Presenti: ' + parseInt(dati_colli_exp[2]));
		           		//verifico che non supero il limite max esubero produzione 
		           		if (parseInt(dati_colli_exp[2]) + parseInt(this.store.proxy.reader.jsonData.properties.sum_colli_PRODUZIONE) > parseInt(dati_colli_exp[1])){
		           			//console.log('segnala_riga_rosso!!!!');
		           			return 'segnala_riga_rosso';
		           		}
					}
				<?php } ?>
		           		
		           //verifico con la data di minima assegnazione disponibile impostata sull'area di spedizione
		           if ( record.get('DATA') <= this.store.proxy.reader.jsonData.properties.area_min_dispo )		           		        
		           		return 'segnala_riga_viola';		
		           																		
		           if ( record.get('DATA') == this.store.proxy.reader.jsonData.properties.max_dispo )		           		        
		           		return 'segnala_riga_giallo';
		         <?php } else {
		         	//scelta spedizione collegata	
		         	$sped_provenienza = $s->get_spedizione($m_params->scelta_sped_collegata_per_sped_id);		         	
		         	?>

		           if ( record.get('STATUS') == 'ERR' )		           		        
		           		return 'segnala_riga_rosso';
		         	
		         <?php } ?>  				           		
		           				           		
		           				           		
		         }   
		    },			
			
			
			store: {
					xtype: 'store',
					autoLoad:true,

					listeners: {
					            load: function () {	
					            var num_ord = parseFloat(this.proxy.reader.jsonData.properties.num_ordini);
					            var sum_vol = parseFloat(this.proxy.reader.jsonData.properties.sum_vol);
					            var sum_pes = parseFloat(this.proxy.reader.jsonData.properties.sum_pes);
					            var sum_pal = parseFloat(this.proxy.reader.jsonData.properties.sum_pal);					            					            
					            				              
					              m_date = this.proxy.reader.jsonData.properties.max_dispo; 		
					              if (m_date.length == 8)
									d_date = new Date(m_date.substr(0,4), m_date.substr(4,2) - 1, m_date.substr(6,2));
								  else
								    d_date = new Date();
								    
					              m_area_min_dispo = this.proxy.reader.jsonData.properties.area_min_dispo; 		
					              if (m_area_min_dispo.length == 8)
									out_area_min_dispo = new Date(m_area_min_dispo.substr(0,4), m_area_min_dispo.substr(4,2) - 1, m_area_min_dispo.substr(6,2));
								  else
								    out_area_min_dispo = false;
								    
								  m_grid = Ext.getCmp('grid-el-scelta-spedizioni');
								  var print_w = m_grid.up('window');
//								  print_w.setTitle('Assegna/Modifica spedizione');

						<?php if (strlen($m_params->scelta_sped_collegata_per_sped_id) == 0){ ?>
					              print_w.setTitle(print_w.title + ' - ATP ' + Ext.Date.format(d_date, 'd/m/Y'));
								  print_w.setTitle(print_w.title + ' (Ordini: ' + num_ord.toFixed(0));					              
					              print_w.setTitle(print_w.title + ' - Volume: ' + sum_vol.toFixed(2));
					              print_w.setTitle(print_w.title + ' - Peso: ' + sum_pes.toFixed(2));
					              print_w.setTitle(print_w.title + ' - Pallet: ' + sum_pal.toFixed(0) + ')');
					              
					              if (out_area_min_dispo != false)
					              	print_w.setTitle(print_w.title + ' - Dopo il ' + Ext.Date.format(out_area_min_dispo, 'd/m/Y'));
					              
					    <?php } ?>          					              					              
					            }
					 },
					
			
	  					proxy: {
								url: 'acs_get_select_json.php?select=get_el_spedizioni&da_oggi=Y&solo_data=' + <?php echo j($solo_data); ?>,
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
			        		    	list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
			        		    	cod_iti: '<?php echo trim($m_params->cod_iti); ?>',
			        		    	solo_scadute: 'N',
			        		    	solo_raffronto: '<?php echo trim($m_params->solo_raffronto); ?>',
			        		    	scelta_sped_collegata_per_sped_id: '<?php echo trim($m_params->scelta_sped_collegata_per_sped_id); ?>',
			        				},
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root',
				        			jsonData: {"bbbbb": 333},						            
						        }
							},
							
		        			fields: [
		            			'CSPROG', 'DATA', 'VETTORE', 'MEZZO', 'CASSA', 'CSDESC'
		            			, 'carico', 'carico_d', 'vmc', 'DATA_PROG', 'ORA_PROG', 'VOL_D', 'VOL_A', 'PES_D', 'PES_A', 'PAL_D', 'PAL_A'
		            			, 'VOL_CARICABILE', 'PES_CARICABILE', 'PAL_CARICABILE'
		            			, 'VOL_DA_CARICARE', 'PES_DA_CARICARE', 'PAL_DA_CARICARE'
		            			, 'ESISTE_CLIENTE', 'DATI_COLLI_PER_DATA', 'CSNSPC' 
		            			, 'STATUS', 'STATUS_MESSAGE'
		        			]							
							
			
			}, //store
			
			
			
			        columns: [	
 						{
			                header   : 'CD',
							width: 30,
						    renderer: function(value, p, record){
						    	if (record.get('ESISTE_CLIENTE') > 0) return '<img src=<?php echo img_path("icone/48x48/sub_black_next.png") ?> width=16>';
						    }							
			            }, {
			                header   : 'Data',  tdCls: ' grassetto',
			                dataIndex: 'DATA',
							renderer: function (value, metaData, record, row, col, store, gridView){						
							  				if ( (record.get('DATA') < this.store.proxy.reader.jsonData.properties.max_dispo ) ||
							  				      record.get('DATA') <= this.store.proxy.reader.jsonData.properties.area_min_dispo)
							  					 metaData.tdCls += ' grassetto';											
							  				return date_from_AS(value);			    
											}			                
			             }, {
			                header   : 'Vettore',
			                dataIndex: 'vmc',
			                flex: 90
			             }, {
			                header   : 'Carico',
			                dataIndex: 'carico_d',
			                width: 50 
			             }, {
			                header   : 'Volume', tdCls: ' grassetto',
							dataIndex: 'VOL_A',
							width: 60,
							renderer: function (value, metaData, record, row, col, store, gridView){						
							  				if (parseFloat(record.get('VOL_DA_CARICARE')) > parseFloat(record.get('VOL_CARICABILE')) && parseFloat(record.get('VOL_D')) > 0)
							  					 metaData.tdCls += ' grassetto';											
							  				return value;			    
											}														
			            }, {
			                header   : 'Max',
							dataIndex: 'VOL_D',
							width: 60							
			            }
			            
			            
			            /*
			            , {
			                header   : 'Pallet',
							dataIndex: 'PAL_A',
							width: 60
			            }, {
			                header   : 'Max',
							dataIndex: 'PAL_D',
							width: 60							
			            }
			            */
			            
			            
			            , {
			                header   : 'Peso',
							dataIndex: 'PES_A',
							width: 60							
			            }, {
			                header   : 'Max',
							dataIndex: 'PES_D',
							width: 60							
			            }, {
			                header   : 'Sped',
			                dataIndex: 'CSPROG',
			                width: 50 
			            }, {
			                header   : 'Ora',
			                dataIndex: 'ORA_PROG', 
			                width    : 50,
			                renderer: function(value, p, record){
			                	return datetime_from_AS(record.get('DATA_PROG'), record.get('ORA_PROG'))
			    			}			                
			             }, {
			                header   : 'Descrizione',
			                dataIndex: 'CSDESC',
			                flex: 60 
			            }
			            
			            
			         ]																					
			
			
			
				  
	            
        }    

]}