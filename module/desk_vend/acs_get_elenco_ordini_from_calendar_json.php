<?php

require_once "../../config.inc.php";

set_time_limit(240);

$s = new Spedizioni();

	//filtro a monte (in base a dove ho fatto click)
	$sottostringhe = explode("|", $_REQUEST["m_id"]);
	
	//TODO: filtrare anch in base a $_REQUEST["node"]???? (in base al sotto-albero aperto)
	
	$liv1 = $sottostringhe[2];
	
	if ($_REQUEST['get_iti_by_par'] == 'Y') //da info (su un ordine Hold)
		$liv2 = $_REQUEST['cod_iti'];	
	else
		$liv2 = $sottostringhe[4];
	//come terza stringa ho il num. spedizione
	$liv3 = $sottostringhe[8];	 

		$filtro = array();
		
		//Per evitare problemi tra TDASPE (non piu' usato) e TAASEP,
		// al momento non filtor per area (tanto la chiave e' itinerario/data)
		//$filtro["area"] = $liv1;

		if (strlen($_REQUEST['sped_ar']) > 0)
			$filtro['ar_spedizioni'] = explode(",", $_REQUEST['sped_ar']);

		$filtro["itinerario"] = $liv2;
		
		$filtro["cliente"] = $liv3;
		//$filtro["vettore"] = substr($liv3, 9, 3);

		$titolo_panel = "Elenco ";
		$tipo_elenco = "LIST";
		
		if (strlen($_REQUEST["da_data"])==8){
			
			if (substr($_REQUEST["col_name"], 0, 2)=="d_")			
			{ //ho selezionato un giorno
				$add_day = (int)substr($_REQUEST["col_name"], 2, 2) - 1;
				$m_data = date('Ymd', strtotime($_REQUEST["da_data"] . " +{$add_day} days"));
				$m_data_per_panel = date('d/m', strtotime($_REQUEST["da_data"] . " +{$add_day} days")); 				
				$m_data_per_tbar = date('d/m/y', strtotime($_REQUEST["da_data"] . " +{$add_day} days"));
				$m_data_to = $m_data; //solo un giorno
				$tipo_elenco = "DAY";
			}
			elseif (trim($_REQUEST["col_name"])=='fl_da_prog')
			{ //ordini non programmabili (TDSWSP=='N') ... non filtro per data
				$filtro["TDSWSP"] = 'N';				
			}				
			else	
			{ //ho selezionato un'icona... non un giorno in particolare
				$m_data = date('Ymd', strtotime($_REQUEST["da_data"] . " +0 days"));
				$m_data_to = date('Ymd', strtotime($_REQUEST["da_data"] . " +14 days"));									
			}
			
			if (!isSet($filtro["TDSWSP"]))
				$filtro["TDSWSP"] = "Y";
			$filtro["data_spedizione"] = $m_data;			
			$filtro["a_data_spedizione"] = $m_data_to;			
		}
	
		
		if (strlen($filtro["cliente"]) > 0)
			$tipo_elenco = "LIST";		
		
		if (trim($_REQUEST["col_name"])=='fl_da_prog')
		 $tipo_elenco = "HOLD";

/*		 
		if (strlen($filtro["cliente"]) > 0 )
			$tipo_elenco = "LIST";
*/			
		
		$titolo_panel = $s->get_titolo_panel_by_tipo($tipo_elenco);
		
		if ($tipo_elenco == 'DAY')
			$titolo_panel .= " " . $m_data_per_panel;
						
		$titolo_panel .= " " . implode(' ', array($liv2, $liv3));

		
		//se ho aperto il livello degli ordini
		//if (($row['TDSECA'] . "___" . $row['TDCCON'] . "___" . $row['TDCDES'])  != $liv2_v){
		if ($_REQUEST['ordina_per_tipo_ord'] != 'Y')
		if (strlen($_REQUEST["node"]) > 0 && $_REQUEST["node"] != "root" && $_REQUEST["node"] != ''){
			$sottostringhe_root = explode("|", $_REQUEST["node"]);
			$liv2_root_explode = explode("___", $sottostringhe_root[4]);
			$filtro['seca'] = $liv2_root_explode[0];
			$filtro['cliente'] = $liv2_root_explode[1];
			$filtro['destinazione'] = $liv2_root_explode[2];
		}
		
		$tbar_title = 'Elenco ordini per data di produzione/disponibilit&agrave;';		
		switch($tipo_elenco){
			case 'HOLD': $tbar_title = 'Elenco per itinerario degli ordini con data produzione/disponibilit&agrave; alla spedizione non programmata'; break;
			case 'DAY': $tbar_title = "Elenco ordini programmati per itinerario e singola data produzione/disponibilit&agrave; alla spedizione ({$m_data_per_tbar})"; break;
			case 'LIST': $tbar_title = 'Elenco ordini programmati per itinerario nel periodo presentato dal "Plan"'; break;			
		}
		
		$items 	= $s->get_elenco_ordini($filtro, $_REQUEST['ordina_per_tipo_ord'], $tipo_elenco);
		$ars_ar = $s->crea_array_valori_el_ordini($items, $tipo_elenco, $_REQUEST['ordina_per_tipo_ord']);	
		$ars 	= $s->crea_alberatura_el_ordini_json($ars_ar, utf8_decode($_REQUEST["node"]), $filtro["itinerario"], $titolo_panel, $tbar_title, $tipo_elenco, $_REQUEST['ordina_per_tipo_ord'], $m_data);
		echo $ars;

		$appLog->save_db();
		

function date_from_as($num)
{
 return substr($num, 6, 2) . "/" . substr($num, 4,2) . "/" . substr($num, 2, 2);	
}

?>
