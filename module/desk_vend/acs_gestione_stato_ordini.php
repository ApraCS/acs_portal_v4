<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));


if ($_REQUEST['fn'] == 'exe_canc_detail'){
    
    $m_params = acs_m_params_json_decode();
 
    
    $sql = "DELETE FROM {$cfg_mod_Spedizioni['file_tabelle']} TA WHERE RRN(TA) = ?";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($m_params->RRN));
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;
}


if ($_REQUEST['fn'] == 'exe_cambia_stato'){

	$m_params = acs_m_params_json_decode();
    $ret = array();

    	$sh = new SpedHistory();
    	$ret_RI = $sh->crea(
    			'cambia_stato',
    			array(
    					"k_ordine" => $m_params->k_ordine,
    					"stato"    => $m_params->form_values->f_stato,
    			        "forza"    => $m_params->forza,
    			        "data_prevista" => $m_params->form_values->f_data_prevista
    			)
    			);
	
	
	$ret['success'] = true;
	$ret['ret_RI'] = $ret_RI;
	
	$ret['esito_atp']  = trim($ret_RI['RIFG01']);
	$ret['msg_atp']  = trim($ret_RI['RINOTE']);
	$ret['esito_cd']  = trim($ret_RI['RIFG02']);
	
	if($ret['esito_cd'] != "" && $ret['esito_atp'] != "")
	    $ret['msg_atp'] .= "<br> (manca anche il codice fiscale)";
	
    if($ret['esito_cd']  == 'Y')
	    $ret['msg_cd_b'] = "Opzione non disponibile: MANCA CODICE FISCALE CLIENTE";
    else
        $ret['msg_cd_f'] = "Opzione non disponibile: MANCA CODICE FISCALE CLIENTE. <br> Forzare la procedura?";
   
    echo acs_je($ret);
	exit;
}


if ($_REQUEST['fn'] == 'exe_inserisci_stato_from'){

	$m_params = acs_m_params_json_decode();

	$codice= $m_params->form_values->f_codice;

	$descr= $m_params->form_values->f_descr;

	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'CHSTF';
	$ar_ins['TAKEY1'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();


	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_inserisci_stato_to'){

	$m_params = acs_m_params_json_decode();

	$codice= $m_params->form_values->f_codice;

	$descr= $m_params->form_values->f_descr;

	$stato_from= $m_params->statof;

	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'CHSTT';
	$ar_ins['TAKEY1'] 	= $stato_from;
	$ar_ins['TAKEY2'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);

	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();

	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_master'){

	$m_params = acs_m_params_json_decode();

	$mod_rows= $m_params->mod_rows;

	foreach ($mod_rows as $v){

		if(isset($v)){
				
			$ar_ins = array();

			$ar_ins['TAKEY1'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TAFG01'] 	= trim($v->TAFG01);
			$ar_ins['TAFG02'] 	= trim($v->TAFG02);
			$ar_ins['TAFG03'] 	= trim($v->TAFG03);
			$ar_ins['TAFG04'] 	= trim($v->TAFG04);
				
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} TA
			SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
			WHERE RRN(TA) = '$v->rrn' ";
				
				

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
				
			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);
				
			//exit;

		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_detail'){

	$m_params = acs_m_params_json_decode();
	$mod_rows= $m_params->mod_rows;

	foreach ($mod_rows as $v){

		if(isset($v)){
				
			$ar_ins = array();
				
			$ar_ins['TAKEY2'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TAFG01'] 	= $v->write_ri;
			$ar_ins['TAFG02'] 	= $v->note_obbl;
			$ar_ins['TAFG03'] 	= $v->contr_cod_f;
			$ar_ins['TAFG04'] 	= $v->contr_data_atp;
			$ar_ins['TAFG05'] 	= $v->p_ord_acq;
			$ar_ins['TAFU01'] 	= $v->richiedi_data_prevista;
			$ar_ins['TACOGE'] 	= $v->prog;
			$ar_ins['TATELE']   = $v->msg_ri;
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} TA
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE RRN(TA) = '$v->rrn' ";

			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);

			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);

			//exit;

		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'get_data_grid'){


	$sql = "SELECT RRN (TA) AS RRN, TADESC, TAKEY1, TAFG01, TAFG02, TAFG03, TAFG04
	FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
	WHERE TATAID='CHSTF' AND TADT='$id_ditta_default'";

	/*print_r($sql);
	 exit;*/

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['codice'] = acs_u8e(trim($row['TAKEY1']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));


		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}


if ($_REQUEST['fn'] == 'get_data_grid_detail'){

	$m_params = acs_m_params_json_decode();
	$stato_from= $m_params->statof;

	$sql = "SELECT RRN(TA) AS RRN, TADESC, TAKEY2, TAFG01, TAFG02, TAFG03, TAFG04, TAFG05, TAFU01, 
            TACOGE, TATELE
	        FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
	        WHERE TADT='$id_ditta_default' AND TATAID='CHSTT' AND TAKEY1='{$stato_from}'";

	/*print_r($sql);
	 exit;*/

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

		$row['codice'] = acs_u8e(trim($row['TAKEY2']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['write_ri'] = acs_u8e(trim($row['TAFG01']));
		$row['note_obbl'] = acs_u8e(trim($row['TAFG02']));
		$row['contr_cod_f'] = acs_u8e(trim($row['TAFG03']));
		$row['contr_data_atp'] = acs_u8e(trim($row['TAFG04']));
		$row['p_ord_acq'] = acs_u8e(trim($row['TAFG05']));
		$row['richiedi_data_prevista'] = acs_u8e(trim($row['TAFU01']));
		$row['msg_ri'] = acs_u8e(trim($row['TATELE']));
		$row['prog'] = acs_u8e(trim($row['TACOGE']));
		$row['rrn'] = acs_u8e(trim($row['RRN']));
		

		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}




if ($_REQUEST['fn'] == 'open_grid'){

	?>

				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'Gestione stati',
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],

					store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'codice', 'descr', 'rrn', 'TAFG01', 'TAFG02', 'TAFG03', 'TAFG04'
		        			]
		    			},
		    		
			        columns: [
			       				        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             },
			             { 
			                header   : 'Non Mod.',
			                tooltip: 'N = Non modificabile (TAFG01)',
			                dataIndex: 'TAFG01', 
			                width: 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             },{ 
			                header   : 'Disab. mod. righe',
			                tooltip: 'TAFG02',
			                dataIndex: 'TAFG02', 
			                width: 110,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             },{ 
			                header   : 'Mostra riga in background',
			                tooltip: 'TAFG03',
			                dataIndex: 'TAFG03', 
			                width: 160,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             },{ 
			                header   : 'Date non mod.',
			                tooltip: 'TAFG04',
			                dataIndex: 'TAFG04', 
			                width: 160,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           	  }
			             }
			            
			         ],  
					
						listeners: {
						
						
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
				
			           voci_menu.push({
				         		text: 'Stati associati',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Stati consentiti', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_detail', {statof: rec.get('codice')}, 900, 450, null, 'icon-folder_search-16');          		
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}
					
					}, 
					
					 viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
					            console.log(m_grid);
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_stato_from',
											        method     : 'POST',
								        			jsonData: {
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												      m_grid.getStore().load();
												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
					   ]
					}, '->' ,
			     
			     {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			                console.log(list_rows_modified);
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_master',
									        method     : 'POST',
						        			jsonData: {
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php 

exit; 

}

if ($_REQUEST['fn'] == 'open_detail'){
	
	$m_params = acs_m_params_json_decode();
	$stato_from= $m_params->statof;

?>

{"success":true, "items": [

        {
        
        xtype: 'grid',
        multiSelect: true,
        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
			store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid_detail',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										
										statof: '<?php echo $stato_from; ?>'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'codice', 'descr', 'write_ri', 'note_obbl', 'prog', 'rrn', 'seq', 'msg_ri', 'contr_cod_f', 'contr_data_atp', 'p_ord_acq', 'richiedi_data_prevista'
		        			]
		    			},
		    		
			        columns: [
									        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			           
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             },
 						{ 
			                header   : 'Sincro Autom.',
			                tooltip  : 'TAFG01',
			                dataIndex: 'write_ri', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             },			                
			                
			                 { 
			                header   : 'Controllo codice fiscale',
			                tooltip  : 'TAFG03',
			                dataIndex: 'contr_cod_f', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             },    { 
			                header   : 'Controllo data ATP',
			                tooltip  : 'TAFG04',
			                dataIndex: 'contr_data_atp', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }, 
			              { 
			                header   : 'Presenza ordini d\'acquisto',
			                tooltip  : 'TAFG05',
			                dataIndex: 'p_ord_acq', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }, 
			              { 
			                header   : 'Riechiedi data prevista',
			                tooltip  : 'TAFU01',
			                dataIndex: 'richiedi_data_prevista', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }
			            
			         ],  
					
		listeners: {
						
						
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
				
			           voci_menu.push({
				         		text: 'Cancella',
				        		iconCls : 'icon-sub_red_delete-16',      		
				        		handler: function() {
				        			Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_canc_detail',
											        method     : 'POST',
								        			jsonData: {
														RRN: rec.get('rrn'),
													},							        
											        success : function(response, opts){											       
												     	grid.getStore().load();												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}
					
					},
					 viewConfig: {
			         getRowClass: function(record, index) {
			         return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_stato_to',
											        method     : 'POST',
								        			jsonData: {
								        				statof: '<?php echo $stato_from ?>',
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												         m_grid.getStore().load();
								            		  
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
			     
			  
					   ]
					},  '->',
					   {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_detail',
									        method     : 'POST',
						        			jsonData: {
						        			    statof: '<?php echo $stato_from ?>',
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }
					
					
					
					
					]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php

exit;

}

if ($_REQUEST['fn'] == 'change_state'){

	$m_params = acs_m_params_json_decode();
	$value = get_stato_consentito($m_params->k_ordine);
	
	
	
	?>
	
	{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: ['->'
			         
			         <?php if(count($value) > 0){?>
			         
			         , {
			            text: 'Conferma',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	var loc_win = this.up('window');
			            	
			          			if (!form.isValid()) return false;
							
								Ext.Ajax.request({
 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_stato',
 						        method     : 'POST',
 			        			jsonData: {
 			        			    k_ordine: '<?php echo $m_params->k_ordine?>',
 			        			    old_stato : '<?php echo $stato?>' ,
 			        			   form_values: form.getValues()
 								},							        
 						        success : function(result, request){
 						        	var jsonData = Ext.decode(result.responseText);
 						        	
 						        	   if(jsonData.esito_atp != ''){
 						        	   
 						        	   console.log('forzatura atp');
 						        	   
 						        	   //se non posso forzare esco
											<?php if(!$auth->is_admin() && $cambia_stato_disabilita_forzatura_atp=='Y'){ ?>
											 	acs_show_msg_error(jsonData.msg_atp);											 	
											 	return;
											<?php } ?>
 						        	   
 						        	   
 						        	   
 						        	    Ext.Msg.confirm('Errore', jsonData.msg_atp, function(btn, text){																							    
    					   				  if (btn == 'yes'){
    					   				  		Ext.Ajax.request({
            		 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_stato',
            		 						        method     : 'POST',
            		 			        			jsonData: {
            		 			        			    k_ordine: '<?php echo $m_params->k_ordine?>',
            		 			        			    forza : 'F',
            		 			        			    form_values: form.getValues()
            		 								},							        
            		 						        success : function(result, request){
            		 						        	var jsonData = Ext.decode(result.responseText);
            		 						        	    if (!Ext.isEmpty(loc_win.events.afteroksave))
                    											loc_win.fireEvent('afterOkSave', loc_win);
                    										 else
                    				            	 			loc_win.close();
            		 			            	   },
            		 						        failure    : function(result, request){
            		 						            Ext.Msg.alert('Message', 'No data to be loaded');
            		 						        }
            		 						    });	
        					   				  }else{
        					   				  	loc_win.close();
        					   				  }	
					   				  
					   				 	});
 						        	   
 						        	   
 						        	   }else if(jsonData.esito_cd == 'Y'){
 						        	   
     						        	   console.log('blocco CD');
     						        	   
     						        	    acs_show_msg_error(jsonData.msg_cd_b);
     						        	    return;
    		 							}else if(jsonData.esito_cd == 'A'){
 						        	   
     						        	   console.log('forzatura cd');
     						        	   
     						        	 Ext.Msg.confirm('Errore', jsonData.msg_cd_f, function(btn, text){																							    
    					   				  if (btn == 'yes'){
    					   				  		Ext.Ajax.request({
            		 						        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_cambia_stato',
            		 						        method     : 'POST',
            		 			        			jsonData: {
            		 			        			    k_ordine: '<?php echo $m_params->k_ordine?>',
            		 			        			    forza : 'F',
            		 			        			    form_values: form.getValues()
            		 								},							        
            		 						        success : function(result, request){
            		 						        	var jsonData = Ext.decode(result.responseText);
            		 						        	    if (!Ext.isEmpty(loc_win.events.afteroksave))
                    											loc_win.fireEvent('afterOkSave', loc_win);
                    										 else
                    				            	 			loc_win.close();
            		 			            	   },
            		 						        failure    : function(result, request){
            		 						            Ext.Msg.alert('Message', 'No data to be loaded');
            		 						        }
            		 						    });	
        					   				  }else{
        					   				  	loc_win.close();
        					   				  }	
					   				  
					   				 	});
    		 							}else{
    		 							
											 if (jsonData.ret_RI.RIESIT == 'W'){
											 	acs_show_msg_error(jsonData.ret_RI.RINOTE);
											 	
											 	return;
											 }		
					        
    	 						        if (!Ext.isEmpty(loc_win.events.afteroksave))
    										loc_win.fireEvent('afterOkSave', loc_win);
    									 else
    			            	 			loc_win.close();
    		            		    }
		 			            		 
		 			            		},
		 						        failure    : function(result, request){
		 						            Ext.Msg.alert('Message', 'No data to be loaded');
		 						        }
		 						    });	
		 			
		
			
			            }
			         }
					 <?php }?>
						
						], items: [
						
					<?php if(count($value) == 0){?>
					
						{
							name: 'f_text',
							xtype: 'displayfield',
							//fieldLabel: 'Nuovo stato',
							value: 'Non ci sono stati selezionabili',
					        forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
												 
						}
					
					<?php }else{?>
						{
							name: 'f_stato',
							xtype: 'combo',
							fieldLabel: 'Nuovo stato',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}, 'TAFU01'],
							    data: [								    
	                                  <?php echo acs_ar_to_select_json($value, '', true, 'N', 'N', 'Y') ?>
								    ] 
							}
							
							
							
							, listeners: {
        						select: function(ele, recs, idx) {
        							var rec = recs[0];
									var m_form = ele.up('form').getForm();
        							if (rec.get('TAFU01') == 'Y'){
        								//mostro field per richiesta data prevista
        								m_form.findField('f_data_prevista').enable();
        							} else {
        								//nascondo field per richiesta data prevista
        								m_form.findField('f_data_prevista').disable();
        							}

        						} //select
    						}
							
												 
						}, {
							     name: 'f_data_prevista'
							   , anchor: '-15', margin: "20 10 10 10"	           		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data evasione prevista'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: false
							   , disabled: true
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
						 	}
 						}
						
					<?php }?>
										 
		           ]}
					 
		           ]
		              }			  
			  
			  
			  ] //ITEM panel
			   
		}

	
	<?php
	exit; 

	
	
}

function controllo_stato_DI($k_ordine, $old_stato, $new_stato){
    
    global $cfg_mod_Spedizioni, $id_ditta_default, $conn, $auth;
    
    $s = new Spedizioni(array('no_verify' => 'Y'));
    
    $ret['cd_error'] = "";
    $ret['forza_cd'] = "";
    $ret['forza_atp'] = "";
    
    if($cod_fiscale == '' && $contr_cod_f == 'Y')
        $ret['cd_error'] = "Opzione non disponibile: MANCA CODICE FISCALE CLIENTE";
        
    if($cod_fiscale == '' && $contr_cod_f == 'A' && $auth->is_admin())
        $ret['forza_cd'] = 'Opzione non disponibile: MANCA CODICE FISCALE CLIENTE.<br>Forzare la procedura?';
            
    if($contr_data_atp == 'A' && $auth->is_admin())
        $ret['forza_atp'] = 'Y';
    
        return $ret;
}

function get_stato_consentito($k_ordine){
	
	global $cfg_mod_Spedizioni, $id_ditta_default, $conn;
	
	$s = new Spedizioni(array('no_verify' => 'Y'));
	
	global $auth;
    if($auth->is_admin()){
        //mostro tutti gli stati dalla tabella stati (stati from) 
        $sql = "SELECT TAKEY1 AS TAKEY2, TADESC FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
                WHERE TADT = '{$id_ditta_default}' AND TATAID = 'CHSTF'";
    } else {        
        //in base alle regole di cambio stato
        $row =  $s->get_ordine_by_k_ordine($k_ordine);
        $stato = $row['TDSTAT'];
        $ret = array();
        
        $sql = "SELECT TAKEY2, TADESC, TAFU01 FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
                WHERE TADT = '{$id_ditta_default}' AND TATAID = 'CHSTT' AND TAKEY1='{$stato}'";
    }	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	echo db2_stmt_errormsg();
	
	while ($row = db2_fetch_assoc($stmt)) {
		$r = array();
			
		$r['id'] 		= $row['TAKEY2'];
		$r['text']		= acs_u8e(trim($row['TADESC']));
		$r['TAFU01']	= trim($row['TAFU01']);
		
		$ret[] = $r;
		
	}
	
	return $ret;
}