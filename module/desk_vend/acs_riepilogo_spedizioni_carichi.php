<?php   

require_once "../../config.inc.php";


$s = new Spedizioni();
$main_module = new Spedizioni();


if ($_REQUEST['fn'] == 'get_json_data_carichi'){
	
	$m_params = acs_m_params_json_decode();

	
    $form_values=$m_params->open_request->form_values;
    $da_disp_colli=$form_values->tipo_elenco;
    $imp = array();

    //controllo data
    if (strlen($form_values->f_data) > 0)
    	$sql_where .= " AND CS.CSDTRG >= {$form_values->f_data}";
    if (strlen($form_values->f_data_a) > 0)
    	$sql_where .= " AND CS.CSDTRG <= {$form_values->f_data_a}";
 
	if($form_values->data_riferimento == "TDDTEP"){
		$tipo_data="TDDTEP";}
	if($form_values->data_riferimento == "TDDTSP"){
		$tipo_data="TDDTSP";}
		
		if($tipo_data==0){
			$tipo_data="TDDTEP";
		}
		
		$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
		
		$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);
		
		$sql_where.= sql_where_by_combo_value('TD.TDCITI', $form_values ->f_itinerario);
		
		$sql_where.= sql_where_by_combo_value('TD.TDCCON', $form_values ->f_cliente);
		
		//controllo opzione ordini con carico
		if ($form_values->f_solo_ordini_con_carico == 'Y')
			$sql_where .= " AND TD.TDFN01 = 1 ";
		
		if(count($m_params->open_request->list_rows)>0 ){
			
				$sql_where.= " AND ( 1=2 ";
				foreach($m_params->open_request->list_rows as $c){
						
					$car = explode("_", $c->k_carico);
					
					$anno_carico= $car[0];
			        $tipo_carico=$car[1];
					$num_carico= $car[2];
					
					$sql_where.= "OR( TDAACA = {$anno_carico} AND TDTPCA='{$tipo_carico}' AND TDNRCA= {$num_carico})";
		
				}
				
				$sql_where.= ")";
			
			}
			
			

			if(count($form_values ->f_pagamento)>0 ){
					
				$pagamento=array();
				$pagamento=$form_values ->f_pagamento;
			
					$sql1="SELECT TDNRCA, TDNBOC, TDASPE, TDTPCA, TDAACA, TA_ITIN.TAKEY1 AS ITIN, TA_ITIN.TADESC AS DESC_ITIN,
					        SUM (TDTIMP) AS IMPORTO, CS.CSDTRG AS DATA, SP.CSCVET AS VETTORE
						  FROM {$cfg_mod_Spedizioni['file_testate']} TD
						  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']}
                		  		ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
    					  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.{$tipo_data}
						  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN ON TD.TDDT=TA_ITIN.TADT AND TD.TDCITI=TA_ITIN.TAKEY1 AND TA_ITIN.TATAID='ITIN'
   						  LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD2
								ON TDDT=RD2.RDDT AND TDOTID=RD2.RDOTID AND TDOINU=RD2.RDOINU AND TDOADO=RD2.RDOADO AND TDONDO=RD2.RDONDO AND RD2.RDTPNO='RGMOB'
						 LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
						  WHERE 1=1 {$sql_where} AND TDCPAG IN (".sql_t_IN($pagamento).")
	                      GROUP BY TDNRCA, TDASPE, TDTPCA, TDAACA, TA_ITIN.TADESC, TA_ITIN.TAKEY1, CS.CSDTRG, SP.CSCVET, TDNBOC";
				
					
				$stmt1 = db2_prepare($conn, $sql1);
	            echo db2_stmt_errormsg();
	            $result = db2_execute($stmt1);
	           
	            while($row1 = db2_fetch_assoc($stmt1)){
	            	
	            	$gruop_by=[$row1["TDNRCA"], $row1["TDASPE"], $row1["TDTPCA"], $row1["TDAACA"], $row1["ITIN"], $row1["DATA"], $row1["VETTORE"], $row1["TDNBOC"]];
	            	
	            	$imp[implode("_", $gruop_by)] =$row1['IMPORTO'];
	            
	            }
	          
			}
			
	

	$sql="SELECT TDNRCA, TDNBOC, TDASPE, TDTPCA, TDAACA, TA_ITIN.TAKEY1 AS ITIN, TA_ITIN.TADESC AS DESC_ITIN, SUM(TDVOLU) AS VOLUME, SUM (TDTIMP) AS IMPORTO, CS.CSDTRG AS DATA, SP.CSCVET AS VETTORE,
	SUM(CASE WHEN TDFU03 IN ('N', '') THEN 0 ELSE RD2.RDQTA2 END) AS PROD, 
	SUM(CASE WHEN TDFU03 IN ('N', '') THEN 0 ELSE RD2.RDQTA3 END) AS RIV
	FROM {$cfg_mod_Spedizioni['file_testate']} TD
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']}
                ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.{$tipo_data}
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN ON TD.TDDT=TA_ITIN.TADT AND TD.TDCITI=TA_ITIN.TAKEY1 AND TA_ITIN.TATAID='ITIN'
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe']} RD2
					ON TDDT=RD2.RDDT AND TDOTID=RD2.RDOTID AND TDOINU=RD2.RDOINU AND TDOADO=RD2.RDOADO AND TDONDO=RD2.RDONDO AND RD2.RDTPNO='RGMOB'
	LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
    WHERE 1=1 {$sql_where}
	GROUP BY TDNRCA, TDASPE, TDTPCA, TDAACA, TA_ITIN.TADESC, TA_ITIN.TAKEY1, CS.CSDTRG, SP.CSCVET, TDNBOC ORDER BY CS.CSDTRG DESC";
	
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['data']	= $row['DATA'];
		$nr['area_spe']	= trim($row['TDASPE']);
		$nr['num_carico']	= trim($row['TDAACA'])."_".trim($row['TDTPCA'])."_".trim($row['TDNRCA']);
		$nr['itin']	= trim($row['DESC_ITIN']);
		$nr['volume']	= trim($row['VOLUME']);
		$nr['imp']	= trim($row['IMPORTO']);
		$gruop_by=[$row["TDNRCA"], $row["TDASPE"], $row["TDTPCA"], $row["TDAACA"], $row["ITIN"], $row["DATA"], $row["VETTORE"], $row["TDNBOC"]];
		$nr['imp_pagam'] = $imp[implode("_", $gruop_by)];
		$nr['prod']	= trim($row['PROD']);
		$nr['riv']	= trim($row['RIV']);
		$nr['vettore'] = $s->decod_std('AUTR', $row['VETTORE']);
		$nr['nr_spe']	= trim($row['TDNBOC']);
		
	    $ar[] = $nr;
	   
	}
	
	

	
	echo acs_je($ar);
	

 exit;	
}

$m_params = acs_m_params_json_decode();

?>


{"success":true, "items": [

{


<?php if($m_params->tipo_elenco !="INFO_PLAN"){
    
	 echo make_tab_closable(); ?>,
	title:'Delivery',
	
	<?php } ?>

            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            autoScroll:true,
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			            
            
            items: [					 	
					 {
			xtype: 'grid',
	
			<?php if($m_params->tipo_elenco !="INFO_PLAN"){ ?>
    
			tbar: new Ext.Toolbar({
	            items:[
	            
	            '<b>Riepilogo piani di spedizione</b>', '->'
	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	    
	        
	        	<?php } ?>
            selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},
			multiSelect: true,			
        
			loadMask: true,	
			 features: [
	
		{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ],
		      ftype: 'summary',
		}],	
	
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_carichi', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['data', 'area_spe', 'num_carico', 
		        			'itin', 'nr_spe', 'vettore', 
		        			{name: 'volume', type: 'float'},
		        			{name: 'imp', type: 'float'},
		        			{name: 'imp_pagam', type: 'float'},
		        			{name: 'prod', type: 'float'},
		        			{name: 'riv', type: 'float'}   
		        			]							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Data',
	                dataIndex: 'data',
	                width: 70,
	                renderer: date_from_AS,
	                filter: {type: 'string'}, 
	                filterable: true,
	                summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return 'Totale'; 
                                } 
	            },{
	                header   : 'AS',
	                dataIndex: 'area_spe',
	                width: 50,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
				{
	                header   : 'Carico',
	                dataIndex: 'num_carico',
	                 width: 150,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
				{
	                header   : 'Itinerario',
	                dataIndex: 'itin',
	               flex:1,
	                filter: {type: 'string'},
	                filterable: true
	            },
				{
	                header   : 'Volume',
	                dataIndex: 'volume',
	                align: 'right',
	                renderer: floatRenderer2,
	                 width: 100,
	                summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            },
				{
	                header   : 'Importo',
	                dataIndex: 'imp',
	                align:'right',
	                renderer: floatRenderer2,
	                 width: 100,
	                 summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            },
	            
	            <?php 
	             if($m_params->tipo_elenco != 'DA_DISP_COLLI') {?>
	            
	            {
	                header   : 'Produzione',
	                dataIndex: 'prod',
	                align:'right',
	                renderer: floatRenderer2,
	               width: 100,
	                 summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            },{
	                header   : 'Rivendita',
	                dataIndex: 'riv',
	                align:'right',
	                renderer: floatRenderer2,
	                width: 100,
	                 summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            }, {
	                header   : 'Imp. pagamento',
	                dataIndex: 'imp_pagam',
	                align:'right',
	                renderer: floatRenderer2,
	                 width: 130,
	                summaryType: 'sum', 
	                summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer2(value); 
                                } 
	            },
	            
	            <?php }?>
	            
	             {
	                header   : 'Vettore',
	                dataIndex: 'vettore',
	                flex: 1
	            },
	            {
	                header   : 'Nr Sped.',
	                dataIndex: 'nr_spe',
	                align:'right',
	                //renderer: floatRenderer2,
	                width: 60
	            }
	        
	         ] ,
	   		
			
		}
		 
						 
				],
			
	       buttons: [
	          <?php 
	          
	          if($m_params->tipo_elenco == 'DA_DISP_COLLI') {?>
	         
	            {
		        	text: 'Stampa',
		            iconCls: 'icon-print-32',
		            scale: 'large',	
		            handler: function() {
		            		var car = this.up('form').down('grid');
                            
                            carico_selected = car.getSelectionModel().getSelection();
                            list_selected_carico = [];
                            
                            for (var i=0; i<carico_selected.length; i++) {
				            list_selected_carico.push(carico_selected[i].data);}
                            
                            if (form.isValid()){
                                form.submit({
                                        url: 'acs_riepilogo_spedizioni_report.php',
                                        params: {
                                          
                                        	list_selected_carico: JSON.stringify(list_selected_carico),
                                        	form_values: JSON.stringify(form.getValues())},
                                        	standardSubmit: true,
                                        	submitEmptyText: false,
                                        	method: 'POST',
                                        	target : '_blank'
                                });
                            }
                        }                  
		  
		        } 
	          
	      
		        <?php }else{?>
		        
		         
		        
		            {
		        	text: 'Visualizza',
		            iconCls: 'icon-folder_search-32',
		            scale: 'large',	                     
		            handler: function() {
			            	var car = this.up('form').down('grid');
                            
                            carico_selected = car.getSelectionModel().getSelection();
                            list_selected_carico = [];
                            
                            for (var i=0; i<carico_selected.length; i++) {
				            list_selected_carico.push(carico_selected[i].data);}
				            
				            if (list_selected_carico.length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare almeno un carico');
									  return false;
								  }
							
							 
							 acs_show_panel_std('acs_riepilogo_spedizioni_tree.php', 'Riepilogo spedizioni', 
							 {
							 
							 <?php if($m_params->solo_plan !="PLAN"){ ?>
							 
							 form_values: form.getValues(), 
							 
							 <?php }?>
							 
							 list_selected_carico:list_selected_carico}, 
							 
							 
							 800, 400, null, 'iconPrint'); 
							//this.up('window').close();
		
			
			
			            }
		        
		        },
		        
		        {
		        	text: 'Stampa',
		            iconCls: 'icon-print-32',
		            scale: 'large',	
		            handler: function() {
		            		var car = this.up('form').down('grid');
                            
                            carico_selected = car.getSelectionModel().getSelection();
                            list_selected_carico = [];
                            
                            for (var i=0; i<carico_selected.length; i++) {
				            list_selected_carico.push(carico_selected[i].data);}
                            
                           
                             <?php if($m_params->solo_plan =="PLAN"){ ?>
                            
                            window.open ('acs_riepilogo_spedizioni_report.php?list_selected_carico=' +JSON.stringify(list_selected_carico),"mywindow");
                                
                            
                             <?php }else{  ?>
                             
                             if(form.isValid()){
                             	form.submit({
                             		url: 'acs_riepilogo_spedizioni_report.php',
                             		params: {
                             
                             			list_selected_carico: JSON.stringify(list_selected_carico),
                             			form_values: JSON.stringify(form.getValues())},
                             			standardSubmit: true,
                             			submitEmptyText: false,
                             			method: 'POST',
                             			target : '_blank'
                             	});
                             }
                             
                             
                              <?php }  ?>
                            
                        }                  
		  
		        } 
		        
		        <?php }?>
	            ]
				
        }
		
 				
	
     ]
        
 }