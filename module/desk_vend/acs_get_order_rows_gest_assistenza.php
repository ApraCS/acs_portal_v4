<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();

	$raw_post_data = acs_raw_post_data();
	if (strlen($raw_post_data) > 0)
	{	
		$m_params = acs_m_params_json_decode();
		$k_ordine = $m_params->k_ordine;
	} else {
		if (isset($_REQUEST['k_ordine']))
			$k_ordine = $_REQUEST['k_ordine'];
	}

	$oe = $main_module->k_ordine_td_decode_xx($k_ordine);
	$oe_orig = $main_module->k_ordine_td_decode_xx($m_params->k_ordine_orig);

	
	// ******************************************************************************************
	// call conferma ord_reso da righe origine
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_conferma_ord_reso_righe_origine'){
		$m_params = acs_m_params_json_decode();
		$ret = array();
		
		$all_ord=$m_params->all_ord;		
		$use_session_history = microtime(true);		
		
		foreach ($all_ord as $v){
			
			
			$sh = new SpedHistory();
			$sh->crea(
					'conferma_ord_reso',
					array(
							"k_ordine"		=> $m_params->k_ordine, //il tddocu
							"use_session_history" => $use_session_history,
							"num_prog"		=>  $v->RDNREC,
							"riga_comp"		=>  $v->riga_comp,
							"commento"		=>  $v->commento,
							"nrec_orig"		=>  $v->nrec_orig,
							"tipo_op"		=>  'CON',
							"causale_nc"	=>  $v->RDISOR
					)
		
					);
		
		}	
		
		$sh = new SpedHistory();
		$sh->crea(
				'conferma_ord_reso',
				array(
						"k_ordine"		=> '', //il tddocu
						"tipo_op"		=>  'CON',
						"end_session_history" => $use_session_history
				)
				);		
	
		$ret['success'] = true;
		echo acs_je($ret);
		exit;
	}
	
	
	
	
	
	// ******************************************************************************************
	// call avanza (per riga)
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_richiesta_avanza'){
		
		ini_set('max_execution_time', 6000);
		
		$m_params = acs_m_params_json_decode();		
		$ret = array();
	
		$ao = new SpedAssegnazioneOrdini();
		//($causale_rilascio, $ar_par, $nrec = 0){
	
		foreach($m_params->nrec as $v){
		  $ao->avanza($m_params->set_causale, array('prog' => $m_params->prog), $v);
	    }
		$ret['success'] = true;
		echo acs_je($ret);
		
		exit;
	}
	
	
	
	
// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	$data = array();
	
	$m_params = acs_m_params_json_decode();
	
	
	global $backend_ERP;
	
	switch ($backend_ERP){

		case 'GL':
			$sql = "SELECT *
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					LEFT OUTER JOIN {$cfg_mod_Admin['file_anag_art']} ART
					ON RD.MECAR0 = ART.MACAR0
					WHERE '1 ' = ? AND RD.MECAD0 = ? AND RD.METND0 = ? AND RD.MEAND0 = ? AND RD.MENUD0 = ?";
			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO']));
			
			while ($row = db2_fetch_assoc($stmt)) {
			$row['data_selezionata'] = sprintf("%08s", $dtep);
				$row['data_consegna'] = $row['MEDT20'];
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['MECAR0']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
			
				$row['RDRIGA'] = $row['MERID0'];
				$row['RDQTA'] = $row['MEQTA0'];
				$row['RDQTE'] = $row['MEQTS0'];
			
				$row['residuo'] = max((int)$row['MEQTA0'] - (int)$row['RDQTE'], 0);
				$row['RDUM'] = $row['MEUNM0'];
				$row['RDDART'] = acs_u8e($row['MADES0']);

				if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
					$row['COMMESSA'] = acs_u8e($row['MECME0']);
				}				
			
				$row['k_ordine'] = $k_ordine;
				$data[] = $row;
			}
							
		break;
		
		
		default: //SV2
		    
		    
			$sql = "SELECT RD.*, SUBSTR(RDFIL1, 6, 3) AS RDISOR
					FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
					WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
					AND RD.RDTISR = '' AND RD.RDSRIG = 0";
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $oe);
			
			while ($row = db2_fetch_assoc($stmt)) {
				$row['data_selezionata'] = sprintf("%08s", $dtep);
					$row['data_consegna'] = sprintf("%02s", $row['RDAAEP']) . sprintf("%02s", $row['RDMMEP']) . sprintf("%02s", $row['RDGGEP']);
				if ($row['data_selezionata'] == $row['data_consegna'])
					$row['inDataSelezionata'] = 0;
				else $row['inDataSelezionata'] = 1;
					$row['RDART'] = acs_u8e($row['RDART']);
				$row['RDDART'] = acs_u8e($row['RDDART']);
				$row['residuo'] = max((int)$row['RDQTA'] - (int)$row['RDQTE'], 0);
				$row['CAUSALE'] = substr($row['RDFIL1'], 5, 3);
				$row['k_ordine'] = $k_ordine;
				$row['RDNREC'] = $row['RDNREC'];
								
				
				$data[] = $row;
			}
							
		
	} //switch $backend_ERP
	

			//metto prima le righe della data selezionata
				////usort($data, "cmp_by_inDataSelezionata");
			
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $main_module->get_ordine_by_k_docu($n_ord);

			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		
exit();				
}

$m_params = acs_m_params_json_decode();
$tipo_show=$m_params->tipo_show;


$ao = new SpedAssegnazioneOrdini();
$rowAO = $ao->get_by_prog($m_params->prog);

$ord =  $main_module->get_ordine_by_k_docu($k_ordine);

$oe = $main_module->k_ordine_td_decode_xx($k_ordine);
$sql_cc = "SELECT RDNREC
        FROM {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
        WHERE RD.RDDT = ? AND RD.RDTIDO = ? AND RD.RDINUM = ? AND RD.RDAADO = ? AND RD.RDNRDO = ?
        AND RD.RDTISR = '' AND RD.RDSRIG = 0 AND RDART IN (" .sql_t_IN( $cfg_mod_Spedizioni["art_todo"]). ") ";

$stmt_cc = db2_prepare($conn, $sql_cc);
echo db2_stmt_errormsg();
$result = db2_execute($stmt_cc, $oe);
while ($row_cc = db2_fetch_assoc($stmt_cc)) {
    $ao->avanza('CC', array('prog' => $m_params->prog), $row_cc['RDNREC']);
}


?>


{"success": true, "items":
	{
		xtype: 'gridpanel',
		loadMask: true,  
		stateful: true,
        stateId: 'seleziona-righe-ordini-assistenza',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
        
        <?php if($tipo_show !='show'){?>
        selModel: {selType: 'checkboxmodel', mode: 'SIMPLE', checkOnly: true},
        <?php }?>
		multiSelect: true,			
		 plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
  	    store: Ext.create('Ext.data.Store', {
					
					
					groupField: 'RDTPNO',			
					autoLoad:true,
					loadMask: true,				        
  					proxy: {
  					
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
							type: 'ajax',
							actionMethods: {
								read: 'POST'
							},							
							reader: {
					            type: 'json',
					            root: 'root'
					        }
					        
					        , extraParams: {
					        	k_ordine: <?php echo j($k_ordine); ?>
					        }
					        
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
							, doRequest: personalizza_extraParams_to_jsonData	  					
  					
  					
  					
  						/*
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>',
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        }
						 */					        
						},
	        			fields: [
	            			{name: 'RDRIGA', type: 'int'}, 'RDNREC', 'RDART', 'RDDART', 'RDUM', 'RDQTA', 'RDQTE', 'RDSTEV', 'RDTPRI', 'RDPRIO', 'data_consegna', 'data_selezionata', 'inDataSelezionata', 'RDSTEV', 'RDSTAT', 'residuo'
	            			, 'RDTIPR', 'RDNRPR', 'RDTICA', 'RDNRCA', 'RDTPLO', 'RDNRLO'
	            			, 'RDQTDX', 'ARSOSP', 'ARESAU', 'RDANOR', 'RDNROR', 'RDDT', 'COMMESSA', 'CAUSALE'
	            			, 'k_ordine', 'RDNREC', 'RDISON', 'RDISOR', 'commento', 'riga_orig', 'nrec_orig', 'riga_comp'
	        			]
	    			}),
	    			
		        columns: [
		        
		            
		            <?php if ($m_params->tipo != 'origine' && $tipo_show != 'show') { ?>
		            {
		                header   : 'Caus.<br/>NC',
		                dataIndex: 'RDISOR', 
		                width    : 30,
		                tooltip  : 'Causale non conformit&agrave'
		             },
		             
		           <?php }?> 
		        
					/*{
		                header   : '<br/>&nbsp;Cons.',
		                dataIndex: 'data_consegna', 
		                width: 60,
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('inDataSelezionata') == 0) return ''; //non mostro la data se e' quella selezionata																	
						  					return date_from_AS(value);			    
										}		                
		                 
		             },		 */     
		             {
		                header   : '<br/>&nbsp;Riga',
		               
		                dataIndex: 'RDRIGA', 
		                width     : 40, align: 'right'
		             },		        
		             {
		                header   : '<br/>&nbsp;Articolo',
		                dataIndex: 'RDART', 
		                width     : 110
		             }
		             
		             
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		    	    		dataIndex: 'ARSOSP',
    	     				tooltip: 'Articoli sospesi',		    	    		 	    
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARSOSP') == 'Y') return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>';
		    			    	}}			    	
		    	    , {text: '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=24>',	width: 30, tdCls: 'tdAction',
		        			dataIndex: 'ARESAU',		
    	     				tooltip: 'Articoli in esaurimento',		        			    	     
		    			    renderer: function(value, p, record){
		    			    	if (record.get('ARESAU') == 'E') return '<img src=<?php echo img_path("icone/48x48/info_black.png") ?> width=18>';
		    			    	}}			    			             
		             
		             
		             , {
		                header   : '<br/>&nbsp;Descrizione',
		                dataIndex: 'RDDART', 
		                flex    : 150               			                
		             },	{
		                header   : '<br/>&nbsp;Commento',
		                width     : 110, 
		                dataIndex: 'commento',
		                renderer: function (value, metaData, record, row, col, store, gridView){		                	
							metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';																																
							return value;			    
						}
		             },	{
		                header   : 'Riga<br/>Orig.',
		                width     : 60, 
		                dataIndex: 'riga_orig',
		                renderer: function (value, metaData, record, row, col, store, gridView){		                	
							metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';																																
							return value;			    
						}
		             },{
		                header   : 'Riga<br/>Comp.',
		                width     : 60, 
		                dataIndex: 'riga_comp',
		                renderer: function (value, metaData, record, row, col, store, gridView){		                	
							metaData.style += 'background-color: white; font-weight: bold; border: 1px solid gray;';																																
							return value;			    
						}
		             }, {
		                header   : '<br/>&nbsp;Um',
		                dataIndex: 'RDUM', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Q.t&agrave;',
		                dataIndex: 'RDQTA', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDART').substr(0,1) == '*') return ''; //riga commento 																											
						  					return floatRenderer2(value);			    
										}		                		                
		             }, {
		                header   : '<br/>&nbsp;Evasa',
		                dataIndex: 'RDQTE', 
		                width    : 50,
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (parseFloat(value) > 0) return floatRenderer2(value); 																											
						  					return ''; 			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;Resid.', 
		                width    : 50,
		                dataIndex: 'residuo', 
		                align: 'right',
						renderer: function (value, metaData, record, row, col, store, gridView){
											if (record.get('RDQTE') == 0) return ''; //non mostro																											
						  					return floatRenderer2(value);			    
										}		                
		                
		             }, {
		                header   : '<br/>&nbsp;S',
		                dataIndex: 'RDSTEV', 
		                width    : 20,
		                align: 'right'
		             }, {
		                header   : '<br/>&nbsp;T.R.',
		                dataIndex: 'RDT	PRI', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Pr.',
		                dataIndex: 'RDPRIO', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;St.',
		                dataIndex: 'RDSTAT', 
		                width    : 30
		             }, {
		                header   : '<br/>&nbsp;Carico',
		                dataIndex: 'RDNRCA', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRCA') == 0) return ''; //non mostro																											
						  					return rec.get('RDTICA') + "_" + rec.get('RDNRCA');			    
										}		                
		             }
		             
		             
				<?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){ ?>
		             , {
		                header   : '<br/>&nbsp;Commessa',
		                dataIndex: 'COMMESSA', 
		                width    : 80
		             }				
		        <?php } else { ?>
		             , {
		                header   : '<br/>&nbsp;Lotto',
		                dataIndex: 'RDNRLO', 
		                width    : 80,
						renderer: function (value, metaData, rec, row, col, store, gridView){
											if (rec.get('RDNRLO') == 0) return ''; //non mostro																											
						  					return rec.get('RDTPLO') + "_" + rec.get('RDNRLO');			    
										}
		             }		        
		        <?php } ?> 
		            
		            
		         ]	    					
		
		, listeners: {		
	 			afterrender: function (comp) {
					comp.up('window').setTitle('<?php echo "Righe ordine {$m_params->tipo} {$oe['TDOADO']}_{$oe['TDONDO']} {$oe['TDPROG']}"; ?>');	 				
	 			}
	 			
	 			
	 			
				 , itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);				      		 
				      var voci_menu = [];
				      
					
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);
				      
				 } //itemcontextmenu	 			
	 			
	 			
	 			, celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();
					  	
					  	selection = iView.getSelectionModel().getSelection();

						if (col_name == 'commento' || col_name == 'riga_orig' || col_name=='riga_comp') {
								// create and show window
								var l_win = new Ext.Window({
								  width: 500
								, height: 160
								, plain: false
								, title: 'Inserimento/Modifica commento'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
										xtype: 'form',
									            bodyStyle: 'padding: 10px',
									            bodyPadding: '5 5 0',
									            frame: false,
									            layout: {
												    type: 'vbox',
												    //align: 'stretch',
												    pack : 'start',
												},
									            title: '',
												buttons: [
												
												{
										            text: 'Seleziona riga origine',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										            
										            	var m2_form = this.up('window').down('form').getForm();
										            	
										            		my_listeners = {
											            		onRowSelected: function(from_win, row_selected){
										        					m2_form.findField('mod_riga_orig').setValue(row_selected.get('RDRIGA'));
										        					m2_form.findField('mod_nrec_orig').setValue(row_selected.get('RDNREC'));
										        					from_win.close();
									        					},
									        					onCompoSelected: function(from_win, row_selected){
										        					m2_form.findField('mod_riga_orig').setValue(row_selected.get('riga_orig'));
										        					m2_form.findField('mod_riga_comp').setValue(row_selected.get('riga_comp'));
										        					m2_form.findField('mod_nrec_orig').setValue(row_selected.get('nrec'));
										        					from_win.close();
									        					}
										            		
										            		};

									        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest_select.php', {
									        				k_ordine: <?php echo j($m_params->k_ordine_orig) ?>,
									        			}, 900, 450, my_listeners, 'icon-folder_search-16');							                      	
										                										                

															
										            }
										        },												
												
												{
										            text: 'Conferma',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										                if (this.up('form').getForm().isValid()){										                
															rec.set('commento', this.up('form').getForm().findField('mod_commento').getValue());
															rec.set('riga_orig', this.up('form').getForm().findField('mod_riga_orig').getValue());
															rec.set('riga_comp', this.up('form').getForm().findField('mod_riga_comp').getValue());
															rec.set('nrec_orig', this.up('form').getForm().findField('mod_nrec_orig').getValue());
															if (iView.selModel.isSelected(rec) == false)
																iView.selModel.select(rec, true, false);
															else {
																iView.selModel.deselect(rec);
																iView.selModel.select(rec, true, false);
															}
															l_win.close();
										                } //isValid
										            }
										        }],             								
										items: [{
							                    xtype: 'textfield',							                    
							                    name: 'mod_commento',    
							                    value: rec.get('commento').trim(),
							                    fieldLabel: 'Commento',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 100,
							                    layout: 'fit',
							                    width: '100%'
							                }, 
										{xtype: 'fieldcontainer',
										layout: { 	type: 'hbox',
												    pack: 'start',
												    align: 'stretch'},						
										items: [{
							                	//riga origine
							                    xtype: 'textfield',							                    
							                    name: 'mod_riga_orig',    
							                    value: rec.get('riga_orig').trim(),
							                    fieldLabel: 'Riga origine',
							                    allowBlank: true,
							                    margin: '0 20 0 0',
							             		flex: 1,
							             		anchor: '-100px',
							                    maxLength: 6,
							                    readOnly: true,
							                    width: '150px'
							                }, {
							                	//riga componente
							                    xtype: 'textfield',							                    
							                    name: 'mod_riga_comp',    
							                    value: rec.get('riga_comp').trim(),
							                    fieldLabel: 'Riga componente',
							                    allowBlank: true,
							             		flex: 1,
							             		anchor: '-100px',
							                    maxLength: 6,
							                    readOnly: true,
							                    width: '150px'
							                } 
												
												]
											 }, {
							                	//riga origine
							                    xtype: 'hiddenfield',							                    
							                    name: 'mod_nrec_orig',    
							                    value: rec.get('nrec_orig').trim()
							                }
							            ]
									}	
								
								] 
								});
								l_win.show(); 							
							
 
						}
													
						return false;

					 }
					}, 
					
					beforeselect: function(grid, record, index, eOpts) {
						
							if(record.get('RDART').substring(0, 1)  == '*')
								return false;
							
       				 	} //beforeselect
	 			
	 			
	 			
			}
			
		, viewConfig: {
		        getRowClass: function(rec, index) {
					if (rec.get('RDART').substr(0,1) == '*') return 'rigaNonSelezionata'; //COMMENTO		        
		        	if (rec.get('RDPRIO') == 'RE') return 'rigaRevocata';
		        	if (parseFloat(rec.get('RDQTA')) <= parseFloat(rec.get('RDQTE')) || rec.get('RDSTEV') == 'S') return 'rigaChiusa';
		        	if (parseFloat(rec.get('RDQTE')) > 0 && parseFloat(rec.get('RDQTE')) < parseFloat(rec.get('RDQTA'))) return 'rigaParziale';
		        	if (rec.get('data_consegna') != rec.get('data_selezionata')) return 'rigaNonSelezionata';		        	
		         }   
		    }												    
			
		    <?php if($tipo_show !='show'){?>
		    
	       ,    dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
				  items: [
                       <?php if ($m_params->tipo != 'origine') { ?>	
	       		{
	         	xtype: 'splitbutton',
	            text: 'Conferma assegnazione causa assistenza', 
	            dock: 'left', 
	            iconCls: 'icon-button_red_play-32',
	            scale: 'large',
	            handler: function(){
	                 this.maybeShowMenu();
	           			},
	            menu: {
	            	xtype: 'menu',
	            	items: [
		        	
		        	<?php 
		        	
		        	foreach ($main_module->find_TA_std('RILAV', $rowAO['ASCAAS'], 'N', 'Y') as $ec) { ?> 	
						{
							xtype: 'button',
				            text: <?php echo j(trim($ec['text'])) ?>,
				            //iconCls: 'icon-save-32', 
				            scale: 'large',
				            textAlign: 'left',
				            handler: function() {
				            
				            var t_grid = this.up('grid');
				            var menu=  this.up('menu');
				            //menu.close();
				         	menu.up('splitbutton').hideMenu();
                            
                            row_selected = t_grid.getSelectionModel().getSelection();
                            list_selected_rows = [];
                            
                            //posso selezionare piu di una riga?
                            
                            for (var i=0; i<row_selected.length; i++) {
				            	list_selected_rows.push(row_selected[i].get('RDNREC'));}
				            	
				          
				            if (list_selected_rows.length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare almeno una riga');
									  return false;
								  }
								  
								 
								   Ext.MessageBox.show({
			          					 msg: 'Loading',
			          					 progressText: 'Loading...',
			          					 width:200,
			          					 wait:true, 
			          					 waitConfig: {interval:200},
			         				  	 icon:'ext-mb-download', //custom class in msg-box.html
			          
			      					 });	
	
								//carico la form dal json ricevuto da php
								Ext.Ajax.request({
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_avanza',
								        timeout: 2400000,
								        jsonData: {
													prog: <?php echo $m_params->prog ?>,
													nrec: list_selected_rows,
												    set_causale: <?php echo j(trim($ec['TAKEY2'])); ?>							        
								        },
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        	var jsonData = Ext.decode(result.responseText);
								        	Ext.MessageBox.hide();
								            t_grid.store.load();
								            
								            				            
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
			
				    }
				        },
				       <?php } ?>  
				            	        	
		        	]
		        }
	        },
	        
	        <?php } ?>	

		   <?php if ($m_params->tipo != 'origine' && $m_params->causale == 'ASSCAU') { ?>	       
		            {xtype: 'button',
		        	text: 'Ordine origine<br/><?php echo implode('_', array($oe_orig['TDOADO'], $oe_orig['TDONDO'])) ?>',
		            iconCls: 'icon-folder_search-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		            	var m_win = this.up('window');
					      
		                acs_show_win_std('Ricerca ordini', 
		                			'acs_modifica_ordine_origine.php?fn=ricerca_ordini_grid', 
		                			{ 
		                			 	cliente: <?php echo j($ord['TDCCON']); ?>,
		                			 	k_ordine: <?php echo j($k_ordine); ?>,
		                			 }, 700, 550, {
										afterSuccessSave: function(from_win, tddocu){   
		                                    //dopo che ha chiuso la maschera di assegnazione indice rottura
		                                    	from_win.close();
		                                    	m_win.close();
		                                    	Ext.getCmp(<?php echo j($m_params->from_win);?>).down('grid').store.load();
		                                    }		                			 
		                			 }, 'icon-shopping_cart_gray-16');					         	
					         	
		        		/*	acs_show_win_std('Ricerca ordine origine - <?php echo j($m_params->denom)?>', 'acs_modifica_ordine_origine.php?fn=ricerca_ordini_grid', {
		        				k_ordine: <?php echo j($m_params->k_ordine_assistenza)?>,
		        				cliente: <?php echo j($m_params->cliente)?>,
		        				from_win_seleziona_ordini: <?php echo j($m_params->from_win_seleziona_ordini)?>,
		        				prog: <?php echo j($m_params->prog)?>,
		        				from_win: this.up('window').id
		        			}, 800, 450, null, 'icon-folder_search-16');
		        		*/	 					         	
  			
            		} //hanlder
		        
		          },	       
			<?php } ?>
			
			{ xtype: 'button',
			 text: 'Commento',
              iconCls: 'icon-copy-32',
		      scale: 'large',	
			  handler: function(){
			  
			   var t_grid = this.up('grid');
               row_selected = t_grid.getSelectionModel().getSelection();
               
               	if (row_selected.length == 0){
        			acs_show_msg_error('Selezionare almeno una riga');
        			return false;			
        		}
               
			   var l_win = new Ext.Window({
								  width: 500
								, height: 160
								, plain: false
								, title: 'Inserimento/Modifica commento'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
										xtype: 'form',
									            bodyStyle: 'padding: 10px',
									            bodyPadding: '5 5 0',
									            frame: false,
									            layout: {
												    type: 'vbox',
												    //align: 'stretch',
												    pack : 'start',
												},
									            title: '',
												buttons: [
																							
												
												{
										            text: 'Conferma',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										                if (this.up('form').getForm().isValid()){										                
															for (var i=0; i<row_selected.length; i++) 
				            									row_selected[i].set('commento', this.up('form').getForm().findField('mod_commento').getValue());
															
															l_win.close();
										                } //isValid
										            }
										        }],             								
										items: [{
							                    xtype: 'textfield',							                    
							                    name: 'mod_commento',    
							                    fieldLabel: 'Commento',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 100,
							                    layout: 'fit',
							                    width: '100%'
							                }, 
										
							            ]
									}	
								
								] 
								});
								l_win.show(); 
			  
			  
			  }
			
			
			},

  	       '->',


<?php if ($rowAO['ASFG03'] == 'Y') {?>
				{
				    xtype: 'button',
		        	text: 'Conferma generazione ordine reso',
		            iconCls: 'icon-button_black_play-32',
		            scale: 'large',	                     
		            handler: function() {
			            	var t_grid = this.up('grid');
                            
                            row_selected = t_grid.getSelectionModel().getSelection();
                            list_selected_rows = [];
                            
                            for (var i=0; i<row_selected.length; i++) {
				            	list_selected_rows.push(row_selected[i].data);}
				            
				            if (list_selected_rows.length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare almeno una riga');
									  return false;
								  }

							   Ext.Msg.confirm('Richiesta conferma', 'Confermi generazione ordine reso?', function(btn, text){																							    
							   if (btn == 'yes'){																	         	
					         	
								Ext.Ajax.request({
								   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_conferma_ord_reso_righe_origine',
								   method: 'POST',
								   jsonData: {
								    k_ordine: row_selected[0].get('k_ordine'),
								   	all_ord: list_selected_rows
								   }, 
								   
								   success: function(response, opts) {								   	
									   t_grid.up('window').close(); 				                      
								   }, 
								   failure: function(response, opts) {								    
								      Ext.Msg.alert('Message', 'No data to be loaded');
								   }
								});						         	
					         	
								}
							   });  			
            		}
		        
		          }
<?php } ?>		          
		   
		   
					 ]
					 
					  }] 		    
		    
		         <?php }?>
	}
}
