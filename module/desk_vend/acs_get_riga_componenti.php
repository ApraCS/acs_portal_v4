<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();

$m_params = acs_m_params_json_decode();

$c_art = acs_u8e($m_params->c_art);
$d_art = acs_u8e($m_params->d_art);
$riga_orig = $m_params->riga;

$from_lotto = $m_params->from_lotto;

if ($from_lotto == 'Y') {
    $row        = $m_params->row;
    $riga_comp  = $m_params->riga_comp;
    $desc_art   = $m_params->desc_art;
    $cod_art    = $m_params->cod_art;
    $dim1       = n($row->dim1,0);
    $dim2       = n($row->dim2,0);
    $dim3       = n($row->dim3,0);
    
    if($m_params->from_op == 'Y'){
        $ar_stmt = array($id_ditta_default, $row->RETPCO, $row->REAACO, $row->RENRCO, $row->RETPSV);
        $sql_where = ' AND REDT = ? AND RETPCO = ? AND REAACO = ? AND RENRCO = ? AND RETPSV = ? ';
    }else{
        $ar_stmt = array('FB', $id_ditta_default, $row->RETPCO, $row->RETPSV, $row->REAACO, $row->RENRCO, $row->RERIGA);
        $sql_where = ' AND RETPSV= ? AND REDT = ? AND RETIDO = ? AND REINUM = ? AND REAADO = ? AND RENRDO = ? AND RENREC = ?';
    }
    
    $sql = "SELECT RETPCO, RETPSV, REAACO, RENRCO, TESTAT
    FROM {$cfg_mod_Spedizioni['file_righe_componenti']} RE
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate_commessa']} TE
      ON RE.REDT = TE.TEDT AND RE.RETPSV = TE.TETPSV AND RE.RETPCO = TE.TETPCO AND RE.REAACO = TE.TEAACO AND RE.RENRCO = TE.TENRCO
    WHERE 1 = 1 {$sql_where} 
    LIMIT 1";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_stmt);
    $row_l1 = db2_fetch_assoc($stmt);
    $row_ta = find_TA_sys('BSTA', trim($row_l1['TESTAT']));
    if(trim($row_l1['TESTAT']) != '')
        $stato = "[".$row_l1['TESTAT']."] ".trim($row_ta[0]['text']);
    else 
        $stato = "";
 
    
}
    

$raw_post_data = acs_raw_post_data();
if (strlen($raw_post_data) > 0)
{   $k_ordine = $m_params->k_ordine;
    $nrec = $m_params->nrec;
}

if (isset($_REQUEST['k_ordine'])){
    $k_ordine = $_REQUEST['k_ordine'];
    $nrec = $_REQUEST['nrec'];
}

$oe = $main_module->k_ordine_td_decode_xx($k_ordine);
$or_row = $main_module->get_ordine_by_k_ordine($k_ordine);
//$od = $main_module->get_rows_ordine_by_num_dtep($k_ordine, '', '', '');
//print_r($od);


if ($_REQUEST['fn'] == 'get_json_data'){
    $data = array();
    
     $params = $m_params->open_request;
     $row = $m_params->open_request->row;
     $sql_campi_select = '';
     $sql_where = '';
     
     if($params->from_op == 'Y' && $params->from_lotto == 'Y'){
         $ar_stmt = array($id_ditta_default, $row->RETPCO, $row->REAACO, $row->RENRCO, $row->RETPSV);
         $sql_where .= ' AND REDT = ? AND RETPCO = ? AND REAACO = ? AND RENRCO = ? AND RETPSV = ? ';
     }else if($params->from_lotto == 'Y' && $params->from_op != 'Y') {
         $ar_stmt = array('FB', $id_ditta_default, $row->RETPCO, $row->RETPSV, $row->REAACO, $row->RENRCO, $row->RERIGA);
         $sql_campi_select .= " ,AR.ARDBPL , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_cicli']} CL WHERE CL.CIDT = RE.REDT AND  CL.CIART = AR.ARDBPL) AS NR_CL, FASI";
         $sql_where .= ' AND RETPSV= ? AND REDT = ? AND RETIDO = ? AND REINUM = ? AND REAADO = ? AND RENRDO = ? AND RENREC = ?';
         $join = " LEFT OUTER JOIN(
                    SELECT COUNT(*) AS FASI, RITPCO, RIAACO, RINRCO, RITPSV, RIRIGA
                    FROM {$cfg_mod_DeskUtility['file_cicli_ricom']} RI
                    WHERE RIDT = '{$id_ditta_default}' AND RITIPO = 'S'
                    GROUP BY RITPCO, RIAACO, RINRCO, RITPSV, RIRIGA
                ) RI
                ON RI.RITPCO = RE.RETIDO AND RI.RIAACO = RE.REAADO AND RI.RINRCO = RE.RENRDO AND RI.RITPSV = RE.REINUM AND RI.RIRIGA = RE.RENREC AND RERIGA = 10";
         
     
     } else {
         $ar_stmt = array('FB', $oe['TDDT'], $oe['TDOTID'], $oe['TDOINU'], $oe['TDOADO'], $oe['TDONDO'], $nrec);
         $sql_where .= ' AND RETPSV= ? AND REDT = ? AND RETIDO = ? AND REINUM = ? AND REAADO = ? AND RENRDO = ? AND RENREC = ?';
         $sql_campi_select .= " ,AR.ARDBPL , (SELECT COUNT(*) FROM {$cfg_mod_DeskUtility['file_cicli']} CL WHERE CL.CIDT = RE.REDT AND  CL.CIART = AR.ARDBPL) AS NR_CL, FASI";
         $join = " LEFT OUTER JOIN(
         SELECT COUNT(*) AS FASI, RITPCO, RIAACO, RINRCO, RITPSV, RIRIGA
         FROM {$cfg_mod_DeskUtility['file_cicli_ricom']} RI
         WHERE RIDT = '{$id_ditta_default}' AND RITIPO = 'S'
         GROUP BY RITPCO, RIAACO, RINRCO, RITPSV, RIRIGA
         ) RI
         ON RI.RITPCO = RE.RETPCO AND RI.RIAACO = RE.REAACO AND RI.RINRCO = RE.RENRCO AND RI.RITPSV = RE.RETPSV AND RI.RIRIGA = RE.RERIGA";
     }    
         
    $sql = "SELECT RE.*, RD.RDRIGA {$sql_campi_select}
    FROM {$cfg_mod_Spedizioni['file_righe_componenti']} RE
    LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_righe_doc_gest']} RD
    ON RE.REDT=RD.RDDT AND RE.RETIDO=RD.RDTIDO AND RE.REINUM=RD.RDINUM AND RE.REAADO=RD.RDAADO AND RE.RENRDO=RD.RDNRDO AND RE.RENREC=RD.RDNREC
    LEFT OUTER JOIN {$cfg_mod_DeskArt['file_anag_art']} AR 
    ON RE.REDT=AR.ARDT AND RE.REART=AR.ARART
    {$join}
    WHERE 1=1 {$sql_where}";
 
    
    $sql_op = "SELECT REAACO AS P_ANNO, RENRCO AS P_NUM, RETPCO AS P_TPO
                FROM {$cfg_mod_DeskArt['file_ricom']} RE
                WHERE REDT = '{$id_ditta_default}' AND RETIDO =  ? /*'CP'*/
                AND REINUM = 'FB' AND REAADO = ? AND RENRDO = ? AND RENREC = ?
                LIMIT 1";
      
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_stmt);
    
    $ar = array();
    $ar_puntatori = array();
    $tmp_ar_id = 0;
    $primo_livello = null;
    
    while ($row = db2_fetch_assoc($stmt)) {
        
        if (is_null($primo_livello)){
            $primo_livello = $row['RELIVE'];
        }
   
        $tmp_ar_id ++;
        
        if($row['RELIVE'] == $primo_livello){
            $ar_puntatori[0] = &$ar;
            $ar_r = &$ar;
         
        }else{
            
            if ($row['RELIVE'] <= $ultimo_liv){
                $ar_r = &$ar_puntatori[$ultimo_liv];
                $ar_r['leaf'] = true;
            }
            
            $ar_r = &$ar_puntatori[$row['RELIVE'] - 1]['children'];
            $ar_puntatori[$row['RELIVE']] = &$ar_r;
        }
        
        
        //$ar_new = $row;
        //$ar_new['children'] = array();
        $ar_new = array();
       
        $ar_new['id'] = $tmp_ar_id;
        $ar_new['task'] = $row['RELIVE'];
        $ar_new['codice'] = acs_u8e($row['REART']);
        $ar_new['desc_art'] = acs_u8e($row['REDART']);
        $ar_new['riga_comp'] = $row['RERIGA'];
        $ar_new['riga_orig'] = $row['RDRIGA'];
        
        $ar_new['var1'] = $row['REVAR1'];
        $dom1 = find_TA_sys('PUVR', trim($row['REVAR1']));
        $risp1 = find_TA_sys('PUVN', trim($row['REVAN1']), null, trim($row['REVAR1']));
        $ar_new['qtip_var1'] = "[".$row['REVAR1']."] ".$dom1[0]['text'] ."<br>[".$row['REVAN1']."] ".$risp1[0]['text'];
      
        $ar_new['var2'] = $row['REVAR2'];
        $dom2 = find_TA_sys('PUVR', trim($row['REVAR2']));
        $risp2 = find_TA_sys('PUVN', trim($row['REVAN2']), trim($row['REVAR2']));
        $ar_new['qtip_var2'] = "[".$row['REVAR2']."] ".$dom2[0]['text'] ."<br>[".$row['REVAN2']."] ".$risp2[0]['text'];
       
        $ar_new['var3'] = $row['REVAR3'];
        $dom3 = find_TA_sys('PUVR', trim($row['REVAR3']));
        $risp3 = find_TA_sys('PUVN', trim($row['REVAN3']), trim($row['REVAR3']));
        $ar_new['qtip_var3'] = "[".$row['REVAR3']."] ".$dom3[0]['text'] ."<br>[".$row['REVAN3']."] ".$risp3[0]['text'];
        
        $ar_new['quant'] = $row['REQTA'];
        $ar_new['dim1'] = $row['REDIM1'];
        $ar_new['dim2'] = $row['REDIM2'];
        $ar_new['dim3'] = $row['REDIM3'];
        $ar_new['collo'] = $row['REPROG'];
        $ar_new['lotto'] = trim($row['RETIDE']);
        
        $stmt_op = db2_prepare($conn, $sql_op);
        echo db2_stmt_errormsg();
        $result = db2_execute($stmt_op, array($row['RETPCO'], $row['REAADO'], $row['RENRDO'], $row['RERIGA']));
        $row_op = db2_fetch_assoc($stmt_op);
        
        $ar_new['P_ANNO'] = $row_op['P_ANNO'];
        $ar_new['P_NUM'] = $row_op['P_NUM'];
        $ar_new['P_TPO'] = $row_op['P_TPO'];
        $ar_new['qtip_lotto'] = $row_op['P_ANNO']."_".$row_op['P_NUM']."_".$row_op['P_TPO'];
        
        $ar_new['fasi']  =  $row['FASI'];
        $ar_new['RETPCO'] = trim($row['RETPCO']);
        $ar_new['RETPSV'] = trim($row['RETPSV']);
        $ar_new['REAACO'] = trim($row['REAACO']);
        $ar_new['RENRCO'] = trim($row['RENRCO']);
        $ar_new['RERIGA'] = trim($row['RERIGA']);
        $ar_new['REAADO'] = trim($row['REAADO']);
        $ar_new['RENRDO'] = trim($row['RENRDO']);
        $ar_new['RENREC'] = trim($row['RENREC']);
        $ar_new['un_mis'] = $row['REUM'];
        $ar_new['nrec'] = $row['RENREC'];
        $ar_new['liv'] = 'liv_'.$row['RELIVE'];
        $ar_new['cicli'] = trim($row['NR_CL']);
        $ar_new['ARDBPL'] = trim($row['ARDBPL']);
        
        $ar_r[$tmp_ar_id] = $ar_new;
       
        $ar_puntatori[$row['RELIVE']] = &$ar_r[$tmp_ar_id];
      
        $ultimo_liv = $row['RELIVE'];
        
 
    } //while
 
    //livello 0 esploso, o togliere??
    $ar_puntatori[$primo_livello]['expanded'] = true;
    //l'ultimo record e' foglia
    $ar_r = &$ar_puntatori[$ultimo_liv];
    $ar_r['leaf'] = true;
    
    foreach($ar as $kar => $row){
        $ret[] = array_values_recursive($ar[$kar]);
    }
    
   
    
    echo acs_je(array(
        'success' 	=> true,
        'children' 	=> $ret
    ));
    
    $appLog->save_db();
    exit();
}
?>


{"success": true, "items":
	{
		xtype: 'treepanel',
		stateful: true,
        stateId: 'seleziona-righe-ordini-distinta',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],

        tbar: new Ext.Toolbar({
	            items:["", '->'
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		         		
	         ]            
	        }),


        flex: 1,
	    useArrows: true,
	    rootVisible: false,
	    loadMask: true,        
		singleExpand: false,	
		store: Ext.create('Ext.data.TreeStore', {
							
					autoLoad:true,
					loadMask: true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&k_ordine=<?php echo $k_ordine; ?>&nrec=<?php echo $nrec; ?>',
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'children',
					            method: 'POST'
					        },
					         extraParams: {
								 open_request: <?php echo acs_je($m_params) ?>
			        			},
					        actionMethods: {read: 'POST'},
					        doRequest: personalizza_extraParams_to_jsonData
						},
	        			fields: [

	            			  'task', 'riga_comp', 'livello', 'codice', 'desc_art', 'un_mis',
	            			   'quant', 'riga_orig', 'nrec', 'dim1', 'dim2', 'dim3', 'collo', 'fasi', 
	            			   'var1', 'var2', 'var3', 'qtip_var1', 'qtip_var2', 'qtip_var3', 'liv', 'lotto',
	            			   'RETPCO', 'RETPSV', 'REAACO', 'RENRCO', 'RERIGA', 'RESTAT', 
	            			   'REAADO', 'RENRDO', 'RENREC', 'cicli', 'ARDBPL', 'qtip_lotto'
	            		]
	    			}),
	    			
	    			<?php $cl = "<img src=" . img_path("icone/48x48/tools.png") . " height=18>"; ?>
	    			<?php $fl = "<img src=" . img_path("icone/48x48/gear.png") . " height=18>"; ?>
	    			
		        columns: [
						        
		             {
		             	xtype: 'treecolumn', 
			            columnId: 'task', 
			            dataIndex: 'task',
			            menuDisabled: true, 
			            sortable: false,
			            header: 'Livello',
		                width     : 100
		             },		
		                {
		                header   : 'Riga',
		                dataIndex: 'riga_comp', 
		                width     : 40
		             },		        
		             {
		                header   : 'Articolo',
		                dataIndex: 'codice', 
		                width     : 110
		             },
		            {
		                header   : 'Descrizione',
		                dataIndex: 'desc_art', 
		                flex    : 150               			                
		             }, {
		                header   : 'V1',
		                dataIndex: 'var1', 
		                width     : 40,
		                renderer: function (value, metaData, record, row, col, store, gridView){
							
							if (record.get('var1') != 0)	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_var1')) + '"';								
							
							if (record.get('var1') == 0) return ''; //non mostro la data se e' quella selezionata																	
		  					return '<b>*</b>';			    
							}	              			                
		             }, {
		                header   : 'V2',
		                dataIndex: 'var2', 
		                width     : 40,
		                renderer: function (value, metaData, record, row, col, store, gridView){
		                    if (record.get('var2') != 0)	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_var2')) + '"';			
		                
							if (record.get('var2') == 0) return ''; //non mostro la data se e' quella selezionata																	
		  					return '<b>*</b>';			    
							}              			                
		             }, {
		                header   : 'V3',
		                dataIndex: 'var3', 
		                width     : 40,
		                renderer: function (value, metaData, record, row, col, store, gridView){
							
							if (record.get('var3') != 0)	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_var3')) + '"';			
							
							if (record.get('var3') == 0) return ''; //non mostro la data se e' quella selezionata																	
		  					return '<b>*</b>';			    
							}              			                
		             },{
		                header   : 'Dim. 1',
		                dataIndex: 'dim1', 
		                width    : 80,
		                align: 'right',
		                renderer : floatRenderer2
		             },{
		                header   : 'Dim. 2',
		                dataIndex: 'dim2', 
		                width    : 80,
		                align: 'right',
		                renderer : floatRenderer2
		             },{
		                header   : 'Dim. 3',
		                dataIndex: 'dim3', 
		                width    : 80,
		                align: 'right',
		                renderer : floatRenderer2
		             },{
		                header   : 'Um',
		                dataIndex: 'un_mis', 
		                width    : 30
		             }, {
		                header   : 'Q.t&agrave;',
		                dataIndex: 'quant', 
		                width    : 50,
		                align: 'right',
		                renderer : floatRenderer2
					                		                
		             }, {
		                header   : 'Collo',
		                dataIndex: 'collo', 
		                width    : 50,
		                align: 'right'
					                		                
		             },{ text: '<br><?php echo $fl; ?>' 
	        		    , width: 30 
            	        , dataIndex: 'fasi'
	        		    , tooltip: 'Fasi di lavorazione'
	        		    , renderer: function(value, p, record) {
	        		          if(record.get('fasi') > 0)return '<img src=<?php echo img_path("icone/48x48/gear.png") ?> width=15>';
	        		        }
	        		    
		        	    }, { text: '<br><?php echo $cl; ?>' 
	        		    , width: 30 
            	       
            	        <?php if($m_params->from_lotto == 'Y') {?>
            	        , dataIndex: 'cicli'
	        		    , tooltip: 'Cicli di lavorazione'
	        		    , renderer: function(value, p, record) {
	        		          if(record.get('cicli') > 0)return '<img src=<?php echo img_path("icone/48x48/tools.png") ?> width=15>';
	        		        }
	        		    <?php } else {?> 
	        		    , dataIndex: 'lotto'
	        		    , tooltip: 'Distinta di lavorazione'
	        		    , renderer: function(value, p, record) {
	        		         p.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_lotto')) + '"';
	    			  	     if(record.get('lotto') == 'L') return '<img src=<?php echo img_path("icone/48x48/tools.png") ?> width=15>';
	    		            }
    		            <?php }?>
		        	    }
		               
		       
		        
		         ]	    					
		
			, listeners: {	
			
			   beforeload: function(store, options) {
                            Ext.getBody().mask('Loading... ', 'loading').show();
                            },

                        load: function () {
                          Ext.getBody().unmask();
                        }, 	
	 			afterrender: function (comp) {
	 				<?php if($m_params->from_op == 'Y' && $m_params->from_lotto == 'Y'){
	 				    $title ="Distinta di lavorazione {$row_l1['RETPCO']}_{$row_l1['REAACO']}_{$row_l1['RENRCO']}_{$row_l1['RETPSV']}"; 
	 				     ?>
	 					comp.up('window').setTitle(<?php echo j($title)?>);	 				
	 				<?php }else if($m_params->from_lotto == 'Y' && $params->from_op != 'Y'){
	 				     $title = "Distinta di lavorazione riga {$riga_comp} [{$cod_art}] {$desc_art} - [Dim. {$dim1} x {$dim2} x {$dim3}] - {$row_l1['RETPCO']}_{$row_l1['REAACO']}_{$row_l1['RENRCO']}_{$row_l1['RETPSV']} Stato {$stato}";
	 				    ?>
	 					 comp.up('window').setTitle(<?php echo j($title); ?>);	 				
               		<?php }else{
               		    $title = "Distinta componenti ordine {$oe['TDOADO']}_{$oe['TDONDO']}_{$or_row['TDOTPD']} {$oe['TDPROG']} riga {$riga_orig} [{$c_art}] {$d_art}";  
               		    ?>	 			
						comp.up('window').setTitle(<?php echo j($title)?>);	 				
	 				<?php }?>
	 		
	 			},
	 			
				celldblclick: {								
					 fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
		           
		           		col_name = iView.getGridColumns()[iColIdx].dataIndex;				  	
					  	rec = iView.getRecord(iRowEl);
					  	var grid = this;
					  	var t_win = this.up('window');
					  	
			  	   		if(col_name == 'lotto' && rec.get('lotto') == 'L'){			
			   			   acs_show_win_std( 'Distinta di lavorazione'
			   							   , 'acs_get_riga_componenti.php'
			   							   , {row : rec.data, from_lotto : 'Y', k_ordine : '<?php echo $k_ordine; ?>', riga_comp : rec.get('riga_comp'), desc_art: rec.get('desc_art'), cod_art : rec.get('codice')}
			   							   , 1300, 450, null
			   							   , 'icon-folder_search-16'
			   							   );      
			   							   		
					    }else if(col_name == 'fasi' && rec.get('fasi') > 0 ){
					    
					       // var row_ri = {RETPCO: 'CP', RETPSV : 'FB', REAACO : rec.get('REAADO'), RENRCO : rec.get('RENRDO'), RERIGA : rec.get('RENREC')};
					        var row_ri = {RETPCO: rec.get('RETPCO'), RETPSV : 'FB', REAACO : rec.get('REAACO'), RENRCO : rec.get('RENRCO'), RERIGA : rec.get('RERIGA')};
					        acs_show_win_std(null
			   							   , '../desk_utility/acs_get_ciclo.php?fn=open_tab'
			   							   , {row : row_ri, k_ordine : '<?php echo $k_ordine; ?>', riga_comp : rec.get('riga_comp'), d_art: rec.get('desc_art'), c_art : rec.get('codice')}
			   							  );      
					    } else { 
					    
					    		if(col_name == 'cicli' && rec.get('cicli') > 0) {
					   			    acs_show_win_std( 'Ciclo lavorazione ' +rec.get('ARDBPL')  + ' - ' +rec.get('task') 
					   			                    , 'acs_anag_art_cicli.php?fn=open_tab'
					   			                    , {c_art: rec.get('ARDBPL'), from_lotto1 : 'Y'}, 850, 400, null, 'icon-tools-16');
					   			                      
					   			 } else { 
					   			 
					   			         <?php if($m_params->from_anag_art == 'Y'){?>	
					      					 show_win_dett_art(rec.get('codice'), rec);
				    					  <?php }else{?>
				    					  t_win.fireEvent('onCompoSelected', t_win, rec);
				    					  <?php }?>	
					                    }
					   
					           }
					  	
					  }
				  }
		
	 			 			
			}
			
		, viewConfig: {
			        getRowClass: function(record, index) {
			           ret = record.get('liv');
			           return ret;																
			         }   
			    }									    
			
		         
	}
}