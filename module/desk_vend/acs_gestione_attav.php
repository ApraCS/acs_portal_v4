<?php

require_once "../../config.inc.php";

$main_module = new Spedizioni();
$m_params = acs_m_params_json_decode();

if(isset($m_params->cfg_mod))
    $cfg_mod = (array)$m_params->cfg_mod;
else
    $cfg_mod = $main_module->get_cfg_mod();

if ($_REQUEST['fn'] == 'exe_inserisci_attav'){

	$codice= $m_params->form_values->f_codice;
	
	$descr= $m_params->form_values->f_descr;
	
	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'ATTAV';
	$ar_ins['TAKEY1'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);
	$ar_ins['TARIF1'] 	= $m_params->form_values->f_rif;
	
	
	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();
	

	$sql = "INSERT INTO {$cfg_mod['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_inserisci_rilav'){

	$codice= $m_params->form_values->f_codice;

	$descr= $m_params->form_values->f_descr;
	
	$attav= $m_params->attav;
	
	$ar_ins = array();
	$ar_ins['TADT'] 	= $id_ditta_default;
	$ar_ins['TATAID'] 	= 'RILAV';
	$ar_ins['TAKEY1'] 	= $attav;
	$ar_ins['TAKEY2'] 	= $codice;
	$ar_ins['TADESC'] 	= utf8_decode($descr);
	
	$ar_ins['TAUSGE'] 	= $auth->get_user();
	$ar_ins['TADTGE']   = oggi_AS_date();
	$ar_ins['TAORGE'] 	= oggi_AS_time();

	$sql = "INSERT INTO {$cfg_mod['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_attav'){

	$mod_rows= $m_params->mod_rows;

	foreach ($mod_rows as $v){

		if(isset($v)){
			
			$ar_ins = array();
		
			$ar_ins['TAKEY1'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TAPESO'] 	= $v->seq;
			$ar_ins['TARIF1'] 	= $v->rif;
			$ar_ins['TAFG03'] 	= $v->f_rif_todo;
			$ar_ins['TAFG01'] 	= $v->f_ut;
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();

			$sql = "UPDATE {$cfg_mod['file_tabelle']} TA
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE RRN(TA) = '$v->rrn' ";
			
			
		
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
			
			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);
			
			//exit;

		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'exe_modifica_rilav'){

	$mod_rows= $m_params->mod_rows;
	
	foreach ($mod_rows as $v){

		if(isset($v)){
			
			$ar_ins = array();
			
			$ar_ins['TAKEY2'] 	= $v->codice;
			$ar_ins['TADESC'] 	= utf8_decode($v->descr);
			$ar_ins['TAFG01'] 	= $v->write_ri;
			$ar_ins['TAFG02'] 	= $v->note_obbl;
			$ar_ins['TACOGE'] 	= $v->prog;
			$ar_ins['TAMAIL'] 	= $v->stati_ammessi;
			$ar_ins['TATELE']   = $v->msg_ri;
			$ar_ins['TAPESO'] 	= $v->seq;
			
			$ar_ins['TARIF2'] 	= $v->TARIF2;
			$ar_ins['TARIF1'] 	= $v->TARIF1;
			$ar_ins['TASTAL'] 	= $v->TASTAL;
			$ar_ins['TAINDI'] 	= $v->TAINDI;
			$ar_ins['TAPROV'] 	= $v->TAPROV;
				
			$ar_ins['TAUSGE'] 	= $auth->get_user();
			$ar_ins['TADTGE']   = oggi_AS_date();
			$ar_ins['TAORGE'] 	= oggi_AS_time();
			
		
			$sql = "UPDATE {$cfg_mod['file_tabelle']} TA
					SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
					WHERE RRN(TA) = '$v->rrn' ";
			
			
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, $ar_ins);
			echo db2_stmt_errormsg($stmt);
			
				
			$ret = array();
			$ret['success'] = true;
			echo acs_je($ret);
			
		}

	}

	exit;
}

if ($_REQUEST['fn'] == 'get_data_grid'){

	$sql_where = '';
	$sql_where .= sql_where_by_combo_value('TA.TARIF1', $m_params->sezione);
	
	$sql = "SELECT RRN (TA) AS RRN, TADESC, TAKEY1, TAPESO, TAFG01, TAFG03, TARIF1
	        FROM {$cfg_mod['file_tabelle']} TA
			WHERE TADT='$id_ditta_default' AND TATAID='ATTAV'
            {$sql_where} ";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {
		
		$row['codice'] = acs_u8e(trim($row['TAKEY1']));
		$row['descr'] = acs_u8e(trim($row['TADESC']));
		$row['f_ut'] = acs_u8e(trim($row['TAFG01']));
		$row['f_rif_todo'] = acs_u8e(trim($row['TAFG03']));
		$row['rif'] = acs_u8e(trim($row['TARIF1']));
		$row['seq'] = (int)$row['TAPESO'];
		$row['rrn'] = acs_u8e(trim($row['RRN']));


		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}


if ($_REQUEST['fn'] == 'get_data_grid_rilav'){

    $attav= $m_params->attav;

	$sql = "SELECT RRN(TA) AS RRN, TA.*
	        FROM {$cfg_mod['file_tabelle']} TA
			WHERE TADT = '{$id_ditta_default}' AND TATAID='RILAV' AND TAKEY1='{$attav}'";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);

	$data = array();

	while ($row = db2_fetch_assoc($stmt)) {

	    $row = array_map('acs_u8e', $row);
	    $row = array_map('rtrim', $row);
	    
		$row['codice']    = $row['TAKEY2'];
		$row['descr']     = $row['TADESC'];
		$row['write_ri']  = $row['TAFG01'];
		$row['note_obbl'] = $row['TAFG02'];
		$row['msg_ri']    = $row['TATELE'];
		$row['prog']      = $row['TACOGE'];
		$row['stati_ammessi'] = $row['TAMAIL'];
		$row['rrn']       = $row['RRN'];
		$row['seq']       = (int)$row['TAPESO'];
		
		$data[] = $row;
	}

	echo acs_je($data);

	exit();

}
 

 

if ($_REQUEST['fn'] == 'open_grid'){

?>

				
{"success":true, "items": [

        {
        
        xtype: 'grid',
        title: 'To Do Entry',
        <?php echo make_tab_closable(); ?>,
		multiSelect: true,
		plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
		store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							        extraParams: <?php echo acs_je($m_params) ?>,
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'seq', 'codice', 'descr', 'rrn', 'f_ut', 'rif', 'f_rif_todo'
		        			]
		    			},
		    		
			        columns: [
    			        { //tasto dx on mobile
        		            xtype:'actioncolumn', width:30,
        		            text: '<img src=<?php echo img_path("icone/24x24/button_grey_play.png") ?> width=20>',
        		            tdCls: 'tdAction', tooltip: 'Opzioni disponibili',
        		            menuDisabled: true, sortable: false,
        		            
        		            items: [{
        		                icon: <?php echo img_path("icone/24x24/button_grey_play.png") ?>,
        		                handler: function(view, rowIndex, colIndex, item, e, record, row) {
        		                	if (view.getSelectionModel().getSelection().length == 0)
        		                		view.getSelectionModel().select([record]);
        		                	view.fireEvent('itemcontextmenu', view, record, null, colIndex, e);		                	
        		                }
        		            }]
        		          },
			       		 {
			                header   : 'Seq',
			                dataIndex: 'seq', 
			                width     : 100,
			                editor: {
			                	xtype: 'textfield',
			                	allowBlank: true
			           	  	}
			             },			        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 100,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },  { 
			                header   : 'Riferimento',
			                dataIndex: 'rif', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			             },{ 
			                header   : 'TAFG01',
			                dataIndex: 'f_ut',
			                editor: {
			                 xtype: 'textfield',
			                 allowBlank: true
			           		},  
			                tooltip : '<b>C</b>: ref. cliente (TDSTOE) <br><b>E</b>: ref.cliente + ref.ordine (TDUSIO)<br> <b>U</b>: utente corrente<br> blank : ref.ordine (TDUSIO)',
			                renderer: function (value, metaData, record, row, col, store, gridView){						
							
							if (record.get('f_ut') != ''){
    							if (record.get('f_ut') == 'C'){
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('ref. cliente') + '"';
    	    		 	        }else if(record.get('f_ut') == 'E'){
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('rec.cliente + ref. ordine') + '"';
    	    		 	        }else if(record.get('f_ut') == 'U'){
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('Utente corrente') + '"';
    	    		 	        }else{
    	    		 	         metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode('ref. ordine') + '"';
    	    		 	        }	    		 	        
	    		 	      }
	    		 	        return value;		    
						}
			             }
			             
			             ,{ 
			                header   : 'TAFG03',
			                dataIndex: 'f_rif_todo',
			                tooltip : 'Riferimento precodificato <br> <b>C</b>: chiuso (tabella TODRF) <br><b>A</b>: Aperto',
			                editor: {
			                 xtype: 'textfield',
			                 allowBlank: true
			           		}    
						}
			             
			            
			         ],  
					
						listeners: {
						
						
						itemcontextmenu : function(grid, rec, node, index, event) {	
							
								event.stopEvent();
											      
			      			  var voci_menu = [];
				
				
			           voci_menu.push({
				         		text: 'Causali di rilascio',
				        		iconCls : 'icon-folder_search-16',          		
				        		handler: function() {
				        			acs_show_win_std('Causali di rilascio [' + rec.get('codice') + '] ' + rec.get('descr'), '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_rilav', {attav: rec.get('codice'), cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>});          		
				        		}
				    		});
				
				  
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					       }).showAt(event.xy);	
								
							
					
					}
					
					}, 
					
					 viewConfig: {
			        getRowClass: function(record, index) {
			           ret =record.get('liv');
			           
			    
			           return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
						{ xtype: 'hiddenfield', name: 'f_rif', value: <?php echo acs_je($m_params->sezione)?>},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
					            console.log(m_grid);
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_attav',
											        method     : 'POST',
								        			jsonData: {
								        			    cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>,
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												      m_grid.getStore().load();
												 
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
					   ]
					}, '->' ,
			     
			     {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_attav',
									        method     : 'POST',
						        			jsonData: {
						        				mod_rows : list_rows_modified,
						        			    cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php 

exit; 

}

if ($_REQUEST['fn'] == 'open_rilav'){

	$attav= $m_params->attav;

?>

{"success":true,
	m_win: {
		width: 1200, height: 500
	}, 
	"items": [
        {
        
        xtype: 'grid',
        multiSelect: true,
        plugins: [
		          Ext.create('Ext.grid.plugin.CellEditing', {
		            clicksToEdit: 1,
		          })
		      ],
			store: {
						
						xtype: 'store',
						autoLoad:true,
					   					        
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_grid_rilav',
								type: 'ajax',
								 actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>,
										attav: '<?php echo $attav; ?>'
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
		        			fields: [
		            			'codice', 'descr', 'write_ri', 'note_obbl', 'prog', 'rrn', 'seq', 'msg_ri', 'stati_ammessi', 
		            			'TARIF2', 'TARIF1', 'TASTAL', 'TAINDI', 'TAPROV'
		        			]
		    			},
		    		
			        columns: [
						{
			                header   : 'Seq',
			                dataIndex: 'seq', 
			                width     : 70,
			                editor: {
			                	xtype: 'textfield',
			                	allowBlank: true
			           	  	}
			             },			        
			       		 {
			                header   : 'Codice',
			                dataIndex: 'codice', 
			                width     : 70,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			           
			             },
			             { 
			                header   : 'Descrizione',
			                dataIndex: 'descr', 
			                flex    : 1,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             },
			             { 
			                header   : 'Note<br/>obbl.',
			                dataIndex: 'note_obbl', 
			                width: 50,
			                editor: {
 			                	xtype: 'textfield',
			                	allowBlank: true
			           		},			    
				         	renderer: function(value, p, record){
	    			    			if (value=='Y'){ 
	    			    			 return '<input type="checkbox" checked="true" />';
	    			    			 }else{
	    			    			  return '<input type="checkbox"/>';
	    			    			 }
	    			    			}
			             },			             
			             { 
			                header   : 'Messaggio RI<br/>personalizzato',
			                dataIndex: 'msg_ri', 
			                width: 120,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             },
			             { 
			                header   : 'Notifica<br/> su RI',
			                tooltip : 'Scrive in RI il messaggio RIL_ENTRY',
			                dataIndex: 'write_ri', 
			                width: 50,
			               editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 },
				         	renderer: function(value, p, record){
	    			    			if (record.get('write_ri')=='Y'){ 
	    			    			 return '<input type="checkbox" checked="true" />';
	    			    			 }else{
	    			    			  return '<input type="checkbox"/>';
	    			    			 }
	    			    			}	
			              
			             },
			             { 
			                header   : 'Call programma',
			                dataIndex: 'prog', 
			                width: 120,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }, { 
			                header   : 'Vincolo su stati',
			                tooltip: 'Elenco stati di partenza ammessi/esclusi, separati dal carattere \'|\' (opzionale) Il primo segmente indica Includi/Escludi. Es: I|AA|ME (Includi AA e ME)',
			                dataIndex: 'stati_ammessi', 
			                width: 120,
			                editor: {
			                xtype: 'textfield',
			                allowBlank: true
			           		 }
			              
			             }, { 
			                header   : 'Next',
			                tooltip: 'Prossima attivit&agrave; da aprire',
			                dataIndex: 'TARIF2', 
			                maxLength: 20,
			                width: 90,
			                editor: {
			                	xtype: 'textfield',
			                	allowBlank: true
			           		 }			              
			             }
			             
			             , { 
			                header   : 'E/A',
			                tooltip: 'Evasione (blank o E) o Avanzamento (A)',
			                dataIndex: 'TASTAL', 
			                maxLength: 20, width: 50,
			                editor: {xtype: 'textfield', allowBlank: true}			              
			             }
			             , { 
			                header   : 'Raggr.',
			                tooltip: 'Raggruppamento (per causali di Avanzamento)',
			                dataIndex: 'TARIF1', 
			                maxLength: 20, width: 90,
			                editor: {xtype: 'textfield', allowBlank: true}			              
			             }, { 
			                header   : 'Icona',
			                tooltip: 'Icona',
			                dataIndex: 'TAINDI', 
			                maxLength: 60, width: 120,
			                editor: {xtype: 'textfield', allowBlank: true}			              
			             }, { 
			                header   : 'G',
			                tooltip: 'Formato grafico (G = sfondo giallo, R = sfondo rosso, ...)',
			                dataIndex: 'TAPROV', 
			                maxLength: 2, width: 50,
			                editor: {xtype: 'textfield', allowBlank: true}			              
			             }
			            
			         ],  
					
		
					 viewConfig: {
			         getRowClass: function(record, index) {
			         return ret;																
			         }   
			    },
					    
				dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
              
                items: [
                
                {
		            xtype: 'form',
					frame: true,
					layout: 'hbox',
					items: [				
						{ xtype: 'textfield', name: 'f_codice', fieldLabel: 'Codice', width: '130', labelWidth: 40, maxLength: 10, margin: "0 10 0 0"},
						{ xtype: 'textfield', name: 'f_descr', fieldLabel: 'Descrizione', width: '300', labelWidth: 60, maxLength: 100},
				
						{
		                     xtype: 'button',
		                     text: 'Inserisci',
					            handler: function(a, b, c, d, e) {
								search_form = this.up('form');
								
					            m_grid = this.up('form').up('panel');
			 			
			 				             	Ext.Ajax.request({
											        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_inserisci_rilav',
											        method     : 'POST',
								        			jsonData: {
								        			    cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>,
								        				attav: '<?php echo $attav ?>',
														form_values: search_form.getValues()
													},							        
											        success : function(response, opts){
											       
												         m_grid.getStore().load();
								            		  
								            		},
											        failure    : function(result, request){
											            Ext.Msg.alert('Message', 'No data to be loaded');
											        }
											    });
					            
					            		
					            }
					     }
			     
			  
					   ]
					},  '->',
					   {
                     xtype: 'button',
               		 text: 'Conferma modifica',
		            iconCls: 'icon-blog_accept-32',
		            scale: 'large',	                     
		            handler: function() {
		            
		             var grid = this.up('grid');
			             
			            
			               rows_modified = grid.getStore().getUpdatedRecords();
                            list_rows_modified = [];
                            
                            for (var i=0; i<rows_modified .length; i++) {
				            list_rows_modified.push(rows_modified[i].data);}
			            
			             
			             	Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_rilav',
									        method     : 'POST',
						        			jsonData: {
						        			    cfg_mod : <?php echo acs_je($m_params->cfg_mod)?>,
						        			    attav: '<?php echo $attav ?>',
						        				mod_rows : list_rows_modified
						        			
											},							        
									        success : function(result, request){
						            			grid.getStore().load();	
						            		   
						            		},
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
		              
                 
			
			            }

			     }
					
					
					
					
					]
		   }]
         
	        																			  			
	            
        }  

]
}

<?php

exit;

}?>
