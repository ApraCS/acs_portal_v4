<?php
require_once "../../config.inc.php";

$s = new Spedizioni();

$dt		= trim($_REQUEST['tddt']);
$cli 	= trim($_REQUEST['tdccon']);
$des 	= trim($_REQUEST['tdcdes']);


function m_append_to_title($txt){
	return j(" [" . $txt . "]");
}







function insert_nota($p, $memo){
	global $conn;
	global $cfg_mod_Spedizioni;

	
	$where = '';
	$where_ar = array();
	
	if (strlen($p['tpno']) > 0){
		$sql_name_field[] .= 'NTTPNO';
		$sql_question_mark[] = '?';
		$sql_field_value[] = $p['tpno']; 
	}
	if (strlen($p['dt']) > 0){
		$sql_name_field[] .= 'NTDT';
		$sql_question_mark[] = '?';
		$sql_field_value[] = $p['dt'];
	}
	if (strlen($p['key1']) > 0){
		$sql_name_field[] .= 'NTKEY1';
		$sql_question_mark[] = '?';
		$sql_field_value[] = $p['key1'];
	}
	if (strlen($p['key2']) > 0){
		$sql_name_field[] .= 'NTKEY2';
		$sql_question_mark[] = '?';
		$sql_field_value[] = $p['key2'];
	}
	if (strlen($p['seq']) > 0){
		$sql_name_field[] .= 'NTSEQU';
		$sql_question_mark[] = '?';
		$sql_field_value[] = $p['seq'];
	}
	
	$sql_name_field[] .= 'NTMEMO';
	$sql_question_mark[] = '?';
	$sql_field_value[] = $memo;	
	
	$sql_name_field_sql = implode(', ', $sql_name_field);
	$sql_question_mark_sql = implode(', ', $sql_question_mark);
	
	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_note']}({$sql_name_field_sql}) VALUES({$sql_question_mark_sql})";
	
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $sql_field_value);

	return acs_je(array('success' => true));
	
}


function update_nota($p, $memo){
	global $conn;
	global $cfg_mod_Spedizioni;


	$where = '';
	$where_ar = array();



	$sql_name_field[] .= 'NTMEMO';
	$sql_question_mark[] = '?';
	$sql_field_value[] = $memo;
	
	$sql_name_field[] .= 'NTID';
	$sql_question_mark[] = '?';
	$sql_field_value[] = $p['id'];
	

	$sql_name_field_sql = implode(', ', $sql_name_field);
	$sql_question_mark_sql = implode(', ', $sql_question_mark);

	$sql = "UPDATE {$cfg_mod_Spedizioni['file_note']} SET NTMEMO=? WHERE NTID=?";

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $sql_field_value);

	return acs_je(array('success' => true));

}







function get_nota($p){
	global $conn;
	global $cfg_mod_Spedizioni;

	$where = '';
	$where_ar = array();
	
	if (strlen($p['id']) > 0){
		$where .= ' AND ID=?';
		$where_ar[] = $p['id'];
	}
	if (strlen($p['tpno']) > 0){
		$where .= ' AND NTTPNO=?';
		$where_ar[] = $p['tpno'];
	}
	if (strlen($p['dt']) > 0){
		$where .= ' AND NTDT=?';
		$where_ar[] = $p['dt'];
	}	
	if (strlen($p['key1']) > 0){
		$where .= ' AND NTKEY1=?';
		$where_ar[] = $p['key1'];
	}	
	if (strlen($p['key2']) > 0){
		$where .= ' AND NTKEY2=?';
		$where_ar[] = $p['key2'];
	}	
	if (strlen($p['seq']) > 0){
		$where .= ' AND NTSEQ=?';
		$where_ar[] = $p['seq'];
	}	
	
	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_note']} WHERE 1=1 {$where} ORDER BY NTSEQU";

	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $where_ar);
	$ret = array();
	while ($row = db2_fetch_assoc($stmt)) {
		$ret[] = $row;
	}
	
  return $ret;
}




// ******************************************************************************************
// UPDATE NOTA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'upd'){

	$m_params = acs_m_params_json_decode();
	
	$note = get_nota((array)$m_params->n_key);

	if (count($note) == 0){
		//inserimento nuova nota
		insert_nota((array)$m_params->n_key, $m_params->form_ep->txt_memo_cli);		
	}
	else if (count($note) == 1){
			//inserimento nuova nota
			update_nota(array('id' => $note[0]['NTID']), $m_params->form_ep->txt_memo_cli);
	}
		
	else $txt_note = 'ERRORE: RIGHE NOTE MULTIPLE';	

	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}
?>
<?php
// ******************************************************************************************
// VISUALIZZAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'view'){
	$txt_note = $s->get_note_cliente_portal2222($dt, $cli, '');

	
?>
{"success":true, "items": [

  {
   xtype: 'container',
	layout: {
	    type: 'vbox',
	    align: 'stretch',
	    pack : 'start'
	},
   items: [
   
 
	   {
            xtype: 'textareafield',
            grow: true,
            name: 'message',
            fieldLabel: 'Segnalazioni per cliente/destinazione',
            flex: 1,
            labelAlign: 'top', labelStyle: 'font-weight:bold;',
            value: <?php echo j($txt_note); ?>
        }
        
   
   ]
   
	, listeners: {		
 			afterrender: function (comp) {
 				//comp.up('window').setTitle(comp.up('window').title + <?php echo m_append_to_title(m_out_cli_des($dt, $cli, $des)); ?>);
 			}
 	}		 	   
   
  }


]}
<?php exit; } ?>
<?php
// ******************************************************************************************
// RECUPERO NOTA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_nota_txt'){
	$m_params = acs_m_params_json_decode();
	
	$note = get_nota((array)$m_params->n_key);
	if (count($note) == 0)
		echo acs_je(array('success' => true, 'txt' => ''));
	else		
		echo acs_je(array('success' => true, 'txt' => $note[0]['NTMEMO'])); 	
	exit;
}
?>
<?php
// ******************************************************************************************
// GESTIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'gest'){

	$m_params = acs_m_params_json_decode();
	
	$note = get_nota((array)$m_params->n_key);
	
	if (count($note) == 0)
		$txt_note = '';
	else if (count($note) == 1)
		$txt_note = $note[0]['NTMEMO'];
	else $txt_note = 'ERRORE: RIGHE NOTE MULTIPLE';	
		
?>
{"success":true, "items": [

  {
   xtype: 'form',
   frame: false,   
   layout: {
	    type: 'vbox',
	    align: 'stretch',
	    pack : 'start'
	},

   buttons: [
				{
		            text: 'Salva',
			        iconCls: 'icon-save-24',		            
			        scale: 'medium',		            
		            handler: function() {
		            	form  = this.up('form').getForm();
		            	m_win = this.up('window');
		            	
		            	
								Ext.Ajax.request({
								        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=upd',
								        jsonData: {n_key: <?php echo acs_je($m_params->n_key) ?>, 
								        		form_ep: form.getValues()
								        },
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){
								        		m_win.fireEvent('afterSave');
								            	m_win.close();			            
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });		            	
		            	 
		                
		            }
		        }    
   ],   	
   items: [
	   {
            xtype: 'textareafield',
            grow: true,
            name: 'txt_memo_cli',
            fieldLabel: 'Note',
            flex: 1,
            labelAlign: 'top',
            value: <?php echo j($txt_note); ?>
        }
   ]

	, listeners: {		
 			afterrender: function (comp) {
 				///comp.up('window').setTitle();
 			}
 	}   
   
  }


]}
<?php exit; } ?>