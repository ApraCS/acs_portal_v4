<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();


$initial_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario
$m_year = strftime("%G", strtotime($initial_data_txt));

$oggi = oggi_AS_date();

//parametri del modulo (legati al profilo)
$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

//elenco possibili utenti destinatari
$user_to = array();
$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$user_to[] = array(trim($u['UTCUTE']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTCUTE']) . ")");

$ar_users_json = acs_je($user_to);

//recupero l'elenco degli ordini interessati
$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

//attualmente e' selezione singola
$row_selected = $m_params->list_selected_id[0];

//recupero causale/utente/ordine da usare come chiave per AS0
$m_k_exp = explode("|", $row_selected->id);
 

if (strlen($m_params->causale) > 0)
 $m_causale = $m_params->causale;
else
 $m_causale		= $m_k_exp[0];

if ($m_causale == 'SBLOC')
	$m_utente		= $m_k_exp[1];
else
	$m_utente		= $m_k_exp[2];
		
	
$m_documento	= $row_selected->k_ordine;
$entry_prog		= $row_selected->prog;


//se passo una causale e uno documento, ricerco la prima attivita aperta
//usato per SBLOC da info
if ($m_params->from_k_ordine == 'Y' && strlen($m_params->causale) > 0){
	global $conn, $cfg_mod_Spedizioni;
	$sql = "SELECT * 
	
				FROM {$cfg_mod_Spedizioni['file_testate']} TD 
				INNER JOIN {$cfg_mod_Spedizioni['file_assegna_ord']} AO 
					ON TD.TDDOCU = AO.ASDOCU	
	
			WHERE (" . $main_module->get_where_std(null, array('filtra_su' => 'AS')) . ") 
			 AND ASFLRI <> 'Y' /* aperta */
			 AND ASCAAS = ? AND ASDOCU = ?";
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($m_params->causale, $m_params->k_ordine));	
	$row = db2_fetch_assoc($stmt);
	
	if ($row == null){
		?>
			{"success":true, "items": [
			        {
			            html: 'ERRORE: record in file ToDo non trovato'
			        }
			]}		
		<?php
		exit;
	}
	
	
	$m_documento	= $m_params->k_ordine;
	$entry_prog		= $row['ASIDPR'];
	$row = null;
	$ar_documenti[] = $m_documento;

	$n_list_selected_id = (object)array(
			'prog' => $entry_prog,
			'k_ordine' => $m_params->k_ordine
	);
	$m_params->list_selected_id = array($n_list_selected_id);
	$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));
	
} else {
	
	//elenco ordini che passo ad un eventuale create_entry
	foreach ($m_params->list_selected_id as $list_selected_ord){
		$ar_documenti[] = $list_selected_ord->k_ordine;
		
	}
	
}

 

global $conn, $cfg_mod_Spedizioni;
$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_assegna_ord']}
		 WHERE ASFLRI <> 'Y' /* aperta */
		   AND ASCAAS = ? AND ASUSAT = ? AND ASDOCU = ?";

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, array($m_causale, $m_utente, $m_documento));

$row = db2_fetch_assoc($stmt);


//causali di rilasio
$causali_rilascio = $s->find_TA_std('RILAV', $m_causale, 'N', 'Y');


//escludo eventualmente alcune causali di rilascio in base all'utente e ai permessi
foreach ($causali_rilascio as $key => $value){
	
	//In base agli stati dei documenti, eventualmente disabilito alcune risposte
	if (strlen(trim($value['TAMAIL'])) > 0){
	    
	    $exp_stati = explode('|', trim($value['TAMAIL']));
	    
	    if ($exp_stati[0] == 'E' || $exp_stati[0] == 'I'){
	        $includi_escludi_stati = $exp_stati[0];
	        array_shift($exp_stati);
	    } else {
	        $includi_escludi_stati = 'I'; //default
	    }
	    
		
		foreach($ar_documenti as $ord_to_test){
			$row_ord_to_test = $s->get_ordine_by_k_docu($ord_to_test);
						
			if ($includi_escludi_stati == 'I')
			  if (!in_array($row_ord_to_test['TDSTAT'], $exp_stati))
			    unset($causali_rilascio[$key]);
			
		   if ($includi_escludi_stati == 'E')
		     if (in_array($row_ord_to_test['TDSTAT'], $exp_stati))
		       unset($causali_rilascio[$key]);
			   
		}
	}
	
	//IMMOR - RIA
	//Riassegnazione referente order grafico, solo se non ancora graficato (TDFN13=1) o sono un amministratore modulo (UPFLMP, p_mod_param)	
	if (trim($value['TAKEY1']) == 'IMMOR' && trim($value['TAKEY2']) == 'RIA'){

		$sql_ord = "SELECT COUNT(*) AS T_ROW 
						FROM {$cfg_mod_Spedizioni['file_testate']} TD
						WHERE " . $s->get_where_std() . " AND TDDOCU IN (" . sql_t_IN($ar_documenti) . ")
						  AND TDFN13 = 0  
						";		
		$stmt_ord = db2_prepare($conn, $sql_ord);
 		$result = db2_execute($stmt_ord);
 		$row_ord = db2_fetch_assoc($stmt_ord);
 		
 		if ($row_ord['T_ROW'] > 0 || $js_parameters->p_mod_param != 'Y') //rimuovo la scelta
 			unset($causali_rilascio[$key]);
	}	
}




?>



{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            defaults:{ anchor: '-10' , labelWidth: 130 },
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'exe_avanzamento_segnalazione_arrivi'
                	}, {
	                	xtype: 'hidden',
	                	name: 'list_selected_id',
	                	value: '<?php echo $list_selected_id_encoded; ?>'
                	}, {
                		xtype: 'hidden',
	                	name: 'f_ril',
                		value : <?php echo j($row_selected->f_ril); ?>
                	}, {
						name: 'f_entry_prog_causale',
						itemId: 'f_entry_prog_causale',
						xtype: 'combo',
						fieldLabel: 'Causale di rilascio',
						displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
						forceSelection: true,
					   	allowBlank: false,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}, 'TARIF2', 'TAFG01', 'TAFG02'],
						    data: [								    
							     <?php echo acs_ar_to_select_json($causali_rilascio, ""); ?>	
							    ],
						},
						
				        listeners: {
				            change: function(field,newVal, a, b, c) {	            	

				            	var form = this.up('form').getForm();				            	
								var m_rec = field.findRecordByValue(newVal);								
								
								if (m_rec.get('TAFG02') == 'Y'){ //note obbligatorie
									form.findField('f_entry_prog_note').allowBlank = false;
								}
								else {
									form.findField('f_entry_prog_note').allowBlank = true;
								}
				            }
				        },						
						
												 
					}, {
						name: 'f_entry_prog_note',
						itemId: 'f_entry_prog_note',
						xtype: 'textfield',
						fieldLabel: 'Note',
					    maxLength: 100							
					},
				
					
					
<?php if ($m_params->causale == 'VERGAR'){ ?>		
					, {
						name: 'f_ord_orig_anno',
						xtype: 'textfield',
						fieldLabel: 'Anno ord. origine',
					    maxLength: 4,
					    allowBlank: false,
					    anchor: '-500',
					    value: '<?php echo $m_year ?>',
					     listeners: {
                		specialkey: function(field, e){
                    		if (e.getKey() == e.ENTER) {
                        		var salva = this.up('form').down('#b_salva')
                        		salva.focus();
                   				 }
               			 }
           			 }
					}, {
						name: 'f_ord_orig_numero',
						xtype: 'textfield',
						fieldLabel: 'Numero ord. origine',
					    maxLength: 6, width: 200,
					    allowBlank: false,
					    anchor: '-500',
					    listeners: {
                		specialkey: function(field, e){
                    		if (e.getKey() == e.ENTER) {
                        		var salva = this.up('form').down('#b_salva')
                        		salva.focus();
                   				 }
               			 }
           			 }					
					},{
							xtype: 'button',	
							text: 'Ricerca ordine',
							width: 200,
							anchor: '-500',
							//iconCls: 'icon-sub_blue_accept-32',
							//scale: 'large',	
							handler: function() {
							
							
							form = this.up('form').getForm();
							
							
							my_listeners = {
                                afterOkSave: function(from_win, tddocu){   
                                    //dopo che ha chiuso la maschera di assegnazione indice rottura
                                   
                                    anno = tddocu.split('_')[3];
                                    numero = tddocu.split('_')[4];
                                    form.findField('f_ord_orig_anno').setValue(anno);
                                    form.findField('f_ord_orig_numero').setValue(numero);
                                    from_win.close();                      

                                    }
                                };
		       
		                     acs_show_win_std('Ricerca ordini', 'acs_modifica_ordine_origine.php?fn=ricerca_ordini_grid', {list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>, cliente:<?php echo $m_params->cliente; ?>}, 700, 550, my_listeners, 'icon-shopping_cart_gray-16');	
							 }
							  
							}
					
						
<?php } ?>					
					
					
					
					],
			buttons: [{
	            text: 'Salva',
	            itemId: 'b_salva',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var window = this.up('window')	            	

					if(form.isValid()){					
					
							var m_f = form.findField('f_entry_prog_causale');
							var raw_causale_selected = m_f.findRecordByValue(m_f.getValue());
							
							var causale_successiva = raw_causale_selected.get('TARIF2');
							
							
							//Test
							if (causale_successiva == 'EXPO2aaaaa'){
									var mw = new Ext.Window({
									  width: 800
									, height: 380
									, minWidth: 300
									, minHeight: 300
									, plain: true
									, title: 'Avanzamento attivit&agrave; collegata'
									, iconCls: 'iconArtCritici'			
									, layout: 'fit'
									, border: true
									, closable: true
									, id_selected: window.id_selected								
									});				    			
					    			mw.show();							
							
										Ext.Ajax.request({
										        url        : 'acs_form_json_avanzamento_entry.php',
										        jsonData: {	
										        			causale: causale_successiva,
										        			list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>,
										        		  },
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										            var jsonData = Ext.decode(result.responseText);
										            mw.add(jsonData.items);
										            mw.doLayout();
										            
										            window.close();
										            				            
										        },
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });							
							  return;
							} //test
							
							
							
							
							
							if (causale_successiva.trim().length > 0) {
							//se richiesto apro la schermata per la segnalazione successiva
							
									var mw = new Ext.Window({
									  width: 800
									, height: 380
									, minWidth: 300
									, minHeight: 300
									, plain: true
									, title: 'Apertura attivit&agrave; collegata'
									, iconCls: 'iconArtCritici'			
									, layout: 'fit'
									, border: true
									, closable: true
									, id_selected: window.id_selected								
									});				    			
					    			mw.show();							
							
										Ext.Ajax.request({
										        url        : 'acs_form_json_create_entry.php',
										        jsonData: {	tipo_op: causale_successiva.trim(),
										        			entry_prog: <?php echo $entry_prog; ?>,
										        			entry_prog_causale: window.down('#f_entry_prog_causale').getValue(),
										        			entry_prog_note: window.down('#f_entry_prog_note').getValue(),
										        			list_selected_id_old: <?php echo acs_je($ar_documenti)?>,
										        			list_selected_id: <?php echo acs_je($m_params->list_selected_id); ?>,
										        			},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										            var jsonData = Ext.decode(result.responseText);
										            mw.add(jsonData.items);
										            mw.doLayout();
										            
										            window.close();
										            				            
										        },
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });
					
									} else { 
										//non e' richiesta la cusale successiva....salvo e chiudo
											
										Ext.getBody().mask('Loading... ', 'loading').show();											
												            	
						                form.submit({
					                            //waitMsg:'Loading...',
					                            success: function(form,action) {					                            
													Ext.getBody().unmask();
													
													//se � un OENTRY devo chiamare APRATOMETRON.EXE
													<?php if ($m_params->auto_set_causale == 'OENTRY'){ 
													    
													    $k_ordine       = $m_params->list_selected_id[0]->k_ordine;
													    $id_tree_record = $m_params->list_selected_id[0]->id;
													    $grid_id        = $m_params->grid_id;
													    ?>
											    	  	
											    	  	
											    	  	
											    	  	 acs_show_win_std('Apertura elaborazione grafica ordine', 
											    	  	 		'acs_elab_grafica_ordine.php', {
											    	  	 			k_ordine : <?php echo j($k_ordine); ?>,
											    	  	 			id_tree_record: <?php echo j($id_tree_record); ?>,
											    	  	 			grid_id: <?php echo j($grid_id); ?>
											    	  	 			}, 800, 500, {
											    	  	 				'close': function(){
											    	  	 				
											    	  	 					//quando chiudo la pagina di elaborazione grafica
											    	  	 					//voglio che venga eventualmente aggiornato lo stato
											    	  	 					//dell'ordine sul tree del ToDo
											    	  	 															    	  	 					
											    	  	 					var m_from_grid = Ext.getCmp(<?php echo j($grid_id); ?>);
											    	  	 					if (!Ext.isEmpty(m_from_grid)){											    	  	 						
											    	  	 						m_from_grid.panel.refresh_stato_per_record(<?php echo j($id_tree_record); ?>);
											    	  	 					}
											    	  	 				}
											    	  	 			}, 'icon-leaf-16')												
														
													<?php } ?>

																		                            
				                    				window.close();        	
					                            },
					                            failure: function(form,action){
					                            	Ext.getBody().unmask();
					                                Ext.MessageBox.alert('Errore', 'Errore imprevisto');		
					                            }
					                        });
				            		}
				    }            	                	                
	            }
	        }],             
			
			
			listeners: {
		        afterrender: function(){
		     	   f = this.getForm().findField('f_entry_prog_causale');
		     	   
		     	   //se imposto direttamente la causale di avanzamento
		     	   <?php if (strlen($m_params->auto_set_causale) > 0) { ?>
		     	    f.setValue(<?php echo j($m_params->auto_set_causale); ?>);
		     	    b_salva = this.down('#b_salva'); //ToDo: come essere sicuri che ci sia solo "Salva"?
		     	    b_salva.fireHandler();
		     	    return;
		     	   <?php } ?>		     	   

		     	   <?php if (count($causali_rilascio) == 1 && strlen(trim($causali_rilascio[0]['TARIF2'])) > 0){ ?>
		     	    f.setValue(<?php echo j(trim($causali_rilascio[0]['id'])); ?>);
		     	    b_salva = this.down('button'); //ToDo: come essere sicuri che ci sia solo "Salva"?
		     	    b_salva.fireHandler();
		     	    //this.up('window').close(); //va in errore     	    
		     	   <?php } ?>
		     	   
		        }
		       
		       
		    }			
			
				
				
        }
]}