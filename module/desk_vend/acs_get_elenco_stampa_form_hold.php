<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$initial_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario
$m_week = strftime("%V", strtotime($initial_data_txt)); 
$m_year = strftime("%G", strtotime($initial_data_txt));

$ar_area_spedizione = $s->get_options_area_spedizione();
$ar_area_spedizione_txt = '';

$ar_ar2 = array();
foreach ($ar_area_spedizione as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";

$ar_area_spedizione_txt = implode(',', $ar_ar2);

$da_form = acs_m_params_json_decode();
$m_params = acs_m_params_json_decode();

	$itin = new Itinerari;
	$itin->load_rec_data_by_k(array('TAKEY1' => trim($m_params->cod_iti)));	



// ******************************************************************************************
// ELENCO CLIENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_el_clienti'){
	
	$ar_clienti = $s->get_el_clienti_hold(trim($m_params->cod_iti), trim($m_params->k_cli_des));	
	echo acs_je($ar_clienti);
	
exit; }

// ******************************************************************************************
// ELENCO AGENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_el_agenti'){

	$ar_clienti = $s->get_el_agenti_hold(trim($m_params->cod_iti), trim($m_params->k_cli_des));
	echo acs_je($ar_clienti);

	exit; }

// ******************************************************************************************
// APERTURA WINDOW ELENCO CLIENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_elenco_clienti'){ ?>
{"success": true, "items": 
  [
	{
		xtype: 'gridpanel',
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_el_clienti',
							type: 'ajax',
					        
							 actionMethods: {
								read: 'POST'
							},
			
							reader: {
								type: 'json',
							}					        
					        
						, extraParams: <?php echo acs_raw_post_data(); ?>
						
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
						, doRequest: personalizza_extraParams_to_jsonData						
											        
						},
	        			fields: ['K_COD', 'TDCCON', 'TDDCON', 'TDCDES', 'TDLOCA', 'LOC_OUT']
	    			}),
	    			
		        columns: [{
			                header   : 'Codice',
			                dataIndex: 'TDCCON', 
			                width: 100, renderer: function (value, metaData, record, row, col, store, gridView){
			                	if (record.get('TDCDES').trim().length > 0)
			                		return value.trim() + ' - ' + record.get('TDCDES').trim();
			                	else
			                		return value.trim(); 
			                }
			             }, {
			                header   : 'Denominazione',
			                dataIndex: 'TDDCON', 
			                flex: 1.4
			             }, {
			                header   : 'Localit&agrave;',
			                dataIndex: 'LOC_OUT', 
			                flex: 1
			             }, {
				            xtype:'actioncolumn', 
				            header: 'Azioni',
		                	tdCls : 'add_action_spaces',				            
				            width:50,
		            		items: [{
		                		icon: <?php echo img_path("icone/16x16/print.png") ?>,

		                		margin: "0 0 0 30",
		                		tooltip: 'Visualizza report',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_cliente', rec.get('K_COD'), 'N');
		                		}
		                	  }, {
		                		icon: <?php echo img_path("icone/16x16/address_black.png") ?>,		                		
		                		tooltip: 'Visualizza e invia e-mail',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_cliente', rec.get('K_COD'), 'Y');
		                		}
		                	  }
		                	]
		                }
		              ]  	
	} 
   ]
 }  	
<?php exit; } ?>
<?php
// ******************************************************************************************
// APERTURA WINDOW ELENCO AGENTI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_elenco_agenti'){ ?>
{"success": true, "items": 
  [
	{
		xtype: 'gridpanel',
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_el_agenti',
							type: 'ajax',
					        
							 actionMethods: {
								read: 'POST'
							},
			
							reader: {
								type: 'json',
							}					        
					        
						, extraParams: <?php echo acs_raw_post_data(); ?>
						
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
						, doRequest: personalizza_extraParams_to_jsonData						
											        
						},
	        			fields: ['K_COD', 'TDCAG1', 'TDDAG1']
	    			}),
	    			
		        columns: [{
			                header   : 'Codice',
			                dataIndex: 'TDCAG1', 
			                width: 100
			             }, {
			                header   : 'Denominazione',
			                dataIndex: 'TDDAG1', 
			                flex: 1.4
			             }, {
				            xtype:'actioncolumn', 
				            header: 'Azioni',
		                	tdCls : 'add_action_spaces',				            
				            width:50,
		            		items: [{
		                		icon: <?php echo img_path("icone/16x16/print.png") ?>,
		                		tooltip: 'Visualizza report',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_agente', rec.get('K_COD'), 'N');
		                		}
		                	  }, {
		                		icon: <?php echo img_path("icone/16x16/address_black.png") ?>,
		                		tooltip: 'Visualizza e invia e-mail',
		                		handler: function(grid, rowIndex, colIndex) {
			  						var rec = grid.getStore().getAt(rowIndex);		 			  						               		
		                			m_win = this.up('window');		                			
		                			m_win.fireEvent('codSelected', 'per_agente', rec.get('K_COD'), 'Y');
		                		}
		                	  }
		                	]
		                }
		              ]  	
	} 
   ]
 }  	
<?php exit; } ?>


<?php
// ******************************************************************************************
// DEFAULT: APERTURA FORM PARAMETRI
// ******************************************************************************************
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
			buttons: [
			
				<?php if ($m_params->from != 'INFO'){ ?>
				
					{
			            text: 'Per cliente',
			            iconCls: 'icon-address_black-24',
			            scale: 'medium',			            
			            handler: function() {
			            
			            	var m_form = this.up('form');
			            
		                   	my_listeners = {
	        					codSelected: function(type, cod, autoEmail){	
	        						
						                m_form.submit({
					                        url: 'acs_print_wrapper.php',
					                        target: '_blank', 
					                        standardSubmit: true,
					                        method: 'POST',
					                        params: {
					                        	r_type: type,
					                        	r_cod_selected: cod,
					                        	r_auto_email: autoEmail
					                        },
					                  });
	        						
	        							        									            
					        		}
			    				};			            
			            
			            			            
				  			acs_show_win_std('Elenco clienti', 
				  			'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_elenco_clienti', 
				  			this.up('form').getValues(), 
				  			500, 450, my_listeners, 'icon-address_black-16');				  						            			                
			            }
			        } , 				
				
				<?php }?>

				<?php if ($m_params->from != 'INFO'){ ?>
				
					{
			            text: 'Per agente',
			            iconCls: 'icon-address_black-24',
			            scale: 'medium',			            
			            handler: function() {
			            
			            	var m_form = this.up('form');
			            
		                   	my_listeners = {
	        					codSelected: function(type, cod, autoEmail){	
	        						
						                m_form.submit({
					                        url: 'acs_print_wrapper.php',
					                        target: '_blank', 
					                        standardSubmit: true,
					                        method: 'POST',
					                        params: {
					                        	r_type: type,
					                        	r_cod_selected: cod,
					                        	r_auto_email: autoEmail
					                        },
					                  });
	        						
	        							        									            
					        		}
			    				};			            
			            
			            			            
				  			acs_show_win_std('Elenco agenti', 
				  			'<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_elenco_agenti', 
				  			this.up('form').getValues(), 
				  			500, 450, my_listeners, 'icon-address_black-16');				  						            			                
			            }
			        } , 				
				
				<?php }?>
				
				
				
				
			
				{
		            text: 'Visualizza',
			        iconCls: 'icon-folder_search-24',		            
			        scale: 'medium',		            
		            handler: function() {
		                this.up('form').submit({
	                        url: 'acs_print_wrapper.php',
	                        target: '_blank', 
	                        standardSubmit: true,
	                        method: 'POST'
	                  });
		                
		            }
		        } 
	        
	        
	        ],             
            
            items: [
            
				{
                	xtype: 'hidden',
                	name: 'tipo',
                	value: 'lista_consegne_programmate_hold'
                }	            
            
            
                , {
                	xtype: 'hidden',
                	name: 'cod_iti',
                	value: '<?php echo $da_form->cod_iti ?>'
                }, {
                	xtype: 'hidden',
                	name: 'k_cli_des',
                	value: '<?php echo $da_form->k_cli_des ?>'
                }, {                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 10',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa importi',
			                boxLabel: 'Si',
			                name: 'stampa_importi',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_importi',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			        }, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a radio button
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa pallet',
			                boxLabel: 'Si',
			                name: 'stampa_pallet',
			                inputValue: 'Y'
			            }, {			            	
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_pallet',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			        }, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a radio button
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Dettaglio ordini',
			                boxLabel: 'Si',
			                name: 'stampa_dettaglio_ordini',
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_dettaglio_ordini',
			                inputValue: 'N'
			            }]
			        }
			        
			        
			        ]			        
			        
			        
			        
			        
			        
			    }  
			    
			    
			    
			    
				,{                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 10',
			        items: [{
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Stampa telefono',
			                boxLabel: 'Si',
			                name: 'stampa_tel',
			                checked: true,			                
			                inputValue: 'Y'
			            }, {
			                boxLabel: 'No',			                
			                name: 'stampa_tel',
			                inputValue: 'N'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			      	}, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Data',
			                boxLabel: 'Progr.',
			                name: 'data_riferimento',
			                inputValue: 'data_programmata'
			            }, {
			                boxLabel: 'Sped.',
			                checked: true,			                
			                name: 'data_riferimento',
			                inputValue: 'data_spedizione'
			            }]
			        }, {
			            xtype: 'component',
			            width: 10
			      	}, {
			            xtype: 'fieldset',
			            flex: 1,
			            title: '',
			            defaultType: 'radio', // each item will be a checkbox
			            layout: 'anchor',
			            defaults: {
			                anchor: '100%',
			                hideEmptyLabel: false
			            },
			            items: [{
			                fieldLabel: 'Indirizzo',
			                boxLabel: 'Sped.',
			                name: 'indirizzo_di',
			                inputValue: 'ind_spedizione',
			                checked: true
			            }, {
			                boxLabel: 'Dest.',			                
			                name: 'indirizzo_di',
			                inputValue: 'ind_destinazione'
			            }]
			        }]			        
			        
			    }	
			    
			    
			    
			    
				, {                 
					xtype: 'container',
			        layout: 'hbox',
			        margin: '0 0 10',
			        items: [			    
			    
					    {
					            xtype: 'fieldset',
					            flex: 1,
					            title: '',
					            defaultType: 'radio', // each item will be a radio button
					            layout: 'hbox',
					            defaults: {
					                anchor: '100%',
					                hideEmptyLabel: false
					            },
					            items: [
					            
		<?php if ($m_params->from != 'INFO'){ ?>			            
					            , {
					                fieldLabel: '% maggiorazione volume',
					                width: 210,
					                labelWidth: 150,
					                boxLabel: 'Si',
					                xtype: 'numberfield',	
					                name: 'perc_magg_volume',
					                value: '<?php echo $itin->rec_data['TAPMIN'] ?>'
					            }
		<?php  } ?>		
			            
					          
					          
					, {
			            xtype: 'component',
			            width: 20
			        }, {
			                fieldLabel: 'Segnalazioni cliente/destinazione',
			                labelWidth: 190,
			                boxLabel: 'Si',
			                name: 'stampa_segnalazioni_cli_des',
			                inputValue: 'Y',
			                width: 250
			            }, {
			                boxLabel: 'No',
			                checked: true,			                
			                name: 'stampa_segnalazioni_cli_des',
			                inputValue: 'N',
			                width: 60,
			                labelWidth: 10
			            }				          
					          
					            
					            ]
					        }	
					        
					        
					        
					        
					        
					        
					        	    
			        
			    	]
			    },		{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							 margin: "10 10 10 10",	
							 anchor: '-5',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							anchor: '-5',
							margin: "20 10 10 10",
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							}
			    
			                       

                
            ]
        }

	
]}