<?php
require_once "../../config.inc.php";

$main_module = new DeskGest();
$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();



if ($_REQUEST['fn'] == 'get_json_data_grid'){
    
    $ar = array();
   
    
    $sql = "SELECT *
            FROM {$cfg_mod_Spedizioni['file_righe_ticket_no_std']} RD
            LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
		        ON RD.RDDT = TD.TDDT AND RD.RDTIDO=TD.TDOTID  AND RD.RDINUM=TD.TDOINU AND RD.RDAADO =TD.TDOADO AND digits(RD.RDNRDO) =TD.TDONDO
            WHERE RDDT = '{$id_ditta_default}' AND K00001 = '{$m_params->ticket}'";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt);
   
    while($row = db2_fetch_assoc($stmt)){
        
        $nr = array();
        $nr['anno']	        = print_date($row['TDOADO'], "%y");
        $nr['numero']	    = sprintf("%06s", trim($row['TDONDO']));
        $nr['riferimento']	= trim($row['TDVSRF']);
        $nr['ordine']	    = implode("_", array( $nr['anno'], trim($row['TDONDO']), trim($row['TDOTPD']) ));
        $nr['tipologia']	= trim($row['TDOTPD']);
        
        $nr['cliente']	    = acs_u8e(trim($row['TDDCON']));
        $nr['raggr']        = trim($row_td['TDCLOR']);
        $nr['data_eva']	    = trim($row['TDDTEP']);
        $nr['data_reg'] 	= trim($row['TDODRE']);
        $nr['stato']	    = trim($row['TDSTAT']);
        $nr['riga']	        = $row['RDRIGA'];
        $nr['RDDART'] 	    = trim($row['RDDART']);
        $nr['RDART']	    = trim($row['RDART']);
        $nr['RDQTA']	    = $row['RDQTA'];
        $nr['RDUM']	        = $row['RDUM'];
        $nr['RDNREC']	    = $row['RDNREC'];
        $nr['fl_art_manc']  = $s->get_fl_art_manc($row);
        $nr['fl_bloc']      = $s->get_ord_fl_bloc($row);
        
        $ar[] = $nr;
        
    }
    
    echo acs_je($ar);
    exit;
}

// ******************************************************************************************
// APERTURA WINDOW ELENCO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_elenco'){ ?>
{"success": true, 
   m_win: {
		title: 'Righe ordine collegate al ticket [' + <?php echo j($m_params->ticket)?> + ']',
		width: 1000, height: 450,
		iconCls: 'icon-folder_search-16'
	}, "items": 
  [
	{
		xtype: 'gridpanel',
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_grid',
							type: 'ajax',
					        
							 actionMethods: {
								read: 'POST'
							},
			
							reader: {
								type: 'json',
							}					        
					        
						, extraParams: <?php echo acs_raw_post_data(); ?>
						
						/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
						, doRequest: personalizza_extraParams_to_jsonData						
											        
						},
	        			fields: ['fl_bloc', 'fl_art_manc', 'raggr', 'ordine', 'anno', 'numero', 'riferimento', 'tipologia', 'stato', 'data_eva', 'data_reg', 'k_ordine', 'riga', 'RDNREC', 'RDDART', 'RDART', 'RDQTA', 'RDUM', 'cliente']
	    			}),
	    			
		        columns: [
		         {
	                header   : 'Nr ordine',
	                dataIndex: 'ordine',
	                width: 100,
	                filter: {type: 'string'}, 
	                filterable: true
	            }, 
	             {
	                header   : 'St.',
	                dataIndex: 'stato',
	                width: 30,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {text: '&nbsp;<br><img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini bloccati',	  
	    			    renderer: function(value, metaData, record){
	    			    
	    			    	if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_gray.png") ?> width=15>';
	    			    	if (record.get('fl_bloc')==2) return '<img src=<?php echo img_path("icone/48x48/power_blue.png") ?> width=15>';			    	
	    			    	if (record.get('fl_bloc')==3) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=15>';
	    			    	if (record.get('fl_bloc')==4) return '<img src=<?php echo img_path("icone/48x48/power_black_blue.png") ?> width=15>';    			    				    	
	    			    	}},
	    			    			
    	         {text: '&nbsp;<br><img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=18>',	width: 30, tdCls: 'tdAction', tooltip: 'Ordini MTO',  
    	    		dataIndex: 'fl_art_manc',
					renderer: function(value, p, record){
    			    	if (record.get('fl_art_manc')==20) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_red.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==5) return '<img src=<?php echo img_path("icone/48x48/info_blue.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==4) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_gray.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==3) return '<img src=<?php echo img_path("icone/48x48/shopping_cart.png") ?> width=15>';
    			    	if (record.get('fl_art_manc')==2) return '<img src=<?php echo img_path("icone/48x48/shopping_cart_green.png") ?> width=15>';			    	
    			    	if (record.get('fl_art_manc')==1) return '<img src=<?php echo img_path("icone/48x48/info_gray.png") ?> width=15>';			    	
    			    	}},
    			
	            {
	                header   : 'Data',
	                dataIndex: 'data_reg',
	                renderer: date_from_AS,
	                width: 60,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Evasione',
	                dataIndex: 'data_eva',
	                renderer: date_from_AS,
	                width: 70
	            },
	           
	            {
	                header   : 'Cliente',
	                dataIndex: 'cliente',
	                width: 150,
	                filter: {type: 'string'},
	                filterable: true
	            },
				{
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                flex:1,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Riga',
	                dataIndex: 'riga',
	                width: 40,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Articolo',
	                dataIndex: 'RDART',
	                filter: {type: 'string'}, 
	                filterable: true,
	                 width: 100
	            },
	            {
	                header   : 'Descrizione',
	                dataIndex: 'RDDART',
	                filter: {type: 'string'}, 
	                filterable: true,
	                flex : 1
	            },
	            {
	                header   : 'UM',
	                dataIndex: 'RDUM',
	                filter: {type: 'string'}, 
	                filterable: true,
	                 width: 40
	            },
	             {
	                header   : 'Q.t&agrave;',
	                dataIndex: 'RDQTA',
	                filter: {type: 'string'}, 
	                filterable: true,
	                width: 50,
	                renderer : floatRenderer2
	            }
	           
	            
		              ]  	
	} 
   ]
 }  	
<?php exit; } 



