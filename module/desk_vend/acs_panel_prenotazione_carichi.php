<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

$js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

function add_array_valori_liv(&$ar, $r){
	$ar['C_ROW']		+= 1;
	if ($r['TDHMIC'] > 0) $ar['C_ROW_CON_PRENOTAZIONE'] +=1;
	
	$ar["S_COLLI"] 		+= $r['S_COLLI'];
	$ar["S_VOLUME"] 	+= $r['S_VOLUME'];
	$ar["S_PALLET"] 	+= $r['S_PALLET'];
	$ar["S_PESO"] 	    += $r['S_PESO'];

	if ($ar['C_ROW_CON_PRENOTAZIONE'] > 0){
		if($ar['C_ROW_CON_PRENOTAZIONE'] == $ar['C_ROW'])
			$ar['CON_PRENOTAZIONE'] = 2;
		else
			$ar['CON_PRENOTAZIONE'] = 1;	
	}
}


// ******************************************************************************************
// MAIN TREE DATA
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	global $cfg_mod_Spedizioni;
	$m_params = acs_m_params_json_decode();
	
	//-----------------------------------------------------------------------------------------
	//assegno stabilimento sulle righe senza (in base a area di spedizione)
	$sql = "SELECT TD.TDSTAB, TA_ASPE.TAKEY1 AS C_AREA, TA_ASPE.TARIF2 AS C_STAB, COUNT(*) AS C_ROW  
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				ON TDDT = SP.CSDT AND TDNBOF = SP.CSPROG AND SP.CSCALE = '*SPR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
				ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE
			WHERE TD.TDSTAB = '' AND " . $s->get_where_std() . " AND TDSWSP='Y'
			GROUP BY TD.TDSTAB, TA_ASPE.TAKEY1, TA_ASPE.TARIF2		
			 ";				


	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt);
	
	while ($r = db2_fetch_assoc($stmt)) {
		if (strlen(trim($r['C_STAB'])) > 0){
			$sql_upd = "UPDATE {$cfg_mod_Spedizioni['file_testate']} TD
						SET TD.TDSTAB = " . sql_t($r['C_STAB']) . "
						WHERE " . $s->get_where_std() . " AND TDSWSP='Y'AND TD.TDSTAB = '' AND  TD.TDNBOF IN (
							SELECT SP.CSPROG
							FROM {$cfg_mod_Spedizioni['file_calendario']} SP
							LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
								ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI							
							WHERE TA_ITIN.TAASPE = " . sql_t($r['C_AREA']) . " 	
						) 
					";
			$stmt_upd = db2_prepare($conn, $sql_upd);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt_upd);	
			echo db2_stmt_errormsg($stmt_upd);
		}
	}
	//-----------------------------------------------------------------------------------------	
	

	
	
	$k_field = "TDDT, TDBCAR, TDDTIC, TDHMIC, TDDTFC, TDHMFC, TDDTSP, TA_ITIN.TADESC AS D_ITIN, TA_TRAS.TAKEY1 AS C_TRASP, TA_TRAS.TADESC AS D_TRASP, TDVETT, TDSTAB, CSPROG, CSVOLD, CSCITI, CSNSPC, SP.CSHMPG, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSCSTR, SP.CSCSTD, SP.CSDESC, TA_ASPE.TAKEY1 AS TAASPE, TDDTSP, SP.CSHMPG, SP.CSPORT, SP.CSDTSC, SP.CSHMSC ";
	$k_field_ord = "TA_TRAS.TADESC, TDDTSP, SP.CSHMPG, SP.CSDTSC, SP.CSHMSC, CSCITI, TDDTIC, TDHMIC";
	$k_field_grp = "TDDT, TDBCAR, TDDTIC, TDHMIC, TDDTFC, TDHMFC, TA_ITIN.TADESC, TA_TRAS.TAKEY1, TA_TRAS.TADESC, TDVETT, TDSTAB, CSPROG, CSVOLD, TDDTSP, CSCITI, CSNSPC, SP.CSHMPG, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSCSTR, SP.CSCSTD, SP.CSDESC, TA_ASPE.TAKEY1, TDDTSP, SP.CSHMPG, SP.CSPORT, SP.CSDTSC, SP.CSHMSC ";
	
	$s_field = $k_field;
	
	$sql = "SELECT $s_field
			, SUM(TDTOCO) AS S_COLLI, SUM(TDVOLU) AS S_VOLUME, SUM(TDBANC) AS S_PALLET, SUM(TDPLOR) AS S_PESO
			, SUM(TDPLOR) AS S_PESOL, SUM(TDPNET) AS S_PESON, SUM(TDTIMP) AS S_IMPORTO
			, SUM(TDCOSP) AS S_COLLI_SPUNTATI
			, COUNT(DISTINCT(CONCAT(TDIDES, CONCAT(TDDLOC, CONCAT(TDDCAP, TDNAZD))))) AS C_CLIENTI_DEST
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
			INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				ON TDDT = SP.CSDT AND TDNBOF = SP.CSPROG AND SP.CSCALE = '*SPR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
				ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
				ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE
				
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'		   
				
			WHERE " . $s->get_where_std() . " AND TDSWSP='Y'
		      AND SP.CSSTSP <> 'DP'					
			";
	
	
    	//parametri dalla form di ingresso
    	$form_ep = (object)$m_params;
    	
    	//data
    	if (strlen($form_ep->f_data_a) > 1)
    		$sql .= " AND TDDTSP >= " . $form_ep->f_data . " AND TDDTSP <= " . $form_ep->f_data_a;
    	else
    			$sql .= " AND TDDTSP = " . $form_ep->f_data;
    	
    	//area spedizione
    	$sql .= sql_where_by_combo_value('TA_ASPE.TAKEY1', $form_ep->f_area_spedizione);
    	
    	//tipologia trasporto (multipla)
    	$sql .= sql_where_by_combo_value('SP.CSTITR', $form_ep->f_trasporto);
    	
    	//divisione
        $sql .= sql_where_by_combo_value('TDCDIV', $form_ep->f_divisione);							
    	
    	//trasportatore
        $sql .= sql_where_by_combo_value('TA_TRAS.TAKEY1', $form_ep->f_trasportatore);
    	
    	//stabilimento
    	$sql .= sql_where_by_combo_value('TDSTAB', $form_ep->f_stabilimento);
    
        //filtri con extraParams in ingresso di tio "ep_"
    	foreach ($_REQUEST as $kv=>$v){
    	 if (substr($kv, 0, 3) == 'ep_' && strlen(trim($v)) > 0 ){
    		$sql .= " AND " . substr($kv, 3, 100) . " = " . sql_t($v);
    	 }
        }
    	
    	$sql .= " GROUP BY {$k_field_grp} ORDER BY $k_field_ord";

						$stmt = db2_prepare($conn, $sql);
						echo db2_stmt_errormsg();									
						$result = db2_execute($stmt);	
	
											
						//creo array
						$my_array = array();
						$n_children = "children";
						
						//CREAZIONE ARRAY AD ALBERO (Referente - Fornitore - Data - Articolo)
						while ($r = db2_fetch_assoc($stmt)) {
							$tmp_ar_id = array();
							
							
							$liv1_v = trim($r['C_TRASP']); //TRASPORTATORE
							$liv2_v = implode("_", 	array(	trim($r['CSCITI']), 
															$r['CSPROG'],
															$r['TDDTSP'],
															$r['CSHMPG']
										));  //ITINERARIO/SPEDIZIONE
							$liv3_v = implode("_", array(	trim($r['TDSTAB']),
															trim($r['TDBCAR']),
															$r['TDDTIC'],
															$r['TDHMIC'],
															$r['TDDTFC'],
															$r['TDHMFC']
										)); //STABILIMENTO
								
							
							//LIVELLO 0 --- TOTALE

							$liv0_v = 'TOTALE';
							$s_ar = &$my_array;
							$liv_c     = $liv0_v;
							if (is_null($s_ar[$liv_c])){
								$s_ar[$liv_c]["liv"]  = "liv_0";
								$s_ar[$liv_c]["id"]   = "TOT";
								$s_ar[$liv_c]["task"] = "Spedizioni del " . print_date($r['TDDTSP']);
								$s_ar[$liv_c]["expanded"]   = true;
							}
							add_array_valori_liv($s_ar[$liv_c], $r);
							$s_ar = &$s_ar[$liv_c][$n_children];							
															
							
							
							// LIVELLO 1 -- TRASPORTATORE
							/////$s_ar = &$my_array;
							$liv_c     = $liv1_v;
							$tmp_ar_id[] = 'liv1;' . $liv_c;
							if (is_null($s_ar[$liv_c])){
								$s_ar[$liv_c]["liv"]  = "liv_1";
								$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
								$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);
								$s_ar[$liv_c]["task"] = acs_u8e($r['D_TRASP']);
								$s_ar[$liv_c]["expanded"]   = true;
							}
							add_array_valori_liv($s_ar[$liv_c], $r);
							$s_ar = &$s_ar[$liv_c][$n_children];
							
							// LIVELLO 2 -- ITINERARIO/SPEDIZIONE
							$liv_c     = $liv2_v;
							$tmp_ar_id[] = 'liv2;' .$liv_c;
							if (is_null($s_ar[$liv_c])){
								$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);								
								$s_ar[$liv_c]["liv"]  = "liv_2";
								$s_ar[$liv_c]["iconCls"]  = "iconSpedizione";
								$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
								$s_ar[$liv_c]["sped_id"]  = $r['CSPROG'];
								$s_ar[$liv_c]["task"] = acs_u8e($r['D_ITIN']) . " [#{$r['CSPROG']}]";
								$s_ar[$liv_c]["TDDTSP"]  = $r['TDDTSP'];
								$s_ar[$liv_c]["CSHMPG"]  = $r['CSHMPG']; //data/ora spedizione
								$s_ar[$liv_c]["TDBCAR"]	 = $r['CSPORT'];
								$s_ar[$liv_c]["CSDTSC"]  = $r['CSDTSC'];
								$s_ar[$liv_c]["CSHMSC"]  = $r['CSHMSC'];	
								
								$s_ar[$liv_c]["data"]  = $r['TDDTSP'];
								$s_ar[$liv_c]["itinerario"]  = $r['CSCITI'];
								
								$s_ar[$liv_c]["vmc"]  = $s->decod_vmc_by_sped_row($r);
								
								$s_ar[$liv_c]["vettore"] = $s->decod_std('AUTR', trim($r['CSCVET']));
								$s_ar[$liv_c]["mezzo"]   = $s->decod_std('AUTO', trim($r['CSCAUT']));
								
								if ((int)$r['CSNSPC'] > 0)
								    $s_ar[$liv_c]["CSNSPC"]  = $r['CSNSPC'];			
								
								$s_ar[$liv_c]["TDDTIC"]   = $r['TDDTIC'];								
								$s_ar[$liv_c]["TDHMIC"]   = $r['TDHMIC'];
								$s_ar[$liv_c]['EL_CARICHI_D'] = $s->get_el_carichi_by_TDNBOF($r['CSPROG'], 'TN', 'N', 'N', null, 'N', 'N', 'N', 'Y', 'Y', 'Y');
								$s_ar[$liv_c]['EL_CARICHI_TOOLTIP'] = $s->get_el_carichi_by_TDNBOF_tooltip($r['CSPROG']);
								
								
								if(trim($s_ar[$liv_c]['EL_CARICHI_D']) == ''){
								    $s_ar[$liv_c]['carico'] = 'N';
								}else{
								    $s_ar[$liv_c]['carico'] = 'Y';
								} 
								
								//costruisco il tooltip in base alle attivita' utente sulla prenotazione carico
								$au = new AttivitaUtente();
								$stmt_au = $au->get_stmt_all_desc($main_module, 'PREN_SPED', $r['TDDT'], array($r['CSPROG']));
								$r_pren_carico = db2_fetch_assoc($stmt_au);
								if ($r_pren_carico != false)
									$s_ar[$liv_c]["CARICO_TOOLTIP"] = 'Prenotazione del ' . db2_ts_to_datetime($r_pren_carico['AUTSCR']);
								
							}
							add_array_valori_liv($s_ar[$liv_c], $r);
							$s_ar = &$s_ar[$liv_c][$n_children];							
							
							
							// LIVELLO 3 -- STABILIMENTO
							$liv_c     = $liv3_v;
							$tmp_ar_id[] = 'liv3;' .$liv_c;
							if (is_null($s_ar[$liv_c])){
								$s_ar[$liv_c]["id"]   = implode("|", $tmp_ar_id);								
								$s_ar[$liv_c]["liv"]  = "liv_3";
								$s_ar[$liv_c]["iconCls"]  = "icon-inbox-16";
								$s_ar[$liv_c]["liv_cod"]  = "{$liv_c}";
								$s_ar[$liv_c]["task"] = acs_u8e($s->decod_std('START', trim($r['TDSTAB'])));
								$s_ar[$liv_c]["sped_id"]  = $r['CSPROG'];
								$s_ar[$liv_c]["C_TRASP"]  = trim($r['C_TRASP']);
								$s_ar[$liv_c]["TDDTSP"]   = trim($r['TDDTSP']);
								$s_ar[$liv_c]["TDSTAB"]   = trim($r['TDSTAB']);
								$s_ar[$liv_c]["TDBCAR"]   = trim($r['TDBCAR']);
								$s_ar[$liv_c]["TDDTIC"]   = $r['TDDTIC'];								
								$s_ar[$liv_c]["TDHMIC"]   = $r['TDHMIC'];								
								$s_ar[$liv_c]["TDDTFC"]   = $r['TDDTFC'];
								$s_ar[$liv_c]["TDHMFC"]   = $r['TDHMFC'];
								$s_ar[$liv_c]["TDDTSP"]  = $r['TDDTSP'];
								$s_ar[$liv_c]["CSHMPG"]  = $r['CSHMPG'];
								
								$s_ar[$liv_c]['EL_CARICHI_D'] = $s->get_el_carichi_by_TDNBOF($r['CSPROG'], 'TN', 'N', 'N', trim($r['TDSTAB']), 'N', 'N', 'N', 'Y', 'Y', 'Y');
								$s_ar[$liv_c]['EL_CARICHI_TOOLTIP'] = $s->get_el_carichi_by_TDNBOF_tooltip($r['CSPROG']);
								
								if(trim($s_ar[$liv_c]['EL_CARICHI_D']) == ''){
								    $s_ar[$liv_c]['carico'] = 'N';
								}else{
								    $s_ar[$liv_c]['carico'] = 'Y';
								} 
								//costruisco il totoltip in base alle attivita' utente sulla prenotazione carico
								$au = new AttivitaUtente();
								$stmt_au = $au->get_stmt_all($main_module, 'PREN_CARICO', $r['TDDT'], array($r['CSPROG'], $r['C_TRASP'], $r['TDSTAB']));
								$r_pren_carico = db2_fetch_assoc($stmt_au);
								if ($r_pren_carico != false)
									$s_ar[$liv_c]["CARICO_TOOLTIP"] = 'Prenotazione del ' . db2_ts_to_datetime($r_pren_carico['AUTSCR']);
								
								$s_ar[$liv_c]["leaf"] = true;
							}
							add_array_valori_liv($s_ar[$liv_c], $r);
							$s_ar = &$s_ar[$liv_c];							
							
							
							
						} //while


		//se sto aggiornando un singolo nodo
		if (isset($_REQUEST['node']) && $_REQUEST['node'] != 'root'){
			$my_array = &$my_array['TOTALE'][$n_children];
			$livs = explode("|", $_REQUEST['node']);
			foreach($livs as $liv){
				if ($_REQUEST['node'] != 'TOT'){
					$k_liv = explode(";", $liv);
					$my_array = &$my_array[$k_liv[1]][$n_children];
				}
			}
		}						
						


		if (count($my_array) == 0){
			$liv0_v = 'TOTALE';
			$s_ar = &$my_array;
			$liv_c     = $liv0_v;
			if (is_null($s_ar[$liv_c])){
				$s_ar[$liv_c]["liv"]  = "liv_0";
				$s_ar[$liv_c]["id"]   = "TOT";
				$s_ar[$liv_c]["task"] = "Spedizioni del " . print_date($m_params->f_data);
				$s_ar[$liv_c][$n_children]['empty'] = array(
						"liv" => "liv_1",
						"id"  => "liv_0_empty",
						"task"=> " - nessuna spedizione ",
						"leaf"=> true
				);
				$s_ar[$liv_c]["expanded"]   = true;
			}
			add_array_valori_liv($s_ar[$liv_c], $r);
			$s_ar = &$s_ar[$liv_c][$n_children];				
		}		
		
		
		
		foreach($my_array as $kar => $r)
			$ar_ret[] = array_values_recursive($my_array[$kar]);
		


		
		echo acs_je($ar_ret);	
	exit;
}





// ******************************************************************************************
// MAIN TREE GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_grid'){
	$m_params = acs_m_params_json_decode();

	//se non viene passato imposto come data quella attuale
	if (strlen($m_params->f_data) == 0)
		$m_params->f_data = oggi_AS_date();
	
	?>

{"success":true, "items": [
	{
		xtype: 'treepanel',		
		id: 'panel-prenotazione_carichi',		
		title: 'Upload',
		cls: 'supply_desk',		
		<?php echo make_tab_closable(); ?>,
		selType: 'cellmodel',
        useArrows: true,
        rootVisible: false,
        
        tbar: new Ext.Toolbar({
            items:['<b>Elenco spedizioni programmate per data di spedizione/trasportatore</b>', '->'
           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
       		<?php echo make_tbar_closable() ?>            
            ]
        }),        
		
		viewConfig: {
		        getRowClass: function(record, index) {
		        	v = record.get('liv');
		            return v;					            
		         	}   
		    },			
		
		
		store: Ext.create('Ext.data.TreeStore', {
	        fields: ['task', 'liv', 'liv_cod', 'sped_id', 'vmc', 'vettore', 'mezzo', 'S_COLLI', 'S_PALLET', 'S_VOLUME', 'S_PESO',
	        		 'TDBCAR', 'TDDTIC', 'TDHMIC', 'TDDTFC', 'TDHMFC', 'TDSTAB', 'C_TRASP', 'TDDTSP', 'carico',
	        		 'TDDTSP', 'CSHMPG', 'CSDTSC', 'CSNSPC', 'CSHMSC', 'CON_PRENOTAZIONE', 'CARICO_TOOLTIP', 'EL_CARICHI_D', 'EL_CARICHI_TOOLTIP',
	        		 'data', 'itinerario'],
	        proxy: {
	            type: 'ajax',
	            method:'post',
	            actionMethods: 'POST',	            
	            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
	            
	            extraParams: <?php echo acs_je($m_params); ?>,
								
										
					/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */					
		            doRequest: function(operation, callback, scope) {
		                var writer  = this.getWriter(),
		                    request = this.buildRequest(operation, callback, scope);
		
		                if (operation.allowWrite()) {
		                    request = writer.write(request);
		                }
		
		                Ext.apply(request, {
		                    headers       : this.headers,
		                    timeout       : this.timeout,
		                    scope         : this,
		                    callback      : this.createRequestCallback(request, operation, callback, scope),
		                    method        : this.getMethod(request),
		                    disableCaching: false // explicitly set it to false, ServerProxy handles caching
		                });
		
						/* extraParams lo passo come jsonData */
		                request.jsonData = this.extraParams;
		
		                Ext.Ajax.request(request);
		                return request;
		            },	   								
										
										
								
	            
	        },
	        
	        
	        
//	        reader: new Ext.data.JsonReader({root: 'root'}),
	        
		      reader: {
		            type: 'json',
//		            root: 'root'
		        },	        
	        
	        
				writer: {
                        encodeRequest: true,
                        type: 'json'
                    },	        
	        
	        folderSort: false,
	        autoLoad:true,
	        
			root: {
			    pid: 'src',
			    text: 'test',
			    expanded: true
			},	        
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }		        
	        
	    }),	
	    
	 <?php
	    $ss = "&nbsp;&nbsp;&nbsp;";
	    $ss .= "<img id=\"cal_prenotazione_carichi_prev_1d\" src=" . img_path("icone/48x48/button_grey_first.png") . " height=30>";
	    $ss .= "<img id=\"cal_prenotazione_carichi_next_1d\" src=" . img_path("icone/48x48/button_grey_last.png") . " height=30>";

	    /*
	       if ($js_parameters->p_solo_inter == 'Y'){
			 //se non ha abilitato il menu, aggiungo l'icona per aprire i report
			 $ss .= "&nbsp;&nbsp;<img id=\"cal_prenotazione_carichi_open_report\" src=" . img_path("icone/48x48/print.png") . " height=30>";			
		   }
		*/
	  ?>
	    
	    
        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            columnId: 'task', 
            flex: 10,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: 'Trasportatore/Spedizione/Stabilimento <?php echo $ss;?>'
        }
        ,{
		   text: 'Sped.coll.',
		   width: 70,
		   dataIndex: 'CSNSPC'
		}
        
        
        ,{
		   text: 'Vettore',
		   flex: 3,
		   dataIndex: 'vettore'
		}
		
		,{
		   text: 'Mezzo',
		   flex: 3,
		   dataIndex: 'mezzo'
		}
		, {
		   text: 'Carichi',
		   width: 80,
		   dataIndex: 'EL_CARICHI_D',
				renderer: function(value, metaData, record){				    			    	
					metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('EL_CARICHI_TOOLTIP')) + '"';    			    	
    			 	return value;				    			    	 			    	
    			}
		},{
		   text: 'Colli',
		   width: 50, align: 'right',
		   dataIndex: 'S_COLLI', 
		   renderer: floatRenderer0
		},{
		   text: 'Pallet',
		   width: 50, align: 'right',
		   dataIndex: 'S_PALLET', 
		   renderer: floatRenderer0
		},{
		   text: 'Volume',
		   width: 60, align: 'right',
		   dataIndex: 'S_VOLUME', 
		   renderer: floatRenderer2
		},{
		   text: 'Peso',
		   width: 60, align: 'right',
		   dataIndex: 'S_PESO', 
		   renderer: floatRenderer0
		}
		
		,{
		   text: '<img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?> width=25>',
		   tooltip: 'Prenotazione carico per stabilimento', menuDisabled: true, sortable: true,
		   width: 30,
		   dataIndex: 'CON_PRENOTAZIONE',    	    	     
    			    renderer: function(value, p, record){
    			    	if (record.get('CON_PRENOTAZIONE')==1) return '<img src=<?php echo img_path("icone/48x48/star_none.png") ?> width=18>';
    			    	if (record.get('CON_PRENOTAZIONE')==2) return '<img src=<?php echo img_path("icone/48x48/star_full_yellow.png") ?> width=18>';			    	
    			    	}		   
		}		
		

		, { header: 'Carico', columns:[
			{
			   text: 'Data',
			   width: 65,
			   dataIndex: 'TDDTIC',
	  		   renderer: date_from_AS		   
			},	{
			   text: 'Ora',
			   width: 45, align: 'right',
			   dataIndex: 'TDHMIC',
	  		   renderer: time_from_AS		   
			}, {
			   text: 'Porta',
		   	   width: 110,
		   	   dataIndex: 'TDBCAR'
			}, {
			   text: 'i',
		   	   width: 32,
    			    renderer: function(value, metaData, record){
    			    	if ( (record.get('liv') == 'liv_3' && record.get('TDDTIC') > 0 ) ||
    			    		 (record.get('liv') == 'liv_2' && record.get('CSHMPG') > 0 )
    			    		){
							metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('CARICO_TOOLTIP')) + '"';    			    	
    			    	 	return '<img src=<?php echo img_path("icone/48x48/keyboard.png") ?> width=18>';
    			    	 }			    	
    			    	}		   	   
			}		
		]} 		
		
		
		

		
	
		
		, { header: 'Spedizione', columns:[
			{
			   text: 'Data',
			   width: 65,
			   dataIndex: 'TDDTSP',
	  		   
			   renderer: function (value, metaData, record, row, col, store, gridView){						
  				if (record.get('liv') == 'liv_2')
	  		   		return date_from_AS(value);  																		    
				}	  		   
	  		   		   
			},	{
			   text: 'Ora',
			   width: 45, align: 'right',
			   dataIndex: 'CSHMPG',
			   renderer: function (value, metaData, record, row, col, store, gridView){						
  				if (record.get('liv') == 'liv_2')
	  		   		return time_from_AS(value);  																		    
				}	  		   
	  		   		   
			}		
		]} 			
		

		, { header: 'Scarico', columns:[
			{
			   text: 'Data',
			   width: 65,
			   dataIndex: 'CSDTSC',
	  		   renderer: date_from_AS		   
			},	{
			   text: 'Ora',
			   width: 45, align: 'right',
			   dataIndex: 'CSHMSC',
	  		   renderer: time_from_AS		   
			}		
		]} 		
		
	  ]	    
	    	
		
		, listeners: {
		
		
				afterrender: function (comp) {
					
			        Ext.get('cal_prenotazione_carichi_prev_1d').on('click', function(){
						data_attuale = js_date_from_AS(comp.store.proxy.extraParams.f_data);
						nuova_data = new Date();
						nuova_data = Ext.Date.add (data_attuale, Ext.Date.DAY, -1);
						comp.store.proxy.extraParams.f_data = Ext.Date.format(nuova_data, 'Ymd');		        	
			        	comp.store.load();	
			        });			
			        Ext.get('cal_prenotazione_carichi_next_1d').on('click', function(){
						data_attuale = js_date_from_AS(comp.store.proxy.extraParams.f_data);
						nuova_data = new Date();
						nuova_data = Ext.Date.add (data_attuale, Ext.Date.DAY, 1);
						comp.store.proxy.extraParams.f_data = Ext.Date.format(nuova_data, 'Ymd');		        	
			        	comp.store.load();	
			        });		
			        
					if (Ext.get("cal_prenotazione_carichi_open_report") != null){
				        Ext.get('cal_prenotazione_carichi_open_report').on('click', function(){
								acs_show_win_std('Programma di carico trasportatori', 'acs_panel_prenotazione_carichi.php', null, 530, 220);
				        });					
					}			        
			        
			        if (Ext.get("cal_prenotazione_carichi_prev_1d") != null)	 Ext.QuickTips.register({target: "cal_prenotazione_carichi_prev_1d",	text: 'Giorno indietro'});        
			        if (Ext.get("cal_prenotazione_carichi_next_1d") != null)	 Ext.QuickTips.register({target: "cal_prenotazione_carichi_next_1d",	text: 'Giorno avanti'});			        	        
					if (Ext.get("cal_prenotazione_carichi_open_report") != null) Ext.QuickTips.register({target: "cal_prenotazione_carichi_open_report",text: 'Report programma carichi'});			        
			        		
				} //afterrender
		
              	, load: function(sender, node, records) {              		
                   	Ext.getBody().unmask();												
              	  }
              		
              		


				, itemcontextmenu : function(grid, rec, node, index, event) {
				  				  event.stopEvent();
							      var record = grid.getStore().getAt(index);		  
							      var voci_menu = [];
							  
							  if(record.get('carico') == 'Y'){    
					           voci_menu.push({	      	
                              		text: 'Avanzamento carico',
                            		iconCls : 'icon-gear-16',      		
                            		handler: function() {
                            				acs_show_win_std('Avanzamento carico', 'acs_form_json_carico.php?fn=open_carico', 
                    	    			  		{		
                    	    			  		 sped_id: rec.get('sped_id'),						    			  		 
                    	    			  		 from_sped: 'Y',
                    	    			  		 from_upload : 'Y'
                    	    			  		}, 500, 250, {				        			
            				        					'afterSave': function(from_win){
            					        					grid.getStore().load();			        					
            		 										from_win.close();	 											
            								        	}
                    							}, 'iconCarico');
                            		}
                        		  });
								}
							      
            <?php if ($js_parameters->p_solo_inter != 'Y'){ ?>
								      voci_menu.push({
						          		text: 'Aggiorna',
		        						iconCls : 'iconRefresh',	          		
		        						handler: function() {
		        						
							      		if (record.get('liv') == 'liv_0')		        						
		        							record.store.treeStore.load();
		        						else if (record.get('liv') == 'liv_3')
		        							record.store.treeStore.load({node: record.parentNode.parentNode});
		        						else
											record.store.treeStore.load({node: record});		        							 
		        							}
		        						});
			<?php } ?>
							      
							      
							      
							      if (record.get('liv') == 'liv_2'){
							      
            <?php if ($js_parameters->p_solo_inter != 'Y'){ ?>							      
									      voci_menu.push({
								      		text: 'Modifica spedizione',
								    		iconCls : 'iconSpedizione',
											handler: function() {
											
							                   	my_listeners = {
						        					acsaftersave: function(){						        														        									            
						        							grid.store.treeStore.load();
										        		}
								    				};												
											
								    			  	acs_show_win_std('Modifica spedizione', 'acs_form_json_spedizione.php', 
								    			  		{		
								    			  		 sped_id: record.get('sped_id'),						    			  		 
								    			  		 from_sped_id: 'Y'
								    			  		}, 600, 550, my_listeners, 'iconSpedizione');											
											
											
												}								    		
							      			});
							      			
							      			
							      			
									      voci_menu.push({
								      		text: 'Elenco scarichi',
								    		iconCls : 'icon-calendar-16',
											handler: function() {										
												mp = Ext.getCmp('m-panel');
												mp.add(
													show_el_ordini(grid, rec, node, index, event, 'acs_get_elenco_ordini_json.php?tipo_cal=per_sped&sped_id=' + rec.get('sped_id'), Ext.id())
								            	).show();											
											 }								    		
							      		  });
							      		  
			<?php } ?>							      		  
							      		  							      			
							      			
							      }

							      
							      
							      
		//Report carico per spedizione
			if (record.get('liv') == 'liv_2'){ //liv sped
			
	  		    voci_menu.push({
	          		text: 'Report scarichi',
	        		iconCls : 'iconPrint',          		
	        		handler: function() {
						
			    			  	acs_show_win_std('Parametri report scarichi', 'acs_get_elenco_stampa_form.php', 
			    			  		{		
			    			  		 rec_id: '|SPED|' + rec.get('sped_id'),						    			  		 
			    			  		 rec_liv: 'liv_0',
			    			  		 tipo_elenco: 'GATE'
			    			  		}, 600, 400, null, 'iconPrint');
														    			  													
	        		
	        		
	        		}
	    		});				
	    		
	    		
	    		
	    		voci_menu.push({
	          		text: 'Sequenza scarichi',
	        		iconCls : 'iconSpedizione',          		
	        		handler: function() {
						
			    			  	acs_show_win_std(null, 'acs_sequenza_scarichi.php?fn=open_tab', 
			    			  		{		
			    			  		 sped_id: rec.get('sped_id')
			    			  		});
														    			  													
	        		
	        		
	        		}
	    		});		
			

     <?php if ($js_parameters->p_solo_inter != 'Y'){ ?>							      			
      		  voci_menu.push({
             		text: 'Report carico',
            		iconCls : 'iconPrint',          		
            		handler: function() {
            			acs_show_win_std('Report carico', 'acs_report_carico.php?fn=get_json_form', {
            				rec_id: '|LIV0|' + rec.get('sped_id'), rec_liv: 'liv_0'
            			}, null, null, null, 'iconPrint');          		
            		}
        		});
     <?php } ?>     
     
           voci_menu.push({
    	        		text: 'Google maps',
    	       		iconCls : 'icon-gmaps_logo-16',          		
    	       		handler: function() {
    	       			sped_id	= rec.get('sped_id');
    	       			carico	= ''; //rec.get('k_carico');
    
    	       			data = rec.get('data');
    	       			itin = rec.get('itinerario');
    	       			gmapPopup('gmap_sped.php?only_view=Y&data=' + data + '&sped_car=' + sped_id + '|' + carico + '&itin_id=' + itin);
    	           		
    	       		} //handler google maps
    	   		});   		
							      			
			}
							      
							      
							      
							      
							      var menu = new Ext.menu.Menu({
							            items: voci_menu
								}).showAt(event.xy);								      
							      
					 }
              		
              		
              		
              		
              	, celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;		
					  	rec = iView.getRecord(iRowEl);
					  	
					  	
					  	// A LIVELLO DI SPEDIZIONE - PRENTAZIONE SPEDIZIONE
						if (rec.get('liv') == 'liv_2'){
					    	mp = Ext.getCmp('m-panel');
					    		Ext.Ajax.request({
					    		        url        : 'acs_panel_prenotazione_spedizioni_cal.php',
					    		        method     : 'POST',
					    		        jsonData: {
					    		        		c_stabilimento: rec.get('TDSTAB'),
					    		        		c_trasp: rec.get('C_TRASP'),
					    		        		data_spedizione: rec.get('TDDTSP'),
					    		        		form_ep: iView.store.treeStore.proxy.extraParams,
					    		        		sped_id_selected: rec.get('sped_id')
					    		        },
					    		        waitMsg    : 'Data loading',
					    		        success : function(result, request){
					    		            var jsonData = Ext.decode(result.responseText);
					    		            mp.add(jsonData.items);
					    		            mp.doLayout();
					    		            mp.show();
					    		        },
					    		        failure    : function(result, request){
					    		            Ext.Msg.alert('Message', 'No data to be loaded');
					    		        }
					    		    });
					    	return false;
						}
					  	
					  	
					  	
					  	
					  	// A LIVELLO DI STABILIMENTO - PRENTAZIONE CARICO
						if (rec.get('liv') == 'liv_3'){
						
						
							//gestione calendario						
					    	mp = Ext.getCmp('m-panel');
					    //	p_prenotazione_carichi_cal = Ext.getCmp('panel-prenotazione_carichi_cal');
					
					    //	if (p_prenotazione_carichi_cal){
					    //		console.log(p_prenotazione_carichi_cal);
					    //		p_prenotazione_carichi_cal.close();		    	
					    //	}
										    		
										    		
					    		Ext.Ajax.request({
					    		        url        : 'acs_panel_prenotazione_carichi_cal.php',
					    		        method     : 'POST',
					    		        jsonData: {
					    		        		c_stabilimento: rec.get('TDSTAB'),
					    		        		c_trasp: rec.get('C_TRASP'),
					    		        		data_spedizione: rec.get('TDDTSP'),
					    		        		data_carico: rec.get('TDDTIC'),
					    		        		form_ep: iView.store.treeStore.proxy.extraParams,
					    		        		sped_id_selected: rec.get('sped_id')
					    		        },
					    		        waitMsg    : 'Data loading',
					    		        success : function(result, request){
					    		            var jsonData = Ext.decode(result.responseText);
					    		            mp.add(jsonData.items);
					    		            mp.doLayout();
					    		            mp.show();
					    //		            p_prenotazione_carichi_cal = Ext.getCmp('panel-prenotazione_carichi_cal');
					    //		            p_prenotazione_carichi_cal.show();
					    		        },
					    		        failure    : function(result, request){
					    		            Ext.Msg.alert('Message', 'No data to be loaded');
					    		        }
					    		    });
					
						
						

						}		
					 }
			},		 
              		
         } //listeners
		
	}	

 ]}
 
<?php exit; } ?>


{"success":true, "items": [

        {
            xtype: 'panel',
            title: '',
            
            layout: 'fit',
			  items: [{
		            xtype: 'form',
		            id: 'm_form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: false,
		            title: '',
		            url: 'acs_op_exe.php',
		            
					buttons: [
					
					
					
					<?php
						//se non ho il menu, entro solo con una sotto visualizzazione
						if ($js_parameters->p_solo_inter != 'Y'){					 
							$f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "OPEN_UPLOAD");
						}	
					  ?>
					
					
			         <?php if ($js_parameters->p_solo_inter != 'Y' || $auth->get_tipologia() == 'TRASP'){?>										
					  {
			            text: 'Spedizioni',
			            iconCls: 'icon-folder_search-32',
			            scale: 'large',
			            handler: function() {
			            	var form = this.up('form').getForm();
			            	m_win = this.up('window');
			            	form_values = form.getValues();
			            	
			            	//in visualizzazione non tengo conto della data finale (usata solo nel report)
			            	form_values['f_data_a'] = null;
			            	 
			            	if(form.isValid()){
			            	
					        	mp = Ext.getCmp('m-panel');
					        	av_prenotazione_carichi = Ext.getCmp('panel-prenotazione_carichi');					        	
					
					        	if (av_prenotazione_carichi){
					        		av_prenotazione_carichi.show();		    	
					        	} else {
					
					        		//carico la form dal json ricevuto da php
					        		Ext.Ajax.request({
					        		        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_grid',
					        		        jsonData   : form_values,
					        		        method     : 'POST',
					        		        waitMsg    : 'Data loading',
					        		        success : function(result, request){
					        		            var jsonData = Ext.decode(result.responseText);
					        		            mp.add(jsonData.items);
					        		            mp.doLayout();
					        		            mp.show();
					        		            av_prenotazione_carichi = Ext.getCmp('panel-prenotazione_carichi').show();
//					        		            av_prenotazione_carichi.store.reload();
					        		            
					        		            m_win.close();        		            
					        		        },
					        		        failure    : function(result, request){
					        		            Ext.Msg.alert('Message', 'No data to be loaded');
					        		        }
					        		    });
					
					        	} 
			            	
			            	
								
							
			            	}
			            }
			         }
			         <?php } ?>
			         
			         
			         <?php if ($js_parameters->p_solo_inter != 'Y' || $auth->get_tipologia() == 'TRASP'){?>
			         , {
		                text: 'Spedizioni',
		                iconCls: 'icon-print-32',
		                scale: 'large',
		                disabled: false,
		                handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
		                        form.submit({
		                        		url: 'acs_report_prenotazione_carichi.php',
		                        		params: {form_ep: JSON.stringify(form.getValues())},
		                                standardSubmit: true,
                                     	submitEmptyText: false,
		                                method: 'POST',
		                                target : '_blank'
		                        });
		                    }
		                }
		            }
		            <?php } ?>
		            
		            
			       <?php if ($js_parameters->p_solo_inter != 'Y' || $auth->get_tipologia() == 'TERZ'){?>		            
		            , {
		                text: 'Carichi',
		                iconCls: 'icon-print-32',
		                scale: 'large',
		                disabled: false,
		                handler: function() {
		                    form = this.up('form').getForm();
		                    if (form.isValid()){
		                        form.submit({
		                        		url: 'acs_report_prenotazione_carichi_montatori.php',
		                        		params: {form_ep: JSON.stringify(form.getValues())},
		                                standardSubmit: true,
                                     	submitEmptyText: false,
		                                method: 'POST',
		                                target : '_blank'
		                        });
		                    }
		                }
		            }
		            <?php } ?>
		            
		            ],   		            
		            
		            items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "10 10 0 10"
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data finale <br>(solo per report)'
						   , name: 'f_data_a'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , margin: "10 10 0 10"
						}
						
						
			//se non ho il menu, entro solo con una sotto visualizzazione  						
			<?php if ($js_parameters->p_solo_inter != 'Y' || $auth->get_tipologia() == 'TRASP'){ ?>																		
						
						, {
							name: 'f_area_spedizione',
							xtype: 'combo',
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('ASPE'), ""); ?>	
								    ] 
							}						 
						}, {
							name: 'f_trasporto',
							xtype: 'combo',
							fieldLabel: 'Tipologia trasporto',
							displayField: 'text',
							valueField: 'id',
						   	allowBlank: true,							
							emptyText: '- seleziona -',
							multiSelect: true,
							forceSelection:true,
						    anchor: '-15',
						    margin: "20 10 10 10",
						    value: <?php echo j(trim($sped['CSTITR'])); ?>,							
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('TTRA'), ""); ?>	
								    ] 
							}						 
						  }
						  
						  
						, {
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}	

<?php 
 //se sono di tipo trasportatore, filtro il trasportatore in base al codice riservatezza dell'account
 if ($auth->get_tipologia() == 'TRASP'){
   $filtra_trasportatore = $auth->get_cod_riservatezza();
   $trasp_obj = new Trasportatori();
   $trasp_obj->load_rec_data_by_k(array('TAKEY1' => $auth->get_cod_riservatezza()));
  ?>  
  , {	xtype: 'hiddenfield'
		, name: 'f_trasportatore'
		, value: <?php echo j($auth->get_cod_riservatezza())?>
   }
  , {	xtype: 'displayfield'
		, value: <?php echo j($trasp_obj->rec_data['TADESC'] . ' [' . $auth->get_cod_riservatezza() . ']')?>
		, fieldLabel: 'Trasportatore'
		, anchor: '-15'
		, margin: "20 10 10 10"		
   }		
  
 <?php } else {?>
							
						, {
							name: 'f_trasportatore',
							xtype: 'combo',
							fieldLabel: 'Trasportatore',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('AVET'), ""); ?>		
								    ] 
								}						 
							}
 <?php } ?>							
						, {
							name: 'f_stabilimento',
							xtype: 'combo',
							fieldLabel: 'Stabilimento',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('START'), ""); ?>		
								    ] 
								}						 
							}								
							
			<?php } ?>																	  
						  
						  ]
		              }			  
			  
			  
			  ]
			   
		}
	]
}		
