<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// open CARICO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_carico'){ 

if ($m_params->from_carico == 'Y'){
	//ricevo anno/tipo/numero di carico
	$liv1_exp = explode("_", $m_params->carico);
	$tp_ca = $liv1_exp[1];
	$aa_ca = $liv1_exp[0];
	$nr_ca = $liv1_exp[2];
	
	$carico = $s->get_carico($tp_ca, $aa_ca, $nr_ca);
	
} else if ($m_params->from_sped == 'Y'){
	//ricevo numero spedizione: se ha un solo carico lo mostro, altrimenti mostro grid con carichi presenti
	
    if($m_params->from_upload == 'Y'){
        $el_carichi = $s->get_el_carichi_by_TDNBOF($m_params->sped_id, 'Y', 'N', 'N', null, 'N', 'Y', 'N',  'N', 'N', 'Y');
    }else{
        $el_carichi = $s->get_el_carichi_by_sped($m_params->sped_id, 'Y', 'N', 'N', null, 'N', 'Y');
    }
  

	if (count($el_carichi) == 1){					//un carico: apro in modifica
		$liv1_exp = explode("_", $el_carichi[0]);
		$tp_ca = $liv1_exp[1];
		$aa_ca = $liv1_exp[0];
		$nr_ca = $liv1_exp[2];
		
		$carico = $s->get_carico($tp_ca, $aa_ca, $nr_ca);
	} else {
		
		//Costruisco la grid per scegliere il carico della spedizione
		?>
		
		{"success":true, "items": [
		
		        {
		            xtype: 'form',
		            bodyStyle: 'padding: 10px',
		            bodyPadding: '5 5 0',
		            frame: true,
		            
		            items: [
		            	{
							xtype: 'displayfield',
							fieldLabel: '',
							value: 'Selezionare un carico da modificare',
					        forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15'
						},
						{
							name: 'f_carico',
							xtype: 'combo',
							multiSelect: false,
							fieldLabel: 'Carico',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "20 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: 								    
								     <?php 
								     	$out_carichi = array();
								     	foreach($el_carichi as $c)
								     		$out_carichi[] = array('id' => $c, 'text'=>$c);
								     	echo acs_je($out_carichi);
								     ?>
							}, //store
							listeners: {
					            change: function(field, newVal) {
					            	var loc_win = this.up('window');
					            	//carico la form per il carico selezionato
									Ext.Ajax.request({
									        url        : '<?php echo $_SERVER['PHP_SELF']; ?>',
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        jsonData: {from_carico: 'Y', 
									                   carico: newVal,
									                   from_upload : <?php echo j($m_params->from_upload); ?>},	        
									        success : function(result, request){
									            var jsonData = Ext.decode(result.responseText);
									            loc_win.removeAll(true);
									            loc_win.add(jsonData.items);
									            loc_win.doLayout();
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });						            	
					            }
					        }						 
						}		            
		            ]
		        }
		 ]
		 }				
		<?php
		exit;
	}
} else {
	//STD: da tree (modifica carico)
	$sottostringhe = explode("|", $m_params->rec_id);
	
	$liv1 = $sottostringhe[2];
	$liv1_exp = explode("___", $liv1);
	
	$tp_ca = $liv1_exp[2]; 
	$aa_ca = $liv1_exp[3];
	$nr_ca = $liv1_exp[4];
	
	$carico = $s->get_carico($tp_ca, $aa_ca, $nr_ca);
}

$stato = trim($carico['PSSTAT']);
$row_stcar = find_TA_std('STCAR', $stato);
$d_stato = trim($row_stcar[0]['text']);
$tree_id = $m_params->tree_id; //id (extjs) albero su cui ho fatto click


?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            items: [{
                	xtype: 'hidden',
                	name: 'fn',
                	value: 'upd_carico'
                	}, {
                	xtype: 'hidden',
                	name: 'dt',
                	value: '<?php echo $carico['PSDT'] ?>'
                	}, {
                	xtype: 'hidden',
                	name: 'tp_ca',
                	value: '<?php echo $carico['PSTPCA'] ?>'
                	}, {
                	xtype: 'hidden',
                	name: 'aa_ca',
                	value: '<?php echo $carico['PSAACA'] ?>'
                	}, {
                	xtype: 'hidden',
                	name: 'nr_ca',
                	value: '<?php echo $carico['PSNRCA'] ?>'
                	}, {
							xtype: 'displayfield',
							fieldLabel: 'Carico',
							value: <?php echo j(implode("_", array($tp_ca, $aa_ca, $nr_ca))) ?>,
					        forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    style: 'font-weight: bold !important',
						    fieldStyle: 'font-weight: bold;',
						},
						<?php if($m_params->from_upload != 'Y'){ ?>
						 {
							name: 'f_descrizione',
							xtype: 'textfield',
							fieldLabel: 'Descrizione',
                			maxLength: 100,  							
						    anchor: '-15',						    
						    value: <?php echo j(trim($carico['PSDESC'])); ?>,							
						  },
						  <?php }?>
						   {
								name: 'f_stato',
								xtype: 'combo',
								fieldLabel: 'Stato',
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
								forceSelection:true,
							    anchor: '-15',
							    value: <?php echo j(trim($carico['PSSTAT'])); ?>,
								store: {
									editable: false,
									autoDestroy: true,
									
								    fields: [{name:'id'}, {name:'text'}],
							    	data: [		
							    	
							    	<?php if($m_params->from_upload == 'Y'){ 
							    	    echo acs_ar_to_select_json($s->find_TA_std('STCAM', trim($carico['PSSTAT']), 'N', 'N', null, null, null, 'N', 'Y'), "");
							    	 }else{
							    	     echo acs_ar_to_select_json($s->find_TA_std('STCAR'), "");
							    	 }?>
							    	
								    ]
								},  listeners: {
						        	afterrender: function(comp){
						        		data = [
						        			{id: <?php echo j($stato)?>, text: <?php echo j("[".trim($stato). "] ".trim($d_stato)) ?>}
						        		];
										comp.store.loadData(data);
										comp.setValue(<?php echo j($stato)?>);
						        	}}						
						 }, {
							name: 'f_cod_est',
							xtype: 'textfield',
							fieldLabel: 'Codice/Rif. esterno',
                			maxLength: 30,  							
						    anchor: '-15',						    
						    value: <?php echo j(trim($carico['PSCOEX'])); ?>,							
						  }, {
							name: 'f_note',
							xtype: 'textfield',
							fieldLabel: 'Note',
                			maxLength: 100,  							
						    anchor: '-15',						    
						    value: <?php echo j(trim($carico['PSNOTE'])); ?>,							
						  }
						  
						  <?php if($m_params->from_upload != 'Y'){ ?>
						  , {
								name: 'f_paga',
								xtype: 'combo',
								fieldLabel: 'Pagamento',
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
								forceSelection:true,
							    anchor: '-15',
							    value: <?php echo j(trim($carico['PSFG01'])); ?>,
								store: {
									editable: false,
									autoDestroy: true,
									
								    fields: [{name:'id'}, {name:'text'}],
							    	data: [								    
								      <?php echo acs_ar_to_select_json($s->find_TA_std('SACAR'), ""); ?>	
								    ]
								}						
						 },{
							name: 'f_perc_pag',
							xtype: 'numberfield',
							fieldLabel: '% pagata',
                			anchor: '-15',
						    hideTrigger:true,					    
						    value: <?php echo j(trim($carico['PSPPAC'])); ?>,							
						  }, {
								name: 'f_seq_in_sped',
								xtype: 'combo',
								fieldLabel: 'Seq. in sped.',
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
								forceSelection:true,
							    anchor: '-15',
							    value: <?php echo j(trim($carico['PSFG02'])); ?>,
								store: {
									editable: false,
									autoDestroy: true,
									
								    fields: [{name:'id'}, {name:'text'}],
							    	data: [								    
								      {id: '0', text: '0'},
								      {id: '1', text: '1'},
								      {id: '2', text: '2'},
								      {id: '3', text: '3'},
								      {id: '4', text: '4'},
								      {id: '5', text: '5'},
								      {id: '6', text: '6'},
								      {id: '7', text: '7'},
								      {id: '8', text: '8'},
								      {id: '9', text: '9'}	
								    ]
								}						
						 },
						 
						     {
					xtype: 'fieldset',
	                title: 'Note amministrative carico',
	                layout: 'anchor',
					flex:1,
	                items: [
						
						<?php 
											
						$commento_txt = $s->get_commento_carico($carico, 'CAR');
						
						for($i=0; $i<= 4;$i++){ ?>
                	
                        	{
        						name: 'f_text_<?php echo $i ?>',
        						xtype: 'textfield',
        						fieldLabel: '',
        					    anchor: '-15',
        					    maxLength: 80,					    
        					   value: <?php echo j(trim($commento_txt[$i])); ?>,							
        					},	
					
					<?php }?>	   
 					
					 
	             ]}
					<?php }?>	 
				],
			buttons: [
			
			{
	            text: 'Rinomina carico',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_m_win = this.up('window');
	            	
	            	var my_listeners = {
    		  			afterConferma: function(from_win){
    							m_tree = Ext.getCmp('<?php echo $m_params->tree_id; ?>');
								m_store = m_tree.getStore();
								m_store.treeStore.load();
								loc_m_win.destroy(); 
								from_win.destroy(); 
			        		}
	    				};	
	            	
    	            		
					acs_show_win_std('Rinomina carico', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=rinomina_carico', {
    			       data_carico: <?php echo acs_je($liv1_exp); ?>,
    			       tree_id : <?php echo j($tree_id); ?>
    				}, 300, 150, my_listeners, 'icon-gear-16')	               
          	                	                
	            }
	        }, '->',
			{
	            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var loc_m_win = this.up('window');
	            	
	                form.submit({
		                            //waitMsg:'Loading...',
		                            success: function(form,action) {
		                            
						   				if (typeof(loc_m_win.initialConfig.listeners) !== 'undefined' &&
									   		typeof(loc_m_win.initialConfig.listeners.afterSave) !== 'undefined')
									    {
											loc_m_win.fireEvent('afterSave', loc_m_win);
									   		return false;						
										}		                            		                            
		                            
		                            	m_tree = Ext.getCmp('<?php echo $tree_id; ?>');
										m_store = m_tree.getStore();
										m_store.treeStore.load();
										loc_m_win.close();  		                            	
		                            },
		                            failure: function(form,action){
		                                Ext.MessageBox.alert('Erro');
		                            }
		                        });
          	                	                
	            }
	        }],             
				
        }
]}

<?php 
}

if ($_REQUEST['fn'] == 'exe_rinomina_carico'){ 
    
    $ret = array();
    
    $dt = $id_ditta_default;
    $data = $m_params->data_carico[0];
    $sped = $m_params->data_carico[1];
    $tp = trim($m_params->data_carico[2]);
    $aa = $m_params->data_carico[3];
    $nr = $m_params->form_values->f_numero;
    
    //verifico che il carico non esista
    $sql = "SELECT count(*) AS COUNT_T FROM {$cfg_mod_Spedizioni['file_carichi']}
            WHERE PSDT = " . sql_t($dt) . "
        				  AND PSTAID = " . sql_t("PRGS") . "
        				  AND PSTPCA = " . sql_t($tp) . "
        				  AND PSAACA = " . sql_t($aa) . "
        				  AND PSNRCA = " . sql_t($nr) . "
		 ";
    
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
    $row = db2_fetch_assoc($stmt); 
    
    if ($row['COUNT_T'] > 0 ){ //gia' esiste
        $ret['success'] = false;
        $ret['msg_error'] = 'Numero gi� esistente';
        echo acs_je($ret);
        exit;
    
    }else{
        
        //recupero l'elenco degli ordini da modificare
        $sql = "SELECT TDDOCU, TDSECA FROM {$cfg_mod_Spedizioni['file_testate']} TD
                WHERE " . $s->get_where_std() . "
				AND TDDTEP = " . $data . "
				AND TDNBOC = " . $sped . "
				AND TDTPCA = " . sql_t($tp) . "
				AND TDAACA = " . $aa . "
				AND TDNRCA = " . $m_params->data_carico[4] . "
			    ORDER BY TDSECA, TDSCOR, TDONDO
		        ";
       
        
        $stmt = db2_prepare($conn, $sql);
        $result = db2_execute($stmt);
        
        while ($row = db2_fetch_assoc($stmt)){
            
            $sh = new SpedHistory();
            $sh->crea(
                'pers',
                array(
                    "messaggio"	=> 'RNM_CAR',
                    "k_ordine"	=> $row['TDDOCU'],
                    "k_carico"	=> implode("_", array($aa, $tp, $nr)),
                    "vals" => array(
                        "RISECA" =>$row['TDSECA'])
                    
                )
                );
            
        }
        
       
        
        $ret['success'] = true;
        echo acs_je($ret);
        exit;
        
        
        
    }
    
}

// ******************************************************************************************
// RINOMINA CARICO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'rinomina_carico'){ ?>

{"success":true, "items": [

{
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            items: [
            {
			name: 'f_numero',
			xtype: 'textfield',
			fieldLabel: 'Nuovo nr carico',
		    anchor: '-15',
		    maxLength : 6							
		 }
            
            ],
            
            buttons: [
            {
	            text: 'Conferma',
	            handler: function() {
	                 var form = this.up('form').getForm();
	                 var loc_m_win = this.up('window');
	                 
	           		 Ext.Ajax.request({
					        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_rinomina_carico',
					        jsonData: {
					          data_carico : <?php echo j($m_params->data_carico); ?>,
					          form_values : form.getValues()
					        
					        },
					        method     : 'POST',
					        waitMsg    : 'Data loading',
					        success : function(result, request){	            	  													        
					            var jsonData = Ext.decode(result.responseText);
							    if(jsonData.success == false){
							        acs_show_msg_error(jsonData.msg_error);
    				        	    return; 
							   }else{
							 		loc_m_win.fireEvent('afterConferma', loc_m_win);
							   }
							 },
					        failure    : function(result, request){
					            Ext.Msg.alert('Message', 'No data to be loaded');
					        }
					    });	
	            
	            
	            }   }]
            
            
            }

]}

<?php     
}
    
    
