<!DOCTYPE html>
<?php
require_once("../../config.inc.php");
$s = new Spedizioni();
$start_point = $s->get_start_map('AR', null);
?>
<html>
<head>
<link rel="stylesheet" href="core.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo site_protocol();?>ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
 <?php require '../../templates/import_gmap_api.php'; ?>


</head>
<body>


<script type="text/javascript">

 var map;
 var address, latitude, longitude = '';
 var km_to_copy;

$(document).ready(function(){

	window.resizeTo(500,600);

<?php
 if (isset($_REQUEST['ind'])) echo "address = '" . strtr(utf8_decode($_REQUEST['ind']), array("'" => "\'")) . "' ;";
 if (isset($_REQUEST['lat'])) echo "latitude = '{$_REQUEST['lat']}' ;";
 if (isset($_REQUEST['lng'])) echo "longitude = '{$_REQUEST['lng']}' ;";
 
 if (isset($_REQUEST['km_field_id'])) echo "km_field_id = '{$_REQUEST['km_field_id']}-inputEl' ;";

?>
	
var geocoder = new google.maps.Geocoder();

if (latitude != '' && longitude != ''){

	map = new GMaps({
	    el: '#basic_map',
	    lat: latitude,
	    lng: longitude,
	    zoom: 10,
	    zoomControl : true,
	    zoomControlOpt: {
	        style : 'SMALL',
	        position: 'TOP_LEFT'
	    },
	    panControl : false
	  });

 	map.drawRoute({
 		  origin: [<?php echo $start_point['LAT']; ?>, <?php echo $start_point['LNG']; ?>],
 		  destination: [latitude, longitude],
 		  travelMode: 'driving',
 		  strokeColor: '#131540',
 		  strokeOpacity: 1,
 		  strokeWeight: 6
 		});
	
  return;	
}

//non ho passato lat e lng.... geolocalizzo
geocoder.geocode( { 'address': address}, function(results, status) {

if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();

    console.log(latitude);
    console.log(longitude);

 map = new GMaps({
    el: '#basic_map',
    lat: latitude,
    lng: longitude,
    zoom: 5,
    zoomControl : true,
    zoomControlOpt: {
        style : 'SMALL',
        position: 'TOP_LEFT'
    },
    panControl : false
  });

 	map.drawRoute({
	  origin: [<?php echo $start_point['LAT']; ?>, <?php echo $start_point['LNG']; ?>],
	  destination: [latitude, longitude],
	  travelMode: 'driving',
	  strokeColor: '#131540',
	  strokeOpacity: 1,
	  strokeWeight: 6
	});


 	//calcolo distanza in km
    map.getRoutes({
        origin: [<?php echo $start_point['LAT']; ?>, <?php echo $start_point['LNG']; ?>],
        destination: [latitude, longitude],
        callback: function (e) {
            var time = 0;
            var distance = 0;
           console.log(e.legs);
           
            for (var i=0; i<e[0].legs.length; i++) {
                time += e[0].legs[i].duration.value;
                distance += e[0].legs[i].distance.value;
            }

            var distance_div = document.getElementById('distance');
            var text = document.createTextNode("Distanza: " + (distance/1000).toFixed(0) + "Km");
            km_to_copy = (distance/1000).toFixed(0); 
            distance_div.appendChild(text);
        }
    }); 



	

    } 
}); 

});

function km_copy_to_opener() {
	km_field = window.opener.document.getElementById(km_field_id);
	km_field.value = km_to_copy;
	close();	
}


function close_return_to_opener() {
	close();	
}


</script>

<div id="basic_map" class="map"></div>
<br/>
<div id="distance"></div>
 <button onclick="km_copy_to_opener()" type="button">CHIUDI (Copia Km)</button> 
 &nbsp;&nbsp;
 <button onclick="close_return_to_opener()" type="button">Chiudi</button>

</body>
</html>
