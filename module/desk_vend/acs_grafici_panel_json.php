<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

ini_set('max_execution_time', 300);


// ******************************************************************************************
// DATI PER CHART2 (area o stab)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_chart2'){
	
	$form_values =	strtr($_REQUEST['form_values'], array('\"' => '"'));
 	$form_values = json_decode($form_values);
 	
 	//$memo_encoded = strtr(acs_je($m_params->memo), array("'" => "\'", '"' => '\"'));
 	
 	$filter_where = '';

 	if (isset($form_values->f_aspe) && strlen($form_values->f_aspe) > 0)
 		$filter_where .= " AND TA_ITIN.TAASPE = " . sql_t($form_values->f_aspe); 
 	
 	if (isset($form_values->f_tipo_ordine) && count($form_values->f_tipo_ordine) > 0)
 		$filter_where .= " AND TDOTPD IN (" . sql_t_IN($form_values->f_tipo_ordine) . ")";
 	
 	if (isset($form_values->f_stato_ordine) && count($form_values->f_stato_ordine) > 0)
 		$filter_where .= " AND TDSTAT IN( " . sql_t_IN($form_values->f_stato_ordine) . ")";

 	if (isset($form_values->f_divisione) && strlen($form_values->f_divisione) > 0)
 		$filter_where .= " AND TDCDIV = " . sql_t($form_values->f_divisione);

 	if (isset($form_values->f_tipologia_ordine) && count($form_values->f_tipologia_ordine) > 0)
 		$filter_where .= " AND TDCLOR IN(" . sql_t_IN($form_values->f_tipologia_ordine) . ")";

 	if (isset($form_values->tipo) && strlen($form_values->tipo) > 0 && $form_values->tipo != 'tutti'){
 		if ($form_values->tipo == 1) //solo
 			$filter_where .= " AND ( TDFN02 = " . sql_t($form_values->tipo) . " OR TDFN03 = " . sql_t($form_values->tipo) . ") ";
 		if ($form_values->tipo == 0) //esclusi
 			$filter_where .= " AND ( TDFN02 = " . sql_t($form_values->tipo) . " AND TDFN03 = " . sql_t($form_values->tipo) . ") "; 		
 	}

 	if (isset($form_values->f_agente) && strlen($form_values->f_agente) > 0 && $form_values->f_agente != 'tutti')
 		$filter_where .= " AND TDCAG1 = " . sql_t($form_values->f_agente);
 	
 	
 	switch ($_REQUEST['campo']){
 		case 'TDSTAB':
 		case 'TDCITI': 			
 		case 'TDCDIV':
 		case 'TDCAVE': 			 			
 		case 'TDVET1': 			
 			$campo_name = $_REQUEST['campo'];
 			$campo_des = $_REQUEST['campo']; 			
 			break;
 		case 'TDCAG1':
 			$campo_name = $_REQUEST['campo'];
 			$campo_des  = 'TDDAG1';
 			break;
 		default: 
 			$campo_name = 'TAASPE';
 			$campo_des = 'TAASPE'; 			
 			break;
 	}
 	

 	//programmati o hold?
 	if ($form_values->programmati_hold == 'HOLD')
 		$filter_where .= " AND TDSWSP = 'N' ";	//hold
 	else if ($form_values->programmati_hold == 'PROG')
 		$filter_where .= " AND TDSWSP = 'Y'  ";	//tutti
 	else
 		$filter_where .= " AND 1=1 ";	//programmati
 	
	$sql = "
			SELECT {$campo_name} AS TDASPE, {$campo_des} AS TDASPE_D, COUNT(*) AS NUM_ORD
			FROM {$cfg_mod_Spedizioni['file_testate']} TD
				INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI
			    
			WHERE " . $s->get_where_std() . " " . $filter_where . "
			GROUP BY {$campo_name}, {$campo_des}
			";
	
	
	$stmt = db2_prepare($conn, $sql);		
	$result = db2_execute($stmt);
	
	$ar_records = array();
	while ($r = db2_fetch_assoc($stmt)){
		
		//decodifico orea o stabilimento
		if ($_REQUEST['campo'] == 'TDSTAB')
			$r['TDASPE_D'] = $s->decod_std('START', $r['TDASPE']);
		else if ($_REQUEST['campo'] == 'TDCITI')
			$r['TDASPE_D'] = $s->decod_std('ITIN', $r['TDASPE']);
		else if ($_REQUEST['campo'] == 'TDVET1')
			$r['TDASPE_D'] = $s->decod_std('AVET', $r['TDASPE']);		
		else if ($_REQUEST['campo'] == 'TDCAG1')
			$r['TDASPE_D'] = $r['TDASPE_D'];		
		else if ($_REQUEST['campo'] == 'TDCDIV')
			$r['TDASPE_D'] = $s->decod_divisione_td($id_ditta_default, $r['TDASPE']);
		else if ($_REQUEST['campo'] == 'TDCAVE')
			$r['TDASPE_D'] = $s->decod_std('MERCA', $r['TDASPE']);		
		
		else
			$r['TDASPE_D'] = $s->decod_std('ASPE', $r['TDASPE']);
		
		$ar_records[] = $r;
	}
	
	$tot_ar_ret = 0;
	foreach($ar_records as $tot_ar)
		$tot_ar_ret += $tot_ar['NUM_ORD'];
		
	$ar_ret = array();
	foreach($ar_records as $ar1){
		//$ar1['NUM_ORD'] = round(($ar1['NUM_ORD'] * 100 / $tot_ar_ret),2);
		$ar_ret[] = $ar1;	
	}	
	
	 
	echo acs_je($ar_ret);
	
	exit;
}	






// ******************************************************************************************
// SE CAMBIA IL TIPO DI GRAFICO... ricarico i dati
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_store'){
	
	$m_params = acs_m_params_json_decode();	
	echo graph_dati_settimana_tipologi_area($m_params->programmati_hold);
	
	exit;
}

	


 function graph_dati_settimana_tipologi_area($type = 'ALL'){
 		global $s;
 		global $conn;
		global $cfg_mod_Spedizioni;
		global $id_ditta_default;
		$ar = array();
		
		$oggi = oggi_AS_date();
		
		$g_field = "TAASPE, TDCLOR, CSAARG, CSNRSE, TDSTAT, TDDSST, TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02";
		$g_field_2 = "TAASPE, TDCLOR, 0, 0";		
		$s_field = " COUNT(*) AS T_ROW ";
		$o_field = " CSAARG, CSNRSE, TDASPE ";

		$data_limite = date('Ymd', strtotime($oggi . " +4 months"));		
		
		if ($type == 'HOLD')
			$sql = "
			SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, -1 AS CSAARG, 0 AS CSNRSE,  0 AS MIN_TDDTEP,
			TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, $s_field
			FROM {$cfg_mod_Spedizioni['file_calendario']}
			INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD 
				ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "
			INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
			  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI						
			WHERE CSCALE = '*CS' AND CSDT = '$id_ditta_default'
					/* AND TDFN11 = 0 solo NON evasi */
					 AND ( (TDSWSP='N' AND TDDTEP < " . $oggi . ") ) /* scaduti */
					GROUP BY $g_field

			UNION
					
			SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, 99999 AS CSAARG, 0 AS CSNRSE,  0 AS MIN_TDDTEP,
			TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, 0 AS T_ROW
			FROM {$cfg_mod_Spedizioni['file_calendario']}
			INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
			  ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "
			INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
			  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI			 		
			WHERE CSCALE = '*CS' AND CSDT='$id_ditta_default'
					/* AND TDFN11 = 0 solo NON evasi */
					 AND ( (TDSWSP='N' AND TDDTEP < " . $oggi . ") ) /* scaduti */
					GROUP BY $g_field
					
					
				UNION
			
					SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, CSAARG AS CSAARG, CSNRSE AS CSNRSE, MIN(TDDTEP) AS MIN_TDDTEP,
					TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, $s_field
					FROM {$cfg_mod_Spedizioni['file_calendario']}
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
					 ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "
				    INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				     ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI					 		
					WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' AND TDSWSP='N' AND TDDTEP >= " . $oggi . " AND TDDTEP < " . $data_limite . "
					GROUP BY $g_field
			
				UNION
			
					SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, 9999 AS CSAARG, 0 AS CSNRSE, 0 AS MIN_TDDTEP,
					TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, $s_field
					FROM {$cfg_mod_Spedizioni['file_calendario']}
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
					 ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "
				    INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				     ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI					 		
					WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' AND TDSWSP='N' AND TDDTEP >= " . $data_limite . "
					GROUP BY $g_field
			
				ORDER BY $o_field
				";
		
		else { //PROGRAMMATI O GLOBALE
			
			$sql = '';
			
		
			if ($type == 'ALL')
				$sql .= "
						SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, -2 AS CSAARG, 0 AS CSNRSE, 0 AS MIN_TDDTEP,
						TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, $s_field
						FROM {$cfg_mod_Spedizioni['file_calendario']} 
							INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
						 ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "
						INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
						  ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI				 		  
						WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' 
						 AND ( (TDSWSP = 'N') ) 							/* hold */
						GROUP BY $g_field
						
						UNION
						";
			
		$sql .= "	

					SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, -1 AS CSAARG, 0 AS CSNRSE,  0 AS MIN_TDDTEP,
					TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, $s_field
					FROM {$cfg_mod_Spedizioni['file_calendario']} 
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
					 ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "  
				   INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				    ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI					 		
					WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' 
					 AND TDFN11 = 0 /* solo NON evasi */			
					 AND ( (TDSWSP='Y' AND TDDTEP < " . $oggi . ") ) /* scaduti */
					GROUP BY $g_field				
				
				UNION

					SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, CSAARG AS CSAARG, CSNRSE AS CSNRSE, MIN(TDDTEP) AS MIN_TDDTEP,
					TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, $s_field 
					FROM {$cfg_mod_Spedizioni['file_calendario']} 
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD 
						ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "
					INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  		ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI								  
					WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' AND TDSWSP='Y' AND TDDTEP >= " . $oggi . " AND TDDTEP < " . $data_limite . " 
					GROUP BY $g_field								
				
				UNION

					SELECT TAASPE AS TDASPE, TDCLOR AS TDCLOR, TDSTAT, TDDSST, 9999 AS CSAARG, 0 AS CSNRSE, 0 AS MIN_TDDTEP,
					TDCDIV, TDDDIV, TDCAG1, TDDAG1, TDOTPD, TDDOTD, TDFN03, TDFN02, $s_field 
					FROM {$cfg_mod_Spedizioni['file_calendario']} 
					INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
						 ON TDDTEP = CSDTRG AND " . $s->get_where_std() . "
					INNER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
				  		ON TD.TDDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = TD.TDCITI						 		  
					WHERE CSDT='$id_ditta_default' AND CSCALE = '*CS' AND TDSWSP='Y' AND TDDTEP >= " . $data_limite . " 
					GROUP BY $g_field
				
				ORDER BY $o_field												  
				";		
		} //PROGRAMMATI O GLOBAL
		
		//$sql .= " ORDER BY $o_field";		
		// echo $sql;
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt);
		
		$ar = array();
		$ar_c = array();
		
		$ar_records = array();
		while ($r = db2_fetch_assoc($stmt)){
			
			if ($r['CSAARG'] == -2) $r['CSAARG'] = 'Hold';
			if ($r['CSAARG'] == -1)  $r['CSAARG'] = "Scaduti [<" . date('d/m') . "]";			
			if ($r['CSAARG'] == 9999)  $r['CSAARG'] = 'Oltre';			
			if ($r['CSAARG'] == 99999)  $r['CSAARG'] = '-';  //per gli hold
			
			if ($r['CSNRSE'] == 0) $r['CSNRSE'] = null;
			
			$r['TDCLOR_DES'] = $s->decod_std('TIPOV', $r['TDCLOR']);
			$r['TDDOTD'] = trim($r['TDDOTD']);
			$r['TDCDIV'] = trim($r['TDCDIV']);
			$r['TDDDIV'] = trim($r['TDDDIV']);
			$r['TDDSST'] = trim($r['TDDSST']);
				
			$ar_records[] = $r;
		}
		return acs_je($ar_records);
 }

	
 
?>

{"success":true, "items": [

 {
  xtype: 'panel',
  layout: 'fit',
  items: 

	{
			xtype: 'panel',
			title: '', 
			layout: {
					type: 'table',
					columns: 2,
	                align : 'stretch',
	                pack  : 'start',
					tdAttrs: {
					        valign: 'top'
					    },				
					tableAttrs: {
					        style: {
					            width: '100%',
					            height: '100%'
					        }
					    }				    
					    
			},	
			layoutConfig: {
                align : 'stretch',
                pack  : 'start'
            },					
				
			
			listeners: {		
					afterrender: function (comp) {
						Ext.getBody().unmask();
					}
			},			
			
			items: [
			
						{
								xtype: 'chart',
								id: 'chartCmp',
								rowspan: 2,
								width: 800,
								height: 600,
								
								store_data: <?php echo graph_dati_settimana_tipologi_area() ?>,

								style:{
									background: '#fff',
                 					height:'100%'
             					},
								animate: true,
								
								legend: {
									position: 'bottom'
								},
								store: {
									fields: [],
									data: []
								},
								
								
								
								axes: [{
									type: 'Numeric',
									grid: true,
									position: 'left',
									fields: [],
									title: 'Numero di ordini',
									
									gridXXXXX: {
										odd: {
											opacity: 1,
											fill: '#ddd',
											stroke: '#bbb',
											'stroke-width': 1
										}
									},
									minimum: 0,
									adjustMinimumByMajorUnit: 0
								}, {
									type: 'Category',
									position: 'bottom',
									fields: [],
									title: 'Anno/Settimana',
									grid: true,
									
									label: {
									

							            renderer: function(v){
							            	year_w = v.split('-');
							            	if (year_w.length > 1 && year_w[1]>0){
							            	 //anno-settimana -> ci aggiungo il mese
							            	  DAY = 86400000;
							            	  year = year_w[0];
							            	  weekNum = year_w[1];
							            	  var year = new Date(year.toString()); // toString first so it parses correctly year numbers
											  var daysToFriday = (5 - year.getDay()); // Note that this can be also negative
											  var fridayOfFirstWeek = new Date(year.getTime() + daysToFriday * DAY);
											  var nthFriday = new Date(fridayOfFirstWeek.getTime() + (7 * (weekNum - 1) * DAY));
											  return v + ' [' + Ext.Date.format(nthFriday, 'M') + ']'
											  
							            	 return v;
							            	}
							                else return v;
							            },
  
							            
									            									
										rotate: {
											degrees: 90
										}
									}
								}],
								series: [{
									type: 'area',
									highlight: false,
									axis: 'left',
									xField: [],
									yField: [],
									style: {
										opacity: 0.93
									}
									
					               , tips: {
					                  trackMouse: true,
					                  width: 260,
					                  height: 38,
					                  renderer: function(storeItem, item) {
					                    //calculate percentage????
					                    this.setTitle(item.storeField + ': ' + storeItem.get(item.storeField));
					                  }
					                }									
									
									
								}]                        
				}
					 
					 

				
					 
					 
					 
					 
			,{			
				xtype: 'container',
				rowspan: 1,
				layout: 'hbox', width: 580,
				items: [

			
			

			//************************************************************			
			//accordion STABILIMENTI/ITINERARI
			//************************************************************			
			, {
				xtype: 'panel',
				layout: 'accordion',
				width: 220,
				margin: '0px 0px 10px 10px',
                items: [
			
			
			
			
			
			
			
			//************************************************************
			// TORTA: AREA DI SPEDIZIONE
			//************************************************************	
			{			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', width: 220,
				title: 'Aree di spedizione',
				items: [				
				
				{
					xtype: 'chart',
					id: 'chartCmp2',
					animate: true,
					shadow: true, 
					height: 237, flex: 1,				
					
					store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart2',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
							extraParams: {},
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['TDASPE','NUM_ORD', 'TDASPE_D']
									
					}, //store					
					
					
					
					
			            shadow: true,
			            /*
			            	legend: {
				            	field: 'TDASPE',
				                position: 'right'
				            },
				        */    
			            insetPadding: 25,
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NUM_ORD',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NUM_ORD'));
			                    });
			                    this.setTitle(storeItem.get('TDASPE') + ' - ' + storeItem.get('TDASPE_D') + ': ' + Math.round(storeItem.get('NUM_ORD') / total * 100) + '% [' + floatRenderer0(storeItem.get('NUM_ORD')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'TDASPE',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmp2'); // id of the chart
									var index = cmp.store.findExact('TDASPE', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.TDASPE; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
				} //chart
				
				]
				} //panel torta area di spedizione
				

			//************************************************************
			// TORTA: DIVISIONI
			//************************************************************	
			, {			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', flex: 1,
				title: 'Divisioni',
				items: [				
				
				{
					xtype: 'chart',
					id: 'chartCmpDivi',
					animate: true,
					shadow: true, 
					height: 237, flex: 1,					
					
					store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart2&campo=TDCDIV',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
							extraParams: {},
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['TDASPE','NUM_ORD', 'TDASPE_D']
									
					}, //store					
					
					
					
					
			            shadow: true,
			            /*
			            	legend: {
				            	field: 'TDASPE',
				                position: 'right'
				            },
				        */    
			            insetPadding: 25,
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NUM_ORD',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NUM_ORD'));
			                    });
			                    this.setTitle(storeItem.get('TDASPE') + ' - ' + storeItem.get('TDASPE_D') + ': ' + Math.round(storeItem.get('NUM_ORD') / total * 100) + '% [' + floatRenderer0(storeItem.get('NUM_ORD')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'TDASPE',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmpDivi'); // id of the chart
									var index = cmp.store.findExact('TDASPE', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.TDASPE; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
				} //chart
				
				]
				} //panel torta divisione
				
				
				

			//************************************************************
			// TORTA: AGENTI
			//************************************************************	
			, {			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', flex: 1,
				title: 'Agenti',
				items: [				
				
				{
					xtype: 'chart',
					id: 'chartCmpAge',
					animate: true,
					shadow: true, 
					height: 237, flex: 1,					
					
					store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart2&campo=TDCAG1',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
							extraParams: {},
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['TDASPE','NUM_ORD', 'TDASPE_D']
									
					}, //store					
					
					
					
					
			            shadow: true,
			            insetPadding: 25,
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NUM_ORD',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NUM_ORD'));
			                    });
			                    this.setTitle(storeItem.get('TDASPE') + ' - ' + storeItem.get('TDASPE_D') + ': ' + Math.round(storeItem.get('NUM_ORD') / total * 100) + '% [' + floatRenderer0(storeItem.get('NUM_ORD')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'TDASPE',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmpAge'); // id of the chart
									var index = cmp.store.findExact('TDASPE', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.TDASPE; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
				} //chart
				
				]
				} //panel torta agenti
								
				
				
				
				
			  ]
			}	
				

				
			//************************************************************			
			//accordion STABILIMENTI/ITINERARI
			//************************************************************			
			, {
				xtype: 'panel',
				layout: 'accordion',
				width: 220,
				margin: '0px 0px 20px 20px',
                items: [
			 
				
				
				
				
			//************************************************************
			// TORTA: STABILIMENTO
			//************************************************************	
			, {			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', flex: 1,
				title: 'Stabilimenti',
				items: [				
				
				{
					xtype: 'chart',
					id: 'chartCmpStab',
					animate: true,
					shadow: true, 
					height: 237, flex: 1,					
					
					store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart2&campo=TDSTAB',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
							extraParams: {},
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['TDASPE','NUM_ORD', 'TDASPE_D']
									
					}, //store					
					
					
					
					
			            shadow: true,
			            /*
			            	legend: {
				            	field: 'TDASPE',
				                position: 'right'
				            },
				        */    
			            insetPadding: 25,
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NUM_ORD',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NUM_ORD'));
			                    });
			                    this.setTitle(storeItem.get('TDASPE') + ' - ' + storeItem.get('TDASPE_D') + ': ' + Math.round(storeItem.get('NUM_ORD') / total * 100) + '% [' + floatRenderer0(storeItem.get('NUM_ORD')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'TDASPE',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmpStab'); // id of the chart
									var index = cmp.store.findExact('TDASPE', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.TDASPE; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
				} //chart
				
				]
				} //panel torta
				

				
				
				
			//************************************************************
			// TORTA: ITINERARI
			//************************************************************	
			, {			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', flex: 1,
				title: 'Itinerari',
				items: [				
				
				{
					xtype: 'chart',
					id: 'chartCmpItin',
					animate: true,
					shadow: true, 
					height: 237, flex: 1,					
					
					store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart2&campo=TDCITI',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
							extraParams: {},
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['TDASPE','NUM_ORD', 'TDASPE_D']
									
					}, //store					
					
					
					
					
			            shadow: true,
			            /*
			            	legend: {
				            	field: 'TDASPE',
				                position: 'right'
				            },
				        */    
			            insetPadding: 25,
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NUM_ORD',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NUM_ORD'));
			                    });
			                    this.setTitle(storeItem.get('TDASPE') + ' - ' + storeItem.get('TDASPE_D') + ': ' + Math.round(storeItem.get('NUM_ORD') / total * 100) + '% [' + floatRenderer0(storeItem.get('NUM_ORD')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'TDASPE',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmpItin'); // id of the chart
									var index = cmp.store.findExact('TDASPE', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.TDASPE; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
				} //chart
				
				]
				} //panel torta
				

				

				
			//************************************************************
			// TORTA: MERCATO
			//************************************************************	
			, {			
				xtype: 'panel',
				rowspan: 1,
				layout: 'fit', flex: 1,
				title: 'Mercato',
				items: [				
				
				{
					xtype: 'chart',
					id: 'chartCmpMerc',
					animate: true,
					shadow: true, 
					height: 237, flex: 1,					
					
					store: {
						xtype: 'store',
						autoLoad:true,	
						proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_chart2&campo=TDCAVE',
							method: 'POST',
							type: 'ajax',
				
							//Add these two properties
							actionMethods: {
								type: 'json',
								read: 'POST'
							},
				
							extraParams: {},
							
							reader: {
								type: 'json',
								method: 'POST',
								root: 'root'							
							},
							
						},
	
						fields: ['TDASPE','NUM_ORD', 'TDASPE_D']
									
					}, //store					
					
					
					
					
			            shadow: true,
			            /*
			            	legend: {
				            	field: 'TDASPE',
				                position: 'right'
				            },
				        */    
			            insetPadding: 25,
			            theme: 'Base:gradients',					
										

					series: [{
			                type: 'pie',
			                field: 'NUM_ORD',
			                showInLegend: true,
			                donut: true,
			                tips: {
			                  trackMouse: true,
			                  width: 260,
			                  height: 38,
			                  renderer: function(storeItem, item) {
			                    //calculate percentage.
			                    var total = 0;
			                    storeItem.store.each(function(rec) {
			                        total += parseFloat(rec.get('NUM_ORD'));
			                    });
			                    this.setTitle(storeItem.get('TDASPE') + ' - ' + storeItem.get('TDASPE_D') + ': ' + Math.round(storeItem.get('NUM_ORD') / total * 100) + '% [' + floatRenderer0(storeItem.get('NUM_ORD')) + ']');
			                  }
			                },
			                highlight: {
			                  segment: {
			                    margin: 20
			                  }
			                },
			                label: {
			                    field: 'TDASPE',
			                    display: 'rotate',
			                    contrast: true,
			                    font: '11px Arial',
								renderer: function (label){
									// this will change the text displayed on the pie
									var cmp = Ext.getCmp('chartCmpMerc'); // id of the chart
									var index = cmp.store.findExact('TDASPE', label); // the field containing the current label
									var data = cmp.store.getAt(index).data;
									return data.TDASPE; // the field containing the label to display on the chart
								}			                    
			                }
			            }]
				} //chart
				
				]
				} //panel torta
				
				
				
				
			
					
				] //accordio items
			}	
				
				
			
				
				
				]
			}
					 
					 
					 
					 
					 			
			
			
			, {
				xtype: 'form',
				bodyStyle: 'padding: 5px',
				frame: true,
				rowspan: 1,
				flex: 1,
				title: '',
				items: [
					{
						xtype: 'fieldset',
						title: '',
						flex: 1,
						layout: 'vbox',
						border: false,
						 items: [
						 
						{ 
						 xtype: 'fieldcontainer',
						 layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						 items: [						 
						 
						 	   {
								name: 'f_aspe',
								width: 250,
								xtype: 'combo',
								fieldLabel: 'Area di sped.',
								displayField: 'text',
								labelAlign:'left',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,								
								allowBlank: true,														
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								    
										 <?php echo acs_ar_to_select_json($s->find_TA_std('ASPE'), "", true, 'N', 'N', 'Y'); ?> 	
										] 
									}			 
								}, {
								name: 'f_divisione',
								xtype: 'combo',
								fieldLabel: 'Divisione',
								flex: 1,
								displayField: 'text',
								labelAlign:'right',
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,								
								allowBlank: true,														
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								    
										 <?php echo acs_ar_to_select_json($s->options_select_divisioni('Y'), '') ?> 	
										] 
									}			 
								}
								
							 ]
							} 	
							
							
							
							, { 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [							
								
							   {
								name: 'f_stato_ordine',
								xtype: 'combo',
								fieldLabel: 'Stato ordine',
								labelAlign: 'right',
								displayField: 'text',
								labelAlign:'left',
								width: 250,								
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
								multiSelect: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								    
										 <?php echo acs_ar_to_select_json($s->get_options_stati_ordine('Y'), '') ?> 	
										] 
									}					 
								},{
								name: 'f_tipo_ordine',
								xtype: 'combo',
								fieldLabel: 'Tipo ordine',
								flex: 1,
								displayField: 'text',
								labelAlign: 'right',								
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,														
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								    
										 <?php echo acs_ar_to_select_json($s->options_select_tipi_ordine('Y'), '') ?> 	
										] 
									}							 
								}
								
							   ]
							  }	
								

							  
							, { 
							 xtype: 'fieldcontainer',
							 layout: { 	type: 'hbox',
									    pack: 'start',
									    align: 'stretch'},						
							 items: [							
							  
								
							  {
								name: 'f_tipologia_ordine',
								xtype: 'combo',
								fieldLabel: 'Tipologia ordine',
								flex: 1,
								displayField: 'text',
								labelAlign:'left',
								width: 250,								
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: true,
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								     	
										 <?php echo acs_ar_to_select_json($s->find_TA_std('TIPOV'), "", true, 'N', 'N', 'Y'); ?>	
										] 
									}							 
								}, {
								name: 'f_agente',
								xtype: 'combo',
								fieldLabel: 'Agente',
								flex: 2,
								displayField: 'text',
								labelAlign:'right',
								flex: 1,								
								valueField: 'id',
								emptyText: '- seleziona -',
								forceSelection:true,
								allowBlank: true,
							  	multiSelect: false,														
								store: {
									autoLoad: true,
									editable: false,
									autoDestroy: true,	 
									fields: [{name:'id'}, {name:'text'}],
									data: [								     	
										 <?php echo acs_ar_to_select_json($s->get_options_agenti('Y'), '') ?> 		
										] 
									}							 
								}
							]
						   }		  
					 ]
					 },{
						xtype: 'fieldset',
						defaultType: 'textfield',
						border: false,  margin: "0 0 0 0", padding: "0 0 0 0",						
						layout: 'anchor',
						defaults: {
							anchor: '100%'
						},
					 	items: [{
							xtype: 'radiogroup',
							anchor: '100%',
							layout: 'hbox',
							flex: 1,
							fieldLabel: '',
	   						defaults: {
			                     width: 90
			                    },						
							items: [
							    {
									xtype: 'label' 
								  ,	width: 120  
								  , text: 'Raggruppamento:'							  
								},
								{
									xtype: 'radio' 
								  , name: 'tipo_grafico'  
								  , boxLabel: 'Tipologia'
								  , inputValue: 'tipologia_ordine'
								  , checked: true
								  
								},
								{
									xtype: 'radio' 
								  , name: 'tipo_grafico'
								  , boxLabel: 'Stato'
								  , inputValue: 'stato_ordine'                           													
								},
								{
									xtype: 'radio' 
								  , name: 'tipo_grafico'
								  , boxLabel: 'Tipo'
								  , inputValue: 'tipo_ordine'                           													
								}],
							listeners: {
							
									change: function (cb, value, oldvalue)
									{																
									}
							}
						   
						}
					 ]
					 }
					 
					 
					 
					 
					  ,{
						xtype: 'fieldset',
						defaultType: 'textfield',
						border: false,  margin: "0 0 0 0", padding: "0 0 0 0",
						layout: 'anchor',
						defaults: {
							anchor: '100%'
						},
					 	items: [{
							xtype: 'radiogroup',
							anchor: '100%',
							layout: 'hbox',
							flex: 1,
							fieldLabel: '',
	   						defaults: {
			                     width: 90
			                    },						
							items: [
							    {
									xtype: 'label' 
								  ,	width: 120  
								  , text: 'Ordini bloccati:'							  
								}, {
									xtype: 'radio'
								  , name: 'tipo'
								  , inputValue: 'tutti' 
								  , boxLabel: 'Inclusi'
								  , checked: true
								},
								{
									xtype: 'radio'
								  , name: 'tipo'                            
								  , inputValue: '0'                            
								  , boxLabel: 'Esclusi'
								},
								{
									xtype: 'radio'
								  , name: 'tipo'                            
								  , inputValue: '1'
								  , boxLabel: 'Solo'
								}],
							listeners: {
							
									change: function (cb, value, oldvalue)
									{							
									
										}
										}
						   
						}
					 ]
					 }
					 
					 
					 
					 
					,{
						xtype: 'fieldset',
						defaultType: 'textfield',
						border: false,  margin: "0 0 0 0", padding: "0 0 0 0",
						layout: 'anchor',
						defaults: {
							anchor: '100%'
						},
					 	items: [{
							xtype: 'radiogroup',
							anchor: '100%',
							layout: 'hbox',
							flex: 1,
							fieldLabel: '',
	   						defaults: {
			                     width: 90
			                    },						
							items: [
							    {
									xtype: 'label' 
								  ,	width: 120  
								  , text: 'Tipo grafico:'							  
								},{
									xtype: 'radio'
								  , name: 'programmati_hold'                            
								  , inputValue: 'ALL'                            
								  , boxLabel: 'Globale'
								  , checked: true								  
								}, {
									xtype: 'radio'
								  , name: 'programmati_hold'
								  , inputValue: 'PROG' 
								  , boxLabel: 'Programmati'
								},
								{
									xtype: 'radio'
								  , name: 'programmati_hold'                            
								  , inputValue: 'HOLD'                            
								  , boxLabel: 'Hold'
								}],
							listeners: {							
									change: function (cb, value, oldvalue){
											//se cambia il tipo di grafico (programmati o hold)
											//ricarico i dati
											Ext.getBody().mask('Dati in elaborazione... ', 'loading').show();
											Ext.Ajax.request({
										        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_store',
										        jsonData: value,
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        
													var jsonData = Ext.decode(result.responseText);										        
										        
										        	//aggiorno store_data
													gr = Ext.getCmp('chartCmp');													
													gr.store_data = jsonData;		
													
													Ext.getBody().unmask();							        
										        										        	
										        },
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });
									
										}
							}
						   
						}
					 ]
					 }					 
					 
					 
					 
					 
					 
					 
	
					
					],
					listeners: {		
							afterrender: function (comp) {
														
							form = comp.up('window').down('form').getForm();
							chart = this.up('window').down('chart');							
														
							array_filtri = form.getValues();							
							
							sto = raggruppa_dati(chart.store_data, ['CSAARG','CSNRSE'], ['TDCLOR_DES'], 'T_ROW', 'dati', array_filtri);							
							ser = raggruppa_dati(chart.store_data, ['CSAARG','CSNRSE'], ['TDCLOR_DES'], 'T_ROW', 'serie', array_filtri);
						
							chart.store = sto;
							chart.axes.items[0].fields = ser;							
							chart.axes.items[1].fields = ['CSAARG-CSNRSE'];

							chart.series.items[0].xField = ['CSAARG-CSNRSE'];					
							chart.series.items[0].yField = ser;
							

							
							}
					},
					
					dockedItems: [{
					                dock: 'bottom',
					                xtype: 'toolbar',
					                scale: 'large',
					                align: 'right',
					                border: true,
					                items: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "PANEL_GRAFICI", 30); ?>{
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-button_black_repeat-32',
					                     text: 'Reimposta',
								            handler: function() {
											var form = this.up('form').getForm();
											form.reset();
											
											//eseguo applica			
											button_applica = this.next();								
											button_applica.fireHandler();										
								            }
								         },{
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-button_black_play-32',
					                     text: 'Applica',
								            handler: function() {
												var form = this.up('form').getForm();
												chart = this.up('window').down('chart');
								
												tipo_selezionato = form.findField('tipo_grafico').getGroupValue();
					
					 							ar_parametri_tipo = ['TDCLOR_DES'];							
												
												if (tipo_selezionato == 'stato_ordine')
												 ar_parametri_tipo = ['TDSTAT'];
												if (tipo_selezionato == 'tipo_ordine')
												 ar_parametri_tipo = ['TDOTPD'];							
					
					
												array_filtri = form.getValues();													
												sto = raggruppa_dati(chart.store_data, ['CSAARG','CSNRSE'], ar_parametri_tipo, 'T_ROW', 'dati', array_filtri);							
												ser = raggruppa_dati(chart.store_data, ['CSAARG','CSNRSE'], ar_parametri_tipo, 'T_ROW', 'serie', array_filtri);
												
												chart.store = sto;							
												
												chart.axes.items[0].fields = ser;							
												chart.axes.items[1].fields = ['CSAARG-CSNRSE'];
					
												chart.series.items[0].xField = ['CSAARG-CSNRSE'];					
												chart.series.items[0].yField = ser;
												
												chart.series.clear();
												chart.surface.removeAll();
					
										        chart.series.add({
										            type:'area',
										            xField:['CSAARG-CSNRSE'],
										            yField: ser,
										            
													highlight: false,
													axis: 'left',
													style: {
														opacity: 0.93
													},										            

													tips: {
									                  trackMouse: true,
									                  width: 260,
									                  height: 38,
									                  renderer: function(storeItem, item) {
									                    //calculate percentage????
									                    this.setTitle(item.storeField + ': ' + storeItem.get(item.storeField));
									                  }
									                }
										            
										        });
																			
												chart.redraw();
					
																			
												//refresh chart2
												chart2 = this.up('window').down('#chartCmp2');
												chart2.store.proxy.extraParams = {form_values: Ext.encode(array_filtri)};
												chart2.store.load();
												
												//refresh chartStab
												chartStab = this.up('window').down('#chartCmpStab');
												chartStab.store.proxy.extraParams = {form_values: Ext.encode(array_filtri)};
												chartStab.store.load();							
												
												//refresh chartItin
												chartItin = this.up('window').down('#chartCmpItin');
												chartItin.store.proxy.extraParams = {form_values: Ext.encode(array_filtri)};
												chartItin.store.load();							
												
												//refresh chartDivi
												chartDivi = this.up('window').down('#chartCmpDivi');
												chartDivi.store.proxy.extraParams = {form_values: Ext.encode(array_filtri)};
												chartDivi.store.load();
												
												//refresh chartAge
												chartAge = this.up('window').down('#chartCmpAge');
												chartAge.store.proxy.extraParams = {form_values: Ext.encode(array_filtri)};
												chartAge.store.load();
												
												//refresh chartMerc
												chartMerc = this.up('window').down('#chartCmpMerc');
												chartMerc.store.proxy.extraParams = {form_values: Ext.encode(array_filtri)};
												chartMerc.store.load();
												
								            }
								         },
										{
					                     xtype: 'button',
						            	 scale: 'large',
						            	 iconCls: 'icon-print-32',
					                     text: 'Report',
								            handler: function() {
								            
							                form = this.up('form').getForm();
							                
							                if (form.isValid()){	                	                
								                form.submit({
								                		url: 'acs_grafici_panel_report.php',
										                standardSubmit: true,
								                        method: 'POST',
														target : '_blank',
														params : {'form_values' : Ext.encode(this.up('form').getForm().getValues())},																                        
								                });
							                }								            
								            
								         }
								        }    								         
								     ]
								    }
								   ]

			}]
        }
      }  
	]
}	