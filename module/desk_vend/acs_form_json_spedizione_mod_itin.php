<?php

require_once "../../config.inc.php";


$tataid = 'SPINT';

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

/********************************************************
 * ESECUZIONE
 ********************************************************/
if ($_REQUEST['fn'] == 'exe_save'){
	
	$sped_id = $m_params->form_values->sped_id;
	$sped = $s->get_spedizione($sped_id);
	
	//scrivo RI
	$sh = new SpedHistory($s);
	$sh->crea(
			'pers',
			array(
					"messaggio"				=> 'CHG_ITIN_SPED',
					"vals" => array(
							"RINSPE" 		=> $sped_id,
							"RICITI" 		=> $m_params->form_values->new_iti,
							"RINOTR"		=> "(ex: {$sped['CSCITI']})"
					),
			)
	);
	
	//aggiorno itinerario su spedizione
	$ar_val = array();
	$ar_val['CSCITI'] 	= $m_params->form_values->new_iti;
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_calendario']} SET " . create_name_field_by_ar_UPDATE($ar_val) . " WHERE  CSDT = ? AND CSPROG = ?";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge(
			$ar_val,
			array($id_ditta_default, $sped_id)
			));
	echo db2_stmt_errormsg($stmt);
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}




$sped_id = $m_params->sped_id;
$sped = $s->get_spedizione($m_params->sped_id);

//recupero (dall'itinerario attuale) l'area di spedizione attuale
$itin_attuale = $s->get_TA_std('ITIN', $sped['CSCITI']);
$itin_attuale_cod = trim($sped['CSCITI']);
$area_attuale_cod = trim($itin_attuale['TAASPE']);

?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            layout: {type: 'vbox', pack: 'start', align: 'stretch'},	
            title: '',
            
            items: [	
            	{
                    xtype: 'hiddenfield',
                    name: 'sped_id',
                    value: <?php echo j($m_params->sped_id)?>
                }, {
		            xtype: 'combo',
					name: 'new_iti',
					id: 'combo_itinerario',
					fieldLabel: 'Itinerario',
					labelAlign: 'right',
		            store: {
		                autoLoad: true,
						proxy: {
				            type: 'ajax',
				            url : 'acs_get_select_json.php?select=std_TA_combo',
				            
		                	extraParams: {
		    		    		taid: 'ITIN',
		    		    		aspe: <?php echo acs_je($area_attuale_cod); ?>
		    				},				            
				            
				            reader: {
				                type: 'json',
				                root: 'options'
				            }
				        },       
						fields: ['id', 'text'],		             	
		            },
		            
		            value: '<?php echo trim($itin_attuale_cod); ?>',            
					valueField: 'id',                       
		            displayField: 'text',
		            anchor: '100%',
		            
			        listeners: {
			            change: function(field,newVal) {	      			            				                        			                  		            	
			            }
			        }            
				
		        }					
					
					 
					

					
				, {
		            xtype: 'combo',
					name: 'area_spedizione',
					id: 'combo_area_spedizione',
					fieldLabel: 'Area spedizione',
					labelAlign: 'right',					
		            store: {
		                autoLoad: true,		            
						proxy: {
				            type: 'ajax',
				            url : 'acs_get_select_json.php?select=std_TA_combo',
				            
		                	extraParams: {
		    		    		taid: 'ASPE'
		    				},				            
				            
				            reader: {
				                type: 'json',
				                root: 'options'
				            }
				        },       
						fields: ['id', 'text'],		             	
		            },
		                        
		            value: '<?php echo trim($area_attuale_cod); ?>',            
					valueField: 'id',                       
		            displayField: 'text',
		            anchor: '100%',
		            
			        listeners: {
			            change: function(field,newVal) {
			            	combo_itin = this.up('window').down('#combo_itinerario').setValue('');
			            	combo_itin = this.up('window').down('#combo_itinerario');
			            	combo_itin.store.proxy.extraParams.aspe = newVal;
			            	combo_itin.store.load(); 	            				            
			            }
			        }            
				
		        }
			],
			
			
			buttons: [{
	            text: 'Salva',
	            scale: 'medium',
	            iconCls: 'icon-save-24',
	            handler: function() {
	            	var m_int_form = this.up('form').getForm();
	            	var m_int_win  = this.up('window');

					if(m_int_form.isValid()){
					    var m_button = this;
						m_button.disable();
						
						Ext.Ajax.request({
						   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save',
						   method: 'POST',
						   jsonData: {form_values: m_int_form.getValues()}, 
						   
						   success : function(result, request){
						   	  
						   	  var jsonData = Ext.decode(result.responseText);
						   	  if (jsonData.success == true)
						   	  	window.location.reload(); //refresh totale della pagina
						   	  else {
						   	  	alert('error on save');
						   	  }	
						   }, 
						   failure: function(response, opts) {
						      alert('error on save');
						   }
						});				

		           	} //form isValid            	                	                
	            }
	        }
	       ],             
				
        }
]}