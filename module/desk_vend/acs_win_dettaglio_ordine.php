<?php
require_once "../../config.inc.php";

$s = new Spedizioni();

if ($_REQUEST['fn'] == 'open_win'){
	$m_params = acs_m_params_json_decode();
	
	$oe = $s->get_ordine_by_k_ordine($m_params->k_ordine);


	?>

{"success":true, "items": [

    {
                xtype: 'tabpanel',
                region: 'east',
                title: '',
                margins: '0 5 0 0',
                tabPosition: 'bottom',

        		imposta_title: function(tddocu){
        				this.setTitle(tddocu);
        			},
                
                items: [Ext.create('Ext.grid.PropertyGrid', {
                        title: 'Riferimenti',
                        closable: false,
                         source:{},
                        disableSelection: true,
                        listeners: {
    					    'beforeedit': {
            					fn: function () {
                					return false;
            						}
        						}, 
        					afterrender: function (comp) {
								Ext.Ajax.request({
							   url: '../desk_vend/acs_get_order_properties.php?m_id=' + '<?php echo $m_params->k_ordine; ?>',
							   success: function(response, opts) {
							      var src = Ext.decode(response.responseText);
							      comp.setSource(src.riferimenti);
						          comp.up('window').setTitle(comp.up('window').title + '<?php echo " {$oe['TDOTPD']}_{$oe['TDOADO']}_{$oe['TDONDO']}{$oe['TDMODI']} {$oe['TDISON']}"; ?>');			      
							      
							   }
							});
	 					}
    					}
                    }),

  				 new Ext.grid.GridPanel({
                        title: 'Cronologia',
                  		store: new Ext.data.Store({
                  									
                  				autoLoad: true,				        
                  	  			proxy: {
                  							url: '../desk_vend/acs_get_order_cronologia.php',
                  							type: 'ajax',
                  							reader: {
                  						      type: 'json',
                  						      root: 'root'
                  						     },
                  						     extraParams: {
                  		    		    		k_ordine: '<?php echo $m_params->k_ordine; ?>'
                  		    				}               						        
   				        
                  						},
                  		        fields: ['data', 'utente', 'attivita', 'stato_data'],
                  		     	groupField: 'data',               		     	
                  			}),
       		    	features: new Ext.create('Ext.grid.feature.Grouping',{
   						groupHeaderTpl: 'Data: {[date_from_AS(values.name)]}',
   						hideGroupedHeader: false
   					}),               			
                     						    						
                        columns: [{
   			                header   : 'Utente',
   			                dataIndex: 'utente', 
   			                flex     : 3
   			             }, {
   				                header   : 'Attivit&agrave;',
   				                dataIndex: 'attivita', 
   				                width    : 70
   				         }, {
   			                header   : 'Stato / Evas. Prog.',
   			                dataIndex: 'stato_data', 
   			                flex     : 5
   			             }],

                        }), 

                    new Ext.grid.GridPanel({
                    title: 'Allegati',
               		 store: new Ext.data.Store({
               									
               				autoLoad: true,				        
               	  			proxy: {
               							url: '../desk_vend/acs_get_order_images.php',
               							type: 'ajax',
               							reader: {
               						      type: 'json',
               						      root: 'root'
               						     },
               						     extraParams: {
               		    		    		k_ordine: '<?php echo $m_params->k_ordine; ?>'
               		    				}               						        
				        
               						},
               		        fields: ['tipo_scheda', 'des_oggetto', 'IDOggetto', 'DataCreazione'],
               		     	groupField: 'tipo_scheda',               		     	
               			}),
    		    	features: new Ext.create('Ext.grid.feature.Grouping',{
						groupHeaderTpl: '{name}',
						hideGroupedHeader: true
					}),               			
                  						    						
                     columns: [{
			                header   : 'Tipo',
			                dataIndex: 'tipo_scheda', 
			                flex     : 5
			             }, {
				                header   : 'Data',
				                dataIndex: 'DataCreazione', 
				                width    : 70
				         }, {
			                header   : 'Nome',
			                dataIndex: 'des_oggetto', 
			                flex     : 5
			             }],

			         listeners: {
				      activate: {
					      fn: function(e){
						   	if (this.store.proxy.extraParams.k_ordine != '')
							   	this.store.load();
					      }
				      },
					       
			   		  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
							  rec = iView.getRecord(iRowEl);							  
							  allegatiPopup('../desk_vend/acs_get_order_images.php?function=view_image&IDOggetto=' + rec.get('IDOggetto'));
						  }
			   		  }
				         
			         }

                     }) 


                    ]
            }
	
]}


<?php 
exit;
}