<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

$list_selected_id = $m_params->selected_id;
$tree_id = $m_params->tree_id;

if ($m_params->by_sped_car == 'Y'){
	// Da Progetta spedizioni
	//dal primo recupero la data per impostare l'anno del carico e la spedizione (per recuperare vettore, .....)
	$fl = $list_selected_id[0]; //primo record
	$sottostringhe = explode("|", $fl);
	
	$day 	= $m_params->data;
	$year 	= substr($day, 0, 4);	
	
	$sped_id 	= $sottostringhe[0];
	$carico_txt = $sottostringhe[1];
	$carico_ar  = $s->k_carico_td_decode($carico_txt);
	
	$sql = "SELECT TDASPE FROM {$cfg_mod_Spedizioni['file_testate']} TD
	        WHERE " . $s->get_where_std() . "
			AND TDDTEP = " . $day . "
			AND TDNBOC = " . $sped_id . "
			AND TDTPCA = " . sql_t($carico_ar['TDTPCA']) . "
			AND TDAACA = " . $carico_ar['TDAACA'] . "
			AND TDNRCA = " . $carico_ar['TDNRCA'] . "
		    ";
	
} else if ($m_params->by_sped_car == 'by_ORD'){
    $fl = $list_selected_id[0];
	$day = $m_params->data;
	$year = substr($m_params->data, 0, 4);
	
	$sql = "SELECT TDASPE FROM {$cfg_mod_Spedizioni['file_testate']} TD
	        WHERE " . $s->get_where_std() . "
			AND TDDOCU = " . sql_t($fl) . "";
	
} else {
	
	// Da DAY
	
	//dal primo recupero la data per impostare l'anno del carico e la spedizione (per recuperare vettore, .....)
	$fl = $list_selected_id[0]; //primo record
	$sottostringhe = explode("|", $fl);
	$day 	= substr($sottostringhe[2], 0, 8);
	$year 	= substr($sottostringhe[2], 0, 4);
	
	$exp_liv1 = explode("___", $sottostringhe[2]);
	$sped_id = $exp_liv1[1];
	$exp_liv2 = explode("___", $sottostringhe[4]);
	
	$sql = "SELECT TDASPE FROM {$cfg_mod_Spedizioni['file_testate']} TD
	        WHERE " . $s->get_where_std() . "
			AND TDDTEP = " . $day . "
			AND TDNBOC = " . $sped_id . "
			AND TDTPCA = " . sql_t($exp_liv1[2]) . "
			AND TDAACA = " . $exp_liv1[3] . "
			AND TDNRCA = " . $exp_liv1[4] . "
			AND TDSECA = " . sql_t($exp_liv2[0]) . "
			AND TDCCON = " . sql_t($exp_liv2[1]) . "
			AND TDCDES = " . sql_t($exp_liv2[2]) . "";
	
	
}

$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt);
$row = db2_fetch_assoc($stmt);
$d_area_spe = "[".trim($row['TDASPE'])."] ".$s->decod_std('ASPE', $row['TDASPE']);
$find_TA = find_TA_std('ASPE', $row['TDASPE']);
$tipo_carico = trim($find_TA[0]['TASITI']);
$area_spe = trim($row['TDASPE']);

//$spedizione = $s->get_spedizione($sped_id); NON SEMBRA USATA 
$ar_ordini  = array();
$ar_abbi    = array();
$ar_warning = array();
foreach ($list_selected_id as $k1){
    
    //nelle testate relative aggiorno il carico
    $sottostringhe = explode("|", $k1);
    
    if ($m_params->by_sped_car == 'Y'){			// per sped_carico (da Progettazione)
        $sped 			= $sottostringhe[0];
        $carico_txt 	= $sottostringhe[1];
        $carico_ar		= $s->k_carico_td_decode($carico_txt);
        
        //recupero l'elenco degli ordini da modificare
        $sql = "SELECT TDDOCU, TDOADO, TDONDO, TDOTPD, TDABBI, TDDCON, TDTDES, TDCCON, TDCDES, TDNBOC, TDIDES, TDDLOC 
                FROM {$cfg_mod_Spedizioni['file_testate']} TD
                WHERE " . $s->get_where_std() . "
							AND TDDTEP = " . $m_params->data . "
							AND TDNBOC = " . $sped . "
							AND TDTPCA = " . sql_t($carico_ar['TDTPCA']) . "
							AND TDAACA = " . $carico_ar['TDAACA'] . "
							AND TDNRCA = " . $carico_ar['TDNRCA'] . "
						    ORDER BY TDONDO
					";
        
    } else if ($r['by_sped_car'] == 'by_ORD') { //ricevo direttamente l'elenco degli ordini
        //recupero l'elenco degli ordini da modificare
        $sql = "SELECT TDDOCU, TDOADO, TDONDO, TDOTPD, TDABBI, TDDCON, TDTDES, TDCCON, TDCDES, TDNBOC, TDIDES, TDDLOC
                FROM {$cfg_mod_Spedizioni['file_testate']} TD
                WHERE " . $s->get_where_std() . "
				AND TDDOCU = " . sql_t($k1) . "
					";
        
        
    } else { 								//per cliente/sequenza/.. da DAY
        $liv1 = $sottostringhe[2];
        $liv2 = $sottostringhe[4];
        
        $s1 = explode("___", $liv1);
        $s2 = explode("___", $liv2);
        
        
        //recupero l'elenco degli ordini da modificare
        $sql = "SELECT TDDOCU, TDOADO, TDONDO, TDOTPD, TDABBI, TDDCON, TDTDES, TDCCON, TDCDES, TDNBOC, TDIDES, TDDLOC 
                FROM {$cfg_mod_Spedizioni['file_testate']} TD
                WHERE " . $s->get_where_std() . "
					AND TDDTEP = " . $s1[0] . "
					AND TDNBOC = " . $s1[1] . "
					AND TDTPCA = " . sql_t($s1[2]) . "
					AND TDAACA = " . $s1[3] . "
					AND TDNRCA = " . $s1[4] . "
					AND TDSECA = " . sql_t($s2[0]) . "
					AND TDCCON = " . sql_t($s2[1]) . "
					AND TDCDES = " . sql_t($s2[2]) . "
			";
    }
  
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt);
   
    $sql_me = "SELECT 
               SUM(CASE WHEN TAKEY4 = 'I' THEN 1 ELSE 0 END) C_INCLUSI,
               SUM(CASE WHEN TAKEY4 = 'E' THEN 1 ELSE 0 END) C_ESCLUSI, TAKEY3
               FROM {$cfg_mod_Spedizioni['file_tabelle']} TA
               WHERE TADT = '{$id_ditta_default}' AND TATAID = 'MZESL'
               AND TAKEY1 = ? AND TAKEY2 = ?  
               GROUP BY TAKEY3";
    
   
    $stmt_me = db2_prepare($conn, $sql_me);

    while ($row = db2_fetch_assoc($stmt)){
        
        if(trim($row['TDTDES']) == 'C' || trim($row['TDTDES']) == 'D'){
            $sped = $s->get_spedizione($row['TDNBOC']);
  
            $result = db2_execute($stmt_me, array(trim($row['TDCCON']), trim($row['TDCDES'])));
            $ar_mezzi = array();
            while($row_me = db2_fetch_assoc($stmt_me)){
                $ar_mezzi[] = rtrim($row_me['TAKEY3']);
                $mezzi_i += $row_me['C_INCLUSI'];
                $mezzi_e += $row_me['C_ESCLUSI'];
              
            }
     
            $sped_auto = rtrim($sped['CSCAUT']);
            $cliente = trim($row['TDDCON']);
            if(trim($row['TDCDES']) != '')
                $destinazione = ", ".trim($row['TDDLOC']).", ".trim($row['TDIDES']);
            
            if(count($ar_mezzi) > 0){
                if($mezzi_i > 0 && !in_array($sped_auto, $ar_mezzi))
                    $ar_warning[] = array('text_msg' => " - ATTENZIONE: Mezzo non ammesso per destinazione {$cliente} {$destinazione}");
                
                if($mezzi_e > 0 && in_array($sped_auto, $ar_mezzi))
                    $ar_warning[] = array('text_msg' => " - ATTENZIONE: Mezzo non ammesso per destinazione {$cliente} {$destinazione}");
            }
            
        }
        
        $ar_ordini[] = trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
        $k_ordine_out = trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
        $ar_abbi[] = array('ordine' => trim($row['TDABBI']),
                           'text_msg' => trim($row['TDABBI'])." [".$k_ordine_out."] - ".trim($row['TDDCON'])
            
        );
       
    }

} 


foreach($ar_abbi as $k => $v){
    if($v['ordine'] != ''){
        if(!in_array($v['ordine'], $ar_ordini)){
            if($k == 0){
                $error_text .= " - ATTENZIONE: Ordini con spedizione ABBINATA non inclusi!";
                $error_text .= "\n{$v['text_msg']}";
            }else{
                $error_text .= "\n{$v['text_msg']}";
            }
        }
    }
}

if(count($ar_warning) > 0){
    foreach($ar_warning as $v)
        $error_text .= "\n{$v['text_msg']}";
}
function out_array_hidden_selected_id($list){
	$ret = array();
	foreach ($list as $l) {
		$ret[] = array('xtype' => "hidden", 'name'=>"list_selected_id[]", 'value'=>$l);
	}
	return implode(',' , array_map('json_encode', $ret));
}



?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: 'acs_op_exe.php',
            
            
            items: [{
	                	xtype: 'hidden',
	                	name: 'fn',
	                	value: 'assegna_carico'
                	}, {
	                	xtype: 'hidden',
	                	name: 'sped_id',
	                	value: '<?php echo $sped_id ?>'
                	}, {
	                	xtype: 'hidden',
	                	name: 'by_sped_car',
	                	value: '<?php echo $m_params->by_sped_car ?>'
                	}, {
	                	xtype: 'hidden',
	                	name: 'dtep',
	                	value: '<?php echo $day ?>'
                	}
                	
               <?php if ($cfg_mod_Spedizioni['carico_per_data'] == 'Y'){ ?> 	
                	, {
						name: 'f_chiave',
						xtype: 'hidden',
						value: '<?php echo $day ?>',
					    anchor: '-15'							
					 }, {
						name: 'f_anno',
						xtype: 'hidden',
						value: '<?php echo $year ?>'							
					 }, {
						xtype: 'textfield',
						disabled: true,
						fieldLabel: 'Data',
						value: '<?php echo print_date($day); ?>',
					    anchor: '-15'							
					 }
					 
			  <?php } else { ?>
                	, {
						name: 'f_chiave',
						xtype: 'hidden',
						value: '<?php echo $year ?>'							
					 }, {
						name: 'f_anno',
						xtype: 'hidden',
						value: '<?php echo $year ?>'							
					 }, {
						xtype: 'textfield',
						fieldLabel: 'Anno',
						disabled: true,
						value: '<?php echo $year ?>',
					    anchor: '-15'							
					 }			  			  
			  <?php }?>		
			  
			   		, {
						name: 'f_aspe',
						xtype: 'hidden',
						value: <?php echo j($area_spe); ?>,						
					 },
					 
					 
			   		 {
						name: 'f_tipo_carico',
						xtype: 'hidden',
						value: <?php echo j($tipo_carico); ?>,						
					 }	 	  
			  	 
					 , {
						name: 'f_d_aspe',
						xtype: 'displayfield',
						fieldLabel: 'Area spedizione',
						value: <?php echo j($d_area_spe); ?>,
					    anchor: '-15'							
					 },
					 
				    {
						name: 'f_carico',
						xtype: 'textfield',
						fieldLabel: 'Carico',
						value: '',
					    anchor: '-15'							
					 }, {
						name: 'f_descrizione',
						xtype: 'textfield',
						fieldLabel: 'Descrizione',
						value: '',
					    anchor: '-15'							
					 }, {
								name: 'f_stato',
								xtype: 'combo',
								fieldLabel: 'Stato',
								displayField: 'text',
								valueField: 'id',								
								emptyText: '- seleziona -',
							    anchor: '-15',
								store: {
									editable: false,
									autoDestroy: true,
								    fields: [{name:'id'}, {name:'text'}],
							    	data: [								    
								      <?php echo acs_ar_to_select_json($s->find_TA_std('STCAR'), ""); ?>	
								    ]
								}						
					  }
					  
			,{
	            xtype:'fieldset',
	            title: 'Assegnazione su carico/spedizione aggiuntiva',
	            collapsible: false,
	            collapsed: false, // fieldset initially collapsed	            
	            defaultType: 'textfield',
	            layout: 'anchor',
	            defaults: {
	                anchor: '100%'
	            },
	            items :[					  
					  
					 {
						name: 'f_seq_inizio_altro_carico',
						xtype: 'textfield',
						fieldLabel: 'Seq. inizio altro carico',
						width: 200,
						anchor: '-350px',
						value: ''							
					 }, {
						name: 'f_volume_limite_primo_carico',
						xtype: 'numberfield', hideTrigger:true,
						fieldLabel: 'Volume limite primo carico',
						width: 200,
						anchor: '-350px',
						value: ''							
					 }, {
		                            xtype: 'checkbox'
		                          , fieldLabel: 'Azzera sequenza'
		                          , name: 'f_azzera_sequenza' 
		                          , boxLabel: 'Si'
		                          , checked: false
		                          , inputValue: 'Y'
		             }
		             
<?php if ($m_params->storeId_rx != 'ext-empty-store' && !is_null($m_params->storeId_rx)){ ?>		             
		             , {
		                            xtype: 'checkbox'
		                          , fieldLabel: 'Riversa su altra spedizione'
		                          , name: 'f_riversa_su_altra_spedizione' 
		                          , boxLabel: 'Si'
		                          , checked: true
		                          , inputValue: 'Y'
		             }, {
						name: 'f_storeId_rx',
						xtype: 'hidden',
						value: '<?php echo $m_params->storeId_rx ?>'							
					 }
<?php } ?>

			]
		} ,
					 <?php echo out_array_hidden_selected_id($list_selected_id) ?>
					 
					 
					 <?php if($error_text != ''){?>
					  ,{
						name: 'f_error_text',
						xtype: 'textareafield',
						fieldCls: 'segnalazioni errore grassetto',	
						height: 70, width: '100%',
						value : <?php echo j($error_text)?>					
					 }
					 <?php }?>
					 
				],
			
			buttons: [{
	            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
		            loc_win = this.up('window');	            	

	            <?php if ($cfg_mod_Spedizioni['carico_per_data'] == 'Y'){ ?>	            	
	            	if (form.findField("f_carico").getValue().trim().length <= 2){
	            <?php } else { ?>
	            	if (form.findField("f_carico").getValue().trim() == ''){	            
	            <?php }?>
	            	
	            	  //recupero il numeratore	            	 
								Ext.Ajax.request({
								        url        : 'acs_op_exe.php?fn=assegna_progressivo_carico',
								        jsonData: form.getValues(),
								        method     : 'POST',
								        waitMsg    : 'Data loading',
								        success : function(result, request){	            	  													        
								            var jsonData = Ext.decode(result.responseText);
								            form.findField("f_carico").setValue(jsonData.nr);	
								        },
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });	            	  
	            	  return;
	            	}
	            	
	            	
	            	
	            	
	            	   onMySuccess = function(form, action){
							m_tree = Ext.getCmp('<?php echo $tree_id; ?>');
			                            	if (typeof(m_tree)!=='undefined'){
												m_store = m_tree.getStore();
												m_store.treeStore.load();
											}
											Ext.getBody().unmask();
											fine = true;
											loc_win.close();	            	   
	            	   };
	            	   onMyFailure = function(form, action){
			                            	Ext.getBody().unmask();
			                            	if (parseInt(action.result.num_ordini_in_carico) > 0){		
			                            		acs_show_msg_error(action.result.error_log);
			                            		fine = true;      
			                            	}                      
			                                else {
			                                  //ordine esistente ma senza ordini associati. Chiedo la forzatura
	
											    Ext.Msg.confirm('Attenzione!', 'Carico gi&agrave; presente. Procedere ugualmente?', function(btn, text){
											      if (btn == 'yes'){
											      	Ext.getBody().mask('Loading... ', 'loading').show();	
											       	form.submit({
											       	 params : {forzaCarico : form.findField('f_carico').getValue()},
											       	 success: onMySuccess,
											       	 failure: onMyFailure
											       	});
											      } else {
											        //nothing
											      }
											    });
	
			                                }	            	   
	            	   };
	            	
	            	


						Ext.getBody().mask('Loading... ', 'loading').show();	            	
		                form.submit({
			                            //waitMsg:'Loading...',
			                            success: onMySuccess,
			                            failure: onMyFailure
			                        });
            
            	                	                
	            }
	        }],             
				
        }
]}
 <?php
 