<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
 
?>

{"success":true, "items": [
{
	            xtype: 'panel',
	            title: '',
	            layout: 'fit',
				cls: 'acs_toolbar',
				tbar: [{
				            xtype: 'buttongroup',
				            columns: 50,
				            title: 'Statistiche',
				            items: [{
				                text: 'Portafoglio ordini',
				                scale: 'large',			                 
				                iconCls: 'icon-grafici-32',
				                iconAlign: 'top',			                
				                 handler : function() {				                	 
				                	 acs_show_win_std('Orizzonte settimanale <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') echo "righe ";  ?>ordini per data programmata', 'acs_grafici_panel_json.php', null, 950, 650, null, 'icon-grafici-16', 'Y', 'Y');
									} //handler function()
				            }, {
				                text: 'Media ordine',
				                scale: 'large',			                 
				                iconCls: 'icon-receipt-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Medie su ordini confermati', 'acs_form_json_media_itinerario_nazione.php?fn=get_parametri_form', null, null, null, null, 'icon-receipt-16', 'Y')
									} //handler function()
				            } 
						    ]
				}, {
				            xtype: 'buttongroup',
				            columns: 50,
				            title: 'Report',
				            items: [{
				                text: 'Deroghe',
				                scale: 'large',			                 
				                iconCls: 'icon-unlock-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Parametri di stampa - Report deroghe', 'acs_report_el_deroghe.php?fn=get_parametri_form', null, 400, 200, null, 'iconPrint')
									} //handler function()
				            }, {
				                text: 'Riprogramm. ordini',
				                scale: 'large',			                 
				                iconCls: 'icon-button_black_repeat_dx-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Parametri di stampa - Report riprogrammazione ordini', 'acs_report_RIPRO.php?fn=get_parametri_form', null, 400, 280, null, 'iconPrint')
									} //handler function()
				            },{
				            	text: 'Resi cliente',
				                scale: 'large',			                 
				                iconCls: 'icon-recycle_bin-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Parametri di stampa - Report Resi cliente', 'acs_report_resi_cliente.php?fn=get_parametri_form', null, 400, 280, null, 'iconPrint')
									} //handler function()
				          
				            }, {
				            	text: 'Anzianit&agrave; stato',
				            	
				                scale: 'large',			                 
				                iconCls: 'icon-clessidra-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Analisi anzianit&agrave; stato ordini','acs_analisi_anzianita_stato_ordini.php?fn=open_parameters', {}, 700, 450, {}, 'icon-clessidra-16');
									 this.up('window').close();
									} //handler function()
				          
				            }, {
				            	text: 'Programmazione',
				            	
				                scale: 'large',			                 
				                iconCls: 'icon-game_pad-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Analisi programmazione evasione ordini/trasporto','acs_ordini.php?fn=open_parameters', {}, 600, 380, {}, 'icon-game_pad-16');
									 this.up('window').close();
									} //handler function()
				          
				            }, {
				            	text: 'Ordini Spediti',
				            	
				                scale: 'large',			                 
				                iconCls: 'icon-delivery-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Riepilogo ordini spediti','acs_ordini_spediti_report.php?fn=open_parameters', {}, 450, 300, {}, 'icon-game_pad-16');
									 this.up('window').close();
									} //handler function()
				          
				            },{
				            	text: 'Booking',
				            	
				                scale: 'large',			                 
				                iconCls: 'icon-blog_compose-32',
				                iconAlign: 'top',			                
				                 handler : function() {
									 acs_show_win_std('Booking','acs_booking.php?fn=open_number', {}, 450, 200, {}, 'icon-game_pad-16');
									 this.up('window').close();
									} //handler function()
				          
				            }]
				}]
			}
]
}	