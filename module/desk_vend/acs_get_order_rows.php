<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

	switch ($_REQUEST['type']){
		case "MTO":
		case "MTS":
			$n_ord = $_REQUEST['nord'];
			$stmt = $s->get_rows_ordine_by_num($n_ord, array('MTO', 'MTS', 'B_MRP'), 'Y');
			$data = array();
			
			
			while ($row = db2_fetch_assoc($stmt)) {
				//if (strlen(trim($row['RDDES1'])) > 0)
				//	$row['RDDES1'] = trim($row['RDDES1']) . "<br>" . trim($row['RDDES2']);
				
				$row['RDDES1'] = acs_u8e($row['RDDES1']);				
				$row['RDDART'] = acs_u8e($row['RDDES1']);
				$row['RDFG02'] = acs_u8e($row['RDFG02']);
				$row['RDFG01'] = acs_u8e($row['RDFG01']);
				$row['maga'] = trim($row['M2MAGA']);
				$row['disponibilita'] = acs_u8e((float)$row['M2QGIA'] + (float)$row['M2QORD'] - (float)$row['M2QFAB']);
				
				if (trim($row['RDDES2']) != ''){
					$ord_ass_exp = explode("_", $row['RDDES2']);
					if (count($ord_ass_exp) >= 5){
						$row['k_ord_art'] = implode("_", array(
							$id_ditta_default,
							trim($ord_ass_exp[4]),
							trim($ord_ass_exp[3]),
							trim($ord_ass_exp[0]),
							trim($ord_ass_exp[1])
						));

						$ord_doc = $s->get_ordine_gest_by_k_docu($row['k_ord_art']);
						if ($ord_doc)
						  $row['maga'] = $ord_doc['TDDEPO'];
						
					}
				}
				
				$row['stato_MTO'] = $s->decod_std('MTOST', trim($row['RDFG03']));
				
				//$row['k_ord_art'] = "{$id_ditta_default}_AO_AO1_".trim($row['RDDES2']);				
			
				//Da TDDOCU ricavo ditta origine
				$row['k_ordine']=$row['TDDOCU'];
				$ordine_exp = $s->k_ordine_td_decode_xx($row['TDDOCU']);
				$row['ditta_origine'] = $ordine_exp['TDDT']; 
				
				//php7
				$row = array_map('utf8_encode', $row);
				
				$accoda_riga = true;
				
				if (isset($_REQUEST['solo_stato']))
				    if (trim($_REQUEST['solo_stato']) != trim($row['RDRIFE']))
				        $accoda_riga = false;
				    
				if (isset($_REQUEST['solo_con_documento']) && $_REQUEST['solo_con_documento'] == 'Y')
				    if (trim($row['RDDES2']) == '')
				        $accoda_riga = false;
							
                if ($accoda_riga)				        
				    $data[] = $row;
			}
			
			$ord_exp = explode('_', $n_ord);
			$anno = $ord_exp[3];
			$ord = $ord_exp[4];			
		  	$ar_ord =  $s->get_ordine_by_k_ordine($n_ord);
			$m_ord = array(
								'TDONDO' => $ar_ord['TDONDO'],
								'TDOADO' => $ar_ord['TDOADO'],								
								'TDDTDS' => print_date($ar_ord['TDDTDS'])
						   );
			
		echo "{\"root\": " . acs_je($data) . ", \"ordine\": " . acs_je($m_ord) . "}";
		
		$appLog->save_db();		
		exit();				
		break;		
	} 


?>
