<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$p_exp = explode("|", $_REQUEST['sped_car']);
$p_exp2 = explode("|", $_REQUEST['sped_car2']);
$data = $_REQUEST['data'];
$sped_id = $p_exp[0];
$sped = $s->get_spedizione($sped_id);


$caricoo_txt = $p_exp[1];

if (strlen(trim($carico_exp)) == 0)
	$per_spedizione = 'Y';
else	
	$carico_exp = $s->k_carico_td_decode($p_exp[1]);



$itin_id 	= trim($_REQUEST['itin_id']);
$it = new Itinerari();
$it->load_rec_data_by_k(array('TAKEY1' => $itin_id));
$itin_descr	= $s->decod_std('ITIN', $itin_id);

//recupero volume spedizione
if ($per_spedizione == 'Y')
	$sped_totali = $s->get_totali_by_sped($sped_id, $id_ditta_default);
else	
	$sped_totali = $s->get_totali_by_sped_car($sped_id, $carico_exp['TDTPCA'], $carico_exp['TDAACA'], $carico_exp['TDNRCA'], $id_ditta_default);
$sped_volume = $sped_totali['TDVOLU'];
$sped_vmc 	 = $s->decod_vmc_by_sped_row($sped, " - ");

//recupero volume spedizione2
if (strlen($_REQUEST['sped_car2']) > 0 && $_REQUEST['sped_car2'] != "ext-empty-store"){
	$sped2_out = "#{$p_exp2[0]}, {$p_exp2[1]}";
	$sped2_id = $p_exp2[0];
	$sped2 = $s->get_spedizione($sped2_id);	
	$carico2_exp = $s->k_carico_td_decode($p_exp2[1]);	
	$sped2_totali = $s->get_totali_by_sped_car($sped2_id, $carico2_exp['TDTPCA'], $carico2_exp['TDAACA'], $carico2_exp['TDNRCA'], $id_ditta_default);
	$sped2_volume = $sped2_totali['TDVOLU'];
	$sped2_vmc 	  = $s->decod_vmc_by_sped_row($sped2, " - ");	
} else {
	$sped2_out = " - ";
	$sped2_volume = 0;
}


?>



<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="core.css">

<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/optimap_style.css">
<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/jquery-ui-1.8.16.custom.css">

<link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />

<script src="<?php echo site_protocol();?>ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo site_protocol();?>ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>

 <?php require '../../templates/import_gmap_api.php'; ?>

 <script type="text/javascript" src="../../../extjs/ext-all.js"></script>

  
 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/acs_js.js"></script>
 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/google-code-prettify/prettify.js"></script>
  
  
 <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/style.css"); ?> />  
  
 <style type="text/css">
  .x-body{font-size: 1.2em; font-family: 'Lucida Grande',Verdana,Arial,Sans-Serif; margin: 5px;}
  .grassetto{font-weight: bold;}
  .page-sub-title{font-size: 0.8em;} 
  .ragsoc{font-size: 12px; color: gray;}
  ul#error_results{font-size: 14px;}
  a{color: black;}
  ul#sortable{font-size: 14px; color: black;}
  ul#sortable li{border: 1px solid gray; 
  	background: url("images/ui-bg_glass_100_f6f6f6_1x400.png") repeat-x scroll 50% 50% #F6F6F6;
    border: 1px solid #CCCCCC;
    padding: 2px;}
  ul#sortable li span.cli{display: inline-block; font-weight: bold;}  
  ul#sortable li span.num{width: 24px; text-align: right; display: inline-block; padding: 3px; margin-right: 10px; border: 1px solid black; background-color: black; color: white; font-weight: bold}
  ul#sortable li.sped_collegata_1 span.num{background-color: #888888;}
  ul#sortable li span.ind_dest{font-weight: normal; font-size: 8px;}
  ul#sortable td.v0{width: 30px;}   
  ul#sortable td.v1{border: 1px solid gray; background-color: white; width: 40px; text-align: center;}
  a.click-to-select-scarico{font-weight: bold;}
  a.click-to-select-scarico[data-is-selected="1"]{color: blue;}
  input.calcButton{font-size: 12px; width: 170px;}
  table.riepilogo{font-size: 14px; border-collapse: collapse;}
  table.riepilogo td{border-left: 1px solid #cccccc; border-right: 1px solid #cccccc; padding: 3px;}
  table.riepilogo tr.seconda_spedizione td{color: #0000ff; background-color: #D1E2D3; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;}
  div#header{padding-bottom: 7px}
  
  
  <?php if ($_REQUEST['only_view'] == 'Y'){ ?>
  	table.riepilogo{display: none;}
  <?php } ?>	
      
 </style>


  <script type="text/javascript">  
  
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {
		
        my_listeners = {
              	onSpedSelected: function(rec_sped_selected){
					carico_exp = rec_sped_selected.get('carico').split('_');
                    var xhr_coll = $.getJSON('acs_form_json_progetta_spedizioni.php?fn=grid_data&for_gmap=Y&sped_id=' + rec_sped_selected.get('CSPROG') + '&data=' + rec_sped_selected.get('DATA') + '&tpca=' + carico_exp[1] + '&aaca=' + carico_exp[0] + '&nrca=' + carico_exp[2]);
                    xhr_coll.done(loadResultsSpedCollegata);              	    
              	    									                  	    
              		}
              };
		
	  	Ext.get("add_link").on('click', function(){
            acs_show_win_std('Raffronta con spedizione', 'acs_form_json_assegna_spedizione.php', {list_selected_id:[], solo_raffronto: 'Y', scelta_sped_collegata_per_sped_id: <?php echo $sped_id; ?>, data: <?php echo $data; ?>, cod_iti: '<?php echo $it->rec_data['TARIF1']; ?>'}, 1050, 400, my_listeners)
	  	});
	  	
		
	});
  </script>
 
 
 
 
 
</head>





<body>

  <script type="text/javascript">
    var map;
    var item, items, markers_data = [], errors_data = [];   
    var responseCount = 0, addressCount;
    var geocoder = new google.maps.Geocoder();
    var data;

    var n_tentativo = 1;

    var xhr_string = 'acs_form_json_progetta_spedizioni.php?fn=grid_data&for_gmap=Y&<?php echo "sped_id={$sped_id}&data={$data}&tpca={$carico_exp['TDTPCA']}&aaca={$carico_exp['TDAACA']}&nrca={$carico_exp['TDNRCA']}" ?>';



	function get_geocode(record, counter){
		if (record['ha_coordinate'] == 'Y'){
			responseCount++;
			return;
		}	
		

        geocoder.geocode( { 'address': record['gmap_ind'].trim()}, function(results, status) {
            console.log('-------------------------');
			console.log('geocoder.geocode');
			console.log(record['gmap_ind'].trim());
			console.log('status: ' + status);
        	responseCount++;	
        	if (status == google.maps.GeocoderStatus.OK) {

            	console.log(results);
        	    var latitude = results[0].geometry.location.lat();
        	    var longitude = results[0].geometry.location.lng();


        	    record['counter'] = counter;
        	    record['valido']  = 'Y';
				record['lat'] = latitude;
				record['lng'] = longitude;
				record['title'] = record['TDDCON'].trim();
				record['icon'] = {
						//'url' : 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + counter + '|FF0000|0000FF',
						'url': 'iconsnew/black' + counter + '.png',
						'size': new google.maps.Size(32, 32)
				};

				markers_data.push(record);
        	} else {						        		

            	
				if (status == google.maps.GeocoderStatus.ZERO_RESULTS || 
					status == google.maps.GeocoderStatus.INVALID_REQUEST) {
	            	record['counter'] = counter;
	        	    record['valido']  = 'N';
	        	    markers_data.push(record);
	            	errors_data.push(record);										
				}						            							            	

            }
			console.log('richiamo esecuzione_finale');
			esecuzione_finale();
        	
        });						


	}

	
	function esecuzione_finale(){		
		console.log('responseCount: ' + responseCount);
		console.log('addressCount: ' + addressCount);
		
		if (responseCount == addressCount){
			//ho finito tutte le richieste
      		//map.addMarkers(markers_data);
            //loadErrorResults();
            console.log('Start EsecuzioneFinale');
            
			Ext.Ajax.request({
			        url        : 'acs_op_exe.php?fn=progetta_spedizioni_memorizza_coordinate',
			        method     : 'POST',
			        waitMsg    : 'Data loading',
        			jsonData: {
        				selected_id: markers_data,
					},							        
			        success : function(result, request){

			        	retry_xhr = $.getJSON(xhr_string);			        	
			        	retry_xhr.done(verifyResults);				        
			        },
			        failure    : function(result, request){
			            Ext.Msg.alert('Message', 'No data to be loaded');
			        }
			    });							

            
		}			 
	}






    

    function confermaSecondaSpedizione() {	

        jQuery('#savingProgress').dialog('open');

        s1 = []; //s1: assegnate a prima spedizione
        s2 = []; //s2: assegnate a seconda spedizione

		for (var j = 0; j < markers_data.length; j++) {
			if (markers_data[j]['is_selected'] == 1)
				s2.push(markers_data[j]);
			else 
				s1.push(markers_data[j]);				 
		}
        
        var response = jQuery.ajax({
        	type: "POST",
        	url: "acs_op_exe.php?fn=progetta_spedizioni&from_gmap=Y",
        	type: "POST",
        	contentType: "application/json; charset=utf-8",
        	async: false,
        	data: JSON.stringify({
        			cod_iti: '<?php echo trim($itin_id); ?>', data: <?php echo $data; ?>,
                	data: <?php echo $data; ?>,
        			sped_car_id: [<?php echo j($_REQUEST['sped_car']); ?>, <?php echo j($_REQUEST['sped_car2']); ?>],
        			selected_id: [s1, s2]        		
            		}),
        	dataType: "json"
        	});

    	if (response.statusText == 'OK'){
    	    window.opener.call_acs_refresh_all('<?php echo $_REQUEST['refresh_ext_id'] ?>');
    	    window.close();	    		
    	}    	
        
    }

    

	function ricalcola_volume_seconda_spedizione(){
		volume_prima_spedizione = 0; volume_seconda_spedizione = <? echo (int)$sped2_volume; ?>;
		for (var j = 0; j < markers_data.length; j++) {
			if (markers_data[j]['is_selected'] == 1)
				volume_seconda_spedizione += parseFloat(markers_data[j]['volume']);
			else 
				volume_prima_spedizione += parseFloat(markers_data[j]['volume']);				 
		}
		$( "#volume_prima_spedizione" ).text(floatRenderer2(volume_prima_spedizione));
		$( "#volume_seconda_spedizione" ).text(floatRenderer2(volume_seconda_spedizione));		
	}


    function loadResults (data) {
        loadResultsExe(data, 0 );
    }	
    function loadResultsSpedCollegata (data) {
        loadResultsExe(data, 1);
    }


    function verifyResults (data) {
        for (var i = 0; i < data.points.length; i++) {
    		if (data.points[i]['ha_coordinate'] != 'Y'){
    			console.log('Ci sono ancora punti senza coordinate');
    			acs_show_msg_error('Non tutti i punti della spedizione sono geolocalizzati');
    			return false;
    		}
        }
        console.log('Tutti i punti hanno le coordinate.... Posso fare reload');
    	location.reload();
    	return true;
    }

    
    
    function loadResultsExe (data, sped_collegata) {
    	
	  if (typeof(sped_collegata) === 'undefined') sped_collegata = 0;
        
      addressCount = data.points.length;

      for (var i = 0; i < data.points.length; i++) {
		if (data.points[i]['ha_coordinate'] != 'Y'){
			if (n_tentativo == 1 && sped_collegata == 0){
				//provo geolocalizzare
				console.log('Avvio geolocalizzazione spedizione principale');

				selected_id = data.points;

			      if (selected_id.length > 0) {
			        items = selected_id;
			
			        for (var i = 1; i <= items.length; i++) {          
					  get_geocode(items[i-1], i);          
			        }
			      }
				
				//-- fine geolocalizzazione
								
			} else {
				acs_show_msg_error('Non tutti i punti della spedizione sono geolocalizzati');
			}			
			return false;
			
		}			          
      }

      
      if (data.points.length > 0) {
        items = data.points;
        
        for (var i = 1; i <= items.length; i++) {          
		  //get_geocode(items[i-1], i);

		  	record = items[i-1];
        	record['counter'] = i;		  	

			record['lat'] = record['LAT'];
			record['lng'] = record['LNG'];

			record['title'] = '<span class=cli>' + record['TDDCON'].trim() + '</span>';
			record['ind_dest'] = record['TDDLOC'].trim() + ' (' + record['TDPROD'].trim() + '), ' + record['TDNAZD'].trim() + ', ' + record['TDIDES'].trim();
			record['is_selected'] = 0;			

			//scorro markers_data per verificare se (in base alle coordinate) il punto esiste gia'
			record['DUPLICATO'] = 'N';
			for (var j = 0; j < markers_data.length; j++) {
				if ((markers_data[j]['lat'] == record['lat'] && markers_data[j]['lng'] == record['lng'])){
					record['DUPLICATO'] = 'Y';

//					if (Ext.array.contains(markers_data[j]['el_clienti'], record['TDDCON'].trim())
					markers_data[j]['el_clienti'][record['TDDCON'].trim()] += 1;

					markers_data[j]['ar_cli_des'].push(record['k_cli_des']);
					markers_data[j]['ar_cli_des_ext'].push(record['k_cli_des_ext']);					
					
					markers_data[j]['title'] += '<span class=cli>' + record['title'].trim() + '</span>';

					if (sped_collegata == 1)					
						markers_data[j]['volume_sped_collegata'] = parseFloat(markers_data[j]['volume_sped_collegata']) + record['TDVOLU'];
					else					
						markers_data[j]['volume'] = parseFloat(markers_data[j]['volume']) + parseFloat(record['TDVOLU']);

				} 
			}
			
			if (record['DUPLICATO'] != 'Y'){

				record['el_clienti'] = [];
				record['ar_cli_des'] = [];
				record['ar_cli_des_ext'] = [];
				record['el_clienti'][record['TDDCON'].trim()] = 1;

				record['ar_cli_des'].push(record['k_cli_des']);;
				record['ar_cli_des_ext'].push(record['k_cli_des_ext']);				
				
				record['volume'] = 0;				
				record['volume_sped_collegata'] = 0;				
				
				if (sped_collegata == 1){
				  record['volume_sped_collegata'] = record['TDVOLU'];					
				  //c_icon = '888888';
				  c_icon = 'grey';
				}
				else {
				  record['volume'] = record['TDVOLU'];					
				  //c_icon = '000000';
				  c_icon = 'black';
				}					
				
				indexRecord = markers_data.length + 1;
	        	record['counter'] = indexRecord;
				record['indexRecord'] = indexRecord;
				record['sped_collegata'] = sped_collegata;	        	
				record['icon'] = {
						//'url' : 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + indexRecord + '|' + c_icon + '|FFFFFF',
						'url': 'iconsnew/' + c_icon + indexRecord + '.png',
						'size': new google.maps.Size(32, 32)
				};	        	
				if (record['ha_coordinate'] == 'Y'){
					//aggiunto il punto					
					markers_data.push(record);					
				}	
				else 
					errors_data.push(record);
			}

        }

        //rimuovo i punti precedenti e rigenero tutti i markers
			map.removeMarkers();
			$('#sortable').empty();			
			

			madd = [];
			for (var i = 0; i < markers_data.length; i++) {

				rm = [];

				//rm['title'] = markers_data[i]['title'];
				rm['title_li'] = '';
				for (key in markers_data[i]['el_clienti']) {
					rm['title_li'] += '<span class=cli>- ' + key;
					if (markers_data[i]['el_clienti'][key] > 1)
					 rm['title_li'] += ' (' + markers_data[i]['el_clienti'][key] + ')';	
					rm['title_li'] += '</span>';
				}

				rm['title'] = '';
				for (key in markers_data[i]['el_clienti']) {
					rm['title'] += key;
				}				
								
				rm['ind_dest'] = markers_data[i]['ind_dest'];
				rm['volume'] = markers_data[i]['volume'];								
				rm['volume_sped_collegata'] = markers_data[i]['volume_sped_collegata'];				
				rm['icon'] = markers_data[i]['icon'];				
				rm['DEC_DEST'] = markers_data[i]['DEC_DEST'];				
				rm['TDDCON'] = markers_data[i]['TDDCON'];				
				rm['TDDLOC'] = markers_data[i]['TDDLOC'];				
				rm['lat'] = markers_data[i]['LAT'];
				rm['lng'] = markers_data[i]['LNG'];
				rm['sped_collegata'] = markers_data[i]['sped_collegata'];				
				madd.push(rm);
			}

			map.addMarkers(madd);			
    		//map.addMarkers(markers_data);        
  		
      }

    }

    function loadErrorResults () {
		for (var i=0; i< errors_data.length; i++){ 
			//$('#error_results').append('<li><a href="#">' + errors_data[i].TDDLOC + '<BR><span class=ragsoc>' + errors_data[i].TDDCON + '</span></a></li>');			
		}                     

      }    
    






    
    function printResults(data) {
      $('#foursquare-results').text(JSON.stringify(data));
      prettyPrint();
    }

    $(document).on('click', '.pan-to-marker', function(e) {        
      e.preventDefault();

      var position, lat, lng, $index;

      $index = $(this).data('marker-index');

      position = map.markers[$index].getPosition();

      lat = position.lat();
      lng = position.lng();
      map.setCenter(lat, lng);
    });


    $(document).on('click', '.click-to-select-scarico', function(e) {
        e.preventDefault();        
    	$index = $(this).data('marker-index');        
    	if ($(this).attr('data-is-selected') == 1)        
			is_selected = 0;
		else	
			is_selected = 1;

		if (is_selected == 1){
			//verifico il volume max della seconda spedizione
			v = $( "#volume_seconda_spedizione" ).text();
			vmax = $( "#volume_max_seconda_spedizione" ).text();
			vrec = markers_data[$index]['volume'];
			if (isNaN(parseFloat(v))) v = 0;
			if (vmax > 0 && (parseFloat(v) + parseFloat(vrec) > parseFloat(vmax))){
				acs_show_msg_error('Superato volume massimo');
				return false;
			}
		}			

		$(this).attr('data-is-selected', is_selected);
		markers_data[$index]['is_selected'] = is_selected; 

		/*
		if (is_selected == 1)
		 c_icon = '0000FF';
		else
		 c_icon = '000000';
		*/ 
		
		v_index = $index + 1;
		
		if (is_selected == 1)
		  var m_url = 'iconsnew/red' + v_index + '.png';
		else
		  var m_url = 'iconsnew/black' + v_index + '.png';
		
		map.markers[$index].setIcon({			
				//'url' : 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + v_index + '|' + c_icon + '|FFFFFF',
				'url': m_url,
				'size': new google.maps.Size(32, 32)
		});
		   	
		ricalcola_volume_seconda_spedizione();				
      });
    
    
    
    $(document).ready(function(){

  	  window.moveTo(0, 0);
	  window.resizeTo(screen.availWidth, screen.availHeight);    	
        
      prettyPrint();
      
      map = new GMaps({
        div: '#map',
        lat: 43.9124756,
        lng: 12.915549400000032
      });

      map.on('marker_added', function (marker) {
        var index = map.markers.indexOf(marker);

        if (parseFloat(marker.volume_sped_collegata) > 0)
            append_voluume_sped_collegata = '<br>[' + floatRenderer2(marker.volume_sped_collegata) + ']';
        else
        	append_voluume_sped_collegata = '';       
        
        $('#sortable').append('<li class=sped_collegata_' + marker.sped_collegata + '><table width=\'100%\'><tr><td class=v0><a href="#" class="pan-to-marker" data-marker-index="' + index + '"><span class=num>' + (index +1) + '</span></a></td><td><span class=cli>' + marker.title_li + '</span><BR><span class=ind_dest>' + marker.ind_dest + '</span></td><td align=right class=v1><a href="#" class="click-to-select-scarico" data-is-selected="0" data-marker-index="' + index + '">' + floatRenderer2(marker.volume) + '</a>' + append_voluume_sped_collegata + '</td></tr></table></li>');

        if (index == map.markers.length - 1) {
          map.fitZoom();
        }
      });

      //var xhr = $.getJSON('acs_form_json_progetta_spedizioni.php?fn=grid_data&for_gmap=Y&<?php echo "sped_id={$sped_id}&data={$data}&tpca={$carico_exp['TDTPCA']}&aaca={$carico_exp['TDAACA']}&nrca={$carico_exp['TDNRCA']}" ?>');
      var xhr = $.getJSON(xhr_string);

      xhr.done(loadResults);      
      xhr.done(loadErrorResults);      
    });



    jQuery(function() {
    	  jQuery("input:button").button();
    	  jQuery("#dialogProgress" ).dialog({
    	    height: 140,
    		modal: true,
    		autoOpen: false
    	  });
    	  jQuery("#savingProgress" ).dialog({
    		    height: 140,
    			modal: true,
    			autoOpen: false
    		  });  
    	  jQuery("#progressBar").progressbar({ value: 0 });

    	});


    
  </script>

    
    <div id="header">
     <table width="98%">
      <tr><td><img src=<?php echo img_path("icone/48x48/gmaps_logo.png") ?>></td>
          <td>
          	<span class="page-title"><strong>Desktop Ordini di Vendita</strong> - Localizza su Google Maps</span>
          	<br><span class="page-sub-title">Itinerario <strong><?php echo $itin_descr;?> [<?php echo $itin_id;?>]</strong> del <strong><?php echo print_date($data);?></strong></span>
          </td>
          

          <td align=right>
            <table class=riepilogo>
             <tr class=prima_spedizione>
             	<td><img id=add_link src=<?php echo img_path("icone/48x48/add_link.png") ?> width=24></td>
             	<td><?php echo "#{$sped_id}, {$caricoo_txt}"?></td>
             	<td align=right>Vol. <span class=grassetto id="volume_prima_spedizione"><?php echo n($sped_volume,2); ?></span></td>
             	<td align=right>Max <span class=grassetto id="volume_max_prima_spedizione"><?php echo n($sped['CSVOLD'], 0) ?></span></td>
             	<td ><?php echo $sped_vmc; ?></td>             	
             	<td><img src=<?php echo img_path("icone/48x48/button_black_repeat.png") ?> width=24 onClick="window.location.reload()"></td>
             </tr>	
             <tr class=seconda_spedizione>
             	<td><img src=<?php echo img_path("icone/48x48/folder_add.png") ?> width=24></td>
             	<td><?php echo "{$sped2_out}" ?></td>
             	<td align=right>Vol. <span class=grassetto id="volume_seconda_spedizione"><?php echo n($sped2_volume,2); ?></span></td>
             	<td align=right>Max <span class=grassetto id="volume_max_seconda_spedizione"><?php echo n($sped2['CSVOLD'], 0) ?></span></td>
             	<td ><?php echo $sped2_vmc; ?></td>             	             	
             	<td>
             	  <?php if ((int)$sped2_id > 0){ ?>
             		<img src=<?php echo img_path("icone/48x48/button_blue_play.png") ?> width=24 onClick="confermaSecondaSpedizione()">
             	  <?php } ?>	
             	</td>
             </tr>	
            </table>          
          </td>          
          
      </table>
    </div>
  
    <div id="body">
    <div class="row">
      <div class="span11">
        <div class="popin">
	      <div class="span6" style="float: left; width: 360px; height: 600px; overflow: scroll;'>
	      
 
	        <h3 style='display: none;'>Destinazioni mancanti:</h3>
	        <ul style='display: none;' id="error_results"></ul>
 	      
	      
	        <h3>Elenco destinazioni:</h3>
	        <ul id="sortable"></ul>

	        
	      </div>        
          <div id="map" style='width: 800px; height: 600px;' class="map"></div>
        </div>
      </div>
      
    </div>
	</div>
	
<div id="dialogProgress" title="Calcolo percorso...">
<div id="savingProgress" title="Salvataggio in corso...">
<div id="progressBar"></div>
</div>	
</body>
</html>
