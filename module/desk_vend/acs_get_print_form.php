<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$initial_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario
$m_week = strftime("%V", strtotime($initial_data_txt)); 
$m_year = strftime("%G", strtotime($initial_data_txt));


//Elenco opzioni "Area spedizione"
$ar_area_spedizione = $s->get_options_area_spedizione();
$ar_area_spedizione_txt = '';

$ar_ar2 = array();
foreach ($ar_area_spedizione as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
$ar_area_spedizione_txt = implode(',', $ar_ar2);

//Elenco opzioni "Tipologia ordini"
$ar_tip_ordini = $s->get_options_tipologia_ordini();
$ar_tip_ordini_txt = '';

$ar_ar2 = array();
foreach ($ar_tip_ordini as $a)
	$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
$ar_tip_ordini_txt = implode(',', $ar_ar2);

?>

{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: false,
            title: '',
            url: 'acs_print_wrapper.php',
			buttonAlign:'center',            
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "PRINT_FORM");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',	            
	            handler: function() {
	                //this.up('form').getForm().isValid();
	                //this.up('form').submit({target: '_blank'});
	                this.up('form').submit({
                        url: 'acs_print_wrapper.php',
                        target: '_blank', 
                        standardSubmit: true,
                        method: 'POST',
                        params : {
                        	'tipologia_ordini' : Ext.encode(this.up('form').getForm().findField('sel_tipologia_ordini').getValue()),
                        	'modello' : Ext.encode(this.up('form').getForm().findField('sel_modello').getValue())
                        },
                  });
	                
	            }
	        }],             
            
            items: [
                
                {
					xtype: 'fieldset',
//	                title: 'Scelta tipo report',
	                defaultType: 'textfield',
	                frame: false,
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
	             items: [
					 {
	                    xtype: 'radiogroup',
	                    anchor: '100%',
	                    fieldLabel: '',
	                    allowBlank: false,
	                    frame: false,
	                    items: [{
		                    	xtype: 'label',
		                    	text: 'Tipo report',
		                    	width: 70
		                    }, {
	                            xtype: 'radio'
	                          , name: 'tipo'
	                          , inputValue: 'elenco' 
	                          , boxLabel: 'ELENCO'
	                        },
	                        {
	                            xtype: 'radio'
	                          , name: 'tipo'                            
	                          , inputValue: 'agenda'                            
	                          , boxLabel: 'AGENDA'
	                        },
	                        {
	                            xtype: 'radio'
	                          , name: 'tipo'                            
	                          , inputValue: 'colli'
	                          , boxLabel: 'COLLI'
	                        }
	                    ]
	                }	             
	             ]
	            },       
	            
	            
                {
					xtype: 'fieldset',
	                //title: 'Scelta periodo',
	                defaultType: 'textfield',
	                layout: 'hbox',
		            items: [
						{
							xtype: 'container',
							flex: 2,
							items: [
							        {
										xtype: 'container',
					            		anchor: '100%',
					            		layout: 'hbox',
					            		items: [
							                {
							                    xtype: 'textfield',
							                    name: 'settimana',
							                    value: '<?php echo $m_week ?>',    
							                    fieldLabel: 'Settimana/Anno',
							                    allowBlank: false,			                    
							                    width: 150, labelWidth: 120
							                }, {xtype: 'label', text: ' / ', anchor:'93%', padding: '2 5 0 5'},                                
							                {
							                    xtype: 'textfield',
							                    name: 'anno',    
							                    value: '<?php echo $m_year ?>',
							                    allowBlank: false,
							                    width: 50
							                }                
					            		
					            		]                	
					                },
				                {
				                    xtype: 'checkboxgroup',
				                    fieldLabel: 'Giorni',
				                    labelWidth: 115,
				                    anchor: '100%',
					                defaults: {
					                    anchor: '100%',
					                    width: 30,
					                    padding: '0 3 0 0'
					                },				                    
				                    items: [
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'L'
				                          , inputValue: '1'
				                          , flex: 1                          
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'M'
				                          , inputValue: 2  
				                          , flex: 1				                                                  
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'M'
				                          , inputValue: 3
				                          , flex: 1				                                                    
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'G'
				                          , inputValue: 4
				                          , flex: 1				                                                    
				                        },
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'V'
				                          , inputValue: 5
				                          , flex: 1				                                                    
				                        },                
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'S'
				                          , inputValue: 6
				                          , flex: 1				                                                    
				                        },                
				                        {
				                            xtype: 'checkbox'
				                          , name: 'gg[]' 
				                          , boxLabel: 'D'
				                          , inputValue: 7
				                          , flex: 1				                                                    
				                        }
				                    ]
				                }								
		            
							]
						}, {
		                    xtype: 'radiogroup',
		                    layout: 'vbox',
		                    fieldLabel: '',
		                    flex: 1,
		                    items: [
		                        {
		                            xtype: 'radio'
		                          , name: 'sceltadata[]' 
		                          , boxLabel: 'Data Programmata'
		                          , inputValue: 'TDDTEP'
		                          , checked: true
		                        },
		                        {
		                            xtype: 'radio'
		                          , name: 'sceltadata[]' 
		                          , boxLabel: 'Data spedizione'
		                          , inputValue: 'TDDTSP'                          
		                        },
		                        {
		                            xtype: 'radio'
		                          , name: 'sceltadata[]' 
		                          , boxLabel: 'Data scarico'
		                          , inputValue: 'SP.CSDTSC'                          
		                        }
		                   ]
		                }
						
						
						
		             ]
	             }, {
					xtype: 'fieldset',
	                title: '',
	                layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [
	             
	             

            , {
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Area di spedizione',
					defaultType: 'textfield',
		 	        layout: 'hbox',
		 	        items: [	             
	             
	             
					{
					    xtype: 'combo',
					    flex: 1,
					    name: 'area_spedizione',
					    autoSelect: false,
					    allowBlank: true,
					    editable: false,
					    triggerAction: 'all',
					    typeAhead: true,
					    width:120,
					    listWidth: 120,
					    enableKeyEvents: true,
					    mode: 'local',
					    store: [
					        <?php echo $ar_area_spedizione_txt ?>
					    ]
					}, {
					    xtype: 'combo',
					    flex: 1.5,					    
					    fieldLabel: 'Divisione', labelAlign: 'right',
					    name: 'divisione',
					    autoSelect: false,
					    allowBlank: true,
					    editable: false,
					    triggerAction: 'all',
					    typeAhead: true,
					    width:120,
					    listWidth: 120,
					    enableKeyEvents: true,
					    displayField: 'text',
						valueField: 'id',					    
					    mode: 'local',
					    store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}
					}
				]
				}	
					
					
					
					
            , {
			        xtype: 'fieldcontainer',
			        fieldLabel: 'Trasportatore',
					defaultType: 'textfield',
		 	        layout: 'hbox',
		 	        items: [	             
					
					, {
					    xtype: 'combo',
					    flex: 1,
					    fieldLabel: '',
					    name: 'sel_trasportatore',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,
					   	multiSelect: false,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('AVET'), ''); ?>
							    ] 
						}	
					}, {
					    xtype: 'combo',
					    flex: 1.5,
					    fieldLabel: 'Tipologia',  labelAlign: 'right',
					    name: 'sel_tipologia_ordini',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,
					   	multiSelect: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), ''); ?>
							    ] 
						}	
					}
					
				]}	,
				
				{
			        xtype: 'fieldcontainer',
			        defaultType: 'textfield',
			        fieldLabel: 'Stabilimento',
		 	        layout: 'hbox',
		 	        items: [	             
					
					, {
					    
					    xtype: 'combo',
					    flex: 1,
					    fieldLabel: '',
					    name: 'stabilimento',
					    displayField: 'text',
						valueField: 'id',
					 	margin: '5 0 0 0',
						emptyText: '- seleziona -',
					   	allowBlank: true,
					   	multiSelect: false,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
							     <?php echo acs_ar_to_select_json(find_TA_std('START'), ''); ?>
							    ] 
						}	
					}, {
					    xtype: 'combo',
					    flex: 1.5,
					    fieldLabel: 'Modello',
					    margin: '5 0 0 0',
					    labelAlign: 'right',
					    name: 'sel_modello',
					    displayField: 'text',
						valueField: 'id',
						emptyText: '- seleziona -',
					   	allowBlank: true,
					   	multiSelect: true,														
						store: {
							autoLoad: true,
							editable: false,
							autoDestroy: true,	 
						    fields: [{name:'id'}, {name:'text'}],
						    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_modello(), '') ?> 	
								    ]  
						}	
					}
					
				]}		
					, {
					  xtype: 'fieldset',
					  layout: 'vbox',
					  margin: "10 0 0 0",					  
					  items: [
					
					{
					  xtype: 'fieldset',
					  border: false, margin: "0 0 0 0", padding: "0 0 0 0",
					  layout: 'hbox',
					  items: [
						{
		                    xtype: 'radiogroup',
		                    layout: 'hbox',
		                    fieldLabel: '',
		                    flex: 3,
		                    defaults: {
		                     width: 105
		                    },
		                    items: [{
		                    	xtype: 'label',
		                    	text: 'Carico',
		                    	width: 70
		                    },
							{
				                boxLabel: 'Tutti',
				                name: 'carico_assegnato',
				                checked: true,			                
				                inputValue: 'T'
				            }, {
				                boxLabel: 'Con carico',
				                checked: false,			                
				                name: 'carico_assegnato',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'Senza',
				                checked: false,			                
				                name: 'carico_assegnato',
				                inputValue: 'N'                
				            }
		                   ]
		                }, {
								name: 'num_carico',
								xtype: 'textfield',								
								fieldLabel: 'Nr. Carico',
								value: '',
								width: 120, labelWidth: 60,
								labelAlign: 'right'
						}					  
					  ]
					  
					}, 
					
					
					
					, {
					  xtype: 'fieldset',
					  border: false, margin: "5 0 0 0", padding: "0 0 0 0",					  
					  layout: 'hbox',
					  items: [

						{
		                    xtype: 'radiogroup',
		                    layout: 'hbox',
		                    fieldLabel: '',
		                    flex: 3,
		                    defaults: {
		                     width: 105,
		                    },		                    
		                    items: [{
			                    	xtype: 'label',
			                    	text: 'Lotto',
			                    	width: 70
			                    },
							{
				                boxLabel: 'Tutti',
				                name: 'lotto_assegnato',
				                checked: true,			                
				                inputValue: 'T'
				            }, {
				                boxLabel: 'Con lotto',
				                checked: false,			                
				                name: 'lotto_assegnato',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'Senza',
				                checked: false,			                
				                name: 'lotto_assegnato',
				                inputValue: 'N'                
				            }
		                   ]
		                }, {
								name: 'num_lotto',
								xtype: 'textfield',
								fieldLabel: 'Nr. Lotto',
								value: '',
								width: 120, labelWidth: 60,
								labelAlign: 'right'														
							 } 	
			             
					  ]
					  }					
					
					
					
					, {
					  xtype: 'fieldset',
					  border: false, margin: "5 0 0 0", padding: "0 0 0 0",					  
					  layout: 'hbox',
					  items: [

						{
		                    xtype: 'radiogroup',
		                    layout: 'hbox',
		                    fieldLabel: '',
		                    flex: 3,
		                    defaults: {
		                     width: 105,
		                    },		                    
		                    items: [{
			                    	xtype: 'label',
			                    	text: 'Sped.da programm.',
			                    	width: 70
			                    },
							{
				                boxLabel: 'Incluse',
				                name: 'escludi_da_prog',
				                checked: true,			                
				                inputValue: 'T'
				            }, {
				                boxLabel: 'Escluse',
				                checked: false,			                
				                name: 'escludi_da_prog',
				                inputValue: 'Y'
				            }, {
				                boxLabel: 'Solo',
				                checked: false,			                
				                name: 'escludi_da_prog',
				                inputValue: 'N'                
				            }
		                   ]
		                }
			             
					  ]
					  }							
					
					
						             
	             ]
	             },	             
	             					  
					  ]
					  }
					
					

	              	            
	            
                
                
                
               , {
                    xtype: 'tabpanel',
					anchor: '100%',                    
                    activeTab: 0,
                    layout: 'fit',
			        defaults :{
			            bodyPadding: 10
			        },
                    items: [
						{
				            xtype: 'panel',
				            title: 'Elenco',
							items: [

								{
			                    xtype: 'checkboxgroup',
			                    anchor: '100%',
			                    fieldLabel: 'Dettaglia',
			                    items: [
			                    	{
			                            xtype: 'checkbox'
			                          , name: 'dettaglio_per_area_spedizione' 
			                          , boxLabel: 'Area di spedizione'
			                          , inputValue: 'Y'
			                          , width: 180, labelWidth: 120			                          
			                        }, {
			                            xtype: 'checkbox'
			                          , name: 'dettaglio_per_carico' 
			                          , boxLabel: 'Carico'
			                          , inputValue: 'Y'
			                          , width: 90, labelWidth: 120			                          
			                        }, {
			                            xtype: 'checkbox'
			                          , name: 'el_dettaglio_per_cliente' 
			                          , boxLabel: 'Cliente'
			                          , inputValue: 'Y'
			                          , width: 90, labelWidth: 120			                          
			                        }, {
			                            xtype: 'checkbox'
			                          , name: 'el_dettaglio_per_ordine' 
			                          , boxLabel: 'Ordine'
			                          , inputValue: 'Y'
			                          , width: 90, labelWidth: 120			                          
			                        }
									]
								}, {
			                    xtype: 'checkboxgroup',
			                    anchor: '100%',
			                    fieldLabel: 'Visualizza',
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'mostra_pallet' 
			                          , boxLabel: 'Pallet'
			                          , inputValue: 'Y' 
			                          , width: 90, labelWidth: 120			                          
			                        }, {			                        
			                            xtype: 'checkbox'
			                          , name: 'mostra_importo' 
			                          , boxLabel: 'Importo'
			                          , inputValue: 'Y'
			                          , width: 90, labelWidth: 120
			                        }, {			                        
			                            xtype: 'checkbox'
			                          , name: 'mostra_tipologie' 
			                          , boxLabel: 'Tipologie'
			                          , inputValue: 'Y'
			                          , width: 90, labelWidth: 120
			                        }, {			                        
			                            xtype: 'checkbox'
			                          , name: 'dettaglia_km_sped' 
			                          , boxLabel: 'Km spediz.'
			                          , inputValue: 'Y'
			                          , width: 90, labelWidth: 120
			                        }, {			                        
			                            xtype: 'checkbox'
			                          , name: 'mostra_porta' 
			                          , boxLabel: 'Porta'
			                          , inputValue: 'Y'
			                          , width: 90, labelWidth: 120
			                        }
									]
								}, {
			                    xtype: 'checkboxgroup',
			                    anchor: '100%',
			                    fieldLabel: 'Evidenzia',
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'evidenzia_inizio_carico' 
			                          , boxLabel: 'Orario inizio carico'
			                          , inputValue: 'Y' 
			                          , width: 180, labelWidth: 120			                          
			                        }
									]
								}]
				        },{
				            xtype: 'panel',
				            title: 'Agenda',
							items: [

							 {
			                    xtype: 'radiogroup',
			                    layout: 'column',
			                    fieldLabel: 'Totalizza',
			                    allowBlank: false,
			                    defaults: {width: 100, labelWidth: 90},
			                    items: [
			                        {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'
			                          , inputValue: 'volume' 
			                          , boxLabel: 'Volume'
			                          , checked: true
			                        },
			                        {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'                            
			                          , inputValue: 'colli'                            
			                          , boxLabel: 'Colli'                         
			                        },
			                        {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'                            
			                          , inputValue: 'pallet'
			                          , boxLabel: 'Pallet'                        
			                        },
			                        {
			                            xtype: 'radio'
			                          , name: 'ag_f_tot'                            
			                          , inputValue: 'n_ordini'
			                          , boxLabel: 'Nr ordini'			                          
			                        }
			                    ]
			               },{
			                    xtype: 'checkboxgroup',
			                    layout: 'column',
			                    fieldLabel: 'Dettaglio per',
			                    defaults: {width: 100, labelWidth: 90},			                    
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_itinerario' 
			                          , boxLabel: 'Itinerario'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_vettore' 
			                          , boxLabel: 'Vettore'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_mezzo' 
			                          , boxLabel: 'Mezzo'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_cliente' 
			                          , boxLabel: 'Cliente'
			                          , inputValue: 'Y'
			                        }
									]
							}, {
			                    xtype: 'checkboxgroup',
			                    layout: 'column',
			                    fieldLabel: 'Evidenzia',
			                    defaults: {width: 100, labelWidth: 90},			                    
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_orario_spedizione' 
			                          , boxLabel: 'Orario'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_dettaglio_per_tip_spedizione' 
			                          , boxLabel: 'Tipologia'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_solo_ordini' 
			                          , boxLabel: 'Solo ordini'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'ag_dett_carichi' 
			                          , boxLabel: 'Nr carichi'
			                          , inputValue: 'Y'
			                        }]
								} ]				            
				        },{
				            xtype: 'panel',
				            title: 'Colli',
				            items: [
								{
			                    xtype: 'checkboxgroup',
			                    layout: 'column',
			                    defaults: {width: 100, labelWidth: 90},			                    
			                    fieldLabel: 'Dettaglio per',
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'cl_dettaglio_per_tip_produttiva' 
			                          , boxLabel: 'Tipologia collo'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'cl_dettaglio_per_area' 
			                          , boxLabel: 'Area sped.'
			                          , inputValue: 'Y'
			                        },{
			                            xtype: 'checkbox'
			                          , name: 'cl_dettaglio_per_itinerario' 
			                          , boxLabel: 'Itinerario'
			                          , inputValue: 'Y'
			                        }
									]
								}, {
			                    xtype: 'checkboxgroup',
			                    fieldLabel: 'Confronta capacit&agrave;',
			                    layout: 'column',
			                    defaults: {width: 100, labelWidth: 90},			                    
			                    items: [
			                        {
			                            xtype: 'checkbox'
			                          , name: 'cl_confronta_cap_prod' 
			                          , boxLabel: 'Si'
			                          , inputValue: 'Y'
			                        }
									]
								}]


				            
				            

				        },{
				            xtype: 'panel',
				            title: 'Descrizione report',
				            items: [
											{
							                    xtype: 'textfield',
							                    name: 'add_descr_report',    
							                    fieldLabel: '',
							                    allowBlank: true,			                    
							                    width: '100%', anchor: '-10'
							                }				            
				            ]
				        }                                          
                    ]
                 }
                 

                
            ]
        }

	
]}