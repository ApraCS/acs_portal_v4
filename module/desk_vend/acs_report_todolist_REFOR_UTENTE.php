<?php
require_once "../../config.inc.php";
set_time_limit(240);
$main_module = new Spedizioni();
$s = new Spedizioni();

$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");


$ar_email_json = acs_je($ar_email_to);

$m_params = acs_m_params_json_decode();

// ******************************************************************************************
// FORM richiesta parametro
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
	
	global $is_linux;
	if ($is_linux == 'Y')
		$_REQUEST['todo_params'] = strtr($_REQUEST['todo_params'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));	
?>	
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
            
            items: [        
					 {
	                    xtype: 'radiogroup',
	                    anchor: '100%',
	                    fieldLabel: '',
	                    allowBlank: false,
	                    frame: false,
	                    items: [{
		                    	xtype: 'label',
		                    	text: 'Dettaglio ordini',
		                    	width: 70
		                    }, {
	                            xtype: 'radio'
	                          , name: 'dett_ord'
	                          , inputValue: 'Y' 
	                          , boxLabel: 'Si'
	                        },
	                        {
	                            xtype: 'radio'
	                          , name: 'dett_ord'                            
	                          , inputValue: 'N'                            
	                          , boxLabel: 'No'
	                        }

	                    ]
	                }							
					        
					        
				],
			buttons: [{
				            text: 'Visualizza',
					        iconCls: 'icon-folder_search-24',		            
					        scale: 'medium',		            
				            handler: function() {
				                this.up('form').submit({
			                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
			                        params: {todo_params: <?php echo j(acs_je($m_params->todo_params)); ?>},
			                        target: '_blank', 
			                        standardSubmit: true,
			                        method: 'POST'
			                  });
				                
				            }
				        }]             
				
        }
]}	
<?php	
	exit;
} //get_json_data 
?>
<?php
// ******************************************************************************************
// REPORT
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_report'){
	require_once("acs_panel_todolist_data.php");
	
	$giorni_stadio = $s->get_giorni_stadio($da_data, $n_giorni);	
	
?>
<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
div#my_content h1{font-size: 22px; padding: 5px;}
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table.acs_report{border-collapse:collapse; width: 95%; margin-left: 20px; margin-right: 20px;}
table.acs_report td, table.acs_report th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}


<?php if ($_REQUEST['dett_ord'] == 'Y'){ ?>
table.acs_report tr.liv_2 td{font-weight: bold; background-color: #a0a0a0;}
table.acs_report tr.liv_3 td{font-weight: bold; background-color: #f0f0f0;}
<?php } else { ?>
table.acs_report tr.liv_2 td{font-weight: bold; background-color: #f0f0f0;}
<?php } ?>
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'N';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div>

<div id='my_content'>
<?php

function out_numero_ordini($val, $r){
  if ($r['liv'] == 'liv_5' && (int)$val > 0){
  	if ($r['fl_da_prog'] == 1) return 'H'; //hold
  	if ($r['n_stadio']	 == 0) return '?'; //sarebbe il divieto
  	if ($r['n_stadio']	 == 1) return 'I'; //ICE
  	if ($r['n_stadio']	 == 2) return 'S'; //Slush
  	if ($r['n_stadio']	 == 3) return 'W'; //Water  	
  }
   else return $val;	
  
}


function stampa_tr($r, $title_cell = ''){
	global $s;
	
	$r['stato_out'] = '';
	if ($r['liv'] == 'liv_5') $r['stato_out'] = $r['stato'];
	
	?>
	<TR class='<?php echo $r['liv']; ?>'>
	

	<?php if ($r['liv'] == 'liv_2'){ ?>
	 <td colspan=12>
	 	<?php
	 		echo "Stato ordine " . $r['task'];
	 	?>
	 </td>
	<?php return; } ?>
	
	<?php if ($r['liv']	 == 'liv_5') { ?>
		<td><?php echo print_date($r['data_assegnazione']) . " " . print_ora($r['ora_assegnazione']); ?></td>
		<td><?php echo print_date($r['data_scadenza']); ?></td>
	<?php } else {?>
		<td colspan=2>&nbsp;</td>
	<?php } ?>	
	
	 <td>
	 	<?php
	 	 if ($r['liv'] == 'liv_5')
	 		echo $s->k_ordine_out($r['k_ordine']);
	 	 else
	 	 	echo $r['task'];
	 	?>
	 </td>
	 
	 
	 <td>
	 	<?php if ($r['liv'] == 'liv_5') echo trim($r['riferimento']); ?>
	 	
	 	<?php
	 	 if ($r['iconCls'] == 'iconBlocco'){
	 	 	if (strlen(trim($r['riferimento'])) > 0)
	 	 		echo "<br/>";
	 	 	echo " [ BLOCCATO ]";
	 	 }
	 	 ?>	 	 	 	
	 	
	 </td>
	 <td width=30><?php echo trim($r['tipo']); ?></td>
	 <td width=60><?php echo print_date($r['data_reg']); ?></td>	 
	 
	 <td width=30><?php echo trim($r['priorita']); ?></td>
	 <td width=60><?php echo print_date($r['cons_rich']); ?></td>
	 <td width=60><?php echo print_date($r['cons_prog']); ?></td>	 
	 
	 <td width=70 class=number><?php if ($r['liv'] == 'liv_5') echo n($r['importo'], 2); ?></td>	 
	 <td width=70 class=number><?php if ($r['liv'] == 'liv_5') echo n($r['colli'], 0); ?></td>
	 <!-- blocco ordine (azzurro) -->
	 <td>
	  <?php
	  	if ($r['fl_bloc']==2 || $r['fl_bloc']==4) //Ordine
	  		echo "O";
	  ?>

	 <?php
	 	if ($r['fl_bloc']==3 || $r['fl_bloc']==4) //Fido
	 		echo "F";	 
	  ?>
	 </td>
	 	 
	</TR>		
<?php		
} 



 //Recupero dei dati
 $livs['0'] = 'IMMOR';
 $livs['1'] = $_REQUEST['id_selected'];
 $filtro 	= imposta_filtri($livs);
 $order_by = get_order_by_by_livs($livs); 
 $items 	= get_elenco_ordini_arrivi($filtro, $order_by); 
 $ar = crea_array_valori_el_ordini_arrivi($items, $tipo_elenco = "", "root", 'Y');
 
//stampo il report 
?>

<?php

//report
foreach($ar['det']['IMMOR']['children'] as $k_ute => $ar_ute){
?>
  <table class='acs_report'>
	<tr>
	 <th colspan=7><h1>Elenco ordini assegnati</h1></th>
 	 <th colspan=8 class=number><h1>Utente: <?php echo $k_ute; ?></h1></th>
 	</tr> 
  </table>	
	<br/>
	<table class='acs_report'>

	<?php	
	
	foreach($ar_ute['children'] as $k_cli => $ar_cli){

    	stampa_tr($ar_cli);
    ?>	
    	<TR class='<?php echo $r['liv']; ?>'>
    	
    	<td>Assegnazione</td>
    	<td>Scadenza</td>
    	
    	<td>Cliente</td>
    	
    	<td>Riferimento</td>
    	<td>Tipo</td>
    	<td>Data</td>
    	
    	<td>Pr</td>
    	<td>Cons.Richiesta</td>
    	<td>Evas.Programm.</td>
    	
    	<td class=number>Importo</td>
    	<td class=number>Colli</td>
    	<!-- blocco ordine (azzurro e nero) -->
    	<td width=30>Blocco<br/>ordine</td> <!-- Azzurro -->
    	
    	</TR>    	
	<?php 	
		foreach($ar_cli['children'] as $k_aspe => $ar_aspe){
			stampa_tr($ar_aspe);

			if ($_REQUEST['dett_ord'] == 'Y' && is_array($ar_aspe['children']))
			foreach($ar_aspe['children'] as $k_ord => $ar_ord){
				stampa_tr($ar_ord);
			}
			
		}
	}
	
	echo "</table>";
	echo "<br/>";
	
	
	
}	
 
?>

 </div>
</body>
</html> 
<?php
 exit;	
}
?>