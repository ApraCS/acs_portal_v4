<?php

require_once("../../config.inc.php");

$main_module = new Spedizioni();
$s = new Spedizioni();


/* ESEMPIO DI FILE BAT
----------------------------------------
@echo off
k:
cd k:\
cd k:\Apra\Apra\Sma1Bar

set /p parametri_sma1bar=< %1%

Sma1Bar.exe %parametri_sma1bar%
----------------------------------------    
 */

// ******************************************************************************************
// download file .bat per esecuzione sma1bar
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_bat_file'){

	header("Content-Description: Sma1bar open file");
	header("Content-Type: application/octet-stream");
	header("Content-disposition: attachment; filename=\"abcde.sma1bar\"");

	$nrdo = $_REQUEST['nrdo'];
	$nrdo_1 = substr($nrdo, 0, 2);
	$nrdo_2 = substr($nrdo, 2, 2);	
	
	//echo "ORD1224;44444;13;14;15;ORD";
	echo implode(";", array($nrdo, "NWO", $nrdo, $nrdo_1, $nrdo_2, $nrdo));
	exit;
}


if ($_REQUEST['fn'] == 'exe_cancella_articoli'){

	$use_session_history = microtime(true);

	$m_params = acs_m_params_json_decode();

	$nrec= $m_params->rows;

	$sh = new SpedHistory();
	$sh->crea(
			'cancella_articoli',
			array(
					"k_ordine"			=> $m_params->k_ordine,
					"nrec"		        => $nrec

			)
			);
		

	exit;
}

// ******************************************************************************************
// EXE modifica info testata (pagamento)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_modifica_info_testata'){
	$m_params = acs_m_params_json_decode();
	
	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= str_repeat("0", 99); //non servono tutti gli altri parametri
	$cl_p .= sprintf("%-50s", trim($m_params->TDDOCU));
	$cl_p .= sprintf("%-1s", 'M'); //modifica
	$cl_p .= sprintf("%-3s", trim($m_params->f_pagamento));
	$cl_p .= sprintf("%-10s", ''); //progetto grafico
	$cl_p .= sprintf("%-1s", trim($m_params->f_invio_conferma));
	$cl_p .= sprintf("%-1s", trim($m_params->f_attesa_conferma));
	$cl_p .= sprintf("%-1s", trim($m_params->f_preferenza));
	
	$cl_p .= sprintf("%-10s", trim($auth->get_user()));
	
	$cl_in 	= array();
	$cl_out = array();
	
	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21H5C('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('UR21H5C', $libreria_predefinita_EXE, $cl_in, null, null);
	}
		
	
	$ret = array();
	if ($call_return){
		$ret['success'] = true;
		$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
	} else {
		$ret['success'] = false;
	}
	
	echo acs_je($ret);
exit;
}


if ($_REQUEST['fn'] == 'exe_cancella_ordine'){
	ini_set('max_execution_time', 3000);
	$m_params = acs_m_params_json_decode();

	//costruzione del parametro
	$cl_p = str_repeat(" ", 246);
	$cl_p .= str_repeat("0", 99); //non servono tutti gli altri parametri
	$cl_p .= sprintf("%-50s", trim($m_params->k_ordine));
	$cl_p .= sprintf("%-1s", 'A'); //annulla
	
	/*print_r($cl_p);
	exit;*/
	
	$cl_in 	= array();
	$cl_out = array();
	
	if ($useToolkit == 'N'){
		//per test in Apra
		$qry1	 =	"CALL {$libreria_predefinita_EXE}.UR21H5C('{$cl_p}')";
		$stmt1   = 	db2_prepare($conn, $qry1);
		$result1 = 	db2_execute($stmt1);
		$call_return['io_param']['LK-AREA'] = "                                                                                                                                                                                                                                                      1 VODDPE201426001777720140108000000005 ACC    prova ACS                        940029000";
		//FINE test per Apra
	} else {
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
			
		$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
		$call_return = $tkObj->PgmCall('UR21H5C', $libreria_predefinita_EXE, $cl_in, null, null);
	}
	
	$val = substr($call_return['io_param']['LK-AREA'], 395, 1);
	
	$ret = array();
	if ($val == 'A'){
		$ret['success'] = true;
		//$ret['call_return'] 	= $call_return['io_param']['LK-AREA'];
	} else {
		$ret['success'] = false;
		$ret['error_message'] = 'Errore: ordine non cancellato';
	}
	
	echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// FORM modifica info testata (pagamento, stato, ...)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'check_cliente_riferimento'){
	$m_params = acs_m_params_json_decode();
	$cliente= $m_params->f_cliente_cod;
	$rif= strtoupper($m_params->vsrf);
	$ret = array();
	
	
	if ($cfg_mod_Spedizioni['disabilita_sprintf_codice_cliente'] == 'Y')
		$where = " AND TD.TDCCON = " . sql_t($cliente);
	else
		$where = " AND TD.TDCCON = " . sql_t(sprintf("%09s", $cliente));
	
	
	$sql = "SELECT TDDTEP, TDDSST, TDOADO, TDONDO
	FROM {$cfg_mod_Spedizioni['file_testate']} TD 
    WHERE " . $s->get_where_std() . "AND UPPER(TDVSRF)=" . sql_t($rif) . " $where";
	

	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while ($row = db2_fetch_assoc($stmt)) {
	
	$ret['num_find']++;
	
		if(!isset($ar_r['num_ord']))
		$ar_r['num_ord'] = array();
		
	
	
		if (!in_array($row['TDONDO'],	$ar_r['num_ord'])) {
			array_push($ar_r['num_ord'], "nr ordine ".trim($row['TDONDO'])." del ".print_date($row['TDDTEP']));
			
		}
	
		$row['ordini_out'] = implode('<br> ' , $ar_r['num_ord']);

		$ret['message'] = implode('<br>' ,  array($row['ordini_out']));
		
	
	}

	
	$ret['success'] = true;
    echo acs_je($ret);
	exit;
}



// ******************************************************************************************
// FORM modifica info testata (pagamento, stato, ...)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'modifica_info_testata'){
	$m_params = acs_m_params_json_decode();
	$ord = $s->get_ordine_by_k_docu($m_params->tddocu);
	$k_ord_out = $s->k_ordine_out($m_params->tddocu);
?>
{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            layout: {
			    type: 'vbox',
			    align: 'stretch',
			    pack : 'start',
			},
			
			
			buttons: [{
		            text: 'Salva',
		            iconCls: 'icon-windows-32',
		            scale: 'large',
		            handler: function() {
		            	var form = this.up('form').getForm();
		            	var loc_win = this.up('window');
		            	
 						if(form.isValid()){ 		
	 						//Call protocollazione
							Ext.Ajax.request({
							        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_modifica_info_testata',
							        method     : 'POST',
							        waitMsg    : 'Data loading',
	 								jsonData:  form.getValues(),  
							        success : function(result, request){
										jsonData = Ext.decode(result.responseText);
	 									loc_win.fireEvent('afterUpdateRecord', loc_win);
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							            loc_win.fireEvent('afterUpdateRecord', loc_win);
							        }
							  });			
 						}

		            }
		        }
		    ],
			
			items: [
            		{
                		xtype: 'hidden',
                		name: 'TDDOCU',
                		value: '<?php echo $ord['TDDOCU'] ?>'
                	}, 
                	
                	{xtype: 'displayfield', fieldLabel: 'Ordine',  value: <?php echo j(trim($k_ord_out)) ?>},
                	{xtype: 'displayfield', fieldLabel: 'Cliente', value: <?php echo j(trim($ord['TDDCON']) . ' ' .  trim($ord['TDCCON'])) ?>},                	

					{xtype: 'displayfield', fieldLabel: 'Log Apra', value: <?php echo j(implode(',', array($ord['TDDTGE'], $ord['TDORGE'], $ord['TDDOCU']))) ?>},
                	
                	{
							name: 'f_pagamento',
							xtype: 'combo',
							fieldLabel: 'Pagamento', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php //echo acs_ar_to_select_json($s->get_options_pagamento(), '') ?>
								     <?php echo acs_ar_to_select_json(find_TA_std('PAGAM'), '') ?> 	
								    ] 
							},
							value: <?php echo j(trim($ord['TDCPAG'])) ?>
					}, {
							name: 'f_invio_conferma',
							xtype: 'combo',
							fieldLabel: 'Invio conferma', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: '', text: ''}, {id: 'S', text: 'S'}, {id: 'N', text: 'N'}
								    ] 
							},
							value: <?php echo j(trim($ord['TDFU01'])) ?>
					}, {
							name: 'f_attesa_conferma',
							xtype: 'combo',
							fieldLabel: 'Attesa conferma', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								       {id: '', text: ''}, {id: 'S', text: 'S'}, {id: 'N', text: 'N'}
								     , {id: '1', text: '1'}, {id: '2', text: '2'}, {id: '3', text: '3'}, {id: '4', text: '4'}								     
								    ] 
							},
							value: <?php echo j(trim($ord['TDFU02'])) ?>
					}, {
							name: 'f_preferenza',
							xtype: 'combo',
							fieldLabel: 'Preferenza', labelAlign: 'left',
							displayField: 'text',
							valueField: 'id',
							anchor: '-15',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     {id: '', text: ''}, {id: 'A', text: 'A'}, {id: 'D', text: 'D'}
								    ] 
							},
							value: <?php echo j(trim($ord['TDFU03'])) ?>
					}, {
							xtype: 'displayfield',
							fieldLabel: 'Stato ordine',
							value: <?php echo j(trim($ord['TDDSST'])) ?>		
							
					}, {
							xtype: 'displayfield',
							fieldLabel: 'Stato cliente',
							value: <?php echo j(trim($ord['TDDSRI'])) ?>		
							
					}
			]
		}
	]
}
<?php
 exit;
}

$addmenu = "
		    
		      voci_menu.push({
	          		text: 'Cancella ordine',
	        		iconCls : 'iconDelete',	
		
	        		handler: function() {
		
		
		         Ext.Msg.confirm('Richiesta conferma', 'Confermi la cancellazione?', function(btn, text){																							    
					   if (btn == 'yes'){
					
		                Ext.getBody().mask('Retrieving image', 'loading').show();
	        			Ext.Ajax.request({
						   url        : '" . $_SERVER['PHP_SELF'] ."?fn=exe_cancella_ordine',
						   method: 'POST',
						   jsonData: {
						   	k_ordine: rec.get('tddocu') 
						   }, 
						   
						   success: function(result, request) {
						   	  Ext.getBody().unmask();
							  var jsonData = Ext.decode(result.responseText);
		                     
							  rec.set('vsrf', '##CANCELLATO##')
		
						   },									
			
			
						   failure: function(response, opts) {
						      Ext.getBody().unmask();
						      alert('error in get image');
						   }
						});						         	
			         	
						}
					   }); 
					  
        		}
    		});
		
 		";

?>
{"success":true, "items": [
	{
		id: 'panel-protocollazione',
		title: 'Heading',
		xtype: 'panel',
        loadMask: true,
        closable: true,		
        layout: 'fit',
		cfg_protocollazione : <?php echo acs_je($cfg_protocollazione); ?>,
		listeners: {
		
	 			afterrender: function (comp) { 			
								<?php $cl = new SpedProtocollazione(); ?>
								<?php echo $cl->out_Writer_Form($cl->out_Writer_Form_initComponent(), "spedprotocollazione") ?>
								<?php echo $cl->out_Writer_Grid($cl->out_Writer_Grid_initComponent_columns(), "spedprotocollazione") ?>
								<?php echo $cl->out_Writer_Model("spedprotocollazione") ?>
								<?php echo $cl->out_Writer_Store("spedprotocollazione") ?>
								<?php echo $cl->out_Writer_sotto_main("spedprotocollazione", 0.4) ?>
								<?php echo $cl->out_Writer_main("Ordini protocollati sessione corrente", "spedprotocollazione", "", 1, 'false', null, "", $addmenu) ?>								

								comp.add(main);
								comp.doLayout();
									 			
	 				}
	 	} 	
		
	}
]
}