<?php

require_once "../../config.inc.php";

$s = new Spedizioni(array('no_verify' => 'Y'));

$m_params = acs_m_params_json_decode();

if ($_REQUEST['fn'] == 'exe_assegna'){
    
    $ret = array();
    
    $sh = new SpedHistory();
    $ret_RI = $sh->crea(
        'pers',
        array(
            "messaggio"	=> 'ASS_BCCO',
            "k_ordine" => $m_params->k_ordine,
            "vals" => array(
                "RICITI" => $m_params->row->codice,
                "RIIDPA" => $m_params->prog,
            )
        )
        );
    
    
    $sql = "SELECT ASFLRI FROM {$cfg_mod_Spedizioni['file_assegna_ord']} WHERE ASIDPR = ?";
    $stmt = db2_prepare($conn, $sql);
    $result = db2_execute($stmt, array($m_params->prog));
    $r = db2_fetch_assoc($stmt);
   
    $ret['success'] = true;
    $ret['ASFLRI'] = $r['ASFLRI'];
    $ret['ret_RI'] = $ret_RI;
    echo acs_je($ret);
    exit;
    
}
// ******************************************************************************************
// ELENCO CONDIZIONI COMMERCIALI PER CLIENTE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_list_cc'){
  
    $ret = array();
    
    $m_params = $m_params->open_request;
     
    $sql_cc = "SELECT *
               FROM {$cfg_mod_Gest['ins_new_anag']['file_appoggio_cc']} CC
               LEFT OUTER JOIN {$cfg_mod_Gest['ins_new_anag']['file_appoggio']} GC
	               ON GC.GCDT = CC.CCDT AND GC.GCPROG = CC.CCPROG
               WHERE CCDT = '{$id_ditta_default}' AND GCCDCF = '{$m_params->cliente}'
               AND CCSEZI = 'BCCO' AND CCCCON <> 'STD'
               ORDER BY CCDTVF DESC, CCDTVI DESC";
    
    $stmt_cc = db2_prepare($conn, $sql_cc);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt_cc);
    echo db2_stmt_errormsg($stmt_cc);
            
            
    while ($row_cc = db2_fetch_assoc($stmt_cc)) {
        $n = array();
        $n['codice'] = trim($row_cc['CCCCON']);
        $n['descrizione'] = $row_cc['CCDCON'];
        $n['prog'] = $row_cc['CCPROG'];
        $n['ditta'] = $row_cc['CCDT'];
        $n['condizioni'] = $row_cc['CCSEZI'];
        $n['dettagli'] = "";
        if(trim($row_cc['CCAG1']) != ''){
            $ta_sys_ag = find_TA_sys('CUAG', trim($row_cc['CCAG1']));
            $n['dettagli'] .= "Agente: ".$ta_sys_ag[0]['text']." [".trim($row_cc['CCAG1'])."]";
        }
        
        
        if($row_cc['CCPA1'] > 0){
            if(trim($row_cc['CCAG1']) != '')
                $n['dettagli'] .= ", ". n($row_cc['CCPA1'], 0)."%";
                else
                    $n['dettagli'] .= "Agente: , ". n($row_cc['CCPA1'], 0)."%";
        }
        
        
        if(trim($row_cc['CCREFE']) != ''){
            $ta_sys_rf = find_TA_sys('BREF', trim($row_cc['CCREFE']));
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Referente: ".$ta_sys_rf[0]['text']." [".trim($row_cc['CCREFE'])."]";
        }
        if(trim($row_cc['CCPAGA']) != ''){
            $ta_sys_pg = find_TA_sys('CUCP', trim($row_cc['CCPAGA']));
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Pagamento: ".$ta_sys_pg[0]['text']." [".trim($row_cc['CCPAGA'])."]";
        }
        
        //des_banca
        if (strlen(trim($row_cc['CCABI'])) > 0 && strlen(trim($row_cc['CCCAB'])) > 0) {
            $sql = "SELECT * FROM {$cfg_mod_Gest['file_CAB']} WHERE XABI=? AND XCAB=?";
            $stmt = db2_prepare($conn, $sql);
            echo db2_stmt_errormsg();
            $result = db2_execute($stmt, array($row_cc['CCABI'], $row_cc['CCCAB']));
            $r_cab = db2_fetch_assoc($stmt);
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Banca: ".trim($r_cab['XDSABI']) ."-".trim($r_cab['XDSCAB']);
        }
        
        if(trim($row_cc['CCSC1']) > 0){
            if(trim($n['dettagli']) != '') $n['dettagli'] .= "<br>";
            $n['dettagli'] .= "Sconti: ".n($row_cc['CCSC1'],2)."+".n($row_cc['CCSC2'],2)."+".n($row_cc['CCSC3'],2)."+".n($row_cc['CCSC4'],2);
        }
        
        if(trim($row_cc['CCLIST']) != ''){
            if(trim($row_cc['CCSC1']) > 0)
                $n['dettagli'] .= " [Listino ".trim($row_cc['CCLIST'])."]";
            else
                $n['dettagli'] .= "<br> [Listino ".trim($row_cc['CCLIST'])."]";
        }
        
        $n['val_ini'] = $row_cc['CCDTVI'];
        $n['val_fin'] = $row_cc['CCDTVF'];
        if($row_cc['CCDTVF'] > 0 && $row_cc['CCDTVF'] < oggi_AS_date())
            $n['scaduta'] = 'Y';
          
        $ret[] = $n;
    }
    
    echo acs_je($ret);
    exit;
}




if ($_REQUEST['fn'] == 'open_grid'){?>

				
{"success":true, "items": [

			{
				xtype: 'grid',
		        loadMask: true,
		        store: {
					xtype: 'store',
					//groupField: 'tipo',
					autoLoad:true,
			        proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_list_cc',
								method: 'POST',								
								type: 'ajax',
								
						      	actionMethods: {
						          read: 'POST'
						      	},
								
			                    extraParams: {
										open_request: <?php echo acs_je($m_params) ?>,
									}
			        			, doRequest: personalizza_extraParams_to_jsonData	
								, reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['cdcf', 'dettagli', 'tipo', 'codice', 'descrizione', 'prog', 'ditta', 'condizioni', 'val_ini', 'val_fin', 'scaduta']							
			
			}, //store
	
	            multiSelect: false,
		        singleExpand: false,
		
				columns: [	
		    		  {text: 'Codice', width: 50, dataIndex: 'codice'}
		    		, {text: 'Descrizione', width: 300, dataIndex: 'descrizione'}
		    		, {text: 'Validit&agrave; iniziale', width: 95, dataIndex: 'val_ini', renderer : date_from_AS}
		    		, {text: 'Validit&agrave; finale', width: 90, dataIndex: 'val_fin', renderer : date_from_AS}
		    		, {text: '&nbsp;', flex: 1, dataIndex: 'dettagli'}
				],
				enableSort: true
				
				
	        	, listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
						  	acs_show_win_std('Dettagli condizione commerciale', 
									'../desk_gest/acs_panel_ins_new_anag.php?fn=open_form_COMM', 
									{   mode: 'EDIT',
						  				modan: 'ToDo',
						  				CCDT: rec.get('ditta'),
						  				CCPROG: rec.get('prog'),
						  				CCSEZI: rec.get('condizioni'),
						  				CCCCON: rec.get('codice'),
						  				cdcf : rec.get('cdcf'),
						  				from_todo : 'Y'
						  			}, 750, 600, null, 'icon-globe-16');	
						  	
	            			my_grid = iView;
	                        
							
			    				
						  	
						  }
					  }
					  
					  , itemcontextmenu : function(grid, rec, node, index, event) {
			          		event.stopEvent();
				  													  
					 		var voci_menu = [];
				     		var row = rec.data;
				     		var m_grid = this;
				     	    var loc_win = this.up('window');
				     		
			     		    voci_menu.push({
			         		   text: 'Assegna all\'ordine',
			        		   iconCls : 'icon-leaf-16',          		
			        		   handler: function () {
			        		   
					    		  Ext.Ajax.request({
								        url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_assegna',
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				k_ordine : <?php echo j($m_params->k_ordine)?>,
					        				prog : <?php echo j($m_params->prog)?>,
										},							        
								        success : function(result, request){
								          var jsonData = Ext.decode(result.responseText);
								          if (jsonData.ret_RI.RIESIT == 'W'){
											 	acs_show_msg_error(jsonData.ret_RI.RINOTE);
											 	button.enable();
											 	return;
										  }else{   
											 loc_win.fireEvent('afterAssegna', loc_win, jsonData.ASFLRI);     	 	
					            		  }
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });
				                }
		    				});
				     		
				     		
				     		var menu = new Ext.menu.Menu({
        				    items: voci_menu
        					}).showAt(event.xy);	
					    
					   }
				},
				viewConfig: {
		        	getRowClass: function(record, index) {
		        
    		           if (record.get('scaduta') == 'Y')
    		           		return ' colora_riga_rosso';	         		
		        		           		
		           return '';																
		         }   
		    }	
				

											    
				    		
	 	}


]}


<?php 
}
