<?php

require_once "../../config.inc.php";


$tataid = 'SPINT';

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();

/********************************************************
 * RICHESTA rimozione spedizione collegata
 ********************************************************/
if ($_REQUEST['fn'] == 'exe_save'){

	$sped_id = sprintf("%08s", $m_params->form_values->sped_id);
	
	//verifico se gia' esiste
	$sql = "SELECT COUNT(*) as C_ROW FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($id_ditta_default, $tataid, $sped_id));
	$row = db2_fetch_assoc($stmt);
	
	if ($row['C_ROW'] == 0) {
		//INSERT
		$ar_ins = array();
		$ar_ins['TADT'] 	= $id_ditta_default;
		$ar_ins['TATAID'] 	= $tataid;
		$ar_ins['TAKEY1'] 	= $m_params->form_values->sped_id;
		$ar_ins['TAMAIL'] 	= $m_params->form_values->f_email;
		$ar_ins['TALOCA'] 	= $m_params->form_values->f_dogana;
		$ar_ins['TARIF2'] 	= $m_params->form_values->f_booking;
		$ar_ins['TAINDI'] 	= $m_params->form_values->f_email_2;
		$ar_ins['TADESC'] 	= utf8_decode($m_params->form_values->f_note_agg);

		$ar_ins['TAUSGE'] 	= $auth->get_user();
		$ar_ins['TADTGE']   = oggi_AS_date();
		$ar_ins['TAORGE'] 	= oggi_AS_time();		
		
		$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_tabelle']}(" . create_name_field_by_ar($ar_ins) . ") VALUES (" . create_parameters_point_by_ar($ar_ins) . ")";		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, $ar_ins);		
	} else {
		//UPDATE
		$ar_ins = array();
		$ar_ins['TAMAIL'] 	= $m_params->form_values->f_email;
		$ar_ins['TALOCA'] 	= $m_params->form_values->f_dogana;
		$ar_ins['TARIF2'] 	= $m_params->form_values->f_booking;
		$ar_ins['TAINDI'] 	= $m_params->form_values->f_email_2;
		$ar_ins['TADESC'] 	= utf8_decode($m_params->form_values->f_note_agg);

		$ar_ins['TAUSGE'] 	= $auth->get_user();
		$ar_ins['TADTGE']   = oggi_AS_date();
		$ar_ins['TAORGE'] 	= oggi_AS_time();
		
		$sql = "UPDATE {$cfg_mod_Spedizioni['file_tabelle']} SET " . create_name_field_by_ar_UPDATE($ar_ins) . " WHERE  TADT = ? AND TATAID = ? AND TAKEY1 = ?";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt, array_merge(
				$ar_ins,
				array($id_ditta_default, $tataid, $m_params->form_values->sped_id)
		));
		echo db2_stmt_errormsg($stmt);		
	}

	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}





$sped_id = sprintf("%08s", $m_params->sped_id);

//recuper i dati gi' inseriti
$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_tabelle']} WHERE TADT = ? AND TATAID = ? AND TAKEY1 = ?";
$stmt = db2_prepare($conn, $sql);
$result = db2_execute($stmt, array($id_ditta_default, $tataid, $sped_id));
$row = db2_fetch_assoc($stmt);


//verifico se almeno un ordine della spedizione ha TDFIL1='Y'
$sqlDogObb = "SELECT COUNT(*) AS C_ROW FROM {$cfg_mod_Spedizioni['file_testate']} TD
        WHERE ". $s->get_where_std()." AND TDNBOC = ? AND TDFIL1='Y' ";
$stmtDogObb = db2_prepare($conn, $sqlDogObb);
$result = db2_execute($stmtDogObb, array($m_params->sped_id));
$rowDogObb = db2_fetch_assoc($stmtDogObb);

$dogana_obbligatoria = false;
if ($rowDogObb['C_ROW'] > 0)
  $dogana_obbligatoria = true;


?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            layout: {type: 'vbox', pack: 'start', align: 'stretch'},	
            title: '',
            
            items: [{
                	xtype: 'hidden',
                	name: 'sped_id',
                	value: '<?php echo $sped_id ?>'
                	},
                	
                	{
                		xtype: 'textfield',
		                fieldLabel: 'E-mail',
		                name: 'f_email',
		                vtype: 'email',
		 				maxLength: 100, anchor: '-15',
		 				value: <?php echo j(trim($row['TAMAIL'])) ?>
		            }, {
                		xtype: 'textfield',
		                fieldLabel: 'E-mail (2)',
		                name: 'f_email_2',
		                vtype: 'email',
		 				maxLength: 100, anchor: '-15',
		 				value: <?php echo j(trim($row['TAINDI'])) ?>
		            }, {
                		xtype: 'textfield',
		                fieldLabel: 'Dogana',
		                name: 'f_dogana',
		 				maxLength: 50, anchor: '-15',
		 				value: <?php echo j(trim($row['TALOCA'])) ?>
		 				<?php if ($dogana_obbligatoria){ ?>
		 					, allowBlank: false
		 				<?php }?>
		            }, {
                		xtype: 'textfield',
		                fieldLabel: 'Nr. booking',
		                name: 'f_booking',
		 				maxLength: 20, anchor: '-15',
		 				value: <?php echo j(trim($row['TARIF2'])) ?>
		            }, {
                		xtype: 'textfield',
		                fieldLabel: 'Avvertenze/Note spedizione',
		                name: 'f_note_agg',
		 				maxLength: 100, anchor: '-15',
		 				value: <?php echo j(trim($row['TADESC'])) ?>
		            }

						   
				],
			buttons: [{
	            text: 'Scheda booking',
	            scale: 'medium',
	            iconCls: 'icon-calendar-24',
	            handler: function() {
	            	var m_int_form = this.up('form').getForm();
	            	var m_int_win  = this.up('window');

					if(m_int_form.isValid()){
					
				  		acs_show_win_std('Inserimento dati di booking', '../desk_booking/acs_booking.php?fn=open_parameters', {form_values: m_int_form.getValues(), nr_sped: <?php echo $m_params->sped_id ?>, mod_sped: 'Y'}, null, null, {}, 'icon-calendar-16', 'Y');			

		           	} //form isValid            	                	                
	            }
	        }, '->', {
	            text: 'Salva',
	            scale: 'medium',
	            iconCls: 'icon-save-24',
	            handler: function() {
	            	var m_int_form = this.up('form').getForm();
	            	var m_int_win  = this.up('window');

					if(m_int_form.isValid()){
					    var m_button = this;
						m_button.disable();
						
						Ext.Ajax.request({
						   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save',
						   method: 'POST',
						   jsonData: {form_values: m_int_form.getValues()}, 
						   
						   success: function(response, opts) {
						   	  m_int_win.close();
						   }, 
						   failure: function(response, opts) {
						      Ext.getBody().unmask();
						      alert('error on save_manual');
						   }
						});				

		           	} //form isValid            	                	                
	            }
	        }
	       ],             
				
        }
]}