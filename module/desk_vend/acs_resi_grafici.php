<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();

?>


{"success":true, "items": [

        {
            xtype: 'container',
            title: 'grafici resi',
            layout: {type: 'vbox', align: 'stretch', pack : 'start', padding:'5px'},
			items: [
			
			//PARTE SUPERIORE 
				{
		            xtype: 'container',
		            title: '',
		            layout: {type: 'hbox', align: 'stretch',pack : 'start', padding:'5px'},
					items: [
						{
							xtype: 'grid'
							, margin: '0 10 0 0'
							, flex: 2
							, title: 'INCIDENZA SU ORDINI INEVASI'
							, store: {
								xtype: 'store',
								autoLoad:true,	
								proxy: {
									url: 'acs_resi_grafici_data.php?fn=get_json_data_incidenza'
									, method: 'POST'
									, type: 'ajax'
									
									, extraParams: {open_request: <?php echo acs_raw_post_data(); ?>}
		
			                        /* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
			                        , doRequest: personalizza_extraParams_to_jsonData
									
									//Add these two properties
									, actionMethods: {read: 'POST'}
									, reader: {type: 'json', method: 'POST', root: 'root'}
			
								},
								
								fields: [ 'task', 'cliente', 'cliente_con_resi', 'perc']										
							}
							, columns: [
								{header: '', dataIndex: 'task', flex: 1, align: 'right', menuDisabled: true, sortable: false},
								{header: 'Clienti', dataIndex: 'cliente', flex: 1, align: 'right', menuDisabled: true, sortable: false},
								{header: 'Clienti con resi', dataIndex: 'cliente_con_resi', flex: 1, align: 'right', menuDisabled: true, sortable: false},
								{header: '%', dataIndex: 'perc', flex: 1, align: 'right', renderer: perc2, menuDisabled: true, sortable: false}
							]
						}, {
							xtype: 'grid'
							, flex: 3
							, title: 'ANZIANIT&Agrave'
							, cls: 'grid-anzianita'
							, store: {
								xtype: 'store',
								autoLoad:true,	
								proxy: {
									url: 'acs_resi_grafici_data.php?fn=get_json_data_anzianita'
									, method: 'POST'
									, type: 'ajax'
									
		                        	, extraParams: {open_request: <?php echo acs_raw_post_data(); ?>}
		
			                        /* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
			                        , doRequest: personalizza_extraParams_to_jsonData
									
									//Add these two properties
									, actionMethods: {read: 'POST'}
									, reader: {type: 'json', method: 'POST', root: 'root'}
								},
								
								fields: ['task', 'm-1', 'm-2', 'm-3', 'm-4', 'm-5', 'm-6', 'a_anno', 'a_due_anni', 'oltre', 'row_format']										
							}
							, columns: [
								{
									header: "", 
									dataIndex: 'task', 
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,
								},{ 	
									header: "<?php echo mounth_year("-1") ?>", 
									cls: 'm-1',
									dataIndex: 'm-1', 
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }	 
								},{
									header: "<?php echo mounth_year("-2") ?>", 
									cls: 'm-2',
									dataIndex: 'm-2', 
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }	
								},{
									header: "<?php echo mounth_year("-3") ?>",
									cls: 'm-3',
									dataIndex: 'm-3', 
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,	
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }	
								},{
									header: "<?php echo mounth_year("-4") ?>",
									cls: 'm-4',
									dataIndex: 'm-4',
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }		
								},{
									header: "<?php echo mounth_year("-5") ?>",
									cls: 'm-5',
									dataIndex: 'm-5',
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }		
								},{
									header: "<?php echo mounth_year("-6") ?>",
									cls: 'm-6',
									dataIndex: 'm-6',
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false, 
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }		
								},{
									header: 'A 1 ANNO',
									cls: 'a_anno',
									dataIndex: 'a_anno',
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }		
								},{
									header: 'A 2 ANNI',
									cls: 'a_due_anni',
									dataIndex: 'a_due_anni',
									flex: 1, align: 'right',
									menuDisabled: true, sortable: false,
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }	
								},{
									header: 'OLTRE',
									cls: 'oltre',
									dataIndex: 'oltre', 
									flex: 1, 
									align: 'right',
									menuDisabled: true, sortable: false,
									renderer: function(value, metaData, record, row, col, store, gridView){
										return window[record.get('row_format')](value);
					                }	
					                       	
								}
							]
						}
		
					]
					  
				  }, 
				  
				  
				  // PARTE INFERIORE
				  {
				  	xtype: 'container',
		            title: '',
		            layout: {type: 'hbox', align: 'stretch',pack : 'start', padding:'5px'},
					items: [
						{
							xtype: 'grid'
							, margin: '0 10 0 0'
							, flex: 2
							, features: [{
							      ftype: 'summary'
							 }]
							, title: 'CAUSE NC'
							, store: {
								xtype: 'store',
								autoLoad:true,	
								proxy: {
									url: 'acs_resi_grafici_data.php?fn=get_json_data_cause_nc'
									, method: 'POST'
									, type: 'ajax'
									
									, extraParams: {open_request: <?php echo acs_raw_post_data(); ?>}
		
			                        /* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
			                        , doRequest: personalizza_extraParams_to_jsonData
									
									//Add these two properties
									, actionMethods: {read: 'POST'}
									, reader: {type: 'json', method: 'POST', root: 'root'}
								},
								
								fields: [{name:'cause_nc', type: 'string'},
											{name:'importo', type: 'float'},
											{name:'perc_importo', type: 'float'},
											{name:'nr_righe', type: 'float'},
											{name:'perc_nr_righe', type: 'float'}
								]										
							}
							, columns: [
								{
									header: 'Cause NC',
									dataIndex: 'cause_nc',
									flex: 1,
									summaryType: 'sum', 
									summaryRenderer: function(value, summaryData, dataIndex) {
                              				return 'TOTALE GENERALE'; 
                              		}	
								},{
									header: 'Importo',
									dataIndex: 'importo',
									flex: 1,
									align: 'right',
									renderer: floatRenderer2,
									summaryType: 'sum', 
                              		summaryRenderer: function(value, summaryData, dataIndex) {
                              				return floatRenderer2(value); 
                              		}	
								},{
									header: '%',
									dataIndex: 'perc_importo',
									flex: 1, 
									align: 'right',
									renderer: perc2
									
								},{
									header: 'Nr righe',
									dataIndex: 'nr_righe',
									flex: 1,
									align: 'right',
									renderer: floatRenderer0,
									summaryType: 'sum', 
									summaryRenderer: function(value, summaryData, dataIndex) {
                              				return floatRenderer0(value); 
                              		}	
								},{
									header: '%',
									dataIndex: 'perc_nr_righe',
									flex: 1,
									align: 'right',
									renderer: perc2
								}
							]
						}, {
						
							xtype: 'panel',
							flex: 1,
							layout: {type: 'vbox', align: 'stretch' },
							title: 'Causa NC',
							items: [				
				
									{
										xtype: 'chart',
										animate: true,
										shadow: true, 
										height: 280, 
										flex: 3,				
					
										store: {
											xtype: 'store',
											autoLoad:true,	
											proxy: {
												url: 'acs_resi_grafici_data.php?fn=get_json_data_chart'
												, method: 'POST'
												, type: 'ajax'
									
												, extraParams: {open_request: <?php echo acs_raw_post_data() ?>}
		
						                        /* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
						                        , doRequest: personalizza_extraParams_to_jsonData
												
												//Add these two properties
												, actionMethods: {type: 'json', read: 'POST'}
												, reader: {type: 'json', method: 'POST', root: 'root'}
												
											}, //proxy

											fields: ['CAUSA_NC','VALORE']
														
										}, //store					

							            shadow: true,
						            	/*
							            	legend: {
								            	field: 'CAUSA_NC',
								                position: 'right'
								            },
							            */
							            insetPadding: 25,
							            theme: 'Base:gradients',					
										

										series: [
											{
								                type: 'pie',
								                field: 'VALORE',
								                showInLegend: true,
								                donut: true,
								                tips: {
								                  trackMouse: true,
								                  width: 260,
								                  height: 38,
								                  renderer: function(storeItem, item) {
								                    //calculate percentage.
								                    var total = 0;
								                    storeItem.store.each(function(rec) {
								                        total += parseFloat(rec.get('VALORE'));
								                    });
								                    this.setTitle(storeItem.get('CAUSA_NC')  + ': ' + perc2(storeItem.get('VALORE') / total) + ' [' + floatRenderer2(storeItem.get('VALORE')) + ']');
								                  }
								                },
								                highlight: {segment: {margin: 20}},
								                label: {
								                    field: 'CAUSA_NC',
								                    display: 'rotate',
								                    contrast: true,
								                    font: '11px Arial'	                    
								                }
								            }
							            ]
									}, //chart
									
						        	{
										xtype: 'radiogroup',
					                    fieldLabel: 'Dettaglio',
					                    flex: 1,
					                    layout: {type: 'hbox', align: 'stretch' },
					                    allowBlank: false,
					                    margin: "5 5 5 10",
				                        listeners: {
									        change: function(field, newValue, oldValue) {
									           console.log(newValue.rb_campo_tot);
									           this.up('panel').down('chart').store.proxy.extraParams.rb_campo_tot = newValue.rb_campo_tot ;
									           this.up('panel').down('chart').store.load();
									        }
									    }, 
	
					                    items: [{
						                            xtype: 'radio'
						                          , name: 'rb_campo_tot' 
						                          , boxLabel: 'Importo'
						                          , inputValue: 'importo'
						                          , checked: true	
						                          , width: 80 
						                                                
						                        },
						                        {
						                            xtype: 'radio'
						                          , name: 'rb_campo_tot' 
						                          , boxLabel: 'Nr righe'
						                          , inputValue: 'nr_righe'		                          
						                          , width: 80
						                          			                                                    
						                        }
					                   ]
									
									}
								]
						} //panel
					
					
					]
				  
				  },  //parte inferiore 
				  {
				  		xtype: 'container',
		            	title: '',
		            	layout: {type: 'hbox', align: 'center', pack: 'center', padding:'30px'},
						items: [
								{
									xtype:'buttongroup',
									margin: '0 5 0 0',
									cls: 'top',
									layout: {type: 'hbox', align: 'stretch', pack : 'start'},
									title:'TOP CLIENTI',
									items:[
										{
											text: 'Importo',
											scale: 'large',
											handler : function() {
												var n_top_clienti = Ext.getCmp('n_top_clienti').getValue();
									 			acs_show_win_std('TOP clienti per importo', 'acs_resi_grafici_top.php',
									 				{open_request: <?php echo acs_raw_post_data(); ?>, "top_request": {"top":n_top_clienti, "code":"TDCCON", "code_d":"TDDCON", "value":"importo"}},
									 			 	550, 500, null, '')
											} //handler function()
										}, {
											xtype: 'numberfield',
								            maxValue: 100,
        									minValue: 10,
        									allowDecimals : false,
								            value: 20,
								            id: 'n_top_clienti',
								            hideTrigger: true,  //Remove spinner buttons
								            validator: function(val) {
								                if (!Ext.isEmpty(val) && val>=10 && val<=100) {			
								                    return true;
								                }
								            }
										},{
											text: 'Righe',
											scale: 'large',
											handler : function() {
												var n_top_clienti = Ext.getCmp('n_top_clienti').getValue();
									 			acs_show_win_std('TOP  clienti per righe', 'acs_resi_grafici_top.php',
									 			 	{open_request: <?php echo acs_raw_post_data(); ?>, "top_request": {"top": n_top_clienti, "code":"TDCCON", "code_d":"TDDCON", "value":"nr_righe"}},
									 			 	550, 500, null, '')
											} //handler function()
										}
									]
								}, {
									xtype:'buttongroup',
									title:'TOP ARTICOLI',
									layout: {type: 'hbox', align: 'stretch', pack : 'start'},
									cls: 'top',
									items:[
										{
											text: 'Importo',
											scale: 'large',
											handler : function() {
												var n_top_articoli = Ext.getCmp('n_top_articoli').getValue();
									 			acs_show_win_std('TOP articoli per importo', 'acs_resi_grafici_top.php',
									 				{open_request: <?php echo acs_raw_post_data(); ?>, "top_request": {"top":n_top_articoli, "code":"RDART", "code_d":"RDDES1", "value":"importo"}},
									 			 	550, 500, null, '')
											} 
										},{
											xtype: 'numberfield',
								            value: 50,
								            maxValue: 100,
        									minValue: 10,
        									allowDecimals : false,
								            id: 'n_top_articoli',
								            hideTrigger: true,  //Remove spinner buttons
								            validator: function(val) {
								                if (!Ext.isEmpty(val)) {			
								                    return true;
								                }
								            }
										},{
											text: 'Righe',
										 	scale: 'large',
										 	handler : function() {
										 		var n_top_articoli = Ext.getCmp('n_top_articoli').getValue();
									 			acs_show_win_std('TOP 50 articoli per righe', 'acs_resi_grafici_top.php',
									 				{open_request: <?php echo acs_raw_post_data(); ?>, "top_request": {"top":n_top_articoli, "code":"RDART", "code_d":"RDDES1","value":"nr_righe"}},
									 			 	550, 500, null, '')
											} 
										}
									]
								}
						
						]
		
				  }
			
			]
			  
		  }
		  
	  ]
	  
  }