<?php

require_once("../../config.inc.php");
require_once("acs_panel_todolist_data.php");

$main_module = new Spedizioni();
$s			 = new Spedizioni();


// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            items: [
            
            		{
                	xtype: 'hidden',
                	name: 'tipo_op',
                	value: 'SBLOC'
                	}, 
            
            {
					            xtype: 'combo',
								name: 'f_cliente_cod',
								fieldLabel: 'Cliente',
								minChars: 2,			
								allowBlank: false,            
					            store: {
					            	pageSize: 1000,            	
									proxy: {
							            type: 'ajax',
							            url : 'acs_get_select_json.php?select=search_cli_anag',
							            reader: {
							                type: 'json',
							                root: 'root',
							                totalProperty: 'totalCount'
							            }
							        },       
									fields: ['cod', 'descr', 'out_ind', 'out_loc'],		             	
					            },
					                        
								valueField: 'cod',                        
					            displayField: 'descr',
					            typeAhead: false,
					            hideTrigger: true,
					            anchor: '100%',
					            
					            
					            listConfig: {
					                loadingText: 'Searching...',
					                emptyText: 'Nessun cliente trovato',
					                
					                // Custom rendering template for each item
					                getInnerTpl: function() {
					                    return '<div class=\"search-item\">' +
					                        '<h3><span>{descr}</span></h3>' +
					                        '[{cod}] {out_loc}' +
					                    '</div>';
					                }                
					                
					            },
					            
					            pageSize: 1000
					        }, 
					        
					       {
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '100%',					   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							}						 
						}, 
						  {
							flex: 1,
							name: 'f_area', anchor: '100%',
							xtype: 'combo',
							fieldLabel: 'Area spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}					        
					        
							
						 , {
							flex: 1,
							name: 'f_itinerario',
							xtype: 'combo', anchor: '100%',
							fieldLabel: 'Itinerario',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ITIN'), '') ?> 	
								    ] 
								}						 
							}
							
							
							
							
							
							
							, {
							flex: 1,							
							name: 'f_rischio', anchor: '100%',
							xtype: 'combo',
							fieldLabel: 'Rischio',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_rischio(), '') ?> 	
								    ] 
								}						 
							}
							, {
							flex: 1,							
							name: 'f_pagamento', anchor: '100%',
							xtype: 'combo',
							fieldLabel: 'Pagamento',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_pagamento(), '') ?> 	
								    ] 
								}						 
							}, 
							{
							flex: 1,
							name: 'f_vettore',
							xtype: 'combo', anchor: '100%',
							fieldLabel: 'Vettore',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   //	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('AUTR'), '') ?> 	
								    ] 
								}						 
							},
							{
    					    xtype: 'fieldcontainer',
    					    flex: 1,
    						layout: { 	type: 'hbox',
    								    pack: 'start',
    								    align: 'stretch'},						
    						items: [
    						
        						{
    							flex: 1,					
    							name: 'f_tipologia_ordine', anchor: '100%',
    							xtype: 'combo',
    							fieldLabel: 'Tipologia ordine',
    							displayField: 'text',
    							valueField: 'id',
    							emptyText: '- seleziona -',
    							forceSelection:true,
    						   	allowBlank: true,
    						   	multiSelect: true,
    							store: {
    								autoLoad: true,
    								editable: false,
    								autoDestroy: true,	 
    							    fields: [{name:'id'}, {name:'text'}],
    							    data: [								    
    								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
    								    ] 
    								}						 
    							},
    							{
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							labelAlign: 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							}
    						
    						]}, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [
						   {
							flex: 1,							
							name: 'f_agente', anchor: '-15',
							xtype: 'combo',
							fieldLabel: 'Agente',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	//multiSelect: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_agenti(), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_referente_cliente', anchor: '-15',
							xtype: 'combo',
							fieldLabel: 'Referente cli.', labelAlign: 'right',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	//multiSelect: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_referente_cliente(), '') ?> 	
								    ] 
								}						 
							}
												
						]							
						}	
							
							
							
							
							
							
							
						, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [							
							
						{
						     name: 'f_data_prod_da'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data prod/dispon'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}, {
						     name: 'f_data_prod_a'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: ' al'
						   , labelAlign: 'right'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}
						
						]
						}
						
, { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [							
							
						{
						     name: 'f_data_emiss_OF_da'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data emiss. OF'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}, {
						     name: 'f_data_emiss_OF_a'                		
						   , xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: ' al'
						   , labelAlign: 'right'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
							 }
						}
						
						]
						}						
						
						
						, {
		                    xtype: 'checkboxgroup',
		                    layout: 'hbox',
		                    fieldLabel: 'Solo stadio',
		                    flex: 3,
		                    defaults: {
//		                     width: 105,
		                     padding: '0 10 10 0'
		                    },
		                    items: [{
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Ice'
				                  , inputValue: '1'
				                  , width: 80                
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Slush'
				                  , inputValue: '2'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Water'
				                  , inputValue: '3'
				                  , width: 80                        
				                 }, {
				                   xtype: 'checkbox'
				                  , name: 'f_stadio' 
				                  , boxLabel: 'Altro'
				                  , inputValue: '0'
				                  , width: 80                        
				                 }
		                   ]
		                }							
					        
					        
				],
			buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($main_module->get_cod_mod(), "ESPO_PAN"); ?> {
	            text: 'Clienti/ordini<br/>bloccati',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	acs_show_panel_std('acs_panel_todolist.php?tab_id=panel-espo&tab_title=Overflow', 'panel-espo', {form_open: form.getValues()});
		            this.up('window').close();            	                	                
	            }
	        }, {
	            text: 'Clienti/ordini<br/>bloccati',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
					acs_show_win_std('Report clienti/ordini bloccati', 'acs_report_todolist_SBLOC.php?fn=open_form_filtri', {todo_params: form.getValues()}, 500, 220, null, 'icon-filter-16');	            	

		            this.up('window').close();            	                	                
	            }
	        }, {
	            text: 'Elenco ordini<br/>bloccati',
	            iconCls: 'icon-folder_search-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	
	            	acs_show_win_std('Elenco ordini bloccati', 'acs_elenco_ordini_bloccati.php', {
		        				form_values: form.getValues(),
		    					},		        				 
		        				1024, 600, {}, 'icon-leaf-16');	  
	            
		            this.up('window').close();            	                	                
	            }
	        }, {
	            text: 'Esposizione<br/>corrente',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	
	            		//verifico che sia stato selezionato un cliente
	            		if (Ext.isEmpty(form.findField("f_cliente_cod").getValue())){
	            			acs_show_msg_error('Selezionare un cliente');
	            		 	return false;
	            		}	            	
	            	
	            	
	            	cod_cli = form.findField("f_cliente_cod").getValue().trim();
	            	
	            	//eseguo richiesta e mostro pagina
						Ext.getBody().mask('Loading... ', 'loading').show();	            
							Ext.Ajax.request({
											url: 'acs_json_analisi_esposizione.php?fn=exe_richiesta_aggiornamento',
									        jsonData: {
									        	add_parameters: 'E',
									        	list_selected_id: cod_cli
									        },
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
												Ext.getBody().unmask();									        
									            acs_show_win_std('Analisi esposizine corrente [' + cod_cli + ']', 'acs_json_analisi_esposizione.php?fn=open_view&view=analisi_esposizione', {list_selected_id: cod_cli}, 600, 500, {}, 'icon-info_blue-16');
									        }, scope: this,
									        failure    : function(result, request){
												Ext.getBody().unmask();									        
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });

		            this.up('window').close();            	                	                
	            }
	        },
	        
	        
	         {
	         	xtype: 'splitbutton',
	            text: 'Estratto conto',
	            iconCls: 'icon-print-32',
	            scale: 'large',
	            handler: function(){acs_show_msg_error('Utilizzare la freccia per selezionare una tipologia di stampa');},
	            menu: {
	            	xtype: 'menu',
		        	items: [	   
		        	
		        	<?php foreach ($cfg_mod_Spedizioni["print_ec"] as $k_ec => $ec) { ?>
		        	
		        	     	
						{
							xtype: 'button',
				            text: <?php echo j($ec); ?>,
				            //iconCls: 'icon-print-32', 
				            scale: 'large',
				            handler: function() {
				            	var form = this.up('form').getForm();
				            	
				            		//verifico che sia stato selezionato un cliente
				            		if (Ext.isEmpty(form.findField("f_cliente_cod").getValue())){
				            			acs_show_msg_error('Selezionare un cliente');
				            		 	return false;
				            		}
				            		
				            	
				            		form.submit({
						                        url: 'acs_report_ec.php?fn=open_report&tipo=<?php echo $k_ec; ?>',
						                        target: '_blank', 
						                        standardSubmit: true,
						                        method: 'POST'
						             });	            	
				
					            /////this.up('window').close();            	                	                
				            }
				        }, 
				        
				    <?php  } ?>    	        	
		        	]
		        }
	        }
	        
	        
	        
	        
	         , {
	            text: 'Contrassegni',
	            iconCls: 'icon-windows-32',
	            scale: 'large',
	            handler: function() {
	            
	            	var form = this.up('form').getForm();	            
	            
	            		//verifico che sia stato selezionato un pagamento
	            		if (Ext.isEmpty(form.findField("f_pagamento").getValue())){
	            			acs_show_msg_error('Selezionare un pagamento');
	            		 	return false;
	            		}	            	
	            
	            	acs_show_panel_std('acs_panel_contrassegni.php?fn=get_json_grid', 'panel-contrassegni', {form_open: form.getValues()});
		            this.up('window').close();            	                	                
	            }
	        }
	        
	        ],
				
        }
]}
<?php	
	exit;
} //open_form_filtri








$img_name = "filtro_disattivo";
$ss .= "<span id=\"todo_fl_solo_con_carico_assegnato\" class=acs_button style=\"\">";
$ss .= "<img id=\"todo_fl_solo_con_carico_assegnato_img\" src=" . img_path("icone/48x48/{$img_name}.png") . " height=20>";
$ss .= "</span>";
?>
{
 "success":true, "items":
 { 
  		xtype: 'treepanel',
	        title: 'To Do List',
	        id: 'grid-arrivi',
	        <?php echo make_tab_closable(); ?>,

	        tbar: new Ext.Toolbar({
	            items:['<b>Attivit&agrave; utente di acquisizione/avanzamento ordini</b>', '->'
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	        
	        
	        cls: 'elenco_ordini arrivi',
	        collapsible: true,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,                    
				    model: 'ModelOrdini',
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
						actionMethods: {read: 'POST'},
                        
                        reader: {
                            root: 'children'
                        },
                    extraParams: {
        		    	ordina_per_tipo_ord: ''
        				}
                    , doRequest: personalizza_extraParams_to_jsonData        				
                    }
                }),
	        multiSelect: true,
	        singleExpand: false,
	
			columns: [	
	    		{text: 'Attivit&agrave;<br>Utente assegnato &nbsp;&nbsp;&nbsp;<?php echo $ss; ?>', xtype: 'treecolumn', flex: 1.2, dataIndex: 'task', sortable: false,
	        	  renderer: function (value, metaData, record, row, col, store, gridView){						
					if (record.get('liv') == 'liv_5') metaData.tdCls += ' auto-height';											
					return value;			    
				}},
	
	
				<?php
						//inclusione colonne flag ordini
						$js_include_flag_ordini = array('menuDisabled' => false, 'sortable' => true);
						require("_include_js_column_flag_ordini.php"); 
					?>,	
	    			    		
	
	    	    {text: 'Riferimento', 	flex: 1, dataIndex: 'riferimento', tdCls: 'auto-height rif'},
	    	    {text: 'Tp', 			width: 30, dataIndex: 'tipo', tdCls: 'tipoOrd', 
	        	    	renderer: function (value, metaData, record, row, col, store, gridView){						
								metaData.tdCls += ' toDoListTp ' + record.get('raggr');
								
								if (record.get('qtip_tipo') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_tipo')) + '"';								
																			
							return value;			    
	    			}},
	    	    {text: 'Data', 			width: 60, dataIndex: 'data_reg', renderer: date_from_AS},
	    	    {text: 'St', 			width: 30, dataIndex: 'stato',
	    			    renderer: function(value, metaData, record){
	    			    
							if (record.get('qtip_stato') != "")	    			    	
								metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_stato')) + '"';	    			    
	    			    
							if (value == 'V') return '';
							if (value == 'R') return '';
							if (value == 'G') return '';
							if (value == 'B') return '';
							return value;
	    			    }	    	     
	    	    },	    	    
	    	    {text: 'Pr', 			width: 35, dataIndex: 'priorita', tdCls: 'tpPri',
	    				renderer: function (value, metaData, record, row, col, store, gridView){

	    					if (record.get('qtip_priorita') != "")	    			    	
									metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(record.get('qtip_priorita')) + '"';
	    				
	    					if (record.get('tp_pri') == 4)
	    						metaData.tdCls += ' tpSfondoRosa';
	
	    					if (record.get('tp_pri') == 6)
	    						metaData.tdCls += ' tpSfondoCelesteEl';					
	    					
	    					return value;
	    				    
	    	    			}},
	    		{text: 'Cons.<br>richiesta',	width: 60, dataIndex: 'cons_rich', renderer: date_from_AS},
	    		{text: 'Dispon.',			width: 70, dataIndex: 'data_disp', renderer: function (value, metaData, record, row, col, store, gridView){						
					if (record.get('fl_data_disp') == '1')
						metaData.tdCls += ' tpSfondoRosa';
					 		 
						return date_from_AS(value, metaData, record);
					}
				},
				{text: 'Produz./<br>Disponib.',	width: 70, dataIndex: 'cons_prog', renderer: date_from_AS},			    		
	
	    	    {text: 'O',					width: 35, dataIndex: 'n_O', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';						
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;
					 		 				
				}},
	    	    {text: 'P',					width: 35, dataIndex: 'n_P', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},						
	    	    {text: 'M',					width: 35, dataIndex: 'n_M', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},
	    	    {text: 'A',					width: 35, dataIndex: 'n_A', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;				 		 				
				}},				
	    	    {text: 'CV',				width: 35, dataIndex: 'n_CV', 	align: 'right', renderer: function (value, metaData, rec, row, col, store, gridView){
	        	    if (parseInt(value) > 0 && rec.get('liv') == 'liv_5'){						
						if (parseInt(rec.get('fl_da_prog'))==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==0) return '<img src=<?php echo img_path("icone/48x48/Divieto_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==1) return '<img src=<?php echo img_path("icone/48x48/bandiera_black.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==2) return '<img src=<?php echo img_path("icone/48x48/bandiera_red.png") ?> width=18>';
	        	    	if (rec.get('n_stadio')==3) return '<img src=<?php echo img_path("icone/48x48/bandiera_green.png") ?> width=18>';
	        	    } else return value;
				}},	    
	    			    	    	    
	    	    {text: 'Colli',				width: 35, dataIndex: 'colli', align: 'right',  renderer: floatRenderer0},	       			    		    
	    	    {text: 'Volume',			width: 50, dataIndex: 'volume', align: 'right', renderer: floatRenderer2, hidden: true},
	
	    	    <?php if ($js_parameters->p_nascondi_importi != 'Y'){ ?>    	    
	    		{text: 'Importo',			width: 90, dataIndex: 'importo', align: 'right', renderer: floatRenderer2},
	    		<?php } ?>
	
	    		{text: 'Scadenza<BR/> / Blocco',			width: 70, dataIndex: 'data_scadenza', renderer: function (value, metaData, record, row, col, store, gridView){
	
	    			//per sblocco
	    			if (record.get('id').split("|")[0] == 'SBLOC'){
	
	    				t = new Date();
	    				t1 = new Date();
	    				t2 = new Date();
	    				t3 = new Date();
	    				t1.setMonth(t.getMonth() - 3);
	    				t2.setMonth(t.getMonth() - 6);    				
	    				t3.setMonth(t.getMonth() - 12);
	    				//se da almeno x mese.... grassetto, se da almeno y mesi .... sfondo rosa
						if (parseFloat(record.get('data_scadenza')) != 0 &&
	    	    				 parseFloat(record.get('data_scadenza')) <= parseFloat(Ext.Date.format(t3, 'Ymd')))
	    					metaData.tdCls += ' tpSfondoRosa grassetto';    				
	    				else if (parseFloat(record.get('data_scadenza')) != 0 &&
	    	    				 parseFloat(record.get('data_scadenza')) <= parseFloat(Ext.Date.format(t2, 'Ymd')))
	    					metaData.tdCls += ' tpSfondoRosa';
	    				else if (parseFloat(record.get('data_scadenza')) != 0 &&
	    	    				 parseFloat(record.get('data_scadenza')) <= parseFloat(Ext.Date.format(t1, 'Ymd')))
	    					metaData.tdCls += ' grassetto';        			
						
	        			 return date_from_AS(value, metaData, record);
	    			} //solo SBLOC
	        								
					if (parseFloat(record.get('data_scadenza')) != 0 && parseFloat(record.get('data_scadenza')) <= parseFloat(Ext.Date.format(new Date(), 'Ymd')))
						metaData.tdCls += ' tpSfondoRosa';
					 		 
					return date_from_AS(value, metaData, record);
				}
			}	    		
    	],
			enableSort: false, // disable sorting

	        listeners: {
	        
					afterrender: function (comp) {
					
						if (Ext.get("todo_fl_solo_con_carico_assegnato") != null){		
					        Ext.get("todo_fl_solo_con_carico_assegnato").on('click', function(){
					        	acs_show_win_std('Selezione ordini con attivit&agrave; di avanzamento evasione', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_filtri', null, 500, 320, null, 'icon-filter-16');
					        });
					        Ext.QuickTips.register({target: "todo_fl_solo_con_carico_assegnato",	text: 'Attiva/Disattiva filtri su attivit&agrave; elencate'});	        
						}					
					
					},	        
	        

		        	beforeload: function(store, options) {
		        		Ext.getBody().mask('Loading... ', 'loading').show();
		        		},		
			
		            load: function () {
		              Ext.getBody().unmask();
		            },
		        

				  celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);

						if (rec.get('liv')=='liv_4' && col_name=='tipo' && rec.get('tipo') != ''){
							iEvent.preventDefault();
							show_win_art_reso(rec.get('k_cli_des'));
							return false;
						}
													
						if (col_name=='art_mancanti' && rec.get('liv')=='liv_5'){
							iEvent.preventDefault();						
							show_win_art_critici(rec.get('k_ordine'), 'MTO');
							return false;							
						}	
						if (col_name=='art_da_prog' && rec.get('liv')=='liv_5'){
							iEvent.preventDefault();												
							show_win_art_critici(rec.get('k_ordine'), 'MTS');
							return false;							
						}														
						if (col_name=='colli' && rec.get('liv')=='liv_5'){
							iEvent.preventDefault();						
							show_win_colli_ord(rec, rec.get('k_ordine'));
							return false;
						}															
						if (col_name=='riferimento' && rec.get('liv')=='liv_4'){
							iEvent.preventDefault();						
							gmapPopup("gmap.php?ind=" + rec.get('gmap_ind'));
							return false;						
						}

					 }
					},										
		       


				  itemcontextmenu : function(grid, rec, node, index, event) {			  	

					  event.stopEvent();
				      var record = grid.getStore().getAt(index);		  
				      var voci_menu = [];
				      

				      //LIVELLO 0
					  if (record.get('id').split("|")[0] == 'IMMOR' && record.get('liv') == 'liv_0'){
						  voci_menu.push({
				      		text: 'Order entry per utente',
				    		iconCls: 'iconPrint',
				    		handler: function() {					 
						        acs_show_win_std('Imposta filtri', 'acs_report_todolist_IMMOR.php?fn=open_form_filtri', null, 500, 220, null, 'icon-filter-16');
						    }
						   }); 					  
					  }
					  if (record.get('id').split("|")[0] == 'IMMOR' && record.get('liv') == 'liv_1'){
						  voci_menu.push({
				      		text: 'Elenco ordini assegnati',
				    		iconCls: 'iconPrint',
				    		handler: function() {
								//window.open('myLog.txt');
								
								
								var submitForm = new Ext.form.FormPanel();
								submitForm.submit({
								        target: '_blank', 
			                        	standardSubmit: true,
			                        	method: 'POST',
								        url: 'acs_report_todolist_REFOR_UTENTE.php?fn=open_report',
								        params: {todo_params: Ext.JSON.encode(arrivi.store.proxy.extraParams),
								        		 id_selected: rec.get('liv_cod'),
								        		 dett_ord: 'Y'},
								        });
								return(false);
						    }
						   }); 					  
					  }					  				      
					  if (record.get('id').split("|")[0] == 'SBLOC' && record.get('liv') == 'liv_0'){
						  voci_menu.push({
				      		text: 'Report clienti/ordini bloccati',
				    		iconCls: 'iconPrint',
				    		handler: function() {					 
						        acs_show_win_std('Report clienti/ordini bloccati', 'acs_report_todolist_SBLOC.php?fn=open_form_filtri', {todo_params: arrivi.store.proxy.extraParams}, 500, 220, null, 'icon-filter-16');
						    }
						   }); 					  
					  }				      
					  if (record.get('id').split("|")[0] == 'SBLOC' && record.get('liv') == 'liv_0'){
						  voci_menu.push({
				      		text: 'Stampa estratto conto',
				    		iconCls: 'iconPrint',
				    		handler: function() {					 
						        acs_show_win_std('Stampa estratto conto', 'acs_report_ec.php?fn=open_form_filtri', {todo_params: arrivi.store.proxy.extraParams}, 500, 220, null, 'icon-filter-16');
						    }
						   }); 					  
					  }				      
					  
					      
					  id_selected = grid.getSelectionModel().getSelection();
					  list_selected_id = [];
					  for (var i=0; i<id_selected.length; i++) 
						list_selected_id.push({id: id_selected[i].get('id'), 
									prog: id_selected[i].get('prog'),
									dt_orig: id_selected[i].get('dt_orig'), 
									k_ordine: id_selected[i].get('k_ordine')});
													      
					  if (rec.get('k_ordine').length > 0 && rec.get('rec_stato')!='Y') //sono nel livello ordine		      
					  voci_menu.push({
			      		text: 'Avanzamento/Rilascio attivit&agrave;',
			    		iconCls: 'iconAccept',
			    		handler: function() {

				    		//verifico che abbia selezionato solo righe a livello ordine 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('k_ordine').length == 0){ //NON sono nel livello ordine
									  acs_show_msg_error('Selezionare solo righe a livello ordine');
									  return false;
								  }
							  }
  	
				    		//verifico che abbia selezionato solo righe non gia' elaborate 
							  for (var i=0; i<id_selected.length; i++){ 
								  if (id_selected[i].get('rec_stato') == 'Y'){ //gia' elaborata
									  acs_show_msg_error('Selezionare solo righe non ancora elaborate');
									  return false;
								  }
							  }  	
  	

			    			//apro form per richiesta parametri
							var mw = new Ext.Window({
							  width: 800
							, height: 380
							, minWidth: 300
							, minHeight: 300
							, plain: true
							, title: 'Avanzamento/Rilascio attivit&agrave;'
							, iconCls: 'iconAccept'			
							, layout: 'fit'
							, border: true
							, closable: true
							, id_selected: id_selected
							, listeners:{
					                 'close': function(win){
					                      for (var i=0; i<id_selected.length; i++){
					                      	//per i record che erano selezionato verifico se hanno 
					                      	//adesso il flag di rilasciato (e devo sbarrarli)
					                        
											Ext.Ajax.request({
												url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_richiesta_record_rilasciato',
										        jsonData: {prog: id_selected[i].get('prog')},
										        method     : 'POST',
										        waitMsg    : 'Data loading',
										        success : function(result, request){
										        	this.set('rec_stato', Ext.decode(result.responseText).ASFLRI);    										        	
										        }, scope: id_selected[i],
										        failure    : function(result, request){
										            Ext.Msg.alert('Message', 'No data to be loaded');
										        }
										    });					                        
					                      }
					                  }
					
					         }								
							});				    			
			    			mw.show();			    			

							//carico la form dal json ricevuto da php
							Ext.Ajax.request({
							        url        : 'acs_form_json_avanzamento_entry.php',
							        jsonData: {list_selected_id: list_selected_id, grid_id: grid.id},
							        method     : 'POST',
							        waitMsg    : 'Data loading',
							        success : function(result, request){
							            var jsonData = Ext.decode(result.responseText);
							            mw.add(jsonData.items);
							            mw.doLayout();				            
							        },
							        failure    : function(result, request){
							            Ext.Msg.alert('Message', 'No data to be loaded');
							        }
							    });
				    		
			    		}
					  });



					  if (record.get('id').split("|")[0] == 'SBLOC' && record.get('liv') == 'liv_2'){
						  voci_menu.push({
					      		text: 'Interrogazione esposizione',
					    		iconCls: 'iconAccept',
					    		handler: function() {

						    		//verifico che abbia selezionato solo righe a livello ordine 
									  if (id_selected.length > 1){
											  acs_show_msg_error('Selezionare un singolo cliente');
											  return false;
										  }
										  
						            //apro la visualizzazione e esco
						            acs_show_win_std('Esposizione [' + record.get('id').split("|")[2] + '] ' + record.get('task').trim(), 'acs_json_analisi_esposizione.php?fn=open_view', {list_selected_id: record.get('id')}, 900, 600, {}, 'icon-info_blue-16');
						            return false;										  
		    			

									//lancio l'elaborazione
									//NON E' PIU' QUI MA DAL BOTTONE DI ANALISI ESPOSIZIONE
									Ext.Ajax.request({
											url: 'acs_json_analisi_esposizione.php?fn=exe_richiesta_analisi_esposizione',
									        jsonData: {list_selected_id: list_selected_id},
									        method     : 'POST',
									        waitMsg    : 'Data loading',
									        success : function(result, request){
									            //var jsonData = Ext.decode(result.responseText);
									            //apro la visualizzazione
									            ////acs_show_win_std('Analisi esposizione', 'acs_json_analisi_esposizione.php?fn=open_view', {list_selected_id: list_selected_id}, 600, 600, {}, 'icon-info_blue-16');
									            acs_show_win_std('Esposizione [' + record.get('id').split("|")[2] + '] ' + record.get('task').trim(), 'acs_json_analisi_esposizione.php?fn=open_view', {list_selected_id: list_selected_id}, 900, 600, {}, 'icon-info_blue-16');
									        },
									        failure    : function(result, request){
									            Ext.Msg.alert('Message', 'No data to be loaded');
									        }
									    });
						    		
					    		}
							  });						  
					  }
					  


				      var menu = new Ext.menu.Menu({
				            items: voci_menu
						}).showAt(event.xy);
					  
				  }, 	
					



		        	
		        	
			        itemclick: function(view,rec,item,index,eventObj) {
			          if (rec.get('liv') == 'liv_5')
			          {	
			        	var w = Ext.getCmp('OrdPropertyGrid');
			        	//var wd = Ext.getCmp('OrdPropertyGridDet');
			        	var wdi = Ext.getCmp('OrdPropertyGridImg');        	
			        	var wdc = Ext.getCmp('OrdPropertyCronologia');

			        	wdi.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdi.store.load()
						wdc.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
						wdc.store.load()			        												        	
			        	
						Ext.Ajax.request({
						   url: 'acs_get_order_properties.php?m_id=' + rec.get('k_ordine'),
						   success: function(response, opts) {
						      var src = Ext.decode(response.responseText);
						      w.setSource(src.riferimenti);
					          Ext.getCmp('OrdPropertiesTab').imposta_title(src.title);						      
						      //wd.setSource(src.dettagli);
						   }
						});
						
        				//files (uploaded) - ToDo: dry 
        				var wdu = Ext.getCmp('OrdPropertyGridFiles');
        				if (!Ext.isEmpty(wdu)){
        					if (wdu.store.proxy.extraParams.k_ordine != rec.get('k_ordine')){				
        			        	wdu.store.proxy.extraParams.k_ordine = rec.get('k_ordine');
        			        	if (wdu.isVisible())		        	
        							wdu.store.load();
        					}					
        				}							
					   }												        	
			         }					            											        	
		        },
			

			viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }			
			    		
 }
} 