<?php
require_once "../../config.inc.php";

$main_module = new Spedizioni(array(
		'abilita_su_modulo' => 'DESK_ACQ',
		'abilita_su_modulo2' => 'DESK_PVEN'
));
$s = new Spedizioni(array(
		'abilita_su_modulo' 	=> 'DESK_ACQ',
		'abilita_su_modulo2' 	=> 'DESK_PVEN'
));

if(isset($cod_mod_provenienza))
    $js_parameters = $auth->get_module_parameters($cod_mod_provenienza);
else
    $js_parameters = $auth->get_module_parameters($main_module->get_cod_mod());

$m_params = acs_m_params_json_decode();

if(isset($m_params->dt))   
    $dt = trim($m_params->dt);
else
    $dt = $id_ditta_default;	

$art	= trim($m_params->rdart);

if (isset($_REQUEST['dt'])){
	$dt 	= trim($_REQUEST['dt']);
	$art 	= trim($_REQUEST['art']);	
}

if(isset($m_params->from_wz))
    $from_wz = $m_params->from_wz;
else
    $from_wz = null;

//recupero descrizione articolo e ultimo aggiornamento MRP
	$des_art = trim($s->get_descrizione_articolo($dt, $art));
	$stmt_riga_giac_art = $s->get_art_giac($dt, $art, $from_wz);
	$row_giac_art = db2_fetch_assoc($stmt_riga_giac_art);
	$tipo_riordino_art = $row_giac_art['M2SWTR'];

	$data_ora_agg = $s->get_data_agg_MRP($dt, $art);
	if ((int)$data_ora_agg['DATA'] == 0)
		$data_ora_agg = $s->get_data_agg_MRP_all();	

	if (strlen(trim($m_params->k_ordine)) > 0){
	    $k_docu = $m_params->k_ordine;
        $row  = $s->get_ordine_by_k_ordine($m_params->k_ordine);
        $ordine = trim($row['TDOADO'])."_".trim($row['TDONDO'])."_".trim($row['TDOTPD']);
	}    
           
	
	
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'exe_annulla'){
    // ******************************************************************************************
	    $ret = array();
	    $m_params = acs_m_params_json_decode();
	    $msg_ri = $m_params->msg_ri;
	    $k_ordine = implode("_", array($id_ditta_default,
	        $m_params->row->M3TIDO,
	        $m_params->row->M3INUM,
	        $m_params->row->M3AADO,
	        sprintf("%06s", $m_params->row->M3NRDO)
	    ));
	    
	    $sh = new SpedHistory();
	    $sh->crea(
	        'pers',
	        array(
	            "messaggio"	=> $msg_ri,
	            "k_ordine"	=> $k_ordine,
	            "vals" => array("RIART" =>$m_params->row->M3ART)
	            
	        )
	        );
	    
	    $ret['success'] = true;
	    echo acs_je($ret);
	    exit;
	    
	}
	
	
	// ******************************************************************************************
    if ($_REQUEST['fn'] == 'exe_gen_doc_pren'){
    // ******************************************************************************************
        $ret = array();
        $m_params = acs_m_params_json_decode();
        $k_ordine = implode("_", array($id_ditta_default, 
                                        $m_params->row->M3TIDO, 
                                        $m_params->row->M3INUM,
                                        $m_params->row->M3AADO,
                                        sprintf("%06s", $m_params->row->M3NRDO)
                                        ));
        
        $sh = new SpedHistory();
        $sh->crea(
            'pers',
            array(
                "messaggio"	=> 'GEN_DOC_PREN',
                "k_ordine"	=> $k_ordine,
                "vals" => array("RIART" =>$m_params->row->M3ART)
                
            )
            );
        
        $ret['success'] = true;
        echo acs_je($ret);
        exit;
        
    }
	
	
	// ******************************************************************************************
	// DATI PER GRID fabbisogno ordine
	// ******************************************************************************************
	if ($_REQUEST['fn'] == 'get_json_data_fabbisogno_ordine'){
	    
		global $cfg_mod_Spedizioni;

		$m_where = " AND M3TRIG = 'I' "; //righe impegno
		
		$ar_ord = $s->k_ordine_td_decode_xx($_REQUEST['k_ordine']);
		
		//2010-12-09
		//M2FU01: flag elaborazione su background (se = Y)		
		
		//se ho il parametro f_solo_articoli_contrassegnati
		//allora devo mostrare solo gli articoli con M2FU01 = 'Y'
		
		//in base ai check box accodo le condizioni (in OR)
		$or_where = [];
		
		if ($m_params->solo_articoli_contrassegnati == 'Y')
		    $or_where[] = " W2.M2FU01 = 'Y' ";

		$swtr = array();
         if ($m_params->mto == 'Y')
             $swtr[] = 'O';
         if ($m_params->discreti == 'Y')
             $swtr[] = 'D';
         
         if(count($swtr) > 0)    
             $or_where[] = "( 1=1" . sql_where_by_combo_value(' W2.M2SWTR', $swtr) . ")";
		
         if ($m_params->esau == 'Y')
             $or_where[] = " (W2.M2SWTR = 'S' AND W2.M2ESAU = 'E') ";
         
         //se ho almeno una condizione (check):
         if (count($or_where) > 0)
            $a_where = " AND (" . implode(' OR ', $or_where) . ")";
         
         
		
		$sql = "SELECT W3.*, W2.M2FU01, W2.M2SWTR, W2.M2ESAU
                FROM {$cfg_mod_Spedizioni['file_disponibilita']} W3
				INNER JOIN  {$cfg_mod_Spedizioni['file_giacenze']} W2
                    ON W3.M3DT = W2.M2DT AND W3.M3ART = W2.M2ART
                WHERE 1=1 {$m_where}
			      AND M3DT = ? AND M3TIDO = ? AND M3INUM = ? AND M3AADO = ? AND M3NRDO = ? 
			      {$a_where}			    
                 ORDER BY 1";
	
			$stmt = db2_prepare($conn, $sql);
			echo db2_stmt_errormsg();
			$result = db2_execute($stmt, array($ar_ord['TDDT'], $ar_ord['TDOTID'], $ar_ord['TDOINU'], $ar_ord['TDOADO'], $ar_ord['TDONDO']));
			
			$ret = array();
			while ($r = db2_fetch_assoc($stmt)) {
				$r['M3DART'] = acs_u8e($r['M3DART']);
				$r['tipo_riordino'] = implode('', array(trim($r['M2SWTR']), trim($r['M2ESAU'])));
				$ret[] = $r;
			}		
		
			echo acs_je($ret);
		exit;
	}
	
	

// ******************************************************************************************
// FABBISOGNO PER ORDINE (RIGHE 'I' PER SINGOLO ORDINE)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'fabbisogno_ordine'){
	$k_docu = $m_params->k_ordine;
	$ord = $s->get_ordine_by_k_docu($k_docu);
	$title_suf = " al ".print_date($data_ora_agg['DATA']); " - " .print_ora($data_ora_agg['ORA']);
	$title_suf .= " (Impegni attivi)"; 
	?>	

{"success": true,

  m_win: {
			title_suf: <?php echo j($title_suf); ?>	
		},
 "items": 
  [
	{
		xtype: 'gridpanel',
		flex: 0.6,
						   
        tbar: new Ext.Toolbar({
            items:['<b>' + <?php echo j("Ordine " . $s->k_ordine_out($k_docu) 
							. ", Stato: " . trim($ord['TDSTAT']) 
							. ", Evasione: " . print_date($ord['TDDTEP'])
							. ", Spedizione: " . print_date($ord['TDDTSP'])
						   ) ?> + '</b>', '->'
            
            
					, {								   	
				            xtype: 'checkbox'				          
					      , width: 110
					      , labelWidth: 170
				          , name: 'f_solo_articoli_contrassegnati' 
				          , boxLabel: 'Contrassegnati'
				          , inputValue: 'Y'
				          
				          <?php if ($cfg_mod_Spedizioni["fabbisogno_default_mostra_solo_articoli_contrassegnati"] == 'N'){ ?>
				          , checked: false				         
				          <?php } else { ?>
				          , checked: true
				          <?php } ?> 
				          
				          , listeners: {
						    	change: function(checkbox, checked){
						    		   var ck_submit_value = checked ? 'Y' : 'N';
									   m_grid = this.up('window').down('grid');
									   m_grid.store.proxy.extraParams.solo_articoli_contrassegnati = ck_submit_value;
									   m_grid.store.load();			            						            			 	            					            					                    		
						    	}
						    }		                          
				        }
				        , {								   	
				            xtype: 'checkbox'				          
					       , width: 50
					      , labelWidth: 170
				          , name: 'mto' 
				          , boxLabel: 'MTO'
				          , inputValue: 'Y'
				          , listeners: {
						    	change: function(checkbox, checked){
						    		   var ck_submit_value = checked ? 'Y' : 'N';
									   m_grid = this.up('window').down('grid');
									   m_grid.store.proxy.extraParams.mto = ck_submit_value;
									   m_grid.store.load();			            						            			 	            					            					                    		
						    	}
						    }		                          
				        }
				        , {								   	
				            xtype: 'checkbox'				          
					        , width: 70
					      , labelWidth: 170
				          , name: 'discreti' 
				          , boxLabel: 'Discreti'
				          , inputValue: 'Y'
				          , listeners: {
						    	change: function(checkbox, checked){
						    		   var ck_submit_value = checked ? 'Y' : 'N';
									   m_grid = this.up('window').down('grid');
									   m_grid.store.proxy.extraParams.discreti = ck_submit_value;
									   m_grid.store.load();			            						            			 	            					            					                    		
						    	}
						    }		                          
				        } , {								   	
				            xtype: 'checkbox'				          
					        , width: 120
					      , labelWidth: 170
				          , name: 'esau' 
				          , boxLabel: 'MTS in Esaurim.'
				          , inputValue: 'Y'
				          , listeners: {
						    	change: function(checkbox, checked){
						    		   var ck_submit_value = checked ? 'Y' : 'N';
									   m_grid = this.up('window').down('grid');
									   m_grid.store.proxy.extraParams.esau = ck_submit_value;
									   m_grid.store.proxy.extraParams.mts = ck_submit_value;
									   m_grid.store.load();			            						            			 	            					            					                    		
						    	}
						    }		                          
				        }
            
            
	           	, {iconCls: 'tbar-x-tool x-tool-print', handler: function(event, toolEl, panel){
					Ext.ux.grid.Printer.print(this.up('grid'));
	           	}}
         ]            
        }),  
		
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_fabbisogno_ordine&k_ordine=<?php echo $m_params->k_ordine; ?>',
							type: 'ajax',
							reader: {
					            type: 'json',
					            method: 'POST',
					            root: 'root'
					        },
					        
					        actionMethods: {
							  read: 'POST'
							},
							        
							
							<?php
    							if ($cfg_mod_Spedizioni["fabbisogno_default_mostra_solo_articoli_contrassegnati"] == 'N')
    							    $fabbisogno_default_mostra_solo_articoli_contrassegnati = 'N';
    							else
    							    $fabbisogno_default_mostra_solo_articoli_contrassegnati = 'Y'; //default
							?>        
							        
					        extraParams: {
								solo_articoli_contrassegnati: <?php echo j($fabbisogno_default_mostra_solo_articoli_contrassegnati); ?>,
								mto : '',
								discreti: '',
								standard : '',
								esau : ''
			        		},
			        				
			        		doRequest: personalizza_extraParams_to_jsonData,	
						},
	        			fields: ['M3DT', 'M3ART', 'M3DART', 'M3DPRO', 'M3TRIG', 'M3RGS1', 'M3UM', 'M3QTA', {name: 'M3QTA_I', type: 'float'}, {name: 'M3QTA_O', type: 'float'}, 'M3DPRO', 'M3DIMP','M3AADO', 'M3NRDO', 'M3NRDO_EXT', 'M3TPDO', 'M3VSRF', 'M3PROG', 'M3FMAN', 'M3DTDI', 'M3DTDS', 'M3STDO', 'M3DTEP', 'M3DRIF', 'M3FG01', 'M3FG05', 'tipo_riordino']
	    			}),
	    			
	    			<?php $cl = "<img src=" . img_path("icone/48x48/tools.png") . " height=18>"; ?>
	    			
		        columns: [{
			                header   : 'UM',
			                dataIndex: 'M3UM', 
			                flex     : 5
			             }
			             
			     
				<?php if ($tipo_riordino_art == 'O'){ ?>
						, {
			                header   : 'Riferim. MTO',
			                dataIndex: 'M3DRIF', 
			                width     : 110
			             }				
				<?php } ?>			     
			             
			             , {
			                header   : 'Data',
			                dataIndex: 'M3DIMP', 
			                width     : 70, renderer: date_from_AS
			             }, {
			                header   : 'Impegnato', 
			                width: 85, align: 'right',
			                dataIndex: 'M3QTA_I',			                
			                renderer: function(value, p, record){if (record.get('M3TRIG') == "I") return floatRenderer4(record.get('M3QTA'))},
							summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer4(parseFloat(value)); 
                                }               			                
			             }, {
			                header   : 'Denominazione',
			                dataIndex: 'M3DART',
			                
							summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return '(Totale)'; 
                                },			                
			                 
			                flex     : 70
			             }, {
			                header   : 'Articolo',
			                dataIndex: 'M3ART', 
			                width: 100
			             }, {
			                header   : 'Disp.Prev.',
			                dataIndex: 'M3DTDI', 
			                width     : 70, 
			                renderer: function(value, metaData, record, row, col, store, gridView){
			                	if (record.get('M3TRIG') == "O" && 
			                		record.get('M3DTDI') ==  record.get('M3DTEP')
			                		) return null;
			                
			                		return date_from_AS(value);
			                }			                
			             }, {
			                header   : 'Disp.Sped.',
			                dataIndex: 'M3DTDS', 
			                width     : 70,
			                renderer: function(value, metaData, record, row, col, store, gridView){
			                	if (record.get('M3TRIG') == "O" && 
			                		record.get('M3DTDS') ==  record.get('M3DTEP')
			                		) return null;
			                
			                		return date_from_AS(value);
			                }			                
			             }, {
			                header   : 'M',
			                width     : 20,
			                dataIndex: 'M3FMAN',
			                tooltip: '<b>M</b>: Articoli mancanti<br><b>C</b>: Articoli critici'
			             }, {
			             	header: 'TR', 
			             	tooltip: 'Tipo riordino<br><b>O</b>: MTO<br><b>D</b>: Discreti<br><b>SE</b>: MTS in esaurimento',
			             	dataIndex: 'tipo_riordino',
			             	width: 33
			             },
			             {  text: '<br><?php echo $cl; ?>',
			                tooltip: 'Distinta di lavorazione',  
			             	dataIndex: 'M3FG05',
			             	width: 33,
			             	renderer: function(value, p, record) {
	    			  	     if(record.get('M3FG05') == 'L')
	    			  	     	return '<img src=<?php echo img_path("icone/48x48/tools.png") ?> width=15>';
	    		            
	    		            }
			             }
			             
			             ]
			             
			             
		, viewConfig: {
		        getRowClass: function(record, index) {
		           if (record.get('M3TRIG')=='GI')
		           		return ' grassetto';		        
		           if (record.get('M3FMAN')=='M')
		           		return ' colora_riga_rosso';
		           if (record.get('M3FMAN')=='C')
		           		return ' colora_riga_giallo';		           		
		           if (record.get('M3FMAN')=='E')
		           		return ' colora_riga_grigio';		           		
		           return '';																
		         }   
		    }			             
			             	    	
			             
		, listeners: {	

			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){

					rec = iView.getRecord(iRowEl);
				    col_name = iView.getGridColumns()[iColIdx].dataIndex;
				    
			         acs_show_win_std('Loading...', '<?php echo $_SERVER['PHP_SELF']; ?>', {dt: rec.get('M3DT'), rdart: rec.get('M3ART')}, 900, 550, null, 'icon-shopping_cart_gray-16');				    
			          
			          
				    

					
				    
					return false;	
				}
			}
		
	 	}			             									
		         
	}		
  ]	
 }
	
<?php	
	exit;
}	







// ******************************************************************************************
// CONTRATTI FORNITURE PROGRAMMATE PER SINGOLO ARTICOLO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'json_contratti_forniture_programmate_art'){

	?>

{"success": true, "items": 
  [
	{
		xtype: 'gridpanel',
		cls: 'acs-light',
		title: <?php echo j($m_params->rddart . " [" . $m_params->rdart . "]"); ?>,
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_contratti_forniture_programmate_art',
							type: 'ajax',
							reader: {
					            type: 'json',
					            root: 'root'
					        },
					      extraParams: <?php echo acs_raw_post_data(); ?>,   
						},
	        			fields: ['DATA', 'QTA']
	    			}),
	    			
		        columns: [{
			                header   : 'Consegna dal',
			                dataIndex: 'DATA', 
			                flex: 1, renderer: date_from_AS
			             }, {
			                header   : 'Quantit&agrave;', 
			                flex: 1, align: 'right',
			                dataIndex: 'QTA'			                							               			                
			             }]
			             
		         
	}		
  ]	
 }
	
<?php	
	exit;
}	



// ******************************************************************************************
// CONTRATTI FORNITURE PROGRAMMATE PER SINGOLO ARTICOLO
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_contratti_forniture_programmate_art'){


	$ar = array();
	
	$sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_WMAF20']} WHERE M2DT=? AND M2ART=?";
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, array($dt, $art));  //$_REQUEST
	
	$r = db2_fetch_assoc($stmt);
	
	$ret = array();
	for ($i=1; $i<=20; $i++){
		if (trim($r["M2FTQ{$i}"]) == 'C')
			$ret[] = array('DATA' => $r["M2DTE{$i}"], 'QTA' => $r["M2QOD{$i}"]);
	}
 echo acs_je($ret);
exit;
}






	


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
    $from_wz = $m_params->from_wz;
    $maga = $m_params->maga;
    
    if (strlen($m_params->progressivo) > 0)
        $progressivo = $m_params->progressivo;
    else 
        $progressivo = 0;
    
	$data = array();

	switch ($_REQUEST['type']){
		default:
			
		    // MOVIMENTI
			if($from_wz == 'Y'){
			    //punto vendita
			 
			    global $backend_ERP;
			    if($backend_ERP == 'GL')
			        $sql_where = " AND M2PROG = '{$progressivo}'";
			    
			    $sql_g = "SELECT M2UM, M2ART, M2DGIA, M2QGIA FROM {$cfg_mod_Spedizioni['file_wizard_MTS_2']} 
			            WHERE M2DT = '{$id_ditta_default}' AND M2MAGA = '{$maga}' AND M2ART = '{$art}'
			            {$sql_where}";
			    
			    
			    
			    
			    $stmt_g = db2_prepare($conn, $sql_g);
			    echo db2_stmt_errormsg();	
			    $result = db2_execute($stmt_g);
			    $row_g = db2_fetch_assoc($stmt_g);
			    
			    $row = array();
			    $row['M3TRIG'] 	= "GI";
			    $row['M3UM'] 	= $row_g["M2UM"];
			    $row['M3ART'] 	= $row_g["M2ART"];
			    $row['M3RGS1'] 	= "Giacenza iniziale";
			    $row['M3DIMP'] 	= $row_g['M2DGIA'];
			    //$row['M2QGIA'] 	= $row_g['M2QGIA'];
			    $row['M3DPRO'] 	= $row_g['M2QGIA'];
			    $data[] = $row;
			    
			    $sql_c = "SELECT SUM(M3QTA) AS C_ROW FROM {$cfg_mod_Spedizioni['file_dettaglio_wizard']}
			    WHERE M3DT = '{$id_ditta_default}' AND M3MAGA = '{$maga}' AND M3ART = '{$art}'
                AND M3FG01 IN ('P', 'R')";
			
			    
			    $stmt_c = db2_prepare($conn, $sql_c);
			    echo db2_stmt_errormsg();
			    $result = db2_execute($stmt_c);
			    $row_c = db2_fetch_assoc($stmt_c);
			    
			} else {
			    if (trim($_REQUEST['tpno']) != 'MTO') {
			        // DISPONIBILITA INIZIALE
			        $stmt = $s->get_art_giac($dt, $art, null, $progressivo);   //$_REQUEST
			        while ($row = db2_fetch_assoc($stmt)) {
			            if ($row['M2SWTR'] != 'O'){
			                $row['M3TRIG'] 	= "GI";
			                $row['M3UM'] 	= $row["M2UM"];
			                $row['M3ART'] 	= $row["M2ART"];
			                $row['M3RGS1'] 	= "Giacenza iniziale";
			                $row['M3DIMP'] 	= $row['M2DGIA'];
			                $row['M3DPRO'] 	= $row['M2QGIA'];
			                
			                $data[] = $row;
			            }
			        }
			    }
			}
			
			
			$stmt = $s->get_art_disp($dt, $art, $row_giac_art['M2SWTR'], $row_giac_art['M2FG01'], $from_wz, $maga, $progressivo);	
			
		

			while ($row = db2_fetch_assoc($stmt)) {
			    
			   
				$row['M3RGS1'] = acs_u8e($row['M3RGS1']);
				$row['M3VSRF'] = acs_u8e($row['M3VSRF']);
				$row['ordine'] = trim($row['M3AADO'])."_".sprintf("%06s", trim($row['M3NRDO']))."_".trim($row['M3TPDO']);
								
				if ($row['M3TRIG'] == 'I')
					$row['M3QTA_I'] = $row['M3QTA']; 
				if ($row['M3TRIG'] == 'O')
					$row['M3QTA_O'] = $row['M3QTA'];
				
			    if($from_wz == 'Y'){
			         $row['M2QGIA'] = $row_g['M2QGIA'];
			         $row['r_rows'] = $row_c['C_ROW'];
			    }
			
				$data[] = $row;
			}
				
			echo "{\"root\": " . acs_je($data) . "}";	
		break;
		}
	
	exit;
}






// ******************************************************************************************
// DATI PER GRID REGOLE RIORDINO DA WMAF20 (per articolo)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data_regole_riordino_WMAF20'){
	global $cfg_mod_DeskAcq;	
	
	if (strlen($_REQUEST['progressivo']) > 0)
	    $progressivo = $_REQUEST['progressivo'];
	else
	    $progressivo = 0;
	
	$m_where = '';
	//articolo/ditta/deposito
	if (strlen(trim($_REQUEST['f_cod_art'])) > 0){
		$m_where .= " AND M2ART = " . sql_t(trim($_REQUEST['f_cod_art']));
		global $backend_ERP;
		if($backend_ERP == 'GL'){
		  $m_where .= " AND M2PROG = " . $progressivo;
		}
	}
	if (strlen(trim($_REQUEST['f_dt'])) > 0)
		$m_where .= " AND M2DT = " . sql_t(trim($_REQUEST['f_dt']));	
    if (strlen(trim($_REQUEST['maga'])) > 0)
        $m_where .= " AND M2MAGA = " . sql_t(trim($_REQUEST['maga']));
	
    $add_select = '';    
	if($_REQUEST['from_wz'] == 'Y'){
	    $file = $cfg_mod_DeskAcq['file_wizard_MTS_2'];
	    $add_select .= ', M2MAGA';
	}
	else 
	    $file = $cfg_mod_DeskAcq['file_WMAF20'];

	$ar = array();
	
		$sql = "SELECT M2DT, M2ART, M2DART, M2FOR1, M2RGS1, M2QCON
					, M2DTGE AS CRDTGE, M2GEM1 AS CRGEM1, M2GEM2 AS CRGEM2, M2GEM3 AS CRGEM3, M2GEM4 AS CRGEM4, M2GEM5 AS CRGEM5, M2GEM6 AS CRGEM6
					, M2GRF1 AS CRGRF1, M2GRF2 AS CRGRF2, M2GRF3 AS CRGRF3, M2GRF4 AS CRGRF4, M2GRF5 AS CRGRF5, M2GRF6 AS CRGRF6
					, M2GCF1 AS CRGCF1, M2GCF2 AS CRGCF2, M2GCF3 AS CRGCF3, M2GCF4 AS CRGCF4, M2GCF5 AS CRGCF5, M2GCF6 AS CRGCF6
					, M2LTFO AS CRLTFO, M2LTRI AS CRLTRI, M2LTLO AS CRLTLO, M2LTPR AS CRLTPR, M2SWTR AS CRSWTR, M2DRIO, M2DIND
					, M2DCSO, M2DEMO, M2DESO, M2RIOL, M2UEMO
					, M2SMIA, M2SMIC, M2RIOR, M2LOTT {$add_select}
		
					FROM {$file} WHERE 1=1 {$m_where} ORDER BY M2ART";
	
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);

		while ($r = db2_fetch_assoc($stmt)) {
		    
		   
		    if (strlen(trim($_REQUEST['maga'])) > 0){
		        $ta_sys = find_TA_sys('MUFD', trim($r['M2MAGA']));
		        $deposito	=  "[".trim($r['M2MAGA'])."] ".$ta_sys[0]['text'];
		    }else{
		      $deposito = "";
		    }
		    $r['out_FOR'] = "<b>".acs_u8e($r['M2RGS1']) . "</b> [" . trim($r['M2FOR1']) . "] <br>" .$deposito;
			$r['out_FOR_grp'] = 'Fornitore: ' . $r['out_FOR'];
			$ar[] = $r;
		}
		echo acs_je($ar);
		exit();
}









/******************************************************
 * GRID JSON
 ******************************************************/

if (strlen($m_params->progressivo) > 0)
    $progressivo = $m_params->progressivo;
else
    $progressivo = 0;

    global $backend_ERP;
    if($backend_ERP == 'GL')
        $sql_where = " AND M2PROG = '{$progressivo}'";
    
    $sql = "SELECT * FROM {$cfg_mod_DeskAcq['file_WMAF20']} WHERE M2DT=? AND M2ART=? {$sql_where}";
    
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, array($dt, $art));  //$_REQUEST
    echo db2_stmt_errormsg($stmt);
    $rec_m2 = db2_fetch_assoc($stmt);



?>


{"success": true,
	m_win: {
		title: 'Background MRP'
		, width: 1100
		, height: 600
		, iconCls: 'icon-shopping_cart_gray-16'
	},
 "items": 
  {
   xtype: 'panel',
	layout: {
	    type: 'vbox',
	    align: 'stretch',
	    pack : 'start',
	},
   items: [
  	
  
	{
		xtype: 'grid',
		title: 'Condizioni riordino standard - Sviluppo disponibilit&agrave;',
		height: 110,
		id: 'regole-riordino',
		loadMask: true,
		closable: false,			
		
		viewConfig: {
		        getRowClass: function(record, index, rowParams, store) {
		            return record.get('rowCls');
		        }
		 },		
		
		features: [
		new Ext.create('Ext.grid.feature.Grouping',{
			groupHeaderTpl: '{[values.rows[0].data.out_FOR_grp]}',
			hideGroupedHeader: false
		}),
		{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true,   // defaults to false (remote filtering)
	
			// Filters are most naturally placed in the column definition, but can also be added here.
		filters: [
		{
			type: 'boolean',
			dataIndex: 'visible'
		}
		]
		}],
	
	
		listeners: {
		
		<?php if($m_params->from_wz == 'Y'){?>
			afterrender: function (comp, a, b, c) {
			
					
							if (Ext.get("righe_vendita") != null){		
					        Ext.get("righe_vendita").on('click', function(){
					       		acs_show_win_std('Righe ordini di vendita aperti - ' + <?php echo j($des_art); ?> +' ['+ <?php echo j($m_params->rdart); ?> +']', '../desk_punto_vendita/acs_righe_vendita.php?fn=open_grid', {c_art: <?php echo j($m_params->rdart); ?>}, 800, 500, null, 'icon-blog-16');              			
					        });
					        Ext.QuickTips.register({target: "Righe ordine cliente",	text: 'Righe ordine cliente'});	        
						}	
	
					
					},
		<?php }?>
			celldblclick: {
				fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						
						rec = iView.getRecord(iRowEl);
					    col_name = iView.getGridColumns()[iColIdx].dataIndex;
					    
						<?php if($m_params->from_wz != 'Y'){?>
					     acs_show_win_std('Contratti forniture programmate', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=json_contratti_forniture_programmate_art', {dt: rec.get('M2DT'), rdart: rec.get('M2ART'), rddart: rec.get('M2DART')}, 500, 550, null, 'icon-shopping_cart_gray-16');				    						
			            <?php }?>
				}
			}
		},
	
	
	
		store: {
			xtype: 'store',
				
			//groupField: 'out_FOR',
			autoLoad:true,
	
			proxy: {
				url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_regole_riordino_WMAF20&disableTot=Y',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: {
				f_cod_art:  '<?php echo $m_params->rdart; ?>',
				f_dt:  '<?php echo $m_params->dt; ?>',
				from_wz : '<?php echo $m_params->from_wz; ?>',
				maga : '<?php echo $m_params->maga; ?>',
				progressivo: <?php echo j($m_params->progressivo); ?>			
				}				
			},
				
			fields: ['out_FOR', 'out_FOR_grp', 'rowCls', 'CRDTGE'
					  	, 'CRGEM1', 'CRGEM2', 'CRGEM3', 'CRGEM4', 'CRGEM5', 'CRGEM6'
 						, 'CRGRF1', 'CRGRF2', 'CRGRF3', 'CRGRF4', 'CRGRF5', 'CRGRF6'					  
 						, 'CRGCF1', 'CRGCF2', 'CRGCF3', 'CRGCF4', 'CRGCF5', 'CRGCF6'
 						, 'CRLTFO', 'CRLTRI', 'CRLTLO', 'CRLTPR', 'CRSWTR', 'CRTPAR', 'CRPRIO'
 						, 'CRTART', 'CRTSOS', 'CRTSCA', 'CRTIDT', 'CRPROG'
 						, 'CRTATT', 'CRTESA', 'CRDTUI', 'M2DRIO', 'M2DIND', 'M2QCON', 'M2DT', 'M2ART', 'M2DART', 'M2DCSO'
 						, 'M2DEMO', 'M2DESO', 'M2RIOL', 'M2UEMO'
 						, 'M2SMIA', 'M2SMIC', 'M2RIOR', 'M2LOTT'
					  ]
						
						
		}, //store
		multiSelect: false,
		
		<?php 
		
		if($m_params->from_wz == 'Y' && $backend_ERP != 'GL'){
		
		$sql = "SELECT COUNT(*) AS C_ROW
		        FROM {$cfg_mod_DeskPVen['file_righe_doc']} RD
		        INNER JOIN {$cfg_mod_DeskPVen['file_testate']} TD
		        ON TD.TDDT = RD.RDDT AND TD.TDOTID = RD.RDTIDO AND TD.TDOINU = RD.RDINUM AND TD.TDOADO = RD.RDAADO AND TD.TDONDO = RD.RDNRDO
		        INNER JOIN {$cfg_mod_DeskPVen['file_tabelle']} TA
		        ON TD.TDDT = TA.TADT AND TATAID = 'CHSTF' AND TD.TDSTAT = TA.TAKEY1 AND TAFG03 = 'Y'
		        WHERE TDDT = '{$id_ditta_default}' AND RDART = '{$m_params->rdart}' AND RDSTEV <>'S' AND RDQTA > RDQTE
		        AND TDOTID = 'VO' AND TDOTPD <> 'MP'";
		
		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();
		$result = db2_execute($stmt);
		$row = db2_fetch_assoc($stmt);
		if($row['C_ROW'] > 0)
		    $img = "<img id=\"righe_vendita\" src=" . img_path("icone/48x48/blog.png") . " height=30>";
		else 
		    $img = "<img id=\"righe_vendita\" src=" . img_path("icone/48x48/blog_delete.png") . " height=30>";
			
			
			
			$ss .= "&nbsp;&nbsp;&nbsp;";
			$ss .= "<span id=\"righe_vendita\" class=acs_button style=\"\">";
			$ss .= $img;
			$ss .= "</span>";
			
		}else{
		    $ss .= "<img id=\"righe_vendita\" src=" . img_path("icone/48x48/blog.png") . " height=30>";
		    
		}		
			
			
		$tooltip_consumi = "Consumi ultimi 5 mesi: <br>" . implode('<br> ', array(
		    $rec_m2['M2UM'] . " " . n($rec_m2['M2CON1']),
		    $rec_m2['M2UM'] . " " . n($rec_m2['M2CON2']),
		    $rec_m2['M2UM'] . " " . n($rec_m2['M2CON3']),
		    $rec_m2['M2UM'] . " " . n($rec_m2['M2CON4']),
		    $rec_m2['M2UM'] . " " . n($rec_m2['M2CON5'])		    
		));
		
			
		?>		
	
		columns: [
			
			{header: 'Fornitore', dataIndex: 'out_FOR', flex: 2, filter: {type: 'string'}, filterable: true,
			  tooltip: <?php echo j($tooltip_consumi); ?>},
			{header: '<?php echo $ss; ?>', width: 50, sortable: false, menuDisabled: true, dataIndex: 'M2QCON',  
				<?php if($m_params->from_wz == 'Y'){?>
				tooltip: 'Righe ordine di vendita',
				<?php }else{?>
				tooltip: 'Contratti forniture programmate',
				<?php }?>
				
				renderer: function (value, metaData, record, row, col, store, gridView){
						if (parseInt(value) > 0) return floatRenderer0(value);
					}			
			},
			{header: 'Giorni di invio', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGEM1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGEM2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGEM3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGEM4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGEM5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGEM6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Giorni di ricezione', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGRF1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGRF2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGRF3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGRF4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGRF5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGRF6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Giorni di consegna', flex: 3,
			 columns: [
			 	{header: 'Lu', dataIndex: 'CRGCF1', width: 23, tdCls: 'lu'},
				{header: 'Ma', dataIndex: 'CRGCF2', width: 23, tdCls: 'ma'},
				{header: 'Me', dataIndex: 'CRGCF3', width: 23, tdCls: 'me'},							 	
			 	{header: 'Gi', dataIndex: 'CRGCF4', width: 23, tdCls: 'gi'},
				{header: 'Ve', dataIndex: 'CRGCF5', width: 23, tdCls: 've'},
				{header: 'Sa', dataIndex: 'CRGCF6', width: 23, tdCls: 'sa'}				
			 ]
			}, {
			 header: 'Lead time', flex: 3,
			 columns: [
			 	{header: 'Art', dataIndex: 'CRLTFO', width: 30, align: 'right', 
					renderer: function (value, metaData, record, row, col, store, gridView){
						tooltip_txt  = 'Scorta minima anagrafica: ' + floatRenderer0(record.get('M2SMIA'));
						tooltip_txt += '<br/>Scorta minima calcolata: '  + floatRenderer0(record.get('M2SMIC'));
						tooltip_txt += '<br/>Punto di riordino: '  + floatRenderer0(record.get('M2RIOR'));
						tooltip_txt += '<br/>Lotto di riordino: '  + floatRenderer0(record.get('M2LOTT'));
					
						metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(tooltip_txt) + '"';
					
						return floatRenderer0(value);
					}			 	
			 	},
			 	{header: 'Rit', dataIndex: 'CRLTRI', width: 30, renderer: floatRenderer0, align: 'right'},			 	
				{header: 'Log', dataIndex: 'CRLTLO', width: 30, renderer: floatRenderer0, align: 'right'},
				{header: 'Prod', dataIndex: 'CRLTPR', width: 30, renderer: floatRenderer0, align: 'right'}			
			 ]
			}, {header: 'Consegna<br>Standard', dataIndex: 'M2DRIO', width: 70, 
					renderer: function (value, metaData, record, row, col, store, gridView){
						tooltip_txt  = date_from_AS(record.get('M2DEMO')) + ': Data emissione ordine';
						tooltip_txt += '<br/>' + date_from_AS(record.get('M2DESO')) + ': Data emissione ordine successivo';
						tooltip_txt += '<br/>' + date_from_AS(record.get('M2RIOL')) + ': Data riordino limite';
						tooltip_txt += '<br/>' + date_from_AS(record.get('M2UEMO')) + ': Data emissione ultimo riordino';
					
						metaData.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(tooltip_txt) + '"';
					
						return date_from_AS(record.get('M2DRIO')) + '<br/>' + '[' + date_from_AS(record.get('M2DCSO')) + ']';
					}
			}, {header: 'Indisponib.<br>fino al', dataIndex: 'M2DIND', width: 70, 
					renderer: function (value, metaData, record, row, col, store, gridView){
						return date_from_AS(value);
					}
			  }, {header: 'T.R.', dataIndex: 'CRSWTR', width: 32}
			  			   
			
		]		
		
		
	}  

	

 , {
		xtype: 'gridpanel',
		flex: 0.6,
		stateful: true,
        stateId: 'seleziona-background',
        stateEvents: ['columnresize', 'columnmove', 'columnhide', 'columnshow' ],
		features: [{
			      ftype: 'summary'
		}],		
		
				store: new Ext.data.Store({
		
					autoLoad:true,				        
  					proxy: {
							url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data&dt=<?php echo $m_params->dt; ?>&art=<?php echo trim($m_params->rdart); ?>',
							type: 'ajax',
							actionMethods: {
					          read: 'POST'
					        },
							extraParams: {
										 from_wz : '<?php echo $m_params->from_wz; ?>',
										 maga : '<?php echo $m_params->maga; ?>',
										 progressivo: <?php echo j($m_params->progressivo); ?>
			        				},
			        				
			        		doRequest: personalizza_extraParams_to_jsonData, 
						
						    reader: {
				            type: 'json',
				            root: 'root'
				        }
						},
	        			fields: [
	        							'M3ART', 'M3DPRO', 'M3TRIG', 'M3RGS1', 'M3UM', 'M3QTA', 
	        							{name: 'M3QTA_I', type: 'float'}, {name: 'M3QTA_O', type: 'float'}, 'M3DPRO', 'M3DIMP', 'M3TIDO', 'M3INUM', 
	        							'M3AADO', 'M3NRDO', 'M3TPDO', 'M3VSRF', 'M3PROG', 'M3FMAN', 'M3DTDI', 'M3DTDS', 'M3STDO', 'M3DTEP', 
	        							'M3DRIF', 'M3FG01', 'M1DTSP', 'M2QGIA', 'r_rows', 'ordine',
	        							'M3FG02', 'M3FG03', 'M3FG04']
	    			}),
	    			
		        columns: [{
			                header   : 'UM',
			                dataIndex: 'M3UM', 
			                width: 28
			             }
			             
				<?php if ($tipo_riordino_art != 'O'){ ?>			             
			             
			             , {
			                header   : 'Disponib.',
			                dataIndex: 'M3DPRO', align: 'right',
			                
			                width     : 70,  renderer: function (value, metaData, record, row, col, store, gridView){
			                	metaData.tdCls += ' grassetto';

			                	if (record.get('M3FMAN') != 'E'){
					  				if (parseFloat(value) < 0)
					  					 metaData.tdCls += ' sfondo_rosso';														   
					  				if (parseFloat(value) == 0)
					  					 metaData.tdCls += ' sfondo_giallo';					  					 
					  				if (parseFloat(value) > 0)
					  					 metaData.tdCls += ' sfondo_verde';					  					 
								} else {
									///return '';
								}
			                	 	                
					  			return floatRenderer4(parseFloat(value));									
								}	
			             }
			             
			     <?php } ?>        
			     
				<?php if ($tipo_riordino_art == 'O'){ ?>
						, {
			                header   : 'Riferim. MTO',
			                dataIndex: 'M3DRIF', 
			                width     : 110,
			   		 }				
				<?php } ?>			     
			             
			             , {
			                header   : 'Data',
			                dataIndex: 'M3DIMP', 
			                width     : 62, renderer: date_from_AS
			             },  
			       
			             
			              {
			                header   : 'Ordinato', 
			                width: 85, align: 'right',
			                dataIndex: 'M3QTA_O',
			                renderer: function(value, p, record){
			                
			                if (record.get('M3TRIG') == "O") 
			                return floatRenderer4(record.get('M3QTA'))
			                
			                
			                 /* if (record.get('M3TRIG') == "GI") 
			               		 return floatRenderer4(record.get('M2QGIA')) */
			                
			                },
							summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer4(parseFloat(value)); 
                                }								
			             }, {
			                header   : 'Impegnato', 
			                width: 85, align: 'right',
			                dataIndex: 'M3QTA_I',			                
			                renderer: function(value, p, record){if (record.get('M3TRIG') == "I") return floatRenderer4(record.get('M3QTA'))},
							summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return floatRenderer4(parseFloat(value)); 
                                }               			                
			             }, {
			                header   : 'Denominazione',
			                dataIndex: 'M3RGS1',
			                
							summaryType: 'sum', summaryRenderer: function(value, summaryData, dataIndex) {
                                       return '(Totale)'; 
                                },			                
			                 
			                flex     : 70, renderer: function (value, metaData, record, row, col, store, gridView){
								if (record.get('M3TRIG') == "O")			                
			                		metaData.tdCls += ' grassetto';
			                	return value;	
			                }	
			             }, {
			                header   : 'Riferimento',
			                dataIndex: 'M3VSRF', 
			                flex     : 40, renderer: function (value, metaData, record, row, col, store, gridView){
								if (record.get('M3TRIG') == "O")			                
			                		metaData.tdCls += ' grassetto';
			                	return value;	
			                }
			             }, {
			                header   : 'Ordine',
			                width	 : 110,
			                dataIndex: 'M3NRDO', 
			                renderer: function(value, metaData, record, row, col, store, gridView){
			                	
			                	if(!Ext.isEmpty(record.get('ordine')) && record.get('ordine') == '<?php echo $ordine; ?>')
		           	                metaData.tdCls += ' sfondo_verde';
			                	
			                	if (record.get('M3TRIG') == "O")			                
			                		metaData.tdCls += ' grassetto';
			                
			                	return "" + record.get('M3AADO') + "_" + record.get('M3NRDO') + "_" + record.get('M3TPDO')
			                }
			             }, {
			                header   : 'St.',
			                dataIndex: 'M3STDO', 
			                width     : 30
			             }, {
			                header   : 'Evasione',
			                dataIndex: 'M3DTEP', 
			                width     : 62, renderer: date_from_AS
			             }, {
			                header   : 'Disp.Prev.',
			                <?php if($m_params->from_wz == 'Y')?>
			                	hidden : true,
			                
			                dataIndex: 'M3DTDI', 
			                width     : 64, 
			                renderer: function(value, metaData, record, row, col, store, gridView){
			                	if (record.get('M3TRIG') == "O" && 
			                		record.get('M3DTDI') ==  record.get('M3DTEP')
			                		) return null;
			                
			                		return date_from_AS(value);
			                }			                
			             }, {
			                header   : 'Disp.Sped.',
			                dataIndex: 'M3DTDS', 
			                <?php if($m_params->from_wz == 'Y')?>
			                	hidden : true,
			                width     : 64,
			                renderer: function(value, metaData, record, row, col, store, gridView){
			                	if (record.get('M3TRIG') == "O" && 
			                		record.get('M3DTDS') ==  record.get('M3DTEP')
			                		) return null;
			                
			                		return date_from_AS(value);
			                }			                
			             }, {
			                header   : 'Data.Sped.',
			                dataIndex: 'M1DTSP', 
			                <?php if($m_params->from_wz == 'Y')?>
			                	hidden : true,
			                width     : 64,
			                renderer: function(value, metaData, record, row, col, store, gridView){			               
			                		return date_from_AS(value);
			                }			                
			             }, {
			                header   : '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=14>',
			                width     : 24,
			                <?php if($m_params->from_wz == 'Y')?>
			                	hidden : true,
			                dataIndex: 'M3FMAN'
			             }, {
			                header   : 'TD', 
			                tooltip: 'Tipo disponibilit&agrave;<br/>P = Prenotato, R = Riservato',			                
			                flex     : 1,
			                dataIndex: 'M3FG01',
			                
							summaryType: function(records, values){
								var conteggio_RP = 0;
								  Ext.each(records || [], function(rec) {
                                      if (rec.get('M3FG01') == 'R' || rec.get('M3FG01') == 'P') {
                                        conteggio_RP +=1;
                                      }
                                    }, this);
                                return conteggio_RP;
                            }, 
							summaryRenderer: function(value, summaryData, dataIndex, r, r2) {
                                       return '(' + value + ')'; 
                            }				                
			                
			             }, {
			                header: '02', width: 24, dataIndex: 'M3FG02',
			                tooltip: '(M3FG02)<BR/>L = Con Lotto',
			                <?php if($m_params->from_wz == 'Y')?>
			                	hidden : true
			                
			             }, {
			                header: '03', width: 24, dataIndex: 'M3FG03',
			                tooltip: '(M3FG03)<BR/>E = Blocco evasione, A = Blocco amministrativo',
			                <?php if($m_params->from_wz == 'Y')?>
			                	hidden : true
			                
			             }, {
			                header: '04', width: 24, dataIndex: 'M3FG04',
			                tooltip: '(M3FG04)<BR/>N = Non programmato',
			                <?php if($m_params->from_wz == 'Y')?>
			                	hidden : true
			                
			             }]
			             
			             
		, viewConfig: {
		        getRowClass: function(record, index) {
		           if (record.get('M3TRIG')=='GI')
		           		return ' grassetto';		        
		           if (record.get('M3FMAN')=='M')
		           		return ' colora_riga_rosso';
		           if (record.get('M3FMAN')=='C')
		           		return ' colora_riga_giallo';		           		
		           if (record.get('M3FMAN')=='E')
		           		return ' colora_riga_grigio';	
		           return '';																
		         }   
		    }			             
			             	    	
			             
		, listeners: {		
		
		 		beforestaterestore: function(comp, state){
                        //ESCLUDO SEMPRE LA MEMORIZZAZIONE DALLA STATEEVENTS
                        delete state.sort;
                    },
		
	 			afterrender: function (comp) {
	 			
	 			<?php if($m_params->from_wz == 'Y'){?>
	 				comp.up('window').setTitle('Programma fabbisogno articoli al <?php echo print_date($data_ora_agg['DATA']); ?> - <?php echo print_ora($data_ora_agg['ORA']); ?> - <?php echo strtr($des_art,array("'" => "\'")) . " [$art] "; ?>');	 				
	 			
	 			<?php }else{?>
	 			
	 				comp.up('window').setTitle('Programma fabbisogno materiali al <?php echo print_date($data_ora_agg['DATA']); ?> - <?php echo print_ora($data_ora_agg['ORA']); ?> - <?php echo strtr($des_art,array("'" => "\'")) . " [" . implode(', ', array($art, $m_params->progressivo)) . "] " . " " . $m_params->varianti; ?>');	 				
	 			
	 			<?php }?>
	 			
					
	 			},
	 			
	 			  itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						 var voci_menu = [];
					     var row = rec.data;
					     var m_grid = this;
					     var loc_win = this.up('window'); 
					     
					     
					   <?php 
					   
					   if($js_parameters->only_view  != 1){  
					     
					     if($m_params->from_wz == 'Y'){  ?>
					     
					    
					     if(rec.get('M3FG01').trim() == 'R'){
					     
					     	
					      voci_menu.push({
			         		text: 'Annulla accantonamento',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		
			        	
			        		
			        		   Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'annullamento?', function(btn, text){																							    
									if (btn == 'yes'){																	         	
										 Ext.Ajax.request({
								         url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_annulla',
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				msg_ri : 'ANN_RIS_PREN'
										},							        
								        success : function(result, request){
								        	
         						          if (!Ext.isEmpty(loc_win.events.afterbackgr))
        									loc_win.fireEvent('afterBackgr', loc_win);
        									
        									
        								 
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });				         	
											         	
									}
								 });
			        		   
			        	 
			        		   }
			        		   
					    		 
				                
			    		});
					     
					     
					     }
					     
					     if(rec.get('M3FG01').trim() == 'P'){
					     
					     	
					      voci_menu.push({
			         		text: 'Annulla richiesta prenotazione',
			        		iconCls : 'icon-sub_red_delete-16',          		
			        		handler: function () {
			        		
			        	
			        		
			        		   Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'annullamento?', function(btn, text){																							    
									if (btn == 'yes'){																	         	
										 Ext.Ajax.request({
								         url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_annulla',
								        method     : 'POST',
					        			jsonData: {
					        				row : row,
					        				msg_ri : 'ANN_DOC_PREN'
										},							        
								        success : function(result, request){
								        	
         						          if (!Ext.isEmpty(loc_win.events.afterbackgr))
        									loc_win.fireEvent('afterBackgr', loc_win);
        									
        									
        								 
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });				         	
											         	
									}
								 });
			        		   
			        	 
			        		   }
			        		   
					    		 
				                
			    		});
					     
					     
					     }
					     
					    
					         
					 if((rec.get('M3FG01').trim() != 'R' && rec.get('M3FG01').trim() != 'P') 
					    && rec.get('M2QGIA') - rec.get('r_rows') - rec.get('M3QTA_I') >= 0){     
					 	 voci_menu.push({
			         		text: 'Genera richiesta prenotazione',
			        		iconCls : 'iconBox',          		
			        		handler: function () {
			        		
			        	
			        		
			        		   Ext.Msg.confirm('Richiesta conferma', 'Confermi la generazione documento di prenotazione materiale?', function(btn, text){																							    
									if (btn == 'yes'){																	         	
										 Ext.Ajax.request({
								         url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_gen_doc_pren',
								        method     : 'POST',
					        			jsonData: {
					        				row : row
										},							        
								        success : function(result, request){
								        	
         						          if (!Ext.isEmpty(loc_win.events.afterbackgr))
        									loc_win.fireEvent('afterBackgr', loc_win);
        									
        									
        								 
					            		   
					            		},
								        failure    : function(result, request){
								            Ext.Msg.alert('Message', 'No data to be loaded');
								        }
								    });				         	
											         	
									}
								 });
			        		   
			        	 
			        		   }
			        		   
					    		 
				                
			    		});
			    		
			    		}
			    	
					   <?php  } 
					   }
					   ?>
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}
	 	}			             									
		         
	}
 ]
 } 	
}
