<?php

require_once "../../config.inc.php";

$s = new Spedizioni();




// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
 
	//costruzione sql	
	global $cfg_mod_Spedizioni;
	
	$m_params = acs_m_params_json_decode();
	$fv = $_REQUEST;
	
	$m_where = ' 1 = 1 ';
	$m_where_ar = array();
	
	if (strlen($fv['f_num_ordine']) > 0){
		$m_where .= " AND RINRDO = ? ";
		$m_where_ar[] = $fv['f_num_ordine'];
	}
	if (strlen($fv['f_num_carico']) > 0){
		$m_where .= " AND RINRCA = ? ";
		$m_where_ar[] = $fv['f_num_carico'];
	}
		
	//DATA RICHIESTA (DAL AL)
	if (strlen($fv['f_data_dal']) > 0){
		$m_where .= " AND RIDTRI >= ? ";
		$m_where_ar[] = $fv['f_data_dal'];
	}	
	if (strlen($fv['f_data_al']) > 0){
		$m_where .= " AND RIDTRI <= ? ";
		$m_where_ar[] = $fv['f_data_al'];
	}	
	

	
	$sql = "SELECT *
				FROM {$cfg_mod_Spedizioni['file_richieste']}
				WHERE {$m_where}
					ORDER BY RITIME
					";
	
	
	
	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, $m_where_ar);
	
	$ar = array();
	
	while ($r = db2_fetch_assoc($stmt)) {		
		$k_ordine = $r['RINRDO'];
		$r['k_ordine'] = $k_ordine;		
		$r['k_ordine_out'] = implode('_' , array(trim($r['RIAADO']), trim($r['RINRDO'])));
		
		$ar[] = $r;
	}
	echo acs_je($ar);
	exit();
}








// ******************************************************************************************
// DATI PER GRID (RAPPORTO ELABORAZIONE)
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_rapporto_sincro_data'){

	//costruzione sql
	global $cfg_mod_Spedizioni;

	$m_params = acs_m_params_json_decode();
	$fv = $_REQUEST;

	$m_where_ar = array();

	$sql = "SELECT * FROM {$cfg_mod_Spedizioni['file_righe']} WHERE RDTPNO = 'ELAB' {$m_where} ORDER BY RDONDO DESC, RDPROG DESC";

	$stmt = db2_prepare($conn, $sql);
	$result = db2_execute($stmt, $m_where_ar);

	$ar = array();

	while ($r = db2_fetch_assoc($stmt)) {
		$r['data'] 	= substr($r['RDONDO'], 6, 8);
		$r['ora'] 	= print_ora($r['RDPROG']);		
		$r['segnalazioni'] 	= trim($r['RDDES1'] . $r['RDDES2']);
		$r['data_ora'] = print_date($r['data']) . " " . $r['ora'];
		$ar[] = $r;
		}
		echo acs_je($ar);
		exit();
	}







// ******************************************************************************************
// GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_grid'){
	
	$m_params = acs_m_params_json_decode();
	
?>
	
{"success":true, "items":
 {
  xtype: 'panel',
  title: 'HistoryLog',
  id: 'panel-alert',
  closable: true,
  autoScroll: true,    
  items: [		
	{
		xtype: 'grid',
		title: 'Registro esecuzione comandi',
		id: 'tab-alert',
		loadMask: true,
		
		features: [
		new Ext.create('Ext.grid.feature.Grouping',{
			groupHeaderTpl: 'Raggruppamento: {name}',
			hideGroupedHeader: false
		}),
		{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true,   // defaults to false (remote filtering)
	
			// Filters are most naturally placed in the column definition, but can also be added here.
		filters: [
		{
			type: 'boolean',
			dataIndex: 'visible'
		}
		]
		}],
	
		store: {
			xtype: 'store',
				
//			groupField: 'TDDTEP',
			autoLoad:true,
	
			proxy: {
				url: 'acs_history_log.php?fn=get_json_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: <?php echo acs_je($m_params->form_values); ?>				
			},
				
			fields: [
				'RITIME', 'RIDTRI', 'RIHMRI', 'RIUSRI', 'RIESIT', 'RIRGES', 'RIDT', 'RITID0', 'RIINUM', 'RIAADO', 'RINRDO'
				, 'k_ordine', 'k_ordine_out'
				, 'RICITI', 'RIVETT', 'RIAUTO', 'RIDTEP', 'RIDTVA', 'RIAZDV', 'RINRCA', 'RISECA'
				, 'RINSPE', 'RINOTR'
					]
						
						
		}, //store
		multiSelect: false,
	
		columns: [
			{header: 'Data', renderer: date_from_AS, dataIndex: 'RIDTRI', width: 80},		
			{header: 'Ora', renderer: time_from_AS, dataIndex: 'RIHMRI', width: 80},			
			{header: 'Utente',  dataIndex: 'RIUSRI', width: 80, filter: {type: 'string'}, filterable: true},
			{header: 'Esito',  dataIndex: 'RIESIT', width: 50, filter: {type: 'string'}, filterable: true},				
			{header: 'Ordine',  dataIndex: 'k_ordine_out', width: 100, filter: {type: 'string'}, filterable: true},
			{header: 'Comando',  dataIndex: 'RIRGES', width: 100, filter: {type: 'string'}, filterable: true},		
			{header: 'Itin.',  dataIndex: 'RICITI', width: 50, filter: {type: 'string'}, filterable: true},
			{header: 'Vett.',  dataIndex: 'RIVETT', width: 50, filter: {type: 'string'}, filterable: true},
			{header: 'Mezzo',  dataIndex: 'RIAUTO', width: 50, filter: {type: 'string'}, filterable: true},
			{header: 'Evas.Progr.', renderer: date_from_AS, dataIndex: 'RIDTEP', width: 80, filter: {type: 'string'}, filterable: true},
			{header: 'Evas.Conf', renderer: date_from_AS, dataIndex: 'RIDTVA', width: 80, filter: {type: 'string'}, filterable: true},															
			{header: 'Carico',  dataIndex: 'RINRCA', width: 80, filter: {type: 'string'}, filterable: true},			
			{header: 'Seq.',  dataIndex: 'RISECA', width: 40, filter: {type: 'string'}, filterable: true},
			{header: 'Nr Sped.',  dataIndex: 'RINSPE', width: 100, filter: {type: 'string'}, filterable: true},
			{header: 'Note',  dataIndex: 'RINOTR', flex: 1, filter: {type: 'string'}, filterable: true},									
		]		 
	}
]
 } 

}	
	
	
<?php 	
  exit();
}









// ******************************************************************************************
// GRID RAPPORTO SINCRONIZZAZIONE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_rapporto_sincro_grid'){

	$m_params = acs_m_params_json_decode();

	?>
	
{"success":true, "items":
 {
  xtype: 'panel',
  title: 'SynchroLog',
  id: 'panel-alert',
  closable: true,
  autoScroll: true,    
  items: [		
	{
		xtype: 'grid',
		title: 'Rapporto sincronizzazione avanzamento ordini/programmi',
		id: 'tab-alert',
		loadMask: true,
		
		features: [
		new Ext.create('Ext.grid.feature.Grouping',{
			groupHeaderTpl: 'Data/ora elaborazione: {name}',
			sortInfo:{field: 'data_ora', direction: 'DESC'},			
			hideGroupedHeader: false
		}),
		{
			ftype: 'filters',
			// encode and local configuration options defined previously for easier reuse
			encode: false, // json encode the filter query
			local: true,   // defaults to false (remote filtering)
	
			// Filters are most naturally placed in the column definition, but can also be added here.
		filters: [
		{
			type: 'boolean',
			dataIndex: 'visible'
		}
		]
		}],
	
		store: {
			xtype: 'store',
				
			groupField: 'data_ora',	
			groupOnSort: false, remoteGroup: true,			
			sortInfo:{field: 'data_ora', direction: 'DESC'},					
			autoLoad:true,
	
			proxy: {
				url: 'acs_history_log.php?fn=get_json_rapporto_sincro_data',
				method: 'POST',
				type: 'ajax',
				
				//Add these two properties
				actionMethods: {
					read: 'POST'					
				},
	
				reader: {
					type: 'json',
					method: 'POST',
					root: 'root'					
				},
			extraParams: <?php echo acs_je($m_params->form_values); ?>				
			},
				
			fields: ['data', 'ora', 'segnalazioni', 'data_ora']
						
						
		}, //store
		multiSelect: false,
	
		columns: [
			{header: 'Segnalazioni',  dataIndex: 'segnalazioni', flex: 1, filterable: true}
		]
		 
	}
]
 } 

}	
	
	
<?php 	
  exit();
}









// ******************************************************************************************
// FORM RICHIESTA PARAMETRI E TIPO STAMPA
// ******************************************************************************************

if ($_REQUEST['fn'] == 'get_parametri_form' || strlen($_REQUEST['fn']) == 0){

?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Parametri interrogazione cronologia comandi',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
     		
     		listeners: {
 				afterrender: function (comp) {
 					//TODO
 					//gestire la grandessa della form
 					//cercare setWidth se lo uso da qualche parte
 				}
 			},	
            
            items: [					 	
					  { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						fieldLabel: 'Data esecuzione',
						items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'dal'
						   , labelWidth: 30
						   , name: 'f_data_dal'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}, {
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'al'
						   , labelAlign: 'right'
						   , name: 'f_data_al'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: true
						   , anchor: '-15'
						   , listeners: {
						       invalid: function (field, msg) {
						       Ext.Msg.alert('', msg);}
								}
						}]
					}, { 
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							name: 'f_num_ordine',
							xtype: 'textfield',
							fieldLabel: 'Nr. Ordine',
							value: '',
							flex: 1							
						 }, {
							name: 'f_num_carico',
							xtype: 'textfield',
							fieldLabel: 'Nr. Carico',
							value: '',
							flex: 1,
							labelAlign: 'right'
						 }
						]
					}
            
	
	
						 
						 
				],
			buttons: [{
	            text: 'Visualizza<br>cronologia',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
	            
	            	var form = this.up('form').getForm();	            
					if(form.isValid()){	            				            
						//carico la form dal json ricevuto da php e la inserisco nel m-panel
						Ext.Ajax.request({
						        url        : 'acs_history_log.php?fn=get_json_grid',
						        method     : 'POST',
						        waitMsg    : 'Data loading',
						        jsonData: {form_values: this.up('form').getForm().getValues()},
						        
						        success : function(result, request){			        	
						        	
									mp = Ext.getCmp('m-panel');					        					        
									mp.remove('panel-history-log');					        
						            var jsonData = Ext.decode(result.responseText);
						            
						            mp.add(jsonData.items);
						            mp.doLayout();
						            mp.items.last().show();
		 							this.print_w.close();
								             
						        },
						        failure    : function(result, request){
						            Ext.Msg.alert('Message', 'No data to be loaded');
						        }
						        				        
						    });
					} //for isValid
            	                	                
	            }
	        }, {
	            text: 'Rapporto<br>sincronizzazione',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {
				            
				//carico la form dal json ricevuto da php e la inserisco nel m-panel
				Ext.Ajax.request({
				        url        : 'acs_history_log.php?fn=get_json_rapporto_sincro_grid',
				        method     : 'POST',
				        waitMsg    : 'Data loading',
				        jsonData: {form_values: this.up('form').getForm().getValues()},
				        
				        success : function(result, request){			        	
				        	
							mp = Ext.getCmp('m-panel');					        					        
							mp.remove('panel-history-log');					        
				            var jsonData = Ext.decode(result.responseText);
				            
				            mp.add(jsonData.items);
				            mp.doLayout();
				            mp.items.last().show();
 							this.print_w.close();
						             
				        },
				        failure    : function(result, request){
				            Ext.Msg.alert('Message', 'No data to be loaded');
				        }
				        				        
				    });
				    
	            
            	                	                
	            }
	        }],             
				
        }
]}

<?php
 	
  exit();
}
