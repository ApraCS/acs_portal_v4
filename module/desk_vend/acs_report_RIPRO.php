<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();




// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_parametri_form'){
?>	


	{"success" : true, "items":
	
		{
	        xtype: 'form',
	        layout: 'form',
	        collapsible: false,
	        id: 'my_form',
	        url: '<?php echo $_SERVER['PHP_SELF']; ?>',
	        frame: false,
	        bodyPadding: '15 15 15 15',
	        width: 350,
	        fieldDefaults: {
	            msgTarget: 'side',
	            labelWidth: 75,
	    		format: 'd/m/Y',
	    		submitFormat: 'Ymd'	            
	        },
	        defaultType: 'datefield',
	        items: [{
	            fieldLabel: 'Periodo dal',
	            name: 'periodo_dal',
	            allowBlank: false,
	            tooltip: 'Periodo iniziale'
	        }, {
								xtype: 'timefield'
							   , fieldLabel: '(ora)'							   								
							   , name: 'periodo_ora_dal'
							   , format: 'H:i'						   
							   , submitFormat: 'His'
							   , allowBlank: true
							   , anchor: '-15'
							   , listeners: {
							       invalid: function (field, msg) {
							       Ext.Msg.alert('', msg);}
									}
			 }, {
	            fieldLabel: 'Periodo al',
	            name: 'periodo_al',
	            allowBlank: false,
	            tooltip: 'Periodo finale'
	        }, {
							flex: 1,
							name: 'area_spedizione',
							xtype: 'combo',
							fieldLabel: 'Area spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 	
								    ] 
								}						 
							}, {
            					xtype: 'numberfield',
            					fieldLabel: 'Numero Lotto',
            					anchor: '-15',
            					name: 'num_lotto',
            					maxLength : 6,
                			    hideTrigger: true
            				},{
								name: 'righe_per_pagina',
								xtype: 'numberfield',
								hideTrigger : true,
								fieldLabel: 'Righe per pagina',
							    anchor: '-15',
							    value: 10,							
							  }
			],

	        buttons: [{
	            text: 'Riepilogo',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	            handler: function() {
	                form = this.up('form').getForm();
	                
	                if (form.isValid()){	                	                
		                form.submit({
				                standardSubmit: true,
		                        method: 'POST',
								target : '_blank'		                        
		                });
	                }	                
	            }
	        }, {
	            text: 'Rapporto',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	            handler: function() {
	                form = this.up('form').getForm();
	                
	                if (form.isValid()){	                	                
		                form.submit({
		                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?tipo_report=RIEPILOGO_ORDINI_PROGRAMMATI',
				                standardSubmit: true,
		                        method: 'POST',
								target : '_blank'		                        
		                });
	                }	                
	            }
	        }, {
	            text: 'Elenco',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	            handler: function() {
	                form = this.up('form').getForm();
	                
	                if (form.isValid()){	                	                
		                form.submit({
		                		url: '<?php echo $_SERVER['PHP_SELF']; ?>?tipo_report=RIEPILOGO_ORDINI_PROGRAMMATI&sottotipo=ELENCO',
				                standardSubmit: true,
		                        method: 'POST',
								target : '_blank'		                        
		                });
	                }	                
	            }
	        }]
	    }
	
		
	}

<?php	
 exit;	
}



$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u){
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");
}

$ar_email_json = acs_je($ar_email_to);


?>


<html class="acs_report">
 <head>

  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> /> 
  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table.acs_report{border-collapse:collapse; width: 100%;}
   table.acs_report td, table.acs_report th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white;}   
   
   div#my_form{padding: 30px; width: 500px; border: 0px solid gray; margin: 20px;}
   div#my_form table th, div#my_form table td{border: 0px;}
   
   html.acs_report body{padding: 10px;}
   html.acs_report h2{font-size: 14px; padding: 10px;}
   

	@media all {
	 .page-break  { display: none; }
	 .onlyPrint { display: none; }
	}
   
   @media print 
	{
	    .noPrint{display:none;}
 		.page-break  { display: block; page-break-before: always; }
 		.onlyPrint{display: block;}	    
	}   
      

      
  </style>
  
  
  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  
  
  
  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
Ext.onReady(function() {
		
  	Ext.get("p_send_email").on('click', function(){

  	 var n_doc = document.implementation.createDocument ('http://www.w3.org/1999/xhtml', 'html',  null);  	  	

	 html_text = document.getElementById('my_content').outerHTML;
//	 html_text = document.getElementsByTagName('tr')[2].outerHTML

//	html_text = '<tr><td align=center></td><td>marosti</td><td>070011625</td><td>L.B. DI BARRA LUIGI</td><td nowrap="">2013_000219_CU (AA)</td><td></td><td align=center>30/07/13</td><td align=center></td><td align=center>05/12/13</td><td align=right>5</td><td align=right>0,29</td><td align=right>404,43</td></tr>';
//	html_text = '<tr><td></td><td>marosti</td><td>070011625</td><td>L.B. DI BARRA LUIGI</td><td></td><td></td><td></td><td></td><td></td><td>5</td><td align="right">0,29</td><td>404,43</td></tr>';


	 html_text = html_text.replace(/&nbsp;/g, ""); //replace altrimenti va in errore	 
	 html_text = html_text.replace(/<br>/g, "<br/>"); //replace altrimenti va in errore	 

	 style_text = document.getElementsByTagName('style')[0].outerHTML;
	 
	 html_text += style_text;
	
    n_doc.documentElement.innerHTML = html_text;
    
    
	html_text = n_doc.getElementsByTagName('html')[0].innerHTML;    
	
  		
 		var f = Ext.create('Ext.form.Panel', {
        frame: true,
        title: '',        
        bodyPadding: 5,
        url: 'acs_op_exe.php',
        fieldDefaults: {
//            labelAlign: 'top',
            msgTarget: 'side',
            labelWidth: 55,
            anchor: '100%'            
        },
		layout: {
            type: 'vbox',
            align: 'stretch'  // Child items are stretched to full width
        },        

        items: [{
        	xtype: 'hidden',
        	name: 'fn',
        	value: 'exe_send_email'
        	}, {
            xtype: 'combo',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: [ 'email', 'descr' ],
                data: <?php echo $ar_email_json ?>
            }),
            displayField: 'descr',
            valueField: 'email',
            fieldLabel: 'A',
            queryMode: 'local',
            selectOnTab: false,
            name: 'to'
        }, {
            xtype: 'textfield',
            name: 'f_oggetto',
            fieldLabel: 'Oggetto',
            anchor: '96%',
		    allowBlank: false,
		    value: <?php echo j($email_subject) ?>	    
        }
        , {
            xtype: 'htmleditor',
            //xtype: 'textareafield',
            name: 'f_text',
            fieldLabel: 'Testo del messaggio',
            layout: 'fit',
            anchor: '96%',
            height: '100%',
		    allowBlank: false,
		    value: html_text,
		    flex: 1, //riempie tutto lo spazio
			hideLabel: true,
            style: 'margin:0' // Remove default margin		              
        }],

        buttons: [{
            text: 'Invia',
            scale: 'large',
            iconCls: 'icon-email_send-32',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	var m_win = this.up('window');	            	
	                form.submit(
								{
		                            //waitMsg:'Loading...',
		                            success: function(form,action) {		                            	
										m_win.close();		        
										Ext.MessageBox.alert('Avviso', 'Email inviata correttamente');                    	
		                            },
		                            failure: function(form,action){
		                                //Ext.MessageBox.alert('Erro');
		                            }
		                        }	                	
	                );
		            //this.up('window').close();            	                	                
	             }            
        		}]
    });


		// create and show window
		var win = new Ext.Window({
		  width: 800
		, height: 380
		, minWidth: 300
		, minHeight: 300
		, plain: true
		, title: 'Invia e-mail'
		, layout: 'fit'
		, border: true
		, closable: true
		, items: f
		});
		win.show().maximize();  
    	
    		
    		
    	});		
		
	});    
    
    
    
    
    

  	function invia_email2(){
	  html_text = document.getElementsByTagName('html')[0].innerHTML;
  		
      var postform = document.getElementById("my_form")
        if(!postform){
                var postform = document.createElement('form');
                postform.setAttribute('id','my_form');
            postform.setAttribute('name','my_form');
                postform.setAttribute('method','POST');
                        postform.setAttribute('target','my_iframe');
                postform.setAttribute('action', 'send_email.php');
            document.body.appendChild(postform);
        }

	var input = document.createElement('input');
    input.type = 'hidden';
    input.name = 'html_text';
    input.value = html_text; //JSON.stringify(document.getElementsByTagName('html')[0].innerHTM);
    postform.appendChild(input);

    postform.submit();   		
  		
  	}
  </script>
  
  
  
  
  

 </head>
 <body>
 	
<div class="page-utility noPrint">
	<A HREF="javascript:window.print()"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>	
	<A HREF="#" onclick="window.open('data:application/vnd.ms-excel,' + '<html><head>' + escape(document.getElementsByTagName('style')[0].outerHTML) + '</head>' + '<body>' + escape(document.getElementById('my_content').outerHTML) + '</body></html>');"><img src=<?php echo img_path("icone/48x48/save.png") ?>></A>
	<img id="p_send_email" src=<?php echo img_path("icone/48x48/address_black.png") ?>>		
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>		
</div> 	

<div id='my_content'>

<?php


//costruzione sql
$ar_seleted_stato = json_decode($_REQUEST['list_selected_stato']);
if (count($ar_seleted_stato) > 0 && strlen($_REQUEST['list_selected_stato']) > 0){
	$m_where = " AND RDRIFE IN (" . sql_t_IN($ar_seleted_stato) . ") ";
} else $m_where = '';
	



/***********************************************************************************************
 * NUOVO REPORT - RIEPILOGO ORDINI RIPROGRAMMATI
 ***********************************************************************************************/
if ($_REQUEST['tipo_report'] == 'RIEPILOGO_ORDINI_PROGRAMMATI'){
	
	if (strlen($_REQUEST['area_spedizione']) > 0)
		$m_where .= " AND TA_ITIN.TAASPE = '{$_REQUEST['area_spedizione']}'"; 
	
	if (strlen($_REQUEST['num_lotto']) > 0)
	    $m_where .= " AND ADNRLO = {$_REQUEST['num_lotto']}"; 

	if ($_REQUEST['sottotipo'] == 'ELENCO')
		$stacco_per_data = 'ADDTEP';
	else	
		$stacco_per_data = 'ADDTRI';
	
	if (strlen($_REQUEST['periodo_ora_dal']) > 0)
		$ora_dal = $_REQUEST['periodo_ora_dal'];
	else
		$ora_dal = 0;
	
	$sql = "SELECT AD.*, TA_ASPE.TAKEY1 AS C_ASPE, TA_ASPE.TADESC AS D_ASPE, TA_RIPRO.TADESC AS D_RIPRO
				FROM {$cfg_mod_Spedizioni['file_deroghe']} AD
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN 
		  			ON '{$id_ditta_default}' = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = ADCITI
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE 		
					ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_RIPRO
					ON TA_RIPRO.TADT = '{$id_ditta_default}' AND TA_RIPRO.TATAID = 'RIPRO' AND TA_RIPRO.TAKEY1 = AD.ADAUTOR 		  			  
				WHERE  ADTP ='R' {$m_where}
				AND ADDTRI >= {$_REQUEST['periodo_dal']} AND ADDTRI <= {$_REQUEST['periodo_al']}
				AND ADHMRI >= {$ora_dal}
				ORDER BY {$stacco_per_data}, TA_ITIN.TAASPE, /*ADHMRI, ADCCON, ADNRDO,*/ ADNRCA, ADSELO
			";
	
			$stmt = db2_prepare($conn, $sql);
			$result = db2_execute($stmt);
	
			$ar = array();
			$ar_tot = array();
			
			

			
			$ar_tot["TOTALI"] = array();
			
			while ($r = db2_fetch_assoc($stmt)) {
				
				$d_ar = &$ar;

				if ($_REQUEST['sottotipo'] == 'ELENCO')
					{$f_liv1 = trim($r["ADDTEP"]); 			$d_liv1 = trim($r["ADDTEP"]);}
				else
					{$f_liv1 = trim($r["ADDTRI"]); 			$d_liv1 = trim($r["ADDTRI"]);}
				
				$f_liv2 = trim($r["C_ASPE"]); 			$d_liv2 = trim($r["D_ASPE"]);
				
				
				//liv1
				if (!isset($d_ar[$f_liv1]))
					$d_ar[$f_liv1] = array("cod" => $f_liv1, "descr"=>$d_liv1, "val" => array(), "children"=>array());
			
					$tmp_ar = &$d_ar[$f_liv1];
			
					//Per sapere su quante date, articoli.... ho problemi con un certo fornitore
					$tmp_ar["val"]['ADDTRI'][$r['ADDTRI']] +=1;
					$tmp_ar["val"]['ADDTEP'][$r['ADDTEP']] +=1;
					$tmp_ar["val"]['TDCCON'][$r['TDCCON']] +=1;
					$tmp_ar["val"]['TDONDO'][$r['TDONDO']] +=1;
					$tmp_ar["val"]['RDART'][$r['RDART']] +=1;
					$tmp_ar["val"]['RDDTDS'][$r['RDDTDS']] +=1;
					$tmp_ar["val"]['RDODER'][$r['RDODER']] +=1;
					
					$d_ar = &$tmp_ar['children'];
					//liv2
					if (!isset($d_ar[$f_liv2]))
						$d_ar[$f_liv2] = array("cod" => $f_liv2, "descr"=>$d_liv2, "val" => array(), "children"=>array());
							
						$tmp_ar = &$d_ar[$f_liv2];
							
						//Per sapere su quante date, articoli.... ho problemi con un certo fornitore
						$tmp_ar["val"]['ADDTRI'][$r['ADDTRI']] +=1;
						$tmp_ar["val"]['ADDTEP'][$r['ADDTEP']] +=1;
						$tmp_ar["val"]['TDCCON'][$r['TDCCON']] +=1;
						$tmp_ar["val"]['TDONDO'][$r['TDONDO']] +=1;
						$tmp_ar["val"]['RDART'][$r['RDART']] +=1;
						$tmp_ar["val"]['RDDTDS'][$r['RDDTDS']] +=1;
						$tmp_ar["val"]['RDODER'][$r['RDODER']] +=1;
					
						//AGGIUNGO IL RECORDI IN LINEA
						$tmp_ar['children'][] = $r;
			
			} //while
			
			
			if (empty($d_ar)){
				echo "<h3 style='color: red;'>Non ci sono righe nel periodo selezionato</h3>";
				exit;
			}
			
			

			if ($_REQUEST['sottotipo'] == 'ELENCO'){
				//titolo pagina
				if ($_REQUEST['periodo_dal'] == $_REQUEST['periodo_al'])
					$txt_filtro_data = " il " . print_date($_REQUEST['periodo_dal']);
				else
					$txt_filtro_data = " dal " . print_date($_REQUEST['periodo_dal']) . " al " . print_date($_REQUEST['periodo_al']);
				
				if (strlen($_REQUEST['periodo_ora_dal']) > 0)
					$txt_filtro_data .= " (ora di inizio: " . print_ora($_REQUEST['periodo_ora_dal']) . ")";
				
				echo "<div class=\"noPrint\"><align=rigth><p align=right>Elenco ordini riprogrammati " . $txt_filtro_data . "<p></div>";
			}
				
			
			$c_tot = 0;
			foreach ($ar as $kdata => $ar_data){
				foreach ($ar_data['children'] as $karea => $ar_area){
					$c_tot++;

					//salto pagina se non sono la prima
					if ($c_tot > 1)
						echo "<div class=\"page-break\"></div>";					
					
					if ($_REQUEST['sottotipo'] == 'ELENCO'){
						//titolo pagina (dopo salto pagina)
						echo "<div class=\"onlyPrint\"><align=rigth><p align=right>Elenco ordini riprogrammati " . $txt_filtro_data . "<p></div>";
					}					
					
					if ($_REQUEST['sottotipo'] == 'ELENCO')
					    echo "<br/>&nbsp;<br/><h4><font size=5>{$ar_area['descr']}</font> - Data evasione programmata: <font size=5><b>" . print_date($kdata) . "</b></font></h4><br/>";
					else  
						echo "<br/>&nbsp;<br/><h2>{$ar_area['descr']}<br/>Rapporto ordini riprogrammati il " . print_date($kdata) . "</h2>";
					
					
					
					echo "<table class=acs_report>";
					
						echo "<tr>";
						
							echo "<th>Ordine Cliente</th>";						
							echo "<th>Carico</th>";
							echo "<th>Lotto/seq</th>";
							echo "<th>Causale<br>/Commento</th>";
						if ($_REQUEST['sottotipo'] != 'ELENCO'){										
							echo "<th>Denominazione</th>";
							echo "<th>Cliente</th>";
							echo "<th>Operatore</th>";
							echo "<th>Ora</th>";
							echo "<th>Evas.Programm.</th>";
						}									
						echo "</tr>";
						
					
					$num_righe = 0;
					foreach ($ar_area['children'] as $krec => $r){
						
						//ogni tot righe salto pagine
						if ($num_righe > 0 && ($num_righe % $_REQUEST['righe_per_pagina']) == 0){
							echo "</table>";
							echo "<div class=\"page-break\"></div>";
							
							if ($_REQUEST['sottotipo'] == 'ELENCO'){
								//titolo pagina (dopo salto pagina)
								echo "<div class=\"onlyPrint\"><align=rigth><p align=right>Elenco ordini riprogrammati " . $txt_filtro_data . "<p></div>";
							}
								
							if ($_REQUEST['sottotipo'] == 'ELENCO')
								echo "<br/>&nbsp;<br/><h4>{$ar_area['descr']} - Data evasione programmata: <font size=5><b>" . print_date($kdata) . "</b></font></h4><br/>";
										else
											echo "<br/>&nbsp;<br/><h2>{$ar_area['descr']}<br/>Rapporto ordini riprogrammati il " . print_date($kdata) . "</h2>";
												
							
							echo "<table class=acs_report>";							
							//echo "<tr class=page-break></tr>";
							$num_righe = 0;
						}
						
						$num_righe++;
						echo "<tr>";
							echo "<td>" . implode('_' , array(trim($r['ADAADO']), "<font size=5><b>" . trim($r['ADNRDO']) . "</b></font>", "<font size=5>" . trim($r['ADTPDO']) . "</font>")) . "</td>";
							if ($cfg_mod_Spedizioni['carico_per_data'] == 'Y')
								echo "<td>" . implode('_' , array(trim($r['ADTPCA']), trim($r['ADAACA']), substr(trim($r['ADNRCA']), 0, strlen(trim($r['ADNRCA'])) - 2) . " <b><font size=5>" . substr(trim($r['ADNRCA']), strlen(trim($r['ADNRCA'])) - 2, 2) . "</font></b>" )) . "</td>";
							else
								echo "<td>" . implode('_' , array(trim($r['ADTPCA']), trim($r['ADAACA']), trim($r['ADNRCA']))) . "</td>";
							echo "<td>" . implode('_' , array(trim($r['ADTPLO']), trim($r['ADAALO']), trim($r['ADNRLO']))) . " <font size=5><b>" . trim($r['ADSELO']) . "</b></font></td>";
							
							echo "<td><font size=5>" . trim($r['D_RIPRO']) . " " . trim($r['ADCOMM']) . "</font></td>";
						if ($_REQUEST['sottotipo'] != 'ELENCO'){	
							echo "<td >" . trim($r['ADDCON']) . "</td>";
							echo "<td >" . trim($r['ADCCON']) . "</td>";
							echo "<td>" . trim($r['ADUSRI']) .  "</td>";
							echo "<td>" . print_ora($r['ADHMRI']) .  "</td>";
							echo "<td align=center>" . print_date($r['ADDTEP']) . "</td>";
						}	
						echo "</tr>";
						
					}
					
					echo "</table>";

				}
			}			
			
			
			
			
						
	
	?>


<?php  exit; } ?>
<?php

	if (strlen($_REQUEST['periodo_ora_dal']) > 0)
		$ora_dal = $_REQUEST['periodo_ora_dal'];
	else
		$ora_dal = 0;

	$sql = "SELECT *
			FROM {$cfg_mod_Spedizioni['file_deroghe']} AD 
			 INNER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
				ON AD.ADRGES = TD.TDDOCU 
			WHERE  ADTP ='R' {$m_where}
	      	AND ADDTRI >= {$_REQUEST['periodo_dal']} AND ADDTRI <= {$_REQUEST['periodo_al']}
	      	AND ADHMRI >= {$ora_dal} 
			ORDER BY ADDTRI, TDCCON, TDONDO 
		";

$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt);

$ar = array();
$ar_tot = array();



/***********************************************************************************************
 *  ELENCO 
 ***********************************************************************************************/
 
if (1 == 1) {
 echo "<h2>Riepilogo riprogrammazione ordini</h2>";

 $f_liv1 = "ADAUTOR"; 			$d_liv1 = "ADAUTOR";
 
 $ar_tot["TOTALI"] = array();
 
 while ($r = db2_fetch_assoc($stmt)) {
	 	
		//liv1
		if (!isset($d_ar[$r[$f_liv1]]))	
				$d_ar[$r[$f_liv1]] = array("cod" => $r[$f_liv1], "descr"=>$r[$d_liv1], 
																  "val" => array(), 
																  "children"=>array()); 														  
																  																  
		 $tmp_ar = &$d_ar[$r[$f_liv1]];

		 //Per sapere su quante date, articoli.... ho problemi con un certo fornitore
		 $tmp_ar["val"]['ADDTRI'][$r['ADDTRI']] +=1;		 
		 $tmp_ar["val"]['ADDTEP'][$r['ADDTEP']] +=1; 
		 $tmp_ar["val"]['TDCCON'][$r['TDCCON']] +=1;					
		 $tmp_ar["val"]['TDONDO'][$r['TDONDO']] +=1;		 
		 $tmp_ar["val"]['RDART'][$r['RDART']] +=1;		 
		 $tmp_ar["val"]['RDDTDS'][$r['RDDTDS']] +=1;		 
		 $tmp_ar["val"]['RDODER'][$r['RDODER']] +=1;		 
		 
		 //AGGIUNGO IL RECORDI IN LINEA
		 $tmp_ar['children'][] = $r;		 
	
 } //while


if (empty($d_ar)){
	echo "<h3 style='color: red;'>Non ci sono righe nel periodo selezionato</h3>";
	exit;
}
 

foreach ($d_ar as $kf => $f){
	echo "<h2>Causale: " . $s->decod_std('RIPRO', $f['cod']) . "</h2>";
	
	$liv_2_inline = '-----xxxxxx------';
	
	echo "<table class=acs_report>";

	  echo "<tr>";
		echo "<th>Data<BR> (# " . count($f['val']['ADDTRI']) . ")</th>";
		echo "<th>Operatore</th>";		
		echo "<th>Cliente<BR> (# " . count($f['val']['TDCCON']) . ")</th>";
		echo "<th>Denominazione</th>";		
		echo "<th>Ordine<br>Cliente<BR> (# " . count($f['val']['TDONDO']) . ")</th>";
		
		echo "<th>Commento</th>";		
		
		echo "<th>Ricezione</th>";		
		echo "<th>Conferma</th>";		
		echo "<th>Evas.Programm.</th>";		
		
		echo "<th>Colli</th>";
		echo "<th>Volume</th>";
		echo "<th>Importo</th>";				
	 echo "</tr>";				
	
	
	foreach ($f['children'] as $r){
	
		if ($liv_2_inline != implode("_", array($r['RDTPNO'], $r['RDRIFE']))){
			echo "<tr class=ag_liv2>";
				echo "<td colspan=12>" . implode(" - ", array($r['RDTPNO'], $r['RDRIFE'])) . "</td>";
			echo "</tr>";
			$liv_2_inline = implode("_", array($r['RDTPNO'], $r['RDRIFE']));
		}
	
		echo "<tr>";
			echo "<td align=center>" . print_date($r['ADDTRI']) . "</td>";
			echo "<td>" . trim($r['ADUSRI']) .  "</td>";			
			echo "<td>" . trim($r['TDCCON']) .  "</td>";			
			echo "<td>" . trim($r['TDDCON']) .  "</td>";			
			echo "<td nowrap>" . implode('_' , array(trim($r['TDOADO']), trim($r['TDONDO']), trim($r['TDOTPD']))) . " (" . trim($r['TDSTAT']) . ")</td>";

			echo "<td >" . trim($r['ADCOMM']) . "</td>";			
			
			
			echo "<td align=center>" . print_date($r['TDODRE']) .  "</td>";
			echo "<td align=center>" . print_date($r['TDDTCF']) . "</td>";
			echo "<td align=center>" . print_date($r['ADDTEP']) . "</td>";
			
			echo "<td align=right>" . trim($r['TDTOCO']) .  "</td>";
			echo "<td align=right>" . n(trim($r['TDVOLU'], 4)) .  "</td>";
			echo "<td align=right>" . n(trim($r['TDINFI'], 2)) .  "</td>";			
			
		echo "</tr>";
	}
	echo "</table>";
} 

 
//echo "<pre>"; print_r($d_ar); echo "</pre>";
 
 
} //if elenco






 
 
 		
?>
  </div>
 </body>
</html>		