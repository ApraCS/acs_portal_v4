<?php

require_once("../../config.inc.php");

$main_module = new Spedizioni();
$s			 = new Spedizioni();


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){

	//parametri del modulo (non legati al profilo)
	$mod_js_parameters = $main_module->get_mod_parameters();
	
	
	//funcioni generiche
	switch($_REQUEST['fn']) {
		case "toggle_fl_solo_con_carico_assegnato":
			$s->toggle_fl_solo_con_carico_assegnato();
		break;
	}
	
	/* CAMPO DI CUI MOSTRARE IL DETTAGLIO */
//	$s->set_calendar_field_dett($_REQUEST['set_field']); //se mi chiede di cambiare campo di dettaglio
//	$field_dett = $s->imposta_field_dett(); //recupero il campo di dettaglio
	$field_dett = $_REQUEST['field_dett'];
	
	/* DATE CALENDARIO */
	$n_giorni 	= 14;

//	$s->set_initial_calendar_date($_REQUEST['move']); //se si muove sul calendario	
//	$da_data_txt = $s->get_initial_calendar_date(); //recupero data iniziale calendario

	$da_data_txt = $_REQUEST['da_data'];
	
	$a_data_txt = date('Y-m-d', strtotime($da_data_txt . " +{$n_giorni} days"));
	$da_data 	= new DateTime($da_data_txt);
	$a_data 	= new DateTime($a_data_txt);
	
	$root_id = $_REQUEST["node"]; //radice o sottonodo?
	$its = $s->get_elenco_itinerari($da_data, $n_giorni, $root_id, $field_dett, 'TDDTSP', $_REQUEST);
	$appLog->add_bp('get_elenco_itinerari - END');
	
	$ars_ar = $s->crea_array_valori($its, $field_dett, $root_id, 'TDDTSP');
	$appLog->add_bp('crea_array_valori - END');
	
	$ar_righe_totali = array();	
	
	//Aggiunto riga tot importo
	$ar_righe_totali[] = 'IMP';
	$ar_righe_totali[] = 'PS'; //capacita' spedizione
	
	//i totali devono essere visti senza filtri sulla riservatezza
	if ($root_id == 'root' || $root_id == '') {
		$totali_its = $s->get_elenco_itinerari($da_data, $n_giorni, $root_id, $field_dett, 'TDDTSP', $_REQUEST, 'Y');
		$totali_ars_ar = $s->crea_array_valori($totali_its, $field_dett, $root_id, 'TDDTSP');
		$ars_ar['tot'] = $totali_ars_ar['tot'];
	}	
	
	$ars = $s->crea_alberatura_calendario_json($ars_ar, $field_dett, $root_id, $da_data_txt, $n_giorni, $ar_righe_totali, 'TDDTSP');	
	$appLog->add_bp('crea_alberatura - END');
	
	//output del risultato	
	echo $ars;
	
	$appLog->save_db();
	exit;
} //get_json_data


// ******************************************************************************************
// FORM FILTRI
// ******************************************************************************************
if ($_REQUEST['fn'] == 'open_form_filtri'){
	?>
{"success":true, "items": [

        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: '',
            url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=save_filtri',
            
            items: [
{
		                    xtype: 'radiogroup',
		                    layout: 'hbox',
		                    fieldLabel: '',
		                    flex: 3,
		                    defaults: {
		                     width: 105,
		                     padding: '10 10 10 10'
		                    },
		                    items: [{
		                    	xtype: 'label',
		                    	text: 'Carico',
		                    	width: 70
		                    },
							{
				                boxLabel: 'Tutti',
				                name: 'f_carico_assegnato',			                
				                inputValue: ''
				            }, {
				                boxLabel: 'Solo con carico assegnato',			                
				                name: 'f_carico_assegnato',
				                inputValue: 'Y',
				                width: 300
				            }
		                   ]
		                }, {
							flex: 1,
							labelWidth: 70,
							name: 'f_divisione',
							xtype: 'combo',
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	anchor: '-15',														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,
							labelWidth: 70,
							name: 'f_mercato',
							xtype: 'combo',
							fieldLabel: 'Mercato',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	anchor: '-15',														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->find_TA_std('MERCA'), ""); ?> 	
								    ] 
								}						 
							}
				],
			buttons: [{
	            text: 'Conferma',
	            handler: function() {
	            	var form = this.up('form').getForm();
	            	
	        	    upd_panel = Ext.getCmp('panel-flights');
	        	                	
					m_p = Ext.merge({}, form.getValues());
					if (typeof(form.getValues().f_carico_assegnato) === 'undefined' && 
						typeof(form.getValues().f_divisione) === 'undefined' &&
						typeof(form.getValues().f_mercato) === 'undefined'
						)
					 icon_status = 'disattivo'
					else
					 icon_status = 'attivo'

					//upd_panel.store.proxy.extraParams = m_p;
					
					upd_panel.store.proxy.extraParams['f_carico_assegnato'] = form.getValues().f_carico_assegnato;  					
					upd_panel.store.proxy.extraParams['f_divisione'] = form.getValues().f_divisione;
					upd_panel.store.proxy.extraParams['f_mercato'] = form.getValues().f_mercato;
										
					upd_panel.store.load();
	            	if (Ext.get("flight_fl_solo_con_carico_assegnato") != null){
	            	 //aggiorno l'icona del filtro (se ho applicato filtri o meno)
	            	  if(icon_status == 'disattivo')
	            	 	Ext.get('flight_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_disattivo.png"); ?>;
	            	  else
	            	  	Ext.get('flight_fl_solo_con_carico_assegnato_img').dom.src = <?php echo img_path("icone/48x48/filtro_attivo.png"); ?>;
	            	}            	            	

		            this.up('window').close();            	                	                
	            }
	        }],             
				
        }
]}
<?php	
	exit;
} //open_form_filtri


?>
{
 "success":true, "items":
 {
  		xtype: 'treepanel',
  		id: 'panel-flights',
  		<?php echo make_tab_closable(); ?>,

        tbar: new Ext.Toolbar({
            items:['<b>Programmazione evasione ordini per data di carico/spedizione</b>', '->'
	           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
	           	<?php echo make_tbar_closable() ?>
            ]
        }),  		
  		
		selType: 'cellmodel',
		cls: 'supply_desk',		
        title: 'Flight',
        collapsible: true,
        useArrows: true,
        rootVisible: false,
        store: Ext.create('Ext.data.TreeStore', {
	        model: 'RowCalendar',
	        proxy: {
	            type: 'ajax',
	            url : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
	            extraParams: {
        		    	da_data: <?php echo oggi_AS_date(); ?>,
        		    	field_dett: 'VOL' 
        		}
	        },
	        
	        reader: new Ext.data.JsonReader(),
	        
	        folderSort: false,
	        
			listeners: {
	        	beforeload: function(store, options) {
	        		Ext.getBody().mask('Loading... ', 'loading').show();
	        		}
	        }
    	}), 
        multiSelect: true,
        singleExpand: false,
        loadMask: true,
        
		listeners: {
		
		
	 			afterrender: function (comp) { 			
	 				//comp.getStore().load();

	 				
						// attacco gli eventi e tooltip
						if (Ext.get("flight_fl_solo_con_carico_assegnato") != null){		
					        Ext.get("flight_fl_solo_con_carico_assegnato").on('click', function(){
					        	acs_show_win_std('Selezione filtri', '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_form_filtri', null, 500, 220, null, 'icon-filter-16');
					        });
					        Ext.QuickTips.register({target: "flight_fl_solo_con_carico_assegnato",	text: 'Attiva/Disattiva filtri su flight'});	        
						}	
	 				
	 				
	 					url_tmp = comp.getStore().proxy.url;
	 				
						// attacco gli eventi											
				        Ext.get("flight_next_1d").on('click', function(){
				        	t = js_date_from_AS(comp.getStore().proxy.extraParams.da_data);
				        	t.setDate(t.getDate() + 1);
				        	comp.getStore().proxy.extraParams.da_data = Ext.Date.format(t, 'Ymd'); 				        												        
							comp.getStore().load();
				        });	        
				        Ext.get("flight_next_1w").on('click', function(){
				        	t = js_date_from_AS(comp.getStore().proxy.extraParams.da_data);
				        	t.setDate(t.getDate() + 7);
				        	comp.getStore().proxy.extraParams.da_data = Ext.Date.format(t, 'Ymd'); 				        												        
							comp.getStore().load();			
				        });
				        Ext.get("flight_prev_1d").on('click', function(){
				        	t = js_date_from_AS(comp.getStore().proxy.extraParams.da_data);
				        	t.setDate(t.getDate() - 1);
				        	comp.getStore().proxy.extraParams.da_data = Ext.Date.format(t, 'Ymd'); 				        												        
							comp.getStore().load();			
				        });
				        Ext.get("flight_prev_1w").on('click', function(){
				        	t = js_date_from_AS(comp.getStore().proxy.extraParams.da_data);
				        	t.setDate(t.getDate() - 7);
				        	comp.getStore().proxy.extraParams.da_data = Ext.Date.format(t, 'Ymd'); 				        												        
							comp.getStore().load();			
				        });
				        Ext.get("flight_reload").on('click', function(){
							comp.getStore().load();			
				        });				        	 			
				        
						if (Ext.get("flight_cal_report_agenda_trasportatori_autisti") != null){		
					        Ext.get("flight_cal_report_agenda_trasportatori_autisti").on('click', function(){
					        	acs_show_win_std('Agenda impegno trasportatori/autisti', 'acs_report_agenda_trasportatori_autisti.php?fn=get_parametri_form', null, 400, 500, null, 'iconPrint')            
					        });
					        Ext.QuickTips.register({target: "flight_cal_report_agenda_trasportatori_autisti",	text: 'Agenda trasportatori'});	        
						}
				        
				        
				    	//tooltip
				        if (Ext.get("flight_prev_1w") != null)	Ext.QuickTips.register({target: "flight_prev_1w",	text: 'Settimana indietro'});
				        if (Ext.get("flight_prev_1d") != null)	Ext.QuickTips.register({target: "flight_prev_1d",	text: 'Giorno indietro'});        
				        if (Ext.get("flight_next_1d") != null)	Ext.QuickTips.register({target: "flight_next_1d",	text: 'Giorno avanti'});        
				        if (Ext.get("flight_next_1w") != null)	Ext.QuickTips.register({target: "flight_next_1w",	text: 'Settimana avanti'});        
				        if (Ext.get("flight_reload") != null)	Ext.QuickTips.register({target: "flight_reload",	text: 'Rigenerazione contenuti finestra corrente'});				        
				        
									        	
							if (Ext.get("flight_avanzamento_produzione") != null){
						        Ext.get("flight_avanzamento_produzione").on('click', function(){
						        	mp = Ext.getCmp('m-panel');
						        	av_prod = Ext.getCmp('panel-avanzamento_produzione');
						
						        	if (av_prod){
						        		av_prod.store.reload();
						        		av_prod.show();		    	
						        	} else {
						
						        		//carico la form dal json ricevuto da php
						        		Ext.Ajax.request({
						        		        url        : 'acs_panel_avanzamento_produzione.php',
						        		        method     : 'GET',
						        		        waitMsg    : 'Data loading',
						        		        success : function(result, request){
						        		            var jsonData = Ext.decode(result.responseText);
						        		            mp.add(jsonData.items);
						        		            mp.doLayout();
						        		            mp.show();
						        		            av_prod = Ext.getCmp('panel-avanzamento_produzione').show();        		            
						        		        },
						        		        failure    : function(result, request){
						        		            Ext.Msg.alert('Message', 'No data to be loaded');
						        		        }
						        		    });
						
						        	}            
						        });
						        Ext.QuickTips.register({target: "flight_avanzamento_produzione",	text: 'Avanzamento carichi'});
							}				        	
									        	
	 				
	 				}, 	
		
		
				load: function(sender, node, records) {
				
			        	//var c = Ext.getCmp('panel-flights');
			        	var c = this;
			        	
			        	Ext.getBody().unmask();
						
						c.headerCt.getHeaderAtIndex(5+1).setText(c.store.proxy.reader.jsonData.columns.c1);						        	
						c.headerCt.getHeaderAtIndex(6+1).setText(c.store.proxy.reader.jsonData.columns.c2);									
						c.headerCt.getHeaderAtIndex(7+1).setText(c.store.proxy.reader.jsonData.columns.c3);						        	
						c.headerCt.getHeaderAtIndex(8+1).setText(c.store.proxy.reader.jsonData.columns.c4);									
						c.headerCt.getHeaderAtIndex(9+1).setText(c.store.proxy.reader.jsonData.columns.c5);									
						c.headerCt.getHeaderAtIndex(10+1).setText(c.store.proxy.reader.jsonData.columns.c6);									
						c.headerCt.getHeaderAtIndex(11+1).setText(c.store.proxy.reader.jsonData.columns.c7);									
						c.headerCt.getHeaderAtIndex(12+1).setText(c.store.proxy.reader.jsonData.columns.c8);						        	
						c.headerCt.getHeaderAtIndex(13+1).setText(c.store.proxy.reader.jsonData.columns.c9);									
						c.headerCt.getHeaderAtIndex(14+1).setText(c.store.proxy.reader.jsonData.columns.c10);						        	
						c.headerCt.getHeaderAtIndex(15+1).setText(c.store.proxy.reader.jsonData.columns.c11);									
						c.headerCt.getHeaderAtIndex(16+1).setText(c.store.proxy.reader.jsonData.columns.c12);									
						c.headerCt.getHeaderAtIndex(17+1).setText(c.store.proxy.reader.jsonData.columns.c13);									
						c.headerCt.getHeaderAtIndex(18+1).setText(c.store.proxy.reader.jsonData.columns.c14);
						
						//coloro il bordo in base allo stadio
						for (var i=6; i<=19; i++) {
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay0');							
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay1');
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay2');
							c.headerCt.getHeaderAtIndex(i).removeCls('stadioDay3');							
						}

						c.headerCt.getHeaderAtIndex(5+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c1);						
						c.headerCt.getHeaderAtIndex(6+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c2);
						c.headerCt.getHeaderAtIndex(7+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c3);
						c.headerCt.getHeaderAtIndex(8+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c4);
						c.headerCt.getHeaderAtIndex(9+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c5);
						c.headerCt.getHeaderAtIndex(10+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c6);
						c.headerCt.getHeaderAtIndex(11+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c7);
						c.headerCt.getHeaderAtIndex(12+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c8);
						c.headerCt.getHeaderAtIndex(13+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c9);
						c.headerCt.getHeaderAtIndex(14+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c10);
						c.headerCt.getHeaderAtIndex(15+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c11);
						c.headerCt.getHeaderAtIndex(16+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c12);
						c.headerCt.getHeaderAtIndex(17+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c13);
						c.headerCt.getHeaderAtIndex(18+1).addCls('stadioDay' + c.store.proxy.reader.jsonData.stadio.c14);



						//tooltip con settimana e stadio
						
						for (var i=6; i<=19; i++) {						
							Ext.QuickTips.unregister(c.headerCt.getHeaderAtIndex(i).el);
							
						}								
						
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(5+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c1,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c1,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(6+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c2,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c2,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(7+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c3,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c3,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(8+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c4,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c4,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(9+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c5,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c5,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(10+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c6,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c6,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(11+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c7,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c7,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(12+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c8,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c8,
						});																														
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(13+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c9,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c9,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(14+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c10,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c10,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(15+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c11,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c11,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(16+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c12,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c12,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(17+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c13,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c13,
						});
						Ext.QuickTips.register({
							target: c.headerCt.getHeaderAtIndex(18+1).el,
							title: c.store.proxy.reader.jsonData.header_qtip_h.c14,
							text: c.store.proxy.reader.jsonData.header_qtip_t.c14,
						});
						
						
				}, 
				
				
				
				celldblclick: {								

					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
				    	
						
						if (col_name=='task' && rec.get('liv')=='liv_totale' && (
								rec.get('id') == 'tot_VOL' || 
								rec.get('id') == 'tot_COL' ||
								rec.get('id') == 'tot_PAL' ||
								rec.get('id') == 'tot_ORD' ||
								rec.get('id') == 'tot_IMP' ||
								rec.get('id') == 'tot_CP'
								) ){										
							iView.store.treeStore.proxy.extraParams.field_dett = rec.get('id').substring(4, 10);
							iView.store.treeStore.load();							
						}
						if (rec.get('liv')=='liv_totale') return false;
						
						//per gli hold vado nello standard (per data evasione programmata)
						if (col_name=='fl_da_prog' && (rec.get('liv')=='liv_2' || rec.get('liv')=='liv_3' || rec.get('liv')=='liv_4')){
							mp = Ext.getCmp('m-panel');
							mp.add(
								show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_get_elenco_ordini_from_calendar_json.php?m_id=' + rec.get('id') + '&da_data=' +rec.get('da_data') + '&col_name=' + col_name, Ext.id())
			            	).show();						 
			              return false;
						}
						
				    	
				    	if (col_name!='task' && (rec.get('liv')=='liv_2' || rec.get('liv')=='liv_3' || rec.get('liv')=='liv_4')){							    							    									    		
							mp = Ext.getCmp('m-panel');
							mp.add(
								show_el_ordini(iView, rec, iColIdx, iCellEl, iRowEl, 'acs_panel_flights_el_ordini.php?m_id=' + rec.get('id') + '&da_data=' +rec.get('da_data') + '&col_name=' + col_name, Ext.id())
			            	).show();
						  return false; //previene expand/collapse di default
				    	}				    	
				    	
				    	
				    }
				},
				
					
					
            itemcontextmenu: function(view,record,item,index,e,eOpts){

                e.stopEvent();
                
				//recupero la cella
				var xPos = e.getXY()[0];
			    var cols = view.getGridColumns();
			    var colSelected = null;
			
			    for(var c in cols) {
			
			        var leftEdge = cols[c].getPosition()[0];
			        var rightEdge = cols[c].getSize().width + leftEdge;
			
			        if(xPos>=leftEdge && xPos<=rightEdge) {
			            colSelected = cols[c];
			        }
			    }				


			    if (record.get('liv') != 'liv_4'){
				  var voci_menu = [];                
			      voci_menu.push({
		      		text: 'Seleziona ordini',
		    		iconCls : 'icon-leaf-16',      		
		    		handler: function() {
		    			acs_show_win_std('Selezione ordini', 'acs_seleziona_ordini.php?fn=open_form', {
		        				solo_senza_carico: 'N',
		        				grid_id: view.id,
		        				tipo_elenco: 'DA_FLIGHT',
			        			da_data: record.get('da_data'),
			        			col_name: colSelected.dataIndex,
			        			record_liv: record.get('liv'),
			        			record_id:  record.get('id'),
		    					},		        				 
		        				1024, 600, {}, 'icon-leaf-16');
		    		}
				  }, {

					  text: 'Selezione spedizioni',
			    		iconCls : 'icon-delivery-16',      		
			    		handler: function() {
							var rec = record;
				    		var col_name = colSelected.dataIndex;
				    		var col_name_exp = col_name.split('_');
				        	var t = js_date_from_AS(rec.get('da_data'));
				        	t.setDate(t.getDate() + parseInt(col_name_exp[1]) -1);
				        	var liv_exp = rec.get('id').split("|");;
				        	var m_data = Ext.Date.format(t, 'Ymd')		
				        	var m_itin = liv_exp[4];
				        	var m_area = liv_exp[2];
				        	//apro win per selezione spedizioni
	
			    			var aree_abilitate_a_filtro_sped = <? echo acs_je($cfg_mod_Spedizioni['aree_abilitate_a_filtro_sped']); ?>;
	
								var my_listeners = {
			        					onSelected: function(from_win, sped_ar){	
	
				    							mp = Ext.getCmp('m-panel');
				    							mp.add(
				    								show_el_ordini(this, rec, 0, 0, 0, 
						    							'acs_panel_flights_el_ordini.php?m_id=' + rec.get('id') + '&da_data=' +rec.get('da_data') + '&col_name=' + col_name, 
						    							Ext.id(),
						    							sped_ar
						    							)
				    			            	).show();			        						
	
									        	from_win.close();
				        						
							        		}
					    				};					        	
	
						        	acs_show_win_std('Selezione spedizioni', 'acs_form_json_filtra_spedizioni_itin_data.php?fn=open_form', 
													{tipo_elenco: 'DA_FLIGHT', itin: m_itin, data: m_data}, 
													1050, 450, my_listeners, 'icon-delivery-16');		
						        	
						    		return false;
			    		}


					  
				  } //estero
				  );
			    }

			      var menu = new Ext.menu.Menu({
			            items: voci_menu
				}).showAt(e.xy);
                             

            }					
					
					
					
					
					
		   }, //listeners

		viewConfig: {
		        getRowClass: function(record, index) {
							        
		        	v = record.get('liv');
		        	
			        	if (v=="liv_totale"){			    
			        		v = v + ' flight ';    		
				        	c = Ext.getCmp('panel-flights');
							if ('tot_' + c.store.proxy.reader.jsonData.field_dett == record.get('id'))
				        		v = v + ' dett_selezionato';												        		
			        	} //la riga di totale selezionata
			        						        	
		            return v;					            
		         	}   
		    },			


		<?php
			$ss = "";
			$ss .= "<img id=\"flight_prev_1w\"src=" . img_path("icone/48x48/button_grey_rew.png") . " height=30>";
			$ss .= "<img id=\"flight_prev_1d\"src=" . img_path("icone/48x48/button_grey_first.png") . " height=30>";
			$ss .= "<img id=\"flight_reload\" src=" .  img_path("icone/48x48/button_grey_repeat.png") . " height=30>";			
			$ss .= "<img id=\"flight_next_1d\" src=" . img_path("icone/48x48/button_grey_last.png") . " height=30>";
			$ss .= "<img id=\"flight_next_1w\" src=" . img_path("icone/48x48/button_grey_ffw.png") . " height=30>";
			$ss .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			
			$img_name = "filtro_disattivo";
			$ss .= "<span id=\"flight_fl_solo_con_carico_assegnato\" class=acs_button style=\"\">";
			$ss .= "<img id=\"flight_fl_solo_con_carico_assegnato_img\" src=" . img_path("icone/48x48/{$img_name}.png") . " height=30>";
			$ss .= "</span>";

			//APERTURA GRAFICO AGENDA TRASPORTATORI
			if ($auth->is_operatore_aziendale() == 1){
				$ss .= "&nbsp;&nbsp;&nbsp;";
				$ss .= "<span id=\"flight_cal_report_agenda_trasportatori_autisti\" class=acs_button style=\"\">";
				$ss .= "<img id=\"flight_cal_report_agenda_trasportatori_autisti_img\" src=" . img_path("icone/48x48/tras_plan.png") . " height=30>";
				$ss .= "</span>";
			}
				


/*
 * ADESSO E' TRA I BOTTONI DELLA FASCETTA			
			//APERTURA Riepilogo avanzamento produzione
			$ss .= "&nbsp;&nbsp;&nbsp;";
			$ss .= "<span id=\"flight_avanzamento_produzione\" class=acs_button style=\"\">";
			$ss .= "<img id=\"flight_avanzamento_produzione_img\" src=" . img_path("icone/48x48/bandiera_scacchi.png") . " height=30>";
			$ss .= "</span>";
*/			
				
				
			
			
			//img per intestazioni colonne flag
			$cf1 = "<img src=" . img_path("icone/48x48/divieto.png") . " height=25>";
			$cf2 = "<img src=" . img_path("icone/48x48/power_black.png") . " height=25>";
			$cf3 = "<img src=" . img_path("icone/48x48/warning_black.png") . " height=25>";
			$cf4 = "<img src=" . img_path("icone/48x48/button_blue_pause.png") . " height=25>";																					
			$cf5 = "<img src=" . img_path("icone/48x48/button_black_eject.png") . " height=25>";
		?>		


        //the 'columns' property is now 'headers'
        columns: [{
            xtype: 'treecolumn', 
            flex: 25,
            dataIndex: 'task',
            menuDisabled: true, sortable: false,
            text: '<?php echo $ss; ?>'
        },{
			    text: '<?php echo $cf1; ?>',
			    width: 30, tooltip: 'Clienti bloccati',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/divieto.png") ?> width=18>';}
		  },{
			    text: '<?php echo $cf2; ?>',
			    width: 30, tooltip: 'Ordini bloccati',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_ord_bloc')==1) return '<img src=<?php echo img_path("icone/48x48/power_black.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf3; ?>',
			    width: 30, tooltip: 'Articoli mancanti',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_art_mancanti')==1) return '<img src=<?php echo img_path("icone/48x48/warning_black.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf4; ?>',
            dataIndex: 'fl_da_prog', tooltip: 'Produzione non programmabile',         			    
			    width: 30,
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false,            		        
			renderer: function(value, p, record){if (record.get('fl_da_prog')==1) return '<img src=<?php echo img_path("icone/48x48/button_blue_pause.png") ?> width=18>';}         			    
		  },{
			    text: '<?php echo $cf5; ?>',
            dataIndex: 'DP_val', tooltip: 'Spedizioni da programmare',         			    
			    width: 50, align: 'right',
		    tdCls: 'tdAction',         			
            menuDisabled: true, sortable: false, renderer: floatRenderer0            		                			    
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_1',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass,
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_2',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_3',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_4',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_5',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_6',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_7',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		   
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_8',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_9',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_10',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_11',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_12',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_13',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		               		   
		  },{
			   text: '&nbsp;<br>&nbsp;', cls: 'plan-day',
		   width: 50,
		   dataIndex: 'd_14',
		   align: 'right',
		   menuDisabled: true, sortable: false,
		   renderer: getCellClass            		   
		  }        			          			  
        ]  
   }
 }  