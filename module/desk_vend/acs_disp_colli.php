<?php   

require_once "../../config.inc.php";


$s = new Spedizioni();
$main_module = new Spedizioni();



function sum_columns_value(&$ar_r, $r){
		global $s;

	$ar_r['colli_tot'] += $r['COLLI'];
	$ar_r['colli_disp'] +=$r['DISP'];
	$ar_r['colli_non_disp'] += ($r['COLLI'] - $r['DISP']);
	$ar_r['colli_tot_prod'] += $r['COLLI_TOT_PROD'];
	$ar_r['colli_disp_prod'] += $r['COLLI_DISP_PROD'];
	$ar_r['colli_non_disp_prod'] += ($r['COLLI_TOT_PROD'] - $r['COLLI_DISP_PROD']);
	$ar_r['colli_tot_riv'] += $r['COLLI_TOT_RIV'];
	$ar_r['colli_disp_riv'] += $r['COLLI_DISP_RIV'];
	$ar_r['colli_non_disp_riv'] += ($r['COLLI_TOT_RIV'] - $r['COLLI_DISP_RIV']);
	$ar_r['da_vern'] += $r['DA_VERN'];
	$ar_r['in_vern'] +=$r['IN_VERN'];
}

function parametri_sql_where($form_values){

	global $s;

	$sql_where.= " WHERE " . $s->get_where_std();
	
	$sql_where.= sql_where_by_combo_value('TD.TDCDIV', $form_values->f_divisione);
	
	$sql_where.= sql_where_by_combo_value('TD.TDASPE', $form_values->f_areaspe);
		
	$sql_where.= sql_where_by_combo_value('TD.TDCLOR', $form_values->f_tipologia_ordine);
		
	$sql_where.= sql_where_by_combo_value('TD.TDSTAT', $form_values->f_stato_ordine);
		
	//controllo HOLD
	$filtro_hold=$form_values->f_hold;
	
	if($filtro_hold== "Y")
		$sql_where.=" AND TDSWSP = 'Y'";
	if($filtro_hold== "N")
	    $sql_where.=" AND TDSWSP = 'N'";
	
	 $filtro_evasi=$form_values->f_evasi;
	
	  //solo ordini evasi
	if ($filtro_evasi == "Y"){
		$sql_where.= " AND TDFN11 = 1 ";
	    }
	 //solo ordini da evadere
	if ($filtro_evasi == "N"){
		$sql_where.= " AND TDFN11 = 0 ";
	    }

	return $sql_where;
}




if ($_REQUEST['fn'] == 'get_parametri_form'){

	//Elenco opzioni "Area spedizione"
	$ar_area_spedizione = $s->get_options_area_spedizione();
	$ar_area_spedizione_txt = '';

	$ar_ar2 = array();
	foreach ($ar_area_spedizione as $a)
		$ar_ar2[] = "[" . implode(',', array_map('sql_t_trim', $a)) . "]";
		$ar_area_spedizione_txt = implode(',', $ar_ar2);
		
   
?>

{"success":true, "items": [

{
	xtype: 'form',
	bodyStyle: 'padding: 10px',
	bodyPadding: '5 5 0',
	frame: false,
	title: '',
	//url: 'acs_riepilogo_spedizioni_carichi.php?fn=get_json_data_carichi',
	buttonAlign:'center',
	buttons: [<?php $f8 = new F8(); echo $f8->write_json_buttons($s->get_cod_mod(), "DISP_COLLI");  ?>{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	             handler: function() {
	                form = this.up('form').getForm();
	                            	                
		               if (form.isValid()){	                	                
			               acs_show_panel_std('acs_disp_colli.php', 'disp_colli', {form_values: form.getValues()});
			               this.up('window').close(); 	  
		                }	
	                
	                               
	            }
	        }],             
            
            items: [
	            
	             {
					xtype: 'fieldset',
					layout: 'anchor',
	                defaults: {
	                    anchor: '100%'
	                },
		             items: [{
						   xtype: 'datefield'
						   , startDay: 1 //lun.
						   , fieldLabel: 'Data'
						   , value: '<?php echo  print_date(oggi_AS_date(), "%d/%m/%Y"); ?>'
						   , name: 'f_data'
						   , format: 'd/m/Y'
						   , submitFormat: 'Ymd'
						   , allowBlank: false
						   , anchor: '-15'
						   , margin: "5 250 5 10"
			
						},
	             {
							name: 'f_areaspe',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Area di spedizione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "10 10 10 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								    <?php echo acs_ar_to_select_json(find_TA_std('ASPE'), '') ?> 
								    ] 
							}						 
						},   	{
							name: 'f_divisione',
							xtype: 'combo',
							multiSelect: true,
							fieldLabel: 'Divisione',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection: true,
						   	allowBlank: true,		
						    anchor: '-15',
						    margin: "15 10 5 10",						   													
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->options_select_divisioni(), '') ?>	
								    ] 
							}						 
						}, 
						{
							flex: 1,						
							name: 'f_tipologia_ordine',
							xtype: 'combo',
							 margin: "20 10 10 10",	
							 anchor: '-15',
							fieldLabel: 'Tipologia ordine',
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
						   	allowBlank: true,
						   	multiSelect: true,
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json(find_TA_std('TIPOV'), '') ?> 	
								    ] 
								}						 
							}, {
							flex: 1,							
							name: 'f_stato_ordine',
							xtype: 'combo',
							fieldLabel: 'Stato ordine',
							anchor: '-15',
							margin: "20 10 10 10",
							displayField: 'text',
							valueField: 'id',
							emptyText: '- seleziona -',
							forceSelection:true,
							multiSelect: true,
						   	allowBlank: true,														
							store: {
								autoLoad: true,
								editable: false,
								autoDestroy: true,	 
							    fields: [{name:'id'}, {name:'text'}],
							    data: [								    
								     <?php echo acs_ar_to_select_json($s->get_options_stati_ordine(), '') ?> 	
								    ] 
								}						 
							}, {
							name: 'f_hold',
							xtype: 'radiogroup',
							fieldLabel: 'Hold',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "20 10 10 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Inclusi'
		                          , inputValue: 'A'
		                          , checked: true
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Esclusi'
		                          , inputValue: 'Y'
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_hold' 
		                          , boxLabel: 'Solo'
		                          , inputValue: 'N'
		                          
		                        }]
						}, 
					{
							name: 'f_evasi',
							xtype: 'radiogroup',
							fieldLabel: 'Ordini evasi',
							labelAlign: 'left',
						   	allowBlank: true,
						   	anchor: '-15',
						    margin: "20 10 0 10",
						   	items: [{
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Tutti'
		                          , inputValue: 'T'
		                          , width: 50
		                         <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] != 'Y') { ?>			                
			                      , checked: true
							<?php } ?>
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Da evadere'
		                          , inputValue: 'N'
								  <?php if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y') { ?>			                
			                      , checked: true
							      <?php } ?>
		                          
		                        }, {
		                            xtype: 'radio'
		                          , name: 'f_evasi' 
		                          , boxLabel: 'Evasi'
								  , checked: false	
		                          , inputValue: 'Y'
		                          
		                        }]
						}
	             
	             ]
	             },                                     
            ]
        }

	
]}

<?php	
 exit;	
}

if ($_REQUEST['fn'] == 'get_data_tree'){

	$m_params = acs_m_params_json_decode();
	$form_values=$m_params->open_request->form_values;
	
	 $sql_where=parametri_sql_where($form_values);
	 
	 $data_limite=$form_values->f_data;
	 
	 $ar = array();
				
				
				if($_REQUEST['node'] == 'SCADUTI'){
					
						
					$sql= "SELECT TDGGRI, TDSTAT, TDDSST, SUM(TDTOCO) AS COLLI, SUM (TDCOPR) DISP,
					SUM(RD_PROD.RDQTA) AS COLLI_TOT_PROD, SUM(RD_PROD.RDQTA2) AS COLLI_DISP_PROD,
				    SUM(RD_RIV.RDQTA) AS COLLI_TOT_RIV, SUM(RD_RIV.RDQTA2) AS COLLI_DISP_RIV,
				    SUM(RD_PROD.DV) AS DA_VERN, SUM(RD_PROD.IV) AS IN_VERN
					FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2,
			            SUM(CASE WHEN RDFORN='DV' THEN RDQTA ELSE 0 END) AS DV,
				        SUM(CASE WHEN RDFORN='IV' THEN RDQTA ELSE 0 END) AS IV
			            FROM {$cfg_mod_Spedizioni['file_righe']} RD_PROD
			            WHERE RD_PROD.RDTPNO='COLLI' AND RD_PROD.RDRIGA=0 AND RD_PROD.RDRIFE = 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_PROD
			            ON TDDT=RD_PROD.RDDT AND TDOTID=RD_PROD.RDOTID AND TDOINU=RD_PROD.RDOINU AND TDOADO=RD_PROD.RDOADO AND TDONDO=RD_PROD.RDONDO
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
			            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
			            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE <> 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_RIV
					ON TDDT=RD_RIV.RDDT AND TDOTID=RD_RIV.RDOTID AND TDOINU=RD_RIV.RDOINU AND TDOADO=RD_RIV.RDOADO AND TDONDO=RD_RIV.RDONDO					   
					   
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
						ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
					$sql_where AND TDDTEP < $data_limite GROUP BY TDGGRI, TDSTAT, TDDSST ORDER BY TDGGRI";
						
					
				
					$stmt= db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt);
					
					while ($r = db2_fetch_assoc($stmt)) {
				
						$tmp_ar_id = array('SCADUTI');
						$ar_new = array();
						$ar_r= &$ar;
				
						if($r['TDGGRI'] >=0 && $r['TDGGRI']<=5){
							$cod_liv1= "D_0_A_5";
							$ar_new['task'] = 'Da meno di 5 gg';
						}
						if($r['TDGGRI'] >5 && $r['TDGGRI']<=10){
							$cod_liv1= "D_5_A_10";
							$ar_new['task'] = 'Da meno di 10 gg';
						}
						if($r['TDGGRI'] >10 && $r['TDGGRI']<=15){
							$cod_liv1= "D_10_A_15";
							$ar_new['task'] = 'Da meno di 15 gg';
						}
						if($r['TDGGRI'] >15 && $r['TDGGRI']<=20){
							$cod_liv1= "D_15_A_20";
							$ar_new['task'] = 'Da meno di 20 gg';
						}
						if($r['TDGGRI'] >20 && $r['TDGGRI']<=25){
							$cod_liv1= "D_20_A_25";
							$ar_new['task'] = 'Da meno di 25 gg';
						}
						if($r['TDGGRI'] >22 && $r['TDGGRI']<=30){
							$cod_liv1= "D_25_A_30";
							$ar_new['task'] = 'Da meno di 30 gg';
						}
					    if($r['TDGGRI'] >30){
							$cod_liv1= "D_30_A_9999";
							$ar_new['task'] = 'Da oltre 30 gg';
					    }
					    
					    
					    $cod_liv2=$r['TDSTAT'];
					    
					
				
							//$ar_r=&$ar_r['children'];
							$liv =$cod_liv1;
							$tmp_ar_id[] = $liv;
							if (!isset($ar_r["{$liv}"])){
							
							$ar_new['id'] = implode("|", $tmp_ar_id);
					        $ar_new['liv'] = 'liv_3';
					        $ar_new['gg_ritardo'] =  $r['TDGGRI'];
					      
							$ar_r["{$liv}"] = $ar_new;
								}
							$ar_r = &$ar_r["{$liv}"];
							sum_columns_value($ar_r, $r);
							
							
							$ar_r=&$ar_r['children'];
							$liv =$cod_liv2;
							$tmp_ar_id[] = $liv;
							if (!isset($ar_r["{$liv}"])){
								$ar_new = array();
								$ar_new['id'] = implode("|", $tmp_ar_id);
								$ar_new['task'] = "[".$r['TDSTAT']."] ".$r['TDDSST'];
								$ar_new['stato'] = $r['TDSTAT'];
								$ar_new['gg_ritardo'] =  $r['TDGGRI'];
								$ar_new['liv'] = 'liv_4';
								$ar_new['leaf']=true;
								$ar_r["{$liv}"] = $ar_new;
							}
							$ar_r = &$ar_r["{$liv}"];
							sum_columns_value($ar_r, $r);
														
				
				
					}
					
					$ret = array();
					foreach($ar as $kar => $v){
						$ret[] = array_values_recursive($ar[$kar]);
					}
					
					echo acs_je(array('success' => true, 'children' => $ret));
					
					exit;
						
				}
				
				
				
				
				
				
		if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){

		$sql_group_by.=" GROUP BY CS.CSAARG, CS.CSNRSE ORDER BY  CS.CSAARG, CS.CSNRSE";
		$data= "";
		$stato= "";
	
		}
		else{
			$anno_set= explode("_", $_REQUEST["node"]);
			$anno= $anno_set[0];
			$set= explode("|", $anno_set[1]);
			$sql_where.=" AND CS.CSAARG=$anno AND CS.CSNRSE=$set[0]";
			$sql_group_by.=" GROUP BY CS.CSAARG, CS.CSNRSE, TDDTEP, TDSTAT, TDDSST, TDONDO ORDER BY TDDTEP";
			$data= ", TDDTEP";
			$stato= ", TDSTAT, TDDSST";
			
			
		}
		
		
		//prima riga scaduti totali
				if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
					
					$ar['TOTALE']=array();
					$ar['TOTALE']['id'] = "TOTALE";
					$ar['TOTALE']['task'] = "TOTALE";
					$ar['TOTALE']['liv'] = 'liv_1';
					$ar['TOTALE']['expanded']= true;
					
		
				$sql_1="SELECT SUM(TDTOCO) AS COLLI, SUM (TDCOPR) DISP,
				SUM(RD_PROD.RDQTA) AS COLLI_TOT_PROD, SUM(RD_PROD.RDQTA2) AS COLLI_DISP_PROD,
				SUM(RD_RIV.RDQTA) AS COLLI_TOT_RIV, SUM(RD_RIV.RDQTA2) AS COLLI_DISP_RIV,
				SUM(RD_PROD.DV) AS DA_VERN, SUM(RD_PROD.IV) AS IN_VERN
				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2,
			            SUM(CASE WHEN RDFORN='DV' THEN RDQTA ELSE 0 END) AS DV,
				        SUM(CASE WHEN RDFORN='IV' THEN RDQTA ELSE 0 END) AS IV
			            FROM {$cfg_mod_Spedizioni['file_righe']} RD_PROD
			            WHERE RD_PROD.RDTPNO='COLLI' AND RD_PROD.RDRIGA=0 AND RD_PROD.RDRIFE = 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_PROD
			            ON TDDT=RD_PROD.RDDT AND TDOTID=RD_PROD.RDOTID AND TDOINU=RD_PROD.RDOINU AND TDOADO=RD_PROD.RDOADO AND TDONDO=RD_PROD.RDONDO
				LEFT OUTER JOIN
					(
		            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
		            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
		            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE <> 'PRODUZIONE'
		            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
		            ) RD_RIV
					ON TDDT=RD_RIV.RDDT AND TDOTID=RD_RIV.RDOTID AND TDOINU=RD_RIV.RDOINU AND TDOADO=RD_RIV.RDOADO AND TDONDO=RD_RIV.RDONDO				
					
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
					ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
		        $sql_where AND TDDTEP < $data_limite";
				
		     //   print_r($sql_1);
		    
				
		        $stmt_1 = db2_prepare($conn, $sql_1);
		        echo db2_stmt_errormsg();
		        $result = db2_execute($stmt_1);
		        $r_1= db2_fetch_assoc($stmt_1);
			
					$ar_new = array();
					$ar_new['id'] = 'SCADUTI';
					$ar_new['colli_tot'] += $r_1['COLLI'];
					$ar_new['colli_disp'] +=$r_1['DISP'];
					$ar_new['colli_non_disp'] += ($r_1['COLLI'] - $r_1['DISP']);
					$ar_new['colli_tot_prod'] += $r_1['COLLI_TOT_PROD'];
					$ar_new['colli_disp_prod'] += $r_1['COLLI_DISP_PROD'];
					$ar_new['colli_non_disp_prod'] += ($r_1['COLLI_TOT_PROD'] - $r_1['COLLI_DISP_PROD']);
					$ar_new['colli_tot_riv'] += $r_1['COLLI_TOT_RIV'];
					$ar_new['colli_disp_riv'] += $r_1['COLLI_DISP_RIV'];
					$ar_new['colli_non_disp_riv'] += ($r_1['COLLI_TOT_RIV'] - $r_1['COLLI_DISP_RIV']);
					if ($ar_new['prod']==''){
						$ar_new['prod']=0;
						}
					$ar_new['da_vern'] += $r_1['DA_VERN'];
					$ar_new['in_vern'] +=$r_1['IN_VERN'];
					$ar_new['task'] = 'Scaduti';
					$ar_new['liv'] = 'liv_2';
					$ar['TOTALE']['children']["SCADUTI"] = $ar_new;
			
					sum_columns_value($ar['TOTALE'], $r_1);
			
					}
		
		
			
				$sql= "SELECT CS.CSAARG, CS.CSNRSE $data $stato, SUM(TDTOCO) AS COLLI, SUM (TDCOPR) DISP, 
				SUM(RD_PROD.RDQTA) AS COLLI_TOT_PROD, SUM(RD_PROD.RDQTA2) AS COLLI_DISP_PROD,
				SUM(RD_RIV.RDQTA) AS COLLI_TOT_RIV, SUM(RD_RIV.RDQTA2) AS COLLI_DISP_RIV,
				SUM(RD_PROD.DV) AS DA_VERN, SUM(RD_PROD.IV) AS IN_VERN
				FROM {$cfg_mod_Spedizioni['file_testate']} TD " . $main_module->add_riservatezza() . "
					LEFT OUTER JOIN
						(
			            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2,
			            SUM(CASE WHEN RDFORN='DV' THEN RDQTA ELSE 0 END) AS DV,
				        SUM(CASE WHEN RDFORN='IV' THEN RDQTA ELSE 0 END) AS IV
			            FROM {$cfg_mod_Spedizioni['file_righe']} RD_PROD
			            WHERE RD_PROD.RDTPNO='COLLI' AND RD_PROD.RDRIGA=0 AND RD_PROD.RDRIFE = 'PRODUZIONE'
			            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
			            ) RD_PROD					   
			            ON TDDT=RD_PROD.RDDT AND TDOTID=RD_PROD.RDOTID AND TDOINU=RD_PROD.RDOINU AND TDOADO=RD_PROD.RDOADO AND TDONDO=RD_PROD.RDONDO
				LEFT OUTER JOIN
					(
		            SELECT RDDT, RDOTID, RDOINU, RDOADO, RDONDO, SUM(RDQTA) AS RDQTA, SUM(RDQTA2) AS RDQTA2
		            FROM {$cfg_mod_Spedizioni['file_righe']} RDO
		            WHERE RDO.RDTPNO='COLLI' AND RDO.RDRIGA=0 AND RDO.RDRIFE <> 'PRODUZIONE'
		            GROUP BY RDDT, RDOTID, RDOINU, RDOADO, RDONDO
		            ) RD_RIV
					ON TDDT=RD_RIV.RDDT AND TDOTID=RD_RIV.RDOTID AND TDOINU=RD_RIV.RDOINU AND TDOADO=RD_RIV.RDOADO AND TDONDO=RD_RIV.RDONDO
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CS
					ON TD.TDDT=CS.CSDT AND CS.CSCALE='*CS' AND CS.CSDTRG= TD.TDDTEP
		        $sql_where AND TDDTEP >= $data_limite $sql_group_by";
					
				
				$stmt = db2_prepare($conn, $sql);
				echo db2_stmt_errormsg();
				$result = db2_execute($stmt);
				
			//	print($sql);

				while ($r = db2_fetch_assoc($stmt)) {
					
					$tmp_ar_id = array();

					$ar_r= &$ar;
					
	
					//stacco dei livelli
					$anno_set= array();
					$anno_set[0]=trim($r['CSAARG']);
					$anno_set[1]=trim($r['CSNRSE']);
					$cod_totale ="TOTALE"; //totale generale
					$cod_liv0 = implode("_", $anno_set); //NUMERO ANNO_SETTIMANA					
					$cod_liv1 =trim($r['TDDTEP']); //data
					$cod_liv2 =trim($r['TDSTAT']); //stato ordine
					
					
					$liv =$cod_totale;
					//$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();					
					}
					
					//facciamo sempre perche' TOTALE viene creato da SCADUTI
					
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);

					$ar_r=&$ar_r['children'];
					$liv =$cod_liv0;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
							if ($ar_new['prod']==''){
							$ar_new['prod']=0;
							}
						$anno=trim($r['CSAARG']);
						$sett=trim($r['CSNRSE']);
						$ar_new['task'] = $anno." Settimana <b>".$sett."</b>";
						
						$ar_new['liv'] = 'liv_2';
						$ar_r["{$liv}"] = $ar_new;
						
					}
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);
	    			
					if ($_REQUEST['node'] == '' || $_REQUEST['node'] == 'root'){
						continue;
					}
					
					
					$ar_r=&$ar_r['children'];
					$liv =$cod_liv1;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
					    setlocale(LC_TIME, it_IT);
					    $ar_new['task'] = ucfirst(strftime("%a %d/%m", strtotime($liv)));
					    $ar_new['data'] = $r['TDDTEP'];
					    $ar_new['liv'] = 'liv_3';
					    $ar_r["{$liv}"] = $ar_new;
					}
					
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);
		
					
					$ar_r=&$ar_r['children'];
					$liv =$cod_liv2;
					$tmp_ar_id[] = $liv;
					if (!isset($ar_r["{$liv}"])){
						$ar_new = array();
						$ar_new['id'] = implode("|", $tmp_ar_id);
						$ar_new['task'] = "[".$r['TDSTAT']."] ".$r['TDDSST'];
						$ar_new['data'] = $r['TDDTEP'];
						$ar_new['stato'] = $r['TDSTAT'];
						$ar_new['liv'] = 'liv_4';
						$ar_new['leaf']=true;
						$ar_r["{$liv}"] = $ar_new;
					}
					$ar_r = &$ar_r["{$liv}"];
					sum_columns_value($ar_r, $r);
					

				} //while
				
				
				if ($_REQUEST['node'] != '' && $_REQUEST['node'] != 'root'){
					$ar= $ar['TOTALE']['children'][$_REQUEST['node']]['children'];
				}
				
				
				$ret = array();
				foreach($ar as $kar => $v){
					$ret[] = array_values_recursive($ar[$kar]);
				}

				echo acs_je(array('success' => true, 'children' => $ret));
				
				exit;
				
}
				
				?>
				
				
				


{"success":true, "items": [

        {
            xtype: 'treepanel' ,
	        title: 'Items' ,
	        tbar: new Ext.Toolbar({
	            items:['<b>Analisi disponibilit&agrave; colli</b>', '->'
	            	
		            , {iconCls: 'tbar-x-tool x-tool-expand', tooltip: 'Espandi tutto', handler: function(event, toolEl, panel){ this.up('panel').expandAll();}}
		            , {iconCls: 'tbar-x-tool x-tool-collapse', tooltip: 'Comprimi tutto', handler: function(event, toolEl, panel){ this.up('panel').collapseAll();}}	            
		           	, {iconCls: 'tbar-x-tool x-tool-refresh', handler: function(event, toolEl, panel){ this.up('panel').getStore().load();}}
		       		<?php echo make_tbar_closable() ?>
	         ]            
	        }),	    
	        <?php echo make_tab_closable(); ?>,
	        flex: 1,
	        useArrows: true,
	        rootVisible: false,
	        loadMask: true,            
			//multiSelect:true,
			singleExpand: false,
	        store: Ext.create('Ext.data.TreeStore', {
                    autoLoad: true,        
                                
				    fields: ['task', 'colli_tot', 'colli_disp', 'colli_non_disp', 'prod', 'liv', 
				    'stato', 'data', 'colli_tot_prod', 'colli_disp_prod', 'colli_non_disp_prod',
				    'colli_tot_riv', 'colli_disp_riv', 'colli_non_disp_riv', 'gg_ritardo', 'da_vern', 'in_vern'],
				                                          
                    proxy: {
                        type: 'ajax',
                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_data_tree',
						actionMethods: {read: 'POST'},
						
						extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>
			        				},
                        
                       	reader: {
                            root: 'children'
                        },
                       	                      
                    	doRequest: personalizza_extraParams_to_jsonData        				
                    }

                }),
				

		columns: [
					{
			            xtype: 'treecolumn', //this is so we know which column will show the tree
			            columnId: 'descr', 
			            flex: 1,
			            dataIndex: 'task',
			            menuDisabled: true, sortable: false,
			            header: 'Settimana',
			               
			        },
			        
			        {header: 'Colli',
                    columns: [
                      {header: 'Totali', dataIndex: 'colli_tot', align: 'right',  width: 100, tdCls: ' grassetto'}
                     , {header: 'Disponibili', dataIndex: 'colli_disp', align: 'right',  width: 100, tdCls: 'disponibile'}
                     , {header: 'Non disponibili', dataIndex: 'colli_non_disp', align: 'right',  width: 100, tdCls: 'non_disponibile'}
                  ]
                 } ,
			        {header: 'Rivendita',
                    columns: [
                      {header: 'Totali', dataIndex: 'colli_tot_riv', align: 'right',  width: 100, tdCls: ' grassetto'}
                     , {header: 'Disponibili', dataIndex: 'colli_disp_riv', align: 'right',  width: 100, tdCls: 'disponibile'}
                     , {header: 'Non disponibili', dataIndex: 'colli_non_disp_riv', align: 'right',  width: 100, tdCls: 'non_disponibile'}
                  ]
                 },{header: 'Produzione',
                    columns: [
                      {header: 'Totali', dataIndex: 'colli_tot_prod', align: 'right',  width: 100, tdCls: ' grassetto'}
                     , {header: 'Disponibili', dataIndex: 'colli_disp_prod', align: 'right',  width: 100, tdCls: 'disponibile'}
                     , {header: 'Non disponibili', dataIndex: 'colli_non_disp_prod', align: 'right',  width: 100, tdCls: 'non_disponibile'}
                  ]
                 },{header: 'Di cui in produzione',
                    columns: [
                      {header: 'Da verniciare', dataIndex: 'da_vern', align: 'right',  width: 100, tdCls: 'vern'}
                     , {header: 'In verniciautra', dataIndex: 'in_vern', align: 'right',  width: 100, tdCls: 'vern'}
                   
                  ]
                 }
	         ],	//columns	
	         
	         listeners: {
	         
	       		  celldblclick: {								
					  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
					  
					  	col_name = iView.getGridColumns()[iColIdx].dataIndex;
					  	rec = iView.getRecord(iRowEl);
					  	iEvent.stopEvent();

						if (rec.get('liv')=='liv_4' || rec.get('liv')=='liv_3'){
						
							console.log(rec);
							
							iEvent.preventDefault();
							acs_show_win_std('Elenco ordini', 'acs_disp_colli_elenco_ordini.php', {
								form_values: form.getValues(),
								stato_selected: rec.get('stato'),
								data_selected:rec.get('data'),
								id_scaduti_rit:rec.get('id')
							}, 1024, 600, {}, 'icon-leaf-16');
							 
                          
							
						}
													
						return false;

					 }
					}
					
			},
	        
 viewConfig: {
		        getRowClass: function(record, index) {			        	
		           v = record.get('liv');
		           
		           if (!Ext.isEmpty(record.get('row_cls')))
		            v = v + ' ' + record.get('row_cls');
		           
		           if (record.get('rec_stato') == 'Y') //rilasciato
		           	v = v + ' barrato';
 		           return v;																
		         }   
		    }
	        																			  			
	            
        }  

]
}


