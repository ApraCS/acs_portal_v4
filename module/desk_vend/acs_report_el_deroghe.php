<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_parametri_form'){
?>	


	{"success" : true, "items":
	
		{
	        xtype: 'form',
	        layout: 'form',
	        collapsible: false,
	        id: 'my_form',
	        url: '<?php echo $_SERVER['PHP_SELF']; ?>',
	        frame: false,
	        bodyPadding: '15 15 15 15',
	        width: 350,
	        fieldDefaults: {
	            msgTarget: 'side',
	            labelWidth: 75,
	    		format: 'd/m/Y',
	    		submitFormat: 'Ymd'	            
	        },
	        defaultType: 'datefield',
	        items: [{
	            fieldLabel: 'Data inserimenti iniziale',
	            labelWidth: 170,
	            name: 'periodo_dal',
	            allowBlank: false,
	            tooltip: 'Periodo iniziale'
	        },{
	            fieldLabel: 'Data inserimento finale',
	            labelWidth: 170,	            
	            name: 'periodo_al',
	            allowBlank: false,
	            tooltip: 'Periodo iniziale'
	        }],

	        buttons: [{
	            text: 'Visualizza',
	            iconCls: 'icon-module-32',
	            scale: 'large',	                     
	            handler: function() {
	                form = this.up('form').getForm();
	                
	                if (form.isValid()){	                	                
		                form.submit({
				                standardSubmit: true,
		                        method: 'POST',
								target : '_blank'		                        
		                });
	                }	                
	            }
	        }]
	    }
	
		
	}

<?php	
 exit;	
}


?>


<html>
 <head>

  <style>
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #888888; font-weight: bold;}
   tr.ag_liv2 td{background-color: #cccccc;}   
   tr.ag_liv1 td{background-color: #ffffff;}   
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white;}   
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
      
  </style>



 </head>
 <body>
 	
<div class="page-utility noPrint">
	<A HREF="javascript:window.print()"><img src=<?php echo img_path("icone/48x48/print.png") ?>></A>
	<span style="float: right;">
		<A HREF="javascript:window.close();"><img src=<?php echo img_path("icone/48x48/sub_blue_delete.png") ?>></A>		
	</span>	
</div> 	

<?php





//costruzione sql

	$sql = "SELECT *
			FROM {$cfg_mod_Spedizioni['file_deroghe']} AD
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
			  ON AD.ADRGES = TD.TDDOCU
			WHERE ADTP = ''
			AND ADDTRI >= {$_REQUEST['periodo_dal']} AND ADDTRI <= {$_REQUEST['periodo_al']}			 
			ORDER BY ADDTRI DESC
			";

$stmt = db2_prepare($conn, $sql);		
$result = db2_execute($stmt);

$ar = array();
$ar_tot = array();



/***********************************************************************************************
 *  ELENCO 
 ***********************************************************************************************/
 
if (1 == 1) {

 echo "<h2>Riepilogo autorizzazioni deroghe</h2>";
 
 $d_ar = array(); 
 $ar_tot["TOTALI"] = array();
 
 while ($r = db2_fetch_assoc($stmt)) {

	$r['ADAUTOR_D'] = $s->decod_std('AUDE', $r['ADAUTOR']);
	$d_ar[] = $r;	
	 		
 } //while



 
echo "<table>";

echo "<th>Data</th>";
echo "<th>Operatore</th>";
echo "<th>Rif.Gestionale</th>";
echo "<th>Cliente</th>";
echo "<th>Riferimento</th>";
echo "<th>Autorizzazione</th>";
echo "<th>Data</th>";
echo "<th>Commento</th>";
echo "<th>Note</th>";

foreach ($d_ar as $kr => $r){
		echo "<tr>";
			echo "<td>" . print_date($r['ADDTRI']) . "</td>";
			echo "<td>" . trim($r['ADUSRI']) .  "</td>";			
			echo "<td nowrap>" . trim($r['ADTIDO']) . " - " . trim($r['ADRGES']) .  "</td>";

			echo "<td>" . trim($r['TDDCON']) . "</td>";			
			echo "<td>" . trim($r['TDVSRF']) . "</td>";			

			echo "<td>" . trim($r['ADAUTOR_D']) .  "</td>";	
			echo "<td>" . print_date($r['ADDTDE']) . "</td>";
			echo "<td>" . trim($r['ADCOMM']) .  "</td>";
			echo "<td>" . trim($r['ADNOTE']) .  "</td>";											
		echo "</tr>";

} 

echo "</table>"; 

 
 
} //if elenco






 
 
 		
?>
 </body>
</html>		