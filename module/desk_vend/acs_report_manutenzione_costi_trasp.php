<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();
$smc = new SpedManutenzioneCosti();

$stmt = $smc->get_stmt_rows();

//parametri del modulo (non legati al profilo)
$mod_js_parameters = $main_module->get_mod_parameters();

//per invio e-mail
$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");


$ar_email_json = acs_je($ar_email_to);


//Se l'ambiente non gestisce i pallet allora uso il volume per ripartire i costi 
if ($mod_js_parameters->v_pallet == 1){
	$field_dettaglio	= 'T_TDBANC';
	$field_totale		= 'S_PALLET';
	$etic_dettaglio 	= '(Pallet)';
} else {	
	$field_dettaglio	= 'T_TDVOLU';
	$field_totale		= 'S_VOLUME';		
	$etic_dettaglio = '(Volume)';
}


?>


<html>
<head>

<style>
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table{border-collapse:collapse; width: 100%;}
div.acs_report table td, table th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px; font-size: 14px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}

div.acs_report h1{font-size: 22px; margin-top: 10px;}
div.acs_report h2{font-size: 16px;}
 
@media print
{
	.noPrint{display:none;}
}




</style>



  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>



</head>
<body>

<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 

<div id='my_content' class=acs_report>

<h1>Riepilogo spedizioni programmate per trasportatore</h1>

<?php

$ar = array();
while ($row = db2_fetch_assoc($stmt)){
 $row = $smc->after_get_row($row, 'Y');
 
 $row['imp_riprog'] 	= $smc->get_importo_riprogrammato_su_sped($row['CSPROG']);
 $row['imp_prog']		= $row['S_IMPORTO'] + $row['imp_riprog'];
 $row['NR_ORDINI_COMPLETI'] = $s->get_nr_ordini_completi_by_sped($row['CSPROG']);

 $row['EL_CARICHI_D'] = $s->get_el_carichi_by_sped($row['CSPROG'], 'TN'); 
 $row['EL_DDT'] 		= $s->get_el_ddt_by_sped($row['CSPROG']);
 $row['HA_DDT']			= ha_ddt($row['EL_DDT']);
 
 if ($row['HA_DDT'] == true || $row['CSDTSP'] >= oggi_AS_date() ){
	$row['CSCSTR_write'] = n($row['CSCSTR'], 2);
 } else {
	//non ho ddt... non conteggio l'importo in nessun campo
	$row['CSCSTR_write'] = "[ " . n($row['CSCSTR'], 2) . " ]";
	$row['CSCSTR']	= 0;
 }

 $row['EL_DIVISIONI'] 	= $s->get_el_divisioni_by_sped($row['CSPROG']);
 
 //genero un array per sapere tutte le divisioni presenti
 foreach($row['EL_DIVISIONI'] as $div){
	$cod_div = cod_div($div['TDCDIV']);
	$ar_divisioni[$cod_div] = $div['TDDDIV'];
 }
 
 $liv0 = implode("_", array($row['CSDTSP'], trim($row['TRASP_D'])));
 
 $d_ar = &$ar; 
 $c_liv = $liv0;
 if (!isset($ar[$c_liv]))
 	$ar[$c_liv] = array("cod" => $c_liv, "data" => $row['CSDTSP'], "trasp" => $row['TRASP_D'], "descr"=>$c_liv, "children"=>array());

 $d_ar = &$d_ar[$c_liv]['children']; 
 
 $d_ar[] = $row;
 
}

//non stampo colonne divisioni se ne ho una sola
if (count($ar_divisioni) == 1)
	$ar_divisioni = array();

// STAMPO REPORT *******************************
foreach ($ar as $k0 => $l0){

 $l0_tot = array();
 $t_colspan = 11;
 $t_colspan += count($ar_divisioni);
 
 echo "
		<table class=tabh2>
		 <tr><td colspan={$t_colspan}  class=\"acs_report\">
			<h2>Spedizioni del " . print_date($l0['data']) . " - " . $l0['trasp'] . "</h2>
		 </td></tr>
	    </table>";
 
 echo "<table class=acs_report>";
 
 ?>
		<tr class="t-l1">
		 <td>Trasportatore</td>
 		 <td>Vettore</td>
 		 <td>Itinerario</td>		 
 		 <td width=90>Elenco carichi</td>		 
 		 <td>Elenco ddt</td>		 		 
 		 <!--  <td>Nr scarichi</td>  -->		 
 		 <td>Km</td> 		 
 		 <td>Colli</td>
    	<?php if ($mod_js_parameters->v_pallet == 1){ ?>			    	
	    	<td>Pallet</td>
	    <?php } ?> 		 		 
 		 <td>Volume</td>		 
 		 <td>Costo trasporto</td>
 		 
 		 <?php
 		 	//Intestazione divisioni
 		  	foreach($ar_divisioni as $kdiv=>$div){
				echo "<td>" . des_div($div) . "</td>";
				echo "<td>{$etic_dettaglio}</td>";
			}			
 		 ?>
 		 
 		 		 
 		</tr>
 		
 <?php 		
 
 	 usort($l0['children'], "cmp_in_fascetta");  
	 foreach ($l0['children'] as $k1 => $l1){		
		?>
		
		<tr>
		 <td><?php echo $l1['TRASP_D']  . " (#{$l1['CSPROG']})"; ?></td>
		 <td><?php echo $l1['VMC_D']; ?>
		 
		 	<?php
		 	 	if (strlen(trim($l1['CSDESC'])) > 0)
		 	 		echo "<br/>" . $l1['CSDESC']; 
		 	?>
		 
		 </td>
		 <td><?php echo $l1['ITIN_D']; ?></td>		 
		 <td><?php echo $l1['EL_CARICHI_D']; ?></td>		 
		 <td class=number><?php echo out_el_ddt($l1['EL_DDT']); ?></td>		 		 
		 <!-- <td class=number><?php echo $l1['C_CLIENTI_DEST']; ?></td>  -->
		 <td class=number><?php echo $l1['CSKMTR']; ?></td>
		 <td class=number><?php echo $l1['S_COLLI']; ?></td>
    	<?php if ($mod_js_parameters->v_pallet == 1){ ?>			    	
	    	 <td class=number><?php echo n($l1[$field_totale], 1); ?></td>
	    <?php } ?>		 		 
		 <td class=number><?php echo n($l1['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo $l1['CSCSTR_write']; ?></td>		 

 		 <?php
 		 	//suddivido costo trasporto in base a pallet
 		  	foreach($ar_divisioni as $kdiv=>$div){ 
				$stampato = false;
				//scorro per cercare il valore della divisione in questa riga
				foreach($l1['EL_DIVISIONI'] as $row_div){
					if (cod_div($row_div['TDCDIV']) == $kdiv){

						$l0_tot['per_div_pallet'][$kdiv] 	+= (float)$row_div[$field_dettaglio];
						$gen_tot['per_div_pallet'][$kdiv] 	+= (float)$row_div[$field_dettaglio];

						echo "<td class=number>";  	
						if ($l1[$field_totale] == 0) echo "&nbsp;";
						else { 							
							$val = ( (float)$row_div[$field_dettaglio] * (float)$l1['CSCSTR'] ) / ( $l1[$field_totale] );
							$l0_tot['per_div'][$kdiv] 	+= $val;
							$gen_tot['per_div'][$kdiv]	+= $val;														
							
							echo n($val, 2);
						}
						echo "</td>";
						
						//num. pallet per divisione
						echo "<td class=number>" . n($row_div[$field_dettaglio], 1) . "</td>";
						$stampato = true;						
					}
				}

				if (!$stampato){
					echo "<td class=number>&nbsp;</td><td class=number>0</td>";
				}
				
			}			
 		 ?>
		 
		 
		</tr>
		
		
		
		<?php
		
		//somme e conteggi
		$l0_tot = somme_conteggi($l0_tot, $l1);
		$gen_tot = somme_conteggi($gen_tot, $l1);		
		
	 } //per ogni riga
	 
	 //totale per area
		 if ($l0_tot['S_IMPORTO'] != 0)
		 	$perc_incidenza = ($l0_tot['CSCSTR']) / $l0_tot['S_IMPORTO']  * 100;
		 else $perc_incidenza = 0;
		 
		 if ($l0_tot['imp_prog'] != 0)
		 	$perc_incidenza_prog = ($l0_tot['CSCSTR']) / $l0_tot['imp_prog']  * 100;
		 else $perc_incidenza_prog = 0;
		 
	 ?>
	 
	 

		<tr class="f-l1">
		 <td colspan=4>Totale area spedizione <?php echo $s->decod_std('ASPE', $l0['area']); ?></td>		 
		 <td class=number></td>		 		 
		 <!--  <td class=number><?php echo $l0_tot['C_CLIENTI_DEST']; ?></td>  -->
		 <td class=number><?php echo $l0_tot['S_KM']; ?></td>		 		 
		 <td class=number><?php echo $l0_tot['S_COLLI']; ?></td>
   		 <?php if ($mod_js_parameters->v_pallet == 1){ ?>
			<td class=number><?php echo n($l0_tot['S_PALLET'], 1); ?></td>   		 
   		 <?php }?>		 		 
		 <td class=number><?php echo n($l0_tot['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo n($l0_tot['CSCSTR'], 2); ?></td>
		 
 		 <?php
 		  	foreach($ar_divisioni as $kdiv=>$div){
				echo "<td class=number>" . n($l0_tot['per_div'][$kdiv], 2) . "</td>";
				echo "<td class=number>" . n($l0_tot['per_div_pallet'][$kdiv], 1) . "</td>";
			}			
 		 ?>		 
		 		 
		</tr>
		

	 
	 
	 <?php
	 
	 
	 
 echo "</table>";

}


// TOTALE GENERALE *******************************************************************
	if ($gen_tot['S_IMPORTO'] != 0)
		$perc_incidenza = ($gen_tot['CSCSTR']) / $gen_tot['S_IMPORTO']  * 100;
	else $perc_incidenza = 0;
		
	if ($gen_tot['imp_prog'] != 0)
		$perc_incidenza_prog = ($gen_tot['CSCSTR']) / $gen_tot['imp_prog']  * 100;
	else $perc_incidenza_prog = 0;

?>


<?php if (count($ar) > 1){ ?>
<h2 class=acs_report>Totale generale</h2>
<table class=acs_report>
		<tr class="t-l1">
		 <td width="50%" colspan=4></td>		 
 		 <td>Nr ordini completi</td>		 		 
 		 <!--  <td>Nr scarichi</td>  -->
 		 <td>Km</td>		 
 		 <td>Colli</td>		 
 		 <td>Volume</td>		 
 		 <td>Costo trasporto</td>
 		 
 		 <?php
 		 	//Intestazione divisioni
 		  	foreach($ar_divisioni as $kdiv=>$div){
				echo "<td>" . des_div($div) . "</td>";
				echo "<td>{$etic_dettaglio}</td>";
			}			
 		 ?>
 		 
 		 		 
 		</tr>
		<tr class="f-l1">
		 <td colspan=4>Totale generale</td>		 
		 <td class=number><?php echo $gen_tot['NR_ORDINI_COMPLETI']; ?></td>		 		 
		 <!-- <td class=number><?php echo $gen_tot['C_CLIENTI_DEST']; ?></td>  -->
		 <td class=number><?php echo $gen_tot['S_KM']; ?></td>		 
		 <td class=number><?php echo $gen_tot['S_COLLI']; ?></td>		 
		 <td class=number><?php echo n($gen_tot['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo n($gen_tot['CSCSTR'], 2); ?></td>
		 
 		 <?php
 		 	//Intestazione divisioni
 		  	foreach($ar_divisioni as $kdiv=>$div){
				echo "<td class=number>" . n($gen_tot['per_div'][$kdiv], 2) . "</td>";
				echo "<td class=number>" . n($gen_tot['per_div_pallet'][$kdiv], 1) . "</td>";
			}			
 		 ?>		 
		 		 
		</tr>
 		
</table>
<?php } ?>




<?php


function cmp_in_fascetta($a, $b)
{
	
	if (strtoupper($a["VMC_D"]) != strtoupper($b["VMC_D"]))
		return strcmp(strtoupper($a["VMC_D"]), strtoupper($b["VMC_D"]));
	return strcmp(strtoupper($a["ITIN_D"]), strtoupper($b["ITIN_D"]));	
	
}


function somme_conteggi($ar, $r){
	$ar['NR_ORDINI_COMPLETI'] 	+= $r['NR_ORDINI_COMPLETI'];
	$ar['C_CLIENTI_DEST'] 		+= $r['C_CLIENTI_DEST'];
	$ar['S_KM'] 				+= $r['CSKMTR'];	
	$ar['S_COLLI'] 				+= $r['S_COLLI'];
	$ar['S_PALLET'] 			+= $r['S_PALLET'];	
	$ar['S_VOLUME'] 			+= $r['S_VOLUME'];
	$ar['S_IMPORTO'] 			+= $r['S_IMPORTO'];
	$ar['imp_prog'] 			+= $r['imp_prog'];
	$ar['imp_riprog'] 			+= $r['imp_riprog'];	
	$ar['CSCSTR'] 				+= $r['CSCSTR'];
	$ar['CSCSTD'] 				+= $r['CSCSTD'];		
  return $ar;	
} 



function ha_ddt($el_ddt){
	$ret = array();

	foreach ($el_ddt as $ddt){
		if ($ddt['TDNRDE'] != 0) return true;

	}
	return false;
}


function out_el_ddt($el_ddt){
	$ret = array();
	$dt_online = 0;
	foreach ($el_ddt as $ddt){

		if ($ddt['TDNRDE'] == 0) continue;

		if ($dt_online !=  $ddt['TDDTRE']){			
			$ret[] = "Data: " . print_date($ddt['TDDTRE']);
			$dt_online = $ddt['TDDTRE'];
		}

		$ret[] = "{$ddt['TDNRDE']}_{$ddt['TDTPDE']}";
	}
 return implode("<br>", $ret);	
}


function cod_div($cod){
	if (trim($cod) == '') return '_';
	else return trim($cod);
}

function des_div($des){
	if (trim($des) == '') return 'Senza divisione';
	else return trim($des);
}


?>


</div>
</body>
</html>


