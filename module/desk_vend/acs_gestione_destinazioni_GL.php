<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$m_params = acs_m_params_json_decode();
$list_selected_id_encoded = strtr(acs_je($m_params->list_selected_id), array("'" => "\'", '"' => '\"'));

if ($_REQUEST['fn'] == 'exe_aggiorna_dest'){
    global $cfg_mod_Spedizioni;
   

    $ar_ins['BFDTX0'] 	= oggi_AS_date();
    
    //update riga
    $sql = "UPDATE {$cfg_mod_Spedizioni['file_anag_cli_des']}
            SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
		    WHERE BFTCF0=1 AND BFCLF0 = '{$m_params->cliente}' AND BFTIN0 = '{$m_params->cod}'";
    
     
    $stmt = db2_prepare($conn, $sql);
    echo db2_stmt_errormsg();
    $result = db2_execute($stmt, $ar_ins);
    
    $ret = array();
    $ret['success'] = true;
    echo acs_je($ret);
    exit;

}

// ******************************************************************************************
// SAVE
// ******************************************************************************************
if ($_REQUEST['fn'] == 'exe_save'){
	global $cfg_mod_Spedizioni;

	//preparo campi per update in base ad agente selezionat
	$ar_ins['BFRAG0'] 	= $_REQUEST['denom1'];
	$ar_ins['BFIND0'] 	= $_REQUEST['ind1'];
	$ar_ins['BFCAP0'] 	= $_REQUEST['cap'];
	$ar_ins['BFLOC0'] 	= $_REQUEST['loc1'];
	$ar_ins['BFPRO0'] 	= $_REQUEST['pro'];
	$ar_ins['BFTEL0'] 	= $_REQUEST['tel'];
	$ar_ins['BFNAZ0'] 	= $_REQUEST['nazione'];
	
	//update riga
	$sql = "UPDATE {$cfg_mod_Spedizioni['file_anag_cli_des']}
		SET " . create_name_field_by_ar_UPDATE($ar_ins) . "
		WHERE BFTCF0=1 AND BFCLF0 = ? AND BFTIN0 = ?";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, array_merge($ar_ins, array($_REQUEST['cod_cli'], $_REQUEST['cod_des'])));
	
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
	exit;
}


// ******************************************************************************************
// CREATE NEW
// ******************************************************************************************
if ($_REQUEST['fn'] == 'create_new'){
	global $cfg_mod_Spedizioni;
	
	global $cfg_mod_Spedizioni;

	//preparo campi per update in base ad agente selezionat
	
	$ar_ins['BFUTX0'] 	= $auth->get_user();
	$ar_ins['BFDTX0'] 	= oggi_AS_date();
	//$ar_ins['BFDTX0'] 	= oggi_AS_time();
	$ar_ins['BFCLF0'] 	= $_REQUEST['cod_cli'];
	$ar_ins['BFTIN0'] 	= $_REQUEST['cod_des'];
	$ar_ins['BFTCF0'] 	= '1'; //tipo cliente
	$ar_ins['BFDES0'] 	= 'Supply Desk';
	
	$ar_ins['BFRAG0'] 	= $_REQUEST['denom1'];
	$ar_ins['BFIND0'] 	= $_REQUEST['ind1'];
	$ar_ins['BFCAP0'] 	= $_REQUEST['cap'];
	$ar_ins['BFLOC0'] 	= $_REQUEST['loc1'];
	$ar_ins['BFPRO0'] 	= $_REQUEST['pro'];
	$ar_ins['BFTEL0'] 	= $_REQUEST['tel'];
	$ar_ins['BFNAZ0'] 	= $_REQUEST['nazione'];
	if($_REQUEST['f_riservata'] == 'Y')
	   $ar_ins['BFLI10'] 	= 'RI';
	
	$sql = "INSERT INTO {$cfg_mod_Spedizioni['file_anag_cli_des']}(
			" . create_name_field_by_ar($ar_ins) . "
			)
			VALUES (" . create_parameters_point_by_ar($ar_ins) . ")
			";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt, $ar_ins);
		
	$ret = array();
	$ret['success'] = true;
	echo acs_je($ret);
 exit;
}


// ******************************************************************************************
// DATI PER GRID
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
	global $cfg_mod_Spedizioni;

	//recupero il codice cliente
	$cod_cli = $m_params->cod_cli;
	
	$ar_dest = $s->get_el_clienti_des_anag(array('cliente_cod' => $m_params->cod_cli, 'gest_des'=>$m_params->gest_des));
	
	echo acs_je(array('root' => $ar_dest, 'properties' => $properties));
	
	exit;
}


//recupero il codice cliente
$cod_cli = $m_params->cod_cli;

?>



{"success":true, "items": [

        {
            xtype: 'grid',
			
			store: {
					xtype: 'store',
					autoLoad:true,
			
	  					proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',								
								type: 'ajax',

								//Add these two properties
							      actionMethods: {
							          read: 'POST'
							      },
								
			                    extraParams: {
			        		    	cod_cli: '<?php echo $cod_cli; ?>',
			        		    	gest_des: '<?php echo $m_params->gest_des; ?>'
			        				},
								reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
						        
								/* PERSONALIZZAZIONE: extraParams lo passo come jsonData */
								, doRequest: personalizza_extraParams_to_jsonData						        
								        
							},
							
		        			fields: ['cod', 'cod_cli', 'dt', 'descr', 'denom', 'IND_D', 'LOC_D', 'CAP_D', 'PRO_D', 'TEL_D', 'NAZ_D', 'riser']							
							
			
			}, //store
			
			
			
	        columns: [	
				{
	                header   : 'Codice',
	                dataIndex: 'cod',
	                width: 50 
	            }, {
	                header   : 'Denominazione',
	                dataIndex: 'denom',
	                flex: 10
	            }, {
	                header   : 'Indirizzo',
	                dataIndex: 'IND_D',
	                flex: 10
	            }, {
	                header   : 'CAP',
	                dataIndex: 'CAP_D',
	                width: 80
	            }, {
	                header   : 'Localit&agrave;',
	                dataIndex: 'LOC_D',
	                flex: 5
	            }, {
	                header   : 'Prov',
	                dataIndex: 'PRO_D',
	                width: 50
	            }, {
	                header   : 'Naz.',
	                dataIndex: 'NAZ_D',
	                width: 50
	            }, {
	                header   : 'RI',
	                dataIndex: 'riser',
	                tooltip : 'Riservata',
	                width: 40,
	                renderer: function(value, p, record){
			               if (record.get('riser') == 'RI') return '<img src=<?php echo img_path("icone/16x16/sub_blue_accept.png") ?>  width=15>';
			               
			               }  
	            }, {
		            xtype:'actioncolumn', 
		            width:50,
		            items: [{
		                icon: <?php echo img_path("icone/16x16/keyboard.png") ?>,  // Use a URL in the icon config
		                tooltip: 'Modifica dati destinazione',
		                handler: function(grid, rowIndex, colIndex) {
		                		var rec = grid.getStore().getAt(rowIndex);
		                		var mgrid = grid;
                    			

								var win = new Ext.Window({
								  width: 800
								, height: 280
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Modifica definitiva dati destinazione sosta tecnica'
								, iconCls: 'icon-outbox-16'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
										xtype: 'form',
									            bodyStyle: 'padding: 10px',
									            bodyPadding: '5 5 0',
									            frame: false,
									            title: '',
									            url: 'acs_print_wrapper.php',
												buttons: [{
										            text: 'Conferma',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										                if (this.up('form').getForm().isValid()){										                
											                this.up('form').submit({
										                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_save',
										                        method: 'POST',
										                        
														success: function (formPanel, action) {
														  win.close(),
										                  mgrid.store.load();
											                },										                        
										                        
										                  });

										                }
										            }
										        }],             								
										items: [
										 	{
							                    xtype: 'hiddenfield',
							                    name: 'dt',
							                    value: rec.get('dt')    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'taid',
							                    value: 'VUDS'    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_cli',
							                    value: rec.get('cod_cli').trim()
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_des',
							                    value: rec.get('cod')    
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '-5',
								                    name: 'denom1',
								                    value: Ext.util.Format.substr(rec.get('denom'), 0, 30).trim(),    
								                    fieldLabel: 'Denominazione',
								                    allowBlank: false,
								                    flex: 1,
								                    maxLength: 60
							                	}]
											}, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '0',							                    
								                    name: 'ind1',    
								                    value: Ext.util.Format.substr(rec.get('IND_D'), 0, 30).trim(),
								                    fieldLabel: 'Indirizzo',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
								                }]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'cap',    
							                    value: rec.get('CAP_D').trim(),
							                    fieldLabel: 'Cap',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 10
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',							                    
								                    name: 'loc1',    
								                    value: Ext.util.Format.substr(rec.get('LOC_D'), 0, 30).trim(),
								                    fieldLabel: 'Loc',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
							                	}]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'pro',    
							                    value: rec.get('PRO_D').trim(),
							                    fieldLabel: 'Provincia',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 2
							                }, {
							                    xtype: 'textfield',							                    
							                    name: 'tel',    
							                    value: rec.get('TEL_D').trim(),
							                    fieldLabel: 'Telefono',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 20
							                }, {
												name: 'nazione',
												xtype: 'combo',
												fieldLabel: 'Nazione',
												displayField: 'text',
												valueField: 'id',
												emptyText: '- seleziona -',
												forceSelection: true,
											   	allowBlank: false,
											    anchor: '-15',
											    value: rec.get('NAZ_D').trim(),							
												store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}],
												    data: [								    
													     <?php echo acs_ar_to_select_json($s->find_TA_std('NAZIO'), ""); ?>	
													    ] 
												}						 
											  }
										]
									}	
								
								] 
								});
								win.show();  
		                		
                    			
                    			
		                }
		            }]
		        }
	            
	         ],																					

	         
	        listeners: {
					  celldblclick: {								
						  fn: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent){
						  	rec = iView.getRecord(iRowEl);
						  	
									Ext.Ajax.request({
									   url: 'acs_op_exe.php?fn=exe_assegna_destinazione_sosta_tecnica',
									   method: 'POST',
									   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
									   			  tipo: '<?php echo $m_params->tipo; ?>',
									   			  to_destinazione: iView.getSelectionModel().getSelection()[0].data.cod
									   			  }, 
									   
									   
									   
									   success: function(response, opts) {					   				
					                                this.print_w.close();
								  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								  				  	if (m_grid.getStore().$className == 'Ext.data.NodeStore')
								  				  		m_grid.getStore().treeStore.load();
								  				  	else	
									   					m_grid.getStore().load();
									   }
									});						  	
						  	
						  	
						  }
					  }	,
					  itemcontextmenu : function(grid, rec, node, index, event) {
				  		event.stopEvent();
				  													  
						  var voci_menu = [];
					      row = rec.data.k_ord_art;
					      
					  	 voci_menu.push({
			         		text: 'Aggiorna destinazione',
			        		iconCls : 'iconRIPRO',          		
			        		handler: function () {
			        		
			        		 Ext.Msg.confirm('Richiesta conferma', 'Confermi l\'operazione?', function(btn, text){																							    
								   if (btn == 'yes'){
        					    		Ext.Ajax.request({
        							   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=exe_aggiorna_dest',
        							   method: 'POST',
        							   jsonData: {
        							    cod : rec.get('cod'),
        							    cliente : rec.get('cod_cli')
        							   
        							   }, 
        							   
        							   success: function(response, opts) {
        							   	  var jsonData = Ext.decode(response.responseText);
        								  grid.getStore().load();					   	  
        							   }, 
        							   failure: function(response, opts) {
        							      alert('error on save_manual');
        							   }
        							});	
							
									}
									});
				                }
			    		});	 
			    	
				      var menu = new Ext.menu.Menu({
				            items: voci_menu
					}).showAt(event.xy);	
			    	
			    	}  
			},
			
			
 			dockedItems: [{
                dock: 'bottom',
                xtype: 'toolbar',
                scale: 'large',
                items: [{
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-button_black_play-32',
                     text: 'Crea nuovo',
			            handler: function() {

			            	/* CREA NUOVO */
			            
		                		var mgrid = this.up('grid');
                    			
								// create and show window
								var win = new Ext.Window({
								  width: 800
								, height: 330
								, minWidth: 300
								, minHeight: 300
								, plain: true
								, title: 'Crea nuova destinazione sosta tecnica'
								, iconCls: 'icon-outbox-16'			
								, layout: 'fit'
								, border: true
								, closable: true
								, items: [
									{
										xtype: 'form',
									            bodyStyle: 'padding: 10px',
									            bodyPadding: '5 5 0',
									            frame: false,
									            title: '',
									            url: 'acs_print_wrapper.php',
												buttons: [{
										            text: 'Conferma',
										            iconCls: 'icon-sub_blue_accept-32',
										            scale: 'large',
										            handler: function() {
										                if (this.up('form').getForm().isValid()){										                
											                this.up('form').submit({
										                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=create_new',
										                        method: 'POST',
										                        
														success: function (formPanel, action) {
														  win.close(),
										                  mgrid.store.load();
											                },										                        
										                        
										                  });

										                }
										            }
										        }],             								
										items: [
										 	{
							                    xtype: 'hiddenfield',
							                    name: 'dt',
							                    value: <?php echo acs_je($k_cod_cli_exp[0]); ?>   
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'taid',
							                    value: 'VUDS'    
							                }, {
							                    xtype: 'hiddenfield',
							                    name: 'cod_cli',
							                    value: <?php echo acs_je(trim($cod_cli)); ?>
							                }, {
								                    xtype: 'textfield',
								                    anchor: '-15',							                    
								                    name: 'cod_des',    
								                    fieldLabel: 'Codice',
								                    allowBlank: false,
								             		flex: 1,
								                    maxLength: 2
								                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '-5',
								                    name: 'denom1',    
								                    fieldLabel: 'Denominazione',
								                    allowBlank: false,
								                    flex: 1,
								                    maxLength: 100
							                	}]
											}, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',
								                    anchor: '0',							                    
								                    name: 'ind1',    
								                    fieldLabel: 'Indirizzo',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
								                }]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'cap',    
							                    fieldLabel: 'Cap',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 10
							                }, {
							                	xtype: 'fieldcontainer',
												layout: 'hbox',
												anchor: '-15',
												items: [{
								                    xtype: 'textfield',							                    
								                    name: 'loc1',
								                    fieldLabel: 'Loc',
								                    allowBlank: true,
								             		flex: 1,
								                    maxLength: 60
							                	}]
											}, {
							                    xtype: 'textfield',							                    
							                    name: 'pro',
							                    fieldLabel: 'Provincia',
							                    allowBlank: false,
							             		flex: 1,
							                    maxLength: 2
							                }, {
							                    xtype: 'textfield',							                    
							                    name: 'tel',
							                    fieldLabel: 'Telefono',
							                    allowBlank: true,
							             		flex: 1,
							                    maxLength: 20
							                }, {
												name: 'nazione',
												xtype: 'combo',
												fieldLabel: 'Nazione',
												displayField: 'text',
												valueField: 'id',
												emptyText: '- seleziona -',
												forceSelection: true,
											   	allowBlank: false,
											    anchor: '-15',							
												store: {
													autoLoad: true,
													editable: false,
													autoDestroy: true,	 
												    fields: [{name:'id'}, {name:'text'}],
												    data: [								    
													     <?php echo acs_ar_to_select_json($s->find_TA_std('NAZIO'), ""); ?>	
													    ] 
												}						 
											  } ,   {
                                                        fieldLabel: 'Riservata',
                                                        name: 'f_riservata',
                                                        flex: 1,
                                                        xtype: 'checkboxfield',
                                                        labelWidth: 100,
                                                        inputValue: 'Y',
                                                        checked : true            
                                                    }          
                            										]
									}	
								
								] 
								});
								win.show(); 			            
			            
			            
			            
			             }                       
                     }, {
                     xtype: 'button',
	            	 scale: 'large',
	            	 iconCls: 'icon-sub_red_delete-32',
                     text: 'Annulla destinazione sosta tecnica selezionata',
			            handler: function() {
							//RIPRISTINO DESTINAZIONE INIZIALE (CLIENTE)	            
			            
											Ext.Ajax.request({
											   url: 'acs_op_exe.php?fn=exe_assegna_destinazione_sosta_tecnica',
											   method: 'POST',
											   jsonData: {list_selected_id: '<?php echo $list_selected_id_encoded; ?>',
											   			  to_destinazione: 'RIPRISTINO'
											   			  }, 
											   
											   success: function(response, opts) {					   				
					                                this.print_w.close();
								  				  	m_grid = Ext.getCmp('<?php echo trim($m_params->grid_id); ?>');
								  				  	if (m_grid.getStore().$className == 'Ext.data.NodeStore')
								  				  		m_grid.getStore().treeStore.load();
								  				  	else	
									   					m_grid.getStore().load();
											   	
											   }
											});	            
			            
			             }                       
                     }]
        		}],				
			
				  
	            
        }    

]}