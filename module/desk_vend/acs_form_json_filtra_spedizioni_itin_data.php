<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$m_params = acs_m_params_json_decode();


// ******************************************************************************************
// DATI PER GRID 
// ******************************************************************************************
if ($_REQUEST['fn'] == 'get_json_data'){
    
    $campo_data = 'TDDTEP';
    
    if ($m_params->tipo_elenco == 'DA_FLIGHT')
        $campo_data = 'TDDTSP';
    
	//costruzione sql
	$sql = "SELECT TDNBOC, COUNT(DISTINCT TDDCON) AS C_CLIENTI, TDCCON, TDDCON, CSDESC, TDDLOC, TDNAZD, TDPROD, TDDNAD
					FROM {$cfg_mod_Spedizioni['file_testate']} TD
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
					WHERE " . $s->get_where_std() . "
					  AND TDCITI = ? AND {$campo_data} = ?
					  GROUP BY TDNBOC, TDCCON, TDDCON, CSDESC, TDDLOC, TDNAZD, TDPROD, TDDNAD
					  ORDER BY TDDCON
					";

					$stmt = db2_prepare($conn, $sql);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt, array($m_params->itin, $m_params->data));

					$ar = array();
					$ar_tot = array();

					while ($r = db2_fetch_assoc($stmt)) {
						$r['carico'] = $s->get_el_carichi_by_sped($r['TDNBOC'], 'N', 'N');
						$r['loca'] = $s->scrivi_rif_destinazione($r['TDDLOC'], $r['TDNAZD'], $r['TDPROD'], $r['TDDNAD']);
						$r['CSDESC'] = acs_u8e($r['CSDESC']);
						$ar[] = $r;
					}
		echo acs_je($ar);
	exit();
}






// ******************************************************************************************
// FORM
// ******************************************************************************************
?>

{"success":true, "items": [
        {
            xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            frame: true,
            title: 'Day <?php echo print_date($m_params->data); ?>, Itinerario <?php echo $s->decod_std('ITIN', $m_params->itin); ?>',
            
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			            
            
            items: [					 	
					{
						xtype: 'grid',
						itemId: 'tabSpedizioni',
						loadMask: true,
						flex: 1,
						 features: [
							{
							ftype: 'filters',
							encode: false, 
							local: true,   
						    filters: [
						       {
						 	type: 'boolean',
							dataIndex: 'visible'
						     }
						      ]
						}],
						selModel: {selType: 'checkboxmodel', mode: 'SIMPLE'},				
						//columnLines: true,			
						store: {
							xtype: 'store',
							autoLoad:true,	
							proxy: {
								url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data',
								method: 'POST',
								type: 'ajax',
					
								//Add these two properties
								actionMethods: {
									read: 'POST'
								},
					
								reader: {
									type: 'json',
									method: 'POST',
									root: 'root'
								}
								, doRequest: personalizza_extraParams_to_jsonData
								, extraParams: <?php echo acs_je($m_params); ?> 
							},
								
							fields: ['TDNBOC', 'TDCCON', 'TDDCON', 'carico', 'CSDESC', 'loca']
										
										
						}, //store
						multiSelect: true,
						
					
						columns: [
							{header: 'Cliente', 		dataIndex: 'TDCCON', width: 80,  filter: {type: 'string'}, filterable: true},						
							{header: 'Denominazione', 	dataIndex: 'TDDCON', flex: 1,  filter: {type: 'string'}, filterable: true},
							{header: 'Carico', 			dataIndex: 'carico', width: 80,  filter: {type: 'string'}, filterable: true},
							{header: 'Localit&agrave;', 		dataIndex: 'loca', flex:1,  filter: {type: 'string'}, filterable: true},
							{header: 'Sped.', 			dataIndex: 'TDNBOC', width: 60,  filter: {type: 'string'}, filterable: true},							
							{header: 'Note spedizione', 		dataIndex: 'CSDESC', flex: 1.2,  filter: {type: 'string'}, filterable: true},
						]
						 
					}	
						 
						 
				],
			buttons: [{
	            text: 'Conferma selezione',
	            iconCls: 'icon-windows-32',
	            scale: 'large',	            
	            handler: function() {

				var form_p = this.up('form');
				var tab_tipo_stato = form_p.down('#tabSpedizioni');
				var loc_win = this.up('window');
	            
				id_selected = tab_tipo_stato.getSelectionModel().getSelection();
  				list_selected_id = [];
  				for (var i=0; i<id_selected.length; i++) 
	   				list_selected_id.push(id_selected[i].data.TDNBOC);				
	            
				loc_win.fireEvent('onSelected', loc_win, list_selected_id);
	            } //handler
	        }
	        ],             
				
        }
]}			            