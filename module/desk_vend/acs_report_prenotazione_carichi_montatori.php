<?php

require_once "../../config.inc.php";

$s = new Spedizioni();
$main_module = new Spedizioni();


$ar_email_to = array();

$users = new Users;
$ar_users = $users->find_all();

foreach ($ar_users as $ku=>$u)
	$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");


$ar_email_json = acs_je($ar_email_to);



?>


<html>
<head>

<style>
div#my_content h1{font-size: 22px; padding: 5px;}
div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
table.acs_report{border-collapse:collapse; width: 100%;}
table.acs_report td, table.acs_report th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
.number{text-align: right;}
tr.liv1 td{background-color: #cccccc; font-weight: bold;}
tr.liv3 td{font-size: 9px;}
tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
 
tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
tr.ag_liv2 td{background-color: #DDDDDD;}
tr.ag_liv1 td{background-color: #ffffff;}
tr.ag_liv0 td{font-weight: bold;}
tr.ag_liv_data th{background-color: #333333; color: white;}


/* acs_report */
.acs_report{font-family: "Times New Roman",Georgia,Serif;}
table.acs_report tr.t-l1 td{font-weight: bold; background-color: #c0c0c0;}
table.acs_report tr.d-l1 td{font-weight: bold; background-color: #e0e0e0;}
h2.acs_report{color: white; background-color: #000000; padding: 5px; margin-top: 10px; margin-bottom: 10px;}
table.tabh2{background-color: black; color: white; padding: 5px; margin-top: 10px; margin-bottom: 10px; width: 100%; font-size: 24px; font-weight: bold;}
table.tabh2 td{font-size: 20px; padding: 5px;}

table.acs_report tr.f-l1 td{font-weight: bold; background-color: #f0f0f0;}
 
@media print
{
	.noPrint{display:none;}
}

</style>


  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
	Ext.onReady(function() {	
	});    
    
  </script>


</head>
<body>



<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 




<div id='my_content'>

<h1>Riepilogo carichi programmati</h1>
<pre>
<?php


function add_array_valori_liv(&$ar, $r){	
	$ar["S_COLLI"] 		+= $r['S_COLLI'];
	$ar["S_VOLUME"] 	+= $r['S_VOLUME'];
	$ar["S_PALLET"] 	+= $r['S_PALLET'];
	$ar["S_PESO"] 	    += $r['S_PESO'];
	$ar["C_CLIENTI_DEST"] += $r['C_CLIENTI_DEST'];	
}


global $cfg_mod_Spedizioni;
$m_params = acs_m_params_json_decode();

$k_field = "TDBCAR, TDDTIC, TDHMIC, TDDTFC, TDHMFC, TDDTSP, TA_ITIN.TADESC AS D_ITIN, TA_TRAS.TAKEY1 AS C_TRASP, TA_TRAS.TADESC AS D_TRASP, TDVETT, TDSTAB, CSPROG, CSVOLD, CSCITI, SP.CSHMPG, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSCSTR, SP.CSCSTD, SP.CSDESC, TA_ASPE.TAKEY1 AS TAASPE, SP.CSDTPG, SP.CSHMPG, SP.CSDTSC, SP.CSHMSC ";
$k_field_ord = "TDDTIC, TDHMIC, SP.CSDTPG, SP.CSHMPG, TDDTSP, CSCITI";
$k_field_grp = "TDBCAR, TDDTIC, TDHMIC, TDDTFC, TDHMFC, TA_ITIN.TADESC, TA_TRAS.TAKEY1, TA_TRAS.TADESC, TDVETT, TDSTAB, CSPROG, CSVOLD, TDDTSP, CSCITI, SP.CSHMPG, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSTISP, SP.CSTITR, SP.CSKMTR, SP.CSCSTR, SP.CSCSTD, SP.CSDESC, TA_ASPE.TAKEY1, SP.CSDTPG, SP.CSHMPG, SP.CSDTSC, SP.CSHMSC ";

$s_field = $k_field;

$sql = "SELECT $s_field
	, SUM(TDTOCO) AS S_COLLI, SUM(TDVOLU) AS S_VOLUME, SUM(TDBANC) AS S_PALLET, SUM(TDPLOR) AS S_PESO
	, SUM(TDPLOR) AS S_PESOL, SUM(TDPNET) AS S_PESON, SUM(TDTIMP) AS S_IMPORTO
	, SUM(TDCOSP) AS S_COLLI_SPUNTATI
	, COUNT(DISTINCT(CONCAT(TDIDES, CONCAT(TDDLOC, CONCAT(TDDCAP, TDNAZD))))) AS C_CLIENTI_DEST
	, MIN(CASE WHEN TDDTSSI > 0 THEN CONCAT(TDDTSSI, CONCAT('|', TDHMSSI)) END) AS MIN_DT_TIME_SPUNTA
	, MAX(CASE WHEN TDDTSSF > 0 THEN CONCAT(TDDTSSF, CONCAT('|', TDHMSSF)) END) AS MAX_DT_TIME_SPUNTA	
	FROM {$cfg_mod_Spedizioni['file_testate']}
			INNER JOIN {$cfg_mod_Spedizioni['file_calendario']} SP
				ON TDDT = SP.CSDT AND TDNBOF = SP.CSPROG AND SP.CSCALE = '*SPR'
				LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
						ON SP.CSDT = TA_ITIN.TADT AND TA_ITIN.TATAID = 'ITIN' AND TA_ITIN.TAKEY1 = SP.CSCITI
						LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ASPE
						ON TA_ASPE.TADT = TA_ITIN.TADT AND TA_ASPE.TATAID = 'ASPE' AND TA_ASPE.TAKEY1 = TA_ITIN.TAASPE

						LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
			   ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
			   LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
			   ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'

			   WHERE " . $s->get_where_std() . " AND TDSWSP='Y' ";


	//parametri dalla form di ingresso
	$form_ep = (object)json_decode($_REQUEST['form_ep']);
	
	
	//se ho open_UPLOAD aggiungo a $form_ep i parametri impostati
	if ((int)$js_parameters->open_UPLOAD > 0){
	
		//verifico se esiste la memorizzazione
		$f8 = new F8();
		$f8_row = $f8->get_memorizzazione_data_by_id($js_parameters->open_UPLOAD, (object)array("mod" => $s->get_cod_mod(), "func" => "OPEN_UPLOAD"));
	
		if ($f8_row != null){
			$open_UPLOAD_parameters = (array)json_decode($f8_row['DFMEMO']);
			$open_UPLOAD_parameters = form_ep_remove_blank_parameters($open_UPLOAD_parameters);
	
			$form_ep = (object)array_merge($open_UPLOAD_parameters, (array)$form_ep);
		} else {
			die("Errore 010010");
		}
	
	}
	
	

	//se non viene passato imposto come data quella attuale
	if (strlen($form_ep->f_data) == 0)
		$form_ep->f_data = oggi_AS_date();	
	
	//data
	if (strlen($form_ep->f_data_a) > 1)
		$sql .= " AND TDDTIC >= " . $form_ep->f_data . " AND TDDTIC <= " . $form_ep->f_data_a;
	else
		$sql .= " AND TDDTIC = " . $form_ep->f_data;
	
		//area spedizione
	if (strlen($form_ep->f_area_spedizione) > 0)
	$sql .= " AND TA_ASPE.TAKEY1 = " . sql_t($form_ep->f_area_spedizione);
	
	//tipologia trasporto (multipla)
	if (count($form_ep->f_trasporto) > 0)
	$sql .= " AND SP.CSTITR IN (" . sql_t_IN($form_ep->f_trasporto) . ")";
		
	//divisione
	$sql .= sql_where_by_combo_value('TDCDIV', $form_ep->f_divisione);
	
	//trasportatore
	$sql .= sql_where_by_combo_value('TA_TRAS.TAKEY1', $form_ep->f_trasportatore);
		
	//stabilimento
	$sql .= sql_where_by_combo_value('TDSTAB', $form_ep->f_stabilimento);
	
	
	//filtri con extraParams in ingresso di tio "ep_"
	foreach ($_REQUEST as $kv=>$v){
	if (substr($kv, 0, 3) == 'ep_' && strlen(trim($v)) > 0 ){
	$sql .= " AND " . substr($kv, 3, 100) . " = " . sql_t($v);
	}
	}

	$sql .= " GROUP BY {$k_field_grp} ORDER BY $k_field_ord";
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);


	$ar = array();
	while ($row = db2_fetch_assoc($stmt)){
	 
	 $liv0 = implode("_", array($row['TDDTIC'], trim($row['TDSTAB'])));				//DATA / TRASPORTATORE
	 $liv1 = implode("_", array($row['CSPROG'])); 									//SPEDIZIONE
	 
	 $row['EL_CARICHI_D'] = $s->get_el_carichi_by_TDNBOF($row['CSPROG'], 'TN', 'N', 'N', $row['TDSTAB']);
	 $row['VMC_D'] = $s->decod_vmc_by_sped_row($row);	 	 

	 add_array_valori_liv($gen_tot, $row);
	 
	 // --- LIV0
	 $d_ar = &$ar; 
	 $c_liv = $liv0;
	 if (!isset($d_ar[$c_liv]))
	 	$d_ar[$c_liv] = array("cod" => $c_liv, 
	 						"data" => $row['TDDTIC'], 
	 						"area" => $row['TAASPE'],
	 						"c_stab"  => $row['TDSTAB'],
	 						"descr"=>$c_liv, 
	 						"children"=>array());
	 add_array_valori_liv($d_ar[$c_liv], $row);
	 $d_ar = &$d_ar[$c_liv]['children']; 

	 // --- LIV1
	 $c_liv = $liv1;
	 if (!isset($d_ar[$c_liv]))
	 	$d_ar[$c_liv] = array("row" => $row,
	 			"children"=>array());
	 add_array_valori_liv($d_ar[$c_liv], $row);	 
	 $d_ar = &$d_ar[$c_liv]['children'];

	 // --- LIV2
	 $c_liv = $liv2;
	 if (!isset($d_ar[$c_liv]))
	 	$d_ar[$c_liv] = array("row" => $row,
	 			"children"=>array());
	 add_array_valori_liv($d_ar[$c_liv], $row);	 
	 $d_ar = &$d_ar[$c_liv]['children'];
	 	 	 
	} //while



// STAMPO REPORT *******************************
foreach ($ar as $k0 => $l0){

 $l0_tot = array();

 echo "
		<table class=tabh2>
		 <tr><td colspan=12  class=\"acs_report\">
			Carichi del " . print_date($l0['data']) . " - " . $s->decod_std('START', $l0['c_stab']) . "
		 </td></tr>
	    </table>";
 
 echo "<table class=acs_report>";
 
 ?>
		<tr class="t-l1">
 		 <td>Ora carico</td> 		 
 		 <td>Porta</td>
 		 <td>Spunta</td>
 		 <td>Trasportatore</td>		
 		 <td>Vettore</td>
 		 <td>Itinerario</td>		 
 		 <td>Elenco carichi</td>		 		 		 
 		 <td>Colli</td>
 		 <td>Pallet</td>
 		 <td>Volume</td>
 		 <td>Peso</td>		  		  	
 		 <td>Data/ora spedizione</td>
 		 <td>Data/ora scarico</td> 		 
 		</tr>
 		
 <?php 		
 
// 	 usort($l0['children'], "cmp_by_TRASP_D");  
	 foreach ($l0['children'] as $k1 => $l1){		
		?>
		
		<tr class="">
		 <td class=number><?php echo print_ora($l1['row']['TDHMIC']) . " - " . print_ora($l1['row']['TDHMFC']) ?></td>
		 <td ><?php echo $l1['row']['TDBCAR']; ?></td>
		 <td>
				<?php
					//Data inizio/fine spunta
					if (strlen($l1['row']['MIN_DT_TIME_SPUNTA'] > 0) || strlen($l1['row']['MAX_DT_TIME_SPUNTA'] > 0 ))
					{
					 $dt_i = explode("|", $l1['row']['MIN_DT_TIME_SPUNTA']);
					 $dt_f = explode("|", $l1['row']['MAX_DT_TIME_SPUNTA']);

					 //verifico se sono al'interno dell'orario prenotato
					 if (
							$dt_i[0]*100000000 + $dt_i[1] < $l1['row']['TDDTIC'] * 100000000 + $l1['row']['TDHMIC'] ||
							$dt_f[0]*100000000 + $dt_f[1] > $l1['row']['TDDTFC'] * 100000000 + $l1['row']['TDHMFC']
					 	)
					 	echo "<img src=" . img_path("icone/48x48/warning_black.png") . " width=14> ";
					 
					 echo print_date($dt_i[0], "%d/%m") . " " . print_ora($dt_i[1]) . " - " . print_date($dt_f[0], "%d/%m") . " " . print_ora($dt_f[1]); 
					}
				 ?>		 
		 </td>		
		 <td><?php echo $l1['row']['D_TRASP'] ?></td>
		 <td><?php echo $l1['row']['VMC_D'] . " (#{$l1['row']['CSPROG']})"; ?>
		 
		 	<?php
		 	 	if (strlen(trim($l1['row']['CSDESC'])) > 0)
		 	 		echo "<br/>" . $l1['row']['CSDESC']; 
		 	?>
		 
		 </td>
		 <td><?php echo $l1['row']['D_ITIN']; ?></td>		 
		 <td><?php echo $l1['row']['EL_CARICHI_D']; ?></td>		 		 		 
		 <td class=number><?php echo $l1['S_COLLI']; ?></td>		 
		 <td class=number><?php echo $l1['S_PALLET']; ?></td>		 
		 <td class=number><?php echo n($l1['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo n($l1['S_PESO'], 2); ?></td>		 		 
		 <td align=center><?php echo print_date($l1['row']['CSDTPG'], "%d/%m") . " " . print_ora($l1['row']['CSHMPG']); ?></td>
		 <td align=center><?php echo print_date($l1['row']['CSDTSC'], "%d/%m") . " " . print_ora($l1['row']['CSHMSC']); ?></td>		 
		</tr>
		
		
		<?php
		
	 } //per ogni riga
	 
	 ?>
	 
		<tr class="f-l1">
		 <td colspan=7>Totale</td>		 		 		 		 		 
		 <td class=number><?php echo $l0['S_COLLI']; ?></td>		 
		 <td class=number><?php echo $l0['S_PALLET']; ?></td>		 
		 <td class=number><?php echo n($l0['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo n($l0['S_PESO'], 2); ?></td>
		 <td colspan=2>&nbsp;</td>		 		 		 		 		 
		</tr>
	 
	 <?php
	 
	 
	 
 echo "</table>";

}



?>


<?php if (count($ar) > 1 && false){ ?>
<h2 class=acs_report>Totale generale</h2>
<table class=acs_report>
		<tr class="t-l1">
		 <td width="50%" colspan=4></td>		 
 		 <td>Nr scarichi</td>		  		 
 		 <td>Colli</td>
 		 <td>Pallet</td>
 		 <td>Volume</td>
 		 <td>Peso</td>		 
 		</tr>
		<tr class="f-l1">
		 <td colspan=4>Totale generale</td>		 
		 <td class=number><?php echo $gen_tot['C_CLIENTI_DEST']; ?></td>		 		 
		 <td class=number><?php echo $gen_tot['S_COLLI']; ?></td>		 
		 <td class=number><?php echo $gen_tot['S_PALLET']; ?></td>		 
		 <td class=number><?php echo n($gen_tot['S_VOLUME'], 2); ?></td>		 
		 <td class=number><?php echo n($gen_tot['S_PESO'], 2); ?></td>
		</tr>
 		
</table>
<?php } ?>




<?php


function cmp_by_TRASP_D($a, $b)
{
	return strcmp($a["TRASP_D"], $b["TRASP_D"]);
}


function somme_conteggi($ar, $r){
	$ar['NR_ORDINI_COMPLETI'] 	+= $r['NR_ORDINI_COMPLETI'];
	$ar['C_CLIENTI_DEST'] 		+= $r['C_CLIENTI_DEST'];
	$ar['S_KM'] 				+= $r['CSKMTR'];	
	$ar['S_COLLI'] 				+= $r['S_COLLI'];
	$ar['S_VOLUME'] 			+= $r['S_VOLUME'];
	$ar['S_IMPORTO'] 			+= $r['S_IMPORTO'];
	$ar['imp_prog'] 			+= $r['imp_prog'];
	$ar['imp_riprog'] 			+= $r['imp_riprog'];	
	$ar['CSCSTR'] 				+= $r['CSCSTR'];
	$ar['CSCSTD'] 				+= $r['CSCSTD'];		
  return $ar;	
} 



?>


</div>
</body>
</html>


