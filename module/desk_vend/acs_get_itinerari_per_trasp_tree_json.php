<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

$stmt = get_elenco_raggruppato_itinerari_per_data($_REQUEST["node"]);

$ar = array();
$oldLocale = setlocale(LC_TIME, 'it_IT');
while ($row = db2_fetch_assoc($stmt)){
	
	$tmp_ar_id = array();	

	$liv0 = implode("_", array(trim($row['C_TRASP'])));	//TRASPORTATORE
	$liv1 = implode("_", array(trim($row['CSCVET']))); 	//VETTORE
	$liv2 = implode("_", array(trim($row['CSPROG'])));	//SPEDIZIONE
	
	//liv 0 ---------------------------
		$d_ar = &$ar;
		$c_liv = $liv0;
		$tmp_ar_id[] = $c_liv;	
		if (!isset($d_ar[$c_liv]))
			$d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id)	, "task" => trim($row['D_TRASP']), "children"=>array());
		
		$d_ar = &$d_ar[$c_liv]['children'];
	

	//liv 1 ---------------------------
		$c_liv = $liv1;
		$tmp_ar_id[] = $c_liv;
		if (!isset($d_ar[$c_liv]))
			$d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id)	, "task" => trim($row['D_VETT']), "children"=>array());
		
		$d_ar = &$d_ar[$c_liv]['children'];
		
	//liv 2 ---------------------------
		$c_liv = $liv2;
		$tmp_ar_id[] = $c_liv;
		if (!isset($d_ar[$c_liv]))
			$d_ar[$c_liv] = array("id" => implode("|", $tmp_ar_id), "children"=>array());
		
		$tipo_sum = imposta_tipo_sum();
		
		
			$d_ar[$c_liv]['task'] = ucfirst(strftime("%a %d/%m", strtotime($row['CSDTSP'])));
		
		$d_ar[$c_liv]['task'] .= " " . print_ora($row['CSHMPG']);
		$d_ar[$c_liv]['task'] .= " ({$tipo_sum}" . number_format($row['TOT_F'], 0) . "/" . number_format($row['DISP_F'], 0) . ")"; 
		$d_ar[$c_liv]['task'] .= " " . $s->decod_mc_by_sped_row($row);

		
		
		$d_ar[$c_liv]['sped_id'] = $row['CSPROG'];
		$d_ar[$c_liv]['iconCls'] = 'iconSpedizione';
		$d_ar[$c_liv]['leaf'] = true;
		
		$d_ar = &$d_ar[$c_liv]['children'];
	
} //while
setlocale(LC_TIME, $oldLocale);

  	//esporto i dati
  	foreach($ar as $kar => $r){
  		$ret[] = array_values_recursive($ar[$kar]);  	
  	}  	

	echo acs_je($ret);


$appLog->add_bp('crea_alberatura_json_per_trasp - END');
$appLog->save_db();





/******************* UTILITY ******************************/
function imposta_tipo_sum(){
	global $s;
	switch($s->imposta_field_dett()){
		case "VOL": $tipo_sum = "V: "; break;
		case "ORD": $tipo_sum = "O: ";  break;
		case "COL": $tipo_sum = "C: "; break;
		case "PAL": $tipo_sum = "P: "; break;
		default: $tipo_sum = ""; break;
	}
	return $tipo_sum;
}


function get_elenco_raggruppato_itinerari_per_data($id){
	global $conn, $s;
	global $cfg_mod_Spedizioni;

	$oggi = oggi_AS_date();
	$next_3_months = date('Ymd', strtotime('+3 month'));

	switch($s->imposta_field_dett()){
		case "VOL": $sum_field = ", SUM(TDVOLU) AS TOT_F, MAX(SP.CSVOLD) AS DISP_F "; break;
		case "ORD": $sum_field = ", COUNT(*)    AS TOT_F, COUNT(*) AS DISP_F ";  break;
		case "COL": $sum_field = ", SUM(TDTOCO) AS TOT_F, SUM(TD.TDTOCO) AS DISP_F "; break;
		case "PAL": $sum_field = ", SUM(TDBANC) AS TOT_F, MAX(SP.CSBAND) AS DISP_F "; break;
		default: $val = 0; break;
	}


	if ($id == "root")
	{
		$sql_where_flt = " 1=1 ";
	}
	else
	{
		//trovo liv1 e liv2 selezionati
		$sottostringhe = explode("|", $id);
		$liv1 = $sottostringhe[2]; //anno-settimana
		$liv2 = $sottostringhe[4];
		$sql_where_flt = " ( TDDTEP='$liv2' ) ";
	}


	$sql = "SELECT SP.CSHMPG, CAL.CSAARG, CAL.CSNRSE, SP.CSCITI, SP.CSDTSP, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSPROG, SP.CSSTSP,
			  TA_TRAS.TAKEY1 AS C_TRASP, TA_TRAS.TADESC AS D_TRASP,
			  TA_VETT.TAKEY1 AS C_VETT, TA_VETT.TADESC AS D_VETT
			  $sum_field
			FROM {$cfg_mod_Spedizioni['file_calendario']} SP
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_testate']} TD
					ON TDDT = SP.CSDT AND SP.CSCALE = '*SPR' AND TDNBOC = SP.CSPROG
					AND  " . $s->get_where_std() . " AND TDSWSP = 'Y' AND $sql_where_flt
			" . $s->add_riservatezza() . "		
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_ITIN
					ON TA_ITIN.TATAID='ITIN' AND TA_ITIN.TADT = SP.CSDT AND TA_ITIN.TAKEY1 = SP.CSCITI
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_calendario']} CAL
					ON CAL.CSDT = SP.CSDT AND CAL.CSCALE = '*CS' AND CAL.CSDTRG = SP.CSDTSP
					
			LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_VETT
	   			ON TA_VETT.TADT = SP.CSDT AND TA_VETT.TAKEY1 = SP.CSCVET AND TA_VETT.TATAID = 'AUTR'
	   		LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_tabelle']} TA_TRAS
	   			ON TA_VETT.TADT = TA_TRAS.TADT AND TA_TRAS.TAKEY1 = TA_VETT.TACOGE AND TA_TRAS.TATAID = 'AVET'							
					
			WHERE (
				   	   (SP.CSSTSP <> 'PI' AND SP.CSDTSP >= {$oggi})
				  	OR ( (SP.CSSTSP = 'PI' OR SP.CSSTSP = '') AND SP.CSDTSP >= {$oggi} AND SP.CSDTSP <= {$next_3_months})				   	    
				  	OR (TD.TDONDO <> '')
				)
				AND " . $s->get_where_std('none', array('filtra_su' => 'SP')) . "
				AND TD.TDNRCA > 0	
			GROUP BY SP.CSHMPG, CAL.CSAARG, CAL.CSNRSE, SP.CSCITI, SP.CSDTSP, SP.CSCVET, SP.CSCAUT, SP.CSCCON, SP.CSPROG, SP.CSSTSP,
					TA_TRAS.TAKEY1, TA_TRAS.TADESC,
					TA_VETT.TAKEY1, TA_VETT.TADESC
			HAVING MAX(TD.TDONDO) IS NOT NULL OR SP.CSSTSP NOT IN('GA', 'AN')
			ORDER BY CAL.CSAARG, CAL.CSNRSE, SP.CSDTSP, SP.CSCVET";

		$stmt = db2_prepare($conn, $sql);
		echo db2_stmt_errormsg();		
		$result = db2_execute($stmt);
	return $stmt;
}










?>
