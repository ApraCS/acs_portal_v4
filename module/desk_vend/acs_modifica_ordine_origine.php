<?php 

require_once "../../config.inc.php";

$main_module = new Spedizioni();
$s = new Spedizioni();
$m_params = acs_m_params_json_decode();

$o_orig = $s->get_ordine_by_k_docu($m_params->k_ordine);

// ******************************************************************************************
// FORM principale
// ******************************************************************************************
if ($_REQUEST['fn'] == 'verifica_ordine'){
    ini_set('max_execution_time', 300000);
    $ret = array();

 	$oe = $s->k_ordine_td_decode_xx($m_params->k_ordine);
 
	//costruzione del parametro
	//ordine assistenza
	$cl_p = str_repeat(" ", 246);
	$cl_p .= sprintf("%-2s", $oe['TDDT']);
	$cl_p .= sprintf("%-2s", $oe['TDOTID']);
	$cl_p .= sprintf("%-3s", $oe['TDOINU']);
	$cl_p .= sprintf("%04d", $oe['TDOADO']);
	$cl_p .= sprintf("%06d", $oe['TDONDO']);
	
	//ordine da abbinare
	$cl_p .= sprintf("%-2s", $oe['TDOTID']);
	$cl_p .= sprintf("%-3s", $oe['TDOINU']);
	$cl_p .= sprintf("%04d", $m_params->rec_selected->anno);
	$cl_p .= sprintf("%06d", $m_params->rec_selected->numero);

	
	$k_ordine_orig = $s->k_ordine_to_anno_numero($m_params->k_ordine, 
			sprintf("%04d", $m_params->rec_selected->anno), 
			sprintf("%06d", $m_params->rec_selected->numero));

	if ($m_params->tipo == 'esegui')
		$cl_p .= "Y";
	else //verifica
		$cl_p .= " ";
		
	$cl_p .= str_repeat(" ", 100);
	
	
	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita})", array(), $cl_return = array());
	$call_return = $tkObj->CLCommand("ADDLIBLE   LIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
	$call_return = $tkObj->CLCommand("CHGCURLIB  CURLIB({$libreria_predefinita_EXE})", array(), $cl_return = array());
		
	$cl_in[] = $tkObj->AddParameterChar('both', 502, 'DATI', 'LK-AREA', $cl_p );
	$call_return = $tkObj->PgmCall('UR21JNC', $libreria_predefinita_EXE, $cl_in, null, null);

	$rets = $call_return['io_param']['LK-AREA'];
	
	$tkObj->disconnect();
	
	$ret['k_ordine']		= $m_params->k_ordine;
	$ret['k_ordine_orig']	= $k_ordine_orig;
	$ret['codice_cliente']  = substr($rets, 279, 9);
	$ret['ragione_sociale'] = substr($rets, 288, 30);
	$ret['riferimento']     = substr($rets, 318, 30);
	$ret['data_ordine']		= substr($rets, 348, 8);
	$ret['fattura']			= substr($rets, 356, 20);
	$ret['data_fattura']	= substr($rets, 376, 8);
	
	$out_ret = array();
	$out_ret[] = "Codice cliente ................... " . $ret['codice_cliente'];
	$out_ret[] = "Denominazione .................... " . $ret['ragione_sociale'];
	$out_ret[] = "Riferimento ...................... " . $ret['riferimento'];
	$out_ret[] = "Data ordine.... .................. " . print_date($ret['data_ordine']);
	$out_ret[] = "Fattura .......................... " . $ret['fattura'];
	$out_ret[] = "Data fattura ..................... " . print_date($ret['data_fattura']);
	
	$ret['out_esito'] = implode("\n", $out_ret); 
	
	$ret['success'] = true;
	echo acs_je($ret);
 
 exit;
}


if ($_REQUEST['fn'] == 'get_json_data_ordini'){
	
	$m_params = acs_m_params_json_decode();
	
	$k_ordine=$m_params->open_request->k_ordine;
	if($k_ordine == '')
		$k_ordine=$m_params->open_request->list_selected_id[0]->k_ordine;
	
	$cliente=$m_params->open_request->cliente;
	if($cliente == '')
		$cliente = $m_params->open_request->form_values->f_cliente_cod;
	
		
	if (strlen($k_ordine) > 0){
		$k_ord= explode("_", $k_ordine);
		$vo = $k_ord[2];
		$sql_where .= " AND TDINUM ='{$vo}'";
	}
	
	$data_eva_i=$m_params->form_values->f_data_eva_i;
	$data_eva_f=$m_params->form_values->f_data_eva_f;
	
	//controllo data
	if (strlen($data_eva_i) > 0)
		$sql_where .= " AND TDDTEP >= '{$data_eva_i}'";
	if (strlen($data_eva_f) > 0)
		$sql_where .= " AND TDDTEP <= '{$data_eva_f}'";
	
	$data_reg_i=$m_params->form_values->f_data_reg_i;
	$data_reg_f=$m_params->form_values->f_data_reg_f;
	
	if (strlen($data_reg_i) > 0)
		$sql_where .= " AND TDDTRG >= '{$data_reg_i}'";
	if (strlen($data_reg_f) > 0)
		$sql_where .= " AND TDDTRG <= '{$data_reg_f}'";
	
	$riferimento=$m_params->form_values->f_rif;

	if (strlen($riferimento) > 0)	
		$sql_where.= " AND UPPER(TDVSRF) LIKE '%" . strtoupper($m_params->form_values->f_rif) . "%' ";
		
	$sql = "SELECT *
	FROM {$cfg_mod_Spedizioni['file_testate_doc_gest_cliente']} TD0
	WHERE TDCCON = '{$cliente}' AND TDTIDO='VO'  {$sql_where} ORDER BY TDAADO DESC FETCH FIRST 50 ROWS ONLY";
	
	/*print_r($sql);
	exit;*/
	
	$stmt = db2_prepare($conn, $sql);
	echo db2_stmt_errormsg();
	$result = db2_execute($stmt);
	
	while($row = db2_fetch_assoc($stmt)){
		$nr = array();
		$nr['anno']	        = trim($row['TDAADO']);
		$nr['numero']	    = trim($row['TDNRDO']);
	    $nr['riferimento']	= trim($row['TDVSRF']);
		$nr['tipologia']	= trim($row['TDTPDO']);
		$nr['data_eva']	    = trim($row['TDDTEP']);
		$nr['data_reg'] 	= trim($row['TDDTRG']);
		$nr['stato']	    = trim($row['TDSTAT']);
		$nr['k_ordine']     = implode("_", array($row['TDDT'], $row['TDTIDO'], $row['TDINUM'], $row['TDAADO'], $row['TDNRDO'], " ", $row['TDDT'] ));
	
		$ar[] = $nr;
	
	}
	
	echo acs_je($ar);
	
	
	exit;
	
}


// ******************************************************************************************
// FORM principale
// ******************************************************************************************
/*if ($_REQUEST['fn'] == 'form_modifica_ordine_origine'){ 
	$m_params = acs_m_params_json_decode();
	$anno_selected= $m_params->ordine_selected->anno;
	$numero_selected= $m_params->ordine_selected->numero;
	
	
	
	?>


	
 {"success":true, "items": [
	
	{
		xtype: 'form',
		bodyStyle: 'padding: 10px',
		bodyPadding: '5 5 0',
		frame: true,
		title: '',
		layout: {
			type: 'vbox',
			align: 'stretch',
			pack : 'start',
		},
		items: [
				{
					xtype: 'fieldcontainer',
					layout: {type: 'hbox', align: 'stretch'},				
					items: [		
								{
									name: 'f_num_ordine',
									margin: "10 40 10 10",
									labelWidth :80,
									anchor: '-15',
									labelAlign: 'left',   
									xtype: 'numberfield',
									fieldLabel: 'Nr. Ordine',
									hideTrigger: true,
									value: '<?php echo $numero_selected?>',
									hidden: true				
        											
								 }, 
								 
								 {						
									name: 'f_anno_ordine',
									margin: "10 80 10 10",  
									anchor: '-15', 
									labelWidth :60,
									xtype: 'numberfield',
									labelAlign: 'left',
									fieldLabel: 'Anno',
									hideTrigger: true,
									value: '<?php echo $anno_selected?>',
									hidden: true					
								 }	
								 
								 
								 ]}
								, {						
									xtype: 'button', itemId: 'b_verifica',
									text: 'Verifica',
									scale: 'large',
						            iconCls: 'icon-button_black_play-32',
						            handler: function() {
						            
						            
  		    			     		      Ext.MessageBox.show({
				          					 msg: 'Verifica in corso',
				          					 progressText: 'Running...',
				          					 width:300,
				          					 wait:true, 
				          					 waitConfig: {interval:200},
				         				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
									       });						            
						            
						            	mm_form = this.up('form')
						            	m_form = this.up('form').getForm();
									         	
										Ext.Ajax.request({
										   timeout: 2400000,
										   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=verifica_ordine',
										   method: 'POST',
										   jsonData: {
										    k_ordine: <?php echo j($m_params->k_ordine)?>,
										   	form_values: this.up('form').getValues(),
										   	tipo: 'verifica'
										   }, 
										   
										   success: function(response, opts) {
										      var jsonData = Ext.decode(response.responseText);

											  f_log = m_form.findField('esito_verifica');
											  f_log.setValue(jsonData.out_esito);
										      
										      //verifico che sia lo stesso cliente
										      if (jsonData.codice_cliente.trim() != <?php echo j($m_params->cliente)?>){
										      
										      
										      	acs_show_msg_error('Cliente non coincidente');
										      	return false;
										      }   

											  mm_form.down('#b_verifica').disable()
											  mm_form.down('#b_conferma').enable()											   				                      
											  mm_form.down('#b_reset').enable()
											  m_form.findField('f_num_ordine').setReadOnly(true);
											  m_form.findField('f_anno_ordine').setReadOnly(true);
											  Ext.MessageBox.hide();												  
										   }, 
										   failure: function(response, opts) {								    
										      Ext.Msg.alert('Message', 'No data to be loaded');
										      Ext.MessageBox.hide();	
										   }
										});						        			 					         	
				  			
				            		} //hanlder
								 }, {
					                fieldLabel: 'Esito richiesta',
					                name: 'esito_verifica',
					                disabled: false,
					                xtype     : 'textareafield',
					 				height: 180,
					 				fieldStyle: {
								     'fontFamily'   : 'courier new',
								     'fontSize'     : '14px'
								   }
					            }
					            
					            
						, { 
						xtype: 'fieldcontainer',
						layout: {type: 'hbox', align: 'stretch'},				
						items: [
					            
					            
								{						
									xtype: 'button', itemId: 'b_reset', flex: 1,
									text: 'Annulla',
									scale: 'large',
						            iconCls: 'icon-button_red_play-32',
						            disabled: true,
						            handler: function() {
						            
						            	mm_form = this.up('form')
						            	m_form = this.up('form').getForm();

										mm_form.down('#b_verifica').enable()
										mm_form.down('#b_conferma').disable()											   				                      
										mm_form.down('#b_reset').disable()
										m_form.findField('esito_verifica').setValue('');										
										m_form.findField('f_num_ordine').setReadOnly(false);
										m_form.findField('f_anno_ordine').setReadOnly(false);
				  			
				            		} //hanlder
								 }					            
					            
					            , { xtype: 'tbfill' }, 
					            
					            {						
									xtype: 'button', itemId: 'b_conferma', flex: 1,
									text: 'Conferma',
									scale: 'large',
						            iconCls: 'icon-button_blue_play-32',
						            disabled: true,
						            handler: function() {
						            	m_win  = this.up('window');
						            	m_form = this.up('form').getForm();
						            	
  		    			     		      Ext.MessageBox.show({
				          					 msg: 'Esecuzione in corso',
				          					 progressText: 'Running...',
				          					 width:300,
				          					 wait:true, 
				          					 waitConfig: {interval:200},
				         				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
									       });							            	
						            	
									         	
										Ext.Ajax.request({
										   timeout: 2400000,
										   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=verifica_ordine',
										   method: 'POST',
										   jsonData: {
										    k_ordine: <?php echo j($m_params->k_ordine)?>,
										   	form_values: this.up('form').getValues(),
										   	tipo: 'esegui'
										   }, 
										   
										   success: function(response, opts) {		
												Ext.MessageBox.hide();			
												
												var jsonData = Ext.decode(response.responseText);
												
												fw = Ext.getCmp(<?php echo j($m_params->from_win);?>);
												fw.close();
												
												fso = Ext.getCmp(<?php echo j($m_params->from_win_seleziona_ordini);?>);
												fso.down('grid').store.load();
												
												acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest_assistenza.php', {
														        				k_ordine: jsonData.k_ordine_orig,
														        				prog: <?php echo j($m_params->prog) ?>,
														        				from_win_seleziona_ordini: <?php echo j($m_params->from_win)?>,
														        				tipo: 'origine'
														        			}, 900, 450, null, 'icon-folder_search-16');											
												
												m_win.close();
										   }, 
										   failure: function(response, opts) {								    
										      Ext.Msg.alert('Message', 'No data to be loaded');
										      Ext.MessageBox.hide();
										   }
										});						        			 					         	
				  			
				            		} //hanlder
								 }
								 
							]
						}		 
		] //items
	}
  ]}		
	
<?php	
	exit;
}*/

if ($_REQUEST['fn'] == 'ricerca_ordini_grid'){
$m_params = acs_m_params_json_decode();


	?>

{"success":true, "items": [

	     {
  			xtype: 'form',
            bodyStyle: 'padding: 10px',
            bodyPadding: '5 5 0',
            autoScroll:true,
            frame: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
     		},
			            
            
            items: [{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_reg_i'
							   , margin: "0 10 0 10"   
							   , flex: 1        
							   , labelWidth :150   		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data registrazione iniziale'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							   
							}, {
							     name: 'f_data_reg_f'
							   , margin: "0 10 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , labelWidth :120
							   , startDay: 1 //lun.
							   , fieldLabel: 'finale'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							 
							}
						
						]
					 },{ 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
							     name: 'f_data_eva_i'
							   , margin: "0 10 0 10"   
							   , flex: 1        
							   , labelWidth :150    		
							   , xtype: 'datefield'
							   , startDay: 1 //lun.
							   , fieldLabel: 'Data evasione iniziale'
							   , labelAlign: 'left'
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							  
							}, {
							     name: 'f_data_eva_f'
							   , margin: "0 10 0 10"						     
							   , flex: 1						                     		
							   , xtype: 'datefield'
							   , labelWidth :120 
							   , startDay: 1 //lun.
							   , fieldLabel: 'finale'
							   , labelAlign: 'right'						   
							   , format: 'd/m/Y'
							   , submitFormat: 'Ymd'
							   , allowBlank: true
							   , anchor: '-15'
							  
							}
						
						]
					 },	 { 
						xtype: 'fieldcontainer',
						layout: { 	type: 'hbox',
								    pack: 'start',
								    align: 'stretch'},						
						items: [{
						
						flex: 1,
			            xtype: 'textfield',
						name: 'f_rif',
						fieldLabel: 'Riferimento',
						labelWidth :150, 
						anchor: '-15',
						margin: "0 10 10 10",	
					
						}, {
							xtype: 'button',	
							text: 'Applica filtri',
							margin: "0 10 10 10",
							anchor: '-15',
							//iconCls: 'icon-sub_blue_accept-32',
							//scale: 'large',	
							handler: function() {
		            	
								l_form = this.up('form').getForm();
	            				l_grid = this.up('window').down('grid');
		            				
	            						l_grid.store.proxy.extraParams.form_values = l_form.getValues();
	            						l_grid.store.load();
	            						l_grid.getView().refresh();	   
		               	  
		                     
							 }
							  
							}
						
						]
					 },				 	
					 {
			xtype: 'grid',
			name: 'f_ordini',
			margin: "0 10 10 10",	
	       features: [
	
				{
			ftype: 'filters',
			encode: false, 
			local: true,   
		    filters: [
		       {
		 	type: 'boolean',
			dataIndex: 'visible'
		     }
		      ]
					}],	
			loadMask: true,	
			
			store: {
						xtype: 'store',
						autoLoad:true,
			
	  							proxy: {
								   url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=get_json_data_ordini', 
								   method: 'POST',								
								   type: 'ajax',

							       actionMethods: {
							          read: 'POST'
							        },
							        
							        
							           extraParams: {
										 open_request: <?php echo acs_raw_post_data(); ?>,
										 form_values: ''
			        				},
			        				
			        				doRequest: personalizza_extraParams_to_jsonData, 
						
								   reader: {
						            type: 'json',
									method: 'POST',						            
						            root: 'root'						            
						        }
							},
							
		        			fields: ['anno', 'numero', 'riferimento', 'tipologia', 'stato', 'data_eva', 'data_reg', 'k_ordine']							
									
			}, //store
				

			      columns: [	
			      {
	                header   : 'Anno',
	                dataIndex: 'anno',
	                width: 40,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
				{
	                header   : 'Numero',
	                dataIndex: 'numero',
	                width: 70,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Tipo',
	                dataIndex: 'tipologia',
	                width: 40,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Data',
	                dataIndex: 'data_reg',
	                renderer: date_from_AS,
	                width: 100,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
	            {
	                header   : 'Evasione',
	                dataIndex: 'data_eva',
	                renderer: date_from_AS,
	                width: 100
	            },
	            {
	                header   : 'Stato',
	                dataIndex: 'stato',
	                width: 50,
	                filter: {type: 'string'}, 
	                filterable: true
	            },
				{
	                header   : 'Riferimento',
	                dataIndex: 'riferimento',
	                flex:1,
	                filter: {type: 'string'}, 
	                filterable: true
	            }
	            
	        
	         ] ,		
	         
	         		listeners: {	
					itemcontextmenu : function(grid, rec, node, index, event) {
		  			event.stopEvent();
		  		
			 
				var voci_menu = [];
					
					voci_menu.push({
		         		text: 'Visualizza righe',
		        		iconCls : 'icon-folder_search-16',          		
		        		handler: function() {
		        			acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest.php', {
		        				k_ordine: rec.get('k_ordine'),
		        				prog: 0,
		        				tipo_show: 'show'
		        				}, 900, 450, null, 'icon-folder_search-16');          		
		        		}
		    		});	
					
					   var menu = new Ext.menu.Menu({
			            items: voci_menu
				       }).showAt(event.xy);
					
						}
					},
	   		
			
		} //fine grid
		 
						 
				],	
				

			
	       buttons: [
	      
	         
	            {
		        	text: 'Seleziona',
		            iconCls: 'icon-sub_blue_accept-32',
		            scale: 'large',	
		       handler: function() {
		       
		       
		       
		       
						            	m_win  = this.up('window');
						            	m_form = this.up('form').getForm();
						            	
									rec_selected = this.up('form').down('grid').getSelectionModel().getSelection()[0].data;
									
										
										 
									          if (!Ext.isEmpty(m_win.events.afteroksave)) {
                                                  m_win.fireEvent('afterOkSave', m_win, rec_selected.k_ordine);
                                              }  
                                               
                                              else {
                                              
                                              
                                              Ext.MessageBox.show({
				          					 msg: 'Esecuzione in corso',
				          					 progressText: 'Running...',
				          					 width:300,
				          					 wait:true, 
				          					 waitConfig: {interval:200},
				         				  	 icon:'ext-mb-download', //custom class in msg-box.html				          
									       });	
									       
                                              
	                                         Ext.Ajax.request({
											   timeout: 2400000,
											   url        : '<?php echo $_SERVER['PHP_SELF']; ?>?fn=verifica_ordine',
											   method: 'POST',
											   jsonData: {
											    k_ordine: <?php echo j($m_params->k_ordine)?>,
											   	form_values: this.up('form').getValues(),
											   	tipo: 'esegui',
											   	rec_selected: rec_selected
											   }, 
											   
											   success: function(response, opts) {		
													Ext.MessageBox.hide();			
													
													var jsonData = Ext.decode(response.responseText);
													
													if (!Ext.isEmpty(m_win.events.aftersuccesssave)) {
                                                  		m_win.fireEvent('afterSuccessSave', m_win, rec_selected.k_ordine);
                                                  		return false;
                                              		}  													
													
													fw = Ext.getCmp(<?php echo j($m_params->from_win);?>);
													fw.close();
													
													fso = Ext.getCmp(<?php echo j($m_params->from_win_seleziona_ordini);?>);
													fso.down('grid').store.load();
													
													acs_show_win_std('Righe ordine', 'acs_get_order_rows_gest_assistenza.php', {
															        				k_ordine: jsonData.k_ordine_orig,
															        				k_ordine_assistenza: <?php echo j($m_params->k_ordine)?>,
															        				prog: <?php echo j($m_params->prog) ?>,
															        				from_win_seleziona_ordini: <?php echo j($m_params->from_win_seleziona_ordini)?>,
															        				tipo: 'origine',
															        				cliente: <?php echo j($m_params->cliente) ?>
															        			}, 900, 450, null, 'icon-folder_search-16');											
													
													m_win.close();
											   }, 
											   failure: function(response, opts) {								    
											      Ext.Msg.alert('Message', 'No data to be loaded');
											      Ext.MessageBox.hide();
											   }
											});
                                               }       						            	
						            	
									         	
																        			 					         	
				  			
				            		}                  
		  
		        } 
	          
	            ]
				
        }
		
 				
	
     ]
        
 }



<?php 
	exit;
	}
?>
