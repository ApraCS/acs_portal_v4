<?php

require_once "../../config.inc.php";


	$s = new Spedizioni();
	$main_module = new Spedizioni();
	
	set_time_limit(240);	

if ($_REQUEST['fn'] == 'open_report'){
	

	$ar_email_to = array();
	$ar_email_to[] = array(trim($itinerario->rec_data['TAMAIL']), "ITINERARIO " . j(trim($itinerario->rec_data['TADESC'])) . " (" .  trim($itinerario->rec_data['TAMAIL']) . ")");
	$ar_email_to[] = array(trim($vettore->rec_data['TAMAIL']), "VETTORE " . j(trim($vettore->rec_data['TADESC'])) . " (" .  trim($vettore->rec_data['TAMAIL']) . ")");	
	
	$users = new Users;
	$ar_users = $users->find_all();
	
	foreach ($ar_users as $ku=>$u){
		$ar_email_to[] = array(trim($u['UTMAIL']), "UTENTE " . j(trim($u['UTDESC'])) . " (" .  trim($u['UTMAIL']) . ")");		
	}

	$ar_email_json = acs_je($ar_email_to);

	if ($is_linux == 'Y')
		$_REQUEST['form_values'] = strtr($_REQUEST['form_values'], array('\"' => '"', "\'" => "'", '\\\\' => '\\'));
	
	
	$filter = (array)json_decode($_REQUEST['f_filter']);
	
		$filtro = array();
		$filtro["cliente"] = $filter['f_cliente_cod'];

		if (strlen($filter['f_destinazione_cod']) > 0)		
			$filtro["destinazione"] = $filter['f_destinazione_cod'];			
		
		$filtro["riferimento"] = $filter['f_riferimento'];
		$filtro["agente"] = $filter['f_agente'];
		
		//data programmata
		if (strlen($filter['f_data_dal']) > 0)
			$filtro["data_dal"] = date('Ymd', strtotime($filter['f_data_dal']));		
		if (strlen($filter['f_data_al']) > 0)
			$filtro["data_al"] = date('Ymd', strtotime($filter['f_data_al']));		

		//data spedizione
		if (strlen($filter['f_data_sped_dal']) > 0)
			$filtro["data_sped_dal"] = date('Ymd', strtotime($filter['f_data_sped_dal']));
		if (strlen($filter['f_data_sped_al']) > 0)
			$filtro["data_sped_al"] = date('Ymd', strtotime($filter['f_data_sped_al']));
		
		
		//data rilascio
		if (strlen($filter['f_data_ril_dal']) > 0)
			$filtro["data_ril_dal"] = date('Ymd', strtotime($filter['f_data_ril_dal']));
		if (strlen($filter['f_data_ril_al']) > 0)
			$filtro["data_ril_al"] = date('Ymd', strtotime($filter['f_data_ril_al']));		
		
		//AREA
		if (strlen(trim($filter['f_area'])) > 0)
			$filtro['area'] = $filter['f_area'];

		//data ricezione (dal, al)
		if (strlen($filter['f_data_ricezione_dal']) > 0)
			$filtro["data_ricezione_dal"] = date('Ymd', strtotime($filter['f_data_ricezione_dal']));
		if (strlen($filter['f_data_ricezione_al']) > 0)
			$filtro["data_ricezione_al"] = date('Ymd', strtotime($filter['f_data_ricezione_al']));
		
		//data conferma (dal, al)
		if (strlen($filter['f_data_conferma_dal']) > 0)
			$filtro["data_conferma_dal"] = date('Ymd', strtotime($filter['f_data_conferma_dal']));
		if (strlen($filter['f_data_conferma_al']) > 0)
			$filtro["data_conferma_al"] = date('Ymd', strtotime($filter['f_data_conferma_al']));		
		
		
		if (strlen($filter['f_data_ricezione']) > 0)
			$filtro["data_ricezione"] = date('Ymd', strtotime($filter['f_data_ricezione']));		
		if (strlen($filter['f_data_conferma']) > 0)
			$filtro["data_conferma"] = date('Ymd', strtotime($filter['f_data_conferma']));
		
		//data disponibilita (dal, al)
		if (strlen($filter['f_data_disp_dal']) > 0)
			$filtro["data_disp_dal"] = date('Ymd', strtotime($filter['f_data_disp_dal']));
		if (strlen($filter['f_data_disp_al']) > 0)
			$filtro["data_disp_al"] = date('Ymd', strtotime($filter['f_data_disp_al']));
						
		//data evasione richiesta (dal, al)
		if (strlen($filter['f_data_ev_rich_dal']) > 0)
			$filtro["data_ev_rich_dal"] = date('Ymd', strtotime($filter['f_data_ev_rich_dal']));
		if (strlen($filter['f_data_ev_rich_al']) > 0)
			$filtro["data_ev_rich_al"] = date('Ymd', strtotime($filter['f_data_ev_rich_al']));
		
		$filtro['carico_assegnato'] = $filter['f_carico_assegnato'];
		$filtro['lotto_assegnato'] 	= $filter['f_lotto_assegnato'];
		$filtro['proforma_assegnato'] 	= $filter['f_proforma_assegnato'];
		
		$filtro['collo_disp'] = $filter['f_collo_disp'];		
		$filtro['collo_sped'] = $filter['f_collo_sped'];		
		
		$filtro['anomalie_evasione']= $filter['f_anomalie_evasione'];				
		$filtro['ordini_evasi'] 	= $filter['f_ordini_evasi'];		
		$filtro['divisione'] 		= $filter['f_divisione'];
		$filtro['modello'] 			= $filter['f_modello'];		
		$filtro['num_ordine'] 		= $filter['f_num_ordine'];
		$filtro['num_carico'] 		= $filter['f_num_carico'];
		$filtro['num_lotto'] 		= $filter['f_num_lotto'];				
		$filtro['num_proforma'] 	= $filter['f_num_proforma'];
		$filtro['indice_rottura'] 	= $filter['f_indice_rottura'];
		$filtro['indice_rottura_assegnato'] 	= $filter['indice_rottura_assegnato'];
		$filtro['num_sped'] 		= $filter['f_num_sped'];
		$filtro['conf'] 		    = $filter['f_conf'];
		$filtro['itinerario'] 		= $filter['f_itinerario'];		
		$filtro['tipologia_ordine'] = $filter['f_tipologia_ordine'];
		$filtro['stato_ordine'] 	= $filter['f_stato_ordine'];		
		$filtro['priorita'] 		= $filter['f_priorita'];		
		$filtro['solo_bloccati']	= $filter['f_solo_bloccati'];
		$filtro['confermati']		= $filter['f_confermati'];
		$filtro['articolo']		    = $filter['f_articolo'];
		$filtro['hold']	            = $filter['f_hold'];
		$filtro['stadio']	        = $filter['f_stadio'];
		$filtro['ava_paga']	        = $filter['f_ava_paga'];
		$filtro['forniture_MTO']	= $filter['f_forniture_MTO'];
		$filtro['prov_d']           = $filter['f_prov_d'];
		$filtro['loc_d']            = $filter['f_loc_d'];
		
		
		$filtro['escludi_filtro_DP'] = 'Y'; //voglio anche le spedizioni DP
		$stmt 	= $s->get_elenco_ordini($filtro, 'N', '', $_REQUEST['f_sceltadata'], false, 'Y');	//con 'search' ordinava per area/itin, qui vogliono invece per data
	
?>


<html>
 <head>
  
  <link rel="stylesheet" type="text/css" href=<?php echo acs_url("css/toolbars.css"); ?> />
  
  <style>
  
   <?php @include '../../personal/report_css_logo_cliente.php'; ?>
  
   div.legenda{border: 1px gray dotted; padding: 3px; font-size: 0.7em; margin-bottom: 10px;}
   table{border-collapse:collapse; width: 100%;}
   table.int1 td, table.int1 th{border: 1px solid gray; padding: 2px 5px; font-size: 11px;}
   .number{text-align: right;}
   table.int1 th.int_data{font-weight: bold; font-size: 13px;}
   tr.liv1 td{background-color: #cccccc; font-weight: bold;}
   tr.liv3 td{font-size: 9px;}
   tr.liv_totale td{background-color: #cccccc; font-weight: bold;}
   
   tr.ag_liv3 td{background-color: #AAAAAA; font-weight: bold;}
   tr.ag_liv2 td{background-color: #DDDDDD;}   
   tr.ag_liv_el_cliente td{background-color: #ffffff; font-weight: bold;}   
   tr.ag_liv_el_carico td{background-color: #cccccc;}  
   tr.ag_liv0 td{font-weight: bold;}   
   tr.ag_liv_data th{background-color: #333333; color: white; font-size: 18px;}   
   tr.ag_liv_area th{background-color: #b0b0b0; color: black;}
   tr.ag_liv1{background-color: #f3f3f3;}
   tr.ag_liv1 td{font-weight: bold;}
   span.sceltadata{font-size: 0.6em; font-weight: normal;}
   span.denominazione_cliente{font-weight: bold;}   
   
   h2.acs_report_title{font-size: 18px; padding: 10px;}
   
   //div#my_content h2{font-size: 1.6em; padding: 7px;}
   
	@media print 
	{
	    .noPrint{display:none;}
	}   
  </style>

  <link rel="stylesheet" type="text/css" href="../../../extjs/resources/css/ext-all.css" />
  <script type="text/javascript" src="../../../extjs/ext-all.js"></script>
  <script src=<?php echo acs_url("js/acs_js.js") ?>></script>  

  <script type="text/javascript">
 
	Ext.Loader.setConfig({
	    enabled: true
	});Ext.Loader.setPath('Ext.ux', '../ux');

    Ext.require(['*']);
    
    
    
	Ext.onReady(function() {
	});    

  </script>


 </head>
 <body>

 
<div class="page-utility noPrint">
<?php 
			$bt_fascetta_print = 'Y';
			$bt_fascetta_email = 'Y';
			$bt_fascetta_excel = 'Y';
			$bt_fascetta_close = 'Y';
			include  "../../templates/bottoni_fascetta.php";
?>	
</div> 
<div id='my_content'>  
 
<?php


if($_REQUEST['f_logo']=='Y'){
	
	//adesso il logo e fisso da personal css
	////echo "<img src=". logo_report_path()." height=70>";
	
}

if($_REQUEST['f_sceltadata'] == 'TDDTEP' ){
    $campo_data = "TDDTEP";
    if($_REQUEST['f_bilingue']=='Y'){
        echo "<h2 class='acs_report_title'> Riferimenti in consegna/Loading Plan </h2>";
    }else{
        echo "<h2 class='acs_report_title'>Riepilogo ordini per data evasione programmata " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";
    }
    
}else{
    $campo_data = "TDDTSP";
    
    if($_REQUEST['f_bilingue']=='Y'){
        echo "<h2 class='acs_report_title'> Riferimenti in consegna/Loading Plan </h2>";
    }else{
        echo "<h2 class='acs_report_title'>Riepilogo ordini per data spedizione " . add_descr_report($_REQUEST['add_descr_report']) .  "</h2>";
    }
    
    
}


$campo_ora = sceltacampo_ora($campo_data);

$_REQUEST['el_dettaglio_per_cliente'] = 'Y';
$_REQUEST['el_dettaglio_per_ordine'] = 'Y';
$_REQUEST['dettaglio_per_carico'] = 'N';
$_REQUEST['mostra_importo'] = 'Y';
 



$ar_tot["TOTALI"] = array();

while ($r = db2_fetch_assoc($stmt)) {
	
    
    if($r["TDSWSP"] == 'N')
	  $f_data = 'Ordini da programmare';
    else
      $f_data = $r[$campo_data];
    
	$f_liv0 = trim($r["TDASPE"]); 					$d_liv1 = trim($r["TDASPE"]);
	$f_liv1 = implode("_", array($r["TDCITI"])); 	$d_liv1 = $s->decod_std('ITIN', $r['TDCITI']);
	$f_liv2 = implode("_", array($r["TDCCON"], $r["TDCDES"]));
	$f_liv2_carico = implode("_", array($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA']));
	$f_liv3 = trim($r['TDDOCU']);					$d_liv2 = trim($r['TDDOCU']);
	

	$r['S_COLLI'] 	= $r['TDTOCO'];
	$r['S_VOLUME'] 	= $r['TDVOLU'];
	$r['S_PALLET'] 	= $r['TDBANC'];
	$r['S_PESOL'] 	= $r['TDPLOR'];
	$r['S_IMPORTO'] = $r['TDTIMP'];
	
	

	$tmp_ar = &$ar_tot["TOTALI"];
	$tmp_ar['VOLUME'] += $r['S_VOLUME'] * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
	$tmp_ar['COLLI'] += $r['S_COLLI'] ;
	$tmp_ar['PALLET'] += $r['S_PALLET'] ;
	$tmp_ar['PESOL'] += $r['S_PESOL'] ;
	$tmp_ar['IMPORTO'] += $r['S_IMPORTO'] ;
	$tmp_ar['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
		
	
	
	if (!isset($ar[$f_data]))
	    $ar[$f_data] = array("cod" => $f_data, "descr"=>$f_data, "hold" => $r['TDSWSP'],
				"val" => array(), "children"=>array());

		$d_ar = &$ar[$f_data]['children'];
			
		$tmp_ar = &$ar[$f_data];
		$tmp_ar["val"]['VOLUME'] += $r['S_VOLUME']  * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
		$tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
		$tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
		$tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
		$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
		$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;


		//liv0
		$cod_liv = $f_liv0;
		if (!isset($d_ar[$cod_liv]))
			$d_ar[$cod_liv] = array("cod" => $cod_liv, "descr"=>$s->decod_std('ASPE', $cod_liv),
					"val" => array(),
					"children"=>array());
				
		 $tmp_ar = &$d_ar[$cod_liv];
		 $tmp_ar["val"]['VOLUME'] += $r['S_VOLUME']  * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
		 $tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
		 $tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
		 $tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
		 $tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
		 $tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
		 	
		 $d_ar = &$tmp_ar['children'];

		 	
		 //liv1
		 $cod_liv = $f_liv1;
		 if (!isset($d_ar[$cod_liv]))
				$d_ar[$cod_liv] = array("cod" => $cod_liv, "descr"=>$cod_liv,
						"val" => array(),
						"itinerario" => $s->decod_std('ITIN', $r['TDCITI']),
						"vettore" => $s->decod_std('AUTR', trim($r['CSCVET'])),
						"vmc"		=> $s->des_vmc_sp($r),
						"TDDTEP" => $r['TDDTEP'],
						"TDNBOC" => $r['TDNBOC'], "CSNSPC" => $r['CSNSPC'],
						"CSTISP" => $r['CSTISP'], "CSTITR" => $r['CSTITR'],
						"CSDTIC" => $r['CSDTIC'], "CSHMIC" => $r['CSHMIC'],
						"CSKMTR" => $r['CSKMTR'], "CSPORT" => $r['CSPORT'],
						"children"=>array());

				$tmp_ar = &$d_ar[$cod_liv];
				$tmp_ar["val"]['VOLUME'] += $r['S_VOLUME']  * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
				$tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
				$tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
				$tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
				$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
				$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;

				$d_ar = &$tmp_ar['children'];
					

				//liv2 //CLIENTE
				$cod_liv = $f_liv2;
				if (!isset($d_ar[$cod_liv]))
					$d_ar[$cod_liv] = array("cod" => $r[$f_liv2], 
							"descr"=>trim($r['TDDCON']) . " (" . implode(", ", array(trim($r['TDDLOC']), trim($r['TDIDES']), trim($r['TDPROD']), trim($r['TDNAZD']))) . ")",
							//"note_carico" => note_carico($r),
							
							$campo_data => $r[$campo_data],
				
							"TDTPCA" => $r['TDTPCA'],
							"TDAACA" => $r['TDAACA'],
							"TDNRCA" => $r['TDNRCA'],
							"val"  => array(), "children"=>array());
					$tmp_ar = &$d_ar[$cod_liv];
					$tmp_ar["val"]['VOLUME'] += $r['S_VOLUME']  * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
					$tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
					$tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
					$tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
					$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
					$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
					$d_ar = &$tmp_ar['children'];
					
					
					require_once("acs_booking.php");
					
					$sql_1 = "SELECT PSFG01
					FROM {$cfg_mod_Spedizioni['file_testate']} TD
					LEFT OUTER JOIN {$cfg_mod_Spedizioni['file_carichi']} PS ON PSTAID = 'PRGS' AND PSDT = TDDT AND PSTPCA = TDTPCA AND PSAACA = TDAACA AND PSNRCA = TDNRCA
					WHERE TDTPCA ='{$r['TDTPCA']}' AND TDAACA = '{$r['TDAACA']}' AND TDNRCA = '{$r['TDNRCA']}'";
					
					$stmt_1 = db2_prepare($conn, $sql_1);
					echo db2_stmt_errormsg();
					$result = db2_execute($stmt_1);
					$row1 = db2_fetch_assoc($stmt_1);
					$pagamento = $row1['PSFG01'];
					if($r['TDAACA'] > 0){
					   //$img_pagamento = get_image_per_carico($pagamento);
					    //$img_pagamento = $s->get_image_per_carico($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA'], $row1['PSFG01']);
					    $code_stato_pagamento = $s->get_stato_pagamento_carico($r['TDTPCA'], $r['TDAACA'], $r['TDNRCA'], $row1['PSFG01']);
					    $img_pagamento = " <div class='img_stato_pagamento img_stato_pagamento_{$code_stato_pagamento}'>&nbsp;</div>";
					}
					else 
					    $img_pagamento = "";
					
					//icona ha booking?
					if ($s->ha_booking_by_sped($r['TDNBOC']))
					    //$icona_booking = '<span style="display: inline"><img class="cell-img" src=' . img_path("icone/48x48/globe.png") . ' height=15 ></a></span>';
					    $icona_booking = ' <b>[Booking]</b>';
					else
					   $icona_booking = '';
					
					//liv2_carico // 	CARICO
					$cod_liv = $f_liv2_carico;
					if (!isset($d_ar[$cod_liv]))
						$d_ar[$cod_liv] = array("cod" => $r[$f_liv2_carico],
						        "descr"=> $cod_liv.$img_pagamento.$icona_booking,
								"note_carico" => note_carico($r),
									
								$campo_data => $r[$campo_data],
					
								"TDTPCA" => $r['TDTPCA'],
								"TDAACA" => $r['TDAACA'],
								"TDNRCA" => $r['TDNRCA'],
								"val"  => array(), "children"=>array());
					
								$commento_txt = 
								$tmp_ar = &$d_ar[$cod_liv];
								$tmp_ar["val"]['VOLUME'] += $r['S_VOLUME']  * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
								$tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
								$tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
								$tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
								$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
								$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
								$d_ar = &$tmp_ar['children'];
							
							
								$stato_pagamento_ordine = $s->get_stato_pagamento_ordine($r['TDDOCU'], $r['TDFG06']);
								$i_paga = $s->gest_image_pagamento($stato_pagamento_ordine);
								if (strlen($i_paga) > 0)
								    //$img_paga = '<span style="display: inline"><img class="cell-img" src=' . img_path($i_paga) . ' height=15 ></a></span>';
								    $img_paga = " <div class='img_stato_pagamento img_stato_pagamento_{$stato_pagamento_ordine}'>&nbsp;</div>";
							    else 
							        $img_paga = "";

					//liv3 //ORDINE
					$cod_liv = $f_liv3;
					if (!isset($d_ar[$cod_liv]))
						$d_ar[$cod_liv] = array("cod" => $r[$f_liv2], 
						    "descr"=>trim($r['TDOADO']) . "_" . trim($r['TDONDO']) . " " . trim($r['TDMODI']) . " " . trim($r['TDOTPD']). " ".$img_paga, 
								$campo_data => $r[$campo_data],
								"TDTPCA" => $r['TDTPCA'],
								"TDAACA" => $r['TDAACA'],
								"TDNRCA" => $r['TDNRCA'],
								"val"  => array(), "children"=>array());
						$tmp_ar = &$d_ar[$cod_liv];
						$tmp_ar["val"]['VOLUME'] += $r['S_VOLUME']  * ( (100 + (int)$_REQUEST['perc_magg_volume']) / 100);
						$tmp_ar["val"]['COLLI'] += $r['S_COLLI'] ;
						$tmp_ar["val"]['PALLET'] += $r['S_PALLET'] ;
						$tmp_ar["val"]['PESOL'] += $r['S_PESOL'] ;
						$tmp_ar["val"]['IMPORTO'] += $r['S_IMPORTO'] ;
						$tmp_ar["val"]['CLIENTI_DEST'] += $r['C_CLIENTI_DEST'] ;
					
						$tmp_ar["row"] = $r;
							

} //while


//echo "<pre>";print_r($ar);exit;





echo "<table class=int1>";

if($_REQUEST['f_bilingue']=='Y'){
	
	
	echo "
					<tr>
					 <th>Cliente/Customer</th>
		   		     <th>Ordine/Order</th>";
	
	if($_REQUEST['f_pagamento'] =='Y')
	    echo "<th>Pagamento/Payment</th>";
	 
		   		    echo "<th>Carico/Shipping Reference</th>
		   			";
	
	if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
		echo "
		   					 <th >Codice</th>
							 <th >Descrizione</th>
							 <th >UM</th>
							 <th class=number>Q.t&agrave;</th>";
	}
	echo "
							 <th class=number>Colli/Items</th>
							 <th class=number>Vol.MC</th>
							 <th class=number>Peso L.KG/<br>Gross W.KG</th>
		   					 <th class=number>Importo/Amount</th>";
	
	
}else{

		echo "
					<tr>
					 <th>Data/Area/Itinerario/Cliente</th>
		   		      <th>Ordine</th>";
		
            		if($_REQUEST['f_pagamento'] =='Y')
            		    echo "<th>Pagamento</th>";
            		echo "<th>Carico</th>";
		   			
		if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			echo "
		   					 <th >Codice</th>
							 <th >Descrizione</th>
							 <th >UM</th>
							 <th class=number>Q.t&agrave;</th>";
		}   			
		echo "
							 <th class=number>Colli</th>
							 <th class=number>Volume</th>
							 <th class=number>Peso</th>
		   					 <th class=number>Importo</th>";	

}

echo "</tr>";




$cl_liv_cont = 0;
if ($_REQUEST['dettaglio_per_carico'] == "Y") $liv2_row_cl = ++$cl_liv_cont;
$liv1_row_cl = ++$cl_liv_cont;



$liv_data_colspan = 3;
$liv_area_colspan = 3;
$r_colspan_carico = 3;
$col_dettaglio    = 4;
if ($_REQUEST['dettaglio_per_carico'] == "Y" || $_REQUEST['el_dettaglio_per_cliente'] == "Y") $r_colspan = 4; else $r_colspan=3;
if ($_REQUEST['dettaglia_km_sped'] == "Y") {$r_colspan++; $r_colspan_carico++;}
if ($_REQUEST['mostra_tipologie'] == "Y") {$r_colspan+=2; $r_colspan_carico+=2;}
if ($_REQUEST['mostra_pallet'] == "Y") $col_dettaglio++;
if ($_REQUEST['mostra_importo'] == "Y") $col_dettaglio++;

if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
	$liv_data_colspan +=4;
}

if ($_REQUEST['f_pagamento'] == 'Y'){
    $liv_data_colspan ++;
}

if(is_array($ar)){
foreach ($ar as $kgg => $gg){
    
    if($gg['hold'] == 'N') 
    echo "<tr style=\"page-break-after: always;\"></tr> ";

	echo "<tr class=ag_liv_data><th colspan={$liv_data_colspan}>";
	
	    if($_REQUEST['f_bilingue']=='Y'){
	        if($gg['hold'] == 'N') 
	            echo $gg['cod']. '/Hold orders';
	         else
	            echo ucfirst(print_date_bl($gg['cod'], "%A %d/%m"));
	     
	    }else{
	        if($gg['hold'] == 'N')
	            echo $gg['cod'];
	        else
	            echo ucfirst(print_date($gg['cod'], "%A %d/%m"));
	     
	    }	
	
	
		
	echo "
	 </th>
	  	  			<th class=number>" . $gg['val']['COLLI'] . "</th>
			  		<th class=number>" . n($gg['val']['VOLUME'],2) . "</th>
	  	  			<th class=number>" . n($gg['val']['PESOL'],2) . "</th>	
					<th class=number>" . n($gg['val']['IMPORTO'], 2) . "</th>";
	echo "</tr>";
	 
	 
	foreach ($gg['children'] as $kl0 => $l0){

		//AREA SPEDIZIONE
		$_REQUEST['dettaglio_per_area_spedizione'] = 'Y';
		if ($_REQUEST['dettaglio_per_area_spedizione'] == "Y"){

			echo "<tr class=ag_liv_area><th colspan={$liv_data_colspan}>" . ucfirst($l0['descr']) . "</th>";
			
			if ($_REQUEST['f_disabilita_tot_area_itin'] == 'Y')
				echo "<th colspan=4>&nbsp;</th>";
			else
				echo "
					  	  			<th class=number>" . $l0['val']['COLLI'] . "</th>
					  	  			<th class=number>" . n($l0['val']['VOLUME'],2) . "</th>
					  	  			<th class=number>" . n($l0['val']['PESOL'],2) . "</th>
									<th class=number>" . n($l0['val']['IMPORTO'], 2) . "</th>";
			echo "</tr>";
		}

		 
		usort($l0['children'], "cmp_el_l1");

		foreach ($l0['children'] as $kl1 => $l1){

			//ITINERARIO
			echo "<tr class=ag_liv2>						
					<td colspan={$liv_data_colspan}>" . $l1['itinerario'] . "</td>";


						if ($_REQUEST['f_disabilita_tot_area_itin'] == 'Y')
							echo "<td colspan=4>&nbsp;</td>";
						else			
							echo "
								<td class=number>" . $l1['val']['COLLI'] . "</td>
				  				<td class=number>" . n($l1['val']['VOLUME'], 2) . "</td>
				  				<td class=number>" . n($l1['val']['PESOL'], 2) . "</td>
				  				<td class=number>" . n($l1['val']['IMPORTO'], 2) . "</td>";
						
				  		echo "</tr>";

				  			// CLIENTE
				  			foreach ($l1['children'] as $kl2 => $l2){
				  				
				  				  
				  				
						  		  echo "<tr class=ag_liv1>
						  		  
						  		  <td colspan={$liv_data_colspan}>" . $l2['descr'] . "</td>";
						  						 
									echo "
										<td class=number>" . $l2['val']['COLLI'] . "</td>
				  						<td class=number>" . n($l2['val']['VOLUME'], 2) . "</td>
				  						<td class=number>" . n($l2['val']['PESOL'], 2) . "</td>
				  						<td class=number>" . n($l2['val']['IMPORTO'], 2) . "</td>";			
						  		    echo "</tr>";
						  		    
						  		    foreach ($l2['children'] as $kl3 => $l3){
						  		    	

						  		    	echo "<tr class=ag_liv_el_cliente>";
						  		    	
						  		    	if($_REQUEST['f_note'] =='Y')
						  		    	    echo "<td colspan={$liv_data_colspan}>" . $l3['descr'] . " [".$l3['note_carico']."]</td>";
						  		    	else
						  		    	    echo "<td colspan={$liv_data_colspan}>" . $l3['descr'] ."</td>";
						  		    	
						  		    	echo "
										<td class=number>" . $l3['val']['COLLI'] . "</td>
				  						<td class=number>" . n($l3['val']['VOLUME'], 2) . "</td>
				  						<td class=number>" . n($l3['val']['PESOL'], 2) . "</td>
				  						<td class=number>" . n($l3['val']['IMPORTO'], 2) . "</td>";
						  		    	echo "</tr>";



			  		    //DOCUMENTI
			  			if ($_REQUEST['el_dettaglio_per_cliente'] == "Y"){			  			
			  			global $id_ditta_default;
			  						  			
			  		    foreach ($l3['children'] as $kl4 => $l4){
			  		    			
			  		    		echo "<tr class=ag_liv{$liv2_row_cl}>
			  		    					<td>" . trim($l4['row']['TDVSRF']) . "</td>			  		    					
			  		    					<td>" . $l4['descr'] . "</td>";
			  		    		
			  		    		           if($_REQUEST['f_pagamento'] == 'Y'){
			  		    		               if(trim($l4['row']['TDAUX1']) != '')
			  		    		                   echo "<td>[" .trim($l4['row']['TDCPAG']).", ".trim($l4['row']['TDAUX1'])."] ".trim($l4['row']['TDDPAG']) ."</td>";
			  		    		               else 
			  		    		                   echo "<td>[" .trim($l4['row']['TDCPAG'])."] ".trim($l4['row']['TDDPAG']) ."</td>";
			  		    		           }
			  		    					
			  		    					if (strlen(trim($l4['TDTPCA'])) > 0)
			  		    						echo "<td>" . implode("_",  array($l4['TDTPCA'], $l4['TDAACA'], $l4['TDNRCA'])) . "</td>";
			  		    					else 
			  		    						echo "<td>&nbsp;</td>";

			  		    					if ($cfg_mod_Spedizioni['gestione_per_riga'] == 'Y'){
			  		    						echo "
													<td class=number2>" . trim($l4['row']['RDART']) . "</td>
					 								<td class=number2>" . trim($l4['row']['RDDES1']) . "</td>
					 								<td class=number>"  . trim($l4['row']['RDUM']) . "</td>
					 								<td class=number>"  . n($l4['row']['RDQTA'], 0) . "</td>";
			  		    					}
			  		    					

											echo "
												<td class=number>" . $l4['val']['COLLI'] . "</td>
						  						<td class=number>" . n($l4['val']['VOLUME'], 2) . "</td>
						  						<td class=number>" . n($l4['val']['PESOL'], 2) . "</td>
						  						<td class=number>" . n($l4['val']['IMPORTO'], 2) . "</td>";			
								  		    echo "</tr>";
			  		    					
											echo "</tr>";


											}
									}  //documenti
									
									
							//riga di stacco per cliente
							echo "<tr class=ag_liv_el_cliente><td colspan={($liv_data_colspan+4)}>&nbsp;</td></tr>";

					
						  		    }//carico	  		    
					} //cliente

					//stamp l'eventuale riga collegata
					if ($_REQUEST['dettaglio_per_carico'] == "Y" && $l1['CSNSPC'] > 0){
													$col_dettaglio_coll = $col_dettaglio +1;
													echo "<tr class=ag_liv{$liv2_row_cl}>
													<td colspan={$r_colspan_carico} align=right>
													<img src=" . img_path("icone/48x48/link_blu.png") . " width=18> Spedizione collegata:</td>
													<td colspan={$col_dettaglio_coll}>" .  $s->get_el_carichi_by_sped($l1['CSNSPC'], 'Y', 'N', 'Y') . "</td>";
													echo "</tr>";
													}
														
														

													}
													}
													}
}

												
	//STAMPO TOTALE
	
	if($_REQUEST['f_bilingue']=='Y')
		$out_tot_gen = 'TOTALE GENERALE / TOTAL';
	else
		$out_tot_gen = 'TOTALE GENERALE';
													
	echo "
		<tr class=liv_totale>
				<td colspan={$liv_data_colspan}>{$out_tot_gen}</td>
				<td class=number>" . $ar_tot['TOTALI']['COLLI'] . "</td>
				<td class=number>" . n($ar_tot['TOTALI']['VOLUME'], 2) . "</td>
				<td class=number>" . n($ar_tot['TOTALI']['PESOL'], 2) . "</td>
				<td class=number>" . n($ar_tot['TOTALI']['IMPORTO'], 2) . "</td>";
	echo "</tr>";


	echo "</table></div></body></html>";
	
}	
	
if ($_REQUEST['fn'] == 'open_form'){
		$m_params = acs_m_params_json_decode();
		
		?>
	
	{"success":true, "items": [
	
	        {
	            xtype: 'form',
	            bodyStyle: 'padding: 10px',
	            bodyPadding: '5 5 0',
	            frame: true,
	            title: '',
	            //url: 'acs_print_lista_consegne.php',
	            
	            items: [
	            	{
                	xtype: 'hidden',
                	name: 'f_filter',
                	value: '<?php echo acs_je($m_params->filter); ?>'
                },{
                    xtype: 'radiogroup',
                    anchor: '100%',
                    fieldLabel: 'Data',
                    items: [
                        {
                            xtype: 'radio'
                          , name: 'f_sceltadata' 
                          , boxLabel: 'Programmata'
                          , inputValue: 'TDDTEP'
                        },
                        {
                            xtype: 'radio'
                          , name: 'f_sceltadata' 
                          , boxLabel: 'Spedizione'
                          , inputValue: 'TDDTSP'
                          , checked: true                          
                        }
                   ]
                } ,
                		{
							flex: 1,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Bilingue',
							labelAlign: 'left',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_bilingue' 
		                          , boxLabel: ''
		                          , checked: true
		                          , inputValue: 'Y'
		                        }]														
						 }
						 
			            , {
			                fieldLabel: '% magg vol',
			                boxLabel: 'Si',
			                xtype: 'numberfield',	
			                name: 'perc_magg_volume',
			                value: <?php echo (int)$cfg_mod_Spedizioni['perc_magg_volume_std']; ?>
			            }
			            
			            , {
							flex: 1,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Dis. tot per area/itin',
							labelAlign: 'left',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_disabilita_tot_area_itin' 
		                          , boxLabel: ''
		                          , checked: true
		                          , inputValue: 'Y'
		                        }]														
						 }, {
							flex: 1,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Note carico',
							labelAlign: 'left',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_note' 
		                          , boxLabel: ''
		                          //, checked: true
		                          , inputValue: 'Y'
		                        }]														
						 }, {
							flex: 1,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Pagamento',
							labelAlign: 'left',
						   	allowBlank: true,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_pagamento' 
		                          , boxLabel: ''
		                          //, checked: true
		                          , inputValue: 'Y'
		                        }]														
						 }
			            						 
						 
						 
						 /*
						 ,  {
							flex: 1,						 
							xtype: 'checkboxgroup',
							fieldLabel: 'Logo',
							labelAlign: 'left',
						   	allowBlank: true,
						   	labelWidth: 140,
						   	items: [{
		                            xtype: 'checkbox'
		                          , name: 'f_logo' 
		                          , boxLabel: ''
		                          ,checked: false
		                          , inputValue: 'Y'
		                        }]														
						 }         
						 */
	            ],
	            
				buttons: [					
					{
			            text: 'Visualizza',
				        iconCls: 'icon-folder_search-24',		            
				        scale: 'medium',		            
			            handler: function() {
			                this.up('form').submit({
		                        url: '<?php echo $_SERVER['PHP_SELF']; ?>?fn=open_report',
		                        target: '_blank', 
		                        standardSubmit: true,
		                        method: 'POST',
		                        params: {
		                        	form_values: Ext.encode(this.up('form').getValues())
		                        }
		                  });
		                  
		                  this.up('window').close();
			                
			            }
			        } 
		        
		        
		        ]            
	            
	           
	}
		
	]}
	
	
	<?php 
	}
	

	function add_descr_report($desc){
		if (strlen($desc) > 0)
			return ' - ' . $desc;
			else return '';
				
	}
	
	
	function sceltadata($campo_data){
		return "(Data programmazione)";
	}
	
	function sceltacampo_ora($campo_data){
		return "SP.CSHMPG";
	}
	
	
	
	function note_carico($row){
	  global $s;
	    
	    $carico = array();
	    $carico['PSDT'] = $row['TDDT'];
	    $carico['PSAACA'] = $row['TDAACA'];
	    $carico['PSNRCA'] = $row['TDNRCA'];
	   
	    $commento_txt = $s->get_commento_carico($carico, 'CAR');
	    
	    $ret = array();
	    foreach($commento_txt as $k => $v){
	        $ret[] = $v;
	              
	    }
	    
	    return implode(', ', $ret);
	    
	}
	
	
	function out_cliente($row){
		$ret = "<span class=denominazione_cliente>{$row['TDDCON']}</span>";
	
		if ($row['DEST_NAZ_C'] == "ITA"){
			$ind = "{$row['DEST_LOCA']} ({$row['DEST_PROV']})";
		} else {
			$ind = "{$row['DEST_LOCA']} ({$row['DEST_NAZ_D']})";
		}
	
		return implode(", ", array($ret, $ind));
	}
	
	
	function cmp_el_l1($a, $b)
	{
		//elenco: ordini it/ve per data
	
		if ($_REQUEST['evidenzia_inizio_carico'] == 'Y'){
			return strcmp("{$a['CSDTIC']}", "{$b['CSDTIC']}");
			if ($a['CSDTIC'] == 0) $a_cmp = $a['DATA'];
			else $a_cmp = $a['CSDTIC'];
			if ($b['CSDTIC'] == 0) $b_cmp = $b['DATA'];
			else $b_cmp = $b['CSDTIC'];
	
			return strcmp($a_cmp . sprintf("%06s", $a['CSHMIC']), $b_cmp . sprintf("%06s", $b['CSHMIC']));
	
		}
	
		return strcmp(strtoupper($a["itinerario"] . $a["vmc"] . $a["CSHMPG"]) , strtoupper($b["itinerario"] . $b["vmc"] . $b["CSHMPG"]));
	}
	
	


?>		