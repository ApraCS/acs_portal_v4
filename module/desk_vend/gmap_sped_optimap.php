<?php

require_once "../../config.inc.php";

$s = new Spedizioni();

?>



<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="core.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="gmaps.js"></script>


 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/google-code-prettify/prettify.js"></script>
 
 <!--  optimap -->
 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/optimap/BpTspSolver.js"></script>
 <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/optimap/tsp.js"></script>  


 <style type="text/css">
  .ragsoc{font-size: 12px; color: gray;}
  ul#error_results{font-size: 14px;}
  
  ul#results{font-size: 14px;}  
 </style>
 
</head>
<body>

  <script type="text/javascript">

  var myMap;
//Your normal Google Map object initialization

//  var myOptions = {
//    zoom: 7,
//    center: new google.maps.LatLng(43.9124756, 12.915549400000032),    
//    mapTypeId: google.maps.MapTypeId.ROADMAP
//  };
//  myMap = new google.maps.Map('map', myOptions);


    $(document).ready(function(){

    	  jQuery("#dialogProgress" ).dialog({
    		    height: 140,
    			modal: true,
    			autoOpen: false
    		  });    	
        
        myMap = new GMaps({
            div: '#map',
            lat: 43.9124756,
            lng: 12.915549402
          });

        directionsPanel = document.getElementById("my_textual_div");        

        // Create the tsp object
        tsp = new BpTspSolver(myMap, directionsPanel);

        // Set your preferences
        tsp.setAvoidHighways(false);
        tsp.setTravelMode(google.maps.DirectionsTravelMode.WALKING);        

        // Add points (by coordinates, or by address).
        // The first point added is the starting location.
        // The last point added is the final destination (in the case of A - Z mode)
        tsp.addWaypoint(new google.maps.LatLng(43.9124756, 13.91554940003), addWaypointCallback);  // Note: The callback is new for version 3, to ensure waypoints and addresses appear in the order they were added in.
        tsp.addWaypoint(new google.maps.LatLng(44.9124756, 14.91554940003), addWaypointCallback);  // Note: The callback is new for version 3, to ensure waypoints and addresses appear in the order they were added in.  
      //  tsp.addAddress(address, addAddressCallback);

        // Solve the problem (start and end up at the first location)
        tsp.solveRoundTrip(onSolveCallback);
      
    });



      








  
  // Or, if you want to start in the first location and end at the last,
  // but don't care about the order of the points in between:
//  tsp.solveAtoZ(onSolveCallback);
  

  // Retrieve the solution (so you can display it to the user or do whatever :-)
//  var dir = tsp.getGDirections();  // This is a normal GDirections object.
  // The order of the elements in dir now correspond to the optimal route.

  // If you just want the permutation of the location indices that is the best route:
//  var order = tsp.getOrder();

  // If you want the duration matrix that was used to compute the route:
//  var durations = tsp.getDurations();

  // There are also other utility functions, see the source.


  function addWaypointCallback(result){
	  //console.log(result);
  }
   
  </script>

    <div id="body">
    <div class="row">
      <div class="span11">
        <div class="popin">
	      <div class="span6" style="float: left; width: 300px; height: 500px; overflow: scroll;'>
	      
 
	        <h3 style='display: none;'>Destinazioni mancanti:</h3>
	        <ul style='display: none;' id="error_results"></ul>
 	      
	      
	        <h3>Elenco destinazioni:</h3>
	        <ul id="results"></ul>

	        <h3>Coordinate:</h3>
	        <div id="optimap_results">
	         <?php echo $s->get_start_map('Y'); ?>
	        </div>
	        <div><a href="http://gebweb.net/optimap/" target="_blank">Vai a Optimap</a></div>	        
	        
	      </div>        
          <div id="map" style='width: 800px; height: 600px;' class="map"></div>
		  <div id="path" class="pathdata"></div>
		  <div id="my_textual_div"></div>   
		  <div id="dialogProgress" title="Calculating route...">			         
        </div>
      </div>
      
    </div>
	</div>
	
<h3><a href="#" class='accHeader'>Export</a></h3>
  <div>
    <div id="exportGoogle"></div>
    <div id="exportDataButton"></div>
    <div id="exportData"></div>
    <div id="exportLabelButton"></div>
    <div id="exportLabelData"></div>
    <div id="exportAddrButton"></div>
    <div id="exportAddrData"></div>
    <div id="exportOrderButton"></div>
    <div id="exportOrderData"></div>
    <div id="garmin"></div>
    <div id="tomtom"></div>
    <div id="durations" class="pathdata"></div>
    <div id="durationsData"></div>
  </div>	
  
 <h3><a href="#" class='accHeader'>Edit Route</a></h3>
  <div>
    <div id="routeDrag"></div>
    <div id="reverseRoute"></div>
  </div>  
	
</body>
</html>
