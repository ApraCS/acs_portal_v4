<?php

require_once("../../config.inc.php");

$m_params = acs_m_params_json_decode();

error_reporting(E_ERROR);
ini_set('display_errors','On');


// emogrifier function
$xmldoc = new DOMDocument();
$xmldoc->strictErrorChecking = false;
$xmldoc->formatOutput = true;
$xmldoc->loadHTML(utf8_decode($m_params->html_text));
$xmldoc->normalizeDocument();

$html = $xmldoc->saveHTML();

$body = acs_emogrify($html, utf8_decode($m_params->style_text));

echo $body;

?>
